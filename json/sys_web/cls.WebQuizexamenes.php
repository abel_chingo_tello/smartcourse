<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_SITIO') or die();
JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
class WebQuizexamenes extends JrWeb
{


    public function __construct()
    {        
        parent::__construct(); 
        $this->oNegQuizexamenes = new NegQuizexamenes;
    }

    public function defecto(){
        if(false === NegSesion::existeSesion()){
            header('Location: '.URL_BASE);
            exit();
        }
        return $this->listar();
    }

    public function listar(){
        try{
            $user=NegSesion::getUsuario();
            $this->idrol=$user["idrol"];
            $this->tuser=$user["tuser"];
            $this->idusuario=$user["idpersona"];
            $this->idproyecto=$user["idproyecto"];
            $examenes=array();
            $filtros= array();
            if(!empty($_REQUEST["titulo"]))$filtros["titulo"]=$_REQUEST["titulo"];
            if($this->idrol==1 && $this->tuser=="s"){
                $examenes=$this->oNegQuizexamenes->buscar($filtros);
            }elseif($this->idrol==1){
                $persona=$this->oNegQuizexamenes->persona();
                $filtros["idproyecto"]=$persona["quiz_idproyecto"];               
                $examenes=$this->oNegQuizexamenes->buscar($filtros);
            }elseif($this->idrol==2){
                $persona=$this->oNegQuizexamenes->persona();
                $filtros["idpersonal"]=$persona['quiz_idpersona']; 
                $filtros["idproyecto"]=$persona["quiz_idproyecto"];                           
                $examenes=$this->oNegQuizexamenes->buscar($filtros);
            }else{
                echo json_encode(array('code'=>'Error','msj'=>'No tiene permisos'));       
                exit(0);
            }
            echo json_encode(array('code'=>200,'datos'=>$examenes,'total'=>count($examenes)));
            exit(0);
        }catch(Exception $ex){
            echo json_encode(array('code'=>'Error','msj'=>$ex->getMessage()));       
            exit(0);
        }
    }
   
    public function duplicar(){
        try{
            $idexamen=!empty($_REQUEST["idexamen"])?$_REQUEST["idexamen"]:0;
            if(empty($idexamen)){
                echo json_encode(array('code'=>'Error','msj'=>'idexamen no definido'));       
                exit(0);
            }
            $datos=$this->oNegQuizexamenes->duplicar($idexamen);
            echo json_encode(array('code'=>200,'datos'=>$datos));
            exit(0);
        }catch(Exception $ex){
            echo json_encode(array('code'=>'Error','msj'=>$ex->getMessage()));       
            exit(0);
        }
    }

    public function guardar(){
        try{

            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            global $aplicacion;       
            $usuarioAct = NegSesion::getUsuario(); 
            $persona=$this->oNegQuizexamenes->persona();

            if(@$calificacionen==='A') $txttotal=@$txtAlfanumerico;
            else $txttotal=@$calificaciontotal;
            $txtportada=@str_replace($this->documento->getUrlBase()."/smartquiz/",'__xRUTABASEx__',$portada);
            $txtportada=@str_replace($this->documento->getUrlBase()."smartquiz/",'__xRUTABASEx__',$portada);
            /*$this->oNegExamenes->__set('idnivel',@$nivel);
            $this->oNegExamenes->__set('idunidad',@$unidad);
            $this->oNegExamenes->__set('idactividad',@$actividad);*/
            $datosexamen=array();
            $datosexamen['idexamen']=@$idexamen;
            $datosexamen['titulo']=@$titulo;
            $datosexamen['descripcion']=@$descripcion;
            $datosexamen['portada']=@$txtportada;
            $datosexamen['fuente']=@$fuente;
            $datosexamen['fuentesize']=@$fuentesize;
            $datosexamen['tipo']=@$tipo;
            $datosexamen['idioma']=!empty($idioma)?$idioma:'ES';
            $datosexamen['aleatorio']=@$aleatorio;
            $datosexamen['calificacion_por']=@$calificacionpor;
            $datosexamen['calificacion_en']=@$calificacionen;        
            $datosexamen['calificacion_total']=$txttotal;
            $datosexamen['calificacion_min']=!empty($calificacionmin)?$calificacionmin:0;
            $datosexamen['tiempo_por']=@$tiempopor;
            $datosexamen['tiempo_total']=@$tiempototal;
            $datosexamen['estado']=!empty($estado)?$estado:0;
            $datosexamen['nintento']=@$nintento;
            $datosexamen['calificacion']=@$calificacion;
            $datosexamen['habilidades_todas']=@$habilidades_todas;
            $datosexamen['origen_habilidades']= (@$tipo=='G')?'JSON':@$origen_habilidades;
            $datosexamen['fuente_externa']=@$fuente_externa;
            $datosexamen['tiene_certificado']=@$tiene_certificado;
            $datosexamen['idproyecto']=$persona["quiz_idproyecto"];
            $datosexamen['idpersona_smartcourse']=@$usuarioAct["idpersona"];
            $datosexamen['retroceder']=!empty($retroceder)?$retroceder:0;
            $datosexamen['tiempoalinicio']=!empty($tiempoalinicio)?$tiempoalinicio:0;
           
            /* Guardar el archivo de certificado */
            if(!empty(@$nombre_certificado)){
                $file = $_FILES["file_certificado"];
                $ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
                $tipo_permitido=array('jpg', 'jpeg','gif','png');
                if(!in_array($ext, $tipo_permitido)){
                    $msjeError = 'El certificado no tiene extensión de imagen.';
                }
                try{
                    $dir_certificados = RUTA_BASE.'smartquiz'.SD.'static'.SD.'img'.SD.'certificados'.SD ;
                    @mkdir($dir_certificados, 0775, true);
                    @chmod($dir_certificados, 0775);
                    $nombrefile=$file["name"];
                    if(move_uploaded_file($file["tmp_name"], $dir_certificados.$nombrefile)) {
                        $datosexamen['nombre_certificado']=str_replace(RUTA_BASE.'smartquiz'.SD, '__xRUTABASEx__'.SD, $dir_certificados.$nombrefile);
                        $msjeError = null;
                    }
                }catch(Exception $e) {
                    $msjeError = 'No se pudo subir el Certificado.';
                }
            }
            $datosexamen['idpersonal']=$persona['quiz_idpersona'];           
            $res=$this->oNegQuizexamenes->guardar($datosexamen);
            echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Assessment')).' '.JrTexto::_('saved successfully'),'new'=>$res)); //
            exit(0);
        }catch(Exception $ex){
            echo json_encode(array('code'=>'Error','msj'=>$ex->getMessage()));       
            exit(0);
        }
    }

    public function eliminar(){
        try {
            if(empty($_REQUEST)){ 
                echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
                exit(0);
            }
            //$this->oNegExamenes->idexamen=;
            $res=$this->oNegQuizexamenes->eliminar($_REQUEST['idexamen']);           
            echo json_encode(array('code'=>200,'msj'=>'Exámen Eliminado'));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
            exit(0);
        }
    }
}