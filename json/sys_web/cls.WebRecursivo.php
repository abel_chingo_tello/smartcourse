<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRecursivo', RUTA_BASE);
class WebRecursivo extends JrWeb
{
	private $oNegRecursivo;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRecursivo = new NegRecursivo;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Recursivo', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["comprobar"])&&@$_REQUEST["comprobar"]!='')$filtros["comprobar"]=$_REQUEST["comprobar"];
			if(isset($_REQUEST["fechaejecucion"])&&@$_REQUEST["fechaejecucion"]!='')$filtros["fechaejecucion"]=$_REQUEST["fechaejecucion"];
			if(isset($_REQUEST["fechafinalizacion"])&&@$_REQUEST["fechafinalizacion"]!='')$filtros["fechafinalizacion"]=$_REQUEST["fechafinalizacion"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRecursivo->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idrecursivo)) {
				$this->oNegRecursivo->idrecursivo = $idrecursivo;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRecursivo->comprobar=@$comprobar;
				$this->oNegRecursivo->fechaejecucion=@$fechaejecucion;
				$this->oNegRecursivo->fechafinalizacion=@$fechafinalizacion;
				
            if($accion=='_add') {
            	$res=$this->oNegRecursivo->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursivo')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRecursivo->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursivo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRecursivo->__set('idrecursivo', $_REQUEST['idrecursivo']);
			$res=$this->oNegRecursivo->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRecursivo->setCampo($_REQUEST['idrecursivo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}