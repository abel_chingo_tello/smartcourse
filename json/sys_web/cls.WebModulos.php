<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegModulos', RUTA_BASE);
class WebModulos extends JrWeb
{
	private $oNegModulos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegModulos = new NegModulos;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Modulos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["icono"])&&@$_REQUEST["icono"]!='')$filtros["icono"]=$_REQUEST["icono"];
			if(isset($_REQUEST["link"])&&@$_REQUEST["link"]!='')$filtros["link"]=$_REQUEST["link"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			// $this->datos=$this->oNegModulos->buscar($filtros);
			$datos_=$this->oNegModulos->buscar($filtros);
			// var_dump($datos_);
			$this->datos = array();
			foreach ($datos_ as $key => $value) {				
				$nombretmp=str_replace('Mis notas','My records',$value["nombre"]);
				$nombretmp=str_replace('Comunidad','Community',$nombretmp);
				$nombretmp=str_replace('Configuración','Setting',$nombretmp);
				$nombretmp=str_replace('Exámenes Externos','External exams',$nombretmp);
				$nombretmp=str_replace('Mi portafolio','My portfolio',$nombretmp);
				$nombretmp=str_replace('Currículo','Curriculum',$nombretmp);
				$nombretmp=str_replace('Familia','Family',$nombretmp);
				$nombretmp=str_replace('Eventos','Events',$nombretmp);
				$nombretmp=str_replace('Participantes','Participants',$nombretmp);
				$nombretmp=str_replace('Revisar Exámenes','Review Exams',$nombretmp);
				$nombretmp=str_replace('Revisar Smartbook','Review Smartbook',$nombretmp);
				$nombretmp=str_replace('Activación de códigos','Activation of codes',$nombretmp);
				$nombretmp=str_replace('Foros','Forums',$nombretmp);
				
				
				$this->datos[] = array_replace($value, array("nombretraducido" => ucfirst(JrTexto::_($nombretmp))));
			}
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idmodulo)) {
				$this->oNegModulos->idmodulo = $idmodulo;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegModulos->nombre=@$nombre;
				$this->oNegModulos->icono=@$icono;
				$this->oNegModulos->link=@$link;
				$this->oNegModulos->estado=@$estado;
				
            if($accion=='_add') {
            	$res=$this->oNegModulos->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Modulos')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegModulos->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Modulos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegModulos->__set('idmodulo', $_REQUEST['idmodulo']);
			$res=$this->oNegModulos->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegModulos->setCampo($_REQUEST['idmodulo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}