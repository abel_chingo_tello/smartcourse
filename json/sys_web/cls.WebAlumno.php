<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE);
class WebAlumno extends JrWeb
{
	private $oNegAlumno;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAlumno = new NegAlumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}


			$filtros=array();
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["foto"])&&@$_REQUEST["foto"]!='')$filtros["foto"]=$_REQUEST["foto"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAlumno->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$dni)) {
				$this->oNegAlumno->dni = $dni;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();           	           		        
			$this->oNegAlumno->ape_paterno=@$ape_paterno;
			$this->oNegAlumno->ape_materno=@$ape_materno;
			$this->oNegAlumno->nombre=@$nombre;
			$this->oNegAlumno->fechanac=@$fechanac;
			$this->oNegAlumno->sexo=@$sexo;
			$this->oNegAlumno->estado_civil=@$estado_civil;
			$this->oNegAlumno->ubigeo=@$ubigeo;
			$this->oNegAlumno->urbanizacion=@$urbanizacion;
			$this->oNegAlumno->direccion=@$direccion;
			$this->oNegAlumno->telefono=@$telefono;
			$this->oNegAlumno->celular=@$celular;
			$this->oNegAlumno->email=@$email;
			$this->oNegAlumno->idugel=@$idugel;
			$this->oNegAlumno->regusuario=@$regusuario;
			$this->oNegAlumno->regfecha=@$regfecha;
			$this->oNegAlumno->usuario=@$usuario;
			$this->oNegAlumno->clave=@$clave;
			$this->oNegAlumno->token=@$token;
			$this->oNegAlumno->foto=@$foto;
			$this->oNegAlumno->estado=@$estado;
			$this->oNegAlumno->situacion=@$situacion;
				
            if($accion=='_add') {
            	$res=$this->oNegAlumno->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAlumno->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAlumno->__set('dni', $_REQUEST['dni']);
			$res=$this->oNegAlumno->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAlumno->setCampo($_REQUEST['dni'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}