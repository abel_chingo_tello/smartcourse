<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-03-2020 
 * @copyright	Copyright (C) 31-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegFamilia_slider', RUTA_BASE);
class WebFamilia_slider extends JrWeb
{
	private $oNegFamilia_slider;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegFamilia_slider = new NegFamilia_slider;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Familia_slider', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["codigo"])&&@$_REQUEST["codigo"]!='')$filtros["codigo"]=$_REQUEST["codigo"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["link"])&&@$_REQUEST["link"]!='')$filtros["link"]=$_REQUEST["link"];
			if(isset($_REQUEST["fecha_hora"])&&@$_REQUEST["fecha_hora"]!='')$filtros["fecha_hora"]=$_REQUEST["fecha_hora"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegFamilia_slider->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
			}
			// var_dump(empty($_FILES));
			// var_dump($_POST);exit();
			// @extract($_POST);
			if(!empty($_FILES)){
				$usuarioAct = NegSesion::getUsuario();
				$estados = array(
					"idpersona" => $usuarioAct["idpersona"]
					,"idproyecto" => $usuarioAct["idproyecto"]
					,"idempresa" => $usuarioAct["idempresa"]
					,"link" => ""
					,"fecha_hora" => date("Y-m-d H:i:s")
				);
				$res=$this->oNegFamilia_slider->agregar($estados);
				$link = NegTools::subirArchivo($_FILES["archivo"], $_POST["directorio"], $res);
				$this->oNegFamilia_slider->setCampo($res, "link", $link);
				echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Imagen Guardada Correctamente'),'newid'=>$res)); 
			} else {
				echo json_encode(array('code'=>300,'msj'=>JrTexto::_('Imagen No Guardada'))); 
			}
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegFamilia_slider->__set('codigo', $_REQUEST['codigo']);
			$res=$this->oNegFamilia_slider->eliminar();
			// var_dump(URL_BASE."static/media/familia-slider/".$res);
			unlink(RUTA_BASE . 'static' . SD . 'media' . SD . 'familia-slider' . SD .$res);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Eliminado Correctamente')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegFamilia_slider->setCampo($_REQUEST['codigo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}