<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegLibre_palabras', RUTA_BASE);
class WebLibre_palabras extends JrWeb
{
	private $oNegLibre_palabras;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegLibre_palabras = new NegLibre_palabras;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Libre_palabras', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idtema"])&&@$_REQUEST["idtema"]!='')$filtros["idtema"]=$_REQUEST["idtema"];
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros["audio"]=$_REQUEST["audio"];
			if(isset($_REQUEST["idagrupacion"])&&@$_REQUEST["idagrupacion"]!='')$filtros["idagrupacion"]=$_REQUEST["idagrupacion"];
			if(isset($_REQUEST["url_iframe"])&&@$_REQUEST["url_iframe"]!='')$filtros["url_iframe"]=$_REQUEST["url_iframe"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegLibre_palabras->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idpalabra)) {
				$this->oNegLibre_palabras->idpalabra = $idpalabra;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegLibre_palabras->idtema=@$idtema;
				$this->oNegLibre_palabras->palabra=@$palabra;
				$this->oNegLibre_palabras->audio=@$audio;
				$this->oNegLibre_palabras->idagrupacion=@$idagrupacion;
				$this->oNegLibre_palabras->url_iframe=@$url_iframe;
				
            if($accion=='_add') {
            	$res=$this->oNegLibre_palabras->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Libre_palabras')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegLibre_palabras->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Libre_palabras')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegLibre_palabras->__set('idpalabra', $_REQUEST['idpalabra']);
			$res=$this->oNegLibre_palabras->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegLibre_palabras->setCampo($_REQUEST['idpalabra'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}