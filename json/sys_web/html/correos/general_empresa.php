<?php
defined('RUTA_BASE') or die();
?>
<style>
table {
border-collapse:collapse;
}

table[class="ecxdeviceWidth"] {
width:600px;
max-width:600px;
}

table[class="ecxbtn"] {
font-size:21px;
}

table[class="ecxWidthTableInt_I"] {
max-width:425px !important;
width:100% !important;
text-align:left !important;
}

table[class="ecxWidthTableInt_D"] {
width:100% !important;
max-width:90px !important;
text-align:right !important;
}

@media only screen and (max-width: 600px), screen and (max-device-width: 600px) {
table[class="ecxdeviceWidth"] {
width:440px !important;
padding:0 !important;
}
table[class="ecxbtn"] {
font-size:18px !important;
}
table[class="ecxWidthTableInt_I"] {
width:440px !important;
}
table[class="ecxWidthTableInt_D"] {
width:440px !important;
text-align:left !important;
}
}

@media only screen and (max-width: 479px), screen and (max-device-width: 479px) {
table[class="ecxdeviceWidth"] {
width:280px !important;
padding:0 !important;
}
table[class="ecxWidthTableInt_I"] {
width:280px !important;
text-align:left !important;
}
table[class="ecxWidthTableInt_D"] {
width:280px !important;
text-align:left !important;
}
}
</style>

<table style="  width: 100% !important;  table-layout: fixed; background: #fff;" align="center">
    <caption >
        <?php echo !empty($this->logoempresa)?('<img src="cid:logoempresa" style="max-width: 150px; max-height: 150px; text-align: center;" alt="logo de empresa">'):'';?>
        <br><h2 style="text-align: center;">Correo informativo <?php echo !empty($this->empresa["nombre"])?$this->empresa["nombre"]:'';?></h2></caption>
    <tbody>
        <tr>
            <td>
                <table class="ecxdeviceWidth" style="max-width:600px;" align="center">
                <tbody>
                    <tr><td colspan="3" style="background: #3c8dbc; color: #f9f3f3; text-align: center; padding: 2ex;  /* padding: 6ex; */">
                        <?php echo @$this->Asunto; ?><br><a href="<?php echo $this->documento->getUrlBase(); ?>" style="color: #ffe520;"><b>visitanos Aqui</b></a></td></tr>
                <tr>
                   
                    <td style="padding-bottom:8px;">
                        <table style="color:#353e4a;font-family:Arial,sans-serif;font-size:15px;" align="center" bgcolor="#ffffff" border="0">
                            <tbody>
                                <?php if(!empty($this->para)){?>    
                                <tr>
                                <td style="color:#1e80b6;padding-top:20px;padding-bottom:10px;padding-left:20px;padding-right:20px;">
                                    <?php echo $this->pasarHtml($this->para)?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td style="line-height:24px;padding-left:20px;padding-right:20px;padding-bottom:10px;">
                                        <?php echo $this->mensaje; ?>
                                  	</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr style=" background: #17a2b8;color: #000;"><td colspan="3">
                    <h2>Necesitas Ayuda?</h2>
                    Escribenos a nuestro correo <b><?php echo !empty($this->empresa["correoempresa"])?$this->empresa["correoempresa"]:'info@eduktvirtual.com';?></b><br>
                    Llámanos al <b><?php echo !empty($this->empresa["telefono"])?$this->empresa["telefono"]:'+51 1-435-3738';?></b><br>
                    Contáctanos en Whatsapp al <b><?php echo !empty($this->empresa["telefono"])?$this->empresa["telefono"]:'+51 1-435-3738';?></b>
                </td>
                </tr>
                <tr>                    
                    <td colspan="3" style="color: #717175;font-size: 12px;padding: 1ex;text-align: center;background: #ecece9;">
                    <?php echo $this->pasarHtml(JrTexto::_('Este email se ha generado automáticamente. Por favor, no contestes a este email'))?>.</td>
                               
                </tr>
            </tbody>
          	</table>
            </td>
        </tr>
    </tbody>
</table>