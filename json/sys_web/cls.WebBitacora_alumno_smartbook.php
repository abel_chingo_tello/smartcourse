<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-11-2019 
 * @copyright	Copyright (C) 28-11-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE);
class WebBitacora_alumno_smartbook extends JrWeb
{
	private $oNegBitacora_alumno_smartbook;		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBitacora_alumno_smartbook = new NegBitacora_alumno_smartbook;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bitacora_alumno_smartbook', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idbitacora_smartbook"])&&@$_REQUEST["idbitacora_smartbook"]!='')$filtros["idbitacora_smartbook"]=$_REQUEST["idbitacora_smartbook"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idsesion"])&&@$_REQUEST["idsesion"]!='')$filtros["idsesion"]=$_REQUEST["idsesion"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(!empty($filtros["idusuario"]))
			if($filtros["idusuario"]=='yo'){
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idusuario"]=$usuarioAct["idpersona"];
			}

						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegBitacora_alumno_smartbook->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if(NegSesion::existeSesion()==false){
				echo json_encode(array('code' => 'Error','msj'=>'Sesion  ha sido Cerrado'));
				exit();
			}
			$usuarioAct = NegSesion::getUsuario();  
           	if($usuarioAct["idrol"]!=3){
            	echo json_encode(array('code'=>200,'msj'=>'Solo guardamos bitacoras  para el Alumno.')); 
            	exit();
            }

            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);	

			$this->oNegBitacora_alumno_smartbook->idcurso=@$idcurso;
			$this->oNegBitacora_alumno_smartbook->idsesion=@$idsesion;
			$this->oNegBitacora_alumno_smartbook->tipo=@$tipo;
			$this->oNegBitacora_alumno_smartbook->idusuario=!empty($idusuario)?$idusuario:@$usuarioAct["idpersona"];
			$this->oNegBitacora_alumno_smartbook->estado=!empty($estado)?$estado:'P';
			$this->oNegBitacora_alumno_smartbook->idcomplementario=@$idcomplementario;
			$this->oNegBitacora_alumno_smartbook->regfecha=!empty($regfecha)?$regfecha:date('Y-m-d H:i:s');
			$this->oNegBitacora_alumno_smartbook->idgrupoauladetalle=@$idgrupoauladetalle;
            $res=$this->oNegBitacora_alumno_smartbook->agregar();
            echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Bitacora_alumno_smartbook')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegBitacora_alumno_smartbook->idcurso=@$idcurso;
				$this->oNegBitacora_alumno_smartbook->idsesion=@$idsesion;
				$this->oNegBitacora_alumno_smartbook->idusuario=@$idusuario;
				$this->oNegBitacora_alumno_smartbook->estado=@$estado;
				$this->oNegBitacora_alumno_smartbook->regfecha=@$regfecha;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegBitacora_alumno_smartbook->__set('idbitacora_smartbook', $_REQUEST['idbitacora_smartbook']);
			$res=$this->oNegBitacora_alumno_smartbook->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegBitacora_alumno_smartbook->setCampo($_REQUEST['idbitacora_smartbook'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}