<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE);
class WebNiveles extends JrWeb
{
	private $oNegNiveles;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNiveles = new NegNiveles;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Niveles', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["imagen"])&&@$_REQUEST["imagen"]!='')$filtros["imagen"]=$_REQUEST["imagen"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegNiveles->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idnivel)) {
				$this->oNegNiveles->idnivel = $idnivel;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegNiveles->nombre=@$nombre;
				$this->oNegNiveles->tipo=@$tipo;
				$this->oNegNiveles->idpadre=@$idpadre;
				$this->oNegNiveles->idpersonal=@$idpersonal;
				$this->oNegNiveles->estado=@$estado;
				$this->oNegNiveles->orden=@$orden;
				$this->oNegNiveles->imagen=@$imagen;
				$this->oNegNiveles->descripcion=@$descripcion;
				
            if($accion=='_add') {
            	$res=$this->oNegNiveles->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Niveles')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNiveles->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Niveles')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegNiveles->__set('idnivel', $_REQUEST['idnivel']);
			$res=$this->oNegNiveles->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegNiveles->setCampo($_REQUEST['idnivel'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}