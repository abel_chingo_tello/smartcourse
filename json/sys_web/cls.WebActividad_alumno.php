<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE);
class WebActividad_alumno extends JrWeb
{
	private $oNegActividad_alumno;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegActividad_alumno = new NegActividad_alumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Actividad_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["iddetalleactividad"])&&@$_REQUEST["iddetalleactividad"]!='')$filtros["iddetalleactividad"]=$_REQUEST["iddetalleactividad"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["porcentajeprogreso"])&&@$_REQUEST["porcentajeprogreso"]!='')$filtros["porcentajeprogreso"]=$_REQUEST["porcentajeprogreso"];
			if(isset($_REQUEST["habilidades"])&&@$_REQUEST["habilidades"]!='')$filtros["habilidades"]=$_REQUEST["habilidades"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["html_solucion"])&&@$_REQUEST["html_solucion"]!='')$filtros["html_solucion"]=$_REQUEST["html_solucion"];
			if(isset($_REQUEST["tipofile"])&&@$_REQUEST["tipofile"]!='')$filtros["tipofile"]=$_REQUEST["tipofile"];
			if(isset($_REQUEST["file"])&&@$_REQUEST["file"]!='')$filtros["file"]=$_REQUEST["file"];
			if(isset($_REQUEST["fechamodificacion"])&&@$_REQUEST["fechamodificacion"]!='')$filtros["fechamodificacion"]=$_REQUEST["fechamodificacion"];
			if(isset($_REQUEST["oldid"])&&@$_REQUEST["oldid"]!='')$filtros["oldid"]=$_REQUEST["oldid"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];

						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegActividad_alumno->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idactalumno)) {
				$this->oNegActividad_alumno->idactalumno = $idactalumno;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegActividad_alumno->iddetalleactividad=@$iddetalleactividad;
				$this->oNegActividad_alumno->idalumno=@$idalumno;
				$this->oNegActividad_alumno->fecha=@$fecha;
				$this->oNegActividad_alumno->porcentajeprogreso=@$porcentajeprogreso;
				$this->oNegActividad_alumno->habilidades=@$habilidades;
				$this->oNegActividad_alumno->estado=@$estado;
				$this->oNegActividad_alumno->html_solucion=@$html_solucion;
				$this->oNegActividad_alumno->tipofile=@$tipofile;
				$this->oNegActividad_alumno->file=@$file;
				$this->oNegActividad_alumno->fechamodificacion=@$fechamodificacion;
				$this->oNegActividad_alumno->oldid=@$oldid;
				$this->oNegActividad_alumno->oldido=@$oldido;
				$this->oNegActividad_alumno->idgrupoauladetalle=@$idgrupoauladetalle;
				
            if($accion=='_add') {
            	$res=$this->oNegActividad_alumno->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Actividad_alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegActividad_alumno->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Actividad_alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegActividad_alumno->__set('idactalumno', $_REQUEST['idactalumno']);
			$res=$this->oNegActividad_alumno->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegActividad_alumno->setCampo($_REQUEST['idactalumno'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}