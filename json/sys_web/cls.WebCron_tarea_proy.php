<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2020 
 * @copyright	Copyright (C) 21-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

JrCargador::clase('sys_negocio::NegCron_tarea_proy', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);

class WebCron_tarea_proy extends JrWeb
{
	private $oNegCron_tarea_proy;
	private $oNegAcad_matricula;
	private $oNegAcad_curso;
	private $oNegAcad_grupoauladetalle;
	public function __construct()
	{
		parent::__construct();
		$this->oNegCron_tarea_proy = new NegCron_tarea_proy;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_curso = new NegAcad_curso;
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function runCron()
	{
		try {

			// $_SESSION["check_cron_again"] = false;
			if (NegSesion::existeSesion()) {
				$usuarioAct = NegSesion::getUsuario();
				$filtros = array(
					'estado' => 0,
					'idproyecto' => $usuarioAct['idproyecto']
				);

				$arrCron = $this->oNegCron_tarea_proy->buscar($filtros);

				if (count($arrCron) > 0) {
					$offset = 5 * 60 * 60; //5 horas a segundos.
					$dateFormat = "Y-m-d H:i:s";
					$currentDateTime = date_create(gmdate($dateFormat, time() - $offset));
					foreach ($arrCron as $i_cron => $cron) {
						$cronDateTime = date_create($cron['fecha_hora']);
						if ($cronDateTime <= $currentDateTime) {
							//obtener alumnos
							$boolVencer = true;
							$arrTmp = array(
								"idcurso" => $cron["idcurso"],
								"idcomplementario" => $cron["idcomplementario"],
								"idproyecto" => $usuarioAct['idproyecto']
							);
							if ($cron["idcomplementario"] == 0) { //Posible curso Autoestudio
								$curso = $this->oNegAcad_curso->getCurso($cron["idcurso"]);
								if ($curso["tipo"] == 2) { #2 Eduktmaestro
									#ningún alumno debería estar matriculado directamente..
									$boolVencer = false;
								}
							} else { #complementarios eduktmaestro
								$arrTmp["iddocente"] = $cron["iddocente"]; // el docente/superadmin/admin en sesión que registró la tarea (menu/pestaña)
								if ($cron["idgrupoauladetalle"] > 0) {
									$arrTmp["idgrupoauladetalle"] = $cron["idgrupoauladetalle"];
								}
							}
							if ($boolVencer) {
								/** En el caso de -1 o null, hará que todos venzan */
								$arrInstanciaCurso = $this->getInstanciaCurso($arrTmp, true); //pueden haber varios cursos si el superadmin modifica un curso de autoestudio
								if (count($arrInstanciaCurso) > 0) {
									foreach ($arrInstanciaCurso as $key => $instanciaCurso) {
										$arrAlumnos = $this->oNegAcad_matricula->buscar(array(
											"idgrupoauladetalle" => $instanciaCurso["idgrupoauladetalle"],
											"fechaactiva" => true
										));
										
										foreach ($arrAlumnos as $i_alumno => $alumno) {
											$filtrarTarea=array(
												"sql" => 1,
												"idproyecto" => $cron["idproyecto"],
												"tipo" => $cron["tipo"],
												"idcurso" => $cron["idcurso"],
												"idcomplementario" => $cron["idcomplementario"],
												"idsesion" => $cron["idsesion"],
												"idpestania" => $cron["idpestania"],
												"idgrupoauladetalle" => $alumno["idgrupoauladetalle"],
												"idalumno" => $alumno["idalumno"],
											);
											// echo json_encode($filtrarTarea);exit(0);
											$arrTareasRevisadas = $this->oNegCron_tarea_proy->buscarTarea($filtrarTarea);
											
											if (count($arrTareasRevisadas) == 0) { //por alumno
												// echo "debe vencer almenos 1 alumno";
												//se busca suponer que el alumno todavía no presenta la tarea...
												//asignar nota 0
												$params = array();
												$params["tipo"] = $cron["tipo"];
												$params["idcurso"] = $cron["idcurso"];
												$params["idsesion"] = $cron["idsesion"];
												$params["idpestania"] = $cron["idpestania"];
												$params["idalumno"] = $alumno["idalumno"];
												$params["recurso"] = $cron["recurso"];
												$params["tiporecurso"] = $cron["tiporecurso"];
												$params["nota"] = 0;
												$params["iddocente"] = $cron["iddocente"];
												$params["idproyecto"] = $cron["idproyecto"];
												$params["nombre"] = $cron["nombre"];
												$params["fecha_calificacion"] = gmdate("Y-m-d", time() - $offset);
												$params["idcomplementario"] = $cron["idcomplementario"];
												$params["idgrupoauladetalle"] = $alumno["idgrupoauladetalle"];
												$params["vencido"] = 1;
												$this->oNegCron_tarea_proy->agregarTarea($params);
											}
											//llegados a este punto, puede que el alumno no haya subido la tarea, pero sí haya enviado un mensaje
											//puede que ya haya sido calificado, como que aún no
											//queda en manos del docente poner la nota correspondiente
										}
									}
									$this->oNegCron_tarea_proy->customUpdate(
										array("idcron_tapr" => $cron["idcron_tapr"]),
										array("estado" => 1)
									);
								}
							}
						}
					}
				}
			}
		} catch (Exception $e) {
			// echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getInstanciaCurso($params = array(), $return = false)
	{ //["idgrupoauladetalle","iddocente", "idcurso", "idcomplementario", "idproyecto"]
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();
			$arrCursos = array();
			if (isset($params["idgrupoauladetalle"]) && !empty($params["idgrupoauladetalle"])) {
				$filtros["idgrupoauladetalle"] = $params["idgrupoauladetalle"];
				$arrInstancias = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			} else {
				if (isset($params["iddocente"])) {
					$filtros["iddocente"] = (isset($params["iddocente"]) && !empty($params["iddocente"])) ? $params["iddocente"] : $oUsuario["idpersona"];
				}
				if (isset($params["idcurso"]) && !empty($params["idcurso"])) {
					$filtros["idcurso"] = $params["idcurso"];
					$filtros["idcomplementario"] = isset($params["idcomplementario"]) ? $params["idcomplementario"] : 0;
				}
				$filtros["idproyecto"] = isset($params["idproyecto"]) ? $params["idproyecto"] : $oUsuario["idproyecto"];
				$arrInstancias = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			}
			$arrCursos = $arrInstancias;
			$this->datos = $arrCursos;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Cron_tarea_proy', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idcron_tapr"]) && @$_REQUEST["idcron_tapr"] != '') $filtros["idcron_tapr"] = $_REQUEST["idcron_tapr"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["fecha_hora"]) && @$_REQUEST["fecha_hora"] != '') $filtros["fecha_hora"] = $_REQUEST["fecha_hora"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["idpestania"]) && @$_REQUEST["idpestania"] != '') $filtros["idpestania"] = $_REQUEST["idpestania"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			if (isset($_REQUEST["iddocente"]) && @$_REQUEST["iddocente"] != '') $filtros["iddocente"] = $_REQUEST["iddocente"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["recurso"]) && @$_REQUEST["recurso"] != '') $filtros["recurso"] = $_REQUEST["recurso"];
			if (isset($_REQUEST["tiporecurso"]) && @$_REQUEST["tiporecurso"] != '') $filtros["tiporecurso"] = $_REQUEST["tiporecurso"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegCron_tarea_proy->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idcron_tapr)) {
				$this->oNegCron_tarea_proy->idcron_tapr = $idcron_tapr;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();

			$this->oNegCron_tarea_proy->idproyecto = $usuarioAct['idproyecto'];
			$this->oNegCron_tarea_proy->fecha_hora = @$fecha_hora;
			$this->oNegCron_tarea_proy->estado = @$estado;
			$this->oNegCron_tarea_proy->idsesion = @$idsesion;
			$this->oNegCron_tarea_proy->idpestania = @$idpestania;
			$this->oNegCron_tarea_proy->idcurso = @$idcurso;
			$this->oNegCron_tarea_proy->idcomplementario = @$idcomplementario;
			$this->oNegCron_tarea_proy->iddocente = $usuarioAct['idpersona'];
			$this->oNegCron_tarea_proy->nombre = @$nombre;
			$this->oNegCron_tarea_proy->tipo = @$tipo;
			$this->oNegCron_tarea_proy->recurso = @$recurso;
			$this->oNegCron_tarea_proy->tiporecurso = @$tiporecurso;
			$this->oNegCron_tarea_proy->idgrupoauladetalle = @$idgrupoauladetalle;

			if ($accion == '_add') {
				$res = $this->oNegCron_tarea_proy->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Cron_tarea_proy')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegCron_tarea_proy->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Cron_tarea_proy')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegCron_tarea_proy->idproyecto = @$idproyecto;
					$this->oNegCron_tarea_proy->fecha_hora = @$fecha_hora;
					$this->oNegCron_tarea_proy->estado = @$estado;
					$this->oNegCron_tarea_proy->idsesion = @$idsesion;
					$this->oNegCron_tarea_proy->idpestania = @$idpestania;
					$this->oNegCron_tarea_proy->idcurso = @$idcurso;
					$this->oNegCron_tarea_proy->idcomplementario = @$idcomplementario;
					$this->oNegCron_tarea_proy->iddocente = @$iddocente;
					$this->oNegCron_tarea_proy->nombre = @$nombre;
					$this->oNegCron_tarea_proy->tipo = @$tipo;
					$this->oNegCron_tarea_proy->recurso = @$recurso;
					$this->oNegCron_tarea_proy->tiporecurso = @$tiporecurso;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$filtros = array();
			if (isset($_REQUEST["idcron_tapr"]) && @$_REQUEST["idcron_tapr"] != '') $filtros["idcron_tapr"] = $_REQUEST["idcron_tapr"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["fecha_hora"]) && @$_REQUEST["fecha_hora"] != '') $filtros["fecha_hora"] = $_REQUEST["fecha_hora"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["idpestania"]) && @$_REQUEST["idpestania"] != '') $filtros["idpestania"] = $_REQUEST["idpestania"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];


			// $this->oNegCron_tarea_proy->__set('idcron_tapr', $_REQUEST['idcron_tapr']);
			$res = $this->oNegCron_tarea_proy->eliminar($filtros);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
	public function updateCron()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$filtros = array();
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["idpestania"]) && @$_REQUEST["idpestania"] != '') $filtros["idpestania"] = $_REQUEST["idpestania"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			$update = array();
			if (isset($_REQUEST["iddocente"]) && @$_REQUEST["iddocente"] != '') $update["iddocente"] = $_REQUEST["iddocente"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $update["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $update["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["recurso"]) && @$_REQUEST["recurso"] != '') $update["recurso"] = $_REQUEST["recurso"];
			if (isset($_REQUEST["tiporecurso"]) && @$_REQUEST["tiporecurso"] != '') $update["tiporecurso"] = $_REQUEST["tiporecurso"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $update["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			$arrCron_tarea_proy = $this->oNegCron_tarea_proy->buscar($filtros);
			$usuarioAct = NegSesion::getUsuario();
			$filtros = array();
			foreach ($arrCron_tarea_proy as $key => $cron_tarea_proy) {
				$filtros["idcron_tapr"] = $cron_tarea_proy['idcron_tapr'];
				$this->oNegCron_tarea_proy->customUpdate($filtros, $update);
			}
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Updated Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => $e->getMessage()));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegCron_tarea_proy->setCampo($_REQUEST['idcron_tapr'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
