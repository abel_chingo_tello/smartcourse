<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_archivo', RUTA_BASE);
class WebNotas_archivo extends JrWeb
{
	private $oNegNotas_archivo;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotas_archivo = new NegNotas_archivo;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_archivo', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["imagen"])&&@$_REQUEST["imagen"]!='')$filtros["imagen"]=$_REQUEST["imagen"];
			if(isset($_REQUEST["identificador"])&&@$_REQUEST["identificador"]!='')$filtros["identificador"]=$_REQUEST["identificador"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["fechareg"])&&@$_REQUEST["fechareg"]!='')$filtros["fechareg"]=$_REQUEST["fechareg"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegNotas_archivo->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idarchivo)) {
				$this->oNegNotas_archivo->idarchivo = $idarchivo;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegNotas_archivo->nombre=@$nombre;
				$this->oNegNotas_archivo->imagen=@$imagen;
				$this->oNegNotas_archivo->identificador=@$identificador;
				$this->oNegNotas_archivo->iddocente=@$iddocente;
				$this->oNegNotas_archivo->fechareg=@$fechareg;
				
            if($accion=='_add') {
            	$res=$this->oNegNotas_archivo->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Notas_archivo')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNotas_archivo->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Notas_archivo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegNotas_archivo->__set('idarchivo', $_REQUEST['idarchivo']);
			$res=$this->oNegNotas_archivo->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegNotas_archivo->setCampo($_REQUEST['idarchivo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}