<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020 
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_criterios', RUTA_BASE);
class WebAcad_criterios extends JrWeb
{
	private $oNegAcad_criterios;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_criterios = new NegAcad_criterios;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();		
			//if(!NegSesion::tiene_acceso('Criterios', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcriterio"])&&@$_REQUEST["idcriterio"]!='')$filtros["idcriterio"]=$_REQUEST["idcriterio"];
			if(isset($_REQUEST["idcapacidad"])&&@$_REQUEST["idcapacidad"]!='')$filtros["idcapacidad"]=$_REQUEST["idcapacidad"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];
			if(!empty($filtros["idcriterio"])){
				$filtros["idcriterio"]=str_replace('|', ',', $filtros["idcriterio"]);
				$filtros["idcriterio"]=explode(',',$filtros["idcriterio"]);
			}
			//var_dump($filtros);		
			$this->datos=$this->oNegAcad_criterios->buscar($filtros);
			//var_dump($this->datos);
			if(!empty($_REQUEST["solocriterios"])){
				$dat=array();
				if(!empty($this->datos))
					foreach ($this->datos as $dk => $dv){
						$dat[]=array('skill_id'=>$dv["idcriterio"],'skill_name'=>$dv["nombre"]);
					}
				echo json_encode(array('code'=>200,'data'=>$dat,'message'=>'ok'));	
			}else
			echo json_encode(array('code'=>200,'data'=>$this->datos));

		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idcriterio)) {
				$this->oNegAcad_criterios->idcriterio = $idcriterio;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegAcad_criterios->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
			$this->oNegAcad_criterios->idcapacidad=@$idcapacidad;
			$this->oNegAcad_criterios->idcurso=@$idcurso;
			$this->oNegAcad_criterios->nombre=@$nombre;
			$this->oNegAcad_criterios->estado=@$estado;
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_criterios->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Criterios')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_criterios->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Criterios')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegAcad_criterios->idcapacidad=@$idcapacidad;
					$this->oNegAcad_criterios->idcurso=@$idcurso;
					$this->oNegAcad_criterios->nombre=@$nombre;
					$this->oNegAcad_criterios->estado=@$estado;
					$this->oNegAcad_criterios->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_criterios->__set('idcriterio', $_REQUEST['idcriterio']);
			$res=$this->oNegAcad_criterios->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_criterios->setCampo($_REQUEST['idcriterio'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}