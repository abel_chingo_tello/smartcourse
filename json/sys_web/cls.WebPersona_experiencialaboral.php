<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_experiencialaboral', RUTA_BASE);
class WebPersona_experiencialaboral extends JrWeb
{
	private $oNegPersona_experiencialaboral;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_experiencialaboral = new NegPersona_experiencialaboral;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_experiencialaboral', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["empresa"])&&@$_REQUEST["empresa"]!='')$filtros["empresa"]=$_REQUEST["empresa"];
			if(isset($_REQUEST["rubro"])&&@$_REQUEST["rubro"]!='')$filtros["rubro"]=$_REQUEST["rubro"];
			if(isset($_REQUEST["area"])&&@$_REQUEST["area"]!='')$filtros["area"]=$_REQUEST["area"];
			if(isset($_REQUEST["cargo"])&&@$_REQUEST["cargo"]!='')$filtros["cargo"]=$_REQUEST["cargo"];
			if(isset($_REQUEST["funciones"])&&@$_REQUEST["funciones"]!='')$filtros["funciones"]=$_REQUEST["funciones"];
			if(isset($_REQUEST["fechade"])&&@$_REQUEST["fechade"]!='')$filtros["fechade"]=$_REQUEST["fechade"];
			if(isset($_REQUEST["fechahasta"])&&@$_REQUEST["fechahasta"]!='')$filtros["fechahasta"]=$_REQUEST["fechahasta"];
			if(isset($_REQUEST["actualmente"])&&@$_REQUEST["actualmente"]!='')$filtros["actualmente"]=$_REQUEST["actualmente"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegPersona_experiencialaboral->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idexperiencia)) {
				$this->oNegPersona_experiencialaboral->idexperiencia = $idexperiencia;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegPersona_experiencialaboral->idpersona=@$idpersona;
				$this->oNegPersona_experiencialaboral->empresa=@$empresa;
				$this->oNegPersona_experiencialaboral->rubro=@$rubro;
				$this->oNegPersona_experiencialaboral->area=@$area;
				$this->oNegPersona_experiencialaboral->cargo=@$cargo;
				$this->oNegPersona_experiencialaboral->funciones=@$funciones;
				$this->oNegPersona_experiencialaboral->fechade=@$fechade;
				$this->oNegPersona_experiencialaboral->fechahasta=@$fechahasta;
				$this->oNegPersona_experiencialaboral->actualmente=@$actualmente;
				$this->oNegPersona_experiencialaboral->mostrar=@$mostrar;
				
            if($accion=='_add') {
            	$res=$this->oNegPersona_experiencialaboral->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_experiencialaboral')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersona_experiencialaboral->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_experiencialaboral')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPersona_experiencialaboral->__set('idexperiencia', $_REQUEST['idexperiencia']);
			$res=$this->oNegPersona_experiencialaboral->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPersona_experiencialaboral->setCampo($_REQUEST['idexperiencia'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}