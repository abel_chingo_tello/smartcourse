<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPermisos', RUTA_BASE);
class WebPermisos extends JrWeb
{
	private $oNegPermisos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPermisos = new NegPermisos;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Permisos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='')$filtros["rol"]=$_REQUEST["rol"];
			if(isset($_REQUEST["menu"])&&@$_REQUEST["menu"]!='')$filtros["menu"]=$_REQUEST["menu"];
			if(isset($_REQUEST["_list"])&&@$_REQUEST["_list"]!='')$filtros["_list"]=$_REQUEST["_list"];
			if(isset($_REQUEST["_add"])&&@$_REQUEST["_add"]!='')$filtros["_add"]=$_REQUEST["_add"];
			if(isset($_REQUEST["_edit"])&&@$_REQUEST["_edit"]!='')$filtros["_edit"]=$_REQUEST["_edit"];
			if(isset($_REQUEST["_delete"])&&@$_REQUEST["_delete"]!='')$filtros["_delete"]=$_REQUEST["_delete"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegPermisos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idpermiso)) {
				$this->oNegPermisos->idpermiso = $idpermiso;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegPermisos->rol=@$rol;
				$this->oNegPermisos->menu=@$menu;
				$this->oNegPermisos->_list=@$_list;
				$this->oNegPermisos->_add=@$_add;
				$this->oNegPermisos->_edit=@$_edit;
				$this->oNegPermisos->_delete=@$_delete;
				
            if($accion=='_add') {
            	$res=$this->oNegPermisos->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Permisos')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPermisos->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Permisos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPermisos->__set('idpermiso', $_REQUEST['idpermiso']);
			$res=$this->oNegPermisos->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPermisos->setCampo($_REQUEST['idpermiso'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}