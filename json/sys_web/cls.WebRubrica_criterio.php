<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020 
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRubrica_criterio', RUTA_BASE);
class WebRubrica_criterio extends JrWeb
{
	private $oNegRubrica_criterio;

	public function __construct()
	{
		parent::__construct();
		$this->oNegRubrica_criterio = new NegRubrica_criterio;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Rubrica_criterio', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idrubrica_criterio"]) && @$_REQUEST["idrubrica_criterio"] != '') $filtros["idrubrica_criterio"] = $_REQUEST["idrubrica_criterio"];
			if (isset($_REQUEST["detalle"]) && @$_REQUEST["detalle"] != '') $filtros["detalle"] = $_REQUEST["detalle"];
			if (isset($_REQUEST["peso"]) && @$_REQUEST["peso"] != '') $filtros["peso"] = $_REQUEST["peso"];
			if (isset($_REQUEST["idrubrica"]) && @$_REQUEST["idrubrica"] != '') $filtros["idrubrica"] = $_REQUEST["idrubrica"];
			if (isset($_REQUEST["idcapacidad"]) && @$_REQUEST["idcapacidad"] != '') $filtros["idcapacidad"] = $_REQUEST["idcapacidad"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegRubrica_criterio->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idrubrica_criterio)) {
				$this->oNegRubrica_criterio->idrubrica_criterio = $idrubrica_criterio;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();


			$this->oNegRubrica_criterio->detalle = @$detalle;
			if (!is_numeric(@$peso)||@$peso=='') {
				@$peso=0;
			}
			$this->oNegRubrica_criterio->peso = @$peso;
			$this->oNegRubrica_criterio->idrubrica = @$idrubrica;
			$this->oNegRubrica_criterio->idcapacidad = @$idcapacidad;

			if ($accion == '_add') {
				$res = $this->oNegRubrica_criterio->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica_criterio')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegRubrica_criterio->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica_criterio')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegRubrica_criterio->detalle = @$detalle;
					$this->oNegRubrica_criterio->peso = @$peso;
					$this->oNegRubrica_criterio->idrubrica = @$idrubrica;
					$this->oNegRubrica_criterio->idcapacidad = @$idcapacidad;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			$this->oNegRubrica_criterio->__set('idrubrica_criterio', $_REQUEST['idrubrica_criterio']);
			$res = $this->oNegRubrica_criterio->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRubrica_criterio->setCampo($_REQUEST['idrubrica_criterio'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
