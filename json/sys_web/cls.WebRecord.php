<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRecord', RUTA_BASE);
class WebRecord extends JrWeb
{
	private $oNegRecord;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRecord = new NegRecord;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Record', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["link"])&&@$_REQUEST["link"]!='')$filtros["link"]=$_REQUEST["link"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["idnivel"])&&@$_REQUEST["idnivel"]!='')$filtros["idnivel"]=$_REQUEST["idnivel"];
			if(isset($_REQUEST["idunidad"])&&@$_REQUEST["idunidad"]!='')$filtros["idunidad"]=$_REQUEST["idunidad"];
			if(isset($_REQUEST["idleccion"])&&@$_REQUEST["idleccion"]!='')$filtros["idleccion"]=$_REQUEST["idleccion"];
			if(isset($_REQUEST["fechareg"])&&@$_REQUEST["fechareg"]!='')$filtros["fechareg"]=$_REQUEST["fechareg"];
			if(isset($_REQUEST["idherramienta"])&&@$_REQUEST["idherramienta"]!='')$filtros["idherramienta"]=$_REQUEST["idherramienta"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRecord->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idrecord)) {
				$this->oNegRecord->idrecord = $idrecord;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRecord->nombre=@$nombre;
				$this->oNegRecord->link=@$link;
				$this->oNegRecord->idpersonal=@$idpersonal;
				$this->oNegRecord->idnivel=@$idnivel;
				$this->oNegRecord->idunidad=@$idunidad;
				$this->oNegRecord->idleccion=@$idleccion;
				$this->oNegRecord->fechareg=@$fechareg;
				$this->oNegRecord->idherramienta=@$idherramienta;
				
            if($accion=='_add') {
            	$res=$this->oNegRecord->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Record')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRecord->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Record')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRecord->__set('idrecord', $_REQUEST['idrecord']);
			$res=$this->oNegRecord->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRecord->setCampo($_REQUEST['idrecord'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}