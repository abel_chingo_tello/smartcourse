<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020 
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_capacidades', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
class WebAcad_capacidades extends JrWeb
{
	private $oNegAcad_capacidades;
	private $oNegAcad_curso;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_capacidades = new NegAcad_capacidades;
		$this->oNegAcad_curso = new NegAcad_curso;
			
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();	
			//if(!NegSesion::tiene_acceso('Capacidades', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcompetencia"])&&@$_REQUEST["idcompetencia"]!='')$filtros["idcompetencia"]=$_REQUEST["idcompetencia"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAcad_capacidades->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idcapacidad)) {
				$this->oNegAcad_capacidades->idcapacidad = $idcapacidad;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegAcad_capacidades->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];       	
	        
			$this->oNegAcad_capacidades->idcompetencia=@$idcompetencia;
				$this->oNegAcad_capacidades->idcurso=@$idcurso;
				$this->oNegAcad_capacidades->nombre=@$nombre;

				
            if($accion=='_add') {
            	$res=$this->oNegAcad_capacidades->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Capacidades')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_capacidades->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Capacidades')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegAcad_capacidades->idcompetencia=@$idcompetencia;
					$this->oNegAcad_capacidades->idcurso=@$idcurso;
					$this->oNegAcad_capacidades->nombre=@$nombre;
					$this->oNegAcad_capacidades->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_capacidades->__set('idcapacidad', $_REQUEST['idcapacidad']);
			$res=$this->oNegAcad_capacidades->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_capacidades->setCampo($_REQUEST['idcapacidad'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}