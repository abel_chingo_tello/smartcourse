<?php

// require_once(RUTA_BASE . "vimeo" . SD . "src" . SD . "Vimeo" . SD . "Vimeo.php");
require_once(RUTA_BASE . "sys_lib" . SD . "vimeo" . SD . "autoload.php");
defined('RUTA_BASE') or die();
include_once('cls.WebMine.php');
JrCargador::clase('sys_negocio::NegVimeo', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
class WebVimeoFix extends JrWeb
{
	private $maxCharsInName = 128;
	private $oWebMine;
	private $oNegAcad_categorias;
	private $oNegAcad_cursodetalle;
	private $oNegProyecto;
	private $oNegVimeo;
	public $debug = false; #true:muestra print_r
	public $fake = false; #true: no sube a vimeo
	public $modo = 0;
	#0:subir a vimeo, registrar en BD y actualizar cursodetalle
	#1:subir a vimeo y registrar en BD 
	public $forzar = false; #fuerza procesado de cursos aunque ya esté en tmp_curso_procesado
	private $estructuraVimeoCurso = [];
	private $estructuraVimeoContenido = [];
	private $videosProcesados = 0;
	private static $instance = NULL;
	private $oClient = null;
	private static $status = [
		"running" => false,
		"idcurso" => null,
		"idcomplementario" => null,
	];
	public function __construct()
	{

		parent::__construct();
		ignore_user_abort(true);
		$this->oNegVimeo = new NegVimeo;
		$this->oWebMine = new WebMine;
		$this->oNegAcad_categorias = new NegAcad_categorias;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegProyecto = new NegProyecto;
	}
	private function getVimeoCnx()
	{
		// try {
		if ($this->oClient == null) {
			#new Vimeo("{client_id}", "{client_secret}", "{access_token}");
			#MY
			// $this->oClient = new \Vimeo\Vimeo("9c6464ebea9c7875a23afddfecba5be6538110f5", "QUs2o06MFaVd5a879xJA12x0npevaHhjniSnNEDG4GRDgo6Fg1HD45BAmYf3F+hCTf70Hk/hq6JbaSmtqlr2qsD/ft0w4QPnLrUZM0Ki+Ur2pbvmDSaJMiu1VLJ4+BYQ", "98a9e67a574af25b32b1e1d9d22e49a7");
			#$this->oClient = new \Vimeo\Vimeo("9c6464ebea9c7875a23afddfecba5be6538110f5", "QUs2o06MFaVd5a879xJA12x0npevaHhjniSnNEDG4GRDgo6Fg1HD45BAmYf3F+hCTf70Hk/hq6JbaSmtqlr2qsD/ft0w4QPnLrUZM0Ki+Ur2pbvmDSaJMiu1VLJ4+BYQ", "47e862c5993be740115292676f84ba34");
			#JP
			$this->oClient = new \Vimeo\Vimeo("5137dfc017712880d0bce4eb99e57fd560b64fdb", "Pq7XdEYa8lnXtNtEg6IUFyHDx5zwOqzIMCaTQBSLZE5n0Zcgsp0LtPtoVc+Pnh4gCILJ4AeMw7jd1HQlAwzZRYnB+WHLAScLTebDmsbWvF+C3oi5q6KXdBHwB7QBhEzR", "d5d98f5404c37e5fe02e3c6ac6158895");
		}
		return $this->oClient;
		// } catch (Exception $e) {
		// 	echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
		// 	exit(0);
		// }
	}
	static public function getInstance()
	{
		if (self::$instance === NULL) {
			self::$instance = new WebVimeoFix();
		}
		return self::$instance;
	}
	static public function getStatus()
	{
		return self::$status;
	}
	public function defecto()
	{
	}
	public function fixVideosCurso($params = array(), $return = false)
	{
		try {
			$params = WebMine::getParams($params);
			$this->videosProcesados = 0;
			$this->estructuraVimeoCurso = [];
			if (empty($params["idproyecto"])) {
				throw new Exception("Debe específicar el idproyecto");
				exit();
			}
			$arrCursosProcesados = $this->oNegVimeo->listarCursoProcesado([
				"idcurso" => $params["idcurso"],
				"idcomplementario" => $params["idcomplementario"]
			]);
			if ($this->forzar || count($arrCursosProcesados) == 0) {
				$arrRsql = $this->oNegVimeo->getNombreCurso([
					"idcurso" => $params["idcurso"],
					"idcomplementario" => $params["idcomplementario"]
				]);
				$oCurso = null;
				if ($arrRsql > 0) {
					$oCurso = $arrRsql[0];
					// echo "!\n";
					// var_dump($oCurso);
				} else {
					echo "\nerror al buscar curso idcurso => $params[idcurso],
					idcomplementario => $params[idcomplementario]\n";
					exit();
				}
				$this->estructuraVimeoCurso = $this->estructurar([
					"oCurso" => $oCurso,
					"idproyecto" => $params["idproyecto"]
				], true);
				self::$status["running"] = true;
				self::$status["idcurso"] = $params["idcurso"];
				self::$status["idcomplementario"] = $params["idcomplementario"];
				if (!isset($params["idcurso"]) || empty($params["idcurso"]) || !isset($params["idcomplementario"])) {
					throw new Exception("Error: Falta de parámetros (idcurso, idcomplementario)", 1);
				}
				$arrCursoDetalle = $this->oWebMine->getCursoDetalle([
					"idcurso" => $params["idcurso"],
					"idcomplementario" => $params["idcomplementario"],
					"espadre" => 0
				], true);
				foreach ($arrCursoDetalle as $key => $cursodetalle) {
					$this->estructuraVimeoContenido = [];
					if (!empty($cursodetalle["idpadre"])) {
						$this->estructuraVimeoContenido[] = $this->getUnidad([ //devuelve el nombre de la unidad
							"idpadre" => $cursodetalle["idpadre"]
						], true);
					}
					$oResponse = $this->process(["cursodetalle" => $cursodetalle], true);
					if (!$this->fake && $oResponse["needUpdate"]) {
						if ($this->modo == 0) {
							$txtjson = $oResponse["txtjson"];
							$rs = $this->oWebMine->updateData([
								"tabla" => "acad_cursodetalle",
								"arrUpdate" => [
									"txtjson" => json_encode($txtjson)
								],
								"arrFiltros" => [
									"idcursodetalle" => $cursodetalle["idcursodetalle"]
								]
							], true);
							if ($rs != "ok") {
								throw new Exception("ERROR al actualizar txtjson");
							}
						}
					}
					if ($this->fake && $oResponse["needUpdate"]) {
						if ($this->debug) {
							echo "\nSumando: " . $this->videosProcesados . "\n";
						}
					}
				}
			} else {
				echo "\nEl curso  idcurso: $params[idcurso], idcc $params[idcomplementario] ya ha sido procesado previamente (tmp_curso_procesado)\n";
				return false;
			}
			$datos = $this->videosProcesados;
			if (!$return) {
				$this->resetStatus();
				echo json_encode(array('code' => 200, 'data' => $datos));
				exit(0);
			} else {
				$this->resetStatus();
				return $datos;
			}
		} catch (Exception $e) {
			$this->resetStatus();
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	private function resetStatus()
	{
		self::$status["running"] = false;
		self::$status["idcurso"] = null;
		self::$status["idcomplementario"] = null;
	}
	public function process($params = array(), $return = false) #por aqui pasa el txtjson de cada sesion (cursodetalle)
	{
		try {
			$params = WebMine::getParams($params);
			$cursodetalle = $params["cursodetalle"];
			$txtjson = $cursodetalle['txtjson'];
			$txtjson = json_decode($cursodetalle['txtjson'], true);
			$oResponse = [
				"txtjson" => $txtjson,
				"needUpdate" => false,
			];
			$needUpdate = false;

			$oRs = $this->needUpload($txtjson, 1, [], $cursodetalle["nombre_detalle"]); #Solo por ser menu

			if ($oRs["needUpdate"]) {
				$needUpdate = $oRs["needUpdate"];
				$txtjson = $oRs["contenido"];
			}
			if (isset($txtjson["typelink"]) && $txtjson["typelink"] == "multioptions") {
				$oRsOptSes = $this->evaluateMultiOption($txtjson, [$cursodetalle["nombre_detalle"]]);
				$needUpdate = $oRsOptSes["needUpdate"];
				if ($needUpdate) {
					$txtjson = $oRsOptSes["contenido"];
				}
			}

			if (isset($txtjson['options']) && !empty($txtjson['options'])) {
				$arrPestanias = (array) $txtjson['options'];
				if (count($arrPestanias) > 0) { #tiene pestañas
					foreach ($arrPestanias as $i => $pestania) {
						$oRs = $this->needUpload($pestania, 2, [$cursodetalle["nombre_detalle"]], $pestania["nombre"]);
						if ($oRs["needUpdate"] == true) {
							$needUpdate = $oRs["needUpdate"];
							$pestania = $oRs["contenido"];
						}
						if ($pestania["type"] == "multioptions") {
							$oRsOpt = $this->evaluateMultiOption($pestania, [$cursodetalle["nombre_detalle"], $pestania["nombre"]]);
							if ($oRsOpt["needUpdate"] == true) {
								$needUpdate = $oRsOpt["needUpdate"];
								$pestania = $oRsOpt["contenido"];
							}
						}
						$arrPestanias[$i] = $pestania;
					}
					if ($needUpdate) {
						$txtjson['options'] = $arrPestanias;
					}
				}
			}

			if ($needUpdate) {
				$oResponse["txtjson"] = $txtjson;
				$oResponse["needUpdate"] = true;
			}
			// echo "KHAAAAAA2";
			// var_dump($oResponse);
			$datos = $oResponse;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $datos));
				exit(0);
			} else {
				return $datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getUnidad($params = array(), $return = false) //devuelve el nombre de la unidad
	{ #1menu, 2pestaña o 3option

		try {
			$filtros = ["idcursodetalle" => $params["idpadre"]];
			if (!empty(self::$status["idcomplementario"])) {
				$filtros["idcomplementario"] = self::$status["idcomplementario"];
			}
			$rs = $this->oNegAcad_cursodetalle->buscar($filtros);
			$nombreUnidad = null;
			if (count($rs) > 0) {
				$nombreUnidad = $rs[0]["nombre"];
			}
			$datos = $nombreUnidad;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $datos));
				exit(0);
			} else {
				return $datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function evaluateMultiOption($contenido, $arrEstructuraTmp, $nombrePestania = null)
	{ #sesion o pestaña
		$oResponse = [
			"needUpdate" => false,
			"contenido" => null
		];
		$needUpdate = false;
		$link = json_decode(base64_decode($contenido['link']), true);
		if ($link['mostrarcomo'] == "menu" || $link['mostrarcomo'] == "slider") {
			$arrMultiOptions = (array) $link['menus'];
			foreach ($arrMultiOptions as $j => $multioption) {
				if (!empty($multioption['tipo']) && strpos("video", $multioption['tipo']) !== false) {
					// $arrEstructuraTmp[] = $multioption["nombre"];
					if ($nombrePestania != null) {
						$arrEstructuraTmp[] = $nombrePestania;
					}
					$oRs = $this->needUpload($multioption, 3, $arrEstructuraTmp, $multioption["nombre"]);
					if ($oRs["needUpdate"] == true) {
						$needUpdate = $oRs["needUpdate"];
						$multioption = $oRs["contenido"];
						$arrMultiOptions[$j] = $multioption;
					}
				}
			}
			if ($needUpdate) {
				$link["menus"] = $arrMultiOptions;
				$contenido['link'] = base64_encode(json_encode($link));
			}
			$oResponse["needUpdate"] = $needUpdate;
			$oResponse["contenido"] = $contenido;
		}
		return $oResponse;
	}
	public function needUpload($contenido, $tipo, $arrEstructuraTmp = [], $nombreContenido) #por aqui pasa txtjson,pestania,option
	{ #1menu, 2pestaña o 3option
		$oResponse = [
			"needUpdate" => false,
			"contenido" => $contenido
		];
		$arrEstructuraVimeo = [];
		if (
			$this->isVideo($contenido, $tipo) &&
			isset($contenido["link"]) &&
			!empty($contenido["link"]) &&
			(!isset($contenido["ubicacion"]) ||
				$contenido["ubicacion"] != "vimeo") # => necesita ser subido a vimeo
		) {
			#si llego aquí, sé que debo actualizar el contenido
			// echo "\nthis->estructuraVimeoCurso:\n";
			// print_r($this->estructuraVimeoCurso);
			// echo "\nthis->estructuraVimeoContenido:\n";
			// print_r($this->estructuraVimeoContenido);
			// echo "\narrEstructuraTmp:\n";
			// print_r($arrEstructuraTmp);
			// if (count($this->estructuraVimeoContenido) > 0) {
			// 	$arrEstructuraVimeo = array_merge(
			// 		$this->estructuraVimeoCurso, #empresa>(carrera>modulo)??\>curso
			// 		$this->estructuraVimeoContenido, #unidad
			// 		$arrEstructuraTmp #sesion>pestaña
			// 	);
			// } else { #menus sin unidad
			// 	$arrEstructuraVimeo = array_merge(
			// 		$this->estructuraVimeoCurso,
			// 		$arrEstructuraTmp
			// 	);
			// }
			// if ($this->debug) {
			// 	echo "\nEstructura Vimeo:\n";
			// 	print_r($arrEstructuraVimeo);
			// 	echo "\nnombre: $nombreContenido\n";
			// }
			// $oRsUri = $this->getVimeoUri([
			// 	"staticlink" => $contenido["link"],
			// 	"arrEstructuraVimeo" => $arrEstructuraVimeo,
			// 	"nombreContenido" => $nombreContenido
			// ], true);

			if (
				#$oRsUri["estado"] == "subido"
				true
			) { #si el estado es no_encontrado, no necesita subir
				// $contenido["link"] = $oRsUri["vimeo_uri"];
				$contenido["ubicacion"] = "vimeo";
				$oResponse["contenido"] = $contenido;
				$oResponse["needUpdate"] = true;
			}
		} else {
			if ($this->debug) {
				if (
					!isset($contenido["link"]) ||
					empty($contenido["link"])
				) {
					echo "\nEl contenido no tiene un link válido.\n";
				}
				if (isset($contenido["ubicacion"]) && $contenido["ubicacion"] == "vimeo") {
					echo "\nEl contenido ya está en vimeo (cursodetalle)\n";
				}
			}
		}
		return $oResponse;
	}
	public function estructurar($params = array(), $return = false)
	{ #1menu, 2pestaña o 3option

		try {
			$params = WebMine::getParams($params);
			if (!isset($this->estructuraVimeoCurso[0])) {
				$rs = $this->oNegProyecto->identificar(["idproyecto" => $params["idproyecto"]]);
				if (count($rs) > 0) {
					$proyecto = $rs[0];
					$this->estructuraVimeoCurso[0] = strtoupper($proyecto["nombreurl"]);
				} else {
					throw new Exception("El proyecto no existe", 1);
				}
			}
			$oCurso = $params["oCurso"];
			// var_dump($params);
			$arrCategorias = [];
			if (isset($oCurso["idcomplementario"]) && !empty($oCurso["idcomplementario"])) { #sería complementario
				if (!empty($oCurso["idcategoria"])) { #puede que no tenga categoría
					$arrCategorias = $this->categorizar(["idcategoria" => $oCurso["idcategoria"]], true);
				}
			} else { #curso principal
				$rs = $this->oNegAcad_categorias->getCursoCategoria([
					"idproyecto" =>  $params["idproyecto"],
					"idcurso" => $oCurso["idcurso"]
				]);
				if (count($rs) > 0) {
					$idcategoria = $rs[0]["idcategoria"];
					$arrCategorias = $this->categorizar(["idcategoria" => $idcategoria], true);
					// echo "\aq2\n";
				}
			}
			if (count($arrCategorias) > 0) {
				$arrCategorias = array_reverse($arrCategorias);
				$this->estructuraVimeoCurso = array_merge($this->estructuraVimeoCurso, $arrCategorias);
			} else {
				$this->estructuraVimeoCurso[] = "Cursos libres";
			}

			$this->estructuraVimeoCurso[] = $oCurso["nombre"];
			$datos = $this->estructuraVimeoCurso;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $datos));
				exit(0);
			} else {
				return $datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function categorizar($params = array(), $return = false)  #por aqui pasa txtjson,pestania,option
	{ #1menu, 2pestaña o 3option

		try {
			// echo "entrando en categorizar()\n";
			$arrResponse = [];
			$idcategoria = $params["idcategoria"];
			if (!empty($idcategoria)) {
				$rs = $this->oNegAcad_categorias->buscar(["idcategoria" => $idcategoria]);
				if (count($rs) > 0) {
					$categoria = $rs[0];
					$arrResponse[] = $categoria["nombre"];
					if (!empty($categoria["idpadre"])) {
						$rsCat = $this->categorizar([
							"idcategoria" => $categoria["idpadre"]
						], true);
						if (count($rsCat) > 0) {
							$arrResponse = array_merge($arrResponse, $rsCat);
						}
					}
				}
			}
			$datos = $arrResponse;
			if (!$return) {
				// echo "return false";
				echo json_encode(array('code' => 200, 'data' => $datos));
				exit(0);
			} else {
				return $datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function isVideo($contenido, $tipo)
	{ #1menu, 2pestaña o 3option
		$tipo;
		$isVideo = false;
		switch ($tipo) {
			case 1: #menu
				if (isset($contenido["typelink"]) && !empty($contenido["typelink"]) && $contenido["typelink"] == "video") {
					$isVideo = true;
				}
				break;
			case 2: #pestaña
				if (isset($contenido["type"]) && !empty($contenido["type"]) && $contenido["type"] == "video") {
					$isVideo = true;
				}
				break;
			case 3: #option
				if (isset($contenido["tipo"]) && !empty($contenido["tipo"]) && $contenido["tipo"] == "video") {
					$isVideo = true;
				}
				break;
			default:
				# code...
				break;
		}
		if ($this->debug) {
			if (!$isVideo) {
				echo "\nEl contenido No es un video: \n";
			} else {
				echo "\nEl contenido Sí es un video: \n";
			}
			print_r($contenido);
		}
		return $isVideo;
	}
	public function getVimeoUri($params = array(), $return = false) #busca en bd o sube y devuelve uri
	{ #1menu, 2pestaña o 3option
		// try {
		// 	$oResponse = [
		// 		"estado" => null,
		// 		"vimeo_uri" => null
		// 	];
		// 	$estado = null;
		// 	$params = WebMine::getParams($params);
		// 	$arrEstructuraVimeo = $params["arrEstructuraVimeo"];
		// 	$nombreContenido = $params["nombreContenido"];

		// 	$link = $params["staticlink"];
		// 	$static_path = $this->fixLocalPath($link);
		// 	$vimeoUri = null;
		// 	$rs = $this->oNegVimeo->buscar(["static_path" => $static_path]);
		// 	if (count($rs) > 0) {
		// 		$vimeoUri = $rs[0]["vimeo_uri"];
		// 		$estado = $rs[0]["estado"];
		// 		if ($this->debug) {
		// 			echo "\nYa encontrado en BD\n";
		// 			print_r($rs[0]);
		// 		}
		// 	} else {
		// 		if ($this->debug) {
		// 			echo "\nNo encontrado en BD\n";
		// 		}
		// 		// $path = $this->fixLocalPath($link);
		// 		// $folderVimeo = array_shift($arrEstructuraVimeo);
		// 		// $arrEstructuraVimeo[] = $nombreContenido;
		// 		// $name = $this->makeName($arrEstructuraVimeo);
		// 		if (
		// 			// !empty($path) &&
		// 			// ($this->fake || file_exists(RUTA_BASE . $path)) && #pvq
		// 			// // file_exists(RUTA_BASE . $path) &&
		// 			// strpos($path, ".") !== false #validar que tenga extension
		// 			true #forzar
		// 		) {
		// 			// if ($this->debug) {
		// 			// 	echo "\nEl archivo Sí  existe en el servidor: " . RUTA_BASE . "$path.\n";
		// 			// }
		// 			// $vimeoUri = $this->uploadFromServer([
		// 			// 	"path" => $path,
		// 			// 	"name" => $name,
		// 			// 	"pathVimeo" => 	$folderVimeo
		// 			// ], true);
		// 			// $estado = "subido";
		// 			// $this->oNegVimeo->agregar([
		// 			// 	"static_path" => $path,
		// 			// 	"vimeo_uri" => $vimeoUri,
		// 			// 	"estado" => $estado,
		// 			// 	"idcurso" => self::$status["idcurso"],
		// 			// 	"idcomplementario" => self::$status["idcomplementario"],
		// 			// ]);
		// 		} else {
		// 			// if ($this->debug) {
		// 			// 	echo "\nEl archivo No existe en el servidor: " . RUTA_BASE . "$path.\n";
		// 			// }
		// 			// $vimeoUri = null;
		// 			// $estado = "no_encontrado";
		// 			// $this->oNegVimeo->agregar([
		// 			// 	"static_path" => $params["staticlink"],
		// 			// 	"vimeo_uri" => null,
		// 			// 	"estado" => $estado,
		// 			// 	"idcurso" => self::$status["idcurso"],
		// 			// 	"idcomplementario" => self::$status["idcomplementario"],
		// 			// ]);
		// 		}
		// 	}
		// 	if ($estado == "subido") {
		// 		$this->videosProcesados++;
		// 	}
		// 	$oResponse["vimeo_uri"] = $vimeoUri;
		// 	$oResponse["estado"] = $estado;
		// 	$datos = $oResponse;
		// 	if (!$return) {
		// 		echo json_encode(array('code' => 200, 'data' => $datos));
		// 		exit(0);
		// 	} else {
		// 		return $datos;
		// 	}
		// } catch (Exception $e) {
		// 	echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
		// 	exit(0);
		// }
	}
	public function uploadFromServer($params, $return = false)
	{
		// try {
		// 	$params = WebMine::getParams($params);
		// 	ini_set('max_execution_time', 0);
		// 	ini_set('max_execution_time', 0);
		// 	set_time_limit(0);
		// 	$path =   $params["path"];
		// 	$pathVimeo =   $params["pathVimeo"];
		// 	$name =   $params["name"];
		// 	$uriVideo = null;
		// 	$oClient = $this->getVimeoCnx();
		// 	if ($this->fake == true) {
		// 		$uriVideo = "fakepath";
		// 	} else {
		// 		$uriFolder = $this->crearFolderVimeo([
		// 			"nombre" => $pathVimeo,
		// 			"cnxVimeo" => $oClient
		// 		], true);
		// 		$idFolder = $this->getIdFromUri($uriFolder);
		// 		// #me delete
		// 		// exit();
		// 		$uriVideo = $oClient->upload(RUTA_BASE . $path, array(
		// 			"name" => $name,
		// 			"description" => "SmartCourse original path: " . $path,
		// 			"privacy" => [
		// 				"download" => false,
		// 				"comments" => "nobody"
		// 			]
		// 		));
		// 		$idVideo = $this->getIdFromUri($uriVideo);
		// 		$response = $oClient->request("/me/projects/$idFolder/videos/$idVideo", [], 'PUT');
		// 		if ($response["status"] != 204) {
		// 			throw new Exception("Error moviendo video a folder:\n" . json_encode($response));
		// 		}
		// 	}
		// 	$datos = $uriVideo;
		// 	if ($this->debug) {
		// 		echo "\nVideo subido a Vimeo, uri: $datos\n";
		// 	}
		// 	if (!$return) {
		// 		echo json_encode(array('code' => 200, 'data' => $datos));
		// 		exit(0);
		// 	} else {
		// 		return $datos;
		// 	}
		// } catch (Exception $e) {
		// 	echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_(print_r($e))));
		// 	exit(0);
		// }
	}
	public function crearFolderVimeo($params = [], $return = false)  #Crea folder (si no existe) y devuelve su uri
	{ #1menu, 2pestaña o 3option

		// try {
		// 	$params = WebMine::getParams($params, true);
		// 	$cnxVimeo = null;
		// 	if (isset($params["cnxVimeo"])) {
		// 		$cnxVimeo = $params["cnxVimeo"];
		// 	} else {
		// 		$cnxVimeo = $this->getVimeoCnx();
		// 	}

		// 	$nombreFolder = "SC_" . $params["nombre"];
		// 	$response = $cnxVimeo->request('/me/projects', [], 'GET');
		// 	if ($response["status"] != 200) {
		// 		print_r($response);
		// 		throw new Exception("Error listando folders en vimeo\n", 1);
		// 	}
		// 	$arrFoldersVimeo = $response["body"]["data"];
		// 	$folderVimeo = null;
		// 	$uriFolder = null;
		// 	$i = array_search($nombreFolder, array_column($arrFoldersVimeo, 'name'));
		// 	if ($i !== false) { # El folder ya existe
		// 		$folderVimeo = $arrFoldersVimeo[$i];
		// 		$uriFolder = $folderVimeo["uri"];
		// 		if ($this->debug) {
		// 			echo "\nEl folder $nombreFolder existe:\n";
		// 		}
		// 	} else {
		// 		if ($this->debug) {
		// 			echo "\nel folder $nombreFolder no existe, creando folder:\n";
		// 		}
		// 		$response = $cnxVimeo->request('/me/projects/', [
		// 			'name' => $nombreFolder
		// 		], 'POST');
		// 		if ($response["status"] != 201) {
		// 			throw new Exception("Error creando folder en vimeo:\n" . json_encode($response));
		// 		}
		// 		$uriFolder = $response["body"]["uri"];
		// 	}
		// 	if ($this->debug) {
		// 		echo "\nuriFolder: $uriFolder";
		// 	}
		// 	$datos = $uriFolder;
		// 	if (!$return) {
		// 		echo json_encode(array('code' => 200, 'data' => $datos));
		// 		exit(0);
		// 	} else {
		// 		return $datos;
		// 	}
		// } catch (Exception $e) {
		// 	echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
		// 	exit(0);
		// }
	}
	private function fixLocalPath($link)
	{
		try {
			$path = explode("?", $link)[0];
			if (strpos($path, "static/") !== false) {
				$path = explode("static/", $path)[1];
				$path = "static/" . $path;
			}
			if (strpos($path, "https://ingenio.com/app/") !== false) {
				$path = str_replace("https://ingenio.com/app/", "", $path);
			}
			return $path;
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	private function getIdFromUri($strUri)
	{
		try {
			$arrTmp = explode("/", $strUri);
			$size = count($arrTmp);
			$rs = null;
			if ($size > 0) {
				$rs = $arrTmp[$size - 1];
			} else {
				throw new Exception("Uri inválida: $strUri");
			}
			return $rs;
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function isAvailable($params = [], $return = false)
	{
		try {
			$params = WebMine::getParams($params);
			$datos = null;
			$uri = $params["uri"];
			$oResponse = [
				"link" => $this->getIdFromUri($uri),
				"ubicacion" => "vimeo"
			];
			$oClient = null;
			$response = null;
			$fxRecuperarVideoStatic = function ($oResponse, $uri) {
				$rs = $this->oNegVimeo->buscar(["vimeo_uri" => $uri]);
				// var_dump($rs);exit();
				if (count($rs) > 0) {
					$oResponse["link"] =  "/" . $rs[0]["static_path"];
					$oResponse["ubicacion"] = "static";
				}
				return $oResponse;
			};
			try {
				$oClient = $this->getVimeoCnx();
				$response = $oClient->request($uri . '?fields=transcode.status');
			} catch (Exception $e) {

				$oResponse = $fxRecuperarVideoStatic($oResponse, $uri);
			}

			if (
				!isset($response['body']['transcode']) ||
				!isset($response['body']['transcode']['status']) ||
				$response['body']['transcode']['status'] !== 'complete'
			) {
				$oResponse = $fxRecuperarVideoStatic($oResponse, $uri);
			}
			$datos = $oResponse;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $datos));
				exit(0);
			} else {
				return $datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	private function makeName($arrName)
	{
		try {
			$fixedName = implode("_", $arrName);
			if (strlen($fixedName) > $this->maxCharsInName) {
				$intCantidadPalabras = count($arrName);
				#si hay 3 items, habrán 2 guiones separando=>resto esos 2 guiones:
				$guiones = $intCantidadPalabras - 1;
				$maxCharsGeneral = $this->maxCharsInName - $guiones;
				$maxLen = $maxCharsGeneral / $intCantidadPalabras;
				$maxLen = round($maxLen, 0, PHP_ROUND_HALF_DOWN); #máximo tamaño por palabra
				foreach ($arrName as $i => $name) {
					$sizeName = strlen($name);
					if ($sizeName > $maxLen) {
						$name = substr($name, 0, $maxLen);
						$arrName[$i] = $name;
					}
				}
				$fixedName = implode("_", $arrName);
			}
			return $fixedName;
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
}
