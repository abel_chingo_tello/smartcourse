<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE);
class WebExamen_alumno extends JrWeb
{
	private $oNegExamen_alumno;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamen_alumno = new NegExamen_alumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idexamen"])&&@$_REQUEST["idexamen"]!='')$filtros["idexamen"]=$_REQUEST["idexamen"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["preguntas"])&&@$_REQUEST["preguntas"]!='')$filtros["preguntas"]=$_REQUEST["preguntas"];
			if(isset($_REQUEST["resultado"])&&@$_REQUEST["resultado"]!='')$filtros["resultado"]=$_REQUEST["resultado"];
			if(isset($_REQUEST["puntajehabilidad"])&&@$_REQUEST["puntajehabilidad"]!='')$filtros["puntajehabilidad"]=$_REQUEST["puntajehabilidad"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];
			if(isset($_REQUEST["resultadojson"])&&@$_REQUEST["resultadojson"]!='')$filtros["resultadojson"]=$_REQUEST["resultadojson"];
			if(isset($_REQUEST["tiempoduracion"])&&@$_REQUEST["tiempoduracion"]!='')$filtros["tiempoduracion"]=$_REQUEST["tiempoduracion"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["intento"])&&@$_REQUEST["intento"]!='')$filtros["intento"]=$_REQUEST["intento"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegExamen_alumno->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idexaalumno)) {
				$this->oNegExamen_alumno->idexaalumno = $idexaalumno;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegExamen_alumno->idexamen=@$idexamen;
				$this->oNegExamen_alumno->idalumno=@$idalumno;
				$this->oNegExamen_alumno->preguntas=@$preguntas;
				$this->oNegExamen_alumno->resultado=@$resultado;
				$this->oNegExamen_alumno->puntajehabilidad=@$puntajehabilidad;
				$this->oNegExamen_alumno->puntaje=@$puntaje;
				$this->oNegExamen_alumno->resultadojson=@$resultadojson;
				$this->oNegExamen_alumno->tiempoduracion=@$tiempoduracion;
				$this->oNegExamen_alumno->fecha=@$fecha;
				$this->oNegExamen_alumno->intento=@$intento;
				
            if($accion=='_add') {
            	$res=$this->oNegExamen_alumno->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Examen_alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegExamen_alumno->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Examen_alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegExamen_alumno->__set('idexaalumno', $_REQUEST['idexaalumno']);
			$res=$this->oNegExamen_alumno->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegExamen_alumno->setCampo($_REQUEST['idexaalumno'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}