<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegMenu', RUTA_BASE);
class WebMenu extends JrWeb
{
	private $oNegMenu;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegMenu = new NegMenu;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Menu', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idmenu"])) $filtros["idmenu"]=$_REQUEST["idmenu"];

			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"]; 
  			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["url"])&&@$_REQUEST["url"]!='')$filtros["url"]=$_REQUEST["url"]; 
  			if(isset($_REQUEST["icono"])&&@$_REQUEST["icono"]!='')$filtros["icono"]=$_REQUEST["icono"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegMenu->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario();   

            if(!empty($idmenu)) {
				$this->oNegMenu->idmenu = $idmenu;
				$accion='_edit';
			}

           	$this->oNegMenu->nombre=@$nombre;
  			$this->oNegMenu->descripcion=@$descripcion;
  			$this->oNegMenu->estado=@$estado;
  			$this->oNegMenu->url=@$url;
  			$this->oNegMenu->icono=@$icono;
  			$this->oNegMenu->usuario_registro=$usuarioAct["idpersona"];
  				        
            if($accion=='_add') {
            	$res=$this->oNegMenu->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Menu')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegMenu->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Menu')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            } 
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegMenu->__set('idmenu', $_REQUEST['idmenu']);
			$res=$this->oNegMenu->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegMenu->setCampo($_REQUEST['idmenu'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}