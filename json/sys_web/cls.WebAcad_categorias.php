<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2019 
 * @copyright	Copyright (C) 16-10-2019. Todos los derechos reservados.
 */ 
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
class WebAcad_categorias extends JrWeb
{
	private $oNegAcad_categorias;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_categorias = new NegAcad_categorias;
				
	}

	public function defecto(){
		return $this->listado();
	}
	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Categorias', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			$conlogin=false;
			if(empty($filtros["idproyecto"])){
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idproyecto"]=$usuarioAct["idproyecto"];
				$conlogin=true;
			}			
			if($conlogin==true){
				$this->datos=$this->oNegAcad_categorias->buscar($filtros);
				echo json_encode(array('code'=>201,'data'=>$this->datos,'msj'=>'miscategorias'));
			}else{
				unset($filtros["idproyecto"]);
				$this->datos=$this->oNegAcad_categorias->buscar($filtros);
				echo json_encode(array('code'=>200,'data'=>$this->datos,'msj'=>'categorias generales'));
			} 	
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	public function buscarcategorias(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Categorias', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];

			$this->datos=$this->oNegAcad_categorias->buscar($filtros);

			echo json_encode(array('code'=>200,'data'=>$this->datos,'msj'=>'miscategorias'));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idcategoria)) {
				$this->oNegAcad_categorias->idcategoria = $idcategoria;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();          	
	        
			$this->oNegAcad_categorias->nombre=@$nombre;
			$this->oNegAcad_categorias->descripcion=@$descripcion;
			$this->oNegAcad_categorias->idpadre=!empty($idpadre)?$idpadre:0;
			$this->oNegAcad_categorias->estado=!empty($estado)?$estado:1;
			$this->oNegAcad_categorias->orden=@$orden;
			if(isset($idproyecto)){
				$this->oNegAcad_categorias->idproyecto=$idproyecto;
			}else{
				$usuarioAct = NegSesion::getUsuario();
				$idproyecto=$usuarioAct["idproyecto"];
				$this->oNegAcad_categorias->idproyecto=$idproyecto;
			}
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_categorias->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Categorias')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_categorias->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Categorias')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            JrCargador::clase('sys_negocio::NegTools', RUTA_RAIZ);
					
		if(!empty($_FILES["imagenfile"])){            	
		    $resi = NegTools::subirFile($_FILES["imagenfile"],'acad_categorias/','','imagen','img_'.$res);
		    if($resi['code']==200) $this->oNegAcad_categorias->setCampo($res,'imagen',$resi["rutaStatic"]."?id=".date('YmdHis'));		            	
		}
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegAcad_categorias->nombre=@$nombre;
					$this->oNegAcad_categorias->descripcion=@$descripcion;
					$this->oNegAcad_categorias->idpadre=@$idpadre;
					$this->oNegAcad_categorias->estado=@$estado;
					$this->oNegAcad_categorias->orden=@$orden;
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_categorias->__set('idcategoria', $_REQUEST['idcategoria']);
			$res=$this->oNegAcad_categorias->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_categorias->setCampo($_REQUEST['idcategoria'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}