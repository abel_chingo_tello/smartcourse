<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE);
class WebAulavirtualinvitados extends JrWeb
{
	private $oNegAulavirtualinvitados;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Aulavirtualinvitados', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idaula"])&&@$_REQUEST["idaula"]!='')$filtros["idaula"]=$_REQUEST["idaula"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["asistio"])&&@$_REQUEST["asistio"]!='')$filtros["asistio"]=$_REQUEST["asistio"];
			if(isset($_REQUEST["como"])&&@$_REQUEST["como"]!='')$filtros["como"]=$_REQUEST["como"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAulavirtualinvitados->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idinvitado)) {
				$this->oNegAulavirtualinvitados->idinvitado = $idinvitado;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegAulavirtualinvitados->idaula=@$idaula;
				$this->oNegAulavirtualinvitados->dni=@$dni;
				$this->oNegAulavirtualinvitados->email=@$email;
				$this->oNegAulavirtualinvitados->asistio=@$asistio;
				$this->oNegAulavirtualinvitados->como=@$como;
				$this->oNegAulavirtualinvitados->usuario=@$usuario;
				
            if($accion=='_add') {
            	$res=$this->oNegAulavirtualinvitados->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Aulavirtualinvitados')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAulavirtualinvitados->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Aulavirtualinvitados')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAulavirtualinvitados->__set('idinvitado', $_REQUEST['idinvitado']);
			$res=$this->oNegAulavirtualinvitados->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAulavirtualinvitados->setCampo($_REQUEST['idinvitado'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}