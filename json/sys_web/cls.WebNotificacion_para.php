<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-02-2021 
 * @copyright	Copyright (C) 17-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotificacion_para', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotificacion_de', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
class WebNotificacion_para extends JrWeb
{
	private $oNegNotificacion_para;
	private $oNegNotificacion_de;
	private $oNegPersonal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotificacion_para = new NegNotificacion_para;
		$this->oNegNotificacion_de = new NegNotificacion_de;
	$this->oNegPersonal = new NegPersonal;
			
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notificacion_para', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idnotipersona"])&&@$_REQUEST["idnotipersona"]!='')$filtros["idnotipersona"]=$_REQUEST["idnotipersona"];
			if(isset($_REQUEST["idnotificacion"])&&@$_REQUEST["idnotificacion"]!='')$filtros["idnotificacion"]=$_REQUEST["idnotificacion"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegNotificacion_para->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idnotipersona)) {
				$this->oNegNotificacion_para->idnotipersona = $idnotipersona;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegNotificacion_para->idnotificacion=@$idnotificacion;
				$this->oNegNotificacion_para->idpersona=@$idpersona;
				$this->oNegNotificacion_para->estado=@$estado;
				
            if($accion=='_add') {
            	$res=$this->oNegNotificacion_para->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Notificacion_para')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNotificacion_para->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Notificacion_para')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegNotificacion_para->idnotificacion=@$idnotificacion;
					$this->oNegNotificacion_para->idpersona=@$idpersona;
					$this->oNegNotificacion_para->estado=@$estado;
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegNotificacion_para->__set('idnotipersona', $_REQUEST['idnotipersona']);
			$res=$this->oNegNotificacion_para->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegNotificacion_para->setCampo($_REQUEST['idnotipersona'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}