<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-11-2020 
 * @copyright	Copyright (C) 17-11-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTb_grupo_trabajo', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
include_once('cls.WebMine.php');
class WebTb_grupo_trabajo extends JrWeb
{
	private $oNegTb_grupo_trabajo;
	private $oNegAcad_matricula;

	public function __construct()
	{
		parent::__construct();
		$this->oNegTb_grupo_trabajo = new NegTb_grupo_trabajo;
		$this->oNegAcad_matricula = new NegAcad_matricula;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Tb_grupo_trabajo', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["id_grupo_trabajo"]) && @$_REQUEST["id_grupo_trabajo"] != '') $filtros["id_grupo_trabajo"] = $_REQUEST["id_grupo_trabajo"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegTb_grupo_trabajo->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function listarConMatriculas()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Tb_grupo_trabajo', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["id_grupo_trabajo"]) && @$_REQUEST["id_grupo_trabajo"] != '') $filtros["id_grupo_trabajo"] = $_REQUEST["id_grupo_trabajo"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$arrRs = $this->oNegTb_grupo_trabajo->listarConMatriculas($filtros);
			
			$arrGrupoTrabajo = [];
			foreach ($arrRs as $i => $rs) {
				#nuevo
				$lastIndex = count($arrGrupoTrabajo) - 1;
				if (
					$lastIndex < 0 ||
					$arrGrupoTrabajo[$lastIndex]["id_grupo_trabajo"] != $rs["id_grupo_trabajo"]
				) {
					$grupoTrabajo = [
						"idgrupoauladetalle" => $rs["idgrupoauladetalle"],
						"id_grupo_trabajo" => $rs["id_grupo_trabajo"],
						"nombre_grupo" => $rs["nombre_grupo"],
						"arrAlumnos" => []
					];
					$arrGrupoTrabajo[] = $grupoTrabajo;
					$lastIndex++;
				}
				#añadiendo si o si
				$arrGrupoTrabajo[$lastIndex]["arrAlumnos"][] = [
					"idmatricula" => $rs["idmatricula"],
					"idalumno" => $rs["idalumno"],
					"nombre_alumno" => $rs["nombre_alumno"],
					"idcomplementario" => $rs["idcomplementario"]
				];
			}
			$this->datos = $arrGrupoTrabajo;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function guardar($params = array())
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			$params = WebMine::getParams($params);
			$arrGroupsSelect = $params["arrGroupsSelect"];

			foreach ($arrGroupsSelect as $i => $grupoTrabajo) {
				$res = "null";
				if ($grupoTrabajo["id"] != -1) {
					// $accion = '_add';
					$arrRs = $this->oNegTb_grupo_trabajo->buscar(["nombre" => $grupoTrabajo["nombre"]]);
					if (!(count($arrRs) > 0)) {
						// if (isset($grupoTrabajo["id_grupo_trabajo"]) && !empty($grupoTrabajo["id_grupo_trabajo"])) {
						// 	$this->oNegTb_grupo_trabajo->id_grupo_trabajo = $grupoTrabajo["id_grupo_trabajo"];
						// 	$accion = '_edit';
						// }
						$this->oNegTb_grupo_trabajo->idgrupoauladetalle =  $grupoTrabajo["idgrupoauladetalle"];
						$this->oNegTb_grupo_trabajo->nombre =  $grupoTrabajo["nombre"];
						// if ($accion == '_add') {
						$res = $this->oNegTb_grupo_trabajo->agregar();
						// } 
						// else {
						// 	$res = $this->oNegTb_grupo_trabajo->editar();
						// }
					} else {
						$res = $arrRs[0]["id_grupo_trabajo"];
					}
				}
				foreach ($grupoTrabajo["arrMatriculas"] as $j => $matricula) {
					$this->oNegAcad_matricula->setCampo($matricula, "id_grupo_trabajo", $res);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => "ok"));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegTb_grupo_trabajo->idgrupoauladetalle = @$idgrupoauladetalle;
					$this->oNegTb_grupo_trabajo->nombre = @$nombre;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegTb_grupo_trabajo->__set('id_grupo_trabajo', $_REQUEST['id_grupo_trabajo']);
			$res = $this->oNegTb_grupo_trabajo->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegTb_grupo_trabajo->setCampo($_REQUEST['id_grupo_trabajo'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
