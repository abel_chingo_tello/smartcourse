<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
class WebAcad_cursodetalle extends JrWeb
{
	private $oNegAcad_cursodetalle;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_cursodetalle', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["orden"]) && @$_REQUEST["orden"] != '') $filtros["orden"] = $_REQUEST["orden"];
			if (isset($_REQUEST["idrecurso"]) && @$_REQUEST["idrecurso"] != '') $filtros["idrecurso"] = $_REQUEST["idrecurso"];
			if (isset($_REQUEST["tiporecurso"]) && @$_REQUEST["tiporecurso"] != '') $filtros["tiporecurso"] = $_REQUEST["tiporecurso"];
			if (isset($_REQUEST["idlogro"]) && @$_REQUEST["idlogro"] != '') $filtros["idlogro"] = $_REQUEST["idlogro"];
			if (isset($_REQUEST["url"]) && @$_REQUEST["url"] != '') $filtros["url"] = $_REQUEST["url"];
			if (isset($_REQUEST["idpadre"]) && @$_REQUEST["idpadre"] != '') $filtros["idpadre"] = $_REQUEST["idpadre"];
			if (isset($_REQUEST["color"]) && @$_REQUEST["color"] != '') $filtros["color"] = $_REQUEST["color"];
			if (isset($_REQUEST["esfinal"]) && @$_REQUEST["esfinal"] != '') $filtros["esfinal"] = $_REQUEST["esfinal"];
			if (isset($_REQUEST["txtjson"]) && @$_REQUEST["txtjson"] != '') $filtros["txtjson"] = $_REQUEST["txtjson"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegAcad_cursodetalle->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idcursodetalle)) {
				$this->oNegAcad_cursodetalle->idcursodetalle = $idcursodetalle;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();


			$this->oNegAcad_cursodetalle->idcurso = @$idcurso;
			$this->oNegAcad_cursodetalle->orden = @$orden;
			$this->oNegAcad_cursodetalle->idrecurso = @$idrecurso;
			$this->oNegAcad_cursodetalle->tiporecurso = @$tiporecurso;
			$this->oNegAcad_cursodetalle->idlogro = @$idlogro;
			$this->oNegAcad_cursodetalle->url = @$url;
			$this->oNegAcad_cursodetalle->idpadre = @$idpadre;
			$this->oNegAcad_cursodetalle->color = @$color;
			$this->oNegAcad_cursodetalle->esfinal = @$esfinal;
			$this->oNegAcad_cursodetalle->txtjson = @$txtjson;

			if ($accion == '_add') {
				$res = $this->oNegAcad_cursodetalle->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Acad_cursodetalle')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegAcad_cursodetalle->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Acad_cursodetalle')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_cursodetalle->__set('idcursodetalle', $_REQUEST['idcursodetalle']);
			$res = $this->oNegAcad_cursodetalle->eliminar();
			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function procesarEnlaces()
	{
		try {
			set_time_limit(0);
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->link = $_REQUEST["link"];
			$nombre = explode("/", $this->link);
			$nombre = end($nombre);
			$nombre = explode(".txt", $nombre)[0];
			$this->link = explode("?id", $this->link)[0];
			$this->link = explode("static/", $this->link)[1];
			$this->link = RUTA_BASE . "static/" . $this->link;
			$directorio = explode($nombre, $this->link)[0];
			$enlaces = array();
			$contador = 1;
			$index = 0;
			$fp = fopen($this->link, "r");
			while (!feof($fp)) {
				$linea = fgets($fp);
				$linea = ltrim($linea);
				$linea = rtrim($linea);
				$validar = preg_replace("/((http|https|www)[^\s]+)/", 'si', $linea);
				if ($contador % 2 != 0) {
					if ($validar != "si") {
						$enlaces[$index]["titulo"] = utf8_encode($linea);
						$contador++;
					} else {
						$enlaces[$index]["titulo"] = utf8_encode("Sin Título");
						$enlaces[$index]["link"] = $linea;
						$index++;
					}
				} else {
					$enlaces[$index]["link"] = $linea;
					$index++;
					$contador++;
				}
			}
			fclose($fp);
			// var_dump($enlaces);exit();
			foreach ($enlaces as $key => $enlace) {
				$PagespeedDataGoogle = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=" . $enlace["link"] . "&screenshot=true");
				$PagespeedDataGoogle = json_decode($PagespeedDataGoogle, true);
				$captura = $PagespeedDataGoogle['screenshot']['data'];
				$captura = str_replace(array('_', '-'), array('/', '+'), $captura);
				$enlace["img"] = "data:image/jpeg;base64," . $captura;
				// $enlace["img"] = $captura;
				$enlaces[$key] = $enlace;
			}
			// echo NegTools::array2json($enlaces);exit();
			$fichero_subido = $directorio . SD . $nombre . ".php";
			$cadena = "";
			if ($archivo = fopen($fichero_subido, "a")) {
				$cadena = '';
				$cadena .= '<?php ';
				$cadena .= '$enlaces = ' . "'" . NegTools::array2json($enlaces) . "'" . ';';
				// $cadena .= '$enlaces = json_decode($enlaces, true);';
				$cadena .= '?>';
				if (fwrite($archivo, $cadena)) {
					$cadena = true;
				}
				fclose($archivo);
			}

			if (file_exists($fichero_subido)) {
				echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
				// return $nombre . ".php";
			}
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function _leerEnlaces()
	{
		try {
			set_time_limit(0);
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$link = $_REQUEST["link"];
			$link = explode(".txt?", $link);
			$link = explode(URL_BASE, $link[0]);
			$link = end($link);
			$link = RUTA_BASE . $link . ".php";

			include_once $link;

			$enlaces = json_decode($enlaces, true);

			foreach ($enlaces as $key => $value) {
				// if (!empty($value["base64"])) {
				// 	if ($value["base64"] == 1) {
				// 		$value["titulo"] = base64_decode($value["titulo"], true);
				// 	}
				// }
				$value["titulo"] = base64_decode($value["titulo"], true);
				$enlaces[$key] = $value;
			}

			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado', 'data' => $enlaces));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function _procesarEnlaces()
	{
		try {
			set_time_limit(0);
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$enlaces = json_decode($_REQUEST["enlaces"], true);
			foreach ($enlaces as $key => $enlace) {
				// var_dump($enlace);
				// if(!empty($enlace["img"])){
				// 	$enlace["img"] = explode("?", $enlace["img"]);
				// 	// $path = URL_BASE . $enlace["img"][0];
				// 	$path = '../' . $enlace["img"][0];
				// 	$type = pathinfo($path, PATHINFO_EXTENSION);
				// 	$data = file_get_contents($path);
				// 	$enlace["img"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
				// } else {
				// 	if(!empty($enlace["base64"])){
				// 		$enlace["img"] = $enlace["base64"];
				// 	} else {
				// 		$path = '../static/img/no-imagen-400x300.png';
				// 		$type = pathinfo($path, PATHINFO_EXTENSION);
				// 		$data = file_get_contents($path);
				// 		$enlace["img"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
				// 	}
				// }
				// $enlace["base64"] = 1;
				$enlace["titulo"] = base64_encode($enlace["titulo"]);
				if (empty($enlace["img"])) {
					if (empty($enlace["base64"])) {
						$enlace["img"] = 'static/img/no-imagen-400x300.png';
					} else {
						$enlace["img"] = $enlace["base64"];
					}
				}
				$enlaces[$key] = $enlace;
			}
			$directorio = $_REQUEST["dirmedia"];
			$nombre = $_REQUEST["nombre"];

			// echo NegTools::array2json($enlaces);exit();
			$fichero_subido = RUTA_BASE . "static/media/" . $directorio . $nombre . ".php";
			$cadena = "";
			if ($archivo = fopen($fichero_subido, "a")) {
				$cadena = '';
				$cadena .= '<?php ';
				$cadena .= '$enlaces = ' . "'" . NegTools::array2json($enlaces) . "'" . ';';
				// $cadena .= '$enlaces = json_decode($enlaces, true);';
				$cadena .= '?>';
				if (fwrite($archivo, $cadena)) {
					$cadena = true;
				}
				fclose($archivo);
			}

			if (file_exists($fichero_subido)) {
				$retorno = "static/media/" . $directorio . $nombre . ".txt?id=32421";
				echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado', 'data' => $retorno));
				// return $nombre . ".php";
			}
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function _procesarLibrary()
	{
		try {
			set_time_limit(0);
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$enlaces = json_decode($_REQUEST["enlaces"], true);
			foreach ($enlaces as $key => $enlace) {
				// var_dump($enlace);
				// if(!empty($enlace["img"])){
				// 	$enlace["img"] = explode("?", $enlace["img"]);
				// 	// $path = URL_BASE . $enlace["img"][0];
				// 	$path = '../' . $enlace["img"][0];
				// 	$type = pathinfo($path, PATHINFO_EXTENSION);
				// 	$data = file_get_contents($path);
				// 	$enlace["img"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
				// } else {
				// 	if(!empty($enlace["base64"])){
				// 		$enlace["img"] = $enlace["base64"];
				// 	} else {
				// 		$path = '../static/img/no-imagen-400x300.png';
				// 		$type = pathinfo($path, PATHINFO_EXTENSION);
				// 		$data = file_get_contents($path);
				// 		$enlace["img"] = 'data:image/' . $type . ';base64,' . base64_encode($data);
				// 	}
				// }
				// $enlace["base64"] = 1;
				$enlace["titulo"] = base64_encode($enlace["titulo"]);
				if (empty($enlace["img"])) {
					if (empty($enlace["base64"])) {
						$enlace["img"] = 'static/img/no-imagen-400x300.png';
					} else {
						$enlace["img"] = $enlace["base64"];
					}
				}
				$enlaces[$key] = $enlace;
			}
			$directorio = $_REQUEST["dirmedia"];
			$nombre = $_REQUEST["nombre"];

			// echo NegTools::array2json($enlaces);exit();
			$fichero_subido = RUTA_BASE . "static/media/" . $directorio . $nombre . ".php";
			$cadena = "";
			if ($archivo = fopen($fichero_subido, "a")) {
				$cadena = '';
				$cadena .= '<?php ';
				$cadena .= '$enlaces = ' . "'" . NegTools::array2json($enlaces) . "'" . ';';
				// $cadena .= '$enlaces = json_decode($enlaces, true);';
				$cadena .= '?>';
				if (fwrite($archivo, $cadena)) {
					$cadena = true;
				}
				fclose($archivo);
			}

			if (file_exists($fichero_subido)) {
				$retorno = "static/media/" . $directorio . $nombre . ".txt?id=32421";
				echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado', 'data' => $retorno));
				// return $nombre . ".php";
			}
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$idcomplementario = 0;
			if (isset($_REQUEST["icc"]) && @$_REQUEST["icc"] != '') {
				$idcomplementario = $_REQUEST["icc"];
			}
			if ($idcomplementario != 0) {
				$this->oNegAcad_cursodetalle->setCampo_($_REQUEST['idcursodetalle'], $_REQUEST['campo'], $_REQUEST['valor'], "acad_cursodetalle_complementario");
			} else {
				$this->oNegAcad_cursodetalle->setCampo_($_REQUEST['idcursodetalle'], $_REQUEST['campo'], $_REQUEST['valor'], "acad_cursodetalle");
			}
			// $this->oNegAcad_cursodetalle->setCampo($_REQUEST['idcursodetalle'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function reordenar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$datos = json_decode($datos, true);
			$usuarioAct = NegSesion::getUsuario();
			$userid = @$usuarioAct['idpersona'];
			if (!empty($datos))
				foreach ($datos as $i => $v) {
					// $this->oNegAcad_cursodetalle->setCampo(intval($v["idcursodetalle"]),'orden',intval($i));
					$tabla = "acad_cursodetalle";
					if ($idcomplementario != 0) {
						$tabla = "acad_cursodetalle_complementario";
					}
					$this->oNegAcad_cursodetalle->setCampo_(intval($v["idcursodetalle"]), 'orden', intval($i), $tabla);
					$this->oNegAcad_cursodetalle->setCampo_(intval($v["idcursodetalle"]), 'idpadre', $v['idpadre'], $tabla);
					if (isset($v['children']) && is_array($v['children'])) {
						$idpadre = $v['idcursodetalle'];
						foreach ($v['children'] as $key => $hijo) {
							$this->oNegAcad_cursodetalle->setCampo_(intval($hijo["idcursodetalle"]), 'orden', intval($key), $tabla);
							$this->oNegAcad_cursodetalle->setCampo_(intval($hijo["idcursodetalle"]), 'idpadre', intval($idpadre), $tabla);
						}
					}
				}
			echo json_encode(array('code' => 'ok', 'msj' => JrTexto::_('Datos ordenados correctamente')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
}
