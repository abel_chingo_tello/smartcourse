<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-09-2019 
 * @copyright	Copyright (C) 27-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE);
class WebUgel extends JrWeb
{
	private $oNegUgel;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegUgel = new NegUgel;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ugel', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];	
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["abrev"])&&@$_REQUEST["abrev"]!='')$filtros["abrev"]=$_REQUEST["abrev"];
			if(isset($_REQUEST["iddre"])&&@$_REQUEST["iddre"]!='')$filtros["iddre"]=$_REQUEST["iddre"];
			if(isset($_REQUEST["iddepartamento"])&&@$_REQUEST["iddepartamento"]!='')$filtros["iddepartamento"]=$_REQUEST["iddepartamento"];
			if(isset($_REQUEST["idprovincia"])&&@$_REQUEST["idprovincia"]!='')$filtros["idprovincia"]=$_REQUEST["idprovincia"];
			
			if(isset($_REQUEST["idproyecto"])&&!empty($_REQUEST["idproyecto"])){
				$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			}else{
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idproyecto"]=$usuarioAct["idproyecto"];
			}
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$this->datos=$this->oNegUgel->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idugel)) {
				$this->oNegUgel->idugel = $idugel;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegUgel->descripcion=@$descripcion;
			$this->oNegUgel->abrev=@$abrev;
			$this->oNegUgel->iddepartamento=@$iddepartamento;
			$this->oNegUgel->idprovincia=@$idprovincia;
			$this->oNegUgel->iddre=@$iddre;
			$this->oNegUgel->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
				
            if($accion=='_add') {
            	$res=$this->oNegUgel->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Ugel')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegUgel->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Ugel')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $jsondata=json_decode($datajson,true);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$dres=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$j++;
					$this->oNegUgel->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
					$iddre=!empty($dres[!empty($v["iddre"])?$v["iddre"]:'_'])?$dres[!empty($v["iddre"])?$v["iddre"]:'_']:$v["iddre"];
					$res=$this->oNegUgel->importar($v["idugel"],$v["descripcion"],$v["abrev"],$v["iddepartamento"],$v["dre"],$v["idprovincia"],$iddre);
					if(empty($dres[!empty($v["iddre"])?$v["iddre"]:'_']))$dres[!empty($v["iddre"])?$v["iddre"]:'_']=$res['_estado']=='N'?$res['_iddre']:$iddre;
					$v['_infoimport']=$res;
					$this->datos[$j]=$v;
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Dre')).' '.JrTexto::_('data Import successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegUgel->__set('idugel', $_REQUEST['idugel']);
			$res=$this->oNegUgel->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegUgel->setCampo($_REQUEST['idugel'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}