<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE);
JrCargador::clase('sys_negocio::NegSence_assigncursos', RUTA_BASE);
class WebAcad_grupoauladetalle extends JrWeb
{
	private $oNegAcad_grupoauladetalle;
	private $oNegAcad_grupoaula;
	private $oNegPersona_rol;
	private $oNegSence_assigncursos;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegPersona_rol = new NegPersona_rol;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegSence_assigncursos = new NegSence_assigncursos;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_grupoauladetalle', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["iddocente"]) && @$_REQUEST["iddocente"] != '') $filtros["iddocente"] = $_REQUEST["iddocente"];
			if (isset($_REQUEST["idlocal"]) && @$_REQUEST["idlocal"] != '') $filtros["idlocal"] = $_REQUEST["idlocal"];
			if (isset($_REQUEST["idambiente"]) && @$_REQUEST["idambiente"] != '') $filtros["idambiente"] = $_REQUEST["idambiente"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["fecha_inicio"]) && @$_REQUEST["fecha_inicio"] != '') $filtros["fecha_inicio"] = $_REQUEST["fecha_inicio"];
			if (isset($_REQUEST["fecha_final"]) && @$_REQUEST["fecha_final"] != '') $filtros["fecha_final"] = $_REQUEST["fecha_final"];
			if (isset($_REQUEST["idgrado"]) && @$_REQUEST["idgrado"] != '') $filtros["idgrado"] = $_REQUEST["idgrado"];
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["fechamodificacion"]) && @$_REQUEST["fechamodificacion"] != '') $filtros["fechamodificacion"] = $_REQUEST["fechamodificacion"];
			if (isset($_REQUEST["oldid"]) && @$_REQUEST["oldid"] != '') $filtros["oldid"] = $_REQUEST["oldid"];
			if (isset($_REQUEST["sql2"]) && @$_REQUEST["sql2"] != '') $filtros["sql2"] = $_REQUEST["sql2"];
			if (isset($_REQUEST["sql3"]) && @$_REQUEST["sql3"] != '') $filtros["sql3"] = $_REQUEST["sql3"];
			if (isset($_REQUEST["idcategoria"]) && @$_REQUEST["idcategoria"] != '') $filtros["idcategoria"] = $_REQUEST["idcategoria"];

			$usuarioAct = NegSesion::getUsuario();
			$this->idproyecto = !empty($usuarioAct["idproyecto"]) ? $usuarioAct["idproyecto"] : 3;
			if (!empty($_REQUEST["idproyecto"])) $this->idproyecto = $_REQUEST["idproyecto"];
			$filtros["idproyecto"] = $this->idproyecto;

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	/**
	 * Funcion que retorna los grupos de estudio del curso  tanto para el docente como el administrador
	 * Esta función se realizo para no repetir codigo y reutilizar, para mejorar la compresión logica del proceso
	 * @param Integer IdCurso : id del curso : Requerido
	 * @param Integer IdComplementario: id de curso complementario : Opcional
	 * @param Integer IdGrupoauladetalle: id del grupoauladetalle : Opcional
	 * @param Array Arreglo donde posee la información de los cursos sences asignados
	 * @return Integer retorna el id de sence_assigncursos o NULL en caso de no encontrar coincidencia
	 */
	public function listadogruposdeestudioxcurso(){
		try{
			$filtros=array();
			$usuarioAct = NegSesion::getUsuario();
			if($usuarioAct["idrol"]==2){
				$filtros["iddocente"]=$usuarioAct["idpersona"];
			}
			if(!empty($_REQUEST["idcomplementario"])) $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			if(empty($_REQUEST["idcurso"])){
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('Nesecita el código del curso')));
				exit(0);
			}
			$filtros["idproyecto"]=$usuarioAct["idproyecto"];
			$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			$datos=$this->oNegAcad_grupoauladetalle->listadogruposdeestudioxcurso($filtros);
			echo json_encode(array('code' => 200, 'data' => $datos));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}




	public function listado2()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_grupoauladetalle', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["iddocente"]) && @$_REQUEST["iddocente"] != '') $filtros["iddocente"] = $_REQUEST["iddocente"];
			if (isset($_REQUEST["idlocal"]) && @$_REQUEST["idlocal"] != '') $filtros["idlocal"] = $_REQUEST["idlocal"];
			if (isset($_REQUEST["idambiente"]) && @$_REQUEST["idambiente"] != '') $filtros["idambiente"] = $_REQUEST["idambiente"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["fecha_inicio"]) && @$_REQUEST["fecha_inicio"] != '') $filtros["fecha_inicio"] = $_REQUEST["fecha_inicio"];
			if (isset($_REQUEST["fecha_final"]) && @$_REQUEST["fecha_final"] != '') $filtros["fecha_final"] = $_REQUEST["fecha_final"];
			if (isset($_REQUEST["idgrado"]) && @$_REQUEST["idgrado"] != '') $filtros["idgrado"] = $_REQUEST["idgrado"];
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["fechamodificacion"]) && @$_REQUEST["fechamodificacion"] != '') $filtros["fechamodificacion"] = $_REQUEST["fechamodificacion"];
			if (isset($_REQUEST["oldid"]) && @$_REQUEST["oldid"] != '') $filtros["oldid"] = $_REQUEST["oldid"];
			if (isset($_REQUEST["idproducto"]) && @$_REQUEST["idproducto"] != '') $filtros["idproducto"] = $_REQUEST["idproducto"];
			if (isset($_REQUEST["sql2"]) && @$_REQUEST["sql2"] != '') $filtros["sql2"] = $_REQUEST["sql2"];
			if (isset($_REQUEST["sql3"]) && @$_REQUEST["sql3"] != '') $filtros["sql3"] = $_REQUEST["sql3"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function buscarespecial()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//obtener valores
			$idgrupoaula = isset($_REQUEST["idgrupoaula"]) ? $_REQUEST["idgrupoaula"] : null;
			$idcurso = isset($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : null;
			$idlocal = isset($_REQUEST["idlocal"]) ? $_REQUEST["idlocal"] : null;
			//validar valores
			if (empty($idgrupoaula)) {
				throw new Exception("Se requiere el id de grupoaula");
			}
			//variables
			$filtros = array();
			//busqueda si existe el grupo
			if (!empty($idcurso)) $filtros['idcurso'] = $idcurso;
			if (!empty($idlocal)) $filtros['idlocal'] = $idlocal;
			$filtros['idgrupoaula'] = $idgrupoaula;
			$filtros['sql3'] = true;
			$rs_grupoaula = $this->oNegAcad_grupoaula->buscar(array('idgrupoaula' => $idgrupoaula));

			if (empty($rs_grupoaula)) {
				throw new Exception("El grupo no existe");
			}

			$rs = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			if (empty($rs)) {
				//no existe insertarlo y retornar el id
				$this->oNegAcad_grupoauladetalle->idgrupoaula = $idgrupoaula;
				$this->oNegAcad_grupoauladetalle->idcurso = $idcurso;
				$this->oNegAcad_grupoauladetalle->iddocente = 0;
				$this->oNegAcad_grupoauladetalle->idlocal = $idlocal;
				$this->oNegAcad_grupoauladetalle->idambiente = 1;
				$this->oNegAcad_grupoauladetalle->idgrado = isset($rs_grupoaula[0]['idgrado']) && !empty($rs_grupoaula[0]['idgrado']) ? $rs_grupoaula[0]['idgrado'] : 0;
				$this->oNegAcad_grupoauladetalle->idseccion = isset($rs_grupoaula[0]['idseccion']) && !empty($rs_grupoaula[0]['idseccion']) ? $rs_grupoaula[0]['idseccion'] : 0;
				$this->oNegAcad_grupoauladetalle->nombre = @$rs_grupoaula[0]['nombre'];
				$this->oNegAcad_grupoauladetalle->fecha_inicio = date('Y-m-d H:i:s');
				$this->oNegAcad_grupoauladetalle->fecha_final = date('Y-m-d H:i:s');

				$idgrupoauladetalle = $this->oNegAcad_grupoauladetalle->agregar();

				$rs = $this->oNegAcad_grupoauladetalle->buscar(array('sql3' => true, 'idgrupoauladetalle' => $idgrupoauladetalle));
			}
			//mostrar resultado
			echo json_encode(array('code' => 200, 'data' => $rs));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile())));
			exit(0);
		}
	}
	/**
	 * Funcion que retorna el id de la tabla sence_assigncursos, especialmente para agregar o editar una asignacion en grupoauladetalle
	 * Esta función se realizo para no repetir codigo y reutilizar, para mejorar la compresión logica del proceso
	 * @param Integer Id del curso a buscar en un arreglo
	 * @param Array Arreglo donde posee la información de los cursos sences asignados
	 * @return Integer retorna el id de sence_assigncursos o NULL en caso de no encontrar coincidencia
	 */
	private function getIdSence_assigncurso($need,$arr){
		$index = false;
		if(!empty($arr) && !empty($need)){ $index = array_search($need,array_column($arr,'idcurso')); }
		return ($index !== false) ? $arr[$index]['id'] : null;
	}
	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			// var_dump($_POST);exit();
			@extract($_POST);
			$accion = '_add';
			if (empty($idgrupoaula)) {
				$usuarioAct = NegSesion::getUsuario();
				$this->oNegAcad_grupoaula->nombre = date('Y_m_d') . "_" . $idgrado . "_" . $idsesion;
				$this->oNegAcad_grupoaula->tipo = 'v';
				$this->oNegAcad_grupoaula->comentario = '';
				$this->oNegAcad_grupoaula->nvacantes = 1000;
				$this->oNegAcad_grupoaula->estado = 1;
				$this->oNegAcad_grupoaula->idproyecto = $usuarioAct["idproyecto"];
				$this->oNegAcad_grupoaula->oldid = '';
				$this->oNegAcad_grupoaula->idgrado = @$idgrado;
				$this->oNegAcad_grupoaula->idsesion = @$idsesion;
				$idgrupoaula = $this->oNegAcad_grupoaula->agregar();
			}
			//OBTENER LOS CURSOS SENCE , mediante un FLAG para saber si es sence o no...
			$arrSenceCursos = [];
			if(!empty($idempresa) && !empty($idproyecto) && !empty($essence)){
				$arrSenceCursos = $this->oNegSence_assigncursos->buscar(['idempresa'=>$idempresa,'idproyecto'=>$idproyecto,'estado'=>1]);
			}//endif
			if (!empty($varioscursos)) {
				$cursos = json_decode($idcurso);
				foreach ($cursos as $k => $v) {
					//VALIDAR AQUI LOS CURSOS SENCE para setear idsence_assigncursos
					$this->oNegAcad_grupoauladetalle->idsence_assigncursos = $this->getIdSence_assigncurso($v,$arrSenceCursos);

					$accion = '_add';
					$haycurso = $this->oNegAcad_grupoauladetalle->buscar(array('idgrupoaula' => $idgrupoaula, 'idcurso' => $v));
					// $haycurso = $this->oNegAcad_grupoauladetalle->buscar(array('sql4' => true, 'idgrupoaula' => $idgrupoaula, 'idcurso' => $v, 'orderby' => 'no'));
					if (!empty($haycurso[0])) {
						$this->oNegAcad_grupoauladetalle->idgrupoauladetalle = $haycurso[0]["idgrupoauladetalle"];
						$accion = '_edit';
					}
					$this->oNegAcad_grupoauladetalle->idgrupoaula = @$idgrupoaula;
					$this->oNegAcad_grupoauladetalle->idcurso = @$v;
					$this->oNegAcad_grupoauladetalle->iddocente = !empty($iddocente) ? $iddocente : 0;
					$this->oNegAcad_grupoauladetalle->idlocal = !empty($idlocal) ? $idlocal : 0;
					$this->oNegAcad_grupoauladetalle->idambiente = !empty($idambiente) ? $idambiente : 0;
					$this->oNegAcad_grupoauladetalle->nombre = !empty($nombre) ? $nombre : ($idlocal . '-' . $v . '-' . $idgrado . '-' . $idsesion . '-' . $idgrupoaula);
					$this->oNegAcad_grupoauladetalle->fecha_inicio = !empty($fecha_inicio) ? $fecha_inicio : date('Y-m-d');
					$this->oNegAcad_grupoauladetalle->fecha_final = !empty($fecha_final) ? $fecha_final : date('Y-m-d');
					$this->oNegAcad_grupoauladetalle->idgrado = !empty($idgrado) ? $idgrado : 0;
					$this->oNegAcad_grupoauladetalle->idseccion = !empty($idsesion) ? $idsesion : 0;
					$this->oNegAcad_grupoauladetalle->fechamodificacion = !empty($fechamodificacion) ? $fechamodificacion : date('Y-m-d');
					$this->oNegAcad_grupoauladetalle->oldid = @$oldid;
					$this->oNegAcad_grupoauladetalle->dias_extradocente = !empty($dias_extradocente) ? $dias_extradocente : ($accion == '_add' ? 1 : null);

					$tipodecurso_ = (@$tipodecurso == "" || @$tipodecurso == null || @$tipodecurso == "1") ? false : true;
					$idcategoria_ = @$idcategoria == "" || @$idcategoria == null ? "0" : $idcategoria;
					if ($accion == '_add') $res = $this->oNegAcad_grupoauladetalle->agregar($tipodecurso_, $idcategoria_);
					else $res = $this->oNegAcad_grupoauladetalle->editar();
				}
				echo json_encode(array('code' => 200, 'msj' => JrTexto::_('saved successfully'), 'newid' => $res, 'idgrupoaula' => $idgrupoaula));
			} else {
				if (empty($idgrupoauladetalle) && !empty($idgrupoaula) && !empty($idcurso)) {
					$haycurso = $this->oNegAcad_grupoauladetalle->buscar(array('idgrupoaula' => @$idgrupoaula, 'idcurso' => @$idcurso));
					if (!empty($haycurso[0])) {
						$idgrupoauladetalle = $haycurso[0]["idgrupoauladetalle"];
					}
				}
				if (!empty(@$idgrupoauladetalle)) {
					$this->oNegAcad_grupoauladetalle->idgrupoauladetalle = $idgrupoauladetalle;
					$accion = '_edit';
				}
				//VALIDAR AQUI LOS CURSOS SENCE para setear idsence_assigncursos
				$this->oNegAcad_grupoauladetalle->idsence_assigncursos = $this->getIdSence_assigncurso($idcurso,$arrSenceCursos);
				$this->oNegAcad_grupoauladetalle->idgrupoaula = @$idgrupoaula;
				$this->oNegAcad_grupoauladetalle->idcurso = @$idcurso;
				$this->oNegAcad_grupoauladetalle->iddocente = !empty($iddocente) ? $iddocente : 0;
				$this->oNegAcad_grupoauladetalle->idlocal = !empty($idlocal) ? $idlocal : 0;
				$this->oNegAcad_grupoauladetalle->idambiente = !empty($idambiente) ? $idambiente : 0;
				$this->oNegAcad_grupoauladetalle->nombre = !empty($nombre) ? $nombre : ($idlocal . '-' . $idcurso . '-' . $idgrado . '-' . $idsesion . '-' . $idgrupoaula);
				$this->oNegAcad_grupoauladetalle->fecha_inicio = !empty($fecha_inicio) ? $fecha_inicio : date('Y-m-d');
				$this->oNegAcad_grupoauladetalle->fecha_final = !empty($fecha_final) ? $fecha_final : date('Y-m-d');
				$this->oNegAcad_grupoauladetalle->idgrado = !empty($idgrado) ? $idgrado : 0;
				$this->oNegAcad_grupoauladetalle->idseccion = !empty($idsesion) ? $idsesion : 0;
				$this->oNegAcad_grupoauladetalle->fechamodificacion = !empty($fechamodificacion) ? $fechamodificacion : date('Y-m-d');
				$this->oNegAcad_grupoauladetalle->oldid = @$oldid;
				$this->oNegAcad_grupoauladetalle->dias_extradocente = !empty($dias_extradocente) ? $dias_extradocente : ($accion == '_add' ? 1 : null);
				if ($accion == '_add') {
					$tipodecurso_ = (@$tipodecurso == "" || @$tipodecurso == null || @$tipodecurso == "1") ? false : true;
					$idcategoria_ = @$idcategoria == "" || @$idcategoria == null ? "0" : $idcategoria;
					$res = $this->oNegAcad_grupoauladetalle->agregar($tipodecurso_, $idcategoria_);
					echo json_encode(array('code' => 200, 'msj' => JrTexto::_('saved successfully'), 'newid' => $res));
				} else {
					$res = $this->oNegAcad_grupoauladetalle->editar();
					echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update successfully'), 'newid' => $res));
				}
			}

			$usuarioAct = NegSesion::getUsuario(); //asignar rol docente    
			//exit(0);
			if (!empty($iddocente)) {
				$this->oNegPersona_rol->idpersonal = $iddocente;
				$this->oNegPersona_rol->idempresa = !empty($idempresa) ? $idempresa : $usuarioAct["idempresa"];
				$this->oNegPersona_rol->idproyecto = !empty($idproyecto) ? $idproyecto : $usuarioAct["idproyecto"];
				$this->oNegPersona_rol->idrol = !empty($idrol) ? $idrol : 2;
				$res = $this->oNegPersona_rol->agregar();
			}
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function guardarespecial()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (empty($idgrupoaula)) {
				$usuarioAct = NegSesion::getUsuario();
				$this->oNegAcad_grupoaula->nombre = date('Y_m_d') . "_" . $idgrado . "_" . $idsesion;
				$this->oNegAcad_grupoaula->tipo = 'v';
				$this->oNegAcad_grupoaula->comentario = '';
				$this->oNegAcad_grupoaula->nvacantes = 1000;
				$this->oNegAcad_grupoaula->estado = 1;
				$this->oNegAcad_grupoaula->idproyecto = $usuarioAct["idproyecto"];
				$this->oNegAcad_grupoaula->oldid = '';
				$this->oNegAcad_grupoaula->idgrado = @$idgrado;
				$this->oNegAcad_grupoaula->idsesion = @$idsesion;
				$idgrupoaula = $this->oNegAcad_grupoaula->agregar();
			}
			if (!empty($varioscursos)) {
				$cursos = json_decode($idcurso);
				foreach ($cursos as $k => $v) {
					$accion = '_add';
					$haycurso = $this->oNegAcad_grupoauladetalle->buscar(array('idgrupoaula' => $idgrupoaula, 'idcurso' => $v));
					if (!empty($haycurso[0])) {
						$this->oNegAcad_grupoauladetalle->idgrupoauladetalle = $haycurso[0]["idgrupoauladetalle"];
						$accion = '_edit';
					}
					$this->oNegAcad_grupoauladetalle->idgrupoaula = @$idgrupoaula;
					$this->oNegAcad_grupoauladetalle->idcurso = @$v;
					$this->oNegAcad_grupoauladetalle->iddocente = !empty($iddocente) ? $iddocente : 0;
					$this->oNegAcad_grupoauladetalle->idlocal = !empty($idlocal) ? $idlocal : 0;
					$this->oNegAcad_grupoauladetalle->idambiente = !empty($idambiente) ? $idambiente : 0;
					$this->oNegAcad_grupoauladetalle->nombre = !empty($nombre) ? $nombre : ($idlocal . '-' . $v . '-' . $idgrado . '-' . $idsesion . '-' . $idgrupoaula);
					$this->oNegAcad_grupoauladetalle->fecha_inicio = !empty($fecha_inicio) ? $fecha_inicio : date('Y-m-d');
					$this->oNegAcad_grupoauladetalle->fecha_final = !empty($fecha_final) ? $fecha_final : date('Y-m-d');
					$this->oNegAcad_grupoauladetalle->idgrado = !empty($idgrado) ? $idgrado : 0;
					$this->oNegAcad_grupoauladetalle->idseccion = !empty($idsesion) ? $idsesion : 0;
					$this->oNegAcad_grupoauladetalle->fechamodificacion = !empty($fechamodificacion) ? $fechamodificacion : date('Y-m-d');
					$this->oNegAcad_grupoauladetalle->oldid = @$oldid;
					if ($accion == '_add') $res = $this->oNegAcad_grupoauladetalle->agregar();
					else $res = $this->oNegAcad_grupoauladetalle->editar();
				}
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Acad_grupoauladetalle')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res, 'idgrupoaula' => $idgrupoaula));
			} else {
				if (!empty(@$idgrupoauladetalle)) {
					$this->oNegAcad_grupoauladetalle->idgrupoauladetalle = $idgrupoauladetalle;
					$accion = '_edit';
				}

				$this->oNegAcad_grupoauladetalle->idgrupoaula = @$idgrupoaula;
				$this->oNegAcad_grupoauladetalle->idcurso = @$idcurso;
				$this->oNegAcad_grupoauladetalle->iddocente = !empty($iddocente) ? $iddocente : 0;
				$this->oNegAcad_grupoauladetalle->idlocal = !empty($idlocal) ? $idlocal : 0;
				$this->oNegAcad_grupoauladetalle->idambiente = !empty($idambiente) ? $idambiente : 0;
				$this->oNegAcad_grupoauladetalle->nombre = !empty($nombre) ? $nombre : ($idlocal . '-' . $idcurso . '-' . $idgrado . '-' . $idsesion . '-' . $idgrupoaula);
				$this->oNegAcad_grupoauladetalle->fecha_inicio = !empty($fecha_inicio) ? $fecha_inicio : date('Y-m-d');
				$this->oNegAcad_grupoauladetalle->fecha_final = !empty($fecha_final) ? $fecha_final : date('Y-m-d');
				$this->oNegAcad_grupoauladetalle->idgrado = !empty($idgrado) ? $idgrado : 0;
				$this->oNegAcad_grupoauladetalle->idseccion = !empty($idsesion) ? $idsesion : 0;
				$this->oNegAcad_grupoauladetalle->fechamodificacion = !empty($fechamodificacion) ? $fechamodificacion : date('Y-m-d');
				$this->oNegAcad_grupoauladetalle->oldid = @$oldid;
				if ($accion == '_add') {
					$res = $this->oNegAcad_grupoauladetalle->agregar();
					echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Acad_grupoauladetalle')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
				} else {
					$res = $this->oNegAcad_grupoauladetalle->editar();
					echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Acad_grupoauladetalle')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
				}
			}

			$usuarioAct = NegSesion::getUsuario(); //asignar rol docente    
			//var_dump($iddocente);
			//exit(0);
			if (!empty($iddocente)) {
				$this->oNegPersona_rol->idpersonal = $iddocente;
				$this->oNegPersona_rol->idempresa = !empty($idempresa) ? $idempresa : $usuarioAct["idempresa"];
				$this->oNegPersona_rol->idproyecto = !empty($idproyecto) ? $idproyecto : $usuarioAct["idproyecto"];
				$this->oNegPersona_rol->idrol = !empty($idrol) ? $idrol : 2;
				$res = $this->oNegPersona_rol->agregar();
			}
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			$this->oNegAcad_grupoauladetalle->__set('idgrupoauladetalle', $_REQUEST['idgrupoauladetalle']);
			$res = $this->oNegAcad_grupoauladetalle->eliminar();
			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
			exit(0);
		}
	}

	public function eliminar_()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			$res = $this->oNegAcad_grupoauladetalle->eliminar_($_REQUEST['idgrupoauladetalle']);
			if ($res) {
				echo json_encode(array('code' => 200, 'msj' => 'Grupo de Estudio Eliminado'));
			} else {
				echo json_encode(array('code' => 'error', 'msj' => "Ya cuenta con matrículas"));
			}
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			$this->oNegAcad_grupoauladetalle->setCampo($_REQUEST['idgrupoauladetalle'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
			exit(0);
		}
	}
}
