<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegResumen_local_habilidad', RUTA_BASE);
class WebResumen_local_habilidad extends JrWeb
{
	private $oNegResumen_local_habilidad;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegResumen_local_habilidad = new NegResumen_local_habilidad;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Resumen_local_habilidad', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["iddre"])&&@$_REQUEST["iddre"]!='')$filtros["iddre"]=$_REQUEST["iddre"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["ubicacion_hab_L"])&&@$_REQUEST["ubicacion_hab_L"]!='')$filtros["ubicacion_hab_L"]=$_REQUEST["ubicacion_hab_L"];
			if(isset($_REQUEST["ubicacion_hab_R"])&&@$_REQUEST["ubicacion_hab_R"]!='')$filtros["ubicacion_hab_R"]=$_REQUEST["ubicacion_hab_R"];
			if(isset($_REQUEST["ubicacion_hab_W"])&&@$_REQUEST["ubicacion_hab_W"]!='')$filtros["ubicacion_hab_W"]=$_REQUEST["ubicacion_hab_W"];
			if(isset($_REQUEST["ubicacion_hab_S"])&&@$_REQUEST["ubicacion_hab_S"]!='')$filtros["ubicacion_hab_S"]=$_REQUEST["ubicacion_hab_S"];
			if(isset($_REQUEST["entrada_hab_L"])&&@$_REQUEST["entrada_hab_L"]!='')$filtros["entrada_hab_L"]=$_REQUEST["entrada_hab_L"];
			if(isset($_REQUEST["entrada_hab_R"])&&@$_REQUEST["entrada_hab_R"]!='')$filtros["entrada_hab_R"]=$_REQUEST["entrada_hab_R"];
			if(isset($_REQUEST["entrada_hab_W"])&&@$_REQUEST["entrada_hab_W"]!='')$filtros["entrada_hab_W"]=$_REQUEST["entrada_hab_W"];
			if(isset($_REQUEST["entrada_hab_S"])&&@$_REQUEST["entrada_hab_S"]!='')$filtros["entrada_hab_S"]=$_REQUEST["entrada_hab_S"];
			if(isset($_REQUEST["salida_hab_L"])&&@$_REQUEST["salida_hab_L"]!='')$filtros["salida_hab_L"]=$_REQUEST["salida_hab_L"];
			if(isset($_REQUEST["salida_hab_R"])&&@$_REQUEST["salida_hab_R"]!='')$filtros["salida_hab_R"]=$_REQUEST["salida_hab_R"];
			if(isset($_REQUEST["salida_hab_W"])&&@$_REQUEST["salida_hab_W"]!='')$filtros["salida_hab_W"]=$_REQUEST["salida_hab_W"];
			if(isset($_REQUEST["salida_hab_S"])&&@$_REQUEST["salida_hab_S"]!='')$filtros["salida_hab_S"]=$_REQUEST["salida_hab_S"];
			if(isset($_REQUEST["examen_b1_hab_L"])&&@$_REQUEST["examen_b1_hab_L"]!='')$filtros["examen_b1_hab_L"]=$_REQUEST["examen_b1_hab_L"];
			if(isset($_REQUEST["examen_b1_hab_R"])&&@$_REQUEST["examen_b1_hab_R"]!='')$filtros["examen_b1_hab_R"]=$_REQUEST["examen_b1_hab_R"];
			if(isset($_REQUEST["examen_b1_hab_W"])&&@$_REQUEST["examen_b1_hab_W"]!='')$filtros["examen_b1_hab_W"]=$_REQUEST["examen_b1_hab_W"];
			if(isset($_REQUEST["examen_b1_hab_S"])&&@$_REQUEST["examen_b1_hab_S"]!='')$filtros["examen_b1_hab_S"]=$_REQUEST["examen_b1_hab_S"];
			if(isset($_REQUEST["examen_b2_hab_L"])&&@$_REQUEST["examen_b2_hab_L"]!='')$filtros["examen_b2_hab_L"]=$_REQUEST["examen_b2_hab_L"];
			if(isset($_REQUEST["examen_b2_hab_R"])&&@$_REQUEST["examen_b2_hab_R"]!='')$filtros["examen_b2_hab_R"]=$_REQUEST["examen_b2_hab_R"];
			if(isset($_REQUEST["examen_b2_hab_W"])&&@$_REQUEST["examen_b2_hab_W"]!='')$filtros["examen_b2_hab_W"]=$_REQUEST["examen_b2_hab_W"];
			if(isset($_REQUEST["examen_b2_hab_S"])&&@$_REQUEST["examen_b2_hab_S"]!='')$filtros["examen_b2_hab_S"]=$_REQUEST["examen_b2_hab_S"];
			if(isset($_REQUEST["examen_b3_hab_L"])&&@$_REQUEST["examen_b3_hab_L"]!='')$filtros["examen_b3_hab_L"]=$_REQUEST["examen_b3_hab_L"];
			if(isset($_REQUEST["examen_b3_hab_R"])&&@$_REQUEST["examen_b3_hab_R"]!='')$filtros["examen_b3_hab_R"]=$_REQUEST["examen_b3_hab_R"];
			if(isset($_REQUEST["examen_b3_hab_W"])&&@$_REQUEST["examen_b3_hab_W"]!='')$filtros["examen_b3_hab_W"]=$_REQUEST["examen_b3_hab_W"];
			if(isset($_REQUEST["examen_b3_hab_S"])&&@$_REQUEST["examen_b3_hab_S"]!='')$filtros["examen_b3_hab_S"]=$_REQUEST["examen_b3_hab_S"];
			if(isset($_REQUEST["examen_b4_hab_L"])&&@$_REQUEST["examen_b4_hab_L"]!='')$filtros["examen_b4_hab_L"]=$_REQUEST["examen_b4_hab_L"];
			if(isset($_REQUEST["examen_b4_hab_R"])&&@$_REQUEST["examen_b4_hab_R"]!='')$filtros["examen_b4_hab_R"]=$_REQUEST["examen_b4_hab_R"];
			if(isset($_REQUEST["examen_b4_hab_W"])&&@$_REQUEST["examen_b4_hab_W"]!='')$filtros["examen_b4_hab_W"]=$_REQUEST["examen_b4_hab_W"];
			if(isset($_REQUEST["examen_b4_hab_S"])&&@$_REQUEST["examen_b4_hab_S"]!='')$filtros["examen_b4_hab_S"]=$_REQUEST["examen_b4_hab_S"];
			if(isset($_REQUEST["examen_t1_hab_L"])&&@$_REQUEST["examen_t1_hab_L"]!='')$filtros["examen_t1_hab_L"]=$_REQUEST["examen_t1_hab_L"];
			if(isset($_REQUEST["examen_t1_hab_R"])&&@$_REQUEST["examen_t1_hab_R"]!='')$filtros["examen_t1_hab_R"]=$_REQUEST["examen_t1_hab_R"];
			if(isset($_REQUEST["examen_t1_hab_W"])&&@$_REQUEST["examen_t1_hab_W"]!='')$filtros["examen_t1_hab_W"]=$_REQUEST["examen_t1_hab_W"];
			if(isset($_REQUEST["examen_t1_hab_S"])&&@$_REQUEST["examen_t1_hab_S"]!='')$filtros["examen_t1_hab_S"]=$_REQUEST["examen_t1_hab_S"];
			if(isset($_REQUEST["examen_t2_hab_L"])&&@$_REQUEST["examen_t2_hab_L"]!='')$filtros["examen_t2_hab_L"]=$_REQUEST["examen_t2_hab_L"];
			if(isset($_REQUEST["examen_t2_hab_R"])&&@$_REQUEST["examen_t2_hab_R"]!='')$filtros["examen_t2_hab_R"]=$_REQUEST["examen_t2_hab_R"];
			if(isset($_REQUEST["examen_t2_hab_W"])&&@$_REQUEST["examen_t2_hab_W"]!='')$filtros["examen_t2_hab_W"]=$_REQUEST["examen_t2_hab_W"];
			if(isset($_REQUEST["examen_t2_hab_S"])&&@$_REQUEST["examen_t2_hab_S"]!='')$filtros["examen_t2_hab_S"]=$_REQUEST["examen_t2_hab_S"];
			if(isset($_REQUEST["examen_t3_hab_L"])&&@$_REQUEST["examen_t3_hab_L"]!='')$filtros["examen_t3_hab_L"]=$_REQUEST["examen_t3_hab_L"];
			if(isset($_REQUEST["examen_t3_hab_R"])&&@$_REQUEST["examen_t3_hab_R"]!='')$filtros["examen_t3_hab_R"]=$_REQUEST["examen_t3_hab_R"];
			if(isset($_REQUEST["examen_t3_hab_W"])&&@$_REQUEST["examen_t3_hab_W"]!='')$filtros["examen_t3_hab_W"]=$_REQUEST["examen_t3_hab_W"];
			if(isset($_REQUEST["examen_t3_hab_S"])&&@$_REQUEST["examen_t3_hab_S"]!='')$filtros["examen_t3_hab_S"]=$_REQUEST["examen_t3_hab_S"];
			if(isset($_REQUEST["prog_hab_A1_L"])&&@$_REQUEST["prog_hab_A1_L"]!='')$filtros["prog_hab_A1_L"]=$_REQUEST["prog_hab_A1_L"];
			if(isset($_REQUEST["prog_hab_A1_R"])&&@$_REQUEST["prog_hab_A1_R"]!='')$filtros["prog_hab_A1_R"]=$_REQUEST["prog_hab_A1_R"];
			if(isset($_REQUEST["prog_hab_A1_W"])&&@$_REQUEST["prog_hab_A1_W"]!='')$filtros["prog_hab_A1_W"]=$_REQUEST["prog_hab_A1_W"];
			if(isset($_REQUEST["prog_hab_A1_S"])&&@$_REQUEST["prog_hab_A1_S"]!='')$filtros["prog_hab_A1_S"]=$_REQUEST["prog_hab_A1_S"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegResumen_local_habilidad->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id)) {
				$this->oNegResumen_local_habilidad->id = $id;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegResumen_local_habilidad->id_ubigeo=@$id_ubigeo;
				$this->oNegResumen_local_habilidad->iddre=@$iddre;
				$this->oNegResumen_local_habilidad->idugel=@$idugel;
				$this->oNegResumen_local_habilidad->idlocal=@$idlocal;
				$this->oNegResumen_local_habilidad->tipo=@$tipo;
				$this->oNegResumen_local_habilidad->ubicacion_hab_L=@$ubicacion_hab_L;
				$this->oNegResumen_local_habilidad->ubicacion_hab_R=@$ubicacion_hab_R;
				$this->oNegResumen_local_habilidad->ubicacion_hab_W=@$ubicacion_hab_W;
				$this->oNegResumen_local_habilidad->ubicacion_hab_S=@$ubicacion_hab_S;
				$this->oNegResumen_local_habilidad->entrada_hab_L=@$entrada_hab_L;
				$this->oNegResumen_local_habilidad->entrada_hab_R=@$entrada_hab_R;
				$this->oNegResumen_local_habilidad->entrada_hab_W=@$entrada_hab_W;
				$this->oNegResumen_local_habilidad->entrada_hab_S=@$entrada_hab_S;
				$this->oNegResumen_local_habilidad->salida_hab_L=@$salida_hab_L;
				$this->oNegResumen_local_habilidad->salida_hab_R=@$salida_hab_R;
				$this->oNegResumen_local_habilidad->salida_hab_W=@$salida_hab_W;
				$this->oNegResumen_local_habilidad->salida_hab_S=@$salida_hab_S;
				$this->oNegResumen_local_habilidad->examen_b1_hab_L=@$examen_b1_hab_L;
				$this->oNegResumen_local_habilidad->examen_b1_hab_R=@$examen_b1_hab_R;
				$this->oNegResumen_local_habilidad->examen_b1_hab_W=@$examen_b1_hab_W;
				$this->oNegResumen_local_habilidad->examen_b1_hab_S=@$examen_b1_hab_S;
				$this->oNegResumen_local_habilidad->examen_b2_hab_L=@$examen_b2_hab_L;
				$this->oNegResumen_local_habilidad->examen_b2_hab_R=@$examen_b2_hab_R;
				$this->oNegResumen_local_habilidad->examen_b2_hab_W=@$examen_b2_hab_W;
				$this->oNegResumen_local_habilidad->examen_b2_hab_S=@$examen_b2_hab_S;
				$this->oNegResumen_local_habilidad->examen_b3_hab_L=@$examen_b3_hab_L;
				$this->oNegResumen_local_habilidad->examen_b3_hab_R=@$examen_b3_hab_R;
				$this->oNegResumen_local_habilidad->examen_b3_hab_W=@$examen_b3_hab_W;
				$this->oNegResumen_local_habilidad->examen_b3_hab_S=@$examen_b3_hab_S;
				$this->oNegResumen_local_habilidad->examen_b4_hab_L=@$examen_b4_hab_L;
				$this->oNegResumen_local_habilidad->examen_b4_hab_R=@$examen_b4_hab_R;
				$this->oNegResumen_local_habilidad->examen_b4_hab_W=@$examen_b4_hab_W;
				$this->oNegResumen_local_habilidad->examen_b4_hab_S=@$examen_b4_hab_S;
				$this->oNegResumen_local_habilidad->examen_t1_hab_L=@$examen_t1_hab_L;
				$this->oNegResumen_local_habilidad->examen_t1_hab_R=@$examen_t1_hab_R;
				$this->oNegResumen_local_habilidad->examen_t1_hab_W=@$examen_t1_hab_W;
				$this->oNegResumen_local_habilidad->examen_t1_hab_S=@$examen_t1_hab_S;
				$this->oNegResumen_local_habilidad->examen_t2_hab_L=@$examen_t2_hab_L;
				$this->oNegResumen_local_habilidad->examen_t2_hab_R=@$examen_t2_hab_R;
				$this->oNegResumen_local_habilidad->examen_t2_hab_W=@$examen_t2_hab_W;
				$this->oNegResumen_local_habilidad->examen_t2_hab_S=@$examen_t2_hab_S;
				$this->oNegResumen_local_habilidad->examen_t3_hab_L=@$examen_t3_hab_L;
				$this->oNegResumen_local_habilidad->examen_t3_hab_R=@$examen_t3_hab_R;
				$this->oNegResumen_local_habilidad->examen_t3_hab_W=@$examen_t3_hab_W;
				$this->oNegResumen_local_habilidad->examen_t3_hab_S=@$examen_t3_hab_S;
				$this->oNegResumen_local_habilidad->prog_hab_A1_L=@$prog_hab_A1_L;
				$this->oNegResumen_local_habilidad->prog_hab_A1_R=@$prog_hab_A1_R;
				$this->oNegResumen_local_habilidad->prog_hab_A1_W=@$prog_hab_A1_W;
				$this->oNegResumen_local_habilidad->prog_hab_A1_S=@$prog_hab_A1_S;
				
            if($accion=='_add') {
            	$res=$this->oNegResumen_local_habilidad->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Resumen_local_habilidad')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegResumen_local_habilidad->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Resumen_local_habilidad')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegResumen_local_habilidad->__set('id', $_REQUEST['id']);
			$res=$this->oNegResumen_local_habilidad->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegResumen_local_habilidad->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}