<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-10-2019 
 * @copyright	Copyright (C) 15-10-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE);
class WebAcad_matricula extends JrWeb
{
	private $oNegAcad_matricula;
	private $oNegBitacora_alumno_smartbook;
 	private $oNegBitacora_smartbook;

	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegBitacora_alumno_smartbook = new NegBitacora_alumno_smartbook;
		$this->oNegBitacora_smartbook = new NegBitacora_smartbook;
		
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["fecha_vencimiento"])&&@$_REQUEST["fecha_vencimiento"]!='')$filtros["fecha_vencimiento"]=$_REQUEST["fecha_vencimiento"];
			if(isset($_REQUEST["oldid"])&&@$_REQUEST["oldid"]!='')$filtros["oldid"]=$_REQUEST["oldid"];
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			//var_dump($filtros);			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(isset($_REQUEST["orderby"])&&@$_REQUEST["orderby"]!='')$filtros["orderby"]=$_REQUEST["orderby"];
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["fechaactiva"])&&@$_REQUEST["fechaactiva"]!='')$filtros["fechaactiva"]=$_REQUEST["fechaactiva"];
			if(isset($_REQUEST["sql"])&&@$_REQUEST["sql"]!='')$filtros["sql"]=$_REQUEST["sql"];
			if(!empty($_REQUEST["idproyecto"])){
				$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			}else{
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idproyecto"]=$usuarioAct["idproyecto"];
			}
			$this->datos=$this->oNegAcad_matricula->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function leer(){
		try {
			$filtros=array();

			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["fecha_vencimiento"])&&@$_REQUEST["fecha_vencimiento"]!='')$filtros["fecha_vencimiento"]=$_REQUEST["fecha_vencimiento"];

			$res=$this->oNegAcad_matricula->buscar2($filtros);		
			echo json_encode(array('code'=>200,'data'=>$res));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
			exit(0);
		}
	}
	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($variosgrupos)){
            	//$cursos=json_decode($idcurso);
            	$alumnos=json_decode($idalumnos);
            	$gruposauladetalle=json_decode($idgruposauladetalle);
            	$matri=array();
            	foreach ($alumnos as $ka => $al){
            		$nmatr=$mmat=0;
            		foreach ($gruposauladetalle as $kg => $g){
            			$hay=$this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$g,'idalumno'=>$al));
            			$accion='_Add';
	            		if(!empty($hay[0])){
	            			$this->oNegAcad_matricula->idmatricula = $hay[0]["idmatricula"];
							$accion='_edit';
	            		}
	            		$usuarioAct = NegSesion::getUsuario();
	            		$this->oNegAcad_matricula->idgrupoauladetalle=@$g;
						$this->oNegAcad_matricula->idalumno=@$al;
	            		$this->oNegAcad_matricula->fecha_registro=!empty($fecha_registro)?$fecha_registro:date('Y-m-d');
						$this->oNegAcad_matricula->estado=!empty($estado)?$estado:1;
						$this->oNegAcad_matricula->idusuario=!empty($idusuario)?$idusuario:$usuarioAct["idpersona"];
						$this->oNegAcad_matricula->fecha_matricula=!empty($fecha_matricula)?$fecha_matricula:date('Y-m-d');
						$this->oNegAcad_matricula->oldid=@$oldid;
						$this->oNegAcad_matricula->fecha_vencimiento=!empty($fecha_vencimiento)?$fecha_vencimiento:date("Y-m-d H:i:s",strtotime(date('Y-m-d')."+ 4 month"));
						if($accion=='_edit'){
							$res=$this->oNegAcad_matricula->editar();
							$mmat++;
						}else {
							$res=$this->oNegAcad_matricula->agregar();
							$nmatr++;
						}            		
            		}
            		$matri[$al]=array('nuevos'=>$nmatr,'actualizados'=>$mmat);
            	}            	
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_matricula')).' '.JrTexto::_('saved successfully'),'data'=>json_encode($matri)));
            }else{
            	$hay=$this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle,'idalumno'=>$idalumno));
        		if(!empty($hay[0])){
        			$this->oNegAcad_matricula->idmatricula = $haycurso[0]["idmatricula"];
					$accion='_edit';
        		}	            
	           	$usuarioAct = NegSesion::getUsuario();
				$this->oNegAcad_matricula->idgrupoauladetalle=@$idgrupoauladetalle;
				$this->oNegAcad_matricula->idalumno=@$idalumno;
				$this->oNegAcad_matricula->fecha_registro=!empty($fecha_registro)?$fecha_registro:date('Y-m-d');
				$this->oNegAcad_matricula->estado=!empty($estado)?$estado:1;
				$this->oNegAcad_matricula->idusuario=!empty($idusuario)?$idusuario:$usuarioAct["idpersona"];
				$this->oNegAcad_matricula->fecha_matricula=!empty($fecha_matricula)?$fecha_matricula:date('Y-m-d');
				$this->oNegAcad_matricula->oldid=@$oldid;
				$this->oNegAcad_matricula->fecha_vencimiento=!empty($fecha_vencimiento)?$fecha_vencimiento:date("Y-m-d H:i:s",strtotime(date('Y-m-d')."+ 4 month"));				
	            if($accion=='_add') {
	            	$res=$this->oNegAcad_matricula->agregar();
	            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_matricula')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
	            }else{
	            	$res=$this->oNegAcad_matricula->editar();
	            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_matricula')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
	            }
            }            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegAcad_matricula->idgrupoauladetalle=@$idgrupoauladetalle;
					$this->oNegAcad_matricula->idalumno=@$idalumno;
					$this->oNegAcad_matricula->fecha_registro=@$fecha_registro;
					$this->oNegAcad_matricula->estado=@$estado;
					$this->oNegAcad_matricula->idusuario=@$idusuario;
					$this->oNegAcad_matricula->fecha_matricula=@$fecha_matricula;
					$this->oNegAcad_matricula->oldid=@$oldid;
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function eliminarmatricula(){
		try{
			if(empty($_REQUEST)){
				throw new Exception("Falta campos por definir", 1);
			}
			if(empty($_REQUEST['idmatricula'])){
				throw new Exception("Error falta definir el id matricula", 1);
			}
			if(empty($_REQUEST['idusuario'])){
				throw new Exception("Error falta definir el id usuario", 1);
			}
			if(empty($_REQUEST['idcurso'])){
				throw new Exception("Error falta definir el id curso", 1);
			}
			$_filtros = array('idusuario'=>$_REQUEST['idusuario'],'idcurso'=>$_REQUEST['idcurso']);
			if(isset($_REQUEST['idcomplementario'])){
				$_filtros['idcomplementario'] = $_REQUEST['idcomplementario'];
			}else{
				//Buscar aquí el complementario de la matrícula
				$rs_matricula = $this->oNegAcad_matricula->getmatricula(['idmatricula'=>$_REQUEST['idmatricula']]);
				if(!empty($rs_matricula)){
					$row = end($rs_matricula);
					if(!empty($row['idcomplementario'])){ $_filtros['idcomplementario'] = $row['idcomplementario']; }
				}
			}
			//borrar la bitacora
			$rs_bas = $this->oNegBitacora_alumno_smartbook->buscar($_filtros);
			if(!empty($rs_bas)){
				$ids = array_values(array_column($rs_bas, 'idbitacora_smartbook'));
				
				if(!$this->oNegBitacora_smartbook->multidelete($ids)){
					throw new Exception("fallo algo al eliminar la bitacora");
				}
				if(!$this->oNegBitacora_alumno_smartbook->multidelete($ids)){
					throw new Exception("fallo algo al eliminar la bitacora alumno");
				}

			}
			//borrar la matricula
			$this->oNegAcad_matricula->__set('idmatricula', $_REQUEST['idmatricula']);
			$res=$this->oNegAcad_matricula->eliminar();		
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit(0);
		}
	}
	/**
	 * Funcion que se encarga de actualizar registro de manera multiple en una tabla de manera generica
	 * Se requiere que los datos que se envien por parametro tengan el campo clave del identificador
	 * ATENCION: Abstener de hacer una logica especial en esta función, ya que se reutilizara en varias funcionalidades, por el simple hecho que es una funcion que opera de forma generica en el sistema
	 * @param Array Datos que seran actualizado de manera multiple
	 */
	public function multipleupdate(){
		try{
			global $aplicacion;
			
			if(empty($_POST)){ throw new Exception("Valores del POST vacio"); }
			if(!empty($_POST['datos']) && !is_array($_POST['datos'])){ $_POST['datos'] = json_decode($_POST['datos'],true); }
			if(empty($_POST['datos'])){ throw new Exception("No hay datos enviados"); }
			
			$data = is_array($_POST['datos']) ? $_POST['datos'] : json_decode($_POST['datos'],true);
			$ids_check = array_column($_POST['datos'],'idmatricula');
			$esEstricto = false;

			if(empty($data) || !is_array($data)){ throw new Exception("No se puede proceder a actualizar, el dato no es un arreglo o es null"); }
			if(empty($ids_check)){ throw new Exception("No hay idmatricula en la data"); }
			if(!empty($_POST['estricto'])){ $esEstricto = true; }
			
			$this->oNegAcad_matricula->multipleupdate($data,$esEstricto);
			
			echo json_encode(array('code'=>200,'msj'=>'Valores Actualizados'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit(0);
		}
	}
		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
				exit(0);
			}

			$this->oNegAcad_matricula->__set('idmatricula', $_REQUEST['idmatricula']);
			$res=$this->oNegAcad_matricula->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
			exit(0);
		}
	}
	/**
	 * Funcion que se encarga de setear campos en la tabla de matriculas de manera directa
	 * @param Integer Id de la matricula
	 * @param String Nombre del campo a modificar
	 * @param String Valor del campo a modificar
	 * @return String JSON del resultado de la petición
	 */
	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
				exit(0);
			}
			$this->oNegAcad_matricula->setCampo($_REQUEST['idmatricula'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit(0);
		}
	}
	public function setCampoAll(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
				exit(0);
			}
			$this->oNegAcad_matricula->setCampoAll($_REQUEST['campoid'],$_REQUEST['valorid'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
			exit(0);
		}
	}   
	public function ingresados(){
		try{
			$filtros = [];

			if (!empty($_REQUEST["idproyecto"])) { $filtros["idproyecto"] = $_REQUEST["idproyecto"] ; }
			if (!empty($_REQUEST["idmatricula"])) { $filtros["idmatricula"] = $_REQUEST["idmatricula"] ; }
			if (!empty($_REQUEST["idalumno"])) { $filtros["idalumno"] = $_REQUEST["idalumno"] ; }
			if (!empty($_REQUEST["fecha_registro"])) { $filtros["fecha_registro"] = $_REQUEST["fecha_registro"] ; }
			if (!empty($_REQUEST["desde_fecha_registro"])) { $filtros["desde_fecha_registro"] = $_REQUEST["desde_fecha_registro"] ; }
			if (!empty($_REQUEST["hasta_fecha_registro"])) { $filtros["hasta_fecha_registro"] = $_REQUEST["hasta_fecha_registro"] ; }
			if (isset($_REQUEST['estado'])) { $filtros['estado'] = $_REQUEST['estado'] ; }
			if (!empty($_REQUEST['tipomatricula'])) { $filtros['tipomatricula'] = $_REQUEST['tipomatricula'] ; }
			if (!empty($_REQUEST['fecha_matricula'])) { $filtros['fecha_matricula'] = $_REQUEST['fecha_matricula'] ; }
			if (!empty($_REQUEST['fecha_vencimiento'])) { $filtros['fecha_vencimiento'] = $_REQUEST['fecha_vencimiento'] ; }
			if (!empty($_REQUEST['idcomplementario'])) { $filtros['idcomplementario'] = $_REQUEST['idcomplementario'] ; }
			if (!empty($_REQUEST['idcategoria'])) { $filtros['idcategoria'] = $_REQUEST['idcategoria'] ; }
			
			$rs = $this->oNegAcad_matricula->ingresados($filtros);

			echo json_encode(["code"=>200,"msj"=>'Proceso completado',"data"=>$rs]);
			exit(0);
		}catch(Exception $e){
			echo json_encode(["code"=> "error", "msj"=>$e->getMessage()]);
			exit(0);
		}
	}
}