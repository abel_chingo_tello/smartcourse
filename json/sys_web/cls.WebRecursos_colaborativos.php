<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-12-2020 
 * @copyright	Copyright (C) 02-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRecursos_colaborativos', RUTA_BASE);
class WebRecursos_colaborativos extends JrWeb
{
	private $oNegRecursos_colaborativos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRecursos_colaborativos = new NegRecursos_colaborativos;
		
	}

	public function defecto(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Recursos_colaborativos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idrecurso"])) $filtros["idrecurso"]=$_REQUEST["idrecurso"];

			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"]; 
  			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"]; 
  			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"]; 
  			if(isset($_REQUEST["file"])&&@$_REQUEST["file"]!='')$filtros["file"]=$_REQUEST["file"]; 
  			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"]; 
  			if(isset($_REQUEST["fecha_creacion"])&&@$_REQUEST["fecha_creacion"]!='')$filtros["fecha_creacion"]=$_REQUEST["fecha_creacion"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;

			if(!empty($filtros["idproyecto"])){
  				$filtros["idproyecto"]=@$usuarioAct["idproyecto"];
  			}
					
			$this->datos=$this->oNegRecursos_colaborativos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            //$empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($idrecurso)) {
				$this->oNegRecursos_colaborativos->idrecurso = $idrecurso;
				$accion='_edit';
			}

           	$this->oNegRecursos_colaborativos->idproyecto=$usuarioAct['idproyecto'];
  			$this->oNegRecursos_colaborativos->titulo=@$titulo;
  			$this->oNegRecursos_colaborativos->descripcion=@$descripcion;
  			$this->oNegRecursos_colaborativos->file=@$file;
  			$this->oNegRecursos_colaborativos->tipo=@$tipo;
  			$this->oNegRecursos_colaborativos->estado=@$estado;
  			$this->oNegRecursos_colaborativos->idusuario=$usuarioAct['idpersona'];
  				        
            if($accion=='_add') {
            	$res=$this->oNegRecursos_colaborativos->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursos_colaborativos')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegRecursos_colaborativos->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursos_colaborativos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRecursos_colaborativos->__set('idrecurso', $_REQUEST['idrecurso']);
			$res=$this->oNegRecursos_colaborativos->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRecursos_colaborativos->setCampo($_REQUEST['idrecurso'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}