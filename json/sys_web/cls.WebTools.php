<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_SITIO') or die();
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
class WebTools extends JrWeb
{


    public function __construct()
    {
        
        parent::__construct(); 
        $this->oNegAcad_matricula = new NegAcad_matricula;
    }

    public function defecto(){
        return false;
    }            
    public function usuario(){
        
        
        $this->datos=$this->user = NegSesion::getUsuario();
        echo json_encode(array('code'=>200,'data'=>$this->datos));
    }
    public function test(){
        $filtros=array();
        if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
        $data = $this->oNegAcad_matricula->buscar($filtros);
        echo json_encode($data);
    }
}