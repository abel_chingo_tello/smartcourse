<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-03-2020 
 * @copyright	Copyright (C) 21-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegValoracion', RUTA_BASE);
class WebValoracion extends JrWeb
{
	private $oNegValoracion;

	public function __construct()
	{
		parent::__construct();
		$this->oNegValoracion = new NegValoracion;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Valoracion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idvaloracion"]) && @$_REQUEST["idvaloracion"] != '') $filtros["idvaloracion"] = $_REQUEST["idvaloracion"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != ''){
				$filtros["idalumno"] = $_REQUEST["idalumno"];				
				$tienecomas=strpos($filtros["idalumno"],",");				
				if ($tienecomas !== false)$filtros["idalumno"]=@explode(",",$filtros["idalumno"]);
			}
			if (isset($_REQUEST["puntuacion"]) && @$_REQUEST["puntuacion"] != '') $filtros["puntuacion"] = $_REQUEST["puntuacion"];
			if (isset($_REQUEST["comentario"]) && @$_REQUEST["comentario"] != '') $filtros["comentario"] = $_REQUEST["comentario"];
			if (isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegValoracion->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function test()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			echo json_encode(NegSesion::getUsuario());
			// echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			if($idgrupoauladetalle<=0){
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Valoracion')) . ' ' . JrTexto::_('Necesita un grupo aula '), 'newid' => $res));
				exit(0);
			}

			$estados = array(
				'idcurso' => $idcurso, 'idgrupoaula' => $idgrupoaula, 'idcomplementario' => $idcomplementario, 'idalumno' => $idalumno, 'puntuacion' => $puntuacion, 'comentario' => $comentario,'idgrupoauladetalle'=>$idgrupoauladetalle
			);

			$res = $this->oNegValoracion->agregar($estados);
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Valoracion')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegValoracion->idcurso = @$idcurso;
					$this->oNegValoracion->idrupoaula = @$idrupoaula;
					$this->oNegValoracion->idcomplementario = @$idcomplementario;
					$this->oNegValoracion->idalumno = @$idalumno;
					$this->oNegValoracion->puntuacion = @$puntuacion;
					$this->oNegValoracion->comentario = @$comentario;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegValoracion->__set('idvaloracion', $_REQUEST['idvaloracion']);
			$res = $this->oNegValoracion->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegValoracion->setCampo($_REQUEST['idvaloracion'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
