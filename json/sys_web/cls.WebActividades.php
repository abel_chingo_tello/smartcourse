<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividades', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE);
class WebActividades extends JrWeb
{
	private $oNegActividades;
	private $oNegActividad;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegActividades = new NegActividades;
		$this->oNegActividad = new NegActividad;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Actividades', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nivel"])&&@$_REQUEST["nivel"]!='')$filtros["nivel"]=$_REQUEST["nivel"];
			if(isset($_REQUEST["unidad"])&&@$_REQUEST["unidad"]!='')$filtros["unidad"]=$_REQUEST["unidad"];
			if(isset($_REQUEST["sesion"])&&@$_REQUEST["sesion"]!='')$filtros["sesion"]=$_REQUEST["sesion"];
			if(isset($_REQUEST["metodologia"])&&@$_REQUEST["metodologia"]!='')$filtros["metodologia"]=$_REQUEST["metodologia"];
			if(isset($_REQUEST["habilidad"])&&@$_REQUEST["habilidad"]!='')$filtros["habilidad"]=$_REQUEST["habilidad"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["url"])&&@$_REQUEST["url"]!='')$filtros["url"]=$_REQUEST["url"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegActividades->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idactividad)) {
				$this->oNegActividades->idactividad = $idactividad;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegActividades->nivel=@$nivel;
				$this->oNegActividades->unidad=@$unidad;
				$this->oNegActividades->sesion=@$sesion;
				$this->oNegActividades->metodologia=@$metodologia;
				$this->oNegActividades->habilidad=@$habilidad;
				$this->oNegActividades->titulo=@$titulo;
				$this->oNegActividades->descripcion=@$descripcion;
				$this->oNegActividades->estado=@$estado;
				$this->oNegActividades->url=@$url;
				$this->oNegActividades->idioma=@$idioma;
				$this->oNegActividades->idpersonal=@$idpersonal;
				
            if($accion=='_add') {
            	$res=$this->oNegActividades->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Actividades')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegActividades->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Actividades')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegActividades->__set('idactividad', $_REQUEST['idactividad']);
			$res=$this->oNegActividades->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegActividades->setCampo($_REQUEST['idactividad'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}