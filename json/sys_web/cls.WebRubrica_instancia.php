<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020 
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRubrica_instancia', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRubrica_indicador_alumno', RUTA_BASE);
class WebRubrica_instancia extends JrWeb
{
	private $oNegRubrica_instancia;

	public function __construct()
	{
		parent::__construct();
		$this->oNegRubrica_instancia = new NegRubrica_instancia;
		$this->oNegRubrica_indicador_alumno = new NegRubrica_indicador_alumno;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Rubrica_instancia', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			$getEditable = false;
			if (isset($_REQUEST["idrubrica_instancia"]) && @$_REQUEST["idrubrica_instancia"] != '') $filtros["idrubrica_instancia"] = $_REQUEST["idrubrica_instancia"];
			if (isset($_REQUEST["idrubrica"]) && @$_REQUEST["idrubrica"] != '') $filtros["idrubrica"] = $_REQUEST["idrubrica"];
			if (isset($_REQUEST["idcursodetalle"]) && @$_REQUEST["idcursodetalle"] != '') $filtros["idcursodetalle"] = $_REQUEST["idcursodetalle"];
			if (isset($_REQUEST["idpestania"]) && @$_REQUEST["idpestania"] != '') $filtros["idpestania"] = $_REQUEST["idpestania"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			if (isset($_REQUEST["getEditable"]) && @$_REQUEST["getEditable"] != '') {
				$getEditable = filter_var($_REQUEST["getEditable"], FILTER_VALIDATE_BOOLEAN);
			}

			$arrRubricaInstancia = $this->oNegRubrica_instancia->buscar($filtros);
			foreach ($arrRubricaInstancia as $key => $instancia) {
				if ($getEditable) {
					$totalIndicadorAlumno = $this->oNegRubrica_indicador_alumno->contar(array('idrubrica_instancia' => $instancia['idrubrica_instancia']))[0]['total'];
					if ($totalIndicadorAlumno > 0) {
						$instancia['editable'] = false;
					} else {
						$instancia['editable'] = true;
					}
					$arrRubricaInstancia[$key] = $instancia;
				}
			}
			$this->datos = $arrRubricaInstancia;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idrubrica_instancia)) {
				$this->oNegRubrica_instancia->idrubrica_instancia = $idrubrica_instancia;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();


			$this->oNegRubrica_instancia->idrubrica = @$idrubrica;
			$this->oNegRubrica_instancia->idcursodetalle = @$idcursodetalle;
			$this->oNegRubrica_instancia->idpestania = @$idpestania;
			$this->oNegRubrica_instancia->idcomplementario = @$idcomplementario;
			$this->oNegRubrica_instancia->idcurso = @$idcurso;

			if ($accion == '_add') {
				$res = $this->oNegRubrica_instancia->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica_instancia')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegRubrica_instancia->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica_instancia')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegRubrica_instancia->idrubrica = @$idrubrica;
					$this->oNegRubrica_instancia->idcursodetalle = @$idcursodetalle;
					$this->oNegRubrica_instancia->idpestania = @$idpestania;
					$this->oNegRubrica_instancia->idcomplementario = @$idcomplementario;
					$this->oNegRubrica_instancia->idcurso = @$idcurso;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function deleteAll()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			if (isset($_REQUEST["idrubrica_instancia"]) && @$_REQUEST["idrubrica_instancia"] != '') $filtros["idrubrica_instancia"] = $_REQUEST["idrubrica_instancia"];
			if (isset($_REQUEST["idrubrica"]) && @$_REQUEST["idrubrica"] != '') $filtros["idrubrica"] = $_REQUEST["idrubrica"];
			if (isset($_REQUEST["idcursodetalle"]) && @$_REQUEST["idcursodetalle"] != '') $filtros["idcursodetalle"] = $_REQUEST["idcursodetalle"];
			if (isset($_REQUEST["idpestania"]) && @$_REQUEST["idpestania"] != '') $filtros["idpestania"] = $_REQUEST["idpestania"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];


			$arrInstancias = $this->oNegRubrica_instancia->buscar($filtros);
			foreach ($arrInstancias as $key => $instancia) {
				$this->oNegRubrica_instancia->__set('idrubrica_instancia', $instancia['idrubrica_instancia']);
				// 
				$filtrosIA = array(
					"idrubrica" => $instancia['idrubrica'],
					"idrubrica_instancia" => $instancia['idrubrica_instancia']
				);
				$arrIndicadorAlumno = $this->oNegRubrica_indicador_alumno->buscar($filtrosIA);
				foreach ($arrIndicadorAlumno as $key => $indicadorAlumno) {
					$this->oNegRubrica_indicador_alumno->idrubrica_indicador_alumno = $indicadorAlumno['idrubrica_indicador_alumno'];
					$this->oNegRubrica_indicador_alumno->eliminar();
				}
				// 
				$res = $this->oNegRubrica_instancia->eliminar();
			}
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_(count($arrInstancias) . ' Record(s) Delete  successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			$this->oNegRubrica_instancia->__set('idrubrica_instancia', $_REQUEST['idrubrica_instancia']);
			$res = $this->oNegRubrica_instancia->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRubrica_instancia->setCampo($_REQUEST['idrubrica_instancia'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
