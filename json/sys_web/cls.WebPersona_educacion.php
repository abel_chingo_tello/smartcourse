<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_educacion', RUTA_BASE);
class WebPersona_educacion extends JrWeb
{
	private $oNegPersona_educacion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_educacion = new NegPersona_educacion;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_educacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["institucion"])&&@$_REQUEST["institucion"]!='')$filtros["institucion"]=$_REQUEST["institucion"];
			if(isset($_REQUEST["tipodeestudio"])&&@$_REQUEST["tipodeestudio"]!='')$filtros["tipodeestudio"]=$_REQUEST["tipodeestudio"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["areaestudio"])&&@$_REQUEST["areaestudio"]!='')$filtros["areaestudio"]=$_REQUEST["areaestudio"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["fechade"])&&@$_REQUEST["fechade"]!='')$filtros["fechade"]=$_REQUEST["fechade"];
			if(isset($_REQUEST["fechahasta"])&&@$_REQUEST["fechahasta"]!='')$filtros["fechahasta"]=$_REQUEST["fechahasta"];
			if(isset($_REQUEST["actualmente"])&&@$_REQUEST["actualmente"]!='')$filtros["actualmente"]=$_REQUEST["actualmente"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegPersona_educacion->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$ideducacion)) {
				$this->oNegPersona_educacion->ideducacion = $ideducacion;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegPersona_educacion->idpersona=@$idpersona;
				$this->oNegPersona_educacion->institucion=@$institucion;
				$this->oNegPersona_educacion->tipodeestudio=@$tipodeestudio;
				$this->oNegPersona_educacion->titulo=@$titulo;
				$this->oNegPersona_educacion->areaestudio=@$areaestudio;
				$this->oNegPersona_educacion->situacion=@$situacion;
				$this->oNegPersona_educacion->fechade=@$fechade;
				$this->oNegPersona_educacion->fechahasta=@$fechahasta;
				$this->oNegPersona_educacion->actualmente=@$actualmente;
				$this->oNegPersona_educacion->mostrar=@$mostrar;
				
            if($accion=='_add') {
            	$res=$this->oNegPersona_educacion->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_educacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersona_educacion->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_educacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPersona_educacion->__set('ideducacion', $_REQUEST['ideducacion']);
			$res=$this->oNegPersona_educacion->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPersona_educacion->setCampo($_REQUEST['ideducacion'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}