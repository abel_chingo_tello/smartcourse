<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_cursohabilidad', RUTA_BASE);
class WebAcad_cursohabilidad extends JrWeb
{
	private $oNegAcad_cursohabilidad;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_cursohabilidad = new NegAcad_cursohabilidad;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_cursohabilidad', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAcad_cursohabilidad->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idcursohabilidad)) {
				$this->oNegAcad_cursohabilidad->idcursohabilidad = $idcursohabilidad;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegAcad_cursohabilidad->texto=@$texto;
				$this->oNegAcad_cursohabilidad->tipo=@$tipo;
				$this->oNegAcad_cursohabilidad->idcurso=@$idcurso;
				$this->oNegAcad_cursohabilidad->idcursodetalle=@$idcursodetalle;
				$this->oNegAcad_cursohabilidad->idpadre=@$idpadre;
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_cursohabilidad->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_cursohabilidad')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_cursohabilidad->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_cursohabilidad')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_cursohabilidad->__set('idcursohabilidad', $_REQUEST['idcursohabilidad']);
			$res=$this->oNegAcad_cursohabilidad->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_cursohabilidad->setCampo($_REQUEST['idcursohabilidad'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}