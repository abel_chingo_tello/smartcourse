<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegProyecto_cursos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebProyecto_cursos extends JrWeb
{
	private $oNegProyecto_cursos;
	private $oNegPersonal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegProyecto_cursos = new NegProyecto_cursos;
		$this->oNegPersonal = new NegPersonal;
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegProyecto_cursos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            $usuarioAct = NegSesion::getUsuario();
            $idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
            $hayProyectocurso=$this->oNegProyecto_cursos->buscar(array('idcurso'=>$idcurso,'idproyecto'=>$idproyecto));
            if(!empty($hayProyectocurso[0])){
				$this->oNegProyecto_cursos->__set('idproycurso', $hayProyectocurso[0]["idproycurso"]);
				$res=$this->oNegProyecto_cursos->eliminar();
				echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('saved successfully'),'newid'=>$res,'asignado'=>0)); 
			}else{
				$this->oNegProyecto_cursos->idcurso=@$idcurso;
				$this->oNegProyecto_cursos->idproyecto=@$idproyecto;
				$res=$this->oNegProyecto_cursos->agregar();
				echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('saved successfully'),'newid'=>$res,'asignado'=>1));

			}
			if(!empty($sks)){
				$estados_proyecto_curso = array(
					"idexterno"=>$idcurso
					,"tipo"=>$tipo
					,'idrol'=>$usuarioAct["idrol"]
					,'idproyecto'=>$usuarioAct["idproyecto"]
					,'idempresa'=>$usuarioAct["idempresa"]
					,'idproducto'=>1
				);

				if($usuarioAct["idpersona"] == $usuarioAct["idrepresentante"]){
					$estados_proyecto_curso['tipodoc'] =$usuarioAct["tipodoc"];
					$estados_proyecto_curso['dni'] =$usuarioAct["dni"];
					$estados_proyecto_curso['usuario'] =$usuarioAct["usuario"];
				} else {
					$persona = $this->oNegPersonal->buscar(array("idpersona" => $usuarioAct["idrepresentante"]))[0];
					$estados_proyecto_curso['tipodoc'] =$persona["tipodoc"];
					$estados_proyecto_curso['dni'] =$persona["dni"];
					$estados_proyecto_curso['usuario'] =$persona["usuario"];
				}

				$POSTDATA = array("estados" => $estados_proyecto_curso);
				$TARGET = _URL_SKS_."/json/cursos/acceso";
				$r2 = NegTools::requestHTTP($POSTDATA, $TARGET);
				if(empty($r2) && $r2['code'] != 200){
					throw new Exception("Ocurrio algo en la peticion");
				}
			}

            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	/**
	 * Funcion generica que inserta 1 o mas registro en la tabla acceso, esta funcion se utiliza en varias funciones para insertar en la tabla, por favor tener precaucion al editar la funcion
	 * @param Array POST Registros a guardar en la tabla
	 * @return Array JSON Respuesta de la petición con el último id guardado en la tabla
	 */
	public function insert(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			
			$data = [];

			if(empty($_POST)){ throw new Exception("POST esta vacio"); }
			
			if(!empty($_POST['JSONList'])){
				$arr = json_decode($_POST['JSONList'],true);
				
				if(empty($arr)){ throw new Exception('JSONList esta vacio'); }

				foreach($arr as $row){
					if(empty($row['idproyecto'])){ throw new Exception("idproyecto vacio"); }
					if(empty($row['idcurso'])){ throw new Exception("idcurso vacio"); }	
					$data[] = $row;
				}
			}else{
				if(empty($_POST['idproyecto'])){ throw new Exception("idproyecto vacio"); }
				if(empty($_POST['idcurso'])){ throw new Exception("idcurso vacio"); }
				$prepare = [ 'idproyecto' => $_POST['idproyecto'], 'idcurso'=> $_POST['idcurso'] ];
				if(!empty($_POST['configuracion_nota'])){ $prepare['configuracion_nota'] = $_POST['configuracion_nota']; }
				if(!empty($_POST['orden'])){ $prepare['orden'] = $_POST['orden']; }
				$data[] = $prepare;
			}
			
			$dato = $this->oNegProyecto_cursos->insert($data);

			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('saved successfully'),'data'=>$dato));
            exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegProyecto_cursos->__set('idproycurso', $_REQUEST['idproycurso']);
			$res=$this->oNegProyecto_cursos->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegProyecto_cursos->setCampo($_REQUEST['idproycurso'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}
	public function setCampoxCurso(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			 $usuarioAct = NegSesion::getUsuario();
            $idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
			$hay=$this->oNegProyecto_cursos->buscar(array('idcurso'=>$_REQUEST['idcurso'],'idproyecto'=>$idproyecto));
			if(!empty($hay[0])){
				$this->oNegProyecto_cursos->setCampo($hay[0]['idproycurso'],$_REQUEST['campo'],$_REQUEST['valor']);
				echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			}else echo json_encode(array('code'=>'Error','msj'=>'Data incomplete'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

}