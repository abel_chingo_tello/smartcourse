<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_horariogrupodetalle', RUTA_BASE);
class WebAcad_horariogrupodetalle extends JrWeb
{
	private $oNegAcad_horariogrupodetalle;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_horariogrupodetalle = new NegAcad_horariogrupodetalle;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_horariogrupodetalle', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["idhorariopadre"])&&@$_REQUEST["idhorariopadre"]!='')$filtros["idhorariopadre"]=$_REQUEST["idhorariopadre"];
			if(isset($_REQUEST["diasemana"])&&@$_REQUEST["diasemana"]!='')$filtros["diasemana"]=$_REQUEST["diasemana"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAcad_horariogrupodetalle->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idhorario)) {
				$this->oNegAcad_horariogrupodetalle->idhorario = $idhorario;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegAcad_horariogrupodetalle->idgrupoauladetalle=@$idgrupoauladetalle;
				$this->oNegAcad_horariogrupodetalle->fecha_inicio=@$fecha_inicio;
				$this->oNegAcad_horariogrupodetalle->fecha_final=@$fecha_final;
				$this->oNegAcad_horariogrupodetalle->descripcion=@$descripcion;
				$this->oNegAcad_horariogrupodetalle->color=@$color;
				$this->oNegAcad_horariogrupodetalle->idhorariopadre=@$idhorariopadre;
				$this->oNegAcad_horariogrupodetalle->diasemana=@$diasemana;
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_horariogrupodetalle->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_horariogrupodetalle')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_horariogrupodetalle->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_horariogrupodetalle')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_horariogrupodetalle->__set('idhorario', $_REQUEST['idhorario']);
			$res=$this->oNegAcad_horariogrupodetalle->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_horariogrupodetalle->setCampo($_REQUEST['idhorario'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}