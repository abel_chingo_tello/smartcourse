<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno_logro', RUTA_BASE);
class WebAlumno_logro extends JrWeb
{
	private $oNegAlumno_logro;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAlumno_logro = new NegAlumno_logro;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Alumno_logro', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["id_alumno"])&&@$_REQUEST["id_alumno"]!='')$filtros["id_alumno"]=$_REQUEST["id_alumno"];
			if(isset($_REQUEST["id_logro"])&&@$_REQUEST["id_logro"]!='')$filtros["id_logro"]=$_REQUEST["id_logro"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["tiporecurso"])&&@$_REQUEST["tiporecurso"]!='')$filtros["tiporecurso"]=$_REQUEST["tiporecurso"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["bandera"])&&@$_REQUEST["bandera"]!='')$filtros["bandera"]=$_REQUEST["bandera"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAlumno_logro->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id_alumno_logro)) {
				$this->oNegAlumno_logro->id_alumno_logro = $id_alumno_logro;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegAlumno_logro->id_alumno=@$id_alumno;
				$this->oNegAlumno_logro->id_logro=@$id_logro;
				$this->oNegAlumno_logro->idrecurso=@$idrecurso;
				$this->oNegAlumno_logro->tiporecurso=@$tiporecurso;
				$this->oNegAlumno_logro->fecha=@$fecha;
				$this->oNegAlumno_logro->bandera=@$bandera;
				
            if($accion=='_add') {
            	$res=$this->oNegAlumno_logro->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Alumno_logro')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAlumno_logro->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Alumno_logro')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAlumno_logro->__set('id_alumno_logro', $_REQUEST['id_alumno_logro']);
			$res=$this->oNegAlumno_logro->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAlumno_logro->setCampo($_REQUEST['id_alumno_logro'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}