<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE);
class WebActividad_detalle extends JrWeb
{
	private $oNegActividad_detalle;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegActividad_detalle = new NegActividad_detalle;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Actividad_detalle', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idactividad"])&&@$_REQUEST["idactividad"]!='')$filtros["idactividad"]=$_REQUEST["idactividad"];
			if(isset($_REQUEST["pregunta"])&&@$_REQUEST["pregunta"]!='')$filtros["pregunta"]=$_REQUEST["pregunta"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["url"])&&@$_REQUEST["url"]!='')$filtros["url"]=$_REQUEST["url"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["tipo_desarrollo"])&&@$_REQUEST["tipo_desarrollo"]!='')$filtros["tipo_desarrollo"]=$_REQUEST["tipo_desarrollo"];
			if(isset($_REQUEST["tipo_actividad"])&&@$_REQUEST["tipo_actividad"]!='')$filtros["tipo_actividad"]=$_REQUEST["tipo_actividad"];
			if(isset($_REQUEST["idhabilidad"])&&@$_REQUEST["idhabilidad"]!='')$filtros["idhabilidad"]=$_REQUEST["idhabilidad"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["texto_edit"])&&@$_REQUEST["texto_edit"]!='')$filtros["texto_edit"]=$_REQUEST["texto_edit"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegActividad_detalle->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$iddetalle)) {
				$this->oNegActividad_detalle->iddetalle = $iddetalle;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegActividad_detalle->idactividad=@$idactividad;
				$this->oNegActividad_detalle->pregunta=@$pregunta;
				$this->oNegActividad_detalle->orden=@$orden;
				$this->oNegActividad_detalle->url=@$url;
				$this->oNegActividad_detalle->tipo=@$tipo;
				$this->oNegActividad_detalle->tipo_desarrollo=@$tipo_desarrollo;
				$this->oNegActividad_detalle->tipo_actividad=@$tipo_actividad;
				$this->oNegActividad_detalle->idhabilidad=@$idhabilidad;
				$this->oNegActividad_detalle->texto=@$texto;
				$this->oNegActividad_detalle->texto_edit=@$texto_edit;
				$this->oNegActividad_detalle->titulo=@$titulo;
				$this->oNegActividad_detalle->descripcion=@$descripcion;
				$this->oNegActividad_detalle->idpersonal=@$idpersonal;
				
            if($accion=='_add') {
            	$res=$this->oNegActividad_detalle->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Actividad_detalle')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegActividad_detalle->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Actividad_detalle')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegActividad_detalle->__set('iddetalle', $_REQUEST['iddetalle']);
			$res=$this->oNegActividad_detalle->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegActividad_detalle->setCampo($_REQUEST['iddetalle'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}