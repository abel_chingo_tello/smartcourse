
<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRoles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto_cursos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegApi', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);

class WebApi extends JrWeb
{
    protected $idproyecto;

    protected $oNegAcad_curso;
    protected $oNegGeneral;
    protected $oNegRoles;
    protected $oNegPersonal;
    protected $oNegPersona_rol;
    protected $oNegBolsa_empresas;
    protected $oNegProyecto;
    protected $oNegAcad_matricula;
    protected $oNegMin_dre;
    protected $oNegUgel;
    protected $oNegProyecto_cursos;
    protected $oNegApi;
    protected $oNegAcad_cursodetalle;
    protected $oNegSesion;
    protected $oNegHistorialSesion;
    protected $oNegAcad_grupoaula;
    protected $oNegAcad_grupoauladetalle;

    public function __construct()
    {
        /**Valores por defecto */
        $this->idproyecto = 0;

        $this->oNegAcad_curso = new NegAcad_curso;
        $this->oNegGeneral = new NegGeneral;
        $this->oNegRoles = new NegRoles;
        $this->oNegPersonal = new NegPersonal;
        $this->oNegPersona_rol = new NegPersona_rol;
        $this->oNegBolsa_empresas = new NegBolsa_empresas;
        $this->oNegProyecto = new NegProyecto;
        $this->oNegAcad_matricula = new NegAcad_matricula;
        $this->oNegMin_dre = new NegMin_dre;
        $this->oNegUgel = new NegUgel;
        $this->oNegProyecto_cursos = new NegProyecto_cursos;
        $this->oNegApi = new NegApi;
        $this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
        $this->oNegSesion = new NegSesion;
        $this->oNegHistorialSesion = new NegHistorial_sesion;
        $this->oNegAcad_grupoaula = new NegAcad_grupoaula;
        $this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;

        parent::__construct();
    }
    private function printJSON($state, $message, $data)
    {
        #header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
        echo json_encode(array('code' => $state, 'message' => $message, 'data' => $data));
        exit(0);
    }
    public function proyecto(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			$filtros=array();
			if(isset($_POST["idproyecto"])&&@$_POST["idproyecto"]!='')$filtros["idproyecto"]=$_POST["idproyecto"];
			if(isset($_POST["idempresa"])&&@$_POST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
					
            $rs = $this->oNegProyecto->buscar($filtros);
            
            $this->printJSON(200, CARPETA_RAIZ, $rs);
        }catch(Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function empresas(){
		try{
			$filtros=array();
			if(isset($_POST["idempresa"])&&@$_POST["idempresa"]!='')$filtros["idempresa"]=$_POST["idempresa"];
			if(isset($_POST["nombre"])&&@$_POST["nombre"]!='')$filtros["nombre"]=$_POST["nombre"];
			if(isset($_POST["rason_social"])&&@$_POST["rason_social"]!='')$filtros["rason_social"]=$_POST["rason_social"];
			if(isset($_POST["ruc"])&&@$_POST["ruc"]!='')$filtros["ruc"]=$_POST["ruc"];
			if(isset($_POST["logo"])&&@$_POST["logo"]!='')$filtros["logo"]=$_POST["logo"];
			if(isset($_POST["direccion"])&&@$_POST["direccion"]!='')$filtros["direccion"]=$_POST["direccion"];
			if(isset($_POST["telefono"])&&@$_POST["telefono"]!='')$filtros["telefono"]=$_POST["telefono"];
			if(isset($_POST["representante"])&&@$_POST["representante"]!='')$filtros["representante"]=$_POST["representante"];
			if(isset($_POST["usuario"])&&@$_POST["usuario"]!='')$filtros["usuario"]=$_POST["usuario"];
			if(isset($_POST["clave"])&&@$_POST["clave"]!='')$filtros["clave"]=$_POST["clave"];
			if(isset($_POST["correo"])&&@$_POST["correo"]!='')$filtros["correo"]=$_POST["correo"];
			if(isset($_POST["estado"])&&@$_POST["estado"]!='')$filtros["estado"]=$_POST["estado"];
			if(isset($_POST["tipo_matricula"])&&@$_POST["tipo_matricula"]!='')$filtros["tipo_matricula"]=$_POST["tipo_matricula"];
			if(isset($_POST["idpersona"])&&@$_POST["idpersona"]!='')$filtros["idpersona"]=$_POST["idpersona"];
						
			if(isset($_POST["texto"])&&@$_POST["texto"]!='')$filtros["texto"]=$_POST["texto"];			
			$this->datos=$this->oNegBolsa_empresas->buscar($filtros);
            // echo json_encode(array('code'=>200,'data'=>$this->datos));
            $this->printJSON(200, "", $this->datos);
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
    }
    public function grupos_estudio()
	{
		$this->documento->plantilla = 'blanco';
		try {
			$filtros = array();          
			// if (isset($_POST["nombre"]) && @$_POST["nombre"] != '') $filtros["nombre"] = $_POST["nombre"];
			// if (isset($_POST["tipo"]) && @$_POST["tipo"] != '') $filtros["tipo"] = $_POST["tipo"];
			// if (isset($_POST["comentario"]) && @$_POST["comentario"] != '') $filtros["comentario"] = $_POST["comentario"];
			// if (isset($_POST["nvacantes"]) && @$_POST["nvacantes"] != '') $filtros["nvacantes"] = $_POST["nvacantes"];
			// if (isset($_POST["estado"]) && @$_POST["estado"] != '') $filtros["estado"] = $_POST["estado"];
			// if (isset($_POST["idgrado"]) && @$_POST["idgrado"] != '') $filtros["idgrado"] = $_POST["idgrado"];
			// if (isset($_POST["idsesion"]) && @$_POST["idsesion"] != '') $filtros["idsesion"] = $_POST["idsesion"];
			// if (isset($_POST["idcurso"]) && @$_POST["idcurso"] != '') $filtros["idcurso"] = $_POST["idcurso"];
			if (isset($_POST["idlocal"]) && @$_POST["idlocal"] != '') $filtros["idlocal"] = $_POST["idlocal"];
			// if (isset($_POST["idlocal_grupo"]) && @$_POST["idlocal_grupo"] != '') $filtros["idlocal_grupo"] = $_POST["idlocal_grupo"];
			if (isset($_POST["idproducto"]) && @$_POST["idproducto"] != '') $filtros["idproducto"] = $_POST["idproducto"];
			// if (isset($_POST["idcategoria"]) && @$_POST["idcategoria"] != '') $filtros["idcategoria"] = $_POST["idcategoria"];
			// if(isset($_POST["fechaactiva"])&&@$_POST["fechaactiva"]!='')$filtros["fechaactiva"]=$_POST["fechaactiva"];
			if (isset($_POST["fechaactiva"]) && @$_POST["fechaactiva"] != '') $filtros["fechaactiva"] = $_POST["fechaactiva"];
			if (isset($_POST["idproyecto"]) && @$_POST["idproyecto"] != '') $filtros["idproyecto"] = $_POST["idproyecto"];

			// if (isset($_POST["texto"]) && @$_POST["texto"] != '') $filtros["texto"] = $_POST["texto"];
            $datos = $this->oNegAcad_grupoaula->buscar($filtros);
            /***********************
            esto es solo si se envia un idgrupoaula
            para retornar solo ese grupo aula y sus cursos.
            */
            $filtros2=array();
			if(isset($_POST["idgrupoaula"]) && @$_POST["idgrupoaula"] != '') $filtros2["idgrupoaula2"] = $_POST["idgrupoaula"];

            $data = array();
            foreach ($datos as $key => $value) {
                $filtros = array();
                $filtros["idgrupoaula"] = $value["idgrupoaula"];
                if(isset($_POST["idcurso"])){
                    if(!empty($_POST["idcurso"])){
                        $filtros["idcurso"] = json_decode($_POST["idcurso"], true);
                    }
                } 
                if(!empty($filtros2["idgrupoaula2"])){             
                    if($filtros2["idgrupoaula2"]==$value["idgrupoaula"]){
                        $detalle = $this->oNegAcad_grupoauladetalle->buscar($filtros);               
                        if(!empty($detalle)){
                            $value["detalle"] = $detalle;
                            $data[] = $value;
                        }
                    }
                }else{
                    $detalle = $this->oNegAcad_grupoauladetalle->buscar($filtros);               
                        if(!empty($detalle)){
                            $value["detalle"] = $detalle;
                            $data[] = $value;
                        }
                }
                // $datos[$key] = $value;
            }
            
            // echo json_encode(array('code' => 200, 'data' => $this->datos));
            $this->printJSON(200, "", $data);
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

      
    public function login()
	{
        // JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
        // $this->oNegSesion = new NegSesion;
		try {
            global $aplicacion;
            // var_dump($_REQUEST);exit();
            // $token = $_REQUEST["token"];
            // $token = explode("&", base64_decode($token, true));
            // $idpersona = $
            // var_dump($token);exit();
            $this->salir();
            if(isset($_REQUEST["token"])){
                if (!empty(@$_REQUEST["token"])) {
                    $token = $_REQUEST["token"];
                    $token = explode("&", base64_decode($token, true));
                    $idpersona = @$token[0];
                    $idproyecto = @$token[1];
                    $idempresa = @$token[2];
                    $fecha = @$token[3];
                    $now = new DateTime("now");
                    $fecha = new DateTime($fecha);
                    if($now < $fecha){
                        $res = $this->oNegSesion->ingresar(array(
                            'idpersona' => $idpersona, 
                            'idempresa' => $idempresa, 
                            'idproyecto' => $idproyecto
                        ));
                        // var_dump($res);
                        // exit();
                        if ($res["code"] == 200 && $res["islogin"] == true) {
                            // $this->iniciarHistorialSesion('P'); // incio sesion en plataforma
                            header('Location: ' . URL_BASE);
                            exit();
                        } else {
                            $this->printJSON(500, "No se inició sesión", "");
                        }
                    } else {
                        $this->printJSON(500, "Token inválido", "");
                    }
                } else {
                    $this->printJSON(500, "Token vacío", "");
                }
            }elseif(!empty($_REQUEST["tokenuser"])){
                $token = $_REQUEST["tokenuser"];
                $token = explode("&", base64_decode($token, true));
                $idproyecto = @$token[0];
                $idempresa = @$token[1];
                $usuario = @$token[2];
                $clave = @$token[3];
                $res = $this->oNegSesion->ingresar(array(
                            'usuario' => $usuario,
                            'clave' => $clave, 
                            'idempresa' => $idempresa, 
                            'idproyecto' => $idproyecto
                        ));                               
                header('Location: ' . URL_BASE);
                exit(); 
            } else {
                $this->printJSON(500, "Token requerido", "");
            }
			// $this->printJSON(500, "Request completed", "");
		} catch (Exception $e) {
			$this->printJSON(500, $e->getMessage(), "");
		}
    }

     protected function iniciarHistorialSesion($lugar = 'P')
	{
        // JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
        // $this->oNegHistorialSesion = new NegHistorial_sesion;
		$usuarioAct = NegSesion::getUsuario();

		$tipousuario = $usuarioAct['idrol'] == 3 ? 'A' : ($usuarioAct['idrol'] == 2 ? 'D' : ($usuarioAct['idrol'] == 1 ? 'M' : 'P'));
		$this->oNegHistorialSesion->tipousuario = $tipousuario;
		$this->oNegHistorialSesion->idusuario = $usuarioAct['idpersona'];
		$this->oNegHistorialSesion->lugar = $lugar;
		$this->oNegHistorialSesion->fechaentrada = date('Y-m-d H:i:s');
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$this->oNegHistorialSesion->idproyecto = $usuarioAct['idproyecto'];
		$idHistSesion = $this->oNegHistorialSesion->agregar();
		$sesion = JrSession::getInstancia();
		$sesion->set('idHistorialSesion', $idHistSesion, NegSesion::$keysesion);
		$sesion->set('fechaentrada', date('Y-m-d H:i:s'), NegSesion::$keysesion);
    }
    public function salir()
	{
		global $aplicacion;
		try {
			if (true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}
			return true;
		} catch (Exception $e) {
			$this->printJSON(400, $e->getMessage(), "");
		}
    }
    protected function terminarHistorialSesion($lugar = 'P')
	{
		$usuarioAct = NegSesion::getUsuario();
		if (!empty($usuarioAct['idHistorialSesion'])) {
			$hay = $this->oNegHistorialSesion->buscar(array('idhistorialsesion' => $usuarioAct['idHistorialSesion']));
			if (!empty($hay[0])) {
				$this->oNegHistorialSesion->setCampo($usuarioAct['idHistorialSesion'], 'fechasalida', date('Y-m-d H:i:s'));
			}
		}
	}
    private function makeDRE($dre)
    {
        try {
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    private function makeUGEL($iddre, $ugel)
    {
        try {
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    public function defecto()
    {
        try {
            global $aplicacion;
            $this->printJSON(200, "Api Work fine!!", "");
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function buscarmatriculados()
    {
        try {
            global $aplicacion;
            $filtros = array();

            if (isset($_REQUEST['idpersona']) && !empty($_REQUEST['idpersona'])) $filtros['idpersona'] = $_REQUEST['idpersona'];
            if (isset($_REQUEST['dni']) && !empty($_REQUEST['dni'])) $filtros['dni'] = $_REQUEST['dni'];
            if (isset($_REQUEST['idcurso']) && !empty($_REQUEST['idcurso'])) $filtros['idcurso'] = $_REQUEST['idcurso'];

            $rs = $this->oNegApi->buscarmatriculados($filtros);

            $this->printJSON(200, "Request completed", $rs);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function cursos()
    {
        try {
            global $aplicacion;
            $callbackidproyecto = isset($_REQUEST['callbackidproyecto']) ? $_REQUEST['callbackidproyecto'] : null;
            $idproyecto = isset($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : null;

            $filtros["estado"] = 1;

            if (!empty($idproyecto)) {
                $filtros["idproyecto"] = $idproyecto;
            } else {
                $filtros["sql2"] = 1;
            }
            $cursosProyecto = $this->oNegAcad_curso->buscar($filtros);
            if (!empty($cursosProyecto) && !empty($callbackidproyecto)) {
                $cursosProyecto[0]['callbackidproyecto'] = $callbackidproyecto;
            }

            $this->printJSON(200, "Request completed", $cursosProyecto);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function tipodocumento()
    {
        try {
            global $aplicacion;
            $result = $this->oNegGeneral->buscar(array('mostrar' => 1, "tipo_tabla" => 'tipodocidentidad'));
            $this->printJSON(200, "Request completed", $result);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function roles()
    {
        try {
            global $aplicacion;
            $result = $this->oNegRoles->buscar();
            $this->printJSON(200, "Request completed", $result);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function configurarcursos()
    {
        try {
            $cursos = (isset($_POST['cursos'])) ? $_POST['cursos'] : null;
            $proyectos = (isset($_POST['proyectos'])) ? $_POST['proyectos'] : null;

            if (empty($proyectos) || empty($cursos)) {
                throw new Exception("Se requiere campos obligatorios");
            }
            foreach ($proyectos as $kproject => $vproject) {
                foreach ($cursos as $value) {
                    $this->oNegProyecto_cursos->idcurso = $value;
                    $this->oNegProyecto_cursos->idproyecto = $vproject;
                    $rproycurso = $this->oNegProyecto_cursos->agregar();
                }
            }


            $this->printJSON(200, "Request completed", true);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function crearproyecto()
    {
        try {
            global $aplicacion;
            $jsonlogin = (isset($_POST['jsonlogin'])) ? $_POST['jsonlogin'] : null;
            $cursos = (isset($_POST['cursos'])) ? $_POST['cursos'] : null;
            $idioma = (isset($_POST['idioma'])) ? $_POST['idioma'] : 'ES';
            $fecha = (isset($_POST['fecha'])) ? $_POST['fecha'] : date('Y-m-d H:i:s');
            $nombreProyecto = (isset($_POST['nombreProyecto'])) ? $_POST['nombreProyecto'] : 'Empty';
            $typeEmpresa = (isset($_POST['typeEmpresa'])) ? $_POST['typeEmpresa'] : 0;
            $logoProyecto = (isset($_POST['logoProyecto'])) ? $_POST['logoProyecto'] : '';
            $permalink = (isset($_POST['permalink'])) ? $_POST['permalink'] : '';
            $idBolsa_empresa = (isset($_POST['idBolsa_empresa'])) ? $_POST['idBolsa_empresa'] : null;

            if (empty($idBolsa_empresa)) {
                throw new Exception("Id bolsa_empresa no es definido");
            }

            //proyecto
            if (empty($jsonlogin)) {
                $jsonlogin = '{"tipologin":1,"tipofondo":"video","colorfondo":"rgba(20, 92, 186, 0.93)","imagenfondo":"","videofondo":"/static/media/empresa/201808151031467.mp4","logo1":"","logo2":"","colorfondoadmin":"rgb(24, 14, 230)","menus":{"0":{"id":"8","nombre":"smartcourse","link":"<DOMINIO>.com/proyecto/cursos/","icono":"fa-tachometer"},"1":{"id":"5","nombre":"smartraking","link":"<DOMINIO>.com/docente/panelcontrol/","icono":"fa-tachometer"}}}';
            }

            $this->oNegProyecto->idempresa  = $idBolsa_empresa;
            $this->oNegProyecto->jsonlogin  = $jsonlogin;
            $this->oNegProyecto->fecha      = $fecha;
            $this->oNegProyecto->idioma     = $idioma;
            $this->oNegProyecto->nombre     = $nombreProyecto;
            $this->oNegProyecto->tipo_empresa = $typeEmpresa;
            $this->oNegProyecto->logo       = $logoProyecto;
            $this->oNegProyecto->nombreurl  = $permalink;

            $idProyecto = $this->oNegProyecto->agregar();

            //retornar los id y datos correspondientes.
            if (empty($idProyecto)) {
                throw new Exception("Error al insertar proyecto", 1);
            }
            //amarrar cursos
            if (!empty($cursos) && is_array($cursos)) {
                foreach ($cursos as $value) {
                    $this->oNegProyecto_cursos->idcurso = $value;
                    $this->oNegProyecto_cursos->idproyecto = $idProyecto;
                    $rproycurso = $this->oNegProyecto_cursos->agregar();
                }
            }
            $datos = array('idproyecto' => $idProyecto);

            $this->printJSON(200, "Request completed", $datos);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function crearproyectoempresa()
    {
        try {
            global $aplicacion;

            //obtener valores
            $infoEmpresa = (isset($_POST['empresa'])) ? $_POST['empresa'] : null;
            $idioma = (isset($_POST['idioma'])) ? $_POST['idioma'] : 'ES';
            $fecha = (isset($_POST['fecha'])) ? $_POST['fecha'] : date('Y-m-d H:i:s');
            $jsonlogin = (isset($_POST['jsonlogin'])) ? $_POST['jsonlogin'] : null;
            $cursos = (isset($_POST['cursos'])) ? $_POST['cursos'] : null;

            //verificar valores
            if (empty($infoEmpresa)) {
                throw new Exception("Algunos campos no se definen", 1);
            }
            //preparar para insertar en las diferentes tablas
            $datos = array();

            //bolsa_empresas
            $this->oNegBolsa_empresas->nombre       = $infoEmpresa['nombre'];
            $this->oNegBolsa_empresas->rason_social = $infoEmpresa['rason_social'];
            $this->oNegBolsa_empresas->ruc          = $infoEmpresa['ruc'];
            $this->oNegBolsa_empresas->logo         = $infoEmpresa['logo'];
            $this->oNegBolsa_empresas->direccion    = $infoEmpresa['direccion'];
            $this->oNegBolsa_empresas->telefono     = $infoEmpresa['telefono'];
            $this->oNegBolsa_empresas->representante = $infoEmpresa['representante'];
            $this->oNegBolsa_empresas->usuario      = $infoEmpresa['usuario'];
            $this->oNegBolsa_empresas->clave        = $infoEmpresa['clave'];
            $this->oNegBolsa_empresas->correo       = $infoEmpresa['correo'];
            $this->oNegBolsa_empresas->estado       = $infoEmpresa['estado'];
            $this->oNegBolsa_empresas->tipo_empresa = $infoEmpresa['tipo_empresa'];
            $this->oNegBolsa_empresas->nombreurl    = '';

            $idBolsa_empresa = $this->oNegBolsa_empresas->agregar();
            if (empty($idBolsa_empresa)) {
                throw new Exception("Error al insertar empresa", 1);
            }
            //proyecto
            if (empty($jsonlogin)) {
                $jsonlogin = '{"tipologin":1,"tipofondo":"video","colorfondo":"rgba(20, 92, 186, 0.93)","imagenfondo":"","videofondo":"/static/media/empresa/201808151031467.mp4","logo1":"","logo2":"","colorfondoadmin":"rgb(24, 14, 230)","menus":{"0":{"id":"8","nombre":"smartcourse","link":"<DOMINIO>.com/proyecto/cursos/","icono":"fa-tachometer"},"1":{"id":"5","nombre":"smartraking","link":"<DOMINIO>.com/docente/panelcontrol/","icono":"fa-tachometer"}}}';
            }

            $this->oNegProyecto->idempresa  = $idBolsa_empresa;
            $this->oNegProyecto->jsonlogin  = $jsonlogin;
            $this->oNegProyecto->fecha      = $fecha;
            $this->oNegProyecto->idioma     = $idioma;

            $idProyecto = $this->oNegProyecto->agregar();

            //retornar los id y datos correspondientes.
            if (empty($idProyecto)) {
                throw new Exception("Error al insertar proyecto", 1);
            }
            //amarrar cursos
            if (!empty($cursos) && is_array($cursos)) {
                foreach ($cursos as $value) {
                    $this->oNegProyecto_cursos->idcurso = $value;
                    $this->oNegProyecto_cursos->idproyecto = $idProyecto;
                    $rproycurso = $this->oNegProyecto_cursos->agregar();
                }
            }
            $datos = array('idempresa' => $idBolsa_empresa, 'idproyecto' => $idProyecto);

            $this->printJSON(200, "Request completed", $datos);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function crearusuario()
    {
        try {
            global $aplicacion;
            //obtener valores
            $dataIdProyecto     = isset($_POST['idproyecto']) ? $_POST['idproyecto'] : null;
            $dataPersona        = isset($_POST['usuario']) ? $_POST['usuario'] : null;
            $dataPersonaRoles   = isset($_POST['roles']) ? $_POST['roles'] : null;
            $dataEmpresa        = isset($_POST['empresa']) ? $_POST['empresa'] : null;
            $dataTypeRol        = isset($_POST['rolname']) ? $_POST['rolname'] : null;
            $dataMatricula      = isset($_POST['matricula']) ? $_POST['matricula'] : null;
            $dataGrupo          = isset($_POST['grupo']) ? $_POST['grupo'] : null;
            //validar valores
            if (empty($dataPersona)) {
                throw new Exception("Se requiere la informacion del usuario");
            }
            if (empty($dataPersonaRoles) && empty($dataTypeRol)) {
                throw new Exception("Se requiere id de rol o nombre para el usuario");
            }
            if (empty($dataPersonaRoles)) {
                //chequear que tipo de rol tiene
                $rsRoles = $this->oNegRoles->buscar();
                if (empty($rsRoles)) {
                    throw new Exception("No hay roles en la plataforma");
                }
                $find = array_search(ucfirst($dataTypeRol), array_column($rsRoles, 'rol'));
                if ($find === false) {
                    throw new Exception("No se encontro el rol con el nombre");
                }
                $dataPersonaRoles[] = $rsRoles[$find]['idrol'];
            }
            //verificar usuario
            //verificar si crearlo o retornar el que se encuentra en bd
            $vrfcrearlo = $this->oNegPersonal->buscar(array('usuario' => $dataPersona['usuario'], 'dni' => $dataPersona['dni']));
            $vrfroles = array(); // verifica los roles del usuario
            if (empty($vrfcrearlo)) {
                $continue = true;
                $case = 1;
                do {
                    $rsPersona = $this->oNegPersonal->buscar(array('usuario' => $dataPersona['usuario']));
                    if (empty($rsPersona)) {
                        $continue = false;
                        break;
                    }
                    $dataPersona['usuario'] = ($case == 1) ? $dataPersona['dni'] : $dataPersona['usuario'] . substr($dataPersona['dni'], 0, rand(1, 5)) . rand(1, 999);
                    $case++;
                } while ($continue == true);
                //insertar persona
                $this->oNegPersonal->tipodoc = !empty($dataPersona['tipodoc']) ? $dataPersona['tipodoc'] : 1;
                $this->oNegPersonal->dni = $dataPersona['dni'];
                $this->oNegPersonal->ape_paterno = $dataPersona['primer_ape'];
                $this->oNegPersonal->ape_materno = $dataPersona['segundo_ape'];
                $this->oNegPersonal->nombre = $dataPersona['nombre'];
                $this->oNegPersonal->fechanac = isset($dataPersona['fechanac']) && !empty($dataPersona['fechanac']) ? $dataPersona['fechanac'] : date("Y-m-d");
                $this->oNegPersonal->sexo = $dataPersona['sexo'];
                $this->oNegPersonal->estado_civil = 'S';
                $this->oNegPersonal->ubigeo = ' ';
                $this->oNegPersonal->urbanizacion = ' ';
                $this->oNegPersonal->direccion = ' ';
                $this->oNegPersonal->telefono = isset($dataPersona['celular']) && !empty($dataPersona['celular']) ? $dataPersona['celular'] : '0';
                $this->oNegPersonal->celular = $this->oNegPersonal->telefono;
                $this->oNegPersonal->email = isset($dataPersona['email']) && !empty($dataPersona['email']) ? $dataPersona['email'] : '0';
                $this->oNegPersonal->idugel = '5';
                $this->oNegPersonal->regusuario = '1';
                $this->oNegPersonal->regfecha = date("Y-m-d");
                $this->oNegPersonal->usuario = $dataPersona['usuario'];
                $this->oNegPersonal->clave = $dataPersona['clave_se'];
                $this->oNegPersonal->token = $dataPersona['clave'];
                $this->oNegPersonal->rol = 3;
                $this->oNegPersonal->foto = '';
                $this->oNegPersonal->estado = 1;
                $this->oNegPersonal->situacion = '1';
                $this->oNegPersonal->idioma = 'ES';
                $this->oNegPersonal->tipousuario = 'n';
                $this->oNegPersonal->idlocal = 16;
                $this->oNegPersonal->esdemo = isset($value['usuario']['esdemo']) && !empty($value['usuario']['esdemo']) ? intval($value['usuario']['esdemo']) : 0;


                $id_persona = $this->oNegPersonal->agregar();
            } else {
                $id_persona = $vrfcrearlo[0]['idpersona'];
                $vrfroles = $this->oNegPersona_rol->buscar(array('idpersonal' => $id_persona, 'idproyecto' => $dataEmpresa['idproyecto'], 'idempresa' => $dataEmpresa['idempresa']));
            } //end if;
            $dataPersona['idpersona'] = $id_persona;
            $rolesInBD = array();
            if (!empty($vrfroles)) {
                foreach ($vrfroles as $vroles) {
                    $rolesInBD[] = intval($vroles['idrol']);
                }
            }
            //insertar roles
            foreach ($dataPersonaRoles as $key => $value) {
                if (!in_array(intval($value), $rolesInBD)) {
                    $this->oNegPersona_rol->idrol = $value;
                    $this->oNegPersona_rol->idpersonal = $id_persona;
                    $this->oNegPersona_rol->idproyecto = $dataEmpresa['idproyecto'];
                    $this->oNegPersona_rol->idempresa = $dataEmpresa['idempresa'];
                    $id_roldetalle = $this->oNegPersona_rol->agregar();
                }
            }


            //insertar matricula
            if (!empty($dataMatricula)) {
                foreach ($dataMatricula as $key => $value) {
                    $this->oNegAcad_matricula->idgrupoauladetalle = $value['idgrupoauladetalle'];
                    $this->oNegAcad_matricula->idalumno = $id_persona;
                    $this->oNegAcad_matricula->fecha_registro = $value['fecha_registro'];
                    $this->oNegAcad_matricula->estado = 1;
                    $this->oNegAcad_matricula->idusuario = $id_persona;
                    $this->oNegAcad_matricula->fecha_matricula = $value['fecha_matricula'];
                    $this->oNegAcad_matricula->fecha_vencimiento = $value['fecha_vencimiento'];

                    $rs = $this->oNegAcad_matricula->agregar();
                }
            }

            //insertar grupo
            if (!empty($dataGrupo)) {
                //do it
            }

            $this->printJSON(200, "Request completed", $dataPersona);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile(), "");
        }
    }
    public function _crearusuariov2()
    {
        try {
            global $aplicacion;
            //obtener valores
            $persona = isset($_POST['persona']) ? $_POST['persona'] : null;
            $personalRol = isset($_POST['persona_rol']) ? $_POST['persona_rol'] : null;

            if (empty($persona)) {
                throw new Exception("Falta persona y persona_rol");
            }

            $rs = $this->oNegApi->_crearusuariov2($persona, $personalRol);

            $this->printJSON(200, "Request completed", array());
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile(), "");
        }
    }
    public function _buscargrupos(){
        try{
            global $aplicacion;
            set_time_limit(0);
            $filtros=array();
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["comentario"])&&@$_REQUEST["comentario"]!='')$filtros["comentario"]=$_REQUEST["comentario"];
			if(isset($_REQUEST["nvacantes"])&&@$_REQUEST["nvacantes"]!='')$filtros["nvacantes"]=$_REQUEST["nvacantes"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["idproducto"])&&@$_REQUEST["idproducto"]!='')$filtros["idproducto"]=$_REQUEST["idproducto"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!=''){
                $filtros["idproyecto"]= (!is_array($_REQUEST["idproyecto"])) ? json_decode($_REQUEST["idproyecto"],true) :$_REQUEST["idproyecto"];
            }
			if(isset($_REQUEST["idlocal_grupo"])&&@$_REQUEST["idlocal_grupo"]!='')$filtros["idlocal_grupo"]=$_REQUEST["idlocal_grupo"];
			if(isset($_REQUEST["fechaactiva"])&&@$_REQUEST["fechaactiva"]!='')$filtros["fechaactiva"]=$_REQUEST["fechaactiva"];
			$this->oNegApi->setLimite(0,500000);
            $datos=$this->oNegApi->_buscargrupos($filtros);
            $this->printJSON(200, "", $datos);
			// echo json_encode(array('code'=>200,'data'=>$datos));
        }catch(Exception $e){
            $this->printJSON(400,$e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile() ,"");
        }
    }
    public function _buscargrupos2(){
        try{
            global $aplicacion;
            set_time_limit(0);
            $filtros=array();
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idproducto"])&&@$_REQUEST["idproducto"]!='')$filtros["idproducto"]=$_REQUEST["idproducto"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idlocal_grupo"])&&@$_REQUEST["idlocal_grupo"]!='')$filtros["idlocal_grupo"]=$_REQUEST["idlocal_grupo"];
			if(isset($_REQUEST["fechaactiva"])&&@$_REQUEST["fechaactiva"]!='')$filtros["fechaactiva"]=$_REQUEST["fechaactiva"];
			$this->oNegApi->setLimite(0,500000);
            $datos=$this->oNegApi->_buscargrupos2($filtros);
            $this->printJSON(200, "", $datos);
			// echo json_encode(array('code'=>200,'data'=>$datos));
        }catch(Exception $e){
            $this->printJSON(400,$e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile() ,"");
        }
    }
    public function _transferir(){
        try{
            global $aplicacion;
            set_time_limit(0);            
            $datos=$this->oNegApi->_transferir($_POST);
            $this->printJSON(200, "", $datos);
			// echo json_encode(array('code'=>200,'data'=>$datos));
        }catch(Exception $e){
            $this->printJSON(400,$e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile() ,"");
        }
    }
    public function editarusuariov1(){
        try{
            global $aplicacion;
            //obtener valores
            $persona = isset($_POST['persona']) ? $_POST['persona'] : null;

            if(empty($persona)){
                throw new Exception("Falta persona");
            }

            $rs = $this->oNegApi->editarusuariov1($persona);

            $this->printJSON(200,"Request completed",array());
        }catch(Exception $e){
            $this->printJSON(400,$e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile() ,"");
        }
    }

    public function agregarrolv1(){
        try{
            global $aplicacion;
            //obtener valores
            $persona_rol = isset($_POST['persona_rol']) ? $_POST['persona_rol'] : null;
            // $personal = isset($_POST['personal']) ? $_POST['personal'] : null;

            if(empty($persona_rol)){
                throw new Exception("Falta persona rol");
            }

            $rs = $this->oNegApi->agregarrolv1($persona_rol);

            $this->printJSON(200,"Request completed",array());
        }catch(Exception $e){
            $this->printJSON(400,$e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile() ,"");
        }
    }
    public function eliminarrolv1(){
        try{
            global $aplicacion;
            //obtener valores
            $persona_rol = isset($_POST['persona_rol']) ? $_POST['persona_rol'] : null;

            if(empty($persona_rol)){
                throw new Exception("Falta persona rol");
            }

            $rs = $this->oNegApi->eliminarrolv1($persona_rol);

            $this->printJSON(200,"Request completed",array());
        }catch(Exception $e){
            $this->printJSON(400,$e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile() ,"");
        }
    }
    public function _buscarCategorias(){
        try{
            set_time_limit(0);
            global $aplicacion;
            // var_dump($_POST);exit();
            $filtros=array();
            if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
            if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
            if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
            if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
            if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
            $respuesta = $this->oNegApi->_buscarCategorias($filtros);
            $this->printJSON(200, "Request completed", json_encode($respuesta));
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile(), "");
        }
    }
    public function _smtp(){
        try{
            set_time_limit(0);
            global $aplicacion;
            $respuesta = $this->oNegApi->_smtp($_POST);
            $this->printJSON(200, "Request completed", $respuesta);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile(), "");
        }
    }
    public function matricularapi(){
        try{
            set_time_limit(0);
            global $aplicacion;
            $datosPreparados = $this->oNegApi->prepararDatos($_POST);
            $respuesta = $this->oNegApi->matricularapi($datosPreparados);
            if (empty($respuesta)) {
                throw new Exception("Fallo algo al insertar");
            }
            $sendData = [
                "usuarios" => $respuesta["usuarios"]
            ];
            $this->printJSON(200, "Request completed", json_encode($sendData));
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile(), "");
        }
    }
    public function _matricular(){
        try{
            set_time_limit(0);
            global $aplicacion;
            // var_dump($_POST);exit();
            $respuesta = $this->oNegApi->_matricular($_POST);
            if (empty($respuesta)) {
                throw new Exception("Fallo algo al insertar");
            }
            $this->printJSON(200, "Request completed", json_encode($respuesta));
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile(), "");
        }
    }
    public function _registrarDocente()
    {
        try {
            set_time_limit(0);
            global $aplicacion;
            // var_dump($_POST);exit();
            $respuesta = $this->oNegApi->_registrarDocente($_POST);
            if (empty($respuesta)) {
                throw new Exception("Fallo algo al insertar");
            }
            $this->printJSON(200, "Request completed", json_encode($respuesta));
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile(), "");
        }
    }
    public function crearmultipleusuarios()
    {
        try {
            global $aplicacion;
            //obtener valores
            $dataUsuarios = isset($_POST['usuario']) ? $_POST['usuario'] : null;
            $dataPersonaRoles = isset($_POST['roles']) ? $_POST['roles'] : null;
            $dataEmpresa = isset($_POST['empresa']) ? $_POST['empresa'] : null;
            $_key_ = isset($_POST['key']) ? (int) $_POST['key'] : null;
            $is_prefijo = isset($_POST['is_prefijo']) ? (int) $_POST['is_prefijo'] : null;
            $prefijo = isset($_POST['prefijo']) ? $_POST['prefijo'] : null;

            //fix de maximo de 88 usuarios
            if (!empty($dataUsuarios) && !is_array($dataUsuarios) && is_string($dataUsuarios)) {
                $dataUsuarios = json_decode($dataUsuarios, true);
            }

            if (empty($dataUsuarios)) {
                throw new Exception("Se requiere la informacion de los usuario");
            }
            if (empty($dataPersonaRoles)) {
                throw new Exception("Se requiere id de rol para los usuarios");
            }

            $users = array();
            $toInserts = array();
            $dateNow = date("Y-m-d");
            foreach ($dataUsuarios as $value) {
                //preparar array para la tabla persona
                // var_dump($value['usuario']);exit();
                $preparePersona = array(
                    'idpersonasks'   => $value['usuario']['idpersonasks'], 'idpersona'   => $value['usuario']['idpersona'], 'tipodoc'   => $value['usuario']['tipodoc'], 'dni'      => $value['usuario']['dni'], 'ape_paterno' => $value['usuario']['primer_ape'], 'ape_materno' => $value['usuario']['segundo_ape'], 'nombre' => $value['usuario']['nombre'], 'fechanac' => $value['usuario']['fechanac'], 'sexo' => $value['usuario']['sexo'], 'estado_civil' => 'S', 'ubigeo' => ' ', 'urbanizacion' => ' ', 'direccion' => ' ', 'telefono' => $value['usuario']['telefono'], 'celular' => $value['usuario']['telefono'], 'email' => $value['usuario']['email'], 'idugel' => '5', 'regusuario' => '1', 'regfecha' => $dateNow, 'usuario' => $value['usuario']['usuario'], 'clave' => $value['usuario']['clave'], 'clavesinmd5' => $value['usuario']['clavesinmd5'], 'token' => $value['usuario']['clave'], 'rol' => 3, 'foto' => '', 'estado' => 1, 'situacion' => '1', 'idioma' => 'ES', 'tipousuario' => 'n', 'idlocal' => 16, 'edad_registro' => @$value['usuario']['edad_registro'], 'instruccion' => $value['usuario']['instruccion'], 'esdemo' => isset($value['usuario']['esdemo']) && !empty($value['usuario']['esdemo']) ? intval($value['usuario']['esdemo']) : 0
                );
                //preparar array para la tabla de persona rol
                $preparePersona_rol = array();
                foreach ($dataPersonaRoles as $rolesvl) {
                    $preparePersona_rol[] = array(
                        'idrol'   => $rolesvl, 'idpersonal'   => null, 'idproyecto'   => $dataEmpresa['idproyecto'], 'idempresa'   => $dataEmpresa['idempresa']
                    );
                }
                //preparar array para la tabla de matricula
                $prepareMatricula = array();
                if (isset($value['matricula']) && !empty($value['matricula'])) {
                    foreach ($value['matricula'] as $matrivalue) {
                        $prepareMatricula[] = array(
                            'idgrupoaula' => $matrivalue['idgrupoaula'], 'idgrupoauladetalle' => $matrivalue['idgrupoauladetalle'], 'idalumno' => null, 'fecha_registro' => $matrivalue['fecha_registro'], 'estado' => 1, 'idusuario' => null, 'fecha_matricula' => $matrivalue['fecha_matricula'], 'fecha_vencimiento' => $matrivalue['fecha_vencimiento'], 'pagorecurso' => (!empty($matrivalue['pagorecurso']) ? $matrivalue['pagorecurso'] : 0), 'tipomatricula' => $matrivalue['tipomatricula'], 'idcomplementario' => 0, 'idcategoria' => 0
                        );
                    } //end foreach matricula
                } //endif

                //set array
                $toInserts[] = array('personal' => $preparePersona, 'personal_rol' => $preparePersona_rol, 'matricula' => $prepareMatricula, 'idcurso' => $value['idcurso']);
                // var_dump($prepareMatricula);
            } //enforeach general

            if (empty($toInserts)) {
                throw new Exception("No hay valores para insertar");
            }
            // var_dump($toInserts);exit();
            $users = $this->oNegApi->InsertMultiple($toInserts, $_key_, $is_prefijo, $prefijo);
            if (empty($users)) {
                throw new Exception("Fallo algo al insertar");
            }
            $this->printJSON(200, "Request completed", $users);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage() . " - " . $e->getLine() . " - " . $e->getFile(), "");
        }
    }
    public function obtenerdreugel()
    {
        try {
            global $aplicacion;
            //obtener datos
            $idproyecto = isset($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : null;
            $dre = isset($_REQUEST['dre']) ? $_REQUEST['dre'] : null;
            $ugel = isset($_REQUEST['ugel']) ? $_REQUEST['ugel'] : null;
            //verificar datos
            if (empty($idproyecto)) {
                throw new Exception("Se necesita le id de proyecto");
            }
            if (empty($dre) || empty($ugel)) {
                throw new Exception("Se necesita el dre y ugel");
            }
            //verificar existencia de dre y ugel
            //de no ser asi crearlos
            $iddre = null;
            $idugel = null;
            $rs = $this->oNegMin_dre->buscarconinsert(array('descripcion' => $dre, 'idproyecto' => $idproyecto));
            $iddre = intval($rs[0]['iddre']);
            $rs2 = $this->oNegUgel->buscarconinsert(array('iddre' => $iddre, 'descripcion' => $ugel, 'idproyecto' => $idproyecto));
            $idugel = intval($rs2[0]['idugel']);

            $this->printJSON(200, "Request completed", array('iddre' => $iddre, 'idugel' => $idugel));
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function creargrupodefault()
    {
        try {
            global $aplicacion;
            //obtener datos
            $idlocal        = isset($_POST['idlocal']) ? $_POST['idlocal'] : 0;
            $nombregrupo    = isset($_POST['nombre']) ? $_POST['nombre'] : null;
            $idproyecto     = isset($_POST['idproyecto']) ? $_POST['idproyecto'] : null;
            $cursos         = isset($_POST['cursos']) ? $_POST['cursos'] : null;
            //validar
            if (empty($cursos) && empty($idproyecto) && empty($nombregrupo)) {
                throw new Exception("Error Processing Request, falta campos obligatorios");
            }
            //variables
            $data = array();
            $tablaGrupoAula = array();
            $info = array();
            //logica
            //crear grupo aula
            $tablaGrupoAula[] = array(
                'nombre' => $nombregrupo, 'tipo' => 'V','idlocal' => $idlocal, 'comentario' => 'grupo por defecto', 'nvacantes' => 1000, 'estado' => 1, 'idproyecto' => $idproyecto
            );
            $rs1 = $this->oNegApi->insertgrupoaula($tablaGrupoAula);
            if (empty($rs1)) {
                throw new Exception("grupo no creado");
            }
            //crear grupoauladetalle segun los cursos
            $info[] = array(
                'idgrupoaula' => $rs1[0],
                'iddocente' => 0,
                'idlocal' => $idlocal,
                'idambiente' => 1,
                'nombre' => $nombregrupo,
                'idcurso' => null //se inserta con un array de cursos
            );
            $rs2 = $this->oNegApi->insertgrupoauladetalle($info, $cursos);
            //salida
            $data = true;
            $this->printJSON(200, "Request completed", $data);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }

    public function obtenerexamenes()
    {
        try {
            global $aplicacion;
            //do it
            $idcurso = isset($_REQUEST['idcurso']) ? $_REQUEST['idcurso'] : null;
            $idpadre = isset($_REQUEST['idpadre']) ? $_REQUEST['idpadre'] : 0;

            if (empty($idcurso)) {
                throw new Exception("Falta definir el idcurso");
            }

            $data = $this->oNegAcad_cursodetalle->soloexamenes($idcurso, $idpadre);
            $this->printJSON(200, "Request completed", $data);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function actualizarconfigexamenes()
    {
        try {
            global $aplicacion;

            $idcurso = isset($_REQUEST['idcurso']) ? $_REQUEST['idcurso'] : null;
            $idproyecto = isset($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : null;
            $config = isset($_REQUEST['config']) ? $_REQUEST['config'] : null;

            if (empty($idcurso) || empty($idproyecto) || empty($config)) {
                throw new Exception("Falta definir campos obligatorios");
            }

            $_json = json_encode($config);

            //buscar el id de proyecto curso
            $rs = $this->oNegProyecto_cursos->buscar(array('idcurso' => $idcurso, 'idproyecto' => $idproyecto));

            if (empty($rs)) {
                throw new Exception("no se encontro ningun registro");
            }

            //actualizar el campo
            $this->oNegProyecto_cursos->setCampo($rs[0]['idproycurso'], 'configuracion_nota', $_json);

            $data = true;
            $this->printJSON(200, "Request completed", $data);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function detallecursoxgrupo()
    {
        try {
            global $aplicacion;
            //obtener datos
            $filtros = array();
            if (!empty($_REQUEST['idgrupoauladetalle'])) {
                $filtros['idgrupoauladetalle'] = $_REQUEST['idgrupoauladetalle'];
            }
            if (!empty($_REQUEST['idcurso'])) {
                $filtros['idcurso'] = $_REQUEST['idcurso'];
            }
            if (!empty($_REQUEST['idproyecto'])) {
                $filtros['idproyecto'] = $_REQUEST['idproyecto'];
            }
            //variables
            $data = $this->oNegApi->detallecursoxgrupo($filtros);

            //logica

            $this->printJSON(200, "Request completed", $data);
        } catch (Exception $e) {
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function grupoauladetalle(){
        try{
            global $aplicacion;
            $filtros = array();
            if (!empty($_REQUEST['idgrupoauladetalle'])) { $filtros['idgrupoauladetalle'] = $_REQUEST['idgrupoauladetalle']; }
            if (!empty($_REQUEST['idcurso'])) { $filtros['idcurso'] = $_REQUEST['idcurso']; }
            if (!empty($_REQUEST['idproyecto'])) { $filtros['idproyecto'] = $_REQUEST['idproyecto']; }
            if (!empty($_REQUEST['iddocente'])) { $filtros['iddocente'] = $_REQUEST['iddocente']; }
            if (!empty($_REQUEST['idgrupoaula'])) { $filtros['idgrupoaula'] = $_REQUEST['idgrupoaula']; }
            $this->oNegApi->setLimite(0,100000);
            $data = $this->oNegApi->grupoauladetalle($filtros);
            
            
            $this->printJSON(200, "Request completed", $data);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function locales(){
        try{
            global $aplicacion;
            $filtros = array();
            
            if(!empty($_REQUEST["idlocal"]))$filtros["idlocal"]=$_REQUEST["idlocal"];
            if(!empty($_REQUEST["nombre"]))$filtros["nombre"]=$_REQUEST["nombre"];
            if(!empty($_REQUEST["direccion"]))$filtros["direccion"]=$_REQUEST["direccion"];
            if(!empty($_REQUEST["id_ubigeo"]))$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
            if(!empty($_REQUEST["tipo"]))$filtros["tipo"]=$_REQUEST["tipo"];
            if(!empty($_REQUEST["vacantes"]))$filtros["vacantes"]=$_REQUEST["vacantes"];
            if(!empty($_REQUEST["idugel"]))$filtros["idugel"]=$_REQUEST["idugel"];
            if(!empty($_REQUEST["idproyecto"]))$filtros["idproyecto"]=$_REQUEST["idproyecto"];
            if(!empty($_REQUEST["oldid"]))$filtros["oldid"]=$_REQUEST["oldid"];
            if(!empty($_REQUEST["texto"]))$filtros["texto"]=$_REQUEST["texto"];
            if(!empty($_REQUEST["idproyecto"]))$filtros["idproyecto"]=$_REQUEST["idproyecto"];

            $this->oNegApi->setLimite(0,100000);
            $data = $this->oNegApi->locales($filtros);
            
            
            $this->printJSON(200, "Request completed", $data);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");
        }
    }
    public function crearrepresentante(){
        try{
            global $aplicacion;
            $data = array();
            
            //validaciones
            if(empty($_POST)){ throw new Exception("No hay ningun campo"); };
            if(empty($_POST["usuario"])){ throw new Exception("Usuario no definido"); };
            if(empty($_POST["empresa"])){ throw new Exception("Empresa no definida"); };

            //logica
            $data = $this->oNegApi->crearrepresentante($_POST);

            $this->printJSON(200, "Request completed", $data);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
    public function borrarrepresentante(){
        try{
            global $aplicacion;
            if(empty($_POST)){ throw new Exception("No hay ningun campo"); };
            if(empty($_POST["usuario"])){ throw new Exception("Usuario no definido"); };
            //preparar la data
            $representantes = array();
            foreach ($_POST["usuario"] as $key => $value) {
                $prepare = array();
                if(!empty($value['buscarusuario'])){
                    if(empty($value['tipodoc']) || empty($value['usuario'])){
                        continue;
                    }
                    $rs = $this->oNegApi->_buscarPersonal(array('tipodoc'=>$value['tipodoc'],'usuario'=> $value['usuario']));
                    if(empty($rs)){
                        continue;
                    }
                    $value = $rs[0];
                }
                if(empty($value['idpersona'])){
                    continue;
                }
                $prepare = $value;
                //buscar persona_rol
                $prepare['persona_rol'] = $this->oNegApi->_buscarRol(array('idpersonal'=>$value['idpersona']));
                $representantes[] = $prepare;
            }//endforeach
            $data = $this->oNegApi->borrarrepresentante($representantes);
            $this->printJSON(200, "Request completed", $data);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
    public function representantecambiaracceso(){
        try{
            if(empty($_REQUEST)){ throw new Exception("Request vacio"); }
            if( !isset($_REQUEST['estado']) ){ throw new Exception("No se define el estado del acceso"); }
            #buscar representante
            if(empty($_REQUEST['idpersona'])){
                #buscar por metodos
                $findcontinue = false;
                $filtros = ['sqlsolopersona'=>1];
                if(!empty($_REQUEST['dni'] && !empty($_REQUEST['tipodoc']))){
                    $filtros['dni'] = $_REQUEST['dni'];
                    $filtros['tipodoc'] = $_REQUEST['tipodoc'];
                    $findcontinue = true;
                }else if(!empty($_REQUEST['usuario'])){
                    $filtros['usuario'] = $_REQUEST['usuario'];
                    $findcontinue = true;
                }else{
                    throw new Exception("Id persona no encontrada");
                }
                if($findcontinue == false){ throw new Exception("No se puede lograr hacer la busqueda"); }
                $rs = $this->oNegPersonal->buscar($filtros);
                if(empty($rs)){ throw new Exception("personal no encontrado"); }
                $_REQUEST['idpersona'] = end($rs)['idpersona'];
            }
            $pk = $_REQUEST['idpersona'];

            $this->oNegPersonal->setCampo($pk, 'estado', $_REQUEST['estado']);
            $this->printJSON(200, "Request completed", []);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }

    /****************************************************************************************************************************
    Metodo que devuelve los idmatriculas de un grupo aula y un array  con datos  de los cursos  a los cuales no esta matriculado
    en el mismo grupo el alumno */


    public function marticulasxgrupo($idgrupoaula=null,$idpersona=null){// devolver matriculas
        $this->documento->plantilla = 'blanco';
        try {
            global $aplicacion;
            $idgrupoaula=!empty($_REQUEST["idgrupoaula"])?$_REQUEST["idgrupoaula"]:$idgrupoaula;
            $idpersona=!empty($_REQUEST["idpersona"])?$_REQUEST["idpersona"]:$idpersona;
            if(empty($idgrupoaula)){
                echo json_encode(array('code'=>'error','msj'=>'Grupo aula no definido'));
                exit(0);    
            }
            if(empty($idpersona) && NegSesion::existeSesion()){ // toma el idpersona logueado
                $usuario=NegSesion::getUsuario();
                $idpersona=$usuario["idpersona"];
            }

            if(empty($idpersona)){
                echo json_encode(array('code'=>'error','msj'=>'idpersona no esta definido'));
                exit(0);    
            }
            $informacion=$this->oNegAcad_matricula->marticulasxgrupo($idgrupoaula,$idpersona);
           // var_dump($informacion);
            echo json_encode(array('code'=>200,'datos'=>$informacion));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
    }
}
