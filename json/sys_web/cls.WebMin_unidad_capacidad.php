<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegMin_unidad_capacidad', RUTA_BASE);
class WebMin_unidad_capacidad extends JrWeb
{
	private $oNegMin_unidad_capacidad;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegMin_unidad_capacidad = new NegMin_unidad_capacidad;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Min_unidad_capacidad', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["unidad"])&&@$_REQUEST["unidad"]!='')$filtros["unidad"]=$_REQUEST["unidad"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idcapacidad"])&&@$_REQUEST["idcapacidad"]!='')$filtros["idcapacidad"]=$_REQUEST["idcapacidad"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegMin_unidad_capacidad->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id)) {
				$this->oNegMin_unidad_capacidad->id = $id;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegMin_unidad_capacidad->idcurso=@$idcurso;
				$this->oNegMin_unidad_capacidad->unidad=@$unidad;
				$this->oNegMin_unidad_capacidad->orden=@$orden;
				$this->oNegMin_unidad_capacidad->idcapacidad=@$idcapacidad;
				$this->oNegMin_unidad_capacidad->idcursodetalle=@$idcursodetalle;
				
            if($accion=='_add') {
            	$res=$this->oNegMin_unidad_capacidad->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_unidad_capacidad')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegMin_unidad_capacidad->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_unidad_capacidad')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegMin_unidad_capacidad->__set('id', $_REQUEST['id']);
			$res=$this->oNegMin_unidad_capacidad->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegMin_unidad_capacidad->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}