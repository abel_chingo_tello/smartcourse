<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-12-2020 
 * @copyright	Copyright (C) 02-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebDescargascontroladas extends JrWeb
{
	private $oNegRecursos_colaborativos;
		
	public function __construct()
	{
		parent::__construct();		
		//$this->oNegRecursos_colaborativos = new NegRecursos_colaborativos;
		
	}

	public function defecto(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Recursos_colaborativos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idrecurso"])) $filtros["idrecurso"]=$_REQUEST["idrecurso"];

			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"]; 
  			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"]; 
  			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"]; 
  			if(isset($_REQUEST["file"])&&@$_REQUEST["file"]!='')$filtros["file"]=$_REQUEST["file"]; 
  			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"]; 
  			if(isset($_REQUEST["fecha_creacion"])&&@$_REQUEST["fecha_creacion"]!='')$filtros["fecha_creacion"]=$_REQUEST["fecha_creacion"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;

			if(!empty($filtros["idproyecto"])){
  				$filtros["idproyecto"]=@$usuarioAct["idproyecto"];
  			}
					
			$this->datos=$this->oNegRecursos_colaborativos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_REQUEST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_REQUEST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $datalink='descargascontroladas';
            $nombrefile='file'.date('YmdHis').'.php';
            if(!empty($_REQUEST["link"])){
            	$link=$_REQUEST["link"];                 
            	$link=str_ireplace('/',SD,$link);            	
            	$linkipost=strripos($link,SD);
            	$datalink=substr($link, 0,$linkipost);
            	$nombrefile=substr($link,$linkipost+1);
            	//var_dump($datalink,$nombrefile);
            }
            $link=RUTA_BASE.'static'.SD.'media'.SD.$datalink;
            @mkdir($link,0777,true);
            @chmod($link, 0777);
            $link=$link.SD.$nombrefile;
            if(file_exists($link)){
				@chmod($link, 0777);
			}
			$fp = fopen($link, "w");	
			$infodata=$_REQUEST;
			if(!empty($infodata["link"]))unset($infodata["link"]);
$txtarchivo='<?php
$infodatosdescarga=<<<STRCHINGO
	'.json_encode($infodata).'	
STRCHINGO;
$infodatosdescarga=json_decode($infodatosdescarga,true);
?>';
$write = fputs($fp,$txtarchivo);
fclose($fp);
@chmod($link,0777);
    echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursos_colaborativos')).' '.JrTexto::_('saved successfully'),'link'=>$datalink.SD.$nombrefile));               
		 exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function link(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$infodatosdescarga=array();
		
			if(!empty($_REQUEST["datalink"])){
				$link=RUTA_BASE.'static'.SD.'media'.SD.$_REQUEST["datalink"];
				if(file_exists($link)){
					@chmod($link, 0777);
					include_once($link);
				}
			}

			//$this->oNegRecursos_colaborativos->__set('idrecurso', $_REQUEST['idrecurso']);
			//$res=$this->oNegRecursos_colaborativos->eliminar();			
			echo json_encode(array('code'=>200,'data'=>$infodatosdescarga));
			exit(0);
		}catch(Exception $e) {
			//echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			//exit(0);
		}
	}
	


	public function verificar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->user=NegSesion::getUsuario();
			$this->empresa=NegSesion::getEmpresa();

			JrCargador::clase('sys_negocio::NegDescargas_asignadas', RUTA_BASE);
			$oNegDescargas_asignadas = new NegDescargas_asignadas;
			$filtros=array();
			$filtros["idpersona"]=$this->user["idpersona"];
			$filtros["idempresa"]=$this->user["idempresa"];
			$filtros["idproyecto"]=$this->user["idproyecto"];
			$filtros["contarconsumidas"]=1;
			$filtros["sqlget"]=1;
			$alldescargas=$oNegDescargas_asignadas->buscar($filtros);

			if($this->user["idrol"]!=3){
				echo json_encode(array('code'=>200,'tienepermiso'=>true));
				exit();
			}

			if(!empty($alldescargas)){
				if(intval($alldescargas["total_descargas"])>$alldescargas["total_consumidas"]){				
					echo json_encode(array('code'=>200,'tienepermiso'=>true));
					exit();
				}
			}
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('No cuenta con suficiente número de descargas asignadas'),'tienepermiso'=>false));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos','tienepermiso'=>false));
			exit(0);
		}	
	}
}