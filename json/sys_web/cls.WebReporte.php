<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegReporte', RUTA_BASE);
class WebReporte extends JrWeb
{
	private $oNegReporte;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegReporte = new NegReporte;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Reporte', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idreporte"])&&@$_REQUEST["idreporte"]!='')$filtros["idreporte"]=$_REQUEST["idreporte"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["id_alumno_grupo"])&&@$_REQUEST["id_alumno_grupo"]!='')$filtros["id_alumno_grupo"]=$_REQUEST["id_alumno_grupo"];
			if(isset($_REQUEST["informacion"])&&@$_REQUEST["informacion"]!='')$filtros["informacion"]=$_REQUEST["informacion"];
			if(isset($_REQUEST["fechacreacion"])&&@$_REQUEST["fechacreacion"]!='')$filtros["fechacreacion"]=$_REQUEST["fechacreacion"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegReporte->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idresumen)) {
				$this->oNegReporte->idresumen = $idresumen;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegReporte->idreporte=@$idreporte;
				$this->oNegReporte->idusuario=@$idusuario;
				$this->oNegReporte->idrol=@$idrol;
				$this->oNegReporte->tipo=@$tipo;
				$this->oNegReporte->id_alumno_grupo=@$id_alumno_grupo;
				$this->oNegReporte->informacion=@$informacion;
				$this->oNegReporte->fechacreacion=@$fechacreacion;
				
            if($accion=='_add') {
            	$res=$this->oNegReporte->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Reporte')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegReporte->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Reporte')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegReporte->__set('idresumen', $_REQUEST['idresumen']);
			$res=$this->oNegReporte->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegReporte->setCampo($_REQUEST['idresumen'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}