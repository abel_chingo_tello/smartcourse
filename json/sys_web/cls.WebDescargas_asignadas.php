<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-12-2020 
 * @copyright	Copyright (C) 02-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegDescargas_asignadas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
class WebDescargas_asignadas extends JrWeb
{
	private $oNegDescargas_asignadas;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegDescargas_asignadas = new NegDescargas_asignadas;
		$this->oNegPersonal = new NegPersonal;
		
	}

	public function defecto(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Descargas_asignadas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["id"])) $filtros["id"]=$_REQUEST["id"];

			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"]; 
  			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"]; 
  			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"]; 
  			if(isset($_REQUEST["codigo_suscripcion"])&&@$_REQUEST["codigo_suscripcion"]!='')$filtros["codigo_suscripcion"]=$_REQUEST["codigo_suscripcion"];   			
  			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;


			if(!empty($filtros["idproyecto"])){
  				$filtros["idproyecto"]=@$usuarioAct["idproyecto"];
  			}
					
			$this->datos=$this->oNegDescargas_asignadas->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function asignar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_REQUEST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $datospersona=array('tipodoc'=>@$_REQUEST["tipodocumento"],'dni'=>@$_REQUEST["numerodocumento"],'usuario'=>@$_REQUEST["usuario"],'sqlsolopersona'=>true);
            if(!empty($_REQUEST["idpersona"])) $datospersona["idpersona"]=$_REQUEST["idpersona"];
            $persona=$this->oNegPersonal->buscar($datospersona);
            if(empty($persona[0])){
            	echo json_encode(array('code'=>'error','msj'=>'no encontramos a ala persona indicada'));
				exit();
            }
            $persona=$persona[0];

            if(empty($_REQUEST["idempresa"])){
				echo json_encode(array('code'=>'error','msj'=>'No ha definido a que empresa pertenece la persona enviada'));
				exit();
			}

			if(empty($_REQUEST["idproyecto"])){
				echo json_encode(array('code'=>'error','msj'=>'No ha definido a que proyecto pertenece la persona enviada'));
				exit();
			}
			$this->oNegDescargas_asignadas->idpersona=$persona["idpersona"];
  			$this->oNegDescargas_asignadas->idempresa=@$_REQUEST["idempresa"];
  			$this->oNegDescargas_asignadas->idproyecto=@$_REQUEST["idproyecto"];
  			$this->oNegDescargas_asignadas->total_asignado=!empty($_REQUEST["cantidad_descarga"])?$_REQUEST["cantidad_descarga"]:1;
           	$this->oNegDescargas_asignadas->idproyecto=$idproyecto;
  			$this->oNegDescargas_asignadas->codigo_suscripcion=$_REQUEST["codigosuscripcion"];
  			$res=$this->oNegDescargas_asignadas->agregar();
            echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Descargas_asignadas')).' '.JrTexto::_('saved successfully'),'newid'=>$res));        
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }

	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_REQUEST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $datospersona=array('tipodoc'=>@$_REQUEST["tipodocumento"],'dni'=>@$_REQUEST["numerodocumento"],'usuario'=>@$_REQUEST["usuario"],'sqlsolopersona'=>true);
            if(!empty($_REQUEST["idpersona"])) $datospersona["idpersona"]=$_REQUEST["idpersona"];
            $persona=$this->oNegPersonal->buscar($datospersona);
            if(empty($persona[0])){
            	echo json_encode(array('code'=>'error','msj'=>'no encontramos a ala persona indicada'));
				exit();
            }
            $persona=$persona[0];

            if(empty($_REQUEST["idempresa"])){
				echo json_encode(array('code'=>'error','msj'=>'No ha definido a que empresa pertenece la persona enviada'));
				exit();
			}

			if(empty($_REQUEST["idproyecto"])){
				echo json_encode(array('code'=>'error','msj'=>'No ha definido a que proyecto pertenece la persona enviada'));
				exit();
			}

			$validarcodigo=!empty($_REQUEST["validarcodigo"])?$_REQUEST["validarcodigo"]:'no';
			if($validarcodigo!='no'){  //falta validar codigo  que  no este usado solo si se reuqiere  que el codigo sea unico 
			}

			$this->oNegDescargas_asignadas->idpersona=$persona["idpersona"];
  			$this->oNegDescargas_asignadas->idempresa=@$_REQUEST["idempresa"];
  			$this->oNegDescargas_asignadas->idproyecto=@$_REQUEST["idproyecto"];
  			$this->oNegDescargas_asignadas->total_asignado=!empty($_REQUEST["cantidad_descarga"])?$_REQUEST["cantidad_descarga"]:1;
           	$this->oNegDescargas_asignadas->idproyecto=$idproyecto;
  			$this->oNegDescargas_asignadas->codigo_suscripcion=$_REQUEST["codigosuscripcion"];
  			$res=$this->oNegDescargas_asignadas->agregar();
            echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Descargas_asignadas')).' '.JrTexto::_('saved successfully'),'newid'=>$res));			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegDescargas_asignadas->__set('id', $_REQUEST['id']);
			$res=$this->oNegDescargas_asignadas->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegDescargas_asignadas->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}