<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020 
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRubrica_competencia', RUTA_BASE);
class WebRubrica_competencia extends JrWeb
{
	private $oNegRubrica_competencia;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRubrica_competencia = new NegRubrica_competencia;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Rubrica_competencia', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idrubrica_competencia"])&&@$_REQUEST["idrubrica_competencia"]!='')$filtros["idrubrica_competencia"]=$_REQUEST["idrubrica_competencia"];
			if(isset($_REQUEST["idrubrica"])&&@$_REQUEST["idrubrica"]!='')$filtros["idrubrica"]=$_REQUEST["idrubrica"];
			if(isset($_REQUEST["idcompetencia"])&&@$_REQUEST["idcompetencia"]!='')$filtros["idcompetencia"]=$_REQUEST["idcompetencia"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRubrica_competencia->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idrubrica_competencia)) {
				$this->oNegRubrica_competencia->idrubrica_competencia = $idrubrica_competencia;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRubrica_competencia->idrubrica=@$idrubrica;
				$this->oNegRubrica_competencia->idcompetencia=@$idcompetencia;
				
            if($accion=='_add') {
            	$res=$this->oNegRubrica_competencia->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rubrica_competencia')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRubrica_competencia->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rubrica_competencia')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegRubrica_competencia->idrubrica=@$idrubrica;
				$this->oNegRubrica_competencia->idcompetencia=@$idcompetencia;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRubrica_competencia->__set('idrubrica_competencia', $_REQUEST['idrubrica_competencia']);
			$res=$this->oNegRubrica_competencia->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRubrica_competencia->setCampo($_REQUEST['idrubrica_competencia'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}