<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSence_assigncursos', RUTA_BASE);

class WebSence_assigncursos extends JrWeb{
    protected $oNegSence_assigncursos;

    public function __construct()
	{
		parent::__construct();
		$this->oNegSence_assigncursos = new NegSence_assigncursos;
    }
    public function defecto(){
        return $this->buscar();
    }
    /**
     * Funcion generica para buscar en la tabla especifica sin datos relacionados
     * Por favor no modificar tanto la logica de la función y si es necesaria, delimitar consulta mediante un parametro especifico
     * @param Array REQUEST para los filtros de la consulta
     */
    public function buscar(){
		$this->documento->plantilla = 'blanco';
        try{
            global $aplicacion;

            $filtros = [];

            if(!empty($_REQUEST['id'])){ $filtros['id'] = $_REQUEST['id']; }
            if(!empty($_REQUEST['idempresa'])){ $filtros['idempresa'] = $_REQUEST['idempresa']; }
            if(!empty($_REQUEST['codsence'])){ $filtros['codsence'] = $_REQUEST['codsence']; }
            if(!empty($_REQUEST['idproyecto'])){ $filtros['idproyecto'] = $_REQUEST['idproyecto']; }
            if(!empty($_REQUEST['idcurso'])){ $filtros['idcurso'] = $_REQUEST['idcurso']; }
            if(!empty($_REQUEST['idcomplementario'])){ $filtros['idcomplementario'] = $_REQUEST['idcomplementario']; }
            if(!empty($_REQUEST['estado'])){ $filtros['estado'] = $_REQUEST['estado']; }
            if(!empty($_REQUEST['fecha_creacion'])){ $filtros['fecha_creacion'] = $_REQUEST['fecha_creacion']; }
            if(!empty($_REQUEST['desdeFecha_creacion'])){ $filtros['desdeFecha_creacion'] = $_REQUEST['desdeFecha_creacion']; }
            if(!empty($_REQUEST['hastaFecha_creacion'])){ $filtros['hastaFecha_creacion'] = $_REQUEST['hastaFecha_creacion']; }
            if(!empty($_REQUEST['fecha_actualizacion'])){ $filtros['fecha_actualizacion'] = $_REQUEST['fecha_actualizacion']; }
            if(!empty($_REQUEST['desdeFecha_actualizacion'])){ $filtros['desdeFecha_actualizacion'] = $_REQUEST['desdeFecha_actualizacion']; }
            if(!empty($_REQUEST['hastaFecha_actualizacion'])){ $filtros['hastaFecha_actualizacion'] = $_REQUEST['hastaFecha_actualizacion']; }

            $this->datos = $this->oNegSence_assigncursos->buscar($filtros);

            echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
    }
    /**
     * Funcion generica para insertar registro en la tabla
     * La función esta destinada a insertar como tabla unica, si se requiere hacer un insertado mas complejo, por favor hacer otra función
     * ya que esta función es generica y se puede reutilizar en diferentes modulos del sistema
     * @param Array REQUEST POST los datos a procesar
     * @return String JSON del resultado de la operación, ademas retorna el ultimo id insertado
     */
    public function insert(){
        $this->documento->plantilla = 'blanco';
        try{
            global $aplicacion;
            
            if(empty($_POST)){ throw new Exception("POST Vacio"); }

            $data = [];

            if(!empty($_POST['idempresa'])){ $data['idempresa'] = $_POST['idempresa']; }
            if(!empty($_POST['idproyecto'])){ $data['idproyecto'] = $_POST['idproyecto']; }
            if(!empty($_POST['idcurso'])){ $data['idcurso'] = $_POST['idcurso']; }
            if(!empty($_POST['idcomplementario'])){ $data['idcomplementario'] = $_POST['idcomplementario']; }
            if(!empty($_POST['codsence'])){ $data['codsence'] = $_POST['codsence']; }
            if(isset($_POST['estado'])){ $data['estado'] = $_POST['estado']; }

            $this->datos = $this->oNegSence_assigncursos->insert([$data]);

            echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
    }
    /**
     * Funcion generica para actualizar registro en la tabla
     * La funcion esta destinada solo a actualizar registros sin logica especial, si se requiere un proceso mas complejo, por favor crea otra funcion preferiblemente
     * Este proceso es reutilizado por varios modulos del sistema, y un cambio drastico puede causar fallas
     * @param Array REQUEST POST los datos a procesar
     * @return Boolean Resultado en TRUE si se realizo el proceso completamente, FALSE algo ocurrio
     */
    public function update(){
        $this->documento->plantilla = 'blanco';
        try{
            global $aplicacion;
            
            if(empty($_POST)){ throw new Exception("POST Vacio"); }
            if(empty($_POST['id'])){ throw new Exception("ID Vacio"); }

            $data = ['id'=>$_POST['id']];
            $esEstricto = !empty($_POST['estricto']) ? true : false;

            if(!empty($_POST['idempresa'])){ $data['idempresa'] = $_POST['idempresa']; }
            if(!empty($_POST['idproyecto'])){ $data['idproyecto'] = $_POST['idproyecto']; }
            if(!empty($_POST['idcurso'])){ $data['idcurso'] = $_POST['idcurso']; }
            if(!empty($_POST['idcomplementario'])){ $data['idcomplementario'] = $_POST['idcomplementario']; }
            if(!empty($_POST['codsence'])){ $data['codsence'] = $_POST['codsence']; }
            if(isset($_POST['estado'])){ $data['estado'] = $_POST['estado']; }

            $this->datos = $this->oNegSence_assigncursos->update([$data],$esEstricto);

            echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
    }
}

?>