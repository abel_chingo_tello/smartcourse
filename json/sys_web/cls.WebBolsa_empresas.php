<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-04-2020 
 * @copyright	Copyright (C) 24-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE);
class WebBolsa_empresas extends JrWeb
{
	private $oNegBolsa_empresas;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBolsa_empresas = new NegBolsa_empresas;				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_empresas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["rason_social"])&&@$_REQUEST["rason_social"]!='')$filtros["rason_social"]=$_REQUEST["rason_social"];
			if(isset($_REQUEST["ruc"])&&@$_REQUEST["ruc"]!='')$filtros["ruc"]=$_REQUEST["ruc"];
			if(isset($_REQUEST["logo"])&&@$_REQUEST["logo"]!='')$filtros["logo"]=$_REQUEST["logo"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["representante"])&&@$_REQUEST["representante"]!='')$filtros["representante"]=$_REQUEST["representante"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["tipo_matricula"])&&@$_REQUEST["tipo_matricula"]!='')$filtros["tipo_matricula"]=$_REQUEST["tipo_matricula"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegBolsa_empresas->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	public function empresas(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_empresas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idempresa"]))$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(!empty($_REQUEST["nombre"]))$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["rason_social"])&&@$_REQUEST["rason_social"]!='')$filtros["rason_social"]=$_REQUEST["rason_social"];
			if(isset($_REQUEST["ruc"])&&@$_REQUEST["ruc"]!='')$filtros["ruc"]=$_REQUEST["ruc"];
			if(isset($_REQUEST["logo"])&&@$_REQUEST["logo"]!='')$filtros["logo"]=$_REQUEST["logo"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["representante"])&&@$_REQUEST["representante"]!='')$filtros["representante"]=$_REQUEST["representante"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["tipo_matricula"])&&@$_REQUEST["tipo_matricula"]!='')$filtros["tipo_matricula"]=$_REQUEST["tipo_matricula"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegBolsa_empresas->empresas($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	/**
	 * Funcion que retorna todas las empresas que posee un usuario mediante idpersona, ademas trae todos los proyectos asociado a la empresa
	 * @param Array $_REQUEST para los filtros de las busquedas
	 * @return String Objeto json 
	 */
	public function buscarempresaxpersona(){
		try{
			$filtros = array();
			if(empty($_REQUEST['idpersona'])){
				throw new Exception("Se requiere el id de la persona");
			}
			$filtros['idpersonal'] = $_REQUEST['idpersona'];
			
			if(!empty($_REQUEST["idempresa"]))$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(!empty($_REQUEST["idproyecto"]))$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(!empty($_REQUEST["idrol"]))$filtros["idrol"]=$_REQUEST["idrol"];
			if(!empty($_REQUEST["estado"]))$filtros["estado"]=$_REQUEST["estado"];

			$this->datos = $this->oNegBolsa_empresas->buscarempresaxpersona($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
            exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            if(!empty($idempresa)) {
				$this->oNegBolsa_empresas->idempresa = $idempresa;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegBolsa_empresas->nombre=@$nombre;
			$this->oNegBolsa_empresas->rason_social=@$rason_social;
			$this->oNegBolsa_empresas->ruc=@$ruc;
			$this->oNegBolsa_empresas->logo=@$logo;
			$this->oNegBolsa_empresas->direccion=@$direccion;
			$this->oNegBolsa_empresas->telefono=@$telefono;
			$this->oNegBolsa_empresas->representante=@$representante;
			if(!empty($usuario))$this->oNegBolsa_empresas->usuario=@$usuario;
			if(!empty($clave))$this->oNegBolsa_empresas->clave=@$clave;
			if(isset($correo))$this->oNegBolsa_empresas->correo=$correo;
			if(isset($estado))$this->oNegBolsa_empresas->estado=$estado;
			if(isset($tipo_matricula))$this->oNegBolsa_empresas->tipo_matricula=$tipo_matricula;
            if($accion=='_add') {
            	$this->oNegBolsa_empresas->idpersona=isset($idpersona)?$idpersona:$usuarioAct["idpersona"];
            	$res=$this->oNegBolsa_empresas->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	if(isset($idpersona))$this->oNegBolsa_empresas->idpersona=@$idpersona;
            	$res=$this->oNegBolsa_empresas->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegBolsa_empresas->nombre=@$nombre;
				$this->oNegBolsa_empresas->rason_social=@$rason_social;
				$this->oNegBolsa_empresas->ruc=@$ruc;
				$this->oNegBolsa_empresas->logo=@$logo;
				$this->oNegBolsa_empresas->direccion=@$direccion;
				$this->oNegBolsa_empresas->telefono=@$telefono;
				$this->oNegBolsa_empresas->representante=@$representante;
				$this->oNegBolsa_empresas->usuario=@$usuario;
				$this->oNegBolsa_empresas->clave=@$clave;
				$this->oNegBolsa_empresas->correo=@$correo;
				$this->oNegBolsa_empresas->estado=@$estado;
				$this->oNegBolsa_empresas->tipo_matricula=@$tipo_matricula;
				$this->oNegBolsa_empresas->idpersona=@$idpersona;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegBolsa_empresas->__set('idempresa', $_REQUEST['idempresa']);
			$res=$this->oNegBolsa_empresas->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}
	public function editarmultiple(){
		try{
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			if(empty($_REQUEST['idempresa'])){ throw new Exception("Id no definido"); }
			
			$toEdit = ['idempresa'=> $_REQUEST['idempresa']];

			if(!empty($_REQUEST['nombre'])){ $toEdit['nombre'] = $_REQUEST['nombre']; }
			if(!empty($_REQUEST['rason_social'])){ $toEdit['rason_social'] = $_REQUEST['rason_social']; }
			if(!empty($_REQUEST['ruc'])){ $toEdit['ruc'] = $_REQUEST['ruc']; }
			if(!empty($_REQUEST['direccion'])){ $toEdit['direccion'] = $_REQUEST['direccion']; }
			if(!empty($_REQUEST['telefono'])){ $toEdit['telefono'] = $_REQUEST['telefono']; }
			if(!empty($_REQUEST['correo'])){ $toEdit['correo'] = $_REQUEST['correo']; }
			if(!empty($_REQUEST['logo'])){ $toEdit['logo'] = $_REQUEST['logo']; }
			if(!empty($_REQUEST['representante'])){ $toEdit['representante'] = $_REQUEST['representante']; }
			if(!empty($_REQUEST['usuario'])){ $toEdit['usuario'] = $_REQUEST['usuario']; }
			if(!empty($_REQUEST['clave'])){ $toEdit['clave'] = $_REQUEST['clave']; }
			if(isset($_REQUEST['estado'])){ $toEdit['estado'] = $_REQUEST['estado']; }
			if(!empty($_REQUEST['tipo_matricula'])){ $toEdit['tipo_matricula'] = $_REQUEST['tipo_matricula']; }
			if(!empty($_REQUEST['idpersona'])){ $toEdit['idpersona'] = $_REQUEST['idpersona']; }
			
			$this->oNegBolsa_empresas->editarmultiple([$toEdit]);

			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Datos Actualizados')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}
	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegBolsa_empresas->setCampo($_REQUEST['idempresa'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}