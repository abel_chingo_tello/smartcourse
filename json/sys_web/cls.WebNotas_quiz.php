<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
class WebNotas_quiz extends JrWeb
{
	private $oNegNotas_quiz;

	public function __construct()
	{
		parent::__construct();
		$this->oNegNotas_quiz = new NegNotas_quiz;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idcursodetalle"]) && @$_REQUEST["idcursodetalle"] != '') $filtros["idcursodetalle"] = $_REQUEST["idcursodetalle"];
			if (isset($_REQUEST["idrecurso"]) && @$_REQUEST["idrecurso"] != '') $filtros["idrecurso"] = $_REQUEST["idrecurso"];
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["nota"]) && @$_REQUEST["nota"] != '') $filtros["nota"] = $_REQUEST["nota"];
			if (isset($_REQUEST["notatexto"]) && @$_REQUEST["notatexto"] != '') $filtros["notatexto"] = $_REQUEST["notatexto"];
			if (isset($_REQUEST["regfecha"]) && @$_REQUEST["regfecha"] != '') $filtros["regfecha"] = $_REQUEST["regfecha"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["calificacion_en"]) && @$_REQUEST["calificacion_en"] != '') $filtros["calificacion_en"] = $_REQUEST["calificacion_en"];
			if (isset($_REQUEST["calificacion_total"]) && @$_REQUEST["calificacion_total"] != '') $filtros["calificacion_total"] = $_REQUEST["calificacion_total"];
			if (isset($_REQUEST["calificacion_min"]) && @$_REQUEST["calificacion_min"] != '') $filtros["calificacion_min"] = $_REQUEST["calificacion_min"];
			if (isset($_REQUEST["tiempo_total"]) && @$_REQUEST["tiempo_total"] != '') $filtros["tiempo_total"] = $_REQUEST["tiempo_total"];
			if (isset($_REQUEST["tiempo_realizado"]) && @$_REQUEST["tiempo_realizado"] != '') $filtros["tiempo_realizado"] = $_REQUEST["tiempo_realizado"];
			if (isset($_REQUEST["calificacion"]) && @$_REQUEST["calificacion"] != '') $filtros["calificacion"] = $_REQUEST["calificacion"];
			if (isset($_REQUEST["habilidades"]) && @$_REQUEST["habilidades"] != '') $filtros["habilidades"] = $_REQUEST["habilidades"];
			if (isset($_REQUEST["habilidad_puntaje"]) && @$_REQUEST["habilidad_puntaje"] != '') $filtros["habilidad_puntaje"] = $_REQUEST["habilidad_puntaje"];
			if (isset($_REQUEST["intento"]) && @$_REQUEST["intento"] != '') $filtros["intento"] = $_REQUEST["intento"];
			if (isset($_REQUEST["idexamenalumnoquiz"]) && @$_REQUEST["idexamenalumnoquiz"] != '') $filtros["idexamenalumnoquiz"] = $_REQUEST["idexamenalumnoquiz"];
			if (isset($_REQUEST["idexamenproyecto"]) && @$_REQUEST["idexamenproyecto"] != '') $filtros["idexamenproyecto"] = $_REQUEST["idexamenproyecto"];
			if (isset($_REQUEST["idexamenidalumno"]) && @$_REQUEST["idexamenidalumno"] != '') $filtros["idexamenidalumno"] = $_REQUEST["idexamenidalumno"];
			if (isset($_REQUEST["datos"]) && @$_REQUEST["datos"] != '') $filtros["datos"] = $_REQUEST["datos"];
			if (isset($_REQUEST["fechamodificacion"]) && @$_REQUEST["fechamodificacion"] != '') $filtros["fechamodificacion"] = $_REQUEST["fechamodificacion"];
			if (isset($_REQUEST["preguntas"]) && @$_REQUEST["preguntas"] != '') $filtros["preguntas"] = $_REQUEST["preguntas"];
			if (isset($_REQUEST["oldid"]) && @$_REQUEST["oldid"] != '') $filtros["oldid"] = $_REQUEST["oldid"];
			if (isset($_REQUEST["oldido"]) && @$_REQUEST["oldido"] != '') $filtros["oldido"] = $_REQUEST["oldido"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegNotas_quiz->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idnota)) {
				$this->oNegNotas_quiz->idnota = $idnota;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();


			$this->oNegNotas_quiz->idcursodetalle = @$idcursodetalle;
			$this->oNegNotas_quiz->idrecurso = @$idrecurso;
			$this->oNegNotas_quiz->idalumno = @$idalumno;
			$this->oNegNotas_quiz->tipo = @$tipo;
			$this->oNegNotas_quiz->nota = @$nota;
			$this->oNegNotas_quiz->notatexto = @$notatexto;
			$this->oNegNotas_quiz->regfecha = @$regfecha;
			$this->oNegNotas_quiz->idproyecto = @$idproyecto;
			$this->oNegNotas_quiz->calificacion_en = @$calificacion_en;
			$this->oNegNotas_quiz->calificacion_total = @$calificacion_total;
			$this->oNegNotas_quiz->calificacion_min = @$calificacion_min;
			$this->oNegNotas_quiz->tiempo_total = @$tiempo_total;
			$this->oNegNotas_quiz->tiempo_realizado = @$tiempo_realizado;
			$this->oNegNotas_quiz->calificacion = @$calificacion;
			$this->oNegNotas_quiz->habilidades = @$habilidades;
			$this->oNegNotas_quiz->habilidad_puntaje = @$habilidad_puntaje;
			$this->oNegNotas_quiz->intento = @$intento;
			$this->oNegNotas_quiz->idexamenalumnoquiz = @$idexamenalumnoquiz;
			$this->oNegNotas_quiz->idexamenproyecto = @$idexamenproyecto;
			$this->oNegNotas_quiz->idexamenidalumno = @$idexamenidalumno;
			$this->oNegNotas_quiz->datos = @$datos;
			$this->oNegNotas_quiz->fechamodificacion = @$fechamodificacion;
			$this->oNegNotas_quiz->preguntas = @$preguntas;
			$this->oNegNotas_quiz->oldid = @$oldid;
			$this->oNegNotas_quiz->oldido = @$oldido;

			if ($accion == '_add') {
				$res = $this->oNegNotas_quiz->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Notas_quiz')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegNotas_quiz->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Notas_quiz')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegNotas_quiz->__set('idnota', $_REQUEST['idnota']);
			$res = $this->oNegNotas_quiz->eliminar();
			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegNotas_quiz->setCampo($_REQUEST['idnota'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
