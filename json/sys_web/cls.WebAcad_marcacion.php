<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_marcacion', RUTA_BASE);
class WebAcad_marcacion extends JrWeb
{
	private $oNegAcad_marcacion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_marcacion = new NegAcad_marcacion;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_marcacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idgrupodetalle"])&&@$_REQUEST["idgrupodetalle"]!='')$filtros["idgrupodetalle"]=$_REQUEST["idgrupodetalle"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idhorario"])&&@$_REQUEST["idhorario"]!='')$filtros["idhorario"]=$_REQUEST["idhorario"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["horaingreso"])&&@$_REQUEST["horaingreso"]!='')$filtros["horaingreso"]=$_REQUEST["horaingreso"];
			if(isset($_REQUEST["horasalida"])&&@$_REQUEST["horasalida"]!='')$filtros["horasalida"]=$_REQUEST["horasalida"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["nalumnos"])&&@$_REQUEST["nalumnos"]!='')$filtros["nalumnos"]=$_REQUEST["nalumnos"];
			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAcad_marcacion->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idmarcacion)) {
				$this->oNegAcad_marcacion->idmarcacion = $idmarcacion;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegAcad_marcacion->idgrupodetalle=@$idgrupodetalle;
				$this->oNegAcad_marcacion->idcurso=@$idcurso;
				$this->oNegAcad_marcacion->idhorario=@$idhorario;
				$this->oNegAcad_marcacion->fecha=@$fecha;
				$this->oNegAcad_marcacion->horaingreso=@$horaingreso;
				$this->oNegAcad_marcacion->horasalida=@$horasalida;
				$this->oNegAcad_marcacion->iddocente=@$iddocente;
				$this->oNegAcad_marcacion->nalumnos=@$nalumnos;
				$this->oNegAcad_marcacion->observacion=@$observacion;
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_marcacion->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_marcacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_marcacion->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_marcacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_marcacion->__set('idmarcacion', $_REQUEST['idmarcacion']);
			$res=$this->oNegAcad_marcacion->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_marcacion->setCampo($_REQUEST['idmarcacion'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}