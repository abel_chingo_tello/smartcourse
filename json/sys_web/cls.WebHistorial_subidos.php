<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegHistorial_subidos', RUTA_BASE);
class WebHistorial_subidos extends JrWeb
{
	private $oNegHistorial_subidos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegHistorial_subidos = new NegHistorial_subidos;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Historial_subidos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre_archivo"])&&@$_REQUEST["nombre_archivo"]!='')$filtros["nombre_archivo"]=$_REQUEST["nombre_archivo"];
			if(isset($_REQUEST["fechasubida"])&&@$_REQUEST["fechasubida"]!='')$filtros["fechasubida"]=$_REQUEST["fechasubida"];
			if(isset($_REQUEST["tabla"])&&@$_REQUEST["tabla"]!='')$filtros["tabla"]=$_REQUEST["tabla"];
			if(isset($_REQUEST["descomprimido"])&&@$_REQUEST["descomprimido"]!='')$filtros["descomprimido"]=$_REQUEST["descomprimido"];
			if(isset($_REQUEST["importacion_total"])&&@$_REQUEST["importacion_total"]!='')$filtros["importacion_total"]=$_REQUEST["importacion_total"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegHistorial_subidos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idsubido)) {
				$this->oNegHistorial_subidos->idsubido = $idsubido;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegHistorial_subidos->nombre_archivo=@$nombre_archivo;
				$this->oNegHistorial_subidos->fechasubida=@$fechasubida;
				$this->oNegHistorial_subidos->tabla=@$tabla;
				$this->oNegHistorial_subidos->descomprimido=@$descomprimido;
				$this->oNegHistorial_subidos->importacion_total=@$importacion_total;
				
            if($accion=='_add') {
            	$res=$this->oNegHistorial_subidos->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Historial_subidos')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegHistorial_subidos->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Historial_subidos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegHistorial_subidos->__set('idsubido', $_REQUEST['idsubido']);
			$res=$this->oNegHistorial_subidos->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegHistorial_subidos->setCampo($_REQUEST['idsubido'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}