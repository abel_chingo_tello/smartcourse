<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula',RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);

class WebExamenes extends JrWeb
{
	private $oNegExamenes;
	private $oNegNotas_quiz;
	private $oNegAcad_cursodetalle;
	private $oNegAcad_matricula;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamenes = new NegExamenes;
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_matricula = new NegAcad_matricula;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examenes', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idnivel"])&&@$_REQUEST["idnivel"]!='')$filtros["idnivel"]=$_REQUEST["idnivel"];
			if(isset($_REQUEST["idunidad"])&&@$_REQUEST["idunidad"]!='')$filtros["idunidad"]=$_REQUEST["idunidad"];
			if(isset($_REQUEST["idactividad"])&&@$_REQUEST["idactividad"]!='')$filtros["idactividad"]=$_REQUEST["idactividad"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["portada"])&&@$_REQUEST["portada"]!='')$filtros["portada"]=$_REQUEST["portada"];
			if(isset($_REQUEST["fuente"])&&@$_REQUEST["fuente"]!='')$filtros["fuente"]=$_REQUEST["fuente"];
			if(isset($_REQUEST["fuentesize"])&&@$_REQUEST["fuentesize"]!='')$filtros["fuentesize"]=$_REQUEST["fuentesize"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["grupo"])&&@$_REQUEST["grupo"]!='')$filtros["grupo"]=$_REQUEST["grupo"];
			if(isset($_REQUEST["aleatorio"])&&@$_REQUEST["aleatorio"]!='')$filtros["aleatorio"]=$_REQUEST["aleatorio"];
			if(isset($_REQUEST["calificacion_por"])&&@$_REQUEST["calificacion_por"]!='')$filtros["calificacion_por"]=$_REQUEST["calificacion_por"];
			if(isset($_REQUEST["calificacion_en"])&&@$_REQUEST["calificacion_en"]!='')$filtros["calificacion_en"]=$_REQUEST["calificacion_en"];
			if(isset($_REQUEST["calificacion_total"])&&@$_REQUEST["calificacion_total"]!='')$filtros["calificacion_total"]=$_REQUEST["calificacion_total"];
			if(isset($_REQUEST["calificacion_min"])&&@$_REQUEST["calificacion_min"]!='')$filtros["calificacion_min"]=$_REQUEST["calificacion_min"];
			if(isset($_REQUEST["tiempo_por"])&&@$_REQUEST["tiempo_por"]!='')$filtros["tiempo_por"]=$_REQUEST["tiempo_por"];
			if(isset($_REQUEST["tiempo_total"])&&@$_REQUEST["tiempo_total"]!='')$filtros["tiempo_total"]=$_REQUEST["tiempo_total"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["nintento"])&&@$_REQUEST["nintento"]!='')$filtros["nintento"]=$_REQUEST["nintento"];
			if(isset($_REQUEST["calificacion"])&&@$_REQUEST["calificacion"]!='')$filtros["calificacion"]=$_REQUEST["calificacion"];
			if(isset($_REQUEST["origen_habilidades"])&&@$_REQUEST["origen_habilidades"]!='')$filtros["origen_habilidades"]=$_REQUEST["origen_habilidades"];
			if(isset($_REQUEST["habilidades_todas"])&&@$_REQUEST["habilidades_todas"]!='')$filtros["habilidades_todas"]=$_REQUEST["habilidades_todas"];
			if(isset($_REQUEST["fuente_externa"])&&@$_REQUEST["fuente_externa"]!='')$filtros["fuente_externa"]=$_REQUEST["fuente_externa"];
			if(isset($_REQUEST["dificultad_promedio"])&&@$_REQUEST["dificultad_promedio"]!='')$filtros["dificultad_promedio"]=$_REQUEST["dificultad_promedio"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegExamenes->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	public function notas(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			$filtros = array();

			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];

			if(!empty($filtros)&&!empty($filtros['idproyecto'])){
				throw new Exception("Some fields is required");
			}
			$filtros['sql'] = 'sql1';
			$this->datos=$this->oNegAcad_matricula->notas($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
			exit(0);
		}catch(Exception $e){
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function te(){
		global $aplicacion;			
		$c = $this->oNegAcad_cursodetalle->sesiones(69,0);
		var_dump($c);
		exit(0);
	}

	public function _notas(){
		try{
			$filtros = array();

			$idrol=!empty($_REQUEST["idrol"])?$_REQUEST["idrol"] : 3; //obtener el rol, por defecto es 3
			
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];

			$filtros['orderby'] = 'strcurso';

			$mismatriculas=$this->oNegAcad_matricula->buscar($filtros);

			$hayexamenescurso=array();
			$datosreturn=array();
			$Dpuntajemax=100;
			$Dpuntajemin=51;

			if(!empty($mismatriculas)){
				foreach ($mismatriculas as $key => $cur) {
					$cur["notaentrada"]=0;
					$cur["notasalida"]=0;
					$cur["notaentrada_enbasea"]=100;
					$cur["notasalida_enbasea"]=100;
					$cur["notaentradaquiz"]=0;
					$cur["notaentradaquiz_enbasea"]=100;
					$cur["notasalidaquiz"]=0;
					$cur["notasalidaquiz_enbasea"]=100;
					$cur["notasalidatiempo"]='00:00:00';
					$cur["notaentradatiempo"]='00:00:00';
					$notaconfig=array();
					$examenes=array();
					//verificar si tiene una configuracion de notas
					if(!empty($cur["configuracion_nota"])){
						$notaconfig=json_decode($cur["configuracion_nota"],true);
						if(empty($notaconfig["tipocalificacion"]) && empty($notaconfig["puntuacion"])){

							$cur["puntaje"]=array('puntajemax'=>intval($Dpuntajemax),'puntajemin'=>intval($Dpuntajemin),'tipocalificacion'=>'N');
						}elseif($notaconfig["tipocalificacion"]=='P'||$notaconfig["tipocalificacion"]=='N'){

							$cur["puntaje"]=array('puntajemax'=>@intval($notaconfig["maxcal"]),'puntajemin'=>@intval($notaconfig["mincal"]),'tipocalificacion'=>@$notaconfig["tipocalificacion"]);	
						}else{
							$cur["puntaje"]=array('tipocalificacion'=>@$notaconfig["tipocalificacion"],'puntuacion'=>@$notaconfig["puntuacion"]);
						} 
						$tmpexamenes=$notaconfig["examenes"];
						if(!empty($tmpexamenes)){
							foreach ($tmpexamenes as $tmpexa){
								if(@$tmpexa["porcentaje"]>0){
									//var_dump($cur["puntaje"]);
									$puntajemax=!empty($cur["puntaje"]['puntajemax'])?$cur["puntaje"]['puntajemax']:$this->Dpuntajemax;
									$puntajemin=!empty($cur["puntaje"]['puntajemin'])?$cur["puntaje"]['puntajemin']:$this->Dpuntajemin;
									$examenes[]=array('idexamen'=>$tmpexa["idexamen"],'tipo'=>@$tmpexa["tipo"],'nombre'=>$tmpexa["nombre"],'idcursodetalle'=>$tmpexa["idcursodetalle"],'nota'=>0,'porcentaje'=>@intval($tmpexa["porcentaje"]),'puntajemax'=>intval($puntajemax),'puntajemin'=>intval($puntajemin),'imagen'=>$tmpexa["imagen"],'idrecurso'=>$tmpexa["idrecurso"]);
								}
							}//endforeach
						}//endif
						$cur["formula"]=$examenes;
					}else{
						//no tener configuracion
						$examenes[]=array('idexamen'=>0,'nota'=>0,'porcentaje'=>100);
						$cur["puntaje"]=array('puntajemax'=>intval($Dpuntajemax),'puntajemin'=>intval($Dpuntajemin),'tipocalificacion'=>'N');
						$cur["formula"]=$examenes;
					}
					$examenes=array(); // falta revisar que tome la nota y asignarla al curso del alumno..
					$idalumno=$cur["idalumno"];
					$notasdelcurso=$this->oNegNotas_quiz->buscar(array('idalumno'=>$idalumno,'idcurso'=>$cur["idcurso"],'idproyecto' => $cur["idproyecto"]));
					$resultadoconnotascurso=array(); // todas las notas por curso si las hubiera
					if(!empty($notasdelcurso)){
						//obtener los id de los examen segun el curso
						if(empty($hayexamenescurso[$cur["idcurso"]])){
							$examenes=$this->oNegAcad_cursodetalle->examenes($cur["idcurso"],0);
							$hayexamenescurso[$cur["idcurso"]]=$examenes;
						}else{
							$examenes=$hayexamenescurso[$cur["idcurso"]];	
						}
						
						if(!empty($examenes)){
							foreach($examenes as $ex){
								//setear las notas
								foreach ($notasdelcurso as $nt) {
									$datosJSON = json_decode($datosJSON,true);
									$curptmax=!empty($cur["puntaje"]["puntajemax"])?$cur["puntaje"]["puntajemax"]:$Dpuntajemax;
									$curptmim=!empty($cur["puntaje"]["puntajemin"])?$cur["puntaje"]["puntajemin"]:$Dpuntajemin;
									$calificacion_total=!empty($nt["calificacion_total"])?ceil($nt["calificacion_total"]):$Dpuntajemax;
									$calificacion_min=!empty($nt["calificacion_min"])?ceil($nt["calificacion_min"]):$Dpuntajemin;
									$calificacion_en=!empty($nt["calificacion_en"])?ceil($nt["calificacion_en"]):'P';
									$tiempo_realizado=!empty($nt["tiempo_realizado"])?ceil($nt["tiempo_realizado"]):'00:00:00';
									$nota=0;
									$nota=$this->notacal($nt["nota"],$calificacion_total,100);
									$nota=$this->notacal($nota,100,$curptmax);// nota equivalente al
									$cur["nota"]=$nota;
									$cur["notaquiz"]=$nt["nota"];
									$cur["notatiempo"]=$tiempo_realizado;
									$cur["notaquiz_enbasea"]=$calificacion_total;
									$cur["nota_enbasea"]=$curptmax;
									$cur["tipoexamen"]=!empty($datosJSON['tipo']) ? $datosJSON['tipo'] : null;
									$cur["num_examen"]=!empty($datosJSON['num_examen']) ? $datosJSON['num_examen'] : null;
									
									//añadir datos a la formula
									if(!empty($cur["formula"])){
										foreach ($cur["formula"] as $fr){
											if(!empty($fr["idexamen"])){
												if($fr["idexamen"]==$nt["idrecurso"]&&$fr["idcursodetalle"]==$nt["idcursodetalle"]){
													$nota=$this->notacal($nt["nota"],100,$calificacion_total); // nota al smarquiz al 100%
													$nota=$this->notacal($nota,100,$fr["puntajemax"]);
													$fr["nota"]=$nota;
													$fr["nota_enbasea"]=$fr["puntajemax"];
													$fr["notaquiz"]=$nt["nota"];
													$fr["notatiempo"]=$tiempo_realizado;
													$fr["notaquiz_enbasea"]=$calificacion_total;
												}
												$newformula[]=$fr;
											}
										}//endforeach
									}//endif formula
									if(!empty($newformula)){
										$cur["formula"]=$newformula;	
									}
								}//endforeach notasdelcurso
							}
						}
					}//endif empty notasdelcurso
					$this->datos[]=$cur;				
				}//endforeach
			}//endif;
			var_dump($this->datos);
			exit();
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}
	private function notacal($pt,$ptmax=100,$equivalente=100){ // regla de tres para calcular nota;
		if($pt==0) return 0;
		return round((($pt*$equivalente)/$ptmax),2);
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idexamen)) {
				$this->oNegExamenes->idexamen = $idexamen;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegExamenes->idnivel=@$idnivel;
				$this->oNegExamenes->idunidad=@$idunidad;
				$this->oNegExamenes->idactividad=@$idactividad;
				$this->oNegExamenes->titulo=@$titulo;
				$this->oNegExamenes->descripcion=@$descripcion;
				$this->oNegExamenes->portada=@$portada;
				$this->oNegExamenes->fuente=@$fuente;
				$this->oNegExamenes->fuentesize=@$fuentesize;
				$this->oNegExamenes->tipo=@$tipo;
				$this->oNegExamenes->grupo=@$grupo;
				$this->oNegExamenes->aleatorio=@$aleatorio;
				$this->oNegExamenes->calificacion_por=@$calificacion_por;
				$this->oNegExamenes->calificacion_en=@$calificacion_en;
				$this->oNegExamenes->calificacion_total=@$calificacion_total;
				$this->oNegExamenes->calificacion_min=@$calificacion_min;
				$this->oNegExamenes->tiempo_por=@$tiempo_por;
				$this->oNegExamenes->tiempo_total=@$tiempo_total;
				$this->oNegExamenes->estado=@$estado;
				$this->oNegExamenes->idpersonal=@$idpersonal;
				$this->oNegExamenes->fecharegistro=@$fecharegistro;
				$this->oNegExamenes->nintento=@$nintento;
				$this->oNegExamenes->calificacion=@$calificacion;
				$this->oNegExamenes->origen_habilidades=@$origen_habilidades;
				$this->oNegExamenes->habilidades_todas=@$habilidades_todas;
				$this->oNegExamenes->fuente_externa=@$fuente_externa;
				$this->oNegExamenes->dificultad_promedio=@$dificultad_promedio;
				
            if($accion=='_add') {
            	$res=$this->oNegExamenes->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Examenes')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegExamenes->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Examenes')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegExamenes->__set('idexamen', $_REQUEST['idexamen']);
			$res=$this->oNegExamenes->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegExamenes->setCampo($_REQUEST['idexamen'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}