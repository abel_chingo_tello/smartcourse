<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-04-2020 
 * @copyright	Copyright (C) 07-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEvento_programacion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegZoomalumnos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegZoom', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
class WebEvento_programacion extends JrWeb
{
	private $oNegEvento_programacion;
	private $oNegAcad_cursodetalle;
	private $oNegAcad_matricula;
	private $oNegAcad_grupoauladetalle;
	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegEvento_programacion = new NegEvento_programacion;
		$this->oNegZoomalumnos = new NegZoomalumnos;
		$this->oNegZoom = new NegZoom;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Evento_programacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["id_programacion"]) && @$_REQUEST["id_programacion"] != '') $filtros["id_programacion"] = $_REQUEST["id_programacion"];
			if (isset($_REQUEST["fecha"]) && @$_REQUEST["fecha"] != '') $filtros["fecha"] = $_REQUEST["fecha"];
			if (isset($_REQUEST["titulo"]) && @$_REQUEST["titulo"] != '') $filtros["titulo"] = $_REQUEST["titulo"];
			if (isset($_REQUEST["detalle"]) && @$_REQUEST["detalle"] != '') $filtros["detalle"] = $_REQUEST["detalle"];
			if (isset($_REQUEST["hora_comienzo"]) && @$_REQUEST["hora_comienzo"] != '') $filtros["hora_comienzo"] = $_REQUEST["hora_comienzo"];
			if (isset($_REQUEST["hora_fin"]) && @$_REQUEST["hora_fin"] != '') $filtros["hora_fin"] = $_REQUEST["hora_fin"];
			if (isset($_REQUEST["idpersona"]) && @$_REQUEST["idpersona"] != '') $filtros["idpersona"] = $_REQUEST["idpersona"];
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["start"]) && @$_REQUEST["start"] != '') $filtros["start"] = $_REQUEST["start"];
			if (isset($_REQUEST["end"]) && @$_REQUEST["end"] != '') $filtros["end"] = $_REQUEST["end"];
			$this->oNegAcad_grupoaula = new NegAcad_grupoaula;

			$oUsuario = NegSesion::getUsuario();
			$filtrosPrev = array();
			$filtrosPrev["idalumno"] = $oUsuario["idpersona"];
			$filtrosPrev["idproyecto"] = $oUsuario["idproyecto"];
			$filtrosPrev['fechaactiva'] = true;
			$arrProfes = $this->oNegAcad_matricula->listaProfes($filtrosPrev); //varios cursos

			$range = '';
			$size = count($arrProfes);
			for ($i = 0; $i < $size; $i++) {
				$profe = $arrProfes[$i];
				$range .= $profe['iddocente'] . ', ';
			}
			//lo de mis docentes, más lo mío:
			$range .= $oUsuario['idpersona'];
			$filtros["range"] = $range;
			$arrEventos = $this->oNegEvento_programacion->buscar($filtros);
			$arrMaped = array();
			foreach ($arrEventos as $key => $evento) {
				$arrMaped[] = array(
					"tipo_evento" => $evento["tipo_evento"],
					"id_programacion" => $evento["id_programacion"],
					"idpersona" => $evento["idpersona"],
					"title" => $evento["titulo"],
					"autor" => $evento["autor"],
					"start" => "$evento[fecha] $evento[hora_comienzo]",
					"end" => "$evento[fecha] $evento[hora_fin]",
					"fecha" => $evento["fecha"],
					"detalle" => $evento["detalle"],
					"hora_comienzo" => $evento["hora_comienzo"],
					"hora_fin" => $evento["hora_fin"],
					"idgrupoaula" => $evento["idgrupoaula"],
					"idgrupoauladetalle" => $evento["idgrupoauladetalle"],
				);
			}
			$arrconferenciasGenericas=array();
			$arrconferenciasGenericas=$this->conferenciasGenericas();
			// ZOOM
			$arrZoom = array();
			$filtros['mysql'] = 2;
			if ($oUsuario["idrol"] == 3) { //alumno
				$filtros["idalumno"] = $oUsuario["idpersona"];
				$arrZoom = $this->oNegZoomalumnos->buscar($filtros);
			} else { //docente o admin
				$filtros["idpersona"] = $oUsuario["idpersona"];
				$arrZoom = $this->oNegZoom->buscar($filtros);				
			}
			
			// ZOOM
			$this->datos = array_merge($arrMaped, $arrZoom,$arrconferenciasGenericas);
			echo json_encode($this->datos);
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function conferenciasGenericas()
	{
		$this->documento->plantilla = 'blanco';
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();
			$arrMatriculas=array();
			if ($oUsuario["idrol"] == 2) {
				$filtros["iddocente"] = $oUsuario["idpersona"];
				$filtros["idproyecto"] = $oUsuario["idproyecto"];
				$arrInstancias=$this->oNegAcad_grupoauladetalle->buscar($filtros);
				$arrMatriculas=$arrInstancias;
			}else{//Alumno
				$filtros["idalumno"] = $oUsuario["idpersona"];
				$filtros["idproyecto"] = $oUsuario["idproyecto"];
				$filtros['fechaactiva'] = true;
				$arrMatriculas = $this->oNegAcad_matricula->buscar($filtros); //varios cursos
			}

			$this->datos = array();
			$arrConferenciasGenericas = array();
			// echo json_encode($arrMatriculas); exit();
			foreach ($arrMatriculas as $i => $matricula) { //dentro de un curso
				$arrCursosDetalles = array();
				if ($oUsuario["idrol"] == 2){
					if ($matricula['idcomplementario']>0) {
						$matricula['tipocurso'] = '2';
					}else{
						$matricula['tipocurso'] = '1';
					}
				}
				if ($matricula['tipocurso'] != '2') {
					$arrCursosDetalles = $this->oNegAcad_cursodetalle->getCursosDetalles(array("idcurso" => $matricula["idcurso"]));
				} else {
					$arrCursosDetalles = $this->oNegAcad_cursodetalle->getCursosDetalles(array("sql" => 'sql1', "idcurso" => $matricula["idcurso"]));
				}
				// echo json_encode($arrCursosDetalles); exit();
				foreach ($arrCursosDetalles as $key => $cursoDetalle) { //dentro de cursosdetalles
					$cursoDetalle["txtjson"] = (array) json_decode($cursoDetalle["txtjson"], true);
					if (isset($cursoDetalle["txtjson"]["options"])) {
						$validar = count($cursoDetalle["txtjson"]["options"]);
					} else {
						$validar = 0;
					}
					if ($validar == 0) { //[SESION] No tiene pestañas (options)
						if (isset($cursoDetalle["txtjson"]["typelink"]) && $cursoDetalle["txtjson"]["typelink"] == "videoconferencia") {
							$arrConferenciasGenericas[] = $this->mapType3($cursoDetalle["txtjson"]["link"], $matricula);
						}
					} else { //[PESTAÑA] Sí tiene pestañas (options)
						//entramos a pestañas (options)
						foreach ($cursoDetalle["txtjson"]["options"] as $keyO => $option) {
							if (isset($option["type"]) && $option["type"] == "videoconferencia") {
								$arrConferenciasGenericas[] = $this->mapType3($option["link"], $matricula);
							}
						}
					}
					$arrCursosDetalles[$key] = $cursoDetalle;
				}
			}
			$this->datos = $arrConferenciasGenericas;
			return $this->datos;
			// echo json_encode(array('code' => 200, 'data' => $this->datos));
			// exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	private function mapType3($strBase64Link, $matricula)
	{
		$strJson = base64_decode($strBase64Link);
		$conferencia = (array) json_decode(utf8_encode($strJson));
		$maped = array(
			"tipo_evento" => 3,
			"title" => strtoupper($matricula["strcurso"]) . ": $conferencia[titulo]",
			"link_mod" => $conferencia["link_mod"],
			"link_visitor" => $conferencia["link_visitor"],
			"fecha" => $conferencia["fecha"],
			"hora_comienzo" => $conferencia["hora_comienzo"],
			"hora_fin" => null,
			"detalle" => $conferencia["detalle"],
			"start" => "$conferencia[fecha] $conferencia[hora_comienzo]",
			"end" => null,
			"autor" => $matricula["strdocente"],
			"idpersona" => null,
		);
		return $maped;
	}
	public function listadoDias()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Evento_programacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["id_programacion"]) && @$_REQUEST["id_programacion"] != '') $filtros["id_programacion"] = $_REQUEST["id_programacion"];
			if (isset($_REQUEST["fecha"]) && @$_REQUEST["fecha"] != '') $filtros["fecha"] = $_REQUEST["fecha"];
			if (isset($_REQUEST["titulo"]) && @$_REQUEST["titulo"] != '') $filtros["titulo"] = $_REQUEST["titulo"];
			if (isset($_REQUEST["detalle"]) && @$_REQUEST["detalle"] != '') $filtros["detalle"] = $_REQUEST["detalle"];
			if (isset($_REQUEST["hora_comienzo"]) && @$_REQUEST["hora_comienzo"] != '') $filtros["hora_comienzo"] = $_REQUEST["hora_comienzo"];
			if (isset($_REQUEST["hora_fin"]) && @$_REQUEST["hora_fin"] != '') $filtros["hora_fin"] = $_REQUEST["hora_fin"];
			if (isset($_REQUEST["idpersona"]) && @$_REQUEST["idpersona"] != '') $filtros["idpersona"] = $_REQUEST["idpersona"];
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			$this->oNegAcad_grupoaula = new NegAcad_grupoaula;

			$oUsuario = NegSesion::getUsuario();
			$filtros["idalumno"] = $oUsuario["idpersona"];
			$filtros["idproyecto"] = $oUsuario["idproyecto"];
			$filtros['fechaactiva'] = true;
			$arrProfes = $this->oNegAcad_matricula->listaProfes($filtros); //varios cursos
			$this->datos = $arrProfes;
			$range = '';
			$size = count($arrProfes);
			for ($i = 0; $i < $size; $i++) {
				$profe = $arrProfes[$i];
				$range .= $profe['iddocente'] . ', ';
			}
			$range .= $oUsuario['idpersona'];
			$filtros = array("range" => $range);
			$arrEventos = $this->oNegEvento_programacion->buscar($filtros);
			$arrDias = array();

			$size = count($arrEventos);
			if ($size) { //si hay eventos...

				$tmpFecha = $arrEventos[0]['fecha'];
				$dia = array(
					"fecha" => $tmpFecha,
					"arrayEventos" => []
				);
				foreach ($arrEventos as $i => $evento) { //recorremos cada evento
					if ($evento['fecha'] === $tmpFecha) { //si aún seguimos en la misma fecha
						$dia['arrayEventos'][] = $evento; //añadimos el evento al día
					}
					if (($i + 1) < $size) { //si hay siguiente iteración
						if ($arrEventos[$i + 1]['fecha'] != $tmpFecha) { //si en la siguiente iteración cambia la fecha
							//significa que el dia acabó
							$tmpFecha = $arrEventos[$i + 1]['fecha'];
							$arrDias[] = $dia;
							$dia = array(
								"fecha" => $tmpFecha,
								"arrayEventos" => []
							);
						}
					}
				}
				$arrDias[] = $dia;
			}

			$this->datos = $arrDias;
			// $this->datos =$arrEventos;

			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function loadSelects()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			$oUsuario = NegSesion::getUsuario();
			$selects = array(
				"arrGrupos" => $this->oNegAcad_grupoaula->buscargrupos(array("mysql1" => "mysql1", "iddocente" => "$oUsuario[idpersona]", "idproyecto" => "$oUsuario[idproyecto]")),
				"arrCursos" => $this->oNegAcad_grupoauladetalle->buscar(array("sql2" => "sql2", "iddocente" => "$oUsuario[idpersona]", "idproyecto" => "$oUsuario[idproyecto]"))
			);

			$this->datos = $selects;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function eventosZoom()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			$oUsuario = NegSesion::getUsuario();
			$arrZoom = array();
			$filtros['mysql'] = 2;
			if ($oUsuario["idrol"] == 3) { //alumno
				$filtros["idalumno"] = $oUsuario["idpersona"];
				$arrZoom = $this->oNegZoomalumnos->buscar($filtros);
			} else { //docente o admin
				$filtros["idpersona"] = $oUsuario["idpersona"];
				$arrZoom = $this->oNegZoom->buscar($filtros);
			}
			$this->datos = $arrZoom;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$id_programacion)) {
				$this->oNegEvento_programacion->id_programacion = $id_programacion;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();
			// echo "<hr>".var_dump($_POST)."<hr>";

			$this->oNegEvento_programacion->fecha = @$fecha;
			$this->oNegEvento_programacion->titulo = @$titulo;
			$this->oNegEvento_programacion->detalle = @$detalle;
			$this->oNegEvento_programacion->hora_comienzo = @$hora_comienzo;
			$this->oNegEvento_programacion->hora_fin = @$hora_fin;
			$this->oNegEvento_programacion->idpersona = $usuarioAct['idpersona'];
			$this->oNegEvento_programacion->idgrupoaula = @$idgrupoaula;
			$this->oNegEvento_programacion->idgrupoauladetalle = @$idgrupoauladetalle;

			if ($accion == '_add') {
				$res = $this->oNegEvento_programacion->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Evento_programacion')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegEvento_programacion->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Evento_programacion')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegEvento_programacion->fecha = @$fecha;
					$this->oNegEvento_programacion->titulo = @$titulo;
					$this->oNegEvento_programacion->detalle = @$detalle;
					$this->oNegEvento_programacion->hora_comienzo = @$hora_comienzo;
					$this->oNegEvento_programacion->hora_fin = @$hora_fin;
					$this->oNegEvento_programacion->idpersona = @$idpersona;
					$this->oNegEvento_programacion->idgrupoaula = @$idgrupoaula;
					$this->oNegEvento_programacion->idgrupoauladetalle = @$idgrupoauladetalle;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegEvento_programacion->__set('id_programacion', $_REQUEST['id_programacion']);
			$res = $this->oNegEvento_programacion->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEvento_programacion->setCampo($_REQUEST['id_programacion'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
