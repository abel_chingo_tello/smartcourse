<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_sesionhtml', RUTA_BASE);
class WebAcad_sesionhtml extends JrWeb
{
	private $oNegAcad_sesionhtml;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_sesionhtml = new NegAcad_sesionhtml;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_sesionhtml', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["html"])&&@$_REQUEST["html"]!='')$filtros["html"]=$_REQUEST["html"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAcad_sesionhtml->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idsesion)) {
				$this->oNegAcad_sesionhtml->idsesion = $idsesion;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegAcad_sesionhtml->idcurso=@$idcurso;
				$this->oNegAcad_sesionhtml->idpersonal=@$idpersonal;
				$this->oNegAcad_sesionhtml->idcursodetalle=@$idcursodetalle;
				$this->oNegAcad_sesionhtml->html=@$html;
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_sesionhtml->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_sesionhtml')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_sesionhtml->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_sesionhtml')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_sesionhtml->__set('idsesion', $_REQUEST['idsesion']);
			$res=$this->oNegAcad_sesionhtml->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_sesionhtml->setCampo($_REQUEST['idsesion'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}