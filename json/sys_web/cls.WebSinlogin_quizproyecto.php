<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-01-2021 
 * @copyright	Copyright (C) 19-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSinlogin_quizproyecto', RUTA_BASE);
class WebSinlogin_quizproyecto extends JrWeb
{
	private $oNegSinlogin_quizproyecto;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSinlogin_quizproyecto = new NegSinlogin_quizproyecto;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Sinlogin_quizproyecto', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idquiz"])&&@$_REQUEST["idquiz"]!='')$filtros["idquiz"]=$_REQUEST["idquiz"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["user_registro"])&&@$_REQUEST["user_registro"]!='')$filtros["user_registro"]=$_REQUEST["user_registro"];
			if(isset($_REQUEST["fecha_hasta"])&&@$_REQUEST["fecha_hasta"]!='')$filtros["fecha_hasta"]=$_REQUEST["fecha_hasta"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			$usuarioAct = NegSesion::getUsuario();
			if(empty($filtros["idproyecto"])) $filtros["idproyecto"]=$usuarioAct["idproyecto"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegSinlogin_quizproyecto->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idasignacion)) {
				$this->oNegSinlogin_quizproyecto->idasignacion = $idasignacion;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
				$this->oNegSinlogin_quizproyecto->idquiz=@$idquiz;
				$this->oNegSinlogin_quizproyecto->idproyecto=@$usuarioAct["idproyecto"];
				$this->oNegSinlogin_quizproyecto->user_registro=@$usuarioAct["idpersona"];
				$this->oNegSinlogin_quizproyecto->fecha_hasta=@!empty($fecha_hasta)?$fecha_hasta:((date('Y')+1).date('-m-d'));
				$this->oNegSinlogin_quizproyecto->estado=isset($estado)?$estado:1;
				
            if($accion=='_add') {
            	$res=$this->oNegSinlogin_quizproyecto->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Sinlogin_quizproyecto')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegSinlogin_quizproyecto->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Sinlogin_quizproyecto')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegSinlogin_quizproyecto->idquiz=@$idquiz;
					$this->oNegSinlogin_quizproyecto->idproyecto=@$idproyecto;
					$this->oNegSinlogin_quizproyecto->user_registro=@$user_registro;
					$this->oNegSinlogin_quizproyecto->fecha_hasta=@$fecha_hasta;
					$this->oNegSinlogin_quizproyecto->estado=@$estado;
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegSinlogin_quizproyecto->setCampo($_REQUEST['idasignacion'],'estado',-1);
			//$res=$this->oNegSinlogin_quizproyecto->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegSinlogin_quizproyecto->setCampo($_REQUEST['idasignacion'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}