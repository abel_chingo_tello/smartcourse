<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamen_ubicacion', RUTA_BASE);
class WebExamen_ubicacion extends JrWeb
{
	private $oNegExamen_ubicacion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamen_ubicacion = new NegExamen_ubicacion;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["activo"])&&@$_REQUEST["activo"]!='')$filtros["activo"]=$_REQUEST["activo"];
			if(isset($_REQUEST["rango_min"])&&@$_REQUEST["rango_min"]!='')$filtros["rango_min"]=$_REQUEST["rango_min"];
			if(isset($_REQUEST["rango_max"])&&@$_REQUEST["rango_max"]!='')$filtros["rango_max"]=$_REQUEST["rango_max"];
			if(isset($_REQUEST["idexam_prerequisito"])&&@$_REQUEST["idexam_prerequisito"]!='')$filtros["idexam_prerequisito"]=$_REQUEST["idexam_prerequisito"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegExamen_ubicacion->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idexamen_ubicacion)) {
				$this->oNegExamen_ubicacion->idexamen_ubicacion = $idexamen_ubicacion;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegExamen_ubicacion->idrecurso=@$idrecurso;
				$this->oNegExamen_ubicacion->idcurso=@$idcurso;
				$this->oNegExamen_ubicacion->tipo=@$tipo;
				$this->oNegExamen_ubicacion->activo=@$activo;
				$this->oNegExamen_ubicacion->rango_min=@$rango_min;
				$this->oNegExamen_ubicacion->rango_max=@$rango_max;
				$this->oNegExamen_ubicacion->idexam_prerequisito=@$idexam_prerequisito;
				
            if($accion=='_add') {
            	$res=$this->oNegExamen_ubicacion->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Examen_ubicacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegExamen_ubicacion->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Examen_ubicacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegExamen_ubicacion->__set('idexamen_ubicacion', $_REQUEST['idexamen_ubicacion']);
			$res=$this->oNegExamen_ubicacion->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegExamen_ubicacion->setCampo($_REQUEST['idexamen_ubicacion'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}