<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-02-2020 
 * @copyright	Copyright (C) 11-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTareas_mensajes', RUTA_BASE);
class WebTareas_mensajes extends JrWeb
{
	private $oNegTareas_mensajes;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTareas_mensajes = new NegTareas_mensajes;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tareas_mensajes', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idtareamensaje"])&&@$_REQUEST["idtareamensaje"]!='')$filtros["idtareamensaje"]=$_REQUEST["idtareamensaje"];
			if(isset($_REQUEST["idtarea"])&&@$_REQUEST["idtarea"]!='')$filtros["idtarea"]=$_REQUEST["idtarea"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='')$filtros["rol"]=$_REQUEST["rol"];
			if(isset($_REQUEST["mensaje"])&&@$_REQUEST["mensaje"]!='')$filtros["mensaje"]=$_REQUEST["mensaje"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegTareas_mensajes->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idtareamensaje)) {
				$this->oNegTareas_mensajes->idtareamensaje = $idtareamensaje;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();	        
			$this->oNegTareas_mensajes->idtarea=@$idtarea;
			$this->oNegTareas_mensajes->idusuario=@$idusuario;
			$this->oNegTareas_mensajes->rol=@$rol;
			$this->oNegTareas_mensajes->mensaje=@$mensaje;
			$this->oNegTareas_mensajes->estado=@$estado;
				
            if($accion=='_add') {
            	$res=$this->oNegTareas_mensajes->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Tareas_mensajes')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegTareas_mensajes->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Tareas_mensajes')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegTareas_mensajes->idtarea=@$idtarea;
					$this->oNegTareas_mensajes->idusuario=@$idusuario;
					$this->oNegTareas_mensajes->rol=@$rol;
					$this->oNegTareas_mensajes->mensaje=@$mensaje;
					$this->oNegTareas_mensajes->estado=@$estado;
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegTareas_mensajes->__set('idtareamensaje', $_REQUEST['idtareamensaje']);
			$res=$this->oNegTareas_mensajes->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegTareas_mensajes->setCampo($_REQUEST['idtareamensaje'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}