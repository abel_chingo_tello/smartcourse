<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegDetalle_matricula_alumno', RUTA_BASE);
class WebDetalle_matricula_alumno extends JrWeb
{
	private $oNegDetalle_matricula_alumno;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegDetalle_matricula_alumno = new NegDetalle_matricula_alumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Detalle_matricula_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idejercicio"])&&@$_REQUEST["idejercicio"]!='')$filtros["idejercicio"]=$_REQUEST["idejercicio"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegDetalle_matricula_alumno->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$iddetalle)) {
				$this->oNegDetalle_matricula_alumno->iddetalle = $iddetalle;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegDetalle_matricula_alumno->idmatricula=@$idmatricula;
				$this->oNegDetalle_matricula_alumno->idejercicio=@$idejercicio;
				$this->oNegDetalle_matricula_alumno->orden=@$orden;
				$this->oNegDetalle_matricula_alumno->estado=@$estado;
				
            if($accion=='_add') {
            	$res=$this->oNegDetalle_matricula_alumno->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Detalle_matricula_alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegDetalle_matricula_alumno->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Detalle_matricula_alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegDetalle_matricula_alumno->__set('iddetalle', $_REQUEST['iddetalle']);
			$res=$this->oNegDetalle_matricula_alumno->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegDetalle_matricula_alumno->setCampo($_REQUEST['iddetalle'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}