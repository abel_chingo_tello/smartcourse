<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020 
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_competencias', RUTA_BASE);
class WebAcad_competencias extends JrWeb
{
	private $oNegAcad_competencias;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_competencias = new NegAcad_competencias;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();	
			//if(!NegSesion::tiene_acceso('Competencias', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAcad_competencias->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idcompetencia)) {
				$this->oNegAcad_competencias->idcompetencia = $idcompetencia;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegAcad_competencias->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
           	$this->oNegAcad_competencias->idcategoria=!empty($idcategoria)?$idcategoria:0;
			$this->oNegAcad_competencias->nombre=@$nombre;
			$this->oNegAcad_competencias->estado=@$estado;
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_competencias->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Competencias')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_competencias->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Competencias')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
				$this->oNegAcad_competencias->idcategoria=@$idcategoria;
				$this->oNegAcad_competencias->nombre=@$nombre;
				$this->oNegAcad_competencias->estado=@$estado;
				$this->oNegAcad_competencias->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
				$newid=$this->oNegUgel->agregar();
				$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_competencias->__set('idcompetencia', $_REQUEST['idcompetencia']);
			$res=$this->oNegAcad_competencias->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_competencias->setCampo($_REQUEST['idcompetencia'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}