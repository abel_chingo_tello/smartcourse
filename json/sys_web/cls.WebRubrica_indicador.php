<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020 
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRubrica_indicador', RUTA_BASE);
class WebRubrica_indicador extends JrWeb
{
	private $oNegRubrica_indicador;

	public function __construct()
	{
		parent::__construct();
		$this->oNegRubrica_indicador = new NegRubrica_indicador;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Rubrica_indicador', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idrubrica_indicador"]) && @$_REQUEST["idrubrica_indicador"] != '') $filtros["idrubrica_indicador"] = $_REQUEST["idrubrica_indicador"];
			if (isset($_REQUEST["detalle"]) && @$_REQUEST["detalle"] != '') $filtros["detalle"] = $_REQUEST["detalle"];
			if (isset($_REQUEST["idrubrica"]) && @$_REQUEST["idrubrica"] != '') $filtros["idrubrica"] = $_REQUEST["idrubrica"];
			if (isset($_REQUEST["idrubrica_criterio"]) && @$_REQUEST["idrubrica_criterio"] != '') $filtros["idrubrica_criterio"] = $_REQUEST["idrubrica_criterio"];
			if (isset($_REQUEST["idrubrica_nivel"]) && @$_REQUEST["idrubrica_nivel"] != '') $filtros["idrubrica_nivel"] = $_REQUEST["idrubrica_nivel"];
			if (isset($_REQUEST["idcriterio"]) && @$_REQUEST["idcriterio"] != '') $filtros["idcriterio"] = $_REQUEST["idcriterio"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegRubrica_indicador->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idrubrica_indicador)) {
				$this->oNegRubrica_indicador->idrubrica_indicador = $idrubrica_indicador;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();


			$this->oNegRubrica_indicador->detalle = @$detalle;
			$this->oNegRubrica_indicador->idrubrica = @$idrubrica;
			$this->oNegRubrica_indicador->idrubrica_criterio = @$idrubrica_criterio;
			$this->oNegRubrica_indicador->idrubrica_nivel = @$idrubrica_nivel;
			$this->oNegRubrica_indicador->idcriterio = @$idcriterio;

			if ($accion == '_add') {
				$res = $this->oNegRubrica_indicador->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica_indicador')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegRubrica_indicador->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica_indicador')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegRubrica_indicador->detalle = @$detalle;
					$this->oNegRubrica_indicador->idrubrica = @$idrubrica;
					$this->oNegRubrica_indicador->idrubrica_criterio = @$idrubrica_criterio;
					$this->oNegRubrica_indicador->idrubrica_nivel = @$idrubrica_nivel;
					$this->oNegRubrica_indicador->idcriterio = @$idcriterio;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] != "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			$this->oNegRubrica_indicador->__set('idrubrica_indicador', $_REQUEST['idrubrica_indicador']);
			$res = $this->oNegRubrica_indicador->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRubrica_indicador->setCampo($_REQUEST['idrubrica_indicador'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
