<?php

/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
class WebSesion extends JrWeb
{
	private $oNegSesion;
	protected $oNegConfig;
	//public $oNegHistorialSesion;
	public function __construct()
	{
		parent::__construct();
		$this->oNegSesion = new NegSesion;
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegHistorialSesion = new NegHistorial_sesion;
	}
	public function defecto()
	{
		return $this->login();
	}
	public function login()
	{
		try {
			global $aplicacion;
			if (true === NegSesion::existeSesion() && (empty($_REQUEST['usuario']) && empty($_REQUEST['clave']) && empty($_REQUEST['token']))) {
				return $aplicacion->redir();
			} elseif (!empty($_REQUEST['token'])) {
				$filtro = array();
				if (!empty($_REQUEST['usuario'])) $filtro['usuario'] = $_REQUEST['usuario'];
				$filtro['token'] = $_REQUEST['token'];
				$filtro['idempresa'] = @$_REQUEST['idempresa'];
				$filtro['idproyecto'] = @$_REQUEST['idproyecto'];
				$res = $this->oNegSesion->ingresar($filtro);
				if ($res["code"] == 200 && $res["islogin"] == true) {
					$this->iniciarHistorialSesion('P'); // incio sesion en plataforma
					return  $aplicacion->redir();
				}
				echo json_encode($res);
			} elseif (!empty($_REQUEST['usuario']) && !empty($_REQUEST['clave'])) {
				$res = $this->oNegSesion->ingresar(array('usuario' => $_REQUEST['usuario'], 'clave' => $_REQUEST['clave'], 'idempresa' => @$_REQUEST['idempresa'], 'idproyecto' => @$_REQUEST['idproyecto']));
				if($res["code"] == 200 && $res["islogin"] == true) {
					$this->iniciarHistorialSesion('P'); // incio sesion en plataforma
					return  $aplicacion->redir();
				}
				echo json_encode($res);
				exit(0);
			} else {
				echo json_encode(array('code' => 'Error', 'msj' => 'Usuario no logueado'));
				exit(0);
			}
			return $this->iniciar(true);
		} catch (Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
		}
	}
	public function iniciar($entro = false)
	{
		try {
			global $aplicacion;
			if (false === NegSesion::existeSesion() && $entro == false) {
				return $this->login();
			}
			return $aplicacion->redir();
			//$aplicacion->typereturn();						
			//return $this->form(@$usuario);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => 'Error al cambiar de rol<br>' . JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function estoylogueado(){		
		global $aplicacion;
		$estoy=NegSesion::existeSesion();
		if($estoy==true){
			echo json_encode(array('code' => 200,'msj'=>true));
		}else echo json_encode(array('code' => 200,'msj'=>false));
		exit();
	}

	public function noroles()
	{
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Change empty'), true);
			$this->documento->plantilla = 'excepcion';
			$this->msj = JrTexto::_('Rol empty') . '!!';
			$this->esquema = 'error/general';
			if (true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}
			return parent::getEsquema();
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => 'Error noroles<br>' . JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function salir()
	{
		global $aplicacion;
		try {
			if (true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Sesion cerrada,correctamente')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => 'Error al cerrar sesion<br>' . JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function cambiar_ambito()
	{
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			//exit($usuarioAct);
			if (!empty($_GET['rol']) && !empty($_GET['idrol'])) {
				$oNegSesion = new NegSesion;
				$cambio = $oNegSesion->cambiar_rol($_GET['idrol'], $_GET['rol'], $usuarioAct["idpersona"], $usuarioAct["idproyecto"], $usuarioAct["idempresa"]);
				$usuarioAct = NegSesion::getUsuario();
			}
			$aplicacion->redir();
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => 'Error al cambiar de rol<br>' . JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function cambiarrol()
	{
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
		
			$cambio = false;
			if (!empty($_REQUEST['rol']) && !empty($_REQUEST['idrol'])) {
				$this->terminarHistorialSesion('P');


				$oNegSesion = new NegSesion;
				$idproyecto = !empty($_REQUEST["idproyecto"]) ? $_REQUEST["idproyecto"] : $usuarioAct["idproyecto"];
				$idempresa = !empty($_REQUEST["idempresa"]) ? $_REQUEST["idempresa"] : $usuarioAct["idempresa"];
				$cambio = $oNegSesion->cambiar_rol($_REQUEST['idrol'], $_REQUEST['rol'], $usuarioAct["idpersona"], $idproyecto, $idempresa);
				if ($cambio == true) {
					JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
					$oNegProyecto = new NegProyecto;
					$proyecto = $oNegProyecto->buscar(array('idempresa' => $idempresa, 'idproyecto' => $idproyecto));
					if (empty($proyecto[0])) {
						echo json_encode(array('code' => 200, 'msj' => 'La empresa aun no tiene un proyecto asignado, Valide su url', 'data' => false));
						exit(0);
					}
					$this->proyecto = $proyecto[0];
					@setcookie("sesionidproyecto", $this->proyecto["idproyecto"], time() + 60 * 60 * 24 * 180, URL_BASE.";SameSite=None");
					$_COOKIE["SC_idproyecto"] = $this->proyecto["idproyecto"];
					@setcookie("userproyecto", json_encode($this->proyecto), time() + 60 * 60 * 24 * 180, URL_BASE.";SameSite=None");
					$_COOKIE['userproyecto'] = json_encode($this->proyecto);
					@setcookie("sesionidempresa", $this->proyecto["idempresa"], time() + 60 * 60 * 24 * 180, URL_BASE.";SameSite=None");
					$_COOKIE['sesionidempresa'] = $this->proyecto["idempresa"];
					$_COOKIE['sesionidproyecto'] = $this->proyecto["idproyecto"];
					@setcookie("sesionnomempresa", $this->proyecto["nombre"], time() + 60 * 60 * 24 * 180, URL_BASE.";SameSite=None");
					$_COOKIE['sesionnomempresa'] = $this->proyecto["nombre"];
					$usuarioAct = NegSesion::getUsuario();
					$this->iniciarHistorialSesion('P');
				}
			}
			echo json_encode(array('code' => 200, 'msj' => 'ok', 'data' => $cambio, 'userempresa' => $this->proyecto, 'userproyecto' => $this->proyecto, 'usersesion' => $usuarioAct));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	protected function iniciarHistorialSesion($lugar = 'P')
	{
		//$usuarioAct = NegSesion::getUsuario();
		//$tipousuario = $usuarioAct['idrol'] == 3 ? 'A' : ($usuarioAct['idrol'] == 2 ? 'D' : ($usuarioAct['idrol'] == 1 ? 'M' : 'P'));
		//$this->oNegHistorialSesion->tipousuario = $tipousuario;
		//$this->oNegHistorialSesion->idusuario = $usuarioAct['idpersona'];
		$this->oNegHistorialSesion->lugar = $lugar;
		$this->oNegHistorialSesion->fechaentrada = date('Y-m-d H:i:s');
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		//$this->oNegHistorialSesion->idproyecto = $usuarioAct['idproyecto'];
		$idHistSesion = $this->oNegHistorialSesion->agregar();
		$sesion = JrSession::getInstancia();
		$sesion->set('idHistorialSesion', $idHistSesion, NegSesion::$keysesion);
		$sesion->set('fechaentrada', date('Y-m-d H:i:s'), NegSesion::$keysesion);
	}
	
	protected function terminarHistorialSesion($lugar = 'P')
	{
		$usuarioAct = NegSesion::getUsuario();
		if (!empty($usuarioAct['idHistorialSesion'])) {
			$hay = $this->oNegHistorialSesion->buscar(array('idhistorialsesion' => $usuarioAct['idHistorialSesion']));
			if (!empty($hay[0])) {
				$this->oNegHistorialSesion->setCampo($usuarioAct['idHistorialSesion'], 'fechasalida', date('Y-m-d H:i:s'));
			}
		}
	}
}
