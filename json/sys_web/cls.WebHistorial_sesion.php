<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
class WebHistorial_sesion extends JrWeb
{
	private $oNegHistorial_sesion;
	private $oNegAcad_matricula;

	public function __construct()
	{
		parent::__construct();
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
		$this->oNegAcad_matricula = new NegAcad_matricula;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Historial_sesion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["tipousuario"]) && @$_REQUEST["tipousuario"] != '') $filtros["tipousuario"] = $_REQUEST["tipousuario"];
			if (isset($_REQUEST["idusuario"]) && @$_REQUEST["idusuario"] != '') $filtros["idusuario"] = $_REQUEST["idusuario"];
			if (isset($_REQUEST["lugar"]) && @$_REQUEST["lugar"] != '') $filtros["lugar"] = $_REQUEST["lugar"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idcc"]) && @$_REQUEST["idcc"] != '') $filtros["idcc"] = $_REQUEST["idcc"];
			if (isset($_REQUEST["fechaentrada"]) && @$_REQUEST["fechaentrada"] != '') $filtros["fechaentrada"] = $_REQUEST["fechaentrada"];
			if (isset($_REQUEST["fechasalida"]) && @$_REQUEST["fechasalida"] != '') $filtros["fechasalida"] = $_REQUEST["fechasalida"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["texto"] = $_REQUEST["idgrupoauladetalle"];
			$this->datos = $this->oNegHistorial_sesion->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardarhistorialsesion()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (NegSesion::existeSesion() == false) {
				echo json_encode(array('code' => 'Error', 'msj' => 'Sesion ha sido cerrado'));
				exit();
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct["idrol"] != 3) {
				echo json_encode(array('code' => 200, 'data' => ['idhistorialsesion' => 0], 'msj' => 'Solo guardamos historial  para el Alumno.'));
				exit();
			}
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);

			$idusuario = $usuarioAct["idpersona"];
			$accion = '_add';
			if (@$lugar != "E") {
				if (!empty($idhistorialsesion)) {
					$hay = $this->oNegHistorial_sesion->buscar(array('idhistorialsesion' => $idhistorialsesion, 'idusuario' => $idusuario));
					if (!empty($hay[0])) {
						$accion = '_edit';
					}
				}
				$this->oNegHistorial_sesion->idusuario = $idusuario;
				$fechasalida = !empty($fechasalida) ? $fechasalida : date('Y-m-d H:i:s');
				if ($accion == '_add') {
					$this->oNegHistorial_sesion->lugar = @$lugar;
					$this->oNegHistorial_sesion->idcurso = @$idcurso;
					$this->oNegHistorial_sesion->idcc = @$idcc;
					$this->oNegHistorial_sesion->fechaentrada = !empty($fechaentrada) ? $fechaentrada : date('Y-m-d H:i:s');
					$this->oNegHistorial_sesion->fechasalida = $fechasalida;
					$this->oNegHistorial_sesion->idgrupoauladetalle = $idgrupoauladetalle;
					$res = $this->oNegHistorial_sesion->agregar();
					echo json_encode(array('code' => 200, 'data' => ['idhistorialsesion' => $res]));
				} else {
					$this->oNegHistorial_sesion->setCampo($idhistorialsesion, 'fechasalida', $fechasalida);
					echo json_encode(array('code' => 200, 'data' => ['idhistorialsesion' => $idhistorialsesion]));
				}
			} else {
				echo json_encode(array('code' => 200, 'data' => ['idhistorialsesion' => 0], 'msj' => 'Solo guardamos historial  para el Alumno.'));
				exit();
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegHistorial_sesion->__set('idhistorialsesion', $_REQUEST['idhistorialsesion']);
			$res = $this->oNegHistorial_sesion->eliminar();
			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegHistorial_sesion->setCampo($_REQUEST['idhistorialsesion'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => 'Valor Actualizado'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function _tiempos()
	{
		if (empty($_REQUEST)) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
		@extract($_POST);
		$filtros = array();
		$filtros["idpersona"] = !empty($_REQUEST["idpersona"]) ? $_REQUEST["idpersona"] : -1;
		if ($filtros["idpersona"] == -1) {
			echo json_encode(array('code' => 'error', 'msj' => 'Necesita personas para ver su tiempo'));
			exit(0);
		}
		$filtros["idcurso"] = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : -1;
		$filtros["idcc"] = !empty($_REQUEST["idcc"]) ? $_REQUEST["idcc"] : 0;
		$filtros["idgrupoauladetalle"] = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : 0;
		$datos = $this->oNegHistorial_sesion->_tiempos($filtros);
		echo json_encode(array('code' => 200, 'data' => $datos));
		exit(0);
	}

	public function _tiemposxalumno()
	{
		if (empty($_REQUEST)) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
		@extract($_POST);
		$user = NegSesion::getUsuario();
		if (empty($idpersona)) {
			echo json_encode(array('code' => 'error', 'msj' => 'Persona no seleccionada'));
			exit(0);
		}
		//$this->tiempos = $this->oNegHistorialsesion->buscartiempos(array('idusuario' => $idpersona, 'idproyecto' => $user["idproyecto"]));
		$tiempos = $this->oNegHistorial_sesion->_tiempos(array('idpersona' => $idpersona));
		$cursos = $this->oNegAcad_matricula->buscar(array('idalumno' => $idpersona, 'idproyecto' => $user["idproyecto"], 'orderby' => ' strcurso'));
		$datos = array();
		if (!empty($tiempos))
			foreach ($tiempos as $key => $value) {
				if (!empty($value["cursos"])) {
					foreach ($value["cursos"] as $k => $cur) {
						$cur["nombre"] = '';
						foreach ($cursos as $key => $cc) {
							$idgrupoauladetalle = !empty($cc["idgrupoauladetalle"]) ? $cc["idgrupoauladetalle"] : 0;
							if ($k == $idgrupoauladetalle) {
								$cur["nombre"] = $cc["strcurso"];
								$cur["idcurso"] = $cc["idcurso"];
								$cur["idgrupoauladetalle"] = $idgrupoauladetalle;
							}
						}
						$value["cursos"][$k] = $cur;
					}
				}
				$datos[$key] = $value;
			}
		echo json_encode(array('code' => 200, 'data' => $datos));
		exit(0);
	}

	public function corregir()
	{
		set_time_limit(0);
		$desde = !empty($_REQUEST["desde"]) ? $_REQUEST["desde"] : 0;
		$this->oNegHistorial_sesion->corregir($desde);
	}
}
