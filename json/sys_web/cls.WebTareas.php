<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-02-2020 
 * @copyright	Copyright (C) 11-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTareas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_archivosalumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_mensajes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
class WebTareas extends JrWeb
{
	private $oNegTareas;
	protected $oNegPersonal;
	protected $oNegProyecto;
	protected $oNegCursodetalle;

	public function __construct()
	{
		parent::__construct();
		$this->oNegTareas = new NegTareas;
		$this->oNegTareas_archivosalumno = new NegTareas_archivosalumno;
		$this->oNegTareas_mensajes = new NegTareas_mensajes;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegProyecto = new NegProyecto;
		$this->oNegCursodetalle = new NegAcad_cursodetalle;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			@extract($_REQUEST);
			$usuarioAct = NegSesion::getUsuario();
			$filtros = array();

			if ($usuarioAct["idrol"] == 3) {
				if (empty($idalumno)) $filtros["idalumno"] = $idalumno = $usuarioAct["idpersona"];
			} else {
				if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
				// if (empty($iddocente)) $filtros["iddocente"] =  $usuarioAct["idpersona"];
			}
			
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["idpestania"]) && @$_REQUEST["idpestania"] != '') $filtros["idpestania"] = $_REQUEST["idpestania"];
			if (isset($_REQUEST["recurso"]) && @$_REQUEST["recurso"] != '') $filtros["recurso"] = $_REQUEST["recurso"];
			if (isset($_REQUEST["tiporecurso"]) && @$_REQUEST["tiporecurso"] != '') $filtros["tiporecurso"] = $_REQUEST["tiporecurso"];
			if (isset($_REQUEST["nota"]) && @$_REQUEST["nota"] != '') $filtros["nota"] = $_REQUEST["nota"];
			if (isset($_REQUEST["notabaseen"]) && @$_REQUEST["notabaseen"] != '') $filtros["notabaseen"] = $_REQUEST["notabaseen"];
			if (isset($_REQUEST["iddocente"]) && @$_REQUEST["iddocente"] != '') $filtros["iddocente"] = $_REQUEST["iddocente"];
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["descripcion"]) && @$_REQUEST["descripcion"] != '') $filtros["descripcion"] = $_REQUEST["descripcion"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			if (isset($_REQUEST["fecha_calificacion"]) && @$_REQUEST["fecha_calificacion"] != '') $filtros["fecha_calificacion"] = $_REQUEST["fecha_calificacion"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			
			$_datos = $this->oNegTareas->buscar($filtros);
			$this->datos=array();
			$haycurso=array();
			$formula=array();
			if(!empty($_datos))
				foreach ($_datos as $ke => $va) {					
					if(!in_array($va["idcurso"]."_".$va["idcomplementario"], $haycurso)){
						$haycurso[]=$va["idcurso"]."_".$va["idcomplementario"];
						$formula=$this->oNegCursodetalle->geformula($va["idcurso"],$va["idcomplementario"]);
						$formula[$va["idcurso"]."_".$va["idcomplementario"]]=$formula;
					}
					$this->datos[]=$va;
				}
			// echo json_encode($this->datos);exit();
			if (!empty($this->datos))
				foreach ($this->datos as $key => $v) {
					$this->datos[$key]["archivos"] = $this->oNegTareas_archivosalumno->buscar(array('idtarea' => $v["idtarea"]));
					$this->datos[$key]["mensajes"] = $this->oNegTareas_mensajes->buscar(array('idtarea' => $v["idtarea"]));
				}

			if ($usuarioAct["idrol"] != 3){
				$filtrosAlumno=array();
				$validargrupo=false;
				if(!empty($_REQUEST["idgrupoauladetalle"])){
					$oNegMatriculas = new NegAcad_matricula;
					$filtrosAlumno["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
					$filtrosAlumno["sql"]='sql-soloalumnos';
					$alumnos=$oNegMatriculas->buscar($filtrosAlumno);
					$validargrupo=true;					
				}
				$haydatos = array();
				foreach ($this->datos as $k => $v) {
					$kk = $v["idcurso"] . "_" . $v["idsesion"] . "_" . $v["idpestania"];
					$formula_curso=array();
					if(!empty($formula[$v["idcurso"]."_".$v["idcomplementario"]])){
						$formula_curso=$formula[$v["idcurso"]."_".$v["idcomplementario"]];						
					}					
					if (empty($haydatos[$kk])) {
						$haydatos[$kk] = array('nombre' => $v["nombre"], 'dt' => array(),'formula_curso'=>$formula_curso);						
					} 
					$existealumno=false;
					if($validargrupo==true){						
						foreach ($alumnos as $k => $alu){
							if($v["idalumno"]==$alu["idalumno"]) $existealumno=true;
						}						
					}
					if($validargrupo==true){
						if($existealumno==true) $haydatos[$kk]['dt'][] = $v;
					}
					else $haydatos[$kk]['dt'][] = $v;
				}

				$this->datos = $haydatos;
			}

			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	

	public function ver(){
		try{
			$this->documento->plantilla = 'blanco';
			global $aplicacion;
			@extract($_REQUEST);
			if(NegSesion::existeSesion()==false){
                echo json_encode(array('code' => 'Error','msj'=>'Sesion ha sido cerrado','data'=>array()));
                exit();
            }
            $user = NegSesion::getUsuario();
			$idusuario=$user["idpersona"];
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$idcomplementario=!empty($_REQUEST["idcomplementario"])?$_REQUEST["idcomplementario"]:0;
			$tipo=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:'T';
			$temas=$this->oNegCursodetalle->temasacalificar($idcurso,$idcomplementario,null,null,true);
			if(empty($temas["alltemas"])){
				echo json_encode(array('code' => 200, 'msj' => JrTexto::_("El Curso no tiene Tareas asignadas"),'data'=>array()));
				exit(0);
			}
			$filtros=array();
			$filtros["idalumno"] = $idusuario;
			$filtros["idcurso"] = $idcurso;
			$filtros["idcomplementario"] = $idcomplementario;
			$filtros["idgrupoauladetalle"] = !empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:null;
			$tareasdelcurso=array();
			$tareasresueltas = $this->oNegTareas->buscar($filtros);
			foreach ($temas["alltemas"] as $kt => $tar){

				if((($tar["tipo"]=="estarea" && $tipo=='T') ||($tar["tipo"]=="esproyecto" && $tipo=='P') || ($tar["tipo"]=='TP'))  && $tar["tiporecurso"]!='smartquiz'){					
					unset($tar["id"]);
					unset($tar["nombre"]);
					unset($tar["nombresesion"]);
					$tar["nombre_alumno"]=$user["nombre_full"];
					if(!empty($tareasresueltas))
					foreach ($tareasresueltas as $kt2 => $tarea){						
						if($tar["idsesion"]==$tarea["idsesion"] && $tar["idpestania"]==$tarea["idpestania"]){
							$tar["idtarea"]=$tarea["idtarea"];
							$tar["idalumno"]=$tarea["idalumno"];
							$tar["recurso"]=$tarea["recurso"];
							$tar["tiporecurso"]=$tarea["tiporecurso"];
							$tar["nota"]=$tarea["nota"];
							$tar["notabaseen"]=$tarea["notabaseen"];
							$tar["fecha_limite"]=$tarea["fecha_limite"];
							$tar["iddocente"]=$tarea["iddocente"];
							$tar["idproyecto"]=$tarea["idproyecto"];
							$tar["descripcion"]=$tarea["descripcion"];
							$tar["fecha_calificacion"]=$tarea["fecha_calificacion"];
							$tar["criterios"]=$tarea["criterios"];
							$tar["criterios_puntaje"]=$tarea["criterios_puntaje"];
							$tar["nombre"]=$tarea["nombre"];
							$tar["idcomplementario"]=$tarea["idcomplementario"];
							$tar["idgrupoauladetalle"]=$tarea["idgrupoauladetalle"];
							$tar["fecha_creacion"]=$tarea["fecha_creacion"];
							$tar["vencido"]=$tarea["vencido"];
							$tar["nombre_docente"]=$tarea["nombre_docente"];							
							$tar["media"]=$tarea["media"];
							$tar["tipo_media"]=$tarea["tipo_media"];
							$tar["archivos"] = $this->oNegTareas_archivosalumno->buscar(array('idtarea' => $tar["idtarea"]));
							$tar["mensajes"] = $this->oNegTareas_mensajes->buscar(array('idtarea' => $tar["idtarea"]));
							//$tar["nombre_alumno"]=$tarea["nombre_alumno"];
							unset($tareasresueltas[$kt2]);
						}
					}
					$tareasdelcurso[]=$tar;
				}
			}
			if(empty($tareasdelcurso)){
				echo json_encode(array('code' => 200, 'msj' => JrTexto::_("El Curso no tiene Tareas asignadas"),'data'=>array()));
				exit(0);
			}
			
			echo json_encode(array('code' => 200, 'data' => $tareasdelcurso));
			exit(0);
		}catch(Exception $ex){
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($ex->getMessage()),'data'=>array()));
			exit(0);
		}
	}

	public function  archivosalumnoymensajes()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			@extract($_REQUEST);
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct["idrol"] == 3) {
				$idalumno = $usuarioAct["idpersona"];
				if (!empty($idtarea)) {
					$filtros = array(
						'idtarea' => $idtarea,
						'idalumno' => $idalumno,
						'idcomplementario' => $idcomplementario,
						'idgrupoauladetalle' => $idgrupoauladetalle
					);
					$tarea = $this->oNegTareas->buscar($filtros);
				}
			} else if ($usuarioAct["idrol"] == 2) {
				$iddocente = $usuarioAct["idpersona"];
				if (!empty($idtarea)) {
					$filtros = array(
						'idtarea' => $idtarea,
						'iddocente' => $iddocente,
						'idcomplementario' => $idcomplementario,
						'idgrupoauladetalle' => $idgrupoauladetalle
					);
					$tarea = $this->oNegTareas->buscar($filtros);
				}
			}

			$idtarea = 0;
			if (!empty($tarea[0])) {
				$idtarea = $idtarea;
			}
			if (empty($idtarea)) {
				if ($usuarioAct["idrol"] == 3) {
					$idalumno = $usuarioAct["idpersona"];
					$filtros = array(
						'idcurso' => $idcurso,
						'idsesion' => $idsesion,
						'idalumno' => $idalumno,
						'tipo' => $tipo,
						'idpestania' => $idpestania,
						'idcomplementario' => $idcomplementario,
						'idgrupoauladetalle' => $idgrupoauladetalle
					);
					$tarea = $this->oNegTareas->buscar($filtros);
				}
				if (!empty($tarea[0])) {
					$idtarea = $tarea[0]["idtarea"];
				}
			}

			$mensajes = array();
			$archivos = array();

			if (!empty($tarea)) {
				$filtrostarea = array();
				$filtrostarea["idtarea"] = $idtarea;
				$mensajes = $this->oNegTareas_mensajes->buscar($filtrostarea);
				$filtrosfile = array();
				$filtrosfile["idtarea"] = $idtarea;
				if ($usuarioAct["idrol"] == 3) {
					$filtrosfile["idalumno"] = $idalumno;
				}
				$archivos = $this->oNegTareas_archivosalumno->buscar($filtrosfile);
				$mensajes = !empty($mensajes) ? $mensajes : false;
				$archivos = !empty($archivos) ? $archivos : false;
				echo json_encode(array('code' => 200, 'data' => array(), 'mensajes' => $mensajes, 'archivos' => $archivos));
				exit(0);
			} else {
				$mensajes = !empty($mensajes) ? $mensajes : false;
				$archivos = !empty($archivos) ? $archivos : false;
				echo json_encode(array('code' => 200, 'data' => array(), 'mensajes' => $mensajes, 'archivos' => $archivos));
				exit(0);
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty($idtarea)) {
				$tarea = $this->oNegTareas->buscar(array('idtarea' => $idtarea));
				if (!empty($tarea[0])) {
					$this->oNegTareas->idtarea = $idtarea;
					$accion = '_edit';
					$idalumno = $this->oNegTareas->idalumno;
				}
			}
			$usuarioAct = NegSesion::getUsuario();
			$asunto = '';
			if ($usuarioAct["idrol"] == 3) {
				$idalumno = $usuarioAct["idpersona"];
				$asunto = 'Presentación de ';
			} else if ($usuarioAct["idrol"] == 2) {
				$iddocente = $usuarioAct["idpersona"];
				$asunto = 'Calificación de ';
			}
			if ($accion == '_add') {
				if ($usuarioAct["idrol"] == 3) {
					$idalumno = $usuarioAct["idpersona"];
					$tarea = $this->oNegTareas->buscar(array('idcurso' => $idcurso, 'idsesion' => $idsesion, 'idalumno' => $idalumno, 'tipo' => $tipo, 'idpestania' => $idpestania,'idcomplementario'=>$idcomplementario, 'idgrupoauladetalle'=>$idgrupoauladetalle));
				} else if ($usuarioAct["idrol"] == 2) {
					$iddocente = $usuarioAct["idpersona"];
					$tarea = $this->oNegTareas->buscar(array('idcurso' => $idcurso, 'idsesion' => $idsesion, 'iddocente' => $iddocente, 'tipo' => $tipo, 'idpestania' => $idpestania,'idcomplementario'=>$idcomplementario, 'idgrupoauladetalle'=>$idgrupoauladetalle));
				}

				if (!empty($tarea[0])) {
					$this->oNegTareas->idtarea = $tarea[0]["idtarea"];
					$accion = '_edit';
				}
			}

			$this->oNegTareas->tipo = @$tipo;
			$this->oNegTareas->idcurso = @$idcurso;
			$this->oNegTareas->idsesion = @$idsesion;
			$this->oNegTareas->idpestania = @$idpestania;
			$this->oNegTareas->idalumno = @$idalumno;
			
			if(!empty($recurso)){
				$ipost = stripos($recurso, 'static');
				if (!empty($ipost)) $recurso = substr($recurso, $ipost);
			}
			$this->oNegTareas->recurso = @$recurso;
			$this->oNegTareas->tiporecurso = @$tiporecurso;
			$this->oNegTareas->nota = @$nota;
			$this->oNegTareas->notabaseen = @$notabaseen;
			$this->oNegTareas->iddocente = @$iddocente;
			$this->oNegTareas->idproyecto = $usuarioAct["idproyecto"];
			$this->oNegTareas->nombre = @$nombre;
			$this->oNegTareas->descripcion = @$descripcion;
			$this->oNegTareas->fecha_calificacion = @$fecha_calificacion;
			$this->oNegTareas->idcomplementario = @$idcomplementario;
			$this->oNegTareas->idgrupoauladetalle = @$idgrupoauladetalle;
			$titulo = '';
			if ($tipo == 'T') $titulo = 'Tarea - ' . $nombre;
			else if ($tipo == 'P') $titulo = 'Proyecto - ' . $nombre;
			else $titulo = 'Trabajo - ' . $nombre;

			if (!empty($criterios)) $this->oNegTareas->criterios = @$criterios;
			if (!empty($criterios_puntaje)) $this->oNegTareas->criterios_puntaje = @$criterios_puntaje;
			$infores = array();
			if ($accion == '_add') {
				$res = $this->oNegTareas->agregar();
				$infores = array('code' => 200, 'msj' => ucfirst(JrTexto::_('Tareas')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res);
			} else {
				if (empty($solomensaje))	$res = $this->oNegTareas->editar();
				else $res = $this->oNegTareas->idtarea;
				$infores = array('code' => 200, 'msj' => ucfirst(JrTexto::_('Tareas')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res);
			}
			if (!empty($res) && !empty($media)) {
				$this->oNegTareas_archivosalumno->idtarea = @$res;
				$this->oNegTareas_archivosalumno->media = @$media;
				$this->oNegTareas_archivosalumno->tipo = @$tipomedia;
				$this->oNegTareas_archivosalumno->idalumno = @$idalumno;
				$infores["idarchivosubido"] = $this->oNegTareas_archivosalumno->agregar();
			}
			if (!empty($res) && !empty($mensaje)) {
				$this->oNegTareas_mensajes->idtarea = @$res;
				$this->oNegTareas_mensajes->idusuario = $usuarioAct["idpersona"];
				$this->oNegTareas_mensajes->rol = $usuarioAct["idrol"];
				$this->oNegTareas_mensajes->mensaje = $mensaje;
				$this->oNegTareas_mensajes->estado = 0;
				$infores["idmensaje"] = $this->oNegTareas_mensajes->agregar();
			}

			$infoEmpresa = $this->oNegProyecto->buscar(array('idproyecto' => $usuarioAct["idproyecto"]));
			if (!empty($infoEmpresa[0])) {
				$this->empresa = $infoEmpresa[0];
				$this->empresa["nombre"] = !empty($this->empresa["nombre"]) ? $this->empresa["nombre"] : @$this->empresa["empresa"];
				JrCargador::clase('jrAdwen::JrCorreo');
				$oCorreo = new JrCorreo;
				$deemail = !empty($this->empresa["correoempresa"]) ? $this->empresa["correoempresa"] : $usuarioAct["email"];
				$denombre = !empty($this->empresa["nombre"]) ? $this->empresa["nombre"] : $usuarioAct["nombre_full"];
				$oCorreo->setRemitente($deemail, $denombre);
				$this->titulo = $titulo;
				$this->Asunto = $asunto . " " . $titulo;
				$this->mensaje = $mensaje;
				//$oCorreo->addDestinarioPhpmailer($usuarioAct["email"],$usuarioAct["nombre_full"]);
				$oCorreo->addDestinarioPhpmailer('coordinacion@eduktvirtual.com', 'Smart Knowledge Solutions');
				// $oCorreo->addDestinarioPhpmailer('info@abacovirtual.edu.pe', 'Smart Knowledge Solutions');
				#$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com', 'Abel');
				if ($usuarioAct["idrol"] == 3 && !empty($usuarioAct["emailpadre"])) $oCorreo->addDestinarioPhpmailer($usuarioAct["emailpadre"], 'Padre de ' . $usuarioAct["nombre_full"]);
				if (!empty($this->empresa["correoempresa"]))
					$oCorreo->addDestinarioPhpmailer($this->empresa["correoempresa"], $this->empresa["nombre"]);
				if (!empty($this->empresa["logoempresa"]) && is_file(RUTA_BASE . $this->empresa["logoempresa"])) {
					$ipost = stripos($this->empresa["logoempresa"], '?');
					if ($ipost === false) $this->logoempresa = $this->empresa["logoempresa"];
					else $this->logoempresa = substr($this->empresa["logoempresa"], 0, $ipost);
					$img = str_replace('\\', '/', RUTA_BASE . $logoempresa);
					$oCorreo->addadjuntos(RUTA_BASE . $this->logoempresa, 'logoempresa');
				}
				if ($usuarioAct["idrol"] == 2) {
					$idpersonas = array($idalumno);

					$personas = $this->oNegPersonal->buscar(array('idpersona' => $idpersonas));
					if (!empty($personas))
						foreach ($personas as $k => $p) {
							$nombre = $p["ape_paterno"] . " " . $p["ape_materno"] . " " . $p["nombre"];
							if (!empty($p["email"]))
								$oCorreo->addDestinarioPhpmailer($p["email"], $nombre);
							if (!empty($p["emailpadre"]))
								$oCorreo->addDestinarioPhpmailer($p["emailpadre"], 'Padre de ' . $nombre);
						}
				}

				$this->para = '';
				$this->esquema = 'correos/general_empresa1';
				$oCorreo->setMensaje(parent::getEsquema());
				$this->oNegProyecto->idproyecto=$usuarioAct["idproyecto"];
			    $proyecto=$this->oNegProyecto->dataProyecto;
				//$oCorreo = new JrCorreo;
				if(!empty($proyecto['correo_servidor'])&& !empty($proyecto['correo_puerto']) && !empty($proyecto['correo_email']) && !empty($proyecto['correo_clave'])){
					$oCorreo->Host=$proyecto['correo_servidor'];
					$oCorreo->Port=intval($proyecto['correo_puerto']);
					$oCorreo->Username=$proyecto['correo_email'];
					$oCorreo->Password=$proyecto['correo_clave'];
					$envio = $oCorreo->sendPhpmailer();
				}				

			}
			echo json_encode($infores);
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegTareas->tipo = @$tipo;
					$this->oNegTareas->idcurso = @$idcurso;
					$this->oNegTareas->idsesion = @$idsesion;
					$this->oNegTareas->idpestania = @$idpestania;
					$this->oNegTareas->idalumno = @$idalumno;
					$this->oNegTareas->recurso = @$recurso;
					$this->oNegTareas->tiporecurso = @$tiporecurso;
					$this->oNegTareas->nota = @$nota;
					$this->oNegTareas->notabaseen = @$notabaseen;
					$this->oNegTareas->iddocente = @$iddocente;
					$this->oNegTareas->idproyecto = @$idproyecto;
					$this->oNegTareas->nombre = @$nombre;
					$this->oNegTareas->descripcion = @$descripcion;
					$this->oNegTareas->fecha_calificacion = @$fecha_calificacion;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function eliminarTodos()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct["idrol"] == 3) {
				echo json_encode(array('code' => 'error', 'msj' => 'No autorizado.'));
				exit(0);
			}
			$filtros = array();
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["idpestania"]) && @$_REQUEST["idpestania"] != '') $filtros["idpestania"] = $_REQUEST["idpestania"];
			if (isset($_REQUEST["idtarea"]) && @$_REQUEST["idtarea"] != '') $filtros["idtarea"] = $_REQUEST["idtarea"];
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["recurso"]) && @$_REQUEST["recurso"] != '') $filtros["recurso"] = $_REQUEST["recurso"];
			if (isset($_REQUEST["tiporecurso"]) && @$_REQUEST["tiporecurso"] != '') $filtros["tiporecurso"] = $_REQUEST["tiporecurso"];
			if (isset($_REQUEST["nota"]) && @$_REQUEST["nota"] != '') $filtros["nota"] = $_REQUEST["nota"];
			if (isset($_REQUEST["notabaseen"]) && @$_REQUEST["notabaseen"] != '') $filtros["notabaseen"] = $_REQUEST["notabaseen"];
			if (isset($_REQUEST["iddocente"]) && @$_REQUEST["iddocente"] != '') $filtros["iddocente"] = $_REQUEST["iddocente"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["descripcion"]) && @$_REQUEST["descripcion"] != '') $filtros["descripcion"] = $_REQUEST["descripcion"];

			if (isset($_REQUEST["fecha_calificacion"]) && @$_REQUEST["fecha_calificacion"] != '') $filtros["fecha_calificacion"] = $_REQUEST["fecha_calificacion"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			if (count($filtros) == 0) {
				echo json_encode(array('code' => 'error', 'msj' => 'Se necesita almenos un parámetro.'));
				exit(0);
			}
			
			if (
				(isset($filtros["idcurso"]) && @$filtros["idcurso"] != '') &&
				(isset($filtros["idcomplementario"]) && @$filtros["idcomplementario"] != '') &&
				(isset($filtros["idsesion"]) && @$filtros["idsesion"] != '') &&
				(isset($filtros["idpestania"]) && @$filtros["idpestania"] != '')
			) {
				$rs = $this->datos = $this->oNegTareas->eliminarTodo($filtros);
			} else {
				echo json_encode(array('code' => 'error', 'msj' => 'Parámetros incompletos'));
				exit(0);
			}


			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => $e->getMessage()));
			exit(0);
		}
	}

	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegTareas->__set('idtarea', $_REQUEST['idtarea']);
			$res = $this->oNegTareas->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegTareas->setCampo($_REQUEST['idtarea'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function updateNota()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			@extract($_POST);

			$usuarioAct = NegSesion::getUsuario();
			$this->oNegTareas->idtarea = $idtarea;
			$nombre = $this->oNegTareas->nombre;
			$this->oNegTareas->fecha_calificacion = date('Y-m-d');
			$this->oNegTareas->nota = !empty($nota) ? $nota : 0;
			if (isset($_REQUEST["notabaseen"]) && @$_REQUEST["notabaseen"] != '') {
				$this->oNegTareas->notabaseen = $_REQUEST["notabaseen"];
			} else {
				// $this->oNegTareas->notabaseen = 100;
				$this->oNegTareas->notabaseen = 20;
			}

			$this->oNegTareas->iddocente = $usuarioAct["idpersona"];
			$this->oNegTareas->criterios_puntaje = !empty($criterios) ? $criterios : '';
			$res = $this->oNegTareas->editar();

			$infoEmpresa = $this->oNegProyecto->buscar(array('idproyecto' => $usuarioAct["idproyecto"]));

			if (!empty($infoEmpresa[0])) {
				$this->empresa = $infoEmpresa[0];
				$this->empresa["nombre"] = !empty($this->empresa["nombre"]) ? $this->empresa["nombre"] : @$this->empresa["empresa"];
				JrCargador::clase('jrAdwen::JrCorreo');
				$oCorreo = new JrCorreo;
				$asunto = 'Calificación de ';
				$titulo = '';
				if ($this->oNegTareas->tipo == 'T') $titulo = 'Tarea - ' . $nombre;
				else if ($this->oNegTareas->tipo == 'P') $titulo = 'Proyecto - ' . $nombre;
				else $titulo = 'Trabajo - ' . $nombre;

				$deemail = !empty($this->empresa["correoempresa"]) ? $this->empresa["correoempresa"] : $usuarioAct["email"];
				$denombre = !empty($this->empresa["nombre"]) ? $this->empresa["nombre"] : $usuarioAct["nombre_full"];
				$oCorreo->setRemitente($deemail, $denombre);
				$this->titulo = $titulo;
				$this->Asunto = $asunto . " " . $titulo;
				$this->mensaje = 'Nota Obtenida ' . $nota;
				$oCorreo->addDestinarioPhpmailer('coordinacion@eduktvirtual.com', 'Smart Knowledge Solutions');
				// $oCorreo->addDestinarioPhpmailer('info@abacovirtual.edu.pe', 'Smart Knowledge Solutions');
				#$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com', 'Abel');
				#$oCorreo->addDestinarioPhpmailer('juanpablo.venegas@eduktvirtual.com', 'Juan Pablo');
				if (!empty($this->empresa["correoempresa"]))
					$oCorreo->addDestinarioPhpmailer($this->empresa["correoempresa"], $this->empresa["nombre"]);
				if (!empty($this->empresa["logoempresa"]) && is_file(RUTA_BASE . $this->empresa["logoempresa"])) {
					$ipost = stripos($this->empresa["logoempresa"], '?');
					if ($ipost === false) $this->logoempresa = $this->empresa["logoempresa"];
					else $this->logoempresa = substr($this->empresa["logoempresa"], 0, $ipost);
					$img = str_replace('\\', '/', RUTA_BASE . $logoempresa);
					$oCorreo->addadjuntos(RUTA_BASE . $this->logoempresa, 'logoempresa');
				}
				if ($usuarioAct["idrol"] == 2) {
					$personas = $this->oNegPersonal->buscar(array('idpersona' => $this->oNegTareas->idalumno));
					if (!empty($personas))
						foreach ($personas as $k => $p) {
							$nombre = $p["ape_paterno"] . " " . $p["ape_materno"] . " " . $p["nombre"];
							if (!empty($p["email"]))
								$oCorreo->addDestinarioPhpmailer($p["email"], $nombre);
							if (!empty($p["emailpadre"]))
								$oCorreo->addDestinarioPhpmailer($p["emailpadre"], 'Padre de ' . $nombre);
						}
				}

				$this->para = '';
				$this->esquema = 'correos/general_empresa1';
				$oCorreo->setMensaje(parent::getEsquema());
				//$envio = $oCorreo->sendPhpmailer();
			}
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Nota Guardada')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
