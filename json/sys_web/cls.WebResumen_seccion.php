<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegResumen_seccion', RUTA_BASE);
class WebResumen_seccion extends JrWeb
{
	private $oNegResumen_seccion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegResumen_seccion = new NegResumen_seccion;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Resumen_seccion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["iddre"])&&@$_REQUEST["iddre"]!='')$filtros["iddre"]=$_REQUEST["iddre"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["idgrado"])&&@$_REQUEST["idgrado"]!='')$filtros["idgrado"]=$_REQUEST["idgrado"];
			if(isset($_REQUEST["idseccion"])&&@$_REQUEST["idseccion"]!='')$filtros["idseccion"]=$_REQUEST["idseccion"];
			if(isset($_REQUEST["seccion"])&&@$_REQUEST["seccion"]!='')$filtros["seccion"]=$_REQUEST["seccion"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["alumno_ubicacion"])&&@$_REQUEST["alumno_ubicacion"]!='')$filtros["alumno_ubicacion"]=$_REQUEST["alumno_ubicacion"];
			if(isset($_REQUEST["ubicacion_A1"])&&@$_REQUEST["ubicacion_A1"]!='')$filtros["ubicacion_A1"]=$_REQUEST["ubicacion_A1"];
			if(isset($_REQUEST["ubicacion_A2"])&&@$_REQUEST["ubicacion_A2"]!='')$filtros["ubicacion_A2"]=$_REQUEST["ubicacion_A2"];
			if(isset($_REQUEST["ubicacion_B1"])&&@$_REQUEST["ubicacion_B1"]!='')$filtros["ubicacion_B1"]=$_REQUEST["ubicacion_B1"];
			if(isset($_REQUEST["ubicacion_B2"])&&@$_REQUEST["ubicacion_B2"]!='')$filtros["ubicacion_B2"]=$_REQUEST["ubicacion_B2"];
			if(isset($_REQUEST["ubicacion_C1"])&&@$_REQUEST["ubicacion_C1"]!='')$filtros["ubicacion_C1"]=$_REQUEST["ubicacion_C1"];
			if(isset($_REQUEST["entrada_prom"])&&@$_REQUEST["entrada_prom"]!='')$filtros["entrada_prom"]=$_REQUEST["entrada_prom"];
			if(isset($_REQUEST["alumno_entrada"])&&@$_REQUEST["alumno_entrada"]!='')$filtros["alumno_entrada"]=$_REQUEST["alumno_entrada"];
			if(isset($_REQUEST["salida_prom"])&&@$_REQUEST["salida_prom"]!='')$filtros["salida_prom"]=$_REQUEST["salida_prom"];
			if(isset($_REQUEST["alumno_salida"])&&@$_REQUEST["alumno_salida"]!='')$filtros["alumno_salida"]=$_REQUEST["alumno_salida"];
			if(isset($_REQUEST["examen_b1_prom"])&&@$_REQUEST["examen_b1_prom"]!='')$filtros["examen_b1_prom"]=$_REQUEST["examen_b1_prom"];
			if(isset($_REQUEST["alumno_examen_b1"])&&@$_REQUEST["alumno_examen_b1"]!='')$filtros["alumno_examen_b1"]=$_REQUEST["alumno_examen_b1"];
			if(isset($_REQUEST["examen_b2_prom"])&&@$_REQUEST["examen_b2_prom"]!='')$filtros["examen_b2_prom"]=$_REQUEST["examen_b2_prom"];
			if(isset($_REQUEST["alumno_examen_b2"])&&@$_REQUEST["alumno_examen_b2"]!='')$filtros["alumno_examen_b2"]=$_REQUEST["alumno_examen_b2"];
			if(isset($_REQUEST["examen_b3_prom"])&&@$_REQUEST["examen_b3_prom"]!='')$filtros["examen_b3_prom"]=$_REQUEST["examen_b3_prom"];
			if(isset($_REQUEST["alumno_examen_b3"])&&@$_REQUEST["alumno_examen_b3"]!='')$filtros["alumno_examen_b3"]=$_REQUEST["alumno_examen_b3"];
			if(isset($_REQUEST["examen_b4_prom"])&&@$_REQUEST["examen_b4_prom"]!='')$filtros["examen_b4_prom"]=$_REQUEST["examen_b4_prom"];
			if(isset($_REQUEST["alumno_examen_b4"])&&@$_REQUEST["alumno_examen_b4"]!='')$filtros["alumno_examen_b4"]=$_REQUEST["alumno_examen_b4"];
			if(isset($_REQUEST["examen_t1_prom"])&&@$_REQUEST["examen_t1_prom"]!='')$filtros["examen_t1_prom"]=$_REQUEST["examen_t1_prom"];
			if(isset($_REQUEST["alumno_examen_t1"])&&@$_REQUEST["alumno_examen_t1"]!='')$filtros["alumno_examen_t1"]=$_REQUEST["alumno_examen_t1"];
			if(isset($_REQUEST["examen_t2_prom"])&&@$_REQUEST["examen_t2_prom"]!='')$filtros["examen_t2_prom"]=$_REQUEST["examen_t2_prom"];
			if(isset($_REQUEST["alumno_examen_t2"])&&@$_REQUEST["alumno_examen_t2"]!='')$filtros["alumno_examen_t2"]=$_REQUEST["alumno_examen_t2"];
			if(isset($_REQUEST["examen_t3_prom"])&&@$_REQUEST["examen_t3_prom"]!='')$filtros["examen_t3_prom"]=$_REQUEST["examen_t3_prom"];
			if(isset($_REQUEST["alumno_examen_t3"])&&@$_REQUEST["alumno_examen_t3"]!='')$filtros["alumno_examen_t3"]=$_REQUEST["alumno_examen_t3"];
			if(isset($_REQUEST["tiempopv"])&&@$_REQUEST["tiempopv"]!='')$filtros["tiempopv"]=$_REQUEST["tiempopv"];
			if(isset($_REQUEST["tiempo_exam"])&&@$_REQUEST["tiempo_exam"]!='')$filtros["tiempo_exam"]=$_REQUEST["tiempo_exam"];
			if(isset($_REQUEST["tiempo_task"])&&@$_REQUEST["tiempo_task"]!='')$filtros["tiempo_task"]=$_REQUEST["tiempo_task"];
			if(isset($_REQUEST["tiempo_smartbook"])&&@$_REQUEST["tiempo_smartbook"]!='')$filtros["tiempo_smartbook"]=$_REQUEST["tiempo_smartbook"];
			if(isset($_REQUEST["tiempo_practice"])&&@$_REQUEST["tiempo_practice"]!='')$filtros["tiempo_practice"]=$_REQUEST["tiempo_practice"];
			if(isset($_REQUEST["prog_A1"])&&@$_REQUEST["prog_A1"]!='')$filtros["prog_A1"]=$_REQUEST["prog_A1"];
			if(isset($_REQUEST["prog_A2"])&&@$_REQUEST["prog_A2"]!='')$filtros["prog_A2"]=$_REQUEST["prog_A2"];
			if(isset($_REQUEST["prog_B1"])&&@$_REQUEST["prog_B1"]!='')$filtros["prog_B1"]=$_REQUEST["prog_B1"];
			if(isset($_REQUEST["prog_B2"])&&@$_REQUEST["prog_B2"]!='')$filtros["prog_B2"]=$_REQUEST["prog_B2"];
			if(isset($_REQUEST["prog_C1"])&&@$_REQUEST["prog_C1"]!='')$filtros["prog_C1"]=$_REQUEST["prog_C1"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegResumen_seccion->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id)) {
				$this->oNegResumen_seccion->id = $id;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegResumen_seccion->id_ubigeo=@$id_ubigeo;
				$this->oNegResumen_seccion->iddre=@$iddre;
				$this->oNegResumen_seccion->idugel=@$idugel;
				$this->oNegResumen_seccion->idlocal=@$idlocal;
				$this->oNegResumen_seccion->idgrado=@$idgrado;
				$this->oNegResumen_seccion->idseccion=@$idseccion;
				$this->oNegResumen_seccion->seccion=@$seccion;
				$this->oNegResumen_seccion->tipo=@$tipo;
				$this->oNegResumen_seccion->alumno_ubicacion=@$alumno_ubicacion;
				$this->oNegResumen_seccion->ubicacion_A1=@$ubicacion_A1;
				$this->oNegResumen_seccion->ubicacion_A2=@$ubicacion_A2;
				$this->oNegResumen_seccion->ubicacion_B1=@$ubicacion_B1;
				$this->oNegResumen_seccion->ubicacion_B2=@$ubicacion_B2;
				$this->oNegResumen_seccion->ubicacion_C1=@$ubicacion_C1;
				$this->oNegResumen_seccion->entrada_prom=@$entrada_prom;
				$this->oNegResumen_seccion->alumno_entrada=@$alumno_entrada;
				$this->oNegResumen_seccion->salida_prom=@$salida_prom;
				$this->oNegResumen_seccion->alumno_salida=@$alumno_salida;
				$this->oNegResumen_seccion->examen_b1_prom=@$examen_b1_prom;
				$this->oNegResumen_seccion->alumno_examen_b1=@$alumno_examen_b1;
				$this->oNegResumen_seccion->examen_b2_prom=@$examen_b2_prom;
				$this->oNegResumen_seccion->alumno_examen_b2=@$alumno_examen_b2;
				$this->oNegResumen_seccion->examen_b3_prom=@$examen_b3_prom;
				$this->oNegResumen_seccion->alumno_examen_b3=@$alumno_examen_b3;
				$this->oNegResumen_seccion->examen_b4_prom=@$examen_b4_prom;
				$this->oNegResumen_seccion->alumno_examen_b4=@$alumno_examen_b4;
				$this->oNegResumen_seccion->examen_t1_prom=@$examen_t1_prom;
				$this->oNegResumen_seccion->alumno_examen_t1=@$alumno_examen_t1;
				$this->oNegResumen_seccion->examen_t2_prom=@$examen_t2_prom;
				$this->oNegResumen_seccion->alumno_examen_t2=@$alumno_examen_t2;
				$this->oNegResumen_seccion->examen_t3_prom=@$examen_t3_prom;
				$this->oNegResumen_seccion->alumno_examen_t3=@$alumno_examen_t3;
				$this->oNegResumen_seccion->tiempopv=@$tiempopv;
				$this->oNegResumen_seccion->tiempo_exam=@$tiempo_exam;
				$this->oNegResumen_seccion->tiempo_task=@$tiempo_task;
				$this->oNegResumen_seccion->tiempo_smartbook=@$tiempo_smartbook;
				$this->oNegResumen_seccion->tiempo_practice=@$tiempo_practice;
				$this->oNegResumen_seccion->prog_A1=@$prog_A1;
				$this->oNegResumen_seccion->prog_A2=@$prog_A2;
				$this->oNegResumen_seccion->prog_B1=@$prog_B1;
				$this->oNegResumen_seccion->prog_B2=@$prog_B2;
				$this->oNegResumen_seccion->prog_C1=@$prog_C1;
				
            if($accion=='_add') {
            	$res=$this->oNegResumen_seccion->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Resumen_seccion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegResumen_seccion->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Resumen_seccion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegResumen_seccion->__set('id', $_REQUEST['id']);
			$res=$this->oNegResumen_seccion->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegResumen_seccion->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}