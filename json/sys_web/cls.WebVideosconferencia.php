<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020 
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
// JrCargador::clase('sys_negocio::NegVideosconferencia', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegZoom', RUTA_BASE);
JrCargador::clase('sys_negocio::NegZoomalumnos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);

class WebVideosconferencia extends JrWeb
{
	private $oZoom;
	private $userID;
	private $tocken;
	private $usuarioAct;
	private $oNegZoom;
	private $oNegZoomalumnos;
	private $oNegMatricula;

	protected $oNegGrupoaulaDetalle;
		
	public function __construct()
	{
		$this->oNegZoom = new NegZoom;
		$this->oNegZoomalumnos= new NegZoomalumnos();		
		$this->oNegMatricula = new NegAcad_matricula();

		$this->usuario = NegSesion::getUsuario();

		$this->userID='juanpablo.venegas@eduktvirtual.com';//'50947009';
		$this->curuserID=$this->usuario["email"];
		$this->tocken='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IjktclB0bkNQUk9pVklWNGctWHZWYVEiLCJleHAiOjE2NzI1NDkxNDAsImlhdCI6MTU3MDE5OTMxOX0.2UIgzmnS8DIcNhbe21IZ9eLJFT_pgMTiBXV_MBPm-dw';

		parent::__construct();
	}
	private function crearMetteng($dat=array()){
		$dt=!empty($dat["start_time"])?$dat["start_time"]:date('Y-m-d')." T ".date("H:i");
		$pw=date('his')."@se";
		$xy=array('url'=>'users/'.$this->curuserID."/meetings/",
			'metodo'=>'POST',
			'datos'=>array(
				  "topic"=>!empty($dat["titulo"])?$dat["titulo"]:"Reunion de Demo API", // tema de la reunion
				  "type"=>!empty($dat["tipo"])?$dat["tipo"]:2, // 1 reunion instantanea, 2 programar reunion , 3 periodica sin horario fijo ,8 con horario fijo
				  "start_time"=>$dt, //"YYYY-mm-dd T HH:ii:ss"
				  "duration"=>!empty($dat["duration"])?$dat["duration"]:60, // duracion en minutos
				  "timezone"=>!empty($dat["timezone"])?$dat["timezone"]:"America/Lima",
				  "password"=>$pw,
				  "agenda"=>!empty($dat["agenda"])?$dat["agenda"]:"agenda de reunion API ZOOM", // descripcion de la reunion
				  //"recurrence"=>array(),
				  "recurrence"=>array(
				    "type"=>1, //1 diario,2 semanalmente 3 mensual
				    "repeat_interval"=>0, //0-90 dias maximo 3 meses
				    "weekly_days"=>1, // 1 domingo ,2 lunes ...
				    "monthly_day"=>1, // dia el mes 1-31
				    "monthly_week"=>1, // -1 ultima semana , 1 al 4 semana 1 al 4
				    "monthly_week_day"=>1, // dias del mes 1 domingo, 2 lunes , etc... - 7 
				    "end_times"=>1,// 1- 50 cuantas veces se va repetir la reunion
				    "end_date_time"=>$dt // fecha en la reunion se pedira antes de que se cancele
				  ),
				  "settings"=>array(
				    "host_video"=>true, // iniciar video del host
				    "participant_video"=>false, // activar video cuando se une participante
				    "cn_meeting"=>false, // bienvenida en china
				    "in_meeting"=>false, // hostmining en india
				    "join_before_host"=>true, // permitir que los participantes se unan antes que el host
				    "mute_upon_entry"=>true, // apagar micho de participantes al ingresar host
				    "watermark"=>false,
				    "use_pmi"=>true,// usar link personal
				    "approval_type"=>2, //0 aprobar automaticamente, 1 aprobar , 2 no se requiere registro
				    "registration_type"=>1, //123 
				    "audio"=>"both",
				    "auto_recording"=>"none", //"local,cloud,none"
				    "enforce_login"=>false,
				    "enforce_login_domains"=>false,
				    "alternative_hosts"=>!empty($dat["invitados"])?$dat["invitados"]:"", // correos de invitados separados por ,
				    "global_dial_in_countries"=>!empty($dat["paises"])?$dat["paises"]:[]
				  )
				)
		);
		$x=NegZoom::requestZoom($this->tocken,$xy);
			
		return array('url'=>@$x["start_url"],'url_invitados'=>@$x["join_url"],'uuid'=>@$x["uuid"],"more"=>json_encode($x));
	}
	private function crearModificar($dat=array()){
		$dt=!empty($dat["start_time"])?$dat["start_time"]:date('Y-m-d')." T ".date("H:i:00");

		$pw=date('his')."@se";
		$xy=array('url'=>'meetings/'.$dat["meetingId"]."/?occurrence_id=0",	
			'metodo'=>'PATCH',		
			'datos'=>array(
				  "topic"=>!empty($dat["titulo"])?$dat["titulo"]:"Reunion de Demo API", // tema de la reunion
				  "type"=>!empty($dat["tipo"])?$dat["tipo"]:2, // 1 reunion instantanea, 2 programar reunion , 3 periodica sin horario fijo ,8 onc horario fijo
				  "start_time"=>$dt, //"YYYY-mm-dd T HH:ii:ss"
				  "duration"=>!empty($dat["duration"])?$dat["duration"]:60, // duracion en minutos
				  "timezone"=>!empty($dat["timezone"])?$dat["timezone"]:"America/Lima",
				  "password"=>$pw,
				  "agenda"=>!empty($dat["agenda"])?$dat["agenda"]:"agenda de reunion API ZOOM", // descripcion de la reunion
				  //"recurrence"=>array(),				  
				  "settings"=>array(
				    "host_video"=>true, // iniciar video del host
				    "participant_video"=>false, // activar video cuando se une participante
				    "cn_meeting"=>false, // bienvenida en china
				    "in_meeting"=>false, // hostmining en india
				    "join_before_host"=>true, // permitir que los participantes se unan antes que el host
				    "mute_upon_entry"=>true, // apagar micho de participantes al ingresar host
				    "watermark"=>false,
				    "use_pmi"=>false,
				    "approval_type"=>2, //0 aprobar automaticamente, 1 aprobar , 2 nose requiere registro
				    "registration_type"=>1, //123 
				    "audio"=>"both",
				    "auto_recording"=>"none", //"local,cloud,none"
				    "enforce_login"=>false,
				    "enforce_login_domains"=>false,
				    "alternative_hosts"=>!empty($dat["invitados"])?$dat["invitados"]:"", // correos de invitados separados por ,
				    "global_dial_in_countries"=>!empty($dat["paises"])?$dat["paises"]:[]
				  )
				)
		);
		$x=NegZoom::requestZoom($this->tocken,$xy);
		return array('url'=>@$x["start_url"],'url_invitados'=>@$x["join_url"],'uuid'=>@$x["uuid"],'more'=>json_encode($x));
	}

	private function crearusuario($dt=array()){		
		$xy=array( //crear usuario
			'metodo'=>'POST',
			'datos'=>array(
				'action'=>'create',
				"user_info"=>array(
					"email"=>!empty($dt["email"])?$dt["email"]:$user["email"],
					'type'=>!empty($dt["tipo"])?$dt["tipo"]:1,// 1 basico 2 pro 3 corp
					'first_name'=>!empty($dt["nombre"])?$dt["nombre"]:'anonimo1',
					'last_name'=>!empty($dt["apellido"])?$dt["apellido"]:'ape anonimo2',
					'password'=>!empty($dt["clave"])?$dt["clave"]:'demo123'/**/
				)
			)
		);
		return NegZoom::requestZoom($this->tocken,$xy);
	}

	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;	

			$idrol = $this->usuario['idrol'];
			$rol = $this->usuario["rol"];

			if($idrol == 1 || $idrol == 2 || $idrol == 3){
				$perms = true;
			}else{
				return $aplicacion->error(JrTexto::_("No tiene Permisos"));
			}
			if(isset($_REQUEST["sql"])&&@$_REQUEST["sql"]!='')$filtros["sql"]=$_REQUEST["sql"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idzoom"])&&@$_REQUEST["idzoom"]!='')$filtros["idzoom"]=$_REQUEST["idzoom"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["urlmoderador"])&&@$_REQUEST["urlmoderador"]!='')$filtros["urlmoderador"]=$_REQUEST["urlmoderador"];
			if(isset($_REQUEST["urlparticipantes"])&&@$_REQUEST["urlparticipantes"]!='')$filtros["urlparticipantes"]=$_REQUEST["urlparticipantes"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["agenda"])&&@$_REQUEST["agenda"]!='')$filtros["agenda"]=$_REQUEST["agenda"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["uuid"])&&@$_REQUEST["uuid"]!='')$filtros["uuid"]=$_REQUEST["uuid"];
			if(isset($_REQUEST["duracion"])&&@$_REQUEST["duracion"]!='')$filtros["duracion"]=$_REQUEST["duracion"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$this->datos=$this->oNegZoom->buscar($filtros);
			
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}
	
	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegZoom->setCampo($_REQUEST['idzoom'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegZoom->__set('idzoom', $_REQUEST['idzoom']);
			$res=$this->oNegZoom->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}
	public function setInvitar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos 1'));
				exit(0);
			}
			@extract($_POST);
			
			$datosalumno=$this->oNegZoomalumnos->buscar(array('idzoom'=>$idzoom,'idalumno'=>$idalumno));
			if($invitar==0){				
				if(!empty($datosalumno[0])){
					$hayalumno=$datosalumno[0];
					$this->oNegZoomalumnos->idzoomalumno=$hayalumno["idzoomalumno"];
					$this->oNegZoomalumnos->eliminar();
					echo json_encode(array('code'=>200,'msj'=>'El alumno ya no esta invitado'));
					exit(0);
				}else{
					echo json_encode(array('code'=>200,'msj'=>'El alumno no esta registrado ya como invitado'));
					exit(0);
				}
			}else{				
				$this->oNegZoomalumnos->idzoom=$idzoom;
				$this->oNegZoomalumnos->idalumno=$idalumno;
				$res=$this->oNegZoomalumnos->agregar();					
				echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Zoom')).' '.JrTexto::_('El alumno ha sido invitado')));			
				exit(0);
			}			
			//$this->oNegZoom->setCampo($_REQUEST['idzoom'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Error al invitar estudiante'));
			exit(0);
		}
	}
	public function verifyuser(){
		try{
			$usuario = !empty($_REQUEST['usuario']) ? $_REQUEST['usuario'] : $this->usuario;
			$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : null;

			if(empty($email)){
				throw new Exception("No se ha especificado un correo electrónico");
			}

           	$this->curuserID = !empty($email) ? $email : $usuario['email'];
			$rst=NegZoom::requestZoom($this->tocken,array('url'=>'users/'.$this->curuserID));
			if(empty($rst["id"])){ // ususario no esta registrado
				$nom =explode(",",$usuario["nombre_full"]);
				$nomuser=@$nom[1];
				$apeuser=@$nom[0];
				$userclave='user'.$usuario["idpersona"]."@".date('Y');
				$this->datos= array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
				if($rst["code"]==1010||$rst["code"]==1001){ // usuario no existe
					$this->datos=array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
					$rst=$this->crearusuario($this->datos);
					throw new Exception("El usuario fue creado en zoom con el email: ".$email." clave: ".$userclave.". Por favor verifique su cuenta para crear la sala correctamente.");
				}
			}
			if(empty($rst['id'])){
				throw new Exception("El usuario no se pudo crear en zoom", 1);
			}
			if($rst['status'] == 'pending'){
				throw new Exception("La invitación de creación de cuenta de zoom aun esta pendiente, por favor verifique su correo electrónico y acepte la invitación.", 1);
			}
			echo json_encode(array('code'=>200,'msj'=>'Usuario valido'));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','msj'=> $e->getMessage()));
			exit(0);
		}
	}
	public function crearsala(){
		try{
			//recibir parametros
			$idpersona = !empty($_REQUEST['idpersona']) ? $_REQUEST['idpersona'] : null;
			$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : null;
			$fecha = !empty($_REQUEST['fecha']) ? $_REQUEST['fecha'] : null;
			$time = !empty($_REQUEST['time']) ? $_REQUEST['time'] : date('H:i');
			$titulo = !empty($_REQUEST['titulo']) ? $_REQUEST['titulo'] : null;
			$agenda = !empty($_REQUEST['agenda']) ? $_REQUEST['agenda'] : null;
			$duracion = !empty($_REQUEST['duracion']) ? $_REQUEST['duracion'] : 60;
			$imagen = !empty($_FILES['imagen']) ? $_FILES['imagen'] : '';
			$usuario = !empty($_REQUEST['usuario']) ? $_REQUEST['usuario'] : $this->usuario;
			$idcursodetalle = !empty($_REQUEST['idcursodetalle']) ? $_REQUEST['idcursodetalle'] : 0;
			$idpestania = !empty($_REQUEST['idpestania']) ? $_REQUEST['idpestania'] : 0;
			$idcurso = !empty($_REQUEST['idcurso']) ? $_REQUEST['idcurso'] : 0;
			$idcomplementario = !empty($_REQUEST['idcomplementario']) ? $_REQUEST['idcomplementario'] : 0;
			$idgrupoauladetalle = !empty($_REQUEST['idgrupoauladetalle']) ? $_REQUEST['idgrupoauladetalle'] : null;
			//fix array
			if(!empty($idgrupoauladetalle) && !is_array($idgrupoauladetalle)){
				$idgrupoauladetalle = explode(',', $idgrupoauladetalle);
			}
			/*-------*/
			//validar 
			if(empty($idpersona) || empty($fecha) || empty($titulo)){
				throw new Exception("Faltan recibir algunos datos al sistema", 1);
			}
			//logica principal
			/* Upload file */
			if(!empty($imagen)){
				$filename = $idpersona.date('Ymd_His').".".basename($imagen['type']);
				$location = RUTA_BASE.SD."static".SD."media".SD."imagenzoom".SD.$filename;
				if(move_uploaded_file($imagen['tmp_name'],$location)){
					$imagen = "/media/imagenzoom/".$filename;
				}else{
					$imagen = '';
				}
			}
			
			$_fecha = "{$fecha} T {$time}";
			//---verificar si la cuenta tiene usuario creado en zoom
           	$this->curuserID = !empty($email) ? $email : $usuario['email'];
			
			$rst=NegZoom::requestZoom($this->tocken,array('url'=>'users/'.$this->curuserID));
			$nom =explode(",",$usuario["nombre_full"]);
			$nomuser=@$nom[1];
			$apeuser=@$nom[0];
			$userclave='user'.$usuario["idpersona"]."@".date('Y');
			if(!empty($rst["id"])){ // ususario ya esta registrado
				$this->datos= array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
			}else{
				if($rst["code"]==1010||$rst["code"]==1001){ // usuario no existe
					$this->datos=array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
					$rst=$this->crearusuario($this->datos);				
				}
			}// endif !empty $rst
           	$dt=array('titulo'=>$titulo,'agenda'=>$agenda,'start_time'=>$_fecha,'duration'=>$duracion);
           	
			$rs=$this->crearMetteng($dt);
			if(empty($rs)){
				throw new Exception("Error Processing Zoom Request", 1);
			}
			//registrar la sala en latabla zoom
			$this->oNegZoom->idpersona=$idpersona;
			$this->oNegZoom->fecha="{$fecha} {$time}";
			$this->oNegZoom->urlmoderador=@$rs['url'];//@$urlmoderador;
			$this->oNegZoom->urlparticipantes=@$rs['url_invitados'];//@$urlparticipantes;
			$this->oNegZoom->titulo=@$titulo;
			$this->oNegZoom->agenda=@$agenda;
			$this->oNegZoom->uuid=@$rs["more"];
			$this->oNegZoom->estado=1;
			$this->oNegZoom->duracion=@$duracion;
			$this->oNegZoom->email=@$email;
			$this->oNegZoom->imagen=@$imagen;
			$this->oNegZoom->idcursodetalle=$idcursodetalle;
			$this->oNegZoom->idcurso=$idcurso;
			$this->oNegZoom->idpestania=$idpestania;
			$this->oNegZoom->idcomplementario=$idcomplementario;
			$idzoom = $this->oNegZoom->agregar();
			if(empty($idzoom)){
				throw new Exception("No se registro la sala zoom correctamente", 1);
			}
			if(!empty($idgrupoauladetalle)){
				//registrar alumnos segun los grupos 
				foreach ($idgrupoauladetalle as $key => $value) {
					//buscar estudiantes
					$rs_mat = $this->oNegMatricula->buscar(array('idgrupoauladetalle'=>$value,'estado'=>1));
					if(!empty($rs_mat)){
						//registrar en zoomalumno
						foreach ($rs_mat as $km => $vm) {
							$this->oNegZoomalumnos->idzoom = $idzoom;
							$this->oNegZoomalumnos->idalumno = $vm['idalumno'];
							$rs_zoomalumno = $this->oNegZoomalumnos->agregar();
						}
					}
				}//endforeach
			}
			$dat = $this->oNegZoom->buscar(array('idzoom'=>$idzoom));
			//retornar mensaje
			echo json_encode(array('code'=>200,'msj'=>'Sala creada','data'=>$dat));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit(0);	
		}
	}
	public function editarsala(){
		try{
			$idzoom = !empty($_REQUEST['idzoom']) ? $_REQUEST['idzoom'] : null;
			$idpersona = !empty($_REQUEST['idpersona']) ? $_REQUEST['idpersona'] : null;
			$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : null;
			$fecha = !empty($_REQUEST['fecha']) ? $_REQUEST['fecha'] : null;
			$time = !empty($_REQUEST['time']) ? $_REQUEST['time'] : date('H:i');
			$titulo = !empty($_REQUEST['titulo']) ? $_REQUEST['titulo'] : null;
			$agenda = !empty($_REQUEST['agenda']) ? $_REQUEST['agenda'] : null;
			$duracion = !empty($_REQUEST['duracion']) ? $_REQUEST['duracion'] : 60;
			$imagen = !empty($_REQUEST['imagen']) ? $_REQUEST['imagen'] : null;
			$usuario = !empty($_REQUEST['usuario']) ? $_REQUEST['usuario'] : $this->usuario;

			if(empty($fecha) || empty($titulo) || empty($idzoom)){
				throw new Exception("Faltan recibir algunos datos al sistema", 1);
			}

			$_fecha = "{$fecha} T {$time}";

			//---verificar si la cuenta tiene usuario creado en zoom
           	$this->curuserID = !empty($email) ? $email : $usuario['email'];

			$rst=NegZoom::requestZoom($this->tocken,array('url'=>'users/'.$this->curuserID));
			$nom =explode(",",$usuario["nombre_full"]);
			$nomuser=@$nom[1];
			$apeuser=@$nom[0];
			$userclave='user'.$usuario["idpersona"]."@".date('Y');
			if(!empty($rst["id"])){ // ususario ya esta registrado
				$this->datos= array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
			}else{
				if($rst["code"]==1010||$rst["code"]==1001){ // usuario no existe
					$this->datos=array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
					$rst=$this->crearusuario($this->datos);				
				}
			}// endif !empty $rst
           	$dt=array('titulo'=>$titulo,'agenda'=>$agenda,'start_time'=>$_fecha,'duration'=>$duracion);
			$dt["meetingId"]=726863468;
			$rs=$this->crearModificar($dt);

			$this->oNegZoom->idzoom = $idzoom;
			$this->oNegZoom->idpersona=$idpersona;
			$this->oNegZoom->fecha="{$fecha} {$time}";
			$this->oNegZoom->urlmoderador=@$rs['url'];//@$urlmoderador;
			$this->oNegZoom->urlparticipantes=@$rs['url_invitados'];//@$urlparticipantes;
			$this->oNegZoom->titulo=@$titulo;
			$this->oNegZoom->agenda=@$agenda;
			$this->oNegZoom->uuid=@$rs["more"];
			$this->oNegZoom->estado=1;
			$this->oNegZoom->duracion=@$duracion;
			$this->oNegZoom->email=@$email;
			/* Upload file */
			if(!empty($imagen)){
				$filename = $idpersona.date('Ymd_His').".".basename($imagen['type']);
				$location = RUTA_BASE.SD."static".SD."media".SD."imagenzoom".SD.$filename;
				if(move_uploaded_file($imagen['tmp_name'],$location)){
					$imagen = "/media/imagenzoom/".$filename;
				}else{
					$imagen = '';
				}
				$this->oNegZoom->imagen=@$imagen;

			}

            $res=$this->oNegZoom->editar();

            if(empty($res)){
            	throw new Exception("No se actualizo el registro en el sistema", 1);
            }
            $dat = $this->oNegZoom->buscar(array('idzoom'=>$idzoom));

			echo json_encode(array('code'=>200,'msj'=>'Sala modificada correctamente','data'=>$dat));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit(0);
		}
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            $usuarioAct = NegSesion::getUsuario();
            $fecha1=!empty($fecha)?($fecha." T ".$time):date('Y-m-d T H:i');
            $duracion=!empty($duracion)?$duracion:60;
            $fecha2=!empty($fecha)?($fecha." ".@$time):date('Y-m-d H:i');
           	$dt=array('titulo'=>$titulo,'agenda'=>$agenda,'start_time'=>$fecha1,'duration'=>$duracion);
            if(!empty(@$idzoom)){
				$this->oNegZoom->idzoom = $idzoom;
				$accion='_edit';
				$dt["meetingId"]=726863468;
				$rs=$this->crearModificar($dt);
			}else{
				$rs=$this->crearMetteng($dt);	
			}
			$this->oNegZoom->idpersona=$usuarioAct["idpersona"];
			$this->oNegZoom->fecha=$fecha2;
			$this->oNegZoom->urlmoderador=@$rs['url'];//@$urlmoderador;
			$this->oNegZoom->urlparticipantes=@$rs['url_invitados'];//@$urlparticipantes;
			$this->oNegZoom->titulo=@$titulo;
			$this->oNegZoom->agenda=@$agenda;
			$this->oNegZoom->uuid=@$rs["more"];
			$this->oNegZoom->estado=1;
			$this->oNegZoom->duracion=@$duracion;
				
            if($accion=='_add') {
            	$res=$this->oNegZoom->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Zoom')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegZoom->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Zoom')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }         			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

}