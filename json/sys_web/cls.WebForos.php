<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020 
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegForos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
class WebForos extends JrWeb
{
	private $oNegForos;
	protected $oNegPersonal;
	protected $oNegProyecto;

	public function __construct()
	{
		parent::__construct();
		$this->oNegForos = new NegForos;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegProyecto = new NegProyecto;
	}

	public function defecto()
	{
		return $this->listado();
	}
	public function guardarImagen()
	{
		$serverPath = 'static/foro-images/';
		$imageFolder = RUTA_BASE . $serverPath;

		reset($_FILES);
		$temp = current($_FILES);
		if (is_uploaded_file($temp['tmp_name'])) {
			if (isset($_SERVER['HTTP_ORIGIN'])) {
				header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
			}
			if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
				header("HTTP/1.1 400 Invalid file name.");
				return;
			}
			if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
				header("HTTP/1.1 400 Invalid extension.");
				return;
			}
			$filetowrite = $imageFolder . $temp['name'];
			move_uploaded_file($temp['tmp_name'], $filetowrite);
			$responsePath = $serverPath . $temp['name'];
			echo json_encode(array('location' => $responsePath));
		} else {
			header("HTTP/1.1 500 Server Error");
		}
	}

	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Foros', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			$usuarioAct = NegSesion::getUsuario();
			if (isset($_REQUEST["otrosdatos"]) && @$_REQUEST["otrosdatos"] != '') $filtros["otrosdatos"] = $_REQUEST["otrosdatos"];

			if (isset($_REQUEST["busqueda"]) && @$_REQUEST["busqueda"] != '') $filtros["busqueda"] = $_REQUEST["busqueda"];
			$filtros["idproyecto"] = $usuarioAct["idproyecto"];
			$this->datos = $this->oNegForos->listar($filtros);

			echo json_encode(array('code' => 200, 'data' => $this->datos));

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function getForoAula()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;

			$filtros = array();
			$filtros["idPersona"] = $_REQUEST["idPersona"];
			$filtros["idComplentario"] = $_REQUEST["idComplentario"];
			$filtros["idCurso"] = $_REQUEST["idCurso"];

			$this->datos = $this->oNegForos->getForoAula($filtros);

			echo json_encode(array('code' => 200, 'data' => $this->datos));

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$publicacion = json_decode(file_get_contents("php://input"), true);
			if (json_last_error() != 0) { //hubo error al decodificar el json
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
			}
			// if (empty($_POST)) {
			// 	echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
			// 	exit(0);
			// }
			// @extract($_POST);
			$accion = '_add';
			// if (!empty(@$idforo)) {
			// 	$this->oNegForos->idforo = $idforo;
			// 	$accion = '_edit';
			// }
			if (isset($_REQUEST["idpublicacion"]) && @$_REQUEST["idpublicacion"] != '') $filtros["idpublicacion"] = $_REQUEST["idpublicacion"];
			if (isset($filtros["idpublicacion"])) {
				$this->oNegForos->idpublicacion = $filtros["idpublicacion"];
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();

			$this->oNegForos->titulo = $publicacion['titulo'];
			$this->oNegForos->subtitulo = $publicacion['subtitulo'];
			$this->oNegForos->estado = $publicacion['estado'];
			$this->oNegForos->contenido = htmlspecialchars($publicacion['contenido']);
			$this->oNegForos->idpersona = $usuarioAct['idpersona'];
			$this->oNegForos->idgruauladet = $publicacion['idgruauladet'];
			$this->oNegForos->idproyecto = $usuarioAct["idproyecto"];

			// $infoEmpresa = $this->oNegProyecto->buscar(array('idproyecto' => $usuarioAct["idproyecto"]));
			// if(false){
			// if (!empty($infoEmpresa[0])) {
			// 	$this->empresa = $infoEmpresa[0];
			// 	$this->empresa["nombre"] = !empty($this->empresa["nombre"]) ? $this->empresa["nombre"] : @$this->empresa["empresa"];
			// 	JrCargador::clase('jrAdwen::JrCorreo');
			// 	$oCorreo = new JrCorreo;
			// 	$deemail = !empty($this->empresa["correoempresa"]) ? $this->empresa["correoempresa"] : $usuarioAct["email"];
			// 	$denombre = !empty($this->empresa["nombre"]) ? $this->empresa["nombre"] : $usuarioAct["nombre_full"];
			// 	$oCorreo->setRemitente($deemail, $denombre);
			// 	$this->titulo = 'FORO : ' . $publicacion['titulo'];
			// 	$this->Asunto = !empty($publicacion['subtitulo']) ? $publicacion['subtitulo'] : 'Foro Publicación';
			// 	$this->mensaje = htmlspecialchars_decode($this->oNegForos->contenido);
			// 	$oCorreo->addDestinarioPhpmailer($usuarioAct["email"], $usuarioAct["nombre_full"]);
			// 	$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com', 'Abel');
			// 	if ($usuarioAct["idrol"] == 3 && !empty($usuarioAct["emailpadre"])) $oCorreo->addDestinarioPhpmailer($usuarioAct["emailpadre"], 'Padre de ' . $usuarioAct["nombre_full"]);
			// 	if (!empty($this->empresa["correoempresa"]))
			// 		$oCorreo->addDestinarioPhpmailer($this->empresa["correoempresa"], $this->empresa["nombre"]);
			// 	if (!empty($this->empresa["logoempresa"]) && is_file(RUTA_BASE . $this->empresa["logoempresa"])) {
			// 		$ipost = stripos($this->empresa["logoempresa"], '?');
			// 		if ($ipost === false) $this->logoempresa = $this->empresa["logoempresa"];
			// 		else $this->logoempresa = substr($this->empresa["logoempresa"], 0, $ipost);
			// 		$img = str_replace('\\', '/', RUTA_BASE . $logoempresa);
			// 		$oCorreo->addadjuntos(RUTA_BASE . $this->logoempresa, 'logoempresa');
			// 	}
			// 	$this->para = '';
			// 	$this->esquema = 'correos/general_empresa1';
			// 	$oCorreo->setMensaje(parent::getEsquema());
			// 	$envio = $oCorreo->sendPhpmailer();
			// }

			// var_dump("aqui pe causha", $accion);exit();
			if ($accion == '_add') {
				$res = $this->oNegForos->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Foros')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegForos->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Foros')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegForos->titulo = @$titulo;
					$this->oNegForos->texto = @$texto;
					$this->oNegForos->estado = @$estado;
					$this->oNegForos->idusuario = @$idusuario;
					$this->oNegForos->rol = @$rol;
					$this->oNegForos->idcurso = @$idcurso;
					$this->oNegForos->idgrupoaula = @$idgrupoaula;
					$this->oNegForos->idgrupoauladetalle = @$idgrupoauladetalle;
					$this->oNegForos->idforopadre = @$idforopadre;
					$this->oNegForos->tipo = @$tipo;
					$this->oNegForos->regfecha = @$regfecha;
					$this->oNegForos->otrosdatos = @$otrosdatos;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegForos->__set('idpublicacion', $_REQUEST['idpublicacion']);
			$res = $this->oNegForos->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegForos->setCampo($_REQUEST['idforo'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
