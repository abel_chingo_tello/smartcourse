<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-02-2018 
 * @copyright	Copyright (C) 09-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebMedia extends JrWeb
{			
	public function __construct()
	{
		parent::__construct();	
				
	}

	public function defecto(){
		return 'Error en media';
	}
	public function subir(){
		ini_set('post_max_size', '1024M');
		ini_set('upload_max_filesize', '1024M');
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            JrCargador::clase('sys_negocio::NegTools', RUTA_RAIZ);
			if(!empty($_FILES['media'])){
					//public static function subirFile($file,$dirmedia,$oldmedia,$typefile,$nombre){
		        $resi = NegTools::subirFile($_FILES['media'],$dirmedia,$oldmedia,$typefile,$nombre);
		        if($resi['code']==200)  echo json_encode(array('code'=>200,'msj'=>'Archivo subido correctamente','media'=>$resi["rutaStatic"]."?id=".date('YmdHis')));
		        else  echo json_encode(array('code'=>'Error','msj'=>'Error Al Subir archivo'));		      
		    }      
		    exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
  
}