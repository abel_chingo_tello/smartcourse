<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegManuales', RUTA_BASE);
class WebManuales extends JrWeb
{
	private $oNegManuales;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegManuales = new NegManuales;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Manuales', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["abreviado"])&&@$_REQUEST["abreviado"]!='')$filtros["abreviado"]=$_REQUEST["abreviado"];
			if(isset($_REQUEST["stock"])&&@$_REQUEST["stock"]!='')$filtros["stock"]=$_REQUEST["stock"];
			if(isset($_REQUEST["total"])&&@$_REQUEST["total"]!='')$filtros["total"]=$_REQUEST["total"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegManuales->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idmanual)) {
				$this->oNegManuales->idmanual = $idmanual;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegManuales->titulo=@$titulo;
				$this->oNegManuales->abreviado=@$abreviado;
				$this->oNegManuales->stock=@$stock;
				$this->oNegManuales->total=@$total;
				
            if($accion=='_add') {
            	$res=$this->oNegManuales->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Manuales')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegManuales->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Manuales')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegManuales->__set('idmanual', $_REQUEST['idmanual']);
			$res=$this->oNegManuales->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegManuales->setCampo($_REQUEST['idmanual'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}