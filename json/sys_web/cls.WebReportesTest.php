<?php
set_time_limit(0);
/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalleTest', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMinedu', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_grado', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE);
JrCargador::clase('sys_negocio::NegResumen', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_competencias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_unidad_capacidad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_archivosalumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_mensajes', RUTA_BASE);
//JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE);
class WebReportesTest extends JrWeb
{
	private $oNegAcad_grupoauladetalle;
	private $oNegAcad_grupoaula;
	private $oNegAcad_matricula;
	private $oNegAcad_cursodetalleTest;
	private $oNegNotas_quiz;
	private $oNegAcad_curso;
	private $oNegPersona_setting;
	private $oNegPersonal;
	private $oNegHistorial_sesion;
	private $oNegActividad_alumno;
	private $oNegActividad;
	private $oNegMinedu;
	private $oNegGrado;
	private $oNegSeccion;
	private $oNegUgel;
	private $oNegDre;
	private $oNegResumen;
	private $oNegCompetencias;
	private $oNegTareas;
	private $oNegMin_unidad_capacidad;
	private $Dpuntajemax = 100;
	private $Dpuntajemin = 51;
	//private $oNegGeneral;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_cursodetalleTest = new NegAcad_cursodetalleTest;
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegReportealumno = new NegReportealumno;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegPersona_setting = new NegPersona_setting;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegActividad = new NegActividad;
		$this->oNegMinedu = new NegMinedu;
		$this->oNegGrado = new NegMin_grado;
		$this->oNegSeccion = new NegMin_sesion;
		$this->oNegUgel = new NegUgel;
		$this->oNegResumen = new NegResumen;
		$this->oNegDre = new NegMin_dre;
		$this->oNegCompetencias = new NegMin_competencias;
		$this->oNegMin_unidad_capacidad = new NegMin_unidad_capacidad;
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegTareas = new NegTareas;
		$this->oNegTareas_archivosalumno = new NegTareas_archivosalumno;
		$this->oNegTareas_mensajes = new NegTareas_mensajes;
		//$this->oNegGeneral = new NegGeneral;

	}
	public function tareasProyectos()
	{
		$this->documento->plantilla = 'blanco';
		try {
			
			$filtros = array();
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			$filtros['fechaactiva'] = true;
			$user = NegSesion::getUsuario();
			// $filtros["idproyecto"]=$user["idproyecto"];
			$arrMatriculas = $this->oNegAcad_matricula->buscar($filtros); //varios cursos
			
			$idproyecto = (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') ? $_REQUEST["idproyecto"] : "T";
			$tipo = (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') ? $_REQUEST["tipo"] : "T";
			$tipo_ = ($tipo == "T") ? "estarea" : "esproyecto";
			
			$this->datos = array();
			
			foreach ($arrMatriculas as $i => $matricula) { //dentro de un curso
				$curso = array(
					"idcurso" => $matricula["idcurso"],
					"strcurso" => $matricula["strcurso"],
					"array" => array()
				);
				$tareas = array();
				$arrCursosDetalles = array();
	// echo json_encode($arrMatriculas);
	// 			exit();
				if ($matricula['tipocurso'] != '2') {
					$arrCursosDetalles = $this->oNegAcad_cursodetalleTest->getCursosDetalles(array("idcurso" => $matricula["idcurso"]));
				} else {
					$arrCursosDetalles = $this->oNegAcad_cursodetalleTest->getCursosDetalles(array("sql" => 'sql1', "idcurso" => $matricula["idcurso"]));
				}
			
				foreach ($arrCursosDetalles as $key => $cursoDetalle) { //dentro de cursosdetalles
					$cursoDetalle["txtjson"] = (array) json_decode($cursoDetalle["txtjson"], true);

					$validar = count($cursoDetalle["txtjson"]["options"]);
					
					if ($validar == 0) { //No tiene pestañas (options)
						//compara si cada cursodetalle de ese curso es tarea o proyecto:
						if (isset($cursoDetalle["txtjson"][$tipo_]) && $cursoDetalle["txtjson"][$tipo_] == "si") {
							$filtrosT = array(
								"sql" => "1",
								"idproyecto" => $idproyecto,
								"idcurso" => $matricula["idcurso"],
								"idsesion" => $cursoDetalle["idcursodetalle"],
								"idpestania" => 0,
								"tipo" => $tipo
							);
							$rsT = $this->oNegTareas->buscar($filtrosT);
							if (count($rsT) == 0) { //no hay registros en la tabla tarea
								$tareas[] = array(
									"id" => 0,
									"idcurso" => $matricula["idcurso"],
									"idsesion" => $cursoDetalle["idcursodetalle"],
									"idpesstania" => 0,
									"nombre" => ($tipo == 'T' ? "Tarea " : "Proyecto ") . $cursoDetalle["nombre_detalle"],
									"respondido" => 0,
									"fecha" => "",
									"nota" => "Sin Calificar",
								);
							} else { //Sí hay registros en la tabla tarea
								$rsT = $rsT[0];
								$tareas[] = array(
									"id" => $rsT["idtarea"],
									"idcurso" => $matricula["idcurso"],
									"idsesion" => $cursoDetalle["idcursodetalle"],
									"idpesstania" => 0,
									"nombre" => ($tipo == 'T' ? "Tarea " : "Proyecto ") . $cursoDetalle["nombre_detalle"],
									"respondido" => 1,
									"fecha" => $rsT["fecha_creacion"],
									"nota" => (empty($rsT["fecha_calificacion"])) ? "Sin Calificar" : $rsT["nota"],
									"fecha_calificacion" => empty($rsT["fecha_calificacion"]) ? "" : $rsT["fecha_calificacion"],
									"notabaseen" => $rsT["notabaseen"],
									"nombre_docente" => $rsT["nombre_docente"],
								);
							}
						}
					} else { //Sí tiene pestañas (options)
						//entramos a pestañas (options)
						foreach ($cursoDetalle["txtjson"]["options"] as $keyO => $option) {
							//compara si cada cursodetalle de ese curso es tarea o proyecto:
							if (isset($option[$tipo_]) && $option[$tipo_] == "si") {
								$filtrosT = array(
									"sql" => "1",
									"idproyecto" => $idproyecto,
									"idcurso" => $matricula["idcurso"],
									"idsesion" => $cursoDetalle["idcursodetalle"],
									"idpestania" => $cursoDetalle["idcursodetalle"] . $keyO,
									"tipo" => $tipo
								);
								$rsT = $this->oNegTareas->buscar($filtrosT);
								if (count($rsT) == 0) { //no hay registros en la tabla tarea
									$tareas[] = array(
										"id" => 0,
										"idcurso" => $matricula["idcurso"],
										"idsesion" => $cursoDetalle["idcursodetalle"],
										"idpesstania" => $cursoDetalle["idcursodetalle"] . $keyO,
										"nombre" => ($tipo == 'T' ? "Tarea " : "Proyecto ") . $option["nombre"],
										"respondido" => 0,
										"fecha" => "",
										"nota" => "Sin Calificar",
									);
								} else { //sí hay registros en la tabla tarea
									$rsT = $rsT[0];
									$tareas[] = array(
										"id" => $rsT["idtarea"],
										"idcurso" => $matricula["idcurso"],
										"idsesion" => $cursoDetalle["idcursodetalle"],
										"idpesstania" => $cursoDetalle["idcursodetalle"] . $keyO,
										"nombre" => ($tipo == 'T' ? "Tarea " : "Proyecto ") . $option["nombre"],
										"respondido" => 1,
										"fecha" => $rsT["fecha_creacion"],
										"nota" => (empty($rsT["fecha_calificacion"])) ? "Sin Calificar" : $rsT["nota"],
										"fecha_calificacion" => empty($rsT["fecha_calificacion"]) ? "" : $rsT["fecha_calificacion"],
										"notabaseen" => $rsT["notabaseen"],
										"nombre_docente" => $rsT["nombre_docente"],
									);
								}
							}
						}
					}
					
					$arrCursosDetalles[$key] = $cursoDetalle;
				}

				$curso["array"] = $tareas;
				$this->datos[] = $curso;
				// $this->datos[] = $cursoDetalle;
				// $this->datos = $arrMatriculas;
			}

			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			print_r($e);
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function notasfinales()
	{
		$user = NegSesion::getUsuario();

		$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
		$idalumno = !empty($_REQUEST["idalumno"]) ? $_REQUEST["idalumno"] : (($idrol == 3) ? $user["idpersona"] : '');

		$iddocente = !empty($_REQUEST["iddocente"]) ? $_REQUEST["iddocente"] : (($idrol == 2) ? $user["idpersona"] : '');
		$idproyecto = !empty($_REQUEST["idproyecto"]) ? $_REQUEST["idproyecto"] : $user["idproyecto"];

		$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : '';
		$esarrayidalumno = stripos($idalumno, ",");
		if ($esarrayidalumno) $idalumno = explode(',', $idalumno);
		$idcursos = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
		$esarraycursos = stripos($idcursos, ",");
		if ($esarraycursos) $idcursos = explode(',', $idcursos);

		if ($idrol == 3) $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idcurso' => $idcursos, 'idalumno' => $idalumno, 'orderby' => 'strcurso', 'idproyecto' => $idproyecto));
		else if ($idrol == 2) $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idcurso' => $idcursos, 'idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $iddocente, 'orderby' => 'strcurso', 'idproyecto' => $idproyecto));
		else $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idcurso' => $idcursos, 'idgrupoauladetalle' => $idgrupoauladetalle, 'orderby' => 'strcurso', 'idproyecto' => $idproyecto));
		echo json_encode(array('code' => 200, 'data' => $mismatriculas));
		exit(0);
		//$hayexamenescurso=array();
		//$datosreturn=array();
		//var_dump($mismatriculas[0]);
	}

	public function notasfinaleshabilidades()
	{
		$user = NegSesion::getUsuario();
		$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
		$idalumno = !empty($_REQUEST["idalumno"]) ? $_REQUEST["idalumno"] : ($idrol == 3) ? $user["idpersona"] : '';
		$iddocente = !empty($_REQUEST["iddocente"]) ? $_REQUEST["iddocente"] : ($idrol == 2) ? $user["idpersona"] : '';
		$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : '';
		$esarrayidalumno = stripos($idalumno, ",");
		if ($esarrayidalumno) $idalumno = explode(',', $idalumno);
		$idcursos = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
		$esarraycursos = stripos($idcursos, ",");
		if ($esarraycursos) $idcursos = explode(',', $idcursos);

		if ($idrol == 3) $mismatriculas = $this->oNegAcad_matricula->notas_habilidades(array('idcurso' => $idcursos, 'idalumno' => $idalumno, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else if ($idrol == 2) $mismatriculas = $this->oNegAcad_matricula->notas_habilidades(array('idcurso' => $idcursos, 'idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $iddocente, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else $mismatriculas = $this->oNegAcad_matricula->notas_habilidades(array('idcurso' => $idcursos, 'idgrupoauladetalle' => $idgrupoauladetalle, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		echo json_encode(array('code' => 200, 'data' => $mismatriculas, 'total' => count($mismatriculas)));
		exit(0);
	}

	public function notas()
	{
		$user = NegSesion::getUsuario();
		$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
		$idalumno = !empty($_REQUEST["idalumno"]) ? $_REQUEST["idalumno"] : ($idrol == 3) ? $user["idpersona"] : '';
		$iddocente = !empty($_REQUEST["iddocente"]) ? $_REQUEST["iddocente"] : ($idrol == 2) ? $user["idpersona"] : '';
		$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : '';
		$esarrayidalumno = stripos($idalumno, ",");
		if ($esarrayidalumno) $idalumno = explode(',', $idalumno);
		$idcursos = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
		$esarraycursos = stripos($idcursos, ",");
		if ($esarraycursos) $idcursos = explode(',', $idcursos);
		//$cursossetting=$this->oNegAcad_curso->buscar(array('idcurso'=>$idcursos,'idproyecto'=>$user["idproyecto"],'sql3'=>true));
		if ($idrol == 3) $mismatriculas = $this->oNegAcad_matricula->buscar(array('idcurso' => $idcursos, 'idalumno' => $idalumno, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else if ($idrol == 2) $mismatriculas = $this->oNegAcad_matricula->buscar(array('idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $iddocente, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else $mismatriculas = $this->oNegAcad_matricula->buscar(array('idgrupoauladetalle' => $idgrupoauladetalle, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		$hayexamenescurso = array();
		$datosreturn = array();
		if (!empty($mismatriculas))
			foreach ($mismatriculas as $cur) {
				unset($cur["txtjson"]);
				unset($cur["aniopublicacion"]);
				unset($cur["autor"]);
				unset($cur["descripcion"]);
				unset($cur["objetivos"]);
				unset($cur["silabo"]);
				unset($cur["costo"]);
				unset($cur["fecharegistro"]);
				unset($cur["vinculosaprendizajes"]);
				unset($cur["materialesyrecursos"]);
				unset($cur["pdf"]);
				unset($cur["abreviado"]);
				unset($cur["esdemo"]);
				unset($cur["tipodoc"]);
				unset($cur["oldid"]);
				unset($cur["telefono"]);
				unset($cur["usuario"]);
				unset($cur["strlocal"]);
				unset($cur["strambiente"]);
				unset($cur["color"]);
				unset($cur["celular"]);
				unset($cur["certificacion"]);
				unset($cur["idusuario"]);
				unset($cur["estado"]);
				unset($cur["idproyecto"]);
				unset($cur["fecha_matricula"]);
				unset($cur["fecha_registro"]);
				unset($cur["fecha_vencimiento"]);
				unset($cur["estado_curso"]);

				$cur["notaentrada"] = 0;
				$cur["notasalida"] = 0;
				$cur["notaentrada_enbasea"] = 100;
				$cur["notasalida_enbasea"] = 100;
				$cur["notaentradaquiz"] = 0;
				$cur["notaentradaquiz_enbasea"] = 100;
				$cur["notasalidaquiz"] = 0;
				$cur["notasalidaquiz_enbasea"] = 100;
				$cur["notasalidatiempo"] = '00:00:00';
				$cur["notaentradatiempo"] = '00:00:00';
				$notaconfig = array();
				$examenes = array();

				if (!empty($cur["configuracion_nota"])) {
					$notaconfig = json_decode($cur["configuracion_nota"], true);
					//var_dump($notaconfig);
					if (empty($notaconfig["tipocalificacion"]) && empty($notaconfig["puntuacion"])) {
						$cur["puntaje"] = array('puntajemax' => intval($this->Dpuntajemax), 'puntajemin' => intval($this->Dpuntajemin), 'tipocalificacion' => 'N');
					} elseif ($notaconfig["tipocalificacion"] == 'P' || $notaconfig["tipocalificacion"] == 'N') {
						$cur["puntaje"] = array('puntajemax' => @intval($notaconfig["maxcal"]), 'puntajemin' => @intval($notaconfig["mincal"]), 'tipocalificacion' => @$notaconfig["tipocalificacion"]);
					} else $cur["puntaje"] = array('tipocalificacion' => @$notaconfig["tipocalificacion"], 'puntuacion' => @$notaconfig["puntuacion"]);
					$tmpexamenes = $notaconfig["examenes"];
					if (!empty($tmpexamenes))
						foreach ($tmpexamenes as $tmpexa) {
							if (@$tmpexa["porcentaje"] > 0) {
								//var_dump($cur["puntaje"]);
								$puntajemax = !empty($cur["puntaje"]['puntajemax']) ? $cur["puntaje"]['puntajemax'] : $this->Dpuntajemax;
								$puntajemin = !empty($cur["puntaje"]['puntajemin']) ? $cur["puntaje"]['puntajemin'] : $this->Dpuntajemin;
								$examenes[] = array('idexamen' => $tmpexa["idexamen"], 'tipo' => @$tmpexa["tipo"], 'nombre' => $tmpexa["nombre"], 'idcursodetalle' => $tmpexa["idcursodetalle"], 'nota' => 0, 'porcentaje' => @intval($tmpexa["porcentaje"]), 'puntajemax' => intval($puntajemax), 'puntajemin' => intval($puntajemin), 'imagen' => $tmpexa["imagen"], 'idrecurso' => $tmpexa["idrecurso"]);
							}
						}
					$cur["formula"] = $examenes;
				} else {
					$examenes[] = array('idexamen' => 0, 'nota' => 0, 'porcentaje' => 100);
					$cur["puntaje"] = array('puntajemax' => intval($this->Dpuntajemax), 'puntajemin' => intval($this->Dpuntajemin), 'tipocalificacion' => 'N');
					$cur["formula"] = $examenes;
				}
				$examenes = array(); // falta revisar que tome la nota y asignarla al curso del alumno..
				$idalumno = $cur["idalumno"];
				$notasdelcurso = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idcurso' => $cur["idcurso"]));
				$resultadoconnotascurso = array(); // todas las notas por curso si las hubiera
				if (!empty($notasdelcurso)) {
					if (empty($hayexamenescurso[$cur["idcurso"]])) {
						$examenes = $this->oNegAcad_cursodetalleTest->examenes($cur["idcurso"], 0);
						$hayexamenescurso[$cur["idcurso"]] = $examenes;
					} else $examenes = $hayexamenescurso[$cur["idcurso"]];
					$nexamen = 1;
					if (!empty($examenes)) {
						$ntexamenes = count($examenes);
						foreach ($examenes as $ex) {
							foreach ($notasdelcurso as $nt) {
								$curptmax = !empty($cur["puntaje"]["puntajemax"]) ? $cur["puntaje"]["puntajemax"] : $this->Dpuntajemax;
								$curptmim = !empty($cur["puntaje"]["puntajemin"]) ? $cur["puntaje"]["puntajemin"] : $this->Dpuntajemin;
								$calificacion_total = !empty($nt["calificacion_total"]) ? ceil($nt["calificacion_total"]) : $this->Dpuntajemax;
								$calificacion_min = !empty($nt["calificacion_min"]) ? ceil($nt["calificacion_min"]) : $this->Dpuntajemin;
								$calificacion_en = !empty($nt["calificacion_en"]) ? ceil($nt["calificacion_en"]) : 'P';
								$tiempo_realizado = !empty($nt["tiempo_realizado"]) ? ceil($nt["tiempo_realizado"]) : '00:00:00';
								$nota = 0;
								//if($calificacion_en=='P'||$calificacion_en=='A') $nota=$nt["nota"];
								//elseif($calificacion_en=='N') $nota=$this->notacal($nt["nota"],$calificacion_total,100);
								//else $nota=$nt["nota"];
								$nota = $this->notacal($nt["nota"], $calificacion_total, 100);
								$nota = $this->notacal($nota, 100, $curptmax); // nota equivalente al puntaje configurado en el curso;
								if ($nexamen == 1 && $ex["idexamen"] == $nt["idrecurso"] && $nt["tipo"] == 'E') {
									$cur["notaentrada"] = $nota;
									$cur["notaentradaquiz"] = $nt["nota"];
									$cur["notaentradatiempo"] = $tiempo_realizado;
									$cur["notaentradaquiz_enbasea"] = $calificacion_total;
									$cur["notaentrada_enbasea"] = $curptmax;
								} elseif ($nexamen == $ntexamenes && $ex["idexamen"] == $nt["idrecurso"] && $nt["tipo"] == 'F') {
									$cur["notasalida"] = $nota;
									$cur["notasalidaquiz"] = $nt["nota"];
									$cur["notasalidatiempo"] = $tiempo_realizado;
									$cur["notasalidaquiz_enbasea"] = $calificacion_total;
									$cur["notasalida_enbasea"] = $curptmax;
								}
							}
							$nexamen++;
						}
						foreach ($notasdelcurso as $nt) {
							$nexamen2 = 1;
							$ntexamenes2 = count($cur["formula"]);
							$newformula = array();
							$calificacion_total = !empty($nt["calificacion_total"]) ? ceil($nt["calificacion_total"]) : $this->Dpuntajemax;
							$calificacion_min = !empty($nt["calificacion_min"]) ? ceil($nt["calificacion_min"]) : $this->Dpuntajemin;
							//$calificacion_en=!empty($nt["calificacion_en"])?ceil($nt["calificacion_en"]):'P';
							$tiempo_realizado = !empty($nt["tiempo_realizado"]) ? ceil($nt["tiempo_realizado"]) : '00:00:00';


							if (!empty($cur["formula"]))
								foreach ($cur["formula"] as $fr) {
									if (!empty($fr["idexamen"])) {
										if ($fr["idexamen"] == $nt["idrecurso"] && $fr["idcursodetalle"] == $nt["idcursodetalle"]) {
											$nota = $this->notacal($nt["nota"], 100, $calificacion_total); // nota al smarquiz al 100%
											$nota = $this->notacal($nota, 100, $fr["puntajemax"]);
											$fr["nota"] = $nota;
											$fr["nota_enbasea"] = $fr["puntajemax"];
											$fr["notaquiz"] = $nt["nota"];
											$fr["notatiempo"] = $tiempo_realizado;
											$fr["notaquiz_enbasea"] = $calificacion_total;
										}
										$newformula[] = $fr;
									}
								}
							if (!empty($newformula))
								$cur["formula"] = $newformula;
						}
					}
				}
				$datosreturn[] = $cur;
			}
		echo json_encode(array('code' => 200, 'data' => $datosreturn));
		exit(0);
	}

	private function notacal($pt, $ptmax = 100, $equivalente = 100)
	{ // regla de tres para calcular nota;
		if ($pt == 0) return 0;
		return round((($pt * $equivalente) / $ptmax), 2);
	}

	public function defecto()
	{
		return $this->notas();
	}

	public function progreso()
	{
		try {
			$filtros = array();
			$user = NegSesion::getUsuario();
			$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
			$idcursos = isset($_REQUEST['idcurso']) ? $_REQUEST['idcurso'] : '';
			if ($idrol == 3) $idalumno = isset($_REQUEST['idalumno']) ? $_REQUEST['idalumno'] : NegSesion::getUsuario()['idpersona'];
			else if ($idrol == 2) $iddocente = isset($_REQUEST['iddocente']) ? $_REQUEST['iddocente'] : NegSesion::getUsuario()['idpersona'];

			$esarraycursos = stripos($idcursos, ",");
			if ($esarraycursos) $idcursos = explode(',', $idcursos);


			//if(empty($idcurso)){
			$idproyecto = isset($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : NegSesion::getUsuario()['idproyecto'];
			//buscar todos los cursos matriculados
			if ($idrol == 3)
				$_cursos = $this->oNegAcad_matricula->buscar(array('idalumno' => $idalumno, 'idcurso' => $idcursos, 'idproyecto' => $idproyecto, 'orderby' => ' strcurso'));
			else if ($idrol == 2)
				$_cursos = $this->oNegAcad_matricula->buscar(array('iddocente' => $iddocente, 'idcurso' => $idcursos, 'idproyecto' => $idproyecto, 'orderby' => ' strcurso'));
			else
				$_cursos = $this->oNegAcad_matricula->buscar(array('idcurso' => $idcursos, 'idproyecto' => $idproyecto, 'orderby' => ' strcurso'));

			foreach ($_cursos as $key => $value) {
				$idcurso[] = $value['idcurso'];
			}


			if (empty($_cursos[0])) {
				throw new Exception("No hay cursos");
			}




			//$rs_cursos = $this->oNegAcad_curso->buscar(array('idcurso'=>$idcurso,'idproyecto'=>));
			/*if(!is_array($idcursos)){
				$idcurso = array($idcurso);
			}*/
			$progresos = array();
			$total = array();

			foreach ($_cursos as $key => $curso) {
				$idcur = $curso["idcurso"];
				$nombre = $curso["strcurso"];
				$idalumno = $curso["idalumno"];
				$total = 0;
				$prepare = array();
				//$_nombre = 'empty';

				$rs_sesion = $this->oNegAcad_cursodetalleTest->sesiones($idcur, 0);
				foreach ($rs_sesion as $k => $tem) {
					$tem["progreso"] = 0;
					$tem["nactividad"] = 0;
					$tem['idrecurso'] = $tem['idcursodetalle'];
					$tem = $this->oNegAcad_curso->getProgresoUnidad($tem, $idcur, $idalumno);
					$prepare[] = $tem;
				}
				if (!empty($prepare)) {
					$prom = 0;
					foreach ($prepare as $p) {
						$prom += doubleval($p['progreso']);
					}
					$total = $prom / count($prepare);
				}
				$progresos[] = array('idcurso' => $idcur, 'nombre' => $nombre, 'progreso' => round($total, 2, PHP_ROUND_HALF_DOWN), 'alumno' => $curso['stralumno']);
			}
			echo json_encode(array('code' => 200, 'data' => $progresos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => $e->getMessage(), 'data' => array()));
			exit(0);
		}
	}

	public function tiempo()
	{
		$user = NegSesion::getUsuario();
		$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
		$idalumno = !empty($_REQUEST["idalumno"]) ? $_REQUEST["idalumno"] : ($idrol == 3) ? $user["idpersona"] : '';
		$iddocente = !empty($_REQUEST["iddocente"]) ? $_REQUEST["iddocente"] : ($idrol == 2) ? $user["idpersona"] : '';
		$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : '';
		$esarrayidalumno = stripos($idalumno, ",");
		if ($esarrayidalumno) $idalumno = explode(',', $idalumno);
		$idcursos = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
		$esarraycursos = stripos($idcursos, ",");
		if ($esarraycursos) $idcursos = explode(',', $idcursos);
		if ($idrol == 3) $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idcurso' => $idcursos, 'idalumno' => $idalumno, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else if ($idrol == 2) $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $iddocente, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idgrupoauladetalle' => $idgrupoauladetalle, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		$datos = array();
		if (!empty($mismatriculas)) {
			foreach ($mismatriculas as $mk => $m) {
				$tiempos = $this->oNegHistorial_sesion->buscartiempos(array('idusuario' => $m["idalumno"], 'idproyecto' => $user["idproyecto"]));
				$tiempopv = 0;
				$tiempoexam = 0;
				$tiempocursos = 0;
				if (!empty($tiempos))
					foreach ($tiempos as $kt => $t) {
						if ($t["lugar"] == 'P') $tiempopv += intval($t['tiempoensegundos']);
						if ($t["idcurso"] == $m["idcurso"] && $t["idcurso"] != null) {
							if ($t["lugar"] == 'E') $tiempoexam += intval($t['tiempoensegundos']);
							if ($t["lugar"] == 'S') $tiempocursos += intval($t['tiempoensegundos']);
						}
					}
				$m["tiempopv"] = $tiempopv;
				$m["tiempoexam"] = $tiempoexam;
				$m["tiempocursos"] = $tiempocursos;
				$datos[] = $m;
			}
		}
		echo json_encode(array('code' => 200, 'data' => $datos));
		exit(0);
	}
}
