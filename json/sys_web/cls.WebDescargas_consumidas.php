<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-12-2020 
 * @copyright	Copyright (C) 02-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegDescargas_consumidas', RUTA_BASE);
class WebDescargas_consumidas extends JrWeb
{
	private $oNegDescargas_consumidas;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegDescargas_consumidas = new NegDescargas_consumidas;
		
	}

	public function defecto(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Descargas_consumidas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["iddescargaconsumida"])) $filtros["iddescargaconsumida"]=$_REQUEST["iddescargaconsumida"];
			if(!empty($_REQUEST["idasignado"])) $filtros["idasignado"]=$_REQUEST["idasignado"];
			if(!empty($_REQUEST["archivo"])) $filtros["archivo"]=$_REQUEST["archivo"];
			if(!empty($_REQUEST["idcurso"])) $filtros["idcurso"]=$_REQUEST["idcurso"];
			if(!empty($_REQUEST["idcc"])) $filtros["idcc"]=$_REQUEST["idcc"];

			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"]; 
  			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"]; 
  			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"]; 
  			if(isset($_REQUEST["codigo_suscripcion"])&&@$_REQUEST["codigo_suscripcion"]!='')$filtros["codigo_suscripcion"]=$_REQUEST["codigo_suscripcion"];   			
  			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
			
			/*if(!empty($filtros["idproyecto"])){
  				$filtros["idproyecto"]=@$usuarioAct["idproyecto"];
  			}*/
					
			$this->datos=$this->oNegDescargas_consumidas->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_REQUEST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_REQUEST);
            $accion='_add'; 


            if(empty($_REQUEST["idpersona"])){
            	$user=NegSesion::getUsuario();
				$idempresa=$user["idempresa"];
				$idproyecto=$user["idproyecto"];
				$idpersona=$user["idpersona"];
            }else{
            	$idpersona=@$_REQUEST["idpersona"];
            	$idempresa=@$_REQUEST["idempresa"];
				$idproyecto=@$_REQUEST["idproyecto"];				

            	if(empty($_REQUEST["idempresa"])){
					echo json_encode(array('code'=>'error','msj'=>'No ha definido a que empresa pertenece la persona enviada'));
					exit();
				}

				if(empty($_REQUEST["idproyecto"])){
					echo json_encode(array('code'=>'error','msj'=>'No ha definido a que proyecto pertenece la persona enviada'));
					exit();
				}
            }
            	
        	if(empty($idpersona)){
				echo json_encode(array('code'=>'error','msj'=>'Defina una persona a quien se le debe asignar más descargas'));
				exit();
			}


			JrCargador::clase('sys_negocio::NegDescargas_asignadas', RUTA_BASE);
			$oNegDescargas_asignadas = new NegDescargas_asignadas;
			$filtros=array();
			$filtros["idpersona"]=$idpersona;
			$filtros["idempresa"]=$idempresa;
			$filtros["idproyecto"]=$idproyecto;
			$filtros["sqlporconsumir"]=1;
			$filtros["sqlget"]=1;

			if(!empty($_REQUEST["idasignado"])){
				$filtros["id"]=@$_REQUEST["idasignado"];
			}
			$alldescargas=$oNegDescargas_asignadas->buscar($filtros);
			if(empty($alldescargas)){
				echo json_encode(array('code'=>'error','msj'=>'ya no cuenta con un limite de descargas activas'));
				exit();	
			}

  			$this->oNegDescargas_consumidas->idasignado=@$alldescargas["id"];
  			$this->oNegDescargas_consumidas->idempresa=@$_REQUEST["archivo"];
  			$this->oNegDescargas_consumidas->idcurso=@$_REQUEST["idcurso"];
  			$this->oNegDescargas_consumidas->strcurso=@$_REQUEST["strcurso"];
           	$this->oNegDescargas_consumidas->idcc=@$_REQUEST["idcc"];
            $res=$this->oNegDescargas_consumidas->agregar();
            echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Descargas_consumidas')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	/*public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegDescargas_consumidas->__set('iddescargaconsumida', $_REQUEST['iddescargaconsumida']);
			$res=$this->oNegDescargas_consumidas->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegDescargas_consumidas->setCampo($_REQUEST['iddescargaconsumida'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  */
 
}