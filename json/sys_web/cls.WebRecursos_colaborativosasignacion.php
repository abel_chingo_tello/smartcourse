<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-12-2020 
 * @copyright	Copyright (C) 07-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRecursos_colaborativosasignacion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRecursos_colabasig_comentario', RUTA_BASE);
class WebRecursos_colaborativosasignacion extends JrWeb
{
	private $oNegRecursos_colaborativosasignacion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRecursos_colaborativosasignacion = new NegRecursos_colaborativosasignacion;
    $this->oNegRecursos_colabasig_comentario = new NegRecursos_colabasig_comentario;
		
	}

	public function defecto(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Recursos_colaborativosasignacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idrecursoasignacion"])) $filtros["idrecursoasignacion"]=$_REQUEST["idrecursoasignacion"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"]; 
  			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"]; 
  			if(isset($_REQUEST["grupo"])&&@$_REQUEST["grupo"]!='')$filtros["grupo"]=$_REQUEST["grupo"]; 
  			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"]; 
  			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"]; 
  			if(isset($_REQUEST["nota_base"])&&@$_REQUEST["nota_base"]!='')$filtros["nota_base"]=$_REQUEST["nota_base"]; 
  			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"]; 
  			if(isset($_REQUEST["fecha_calificacion"])&&@$_REQUEST["fecha_calificacion"]!='')$filtros["fecha_calificacion"]=$_REQUEST["fecha_calificacion"]; 
  			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"]; 
  			if(isset($_REQUEST["respuestas"])&&@$_REQUEST["respuestas"]!='')$filtros["respuestas"]=$_REQUEST["respuestas"]; 
  			if(isset($_REQUEST["fecha_respuesta"])&&@$_REQUEST["fecha_respuesta"]!='')$filtros["fecha_respuesta"]=$_REQUEST["fecha_respuesta"]; 
  			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"]; 
  			if(isset($_REQUEST["idpestania"])&&@$_REQUEST["idpestania"]!='')$filtros["idpestania"]=$_REQUEST["idpestania"]; 
  			if(isset($_REQUEST["fecha_creacion"])&&@$_REQUEST["fecha_creacion"]!='')$filtros["fecha_creacion"]=$_REQUEST["fecha_creacion"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			  if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegRecursos_colaborativosasignacion->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($idrecursoasignacion)) {
      				$this->oNegRecursos_colaborativosasignacion->idrecursoasignacion = $idrecursoasignacion;
      				$accion='_edit';
      			}

            $this->oNegRecursos_colaborativosasignacion->idrecurso=@$idrecurso;
      			$this->oNegRecursos_colaborativosasignacion->idalumno=@$idalumno;
      			$this->oNegRecursos_colaborativosasignacion->grupo=@$grupo;
      			$this->oNegRecursos_colaborativosasignacion->idgrupoauladetalle=@$idgrupoauladetalle;
      			$this->oNegRecursos_colaborativosasignacion->nota=@$nota;
      			$this->oNegRecursos_colaborativosasignacion->nota_base=@$nota_base;
      			$this->oNegRecursos_colaborativosasignacion->iddocente=@$iddocente;
      			$this->oNegRecursos_colaborativosasignacion->fecha_calificacion=@$fecha_calificacion;
      			$this->oNegRecursos_colaborativosasignacion->titulo=@$titulo;
      			$this->oNegRecursos_colaborativosasignacion->respuestas=@$respuestas;
      			$this->oNegRecursos_colaborativosasignacion->fecha_respuesta=@$fecha_respuesta;
      			$this->oNegRecursos_colaborativosasignacion->idcursodetalle=@$idcursodetalle;
      			$this->oNegRecursos_colaborativosasignacion->idpestania=@$idpestania;
      			$this->oNegRecursos_colaborativosasignacion->fecha_creacion=@$fecha_creacion;
  				        
            if($accion=='_add') {
            	$res=$this->oNegRecursos_colaborativosasignacion->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursos_colaborativosasignacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegRecursos_colaborativosasignacion->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursos_colaborativosasignacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarasignaciones(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario();

            //$this->oNegRecursos_colaborativosasignacion->idrecurso=@$idrecurso;
            $recursos=array(
            	'idrecurso'=>$idrecurso,
            	'titulo'=>$titulo,
            	'descripcion'=>$descripcion,
            	'tipo'=>$tipo,
            	'estado'=>1,
            	'file'=>@$file,
            	'idproyecto'=>@$usuarioAct["idproyecto"],
            	'temasxgrupo'=>@$temasxgrupo,
            	'grupos'=>json_decode($grupos,true),
            	'idcursodetalle'=>@$idcursodetalle,
            	'idgrupoauladetalle'=>@$idgrupoauladetalle,
            	'idpestania'=>$idpestania,
            	'idusuario'=>$usuarioAct["idpersona"],
            	'idproyecto'=>@$usuarioAct["idproyecto"],
            	'ngrupos'=>$ngrupos
            );
            
            $res=$this->oNegRecursos_colaborativosasignacion->agregarasignacion($recursos);
            echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursos_colaborativosasignacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

  public function guardar_respuesta(){
    try {
      if(empty($_REQUEST)){ 
        echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos dd'));
        exit(0);
      }
      $usuarioAct = NegSesion::getUsuario();
      $fecha=date('Y-m-d h:i:s');
      $idrecursoasignacion=$_REQUEST['idrecursoasignacion'];
      if($_REQUEST["esrespuesta"]=='si'){
        $acc='add';        
        if(!empty($_REQUEST["idrecursocomentario"])){          
          $hayrecursocomentario=$this->oNegRecursos_colabasig_comentario->buscar(array('idrecursocomentario'=>$_REQUEST["idrecursocomentario"],'sqlget'=>true));
        //  var_dump($hayrecursocomentario);
         // exit();
          if(!empty($hayrecursocomentario)) {
            $acc='edit';
             $this->oNegRecursos_colabasig_comentario->idrecursocomentario=$_REQUEST["idrecursocomentario"];
          }
        }  
       // var_dump($_REQUEST);
        $this->oNegRecursos_colabasig_comentario->idasignacion=@$idrecursoasignacion;
        $this->oNegRecursos_colabasig_comentario->idpersona=@$usuarioAct["idpersona"];
        $this->oNegRecursos_colabasig_comentario->comentario=@$_REQUEST["comentario"];
        $this->oNegRecursos_colabasig_comentario->fecha=@$fecha;
        $this->oNegRecursos_colabasig_comentario->file=@$file;        
        $this->oNegRecursos_colabasig_comentario->idpadre=@$_REQUEST["idpadre"];        
        if($acc=='edit') {
          $res=$this->oNegRecursos_colabasig_comentario->editar();              
        }else{
          $res=$this->oNegRecursos_colabasig_comentario->agregar();        
        }
        $respuesta=array('idrecursocomentario'=>$res,'idpadre'=>@$_REQUEST["idpadre"]);
      }else{
        $respuesta='';
        $this->oNegRecursos_colaborativosasignacion->setCampo($idrecursoasignacion,'respuestas',$_REQUEST['comentario']);
        $this->oNegRecursos_colaborativosasignacion->setCampo($idrecursoasignacion,'fecha_respuesta',$fecha);
      }

      echo json_encode(array('code'=>200,'respuesta'=>@$respuesta,'idrecursoasignacion'=>$idrecursoasignacion,'fecha'=>$fecha,'msj'=>ucfirst(JrTexto::_('Tu respuesta se guardó correctamente'))));
      exit();
    }catch(Exception $e) {
      echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
      exit(0);
    }
  }
  public function guardar_nota(){
    try {
      if(empty($_REQUEST)){ 
        echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
        exit(0);
      }
      $recursoasignacion=$this->oNegRecursos_colaborativosasignacion->buscar(array('idrecursoasignacion'=>$_REQUEST['idrecursoasignacion']));
      if(empty($recursoasignacion)){
        echo json_encode(array('code'=>'error','msj'=>'Asignación no existe'));
        exit(0);
      }
      $usuarioAct = NegSesion::getUsuario();
      $estados=array('nota'=>$_REQUEST["nota"],'nota_base'=>$_REQUEST["nota_base"],'iddocente'=>$usuarioAct["idpersona"],'fecha_calificacion'=>date('Y-m-d h:i:s'));
      if($_REQUEST["tipo"]=='foro'){
        $condicion=array('idrecursoasignacion'=>$_REQUEST['idrecursoasignacion']);
        $this->oNegRecursos_colaborativosasignacion->actualizar2($estados,$condicion);        
      }else{ // tipo Wiki
        $recursoasignacion=$recursoasignacion[0];
        JrCargador::clase('sys_negocio::NegRecursos_colaborativos', RUTA_BASE);
        $oNegRecursos=new NegRecursos_colaborativos;
        $recurso=$oNegRecursos->buscar(array('idrecurso'=>$recursoasignacion["idrecurso"]));
        if(empty($recurso)){
          echo json_encode(array('code'=>'error','msj'=>'Recurso  ya no existe'));
          exit(0);
        }

        $usuarioAct = NegSesion::getUsuario();
        $recurso=$recurso[0];

        $condicion=array('idrecurso'=>$recursoasignacion["idrecurso"],'idgrupoauladetalle'=>$recursoasignacion["idgrupoauladetalle"],'grupo'=>$recursoasignacion["grupo"],'idcursodetalle'=>$recursoasignacion["idcursodetalle"]);
        if(!empty($recursoasignacion["idpestania"])) $condicion['idpestania']=$recursoasignacion["idpestania"];
        $this->oNegRecursos_colaborativosasignacion->actualizar2($estados,$condicion);
      }
      echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('La calificación se guardó correctamente'))));
      exit();
    }catch(Exception $e) {
      echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
      exit(0);
    }
  }

  public function guardar_respuestawiki(){
    try {
      if(empty($_REQUEST)){ 
        echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
        exit(0);
      }


      $recursoasignacion=$this->oNegRecursos_colaborativosasignacion->buscar(array('idrecursoasignacion'=>$_REQUEST['idrecursoasignacion']));
      if(empty($recursoasignacion)){
        echo json_encode(array('code'=>'error','msj'=>'Asignación de wiki no existe'));
        exit(0);
      }

      $recursoasignacion=$recursoasignacion[0];
      JrCargador::clase('sys_negocio::NegRecursos_colaborativos', RUTA_BASE);
      $oNegRecursos=new NegRecursos_colaborativos;
      $recurso=$oNegRecursos->buscar(array('idrecurso'=>$recursoasignacion["idrecurso"]));
      if(empty($recurso)){
        echo json_encode(array('code'=>'error','msj'=>'Recurso  ya no existe'));
        exit(0);
      }

      $usuarioAct = NegSesion::getUsuario();
      $recurso=$recurso[0];



$archivo=RUTA_BASE.'static'.SD.'media'.SD.'wikis';
      @mkdir($archivo,0777,true);
      @chmod($archivo, 0777);
      $archivo=$archivo.SD.'E'.$usuarioAct["idempresa"]."_P".$usuarioAct["idproyecto"];
      @mkdir($archivo,0777,true);
      @chmod($archivo, 0777);
      //$archivo=$archivo.SD.'curso_'.$idcurso."_".$idcc;
      //@mkdir($archivo,0777,true);   
      $archivo=$archivo.SD.'GAD'.$recursoasignacion["idgrupoauladetalle"].'_R'.$recursoasignacion["idrecurso"]."_A".$usuarioAct["idpersona"].".php";
      if(file_exists($archivo)) {
        @rename($archivo,$archivo."_".date('YMDHis'));
        @chmod($archivo, 0777);
      }
      $fp = fopen($archivo, "a"); 

      $respuestatxt=base64_encode(preg_replace('/\s+/', ' ', $_REQUEST['comentario']));
      $textexamen = <<<STRWIKI
<?php 
\$respuesta=<<<STRCHINGO
  $respuestatxt 
STRCHINGO;
\$respuesta=base64_decode(trim(\$respuesta));
?>
STRWIKI;
$write = fputs($fp,$textexamen);
      fclose($fp);
      @chmod($archivo,0777);

      $pos = strpos($archivo, 'static');
      if($pos!==false){
        $archivo=substr($archivo, $pos);
      }

      $estados=array('respuestas'=>$archivo,'fecha_respuesta'=>date('Y-m-d h:i:s'));
      $condicion=array('idrecurso'=>$recursoasignacion["idrecurso"],'idgrupoauladetalle'=>$recursoasignacion["idgrupoauladetalle"],'grupo'=>$recursoasignacion["grupo"],'idcursodetalle'=>$recursoasignacion["idcursodetalle"]);
      if(!empty($recursoasignacion["idpestania"])) $condicion['idpestania']=$recursoasignacion["idpestania"];


      $this->oNegRecursos_colaborativosasignacion->actualizar2($estados,$condicion);
      echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Tu respuesta se guardó correctamente'))));
      exit();
    }catch(Exception $e) {
      echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
      exit(0);
    }
  }


	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRecursos_colaborativosasignacion->__set('idrecursoasignacion', $_REQUEST['idrecursoasignacion']);
			$res=$this->oNegRecursos_colaborativosasignacion->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRecursos_colaborativosasignacion->setCampo($_REQUEST['idrecursoasignacion'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}