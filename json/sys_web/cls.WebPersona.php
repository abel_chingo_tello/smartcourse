<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-09-2019 
 * @copyright	Copyright (C) 12-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE);
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
class WebPersona extends JrWeb
{
	private $oNegPersona;
	private $oNegPersona_rol;
	private $oNegProyecto;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona = new NegPersonal;
		$this->oNegProyecto = new NegProyecto;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='') $filtros["rol"]=$_REQUEST["rol"];
			else{$filtros["rol"]=1;}
			$this->idrol=$filtros["rol"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];

			if(isset($_REQUEST["dre"])&&@$_REQUEST["dre"]!=''&&@$_REQUEST["dre"]!='undefined')$filtros["iddre"]=$_REQUEST["dre"];
			if(isset($_REQUEST["ugel"])&&@$_REQUEST["ugel"]!=''&&@$_REQUEST["ugel"]!='undefined')$filtros["idugel"]=$_REQUEST["ugel"];
			if(isset($_REQUEST["iiee"])&&@$_REQUEST["iiee"]!=''&&@$_REQUEST["iiee"]!='undefined')$filtros["idiiee"]=$_REQUEST["iiee"];
			if(isset($_REQUEST["curso"])&&@$_REQUEST["curso"]!=''&&@$_REQUEST["curso"]!='undefined')$filtros["idcurso"]=$_REQUEST["curso"];
			if(isset($_REQUEST["esdemo"])&&@$_REQUEST["esdemo"]!=''&&@$_REQUEST["esdemo"]!='undefined')$filtros["idesdemo"]=$_REQUEST["esdemo"];
			if(isset($_REQUEST["sqlsolopersona"])&&@$_REQUEST["sqlsolopersona"]!=''&&@$_REQUEST["sqlsolopersona"]!='undefined')$filtros["sqlsolopersona"]=$_REQUEST["sqlsolopersona"];
			if(isset($_REQUEST["validarusuario"])&&@$_REQUEST["validarusuario"]!=''&&@$_REQUEST["validarusuario"]!='undefined'){
				$filtros["validarusuario"]=true;
				$filtros["sqlsolopersona"]=true;
			}
			$usuarioAct=NegSesion::getUsuario();
			$filtros["idproyecto"]=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			if(!empty($_REQUEST["idproyecto"]))$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			if(isset($_REQUEST["sql4"])&&@$_REQUEST["sql4"]!='')$filtros["sql4"]=$_REQUEST["sql4"];
			if(isset($filtros['sql4'])){
				$filtros['sql4'] = $usuarioAct["idpersona"];
			}
			$this->oNegPersona->setLimite(0,900000);
			$this->datos=$this->oNegPersona->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function leer(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>JrTexto::_('Data incomplete')));
				exit(0);
			}
			$idpersona = $_REQUEST['idpersona'];
			if(!is_numeric($_REQUEST['idpersona']) && !is_array($_REQUEST['idpersona']) && is_string($_REQUEST['idpersona'])){
				$idpersona = json_decode($_REQUEST['idpersona'],true);				
			}
			$this->oNegPersona->setLimite(0,100000);
			$res=$this->oNegPersona->buscar(array("idpersona" => $idpersona));
			// var_dump(count($res));exit();
			echo json_encode(array('code'=>200,'data'=>$res));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('Data incomplete')));
			exit(0);
		}
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            $datos=array();
            if(!empty($idpersona)){
				$datos=$this->oNegPersona->buscar(array('idpersona'=>$idpersona));
				if(!empty($datos[0])){
					$accion='edit';
					$this->oNegPersona->set('idpersona',$idpersona);
				}
			}
			if(!empty($dni)&&empty($datos)){
				$datos=$this->oNegPersona->buscar(array('dni'=>$dni));
				if(!empty($datos[0])){
					$this->oNegPersona->set('idpersona',$datos[0]["idpersona"]);
					$accion='edit';
				}
			}
			
			$usuarioAct = NegSesion::getUsuario();
			foreach ($_POST as $key => $value){
				if($key!='idpersona')
					$this->oNegPersona->set($key,$value);				
			}
            if($accion=='_add'){            	
           		$this->oNegPersona->set('regusuario',$usuarioAct["idpersona"]);
           		$this->oNegPersona->set('rol',$idrol);
            	$res=$this->oNegPersona->agregar();
            	$rr=array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona')).' '.JrTexto::_('saved successfully'),'newid'=>$res); 
            }else{
            	$res=$this->oNegPersona->editar();
            	$rr=array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona')).' '.JrTexto::_('update successfully'),'newid'=>$res); 
            }
            $this->oNegPersona_rol = new NegPersona_rol;
            $idempresa=!empty($idempresa)?$idempresa:$usuarioAct["idempresa"];
            $idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
            $idrol=!empty($idrol)?$idrol:3;
            $hayrol=$this->oNegPersona_rol->buscar(array('idpersonal'=>$res,'idproyecto'=>$idproyecto,'idempresa'=>$idempresa,'idrol'=>$idrol));
           // var_dump($hayrol);
            if(empty($hayrol)){
            	$this->oNegPersona_rol->idpersonal=$res;
            	$this->oNegPersona_rol->idempresa=$idempresa;
            	$this->oNegPersona_rol->idproyecto=$idproyecto;
            	$this->oNegPersona_rol->idrol=$idrol;
            	$this->oNegPersona_rol->agregar();
            }
            JrCargador::clase('sys_negocio::NegTools', RUTA_RAIZ);					
			if(!empty($_FILES["fotofile"])){
		        $resi = NegTools::subirFile($_FILES["fotofile"],'usuarios/','','foto','img_'.$res);
		        if($resi['code']==200){
		        	$rr['file']=$resi["rutaStatic"]."?id=".date('YmdHis');
		        	$this->oNegPersona->setCampo($res,'foto',$rr['file']);
		        }
		    }
		    echo json_encode($rr);
		    exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','data'=>$this->datos,'msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function editarPerfil(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			$idpersona = $_POST["idpersona"];
			if($_POST["validarFile"] == "1"){
				$_POST["foto"] = NegTools::subirArchivo($_FILES["archivo"], "usuarios", $idpersona);
			}
			unset($_POST["idpersona"]);
			unset($_POST["archivo"]);
			unset($_POST["validarFile"]);
			$this->oNegPersona->editar_($idpersona, $_POST);
			echo json_encode(array('code'=>200, "foto"=>@$_POST["foto"]."?x=".rand(0, 99999)));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->datos=array();
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(empty($_POST)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_("Data Incompleta")));
		 		exit(0);
			}else{
				@extract($_POST);
				if(!empty($_POST["datajson"])){
					$dt=json_decode($_POST["datajson"]);
					if(!empty($dt)){						
						$usuarioAct = NegSesion::getUsuario();
						foreach($dt as $v){
							$per=0;							
							if(!empty($v->idpersona)){
								$datos=$this->oNegPersona->buscar(array('idpersona'=>$v->idpersona));
								if(!empty($datos[0])){
								  $per=$datos[0]["idpersona"];
								}
							}
							if(!empty($v->dni)&&empty($per)){
								$datos=$this->oNegPersona->buscar(array('dni'=>$v->dni));
								if(!empty($datos[0])){
									$per=$datos[0]["idpersona"];
								}
							}
							$idempresa=!empty($idempresa)?$idempresa:$usuarioAct["idempresa"];
							$idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
							$idrol=!empty($idrol)?$idrol:3;
							if(empty($per)){
								$datos=$this->oNegPersona->buscar(array('usuario'=>$v->usuario));
								if(!empty($datos[0])){
									$v->usuario=$v->usuario.date('Y1');
								}							
								$idioma=!empty($idioma)?$idioma:'ES';	
								$fechanac=NegTools::chekFecha($v->fechanac,'ymd')==true?$v->fechanac:date('y-m-d');
								$per=$this->oNegPersona->importar(@$v->idpersona,@$v->tipodoc,@$v->dni,@$v->ape_paterno,@$v->ape_materno,@$v->nombre,$fechanac,@$v->sexo,@$v->estado_civil,$v->ubigeo,$v->urbanizacion,@$v->direccion,@$v->telefono,@$v->celular,@$v->email,@$v->idugel,$usuarioAct["idpersona"],date('Y-m-d'),@$v->usuario,@$v->clave,@$v->token,$idrol,'',@$v->estado,@$v->situacion,$idioma,'n',@$v->idiiee,$idrol,@$v->ugel,@$v->iiee,$idproyecto,$idempresa);
							}else{
								$this->oNegPersona_rol = new NegPersona_rol;					            
					            $hayrol=$this->oNegPersona_rol->buscar(array('idpersonal'=>$per,'idproyecto'=>$idproyecto,'idempresa'=>$idempresa,'idrol'=>$idrol));
					            if(empty($hayrol)){
					            	$this->oNegPersona_rol->idpersonal=$per;
					            	$this->oNegPersona_rol->idempresa=$idempresa;
					            	$this->oNegPersona_rol->idproyecto=$idproyecto;
					            	$this->oNegPersona_rol->idrol=$idrol;
					            	$this->oNegPersona_rol->agregar();
					            }
							}
							$this->datos[$v->dni]=array('idpersona'=>$per,'dni'=>$v->dni,'msj'=>($per==-1?'Error':200));
						}
					}
				}
			}
			echo json_encode(array('code'=>200,'data'=>$this->datos,'msj'=>JrTexto::_("Datos Importados")));
		 	exit(0);
		}catch(Exception $e) {
            echo json_encode(array('code'=>'Error','data'=>$this->datos,'msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>JrTexto::_('Data incomplete')));
				exit(0);
			}

			$this->oNegPersona->__set('idpersona', $_REQUEST['idpersona']);
			$res=$this->oNegPersona->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('Data incomplete')));
			exit(0);
		}
	}
	public function eliminarusuario(){
		try{
			global $aplicacion;
			if(empty($_POST)){ throw new Exception("POST vacio"); }
			if(empty($_POST['idpersona'])){ throw new Exception("idpersona vacio"); }
			$idpersona = json_decode($_POST['idpersona'],true);
			$_data = (!is_array($idpersona)) ? [$idpersona] : $idpersona;
			$this->oNegPersona->eliminarusuario($_data);

			echo json_encode(array('code'=>200,'msj'=>'Eliminado'));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>JrTexto::_('Data incomplete')));
				exit(0);
			}
			if(!empty($_REQUEST['multiplescampos'])){
				//multiplescampos
				foreach ($_REQUEST['multiplescampos'] as $campo => $valor) {
					$this->oNegPersona->setCampo($_REQUEST['idpersona'],$campo,$valor);
				}
			}else{
				$this->oNegPersona->setCampo($_REQUEST['idpersona'],$_REQUEST['campo'],$_REQUEST['valor']);
			}
			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('Data incomplete')));
			exit(0);
		}
	}

	public function guardarclave(){		
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);
            }      
			global $aplicacion;
            @extract($_POST);
            if(!empty($idpersona)) {
				$this->oNegPersona->idpersona = $idpersona;
				$this->oNegPersona->email=@$email;
				$this->oNegPersona->usuario=@$usuario;
				$this->oNegPersona->clave=$clave;
				$this->oNegPersona->token='cambiarclave';
				$res=$this->oNegPersona->editar();	
				if(!empty($res)){
					$usuarioAct = NegSesion::getUsuario();
					JrCargador::clase('jrAdwen::JrCorreo');
					$oCorreo = new JrCorreo;
					$this->empresa=$this->oNegProyecto->buscar(array('idproyecto'=>$usuarioAct["idproyecto"]));
					if(!empty($this->empresa[0]))$this->empresa=$this->empresa[0];
						$deemail=!empty($this->empresa["correoempresa"])?$this->empresa["correoempresa"]:$usuarioAct["email"];
						$denombre=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:$usuarioAct["nombre_full"];
						$oCorreo->setRemitente($deemail,$denombre);

						$this->Asunto=JrTexto::_('Change password');
						$this->mensaje='<h1>'.JrTexto::_('User Information').'</h1><br>';
						$this->mensaje.='<b>'.JrTexto::_('User').' : </b>'.$usuario."<br>";
						$this->mensaje.='<b>'.JrTexto::_('Password').' : </b>'.$clave."<br>";
						$this->mensaje.='<b>'.JrTexto::_('platform access link').' : </b><br>'.URL_BASE."<br>";
						$oCorreo->setAsunto($this->Asunto);

							$oCorreo->addDestinarioPhpmailer($deemail,$denombre);
							$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com','Abel');
							$oCorreo->addDestinarioPhpmailer($email,$usuario);
							if(!empty($this->empresa)){								
								if(!empty($emp["correoempresa"]))
								$oCorreo->addDestinarioPhpmailer($this->empresa["correoempresa"],$this->empresa["nombre"]);
								//var_dump($this->empresa["logoempresa"]);
								if(!empty($this->empresa["logoempresa"]) && is_file(RUTA_BASE.$this->empresa["logoempresa"])){
									@chmod(RUTA_BASE.$this->empresa["logoempresa"], 0777);
									$ipost=stripos($this->empresa["logoempresa"],'?');
									if($ipost===false) $this->logoempresa=$this->empresa["logoempresa"];
									else $this->logoempresa=substr($this->empresa["logoempresa"],0,$ipost);
									$img=str_replace('\\', '/', RUTA_BASE.$logoempresa);
									$oCorreo->addadjuntos(RUTA_BASE.$this->logoempresa,'logoempresa');
								}
							}

							$this->esquema = 'correos/general_empresa';
							$oCorreo->setMensaje(parent::getEsquema());
							$envio=$oCorreo->sendPhpmailer();
				}

            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Password')).' '.JrTexto::_('update successfully'),'newid'=>$res));
			}
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	} 
	public function borrarhistorial(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $this->oNegPersona->borrarhistorial($idpersona);
            echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('history removed'))); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	public function changePassAct(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;

           	$usuarioAct = NegSesion::getUsuario();
           	parse_str($_POST["p_datos"], $datosArray);
			
			$id = $usuarioAct['idpersona'];
			$clave = md5($datosArray["passNueva"]);

			$this->oNegPersona->setCampo($id,"clave",$clave);
			
			$sesion = JrSession::getInstancia();
			$sesion->set('clave', $clave);
			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
		    exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
}