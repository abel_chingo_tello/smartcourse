<?php

use function PHPSTORM_META\map;

set_time_limit(0);

defined('RUTA_BASE') or die();
include_once('cls.WebMine.php');
#JrCargador::clase('sys_negocio::NegEnglish_actividad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_smartbook_se', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_competencias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegReporteModular', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE);


class WebReporteModular extends JrWeb
{
	private $oWebMine;
	private $oNegBitacora_smartbook_se;
	private $oNegActividad_alumno;
	private $oNegNotas_quiz;
	private $oNegAcad_competencias;
	private $oNegReporteModular;
	protected $oNegBitacora_smartbook;
	#private $oNegEnglish_Actividad;
	public function __construct()
	{
		parent::__construct();
		$this->oWebMine = new WebMine;
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegBitacora_smartbook_se = new NegBitacora_smartbook_se;
		$this->oNegBitacora_smartbook = new NegBitacora_smartbook;
		$this->oNegAcad_competencias = new NegAcad_competencias;
		$this->oNegReporteModular = new NegReporteModular;
	}
	public function getInitialData($params = array(), $return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$this->datos = [
				"idrol" => $oUsuario["idrol"],
				"data" => $oUsuario
			];
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				// echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getCursos($params = array(), $return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$arrAlumnos = array();
			$this->datos = $this->oWebMine->getCursosUsuario(true);
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				// echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getContenidoCurso($params = array(), $return = false) //Sesiones del curso como en CC
	{
		try {
			if (isset($params['idcomplementario']) && $params['idcomplementario'] != 0) {
				$this->datos = $this->oWebMine->getSesionesCurso(['idcomplementario' => $params['idcomplementario']], true);
			} else {
				$this->datos = $this->oWebMine->getSesionesCurso(['idcurso' => $params['idcurso']], true);
			}
			if (!$return) {
				// echo json_encode(array('code' => 200, 'data' => $this->datos));
				echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getAlumnosCurso($params = array(), $return = false) //Alumnos matriculados en un curso
	{
		try {
			$this->datos = $this->oWebMine->getAlumnosCurso([
				'idgrupoauladetalle' => $params['idgrupoauladetalle'],
				'fechaactiva' => true
			], true);
			if (!$return) {
				// echo json_encode(array('code' => 200, 'data' => $this->datos));
				echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getCursoProgreso($params = array(), $return = false)
	{
		try {
			$params = WebMine::getParams($params);

			if (isset($params["idcurso"]) || isset($params["idcomplementario"])) { //si me piden solo 1 curso
				#en desuso
				if (!isset($params["idgrupoauladetalle"]) || empty($params["idgrupoauladetalle"])) {
					$params["idgrupoauladetalle"] = 0; #para reportes de antonio
					#throw new Exception("para listar por curso, debe especificar el idgrupoauladetalle");
				}
				$curso = $this->oWebMine->getInstanciaCurso($params, true)[0];
				$params["curso"] = $curso;
				$curso = $this->getAvance($params, true);
				$this->datos = $curso;
			} else {
				$arrCursos = $this->oWebMine->getCursosAlumno([
					"idalumno" => $params["idalumno"]
				], true);
				foreach ($arrCursos as $i => $curso) {
					$curso = $this->getAvance([
						"idcurso" => $curso["idcurso"],
						"idcomplementario" => $curso["idcomplementario"],
						"idgrupoauladetalle" => $curso["idgrupoauladetalle"],
						"idalumno" => $params["idalumno"],
						"curso" => $curso
					], true);
					$arrCursos[$i] = $curso;
				}
				$this->datos = $arrCursos;
			}
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				// echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getAvance($params = array(), $return = false) //recibe un curso y lo devuelve con sus sesiones por unidad
	{ //agrupa por bimestre
		try {
			// $params = [
			// 	"idcurso" => 206,
			// 	"idcomplementario" => 19,
			// 	"idalumno" => 492,
			// 	"curso" => ,
			// ];
			if (isset($_REQUEST["params"]) && @$_REQUEST["params"] != '') {
				$params = json_decode($_REQUEST["params"]);
				unset($_REQUEST);
			}
			if (!is_array($params)) {
				$params = get_object_vars($params);
			}
			$curso = $params["curso"];
			$arrContenido = $this->getContenidoCurso($params, true); //sesiones del curso

			$arrUnidades = [];
			$arrSesionesSueltas = []; //sesiones que no pertenecen a ninguna unidad
			$sesionesCompletadasSueltas = 0;
			$sumaProgresoSinUnidad = 0;
			$sumaProgresoCurso = 0;
			$arrExamenesCurso = [];
			$arrSmartbookCurso = [];
			$arrExamenesSinUnidad = [];
			$arrSmartbookSinUnidad = [];
			foreach ($arrContenido as $i => $contenido) {
				$unidad = [
					"nombre" => $contenido["nombre"],
					"cantSesiones" => count($contenido["hijos"]),
					"progreso" => 0
				];
				$arrExamenes = [];
				$arrSmartbook = [];
				$sumaProgreso = 0;
				$arrSesiones = [];
				$sesionesCompletadas = 0;
				if ($contenido["espadre"] == 1 && count($contenido["hijos"]) > 0) { //solo Contenedores con contenido
					$arrHijos = $contenido["hijos"];
					foreach ($arrHijos as $j => $sesion) { //Cada sesion dentro del contenedor
						$sesionEvaluada = $this->evaluarSesion([
							"sesion" => $sesion,
							"idalumno" => $params["idalumno"],
							"idcurso" => $params["idcurso"],
							"idcomplementario" => $params["idcomplementario"],
							"idgrupoauladetalle" => $params["idgrupoauladetalle"]
						], true);
						if ($sesionEvaluada != null) { //evitar sesiones vacías
							if (isset($sesionEvaluada["esexamen"])) {
								$arrExamenes[] = $sesionEvaluada["esexamen"]["idexamen"];
							} elseif (isset($sesionEvaluada["arrExamenes"])) {
								$arrExamenes = array_merge($arrExamenes, $sesionEvaluada["arrExamenes"]);
							}
							if (isset($sesionEvaluada["essmartbook"])) {
								$arrSmartbook[$sesionEvaluada["essmartbook"]["pv"]][] = $sesionEvaluada["essmartbook"]["idsmartbook"];
							} elseif (isset($sesionEvaluada["arrSmartbook"])) {
								$arrSmartbook = array_merge_recursive($arrSmartbook, $sesionEvaluada["arrSmartbook"]);
							}
							$arrSesiones[] = $sesionEvaluada;
							if ($sesionEvaluada["progreso"] == 100) {
								$sesionesCompletadas++;
							}
							$sumaProgreso += $sesionEvaluada["progreso"];
						}
					}
					$unidad["arrSesiones"] = $arrSesiones;
					$unidad["sesionesCompletadas"] = $sesionesCompletadas;
					if (count($arrSmartbook) > 0) {
						$unidad["arrSmartbook"] = $arrSmartbook;
						$arrSmartbookCurso = array_merge_recursive($arrSmartbookCurso, $arrSmartbook);
					}
					if (count($arrExamenes) > 0) {
						$unidad["arrExamenes"] = $arrExamenes;
						$arrExamenesCurso = array_merge($arrExamenesCurso, $arrExamenes);
					}
					// si la unidad no tuvo sesiones con contenido, no la considero
					if (count($unidad["arrSesiones"]) > 0) {
						$unidad["progreso"] = $sumaProgreso / count($unidad["arrSesiones"]);
						$arrUnidades[] = $unidad;
						$sumaProgresoCurso += $unidad["progreso"];
					}
				} elseif ($contenido["espadre"] == 0) { //sesion simple (sin unidad)
					$sesionEvaluada = $this->evaluarSesion([
						"sesion" => $contenido,
						"idalumno" => $params["idalumno"],
						"idcurso" => $params["idcurso"],
						"idcomplementario" => $params["idcomplementario"],
						"idgrupoauladetalle" => $params["idgrupoauladetalle"]
					], true);
					// if ($params["idcurso"] == 810) {
					// 	// echo json_encode($arrContenido);
					// 	var_dump($sesionEvaluada);
					// 	exit();
					// }
					if ($sesionEvaluada != null) { //evitar sesiones vacías
						if (isset($sesionEvaluada["esexamen"])) {
							$arrExamenesSinUnidad[] = $sesionEvaluada["esexamen"]["idexamen"];
						} elseif (isset($sesionEvaluada["arrExamenes"])) {
							$arrExamenesSinUnidad = array_merge($arrExamenesSinUnidad, $sesionEvaluada["arrExamenes"]);
						}
						if (isset($sesionEvaluada["essmartbook"])) {
							$arrSmartbookSinUnidad[$sesionEvaluada["essmartbook"]["pv"]][] = $sesionEvaluada["essmartbook"]["idsmartbook"];
						} elseif (isset($sesionEvaluada["arrSmartbook"])) {
							$arrSmartbookSinUnidad = array_merge_recursive($arrSmartbookSinUnidad, $sesionEvaluada["arrSmartbook"]);
						}
						$arrSesionesSueltas[] = $sesionEvaluada;
						if ($sesionEvaluada["progreso"] == 100) {
							$sesionesCompletadasSueltas++;
						}
						$sumaProgresoSinUnidad += $sesionEvaluada["progreso"];
					}
				}
			}
			if (count($arrSesionesSueltas) > 0) {
				$sinUnidad = [
					"nombre" => "Sin Unidad",
					"cantSesiones" => count($arrSesionesSueltas),
					"progreso" => $sumaProgresoSinUnidad / count($arrSesionesSueltas),
					"arrSesiones" => $arrSesionesSueltas,
					"sesionesCompletadas" => $sesionesCompletadasSueltas
				];
				if (count($arrSmartbookSinUnidad) > 0) {
					$sinUnidad["arrSmartbook"] = $arrSmartbookSinUnidad;
					$arrSmartbookCurso = array_merge_recursive($arrSmartbookCurso, $arrSmartbookSinUnidad);
				}
				if (count($arrExamenesSinUnidad) > 0) {
					$sinUnidad["arrExamenes"] = $arrExamenesSinUnidad;
					$arrExamenesCurso = array_merge($arrExamenesCurso, $arrExamenesSinUnidad);
				}
				$sumaProgresoCurso += $sinUnidad["progreso"];
				$arrUnidades[] = $sinUnidad;
			}
			if (count($arrUnidades) > 0) {
				$curso["progreso"] = $sumaProgresoCurso / count($arrUnidades);
			} else {
				#Curso dañado: sin sesiones o sesion sin contenido
				$curso["progreso"] = 0;
			}
			$curso["arrUnidades"] = $arrUnidades;
			if (count($arrSmartbookCurso) > 0) {
				$curso["arrSmartbook"] = $arrSmartbookCurso;
			}
			if (count($arrExamenesCurso) > 0) {
				$curso["arrExamenes"] = $arrExamenesCurso;
			}
			$this->datos = $curso;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				// echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function evaluarSesion($params = array(), $return = false) //por aqui pasa cada sesion
	{
		try {
			// $params = ["sesion" => null, "idalumno" => null];
			$sesion = $params["sesion"];
			$idalumno = $params["idalumno"];
			// $params = null;
			$sesion["txtjson"] = (array) json_decode($sesion["txtjson"], true);
			$arrPestanias = [];
			if (isset($sesion["txtjson"]["options"])) {
				$arrPestanias = $sesion["txtjson"]["options"];
			}
			$arrExamenes = [];
			$arrSmartbook = [];
			if (count($arrPestanias) > 0) {
				$sumaProgreso = 0;
				$arrPestaniasEvaluadas = [];

				foreach ($arrPestanias as $i => $pestania) {
					# evaluar contenido de pestaña
					if (
						isset($pestania["type"]) &&
						(!isset($pestania["_vermenu"]) || $pestania["_vermenu"] == "si") #p=>q
					) { //evitar pestañas vacías y ocultas
						$pestania["idsesion"] = $sesion["idcursodetalle"];
						$pestania["idpestania"] = $pestania["id"];
						$pestania = $this->evaluarContenido([
							"contenido" => $pestania,
							"idalumno" => $idalumno,
							"idcurso" => $params["idcurso"],
							"idcomplementario" => $params["idcomplementario"],
							"idgrupoauladetalle" => $params["idgrupoauladetalle"]
						], true);
						if (isset($pestania["esexamen"])) {
							$arrExamenes[] = $pestania["esexamen"]["idexamen"];
						}
						if (isset($pestania["essmartbook"])) {
							$arrSmartbook[$pestania["essmartbook"]["pv"]][] = $pestania["essmartbook"]["idsmartbook"];
						}
						$sumaProgreso += $pestania["progreso"];
						$arrPestaniasEvaluadas[] = $pestania;
					}
				}
				if (count($arrSmartbook) > 0) {
					$sesion["arrSmartbook"] = $arrSmartbook;
				}
				if (count($arrExamenes) > 0) {
					$sesion["arrExamenes"] = $arrExamenes;
				}
				//puede que las pestañas no tengan contenido: de ser asi le estoy poniendo 100
				$sesion["progreso"] = count($arrPestaniasEvaluadas) > 0 ? $sumaProgreso / count($arrPestaniasEvaluadas) : 100;
				$sesion["arrPestanias"] = $arrPestaniasEvaluadas; //solo demostrativo; QUITAR
			} else {
				# evaluar contenido de sesion
				if (
					isset($sesion["txtjson"]["typelink"]) &&
					(!isset($sesion["txtjson"]["_vermenu"]) || $sesion["txtjson"]["_vermenu"] == "si") #p=>q
				) { //evitar menus vacíos y ocultos
					$sesion["txtjson"]["idsesion"] = $sesion["idcursodetalle"];
					$sesion["txtjson"]["idpestania"] = 0;
					$sesion = $this->evaluarContenido([
						"contenido" => $sesion,
						"idalumno" => $idalumno,
						"idcurso" => $params["idcurso"],
						"idcomplementario" => $params["idcomplementario"],
						"idgrupoauladetalle" => $params["idgrupoauladetalle"]
					], true);
					if (isset($sesion["esexamen"])) {
						$arrExamenes[] = $sesion["esexamen"]["idexamen"];
					}
					if (isset($sesion["essmartbook"])) {
						$arrSmartbook[$sesion["essmartbook"]["pv"]][] = $sesion["essmartbook"]["idsmartbook"];
					}
					// $sesion["progreso"] = $rs["progreso"];
				} else {
					$sesion = null;
				}
			}
			if (count($arrSmartbook) > 0) {
				$sesion["arrSmartbook"] = $arrSmartbook;
			}
			if (count($arrExamenes) > 0) {
				$sesion["arrExamenes"] = $arrExamenes;
			}
			$this->datos = $sesion;
			if (!$return) {
				// echo json_encode(array('code' => 200, 'data' => $this->datos));
				echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function evaluarContenido($params = array(), $return = false) //por aquí pasa cada pestaña o sesion sin pestañas (txtjson)
	{
		try { //curso + alumno => matricula
			$oUsuario = NegSesion::getUsuario();
			// $params = ["contenido" => null, "idalumno" => null];
			$contenido = $params["contenido"]; //puede ser menu sin hijos o pestaña
			// $params = null;
			if (isset($contenido["txtjson"])) {
				$contenido = $contenido["txtjson"];
			}
			$tipo = isset($contenido["typelink"]) ? $contenido["typelink"] : $contenido["type"]; //menu o pestaña, en ese orden
			if (isset($contenido["estarea"])&&($contenido["estarea"] == "si" || $contenido["esproyecto"] == "si")) {
				$tipo = "estareaproyecto";
			}
			$progreso = 0;
			// $arrHabilidades = null;
			switch ($tipo) {
				case 'smartquiz':
					$query = null;
					$parts = parse_url($contenido["link"]);
					parse_str($parts['query'], $query);
					$idexamen = $query['idexamen'];
					$params["contenido"]["esexamen"] = [
						"idexamen" => $idexamen
					];
					$rs = $this->oWebMine->getData([
						"tabla" => "notas_quiz",
						"arrCampos" => ["idnota"],
						"arrFiltros" => [
							"idrecurso" => $idexamen,
							"idalumno" => $params["idalumno"],
							#mine-descomentar
							"idgrupoauladetalle" => $params["idgrupoauladetalle"]
						]
					], true);
					if (count($rs) > 0) {
						$progreso = 100;
					}
					break;
				case 'smartenglish': //smartbook
					$query = null;
					$parts = parse_url($contenido["link"]);
					parse_str($parts['query'], $query);
					$idrecurso = $query['idrecurso'];
					$bd_se = $query['PV'] == "PVADU_1" ? NAME_BDSEAdultos : NAME_BDSEAdolecentes;
					$params["contenido"]["essmartbook"] = [
						"idsmartbook" => $idrecurso,
						"pv" => $query['PV'] == "PVADU_1" ? "adultos" : "adolescentes"
					];
					$this->oNegActividad_alumno = new NegActividad_alumno;
					$arrRs = $this->oNegActividad_alumno->getPorcentajeSmartbookAlumno([
						'idproyecto' => $oUsuario["idproyecto"],
						'idcurso' => $params["idcurso"],
						'idcomplementario' => $params["idcomplementario"],
						"idgrupoauladetalle" => $params["idgrupoauladetalle"],
						"idrecurso" => $idrecurso,
						"idalumno" => $params["idalumno"],
						"bd_se" => $bd_se,
					]);
					if (count($arrRs) > 0) {
						$progreso = $arrRs[0]["progreso_smartbook"];
					} else {
						#el smartbook no existe??
						$progreso = 0;
					}

					break;
				case 'estareaproyecto':
					$rs = $this->oWebMine->getData([
						"tabla" => "tareas",
						"arrCampos" => ["idtarea"],
						"arrFiltros" => [
							"idsesion" => $contenido["idsesion"],
							"idpestania" => $contenido["idpestania"],
							"idalumno" => $params["idalumno"],
							"idcurso" => $params["idcurso"],
							"idcomplementario" => $params["idcomplementario"],
							#mine-descomentar
							"idgrupoauladetalle" => $params["idgrupoauladetalle"]
						]
					], true);
					if (count($rs) > 0) {
						$progreso = 100;
					}
					break;
				case 'foro':
					$confForo = json_decode(base64_decode($contenido["link"]), true);
					$idpublicacion = $confForo["idPublicacionForo"];
					$rs = $this->oWebMine->getData([
						"tabla" => "foro_comentario",
						"arrCampos" => ["idcomentario"],
						"arrFiltros" => [
							"idpersona" => $params["idalumno"],
							"idpublicacion" => $idpublicacion,
						]
					], true);
					if (count($rs) > 0) {
						$progreso = 100;
					}
					break;
				default: //pdf o cualquier cosa que sea lectura, para traer los clicks
					$filtros = [
						"idsesion" => $contenido["idsesion"],
						"idcurso" => $params["idcurso"],
						"idusuario" => $params["idalumno"],
						"idcomplementario" => $params["idcomplementario"],
						"idgrupoauladetalle" => $params["idgrupoauladetalle"]
					];
					if ($contenido["idpestania"] != 0) {
						$filtros["idsesionB"] = $contenido["idpestania"];
					}
					$rs = $this->oNegBitacora_smartbook->getProgresoBitacoraSesiones($filtros);
					if (count($rs) > 0) {
						// echo json_encode($rs);exit();
						$progreso = $rs[0]["progreso"];
					}
					break;
			}
			$params["contenido"]["progreso"] = $progreso;
			$this->datos = $params["contenido"];
			if (!$return) {
				// echo json_encode(array('code' => 200, 'data' => $this->datos));
				echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	/**
	 * Funcion que se encarga de retornar las competencias, capacidades y criterios de un curso, ademas del progreso por habilidades
	 * NOTA: por ahora es contemplado por curso principal mas no extension.
	 * @param Bool returns, retornara un TRUE si se quiere retornar como arreglo o FALSE si se quiere retornar como JSON
	 * @param Array parametros por defecto si se llama dentro del mismo webreportemodular.php
	 * @param Array $_REQUEST por GET o POST
	 * @return Object JSON o Array segun el parametro de retorno
	 */
	public function getCompetencias($params = ['idcurso'], $return = false)
	{
		try {
			//obtener los parametros
			$params = WebMine::getParams($params);

			$oUsuario = NegSesion::getUsuario(); //obtener sesion del usuario actual

			$arrCompetencias = array(); //arreglo principal
			$filtros = array(); //filtro de busqueda

			if (!empty($params['idcurso'])) $filtros['idcurso'] = $params['idcurso'];
			if (!empty($params['idproyecto'])) $filtros['idproyecto'] = $params['idproyecto'];
			if (!empty($params['estado'])) $filtros['estado'] = $params['estado'];
			$filtros['idcategoria'] = isset($params['idcategoria']) ? $params['idcategoria'] : 0;

			$rs_competencias = $this->oNegAcad_competencias->competencias($filtros);
			if (!empty($params['arrExamenes']) || !empty($params['arrSmartbook'])) {
				$parametros = array(
					'idcurso' => $params['idcurso'],
					'idalumno' => $params['idalumno']
				);
				if (isset($params['idcomplementario'])) {
					$parametros['idcomplementario'] = $params['idcomplementario'];
				}
				if (isset($params['idgrupoauladetalle'])) {
					$parametros['idgrupoauladetalle'] = $params['idgrupoauladetalle'];
				}

				if (!empty($params['arrExamenes'])) {
					$parametros['arrExamenes'] = $params['arrExamenes'];
				} else if (!empty($params['arrSmartbook'])) {
					$parametros['arrSmartbook'] = $params['arrSmartbook'];
				}

				$rs_progresohabilidad = $this->getProgresoHabilidad($parametros, true);
				if (!empty($rs_progresohabilidad) && !empty($rs_competencias)) {
					foreach ($rs_competencias as $key => $competencia) {
						$rs_competencias[$key]['progreso'] = 0;
						if (empty($competencia['capacidades'])) {
							continue;
						}
						//obtener todos los criterios de la competencia
						$habilidades_competencia = [];
						foreach ($competencia['capacidades'] as $capacidad) {
							$criterios = array_unique(array_column($capacidad['criterios'], 'nombre'));
							$criterios = array_map('strtolower', $criterios);
							$habilidades_competencia = array_flip(array_merge(array_flip($habilidades_competencia), array_flip($criterios)));
						}
						if (!empty($habilidades_competencia)) {
							$sumatoria = 0;
							$cantidad = 0;
							foreach ($rs_progresohabilidad as $progresohabilidad) {
								if (in_array(strtolower($progresohabilidad['skill_name']), $habilidades_competencia)) {
									$cantidad++;
									$sumatoria += $progresohabilidad['progreso'];
								}
							} //endforeach
							if ($sumatoria > 0) {
								$rs_competencias[$key]['progreso'] = ($sumatoria / $cantidad);
							}
						}
					}
				}
			}
			$arrCompetencias = $rs_competencias;

			$this->datos = $arrCompetencias;

			//Data de salida
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	/**
	 * Funcion que retorna todos los grupos y cursos asociados de un usuario, docente o de un proyecto y empresa determinado
	 * Esta funcion es una version extendida de getCursosUsuario de WebReportes, en el cual se mantiene la logica original pero con mas opciones para filtrar y variables
	 * NOTA: por ahora es contemplado por curso principal mas no extension.
	 * @param Array $_REQUEST por GET o POST
	 * @return Object JSON o Array segun el parametro de retorno
	 */
	// public function getCursosUsuarioExtendido(){
	// 	try{
			
	// 		$filtros = array();
	// 		$arrCursos = array();
			
	// 		$_idrol = !empty($_REQUEST['idrol']) ? $_REQUEST['idrol'] : NegSesion::getUsuario()["idrol"];
	// 		$_idpersona = !empty($_REQUEST['idpersona']) ? $_REQUEST['idpersona'] : NegSesion::getUsuario()["idpersona"];
	// 		$_idproyecto = !empty($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : NegSesion::getUsuario()["idproyecto"];

	// 		if ($_idrol == 2) {

	// 		}elseif($_idrol == 3){

	// 		}elseif($_idrol == 3){

	// 		}
			
	// 		echo json_encode(array('code' => 200, 'data' => $this->datos));
	// 		exit(0);
	// 	}catch(Exception $e){
	// 		echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
	// 		exit(0);
	// 	}
	// }
	public function getProgresoHabilidad($params = ["idcurso", "idcoplementario", "idalumno", "arrSmartbook", "arrExamenes"], $return = false)
	{ //recibe menu/pestaña y devuelve habilidades
		try {
			$params = WebMine::getParams($params);
			$oUsuario = NegSesion::getUsuario();
			$arrHabilidadesSB = [];
			$arrHabilidadesExamen = [];
			$arrHabilidadesSBEMerge = [];
			$arrHabilidades = []; //TODAS

			if (isset($params["arrSmartbook"])) {
				$arrSmartbook = $params["arrSmartbook"];
				// echo json_encode($params);exit();
				$paramsSmartbook = [
					"idalumno" => $params["idalumno"],
					"idcurso" =>	$params["idcurso"],
					"idcomplementario" => $params["idcomplementario"],
				];
				if (isset($params["idgrupoauladetalle"]) && !empty($params["idgrupoauladetalle"])) {
					$paramsSmartbook["idgrupoauladetalle"] = $params["idgrupoauladetalle"];
				}
				if (isset($arrSmartbook["adultos"])) {
					$paramsSmartbook["idrecurso"] = $arrSmartbook["adultos"];
					$paramsSmartbook["PV"] = "PVADU_1";
				} elseif (isset($arrSmartbook["adolescentes"])) {
					$paramsSmartbook["idrecurso"] = $arrSmartbook["adolescentes"];
					$paramsSmartbook["PV"] = "idk";
				}
				$arrHabilidadesSB = $this->getProgresoHabilidadSmartbook($paramsSmartbook, true);
			}
			$paramsExamen = [
				"idalumno" => $params["idalumno"],
			];
			if (isset($params["idgrupoauladetalle"]) && !empty($params["idgrupoauladetalle"])) {
				$paramsExamen["idgrupoauladetalle"] = $params["idgrupoauladetalle"];
			}
			if (isset($params["arrExamenes"])) {
				$paramsExamen["idexamen"] = $params["arrExamenes"];
				$arrHabilidadesExamen = $this->getProgresoHabilidadExamen($paramsExamen, true);
			}
			if (count($arrHabilidadesSB) == 0) {
				if (count($arrHabilidadesExamen) > 0) {
					$arrHabilidadesSBEMerge = $arrHabilidadesExamen;
				}
			} elseif (count($arrHabilidadesExamen) == 0) {
				if (count($arrHabilidadesSB) > 0) {
					$arrHabilidadesSBEMerge = $arrHabilidadesSB;
				}
			} else {
				$arrHabilidadesMergeI = []; //INTERSECCIÓN
				foreach ($arrHabilidadesSB as $i => $habilidadSB) {
					foreach ($arrHabilidadesExamen as $j => $habilidadEX) {
						if (strcasecmp($habilidadSB["skill_name"], $habilidadEX["skill_name"]) == 0) {
							$arrHabilidadesMergeI[] = [
								"skill_name" => $habilidadSB["skill_name"],
								"progreso" => (float) ($habilidadSB["progreso"] + $habilidadEX["progreso"]) / 2
							];
						}
					}
				}

				$arrHabilidadesMergeL = []; //Izquierda
				foreach ($arrHabilidadesSB as $i => $habilidadSB) {
					$found = false;
					foreach ($arrHabilidadesMergeI as $j => $habilidadI) {
						if (strcasecmp($habilidadSB["skill_name"], $habilidadI["skill_name"]) == 0) {
							$found = true;
						}
					}
					if (!$found) {
						$arrHabilidadesMergeL[] = [
							"skill_name" => $habilidadSB["skill_name"],
							"progreso" => $habilidadSB["progreso"]
						];
					}
				}

				$arrHabilidadesMergeR = []; //Derecha
				foreach ($arrHabilidadesExamen as $i => $habilidadEX) {
					$found = false;
					foreach ($arrHabilidadesMergeI as $j => $habilidadI) {
						if (strcasecmp($habilidadEX["skill_name"], $habilidadI["skill_name"]) == 0) {
							$found = true;
						}
					}
					if (!$found) {
						$arrHabilidadesMergeR[] = [
							"skill_name" => $habilidadEX["skill_name"],
							"progreso" => $habilidadEX["progreso"]
						];
					}
				}
				$arrHabilidadesSBEMerge = array_merge($arrHabilidadesMergeL, $arrHabilidadesMergeI, $arrHabilidadesMergeR);
			}

			if (isset($params["merge"]) && $params["merge"] == true && isset($params["idcurso"])) {
				$arrHabilidadesCurso = $this->oWebMine->getData([
					"tabla" => "acad_criterios",
					"arrCampos" => ["idcriterio", "nombre as skill_name, 'null' as progreso"],
					"arrFiltros" => [
						"idcurso" => $params["idcurso"],
						"estado" => 1,
						"idproyecto" => $oUsuario["idproyecto"]
					]
				], true);
				if (count($arrHabilidadesCurso) == 0) {
					if (count($arrHabilidadesSBEMerge) > 0) {
						$arrHabilidades = $arrHabilidadesSBEMerge;
					}
				} elseif (count($arrHabilidadesSBEMerge) == 0) {
					if (count($arrHabilidadesCurso) > 0) {
						$arrHabilidades = $arrHabilidadesCurso;
					}
				} else { // hay de curso y SBE
					$arrHabilidadesMergeI = []; //INTERSECCIÓN
					foreach ($arrHabilidadesSBEMerge as $i => $habilidadSBE) {
						foreach ($arrHabilidadesCurso as $j => $habilidadCurso) {
							if (strcasecmp($habilidadSBE["skill_name"], $habilidadCurso["skill_name"]) == 0) {
								$arrHabilidadesMergeI[] = [
									"skill_name" => $habilidadSBE["skill_name"],
									"progreso" => (float) $habilidadSBE["progreso"]
								];
							}
						}
					}
					$arrHabilidadesMergeL = []; //Izquierda
					foreach ($arrHabilidadesSBEMerge as $i => $habilidadSBE) {
						$found = false;
						foreach ($arrHabilidadesMergeI as $j => $habilidadI) {
							if (strcasecmp($habilidadSBE["skill_name"], $habilidadI["skill_name"]) == 0) {
								$found = true;
							}
						}
						if (!$found) {
							$arrHabilidadesMergeL[] = [
								"skill_name" => $habilidadSBE["skill_name"],
								"progreso" => $habilidadSBE["progreso"]
							];
						}
					}
					$arrHabilidadesMergeR = []; //Derecha
					foreach ($arrHabilidadesCurso as $i => $habilidadCurso) {
						$found = false;
						foreach ($arrHabilidadesMergeI as $j => $habilidadI) {
							if (strcasecmp($habilidadCurso["skill_name"], $habilidadI["skill_name"]) == 0) {
								$found = true;
							}
						}
						if (!$found) {
							$arrHabilidadesMergeR[] = [
								"skill_name" => $habilidadCurso["skill_name"],
								"progreso" => null
							];
						}
					}
					$arrHabilidades = array_merge($arrHabilidadesMergeL, $arrHabilidadesMergeI, $arrHabilidadesMergeR);
				}
			} else {
				$arrHabilidades = $arrHabilidadesSBEMerge;
			}
			$this->datos = $arrHabilidades;
			// $this->datos = $arrHabilidadesSB;
			// $this->datos = $arrHabilidadesExamen;

			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getProgresoHabilidadExamen($params = ["idexamen", "idalumno"], $return = false)
	{ //recibe menu/pestaña y devuelve habilidades
		try {
			$params = WebMine::getParams($params);

			$this->datos = $this->oNegReporteModular->getProgresoHabilidadExamen($params);

			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				// echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getProgresoHabilidadSmartbook($params = ["idrecurso", "idalumno", "PV"], $return = false)
	{ //recibe menu/pestaña y devuelve habilidades
		try {
			//$this->getHabilidadSmartbook([
			// 	"idrecurso" => $idrecurso,
			// 	'idalumno' => $params["idalumno"],
			// 	"PV" => $PV
			// ], true);
			$oUsuario = NegSesion::getUsuario();
			$params = WebMine::getParams($params);
			$bd_se = $params["PV"] == "PVADU_1" ? NAME_BDSEAdultos : NAME_BDSEAdolecentes;
			$this->oNegActividad_alumno = new NegActividad_alumno;
			$params["idproyecto"] = $oUsuario["idproyecto"];
			$params["bd_se"] = $bd_se;
			$arrHabilidades = $this->oNegActividad_alumno->getProgresoHabilidadesSB($params);
			$params["concatenado"] = true;
			$arrHabilidadesConcat = $this->oNegActividad_alumno->getProgresoHabilidadesSB($params);
			foreach ($arrHabilidadesConcat as $i => $habilidadConcat) {
				$arrIdHabilidades = explode('|', $habilidadConcat["skill_id"]);
				foreach ($arrIdHabilidades as $j => $idhabilidad) {
					$key = array_search($idhabilidad, array_column($arrHabilidades, 'skill_id'));
					$cantidad = $arrHabilidades[$key]["cantidad"];
					$progreso = $arrHabilidades[$key]["progreso"];
					$arrHabilidades[$key]["progreso"] = round((float)((float)$progreso * $cantidad + (float)$habilidadConcat["progreso"]) / ($cantidad + 1), 2);
					$arrHabilidades[$key]["cantidad"] = $cantidad + 1;
				}
			}
			#Todos tanto practice como dby
			$this->datos = $arrHabilidades;
			#$this->datos = $arrPractice['act']['det'];

			#array_merge($arrPractice,$arrDBY);
			if (!$return) {
				// echo json_encode(array('code' => 200, 'data' => $this->datos));
				echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
}
