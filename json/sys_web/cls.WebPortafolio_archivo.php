<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-05-2020 
 * @copyright	Copyright (C) 04-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPortafolio_archivo', RUTA_BASE);
class WebPortafolio_archivo extends JrWeb
{
	private $oNegPortafolio_archivo;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPortafolio_archivo = new NegPortafolio_archivo;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Portafolio_archivo', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["idfolder"])&&@$_REQUEST["idfolder"]!='')$filtros["idfolder"]=$_REQUEST["idfolder"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["ubicacion"])&&@$_REQUEST["ubicacion"]!='')$filtros["ubicacion"]=$_REQUEST["ubicacion"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["extension"])&&@$_REQUEST["extension"]!='')$filtros["extension"]=$_REQUEST["extension"];
			if(isset($_REQUEST["idportafolio"])&&@$_REQUEST["idportafolio"]!='')$filtros["idportafolio"]=$_REQUEST["idportafolio"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegPortafolio_archivo->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idarchivo)) {
				$this->oNegPortafolio_archivo->idarchivo = $idarchivo;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegPortafolio_archivo->idfolder=@$idfolder;
				$this->oNegPortafolio_archivo->nombre=@$nombre;
				$this->oNegPortafolio_archivo->ubicacion=@$ubicacion;
				$this->oNegPortafolio_archivo->tipo=@$tipo;
				$this->oNegPortafolio_archivo->extension=@$extension;
				$this->oNegPortafolio_archivo->idportafolio=@$idportafolio;
				
            if($accion=='_add') {
            	$res=$this->oNegPortafolio_archivo->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Portafolio_archivo')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPortafolio_archivo->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Portafolio_archivo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegPortafolio_archivo->idfolder=@$idfolder;
				$this->oNegPortafolio_archivo->nombre=@$nombre;
				$this->oNegPortafolio_archivo->ubicacion=@$ubicacion;
				$this->oNegPortafolio_archivo->tipo=@$tipo;
				$this->oNegPortafolio_archivo->extension=@$extension;
				$this->oNegPortafolio_archivo->idportafolio=@$idportafolio;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPortafolio_archivo->__set('idarchivo', $_REQUEST['idarchivo']);
			$res=$this->oNegPortafolio_archivo->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPortafolio_archivo->setCampo($_REQUEST['idarchivo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}