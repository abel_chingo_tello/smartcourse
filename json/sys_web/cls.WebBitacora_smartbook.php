<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-11-2019 
 * @copyright	Copyright (C) 28-11-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE);
class WebBitacora_smartbook extends JrWeb
{
	private $oNegBitacora_smartbook;
	private $oNegBitacora_alumno_smartbook;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBitacora_smartbook = new NegBitacora_smartbook;
		$this->oNegBitacora_alumno_smartbook = new NegBitacora_alumno_smartbook;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bitacora_smartbook', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idbitacora_alum_smartbook"])&&@$_REQUEST["idbitacora_alum_smartbook"]!='')$filtros["idbitacora_alum_smartbook"]=$_REQUEST["idbitacora_alum_smartbook"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idsesion"])&&@$_REQUEST["idsesion"]!='')$filtros["idsesion"]=$_REQUEST["idsesion"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["pestania"])&&@$_REQUEST["pestania"]!='')$filtros["pestania"]=$_REQUEST["pestania"];
			if(isset($_REQUEST["total_pestanias"])&&@$_REQUEST["total_pestanias"]!='')$filtros["total_pestanias"]=$_REQUEST["total_pestanias"];
			if(isset($_REQUEST["fechahora"])&&@$_REQUEST["fechahora"]!='')$filtros["fechahora"]=$_REQUEST["fechahora"];
			if(isset($_REQUEST["progreso"])&&@$_REQUEST["progreso"]!='')$filtros["progreso"]=$_REQUEST["progreso"];
			if(isset($_REQUEST["idcomplementario"])&&@$_REQUEST["idcomplementario"]!='')$filtros["idcomplementario"]=$_REQUEST["idcomplementario"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["otros_datos"])&&@$_REQUEST["otros_datos"]!='')$filtros["otros_datos"]=$_REQUEST["otros_datos"];
			if($filtros["idusuario"]=='yo'){
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idusuario"]=$usuarioAct["idpersona"];
			}			
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegBitacora_smartbook->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if(NegSesion::existeSesion()==false){
				echo json_encode(array('code' => 'Error','msj'=>'Sesion ha sido cerrado'));
				exit();
			}

			$usuarioAct = NegSesion::getUsuario();
           	if($usuarioAct["idrol"]!=3){
            	echo json_encode(array('code'=>200,'msj'=>'Solo guardamos bitacoras  para el Alumno.')); 
            	exit();
            }

            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);            
           

			$idusuario=!empty($idusuario)?$idusuario:@$usuarioAct["idpersona"];
			$procesar=true;      
			$this->oNegBitacora_smartbook->idbitacora_alum_smartbook=@$idbitacora_alum_smartbook;
			$this->oNegBitacora_smartbook->idcurso=$idcurso;
			$this->oNegBitacora_smartbook->idsesion=$idsesion;
			$this->oNegBitacora_smartbook->idsesionB=$idsesionB;
			$this->oNegBitacora_smartbook->idusuario=$idusuario;
			$this->oNegBitacora_smartbook->idcomplementario=@$idcomplementario;
			$this->oNegBitacora_smartbook->pestania=!empty($pestania)?$pestania:'';
			$this->oNegBitacora_smartbook->total_pestanias=!empty($total_pestanias)?$total_pestanias:1;
			$this->oNegBitacora_smartbook->fechahora=!empty($regfecha)?$regfecha:date('Y-m-d H:i:s');
			$this->oNegBitacora_smartbook->progreso=!empty($progreso)?$progreso:100;
			$this->oNegBitacora_smartbook->otros_datos=!empty($otros_datos)?$otros_datos:'';
			$this->oNegBitacora_smartbook->idgrupoauladetalle=@$idgrupoauladetalle;	           
            $res=$this->oNegBitacora_smartbook->agregar();	            	
            $rr=array('code'=>200,'msj'=>ucfirst(JrTexto::_('Bitacora_smartbook')).' '.JrTexto::_('saved successfully'),'newid'=>$res); 
        	$n=0;
        	$tp=1;
        	$hay2=$this->oNegBitacora_smartbook->buscar(array('idcurso'=>$idcurso,'idsesion'=>$idsesion,'idbitacora_alum_smartbook'=>$idbitacora_alum_smartbook,'idusuario'=>$idusuario,'idcomplementario'=>@$idcomplementario,'idgrupoauladetalle'=>@$idgrupoauladetalle));
        	if(!empty($hay2)){         	
            	foreach ($hay2 as $v){
	            	if($v["progreso"]==100){
	            		$n++;
	            	}
		            if($v["total_pestanias"]>$tp)$tp=$v["total_pestanias"];
            	}
            }
        	$rr["termino"]=($n==$tp)?true:false;
        	if($rr["termino"]==true){
        		$this->oNegBitacora_alumno_smartbook->idcurso=@$idcurso;
				$this->oNegBitacora_alumno_smartbook->idsesion=@$idsesion;
				$this->oNegBitacora_alumno_smartbook->idusuario=$idusuario;
				$this->oNegBitacora_alumno_smartbook->idcomplementario=@$idcomplementario;
				$this->oNegBitacora_alumno_smartbook->idgrupoauladetalle=@$idgrupoauladetalle;
				$this->oNegBitacora_alumno_smartbook->estado='T';
				$this->oNegBitacora_alumno_smartbook->regfecha=date('Y-m-d H:i:s');
				$this->oNegBitacora_alumno_smartbook->agregar();
        	}
        	$rr["progreso"]=$progreso;
            echo json_encode($rr),			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegBitacora_smartbook->idbitacora_alum_smartbook=@$idbitacora_alum_smartbook;
					$this->oNegBitacora_smartbook->idcurso=@$idcurso;
					$this->oNegBitacora_smartbook->idsesion=@$idsesion;
					$this->oNegBitacora_smartbook->idusuario=@$idusuario;
					$this->oNegBitacora_smartbook->pestania=@$pestania;
					$this->oNegBitacora_smartbook->total_pestanias=@$total_pestanias;
					$this->oNegBitacora_smartbook->fechahora=@$fechahora;
					$this->oNegBitacora_smartbook->progreso=@$progreso;
					$this->oNegBitacora_smartbook->otros_datos=@$otros_datos;
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegBitacora_smartbook->__set('idbitacora', $_REQUEST['idbitacora']);
			$res=$this->oNegBitacora_smartbook->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegBitacora_smartbook->setCampo($_REQUEST['idbitacora'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}