<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAporte_respuesta', RUTA_BASE);
class WebAporte_respuesta extends JrWeb
{
	private $oNegAporte_respuesta;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAporte_respuesta = new NegAporte_respuesta;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Aporte_respuesta', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["personal"])&&@$_REQUEST["personal"]!='')$filtros["personal"]=$_REQUEST["personal"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["contenido"])&&@$_REQUEST["contenido"]!='')$filtros["contenido"]=$_REQUEST["contenido"];
			if(isset($_REQUEST["archivo"])&&@$_REQUEST["archivo"]!='')$filtros["archivo"]=$_REQUEST["archivo"];
			if(isset($_REQUEST["fecha_hora"])&&@$_REQUEST["fecha_hora"]!='')$filtros["fecha_hora"]=$_REQUEST["fecha_hora"];
			if(isset($_REQUEST["respuesta"])&&@$_REQUEST["respuesta"]!='')$filtros["respuesta"]=$_REQUEST["respuesta"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAporte_respuesta->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$codigo)) {
				$this->oNegAporte_respuesta->codigo = $codigo;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegAporte_respuesta->idcurso=@$idcurso;
				$this->oNegAporte_respuesta->personal=@$personal;
				$this->oNegAporte_respuesta->titulo=@$titulo;
				$this->oNegAporte_respuesta->contenido=@$contenido;
				$this->oNegAporte_respuesta->archivo=@$archivo;
				$this->oNegAporte_respuesta->fecha_hora=@$fecha_hora;
				$this->oNegAporte_respuesta->respuesta=@$respuesta;
				
            if($accion=='_add') {
            	$res=$this->oNegAporte_respuesta->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Aporte_respuesta')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAporte_respuesta->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Aporte_respuesta')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAporte_respuesta->__set('codigo', $_REQUEST['codigo']);
			$res=$this->oNegAporte_respuesta->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAporte_respuesta->setCampo($_REQUEST['codigo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}