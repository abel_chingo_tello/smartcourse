<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRub_pregunta', RUTA_BASE);
class WebRub_pregunta extends JrWeb
{
	private $oNegRub_pregunta;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRub_pregunta = new NegRub_pregunta;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Rub_pregunta', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["otros_datos"])&&@$_REQUEST["otros_datos"]!='')$filtros["otros_datos"]=$_REQUEST["otros_datos"];
			if(isset($_REQUEST["id_estandar"])&&@$_REQUEST["id_estandar"]!='')$filtros["id_estandar"]=$_REQUEST["id_estandar"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["activo"])&&@$_REQUEST["activo"]!='')$filtros["activo"]=$_REQUEST["activo"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRub_pregunta->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id_pregunta)) {
				$this->oNegRub_pregunta->id_pregunta = $id_pregunta;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRub_pregunta->nombre=@$nombre;
				$this->oNegRub_pregunta->otros_datos=@$otros_datos;
				$this->oNegRub_pregunta->id_estandar=@$id_estandar;
				$this->oNegRub_pregunta->tipo=@$tipo;
				$this->oNegRub_pregunta->activo=@$activo;
				
            if($accion=='_add') {
            	$res=$this->oNegRub_pregunta->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_pregunta')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRub_pregunta->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_pregunta')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRub_pregunta->__set('id_pregunta', $_REQUEST['id_pregunta']);
			$res=$this->oNegRub_pregunta->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRub_pregunta->setCampo($_REQUEST['id_pregunta'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}