<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020 
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRubrica_indicador_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRubrica_indicador', RUTA_BASE);
class WebRubrica_indicador_alumno extends JrWeb
{
	private $oNegRubrica_indicador_alumno;
	private $oNegRubrica_indicador;
	public function __construct()
	{
		parent::__construct();
		$this->oNegRubrica_indicador_alumno = new NegRubrica_indicador_alumno;
		$this->oNegRubrica_indicador = new NegRubrica_indicador;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Rubrica_indicador_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idrubrica_indicador_alumno"]) && @$_REQUEST["idrubrica_indicador_alumno"] != '') $filtros["idrubrica_indicador_alumno"] = $_REQUEST["idrubrica_indicador_alumno"];
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["idrubrica_indicador"]) && @$_REQUEST["idrubrica_indicador"] != '') $filtros["idrubrica_indicador"] = $_REQUEST["idrubrica_indicador"];
			if (isset($_REQUEST["idrubrica"]) && @$_REQUEST["idrubrica"] != '') $filtros["idrubrica"] = $_REQUEST["idrubrica"];
			if (isset($_REQUEST["idrubrica_instancia"]) && @$_REQUEST["idrubrica_instancia"] != '') $filtros["idrubrica_instancia"] = $_REQUEST["idrubrica_instancia"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegRubrica_indicador_alumno->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			//mine
			$res = 'eliminado';
			$arrIndicadorAlumno = json_decode(file_get_contents("php://input"), true);
			// print_r($arrIndicadorAlumno);exit();
			foreach ($arrIndicadorAlumno as $key => $newIndicadorAlumno) {
				//obtengo el indicador del newIndicadorAlumno
				$filtros = array(
					'idrubrica_indicador' => $newIndicadorAlumno['idrubrica_indicador'],
					'idrubrica' => $newIndicadorAlumno['idrubrica'],
				);
				$indicador = $this->oNegRubrica_indicador->buscar($filtros)[0];

				//obtengo todos los indicadorAlumno ya existentes, con el idcriterio en arrIndicadorAlumnoC
				$filtros = array(
					'idalumno' => $newIndicadorAlumno['idalumno'],
					'idrubrica' => $newIndicadorAlumno['idrubrica'],
					'idrubrica_instancia' => $newIndicadorAlumno['idrubrica_instancia'],
					'idgrupoauladetalle' => $newIndicadorAlumno['idgrupoauladetalle'],
					'idrubrica_criterio' => $indicador['idrubrica_criterio']
				);
				$arrIndicadorAlumnoC = $this->oNegRubrica_indicador_alumno->buscarCriterio($filtros);
				// print_r($arrIndicadorAlumnoC);exit();
				foreach ($arrIndicadorAlumnoC as $key => $IndicadorAlumnoC) {
					//comparo si tienen el mismo idcriterio
					if ($IndicadorAlumnoC['idrubrica_criterio'] == $indicador['idrubrica_criterio']) {
						$this->oNegRubrica_indicador_alumno->idrubrica_indicador_alumno = $IndicadorAlumnoC['idrubrica_indicador_alumno'];
						$this->oNegRubrica_indicador_alumno->eliminar();
					}
				}
				$this->oNegRubrica_indicador_alumno->idalumno = $newIndicadorAlumno['idalumno'];
				$this->oNegRubrica_indicador_alumno->idrubrica_indicador = $newIndicadorAlumno['idrubrica_indicador'];
				$this->oNegRubrica_indicador_alumno->idrubrica = $newIndicadorAlumno['idrubrica'];
				$this->oNegRubrica_indicador_alumno->idrubrica_instancia = $newIndicadorAlumno['idrubrica_instancia'];
				$this->oNegRubrica_indicador_alumno->idgrupoauladetalle = $newIndicadorAlumno['idgrupoauladetalle'];
				$res = $this->oNegRubrica_indicador_alumno->agregar();
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica_indicador_alumno')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegRubrica_indicador_alumno->idalumno = @$idalumno;
					$this->oNegRubrica_indicador_alumno->idrubrica_indicador = @$idrubrica_indicador;
					$this->oNegRubrica_indicador_alumno->idrubrica = @$idrubrica;
					$this->oNegRubrica_indicador_alumno->rubrica_instancia_idrubrica_instancia = @$rubrica_instancia_idrubrica_instancia;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function eliminarPorInstancia()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Rubrica_indicador_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idrubrica"]) && @$_REQUEST["idrubrica"] != '') $filtros["idrubrica"] = $_REQUEST["idrubrica"];
			if (isset($_REQUEST["idrubrica_instancia"]) && @$_REQUEST["idrubrica_instancia"] != '') $filtros["idrubrica_instancia"] = $_REQUEST["idrubrica_instancia"];

			$arrIndicadorAlumno = $this->oNegRubrica_indicador_alumno->buscar($filtros);
			foreach ($arrIndicadorAlumno as $key => $indicadorAlumno) {
				$this->oNegRubrica_indicador_alumno->idrubrica_indicador_alumno = $indicadorAlumno['idrubrica_indicador_alumno'];
				$this->oNegRubrica_indicador_alumno->eliminar();
			}
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			$this->oNegRubrica_indicador_alumno->__set('idrubrica_indicador_alumno', $_REQUEST['idrubrica_indicador_alumno']);
			$res = $this->oNegRubrica_indicador_alumno->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRubrica_indicador_alumno->setCampo($_REQUEST['idrubrica_indicador_alumno'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
