<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegConfiguracion_docente', RUTA_BASE);
class WebConfiguracion_docente extends JrWeb
{
	private $oNegConfiguracion_docente;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegConfiguracion_docente = new NegConfiguracion_docente;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Configuracion_docente', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"];
			if(isset($_REQUEST["idnivel"])&&@$_REQUEST["idnivel"]!='')$filtros["idnivel"]=$_REQUEST["idnivel"];
			if(isset($_REQUEST["idunidad"])&&@$_REQUEST["idunidad"]!='')$filtros["idunidad"]=$_REQUEST["idunidad"];
			if(isset($_REQUEST["idactividad"])&&@$_REQUEST["idactividad"]!='')$filtros["idactividad"]=$_REQUEST["idactividad"];
			if(isset($_REQUEST["tiempototal"])&&@$_REQUEST["tiempototal"]!='')$filtros["tiempototal"]=$_REQUEST["tiempototal"];
			if(isset($_REQUEST["tiempoactividades"])&&@$_REQUEST["tiempoactividades"]!='')$filtros["tiempoactividades"]=$_REQUEST["tiempoactividades"];
			if(isset($_REQUEST["tiempoteacherresrc"])&&@$_REQUEST["tiempoteacherresrc"]!='')$filtros["tiempoteacherresrc"]=$_REQUEST["tiempoteacherresrc"];
			if(isset($_REQUEST["tiempogames"])&&@$_REQUEST["tiempogames"]!='')$filtros["tiempogames"]=$_REQUEST["tiempogames"];
			if(isset($_REQUEST["tiempoexamenes"])&&@$_REQUEST["tiempoexamenes"]!='')$filtros["tiempoexamenes"]=$_REQUEST["tiempoexamenes"];
			if(isset($_REQUEST["escalas"])&&@$_REQUEST["escalas"]!='')$filtros["escalas"]=$_REQUEST["escalas"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegConfiguracion_docente->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idconfigdocente)) {
				$this->oNegConfiguracion_docente->idconfigdocente = $idconfigdocente;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegConfiguracion_docente->iddocente=@$iddocente;
				$this->oNegConfiguracion_docente->idrol=@$idrol;
				$this->oNegConfiguracion_docente->idnivel=@$idnivel;
				$this->oNegConfiguracion_docente->idunidad=@$idunidad;
				$this->oNegConfiguracion_docente->idactividad=@$idactividad;
				$this->oNegConfiguracion_docente->tiempototal=@$tiempototal;
				$this->oNegConfiguracion_docente->tiempoactividades=@$tiempoactividades;
				$this->oNegConfiguracion_docente->tiempoteacherresrc=@$tiempoteacherresrc;
				$this->oNegConfiguracion_docente->tiempogames=@$tiempogames;
				$this->oNegConfiguracion_docente->tiempoexamenes=@$tiempoexamenes;
				$this->oNegConfiguracion_docente->escalas=@$escalas;
				
            if($accion=='_add') {
            	$res=$this->oNegConfiguracion_docente->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Configuracion_docente')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegConfiguracion_docente->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Configuracion_docente')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegConfiguracion_docente->__set('idconfigdocente', $_REQUEST['idconfigdocente']);
			$res=$this->oNegConfiguracion_docente->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegConfiguracion_docente->setCampo($_REQUEST['idconfigdocente'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}