<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE);
class WebGrupos extends JrWeb
{
	private $oNegGrupos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGrupos = new NegGrupos;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Grupos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
			if(isset($_REQUEST["idasistente"])&&@$_REQUEST["idasistente"]!='')$filtros["idasistente"]=$_REQUEST["idasistente"];
			if(isset($_REQUEST["fechainicio"])&&@$_REQUEST["fechainicio"]!='')$filtros["fechainicio"]=$_REQUEST["fechainicio"];
			if(isset($_REQUEST["fechafin"])&&@$_REQUEST["fechafin"]!='')$filtros["fechafin"]=$_REQUEST["fechafin"];
			if(isset($_REQUEST["dias"])&&@$_REQUEST["dias"]!='')$filtros["dias"]=$_REQUEST["dias"];
			if(isset($_REQUEST["horas"])&&@$_REQUEST["horas"]!='')$filtros["horas"]=$_REQUEST["horas"];
			if(isset($_REQUEST["horainicio"])&&@$_REQUEST["horainicio"]!='')$filtros["horainicio"]=$_REQUEST["horainicio"];
			if(isset($_REQUEST["horafin"])&&@$_REQUEST["horafin"]!='')$filtros["horafin"]=$_REQUEST["horafin"];
			if(isset($_REQUEST["valor"])&&@$_REQUEST["valor"]!='')$filtros["valor"]=$_REQUEST["valor"];
			if(isset($_REQUEST["valor_asi"])&&@$_REQUEST["valor_asi"]!='')$filtros["valor_asi"]=$_REQUEST["valor_asi"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["matriculados"])&&@$_REQUEST["matriculados"]!='')$filtros["matriculados"]=$_REQUEST["matriculados"];
			if(isset($_REQUEST["nivel"])&&@$_REQUEST["nivel"]!='')$filtros["nivel"]=$_REQUEST["nivel"];
			if(isset($_REQUEST["codigo"])&&@$_REQUEST["codigo"]!='')$filtros["codigo"]=$_REQUEST["codigo"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegGrupos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idgrupo)) {
				$this->oNegGrupos->idgrupo = $idgrupo;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegGrupos->iddocente=@$iddocente;
				$this->oNegGrupos->idlocal=@$idlocal;
				$this->oNegGrupos->idambiente=@$idambiente;
				$this->oNegGrupos->idasistente=@$idasistente;
				$this->oNegGrupos->fechainicio=@$fechainicio;
				$this->oNegGrupos->fechafin=@$fechafin;
				$this->oNegGrupos->dias=@$dias;
				$this->oNegGrupos->horas=@$horas;
				$this->oNegGrupos->horainicio=@$horainicio;
				$this->oNegGrupos->horafin=@$horafin;
				$this->oNegGrupos->valor=@$valor;
				$this->oNegGrupos->valor_asi=@$valor_asi;
				$this->oNegGrupos->regusuario=@$regusuario;
				$this->oNegGrupos->regfecha=@$regfecha;
				$this->oNegGrupos->matriculados=@$matriculados;
				$this->oNegGrupos->nivel=@$nivel;
				$this->oNegGrupos->codigo=@$codigo;
				
            if($accion=='_add') {
            	$res=$this->oNegGrupos->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Grupos')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegGrupos->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Grupos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegGrupos->__set('idgrupo', $_REQUEST['idgrupo']);
			$res=$this->oNegGrupos->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegGrupos->setCampo($_REQUEST['idgrupo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}