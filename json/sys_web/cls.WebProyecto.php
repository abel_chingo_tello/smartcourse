<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-04-2020 
 * @copyright	Copyright (C) 30-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
class WebProyecto extends JrWeb
{
	private $oNegProyecto;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegProyecto = new NegProyecto;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["jsonlogin"])&&@$_REQUEST["jsonlogin"]!='')$filtros["jsonlogin"]=$_REQUEST["jsonlogin"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo_empresa"])&&@$_REQUEST["tipo_empresa"]!='')$filtros["tipo_empresa"]=$_REQUEST["tipo_empresa"];
			if(isset($_REQUEST["logo"])&&@$_REQUEST["logo"]!='')$filtros["logo"]=$_REQUEST["logo"];
			if(isset($_REQUEST["nombreurl"])&&@$_REQUEST["nombreurl"]!='')$filtros["nombreurl"]=$_REQUEST["nombreurl"];
			if(isset($_REQUEST["tipo_portal"])&&@$_REQUEST["tipo_portal"]!='')$filtros["tipo_portal"]=$_REQUEST["tipo_portal"];
			if(isset($_REQUEST["correo_servidor"])&&@$_REQUEST["correo_servidor"]!='')$filtros["correo_servidor"]=$_REQUEST["correo_servidor"];
			if(isset($_REQUEST["correo_puerto"])&&@$_REQUEST["correo_puerto"]!='')$filtros["correo_puerto"]=$_REQUEST["correo_puerto"];
			if(isset($_REQUEST["correo_email"])&&@$_REQUEST["correo_email"]!='')$filtros["correo_email"]=$_REQUEST["correo_email"];
			if(isset($_REQUEST["correo_clave"])&&@$_REQUEST["correo_clave"]!='')$filtros["correo_clave"]=$_REQUEST["correo_clave"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegProyecto->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idproyecto)) {
				$this->oNegProyecto->idproyecto = $idproyecto;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario(); 
			$this->oNegProyecto->idempresa=@$idempresa;
			$this->oNegProyecto->jsonlogin=@$jsonlogin;
			$this->oNegProyecto->fecha=!empty($fecha)?$fecha:date('Y-m-d');
			$this->oNegProyecto->idioma=!empty($idioma)?$idioma:'ES';
			$this->oNegProyecto->nombre=!empty($nombre)?$nombre:'';
			$this->oNegProyecto->tipo_empresa=!empty($tipo_empresa)?$tipo_empresa:0;
			$this->oNegProyecto->logo=!empty($logo)?$logo:'';
			$this->oNegProyecto->nombreurl=!empty($nombreurl)?$nombreurl:'';
			$this->oNegProyecto->tipo_portal=!empty($tipo_portal)?$tipo_portal:2;
				
            if($accion=='_add') {
            	$res=$this->oNegProyecto->agregar();
            	$rr=array('code'=>200,'msj'=>ucfirst(JrTexto::_('Proyecto')).' '.JrTexto::_('saved successfully'),'newid'=>$res);
            }else{
            	$res=$this->oNegProyecto->editar();
            	$rr=array('code'=>200,'msj'=>ucfirst(JrTexto::_('Proyecto')).' '.JrTexto::_('update successfully'),'newid'=>$res);  
            }
            if(!empty($solocursosprincipales))
            	$res=$this->oNegProyecto->setCampo($res,'solocursosprincipales',$solocursosprincipales);
            NegSesion::set('idioma',$idioma,'idioma__');
            echo json_encode($rr);  			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarservidorcorreo(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            if(empty($idproyecto)||empty($servidor)||empty($puerto)||empty($correo)||empty($clave)){
            	echo json_encode(array('code'=>'Error','msj'=>'Datos incompletos'));
            	exit();
            }
            $usuarioAct = NegSesion::getUsuario(); 
            $idproyecto=!empty($idproyecto)?$idproyecto:$user["idproyecto"];
            if(empty($idproyecto)){
            	echo json_encode(array('code'=>'Error','msj'=>'Proyecto no existe'));
            	exit();
            }
            $this->oNegProyecto->idproyecto=$usuarioAct["idproyecto"];
			$proyecto=$this->oNegProyecto->dataProyecto;
            if(empty($proyecto)){
            	echo json_encode(array('code'=>'Error','msj'=>'Proyecto no existe'));
            	exit();	
            }
            $campos=array('correo_servidor'=>$servidor,'correo_puerto'=>$puerto,'correo_email'=>$email,'correo_clave'=>$clave);
            $this->oNegProyecto->actulizarcampos($id,$campos);
            echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Proyecto')).' '.JrTexto::_('update successfully'),'newid'=>$res));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegProyecto->idempresa=@$idempresa;
				$this->oNegProyecto->jsonlogin=@$jsonlogin;
				$this->oNegProyecto->fecha=@$fecha;
				$this->oNegProyecto->idioma=@$idioma;
				$this->oNegProyecto->nombre=@$nombre;
				$this->oNegProyecto->tipo_empresa=@$tipo_empresa;
				$this->oNegProyecto->logo=@$logo;
				$this->oNegProyecto->nombreurl=@$nombreurl;
				$this->oNegProyecto->tipo_portal=@$tipo_portal;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegProyecto->__set('idproyecto', $_REQUEST['idproyecto']);
			$res=$this->oNegProyecto->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegProyecto->setCampo($_REQUEST['idproyecto'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}