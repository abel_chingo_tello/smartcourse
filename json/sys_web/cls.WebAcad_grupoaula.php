<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		14-10-2019 
 * @copyright	Copyright (C) 14-10-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
class WebAcad_grupoaula extends JrWeb
{
	private $oNegAcad_grupoaula;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_grupoaula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["comentario"]) && @$_REQUEST["comentario"] != '') $filtros["comentario"] = $_REQUEST["comentario"];
			if (isset($_REQUEST["nvacantes"]) && @$_REQUEST["nvacantes"] != '') $filtros["nvacantes"] = $_REQUEST["nvacantes"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["idgrado"]) && @$_REQUEST["idgrado"] != '') $filtros["idgrado"] = $_REQUEST["idgrado"];
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idlocal"]) && @$_REQUEST["idlocal"] != '') $filtros["idlocal"] = $_REQUEST["idlocal"];
			if (isset($_REQUEST["idlocal_grupo"]) && @$_REQUEST["idlocal_grupo"] != '') $filtros["idlocal_grupo"] = $_REQUEST["idlocal_grupo"];
			if (isset($_REQUEST["idproducto"]) && @$_REQUEST["idproducto"] != '') $filtros["idproducto"] = $_REQUEST["idproducto"];
			if (isset($_REQUEST["idcategoria"]) && @$_REQUEST["idcategoria"] != '') $filtros["idcategoria"] = $_REQUEST["idcategoria"];
			// if(isset($_REQUEST["fechaactiva"])&&@$_REQUEST["fechaactiva"]!='')$filtros["fechaactiva"]=$_REQUEST["fechaactiva"];
			if (isset($_REQUEST["fechaactiva"]) && @$_REQUEST["fechaactiva"] != '') $filtros["fechaactiva"] = $_REQUEST["fechaactiva"];
			if (isset($_REQUEST["fechavencida"]) && @$_REQUEST["fechavencida"] != '') $filtros["fechavencida"] = $_REQUEST["fechavencida"];
			
			if (empty($_REQUEST["nologin"])) {
				if (!empty($_REQUEST["idproyecto"])) {
					$filtros["idproyecto"] = $_REQUEST["idproyecto"];
				} else {
					$usuarioAct = NegSesion::getUsuario();
					$filtros["idproyecto"] = $usuarioAct["idproyecto"];
				}
			}

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegAcad_grupoaula->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function buscargrupos()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$filtros = array();
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["comentario"]) && @$_REQUEST["comentario"] != '') $filtros["comentario"] = $_REQUEST["comentario"];
			if (isset($_REQUEST["nvacantes"]) && @$_REQUEST["nvacantes"] != '') $filtros["nvacantes"] = $_REQUEST["nvacantes"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["idlocal"]) && @$_REQUEST["idlocal"] != '') $filtros["idlocal"] = $_REQUEST["idlocal"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idcategoria"]) && @$_REQUEST["idcategoria"] != '') $filtros["idcategoria"] = $_REQUEST["idcategoria"];
			if (isset($_REQUEST["idproducto"]) && @$_REQUEST["idproducto"] != '') $filtros["idproducto"] = $_REQUEST["idproducto"];
			if (isset($_REQUEST["fechaactiva"]) && @$_REQUEST["fechaactiva"] != '') $filtros["fechaactiva"] = $_REQUEST["fechaactiva"];
			if (!empty($_REQUEST["groupby"])) $filtros["groupby"] = $_REQUEST["groupby"];

			if (!empty($_REQUEST["idproyecto"])) {
				$filtros["idproyecto"] = $_REQUEST["idproyecto"];
			} else {
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idproyecto"] = $usuarioAct["idproyecto"];
			}
			$this->oNegAcad_grupoaula->setLimite(0, 500000);
			$datos = $this->oNegAcad_grupoaula->buscargrupos($filtros);
			echo json_encode(array('code' => 200, 'data' => $datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	/**
	 * Funcion que retorna todos los grupos y cursos asociados de un proyecto y empresa determinado
	 * @param Array $_REQUEST por GET o POST
	 * @return Object JSON
	 */
	public function gruposestudios(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros = array();

			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["comentario"]) && @$_REQUEST["comentario"] != '') $filtros["comentario"] = $_REQUEST["comentario"];
			if (isset($_REQUEST["nvacantes"]) && @$_REQUEST["nvacantes"] != '') $filtros["nvacantes"] = $_REQUEST["nvacantes"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["idgrado"]) && @$_REQUEST["idgrado"] != '') $filtros["idgrado"] = $_REQUEST["idgrado"];
			if (isset($_REQUEST["idsesion"]) && @$_REQUEST["idsesion"] != '') $filtros["idsesion"] = $_REQUEST["idsesion"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idlocal"]) && @$_REQUEST["idlocal"] != '') $filtros["idlocal"] = $_REQUEST["idlocal"];
			if (isset($_REQUEST["idlocal_grupo"]) && @$_REQUEST["idlocal_grupo"] != '') $filtros["idlocal_grupo"] = $_REQUEST["idlocal_grupo"];
			if (isset($_REQUEST["idproducto"]) && @$_REQUEST["idproducto"] != '') $filtros["idproducto"] = $_REQUEST["idproducto"];
			if (isset($_REQUEST["idcategoria"]) && @$_REQUEST["idcategoria"] != '') $filtros["idcategoria"] = $_REQUEST["idcategoria"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			
			$datos = $this->oNegAcad_grupoaula->gruposestudios($filtros);
			echo json_encode(array('code' => 200, 'data' => $datos));
			exit(0);
		}catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function gruposxmatricula()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$filtros = array();
			if (!empty($_REQUEST["idmatricula"])){
				$v = !is_array($_REQUEST["idmatricula"]) ? json_decode($_REQUEST["idmatricula"],true) : null;
				$filtros["idmatricula"] = empty($v) || is_numeric($v) ? $_REQUEST["idmatricula"] : $v;
			}

			if (!empty($_REQUEST["idalumno"])) $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (!empty($_REQUEST["idgrupoaula"])) $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (!empty($_REQUEST["idnivel"])) $filtros["idnivel"] = $_REQUEST["idnivel"];
			if (!empty($_REQUEST["idcarrera"])) $filtros["idcarrera"] = $_REQUEST["idcarrera"];
			if (!empty($_REQUEST["idgrupoauladetalle"])) $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];

			$this->oNegAcad_grupoaula->setLimite(0, 900000);
			$datos = $this->oNegAcad_grupoaula->gruposxmatricula($filtros);
			echo json_encode(array('code' => 200, 'data' => $datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idgrupoaula)) {
				$this->oNegAcad_grupoaula->idgrupoaula = $idgrupoaula;
				$accion = '_edit';
			}
			$idproyecto = "";
			if (empty($_REQUEST["idproyecto"])) {
				$usuarioAct = NegSesion::getUsuario();
				$idproyecto = $usuarioAct["idproyecto"];
			} else {
				$idproyecto = $_REQUEST["idproyecto"];
			}
			$this->oNegAcad_grupoaula->nombre = @$nombre;
			$this->oNegAcad_grupoaula->tipo = @$tipo;
			$this->oNegAcad_grupoaula->comentario = @$comentario;
			$this->oNegAcad_grupoaula->nvacantes = @$nvacantes;
			$this->oNegAcad_grupoaula->estado = @$estado;
			$this->oNegAcad_grupoaula->idproyecto = $idproyecto;
			$this->oNegAcad_grupoaula->oldid = @$oldid;
			$this->oNegAcad_grupoaula->idgrado = @$idgrado;
			$this->oNegAcad_grupoaula->idsesion = @$idsesion;
			$this->oNegAcad_grupoaula->idcategoria = @$idcategoria;
			$this->oNegAcad_grupoaula->idlocal = @$idlocal;
			$this->oNegAcad_grupoaula->idproducto = @$idproducto;
			$this->oNegAcad_grupoaula->tipodecurso = @$tipodecurso;
			$this->oNegAcad_grupoaula->fecha_inicio = @$fecha_inicio;
			$this->oNegAcad_grupoaula->tipogrupo = !empty($tipogrupo) ? $tipogrupo : 1; //1 = normal, 2 = sence
			$this->oNegAcad_grupoaula->codaccionsence = !empty($codaccionsence) ? $codaccionsence : null;
			$this->oNegAcad_grupoaula->idcarrera = !empty($idcarrera) ? $idcarrera : null;
			if (!isset($complementario)) {
				$complementario = false;
			}
			if ($accion == '_add') {
				$res = $this->oNegAcad_grupoaula->agregar($complementario);
				echo json_encode(array('code' => 200, 'msj' =>  JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegAcad_grupoaula->editar($complementario);
				echo json_encode(array('code' => 200, 'msj' =>  JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegAcad_grupoaula->nombre = @$nombre;
					$this->oNegAcad_grupoaula->tipo = @$tipo;
					$this->oNegAcad_grupoaula->comentario = @$comentario;
					$this->oNegAcad_grupoaula->nvacantes = @$nvacantes;
					$this->oNegAcad_grupoaula->estado = @$estado;
					$this->oNegAcad_grupoaula->idproyecto = $usuarioAct["idproyecto"];
					$this->oNegAcad_grupoaula->oldid = @$oldid;
					$this->oNegAcad_grupoaula->idgrado = @$idgrado;
					$this->oNegAcad_grupoaula->idsesion = @$idsesion;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			$this->oNegAcad_grupoaula->__set('idgrupoaula', $_REQUEST['idgrupoaula']);
			$res = $this->oNegAcad_grupoaula->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => $e->getMessage()));
			exit(0);
		}
	}

	public function eliminar_()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			$res = $this->oNegAcad_grupoaula->eliminar_($_REQUEST['idgrupoaula']);
			if ($res) {
				echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			} else {
				echo json_encode(array('code' => "error", 'msj' => "Algún curso asignado cuenta con matrículas"));
			}
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => $e->getMessage()));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			$this->oNegAcad_grupoaula->setCampo($_REQUEST['idgrupoaula'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => JrTexto::_('data incomplete')));
			exit(0);
		}
	}
}
