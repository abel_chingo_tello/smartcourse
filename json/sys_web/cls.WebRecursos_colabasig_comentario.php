<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-01-2021 
 * @copyright	Copyright (C) 21-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRecursos_colabasig_comentario', RUTA_BASE);
class WebRecursos_colabasig_comentario extends JrWeb
{
	private $oNegRecursos_colabasig_comentario;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRecursos_colabasig_comentario = new NegRecursos_colabasig_comentario;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Recursos_colabasig_comentario', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idasignacion"])&&@$_REQUEST["idasignacion"]!='')$filtros["idasignacion"]=$_REQUEST["idasignacion"];
			if(isset($_REQUEST["idrecursoasignacion"])&&@$_REQUEST["idrecursoasignacion"]!='')$filtros["idasignacion"]=$_REQUEST["idrecursoasignacion"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			
			if(isset($_REQUEST["idrecursocomentario"])&&@$_REQUEST["idrecursocomentario"]!='')$filtros["idrecursocomentario"]=$_REQUEST["idrecursocomentario"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["comentario"])&&@$_REQUEST["comentario"]!='')$filtros["comentario"]=$_REQUEST["comentario"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["file"])&&@$_REQUEST["file"]!='')$filtros["file"]=$_REQUEST["file"];
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRecursos_colabasig_comentario->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idrecursocomentario)) {
				$this->oNegRecursos_colabasig_comentario->idrecursocomentario = $idrecursocomentario;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRecursos_colabasig_comentario->idasignacion=@$idasignacion;
				$this->oNegRecursos_colabasig_comentario->idpersona=@$idpersona;
				$this->oNegRecursos_colabasig_comentario->comentario=@$comentario;
				$this->oNegRecursos_colabasig_comentario->fecha=@$fecha;
				$this->oNegRecursos_colabasig_comentario->file=@$file;
				
            if($accion=='_add') {
            	$res=$this->oNegRecursos_colabasig_comentario->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursos_colabasig_comentario')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRecursos_colabasig_comentario->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Recursos_colabasig_comentario')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegRecursos_colabasig_comentario->idasignacion=@$idasignacion;
				$this->oNegRecursos_colabasig_comentario->idpersona=@$idpersona;
				$this->oNegRecursos_colabasig_comentario->comentario=@$comentario;
				$this->oNegRecursos_colabasig_comentario->fecha=@$fecha;
				$this->oNegRecursos_colabasig_comentario->file=@$file;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRecursos_colabasig_comentario->__set('idrecursocomentario', $_REQUEST['idrecursocomentario']);
			$res=$this->oNegRecursos_colabasig_comentario->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRecursos_colabasig_comentario->setCampo($_REQUEST['idrecursocomentario'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}