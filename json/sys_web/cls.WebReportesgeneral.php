<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegReportesgeneral', RUTA_BASE, 'sys_negocio');

class WebReportesgeneral extends JrWeb{
    protected $oNegReportesgeneral;
    
    public function __construct()
    {
        $this->oNegReportesgeneral = new NegReportesgeneral;

        parent::__construct();
    }
    private function printJSON($state, $message, $data)
    {
        echo json_encode(array('code' => $state, 'message' => $message, 'data' => $data));
        exit(0);
    }
    public function buscar(){
        try{
            global $aplicacion;
            $filtros = [];

            if(!empty($_REQUEST['id'])){ $filtros['id'] = $_REQUEST['id']; }
            if(!empty($_REQUEST['nombre'])){ $filtros['nombre'] = $_REQUEST['nombre']; }
            if(!empty($_REQUEST['likenombre'])){ $filtros['likenombre'] = $_REQUEST['likenombre']; }
            if(!empty($_REQUEST['descripcion'])){ $filtros['descripcion'] = $_REQUEST['descripcion']; }
            if(!empty($_REQUEST['icono'])){ $filtros['icono'] = $_REQUEST['icono']; }
            if(!empty($_REQUEST['likeicono'])){ $filtros['likeicono'] = $_REQUEST['likeicono']; }
            if(isset($_REQUEST['estado']) && $_REQUEST['estado'] != ''){ $filtros['estado'] = $_REQUEST['estado']; }

            $rs = $this->oNegReportesgeneral->buscar($filtros);

            $this->printJSON(200, "Request completed", $rs);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
    public function registrar(){
        try{
            global $aplicacion;

            $create = [];
            if(empty($_POST)){ throw new Exception("Campos vacios"); }
            #obtener los parametros
            extract($_POST,EXTR_PREFIX_ALL,"rq");
            #logica principal
            if(!empty($rq_nombre)){ $create['nombre'] = $rq_nombre; }
            if(!empty($rq_descripcion)){ $create['descripcion'] = $rq_descripcion; }
            if(!empty($rq_icono)){ $create['icono'] = $rq_icono; }
            if(!empty($rq_ruta)){ $create['ruta'] = $rq_ruta; }
            if(isset($rq_estado)){ $create['estado'] = $rq_estado; }

            #arrojar resultados
            $rs = $this->oNegReportesgeneral->insertar($create);

            $this->printJSON(200, "Request completed", $rs);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
}

?>