<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE);
class WebExamenes_preguntas extends JrWeb
{
	private $oNegExamenes_preguntas;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamenes_preguntas = new NegExamenes_preguntas;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examenes_preguntas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idexamen"])&&@$_REQUEST["idexamen"]!='')$filtros["idexamen"]=$_REQUEST["idexamen"];
			if(isset($_REQUEST["pregunta"])&&@$_REQUEST["pregunta"]!='')$filtros["pregunta"]=$_REQUEST["pregunta"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["ejercicio"])&&@$_REQUEST["ejercicio"]!='')$filtros["ejercicio"]=$_REQUEST["ejercicio"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["tiempo"])&&@$_REQUEST["tiempo"]!='')$filtros["tiempo"]=$_REQUEST["tiempo"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["template"])&&@$_REQUEST["template"]!='')$filtros["template"]=$_REQUEST["template"];
			if(isset($_REQUEST["habilidades"])&&@$_REQUEST["habilidades"]!='')$filtros["habilidades"]=$_REQUEST["habilidades"];
			if(isset($_REQUEST["idcontenedor"])&&@$_REQUEST["idcontenedor"]!='')$filtros["idcontenedor"]=$_REQUEST["idcontenedor"];
			if(isset($_REQUEST["dificultad"])&&@$_REQUEST["dificultad"]!='')$filtros["dificultad"]=$_REQUEST["dificultad"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegExamenes_preguntas->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idpregunta)) {
				$this->oNegExamenes_preguntas->idpregunta = $idpregunta;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegExamenes_preguntas->idexamen=@$idexamen;
				$this->oNegExamenes_preguntas->pregunta=@$pregunta;
				$this->oNegExamenes_preguntas->descripcion=@$descripcion;
				$this->oNegExamenes_preguntas->ejercicio=@$ejercicio;
				$this->oNegExamenes_preguntas->idpadre=@$idpadre;
				$this->oNegExamenes_preguntas->tiempo=@$tiempo;
				$this->oNegExamenes_preguntas->puntaje=@$puntaje;
				$this->oNegExamenes_preguntas->idpersonal=@$idpersonal;
				$this->oNegExamenes_preguntas->fecharegistro=@$fecharegistro;
				$this->oNegExamenes_preguntas->template=@$template;
				$this->oNegExamenes_preguntas->habilidades=@$habilidades;
				$this->oNegExamenes_preguntas->idcontenedor=@$idcontenedor;
				$this->oNegExamenes_preguntas->dificultad=@$dificultad;
				
            if($accion=='_add') {
            	$res=$this->oNegExamenes_preguntas->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Examenes_preguntas')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegExamenes_preguntas->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Examenes_preguntas')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegExamenes_preguntas->__set('idpregunta', $_REQUEST['idpregunta']);
			$res=$this->oNegExamenes_preguntas->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegExamenes_preguntas->setCampo($_REQUEST['idpregunta'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}