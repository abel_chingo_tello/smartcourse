<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020 
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRubrica', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRubrica_nivel', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRubrica_criterio', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRubrica_indicador', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRubrica_indicador_alumno', RUTA_BASE);
class WebRubrica extends JrWeb
{
	private $oNegRubrica;

	public function __construct()
	{
		parent::__construct();
		$this->oNegRubrica = new NegRubrica;
		$this->oNegRubrica_nivel = new NegRubrica_nivel;
		$this->oNegRubrica_criterio = new NegRubrica_criterio;
		$this->oNegRubrica_indicador = new NegRubrica_indicador;
		$this->oNegRubrica_indicador_alumno = new NegRubrica_indicador_alumno;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$filtros = array();
			$getEditable = false;
			if (isset($_REQUEST["idrubrica"]) && @$_REQUEST["idrubrica"] != '') $filtros["idrubrica"] = $_REQUEST["idrubrica"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["fecha_creacion"]) && @$_REQUEST["fecha_creacion"] != '') $filtros["fecha_creacion"] = $_REQUEST["fecha_creacion"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["listAll"]) && @$_REQUEST["listAll"] != '') $filtros["listAll"] = $_REQUEST["listAll"];
			if (isset($_REQUEST["getAditional"]) && @$_REQUEST["getAditional"] != '') $filtros["getAditional"] = $_REQUEST["getAditional"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			if (isset($_REQUEST["getEditable"]) && @$_REQUEST["getEditable"] != '') {
				$getEditable = filter_var($_REQUEST["getEditable"], FILTER_VALIDATE_BOOLEAN);
			}

			$usuarioAct = NegSesion::getUsuario();
			// if ($usuarioAct['idrol'] <= "2") {
			if (intval($usuarioAct['idrol']) <= 2) {
				if (!isset($filtros["listAll"])) {
					$filtros['iddocente'] = $usuarioAct['idpersona'];
					$filtros['idproyecto'] = $usuarioAct['idproyecto'];
				}
			}
			
			global $aditionalFound;
			$aditionalFound = false;
			$fxProcesar = function ($arrRubrica, $getEditable,$filtros) {
				foreach ($arrRubrica as $key => $rubrica) {
					if ($getEditable) {
						$totalIndicadorAlumno = $this->oNegRubrica_indicador_alumno->contar(array('idrubrica' => $rubrica['idrubrica']))[0]['total'];
						if ($totalIndicadorAlumno > 0) {
							$rubrica['editable'] = false;
						} else {
							$rubrica['editable'] = true;
						}
					}
					if (isset($filtros["getAditional"])) {
						if ($filtros["getAditional"] == $rubrica["idrubrica"]) {
							global $aditionalFound;
							$aditionalFound = true;
						}
					}
					$rubrica['arrNivel'] = $this->oNegRubrica_nivel->buscar(array('idrubrica' => $rubrica['idrubrica']));
					$rubrica['arrCriterio'] = $this->oNegRubrica_criterio->buscar(array('idrubrica' => $rubrica['idrubrica']));
					$rubrica['arrIndicador'] = $this->oNegRubrica_indicador->buscar(array('idrubrica' => $rubrica['idrubrica']));
					$arrRubrica[$key] = $rubrica;
				}
				return $arrRubrica;
			};


			$arrRubrica = $this->oNegRubrica->buscar($filtros);
			$arrRubrica = $fxProcesar($arrRubrica, $getEditable,$filtros);
			if (isset($filtros["getAditional"]) && !$aditionalFound) {
				$arrRubricaAdicional = $this->oNegRubrica->buscar(["idrubrica" => $filtros["getAditional"]]);
				$arrRubricaAdicional = $fxProcesar($arrRubricaAdicional, $getEditable,$filtros);
				$arrRubrica = array_merge($arrRubricaAdicional, $arrRubrica);
			}

			$this->datos = $arrRubrica;

			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';

		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idrubrica)) {
				$this->oNegRubrica->idrubrica = $idrubrica;
				$accion = '_edit';
			}



			$this->oNegRubrica->nombre = @$nombre;
			$this->oNegRubrica->fecha_creacion = @$fecha_creacion;
			$this->oNegRubrica->idproyecto = $usuarioAct['idproyecto'];;
			$this->oNegRubrica->estado = @$estado;
			$this->oNegRubrica->iddocente = $usuarioAct['idpersona'];

			if ($accion == '_add') {
				$res = $this->oNegRubrica->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegRubrica->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Rubrica')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegRubrica->nombre = @$nombre;
					$this->oNegRubrica->fecha_creacion = @$fecha_creacion;
					$this->oNegRubrica->idproyecto = @$idproyecto;
					$this->oNegRubrica->estado = @$estado;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('No tiene permisos')));
				exit(0);
			}
			$this->oNegRubrica->__set('idrubrica', $_REQUEST['idrubrica']);
			$res = $this->oNegRubrica->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRubrica->setCampo($_REQUEST['idrubrica'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
