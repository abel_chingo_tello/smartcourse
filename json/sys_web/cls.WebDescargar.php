<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020 
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
ini_set('max_execution_time', 0);
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_criterios', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_niveles', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_actividad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_herramientas', RUTA_BASE);
class WebDescargar extends JrWeb
{
	private $oNegAcad_criterios;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_criterios = new NegAcad_criterios;
				
	}

	public function defecto(){
		return $this->procesar();
	}

	private function limpiarlink($link,$type=false,$ruta_sesion=false,$SC=true){
		$linktmp=explode("?", $link);
		$link=$linktmp[0];
		$pos=stripos($link,"static/media/");		
		if($pos!==false) $link=substr($link, $pos);
		if($type==false) $link=str_replace('/', SD, $link);
		return $SC?RUTA_BASE.$link:$link;
	}

	private function full_copy( $source, $target){
		//echo "<br>copy===================== <br>".$source."<br>";
		//echo "<br>".$source."<br>".$target."<br><br>";
	    if (is_dir($source)){
	        @mkdir($target,0777,true);
	        chmod($target, 0777);
	        $d = dir( $source );
	        while(FALSE!==($entry=$d->read())){
	            if($entry == '.' || $entry == '..'){
	                continue;
	            }
	            $Entry = $source.'/'.$entry;
	            if (is_dir($Entry)) {
	                $this->full_copy( $Entry, $target . '/' . $entry );                
	                continue;
	            }
	            copy($Entry, $target . '/' . $entry);
	            chmod($target . '/' . $entry, 0777);
	        }
	        $d->close();
	    }else {
			if(is_file($source)){
				//echo $source."<br>";
	        	copy($source,$target);
	        	@chmod($target, 0777);
			}
	    }
	}

	private function extraerarchivosdehtml($params){
		if(!empty($params["html"])){
			$re_extractvideo = '/src=["\']([^ ^"^\']*)["\']/ims';
			preg_match_all($re_extractvideo  , $params["html"] , $matches);
			if(!empty($matches[1])){
				$existe=array();
				$iim=0;
				foreach($matches[1] as $ilink=>$linktmp){					
					$linktmp1=trim($linktmp);
					if(!empty($linktmp1)){
						$pos=stripos($linktmp1,"novideo.png");	
						$pos2=stripos($linktmp1,"noimage.png");
						$pos3=stripos($linktmp1,"es.png");
						$pos4=stripos($linktmp1,"en.png");
						$pos5=stripos($linktmp1,"ui.min.js");
						if($pos==false && $pos2==false && $pos3==false && $pos4==false && $pos5==false && !in_array($linktmp1,$existe)){
							$existe[]=$linktmp1;
							$linktmp1=$this->limpiarlink($linktmp1,false,false,false);
							//'ruta'=>$directoriotmp,'i'=>$i,'RUTA_BASE_SE'=>$RUTA_BASE_SE
							if(!empty($params["RUTA_BASE_SE"])){
								$iim++;
								$linktmp2=$params["RUTA_BASE_SE"].$linktmp1;
								$extencion=substr($linktmp1,stripos($linktmp1,"."));
								$newruta=$params["ruta"].(!empty($params['i'])?($params['i']."_"):'').($iim).$extencion;
								//echo $linktmp2."<br>".$newruta."<br><br>";
								$this->full_copy($linktmp2,$newruta);
							}
							//echo $linktmp1."<br>";
						}
					}
				}
			}
		}
	}

	private function descargartema($RUTA_TEMA,$contador,$infotema,$ruta_sesion=false){		
		if(!empty($infotema)){
			$typelink=strtolower($infotema["typelink"]);		
			switch ($typelink){
				case 'imagen':
				case 'audio':
				case 'video':
				case 'pdf':
					$link=$this->limpiarlink($infotema["link"]);
					$extencion=substr($link,stripos($link,"."));
					$newarchivo= $RUTA_TEMA.$ruta_sesion.$extencion;
					$this->full_copy($link,$newarchivo);
					break;
				case 'html':					
					$link=$this->limpiarlink($infotema["link"],'html',$ruta_sesion);

					$partes=preg_split('/\/ses_(.+?)\//im',$link);
					if(empty($partes[1])){
						$partes=preg_split('/\/curso_(.+?)\//im',$link);
						if(empty($partes[1])){
							$partes=preg_split('/\/c_c_(.+?)\//im',$link);
						}
					}
					if(!empty($partes[1])){	
						$pos=explode("/",$partes[1]);
						if(!empty($pos[1])){									
							$pos2=explode("/".$pos[0], $link);
							$pos3=stripos($link, "/".$pos[0]);
							if($pos3!==false){
								$pos3=$pos3+strlen("/".$pos[0]);
								$link=substr($link, 0,$pos3);
							}							
						}
					}

					$newarchivo= $RUTA_TEMA;
					$pos=stripos($link,".");
					if($pos!==false){
						$extencion=substr($link,$pos);
						$newarchivo.=$ruta_sesion.$extencion;

					}else{

						if(is_dir($link)){
							$RUTA_TEMA=$newarchivo.SD.$ruta_sesion;
							@mkdir($RUTA_TEMA,0777,true);
							@chmod($RUTA_TEMA, 0777);
							$newarchivo= $RUTA_TEMA;
						}
					}					
					$link=str_replace('/', SD,$link);
					$this->full_copy($link, $newarchivo);								
					break;
				case 'smartlibrery':
					$link=$infotema["link"];
					$link=$this->limpiarlink($infotema["link"]);
					$link=str_replace('.txt', '.php', $link);
					if(is_file($link)){
						include_once($link);
						//variable que viene desde el archivo generado;
						$enlaces=json_decode($enlaces,true);
						$directorio=$RUTA_TEMA.$ruta_sesion;
						//echo $directorio."<br>";
						@mkdir($directorio,0777,true);
						@chmod($directorio,0777);

						foreach ($enlaces as $key => $enl) {
							$img=$enl["img"];
							$nkey=$key+1;
							if(!empty($img)){
								$img=$this->limpiarlink($img);
								$extencion=substr($img,stripos($img,"."));								
								$newarchivo= $directorio.SD.$nkey."_imagen".$extencion;
								$this->full_copy($img,$newarchivo);
							}
							$link=$enl["link"];
							if(!empty($link)){
								$link=$this->limpiarlink($link);
								$extencion=substr($link,stripos($link,"."));
								$newarchivo=$directorio.SD.$nkey."_enlace".$extencion;
								$this->full_copy($link,$newarchivo);
							}							
						}
					}
					break;
				case 'tabladeaudios':
					$enlaces=json_decode(base64_decode($infotema["link"]),true);
					$directorio=$RUTA_TEMA.$ruta_sesion;
					@mkdir($directorio,0777,true);
					@chmod($directorio,0777);

					//var_dump($enlaces["menu"]);
					if(!empty($enlaces["menu"]));
					foreach($enlaces["menu"] as $key => $enl){	
						$nkey=$key+1;	
						$link=$enl["link"];
						if(!empty($link)){
							$link=$this->limpiarlink($link);
							$extencion=substr($link,stripos($link,"."));
							$newarchivo=$directorio.SD.$nkey."_".$enl["tipo"].$extencion;
							$this->full_copy($link,$newarchivo);
						}							
					}
					break;
				case 'multioptions':
					$link=$infotema["link"];
					$enlaces=json_decode(base64_decode($infotema["link"]),true);
					$directorio=$RUTA_TEMA.$ruta_sesion;
					@mkdir($directorio,0777,true);
					@chmod($directorio,0777);

					if(!empty($enlaces["menus"]))
					foreach($enlaces["menus"] as $key => $enl){	
						$nkey=$key+1;	
						$link=$enl["link"];
						if(!empty($link)){
							$link=$this->limpiarlink($link);
							$extencion=substr($link,stripos($link,"."));
							$newarchivo=$directorio.SD.$nkey."_".$enl["tipo"].$extencion;
							$this->full_copy($link,$newarchivo);
						}							
					}
					break;
				case 'smartenglish':
					$link=$infotema["link"];
					$directorio=$RUTA_TEMA.$ruta_sesion;
					//echo "<br>".$directorio."<br>";
					@mkdir($directorio,0777,true);
					@chmod($directorio,0777);
					$linktmp1=explode('?', $link)[1];
					$linktmp=explode('&', $linktmp1);
					$REQUEST=array();
					foreach ($linktmp as $key => $value) {
						$datatmp2=explode('=', $value);
						$REQUEST[$datatmp2[0]]=$datatmp2[1];
					}

					$DB=NAME_BDSEAdolecentes;
			        $url_SE=URL_SEAdolecentes;
					$RUTA_BASE_SE=RUTA_BASE_SEJ;
			        //$this->css='ver';
			        $PV=!empty($REQUEST['PV'])?$REQUEST['PV']:'PVADU_1';
			        if($PV=='PVADU_1'){
			            $DB=NAME_BDSEAdultos;
			            $url_SE=URL_SEAdultos;
						$RUTA_BASE_SE=RUTA_BASE_SEA;
			            //$this->css='ver_adultos';
			        }
			        $idcursodetalle_se=!empty($REQUEST["idcursodetalle"])?$REQUEST["idcursodetalle"]:-1;
			        $filtros=array();
			        $filtros['idcursodetalle']=$idcursodetalle_se;
            		$filtros['BD']=$DB;


					$NegEnglish_cursodetalle = new NegEnglish_cursodetalle($DB);
            		$cursodetalle=$NegEnglish_cursodetalle->buscar($filtros);					
            		$cursodetalle0=$cursodetalle[0];  
            		$idcursoSE=$cursodetalle0["idcurso"];
            		//$idcurso=$idcursoSE;
            		$idcursoDetalle=$cursodetalle0["idcursodetalle"];
            		$cursos=$NegEnglish_cursodetalle->buscarcurso(array('idcurso'=>$idcursoSE,'PV'=>@$PV,'BD'=>$DB));


		            $curso=array();
		            if(!empty($cursos[0])) $curso=$cursos[0];                     
		            $idrecurso=$cursodetalle0["idrecurso"];
		            //$idlogro=$cursodetalle0["idlogro"];
		            //$orden=$cursodetalle0["orden"];
		            $idactividad=$idrecurso;
					
					$NegEnglish_Niveles = new NegEnglish_niveles($DB);
		            $sesiones=$NegEnglish_Niveles->buscar(array('tipo'=>'L','idnivel'=>$idactividad,'BD'=>$DB));

		            $sesion=array();
            		if(!empty($sesiones[0])) $sesion=$sesiones[0];
					$NegEnglish_Actividad = new NegEnglish_actividad($DB);
            		$ejercicios = $NegEnglish_Actividad->fullActividades(array('sesion'=>$idactividad,'BD'=>$DB));

            		$look = !empty($ejercicios)?$ejercicios[1]:null;
            		$practice = !empty($ejercicios)?$ejercicios[2]:null;
            		$dby = !empty($ejercicios)?$ejercicios[3]:null;
            		$vocabulario=array();
		            $games=array();

					$NegEnglish_Herramientas = new NegEnglish_herramientas($DB);
		            $herr=$NegEnglish_Herramientas->buscar(array('idactividad'=>$idactividad,'BD'=>$DB));
		            if(!empty($herr)){
		                foreach ($herr as $hkey => $hvalue){
		                    if($hvalue['tool']=='G'){
		                        $games[]=$hvalue;
		                    }else if($hvalue['tool']=='V'){
		                        $vocabulario[0]= $hvalue;           
		                    }
		                }
		            }
					//portada de smartbook;
					if(!empty($url_SE.$sesion["imagen"])){
						$link=$this->limpiarlink($url_SE.$sesion["imagen"],false,false,false);
						$extencion=substr($link,stripos($link,"."));						
						$this->full_copy($RUTA_BASE_SE.$link, $directorio.SD."portada.".$extencion);
					}

					if(!empty($look["act"]["det"])){						
						$directoriotmp=$directorio.SD.'look'.SD;
						@mkdir($directoriotmp,0777,true);
						@chmod($directoriotmp,0777);
						foreach (@$look['act']['det'] as $i => $det_html){
							$this->extraerarchivosdehtml(array('html'=>$det_html["texto_edit"],'ruta'=>$directoriotmp,'i'=>'','RUTA_BASE_SE'=>$RUTA_BASE_SE));
						}
					}

					if(!empty($practice["act"]["det"])){
						$directoriotmp=$directorio.SD.'practice'.SD;
						@mkdir($directoriotmp,0777,true);
						@chmod($directoriotmp,0777);
						foreach (@$practice['act']['det'] as $i => $det_html){
							$this->extraerarchivosdehtml(array('html'=>$det_html["texto_edit"],'ruta'=>$directoriotmp,'i'=>$i+1,'RUTA_BASE_SE'=>$RUTA_BASE_SE));																				
						}
					}

					if(!empty($dby["act"]["det"])){
						$directoriotmp=$directorio.SD.'dby'.SD;
						@mkdir($directoriotmp,0777,true);
						@chmod($directoriotmp,0777);
						foreach (@$dby['act']['det'] as $i => $det_html){
							$this->extraerarchivosdehtml(array('html'=>$det_html["texto_edit"],'ruta'=>$directoriotmp,'i'=>$i+1,'RUTA_BASE_SE'=>$RUTA_BASE_SE));
																			
						}
					}

					$directoriotmp=$directorio.SD.'vocabulario'.SD;
					@mkdir($directoriotmp,0777,true);
					@chmod($directoriotmp,0777);
					$this->extraerarchivosdehtml(array('html'=>$vocabulario[0]["texto"],'ruta'=>$directoriotmp,'i'=>'','RUTA_BASE_SE'=>$RUTA_BASE_SE));					

					if(!empty($games)){
						$directoriotmp=$directorio.SD.'game'.SD;
						@mkdir($directoriotmp,0777,true);
						@chmod($directoriotmp,0777);
						foreach($games as $i=>$game){
							$this->extraerarchivosdehtml(array('html'=>$game["texto"],'ruta'=>$directoriotmp,'i'=>$i+1,'RUTA_BASE_SE'=>$RUTA_BASE_SE));
							//$this->extraerarchivosdehtml(array('html'=>$game["texto"]));
						}
					}
					break;
				case 'smartquiz':
					$link=$infotema["link"];
					$directorio=$RUTA_TEMA.$ruta_sesion;
					@mkdir($directorio,0777,true);
					@chmod($directorio,0777);
					//echo "<br>------------------------------------     $link       --------------------------------------------<br>";

					
					//var_dump($infotema);
					//echo "<br>_____________________________________  $directorio   ___________________________________________<br>";
					break;

					//var_dump($infotema);
					break;
				default:
					break;
			}
		}
	}

	private  function limpiarnombredir($nombre){
		$buscar=array('/[(\s)]+/','/á/','/é/','/í/','/ó/','/ú/');
		$sustituir=array('_','a','e','i','o','u');
		$nombre=preg_replace($buscar,$sustituir,$nombre);
		return utf8_encode($nombre);
	}

	public function procesar(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();	
			@extract($_REQUEST);	
			$idcurso=!empty($idcurso)?$idcurso:0;
			$idcc=!empty($idcomplementario)?$idcomplementario:0;
			JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
			$oNegCurso = new NegAcad_curso;
			JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
			$oNegCursodetalle = new NegAcad_cursodetalle;
			$temas=array();
			if(!empty($idcc)){			
				$curso=$oNegCurso->buscar(array("complementario"=>true,"idcomplementario"=>$idcc,'idcurso'=>$idcurso));
				$curso=$curso[0];
				$temas=$oNegCursodetalle->buscarconnivel(array('idcurso'=>$idcc,"complementario"=>true));
			}else{
				$temas=$oNegCursodetalle->buscarconnivel(array('idcurso'=>$idcurso));
				$curso=$oNegCurso->buscar(array('idcurso'=>$idcurso));
				$curso=$curso[0];
			}

			$nombrecurso=$curso["nombre"].'_'.$idcurso."_".$idcc;
			$nombrecurso=$this->limpiarnombredir($nombrecurso);
			$RUTA_CURSO=RUTA_BASE.'static'.SD.'descarga'.SD.'cursos'.SD.$nombrecurso.SD;
			if(is_dir($RUTA_CURSO)) $this->eliminardirectorio($RUTA_CURSO);	
			if(!is_dir($RUTA_CURSO)) {				
				@mkdir($RUTA_CURSO,0777,true);	@chmod($RUTA_CURSO, 0777); }
			if(!empty($temas)){
				$RUTA_TEMA=$RUTA_CURSO;
				foreach ($temas as $key => $tem){
					$idcursodetalle=$tem["idcursodetalle"];
					$infocurso=json_decode($tem["txtjson"],true);			
					$nombre=trim(str_pad(($key+1),2,"0",STR_PAD_LEFT)."_".$tem["nombre"]);
					$nombre=$this->limpiarnombredir($nombre);
					if(!empty($infocurso)){
						//echo $RUTA_TEMA.$nombre."<br>";
						if(empty($infocurso["options"])){
							$this->descargartema($RUTA_TEMA,1,$infocurso,$nombre);
						}else{
							$RUTA_TEMA1=trim($RUTA_TEMA.$nombre);
							if(!is_dir($RUTA_TEMA1)){@mkdir($RUTA_TEMA1,0777,true);}
							@chmod($RUTA_TEMA1, 0777);	
							$j=0;						
							foreach ($infocurso["options"] as $optk => $opt){
								$j++;
								if(!empty($opt["link"])){
									$nombre=trim("Pes_".str_pad($j,2,"0",STR_PAD_LEFT)."_".$opt["nombre"]);
									$nombre=$this->limpiarnombredir($nombre);
									$opt["typelink"]=$opt["type"];
									//if(empty($opt["type"])) var_dump($opt);
									$RUTA_TEMA2=$RUTA_TEMA1.SD;													
									$this->descargartema($RUTA_TEMA2,1,$opt,$nombre);
								}
							}
						}

					}else{
						$RUTA_TEMA0=$RUTA_CURSO.$nombre;
						if(!is_dir($RUTA_TEMA0))@mkdir($RUTA_TEMA0,0777,true);
						$RUTA_TEMA0=$RUTA_TEMA0.SD;
						@chmod($RUTA_TEMA0, 0777);						
						if(!empty($tem["hijos"])){
							$RUTA_TEMA_TMP=$RUTA_TEMA0;
							foreach($tem["hijos"] as $temk => $temv){
								$infocurso2=json_decode($temv["txtjson"],true);
								$nombre=str_pad(($temk+1),2,"0",STR_PAD_LEFT)."_".@$temv["nombre"];
								$nombre=$this->limpiarnombredir($nombre);
								if(empty($infocurso2["options"])){									
									$link=$infocurso2["link"];
									//echo $link;				0		
									$this->descargartema($RUTA_TEMA_TMP,1,$infocurso2,$nombre);
								}else{
									
									$RUTA_TEMA5=trim($RUTA_TEMA_TMP.$nombre);																
									if(!is_dir($RUTA_TEMA5)){ 
										@mkdir($RUTA_TEMA5,0777,true);
									}
									$RUTA_TEMA5=$RUTA_TEMA5.SD;
									@chmod($RUTA_TEMA5, 0777);
									$jj=0;								
									foreach ($infocurso2["options"] as $optk => $opt){	
										$jj++;
										$nombre=trim("Pest_".str_pad($jj,2,"0",STR_PAD_LEFT)."_".$opt["nombre"]);
										$nombre=$this->limpiarnombredir($nombre);
										$opt["typelink"]=$opt["type"];
										$RUTA_TEMA6=$RUTA_TEMA5;																	
										$this->descargartema($RUTA_TEMA6,1,$opt,$nombre);
									}									
								}																							
							}
						}
					}						
				}
			}
			$rutazip=$this->comprimir_ini($RUTA_CURSO,$nombrecurso);
			echo json_encode(array('code'=>200,'data'=>'ok','zip'=>$rutazip,'adescarga'=>"<a target='_blank' href='".$rutazip."'>Descargar ".$nombrecurso." </a>"));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	private function comprimir($dir, $zip) {
		if (is_dir($dir)) {
			if ($da = opendir($dir)) {
			while (($archivo = readdir($da)) !== false) {			
				if (is_dir($dir . $archivo) && $archivo != "." && $archivo != "..") {
				//echo "<strong>Creando directorio: $dir$archivo</strong><br/>";
				$this->comprimir($dir . $archivo . "/", $zip);	
				} elseif (is_file($dir . $archivo) && $archivo != "." && $archivo != "..") {
				//echo "Agregando archivo: $dir$archivo <br/>";
				$fileruta=substr($dir.$archivo,stripos($dir.$archivo,RUTA_BASE.'static'.SD.'descarga'.SD));
				$zip->addFile($dir . $archivo, $fileruta);
				}
			}		
			closedir($da);
			}
		}
	}

	private function comprimir_ini($dir,$nombre){
		$zip = new ZipArchive();
		$archivoZip = $nombre.".zip";
		if ($zip->open($dir.$archivoZip, ZIPARCHIVE::CREATE) === true) {
			$this->comprimir($dir, $zip);
			$zip->close();
			//rename("$dir"."$archivoZip", "$dir"."$archivoZip");
			//echo "$dir"."$archivoZip";			
			if (file_exists($dir.$archivoZip)) {
				chmod("$dir"."$archivoZip",0777);
				return URL_BASE."static/descarga/cursos/$nombre/$archivoZip";
				//echo "Proceso Finalizado!! <br/><br/>
				//	Descargar: <a target='_blank' href='../../static/descarga/cursos/$nombre/$archivoZip'>$archivoZip</a>";
			} else {
				echo json_encode(array('code'=>'Error','msj'=>'Error al comprimir archivos '.$nombre));
		 		exit(0);
				//echo "Error, archivo zip no ha sido creado!!";
			}
		}
	}

	public  function eliminardirectorio($dir) {		
		$files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
		(is_dir("$dir/$file")) ? $this->eliminardirectorio("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}
}