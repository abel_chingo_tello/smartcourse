<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegCorregir', RUTA_BASE);
set_time_limit(0);
class WebCorregir extends JrWeb
{
	private $oNegCorregir;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegCorregir = new Negcorregir;
	}

	public function defecto(){
		$desde=!empty($_REQUEST["desde"])?$_REQUEST["desde"]:0;
		$tabla=!empty($_REQUEST["tabla"])?$_REQUEST["tabla"]:'';
		if($tabla=='historial_sesion') $this->oNegCorregir->historial_sesion($desde);
		else if($tabla=='valoracion') $this->oNegCorregir->valoracion($desde);
		else if($tabla=='notas_quiz') $this->oNegCorregir->notas_quiz($desde);
		else if($tabla=='tareas') $this->oNegCorregir->tareas($desde);
		else if($tabla=='bitacora_alumno_smartbook') $this->oNegCorregir->bitacora_alumno_smartbook($desde);
		else if($tabla=='bitacora_alumno_smartbook_se') $this->oNegCorregir->bitacora_alumno_smartbook_se($desde);
		else if($tabla=='transferencias') $this->oNegCorregir->transferencias($desde);
		else if($tabla=='actividad_alumno') $this->oNegCorregir->actividad_alumno($desde);
		else if($tabla=='eliminarinfouser'){
			if(!empty($_REQUEST["idalumno"])){
				$idalumno=$_REQUEST["idalumno"];
				$this->oNegCorregir->eliminarinfouser($idalumno);
			}else{
				
				$idalumno=1; //abelchingo
				$this->oNegCorregir->eliminarinfouser($idalumno);
				//5; //arianyisabel				
				//18; // maria_cristina19				
				//19; // malop_33				
				//2190 //  SANCHEZ ALLENDE, DIEGO
				//2191 //SANCHEZ ALLENDE, WILMER
			}
		}
	}
}