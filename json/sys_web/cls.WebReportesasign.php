<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegReportesasign', RUTA_BASE, 'sys_negocio');

class WebReportesasign extends JrWeb{
    protected $oNegReportesasign;
    
    public function __construct()
    {
        $this->oNegReportesasign = new NegReportesasign;

        parent::__construct();
    }
    private function printJSON($state, $message, $data)
    {
        echo json_encode(array('code' => $state, 'message' => $message, 'data' => $data));
        exit(0);
    }
    public function buscar_empresa(){
        try{
            global $aplicacion;
            
            $this->printJSON(200, "Request completed", $rs);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
    public function buscar_proyecto(){
        try{
            global $aplicacion;
            
            $this->printJSON(200, "Request completed", $rs);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
    /**
     * Buscar asignaciones tanto general como especificos
     * @param Array $_REQUEST filtros o parametros para la busqueda
     * @return String JSON
     */
    public function asignaciones(){
        try{
            global $aplicacion;
            $filtros = [];
            if(!empty($_REQUEST['idbolsa_empresa'])){ $filtros['idbolsa_empresa'] = $_REQUEST['idbolsa_empresa']; }
            if(!empty($_REQUEST['idreportegeneral'])){ $filtros['idreportegeneral'] = $_REQUEST['idreportegeneral']; }
            if(!empty($_REQUEST['idproyecto'])){ $filtros['idproyecto'] = $_REQUEST['idproyecto']; }
            if(!empty($_REQUEST['nullidproyecto'])){ $filtros['nullidproyecto'] = $_REQUEST['nullidproyecto']; }
            if(!empty($_REQUEST['estado'])){ $filtros['estado'] = $_REQUEST['estado']; }
            if(!empty($_REQUEST['groupby'])){ $filtros['groupby'] = $_REQUEST['groupby']; }

            $rs = $this->oNegReportesasign->asignaciones($filtros);
            
            $this->printJSON(200, "Request completed", $rs);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
    public function registrarproyecto(){
        try{
            global $aplicacion;

            $create = [];
            if(empty($_POST)){ throw new Exception("Campos vacios"); }
            #obtener los parametros
            if(empty($_POST['idbolsa_empresa']) || empty($_POST['idreportegeneral'])){ throw new Exception("Campos requeridos estan vacios: idbolsa_empresa, idreportegeneral"); }
            extract($_POST,EXTR_PREFIX_ALL,"rq");
            #logica principal
            if(!empty($rq_idbolsa_empresa)){ $create['idbolsa_empresa'] = $rq_idbolsa_empresa; }
            if(!empty($rq_idreportegeneral)){ $create['idreportegeneral'] = $rq_idreportegeneral; }
            if(!empty($rq_idproyecto)){ $create['idproyecto'] = $rq_idproyecto; }
            if(isset($rq_estado)){ $create['estado'] = $rq_estado; }

            #arrojar resultados
            $rs = $this->oNegReportesasign->insertarproyecto($create);

            $this->printJSON(200, "Request completed", $rs);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
    public function delete(){
        try{
            global $aplicacion;

            $create = [];
            if(empty($_POST)){ throw new Exception("Campos vacios"); }
            #obtener los parametros
            if(empty($_POST['id'])){ throw new Exception("id vacios"); }
            
            extract($_POST,EXTR_PREFIX_ALL,"rq");
            #logica principal
            
            #arrojar resultados
            $rs = $this->oNegReportesasign->delete($rq_id);

            $this->printJSON(200, "Request completed", $rs);
        }catch(Exception $e){
            $this->printJSON(400, $e->getMessage(), "");   
        }
    }
}

?>