<?php
JrCargador::clase('sys_negocio::NegVimeo', RUTA_BASE);
include_once('cls.WebVimeo.php');
include_once('cls.WebVimeoFix.php');
defined('RUTA_BASE') or die();

class WebVimeoInterface extends JrWeb
{
	private $oWebVimeo;
	private $oNegVimeo;

	public function __construct()
	{
		parent::__construct();
		$this->oNegVimeo = new NegVimeo;
		$this->oWebVimeo = null;
	}
	public function procesarSubidaEmpresa($params = [], $return = false)
	{
		try {
			ignore_user_abort(true);
			$params = WebMine::getParams($params);
			$options = [];
			if (!isset($params["debug"])) {
				$options["debug"] = false;
			} else {
				$options["debug"] = (($params["debug"] == "true") ? true : false);
			}
			if (!isset($params["fake"])) {
				$options["fake"] = false;
			} else {
				$options["fake"] = (($params["fake"] == "true") ? true : false);
			}
			if (!isset($params["forzar"])) {
				$options["forzar"] = false;
			} else {
				$options["forzar"] = (($params["forzar"] == "true") ? true : false);
			}
			if (!isset($params["modo"])) {
				$options["modo"] = 0;
			} else {
				$options["modo"] = $params["modo"];
			}
			if (!isset($params["idproyecto"]) || empty($params["idproyecto"])) {
				throw new Exception("Error: Falta de parámetros (idproyecto)");
			}

			$this->oWebVimeo = WebVimeo::getInstance();
			$status = WebVimeo::getStatus();
			if (!$status["running"]) {
				$arrCursos = [];
				if (isset($params["filtrar"]) && $params["filtrar"] == "ingles") {
					$arrCursos = $this->oNegVimeo->listarCursosEmpresaIngles([
						"idproyecto" => $params["idproyecto"]
					]);
				} else {
					$arrCursos = $this->oNegVimeo->listarCursosEmpresa([
						"idproyecto" => $params["idproyecto"]
					]);
				}
				$options["idproyecto"] = $params["idproyecto"];
				foreach ($arrCursos as $key => $curso) {
					$options["idcurso"] = $curso["idcurso"];
					$options["idcomplementario"] = $curso["idcomplementario"];
					$this->procesarSubidaCurso($options, true);
				}
			} else {
				echo "\nprocesarSubidaEmpresa(): El proceso de subida ya está siendo ejecutado. idcurso: $status[idcurso], $status[idcomplementario]\n";
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function procesarSubidaCurso($params = array(), $return = false)
	{
		try {
			ignore_user_abort(true);
			$datos = "done";
			$params = WebMine::getParams($params);
			// if ($this->oWebVimeo == null) {
			if (!isset($params["debug"])) {
				$params["debug"] = false;
			}
			if (!isset($params["fake"])) {
				$params["fake"] = false;
			} else {
				$params["fake"] = (($params["fake"] == "true") ? true : false);
			}
			if (!isset($params["forzar"])) {
				$params["forzar"] = false;
			} else {
				$params["forzar"] = (($params["forzar"] == "true") ? true : false);
			}
			if (!isset($params["modo"])) {
				$params["modo"] = 0;
			}
			if (empty($params["idproyecto"])) {
				throw new Exception("Debe específicar el idproyecto");
				exit();
			}
			$this->oWebVimeo = WebVimeo::getInstance();
			$this->oWebVimeo->debug = $params["debug"];
			$this->oWebVimeo->fake = $params["fake"];
			$this->oWebVimeo->modo = $params["modo"];
			$this->oWebVimeo->forzar = $params["forzar"];
			$status = WebVimeo::getStatus();
			if (!$status["running"]) {
				echo "\nComenzando procesado de idcurso: $params[idcurso], idcomplementario: $params[idcomplementario]\n";
				$videosProcesados = $this->oWebVimeo->subirVideosCurso([
					"idcurso" => $params["idcurso"],
					"idcomplementario" => $params["idcomplementario"],
					"idproyecto" => $params["idproyecto"],
				], true);
				if ($videosProcesados !== false) {
					echo "\n----videosProcesados-----$videosProcesados\n";
					$this->oNegVimeo->insertarCursoProcesado([
						"idcurso" => $params["idcurso"],
						"idcomplementario" => $params["idcomplementario"],
						"estado" => "procesado",
						"videos_procesados" => $videosProcesados,
					]);
					echo "\nEl curso idcurso: $params[idcurso], idcomplementario: $params[idcomplementario] terminó de procesarse.\n\n";
				}
			} else {
				echo "\nprocesarSubidaCurso(): El proceso de subida ya está siendo ejecutado. idcurso: $status[idcurso], $status[idcomplementario]\n";
			}
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $datos));
				exit(0);
			} else {
				return $datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function procesarFixCurso($params = array(), $return = false)
	{
		try {
			ignore_user_abort(true);
			$datos = "done";
			$params = WebMine::getParams($params);
			// if ($this->oWebVimeo == null) {
			if (!isset($params["debug"])) {
				$params["debug"] = false;
			}
			if (!isset($params["fake"])) {
				$params["fake"] = false;
			} else {
				$params["fake"] = (($params["fake"] == "true") ? true : false);
			}
			if (!isset($params["forzar"])) {
				$params["forzar"] = false;
			} else {
				$params["forzar"] = (($params["forzar"] == "true") ? true : false);
			}
			if (!isset($params["modo"])) {
				$params["modo"] = 0;
			}
			if (empty($params["idproyecto"])) {
				throw new Exception("Debe específicar el idproyecto");
				exit();
			}
			$this->oWebVimeo = WebVimeoFix::getInstance();
			$this->oWebVimeo->debug = $params["debug"];
			$this->oWebVimeo->fake = $params["fake"];
			$this->oWebVimeo->modo = $params["modo"];
			$this->oWebVimeo->forzar = $params["forzar"];
			$status = WebVimeoFix::getStatus();
			if (!$status["running"]) {
				echo "\nComenzando procesado de idcurso: $params[idcurso], idcomplementario: $params[idcomplementario]\n";
				$videosProcesados = $this->oWebVimeo->fixVideosCurso([
					"idcurso" => $params["idcurso"],
					"idcomplementario" => $params["idcomplementario"],
					"idproyecto" => $params["idproyecto"],
				], true);
				if ($videosProcesados !== false) {
					echo "\n----videosProcesados-----$videosProcesados\n";
					$this->oNegVimeo->insertarCursoProcesado([
						"idcurso" => $params["idcurso"],
						"idcomplementario" => $params["idcomplementario"],
						"estado" => "procesado",
						"videos_procesados" => $videosProcesados,
					]);
					echo "\nEl curso idcurso: $params[idcurso], idcomplementario: $params[idcomplementario] terminó de procesarse.\n\n";
				}
			} else {
				echo "\nprocesarSubidaCurso(): El proceso de subida ya está siendo ejecutado. idcurso: $status[idcurso], $status[idcomplementario]\n";
			}
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $datos));
				exit(0);
			} else {
				return $datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
}
