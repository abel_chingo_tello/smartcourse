<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRub_estandar', RUTA_BASE);
class WebRub_estandar extends JrWeb
{
	private $oNegRub_estandar;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRub_estandar = new NegRub_estandar;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Rub_estandar', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["id_rubrica"])&&@$_REQUEST["id_rubrica"]!='')$filtros["id_rubrica"]=$_REQUEST["id_rubrica"];
			if(isset($_REQUEST["puntuacion"])&&@$_REQUEST["puntuacion"]!='')$filtros["puntuacion"]=$_REQUEST["puntuacion"];
			if(isset($_REQUEST["id_dimension"])&&@$_REQUEST["id_dimension"]!='')$filtros["id_dimension"]=$_REQUEST["id_dimension"];
			if(isset($_REQUEST["activo"])&&@$_REQUEST["activo"]!='')$filtros["activo"]=$_REQUEST["activo"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["duplicado_id_estandar"])&&@$_REQUEST["duplicado_id_estandar"]!='')$filtros["duplicado_id_estandar"]=$_REQUEST["duplicado_id_estandar"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRub_estandar->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id_estandar)) {
				$this->oNegRub_estandar->id_estandar = $id_estandar;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRub_estandar->nombre=@$nombre;
				$this->oNegRub_estandar->id_rubrica=@$id_rubrica;
				$this->oNegRub_estandar->puntuacion=@$puntuacion;
				$this->oNegRub_estandar->id_dimension=@$id_dimension;
				$this->oNegRub_estandar->activo=@$activo;
				$this->oNegRub_estandar->tipo=@$tipo;
				$this->oNegRub_estandar->duplicado_id_estandar=@$duplicado_id_estandar;
				
            if($accion=='_add') {
            	$res=$this->oNegRub_estandar->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_estandar')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRub_estandar->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_estandar')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRub_estandar->__set('id_estandar', $_REQUEST['id_estandar']);
			$res=$this->oNegRub_estandar->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRub_estandar->setCampo($_REQUEST['id_estandar'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}