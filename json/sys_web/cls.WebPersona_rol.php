<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-09-2019 
 * @copyright	Copyright (C) 12-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE);
class WebPersona_rol extends JrWeb
{
	private $oNegPersona_rol;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_rol = new NegPersona_rol;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_rol', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["iddetalle"])&&@$_REQUEST["iddetalle"]!='')$filtros["iddetalle"]=$_REQUEST["iddetalle"];
			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			//if(isset($_REQUEST["fecha_creacion"])&&@$_REQUEST["fecha_creacion"]!='')$filtros["fecha_creacion"]=$_REQUEST["fecha_creacion"];
			//if(isset($_REQUEST["fecha_actualizacion"])&&@$_REQUEST["fecha_actualizacion"]!='')$filtros["fecha_actualizacion"]=$_REQUEST["fecha_actualizacion"];
			//if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			$this->datos=$this->oNegPersona_rol->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $usuarioAct = NegSesion::getUsuario();           
        	$this->oNegPersona_rol->idpersonal=$idpersona;
        	$this->oNegPersona_rol->idempresa=!empty($idempresa)?$idempresa:$usuarioAct["idempresa"];
        	$this->oNegPersona_rol->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
        	$this->oNegPersona_rol->idrol=!empty($idrol)?$idrol:3;
        	$res=$this->oNegPersona_rol->agregar();
        	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_rol')).' '.JrTexto::_('saved successfully'),'newid'=>$res));                  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPersona_rol->__set('iddetalle', $_REQUEST['iddetalle']);
			$res=$this->oNegPersona_rol->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPersona_rol->setCampo($_REQUEST['iddetalle'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}