<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-09-2019 
 * @copyright	Copyright (C) 12-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
class WebEmpresa extends JrWeb
{
	private $oNegEmpresa;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegEmpresa = new NegEmpresa;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["rason_social"])&&@$_REQUEST["rason_social"]!='')$filtros["rason_social"]=$_REQUEST["rason_social"];
			if(isset($_REQUEST["ruc"])&&@$_REQUEST["ruc"]!='')$filtros["ruc"]=$_REQUEST["ruc"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["idpais"])&&@$_REQUEST["idpais"]!='')$filtros["idpais"]=$_REQUEST["idpais"];
			if(isset($_REQUEST["iddepartamento"])&&@$_REQUEST["iddepartamento"]!='')$filtros["iddepartamento"]=$_REQUEST["iddepartamento"];
			if(isset($_REQUEST["idprovincia"])&&@$_REQUEST["idprovincia"]!='')$filtros["idprovincia"]=$_REQUEST["idprovincia"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegEmpresa->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id)) {
				$this->oNegEmpresa->id = $id;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegEmpresa->nombre=@$nombre;
				$this->oNegEmpresa->rason_social=@$rason_social;
				$this->oNegEmpresa->ruc=@$ruc;
				$this->oNegEmpresa->direccion=@$direccion;
				$this->oNegEmpresa->telefono=@$telefono;
				$this->oNegEmpresa->email=@$email;
				$this->oNegEmpresa->idpersona=@$idpersona;
				$this->oNegEmpresa->idpais=@$idpais;
				$this->oNegEmpresa->iddepartamento=@$iddepartamento;
				$this->oNegEmpresa->idprovincia=@$idprovincia;
				$this->oNegEmpresa->estado=@$estado;
				
            if($accion=='_add') {
            	$res=$this->oNegEmpresa->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegEmpresa->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegEmpresa->__set('id', $_REQUEST['id']);
			$res=$this->oNegEmpresa->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEmpresa->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}