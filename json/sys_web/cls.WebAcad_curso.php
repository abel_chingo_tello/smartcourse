<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
class WebAcad_curso extends JrWeb
{
	private $oNegAcad_curso;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_curso = new NegAcad_curso;
				
	}

	public function defecto(){
		return $this->listado();
	}
	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_curso', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["imagen"])&&@$_REQUEST["imagen"]!='')$filtros["imagen"]=$_REQUEST["imagen"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["vinculosaprendizajes"])&&@$_REQUEST["vinculosaprendizajes"]!='')$filtros["vinculosaprendizajes"]=$_REQUEST["vinculosaprendizajes"];
			if(isset($_REQUEST["materialesyrecursos"])&&@$_REQUEST["materialesyrecursos"]!='')$filtros["materialesyrecursos"]=$_REQUEST["materialesyrecursos"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["objetivos"])&&@$_REQUEST["objetivos"]!='')$filtros["objetivos"]=$_REQUEST["objetivos"];
			if(isset($_REQUEST["certificacion"])&&@$_REQUEST["certificacion"]!='')$filtros["certificacion"]=$_REQUEST["certificacion"];
			if(isset($_REQUEST["costo"])&&@$_REQUEST["costo"]!='')$filtros["costo"]=$_REQUEST["costo"];
			if(isset($_REQUEST["silabo"])&&@$_REQUEST["silabo"]!='')$filtros["silabo"]=$_REQUEST["silabo"];
			if(isset($_REQUEST["e_ini"])&&@$_REQUEST["e_ini"]!='')$filtros["e_ini"]=$_REQUEST["e_ini"];
			if(isset($_REQUEST["pdf"])&&@$_REQUEST["pdf"]!='')$filtros["pdf"]=$_REQUEST["pdf"];
			if(isset($_REQUEST["abreviado"])&&@$_REQUEST["abreviado"]!='')$filtros["abreviado"]=$_REQUEST["abreviado"];
			if(isset($_REQUEST["e_fin"])&&@$_REQUEST["e_fin"]!='')$filtros["e_fin"]=$_REQUEST["e_fin"];
			if(isset($_REQUEST["aniopublicacion"])&&@$_REQUEST["aniopublicacion"]!='')$filtros["aniopublicacion"]=$_REQUEST["aniopublicacion"];
			if(isset($_REQUEST["autor"])&&@$_REQUEST["autor"]!='')$filtros["autor"]=$_REQUEST["autor"];
			if(isset($_REQUEST["txtjson"])&&@$_REQUEST["txtjson"]!='')$filtros["txtjson"]=$_REQUEST["txtjson"];						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["edukt"])&&@$_REQUEST["edukt"]!='')$filtros["edukt"]=$_REQUEST["edukt"];
			if(isset($_REQUEST["eduktmaestro"])&&@$_REQUEST["eduktmaestro"]!='')$filtros["eduktmaestro"]=$_REQUEST["eduktmaestro"];			
			if(!empty($_REQUEST["idproyecto"])){
				$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			}else{
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];
			}
			$datos=$datos1=array();
			if(empty($_REQUEST["noidproyecto"])){
				$datos=$this->oNegAcad_curso->buscarcursos($filtros); // solo mis cursos
			}else{
				if($_REQUEST["noidproyecto"]=='todos'){ // primero mis cursos y despues lo no asignados		
					$datos=$this->oNegAcad_curso->buscarcursos($filtros);
					$filtros['noidproyecto']=$_REQUEST["noidproyecto"];
					$filtros['idmiscursos']=array();
					foreach ($datos as $k => $v) {
						$filtros['idmiscursos'][]=$v["idcurso"];
					}		
					$datos1=$this->oNegAcad_curso->buscarcursos($filtros);
				}else{
					$datos=$this->oNegAcad_curso->buscarcursos($filtros);
					$filtros['noidproyecto']='solo'; // solo los cursos no asinados ,esto falta
					$filtros['idmiscursos']=array();
					foreach ($datos as $k => $v) {
						$filtros['idmiscursos'][]=$v["idcurso"];
					}					
					$datos1=$this->oNegAcad_curso->buscarcursos($filtros);
				}
				if(!empty($datos1))
					foreach ($datos1 as $v) {
						$datos[]=$v;
					}
			}			
			echo json_encode(array('code'=>200,'data'=>$datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function cursosprincipales(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();
			$usuarioAct = NegSesion::getUsuario();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			//if(!empty($_REQUEST["sqlasignados"])){ 
			//	$filtros["sqlasignados"]=true;
			//	if(!empty($_REQUEST["noasignados"])) $filtros["noasignados"]=true;
			//}
			$filtros["sqlasignados"]=true;
			$filtros["noasignados"]=true;
			$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];
			$datos=$this->oNegAcad_curso->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$datos));
		 	exit(0);
		}catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}


	public function cursos(){ //mostrarcursos
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();
			$filtros["todos"]=isset($_REQUEST["todos"])?true:false;
			$filtros["allcategorias"]=isset($_REQUEST["todos"])?true:false;
			$datos=$this->oNegAcad_curso->cursos($filtros);

			echo json_encode(array('code'=>200,'data'=>$datos));
			//$datos1=$this->oNegAcad_curso->buscarcursos($filtros);
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function synccursos_(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			$datos=$this->oNegAcad_curso->todos($filtros);	
			echo json_encode(array('code'=>200,'data'=>$datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function buscarcursos(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];						
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			// if(!empty($_REQUEST["idcarrera"])){ $filtros["idcarrera"] = $_REQUEST["idcarrera"]; }
			if(isset($_REQUEST["orderby"])&&@$_REQUEST["orderby"]!='')$filtros["orderby"]=$_REQUEST["orderby"];
			if(isset($_REQUEST["edukt"])&&@$_REQUEST["edukt"]!='')$filtros["edukt"]=$_REQUEST["edukt"];
			if(isset($_REQUEST["eduktmaestro"])&&@$_REQUEST["eduktmaestro"]!='')$filtros["eduktmaestro"]=$_REQUEST["eduktmaestro"];
			
			$datos=$this->oNegAcad_curso->buscarcursos($filtros);	
			echo json_encode(array('code'=>200,'data'=>$datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function copiar(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
            if(empty($_REQUEST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_REQUEST);	
			$idcurso=!empty($idcurso)?$idcurso:0;
			$idcc=!empty($idcomplementario)?$idcomplementario:0;
			$accion=!empty($accion)?$accion:'copiar';
			$tipo=!empty($tipo)?$tipo:1;
			$como=!empty($como)?$como:1;
			$idproyecto=!empty($idproyecto)?$idproyecto:1;
			$nombrecurso=!empty($nombre)?$nombre:'';
			JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
 			$oNegProyecto = new NegProyecto;
			$proyecto=$oNegProyecto->buscar(array('idproyecto'=>$idproyecto));
			if(empty($proyecto[0])){
				echo json_encode(array('code'=>'Error','msj'=>'La empresa seleccionada no tiene proyecto creado'));
				exit();
			}
			$proyecto=$proyecto[0];
			JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
			$oNegCurso = new NegAcad_curso;			
			JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
			$oNegCursodetalle = new NegAcad_cursodetalle;
 			JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
			$oNegPersonal = new NegPersonal;

 			$curso=array();
 			$temas=array();
			if(!empty($idcc)){
				$accion=='copiar';
				$curso=$oNegCurso->buscar(array("complementario"=>true,"idcomplementario"=>$idcc,'idcurso'=>$idcurso));				
				$temas=$oNegCursodetalle->buscarconnivel(array('idcurso'=>$idcc,"complementario"=>true));
			}else{
				$temas=$oNegCursodetalle->buscarconnivel(array('idcurso'=>$idcurso));
				if($tipo==2) $accion=='copiar';
				if($accion=='copiar')
					$curso=$oNegCurso->buscar(array('idcurso'=>$idcurso));
			}
			if(empty($curso[0]) && $accion=='copiar'){
				echo json_encode(array('code'=>'Error','msj'=>'Curso  no existe'));
				exit();
			} 
			if(empty($temas) && $accion=='copiar'){
				echo json_encode(array('code'=>'Error','msj'=>'Este curso no tiene temas'));
				exit();
			}
			JrCargador::clase('sys_negocio::NegProyecto_cursos', RUTA_BASE);
			$oNegProyecto_cursos = new NegProyecto_cursos;
			$usuarioAct = NegSesion::getUsuario();
			$estados_proyecto_curso = array(
				"idexterno"=>$idcurso
				,"tipo"=>$como
				,'idrol'=>$usuarioAct["idrol"]
				,'idproyecto'=>$idproyecto
				,'idempresa'=>$proyecto["idempresa"]
				,'idproducto'=>1
				,'validando'=>true
			);
			//var_dump($proyecto);
			$persona = $oNegPersonal->buscar(array("idpersona" => $proyecto["idrepresentante"]))[0];
			//var_dump($persona);

			$estados_proyecto_curso['tipodoc'] =$persona["tipodoc"];
			$estados_proyecto_curso['dni'] =$persona["dni"];
			$estados_proyecto_curso['usuario'] =$persona["usuario"];

			if($accion!='copiar'){
				$hayProyectocurso=$oNegProyecto_cursos->buscar(array('idcurso'=>$idcurso,'idproyecto'=>$idproyecto));
				if(empty($hayProyectocurso)){
					$oNegProyecto_cursos->idcurso=@$idcurso;
					$oNegProyecto_cursos->idproyecto=@$idproyecto;
					$res=$oNegProyecto_cursos->agregar();	
				}				
				//var_dump($estados_proyecto_curso);
				$POSTDATA = array("estados" => $estados_proyecto_curso);
				$TARGET = _URL_SKS_."/json/cursos/acceso";
				$r2 = NegTools::requestHTTP($POSTDATA, $TARGET);
				if(empty($r2) && $r2['code'] != 200){
					echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Curso asignado correctamente a la empresa, pero no se pudo registrar en SKS '),'asignado'=>1));
					exit();
				}
				echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Curso asignado correctamente'),'asignado'=>1));
				exit();

			}else{
				$usuarioAct = NegSesion::getUsuario();			
				$datoscurso=$oNegCurso->duplicarcurso($idcurso,$idcc,$como,$idproyecto,$usuarioAct["idpersona"],$temas,$nombrecurso);
				if(!empty($datoscurso)){
					$newidcurso=$datoscurso["idcurso"];
					$datenow = date("Y-m-d H:i:s");
					$estados_curso = array(
						'idexterno'=>$newidcurso
						,'idrol'=>$usuarioAct["idrol"]
						,'tuser'=>$usuarioAct["tuser"]
						,'idproyecto'=>$idproyecto
						,'idempresa'=>$proyecto["idempresa"]
						,'idproducto'=>$como==2?5:1    // 1 edukvirtual , 5 eduktmaestro
						,'idgrupo'=>0
						,'nombre'=>@trim($datoscurso["nombrecurso"])
						,'descripcion'=>@trim($datoscurso["descripcioncurso"])
						,'tipo'=>$como
						,'estado'=>1
						,'fecha_creacion'=>$datenow
						,'fecha_actualizacion'=>$datenow
					);
					$persona = $oNegPersonal->buscar(array("idpersona" => $proyecto["idrepresentante"]))[0];
					$estados_curso['tipodoc'] =$persona["tipodoc"];
					$estados_curso['dni'] =$persona["dni"];
					$estados_curso['usuario'] =$persona["usuario"];
					$POSTDATA = array("estados" => $estados_curso);
					$TARGET = _URL_SKS_."/json/cursos/registrar";
					$r2 = NegTools::requestHTTP($POSTDATA, $TARGET);
					if(empty($r2) && $r2['code'] != 200){
						echo json_encode(array('code'=>200,'msj'=>'Curso copiado correctamente SC , pero  no se pudo asignar en SKS'));
		 				exit(0);
					}
					$estados_proyecto_curso["idexterno"]=$newidcurso;
					$estados_proyecto_curso["validando"]=false;
					$POSTDATA = array("estados" => $estados_proyecto_curso);
					$TARGET = _URL_SKS_."/json/cursos/acceso";
					$r2 = NegTools::requestHTTP($POSTDATA, $TARGET);
					if(empty($r2) && $r2['code'] != 200){
						echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Curso asignado correctamente a la empresa, pero no se pudo registrar en SKS '),'asignado'=>1));
						exit();
					}
				}
			}
			echo json_encode(array('code'=>200,'msj'=>'Datos copiados correctamente'));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }

	}

	public function reordenar(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			if(empty($_REQUEST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $datos=json_decode($_REQUEST["datos"],true);
            $usuarioAct = NegSesion::getUsuario();
            $idproyecto=$usuarioAct["idproyecto"];
            JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
			$oNegCurso = new NegAcad_curso;
			$oNegCurso->reordenar($idproyecto,$datos);			
            echo json_encode(array('code'=>200,'msj'=>'Cursos ordenados correctamente'));
		 	exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}

	}

	public function buscar(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();
			//$filtros["sql3"] = true;
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			$datos = $this->oNegAcad_curso->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$datos));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idcurso)) {
				$this->oNegAcad_curso->idcurso = $idcurso;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegAcad_curso->nombre=@$nombre;
			$this->oNegAcad_curso->imagen=@$imagen;
			$this->oNegAcad_curso->descripcion=@$descripcion;
			$this->oNegAcad_curso->estado=@$estado;
			$this->oNegAcad_curso->fecharegistro=@$fecharegistro;
			$this->oNegAcad_curso->idusuario=@$idusuario;
			$this->oNegAcad_curso->vinculosaprendizajes=@$vinculosaprendizajes;
			$this->oNegAcad_curso->materialesyrecursos=@$materialesyrecursos;
			$this->oNegAcad_curso->color=@$color;
			$this->oNegAcad_curso->objetivos=@$objetivos;
			$this->oNegAcad_curso->certificacion=@$certificacion;
			$this->oNegAcad_curso->costo=@$costo;
			$this->oNegAcad_curso->silabo=@$silabo;
			$this->oNegAcad_curso->e_ini=@$e_ini;
			$this->oNegAcad_curso->pdf=@$pdf;
			$this->oNegAcad_curso->abreviado=@$abreviado;
			$this->oNegAcad_curso->e_fin=@$e_fin;
			$this->oNegAcad_curso->aniopublicacion=@$aniopublicacion;
			$this->oNegAcad_curso->autor=@$autor;
			$this->oNegAcad_curso->txtjson=@$txtjson;
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_curso->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_curso')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_curso->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_curso')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {			
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			@extract($_REQUEST);
			$user = NegSesion::getUsuario();
			$eliminar=!empty($eliminar)?$eliminar:false;
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:-1;
			$idcc=!empty($_REQUEST["idcc"])?$_REQUEST["idcc"]:0;
			$tipo=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:-1;			
			$this->oNegAcad_curso->validar_y_eliminar($idcurso,$idcc,$tipo,$eliminar);
			$POSTDATA = array(
				"idexterno" => $idcurso,
				"tipo" => $tipo,
				'idproyecto'=>$user["idproyecto"],
				//'idcc'=>$idcc
			);
			$TARGET = _URL_SKS_."/json/cursos/eliminar";
			if(empty($idcc)){
				$r2 = NegTools::requestHTTP($POSTDATA, $TARGET);
				if(empty($r2) && $r2['code'] != 200){
					throw new Exception("Ocurrio algo en la peticion");
				}
			}
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','msj'=>'Error al eliminar curso '.$e->getMessage()));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			// $idcomplementario = (isset($_REQUEST["icc"]) ? );
			$idcomplementario = 0;
			if(isset($_REQUEST["icc"])&&@$_REQUEST["icc"]!=''){
				$idcomplementario = $_REQUEST["icc"];
			}
			if($idcomplementario != 0){
				$this->oNegAcad_curso->setCampo_($idcomplementario,$_REQUEST['campo'],$_REQUEST['valor'], "acad_curso_complementario");
			} else {
				$this->oNegAcad_curso->setCampo_($_REQUEST['idcurso'],$_REQUEST['campo'],$_REQUEST['valor'], "acad_curso");
				if($_REQUEST['campo'] == "nombre"){
					// var_dump(11);
					$POSTDATA = array(
						"idexterno" => $_REQUEST['idcurso'],
						"idproducto_url" => 1,
						"valor" => $_REQUEST['valor']
					);
					$TARGET = _URL_SKS_."/json/cursos/setnombre";
					$r2 = NegTools::requestHTTP($POSTDATA, $TARGET);
					if(empty($r2) && $r2['code'] != 200){
						throw new Exception("Ocurrio algo en la peticion");
					}
				}
			}
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
	public function leer(){
		try {
			$filtros = array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];

			$res=$this->oNegAcad_curso->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$res));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	} 

	public function guardarcategoria(){
		try {
			$filtros = array();
			$filtros["idcurso"]=isset($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$filtros["idcomplementario"]=isset($_REQUEST["idcomplementario"])?$_REQUEST["idcomplementario"]:0;
			$filtros["idcategoria"]=isset($_REQUEST["idcategoria"])?$_REQUEST["idcategoria"]:0;
			$filtros["tipo"]=isset($_REQUEST["tipo"])?$_REQUEST["tipo"]:1;
			if(empty($filtros["idcurso"])) echo json_encode(array('code'=>200,'msj'=>'Curso no existe'));			
			$res=$this->oNegAcad_curso->guardarcategoria($filtros);
			echo json_encode(array('code'=>200,'msj'=>'Datos Guardados '));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}	
	}  
}