<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-03-2020 
 * @copyright	Copyright (C) 17-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_mensaje', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
class WebAcad_mensaje extends JrWeb
{
	private $oNegAcad_mensaje;
	protected $oNegPersonal;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_mensaje = new NegAcad_mensaje;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegProyecto = new NegProyecto;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_mensaje', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["id"]) && @$_REQUEST["id"] != '') $filtros["id"] = $_REQUEST["id"];
			if (isset($_REQUEST["de"]) && @$_REQUEST["de"] != '') $filtros["de"] = $_REQUEST["de"];
			if (isset($_REQUEST["asunto"]) && @$_REQUEST["asunto"] != '') $filtros["asunto"] = $_REQUEST["asunto"];
			if (isset($_REQUEST["mensaje"]) && @$_REQUEST["mensaje"] != '') $filtros["mensaje"] = $_REQUEST["mensaje"];
			if (isset($_REQUEST["fecha_hora"]) && @$_REQUEST["fecha_hora"] != '') $filtros["fecha_hora"] = $_REQUEST["fecha_hora"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];

			if (isset($_REQUEST["para"]) && @$_REQUEST["para"] != '') {
				if ($_REQUEST["para"] == "mi") {
					$usuarioActual = NegSesion::getUsuario();
					$filtros["para"] = $usuarioActual["idpersona"];
				} else {
					$filtros["para"] = $_REQUEST["para"];
				}
			}
			$this->datos = $this->oNegAcad_mensaje->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$usuarioAct = NegSesion::getUsuario();

			$estados = array(
				'de' => $usuarioAct["idpersona"], 'asunto' => $asunto, 'mensaje' => $mensaje, 'fecha_hora' => date("Y-m-d H:i:s")
			);

			$infoEmpresa = $this->oNegProyecto->buscar(array('idproyecto' => $usuarioAct["idproyecto"]));
			if ($id == '') {
				$res = $this->oNegAcad_mensaje->agregar($estados, $para);
				echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Mensaje Enviado'), 'newid' => $res));
			} else {
				$estados["id"] = $id;
				$res = $this->oNegAcad_mensaje->editar($estados);
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Acad_mensaje')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}
			// if (!empty($infoEmpresa[0])) {
			// 	$this->empresa = $infoEmpresa[0];
			// 	$this->empresa["nombre"] = !empty($this->empresa["nombre"]) ? $this->empresa["nombre"] : @$this->empresa["empresa"];
			// 	JrCargador::clase('jrAdwen::JrCorreo');
			// 	$oCorreo = new JrCorreo;
			// 	$deemail = !empty($this->empresa["correoempresa"]) ? $this->empresa["correoempresa"] : $usuarioAct["email"];
			// 	$denombre = !empty($this->empresa["nombre"]) ? $this->empresa["nombre"] : $usuarioAct["nombre_full"];
			// 	$oCorreo->setRemitente($deemail, $denombre);
			// 	$this->titulo = '---';
			// 	$this->Asunto = $asunto;
			// 	$this->mensaje = $mensaje;
			// 	if (!empty($usuarioAct["email"])) $oCorreo->addDestinarioPhpmailer($usuarioAct["email"], $usuarioAct["nombre_full"]);
			// 	//$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com','Abel');
			// 	if ($usuarioAct["idrol"] == 3 && !empty($usuarioAct["emailpadre"])) $oCorreo->addDestinarioPhpmailer($usuarioAct["emailpadre"], 'Padre de ' . $usuarioAct["nombre_full"]);
			// 	$filelogo = !empty($this->empresa["logo"]) ? $this->empresa["logo"] : $this->empresa["logoempresa"];
			// 	if (!empty($filelogo)) {
			// 		$ipost = stripos($filelogo, '?');
			// 		if ($ipost === false) $filelogo = $filelogo;
			// 		else $filelogo = substr($filelogo, 0, $ipost);
			// 	}
			// 	if (!empty($filelogo) && is_file(RUTA_BASE . $filelogo)) {
			// 		$ipost = stripos($filelogo, '?');
			// 		if ($ipost === false) $this->logoempresa = $filelogo;
			// 		else $this->logoempresa = substr($filelogo, 0, $ipost);
			// 		$img = str_replace('\\', '/', RUTA_BASE . $this->logoempresa);
			// 		$oCorreo->addadjuntos(RUTA_BASE . $this->logoempresa, 'logoempresa');
			// 	}

			// 	if (!empty($this->empresa["correoempresa"]))
			// 		$oCorreo->addDestinarioPhpmailer($this->empresa["correoempresa"], $this->empresa["nombre"]);

			// 	$idpersonas = array();
			// 	if (!empty($para)) {
			// 		foreach ($para as $k => $v) {
			// 			$idpersonas[] = $v;
			// 		}
			// 	} else {
			// 		echo json_encode(array('code' => 'Error', 'msj' => 'No hay destintarios validos en este Mensaje'));
			// 		exit(0);
			// 	}

			// 	$personas = $this->oNegPersonal->buscar(array('idpersona' => $idpersonas));
			// 	if (!empty($personas))
			// 		foreach ($personas as $k => $p) {
			// 			$nombre = $p["ape_paterno"] . " " . $p["ape_materno"] . " " . $p["nombre"];
			// 			if (!empty($p["email"]))
			// 				$oCorreo->addDestinarioPhpmailer($p["email"], $nombre);
			// 			if (!empty($p["emailpadre"]))
			// 				$oCorreo->addDestinarioPhpmailer($p["emailpadre"], 'Padre de ' . $nombre);
			// 		}

			// 	$this->para = '';
			// 	try {
			// 		$this->esquema = 'correos/general_empresa1';
			// 		$oCorreo->setMensaje(parent::getEsquema());
			// 		$envio = $oCorreo->sendPhpmailer();
			// 	} catch (Exception $ex) {
			// 		// echo json_encode(array('code' => 'Error', 'msj' => 'No Se enviaron los correos intentelo mas tarde'));
			// 		exit(0);
			// 	}
			// }


			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegAcad_mensaje->de = @$de;
					$this->oNegAcad_mensaje->asunto = @$asunto;
					$this->oNegAcad_mensaje->mensaje = @$mensaje;
					$this->oNegAcad_mensaje->fecha_hora = @$fecha_hora;
					$this->oNegAcad_mensaje->estado = @$estado;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_mensaje->__set('id', $_REQUEST['id']);
			$res = $this->oNegAcad_mensaje->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_mensaje->setCampo($_REQUEST['id'], $_REQUEST['campo'], $_REQUEST['valor'], $_REQUEST['tabla']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
