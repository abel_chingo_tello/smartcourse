<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAmbiente', RUTA_BASE);
class WebAmbiente extends JrWeb
{
	private $oNegAmbiente;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAmbiente = new NegAmbiente;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ambiente', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["numero"])&&@$_REQUEST["numero"]!='')$filtros["numero"]=$_REQUEST["numero"];
			if(isset($_REQUEST["capacidad"])&&@$_REQUEST["capacidad"]!='')$filtros["capacidad"]=$_REQUEST["capacidad"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["turno"])&&@$_REQUEST["turno"]!='')$filtros["turno"]=$_REQUEST["turno"];						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(empty($_REQUEST["idproyecto"])){
				$usuarioAct = NegSesion::getUsuario();
			}
			$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];		
			$this->datos=$this->oNegAmbiente->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idambiente)) {
				$this->oNegAmbiente->idambiente = $idambiente;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();          	
	        
			$this->oNegAmbiente->idlocal=@$idlocal;
			$this->oNegAmbiente->numero=@$numero;
			$this->oNegAmbiente->capacidad=@$capacidad;
			$this->oNegAmbiente->tipo=@$tipo;
			$this->oNegAmbiente->estado=!isset($estado)?$estado:1;
			$this->oNegAmbiente->turno=@$turno;
			$this->oNegAmbiente->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];	
            if($accion=='_add') {
            	$res=$this->oNegAmbiente->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Ambiente')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAmbiente->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Ambiente')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
				exit(0);
			}

			$this->oNegAmbiente->__set('idambiente', $_REQUEST['idambiente']);
			$res=$this->oNegAmbiente->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
				exit(0);
			}
			$this->oNegAmbiente->setCampo($_REQUEST['idambiente'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>JrTexto::_('data incomplete')));
			exit(0);
		}
	}   
}