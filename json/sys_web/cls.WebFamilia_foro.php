<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-03-2020 
 * @copyright	Copyright (C) 30-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegFamilia_foro', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebFamilia_foro extends JrWeb
{
	private $oNegFamilia_foro;
	private $oNegPersonal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegFamilia_foro = new NegFamilia_foro;
		$this->oNegPersonal = new NegPersonal;
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Familia_foro', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["codigo"])&&@$_REQUEST["codigo"]!='')$filtros["codigo"]=$_REQUEST["codigo"];
			if(isset($_REQUEST["persona"])&&@$_REQUEST["persona"]!='')$filtros["persona"]=$_REQUEST["persona"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["likes"])&&@$_REQUEST["likes"]!='')$filtros["likes"]=$_REQUEST["likes"];
			if(isset($_REQUEST["like_de"])&&@$_REQUEST["like_de"]!='')$filtros["like_de"]=$_REQUEST["like_de"];
			if(isset($_REQUEST["contenido"])&&@$_REQUEST["contenido"]!='')$filtros["contenido"]=$_REQUEST["contenido"];
			if(isset($_REQUEST["fecha_hora"])&&@$_REQUEST["fecha_hora"]!='')$filtros["fecha_hora"]=$_REQUEST["fecha_hora"];
			if(isset($_REQUEST["respuesta"])&&@$_REQUEST["respuesta"]!='')$filtros["respuesta"]=$_REQUEST["respuesta"];
			if(isset($_REQUEST["sql"])&&@$_REQUEST["sql"]!='')$filtros["sql"]=$_REQUEST["sql"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegFamilia_foro->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);

			$idproyecto = $_REQUEST["idproyecto"];
			$idempresa = $_REQUEST["idempresa"];
			$filtros = array(
				"tipodoc" => $_REQUEST["tipodoc"],
				"dni" => $_REQUEST["dni"],
				"ape_paterno" => $_REQUEST["codigo_"],
				"idproyecto" => $idproyecto,
				"idempresa" => $idempresa,
				"idrol" => 3
			);
			$personal = $this->oNegPersonal->webService($filtros);
			if(count($personal) > 0){
				$personal = $personal[0];
				$persona = "Padre/Madre de " . $personal["nombre"] . " " . $personal["ape_paterno"];
				$estados = array(
					'codigo' => @$codigo
					,'persona'=>$persona
					,'idproyecto'=>$idproyecto
					,'idempresa'=>$idempresa
					,'contenido'=>$contenido
					,'fecha_hora'=>date("Y-m-d H:i:s")
				);
	
				if(!empty(@$respuesta)) {
					$estados['respuesta'] = $respuesta;
				}
					
				if(empty(@$codigo)) {
					$res=$this->oNegFamilia_foro->agregar($estados);
					 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Familia_foro')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
				}else{
					$res=$this->oNegFamilia_foro->editar();
					echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Familia_foro')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
				}
			} else {
				echo json_encode(array('code'=>300,'msj'=>JrTexto::_('Datos del Alumno Incorrectos'))); 
			}
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar2(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
			@extract($_POST);
			
			$idproyecto = $_REQUEST["idproyecto"];
			$idempresa = $_REQUEST["idempresa"];
			$personal = NegSesion::getUsuario();
			$estados = array(
				'codigo' => @$codigo
				,'persona'=>$personal["nombre_full"]
				,'idproyecto'=>$idproyecto
				,'idempresa'=>$idempresa
				,'contenido'=>$contenido
				,'fecha_hora'=>date("Y-m-d H:i:s")
			);

			if(!empty(@$respuesta)) {
				$estados['respuesta'] = $respuesta;
			}
				
			if(empty(@$codigo)) {
				$res=$this->oNegFamilia_foro->agregar($estados);
					echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Familia_foro')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
			}else{
				$res=$this->oNegFamilia_foro->editar();
				echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Familia_foro')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
			}
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegFamilia_foro->persona=@$persona;
				$this->oNegFamilia_foro->idproyecto=@$idproyecto;
				$this->oNegFamilia_foro->idempresa=@$idempresa;
				$this->oNegFamilia_foro->likes=@$likes;
				$this->oNegFamilia_foro->like_de=@$like_de;
				$this->oNegFamilia_foro->contenido=@$contenido;
				$this->oNegFamilia_foro->fecha_hora=@$fecha_hora;
				$this->oNegFamilia_foro->respuesta=@$respuesta;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegFamilia_foro->__set('codigo', $_REQUEST['codigo']);
			$res=$this->oNegFamilia_foro->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegFamilia_foro->setCampo($_REQUEST['codigo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}