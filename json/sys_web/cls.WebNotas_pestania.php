<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_pestania', RUTA_BASE);
class WebNotas_pestania extends JrWeb
{
	private $oNegNotas_pestania;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotas_pestania = new NegNotas_pestania;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["abreviatura"])&&@$_REQUEST["abreviatura"]!='')$filtros["abreviatura"]=$_REQUEST["abreviatura"];
			if(isset($_REQUEST["tipo_pestania"])&&@$_REQUEST["tipo_pestania"]!='')$filtros["tipo_pestania"]=$_REQUEST["tipo_pestania"];
			if(isset($_REQUEST["tipo_info"])&&@$_REQUEST["tipo_info"]!='')$filtros["tipo_info"]=$_REQUEST["tipo_info"];
			if(isset($_REQUEST["info_valor"])&&@$_REQUEST["info_valor"]!='')$filtros["info_valor"]=$_REQUEST["info_valor"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["idpestania_padre"])&&@$_REQUEST["idpestania_padre"]!='')$filtros["idpestania_padre"]=$_REQUEST["idpestania_padre"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegNotas_pestania->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idpestania)) {
				$this->oNegNotas_pestania->idpestania = $idpestania;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegNotas_pestania->nombre=@$nombre;
				$this->oNegNotas_pestania->abreviatura=@$abreviatura;
				$this->oNegNotas_pestania->tipo_pestania=@$tipo_pestania;
				$this->oNegNotas_pestania->tipo_info=@$tipo_info;
				$this->oNegNotas_pestania->info_valor=@$info_valor;
				$this->oNegNotas_pestania->color=@$color;
				$this->oNegNotas_pestania->orden=@$orden;
				$this->oNegNotas_pestania->idarchivo=@$idarchivo;
				$this->oNegNotas_pestania->idpestania_padre=@$idpestania_padre;
				
            if($accion=='_add') {
            	$res=$this->oNegNotas_pestania->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNotas_pestania->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegNotas_pestania->__set('idpestania', $_REQUEST['idpestania']);
			$res=$this->oNegNotas_pestania->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegNotas_pestania->setCampo($_REQUEST['idpestania'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}