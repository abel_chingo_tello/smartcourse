<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE);
class WebGrupo_matricula extends JrWeb
{
	private $oNegGrupo_matricula;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGrupo_matricula = new NegGrupo_matricula;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Grupo_matricula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idgrupo"])&&@$_REQUEST["idgrupo"]!='')$filtros["idgrupo"]=$_REQUEST["idgrupo"];
			if(isset($_REQUEST["fechamatricula"])&&@$_REQUEST["fechamatricula"]!='')$filtros["fechamatricula"]=$_REQUEST["fechamatricula"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["subgrupos"])&&@$_REQUEST["subgrupos"]!='')$filtros["subgrupos"]=$_REQUEST["subgrupos"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegGrupo_matricula->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idgrupo_matricula)) {
				$this->oNegGrupo_matricula->idgrupo_matricula = $idgrupo_matricula;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegGrupo_matricula->idalumno=@$idalumno;
				$this->oNegGrupo_matricula->idgrupo=@$idgrupo;
				$this->oNegGrupo_matricula->fechamatricula=@$fechamatricula;
				$this->oNegGrupo_matricula->estado=@$estado;
				$this->oNegGrupo_matricula->regusuario=@$regusuario;
				$this->oNegGrupo_matricula->regfecha=@$regfecha;
				$this->oNegGrupo_matricula->subgrupos=@$subgrupos;
				
            if($accion=='_add') {
            	$res=$this->oNegGrupo_matricula->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Grupo_matricula')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegGrupo_matricula->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Grupo_matricula')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegGrupo_matricula->__set('idgrupo_matricula', $_REQUEST['idgrupo_matricula']);
			$res=$this->oNegGrupo_matricula->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegGrupo_matricula->setCampo($_REQUEST['idgrupo_matricula'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}