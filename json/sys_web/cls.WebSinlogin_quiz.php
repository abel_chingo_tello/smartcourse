<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-01-2021 
 * @copyright	Copyright (C) 15-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSinlogin_quiz', RUTA_BASE);
class WebSinlogin_quiz extends JrWeb
{
	private $oNegSinlogin_quiz;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSinlogin_quiz = new NegSinlogin_quiz;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	 

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegSinlogin_quiz->idnota=@$idnota;
				$this->oNegSinlogin_quiz->idexamen=@$idexamen;
				$this->oNegSinlogin_quiz->tipo=@$tipo;
				$this->oNegSinlogin_quiz->nota=@$nota;
				$this->oNegSinlogin_quiz->notatexto=@$notatexto;
				$this->oNegSinlogin_quiz->regfecha=@$regfecha;
				$this->oNegSinlogin_quiz->idproyecto=@$idproyecto;
				$this->oNegSinlogin_quiz->calificacion_en=@$calificacion_en;
				$this->oNegSinlogin_quiz->calificacion_total=@$calificacion_total;
				$this->oNegSinlogin_quiz->calificacion_min=@$calificacion_min;
				$this->oNegSinlogin_quiz->tiempo_total=@$tiempo_total;
				$this->oNegSinlogin_quiz->tiempo_realizado=@$tiempo_realizado;
				$this->oNegSinlogin_quiz->calificacion=@$calificacion;
				$this->oNegSinlogin_quiz->habilidades=@$habilidades;
				$this->oNegSinlogin_quiz->habilidad_puntaje=@$habilidad_puntaje;
				$this->oNegSinlogin_quiz->intento=@$intento;
				$this->oNegSinlogin_quiz->datos=@$datos;
				$this->oNegSinlogin_quiz->fechamodificacion=@$fechamodificacion;
				$this->oNegSinlogin_quiz->preguntas=@$preguntas;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegSinlogin_quiz->__set('idnota', $_REQUEST['idnota']);
			$res=$this->oNegSinlogin_quiz->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegSinlogin_quiz->setCampo($_REQUEST['idnota'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}