<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegReportes_resumen', RUTA_BASE);
class WebReportes_resumen extends JrWeb
{
	private $oNegReportes_resumen;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegReportes_resumen = new NegReportes_resumen;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Reportes_resumen', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["fecha_modificacion"])&&@$_REQUEST["fecha_modificacion"]!='')$filtros["fecha_modificacion"]=$_REQUEST["fecha_modificacion"];
			if(isset($_REQUEST["prog_curso_A1"])&&@$_REQUEST["prog_curso_A1"]!='')$filtros["prog_curso_A1"]=$_REQUEST["prog_curso_A1"];
			if(isset($_REQUEST["prog_hab_L_A1"])&&@$_REQUEST["prog_hab_L_A1"]!='')$filtros["prog_hab_L_A1"]=$_REQUEST["prog_hab_L_A1"];
			if(isset($_REQUEST["prog_hab_R_A1"])&&@$_REQUEST["prog_hab_R_A1"]!='')$filtros["prog_hab_R_A1"]=$_REQUEST["prog_hab_R_A1"];
			if(isset($_REQUEST["prog_hab_W_A1"])&&@$_REQUEST["prog_hab_W_A1"]!='')$filtros["prog_hab_W_A1"]=$_REQUEST["prog_hab_W_A1"];
			if(isset($_REQUEST["prog_hab_S_A1"])&&@$_REQUEST["prog_hab_S_A1"]!='')$filtros["prog_hab_S_A1"]=$_REQUEST["prog_hab_S_A1"];
			if(isset($_REQUEST["prog_curso_A2"])&&@$_REQUEST["prog_curso_A2"]!='')$filtros["prog_curso_A2"]=$_REQUEST["prog_curso_A2"];
			if(isset($_REQUEST["prog_hab_L_A2"])&&@$_REQUEST["prog_hab_L_A2"]!='')$filtros["prog_hab_L_A2"]=$_REQUEST["prog_hab_L_A2"];
			if(isset($_REQUEST["prog_hab_R_A2"])&&@$_REQUEST["prog_hab_R_A2"]!='')$filtros["prog_hab_R_A2"]=$_REQUEST["prog_hab_R_A2"];
			if(isset($_REQUEST["prog_hab_W_A2"])&&@$_REQUEST["prog_hab_W_A2"]!='')$filtros["prog_hab_W_A2"]=$_REQUEST["prog_hab_W_A2"];
			if(isset($_REQUEST["prog_hab_S_A2"])&&@$_REQUEST["prog_hab_S_A2"]!='')$filtros["prog_hab_S_A2"]=$_REQUEST["prog_hab_S_A2"];
			if(isset($_REQUEST["prog_curso_B1"])&&@$_REQUEST["prog_curso_B1"]!='')$filtros["prog_curso_B1"]=$_REQUEST["prog_curso_B1"];
			if(isset($_REQUEST["prog_hab_L_B1"])&&@$_REQUEST["prog_hab_L_B1"]!='')$filtros["prog_hab_L_B1"]=$_REQUEST["prog_hab_L_B1"];
			if(isset($_REQUEST["prog_hab_R_B1"])&&@$_REQUEST["prog_hab_R_B1"]!='')$filtros["prog_hab_R_B1"]=$_REQUEST["prog_hab_R_B1"];
			if(isset($_REQUEST["prog_hab_W_B1"])&&@$_REQUEST["prog_hab_W_B1"]!='')$filtros["prog_hab_W_B1"]=$_REQUEST["prog_hab_W_B1"];
			if(isset($_REQUEST["prog_hab_S_B1"])&&@$_REQUEST["prog_hab_S_B1"]!='')$filtros["prog_hab_S_B1"]=$_REQUEST["prog_hab_S_B1"];
			if(isset($_REQUEST["prog_curso_B2"])&&@$_REQUEST["prog_curso_B2"]!='')$filtros["prog_curso_B2"]=$_REQUEST["prog_curso_B2"];
			if(isset($_REQUEST["prog_hab_L_B2"])&&@$_REQUEST["prog_hab_L_B2"]!='')$filtros["prog_hab_L_B2"]=$_REQUEST["prog_hab_L_B2"];
			if(isset($_REQUEST["prog_hab_R_B2"])&&@$_REQUEST["prog_hab_R_B2"]!='')$filtros["prog_hab_R_B2"]=$_REQUEST["prog_hab_R_B2"];
			if(isset($_REQUEST["prog_hab_W_B2"])&&@$_REQUEST["prog_hab_W_B2"]!='')$filtros["prog_hab_W_B2"]=$_REQUEST["prog_hab_W_B2"];
			if(isset($_REQUEST["prog_hab_S_B2"])&&@$_REQUEST["prog_hab_S_B2"]!='')$filtros["prog_hab_S_B2"]=$_REQUEST["prog_hab_S_B2"];
			if(isset($_REQUEST["prog_curso_C1"])&&@$_REQUEST["prog_curso_C1"]!='')$filtros["prog_curso_C1"]=$_REQUEST["prog_curso_C1"];
			if(isset($_REQUEST["prog_hab_L_C1"])&&@$_REQUEST["prog_hab_L_C1"]!='')$filtros["prog_hab_L_C1"]=$_REQUEST["prog_hab_L_C1"];
			if(isset($_REQUEST["prog_hab_R_C1"])&&@$_REQUEST["prog_hab_R_C1"]!='')$filtros["prog_hab_R_C1"]=$_REQUEST["prog_hab_R_C1"];
			if(isset($_REQUEST["prog_hab_W_C1"])&&@$_REQUEST["prog_hab_W_C1"]!='')$filtros["prog_hab_W_C1"]=$_REQUEST["prog_hab_W_C1"];
			if(isset($_REQUEST["prog_hab_S_C1"])&&@$_REQUEST["prog_hab_S_C1"]!='')$filtros["prog_hab_S_C1"]=$_REQUEST["prog_hab_S_C1"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegReportes_resumen->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idresumen)) {
				$this->oNegReportes_resumen->idresumen = $idresumen;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegReportes_resumen->idalumno=@$idalumno;
				$this->oNegReportes_resumen->idlocal=@$idlocal;
				$this->oNegReportes_resumen->fecha_modificacion=@$fecha_modificacion;
				$this->oNegReportes_resumen->prog_curso_A1=@$prog_curso_A1;
				$this->oNegReportes_resumen->prog_hab_L_A1=@$prog_hab_L_A1;
				$this->oNegReportes_resumen->prog_hab_R_A1=@$prog_hab_R_A1;
				$this->oNegReportes_resumen->prog_hab_W_A1=@$prog_hab_W_A1;
				$this->oNegReportes_resumen->prog_hab_S_A1=@$prog_hab_S_A1;
				$this->oNegReportes_resumen->prog_curso_A2=@$prog_curso_A2;
				$this->oNegReportes_resumen->prog_hab_L_A2=@$prog_hab_L_A2;
				$this->oNegReportes_resumen->prog_hab_R_A2=@$prog_hab_R_A2;
				$this->oNegReportes_resumen->prog_hab_W_A2=@$prog_hab_W_A2;
				$this->oNegReportes_resumen->prog_hab_S_A2=@$prog_hab_S_A2;
				$this->oNegReportes_resumen->prog_curso_B1=@$prog_curso_B1;
				$this->oNegReportes_resumen->prog_hab_L_B1=@$prog_hab_L_B1;
				$this->oNegReportes_resumen->prog_hab_R_B1=@$prog_hab_R_B1;
				$this->oNegReportes_resumen->prog_hab_W_B1=@$prog_hab_W_B1;
				$this->oNegReportes_resumen->prog_hab_S_B1=@$prog_hab_S_B1;
				$this->oNegReportes_resumen->prog_curso_B2=@$prog_curso_B2;
				$this->oNegReportes_resumen->prog_hab_L_B2=@$prog_hab_L_B2;
				$this->oNegReportes_resumen->prog_hab_R_B2=@$prog_hab_R_B2;
				$this->oNegReportes_resumen->prog_hab_W_B2=@$prog_hab_W_B2;
				$this->oNegReportes_resumen->prog_hab_S_B2=@$prog_hab_S_B2;
				$this->oNegReportes_resumen->prog_curso_C1=@$prog_curso_C1;
				$this->oNegReportes_resumen->prog_hab_L_C1=@$prog_hab_L_C1;
				$this->oNegReportes_resumen->prog_hab_R_C1=@$prog_hab_R_C1;
				$this->oNegReportes_resumen->prog_hab_W_C1=@$prog_hab_W_C1;
				$this->oNegReportes_resumen->prog_hab_S_C1=@$prog_hab_S_C1;
				
            if($accion=='_add') {
            	$res=$this->oNegReportes_resumen->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Reportes_resumen')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegReportes_resumen->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Reportes_resumen')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegReportes_resumen->__set('idresumen', $_REQUEST['idresumen']);
			$res=$this->oNegReportes_resumen->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegReportes_resumen->setCampo($_REQUEST['idresumen'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}