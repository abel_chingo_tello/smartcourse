<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRub_detalle_respuestas', RUTA_BASE);
class WebRub_detalle_respuestas extends JrWeb
{
	private $oNegRub_detalle_respuestas;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRub_detalle_respuestas = new NegRub_detalle_respuestas;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Rub_detalle_respuestas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["id_respuestas"])&&@$_REQUEST["id_respuestas"]!='')$filtros["id_respuestas"]=$_REQUEST["id_respuestas"];
			if(isset($_REQUEST["id_pregunta"])&&@$_REQUEST["id_pregunta"]!='')$filtros["id_pregunta"]=$_REQUEST["id_pregunta"];
			if(isset($_REQUEST["respuesta"])&&@$_REQUEST["respuesta"]!='')$filtros["respuesta"]=$_REQUEST["respuesta"];
			if(isset($_REQUEST["puntaje_obtenido"])&&@$_REQUEST["puntaje_obtenido"]!='')$filtros["puntaje_obtenido"]=$_REQUEST["puntaje_obtenido"];
			if(isset($_REQUEST["id_dimension"])&&@$_REQUEST["id_dimension"]!='')$filtros["id_dimension"]=$_REQUEST["id_dimension"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRub_detalle_respuestas->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id_det_rptas)) {
				$this->oNegRub_detalle_respuestas->id_det_rptas = $id_det_rptas;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRub_detalle_respuestas->id_respuestas=@$id_respuestas;
				$this->oNegRub_detalle_respuestas->id_pregunta=@$id_pregunta;
				$this->oNegRub_detalle_respuestas->respuesta=@$respuesta;
				$this->oNegRub_detalle_respuestas->puntaje_obtenido=@$puntaje_obtenido;
				$this->oNegRub_detalle_respuestas->id_dimension=@$id_dimension;
				
            if($accion=='_add') {
            	$res=$this->oNegRub_detalle_respuestas->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_detalle_respuestas')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRub_detalle_respuestas->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_detalle_respuestas')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRub_detalle_respuestas->__set('id_det_rptas', $_REQUEST['id_det_rptas']);
			$res=$this->oNegRub_detalle_respuestas->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRub_detalle_respuestas->setCampo($_REQUEST['id_det_rptas'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}