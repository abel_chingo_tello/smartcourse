<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-04-2020 
 * @copyright	Copyright (C) 07-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEvento_programacion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegZoomalumnos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegZoom', RUTA_BASE);
class WebParticipantes extends JrWeb
{
	private $oNegEvento_programacion;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegEvento_programacion = new NegEvento_programacion;
		$this->oNegZoomalumnos = new NegZoomalumnos;
		$this->oNegZoom = new NegZoom;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
			$oUsuario = NegSesion::getUsuario();
			$arrCursos = $this->oNegAcad_grupoauladetalle->buscar(array(
				"sql2" => "sql2",
				"iddocente" => "$oUsuario[idpersona]",
				"idproyecto" => "$oUsuario[idproyecto]"
			));

			foreach ($arrCursos as $key => $curso) {
				$arrAlumnos = array();
				$filtros['fechaactiva'] = true; //quitar para mostrar alumnoeiger
				$filtros['idgrupoauladetalle'] = $curso['idgrupoauladetalle'];
				$arrAlumnos = $this->oNegAcad_matricula->buscar($filtros);
				$curso['arrAlumnos'] = $arrAlumnos;
				$arrCursos[$key] = $curso;
			}

			$this->datos = $arrCursos;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function eventosZoom()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			$oUsuario = NegSesion::getUsuario();
			$arrZoom = array();
			$filtros['mysql'] = 2;
			if ($oUsuario["idrol"] == 3) { //alumno
				$filtros["idalumno"] = $oUsuario["idpersona"];
				$arrZoom = $this->oNegZoomalumnos->buscar($filtros);
			} else { //docente o admin
				$filtros["idpersona"] = $oUsuario["idpersona"];
				$arrZoom = $this->oNegZoom->buscar($filtros);
			}
			$this->datos = $arrZoom;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$id_programacion)) {
				$this->oNegEvento_programacion->id_programacion = $id_programacion;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();
			// echo "<hr>".var_dump($_POST)."<hr>";

			$this->oNegEvento_programacion->fecha = @$fecha;
			$this->oNegEvento_programacion->titulo = @$titulo;
			$this->oNegEvento_programacion->detalle = @$detalle;
			$this->oNegEvento_programacion->hora_comienzo = @$hora_comienzo;
			$this->oNegEvento_programacion->hora_fin = @$hora_fin;
			$this->oNegEvento_programacion->idpersona = $usuarioAct['idpersona'];
			$this->oNegEvento_programacion->idgrupoaula = @$idgrupoaula;
			$this->oNegEvento_programacion->idgrupoauladetalle = @$idgrupoauladetalle;

			if ($accion == '_add') {
				$res = $this->oNegEvento_programacion->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Evento_programacion')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegEvento_programacion->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Evento_programacion')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegEvento_programacion->fecha = @$fecha;
					$this->oNegEvento_programacion->titulo = @$titulo;
					$this->oNegEvento_programacion->detalle = @$detalle;
					$this->oNegEvento_programacion->hora_comienzo = @$hora_comienzo;
					$this->oNegEvento_programacion->hora_fin = @$hora_fin;
					$this->oNegEvento_programacion->idpersona = @$idpersona;
					$this->oNegEvento_programacion->idgrupoaula = @$idgrupoaula;
					$this->oNegEvento_programacion->idgrupoauladetalle = @$idgrupoauladetalle;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegEvento_programacion->__set('id_programacion', $_REQUEST['id_programacion']);
			$res = $this->oNegEvento_programacion->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEvento_programacion->setCampo($_REQUEST['id_programacion'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
