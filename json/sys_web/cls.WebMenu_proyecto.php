<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-11-2020 
 * @copyright	Copyright (C) 12-11-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegMenu_proyecto', RUTA_BASE);
class WebMenu_proyecto extends JrWeb
{
	private $oNegMenu_proyecto;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegMenu_proyecto = new NegMenu_proyecto;
		
	}

	public function defecto(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Menu_proyecto', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idmenuproyecto"])) $filtros["idmenuproyecto"]=$_REQUEST["idmenuproyecto"];

			if(isset($_REQUEST["idmenu"])&&@$_REQUEST["idmenu"]!='')$filtros["idmenu"]=$_REQUEST["idmenu"]; 
  			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"]; 
  			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"]; 
  			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"]; 
  			if(isset($_REQUEST["insertar"])&&@$_REQUEST["insertar"]!='')$filtros["insertar"]=$_REQUEST["insertar"]; 
  			if(isset($_REQUEST["modificar"])&&@$_REQUEST["modificar"]!='')$filtros["modificar"]=$_REQUEST["modificar"]; 
  			if(isset($_REQUEST["eliminar"])&&@$_REQUEST["eliminar"]!='')$filtros["eliminar"]=$_REQUEST["eliminar"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"]; 
  			$filtros["enorden"]=!empty($_REQUEST["enorden"])?true:false;
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
			$usuarioAct = NegSesion::getUsuario(); 

  			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"]; 
  			if(empty($filtros["idproyecto"])){
  				$filtros["idproyecto"]=@$usuarioAct["idproyecto"];
  			}

  			//var_dump(expression)
					
			$this->datos=$this->oNegMenu_proyecto->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            
  			
            if(!empty($idmenuproyecto)) {
				$this->oNegMenu_proyecto->idmenuproyecto = $idmenuproyecto;
				$accion='_edit';
			}

           	$this->oNegMenu_proyecto->idmenu=!empty($idmenu)?$idmenu:'null';
  			$this->oNegMenu_proyecto->idproyecto=@$usuarioAct["idproyecto"];
  			$this->oNegMenu_proyecto->idrol=@$idrol;
  			$this->oNegMenu_proyecto->orden=!empty($orden)?$orden:1;
  			$this->oNegMenu_proyecto->idpadre=!empty($idpadre)?$idpadre:'null';
  			$this->oNegMenu_proyecto->insertar=isset($insertar)?$insertar:1;
  			$this->oNegMenu_proyecto->modificar=isset($modificar)?$modificar:1;
  			$this->oNegMenu_proyecto->eliminar=isset($eliminar)?$eliminar:1;
  			$this->oNegMenu_proyecto->usuario_registro=$usuarioAct["idpersona"];
  			//$this->oNegMenu_proyecto->fecha_registro=@$fecha_registro;
  			  				        
            if($accion=='_add') {
            	$res=$this->oNegMenu_proyecto->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Menu_proyecto')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegMenu_proyecto->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Menu_proyecto')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegMenu_proyecto->__set('idmenuproyecto', $_REQUEST['idmenuproyecto']);
			$res=$this->oNegMenu_proyecto->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegMenu_proyecto->setCampo($_REQUEST['idmenuproyecto'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
	
	public function guardarorden(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegMenu_proyecto->guardarorden($_REQUEST['datos']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('El orden se ha actualizado')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}	 
}