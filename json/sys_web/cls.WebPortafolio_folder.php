<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-05-2020 
 * @copyright	Copyright (C) 04-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPortafolio_folder', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPortafolio_archivo', RUTA_BASE);
class WebPortafolio_folder extends JrWeb
{
	private $oNegPortafolio_folder;

	public function __construct()
	{
		parent::__construct();
		$this->oNegPortafolio_folder = new NegPortafolio_folder;
		$this->oNegPortafolio_archivo = new NegPortafolio_archivo;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Portafolio_folder', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idfolder"]) && @$_REQUEST["idfolder"] != '') $filtros["idfolder"] = $_REQUEST["idfolder"];
			if (isset($_REQUEST["idfolder_padre"]) && @$_REQUEST["idfolder_padre"] != '') $filtros["idfolder_padre"] = $_REQUEST["idfolder_padre"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["idportafolio"]) && @$_REQUEST["idportafolio"] != '') $filtros["idportafolio"] = $_REQUEST["idportafolio"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->datos = $this->oNegPortafolio_folder->buscar($filtros);
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getFolder()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Portafolio_folder', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idfolder"]) && @$_REQUEST["idfolder"] != '') $filtros["idfolder"] = $_REQUEST["idfolder"];
			if (isset($_REQUEST["idfolder_padre"]) && @$_REQUEST["idfolder_padre"] != '') $filtros["idfolder_padre"] = $_REQUEST["idfolder_padre"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["idportafolio"]) && @$_REQUEST["idportafolio"] != '') $filtros["idportafolio"] = $_REQUEST["idportafolio"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$arrFolder = $this->oNegPortafolio_folder->buscar($filtros);
			$folder = $this->oNegPortafolio_folder->buscar(array(
				"idportafolio" => $filtros["idportafolio"],
				"idfolder" => $filtros["idfolder"] 
			))[0];
			foreach ($arrFolder as $key => $folder) {
				$arrFolder = $this->oNegPortafolio_folder->buscar(array(
					"idportafolio" => $folder['idportafolio'],
					"idfolder_padre" => $folder['idfolder']
				));
				$arrArchivo = $this->oNegPortafolio_archivo->buscar(array(
					"idportafolio" => $folder['idportafolio'],
					"idfolder" => $folder['idfolder']
				));
				$folder["arrContenido"] = array_merge($arrFolder, $arrArchivo);
			}
			$this->datos = $folder;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idfolder)) {
				$this->oNegPortafolio_folder->idfolder = $idfolder;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();


			$this->oNegPortafolio_folder->idfolder_padre = @$idfolder_padre;
			$this->oNegPortafolio_folder->nombre = @$nombre;
			$this->oNegPortafolio_folder->idportafolio = @$idportafolio;

			if ($accion == '_add') {
				$res = $this->oNegPortafolio_folder->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Portafolio_folder')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegPortafolio_folder->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Portafolio_folder')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegPortafolio_folder->idfolder_padre = @$idfolder_padre;
					$this->oNegPortafolio_folder->nombre = @$nombre;
					$this->oNegPortafolio_folder->idportafolio = @$idportafolio;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPortafolio_folder->__set('idfolder', $_REQUEST['idfolder']);
			$res = $this->oNegPortafolio_folder->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPortafolio_folder->setCampo($_REQUEST['idfolder'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
