<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-03-2020 
 * @copyright	Copyright (C) 11-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso_consulta', RUTA_BASE);
class WebAcad_curso_consulta extends JrWeb
{
	private $oNegAcad_curso_consulta;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_curso_consulta = new NegAcad_curso_consulta;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_curso_consulta', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["codigo"])&&@$_REQUEST["codigo"]!='')$filtros["codigo"]=$_REQUEST["codigo"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["iddetalle"])&&@$_REQUEST["iddetalle"]!='')$filtros["iddetalle"]=$_REQUEST["iddetalle"];
			if(isset($_REQUEST["tipocurso"])&&@$_REQUEST["tipocurso"]!='')$filtros["tipocurso"]=$_REQUEST["tipocurso"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["contenido"])&&@$_REQUEST["contenido"]!='')$filtros["contenido"]=$_REQUEST["contenido"];
			if(isset($_REQUEST["idcomplementario"])&&@$_REQUEST["idcomplementario"]!='')$filtros["idcomplementario"]=$_REQUEST["idcomplementario"];
			if(isset($_REQUEST["idpestania"])&&@$_REQUEST["idpestania"]!='')$filtros["idpestania"]=$_REQUEST["idpestania"];
			if(isset($_REQUEST["fecha_hora"])&&@$_REQUEST["fecha_hora"]!='')$filtros["fecha_hora"]=$_REQUEST["fecha_hora"];
			if(isset($_REQUEST["respuesta"])&&@$_REQUEST["respuesta"]!='')$filtros["respuesta"]=$_REQUEST["respuesta"];
			if(isset($_REQUEST["sql"])&&@$_REQUEST["sql"]!='')$filtros["sql"]=$_REQUEST["sql"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAcad_curso_consulta->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
			$usuarioAct = NegSesion::getUsuario();

			$estados = array(
				'codigo' => @$codigo
				,'idcurso'=>$idcurso
				,'idcategoria'=>(empty(@$idcategoria) ? 0 : @$idcategoria)
				,'idgrupoaula'=>(empty(@$idgrupoaula) ? 0 : @$idgrupoaula)
				,'iddetalle'=>$iddetalle
				,'tipocurso'=>(empty(@$tipocurso) ? 0 : @$tipocurso)
				,'idpersona'=>$usuarioAct["idpersona"]
				,'contenido'=>$contenido
				,'idpestania'=>$idpestania
				,'idcomplementario'=>$idcomplementario
				,'fecha_hora'=>date("Y-m-d H:i:s")
			);

			if(!empty(@$respuesta)) {
				$estados['respuesta'] = $respuesta;
			}
				
            if(empty(@$codigo)) {
            	$res=$this->oNegAcad_curso_consulta->agregar($estados);
            	 echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Pregunta Registrada Correctamente'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_curso_consulta->editar($estados);
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Acad_curso_consulta')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$this->oNegAcad_curso_consulta->idcurso=@$idcurso;
				$this->oNegAcad_curso_consulta->idcategoria=@$idcategoria;
				$this->oNegAcad_curso_consulta->idgrupoaula=@$idgrupoaula;
				$this->oNegAcad_curso_consulta->iddetalle=@$iddetalle;
				$this->oNegAcad_curso_consulta->tipocurso=@$tipocurso;
				$this->oNegAcad_curso_consulta->idpersona=@$idpersona;
				$this->oNegAcad_curso_consulta->contenido=@$contenido;
				$this->oNegAcad_curso_consulta->fecha_hora=@$fecha_hora;
				$this->oNegAcad_curso_consulta->respuesta=@$respuesta;
									$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAcad_curso_consulta->__set('codigo', $_REQUEST['codigo']);
			$res=$this->oNegAcad_curso_consulta->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}
	public function agregarlike(){
		try{
			if(empty($_REQUEST) || empty($_REQUEST['codigo']) || empty($_REQUEST['idpersona'])){
				throw new Exeption("Falta campos a definir");
			}

			$rs = $this->oNegAcad_curso_consulta->buscar(array('codigo'=>$_REQUEST['codigo']));
			//'idpersona'=>$_REQUEST['idpersona']
			if(empty($rs)){
				throw new Exception("No se encontro el registro");
			}
			$likedeArr = explode(',',$rs[0]['like_de']);
			$likes = $rs[0]['likes'];
			$findarray = array_search($_REQUEST['idpersona'],$likedeArr);
			if($findarray === false){
				$likes = empty($likes) ? 1 : (intval($likes) + 1) ;
				$likedeArr[] = $_REQUEST['idpersona'];
			}else{
				$likes = empty($likes) ? 0 : (intval($likes) - 1) ;
				unset($likedeArr[$findarray]);
			}
			$likedeVal = implode(',',$likedeArr);
			$likedeVal = ltrim($likedeVal,',');
			$estados = array(
				'codigo' => $_REQUEST['codigo']
				,'like_de' => @$likedeVal
				,'likes'=>$likes
				,'fecha_hora' => $rs[0]['fecha_hora']
			);
			$res=$this->oNegAcad_curso_consulta->editar($estados);

			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Likes guardado'),'likes'=>$likes));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit(0);
		}
	}
	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAcad_curso_consulta->setCampo($_REQUEST['codigo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}