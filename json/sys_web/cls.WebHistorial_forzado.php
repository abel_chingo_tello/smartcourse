<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegHistorial_forzado', RUTA_BASE);
class WebHistorial_forzado extends JrWeb
{
	private $oNegHistorial_forzado;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegHistorial_forzado = new NegHistorial_forzado;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Historial_forzado', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idhistorialsin"])&&@$_REQUEST["idhistorialsin"]!='')$filtros["idhistorialsin"]=$_REQUEST["idhistorialsin"];
			if(isset($_REQUEST["nombre_archivo"])&&@$_REQUEST["nombre_archivo"]!='')$filtros["nombre_archivo"]=$_REQUEST["nombre_archivo"];
			if(isset($_REQUEST["fechaentrada"])&&@$_REQUEST["fechaentrada"]!='')$filtros["fechaentrada"]=$_REQUEST["fechaentrada"];
			if(isset($_REQUEST["estadoexp"])&&@$_REQUEST["estadoexp"]!='')$filtros["estadoexp"]=$_REQUEST["estadoexp"];
			if(isset($_REQUEST["estadoimp"])&&@$_REQUEST["estadoimp"]!='')$filtros["estadoimp"]=$_REQUEST["estadoimp"];
			if(isset($_REQUEST["tabla"])&&@$_REQUEST["tabla"]!='')$filtros["tabla"]=$_REQUEST["tabla"];
			if(isset($_REQUEST["fechamodificacion"])&&@$_REQUEST["fechamodificacion"]!='')$filtros["fechamodificacion"]=$_REQUEST["fechamodificacion"];
			if(isset($_REQUEST["completo"])&&@$_REQUEST["completo"]!='')$filtros["completo"]=$_REQUEST["completo"];
			if(isset($_REQUEST["contactualizador"])&&@$_REQUEST["contactualizador"]!='')$filtros["contactualizador"]=$_REQUEST["contactualizador"];
			if(isset($_REQUEST["estadoimpac"])&&@$_REQUEST["estadoimpac"]!='')$filtros["estadoimpac"]=$_REQUEST["estadoimpac"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegHistorial_forzado->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idhisesion)) {
				$this->oNegHistorial_forzado->idhisesion = $idhisesion;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegHistorial_forzado->idhistorialsin=@$idhistorialsin;
				$this->oNegHistorial_forzado->nombre_archivo=@$nombre_archivo;
				$this->oNegHistorial_forzado->fechaentrada=@$fechaentrada;
				$this->oNegHistorial_forzado->estadoexp=@$estadoexp;
				$this->oNegHistorial_forzado->estadoimp=@$estadoimp;
				$this->oNegHistorial_forzado->tabla=@$tabla;
				$this->oNegHistorial_forzado->fechamodificacion=@$fechamodificacion;
				$this->oNegHistorial_forzado->completo=@$completo;
				$this->oNegHistorial_forzado->contactualizador=@$contactualizador;
				$this->oNegHistorial_forzado->estadoimpac=@$estadoimpac;
				
            if($accion=='_add') {
            	$res=$this->oNegHistorial_forzado->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Historial_forzado')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegHistorial_forzado->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Historial_forzado')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegHistorial_forzado->__set('idhisesion', $_REQUEST['idhisesion']);
			$res=$this->oNegHistorial_forzado->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegHistorial_forzado->setCampo($_REQUEST['idhisesion'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}