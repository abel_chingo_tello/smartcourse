<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE);
class WebLocal extends JrWeb
{
	private $oNegLocal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegLocal = new NegLocal;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Local', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["vacantes"])&&@$_REQUEST["vacantes"]!='')$filtros["vacantes"]=$_REQUEST["vacantes"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["oldid"])&&@$_REQUEST["oldid"]!='')$filtros["oldid"]=$_REQUEST["oldid"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			if(empty($_REQUEST["idproyecto"])){
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idproyecto"] = $usuarioAct["idproyecto"];
			}
			$this->datos=$this->oNegLocal->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idlocal)) {
				$this->oNegLocal->idlocal = $idlocal;
				$accion='_edit';
			}
			if(empty($idproyecto)){
           		$usuarioAct = NegSesion::getUsuario();
			}
	        
			$this->oNegLocal->nombre=@$nombre;
			$this->oNegLocal->direccion=@$direccion;
			$this->oNegLocal->id_ubigeo=@$id_ubigeo;
			$this->oNegLocal->tipo=@$tipo;
			$this->oNegLocal->vacantes=@$vacantes;
			$this->oNegLocal->idugel=@$idugel;
			$this->oNegLocal->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
			$this->oNegLocal->oldid=@$oldid;
            if($accion=='_add') {
            	$res=$this->oNegLocal->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Local')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegLocal->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Local')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegLocal->__set('idlocal', $_REQUEST['idlocal']);
			$res=$this->oNegLocal->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegLocal->setCampo($_REQUEST['idlocal'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $jsondata=json_decode($datajson,true);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$dres=$ugel=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					$j++;
					$this->oNegLocal->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
					$iddre=!empty($dres[!empty($v["iddre"])?$v["iddre"]:'_'])?$dres[!empty($v["iddre"])?$v["iddre"]:'_']:$v["iddre"];
					$idugel=!empty($ugel[!empty($v["idugel"])?$v["idugel"]:'_'])?$ugel[!empty($v["idugel"])?$v["idugel"]:'_']:$v["idugel"];
					//$this->oNegLocal->importar(@$v->iddre,@$v->dre,@$v->idugel,@$v->ugel,@$v->idiiee,@$v->iiee,@$v->direccion,@$v->ubigeo,@$v->tipo,@$v->nvacantes,$this->usuarioAct["idproyecto"]);
					$res=$this->oNegLocal->importar($iddre,@$v["dre"],@$idugel,@$v["ugel"],@$v["idiiee"],@$v["iiee"],@$v["direccion"],@$v["ubigeo"],@$v["tipo"],@$v["nvacantes"],$usuarioAct["idproyecto"]);

					if(empty($dres[!empty($v["iddre"])?$v["iddre"]:'_']))$dres[!empty($v["iddre"])?$v["iddre"]:'_']=$res['_estado']=='N'?$res['_iddre']:$iddre;
					if(empty($ugel[!empty($v["idugel"])?$v["idugel"]:'_']))$ugel[!empty($v["idugel"])?$v["idugel"]:'_']=$res['_estado']=='N'?$res['_idugel']:$idugel;

					$v['_infoimport']=$res;
					$this->datos[$j]=$v;
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Dre')).' '.JrTexto::_('data Import successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}   
}