<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-05-2020 
 * @copyright	Copyright (C) 04-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPortafolio', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPortafolio_folder', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPortafolio_archivo', RUTA_BASE);
class WebPortafolio extends JrWeb
{
	private $oNegPortafolio;

	public function __construct()
	{
		parent::__construct();
		$this->oNegPortafolio = new NegPortafolio;
		$this->oNegPortafolio_folder = new NegPortafolio_folder;
		$this->oNegPortafolio_archivo = new NegPortafolio_archivo;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			$filtros = array();
			$filtros = array(
				"idalumno" => $usuarioAct['idpersona'],
				"idproyecto" => $usuarioAct['idproyecto']
			);
			if (isset($_REQUEST["idportafolio"]) && @$_REQUEST["idportafolio"] != '') $filtros["idportafolio"] = $_REQUEST["idportafolio"];
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["idfolder_raiz"]) && @$_REQUEST["idfolder_raiz"] != '') $filtros["idfolder_raiz"] = $_REQUEST["idfolder_raiz"];
			
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$arrPortafolio = $this->oNegPortafolio->buscar($filtros);
			if (count($arrPortafolio) == 0) {
				$this->oNegPortafolio->idalumno = $filtros['idalumno'];
				$this->oNegPortafolio->idproyecto = $filtros['idproyecto'];
				$idPortafolio = $this->oNegPortafolio->agregar();
				$this->oNegPortafolio_folder->nombre = 'root';
				$this->oNegPortafolio_folder->idportafolio = $idPortafolio;
				$this->oNegPortafolio_folder->idfolder_padre = 0;
				$idFolder=$this->oNegPortafolio_folder->agregar();
				$arrPortafolio = $this->oNegPortafolio->buscar($filtros);
			}
			$portafolio = $arrPortafolio[0];

			$folderRaiz = $this->oNegPortafolio_folder->buscar(array(
				"idportafolio" => $portafolio['idportafolio'],
				"idfolder_padre" =>'0'
			))[0];
			$arrFolder = $this->oNegPortafolio_folder->buscar(array(
				"idportafolio" => $portafolio['idportafolio'],
				"idfolder_padre" => $folderRaiz['idfolder']
			));
			$arrArchivo = $this->oNegPortafolio_archivo->buscar(array(
				"idportafolio" => $folderRaiz['idportafolio'],
				"idfolder" => $folderRaiz['idfolder']
			));
			$folderRaiz["arrContenido"] = array_merge($arrFolder, $arrArchivo);
			$portafolio["folder"] = $folderRaiz;
			$this->datos = $portafolio;

			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$accion = '_add';
			if (!empty(@$idportafolio)) {
				$this->oNegPortafolio->idportafolio = $idportafolio;
				$accion = '_edit';
			}
			$usuarioAct = NegSesion::getUsuario();


			$this->oNegPortafolio->idalumno = @$idalumno;
			$this->oNegPortafolio->idproyecto = @$idproyecto;

			if ($accion == '_add') {
				$res = $this->oNegPortafolio->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Portafolio')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegPortafolio->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Portafolio')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegPortafolio->idalumno = @$idalumno;
					$this->oNegPortafolio->idproyecto = @$idproyecto;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPortafolio->__set('idportafolio', $_REQUEST['idportafolio']);
			$res = $this->oNegPortafolio->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPortafolio->setCampo($_REQUEST['idportafolio'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function guardarImagen()
	{
		$serverPath = 'static/portafolio-img/';
		$imageFolder = RUTA_BASE . $serverPath;

		reset($_FILES);
		$temp = current($_FILES);
		if (is_uploaded_file($temp['tmp_name'])) {
			if (isset($_SERVER['HTTP_ORIGIN'])) {
				header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
			}
			if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
				//header("HTTP/1.1 400 Invalid file name.");
				echo json_encode(array('code' => 200, 'msj' => 'nombre invalido', 'estado' => 0));
				exit(0);
			}
			if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png", "jpeg"))) {
				//header("HTTP/1.1 400 Invalid extension.");
				echo json_encode(array('code' => 200, 'msj' => 'Extension invalida', 'estado' => 0));
				exit(0);
			}
			$filetowrite = $imageFolder . $temp['name'];
			move_uploaded_file($temp['tmp_name'], $filetowrite);
			$responsePath = $serverPath . $temp['name'];
			$nombre = $temp['name'];
			echo json_encode(array('code' => 200, 'msj' => 'imagen guardada', 'location' => $responsePath, 'nombre' => $nombre, 'estado' => 1));
			exit(0);
		} else {
			header("HTTP/1.1 500 Server Error");
		}
	}

	public function guardarEstilo()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$filtros = array();
			
			if (isset($_REQUEST["idPorta"]) && @$_REQUEST["idPorta"] != '') $filtros["idportafolio"] = $_REQUEST["idPorta"];
			if (isset($_REQUEST["idAlum"]) && @$_REQUEST["idAlum"] != '') $filtros["idalumno"] = $_REQUEST["idAlum"];
			if (isset($_REQUEST["idProy"]) && @$_REQUEST["idProy"] != '') $filtros["idproyecto"] = $_REQUEST["idProy"];
			if (isset($_REQUEST["fondo"]) && @$_REQUEST["fondo"] != '') $filtros["fondo"] = $_REQUEST["fondo"];

			$res = $this->oNegPortafolio->guardarEstilo($filtros);
			echo json_encode(array('code' => 200, 'msj' => 'ok', 'data' => $res));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

}
