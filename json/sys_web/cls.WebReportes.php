<?php
set_time_limit(0);
/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
include_once('cls.WebMine.php');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMinedu', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_grado', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE);
JrCargador::clase('sys_negocio::NegResumen', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_competencias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_unidad_capacidad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_archivosalumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_mensajes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_mensajes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegReportes_mod', RUTA_BASE);
JrCargador::clase('sys_datos::DatRecursos_colaborativosasignacion', RUTA_BASE);

//JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE);
class WebReportes extends JrWeb
{
	private $oNegAcad_grupoauladetalle;
	private $oNegCursoDetalle;
	private $oNegAcad_grupoaula;
	private $oNegAcad_matricula;
	private $oNegAcad_cursodetalle;
	private $oNegNotas_quiz;
	private $oNegAcad_curso;
	private $oNegQuizexamenes;
	private $oNegPersona_setting;
	private $oNegPersonal;
	private $oNegHistorial_sesion;
	private $oNegActividad_alumno;
	private $oNegActividad;
	private $oNegMinedu;
	private $oNegGrado;
	private $oNegSeccion;
	private $oNegUgel;
	private $oNegDre;
	private $oNegResumen;
	private $oNegCompetencias;
	private $oNegTareas;
	private $oNegMin_unidad_capacidad;
	private $oNegReportes_mod;
	private $oDatWikiforo;

	private $Dpuntajemax = 100;
	private $Dpuntajemin = 51;
	private $oWebMine;
	//private $oNegGeneral;

	public function __construct()
	{
		parent::__construct();
		$this->oWebMine = new WebMine;
		$this->oNegQuizexamenes = new NegQuizexamenes;
		$this->oNegCursoDetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegReportealumno = new NegReportealumno;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegPersona_setting = new NegPersona_setting;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegActividad = new NegActividad;
		$this->oNegMinedu = new NegMinedu;
		$this->oNegGrado = new NegMin_grado;
		$this->oNegSeccion = new NegMin_sesion;
		$this->oNegUgel = new NegUgel;
		$this->oNegResumen = new NegResumen;
		$this->oNegDre = new NegMin_dre;
		$this->oNegCompetencias = new NegMin_competencias;
		$this->oNegMin_unidad_capacidad = new NegMin_unidad_capacidad;
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegTareas = new NegTareas;
		$this->oNegTareas_archivosalumno = new NegTareas_archivosalumno;
		$this->oNegTareas_mensajes = new NegTareas_mensajes;
		$this->oNegReportes_mod = new NegReportes_mod;
		$this->oDatWikiforo = new DatRecursos_colaborativosasignacion;
		//$this->oNegGeneral = new NegGeneral;

	}
	public function notasFormula() //mine Reporte Mis Notas
	{
		$this->documento->plantilla = 'blanco';
		try {

			$filtros = array();
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $filtros["idcomplementario"] = $_REQUEST["idcomplementario"];
			// $arrCursos = $this->getCursosUsuario(true);
			$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : 0;
			$icc = !empty($_REQUEST["icc"]) ? $_REQUEST["icc"] : 0;
			$tipo = !empty($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : 1;
			$oUsuario = NegSesion::getUsuario();
			$idcategoria = !empty($_REQUEST["idcategoria"]) ? $_REQUEST["idcategoria"] : '';
			$arrCursos_formula = array();
			$arrAlumnos = array();
			$arrCursos = $this->getInstanciaCurso($filtros, true);

			foreach ($arrCursos as $key => $curso) { //obtengo las formulas

				#	$filtrosAlmunos['fechaactiva'] = true; //quitar para mostrar alumnoeiger
				$filtrosAlmunos['estado'] = true;
				$filtrosAlmunos['idgrupoauladetalle'] = $curso['idgrupoauladetalle'];
				if (isset($filtros["idalumno"]) && !empty($filtros["idalumno"])) { //si no hay idalumno en específico, me traigo todos 
					$filtrosAlmunos['idalumno'] = $filtros["idalumno"];
				}
				$arrAlumnos = $this->oNegAcad_matricula->buscar($filtrosAlmunos);
				$arrCursos_formula[] = $this->oNegCursoDetalle->temasacalificar($curso["idcurso"], $curso["idcomplementario"], $curso["tipocurso"], $idcategoria);
			}

			foreach ($arrAlumnos as $i => $alumno) {
				foreach ($arrCursos_formula as $j => $curso_formula) {
					//cada formula del curso
					$strSesionesBorradas = "";
					$arrNewGrupos = array();
					if (count($curso_formula['formulaActual']) != 0) {
						//start formula
						$arrGrupos = $curso_formula['formulaActual']['notas'];
						// echo json_encode($curso_formula);exit();
						if (!is_array($arrGrupos)) {
							$arrGrupos = array();
						}
						foreach ($arrGrupos as $k => $grupo) {
							$arrSesionFormulaSinBorrar = array();
							foreach ($grupo['formula'] as $l => $sesionFormula) {
								if ($sesionFormula['borrado']) {
									$strSesionesBorradas = $strSesionesBorradas . $sesionFormula['id'] . $sesionFormula['typerecurso'] . '-';
								} else {
									$arrSesionFormulaSinBorrar[] = $sesionFormula;
								}
							}
							$grupo['formula'] = $arrSesionFormulaSinBorrar;
							// $arrGrupos[$k]=$grupo;
							if (count($grupo['formula']) > 0) {
								$arrNewGrupos[] = $grupo;
							}
						}
						$curso_formula['formulaActual']['notas'] = $arrNewGrupos;
						//end formula
					}
					$arrTemasAll = $curso_formula['temasaevaluar'];
					$arrTemasSinBorrar = array();
					foreach ($arrTemasAll as $k => $tema) {
						if (strpos($strSesionesBorradas, "$tema[id]$tema[tipo]") === false) {
							$arrTemasSinBorrar[] = $tema;
						}
					}
					$curso_formula['temasaevaluar'] = $arrTemasSinBorrar;
					//obtiene notas de todos los temas, como debe de ser
					$curso_nota = $this->getNotasCurso($curso_formula, $alumno["idalumno"], $alumno["idgrupoauladetalle"], true);
					if (count($curso_nota["formulaActual"]) == 0) {
						$curso_nota["formulaActual"]['formulatxt'] = "Fórmula no asignada";
						$curso_nota["formulaActual"]['notas'] = array();
					} else {
					}
					$curso_formula = $curso_nota;
					$arrCursos_formula[$j] = $curso_formula;
				}
				$alumno["arrCursos_nota"] = $arrCursos_formula;
				$arrAlumnos[$i] = $alumno;
			}
			$this->datos = $arrAlumnos;
			// echo htmlspecialchars(json_encode($this->datos));
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {

			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	private function convertirNota($nota, $notaMax, $conf_notaMax)
	{
		if (strpos($notaMax, '{') !== false) {
			$jsonConfNota = json_decode($notaMax, true);
			$notaMax = $jsonConfNota[0]["max"];
		}
		$nota_convertida = null;
		$nota_convertida = ((float) $nota * (float) $conf_notaMax) / (float) $notaMax;
		return round($nota_convertida, 2);
	}
	public function getNotasCurso($curso = null, $idalumno = null, $idgrupoauladetalle,  $return = false)
	{
		try { //tenga formula o no
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();

			$config_maxCal = 20;
			if (isset($curso['formulaActual']['maxcal'])) {
				$config_maxCal = $curso['formulaActual']['maxcal'];
			}

			$arrTemasAevaluar = $curso["temasaevaluar"];
			foreach ($arrTemasAevaluar as $key => $tema){
					$tipotema=$tema["tipo"];
					if($tipotema=='smartquiz'){
						$arrExamen = $this->datos = $this->oNegQuizexamenes->buscar(array("idexamen" => $tema["idexamen"]));
						$order = null;
						$elegirNota = null;
						if (count($arrExamen) > 0) {
							$elegirNota = $arrExamen[0]['calificacion'];
						} else {
							$elegirNota = "U";
						}
						if ($elegirNota == "U") {
							$order = "idnota";
						} elseif ($elegirNota == "M") {
							$order = "nota";
						}
						$arrNotaExamen = $this->datos = $this->oNegNotas_quiz->buscar(array(
							"idrecurso" => $tema["idexamen"],
							"idcurso" => $curso["idcursoprincipal"],
							"idalumno" => $idalumno,
							"idcomplementario" => $curso["idcomplementario"],
							"idgrupoauladetalle" => $idgrupoauladetalle,
							"order" => $order
						));
						if (count($arrNotaExamen) > 0) {
							// print_r($arrNotaExamen);exit();
							$examen = $arrNotaExamen[0];
							// echo json_encode($examen);
							// exit();
							// if (false) 
							if ($examen["nota"] != null && $config_maxCal != $examen["calificacion_total"]) //realiza la conversión si se encuentran en distintas bases
							{ //no entra si el curso es 20//se supone que es un numero
								$tema["nota"] = (float) $this->convertirNota($examen["nota"], $examen["calificacion_total"], $config_maxCal);
							} else { //puede ser null o otra cosa?
								$tema["nota"] = $examen["nota"];
							}
						}else{
							$tema["nota"] = null; //no lo ha rendido
						}
					}elseif($tipotema=='wiki' || $tipotema=='foro'){
						$tema["nota"]= $this->getNotaWikiforo($tema, $idalumno, $idgrupoauladetalle,$config_maxCal, true);


					}elseif($tipotema=='estarea' || $tipotema=='esproyecto'){
						$tema["nota"] = $this->getNotaTareaProyecto($tema, $idalumno, $idgrupoauladetalle, true);
					}				
				$arrTemasAevaluar[$key] = $tema;
			}
			$curso["temasaevaluar"] = $arrTemasAevaluar;
			$this->datos = $curso;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getNotaTareaProyecto($tareaproyecto, $idalumno, $idgrupoauladetalle, $return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array(
				"sql" => "1",
				"idproyecto" => $oUsuario["idproyecto"],
				"idalumno" => $idalumno,
				"idsesion" => $tareaproyecto["idsesion"],
				"idpestania" => $tareaproyecto["idpestania"],
				"idgrupoauladetalle" => $idgrupoauladetalle
			);
			$arrTarea_nota = $this->oNegTareas->buscar($filtros);
			$nota = null;
			if (count($arrTarea_nota) > 0) {
				$nota =  (empty($arrTarea_nota[0]["fecha_calificacion"])) ? "Sin Calificar" : $arrTarea_nota[0]["nota"]; //$arrTarea_nota[0]["nota"];
			}
			// echo "<hr>";
			// print_r($arrTarea_nota);
			// echo "<hr>";
			$this->datos = $nota;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function getNotaWikiforo($_forowiki, $idalumno, $idgrupoauladetalle,$config_maxCal,$return = true){
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros=array(
				'idalumno'=>$idalumno,
				'idgrupoauladetalle'=>$idgrupoauladetalle,
				'idrecurso'=>$_forowiki["idrecurso"],
				'idpestania'=>$_forowiki["idpestania"],
				'idcursodetalle'=>$_forowiki["idsesion"],
				'solonota'=>true,
				'sqlget'=>true,
			);
			if(empty($_forowiki["idpestania"])) $filtros["idpestania"]='null';
			$forowiki=$this->oDatWikiforo->buscar($filtros);
			$nota=null;
			if(!empty($forowiki["nota"]) || !empty($forowiki["nota_base"])){
				$nota=(float)$forowiki["nota"];
				if($nota==0 || $forowiki["nota_base"]==$config_maxCal){
					$this->datos=$nota;
				}else{
					$this->datos=(float)$this->convertirNota($forowiki["nota"], $forowiki["nota_base"], $config_maxCal);
				}
			}else{
				$this->datos="Sin calificar";
			}

			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function getInstanciaCurso($params = array(), $return = false)
	{ //["idgrupoauladetalle","iddocente", "idcurso", "idcomplementario", "idproyecto"]
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();
			$arrCursos = array();
			if (isset($params["idgrupoauladetalle"]) && !empty($params["idgrupoauladetalle"])) {
				$filtros["idgrupoauladetalle"] = $params["idgrupoauladetalle"];
				$arrInstancias = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			} else {
				$filtros["iddocente"] = (isset($params["iddocente"]) && !empty($params["iddocente"])) ? $params["iddocente"] : $oUsuario["idpersona"];
				if (isset($params["idcurso"]) && !empty($params["idcurso"])) {
					$filtros["idcurso"] = $params["idcurso"];
					$filtros["idcomplementario"] = isset($params["idcomplementario"]) ? $params["idcomplementario"] : 0;
				}
				$filtros["idproyecto"] = isset($params["idproyecto"]) ? $params["idproyecto"] : $oUsuario["idproyecto"];
				$arrInstancias = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			}
			$arrCursos = $arrInstancias;
			$this->datos = $arrCursos;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	/**
	 * La funcion retorna toda la informacion de las habilidades por sesiones.
	 * Funcion creado para el reporte de habilidad por sesiones o para cualquiera que necesite dicha info
	 * @param Array $_REQUEST por GET o POST
	 * @return Object JSON o Array segun el parametro de retorno
	 */

	public function habilidadxsesiones()
	{
		try {
			$params = [];
			$filtros = [];
			$this->datos = [];
			$_idunidad = 0;
			$_idsesion = 0;

			$oUsuario = NegSesion::getUsuario();
			//obtener los parametros
			$params = !empty($_POST['params']) && is_array($_POST['params']) ? $_POST['params'] : WebMine::getParams($params);

			if (empty($params)) {
				throw new Exception("params is empty");
			}

			#Requerir parametros
			if (empty($params['grupo'])) {
				throw new Exception("grupo is empty");
			}
			if (empty($params['curso'])) {
				throw new Exception("curso is empty");
			}
			if (!isset($params['idcomplementario'])) {
				throw new Exception("idcomplementario is empty");
			}
			if (empty($params['idproyecto'])) {
				throw new Exception("idproyecto is empty");
			}
			if (empty($params['idrecurso'])) {
				throw new Exception("idrecurso is empty");
			}
			#preparar busqueda
			$filtros['idgrupoauladetalle'] = $params['grupo'];
			$filtros['idcurso'] = $params['curso'];
			$filtros['idcomplementario'] = $params['idcomplementario'];
			$filtros['idproyecto'] = $params['idproyecto'];
			$filtros['idrecurso'] = $params['idrecurso'];
			$filtros['bd_se'] = !empty($params['pv_smartenglish']) ? ($params['pv_smartenglish'] == 'adultos' ? NAME_BDSEAdultos : NAME_BDSEAdolecentes) : NAME_BDSEAdultos;
			$filtros['esexamen'] = !empty($params['esexamen']) ? true : false;
			//buscar alumnos
			$filtrosAlumnos = ['idgrupoauladetalle' => $params['grupo']];

			if (!empty($params['alumnos'])) {
				$filtrosAlumnos['idalumno'] = $params['alumnos'];
			}

			$rs_alumnos = $this->oNegReportes_mod->findAlumnosxgrupo($filtrosAlumnos);

			if (!empty($rs_alumnos)) {
				$filtros['alumnos'] = $rs_alumnos;
				$this->datos = $this->oNegReportes_mod->findHabilidadxsesion($filtros);
			}
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getCursosUsuario($return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();

			$_idrol = !empty($_REQUEST['idrol']) ? $_REQUEST['idrol'] : $oUsuario["idrol"];
			$_idpersona = !empty($_REQUEST['idpersona']) ? $_REQUEST['idpersona'] : $oUsuario["idpersona"];
			$_idproyecto = !empty($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : $oUsuario["idproyecto"];

			$filtros = array();
			$arrCursos = array();
			if ($_idrol == 2) {
				$filtros["iddocente"] = $_idpersona;
				$filtros["idproyecto"] = $_idproyecto;
				$arrInstancias = $this->oNegAcad_grupoauladetalle->buscar($filtros);
				$arrCursos = $arrInstancias;
			} elseif ($_idrol == 3) { //Alumno
				$filtros["idalumno"] = $_idpersona;
				$filtros["idproyecto"] = $_idproyecto;
				// $filtros['fechaactiva'] = true;
				$filtros['estado'] = 1;
				$arrCursos = $this->oNegAcad_matricula->buscar($filtros); //varios cursos
			} elseif ($_idrol == 1) {
				// $filtros["iddocente"] = $oUsuario["idpersona"];
				$filtros["idproyecto"] = $_idproyecto;
				$filtros["estado"] = "1";
				$arrGrupoAula = $this->oNegAcad_grupoaula->buscar($filtros);
				// echo json_encode($arrGrupoAula);
				// exit();
				foreach ($arrGrupoAula as $i => $grupo) {
					$filtros["idgrupoaula"] = $grupo["idgrupoaula"];
					$grupo["arrCursos"] = $this->oNegAcad_grupoauladetalle->buscar($filtros);
					$arrGrupoAula[$i] = $grupo;
				}
				$arrCursos = $arrGrupoAula;
			} //Admin
			$this->datos = $arrCursos;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getAlumnosAdmin()
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();
			$arrAlumnos = array();
			if ($oUsuario["idrol"] == 1) {
				$filtros["idproyecto"] = $oUsuario["idproyecto"];
				$filtros["estado"] = "1";
				$filtros["orderby"] = false;
				$arrAlumnos = $this->oNegAcad_matricula->listarAlumnos($filtros);
			}
			$this->datos = $arrAlumnos;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getCursosAlumno()
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();
			$arrCursos = array();
			$arrCursos_formula = array();
			$idAlunmo = !empty($_REQUEST["idAlunmo"]) ? $_REQUEST["idAlunmo"] : 0;
			$filtros["idalumno"] = $idAlunmo;
			$filtros["idproyecto"] = $oUsuario["idproyecto"];
			$filtros['estado'] = 1;
			// $filtros['fechaactiva'] = true;
			$arrCursos = $this->oNegAcad_matricula->buscar($filtros);

			$idcategoria = !empty($_REQUEST["idcategoria"]) ? $_REQUEST["idcategoria"] : '';
			foreach ($arrCursos as $i => $curso) { //obtengo las formulas
				$arrCursos_formula[] = $this->oNegCursoDetalle->temasacalificar($curso["idcurso"], $curso["idcomplementario"], $curso["tipocurso"], $idcategoria);
				foreach ($arrCursos_formula as $j => $curso_formula) {
					$curso_nota = $this->getNotasCurso($curso_formula, $idAlunmo, $curso["idgrupoauladetalle"], true);
					// print_r($curso_nota );
					if (count($curso_nota["formulaActual"]) == 0) {
						$curso_nota["formulaActual"]['formulatxt'] = "Fórmula no asignada";
						$curso_nota["formulaActual"]['notas'] = array();
					}
					$curso_formula = $curso_nota;
					$arrCursos_formula[$j] = $curso_formula;
				}
				$arrCursos[$i]["arrCursos_nota"] = $arrCursos_formula;
				$arrCursos_formula = [];
			}
			$this->datos = $arrCursos;
			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function tareasProyectos()
	{
		$this->documento->plantilla = 'blanco';
		try {
			$filtros = array();
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $filtros["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			$filtros['fechaactiva'] = true;
			$arrMatriculas = $this->oNegAcad_matricula->buscar($filtros); //varios cursos
			$idproyecto = (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') ? $_REQUEST["idproyecto"] : "T";
			$tipo = (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') ? $_REQUEST["tipo"] : "T";
			$tipo_ = ($tipo == "T") ? "estarea" : "esproyecto";

			$this->datos = array();
			// echo json_encode($arrMatriculas); exit();
			foreach ($arrMatriculas as $i => $matricula) { //dentro de un curso
				// var_dump($matricula);
				$curso = array(
					"idcurso" => $matricula["idcurso"],
					"strcurso" => $matricula["strcurso"],
					"array" => array()
				);
				$tareas = array();
				$arrCursosDetalles = array();

				if ($matricula['tipocurso'] != '2') {
					$arrCursosDetalles = $this->oNegAcad_cursodetalle->getCursosDetalles(array("idcurso" => $matricula["idcurso"]));
				} else {
					$arrCursosDetalles = $this->oNegAcad_cursodetalle->getCursosDetalles(array("sql" => 'sql1', "idcurso" => $matricula["idcurso"]));
				}

				foreach ($arrCursosDetalles as $key => $cursoDetalle) { //dentro de cursosdetalles
					$cursoDetalle["txtjson"] = (array) json_decode($cursoDetalle["txtjson"], true);
					if (isset($cursoDetalle["txtjson"]["options"])) {
						$validar = count($cursoDetalle["txtjson"]["options"]);
					} else {
						$validar = 0;
					}
					if ($validar == 0) { //No tiene pestañas (options)
						//compara si cada cursodetalle de ese curso es tarea o proyecto:
						if (isset($cursoDetalle["txtjson"][$tipo_]) && $cursoDetalle["txtjson"][$tipo_] == "si") {
							$filtrosT = array(
								"sql" => "1",
								"idproyecto" => $idproyecto,
								"idcurso" => $matricula["idcurso"],
								"idsesion" => $cursoDetalle["idcursodetalle"],
								"idpestania" => 0,
								"tipo" => $tipo
							);
							$rsT = $this->oNegTareas->buscar($filtrosT);
							if (count($rsT) == 0) { //no hay registros en la tabla tarea
								$tareas[] = array(
									"id" => 0,
									"idcurso" => $matricula["idcurso"],
									"idsesion" => $cursoDetalle["idcursodetalle"],
									"idpesstania" => 0,
									"nombre" => ($tipo == 'T' ? "Tarea " : "Proyecto ") . $cursoDetalle["nombre_detalle"],
									"respondido" => 0,
									"fecha" => "",
									"nota" => "Sin Calificar",
								);
							} else { //Sí hay registros en la tabla tarea
								$rsT = $rsT[0];
								$tareas[] = array(
									"id" => $rsT["idtarea"],
									"idcurso" => $matricula["idcurso"],
									"idsesion" => $cursoDetalle["idcursodetalle"],
									"idpesstania" => 0,
									"nombre" => ($tipo == 'T' ? "Tarea " : "Proyecto ") . $cursoDetalle["nombre_detalle"],
									"respondido" => 1,
									"fecha" => $rsT["fecha_creacion"],
									"nota" => (empty($rsT["fecha_calificacion"])) ? "Sin Calificar" : $rsT["nota"],
									"fecha_calificacion" => empty($rsT["fecha_calificacion"]) ? "" : $rsT["fecha_calificacion"],
									"notabaseen" => $rsT["notabaseen"],
									"nombre_docente" => $rsT["nombre_docente"],
								);
							}
						}
					} else { //Sí tiene pestañas (options)
						//entramos a pestañas (options)
						foreach ($cursoDetalle["txtjson"]["options"] as $keyO => $option) {
							//compara si cada cursodetalle de ese curso es tarea o proyecto:
							if (isset($option[$tipo_]) && $option[$tipo_] == "si") {
								$filtrosT = array(
									"sql" => "1",
									"idproyecto" => $idproyecto,
									"idcurso" => $matricula["idcurso"],
									"idsesion" => $cursoDetalle["idcursodetalle"],
									"idpestania" => $cursoDetalle["idcursodetalle"] . $keyO,
									"tipo" => $tipo
								);
								$rsT = $this->oNegTareas->buscar($filtrosT);
								if (count($rsT) == 0) { //no hay registros en la tabla tarea
									$tareas[] = array(
										"id" => 0,
										"idcurso" => $matricula["idcurso"],
										"idsesion" => $cursoDetalle["idcursodetalle"],
										"idpesstania" => $cursoDetalle["idcursodetalle"] . $keyO,
										"nombre" => ($tipo == 'T' ? "Tarea " : "Proyecto ") . $option["nombre"],
										"respondido" => 0,
										"fecha" => "",
										"nota" => "Sin Calificar",
									);
								} else { //sí hay registros en la tabla tarea
									$rsT = $rsT[0];
									$tareas[] = array(
										"id" => $rsT["idtarea"],
										"idcurso" => $matricula["idcurso"],
										"idsesion" => $cursoDetalle["idcursodetalle"],
										"idpesstania" => $cursoDetalle["idcursodetalle"] . $keyO,
										"nombre" => ($tipo == 'T' ? "Tarea " : "Proyecto ") . $option["nombre"],
										"respondido" => 1,
										"fecha" => $rsT["fecha_creacion"],
										"nota" => (empty($rsT["fecha_calificacion"])) ? "Sin Calificar" : $rsT["nota"],
										"fecha_calificacion" => empty($rsT["fecha_calificacion"]) ? "" : $rsT["fecha_calificacion"],
										"notabaseen" => $rsT["notabaseen"],
										"nombre_docente" => $rsT["nombre_docente"],
									);
								}
							}
						}
					}
					$arrCursosDetalles[$key] = $cursoDetalle;
				}
				$curso["array"] = $tareas;
				$this->datos[] = $curso;
				// $this->datos[] = $cursoDetalle;
				// $this->datos = $arrMatriculas;
			}

			echo json_encode(array('code' => 200, 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function notasfinales()
	{
		$user = NegSesion::getUsuario();

		$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
		$idalumno = !empty($_REQUEST["idalumno"]) ? $_REQUEST["idalumno"] : (($idrol == 3) ? $user["idpersona"] : '');

		$iddocente = !empty($_REQUEST["iddocente"]) ? $_REQUEST["iddocente"] : (($idrol == 2) ? $user["idpersona"] : '');
		$idproyecto = !empty($_REQUEST["idproyecto"]) ? $_REQUEST["idproyecto"] : $user["idproyecto"];

		$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : '';
		$esarrayidalumno = stripos($idalumno, ",");
		if ($esarrayidalumno) $idalumno = explode(',', $idalumno);
		$idcursos = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
		$esarraycursos = stripos($idcursos, ",");
		if ($esarraycursos) $idcursos = explode(',', $idcursos);

		if ($idrol == 3) $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idcurso' => $idcursos, 'idalumno' => $idalumno, 'orderby' => 'strcurso', 'idproyecto' => $idproyecto));
		else if ($idrol == 2) $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idcurso' => $idcursos, 'idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $iddocente, 'orderby' => 'strcurso', 'idproyecto' => $idproyecto));
		else $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idcurso' => $idcursos, 'idgrupoauladetalle' => $idgrupoauladetalle, 'orderby' => 'strcurso', 'idproyecto' => $idproyecto));
		echo json_encode(array('code' => 200, 'data' => $mismatriculas));
		exit(0);
		//$hayexamenescurso=array();
		//$datosreturn=array();
		//var_dump($mismatriculas[0]);
	}
	public function notasfinaleshabilidades()
	{
		$user = NegSesion::getUsuario();
		$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
		$idalumno = !empty($_REQUEST["idalumno"]) ? $_REQUEST["idalumno"] : (($idrol == 3) ? $user["idpersona"] : '');
		$iddocente = !empty($_REQUEST["iddocente"]) ? $_REQUEST["iddocente"] : (($idrol == 2) ? $user["idpersona"] : '');
		$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : '';
		$esarrayidalumno = stripos($idalumno, ",");
		if ($esarrayidalumno) $idalumno = explode(',', $idalumno);
		$idcursos = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
		$esarraycursos = stripos($idcursos, ",");
		if ($esarraycursos) $idcursos = explode(',', $idcursos);

		if ($idrol == 3) $mismatriculas = $this->oNegAcad_matricula->notas_habilidades(array('idcurso' => $idcursos, 'idalumno' => $idalumno, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else if ($idrol == 2) $mismatriculas = $this->oNegAcad_matricula->notas_habilidades(array('idcurso' => $idcursos, 'idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $iddocente, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else $mismatriculas = $this->oNegAcad_matricula->notas_habilidades(array('idcurso' => $idcursos, 'idgrupoauladetalle' => $idgrupoauladetalle, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		echo json_encode(array('code' => 200, 'data' => $mismatriculas, 'total' => count($mismatriculas)));
		exit(0);
	}
	public function notas()
	{
		$user = NegSesion::getUsuario();
		$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
		$idalumno = !empty($_REQUEST["idalumno"]) ? $_REQUEST["idalumno"] : (($idrol == 3) ? $user["idpersona"] : '');
		$iddocente = !empty($_REQUEST["iddocente"]) ? $_REQUEST["iddocente"] : (($idrol == 2) ? $user["idpersona"] : '');
		$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : '';
		$esarrayidalumno = stripos($idalumno, ",");
		if ($esarrayidalumno) $idalumno = explode(',', $idalumno);
		$idcursos = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
		$esarraycursos = stripos($idcursos, ",");
		if ($esarraycursos) $idcursos = explode(',', $idcursos);
		//$cursossetting=$this->oNegAcad_curso->buscar(array('idcurso'=>$idcursos,'idproyecto'=>$user["idproyecto"],'sql3'=>true));
		if ($idrol == 3) $mismatriculas = $this->oNegAcad_matricula->buscar(array('idcurso' => $idcursos, 'idalumno' => $idalumno, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else if ($idrol == 2) $mismatriculas = $this->oNegAcad_matricula->buscar(array('idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $iddocente, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else $mismatriculas = $this->oNegAcad_matricula->buscar(array('idgrupoauladetalle' => $idgrupoauladetalle, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		$hayexamenescurso = array();
		$datosreturn = array();
		if (!empty($mismatriculas))
			foreach ($mismatriculas as $cur) {
				unset($cur["txtjson"]);
				unset($cur["aniopublicacion"]);
				unset($cur["autor"]);
				unset($cur["descripcion"]);
				unset($cur["objetivos"]);
				unset($cur["silabo"]);
				unset($cur["costo"]);
				unset($cur["fecharegistro"]);
				unset($cur["vinculosaprendizajes"]);
				unset($cur["materialesyrecursos"]);
				unset($cur["pdf"]);
				unset($cur["abreviado"]);
				unset($cur["esdemo"]);
				unset($cur["tipodoc"]);
				unset($cur["oldid"]);
				unset($cur["telefono"]);
				unset($cur["usuario"]);
				unset($cur["strlocal"]);
				unset($cur["strambiente"]);
				unset($cur["color"]);
				unset($cur["celular"]);
				unset($cur["certificacion"]);
				unset($cur["idusuario"]);
				unset($cur["estado"]);
				unset($cur["idproyecto"]);
				unset($cur["fecha_matricula"]);
				unset($cur["fecha_registro"]);
				unset($cur["fecha_vencimiento"]);
				unset($cur["estado_curso"]);

				$cur["notaentrada"] = 0;
				$cur["notasalida"] = 0;
				$cur["notaentrada_enbasea"] = 100;
				$cur["notasalida_enbasea"] = 100;
				$cur["notaentradaquiz"] = 0;
				$cur["notaentradaquiz_enbasea"] = 100;
				$cur["notasalidaquiz"] = 0;
				$cur["notasalidaquiz_enbasea"] = 100;
				$cur["notasalidatiempo"] = '00:00:00';
				$cur["notaentradatiempo"] = '00:00:00';
				$notaconfig = array();
				$examenes = array();

				if (!empty($cur["configuracion_nota"])) {
					$notaconfig = json_decode($cur["configuracion_nota"], true);
					//var_dump($notaconfig);
					if (empty($notaconfig["tipocalificacion"]) && empty($notaconfig["puntuacion"])) {
						$cur["puntaje"] = array('puntajemax' => intval($this->Dpuntajemax), 'puntajemin' => intval($this->Dpuntajemin), 'tipocalificacion' => 'N');
					} elseif ($notaconfig["tipocalificacion"] == 'P' || $notaconfig["tipocalificacion"] == 'N') {
						$cur["puntaje"] = array('puntajemax' => @intval($notaconfig["maxcal"]), 'puntajemin' => @intval($notaconfig["mincal"]), 'tipocalificacion' => @$notaconfig["tipocalificacion"]);
					} else $cur["puntaje"] = array('tipocalificacion' => @$notaconfig["tipocalificacion"], 'puntuacion' => @$notaconfig["puntuacion"]);
					$tmpexamenes = $notaconfig["examenes"];
					if (!empty($tmpexamenes))
						foreach ($tmpexamenes as $tmpexa) {
							if (@$tmpexa["porcentaje"] > 0) {
								//var_dump($cur["puntaje"]);
								$puntajemax = !empty($cur["puntaje"]['puntajemax']) ? $cur["puntaje"]['puntajemax'] : $this->Dpuntajemax;
								$puntajemin = !empty($cur["puntaje"]['puntajemin']) ? $cur["puntaje"]['puntajemin'] : $this->Dpuntajemin;
								$examenes[] = array('idexamen' => $tmpexa["idexamen"], 'tipo' => @$tmpexa["tipo"], 'nombre' => $tmpexa["nombre"], 'idcursodetalle' => $tmpexa["idcursodetalle"], 'nota' => 0, 'porcentaje' => @intval($tmpexa["porcentaje"]), 'puntajemax' => intval($puntajemax), 'puntajemin' => intval($puntajemin), 'imagen' => $tmpexa["imagen"], 'idrecurso' => $tmpexa["idrecurso"]);
							}
						}
					$cur["formula"] = $examenes;
				} else {
					$examenes[] = array('idexamen' => 0, 'nota' => 0, 'porcentaje' => 100);
					$cur["puntaje"] = array('puntajemax' => intval($this->Dpuntajemax), 'puntajemin' => intval($this->Dpuntajemin), 'tipocalificacion' => 'N');
					$cur["formula"] = $examenes;
				}
				$examenes = array(); // falta revisar que tome la nota y asignarla al curso del alumno..
				$idalumno = $cur["idalumno"];
				$notasdelcurso = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idcurso' => $cur["idcurso"]));
				$resultadoconnotascurso = array(); // todas las notas por curso si las hubiera
				if (!empty($notasdelcurso)) {
					if (empty($hayexamenescurso[$cur["idcurso"]])) {
						$examenes = $this->oNegAcad_cursodetalle->examenes($cur["idcurso"], 0);
						$hayexamenescurso[$cur["idcurso"]] = $examenes;
					} else $examenes = $hayexamenescurso[$cur["idcurso"]];
					$nexamen = 1;
					if (!empty($examenes)) {
						$ntexamenes = count($examenes);
						foreach ($examenes as $ex) {
							foreach ($notasdelcurso as $nt) {
								$curptmax = !empty($cur["puntaje"]["puntajemax"]) ? $cur["puntaje"]["puntajemax"] : $this->Dpuntajemax;
								$curptmim = !empty($cur["puntaje"]["puntajemin"]) ? $cur["puntaje"]["puntajemin"] : $this->Dpuntajemin;
								$calificacion_total = !empty($nt["calificacion_total"]) ? ceil($nt["calificacion_total"]) : $this->Dpuntajemax;
								$calificacion_min = !empty($nt["calificacion_min"]) ? ceil($nt["calificacion_min"]) : $this->Dpuntajemin;
								$calificacion_en = !empty($nt["calificacion_en"]) ? ceil($nt["calificacion_en"]) : 'P';
								$tiempo_realizado = !empty($nt["tiempo_realizado"]) ? ceil($nt["tiempo_realizado"]) : '00:00:00';
								$nota = 0;
								//if($calificacion_en=='P'||$calificacion_en=='A') $nota=$nt["nota"];
								//elseif($calificacion_en=='N') $nota=$this->notacal($nt["nota"],$calificacion_total,100);
								//else $nota=$nt["nota"];
								$nota = $this->notacal($nt["nota"], $calificacion_total, 100);
								$nota = $this->notacal($nota, 100, $curptmax); // nota equivalente al puntaje configurado en el curso;
								if ($nexamen == 1 && $ex["idexamen"] == $nt["idrecurso"] && $nt["tipo"] == 'E') {
									$cur["notaentrada"] = $nota;
									$cur["notaentradaquiz"] = $nt["nota"];
									$cur["notaentradatiempo"] = $tiempo_realizado;
									$cur["notaentradaquiz_enbasea"] = $calificacion_total;
									$cur["notaentrada_enbasea"] = $curptmax;
								} elseif ($nexamen == $ntexamenes && $ex["idexamen"] == $nt["idrecurso"] && $nt["tipo"] == 'F') {
									$cur["notasalida"] = $nota;
									$cur["notasalidaquiz"] = $nt["nota"];
									$cur["notasalidatiempo"] = $tiempo_realizado;
									$cur["notasalidaquiz_enbasea"] = $calificacion_total;
									$cur["notasalida_enbasea"] = $curptmax;
								}
							}
							$nexamen++;
						}
						foreach ($notasdelcurso as $nt) {
							$nexamen2 = 1;
							$ntexamenes2 = count($cur["formula"]);
							$newformula = array();
							$calificacion_total = !empty($nt["calificacion_total"]) ? ceil($nt["calificacion_total"]) : $this->Dpuntajemax;
							$calificacion_min = !empty($nt["calificacion_min"]) ? ceil($nt["calificacion_min"]) : $this->Dpuntajemin;
							//$calificacion_en=!empty($nt["calificacion_en"])?ceil($nt["calificacion_en"]):'P';
							$tiempo_realizado = !empty($nt["tiempo_realizado"]) ? ceil($nt["tiempo_realizado"]) : '00:00:00';


							if (!empty($cur["formula"]))
								foreach ($cur["formula"] as $fr) {
									if (!empty($fr["idexamen"])) {
										if ($fr["idexamen"] == $nt["idrecurso"] && $fr["idcursodetalle"] == $nt["idcursodetalle"]) {
											$nota = $this->notacal($nt["nota"], 100, $calificacion_total); // nota al smarquiz al 100%
											$nota = $this->notacal($nota, 100, $fr["puntajemax"]);
											$fr["nota"] = $nota;
											$fr["nota_enbasea"] = $fr["puntajemax"];
											$fr["notaquiz"] = $nt["nota"];
											$fr["notatiempo"] = $tiempo_realizado;
											$fr["notaquiz_enbasea"] = $calificacion_total;
										}
										$newformula[] = $fr;
									}
								}
							if (!empty($newformula))
								$cur["formula"] = $newformula;
						}
					}
				}
				$datosreturn[] = $cur;
			}
		echo json_encode(array('code' => 200, 'data' => $datosreturn));
		exit(0);
	}
	private function notacal($pt, $ptmax = 100, $equivalente = 100)
	{ // regla de tres para calcular nota;
		if ($pt == 0) return 0;
		return round((($pt * $equivalente) / $ptmax), 2);
	}
	public function defecto()
	{
		return $this->notas();
	}
	public function progreso()
	{
		try {
			$filtros = array();
			$user = NegSesion::getUsuario();
			$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
			// $idcursos = isset($_REQUEST['idcurso']) ? $_REQUEST['idcurso'] : '';
			$idgrupoauladetalle = isset($_REQUEST['idgrupoauladetalle']) ? $_REQUEST['idgrupoauladetalle'] : '';
			if ($idrol == 3) $idalumno = isset($_REQUEST['idalumno']) ? $_REQUEST['idalumno'] : NegSesion::getUsuario()['idpersona'];
			else if ($idrol == 2) $iddocente = isset($_REQUEST['iddocente']) ? $_REQUEST['iddocente'] : NegSesion::getUsuario()['idpersona'];
			//solo el alumno puede elegir varios cursos a la vez:
			$esarraycursos = stripos($idgrupoauladetalle, ",");
			if ($esarraycursos) $idgrupoauladetalle = explode(',', $idgrupoauladetalle);


			//if(empty($idcurso)){
			$idproyecto = isset($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : NegSesion::getUsuario()['idproyecto'];
			//buscar todos los cursos matriculados
			if ($idrol == 3) {
				$_cursos = $this->oNegAcad_matricula->buscar(array('idalumno' => $idalumno, 'idgrupoauladetalle' => $idgrupoauladetalle, 'idproyecto' => $idproyecto, 'orderby' => ' strcurso'));
			} else if ($idrol == 2) {
				$_cursos = $this->oNegAcad_matricula->buscar(array('iddocente' => $iddocente, 'idgrupoauladetalle' => $idgrupoauladetalle, 'idproyecto' => $idproyecto, 'orderby' => ' strcurso'));
				// $_cursos =$this->oWebMine->getInstanciaCurso( array("idgrupoauladetalle"=>$idgrupoauladetalle), true);
				// echo json_encode($_cursos);
				// exit();
			} else {
				$_cursos = $this->oNegAcad_matricula->buscar(array('idgrupoauladetalle' => $idgrupoauladetalle, 'idproyecto' => $idproyecto, 'orderby' => ' strcurso'));
			}

			// foreach ($_cursos as $key => $value) {
			// 	$idcurso[] = $value['idcurso'];
			// }


			if (count($_cursos) == 0) {
				echo json_encode(array('code' => 200, 'data' => []));
				exit();
				// throw new Exception("No hay alumnos matriculados");
			}




			//$rs_cursos = $this->oNegAcad_curso->buscar(array('idcurso'=>$idcurso,'idproyecto'=>));
			/*if(!is_array($idcursos)){
				$idcurso = array($idcurso);
			}*/
			$arrCursoAlumno = array();
			foreach ($_cursos as $key => $curso) { //CADA ALUMNO/MATRICULA
				$idcur = $curso["idcurso"];
				$mineTipocurso = "any";
				if ($curso["idcomplementario"] != "0") {
					$idcur = $curso["idcomplementario"];
					$mineTipocurso = "complementario";
				}
				$nombre = $curso["strcurso"];
				$idalumno = $curso["idalumno"];
				$progresoCurso = 0;
				$arrSesionesConProgreso = array();
				//$_nombre = 'empty';
				//TODAS LAS SESIONES DEL CURSO??:
				$arrSesiones = $this->oNegAcad_cursodetalle->sesiones($idcur, 0, $mineTipocurso);
				// echo json_encode($arrSesiones);exit();
				foreach ($arrSesiones as $k => $sesion) {
					//SE OBTIENE EL PROGRESO DEL ALUMNO EN C/U DE LAS SESIONES/CONTENEDORES
					$sesion = $this->oNegAcad_curso->getProgresoUnidad($sesion, $idcur, $idalumno, $mineTipocurso == "complementario");
					$arrSesionesConProgreso[] = $sesion; //SIGUEN TODAS LAS SESIONES, PERO CON SU PROGRESO
				}
				if (!empty($arrSesionesConProgreso)) {
					$sumaProgreso = 0;
					foreach ($arrSesionesConProgreso as $sesionConProgreso) {
						$sumaProgreso += doubleval($sesionConProgreso['progreso']);
					}
					$progresoCurso = $sumaProgreso / count($arrSesionesConProgreso);
				}
				$arrCursoAlumno[] = array('idcurso' => $idcur, 'nombre' => $nombre, 'arrSesionesConProgreso' => $arrSesionesConProgreso, 'progreso' => round($progresoCurso, 2, PHP_ROUND_HALF_DOWN), 'alumno' => $curso['stralumno']);
			}
			echo json_encode(array('code' => 200, 'data' => $arrCursoAlumno));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => $e->getMessage(), 'data' => array()));
			exit(0);
		}
	}
	public function tiempo()
	{
		$user = NegSesion::getUsuario();
		$idrol = !empty($_REQUEST["idrol"]) ? $_REQUEST["idrol"] : $user["idrol"];
		$idalumno = !empty($_REQUEST["idalumno"]) ? $_REQUEST["idalumno"] : (($idrol == 3) ? $user["idpersona"] : '');
		$iddocente = !empty($_REQUEST["iddocente"]) ? $_REQUEST["iddocente"] : (($idrol == 2) ? $user["idpersona"] : '');
		$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : '';
		$esarrayidalumno = stripos($idalumno, ",");
		if ($esarrayidalumno) $idalumno = explode(',', $idalumno);
		$idcursos = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
		$esarraycursos = stripos($idcursos, ",");
		if ($esarraycursos) $idcursos = explode(',', $idcursos);
		if ($idrol == 3) $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idcurso' => $idcursos, 'idalumno' => $idalumno, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else if ($idrol == 2) $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $iddocente, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		else $mismatriculas = $this->oNegAcad_matricula->notas_config(array('idgrupoauladetalle' => $idgrupoauladetalle, 'orderby' => 'strcurso', 'idproyecto' => $user["idproyecto"]));
		$datos = array();
		if (!empty($mismatriculas)) {
			foreach ($mismatriculas as $mk => $m) {
				$tiempos = $this->oNegHistorial_sesion->buscartiempos(array('idusuario' => $m["idalumno"], 'idproyecto' => $user["idproyecto"]));
				$tiempopv = 0;
				$tiempoexam = 0;
				$tiempocursos = 0;
				if (!empty($tiempos))
					foreach ($tiempos as $kt => $t) {
						if ($t["lugar"] == 'P') $tiempopv += intval($t['tiempoensegundos']);
						if ($t["idcurso"] == $m["idcurso"] && $t["idcurso"] != null) {
							if ($t["lugar"] == 'E') $tiempoexam += intval($t['tiempoensegundos']);
							if ($t["lugar"] == 'S') $tiempocursos += intval($t['tiempoensegundos']);
						}
					}
				$m["tiempopv"] = $tiempopv;
				$m["tiempoexam"] = $tiempoexam;
				$m["tiempocursos"] = $tiempocursos;
				$datos[] = $m;
			}
		}
		echo json_encode(array('code' => 200, 'data' => $datos));
		exit(0);
	}
}
