<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-09-2019 
 * @copyright	Copyright (C) 12-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegCiudad', RUTA_BASE);
class WebCiudad extends JrWeb
{
	private $oNegCiudad;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegCiudad = new NegCiudad;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ciudad', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpais"])&&@$_REQUEST["idpais"]!='')$filtros["idpais"]=$_REQUEST["idpais"];
			if(isset($_REQUEST["idregion"])&&@$_REQUEST["idregion"]!='')$filtros["idregion"]=$_REQUEST["idregion"];
			if(isset($_REQUEST["iddepartamento"])&&@$_REQUEST["iddepartamento"]!='')$filtros["iddepartamento"]=$_REQUEST["iddepartamento"];
			if(isset($_REQUEST["idprovincia"])&&@$_REQUEST["idprovincia"]!='')$filtros["idprovincia"]=$_REQUEST["idprovincia"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["abreviacion"])&&@$_REQUEST["abreviacion"]!='')$filtros["abreviacion"]=$_REQUEST["abreviacion"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegCiudad->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id)) {
				$this->oNegCiudad->id = $id;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegCiudad->idpais=@$idpais;
				$this->oNegCiudad->idregion=@$idregion;
				$this->oNegCiudad->iddepartamento=@$iddepartamento;
				$this->oNegCiudad->idprovincia=@$idprovincia;
				$this->oNegCiudad->nombre=@$nombre;
				$this->oNegCiudad->abreviacion=@$abreviacion;
				
            if($accion=='_add') {
            	$res=$this->oNegCiudad->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Ciudad')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegCiudad->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Ciudad')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegCiudad->__set('id', $_REQUEST['id']);
			$res=$this->oNegCiudad->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegCiudad->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}