<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAulasvirtuales', RUTA_BASE);
class WebAulasvirtuales extends JrWeb
{
	private $oNegAulasvirtuales;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAulasvirtuales = new NegAulasvirtuales;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Aulasvirtuales', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idnivel"])&&@$_REQUEST["idnivel"]!='')$filtros["idnivel"]=$_REQUEST["idnivel"];
			if(isset($_REQUEST["idunidad"])&&@$_REQUEST["idunidad"]!='')$filtros["idunidad"]=$_REQUEST["idunidad"];
			if(isset($_REQUEST["idactividad"])&&@$_REQUEST["idactividad"]!='')$filtros["idactividad"]=$_REQUEST["idactividad"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["moderadores"])&&@$_REQUEST["moderadores"]!='')$filtros["moderadores"]=$_REQUEST["moderadores"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["video"])&&@$_REQUEST["video"]!='')$filtros["video"]=$_REQUEST["video"];
			if(isset($_REQUEST["chat"])&&@$_REQUEST["chat"]!='')$filtros["chat"]=$_REQUEST["chat"];
			if(isset($_REQUEST["notas"])&&@$_REQUEST["notas"]!='')$filtros["notas"]=$_REQUEST["notas"];
			if(isset($_REQUEST["dirigidoa"])&&@$_REQUEST["dirigidoa"]!='')$filtros["dirigidoa"]=$_REQUEST["dirigidoa"];
			if(isset($_REQUEST["estudiantes"])&&@$_REQUEST["estudiantes"]!='')$filtros["estudiantes"]=$_REQUEST["estudiantes"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["fecha_creado"])&&@$_REQUEST["fecha_creado"]!='')$filtros["fecha_creado"]=$_REQUEST["fecha_creado"];
			if(isset($_REQUEST["portada"])&&@$_REQUEST["portada"]!='')$filtros["portada"]=$_REQUEST["portada"];
			if(isset($_REQUEST["filtroestudiantes"])&&@$_REQUEST["filtroestudiantes"]!='')$filtros["filtroestudiantes"]=$_REQUEST["filtroestudiantes"];
			if(isset($_REQUEST["nparticipantes"])&&@$_REQUEST["nparticipantes"]!='')$filtros["nparticipantes"]=$_REQUEST["nparticipantes"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAulasvirtuales->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$aulaid)) {
				$this->oNegAulasvirtuales->aulaid = $aulaid;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegAulasvirtuales->idnivel=@$idnivel;
				$this->oNegAulasvirtuales->idunidad=@$idunidad;
				$this->oNegAulasvirtuales->idactividad=@$idactividad;
				$this->oNegAulasvirtuales->fecha_inicio=@$fecha_inicio;
				$this->oNegAulasvirtuales->fecha_final=@$fecha_final;
				$this->oNegAulasvirtuales->titulo=@$titulo;
				$this->oNegAulasvirtuales->descripcion=@$descripcion;
				$this->oNegAulasvirtuales->moderadores=@$moderadores;
				$this->oNegAulasvirtuales->estado=@$estado;
				$this->oNegAulasvirtuales->video=@$video;
				$this->oNegAulasvirtuales->chat=@$chat;
				$this->oNegAulasvirtuales->notas=@$notas;
				$this->oNegAulasvirtuales->dirigidoa=@$dirigidoa;
				$this->oNegAulasvirtuales->estudiantes=@$estudiantes;
				$this->oNegAulasvirtuales->dni=@$dni;
				$this->oNegAulasvirtuales->fecha_creado=@$fecha_creado;
				$this->oNegAulasvirtuales->portada=@$portada;
				$this->oNegAulasvirtuales->filtroestudiantes=@$filtroestudiantes;
				$this->oNegAulasvirtuales->nparticipantes=@$nparticipantes;
				
            if($accion=='_add') {
            	$res=$this->oNegAulasvirtuales->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Aulasvirtuales')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAulasvirtuales->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Aulasvirtuales')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegAulasvirtuales->__set('aulaid', $_REQUEST['aulaid']);
			$res=$this->oNegAulasvirtuales->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegAulasvirtuales->setCampo($_REQUEST['aulaid'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}