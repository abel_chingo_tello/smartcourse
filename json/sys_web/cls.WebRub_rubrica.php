<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRub_rubrica', RUTA_BASE);
class WebRub_rubrica extends JrWeb
{
	private $oNegRub_rubrica;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRub_rubrica = new NegRub_rubrica;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Rub_rubrica', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["foto"])&&@$_REQUEST["foto"]!='')$filtros["foto"]=$_REQUEST["foto"];
			if(isset($_REQUEST["tipo_encuestado"])&&@$_REQUEST["tipo_encuestado"]!='')$filtros["tipo_encuestado"]=$_REQUEST["tipo_encuestado"];
			if(isset($_REQUEST["opcion_publico"])&&@$_REQUEST["opcion_publico"]!='')$filtros["opcion_publico"]=$_REQUEST["opcion_publico"];
			if(isset($_REQUEST["autor"])&&@$_REQUEST["autor"]!='')$filtros["autor"]=$_REQUEST["autor"];
			if(isset($_REQUEST["tipo_letra"])&&@$_REQUEST["tipo_letra"]!='')$filtros["tipo_letra"]=$_REQUEST["tipo_letra"];
			if(isset($_REQUEST["tamanio_letra"])&&@$_REQUEST["tamanio_letra"]!='')$filtros["tamanio_letra"]=$_REQUEST["tamanio_letra"];
			if(isset($_REQUEST["fecha_creacion"])&&@$_REQUEST["fecha_creacion"]!='')$filtros["fecha_creacion"]=$_REQUEST["fecha_creacion"];
			if(isset($_REQUEST["tipo_rubrica"])&&@$_REQUEST["tipo_rubrica"]!='')$filtros["tipo_rubrica"]=$_REQUEST["tipo_rubrica"];
			if(isset($_REQUEST["puntuacion_general"])&&@$_REQUEST["puntuacion_general"]!='')$filtros["puntuacion_general"]=$_REQUEST["puntuacion_general"];
			if(isset($_REQUEST["tipo_pregunta"])&&@$_REQUEST["tipo_pregunta"]!='')$filtros["tipo_pregunta"]=$_REQUEST["tipo_pregunta"];
			if(isset($_REQUEST["id_usuario"])&&@$_REQUEST["id_usuario"]!='')$filtros["id_usuario"]=$_REQUEST["id_usuario"];
			if(isset($_REQUEST["activo"])&&@$_REQUEST["activo"]!='')$filtros["activo"]=$_REQUEST["activo"];
			if(isset($_REQUEST["registros"])&&@$_REQUEST["registros"]!='')$filtros["registros"]=$_REQUEST["registros"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRub_rubrica->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id_rubrica)) {
				$this->oNegRub_rubrica->id_rubrica = $id_rubrica;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRub_rubrica->titulo=@$titulo;
				$this->oNegRub_rubrica->descripcion=@$descripcion;
				$this->oNegRub_rubrica->foto=@$foto;
				$this->oNegRub_rubrica->tipo_encuestado=@$tipo_encuestado;
				$this->oNegRub_rubrica->opcion_publico=@$opcion_publico;
				$this->oNegRub_rubrica->autor=@$autor;
				$this->oNegRub_rubrica->tipo_letra=@$tipo_letra;
				$this->oNegRub_rubrica->tamanio_letra=@$tamanio_letra;
				$this->oNegRub_rubrica->fecha_creacion=@$fecha_creacion;
				$this->oNegRub_rubrica->tipo_rubrica=@$tipo_rubrica;
				$this->oNegRub_rubrica->puntuacion_general=@$puntuacion_general;
				$this->oNegRub_rubrica->tipo_pregunta=@$tipo_pregunta;
				$this->oNegRub_rubrica->id_usuario=@$id_usuario;
				$this->oNegRub_rubrica->activo=@$activo;
				$this->oNegRub_rubrica->registros=@$registros;
				
            if($accion=='_add') {
            	$res=$this->oNegRub_rubrica->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_rubrica')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRub_rubrica->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_rubrica')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRub_rubrica->__set('id_rubrica', $_REQUEST['id_rubrica']);
			$res=$this->oNegRub_rubrica->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRub_rubrica->setCampo($_REQUEST['id_rubrica'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}