<?php
set_time_limit(0);

defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMinedu', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_grado', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE);
JrCargador::clase('sys_negocio::NegResumen', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_competencias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_unidad_capacidad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_archivosalumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTareas_mensajes', RUTA_BASE);
JrCargador::clase('sys_datos::DatMine', RUTA_BASE);

class WebMine extends JrWeb
{
	private $oNegAcad_grupoauladetalle;
	private $oNegCursoDetalle;
	private $oNegAcad_grupoaula;
	private $oNegAcad_matricula;
	private $oNegAcad_cursodetalle;
	private $oNegNotas_quiz;
	private $oNegAcad_curso;
	private $oNegQuizexamenes;
	private $oNegPersona_setting;
	private $oNegPersonal;
	private $oNegHistorial_sesion;
	private $oNegActividad;
	private $oNegMinedu;
	private $oNegGrado;
	private $oNegSeccion;
	private $oNegUgel;
	private $oNegDre;
	private $oNegResumen;
	private $oNegCompetencias;
	private $oNegTareas;
	private $oNegMin_unidad_capacidad;
	private $oDatMine;
	private $asd;
	public function __construct()
	{
		parent::__construct();

		$this->oNegQuizexamenes = new NegQuizexamenes;
		$this->oNegCursoDetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegReportealumno = new NegReportealumno;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegPersona_setting = new NegPersona_setting;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
		$this->oNegActividad = new NegActividad;
		$this->oNegMinedu = new NegMinedu;
		$this->oNegGrado = new NegMin_grado;
		$this->oNegSeccion = new NegMin_sesion;
		$this->oNegUgel = new NegUgel;
		$this->oNegResumen = new NegResumen;
		$this->oNegDre = new NegMin_dre;
		$this->oNegCompetencias = new NegMin_competencias;
		$this->oNegMin_unidad_capacidad = new NegMin_unidad_capacidad;
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegTareas = new NegTareas;
		$this->oNegTareas_archivosalumno = new NegTareas_archivosalumno;
		$this->oNegTareas_mensajes = new NegTareas_mensajes;
		$this->oDatMine = new DatMine;
		//$this->oNegGeneral = new NegGeneral;

	}
	public static function getParams($params = [], $force = false)
	{
		if (isset($_REQUEST["params"]) && @$_REQUEST["params"] != '') {
			$params = json_decode($_REQUEST["params"], true);
			unset($_REQUEST);
		} else {
			if (count($params) == 0) {
				$params = $_REQUEST;
				unset($_REQUEST);
			}
		}
		if (!is_array($params)) {
			$params = get_object_vars($params);
		}
		if ($force && count($params) < 1) {
			throw new Exception("Se necesitan parámetros");
		}
		return $params;
	}
	public function getCursosUsuario($return = false) //para selects
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();
			$arrCursos = array();
			if ($oUsuario["idrol"] == 2) {
				$filtros["iddocente"] = $oUsuario["idpersona"];
				$filtros["idproyecto"] = $oUsuario["idproyecto"];
				$arrInstancias = $this->oNegAcad_grupoauladetalle->buscar($filtros);
				$arrCursos = $arrInstancias;
			} elseif ($oUsuario["idrol"] == 3) { //Alumno
				$filtros["idalumno"] = $oUsuario["idpersona"];
				$filtros["idproyecto"] = $oUsuario["idproyecto"];
				$filtros['fechaactiva'] = true;
				$arrCursos = $this->oNegAcad_matricula->buscar($filtros); //varios cursos
			} elseif ($oUsuario["idrol"] == 1) {
				$filtros["idproyecto"] = $oUsuario["idproyecto"];
				$filtros["estado"] = "1";
				$arrGrupoAula = $this->oNegAcad_grupoaula->buscar($filtros);
				foreach ($arrGrupoAula as $i => $grupo) {
					$filtros["idgrupoaula"] = $grupo["idgrupoaula"];
					$grupo["arrCursos"] = $this->oNegAcad_grupoauladetalle->buscar($filtros);
					$arrGrupoAula[$i] = $grupo;
				}
				$arrCursos = $arrGrupoAula;
			} //Admin
			$this->datos = $arrCursos;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getCursosAlumno($params, $return = false) //para selects
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$params = WebMine::getParams($params);
			$filtros = array();
			$arrCursos = array();
			//Alumno
			$filtros["idalumno"] = $params["idalumno"];
			$filtros["idproyecto"] = $oUsuario["idproyecto"];
			// $filtros['fechaactiva'] = true;
			$filtros['estado'] = 1;
			$arrCursos = $this->oNegAcad_matricula->buscar($filtros); //varios cursos

			$this->datos = $arrCursos;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getGrupoAulaAlumnos($return = false) //para selects
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();
			$arrGrupos = [];
			if ($oUsuario["idrol"] == 2) {
				$filtros["iddocente"] = $oUsuario["idpersona"];
				$filtros["idproyecto"] = $oUsuario["idproyecto"];
				$arrGrupos = $this->oNegAcad_grupoaula->getGrupoAulaRol($filtros);
				foreach ($arrGrupos as $i => $grupo) {
					$arrGrupos[$i]["arrAlumnos"] = $this->oNegAcad_grupoaula->getAlumnos([
						"idgrupoaula" => $grupo["idgrupoaula"],
						"iddocente" => $oUsuario["idpersona"]
					]);
				}
			} elseif ($oUsuario["idrol"] == 1) {
				$filtros["idproyecto"] = $oUsuario["idproyecto"];
				$arrGrupos = $this->oNegAcad_grupoaula->getGrupoAulaRol($filtros);
				foreach ($arrGrupos as $i => $grupo) {
					$arrGrupos[$i]["arrAlumnos"] = $this->oNegAcad_grupoaula->getAlumnos([
						"idgrupoaula" => $grupo["idgrupoaula"]
					]);
				}
			}

			$this->datos = $arrGrupos;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getAlumnosCurso($params = array(), $return = false) //devuelve los alumnos de un curso
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$arrAlumnos = array();
			// $params['fechaactiva'] = true; //quitar para mostrar alumnoeiger
			// $params['idgrupoauladetalle'] = $curso['idgrupoauladetalle'];
			$arrAlumnos = $this->oNegAcad_matricula->buscar($params);
			$this->datos = $arrAlumnos;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getSesionesCurso($params = array(), $return = false) //
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			if (isset($_REQUEST["params"]) && @$_REQUEST["params"] != '') $params = json_decode($_REQUEST["params"]);
			if (!is_array($params)) {
				$params = get_object_vars($params);
			}

			if (isset($params['idcomplementario']) && $params['idcomplementario'] != 0) {
				$this->datos = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso' => $params['idcomplementario'], 'complementario' => true));
			} else {
				$this->datos = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso' => $params['idcurso'],));
			}
			// $this->datos = ;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getSesionesFiltradas($params = array(), $return = false)
	{
		try {
			if (isset($_REQUEST["params"]) && @$_REQUEST["params"] != '') $params = json_decode($_REQUEST["params"], true);
			// $params = get_object_vars($params);
			$arrSesionesCurso = $this->getSesionesCurso($params, true);
			global $arrCollector;
			$arrCollector = array();
			$fxOperarSesion = function ($sesion, $filter) {
				global $arrCollector;
				$txtjson = json_decode($sesion['txtjson'], true);
				$arrPestanias = (array) $txtjson['options'];
				if (count($arrPestanias) > 0) { //tiene pestañas
					foreach ($arrPestanias as $j => $pestania) {
						$pestania['nombrePadre'] = $sesion['nombre'];
						// echo "$filter == $pestania[type]? <hr><hr><hr>";
						if (!empty($pestania['type']) && strpos($filter, $pestania['type']) !== false) {
							$arrRs = [];
							if ($pestania['type'] == "multioptions") {
								$arrRs = $this->filtrarMultioptions(
									[
										"sesion" => $pestania,
										"filter" => $filter
									],
									true
								);
								$pestania["arrMultioptions"] = $arrRs;
							}
							if (!($pestania['type'] == "multioptions") || count($arrRs) > 0) { #p=>q
								$pestania['tipoMenu'] = "pestania";
								array_push($arrCollector, $pestania);
							}
						}
					}
				} else {
					if (!empty($sesion['typelink']) && strpos($filter, $sesion['typelink']) !== false) {
						$arrRs = [];
						if ($sesion['typelink'] == "multioptions") {
							$arrRs = $this->filtrarMultioptions(
								[
									"sesion" => $sesion,
									"filter" => $filter
								],
								true
							);
							$sesion["arrMultioptions"] = $arrRs;
						}
						if (!($sesion['typelink'] == "multioptions") || count($arrRs) > 0) { #p=>q
							$sesion['tipoMenu'] = "sesion";
							array_push($arrCollector, $sesion);
						}
					}
				}
			};
			foreach ($arrSesionesCurso as $i => $sesion) { //sesion o pestaña
				if ($sesion['espadre'] == 0) {
					$fxOperarSesion($sesion, $params['filter']);
				} elseif ($sesion['espadre'] == 1) { //contenedores
					if (count($sesion['hijos']) > 0) {
						// echo "este contenedor tiene ".count($sesion['hijos'])." hijos <hr>";
						foreach ($sesion['hijos'] as $k => $sesionHija) {
							// echo "ejecutando for $k <hr>";
							$fxOperarSesion($sesionHija, $params['filter']);
						}
					}
				}
			}
			$this->datos = $arrCollector;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function filtrarMultioptions($params = array(), $return = false) //filtra sesiones o pestañas
	{
		try {
			$params = WebMine::getParams($params);
			global $arrCollectorMultiOpt;
			$arrCollectorMultiOpt = [];
			$fxOperarSesion = function ($sesion, $filter) {
				global $arrCollectorMultiOpt;
				$link = json_decode(base64_decode($sesion['link']), true);
				// echo json_encode($link);exit();
				if ($link['mostrarcomo'] == "menu") {
					$arrMultiOptions = (array) $link['menus'];
					// echo json_encode($arrMultiOptions);exit();
					if (count($arrMultiOptions) > 0) { //tiene 
						foreach ($arrMultiOptions as $j => $multioption) {
							// echo "$filter == $multioption[type]? <hr><hr><hr>";
							if (!empty($multioption['tipo']) && strpos($filter, $multioption['tipo']) !== false) {
								// if ($multioption['type'] == $filter) {
								$multioption['tipoMenu'] = "multioption";
								// echo "añadiendo pestaña<hr>";
								array_push($arrCollectorMultiOpt, $multioption);
							}
						}
						// echo json_encode($arrCollectorMultiOpt);exit();
					}
				} else {
					echo json_encode(array('code' => 'Error', 'msj' => "mostrarcomo diferente de 'menu'"));
				}

				return $arrCollectorMultiOpt;
			};
			if (isset($params["sesion"])) { //sesion/pestaña
				$this->datos = $fxOperarSesion($params["sesion"], $params['filter']);
			} else {
				echo json_encode(array('code' => 'Error', 'msj' => "Faltan parámetros"));
			}
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getCursoDetalle($params = array(), $return = false) //filtra sesiones o pestañas
	{
		try {
			$params = WebMine::getParams($params);
			$this->datos = $this->oNegAcad_cursodetalle->getCursosDetalles($params);
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getFormulaCurso($params = [], $return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			if (isset($_REQUEST["params"]) && @$_REQUEST["params"] != '') {
				$params = json_decode($_REQUEST["params"]);
				unset($_REQUEST);
			}
			if (!is_array($params)) {
				$params = get_object_vars($params);
			}
			$formula = $this->oNegCursoDetalle->temasacalificar($params["idcurso"], $params["idcomplementario"], $params["tipocurso"], null);
			$this->datos = $formula;
			if (!$return) {
				echo htmlspecialchars(json_encode(array('code' => 200, 'data' => $this->datos)));
				// echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getExamenesCursoAlumno($params = [], $return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			if (isset($_REQUEST["idexamen"]) && @$_REQUEST["idexamen"] != '') $params["idexamen"] = $_REQUEST["idexamen"];
			if (isset($_REQUEST["idalumno"]) && @$_REQUEST["idalumno"] != '') $params["idalumno"] = $_REQUEST["idalumno"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $params["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idcomplementario"]) && @$_REQUEST["idcomplementario"] != '') $params["idcomplementario"] = $_REQUEST["idcomplementario"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $params["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			// $arrExamenes = $this->datos = $this->oNegNotas_quiz->buscar($params);
			$arrExamenOriginal = $this->datos = $this->oNegQuizexamenes->buscar(array(
				"idexamen" => $params["idexamen"],
			));
			$order = null;
			$elegirNota = null;
			if (count($arrExamenOriginal) > 0) {
				$elegirNota = $arrExamenOriginal[0]['calificacion'];
			} else {
				$elegirNota = "U";
			}
			if ($elegirNota == "U") {
				$order = "idnota";
			} elseif ($elegirNota == "M") {
				$order = "nota";
			}
			$filtros = array(
				"idrecurso" => $params["idexamen"],
				"idalumno" => $params["idalumno"],
				// "idcursodetalle" => $tema["idsesion"],
				"order" => $order
			);
			if (isset($params['idcurso'])) {
				$filtros['idcurso'] = $params['idcurso'];
			}
			if (isset($params['idcomplementario'])) {
				$filtros['idcomplementario'] = $params['idcomplementario'];
			}
			if (isset($params['idgrupoauladetalle'])) {
				$filtros['idgrupoauladetalle'] = $params['idgrupoauladetalle'];
			}
			if ($filtros['idcomplementario'] == 'null') $filtros['idcomplementario'] = 0;

			// print_r($filtros);
			// exit();
			$arrExamenAlumno = $this->datos = $this->oNegNotas_quiz->buscar($filtros);
			if (!empty($arrExamenAlumno)) {
				$extemporal = array();
				foreach ($arrExamenAlumno as $k => $ex) {
					if ($ex["idexamenproyecto"] == -100) {
						$preguntas = json_decode($ex["preguntas"], true);
						$ipost = stripos($preguntas["archivo"], 'static');
						$archivo = RUTA_BASE . substr($preguntas["archivo"], $ipost);
						if (file_exists($archivo)) {
							@chmod($archivo, 0777);
							require_once($archivo);
							$ex["preguntas"] = $preguntas;
						} else {
							$ex["preguntas"] = null;
						}
					}
					$extemporal[$k] = $ex;
				}
				$arrExamenAlumno = $extemporal;
			}
			$this->datos = $arrExamenAlumno;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	// public function identificarCurso($params = array(), $return = false) //recibe idcurso|idgrupoauladetalle||idcomplementario
	// {
	// 	try {
	// 		if (isset($_REQUEST["params"]) && @$_REQUEST["params"] != '') $params = json_decode($_REQUEST["params"]);
	// 		if (!is_array($params)) {
	// 			$params = get_object_vars($params);
	// 		}

	// 		if (isset($params['idcomplementario'])&&$params['idcomplementario'] != 0) {
	// 			$this->datos = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso' => $params['idcomplementario'], 'complementario' => true));
	// 		} else {
	// 			$this->datos = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso' => $params['idcurso'],));
	// 		}
	// 		// $this->datos = ;
	// 		if (!$return) {
	// 			echo json_encode(array('code' => 200, 'data' => $this->datos));
	// 			exit(0);
	// 		} else {
	// 			return $this->datos;
	// 		}
	// 	} catch (Exception $e) {
	// 		echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
	// 		exit(0);
	// 	}
	// }
	public function getData($params = [], $return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();

			if (isset($_REQUEST["params"]) && @$_REQUEST["params"] != '') $params = json_decode($_REQUEST["params"]);
			// print_r($params);exit();
			if (!is_array($params)) {
				$params = get_object_vars($params);
			}
			$this->datos = $this->oDatMine->getData($params);
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function updateData($params = [], $return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$params = WebMine::getParams($params);
			// if (isset($_REQUEST["params"]) && @$_REQUEST["params"] != '') $params = json_decode($_REQUEST["params"]);
			// print_r($params);exit();
			// $params = get_object_vars($params);
			if (
				!isset($params["arrUpdate"]) || count($params["arrUpdate"]) < 1 ||
				!isset($params["arrFiltros"]) || count($params["arrFiltros"]) < 1
			) {
				throw new Exception("ERROR: Insuficientes parámetros");
			}
			$this->datos = $this->oDatMine->updateData($params);
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	private function convertirNota($nota, $notaMax, $conf_notaMax)
	{
		$nota_convertida = null;
		$nota_convertida = ((float) $nota * (float) $conf_notaMax) / (float) $notaMax;
		return round($nota_convertida, 2);
	}
	public function getNotasCurso($curso = null, $idalumno = null,  $return = false)
	{
		try { //tenga formula o no
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();

			$config_maxCal = 20;
			if (isset($curso['formulaActual']['maxcal'])) {
				$config_maxCal = $curso['formulaActual']['maxcal'];
			}

			$arrTemasAevaluar = $curso["temasaevaluar"];
			foreach ($arrTemasAevaluar as $key => $tema) {
				switch ($tema["tipo"]) {
					case 'smartquiz':
						$arrExamen = $this->datos = $this->oNegQuizexamenes->buscar(array(
							"idexamen" => $tema["idexamen"],
						));
						$order = null;
						$elegirNota = null;
						if (count($arrExamen) > 0) {
							$elegirNota = $arrExamen[0]['calificacion'];
						} else {
							$elegirNota = "U";
						}
						if ($elegirNota == "U") {
							$order = "idnota";
						} elseif ($elegirNota == "M") {
							$order = "nota";
						}
						$arrNotaExamen = $this->datos = $this->oNegNotas_quiz->buscar(array(
							"idrecurso" => $tema["idexamen"],
							"idcurso" => $curso["idcursoprincipal"],
							"idalumno" => $idalumno,
							"idcomplementario" => $curso["idcomplementario"],
							// "idcursodetalle" => $tema["idsesion"],
							"order" => $order
						));
						if (count($arrNotaExamen) > 0) {
							// print_r($arrNotaExamen);exit();
							$examen = $arrNotaExamen[0];
							// echo json_encode($examen);
							// exit();
							// if (false) 
							if ($examen["nota"] != null && $config_maxCal != $examen["calificacion_total"]) //realiza la conversión si se encuentran en distintas bases
							{ //no entra si el curso es 20//se supone que es un numero
								$tema["nota"] = (float) $this->convertirNota($examen["nota"], $examen["calificacion_total"], $config_maxCal);
							} else { //puede ser null o otra cosa?
								$tema["nota"] = $examen["nota"];
							}
						} else {
							$tema["nota"] = null; //no lo ha rendido
						}
						break;
					case 'estarea' || 'esproyecto':
						$tema["nota"] = $this->getNotaTareaProyecto($tema, $idalumno, true);
						break;
					default:
						break;
				}
				$arrTemasAevaluar[$key] = $tema;
			}
			$curso["temasaevaluar"] = $arrTemasAevaluar;
			$this->datos = $curso;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getNotaTareaProyecto($tareaproyecto, $idalumno, $return = false)
	{
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array(
				"sql" => "1",
				"idproyecto" => $oUsuario["idproyecto"],
				"idalumno" => $idalumno,
				"idsesion" => $tareaproyecto["idsesion"],
				"idpestania" => $tareaproyecto["idpestania"],
			);
			$arrTarea_nota = $this->oNegTareas->buscar($filtros);
			$nota = null;
			if (count($arrTarea_nota) > 0) {
				$nota =  (empty($arrTarea_nota[0]["fecha_calificacion"])) ? "Sin Calificar" : $arrTarea_nota[0]["nota"]; //$arrTarea_nota[0]["nota"];
			}
			// echo "<hr>";
			// print_r($arrTarea_nota);
			// echo "<hr>";
			$this->datos = $nota;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function getInstanciaCurso($params = array(), $return = false)
	{ //["idgrupoauladetalle","iddocente", "idcurso", "idcomplementario", "idproyecto"]
		try {
			$oUsuario = NegSesion::getUsuario();
			$filtros = array();
			$arrCursos = array();
			if (isset($params["idgrupoauladetalle"]) && !empty($params["idgrupoauladetalle"])) {
				$filtros["idgrupoauladetalle"] = $params["idgrupoauladetalle"];
				$arrInstancias = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			} else {
				if (isset($params["iddocente"]) && !empty($params["iddocente"])) {
					$filtros["iddocente"] =  $params["iddocente"];
				}
				if (isset($params["idcurso"]) && !empty($params["idcurso"])) {
					$filtros["idcurso"] = $params["idcurso"];
					$filtros["idcomplementario"] = isset($params["idcomplementario"]) ? $params["idcomplementario"] : 0;
				}
				$filtros["idproyecto"] = isset($params["idproyecto"]) ? $params["idproyecto"] : $oUsuario["idproyecto"];
				$arrInstancias = $this->oNegAcad_grupoauladetalle->buscar($filtros);
			}
			$arrCursos = $arrInstancias;
			$this->datos = $arrCursos;
			if (!$return) {
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} else {
				return $this->datos;
			}
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
}
