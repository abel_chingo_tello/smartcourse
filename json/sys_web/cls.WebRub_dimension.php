<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRub_dimension', RUTA_BASE);
class WebRub_dimension extends JrWeb
{
	private $oNegRub_dimension;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegRub_dimension = new NegRub_dimension;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Rub_dimension', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo_pregunta"])&&@$_REQUEST["tipo_pregunta"]!='')$filtros["tipo_pregunta"]=$_REQUEST["tipo_pregunta"];
			if(isset($_REQUEST["escalas_evaluacion"])&&@$_REQUEST["escalas_evaluacion"]!='')$filtros["escalas_evaluacion"]=$_REQUEST["escalas_evaluacion"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["otros_datos"])&&@$_REQUEST["otros_datos"]!='')$filtros["otros_datos"]=$_REQUEST["otros_datos"];
			if(isset($_REQUEST["id_rubrica"])&&@$_REQUEST["id_rubrica"]!='')$filtros["id_rubrica"]=$_REQUEST["id_rubrica"];
			if(isset($_REQUEST["activo"])&&@$_REQUEST["activo"]!='')$filtros["activo"]=$_REQUEST["activo"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegRub_dimension->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$id_dimension)) {
				$this->oNegRub_dimension->id_dimension = $id_dimension;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegRub_dimension->nombre=@$nombre;
				$this->oNegRub_dimension->tipo_pregunta=@$tipo_pregunta;
				$this->oNegRub_dimension->escalas_evaluacion=@$escalas_evaluacion;
				$this->oNegRub_dimension->tipo=@$tipo;
				$this->oNegRub_dimension->otros_datos=@$otros_datos;
				$this->oNegRub_dimension->id_rubrica=@$id_rubrica;
				$this->oNegRub_dimension->activo=@$activo;
				
            if($accion=='_add') {
            	$res=$this->oNegRub_dimension->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_dimension')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegRub_dimension->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Rub_dimension')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegRub_dimension->__set('id_dimension', $_REQUEST['id_dimension']);
			$res=$this->oNegRub_dimension->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegRub_dimension->setCampo($_REQUEST['id_dimension'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}