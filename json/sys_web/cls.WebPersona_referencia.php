<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_referencia', RUTA_BASE);
class WebPersona_referencia extends JrWeb
{
	private $oNegPersona_referencia;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_referencia = new NegPersona_referencia;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_referencia', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["cargo"])&&@$_REQUEST["cargo"]!='')$filtros["cargo"]=$_REQUEST["cargo"];
			if(isset($_REQUEST["relacion"])&&@$_REQUEST["relacion"]!='')$filtros["relacion"]=$_REQUEST["relacion"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegPersona_referencia->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idreferencia)) {
				$this->oNegPersona_referencia->idreferencia = $idreferencia;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			$this->oNegPersona_referencia->idpersona=@$idpersona;
				$this->oNegPersona_referencia->nombre=@$nombre;
				$this->oNegPersona_referencia->cargo=@$cargo;
				$this->oNegPersona_referencia->relacion=@$relacion;
				$this->oNegPersona_referencia->correo=@$correo;
				$this->oNegPersona_referencia->telefono=@$telefono;
				$this->oNegPersona_referencia->mostrar=@$mostrar;
				
            if($accion=='_add') {
            	$res=$this->oNegPersona_referencia->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_referencia')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersona_referencia->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_referencia')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPersona_referencia->__set('idreferencia', $_REQUEST['idreferencia']);
			$res=$this->oNegPersona_referencia->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPersona_referencia->setCampo($_REQUEST['idreferencia'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}