<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020 
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegForoInteraccion', RUTA_BASE);
class WebForoInteraccion extends JrWeb
{
	private $oNegForoInteraccion;

	public function __construct()
	{
		parent::__construct();
		$this->oNegForoInteraccion = new NegForoInteraccion;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$filtros = array();
			if (isset($_REQUEST["idpublicacion"]) && @$_REQUEST["idpublicacion"] != '') $filtros["idpublicacion"] = $_REQUEST["idpublicacion"];
			if (empty($filtros)) {
				throw new Exception("Some fields is required");
			}
			$resPromedio = $this->oNegForoInteraccion->promedio($filtros);
			$this->datos = $resPromedio[0];


			echo json_encode(array('code' => 200, 'data' => $this->datos));

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function buscar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$filtros = array();
			if (isset($_REQUEST["idforo_interaccion"]) && @$_REQUEST["idforo_interaccion"] != '') $filtros["idforo_interaccion"] = $_REQUEST["idforo_interaccion"];
			$this->datos = null;
			if (count($filtros)) {
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idpersona"] = $usuarioAct['idpersona'];
				$this->datos = $this->oNegForoInteraccion->buscar($filtros)[0];
			}

			echo json_encode(array('code' => 200, 'data' => $this->datos));

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$interaccion = json_decode(file_get_contents("php://input"), true);
			if (json_last_error() != 0) { //hubo error al decodificar el json
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
			}

			$usuarioAct = NegSesion::getUsuario();
			$this->oNegForoInteraccion->idpersona = $usuarioAct['idpersona'];
			$this->oNegForoInteraccion->idpublicacion = $interaccion['idpublicacion'];
			$this->oNegForoInteraccion->puntuacion = $interaccion['puntuacion'];
			// $this->oNegForoInteraccion->idforo_interaccion = $interaccion['idforo_interaccion'];


			// if ($accion == '_add') {
			$res = $this->oNegForoInteraccion->agregar();
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Foros')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			// } else {
			// 	$res = $this->oNegForoInteraccion->editar();
			// 	echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Foros')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			// }

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegForoInteraccion->titulo = @$titulo;
					$this->oNegForoInteraccion->texto = @$texto;
					$this->oNegForoInteraccion->estado = @$estado;
					$this->oNegForoInteraccion->idusuario = @$idusuario;
					$this->oNegForoInteraccion->rol = @$rol;
					$this->oNegForoInteraccion->idcurso = @$idcurso;
					$this->oNegForoInteraccion->idgrupoaula = @$idgrupoaula;
					$this->oNegForoInteraccion->idgrupoauladetalle = @$idgrupoauladetalle;
					$this->oNegForoInteraccion->idforopadre = @$idforopadre;
					$this->oNegForoInteraccion->tipo = @$tipo;
					$this->oNegForoInteraccion->regfecha = @$regfecha;
					$this->oNegForoInteraccion->otrosdatos = @$otrosdatos;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegForoInteraccion->__set('idforo', $_REQUEST['idforo']);
			$res = $this->oNegForoInteraccion->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegForoInteraccion->setCampo($_REQUEST['idforo'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
