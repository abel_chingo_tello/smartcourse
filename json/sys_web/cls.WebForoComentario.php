<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020 
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegForoComentario', RUTA_BASE);
class WebForoComentario extends JrWeb
{
	private $oNegForoComentario;

	public function __construct()
	{
		parent::__construct();
		$this->oNegForoComentario = new NegForoComentario;
	}

	public function defecto()
	{
		return $this->listado();
	}


	public function listado()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Foros', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idforo"]) && @$_REQUEST["idforo"] != '') $filtros["idforo"] = $_REQUEST["idforo"];
			if (isset($_REQUEST["titulo"]) && @$_REQUEST["titulo"] != '') $filtros["titulo"] = $_REQUEST["titulo"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["idusuario"]) && @$_REQUEST["idusuario"] != '') $filtros["idusuario"] = $_REQUEST["idusuario"];
			if (isset($_REQUEST["rol"]) && @$_REQUEST["rol"] != '') $filtros["rol"] = $_REQUEST["rol"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["idforopadre"]) && @$_REQUEST["idforopadre"] != '') $filtros["idforopadre"] = $_REQUEST["idforopadre"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["regfecha"]) && @$_REQUEST["regfecha"] != '') $filtros["regfecha"] = $_REQUEST["regfecha"];
			if (isset($_REQUEST["otrosdatos"]) && @$_REQUEST["otrosdatos"] != '') $filtros["otrosdatos"] = $_REQUEST["otrosdatos"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];

			$this->datos = $this->oNegForoComentario->listar();

			echo json_encode(array('code' => 200, 'data' => $this->datos));
			
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function buscar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Foros', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idpublicacion"]) && @$_REQUEST["idpublicacion"] != '') $filtros["idpublicacion"] = $_REQUEST["idpublicacion"];
			if (isset($_REQUEST["titulo"]) && @$_REQUEST["titulo"] != '') $filtros["titulo"] = $_REQUEST["titulo"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["idusuario"]) && @$_REQUEST["idusuario"] != '') $filtros["idusuario"] = $_REQUEST["idusuario"];
			if (isset($_REQUEST["rol"]) && @$_REQUEST["rol"] != '') $filtros["rol"] = $_REQUEST["rol"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["idforopadre"]) && @$_REQUEST["idforopadre"] != '') $filtros["idforopadre"] = $_REQUEST["idforopadre"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["regfecha"]) && @$_REQUEST["regfecha"] != '') $filtros["regfecha"] = $_REQUEST["regfecha"];
			if (isset($_REQUEST["otrosdatos"]) && @$_REQUEST["otrosdatos"] != '') $filtros["otrosdatos"] = $_REQUEST["otrosdatos"];

			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];

			$this->datos = $this->oNegForoComentario->buscar($filtros);

			echo json_encode(array('code' => 200, 'data' => $this->datos));
			
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}
	public function guardar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$comentario = json_decode(file_get_contents("php://input"), true);
			if (json_last_error() != 0) { //hubo error al decodificar el json
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
			}
			// if (empty($_POST)) {
			// 	echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
			// 	exit(0);
			// }
			// @extract($_POST);
			$accion = '_add';
			// if (!empty(@$idforo)) {
			// 	$this->oNegForoComentario->idforo = $idforo;
			// 	$accion = '_edit';
			// }
			$usuarioAct = NegSesion::getUsuario();
			$this->oNegForoComentario->contenido = $comentario['contenido'];
			$this->oNegForoComentario->idpublicacion = $comentario['idpublicacion'];
			$this->oNegForoComentario->idcomentariopadre = $comentario['idcomentariopadre']==null?0:$comentario['idcomentariopadre'];
			$this->oNegForoComentario->idpersona = $usuarioAct['idpersona'];

			if ($accion == '_add') {
				$res = $this->oNegForoComentario->agregar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Foros')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $res));
			} else {
				$res = $this->oNegForoComentario->editar();
				echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Foros')) . ' ' . JrTexto::_('update successfully'), 'newid' => $res));
			}

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function setNota()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$filtros = array();
			$filtros["nota"] = $_REQUEST["nota"];
			$filtros["idComent"] = $_REQUEST["idComent"];
			$this->datos = $this->oNegForoComentario->setNota($filtros);
			echo json_encode(array('code' => 200, 'msj' => 'Se registro correctamente', 'datos' => $this->datos));

			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function importar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}

			@extract($_POST);
			$jsondata = json_decode($datajson);
			$usuarioAct = NegSesion::getUsuario();
			if (!empty($jsondata)) {
				$this->datos = array();
				$j = 1;
				foreach ($jsondata as $v) { // falta fotas las validaciones
					$this->oNegForoComentario->titulo = @$titulo;
					$this->oNegForoComentario->texto = @$texto;
					$this->oNegForoComentario->estado = @$estado;
					$this->oNegForoComentario->idusuario = @$idusuario;
					$this->oNegForoComentario->rol = @$rol;
					$this->oNegForoComentario->idcurso = @$idcurso;
					$this->oNegForoComentario->idgrupoaula = @$idgrupoaula;
					$this->oNegForoComentario->idgrupoauladetalle = @$idgrupoauladetalle;
					$this->oNegForoComentario->idforopadre = @$idforopadre;
					$this->oNegForoComentario->tipo = @$tipo;
					$this->oNegForoComentario->regfecha = @$regfecha;
					$this->oNegForoComentario->otrosdatos = @$otrosdatos;
					$newid = $this->oNegUgel->agregar();
					$this->datos[$j] = array('code' => 200, 'id' => $newid, 'msj' => JrTexto::_('saved successfully'), 'data' => $v);
				}
			}
			echo json_encode(array('code' => 200, 'msj' => ucfirst(JrTexto::_('Min_dre')) . ' ' . JrTexto::_('update successfully'), 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function eliminar()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}

			$this->oNegForoComentario->__set('idforo', $_REQUEST['idforo']);
			$res = $this->oNegForoComentario->eliminar();
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Delete Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo()
	{
		try {
			if (empty($_REQUEST)) {
				echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
				exit(0);
			}
			$this->oNegForoComentario->setCampo($_REQUEST['idforo'], $_REQUEST['campo'], $_REQUEST['valor']);
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('update Record successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
			exit(0);
		}
	}
}
