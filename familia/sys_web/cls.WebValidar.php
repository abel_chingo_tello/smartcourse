<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebValidar extends JrWeb
{
	private $oNegPersonal;

	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
	}

	public function defecto(){
		try{
			global $aplicacion;
			$this->documento->plantilla =!empty($_REQUEST["tpl"])?$_REQUEST["tpl"]:'general';
			$this->documento->setTitulo("Seguimiento familiar", false);
			$this->esquema = 'home/index';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function apoderado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$idproyecto = $_REQUEST["idproyecto"];
			$idempresa = $_REQUEST["idempresa"];
			$filtros = array(
				"tipodoc" => $_REQUEST["tipodoc"],
				"dni" => $_REQUEST["dni"],
				"ape_paterno" => $_REQUEST["codigo"],
				"idproyecto" => $idproyecto,
				"idempresa" => $idempresa,
				"idrol" => 3
			);
			$personal = $this->oNegPersonal->webService($filtros);
			
			if(count($personal) > 0){
				$personal = $personal[0];
				// var_dump($personal);
				$idpersona = $personal["idpersona"];
				$datos = '<iframe class="iframecontent" src="'.URL_BASE.'familia/reportes?plt=sintop&idcategoria=0&idproyecto='.$idproyecto.'&idempresa='.$idempresa.'&idpersona='.$idpersona.'&webservice=1&_u='.$personal['usuario'].'&_c='.$personal['clave'].'" frameborder="0"></iframe>';
			} else {
				$datos = '<div class="rpt-text my-font">Los Datos Ingresados Son Incorrectos ...</div>';
			}
			echo json_encode(array('code'=>200,'data'=>$datos));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}


	public function tipodoc(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$POSTDATA = array();
			$TARGET = _URL_SKS_."/json/tipo_documento";
			$r2 = NegTools::requestHTTP($POSTDATA, $TARGET);
			if(empty($r2) && $r2['code'] != 200){
				throw new Exception("Ocurrio algo en la peticion");
			}
			$datos = $r2["data"];
			echo json_encode(array('code'=>200,'data'=>$datos));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}
	
}