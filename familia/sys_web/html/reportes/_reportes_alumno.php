<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>
<style>
  .x_panel{
    margin-top: 50px;
  }
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2 class="titulo-p"><?php echo JrTexto::_("Reports"); ?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content x_content_2">
        <div class="col-md-12 col-sm-12 text-center listRep"></div>
      </div>
    </div>
  </div>
</div>
<script>
  // addWidgetPanel({
  //   contentClass: 'listRep',
  //   link: _sysUrlBase_+"familia/reportes/_notas/?idalumno=<?php //echo $this->idpersona; ?>&idproyecto=<?php //echo $this->idproyecto; ?>&idempresa=<?php //echo $this->idempresa; ?>&skiplogin=1&_user=<?php //echo $this->usuario ?>&_clave=<?php //echo $this->clave ?>&_dir=reportes/_notas",
  //   icono: "fa-flag",
  //   titulo: "<?php //echo JrTexto::_("My records"); ?>"
  // });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"familia/reportes/_reporteNotas/?idalumno=<?php echo $this->idpersona; ?>&idproyecto=<?php echo $this->idproyecto; ?>&idempresa=<?php echo $this->idempresa; ?>&skiplogin=1&_user=<?php echo $this->usuario ?>&_clave=<?php echo $this->clave ?>&_dir=reportes/reporteNotas",
    icono: "fa-flag",
    titulo: "<?php echo JrTexto::_("My records"); ?>"
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"familia/reportes/_tiempo/?idalumno=<?php echo $this->idpersona; ?>&idproyecto=<?php echo $this->idproyecto; ?>&idempresa=<?php echo $this->idempresa; ?>&skiplogin=1&_user=<?php echo $this->usuario ?>&_clave=<?php echo $this->clave ?>&_dir=reportes/_tiempo",
    icono: "fa-clock-o",
    titulo: "<?php echo JrTexto::_("Study Time"); ?>",
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"familia/reportes/_progreso/?idalumno=<?php echo $this->idpersona; ?>&idproyecto=<?php echo $this->idproyecto; ?>&idempresa=<?php echo $this->idempresa; ?>&skiplogin=1&_user=<?php echo $this->usuario ?>&_clave=<?php echo $this->clave ?>&_dir=reportes/_progreso",
    icono: "fa-tasks",
    titulo: "<?php echo JrTexto::_("My Progress"); ?>",
    //ribbon: true
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"familia/reportes/_tareasProyectos/?tipo=T&idalumno=<?php echo $this->idpersona; ?>&idproyecto=<?php echo $this->idproyecto; ?>&idempresa=<?php echo $this->idempresa; ?>",
    icono: "fa-sticky-note",
    titulo: "<?php echo JrTexto::_("Tareas"); ?>",
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"familia/reportes/_tareasProyectos/?tipo=P&idalumno=<?php echo $this->idpersona; ?>&idproyecto=<?php echo $this->idproyecto; ?>&idempresa=<?php echo $this->idempresa; ?>",
    icono: "fa-book",
    titulo: "<?php echo JrTexto::_("Proyectos"); ?>",
  });

</script>