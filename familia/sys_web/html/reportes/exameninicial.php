<style css>
.cajaselect {  
    overflow: hidden; width: 230px; position:relative; font-size: 1.8em;
}
select:focus{ outline: none;}
.cajaselect::after{ font-family: FontAwesome; content: "\f0dd"; display: inline-block; text-align: center; width: 30px; height: 100%; background-color: #4683af; position: absolute; top: 0; right: 0px; pointer-events: none; color: antiquewhite; bottom: 0px; }
.select-ctrl-wrapper:after{ right:0!important; max-width:100% }
.select-ctrl{ max-width:100% }
.padding-10{ padding:10px; }
.padding-15{ padding:15px; }
.btn-preview { border-radius: 50%; padding:5px 10px; font-size:small; }
</style>
<div class="padding-15"></div>
<!--START PANEL -->
<div class="panel panel-primary">
    <div class="panel-heading">
        <h5>Reporte - Exámenes Iniciales</h5>
    </div>
    <!--START PANEL ROW-->
    <div class="panel-body row">
        <!--START FILTERS-->
        <div class="col-lg-3 col-md-3">
            <div style="display:block;width:100%; margin:10px 0;">
                <div><p style="font-size:large; display:inline-block; margin:0; padding-right:15px;">Tipo:</p></div>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="select_viewGeneral" id="select_viewTipos" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
                        <option value="">todos</option>
                        <?php if(!empty($this->fktipos)) foreach ($this->fktipos as $fk) { ?>
                            <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fktipo?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div style="display:block;width:100%; margin:10px 0;">
                <div><p style="font-size:large; display:inline-block; margin:0; padding-right:15px;">Grupos/Áreas:</p></div>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="select_viewGeneral" id="select_viewGrupos" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
                        <?php if(!empty($this->gruposaula)) foreach ($this->gruposaula as $fk) { ?>
                            <option value="<?php echo $fk["idgrupoaula"]?>" <?php echo $fk["idgrupoaula"]==@$this->fkgrupoaula?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                        <?php }else{ ?>
                            <option value="0">No hay grupos/áreas matriculados</option>
                        <?php }?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-5">
            <div style="display:block;width:100%; margin:10px 0;">
                <div><p style="font-size:large; display:inline-block; margin:0; padding-right:15px;">Cursos:</p></div>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="select_viewGeneral" id="select_viewCursos" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
                        <?php 
                            if(empty($this->cursos)){
                                echo '<option value="0">No hay cursos matriculados</option>';
                            }else{
                                foreach($this->cursos as $key => $value){
                                    echo "<option value='{$value['idcurso']}'>{$value['nombre']}</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1">
            <br>
            <button type="button" class="btn btn-default" id="cleandate">Limpiar</button>
        </div>
        <div class="col-lg-3 col-md-3">
            <div><p style="font-size:large; display:inline-block; margin:0; padding-right:15px;">Desde:</p></div>
            <input type="text"  class="form-control" id="datepicker01" />
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="form-group">
                <div><p style="font-size:large; display:inline-block; margin:0; padding-right:15px;">Hasta:</p></div>
                <input type="text" class="form-control" id="datepicker02" />
            </div>
        </div>
        <div class="col-lg-1 col-md-1">
            <br>
            <button type="button" class="btn btn-primary" id="findResult">Buscar</button>
        </div>
        <div class="col-lg-1 col-md-1" id="sendReport-container">
            <br>
            <button type="button" class="btn btn-success" id="sendReport">Enviar reporte por correo</button>
        </div>
        <!--END FILTERS-->
        <div class="col-lg-12 col-md-12">
            <div class="padding-10" style="border-bottom:1px solid gray;"></div>
            <div class="padding-10" ></div>
            <div id="table_container">
                <table id="tableview" class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Nota</th>
                            <th>Ver examen</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--content-->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <!--END PANEL ROW-->
</div>
<!--END PANEL-->
<!-- Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!--contenido-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var idproyecto = <?php echo $this->user['idproyecto']; ?>;
var valempresa = <?php echo $this->user['idempresa']; ?>;
var objAlumnos = null;
var path = new Array();

function dibujarDatatable(tablename){
    if ($.fn.DataTable.isDataTable(tablename)) {
        $(tablename).DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $(tablename).DataTable();
}
function refreshTable(tablename,data){
    var filas = '';
    var tabla_tmp = $('<table></table>');
    tabla_tmp.attr('id',$(tablename).attr('id'));
    tabla_tmp.attr('class',$(tablename).attr('class'));
    tabla_tmp.append('<thead>'+$(tablename).find('thead').html()+'</thead><tbody>'+data+'</tbody>');
    if($('body').find(tablename+'_wrapper').length > 0){
        var contenedor = $(tablename+'_wrapper').parent();
        $(tablename+'_wrapper').remove();
        contenedor.append(tabla_tmp);
        dibujarDatatable(tablename);
    }
    return true;
}
function loadCursos(){
    var tipoVal = $('#select_viewTipos').val();
    var idgrupoaula = $('#select_viewGrupos').val();
    $.ajax({
        url: _sysUrlBase_ + '/acad_grupoauladetalle/buscarjson',
        type: 'POST',
        dataType: 'json',
        data: {'tipo':tipoVal,'idgrupoaula':idgrupoaula, 'idproyecto':idproyecto}
    }).done(function(resp){
        if(resp.code == "ok"){
            $('#select_viewCursos').html(' ');
            if(resp.data != '' && resp.data != null){
                $('#select_viewCursos').append('<option value="0">Seleccionar curso</option>');
                for(value of resp.data){
                    $('#select_viewCursos').append('<option value="'+value.idcurso+'">'+value.strcurso+'</option>');
                }
            }else{
                $('#select_viewCursos').append('<option value="0">No hay cursos matriculados</option>');
            }
            $('#select_viewCursos').css('max-width','100%');
            $('#select_viewCursos').parent().css('max-width','100%');
        }else{
            alert(resp.msj);
        }
    }).fail(function(xhr, textStatus,errorThrow){
        alert(xhr.responseText);
    });
}
function loadGrupos(){
    var tipoVal = $('#select_viewTipos').val();
    $.ajax({
        url: _sysUrlBase_ + '/acad_grupoaula/buscarjson',
        type: 'POST',
        dataType: 'json',
        data: {'tipo':tipoVal, 'idproyecto':idproyecto}
    }).done(function(resp){
        if(resp.code == "ok"){
            $('#select_viewGrupos').html(' ');
            if(resp.data != '' && resp.data != null){
                $('#select_viewGrupos').append('<option value="0">Seleccionar grupo</option>');
                for(value of resp.data){
                    $('#select_viewGrupos').append('<option value="'+value.idgrupoaula+'">'+value.nombre+'</option>');
                }
            }else{
                $('#select_viewGrupos').append('<option value="0">No hay grupos/áreas matriculados</option>');
            }
            $('#select_viewCursos').css('max-width','100%');
            $('#select_viewCursos').parent().css('max-width','100%');
        }else{
            alert(resp.msj);
        }
    }).fail(function(xhr, textStatus,errorThrow){
        alert(xhr.responseText);
    });
}
function loadAlumnos(){
    var idgrupoaula = $('#select_viewGrupos').val();
    var idcurso = $('#select_viewCursos').val();
    $.ajax({
        url: _sysUrlBase_ + '/reportes/alumnosjson',
        type: 'POST',
        dataType: 'json',
        data: {'idgrupoaula':idgrupoaula,'idcurso' :idcurso,'idproyecto':idproyecto,'fecha1': $('#datepicker01').val(),'fecha2': $('#datepicker02').val() }
    }).done(function(resp){
        if(resp.code == "ok"){
            if(resp.data != '' && resp.data != null){
                $('#sendReport-container').show();
                objAlumnos = resp.data;
                var filas = '';
                var i = 1;
                for(valor of resp.data){
                    var boton = '<button type="button" class="btn btn-secondary btn-preview disabled" ><i class="fa fa-eye-slash" aria-hidden="true"></i></button>';
                    if(valor.entrada != null && parseFloat(valor.entrada.nota) > 0){
                        boton = '<button type="button" class="btn btn-primary btn-preview" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye" aria-hidden="true"></i></button>';
                    }
                    filas = filas.concat('<tr data-vector="'+(i-1)+'"><td>'+i+'</td><td>'+valor.nombre+'</td><td>'+(parseFloat(valor.entrada.nota) * 0.20).toFixed(2)+' pts / 20 pts</td><td>'+boton+'</td></tr>');
                    i++;
                }
                // do it
                refreshTable('#tableview',filas);
            }else{
                $('#sendReport-container').hide();
                refreshTable('#tableview','');
            }
        }else{
            alert(resp.message);
        }
    }).fail(function(xhr, textStatus,errorThrow){
        alert(xhr.responseText);
    });
}
function getTextSelect(obj, value){
    var text = '';
    obj.find('option').each(function(e){
        if($(this).attr('value') == value){
            text = $(this).text(); 
            return;
        }
    });
    return text;
}
function _sendemail(_path){
    var datos = {
        'empresa': valempresa,
        'filespath' : _path,
        'asunto': 'Informe de un reporte',
        'nameReporte': getTextSelect($('#select_viewCursos'), $('#select_viewCursos').val())
    }
    $.ajax({
        url: _sysUrlBase_ + '/sendemail/reporteasocios', type: 'POST', dataType: 'json', data: datos
    }).done(function(resp){
        if(resp.code == "ok"){
            alert("Se envio el informe correctamente");
        }else{
            alert(resp.message);
        }
    }).fail(function(xhr, textStatus,errorThrow){
        alert(xhr.responseText);
    });
}
function generarXLS(datos){

}
function sendemail(){
    var idgrupoaula = $('#select_viewGrupos').val();
    var idcurso = $('#select_viewCursos').val();
    var datos = {'type': 'entrada','idgrupoaula':idgrupoaula,'idcurso' :idcurso,'idproyecto':idproyecto,'fecha1': $('#datepicker01').val(),'fecha2': $('#datepicker02').val() };
    path = null; 
    path = new Array();
    $.ajax({
        url: _sysUrlBase_ + '/reportes/alumnospdffile',
        type: 'POST',
        dataType: 'json',
        data: datos
    }).done(function(resp){
        if(resp.code == "ok"){
            if(resp.data != '' && resp.data != null){
                // generarXLS(datos);
                path.push(resp.data);
                _sendemail(path);
            }
        }else{
            alert(resp.message);
        }
    }).fail(function(xhr, textStatus,errorThrow){
        alert(xhr.responseText);
    });
}

$(document).ready(function(){
    $('#sendReport-container').hide();

    $('#datepicker01').datetimepicker({ format:'YYYY-MM-DD' });
    $('#datepicker02').datetimepicker({useCurrent: false, format: 'YYYY-MM-DD'});
    $("#datepicker01").on("dp.change", function (e) {
        $('#datepicker02').data("DateTimePicker").minDate(e.date);
    });
    $("#datepicker02").on("dp.change", function (e) {
        $('#datepicker01').data("DateTimePicker").maxDate(e.date);
    });
    // $('#tableview').dataTable();
    dibujarDatatable('#tableview');

    //llenar el filtro de cursos
    loadCursos();
    $('#select_viewTipos').change(function(){
        loadGrupos();
        $('#select_viewGrupos').trigger('change');
    });
    $('#select_viewGrupos').change(function(){
        loadCursos();
    });
    $('#select_viewCursos').change(function(){
        loadAlumnos();
    });
    $('#findResult').on('click',function(){
        loadAlumnos();
    });
    $('#sendReport').on('click',function(){
        sendemail();
    });
    $('#cleandate').on('click',function(){
        $('#datepicker01').val('');
        $('#datepicker02').val('');
    });
    $('body').on('click','.btn-preview',function(){
        if(objAlumnos != null){
            var index = parseInt($(this).closest('tr').data('vector'));
            var modal = $('#viewModal');
            var modalBody = modal.find('.modal-body');
            var previewExamen = '';
            var json = JSON.parse(objAlumnos[index].entrada.preguntas);

            for(var i = 0; i < Object.keys(json).length; i++){
                previewExamen = previewExamen.concat(json[i]);
            }
            modalBody.html('<div style="">'+previewExamen+'</div>');
        }
    });
});
</script>