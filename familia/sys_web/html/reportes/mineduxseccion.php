<!-- <?php var_dump($this->ubicaciones_filtros['ugel']); ?> -->
<style>
.container-select{ display:inline-block; }
select.select-ctrl{ padding-right: 35px; }
.select-ctrl-wrapper:after{
  right:0!important;
}
.boxselect{ background-color:#ffffff; border-radius:1em;border:2px solid #4683af; padding:7px 10px; }
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}
.divisor{text-align: center;}
.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<div  id="container-report" style="margin:0!important">
    <div class="row">
        <!--Combobox select-->
        <div class="col-md-12 boxselect">
            <div class="divisor">
                <div class="container-select">
                    <h4 style="display:inline-block;"><?php echo JrTexto::_('DRE');?></h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="iddre" id="iddre" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                            <?php
                                if(!empty($this->ubicaciones_filtros['departamentos'])){
                                    if(count($this->ubicaciones_filtros['departamentos']) > 1){
                                        foreach($this->ubicaciones_filtros['departamentos'] as $value){
                                            echo '<option value="'.$value['departamento'].'">'.$value['ciudad'].'</option>';
                                        }
                                    }else{
                                        echo '<option value="'.$this->ubicaciones_filtros['departamentos'][0]['departamento'].'" selected>'.$this->ubicaciones_filtros['departamentos'][0]['ciudad'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="container-select">
                    <h4 style="display:inline-block;"><?php echo JrTexto::_('UGEL');?></h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="idugel" id="idugel" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                            <?php
                                if(!empty($this->ubicaciones_filtros['ugel'])){
                                    if(count($this->ubicaciones_filtros['ugel']) > 1){
                                        foreach($this->ubicaciones_filtros['ugel'] as $value){
                                            echo '<option value="'.$value['idugel'].'">'.$value['descripcion'].'</option>';
                                        }
                                    }else{
                                        echo '<option value="'.$this->ubicaciones_filtros['ugel'][0]['idugel'].'" selected>'.$this->ubicaciones_filtros['ugel'][0]['ciudad'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="container-select">
                    <h4 style="display:inline-block;"><?php echo JrTexto::_('School');?></h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="idcolegio" id="idcolegio" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="container-select">
                    <h4 style="display:inline-block;"><?php echo JrTexto::_('Course'); ?></h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="idcurso" id="idcurso" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                        </select>
                    </div>
                </div>
            
            </div>
            <div class="divisor">
                
                <div class="container-select">
                    <h4 style="display:inline-block;"><?php echo JrTexto::_('Grades'); ?></h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="idgrados" id="idgrados" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                            <?php
                                if(!empty($this->grados)){
                                    foreach($this->grados as $grado){
                                        echo '<option value="'.$grado["idgrado"].'">'.$grado["descripcion"].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="container-select">
                    <label><input type="checkbox" id="checkbox_teacher" /><?php echo JrTexto::_('All teachers'); ?></label><br>
                    <h4 style="display:inline-block;"><?php echo JrTexto::_('Teacher'); ?></h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="iddocente" id="iddocente" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="container-select">
                    <label><input type="checkbox" id="checkbox_section" /><?php echo JrTexto::_('All sections'); ?></label><br>
                    <h4 style="display:inline-block;"><?php echo JrTexto::_('Section'); ?></h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="idseccion" id="idseccion" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="padding:10px 0;"></div>
            <button class="btn btn-primary" style="display:block; width:50%; margin:0 auto;" id="generar" type="button">Generar Reporte</button>
        </div>
        <div class="col-xs-12">
            <div style="padding:10px 0;"></div>
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="t_resultseccion" class="table">
                            <thead>
                                <th>columna1</th>
                                <th>columna2</th>
                                <th>columna3</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var rol = <?php echo $this->user['idrol'] ?>;
var iiee = null;
var ubicaciones_filtros = <?php echo json_encode($this->ubicaciones_filtros) ?>;
var cursos = <?php echo (!empty($this->cursosProyecto)) ? json_encode($this->cursosProyecto) : 'null' ?>;
var seccion = <?php echo (!empty($this->seccion)) ? json_encode($this->seccion) : 'null' ?>;
// var grados = <?php echo (!empty($this->grados)) ? json_encode($this->grados) : 'null' ?>
var docentes = null;

function dibujarDatatable(tablename){
    if ($.fn.DataTable.isDataTable(tablename)) {
        $(tablename).DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $(tablename).DataTable({
        "pageLength": 50,
        "searching": false,
        "processing": false
    });
}

Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});

console.log(ubicaciones_filtros);
var nombreselect = function(obj,value){
  var resultado = '';
  $(obj).find('option').each(function(k,v){
    if($(this).attr('value') == value){
      resultado = $(this).text();
    }
  });
  return resultado;
};

function loadIIEE(idrol = null){
    var datos = new Object;
    datos.idproyecto = 3;
    if(idrol == 8){
        //codigo ubigeo
        if(Object.keys(ubicaciones_filtros.departamentos).length > 0 && ubicaciones_filtros.departamentos != null){
            datos.idubigeo = ubicaciones_filtros.departamentos[0].id_ubigeo;
        }
    }
    if(idrol == 7){
        //id ugel
        if(Object.keys(ubicaciones_filtros.ugel).length > 0 && ubicaciones_filtros.ugel != null){
            datos.idugel = ubicaciones_filtros.ugel[0].idugel;
        }
    }
    if(idrol == 6){
        //id local
    }

    $.ajax({
        url: _sysUrlBase_+'/reportes/listarinstituciones',
        type: 'POST',
        dataType: 'json',
        data: datos,
    }).done(function(resp) {
        if(resp.code=='ok'){
            if(Object.keys(resp.data).length > 0){
                iiee = resp.data;
            }else{
                $('#iiee').html('<option value="0">Sin datos</option>');
            }
            // console.log(iiee);
        }else{
            return false;
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
}
function getsecciondocente(ide,obj){
    var resultado = null;
    if(obj != null & typeof obj == 'object'){
        for(var i in obj){
            if(obj[i].id == ide){
                resultado = obj[i].secciones;
                break;
            }
        }
    }
    return resultado;
}

function showresult(d,t){
    var _url = _sysUrlBase_+'/reportes/minedu_ubicacion_seccion_json';
    $.ajax({
        url: _url,
        type: 'POST',
        dataType: 'json',
        data: d,
    }).done(function(resp) {
        if(resp.code=='ok'){
            if(Object.keys(resp.data).length > 0){
                //mostrar resultados
            }else{
                //refrescar tabla
            }
            // console.log(iiee);
        }else{
            return false;
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
}

function showalert(){
    $.alert({
        icon: 'fa fa-warning',
        title: 'Error!',
        content: 'Faltan campos obligatorios (*)',
        confirmButton: 'Ok',
        // cancelButton: 'Cerrar',
        theme: 'light',
        typeAnimated: true,
        confirmButtonClass: 'btn-danger',
    });
}

$('document').ready(function(){
    loadIIEE(rol);
    dibujarDatatable('#t_resultseccion');
    $('#iddre').change(function(){
        if($(this).val() != 0){
            $('#idugel').html('<option value="0">\<?php echo JrTexto::_("Select") ?></option>');
            if(ubicaciones_filtros.ugel != null){
                for(var i in ubicaciones_filtros.ugel){
                    if(typeof ubicaciones_filtros.ugel[i] == 'object'){
                        if(ubicaciones_filtros.ugel[i].iddepartamento.substring(0,2) == $(this).val()){
                            $('#idugel').append('<option value="' + ubicaciones_filtros.ugel[i].idugel +'">' + ubicaciones_filtros.ugel[i].descripcion +'</option>');
                        }
                    }
                }//end for
            }//endif ubicaciones_filtros.ugel
        }
    });

    $('#idugel').change(function(){
        if($(this).val() != 0){
            $('#idcolegio').html('<option value="0">\<?php echo JrTexto::_("Select") ?></option>');
            if(iiee != null){
                for(var i in iiee){
                    if(typeof iiee[i] == 'object'){
                        if(iiee[i].idugel == $(this).val()){
                            $('#idcolegio').append('<option value="' + iiee[i].idlocal +'">' + iiee[i].direccion +'</option>');
                        }
                    }
                }//end for
            }
        }
    });
    $('#idcolegio').change(function(){
        if($(this).val() != 0 ){
            $('#idcurso').html('<option value="0">\<?php echo JrTexto::_("Select") ?></option>');
            if(cursos != null){
                for(var i in cursos){
                    if(typeof cursos[i] == 'object'){
                        $('#idcurso').append('<option value="' + cursos[i].idcurso +'">' + cursos[i].nombre +'</option>');
                    }
                }//end for
            }
            $.ajax({
                url: _sysUrlBase_+'/reportes/getdocentexcurso',
                type: 'POST',
                dataType: 'json',
                data: {'idlocal' : $(this).val()},
            }).done(function(resp) {
                if(resp.code=='ok'){
                    if(Object.keys(resp.data).length > 0){
                        docentes = resp.data;
                    }
                }else{
                    return false;
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
        }else{
            $('#idcurso').html('<option value="0">\<?php echo JrTexto::_("Select") ?></option>');
        }
    });

    $('#idcurso').change(function(){
        $('#idgrados').val(0);
    });
    $('#idgrados').change(function(){
        var idcurso = $('#idcurso').val();
        var idgrado = $(this).val();
        if( idgrado != 0 && idcurso != 0){
            if(docentes != null){
                var obj = $('#iddocente');
                // console.log(docentes);
                obj.html('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
                for(var i in docentes[idcurso][idgrado]){
                    if(typeof docentes[idcurso][idgrado][i] == 'object'){
                        var result = docentes[idcurso][idgrado][i];
                        obj.append('<option value="' + docentes[idcurso][idgrado][i].id +'">' + docentes[idcurso][idgrado][i].nombre +'</option>');
                    }
                }//end for
            }//end if docentes != null
        }
    });
    $('#iddocente').change(function(){
        // console.log(seccion);
        var idcurso = $('#idcurso').val();
        var idgrado = $('#idgrados').val();
        var iddocente = $('#iddocente').val();
        var objseccion = $('#idseccion');
        if(iddocente != 0 && idgrado != 0 && idcurso != 0 ){
            objseccion.html('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
            if(seccion != null){
                var secciones_docente = getsecciondocente(iddocente,docentes[idcurso][idgrado]);
                // console.log(secciones_docente);
                if(secciones_docente != null){
                    for(var i in secciones_docente){
                        for(var j = 0; j < Object.keys(seccion).length; j++){
                            if(seccion[j].idsesion == i){
                                objseccion.append('<option value="'+ secciones_docente[i] +'">'+seccion[j].descripcion+'</option>');
                                break;
                            }//end if search
                        }//end for
                    }// end for secciones_docente
                }
            }
        }
    });
    $('#checkbox_section').change(function(){
        if($(this).is(":checked") == true){
            $('#idseccion').attr('disabled',true);
        }else{
            $('#idseccion').attr('disabled',false);
        }
    });

    $('#checkbox_teacher').change(function(){
        if($('#checkbox_teacher').is(':checked') == true){
            $('#iddocente').prop('disabled',true);
            $('#iddocente').val(0);
            if(seccion != null){
                $('#idseccion').html('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
                for(var j = 0; j < Object.keys(seccion).length; j++){
                    $('#idseccion').append('<option value="'+ seccion[j].idsesion +'">'+seccion[j].descripcion+'</option>');
                }//end for
            }
        }else{
            $('#iddocente').prop('disabled',false);
            if($('#checkbox_section').is(':checked') == false){
                $('#idseccion').html('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
            }
        }
    });
    /**CLICK EVENTS */
    $('#generar').on('click',function(){
        var idlocal = $('#idcolegio').val();
        var idcurso = $('#idcurso').val();
        var idgrado = $('#idgrados').val();
        var iddocente = $('#iddocente').val();
        var idseccion = $('#idseccion').val();
        var checkseccion = $('#checkbox_section').is(':checked');
        var checkdocente = $('#checkbox_teacher').is(':checked');
        var datos = new Object;

        if(idlocal != 0){
            datos.local = idlocal;
        }
        if(idgrado != 0){
            datos.grado = idgrado;
        }
        if(idcurso != 0){
            datos.curso = idcurso;
        }
        
        if(checkdocente == false && iddocente != 0){
            datos.docente = iddocente;
        }
        if(checkseccion == false && idseccion != 0){
            datos.idgrupoauladetalle = idseccion;
        }
        if(checkseccion == true && idseccion != 0){
            datos.seccion = idseccion;
        }

            // if(idseccion != 0){
        //         datos.idgrupoauladetalle = idseccion;
        //     }
        // }
        // }else{
        //     if(idlocal!=0 && idcurso!=0 && idgrado!=0 && iddocente != 0){
        //         datos.idlocal = idlocal;
        //         datos.idcurso = idcurso;
        //         datos.idgrado = idgrado;
        //         datos.iddocente = iddocente;
        //     }
        // }
        var consultar = true;
        if(Object.keys(datos).length > 0){
            if(checkseccion == true){
                if(checkdocente == false && iddocente == 0){
                    showalert();
                    consultar = false;
                }else if(checkdocente == false && idgrado == 0 && idcurso == 0){
                    showalert();
                    consultar = false;
                }
            }else{
                if(idseccion == 0){
                    showalert();
                    consultar = false;
                }
            }
            if(consultar == true){
                alert("consultar");
                //showresult(datos,"#t_resultseccion");
            }
        }else{
            //mostrar alerta
            showalert();
        }
    });
});

</script>