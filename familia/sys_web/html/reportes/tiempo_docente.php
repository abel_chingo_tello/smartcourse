<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Time"); ?>'
  });
</script>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
          <div class="row">
              <div class="col-md-6">
                  <div class="btn-group">                     
                      <button type="button" class="btn btn-xs btn-success btnexportar_xlsx"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx</button>                     
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF</button>
                     
                      <button type="button" class="btn btn-xs btn-warning btnimprimir_2"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?></button>
                  </div>
              </div>
              <div class="col-md-6 text-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                      <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="area_char"><i class="fa fa-area-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar_char" ><i class="fa fa-bar-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="line_char" ><i class="fa fa-line-chart"></i></button>
                  </div>
              </div>
          </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_title">
      <div class="row">
            <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2" >
                <label class="control-label"><?php echo JrTexto::_('Study Groups'); ?></label>
                <select id="idgrupoaula" name="idgrupoaula" class="form-control " data-size="10">
                  <?php 
                  $haygrupo=array();
                  if(!empty($this->gruposauladetalle))
                  foreach ($this->gruposauladetalle as $fk){
                    if(!in_array($fk["idgrupoaula"], $haygrupo)){
                        $haygrupo[]=$fk["idgrupoaula"];  ?>
                        <option  value="<?php echo $fk["idgrupoaula"]?>" <?php echo $this->idgrupoaula==$fk["idgrupoaula"]?'selected="selectted"':'';?>><?php echo $fk["strgrupoaula"] ?></option><?php } ?>
                    <?php } ?>
                </select>
            </div>
            <div class=" col-md-3 form-group">
                <label class="control-label"><?php echo JrTexto::_('Course');?> <span class="required"> * </span></label>
                <select id="idcurso" name="idcurso" class="form-control " title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                  <?php if(!empty($this->cursos[$this->idgrupoaula])){
                  foreach ($this->cursos[$this->idgrupoaula] as $fk) { ?>
                    <option  value="<?php echo $fk["idcurso"]?>" idgrupoauladetalle="<?php echo $fk["idgrupoauladetalle"]; ?>" <?php echo @$this->idcurso==$fk["idcurso"]?'selected="selectted"':'';?>><?php echo $fk["strcurso"] ?></option>
              <?php }} ?>
                </select> 
            </div>            
        </div> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="row" id="pnl-panelreporte"  style="display:none">            
        </div>
        <div class="row" id="impresion"  style="display:none">
            <div class="table-responsive">
            </div>
        </div>
        <div id="sindatos" style="display:none">
            <div class="row">
            <div class="container">
                <div class="row">
                    <div class="jumbotron col-md-12 text-center">
                        <h1><strong>Opss!</strong></h1>                
                        <div class="disculpa"><?php echo JrTexto::_('Sorry, You do not have students enrolled')?>:</div>                
                    </div>
                </div>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var __formatotiempo=function(seg){    
    var _f=function(t,d){return t.toString().padStart(d,'0')}
    var _m=function(s){var m_=parseInt(s/60); var s_=s%60; return {'m':_f(m_,2),'s':_f(s_,2)}}
    var _h=function(s){var h_=parseInt(s/3600);var m_=_m(s%3600); m_.h=_f(h_,2); return m_; }
    var _d=function(s){var d_=parseInt(s/86400);var h_=_h(s%86400);h_.d=_f(d_,2); return h_; }
    if(seg<60) return {'s':_f(seg,2)}
    else if(seg<3600) return _m(seg); 
    else if(seg<86400) return _h(seg); 
    else return _d(seg);    
  }
var __formatodhms=function(seg){
    var t=__formatotiempo(seg);
    return (t.d!=undefined?(t.d+'d '):'')+(t.h!=undefined?(t.h+'h '):'')+(t.m!=undefined?(t.m+"m "):'')+t.s+"s";
}
    $(document).ready(function(){
        __logoEmpresaimagebase64();
        google.charts.load('current', {'packages':['corechart']});
        var grupos=<?php echo !empty($this->cursos)?(json_encode($this->cursos).';'):'[];'; ?>

        function charst(tipo,dt,options,idelem){
            var selbase=localStorage.getItem('notabase')||'';                
            var dataexamenes=localStorage.getItem('dataexamenes');
            var dt=JSON.parse(dataexamenes);
            var datos=[];
            var nmaxformula=1;
            var bases=[];
            var data = new google.visualization.DataTable();
            data.addColumn('string', '<?php echo ucfirst(JrTexto::_('Students')); ?>');
            data.addColumn('number', '<?php  echo ucfirst(JrTexto::_('Tiempo(Horas)')); ?>');
            if(dt.length){

                var strcurso=dt[0].strcurso!=undefined?dt[0].strcurso:'';                      
                if(dt.length)
                    $.each(dt,function(i,c){ 
                    console.log(c);                        
                        var tc= __formatotiempo(c.tiempocursos).h||0;
                        console.log(tc);
                        data.addRow([c.stralumno,tc]);
                    })
                    options.title='<?php echo JrTexto::_('Course') ?> : '+strcurso+ ' -  ' +options.title;

                    //console.log(datos);
                    if(tipo=='area') var chart = new google.visualization.AreaChart(document.getElementById(idelem));
                    else if(tipo=='line')var chart = new google.visualization.LineChart(document.getElementById(idelem));
                    else if(tipo=='pie')var chart = new google.visualization.PieChart(document.getElementById(idelem));
                    else var chart = new google.visualization.BarChart(document.getElementById(idelem));
                    chart.draw(data, options);       
                }

            
        }

        var verrptalumno=1;
        localStorage.setItem('rtpalumno','ver_rpt'+verrptalumno);
        var idnumgrafico=0;

        var vistaimpresion=function(){
            var selbase=localStorage.getItem('notabase')||'';                
            var dataexamenes=localStorage.getItem('dataexamenes');
            var dt=JSON.parse(dataexamenes);
            var datos=[];
            var nmaxformula=1;
            var bases=[];
              if(dt.length){

                 var strcurso=dt[0].strcurso!=undefined?dt[0].strcurso:'';   
                    var table='<div class="container"> <div class="col-md-12 table-reponsive"><table id="tableimprimir"  class="table table-striped table-hover"><thead><tr class="headings"><th>#</th>';
                      table+='<th><?php echo JrTexto::_("Course") ;?>: '+strcurso+'</th><th class="text-center">Tiempo de PV</th><th>Tiempo en curso</th></tr></thead><tbody>';
                      total=0;
                      if(dt.length)
                        $.each(dt,function(i,c){
                            table+='<tr><td  style="padding:0.5ex 2ex">'+(i+1)+'</td><td  style="padding:0.5ex 1ex">'+c.stralumno+'</td>';
                            table+='<td style="padding:0.5ex 1ex" class=" text-center">'+(__formatotiempo(c.tiempopv).h||0)+'h</td><td>'+(__formatotiempo(c.tiempocursos).h||0)+'h</td>';
                            table+='</tr>';
                            total++;
                        })
                    table+='<tr><td></td><td colspan="2" class="text-center"><?php echo JrTexto::_("Total of records"); ?> : '+total+'</td><td></td><td style="display:none;"></td></tr></tbody>';
                    table+='</table></div></div>';
                    $('#impresion .table-responsive').html(table);
                    $('#impresion').find('#tableimprimir').DataTable({
                            ordering: false,                        
                            searching: false,
                            paging: false,
                            dom: 'Bfrtip',
                            buttons: [
                              {
                                extend: "excelHtml5",
                                messageTop: '',
                                title:'<?php echo JrTexto::_('Report') ?>',
                                customize: function(xlsx){
                                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                    var col=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                                    $tr=$('#tableimprimir tbody').find('tr');
                                    $.each($tr,function(ir, row){
                                        $(this).find('td').each(function(ic,td){
                                            var xcol=col[ic];
                                            if(ir==0) $('row c[r="'+xcol+'1"]',sheet).attr('s',32);
                                            else{
                                                var s=ir%2==0?8:20;
                                                $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',s);
                                            }   
                                        })
                                    })
                                }
                              },{
                                extend: "pdfHtml5",
                                messageTop: '',
                                title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Course notes')." - ".date('d-m-Y'); ?>',
                                customize: function(doc){
                                    doc.pageMargins = [12,12,12,12];
                                    $tr=$('#tableimprimir tbody').find('tr');
                                    var ntr=$tr.length;
                                    var ntd=$tr.eq(0).find('td').length;
                                    var ncolA=[15,80];
                                    for(i=0;i<ntd-2;i++){
                                        ncolA.push("*");
                                    }
                                    doc.content[1].table.widths = ncolA;
                                    var tblBody = doc.content[1].table.body;
                                    $tr.each(function (ir, row){
                                        var color=((ir%2==0)?'':'#e4e1e1');
                                        if($(this).hasClass('nomcursos')){
                                            $(this).find('td').each(function(ic,td){
                                                tblBody[ir][ic].fillColor='#5c5858';
                                                tblBody[ir][ic].color="#FFFFFF";  
                                            })
                                        }
                                    })                                
                                    tblBody[ntr][0].alignment= 'left'; 
                                    if(_logobase64!=''){
                                        doc.content.splice(0, 0, {
                                            margin: [ 0, 0, 0, 12 ],
                                            alignment: 'center',
                                            image:_logobase64
                                        });
                                    }
                                }

                              },{
                                extend:"print",
                                title: '<?php echo JrTexto::_('Report')." ".JrTexto::_('Course notes')." - ".date('d-m-Y'); ?>',
                                customize: function(win){
                                    var body=$(win.document.body);
                                    var title=body.find('h1').first().text();
                                    if(_logobase64!=''){
                                        body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="'+_logobase64+'"/><br>'+title+'</div>');
                                    }else{
                                         body.find('h1').first().html('<div style="text-align:center; width:100%;">'+title+'</div>');
                                    }
                                    table=body.find('table');
                                    table.find('th').each(function(index){
                                       table.find('th').css('background-color', '#cccccc')
                                    });
                                }
                              }
                            ]                  
                    })                
                
                }else{                
                    $('#impresion .table-responsive tbody').html(tr1);
                    $('#impresion').find('#tableimprimir').DataTable();
                }
        }
        var mostrarresultado=function(){
            var dataexamenes=localStorage.getItem('dataexamenes');
            var vistareporte=localStorage.getItem('vistaReporte1')||$('.changevista.active').attr('vista');
            var selbase=localStorage.getItem('notabase')||'';
            var dt=JSON.parse(dataexamenes);
            var bases=[];
            if(vistareporte=='table'){ 
                    var strcurso=dt[0].strcurso!=undefined?dt[0].strcurso:'';   
                    var table='<div class="container"> <div class="col-md-12 table-reponsive"><table class="table table-striped table-hover"><thead><tr class="headings"><th>#</th>';
                      table+='<th><?php echo JrTexto::_("Course") ;?>: '+strcurso+'</th><th class="text-center">Tiempo de PV</th><th>Tiempo en curso</th></tr></thead><tbody>';
                      if(dt.length)
                        $.each(dt,function(i,c){
                            table+='<tr><td  style="padding:0.5ex 2ex">'+(i+1)+'</td><td  style="padding:0.5ex 1ex">'+c.stralumno+'</td>';                                                     
                            table+='<td style="padding:0.5ex 1ex" class=" text-center">'+(__formatotiempo(c.tiempopv).h||0)+'h</td><td>'+(__formatotiempo(c.tiempocursos).h||0)+'h</td>';    
                            table+='</tr>';
                        })
                    table+='</tbody>';
                    table+='</table></div></div>';
                    $('#pnl-panelreporte').html(table);
                   
                    $('[rel="popover"]').click(function(ev){return false}).popover({
                        container: 'body',
                        placement:'top',
                        trigger: 'hover',
                        html: true,
                        content: function () {
                            var clone = $($(this).find('table').clone(true)).show();
                            return clone;
                        }
                    })
                    $('#pnl-panelreporte').find("#changepuntaje").children().remove();                    
                    $('#pnl-panelreporte table').DataTable(
                        {
                            ordering: false,                        
                            searching: false,
                            pageLength: 50,
                            info:false,
                            oLanguage: {
                                "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
                                "oPaginate": {
                                    "sFirst": "<?php echo JrTexto::_("Next page") ?>", // This is the link to the first page
                                    "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                                    "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                                    "sLast": "<?php echo JrTexto::_("Previous page") ?>" // This is the link to the last page
                                }
                            }
                        }
                    );
                
            }else if(vistareporte=='area_char'){                 
                var divid='grafico'+idnumgrafico;
                var div='<div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Students')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Tiempo en cursos')); ?>'},
                        vAxis: {minValue: 0}
                    };
                google.setOnLoadCallback(charst('area',dt,options,divid));
            }else if(vistareporte=='bar_char'){               
                var divid='grafico'+idnumgrafico;
                var div='<div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        orientation:'horizontal',
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Students')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Tiempo en cursos')); ?>'},
                        vAxis: {minValue: 0}
                    };
                google.setOnLoadCallback(charst('bar',dt,options,divid));
            }else if(vistareporte=='line_char'){                 
                var divid='grafico'+idnumgrafico;
                var div='<div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Students')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Tiempo en cursos')); ?>'},
                        vAxis: {minValue: 0}
                    };
                google.setOnLoadCallback(charst('line',dt,options,divid));      
            }else if(vistareporte=='pie_char'){                
                var divid='grafico'+idnumgrafico;
                var div='<div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Students')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Tiempo en cursos')); ?>'},
                        vAxis: {minValue: 0}
                    };
                google.setOnLoadCallback(charst('pie',dt,options,divid)); 
            }else{
                table='';
                $('#pnl-panelreporte').html(table);
            }        
            vistaimpresion();
            //console.log(vistareporte,dt);
        }

        var obtnerresultado=function(){
            var rtpalumno=localStorage.getItem('rtpalumno');
            if(rtpalumno=='ver_rpt'+verrptalumno) return mostrarresultado();
            localStorage.setItem('rtpalumno','ver_rpt'+verrptalumno);
            var data=new FormData()
            $cursos=$('select#idcurso');            
            data.append('idgrupoauladetalle',$cursos.find("option:selected").attr('idgrupoauladetalle'));
            data.append('idcurso',$cursos.val());
            data.append('idgrupoaula',$('select#idgrupoaula').val());
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/reportes/tiempo',
                callback:function(rs){
                  if(rs.code==200){
                    if(rs.data.length){
                        var datos=rs.data;
                        $('#sindatos').hide(0);
                        $('#pnl-panelreporte').show();
                        localStorage.setItem('dataexamenes',JSON.stringify(datos));
                        mostrarresultado();
                    }else{
                        $('#sindatos').show(0);
                        $('#pnl-panelreporte').hide();
                        localStorage.setItem('dataexamenes','[]');
                        $('#impresion table tbody').empty();
                        if(!$.fn.DataTable.isDataTable( '#impresion table')) {
                          $('#impresion table').DataTable();
                        }else{
                            table = $('#impresion table').DataTable();
                            table.clear();
                            table.draw();
                        }
                        $('#pnl-panelreporte  table tbody').empty();
                        if(!$.fn.DataTable.isDataTable( '#pnl-panelreporte  table')) {
                          $('#pnl-panelreporte  table').DataTable();
                        }else{
                            table = $('#pnl-panelreporte  table').DataTable();
                            table.clear();
                            table.draw();
                        }
                        $('#impresion2  table tbody').empty();
                        if(!$.fn.DataTable.isDataTable( '#impresion2  table')) {
                          $('#impresion2  table').DataTable();
                        }else{
                            table = $('#impresion2  table').DataTable();
                            table.clear();
                            table.draw();
                        }
                    }
                  }
                }
            });
        }

        $('#idgrupoaula').change(function(ev){
            var idgrupoaula=$(this).val();
            $cursos=$('select#idcurso');
            $cursos.empty();
            if(grupos[idgrupoaula].length){
                $.each(grupos[idgrupoaula],function(i,v){
                    $cursos.append('<option value="'+v.idcurso+'" idgrupoauladetalle="'+v.idgrupoauladetalle+'">'+v.strcurso+'</option>');
                })
            }
            $cursos.trigger('change');
        })
        $('#idcurso').change(function(ev){
            verrptalumno++;
            obtnerresultado();
        })

        $('.changevista').click(function(ev){
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            localStorage.setItem('vistaReporte1',$(this).attr('vista'));
            mostrarresultado();
        })
        $('#pnl-panelreporte').on('change','table select#changepuntaje',function(ev){
            ev.preventDefault();
            _this=$(this);
            var val=_this.val();
            var td=$('#pnl-panelreporte table td.shownota');
            $.each(td,function(vv){
                var c=parseFloat(parseFloat($(this).attr('nota'))*parseFloat(val)/parseFloat($(this).attr('de'))).toFixed(2);
                $(this).children('h3').text(c);
            })
            localStorage.setItem('notabase',val);
            vistaimpresion();
            //ev.stopPropagation();
        }).on('change','#notabase',function(ev){
            localStorage.setItem('notabase',$(this).val());
            mostrarresultado();
        })

        $('.btnexportar_xlsx').click(function(ev){
             setTimeout(function(){$('#impresion .dt-buttons').find('.buttons-excel').trigger('click')},1000);
        })
       
        $('.btnexportar_pdf').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-pdf').trigger('click');
        });       
        $('.btnimprimir_2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-print').trigger('click');            
        });
        $('#tableimprimir').DataTable({
            ordering: false,
            searching: false,
            pageLength: 50,
            dom: 'Bfrtip',
            buttons: [
              {
                extend: "pdfHtml5",
                messageTop: '',
                title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Course notes')." - ".date('d-m-Y'); ?>',
                customize: function(doc){
                    doc.pageMargins = [12,12,12,12];
                    doc.content[1].table.widths = ["*", "*", "*"];
                    var tblBody = doc.content[1].table.body;
                    $('#tableimprimir tbody').find('tr').each(function (ir, row){
                        if($(this).hasClass('nomexamen')){
                            $(this).find('td').each(function(ic,td){
                                tblBody[ir][ic].fillColor='#e4e1e1';
                            })
                        }
                        //var rowElt = row;
                        /*doc.content[1].layout = {
                        hLineWidth: function(i, node) {
                            return (i === 0 || i === node.table.body.length) ? 2 : 1;},
                        vLineWidth: function(i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 2 : 1;},
                        hLineColor: function(i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';},
                        vLineColor: function(i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';}
                        };*/ 
                    })                 
                }
              },{
                extend: "excelHtml5",
                messageTop: '',
                title:'<?php echo JrTexto::_('Report') ?>',
                customize: function(xlsx){
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var col=['A','B','C','D','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                    $('#tableimprimir tbody').find('tr').each(function (ir, row){
                        if($(this).hasClass('nomexamen')){
                            $(this).find('td').each(function(ic,td){
                                var xcol=col[ic];
                                $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',32);
                                //tblBody[ir][ic].fillColor='#17a2b8';
                            })
                        }
                    })
                    console.log(sheet);
                }
              }
            ]
        });
        $('#pnl-panelreporte .dt-buttons').hide();
        $('#impresion .dt-buttons').hide();
        //$('#impresion').hide();

        verrptalumno++;
        vistareporte=localStorage.getItem('vistaReporte1')||'table';
        obtnerresultado();
        setTimeout(function(){$('.changevista[vista="'+vistareporte+'"]').trigger('click')},2000);
    })
</script>