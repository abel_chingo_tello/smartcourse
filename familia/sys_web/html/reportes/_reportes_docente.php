<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2 class="titulo-p"><?php echo JrTexto::_("Reports"); ?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content x_content_2">
        <div class="col-md-12 col-sm-12 text-center listRep"></div>
      </div>
    </div>
  </div>
</div>

<script>
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_notas/?tipoexamen=entrada",
    icono: "fa-sign-in",
    titulo: "<?php echo JrTexto::_("Beginning Test"); ?>"
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_notas/?tipoexamen=salida",
    icono: "fa-sign-out",
    titulo: "<?php echo JrTexto::_("Final Test"); ?>"
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_notas/?tipoexamen=entradaysalida",
    icono: "fa-bar-chart",
    titulo: "<?php echo JrTexto::_("Beginning vs. Final Test"); ?>"
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_notas/",
    icono: "fa-flag",
    titulo: "<?php echo JrTexto::_("Notes"); ?>"
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_tiempo/",
    icono: "fa-clock-o",
    titulo: "<?php echo JrTexto::_("Study Time"); ?>",
    //ribbon: true
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_progreso/",
    icono: "fa-tasks",
    titulo: "<?php echo JrTexto::_("My Progress"); ?>",
    //ribbon: true
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_criterios/",
    icono: "fa-tasks",
    titulo: "<?php echo JrTexto::_("Por Criterios"); ?>",
    //ribbon: true
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_cursos/",
    icono: "fa-tasks",
    titulo: "<?php echo JrTexto::_("Reporte de Curso"); ?>",
    //ribbon: true
  });
  addWidgetPanel({
    contentClass: 'listRep',
    link: _sysUrlBase_+"reportes/_modulo/",
    icono: "fa-tasks",
    titulo: "<?php echo JrTexto::_("Por Modulos"); ?>",
    ribbon: true
  });
</script>