<?php defined("RUTA_BASE") or die();
$urlreportes = $this->documento->getUrlBase() . "reportes/";
?>
<script>
  var _sysUrlBase_ = "<?= $this->documento->getUrlBase() ?>";
  var idalumno = "<?= $this->idalumno ?>";
  var tipo = "<?= $this->tipo ?>";
  var idproyecto = "<?= $this->idproyecto ?>";
</script>
<style>
  section.boxContacto {
    height: auto !important;
    min-height: 100vh;
  }

  iframe.iframecontent {
    height: auto !important;
    min-height: 100vh;
  }
</style>
<div class="paris-navbar-container"></div>
<div class="container">
  <div class="row my-bg-main">
    <div class="col-sm-12">
      <br>
      <div class="max-w my-center">

        <form onsubmit="return false" class="col-sm-12 my-card">
          <div class="form-row">
            <div class="form-group col-sm-12 my-row">
              <label for="curso" class="my-center">
                <h6>Curso:</h6>
              </label>
              <select id="selectCursos" name="" class="form-control " id=""></select>
            </div>
          </div>

        </form>
      </div>

      <br><br>
      <div id="cont-tareas" class="row">

      </div>
    </div>
  </div>
</div>
<script>
  var _tipo = null;
  $(document).ready(() => {
    _tipo = '<?php echo JrTexto::_($this->tipo == 'T' ? "Tareas" : "Proyectos"); ?>'
    addItemNavBar({
      text: _tipo
    });
  })
</script>