<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Notes"); ?>'
  });
</script>
<input type="hidden" name="idalumno" id="idalumno" value="<?php echo $this->idalumno; ?>">
</div>
<div class="col-md-12" >
  <div class="x_panel">
    <div class="x_title">
          <div class="row">
              <div class="col-md-6">
                  <div class="btn-group">
                      <!--button type="button" class="btn btn-xs btn-success btnexportar_xlsx"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_1</button>
                      <button type="button" class="btn btn-xs btn-success btnexportar_xlsx2"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_2</button>
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_1</button>
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf2"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_2</button-->
                      <button type="button" class="btn btn-xs btn-warning btnimprimir"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?></button>
                      <!--button type="button" class="btn btn-xs btn-warning btnimprimir_2"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_2</button-->
                  </div>
              </div>
              <div class="col-md-6 text-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                      <!--button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button-->
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar_char" ><i class="fa fa-bar-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="area_char"><i class="fa fa-area-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="line_char" ><i class="fa fa-line-chart"></i></button>
                  </div>
              </div>
          </div>
        <div class="clearfix"></div>
      </div>
<div class="row" id="zonainfo">
 
    <div class="col-md-4 col-sm-6 col-xs-12 text-center">
    <div class="x_panel">
      <div class="x_title">
        <h6><?php echo JrTexto::_('Study Time in the Virtual Platform'); ?></h6>
      </div>
      <div class="">
        <h1 class="changetiempo"><?php echo $this->tiempopv; ?></h1>
      </div>
    </div>
  </div>

  <div class="col-md-4 col-sm-6 col-xs-12 text-center">
    <div class="x_panel">
      <div class="x_title">
        <h6><?php echo JrTexto::_('Tiempo en Examenes'); ?></h6>
      </div>
      <div class="">
        <h1 class="changetiempo"><?php echo $this->tiempoexam; ?></h1>
      </div>
    </div>
  </div>

  <div class="col-md-4 col-sm-6 col-xs-12 text-center">
    <div class="x_panel">
      <div class="x_title">
        <h6><?php echo JrTexto::_('Tiempo en Cursos'); ?></h6>
      </div>
      <div class="">
        <h1 class="changetiempo"><?php echo $this->tiempocursos; ?></h1>
      </div>
    </div>
  </div>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h6><?php echo JrTexto::_('Tiempo en Cursos'); ?></h6>
    </div>
    <div id="pnl-panelreporte">
      
    </div>
  </div>
</div>
  </div>   
 </div>

 <script type="text/javascript">
  var __formatotiempo=function(seg){    
    var _f=function(t,d){return t.toString().padStart(d,'0')}
    var _m=function(s){var m_=parseInt(s/60); var s_=s%60; return {'m':_f(m_,2),'s':_f(s_,2)}}
    var _h=function(s){var h_=parseInt(s/3600);var m_=_m(s%3600); m_.h=_f(h_,2); return m_; }
    var _d=function(s){var d_=parseInt(s/86400);var h_=_h(s%86400);h_.d=_f(d_,2); return h_; }
    if(seg<60) return {'s':_f(seg,2)}
    else if(seg<3600) return _m(seg); 
    else if(seg<86400) return _h(seg); 
    else return _d(seg);    
  }
  var __formatodhms=function(seg){
    var t=__formatotiempo(seg);
    return (t.d!=undefined?(t.d+'d '):'')+(t.h!=undefined?(t.h+'h '):'')+(t.m!=undefined?(t.m+"m "):'')+t.s+"s";
  }
  $(document).ready(function(){
    $('.changetiempo').each(function(i,v){
      $(v).text(__formatodhms($(v).text()));
    })
    __logoEmpresaimagebase64();
        google.charts.load('current', {'packages':['corechart']});

    function charst(tipo,dt,options,idelem){
      //google.load("visualization", "1", {packages:["corechart"]});
      var selbase=localStorage.getItem('notabase')||'';
      var bases=[];
      var strcurso=[];
      var data = new google.visualization.DataTable();
      data.addColumn('string', '<?php echo ucfirst(JrTexto::_('Courses')); ?>');
      
      var datos=[]; 
      var _cursos=[];
      if(dt.length)
          $.each(dt,function(i,c){ 
            if(c.lugar=='S'){
              if($.inArray(c.idcurso,datos)==-1){
                datos.push(c.idcurso);
                _cursos[c.idcurso]={nombre:c.nombre,tiempo:c.tiempoensegundos};
              }else{
                _cursos[c.idcurso].tiempo=_cursos[c.idcurso].tiempo+c.tiempoensegundos;
              }
            }             
          })
      var maxt=0;
      var tipo_='s';
      var nomtipo='Segundos';
      if(_cursos.length){
        for(var i=0;i<_cursos.length;i++){
          if(_cursos[i]!=undefined){
            maxt=(maxt<parseInt(_cursos[i].tiempo)?parseInt(_cursos[i].tiempo):maxt);                
          }
        }
        if(maxt<60) {tipo_='s'; nomtipo='Segundos'; }
        else if(maxt<3600) {tipo_='m'; nomtipo='Minutos';}
        else if(maxt<86400) {tipo_='h'; nomtipo='Horas';}
      }
      data.addColumn('number', nomtipo);
      


      if(_cursos.length){        
         
          
          for(var i=0;i<_cursos.length;i++){
            if(_cursos[i]!=undefined){
              var t=__formatotiempo(_cursos[i].tiempo);
              data.addRow([_cursos[i].nombre,parseInt(t[tipo_])]);
            }
          }
          options.title=options.title+' ';

          //console.log(datos);
          if(tipo=='area') var chart = new google.visualization.AreaChart(document.getElementById(idelem));
          else if(tipo=='line')var chart = new google.visualization.LineChart(document.getElementById(idelem));
          else if(tipo=='pie')var chart = new google.visualization.PieChart(document.getElementById(idelem));
          else var chart = new google.visualization.BarChart(document.getElementById(idelem));
          chart.draw(data, options);       
        }
      }
    var idnumgrafico=0;
    var verrptalumno=1;
    var mostrarresultado=function(){
      var dt=<?php echo json_encode($this->tiempos); ?>;
      var vistareporte=localStorage.getItem('vistaReporte1')||$('.changevista.active').attr('vista');
      console.log(vistareporte);
      if(vistareporte=='table') vistareporte='bar_char';
     // var selbase=localStorage.getItem('notabase')||'';
      //var dt=JSON.parse(dataexamenes);          
          if(vistareporte=='area_char'){                 
              var divid='grafico'+idnumgrafico;
              var div='<div id="'+divid+'" style="width:100%"></div>';
              $('#pnl-panelreporte').html(div);
              var options = {
                      'legend':'none',
                      'is3D':true,                        
                      height:560,
                      title: '<?php echo ucfirst(JrTexto::_("Tiempo de cursos")); ?>',
                      hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                      vAxis: {title: '<?php echo ucfirst(JrTexto::_('horas')); ?>'},
                      vAxis: {minValue: 0}
                  };
              google.setOnLoadCallback(charst('area',dt,options,divid));
          }else if(vistareporte=='bar_char'){               
              var divid='grafico'+idnumgrafico;
              var div='<div id="'+divid+'" style="width:100%"></div>';
              $('#pnl-panelreporte').html(div);
              var options = {
                      'legend':'none',
                      'is3D':true,                        
                      height:560,
                      orientation:'horizontal',
                      title: '<?php echo ucfirst(JrTexto::_("Tiempo de Cursos")); ?>',
                      hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                      vAxis: {title: '<?php echo ucfirst(JrTexto::_('Horas')); ?>'},
                      vAxis: {minValue: 0}
                  };
              google.setOnLoadCallback(charst('bar',dt,options,divid));
          }else if(vistareporte=='line_char'){                 
              var divid='grafico'+idnumgrafico;
              var div='<div id="'+divid+'" style="width:100%"></div>';
              $('#pnl-panelreporte').html(div);
              var options = {
                      'legend':'none',
                      'is3D':true,                        
                      height:560,
                      title: '<?php echo ucfirst(JrTexto::_("Tiempo de cursos")); ?>',
                      hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                      vAxis: {title: '<?php echo ucfirst(JrTexto::_('Horas')); ?>'},
                      vAxis: {minValue: 0}
                  };
              google.setOnLoadCallback(charst('line',dt,options,divid));      
          }else if(vistareporte=='pie_char'){                
              var divid='grafico'+idnumgrafico;
              var div='<div id="'+divid+'" style="width:100%"></div>';
              $('#pnl-panelreporte').html(div);
              var options = {
                      'legend':'none',
                      'is3D':true,                        
                      height:560,
                      title: '<?php echo ucfirst(JrTexto::_("Tiempo de cursos")); ?>',
                      hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                      vAxis: {title: '<?php echo ucfirst(JrTexto::_('Horas')); ?>'},
                      vAxis: {minValue: 0}
                  };
              google.setOnLoadCallback(charst('pie',dt,options,divid)); 
          }
          //vistaimpresion();
          //console.log(vistareporte,dt);
      }

      $('.changevista').click(function(ev){
        $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
        $(this).addClass('active btn-primary').removeClass('btn-secondary');
        localStorage.setItem('vistaReporte1',$(this).attr('vista'));
        mostrarresultado();
      })

      setTimeout(function(){$('.changevista').first().trigger('click');},2000);
       $('.btnimprimir').click(function(ev){
        $('#zonainfo').imprimir();
      })
      //mostrarresultado();     
})


      </script>