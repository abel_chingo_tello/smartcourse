<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Notas"); ?>'
        });
        var __usuario = <?= json_encode($this->usuario) ?>
    </script>
<?php } ?>

<div class="container my-font-all">
    <div class="row my-x-center">
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="">Cursos</label>
                <select class="form-control" name="" id="selectCursos">
                </select>
            </div>
        </div>
    </div>
    <div class="row col-sm-12 my-x-center">
        <div class="box-cont">
            <table id="my-table" class="mdl-data-table my-font my-card my-shadow" style="width:100%">
                <thead id="tb-head" class="my-text-center">
                    <!-- <tr>
                        <th></th>
                        <th></th>
                    </tr> -->
                </thead>
                <tbody id="tbody">
                    <tr class="odd">
                        <td id="tb-msg" valign="top" colspan="4" class="dataTables_empty">Cargando...</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
    <div class="row col-sm my-shadow my-card graf-cont">
        <h1 class="title-graf">Estadísticas</h1>
        <div class="row col-sm-12 my-x-center chart-cont">
            <canvas id="myChart" class="doughnut"></canvas>
        </div>
        <div class="row col-sm-12 my-x-center chart-cont">
            <canvas id="myChart2" class="line"></canvas>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>