<?php
defined('RUTA_BASE') or die();
$idgui = uniqid();

$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$url = $this->documento->getUrlBase();

echo "<input type='hidden' id='proyecto' value='{$this->user['idproyecto']}'/>";
echo "<input type='hidden' id='idpersona' value='{$this->user['idpersona']}'/>";
//variables to json
$json_progresos = json_encode($this->progresos);
?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: black; font-size: large; }

.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){ margin:0 auto; }
.text-nota1{ font-size: x-large; font-weight: bold; color: #b73333; }
.text-custom1 { font-size: large; border-bottom: 1px solid gray; border-radius: 10%; box-shadow: 0px 1px #e20b0b; }
#tabla_alumnos td{ cursor:pointer; }
#tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
#tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
#tabla_alumnos_length { display: none; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>


<?php if(!$this->isIframe): ?>
<!-- <div style="position:relative;  margin:10px 0;">
    <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
</div> -->
<?php endif; ?>

<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <div class="col-md-12">
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
      <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li>                  
      <li class="active">
        <?php echo JrTexto::_("Teacher Progress"); ?>
      </li>
    </ol>
  </div>
</div>
<?php endif; ?>

<!--START CONTAINER-->
<div class="">
	<!--START PANEL-->
    <div class = 'panel panel-primary' >
    	<div class="panel-heading" style="text-align:left;">
    		<?php echo JrTexto::_("Report");?>
    	</div>
    	<div class = 'panel-body' style="text-align: center;  " >
            <h3 style="font-weight:bold;"><?php echo JrTexto::_("Teacher Progress"); ?></h3>
            <div class="row" style="margin:15px auto;">
                <div class="col-md-4" style="/*height:90px;*/">
                    <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                    <h2><?php echo $this->fullname ?></h2>
                </div>
                <div class="col-md-8">
                  <div class="col-sm-12">
                    <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Courses"); ?></p>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="select_viewTable" id="select_viewTable" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                          <?php
                            if(!empty($this->cursos)){               
                              foreach ($this->cursos as $key => $lista_nivel){
                                $nombre=$lista_nivel['nombre'];
                                $idnivel=$lista_nivel['idcurso'];
                                echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                              }
                            }else{
                              echo '<option value="0">Sin cursos registrado</option>';
                            }
                          ?>
                        </select>
                    </div>
                  </div>
                	
                  <div id="progress_total" class="col-sm-12" style="text-align:center;">
                    <h4 style="padding-top:15px;"><?php echo JrTexto::_("Total Progress in The Course"); ?> <span class="titleCursoName">A1</span></h4>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                        aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width:35%">
                            <!-- 40% Complete (success) -->
                        </div>
                    </div>
                    <div class="countPercentage" data-value="35"><span>0</span>%</div>
                  </div>
                </div>
                <!--end top view -->
                <?php if(!empty($this->progresos)): ?>
                  <div class="col-md-12">
                    <!--start caja-->
                    
                    <!--start graficos-->
                    <div class="col-sm-6" style="border:1px solid gray; border-radius:1em; padding:5px 0; box-shadow: -3px 3px 14px;">
                      <div style="height:50px;"></div>
                      <h3><?php echo JrTexto::_("Units"); ?></h3>
                      <div>
                        <canvas id="chart1" width="800" height="700" class="chartjs-render-monitor" style="display: block;"></canvas>
                      </div>
                    </div>
                    <div class="col-sm-6" style="border:1px solid gray; border-radius:1em; padding:5px 0; box-shadow: 3px 3px 14px;">
                      <div style="height:50px; display:inline-block;">
                        <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Unit"); ?></p>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="select_viewActivity" id="select_viewActivity" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                              <option value="0"><?php echo JrTexto::_("No Data"); ?></option>
                            </select>
                        </div>
                      </div>
                      <h3><?php echo JrTexto::_("Sessions"); ?></h3>
                      <div>
                        <canvas id="chart2" width="800" height="700" class="chartjs-render-monitor" style="display: block;"></canvas>                      
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
                
            </div>
            <!--END row-->
        </div>
</div>
<!--END CONTAINER-->

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3><?php echo JrTexto::_("Students Reports"); ?></h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <div id="info" style="display:block;">
                <h4 id="infoDre" style="display:inline-block;padding: 0 10px;">DRE:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">Lambayeque</span></h4>
                <h4 id="infoUgel" style="display:inline-block;padding: 0 10px;">UGEL:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">UGEL Lambayeque</span></h4>
            </div>
            <div id="idcolegio-container" style="display: inline-block;">
                
                <h4 style="display:inline-block;"><?php echo JrTexto::_("School"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select id="idcolegio" style="width:200px;" class="select-docente form-control select-ctrl ">
                        <?php if(!empty($this->miscolegios)){
                        echo '<option value="0">'.JrTexto::_("Select").'</option>';
                        foreach ($this->miscolegios  as $c) {
                            if(empty($cursos)) $cursos=$c["cursos"];
                            echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                        }} ?>
                    </select>
                </div>
            </div>
            <div id="idcurso-container" style="display: inline-block;">
                <h4 style="display:inline-block;"><?php echo JrTexto::_("Course"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="idcurso" id="idcurso" style="width:150px;" class="select-docente form-control select-ctrl">
                      <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                    </select>
                </div>
            </div>
            <div id="idgrados-container" style="display: inline-block;">
                <h4 style="display: inline-block;"><?php echo JrTexto::_("Grade"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="grados" id="idgrados" style="width:150px;" class="select-docente form-control select-ctrl">
                      <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                    </select>
                </div>
            </div>
            <div id="idseccion-container" style="display: inline-block;">
                <h4 style="display: inline-block;"><?php echo JrTexto::_("Section"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="seccion" id="idseccion" style="width:150px;" class="select-docente form-control select-ctrl select-nivel">
                        <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                    </select>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="" id="contenedorAlumnos" style="border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
              <h4 style="font-weight: bold; padding: 10px; background-color: #4683af; color: white; margin-top: 0;"><?php echo JrTexto::_("Students"); ?></h4>       
              <div class="table-responsive">
                <table id="tabla_alumnos" class="table table-bordered table-hover tablaAlumnos">
                    <thead>
                        <tr><td><?php echo JrTexto::_("Names"); ?></td></tr>
                    </thead>
                </table>              
              </diV>
            </div>
          </div>
          <div class="col-md-9" style="border-radius:0.4em;">
              <div style="margin:5px 0;"></div>
              <div id="viewContent" style="border-radius:0.4em; background-color:#f7f7f7; border: 2px solid #337ab7; text-align:center;">
                  <h1><i class="fa fa-minus-circle fa-3x"></i></h1>
                  <h4><?php echo JrTexto::_("Select Student"); ?></h4>
              </div>
          </div>
          <div class="col-md-9" style="text-align:right;">
              <div style="padding:5px 0;"></div>
              <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_("View Results by Section"); ?></a>
          </div>
        </div>
      </div>
    </div>
    <!--end panel-->

<script type="text/javascript">
var progresos = <?php echo $json_progresos ?>;
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;

function frameload(){
  $('.loading-chart').remove();
}

function nameSelect(select,value){
    var text = null;
    select.find('option').each(function(k,v){
    if($(this).attr('value') == value){
        text = $(this).text();
    }
    });
    return text;
}

function drawBarHorizontal(ctx,data){
    var myPieChart = new Chart(ctx,{
        type: 'horizontalBar',
        data: data,
        options: {
        legend: false,
            scales: {
                xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Percentage'
                },
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}
function countText(obj,valueText, ToValue){
var currentValue = parseInt(valueText);
var nextVal = ToValue;
var diff = nextVal - currentValue;
var step = ( 0 < diff ? 1 : -1 );
for (var i = 0; i < Math.abs(diff); ++i) {
    setTimeout(function() {
        currentValue += step
        obj.text(currentValue);
    }, 50 * i)   
}
}

function dibujarDatatable(){
    if ($.fn.DataTable.isDataTable("#tabla_alumnos")) {
        $('#tabla_alumnos').DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $('#tabla_alumnos').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "displayLength": 25,
        "pagingType": "simple"
    });
}

function drawprogresoxactividad(id,index){
    if(id != 0){
      var primero = true;
      var obj = $('#chart2');
      var labels = new Array();
      var data_progreso = new Array();

      for(var j in progresos[id][index]['hijo']){
        if(progresos[id][index]['hijo'][j] && parseInt(j) > -1){
          labels.push(progresos[id][index]['hijo'][j]['nombre']);
          data_progreso.push(progresos[id][index]['hijo'][j]['progreso']);
        }
      }
      
      chartData = {
        labels: labels,
        datasets: [{
          backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)'],
          data: data_progreso,
          borderColor: 'white',
          borderWidth: 2
        }]
      };
      var _ide = obj.attr('id');
      obj.parent().html("").html('<canvas id="'+_ide+'" width="800" height="700" class="chartjs-render-monitor"></canvas>'); //No es la mejor opcion investigar update de la libreria
      obj = document.getElementById(_ide).getContext('2d');
      drawBarHorizontal(obj,chartData);
    }//endif id
  }
  function drawprogresos(id){
    if(id != 0){
      
      //draw total progress
      var progressbar = $('#progress_total').find('.progress-bar');
      var total = 0;
      for(var i in progresos[id]){
        if(progresos[id][i] && parseInt(i) > -1 ){
          total += progresos[id][i]['progreso'];
          // total +=100; 
        }
      }

      total = (total * 100) / (100 * progresos[id].length) ;
      progressbar.css('width',total.toString()+"%");
      countTextObject = $('#progress_total').find('.countPercentage');
      countTextObject.data('value',total);
      countText(countTextObject.find('span'), countTextObject.find('span').text(), total );
      $('#progress_total').find('.titleCursoName').html(nameSelect($('#select_viewTable'),$('#select_viewTable').val()));
      //draw progress unidad
      var obj = $('#chart1');
      var labels = new Array();
      var data_progreso = new Array();
      for(var i in progresos[id]){
        if(progresos[id][i] && parseInt(i) > -1 ){
          labels.push(progresos[id][i]['nombre']);
          data_progreso.push(progresos[id][i]['progreso']);
        }
      }

      var chartData = {
        labels: labels,
        datasets: [{
          backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)'],
          data: data_progreso,
          borderColor: 'white',
          borderWidth: 2
        }]

      };
      var _ide = obj.attr('id');
      obj.parent().html("").html('<canvas id="'+_ide+'" width="800" height="700" class="chartjs-render-monitor"></canvas>'); //No es la mejor opcion investigar update de la libreria
      obj = document.getElementById(_ide).getContext('2d');
  		drawBarHorizontal(obj,chartData);
      //draw parte actividad...
      obj = $('#chart2');
      labels = new Array();
      data_progreso = new Array();
      var primero = true;
      var select_unidad = $('#select_viewActivity');
      select_unidad.html('');
      /* recorrido de objeto */
      for(var i in progresos[id]){
        if(primero === true){
          //code here
          for(var j in progresos[id][i]['hijo']){
            if(progresos[id][i]['hijo'][j] && parseInt(j) > -1){
              labels.push(progresos[id][i]['hijo'][j]['nombre']);
              data_progreso.push(progresos[id][i]['hijo'][j]['progreso']);
            }
          }
          primero = false;
        }
        if(progresos[id][i]['nombre']){          
          if(progresos[id][i]['nombre']){
            select_unidad.append('<option value="'+i+'">'+progresos[id][i]['nombre']+'</option>');
          }
        }//endif
      }
      
      
      chartData = {
        labels: labels,
        datasets: [{
          backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)'],
          data: data_progreso,
          borderColor: 'white',
          borderWidth: 2
        }]
      };
      var _ide = obj.attr('id');
      obj.parent().html("").html('<canvas id="'+_ide+'" width="800" height="700" class="chartjs-render-monitor"></canvas>'); //No es la mejor opcion investigar update de la libreria
      obj = document.getElementById(_ide).getContext('2d');
      drawBarHorizontal(obj,chartData);
    }//endif
  }

  var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idcurso').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idgrados').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');
        $('#idseccion').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');              
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
  }

  $('document').ready(function(){
    //..
      $('#info').hide();
      $('#idcurso-container').hide();
      $('#idseccion-container').hide();
      $('#idgrados-container').hide();

        dibujarDatatable();
        var id = $('#select_viewTable').val();
        drawprogresos(id);

        $('#select_viewTable').on('change',function(){
        drawprogresos($('#select_viewTable').val());
        });
        $('#select_viewActivity').on('change',function(){
        // drawprogresos($('#select_viewTable').val());
        drawprogresoxactividad($('#select_viewTable').val(),$('#select_viewActivity').val());
        });
        
    $('#idcolegio').change(function(){
      //change width
      var texto = nameSelect($('#idcolegio'),$('#idcolegio').val());
      // console.log(texto);
      $(this).css('width',(texto.length + 200)+'px');
        if($('#idcolegio').val() != 0){
            $('#idcurso-container').show();
            var idcolegio=$(this).val()||'';
            for (var i = 0; i < datoscurso.length; i++) {
              if(datoscurso[i].idlocal==idcolegio){              
                    datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
                  actualizarcbo($(this),datoscurso[i].cursos);
              }
            }

          //buscar informacion del colegio...
          $.ajax({
                url: _sysUrlBase_+'/local/infolocal',
                type: 'POST',
                dataType: 'json',
                data: {'idlocal': $('#idcolegio').val()},
            }).done(function(resp) {
                if(resp.code=='ok'){
                    if(Object.keys(resp.data).length > 0){
                        var dre = null;
                        if(resp.data[0].dre == null || resp.data[0].dre == 0){
                            dre = resp.data[0].departamento;
                        }else{
                            dre = resp.data[0].dre;
                        }
                        $('#info').find('#infoDre').find('span').text(dre);
                        $('#info').find('#infoUgel').find('span').text(resp.data[0].ugel);
                    }
                } else {
                    return false;
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
            $('#info').show();
        }else{
            $('#info').hide();
        }
      });
      $('#idcurso').change(function(){
        //change width
        var texto = nameSelect($('#idcurso'),$('#idcurso').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcurso').val() != 0){
            $('#idgrados-container').show();
            var idcurso=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;                
                    actualizarcbo($(this),grados);
                }
        }
      });
      $('#idgrados').change(function(){
        //change width
        var texto = nameSelect($('#idgrados'),$('#idgrados').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idgrados').val() != 0){
            $('#idseccion-container').show();
            var idgrados=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            var idcurso=$('#idcurso').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;
                    for (var h = 0; h < grados.length; h++){
                        if(grados[h].idgrado==idgrados){
                            var secciones=grados[h].secciones;
                            secciones.sort(function(a, b){return a.seccion - b.seccion;});
                            actualizarcbo($(this),secciones);
                        }
                    }
                    
                }
        }
      });
      $('#idseccion').change(function(){
        //change width
        var texto = nameSelect($('#idseccion'),$('#idseccion').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        // alert($(this).val());
        $.ajax({
            url: _sysUrlBase_+'/reportes/listaGrupoAlumnos',
            type: 'POST',
            dataType: 'json',
            data: {'idgrupoaula': $(this).val(), 'idproyecto': $('#proyecto').val()},
        }).done(function(resp) {
            if(resp.code=='ok'){
                var alumnos = $('<tr></tr>');
                var filas = '';
                var tabla_tmp = $('<table></table>');
                tabla_tmp.attr('id','tabla_alumnos');
                tabla_tmp.addClass('table table-bordered table-hover tablaAlumnos');
                tabla_tmp.append('<thead><tr><td>\<?php echo JrTexto::_("Names") ?></td></tr></thead><tbody></tbody>');
                console.log(resp.data.alumnos);
                for (var i = resp.data.alumnos.length - 1; i >= 0; i--) {
                    //data-dni="'+resp.data.alumnos[i].dni+'"
                    filas = filas.concat('<tr data-id="'+resp.data.alumnos[i].id+'" data-dni="'+resp.data.alumnos[i].dni+'" ><td>'+resp.data.alumnos[i].nombre+'</td></tr>');        
                }
                tabla_tmp.find('tbody').html(filas);

                $('#tabla_alumnos_wrapper').remove();
                $('#contenedorAlumnos').append(tabla_tmp);
                dibujarDatatable();
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
    });

    $('#contenedorAlumnos').on('click','.tablaAlumnos tbody tr',function(){
        if($(this).data('id')){
            var controllerAndFunction = '/reportealumno/progreso';
            var url = _sysUrlBase_ + controllerAndFunction +'?isIframe=1&idalumno='+$(this).data('id')+'&idproyecto='+$('#proyecto').val()+'&plt=sintop2';
            $('#viewContent').html('');
            $('#viewContent').append('<iframe onload="frameload();" src="'+url+'" style="min-height:600px; position:relative; width:98%; margin:8px;"></iframe');
            $('#viewContent').append('<div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="'+_sysUrlBase_+'/static/tema/css/images/cargando.gif" class="img-responsive"></div>');            
            // $('#viewContent').append('<h1 class="loading-chart" style="position:absolute;top:0;font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading... <span class="nameMenu"></span></span></h1>');
        }
    });
	});
</script>