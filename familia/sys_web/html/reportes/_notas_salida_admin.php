<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Final Test"); ?>'
  });
</script>
<input type="hidden" name="idalumno" id="idalumno" value="<?php echo $this->idalumno; ?>">

<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
          <div class="row">
              <div class="col-md-6">
                  <div class="btn-group">
                      <button type="button" class="btn btn-xs btn-success btnexportar_xlsx"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_1</button>
                      <button type="button" class="btn btn-xs btn-success btnexportar_xlsx2"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_2</button>
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_1</button>
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf2"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_2</button>
                      <button type="button" class="btn btn-xs btn-warning btnimprimir"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_1</button>
                      <button type="button" class="btn btn-xs btn-warning btnimprimir_2"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_2</button>
                  </div>
              </div>
              <div class="col-md-6 text-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                      <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="area_char"><i class="fa fa-area-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar_char" ><i class="fa fa-bar-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="line_char" ><i class="fa fa-line-chart"></i></button>
                  </div>
              </div>
          </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_title">
      <div class="row">
            <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
                <label class="control-label"><?php echo JrTexto::_('DRE'); ?></label>
                  <select id="iddre" name="iddre" class="form-control" idsel="<?php echo @$this->iddre; ?>" >
                    <option value="0"><?php echo JrTexto::_('Only'); ?></option>
                      <?php 
                      if(!empty($this->dress))
                      foreach ($this->dress as $fk) { ?>
                        <option value="<?php echo $fk["iddre"]?>" <?php echo @$this->iddre==$fk["iddre"]?'selected="selected"':'';?> ><?php echo $fk["descripcion"] ?></option>
                      <?php } ?>
                  </select>
            </div>
            <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1" >
                <label class="control-label"><?php echo JrTexto::_('Ugel'); ?></label>
                  <select id="idugel" name="idugel" class="form-control" idsel="<?php echo @$this->idugel; ?>">
                    <option value="0"><?php echo JrTexto::_('Only'); ?></option>
                  </select>
            </div>
            <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1" >
                <label class="control-label"><?php echo JrTexto::_('Local/IIEE'); ?></label>
                  <select id="idlocal" name="idlocal" class="form-control" idsel="<?php echo @$this->idlocal; ?>">
                    <option value="0"><?php echo JrTexto::_('Only'); ?></option>
                  </select>
            </div>
            <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
                <label class="control-label"><?php echo JrTexto::_('Classroom'); ?></label>
                  <select id="idambiente" name="idambiente" class="form-control" idsel="<?php echo @$this->idambiente; ?>" >
                    <option value="0"><?php echo JrTexto::_('Only'); ?></option>
                  </select>
            </div>
            <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2" >
                <label class="control-label"><?php echo JrTexto::_('Groups/Area'); ?></label>
                  <select id="idgrupoaula" name="idgrupoaula" class="form-control" idsel="<?php echo @$this->idgrupoaula; ?>" >
                   </select>
            </div>
            <div class=" col-md-3 form-group">
                <label class="control-label"><?php echo JrTexto::_('Course');?> <span class="required"> * </span></label>
                <select id="idcurso" name="idcurso" class="form-control " title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                <?php if(!empty($this->cursos[$this->idgrupoaula])){
                  foreach ($this->cursos[$this->idgrupoaula] as $fk) { ?>
                    <option  value="<?php echo $fk["idcurso"]?>" idgrupoauladetalle="<?php echo $fk["idgrupoauladetalle"]; ?>" <?php echo @$this->idcurso==$fk["idcurso"]?'selected="selectted"':'';?>><?php echo $fk["strcurso"] ?></option>
                <?php }} ?>
                </select>
            </div>            
        </div> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="row" id="pnl-panelreporte" >            
        </div>
        <div class="row" id="impresion"style="display: none;">
            <div class="table-responsive">
            </div>
        </div>
        <div id="impresion2"style="display: none;">
            <div class="table-responsive">    
            </div>
        </div>
        <div id="sindatos" style="display:none">
            <div class="container">
            <div class="row">
                <div class="jumbotron">
                    <h1><strong>Opss!</strong></h1>                
                    <div class="disculpa"><?php echo JrTexto::_('Sorry, You do not have students enrolled')?>:</div>                
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        __logoEmpresaimagebase64();
        google.charts.load('current', {'packages':['corechart']});
        var grupos=<?php echo !empty($this->cursos)?(json_encode($this->cursos).';'):'[];'; ?>

        function charst(tipo,dt,options,idelem){
            var selbase=localStorage.getItem('notabase')||'';
            var bases=[];
            var strcurso=[];
            var data = new google.visualization.DataTable();
            data.addColumn('string', '<?php echo ucfirst(JrTexto::_('Courses')); ?>');
            data.addColumn('number', '<?php  echo ucfirst(JrTexto::_('Notas')); ?>');
             var dtarray=dt;
            for(i_=0;i_<dtarray.length;i_++){
                var dt=dtarray[i_];
                if(dt==null) continue;
                var strcurso=dt[0].strcurso!=undefined?dt[0].strcurso:'';

                var datos=[];        
                if(dt.length)
                    $.each(dt,function(i,c){ 
                        var formula=c.formula;
                        var iex=1;
                        var prom=0;
                        if(formula.length)
                            $.each(formula,function(j,f){
                                prom+=(f.nota*f.porcentaje);
                                de=f.puntajemax!=undefined?f.puntajemax:100;
                                if(iex==1&&(selbase==''||selbase==null||selbase=='null'))selbase=de;
                                iex++;
                                if($.inArray(de,bases)==-1) bases.push(de);
                            })
                        prom=parseFloat(parseFloat(prom/100).toFixed(2));
                        datos.push({idcurso:c.idcurso,nombrecurso:c.strcurso,nota:prom,de:de,masinfo:c});
                        
                    }) 
                bases.sort((a,b)=>a-b);
                if(bases.length)
                $.each(bases,function(i,v){
                    if(bases.length==1&&i==0){
                            selbase=v;
                            localStorage.setItem('notabase',v);
                    }
                    $('#notabase').append('<option values="'+v+'" '+(v==selbase?'selected="selected"':'')+'>'+v+'</option>');
                });

                if(datos.length){
                    $.each(datos,function(i,v){
                        var prom=parseFloat(parseFloat(v.masinfo.notasalida)*parseFloat(selbase)/parseFloat(v.masinfo.notasalida_enbasea));             
                        data.addRow([v.masinfo.stralumno,prom]);
                    })
                    options.title='<?php echo JrTexto::_('Course') ?> : '+strcurso+ ' -  ' +options.title+' /'+selbase;

                    //console.log(datos);
                    if(tipo=='area') var chart = new google.visualization.AreaChart(document.getElementById(idelem));
                    else if(tipo=='line')var chart = new google.visualization.LineChart(document.getElementById(idelem));
                    else if(tipo=='pie')var chart = new google.visualization.PieChart(document.getElementById(idelem));
                    else var chart = new google.visualization.BarChart(document.getElementById(idelem));
                    chart.draw(data, options);       
                }
            }
            
        }

        var verrptalumno=1;
        localStorage.setItem('rtpalumno','ver_rpt'+verrptalumno);
        var idnumgrafico=0;

        var vistaimpresion=function(){
            var selbase=localStorage.getItem('notabase')||'';                
            var dataexamenes=localStorage.getItem('dataexamenes');
            var dtarray=JSON.parse(dataexamenes);
            if(dtarray.length){
                for(i_=0;i_<dtarray.length;i_++){
                    var dt=dtarray[i_];
                    if(dt==null) continue;
                    var strcurso=dt[0].strcurso!=undefined?dt[0].strcurso:'';
                    var datos=[];
                    var nmaxformula=0;
                    var tdnomexam='';
                    if(dt.length)
                        $.each(dt,function(i,c){
                            var formula=c.formula;
                            var iex=1;
                            var prom=0;
                            tdnomexam='';
                            if(nmaxformula<formula.length) nmaxformula=formula.length;
                            if(formula.length)
                                $.each(formula,function(j,f){
                                    prom+=(f.nota*f.porcentaje);
                                    de=f.puntajemax!=undefined?f.puntajemax:100;
                                    if(iex==1&&(selbase==''||selbase==null||selbase=='null'))selbase=de;

                                    tdnomexam+='<th>'+((iex==1&&nmaxformula==1)||(nmaxformula==j+1)?'<?php echo JrTexto::_('Final Test'); ?>':'E'+(j+1))+'/('+f.porcentaje+')</th>';
                                    iex++;
                                })
                            prom=parseFloat(parseFloat(prom/100).toFixed(2));
                            datos.push({idcurso:c.idcurso,nombrecurso:c.strcurso,nota:prom,de:de,formula:formula,masinfo:c});
                        }) 
                    //if(nmaxformula<2) nmaxformula=3;
                    var total=0;
                    var tr='';
                    var tr1='<thead><tr style="color:#ffffff; background-color:#5c5858;"><th>#</th><th><?php echo JrTexto::_('Course') ?> : '+strcurso+'</th><th><?php echo JrTexto::_('Final Test'); ?></th><th><?php echo JrTexto::_('Final Test'); ?>/('+selbase+')</th></tr></thead><tbody>'; 
                    if(datos.length){
                        var tmpdata=[];
                        $.each(datos,function(i,v){
                            var prom=parseFloat(parseFloat(v.masinfo.notasalida)*parseFloat(selbase)/v.masinfo.notasalida_enbasea);
                            tr+='<tr><td class="text-center">'+(i+1)+'</td><td >'+v.masinfo.stralumno+'</td><td class="text-center">'+prom.toFixed(2)+'</td></tr>';
                            total++;
                            tr1+='<tr ><td class="text-center">'+(i+1)+'</td><td> '+v.masinfo.stralumno+'</td><td class="text-center">'+parseFloat(v.masinfo.notasalidaquiz).toFixed(2)+'/('+v.masinfo.notasalidaquiz_enbasea+')</td><td class="text-center">'+prom.toFixed(2)+'</td></tr>';
                        }) 
                    } 
                      
                     tr1='<table class="table-striped" nmaxformula="'+nmaxformula+'" id="tableimprimir" style="width: 100%">'+tr1+'</tbody></table>';

                    tr+='<tr><td></td><td colspan="2" class="text-center"><?php echo JrTexto::_("Total of records"); ?> : '+total+'</td><td style="display:none;"></td></tr>';
                    tr='<table class="table-striped" id="tableimprimir2" style="width: 100%"><thead><tr><th>#</th><th><?php echo JrTexto::_('Course') ?> : '+strcurso+'</th><th><?php echo JrTexto::_('Final Test'); ?>/('+selbase+')</th></tr></thead><tbody>'+tr+'</tbody></table>';

                   
                    $('#impresion2 .table-responsive').html(tr);
                    $('#impresion2').find('#tableimprimir2').DataTable({
                            ordering: false,                        
                            searching: false,
                            paging: false,
                            dom: 'Bfrtip',
                            buttons: [
                              {
                                extend: "excelHtml5",
                                messageTop: '',
                                title:'<?php echo JrTexto::_('Report') ?>',
                                customize: function(xlsx){
                                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                    var col=['A','B','C','D','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                                    $tr=$('#tableimprimir2 tbody').find('tr');
                                    $.each($tr,function(ir, row){
                                        $(this).find('td').each(function(ic,td){
                                            var xcol=col[ic];
                                            if(ir==0) $('row c[r="'+xcol+'1"]',sheet).attr('s',32);
                                            else{
                                                var s=ir%2==0?8:20;
                                                $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',s);
                                            }   
                                        })
                                    })
                                }
                              },{
                                extend: "pdfHtml5",
                                messageTop: '',
                                title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Final Test')." - ".date('d-m-Y'); ?>',
                                customize: function(doc){
                                    doc.pageMargins = [12,12,12,12];
                                    $tr=$('#tableimprimir2 tbody').find('tr');
                                    var ntr=$tr.length;
                                    var ntd=$tr.eq(0).find('td').length;
                                    var ncolA=[20,"*"];
                                    for(i=0;i<ntd-2;i++){
                                        ncolA.push("*");
                                    }
                                    doc.content[1].table.widths = ncolA;
                                    var tblBody = doc.content[1].table.body;
                                    $tr.each(function (ir, row){
                                        var color=((ir%2==0)?'':'#e4e1e1');
                                        if($(this).hasClass('nomcursos')){
                                            $(this).find('td').each(function(ic,td){
                                                tblBody[ir][ic].fillColor='#5c5858';
                                                tblBody[ir][ic].color="#FFFFFF";  
                                            })
                                        }
                                    })                                
                                    tblBody[ntr][0].alignment= 'left';
                                    if(_logobase64!=''){
                                        doc.content.splice(0, 0, {
                                            margin: [ 0, 0, 0, 12 ],
                                            alignment: 'center',
                                            image:_logobase64
                                        });
                                    }
                                }
                              },{
                                extend:"print",
                                title: '<?php echo JrTexto::_('Report')." ".JrTexto::_('Final Test')." - ".date('d-m-Y'); ?>',
                                customize: function(win){
                                    var body=$(win.document.body);
                                    var title=body.find('h1').first().text();
                                    if(_logobase64!=''){
                                        body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="'+_logobase64+'"/><br>'+title+'</div>');
                                    }else{
                                         body.find('h1').first().html('<div style="text-align:center; width:100%;">'+title+'</div>');
                                    }                                    
                                    body.find('th').css('background-color', '#cccccc')
                                    
                                }
                              }
                            ]                  
                    })

                    $('#impresion .table-responsive').html(tr1);
                    $('#impresion').find('#tableimprimir').DataTable({
                            ordering: false,                        
                            searching: false,
                            paging: false,
                            dom: 'Bfrtip',
                            buttons: [
                              {
                                extend: "excelHtml5",
                                messageTop: '',
                                title:'<?php echo JrTexto::_('Report') ?>',
                                customize: function(xlsx){
                                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                    var col=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                                    $tr=$('#tableimprimir tbody').find('tr');
                                    $.each($tr,function(ir, row){
                                        $(this).find('td').each(function(ic,td){
                                            var xcol=col[ic];
                                            if(ir==0) $('row c[r="'+xcol+'1"]',sheet).attr('s',32);
                                            else{
                                                var s=ir%2==0?8:20;
                                                $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',s);
                                            }   
                                        })
                                    })
                                }
                              },{
                                extend: "pdfHtml5",
                                messageTop: '',
                                title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Final Test')." - ".date('d-m-Y'); ?>',
                                customize: function(doc){
                                   doc.pageMargins = [12,12,12,12];
                                    $tr=$('#tableimprimir tbody').find('tr');
                                    var ntr=$tr.length;
                                    var ntd=$tr.eq(0).find('td').length;
                                    var ncolA=[20,100];
                                    for(i=0;i<ntd-2;i++){
                                        ncolA.push("*");
                                    }
                                    doc.content[1].table.widths = ncolA;
                                    var tblBody = doc.content[1].table.body;
                                    var ntr=$tr.length;
                                    $tr.each(function (ir, row){
                                        var color=((ir%2==0)?'':'#e4e1e1');
                                          $(this).find('td').each(function(ic,td){
                                            if(ir==0) color='#7D94D0';                                        
                                            tblBody[ir][ic].fillColor=color;                                      
                                            if(ic==0)tblBody[ir][ic].alignment= 'center';
                                        })
                                    })
                                    tblBody[ntr][0].alignment= 'center';
                                    if(_logobase64!=''){
                                        doc.content.splice(0, 0, {
                                            margin: [ 0, 0, 0, 12 ],
                                            alignment: 'center',
                                            image:_logobase64
                                        });
                                    }
                                }

                              },{
                                extend:"print",
                                title: '<?php echo JrTexto::_('Report')." ".JrTexto::_('Final Test')." - ".date('d-m-Y'); ?>',
                                customize: function(win){
                                    var body=$(win.document.body);
                                    var title=body.find('h1').first().text();
                                    if(_logobase64!=''){
                                        body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="'+_logobase64+'"/><br>'+title+'</div>');
                                    }else{
                                         body.find('h1').first().html('<div style="text-align:center; width:100%;">'+title+'</div>');
                                    }                                    
                                    body.find('th').css('background-color', '#cccccc')
                                }
                              }
                            ]                  
                    })                
                }
            }else{
                $('#impresion2 .table-responsive tbody').empty();
                $('#impresion2').find('#tableimprimir2').DataTable();
                $('#impresion .table-responsive tbody').html(tr1);
                $('#impresion').find('#tableimprimir').DataTable();
            }
        }
        var mostrarresultado=function(){
            var dataexamenes=localStorage.getItem('dataexamenes');
            var vistareporte=localStorage.getItem('vistaReporte1')||$('.changevista.active').attr('vista');
            var selbase=localStorage.getItem('notabase')||'';
            var dtarray=JSON.parse(dataexamenes);
            if(vistareporte=='table'){
                for(i_=0;i_<dtarray.length;i_++){
                    var dt=dtarray[i_];
                    if(dt==null) continue;
                    var strcurso=dt[0].strcurso!=undefined?dt[0].strcurso:'';
                   var table='<div class="container"> <div class="col-md-12 table-reponsive"><table class="table table-striped table-hover"><thead><tr class="headings"><th>#</th>';
                      table+='<th><?php echo JrTexto::_("Course") ;?>: '+strcurso+'</th><th class="text-center"><?php echo JrTexto::_("Final test") ;?>/<select id="changepuntaje"></select></th></tr></thead><tbody>';
                      var bases=[];
                      if(dt.length)
                        $.each(dt,function(i,c){
                            table+='<tr><td  style="padding:0.5ex 2ex">'+(i+1)+'</td><td  style="padding:0.5ex 1ex">'+c.stralumno+'</td>';
                            var formula=c.formula;
                           var formula=c.formula;
                            var notaentrada=c.notaentrada;
                            var notasalida=c.notasalida;
                            var notaentrada_enbasea=c.notaentrada_enbasea;
                            var notasalida_enbasea=c.notasalida_enbasea;
                            var table2='<div class="col-md-12 table-reponsive table-sm" style="display:none"><table class="table" style="margin:0px" ><thead >';
                                table2_1='';
                                table2_2='';
                                var iex=1;
                                var tex=formula.length;
                                var prom=0;
                                var de='';
                                if(formula.length)
                                    $.each(formula,function(j,f){
                                       prom+=(f.nota*f.porcentaje);
                                        de=f.puntajemax!=undefined?f.puntajemax:100;
                                        if(iex==1&&(selbase==''||selbase==null||selbase=="null")){ selbase=de;}
                                        if($.inArray(de,bases)==-1) bases.push(de);
                                        iex++;
                                    })
                                 table2_1+='<th><?php echo JrTexto::_("Final test") ;?> ('+c.notasalida_enbasea+')</th>';
                                table2_2+='<td style="padding:0.5ex" >'+notasalida+'</td>';

                                 table2+=table2_1+'</thead><tbody>'+table2_2+'</tbody></table></div>';
                                table+='<td style="padding:0.5ex 1ex" class="shownota text-center" rel="popover"  de="'+de+'" nota="'+parseFloat(notasalida).toFixed(2)+'"><h3 style="margin:0px; font-size: 1.3rem;"></h3>'+table2+'</td>';    
                                table+='</tr>';
                        })
                    table+='</tbody>';
                    table+='</table></div></div>';
                    $('#pnl-panelreporte').html(table);
                    $('[rel="popover"]').click(function(ev){return false}).popover({
                        container: 'body',
                        placement:'top',
                        trigger: 'hover',
                        html: true,
                        content: function () {
                            var clone = $($(this).find('table').clone(true)).show();
                            return clone;
                        }
                    })
                    $('#pnl-panelreporte').find("#changepuntaje").children().remove();
                    bases.sort((a,b)=>a-b);
                    $.each(bases,function(i,v){
                        if(bases.length==1&&i==0) {
                            selbase=v;
                            localStorage.setItem('notabase',v);
                        }
                        $('#pnl-panelreporte').find("#changepuntaje").append('<option values="'+v+'" '+(v==selbase?'selected="selected"':'')+'>'+v+'</option>');
                    })
                    $('#pnl-panelreporte').find("#changepuntaje").val(selbase).trigger('change');
                    $('#pnl-panelreporte table').DataTable(
                        {
                            ordering: false,                        
                            searching: false,
                            pageLength: 50,
                            info:false,
                            oLanguage: {
                                "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
                                "oPaginate": {
                                    "sFirst": "<?php echo JrTexto::_("Next page") ?>", // This is the link to the first page
                                    "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                                    "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                                    "sLast": "<?php echo JrTexto::_("Previous page") ?>" // This is the link to the last page
                                }
                            },
                                
                        }
                    );
                }
            }else if(vistareporte=='area_char'){                 
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notes')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('area',dtarray,options,divid));
            }else if(vistareporte=='bar_char'){               
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        orientation:'horizontal',
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('bar',dtarray,options,divid));
            }else if(vistareporte=='line_char'){                 
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('line',dtarray,options,divid));      
            }else if(vistareporte=='pie_char'){                
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notes')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('pie',dtarray,options,divid)); 
            }else{
                table='';
                $('#pnl-panelreporte').html(table);
            }        
            vistaimpresion();
            //console.log(vistareporte,dt);
        }

        var obtnerresultado=function(){
            var rtpalumno=localStorage.getItem('rtpalumno');
            if(rtpalumno=='ver_rpt'+verrptalumno) return mostrarresultado();
            localStorage.setItem('rtpalumno','ver_rpt'+verrptalumno);
            var data=new FormData()
            $cursos=$('select#idcurso');
            var idgrupoauladetalle=$cursos.find("option:selected").attr('idgrupoauladetalle');
            if(idgrupoauladetalle==undefined) return;
            var idcurso=$cursos.val();
            if(idcurso=='null'||idcurso==null) return;
            var idgrupoaula=$('select#idgrupoaula').val();
            if(idgrupoaula==null||idgrupoaula=='null') return;
            data.append('idgrupoauladetalle',idgrupoauladetalle);
            data.append('idcurso',idcurso);
            data.append('idgrupoaula',idgrupoaula);
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/reportes/notas',
                callback:function(rs){
                  if(rs.code==200){
                    if(rs.data.length){
                        var datos=[];
                        $.each(rs.data,function(i,v){
                             if(datos[v.idcurso]==undefined) datos[v.idcurso]=[];
                            datos[v.idcurso].push(v);
                        })
                        $('#sindatos').hide(0);
                        $('#pnl-panelreporte').show();
                        localStorage.setItem('dataexamenes',JSON.stringify(datos));
                        mostrarresultado();
                    }else{
                        $('#sindatos').show(0);
                        $('#pnl-panelreporte').hide();
                        localStorage.setItem('dataexamenes','[]');
                        $('#impresion table tbody').empty();
                        if(!$.fn.DataTable.isDataTable( '#impresion table')) {
                          $('#impresion table').DataTable();
                        }else{
                            table = $('#impresion table').DataTable();
                            table.clear();
                            table.draw();
                        }
                        $('#pnl-panelreporte  table tbody').empty();
                        if(!$.fn.DataTable.isDataTable( '#pnl-panelreporte  table')) {
                          $('#pnl-panelreporte  table').DataTable();
                        }else{
                            table = $('#pnl-panelreporte  table').DataTable();
                            table.clear();
                            table.draw();
                        }
                        $('#impresion2  table tbody').empty();
                        if(!$.fn.DataTable.isDataTable( '#impresion2  table')) {
                          $('#impresion2  table').DataTable();
                        }else{
                            table = $('#impresion2  table').DataTable();
                            table.clear();
                            table.draw();
                        }
                    }
                  }
                }
            });
        }

        var grupos=[];
        var gruposauladetalle=[];
        $('select#iddre').on('change',function(ev){  
            $sel=$('select#idugel');
            $sel.children('option:eq(0)').siblings().remove();
            if($(this).val()==''||$(this).val()=='0') {
              return $sel.trigger('change');
            }
            var fd = new FormData();
            fd.append('iddre',$(this).val()); 
            __sysAyax({
              fromdata:fd,
              url:_sysUrlBase_+'json/ugel',
              callback:function(rs){        
                var idsel=$sel.attr('idsel');
                dt=rs.data;
                $.each(dt,function(i,v){
                  $sel.append('<option value="'+v.idugel+'" '+(v.idugel==idsel?'selected="selected"':'')+ '>'+v.idugel+" : "+ v.descripcion+'</option>');
                })
                $sel.trigger('change');
            }})
        })
        $('select#idugel').on('change',function(ev){
            var $selugel=$('select#idlocal');
            $selugel.children('option:eq(0)').siblings().remove();
            if($(this).val()==''||$(this).val()=='0') {
            return $selugel.trigger('change');
            }     
            var fd = new FormData();
            fd.append('idugel',$(this).val()); 
            __sysAyax({
            fromdata:fd,
            url:_sysUrlBase_+'json/local',
            callback:function(rs){
            var idsel=$selugel.attr('idsel');
            dt=rs.data;
            $.each(dt,function(i,v){
              $selugel.append('<option value="'+v.idlocal+'" '+(v.idlocal==idsel?'selected="selected"':'')+ '>'+v.idlocal+" : "+ v.nombre+'</option>');
            })
            $selugel.trigger('change');
            }})
        })
        $('select#idlocal').on('change',function(ev){
            var $sello=$('select#idambiente');
            $sello.children('option:eq(0)').siblings().remove();
            if($(this).val()==''||$(this).val()=='0') {
              return $sello.trigger('change');
            }     
            var fd = new FormData();
            fd.append('idlocal',$(this).val()); 
            __sysAyax({
              fromdata:fd,
              url:_sysUrlBase_+'json/ambiente',
              callback:function(rs){
                var idsel=$sello.attr('idsel');
                dt=rs.data;
                $.each(dt,function(i,v){
                   $sello.append('<option value="'+v.idambiente+'" '+(v.idambiente==idsel?'selected="selected"':'')+ '>'+v.tipo_ambiente+" : "+ v.numero+'</option>');
                })
                $sello.trigger('change');
            }})
        })
        $('select#idambiente').on('change',function(ev){
            var $selg=$('select#idgrupoaula');    
            $selg.children('option:eq(0)').siblings().remove();      
            if($(this).val()=='') {
              return $selg.trigger('change');
            } 
            var fd = new FormData();
            fd.append('idlocal', $('select#idlocal').val());
            fd.append('idambiente',$(this).val()); 
            __sysAyax({
              fromdata:fd,
              url:_sysUrlBase_+'json/acad_grupoauladetalle',
              callback:function(rs){
                var idsel=$selg.attr('idsel');
                dt=rs.data;
                grupos=[];
                $.each(dt,function(i,v){
                  if(grupos[v.idgrupoaula]==undefined){
                    $selg.append('<option value="'+v.idgrupoaula+'" '+(v.idgrupoaula==idsel?'selected="selected"':'')+ '>'+v.idgrupoaula+" : "+ v.strgrupoaula+'</option>');
                    var tt=[];
                    tt[v.idgrupoauladetalle]=v;
                    grupos[v.idgrupoaula]={'nombre':v.nombre,'cursossel':tt};       
                  }else{
                    var tt=grupos[v.idgrupoaula].cursossel;
                    tt[v.idgrupoauladetalle]=v;
                     grupos[v.idgrupoaula].cursossel=tt;
                  }
                })
                $selg.trigger('change');
            }})
        })

        $('select#idgrupoaula').on('change',function(ev){
            $selcursos=$('select#idcurso');
            $selcursos.empty();
            gruposauladetalle=[];
            var _grupossel=grupos[$(this).val()]||{nombre:'',cursossel:[]};
            if(_grupossel.cursossel.length){
              $.each(_grupossel.cursossel,function(i,v){
                if(v!=undefined){
                    $cursos.append('<option value="'+v.idcurso+'" idgrupoauladetalle="'+v.idgrupoauladetalle+'">'+v.strcurso+'</option>');                   
                }
              })
            }
            $selcursos.trigger('change');
        })

        $('#idcurso').change(function(ev){
            verrptalumno++;
            obtnerresultado();
        })

        $('.changevista').click(function(ev){
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            localStorage.setItem('vistaReporte1',$(this).attr('vista'));
            mostrarresultado();
        })
        $('#pnl-panelreporte').on('change','table select#changepuntaje',function(ev){
            ev.preventDefault();
            _this=$(this);
            var val=_this.val();
            var td=$('#pnl-panelreporte table td.shownota');
            $.each(td,function(vv){
                var c=parseFloat(parseFloat($(this).attr('nota'))*parseFloat(val)/parseFloat($(this).attr('de'))).toFixed(2);
                $(this).children('h3').text(c);
            })
            localStorage.setItem('notabase',val);
            vistaimpresion();
            //ev.stopPropagation();
        }).on('change','#notabase',function(ev){
            localStorage.setItem('notabase',$(this).val());
            mostrarresultado();
        })


        $('.btnexportar_xlsx').click(function(ev){
            setTimeout(function(){$('#impresion2').find('.buttons-excel').trigger('click');},1500); 
        })
        $('.btnexportar_xlsx2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-excel').trigger('click');
        })
        $('.btnexportar_pdf').click(function(ev){
            var vistareporte=localStorage.getItem('vistaReporte1')||$('.changevista.active').attr('vista');           
            if(vistareporte=='table'){                
                setTimeout(function(){$('#impresion2').find('.buttons-pdf').trigger('click');},1500);               
            }else {
                const filename  = 'report_'+vistareporte+'.pdf';
                $('#pnl-panelreporte').children('.col-md-3').hide();
                html2canvas(document.querySelector('#pnl-panelreporte'),{scale: 1}).then(canvas => {
                    let pdf = new jsPDF({orientation: 'landscape'});//
                    pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 320, 180);
                    pdf.save(filename);
                    $('#pnl-panelreporte').children('.col-md-3').show();
                });
            }
        });
        $('.btnexportar_pdf2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-pdf').trigger('click');
        });
        $('.btnimprimir').click(function(ev){
             if(vistareporte=='table'){ 
            setTimeout(function(){$('#impresion2').find('.buttons-print').trigger('click');},1500);
             }else {
                $('#pnl-panelreporte').children('.col-md-3').hide(0).css('display','none');
                $('#pnl-panelreporte').imprimir();
                $('#pnl-panelreporte').children('.col-md-3').show(0).css('display','inline-block');
            }
        });
         $('.btnimprimir_2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-print').trigger('click');            
        });
        $('#tableimprimir').DataTable({
            ordering: false,
            searching: false,
            pageLength: 50,
            dom: 'Bfrtip',
            buttons: [
              {
                extend: "pdfHtml5",
                messageTop: '',
                title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Final Test')." - ".date('d-m-Y'); ?>',
                customize: function(doc){
                    doc.pageMargins = [12,12,12,12];
                    doc.content[1].table.widths = ["*", "*", "*"];
                    var tblBody = doc.content[1].table.body;
                    $('#tableimprimir tbody').find('tr').each(function (ir, row){
                        if($(this).hasClass('nomexamen')){
                            $(this).find('td').each(function(ic,td){
                                tblBody[ir][ic].fillColor='#e4e1e1';
                            })
                        }
                    })                 
                }
              },{
                extend: "excelHtml5",
                messageTop: '',
                title:'<?php echo JrTexto::_('Report') ?>',
                customize: function(xlsx){
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var col=['A','B','C','D','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                    $('#tableimprimir tbody').find('tr').each(function (ir, row){
                        if($(this).hasClass('nomexamen')){
                            $(this).find('td').each(function(ic,td){
                                var xcol=col[ic];
                                $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',32);
                                //tblBody[ir][ic].fillColor='#17a2b8';
                            })
                        }
                    })
                }
              }
            ]
        });
        var infoemp=__infoempresa();
        if(infoemp.tipoempresa1==1) $('#iddre').trigger('change');
        else $('#idlocal').trigger('change');
        $('#pnl-panelreporte .dt-buttons').hide();
        $('#impresion .dt-buttons').hide();
        //$('#impresion').hide();

        verrptalumno++;
        vistareporte=localStorage.getItem('vistaReporte1')||'table';
        obtnerresultado();
        setTimeout(function(){$('.changevista[vista="'+vistareporte+'"]').trigger('click')},2000);
    })


</script>
