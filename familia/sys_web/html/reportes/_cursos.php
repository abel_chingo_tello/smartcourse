<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Tareas"); ?>'
    });
    /*addButonNavBar({
      class: 'btn btn-success btn-sm Agreggarforo',
      text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('add') ?>',
      href: '<?php echo $this->documento->getUrlSitio(); ?>/foros/agregar'
    });*/
  </script>
<?php } ?>
<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
<?php } ?>
<style type="text/css">
  #tb-comentarios_wrapper>div>a.paginate_button.previous.disabled,
  #tb-comentarios_wrapper>div>a.paginate_button.next.disabled {
    padding: 3px !important;
    color: #a2a2a2 !important;
    background-color: white !important;
  }

  table.dataTable tbody tr {
    background-color: transparent;
  }

  #tb-comentarios {
    width: 100%;
  }

  td {
    padding: 0 !important;
  }

  table,
  th {
    border: none !important;
  }

  .dataTables_wrapper {
    margin-bottom: 20px;
  }

  .dataTables_length {
    font-style: italic;
  }

  .lead {
    font-size: 2rem;
    font-weight: 300;
  }

  .media,
  .mb-4 {
    background-color: #ffffff66;
    padding: 15px;
    border-radius: 10px;
  }

  .mb-4,
  .my-4 {
    margin-bottom: 1.5rem;
  }

  .media {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
  }

  .media-body {
    -ms-flex: 1;
    flex: 1;
  }

  .mr-3,
  .mx-3 {
    margin-right: 1rem;
  }

  .d-flex {
    display: -ms-flexbox;
    display: flex;
  }

  .rounded-circle {
    border-radius: 50%;
  }

  img {
    vertical-align: middle;
    border-style: none;
  }

  .mt-0,
  .my-0 {
    margin-top: 0;
  }

  .media-body>h5 {
    font-weight: 800;
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  .media:hover,
  .mb-4:hover {
    background-color: white;
  }

  .titulo-comentarios {
    margin-top: 15px;
    margin-bottom: 20px;
  }

  .my-center {
    display: flex;
    justify-content: center;
  }

  #nombre-curso {
    margin-top: 50px;
    margin-bottom: 30px;
  }

  .cont-comentarios {
    margin-top: 0;
  }

  .cont-comentario {
    padding-left: 10px;
  }

  .hide {
    display: none;
  }

  .show {
    display: block
  }

  #no-comentarios {
    font-style: italic;
  }

  @media only screen and (max-width: 390px) {
    .lead {
      font-size: 1.4rem;
    }

    .media-body>h5 {
      font-weight: 600;
      font-size: 15px;
    }
  }
</style>
<div class="row my-center">
  <div class="form-group col-sm-12 col-md-4 ">
    <label class="control-label"><?php echo JrTexto::_('Grupo'); ?></label>
    <select id="idgrupoaula" name="idgrupoaula" class="form-control">
      <?php if (!empty($this->misgrupos))
        foreach ($this->misgrupos as $fk => $v) {
          //var_dump($v);
      ?>
        <option value="<?php echo $fk; ?>"><?php echo $v["nombre"] ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group col-sm-12 col-md-4 ">
    <label class="control-label"><?php echo JrTexto::_('Curso'); ?></label>
    <select id="idcurso" name="idcurso" class="form-control" idsel="<?php echo @$this->idcurso; ?>">
      <?php if (!empty($this->gruposauladetalle))
        foreach ($this->gruposauladetalle as $fk) { ?>
        <option value="<?php echo $fk["idcurso"] ?>" data-idgrupoaula="<?php echo $fk["idgrupoaula"] ?>" data-idgrupoauladetalle="<?php echo $fk["idgrupoauladetalle"] ?>" data-idcomplementario="<?php echo $fk["idcomplementario"] ?>" style="display: none;"><?php echo $fk["strcurso"] ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="container my-center">

    <div class="col-sm-12 col-md-10 hide" id="detalle-curso">
      <div class="tabla">
        <div class="row">
          <div class="nombre-curso col-12">
            <h1 id="nombre-curso"></h1>
          </div>
        </div>
        <div class="row  text-center">
          <div class="estrellas col-6">
            <h5>Valoracion de los alumnos:</h5>
            <p class="lead text-center"><i class="fa fa-star" data-value="1"></i><i class="fa fa-star" data-value="2"></i><i class="fa fa-star" data-value="3"></i><i class="fa fa-star" data-value="4"></i><i class="fa fa-star" data-value="5"></i></p>
          </div>
          <div class="num-votaciones col-6">
            <h5>Cantidad de valoraciones:</h5>
            <p id="num-val"></p>
          </div>
        </div>
        <div class="row cont-comentarios ">
          <div class="col-12">
            <h5 class="titulo-comentarios">Comentarios:</h5>
            <div id="contenedor-tb" class="comentario col-sm-12">

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {

    var table = drawTable();

    $('select#idgrupoaula').change(function(ev) {
      $('select#idcurso').children('option').hide();
      $('select#idcurso').children('option').removeAttr("selected");
      console.log('$(this).val(): ', $(this).val());
      $('select#idcurso option[data-idgrupoaula="' + $(this).val() + '"]').show();
      $('select#idcurso option[data-idgrupoaula="' + $(this).val() + '"]').first().attr('selected', "selected");
      $('select#idcurso').trigger('change');
    })
    // ((WORKING))
    $('select#idcurso').change(function(ev) {
      clearStars();
      var idgrupoaula = $('select#idcurso option:selected').attr('data-idgrupoaula') || 0;
      var idcomplementario = $('select#idcurso option:selected').attr('data-idcomplementario') || 0;
      var idcurso = $('select#idcurso option:selected').val() || 0;
      var nombreCurso = $('select#idcurso option:selected').text()

      let data = new FormData;
      data.append('idgrupoaula', idgrupoaula)
      data.append('idcomplementario', idcomplementario)
      data.append('idcurso', idcurso)
      let url = `${_sysUrlBase_}json/valoracion`;
      fetch(url, {
        method: 'POST',
        body: data
      }).then(respuesta => {
        return respuesta.json();
      }).then(jsonRespuesta => {
        if (jsonRespuesta.code === 200 && jsonRespuesta.data.length >= 0) {
          table.destroy();
          let arrInteraccion = jsonRespuesta.data;
          console.log("data servidor: ", arrInteraccion);
          let puntuacionPromedio = 0;
          let html = ``;
          arrInteraccion.forEach(interaccion => {
            puntuacionPromedio += interaccion.puntuacion;
            if (interaccion.hasOwnProperty('comentario') && interaccion.comentario && interaccion.comentario != '') {
              html += ` 
                            <tr>
                              <td>
                                <div class="media mb-4">
                                  <!-- <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""> -->
                                  <div class="media-body">
                                    <h5 class="mt-0">${interaccion.nombre_alumno}</h5>
                                    <p class="cont-comentario">${interaccion.comentario}</p>
                                    <div class="media mt-4" style="display:none">
                                      <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                                      <div class="media-body">
                                        <h5 class="mt-0">Commenter Name 2</h5>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                            </tr>`;

            }
          });
          if (html == ``) {
            html = `<div id="no-comentarios" class="media-body media">
                    <h5>Aú no hay comentarios...</h5>
                  </div>`;
            $('#contenedor-tb').html(html);
          } else {
            let htmlTable = `<table id="tb-comentarios">
                                <thead>
                                  <tr>
                                    <th></th>
                                  </tr>
                                </thead>
                                <tbody id="lista_comentarios">
                                </tbody>
                              </table>`;
            $('#contenedor-tb').html(htmlTable);
            $('#lista_comentarios').html(html);
            table = drawTable();
          }

          puntuacionPromedio = Math.round(puntuacionPromedio / arrInteraccion.length);
          $('#num-val').html(arrInteraccion.length);
          $('#nombre-curso').html(nombreCurso);
          stars(puntuacionPromedio);

          $('#detalle-curso').removeClass("hide");
        }
      })
    })
    $('select#idgrupoaula').trigger('change');

  })

  function drawTable() {
    table = $('#tb-comentarios').DataTable({
      searching: false,
      ordering: false,
      info: false,
      paging: true,
      language: {
        search: "Buscar:",
        lengthMenu: "Mostrando los primeros _MENU_ comentarios",
        info: "Mostrando _START_ a _END_ comentarios de un total de _TOTAL_ ",
        paginate: {
          first: "Primera página",
          previous: "Anterior",
          next: "Siguiente",
          last: "Última página"
        }
      }
    });
    return table;
  }

  function stars(puntuacionPromedio) {
    for (var i = 1; i <= puntuacionPromedio; i++) {
      $('i.fa-star[data-value="' + i + '"]').css({
        color: '#F00'
      });
    }
  }

  function clearStars() {
    for (var i = 1; i <= 5; i++) {
      $('i.fa-star[data-value="' + i + '"]').css({
        color: '#555'
      });
    }
  }
</script>