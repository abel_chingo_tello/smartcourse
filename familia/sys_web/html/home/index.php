<style>
    #inicio {
        min-height: 100vh;
    }
    .my-center {
        /* max-width: 90% !important ; */
    }
    #subir>i {
        position: relative;
        top: -3px;
    }
    .form-group {
        margin-bottom: 0 !important;
    }
    .iframecontent {
        border: 0px;
        width: 100%;
        height: calc(100vh - 300px);
        margin: 0px;
        padding: 0px;
        border-top-left-radius: 0.8ex;
    }
    #consultar {
        min-height: 100vh;
        max-height: auto !important;
    }
    .fix-top {
        padding-top: 60px !important;
    }
    #foro {
        min-height: 100vh;
        max-height: auto !important;
    }
    section.boxContacto {
    height: auto !important;
    min-height: 100vh;
  }

  iframe.iframecontent {
    height: auto !important;
    min-height: 100vh;
  }
</style>

<a id="subir" class="scroll" href="#inicio">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</a>

<section class="col-12 rpt-cont" id="inicio">
    <div class="my-center">
        <div class="col-sm-12">
            <div class="my-cont">
                <div class="bd-example">
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php
                            foreach ($this->slider as $key => $value) {
                                ?>
                                <li data-target="#carouselExampleCaptions" data-slide-to="<?php echo $key; ?>" class="<?php echo ($key == 0) ? 'active' : ''; ?>"></li>
                                <?php
                            }
                            ?>
                        </ol>
                        <div class="carousel-inner">
                            <?php
                            foreach ($this->slider as $key => $value) {
                                $src = URL_BASE . 'static/media/familia-slider/' . $value["link"] . '?x=' . rand(0, 99999);
                                ?>
                                <div class="carousel-item<?php echo ($key == 0) ? ' active' : ''; ?>">
                                    <img src="<?php echo $src; ?>" class="d-block w-100" alt="Imagen Slider <?php echo $key + 1; ?>" style="width: 100%; border-radius: 0 !important; height: calc(100vh - 180px);">
                                    <!-- <div class="carousel-caption d-none d-md-block">
                                        <h5>Third slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div> -->
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Anterior</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Siguiente</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="fix-top boxContacto obsContacto" id="consultar" style="height: calc(100vh - 120px);">
    <h3 class="title my-font">Consultar</h3>
    <div class="container form">
        <form id="frmConsulta">
            <div class="form-row my-font">
                <div class="form-group col-md-4">
                    <select id="cmbtipodocumento" class="form-control" required></select>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="dni" placeholder="Documento del Alumno" maxlength="8" data-inputmask="'mask': '99999999'" required>
                </div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-control" id="codigo" placeholder="Apellido Paterno del Alumno" autocomplete="off" required>
                </div>
                <div class="form-group col-md-2">
                    <button type="submit" class="botonEnviarMensaje">Consultar</button>
                </div>
            </div>
        </form>
    </div>
    <br>
    <div class="my-center">
        <div class="col-sm-12">
            <div class="my-cont" id="contenedor">
                <div class="rpt-text my-font">Seleccione un reporte...</div>
            </div>
        </div>
    </div>
</section>
<!-- Sección Contacto -->
<section class="fix-top col-12 rpt-cont" id="foro" style="height: calc(100vh - 120px);">
    <h3 class="title my-font">Foro de Familia</h3>
    <div class="my-center">
        <div class="col-sm-12">
            <div class="my-cont">
                <form id="frmAddConsulta">
					<div class="modal fade" id="mdAddConsulta" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="mdAddConsultaTitle"></h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <input type="hidden" id="respuesta">
								<div class="modal-body">
									<div class="row">
										<div class="col-md-4">
                                            <select id="cmbtipodocumento2" class="form-control" required></select>
                                        </div>
										<div class="col-md-3">
                                            <input type="text" class="form-control" id="dni2" placeholder="Documento del Alumno" maxlength="8" data-inputmask="'mask': '99999999'" required>
                                        </div>
										<div class="col-md-5">
                                            <input type="text" class="form-control" id="codigo2" placeholder="Apellido Paterno del Alumno" autocomplete="off" required>
                                        </div>
										<div class="col-md-12">
											<p>Contenido de la Consulta <font color="red">(*)</font>
												<textarea type="text" id="txtcontenidoc" name="txtcontenidoc" class="form-control input-sm" required="" rows="3" style="resize: none;"></textarea>
											</p>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary" aria-hidden="true"><i class="fa fa-save"> </i> Guardar</button>
									<button id="btncerrarc" type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-close"> </i> Cerrar</button>
								</div>
							</div>
						</div>
					</div>
				</form>
                <div id="foro" class="card collapsed-card" style="margin-bottom: 0;">
					<div class="card-header text-center">
                        <button type="button" id="btnAddConsulta" class="btn btn-outline-primary btn-xs" data-toggle="modal" data-target="#mdAddConsulta">
                            <i class="fas fa-plus-circle"></i> Hacer una Pregunta
                        </button>
					</div>
					<div class="card-body">
						<div class="col-md-12">
							<div style="overflow-y: scroll; max-height: calc(100vh - 140px);" id="listarConsulta"></div>
						</div>
					</div>

				</div>
                <!-- <div class="rpt-text my-font">Seleccione un reporte...</div> -->
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $("#frmConsulta").submit(function(evt) {
        evt.preventDefault();
        $.post(_sysUrlBase_+'json/sesion/salir', function(data, status){if(data.code==200){ }},'json');
        
        $.post(_sysUrlBase_ + "familia/validar/apoderado", {
            tipodoc: $("#cmbtipodocumento").val(),
            dni: $("#dni").val(),
            codigo: $("#codigo").val(),
            idproyecto: "<?php echo $this->proyecto['idproyecto']; ?>",
            idempresa: "<?php echo $this->proyecto['idempresa']; ?>"
        }, function(data) {
            if (data.code === 200) {
                $("#contenedor").html(data.data);
            }
        }, 'json');
    });
    $("#cmbtipodocumento").change(function() {
        var longitud = $("#cmbtipodocumento option:selected").data("longitud");
        $("#dni").attr("maxlength", longitud).val($("#dni").val().substring(0, longitud)).attr("data-inputmask", "'mask' : '" + "9".repeat(longitud) + "'");
        $(":input").inputmask();
    });
    $("#cmbtipodocumento2").change(function() {
        var longitud = $("#cmbtipodocumento2 option:selected").data("longitud");
        $("#dni2").attr("maxlength", longitud).val($("#dni2").val().substring(0, longitud)).attr("data-inputmask", "'mask' : '" + "9".repeat(longitud) + "'");
        $(":input").inputmask();
    });
    function abrirModalAddConsulta(respuesta){
        if(respuesta == ""){
            $("#mdAddConsultaTitle").html("Agregar Pregunta");
        } else {
            $("#mdAddConsultaTitle").html("Agregar Respuesta");
        }
        $("#cmbtipodocumento2").val("1").trigger("change");
        $("#dni2").val("");
        $("#codigo2").val("");
        $("#txtcontenidoc").val("");
        $("#respuesta").val(respuesta);
    }
    $("#btnAddConsulta").click(function(){
        abrirModalAddConsulta("");
    });
    function listarConsultas() {
		$.post(_sysUrlBase_+"json/familia_foro", 
		{
			sql: "1",
			idproyecto: "<?php echo $this->proyecto['idproyecto']; ?>",
            idempresa: "<?php echo $this->proyecto['idempresa']; ?>"
		}
		,function (datosJSON) {
			if (datosJSON.code === 200) {
				var html = "",
					contador = 0,
                    ultimo,
                    style_;
				if (Object.keys(datosJSON.data).length > 0) {
					$.each(datosJSON.data, function(i, item) {
						if (item.padre === "p" && i > 0) {
							html += '</div></div>';

							html += '<div class="card-footer" data-toggle="modal" data-target="#mdAddConsulta" onclick="abrirModalAddConsulta(' + ultimo + ')" style="cursor: pointer;"><i class="fa fa-pencil"></i>&nbsp;Escribir una Respuesta</div>';

							html += '</div><br>';
						}
						if (item.padre === "p") {
							ultimo = item.codigo;
							html += '<div class="post card">';

							//preguntas
							html += '<div class="card-header"><span data-card-widget="collapse" style="cursor:pointer; font-size: 15px;"><b>' + item.persona + '</b> [' + item.fecha + ']<span class="badge badge-light ml-2 commentbox"><span class="ncomment">' + item.hijos + '</span> <i class="fa fa-commenting-o" aria-hidden="true"></i></span>';
                            html += '<p style="word-wrap: break-word;">' + item.contenido + '</p></div>';
                            style_ = '';
                            if(parseInt(item.hijos) == 0){
                                style_ = ' style="display: none;"';
                            }
							html += '<div class="card-body"' + style_ + '>';
							html += '<div style="margin-left: 20px;">';
						} else {
							//respuestas
							html += '<span style="font-size: 15px;"><b>' + item.persona + '</b> [' + item.fecha + ']</span>';
							html += '<p style="word-wrap: break-word;">' + item.contenido + '</p>';
						}
						contador++;
					});

					if (contador > 0) {
						html += '</div></div>';

						html += '<div class="card-footer" data-toggle="modal" data-target="#mdAddConsulta" onclick="abrirModalAddConsulta(' + ultimo + ')" style="cursor: pointer;"><i class="fa fa-pencil"></i>&nbsp;Escribir una Respuesta</div>';

						html += '</div><br>';
					}
					$("#listarConsulta").html(html);
				} else {
					$("#listarConsulta").html(`
						<hr>
						<div class="row justify-content-center">
							<div class="col-sm-12 text-center">
								<h4 style="color:#50555a;">¿Tienes alguna duda?. <span class="h5">Te podemos ayudar</span></h4>
							</div>
						</div>
					`);
				}
			}
		}, "json");
    }
    $("#frmAddConsulta").submit(function(evento) {
		evento.preventDefault();

		$.post(_sysUrlBase_ + "json/familia_foro/guardar", {
            tipodoc: $("#cmbtipodocumento2").val(),
            dni: $("#dni2").val(),
            codigo_: $("#codigo2").val(),
			idproyecto: "<?php echo $this->proyecto['idproyecto']; ?>",
            idempresa: "<?php echo $this->proyecto['idempresa']; ?>",
			contenido: $("#txtcontenidoc").val(),
			respuesta: $("#respuesta").val()
		}, function(resultado) {
			if (resultado.code === 200) {
				$("#btncerrarc").click();
				listarConsultas();
			} else if(resultado.code === 300){
                alert(resultado.msj);
            }
		}, 'json');
	});
	
    $(document).ready(function() {
        listarConsultas();
        $(":input").inputmask();
        $.post(_sysUrlBase_ + 'familia/validar/tipodoc', function(data) {
            if (data.code == 200) {
                if (data.data != null && Object.keys(data.data).length > 0) {
                    for (var value of data.data) {
                        $('#cmbtipodocumento').append('<option value="' + value.id + '" data-longitud="' + value.longitud + '">' + value.nombre + '</option>');
                        $('#cmbtipodocumento2').append('<option value="' + value.id + '" data-longitud="' + value.longitud + '">' + value.nombre + '</option>');
                    }
                }
            }
        }, 'json');
    });
    $(window).on('beforeunload', function(){
         $.post(_sysUrlBase_+'json/sesion/salir', function(data, status){if(data.code==200){ }},'json');
    });
</script>