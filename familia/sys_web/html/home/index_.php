<a id="subir" class="icon-arrow-up scroll" href="#linkHome"></a>
<!-- Sección Contacto -->
<section class="boxContacto obsContacto" id="linkContacto">
    <h3 class="title my-font">Consultar</h3>
    <div class="container form">
        <form id="miForm" onsubmit="return validate()">
            <div class="form-row my-font">
                <div class="form-group col-md-6">
                    <label for="dni"><b>Dni de su hijo</b></label>
                    <input type="text" class="form-control" id="dni" placeholder="DNI" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="codigo"><b>Código</b></label>
                    <input type="text" class="form-control" id="codigo" placeholder="Código" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="select-rpt"><b>Tipo de reporte</b></label>
                    <select name="rpt" required class="form-control" placeholder="País" id="select-rpt">
                        <option selected disabled value="">Seleccione un reporte...</option>
                        <option value="">Reporte Perrón 1</option>
                        <option value="">Reporte Perrón 2</option>
                        <option value="">Reporte Perrón 3</option>
                    </select>
                </div>
            </div>
            <button type="submit" class="botonEnviarMensaje">Consultar</button>
        </form>
    </div>
</section>
<section class="col-12 rpt-cont">
    <div class="row my-center">
        <div class="col-sm-12 size">
            <div class="my-cont">
                <div class="rpt-text my-font">Seleccione un reporte...</div>
            </div>
        </div>
    </div>
</section>
<!-- Sección Contacto -->


<script type="text/javascript">
    //to do
    $(document).ready(function() {
        console.log("ready");
    });
</script>