<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegFamilia_slider', RUTA_BASE);
class WebDefecto extends JrWeb
{
	private $oNegFamilia_slider;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegFamilia_slider = new NegFamilia_slider;
	}

	public function defecto(){
		try{
			global $aplicacion;
			$url=explode('.', $_SERVER['HTTP_HOST']);
			$nombreurl = array_shift($url);
			JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
			$oNegProyecto = new NegProyecto;
			$proyecto=$oNegProyecto->buscar(array('nombreurl'=>$nombreurl));
			if(empty($proyecto[0])){
				exit("La Empresa que esta intentando ingresar no existe, Valide su url");
			}
			$this->proyecto=$proyecto[0];
			$filtro_foro = array(
				"idproyecto"=>$this->proyecto['idproyecto']
				,"idempresa"=>$this->proyecto['idempresa']
			);
			$this->slider = $this->oNegFamilia_slider->buscar($filtro_foro);
			// var_dump($this->proyecto);
			$this->documento->plantilla =!empty($_REQUEST["tpl"])?$_REQUEST["tpl"]:'general';
			$this->documento->setTitulo("Seguimiento familiar", false);
			$this->esquema = 'home/index';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}