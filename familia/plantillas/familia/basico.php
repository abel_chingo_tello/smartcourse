<?php
defined('_BOOT_') or die('');
$version = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $documento->getUrlStatic();
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <jrdoc:incluir tipo="cabecera" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte</title>
    <link href="<?php echo $rutastatic; ?>/tema/paris/propio/template.css?vs=<?php echo $version; ?>" rel="stylesheet">
    <script>
        var navBarParisReturnText = "<?php echo JrTexto::_('Back'); ?>";
    </script>
    <script src="<?php echo $rutastatic; ?>/tema/paris/gentelella/jquery/dist/jquery.min.js?vs=<?php echo $version; ?>"></script>
    <script src="<?php echo $rutastatic; ?>/tema/paris/propio/template.js?vs=<?php echo $version; ?>"></script>
    <style>
        .x_panel {
            position: relative;
            width: 100%;
            margin-bottom: 10px;
            padding: 10px 17px;
            display: inline-block;
            background: #fff;
            border: 1px solid #E6E9ED;
            -moz-column-break-inside: avoid;
            column-break-inside: avoid;
            opacity: 1;
            transition: all .2s ease;
        }
        body{
            padding-top: 10px;
        }
    </style>
</head>

<body>
    <jrdoc:incluir tipo="recurso" />
    <jrdoc:incluir tipo="docsJs" />

</body>

</html>