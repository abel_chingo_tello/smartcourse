<?php 
defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';
$rutastatic=$this->documento->getUrlStatic();
// $idrol = $this->usuario["idrol"];
?>
<!-- Contenedor Header -->
<header>
    <nav id="navFixed" class="navbar fixed-top navbar-expand-lg navbar-light bg-light p-10px">
        <a class="navbar-brand center-row" href="#linkHome">
            <img src="<?php echo URL_BASE . $this->logo_emp . "?x=" . rand(0, 99999); ?>" alt="">
            <span class="edukt">Familia</span> <span class="maestro"><?php echo $this->nomempresa; ?></span><small class="dot">.</small></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul id="scrollsections-navigation" class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a class="nav-link scroll current" onclick="scroll_to('inicio')" href="#inicio">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll" onclick="scroll_to('consultar')"
                        href="#consultar">Consultar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll" onclick="scroll_to('foro')" href="#foro">Foro</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link scroll" onclick="scroll_to('linkTestimonial')"
                        href="#linkTestimonial">Testimonios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll" onclick="scroll_to('linkContacto')" href="#linkContacto">Contacto</a>
                </li> -->

            </ul>
        </div>
    </nav>
    <!-- <section id="linkHome" class="fixBox">
        <div class="boxLogo">
            <h1 class="title"><span class="edukt">Edukt</span><span class="maestro">Maestro</span><small
                    class="dot">.</small></h1>
        </div>

        <div class="boxHeader ">
            <div class="textosHeader">
                <h1><b> Enseñanza Virtual</b></h1>
                <p>Una Plataforma Virtual que permite a los
                    colegios, universidades, institutos e
                    instituciones en general, desarrollar sus
                    procesos de enseñanza y aprendizaje, con
                    su propia metodología.</p>

                <a class="botonGetStarted" onclick="scroll_to('linkFeatures')" href="#"
                    rel="nofollow noopener">Saber
                    más</a>

            </div>

            <div class="imgHeader">
                <img src="images/landing_1.png" alt="">
            </div>
        </div>
    </section> -->

</header>
<!-- Fin Contenedor Header -->

<script type="text/javascript">
  //to do
</script>