<?php
/**
 * @autor		Alvaro Alonso Cajusol Vallejos
 * @fecha		10/01/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_RAIZ') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
class Metadata extends JrModulo
{
	protected $oNegProyecto;
	protected $oNegBolsa_empresas;

	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegBolsa_empresas = new NegBolsa_empresas;
		$this->oNegProyecto = new NegProyecto;
		$this->modulo = 'metadata';
	}
	
	public function mostrar($html=null)
	{
		try {
			$url=explode('.', $_SERVER['HTTP_HOST']);
			$nombreurl = array_shift($url);
			$proyecto=$this->oNegProyecto->buscar(array('nombreurl'=>$nombreurl));
			if(empty($proyecto[0])){
				exit("La Empresa que esta intentando ingresar no existe, Valide su url");
			}
			$this->proyecto=$proyecto[0];
			$this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->proyecto["idempresa"]))[0];
			$this->nomempresa = $this->empresa["nombre"];
			$this->logo_emp = $this->empresa["logo"];

			if(empty($html)){
				$this->esquema = 'metadata';
			}else $this->esquema = $html;			
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}