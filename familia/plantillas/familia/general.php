<?php
defined('_BOOT_') or die('');
$version = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $documento->getUrlStatic();
?>
<!DOCTYPE html>
<html>

<head>
    <jrdoc:incluir tipo="modulo" nombre="metadata" />
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/fontawesome-free/css/all.min.css">
  <link href="<?php echo $rutastatic; ?>/tema/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo $rutastatic; ?>/libs/pnotify/pnotify.min.css" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- JQUERY -->
  <script type="text/javascript" src="<?php echo $rutastatic; ?>/familia/js/jquery-3.4.1.min.js"></script>
  <!-- <script src="<?php echo $documento->getUrlStatic()?>/familia/js/jquery.min.js"></script> -->
  <script src="<?php echo $rutastatic; ?>/js/funciones.js?vs=<?php echo $version; ?>"></script>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  
  <!-- BOOTSTRAP CSS -->
  <link href="<?php echo $rutastatic; ?>/familia/css/bootstrap-4.3.1.min.css" rel="stylesheet">
  <!-- ICONMOON -->
  <link rel="stylesheet" type="text/css" href="<?php echo $rutastatic; ?>/familia/css/icomoon.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $rutastatic; ?>/familia/css/estilos.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $rutastatic; ?>/familia/css/mine.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $rutastatic; ?>/familia/css/familia.css">
  <link href="<?php echo $rutastatic; ?>/libs/select2/select2.min.css" rel="stylesheet">
  <script src="<?php echo $rutastatic; ?>/libs/select2/select2.min.js"></script>
  
  <script src="<?php echo $rutastatic; ?>/js/funciones.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $rutastatic; ?>/js/smartcourse.js?vs=<?php echo $version; ?>"></script>
  <script type="text/javascript" src="<?php echo $rutastatic; ?>/libs/sweetalert/sweetalert2.all.min.js"></script>
  <script src="<?php echo $rutastatic; ?>/libs/pnotify/pnotify.min.js"></script>
  <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/poppers/dist/umd/popper.min.js?vs=<?php echo $version; ?>"></script>
  <script type="text/javascript" src="<?php echo $rutastatic; ?>/familia/js/bootstrap-4.3.1.min.js"></script>
  <!-- <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script> -->
  <script src="<?php echo $rutastatic; ?>/libs/scrollSections/plugins.js"></script>
  <script src="<?php echo $rutastatic; ?>/libs/scrollSections/modernizr-2.5.3.min.js"></script>
  <script src="<?php echo $rutastatic; ?>/libs/scrollSections/jquery.scrollSections-0.4.3.min.js"></script>
  <script type="text/javascript" src="<?php echo $rutastatic; ?>/familia/js/dataApi.js"></script>
  <script type="text/javascript" src="<?php echo $rutastatic; ?>/familia/js/app.js"></script>
  

  <script>
    var _sysUrlBase_ = '<?php echo $documento->getUrlBase() ?>';
    var sitio_url_base = _sysUrlSitio_ = '<?php echo $documento->getUrlSitio() ?>';
    var _sysIdioma_ = '<?php echo $documento->getIdioma() ?>';
    var _sysUrlStatic_ = '<?php echo $rutastatic; ?>';
    var _menus_ = new Array();
  </script>
    <jrdoc:incluir tipo="cabecera" />
  
</head>
<!-- hold-transition sidebar-mini accent-warning -->

<body class="hold-transition sidebar-mini _texto_">
  <div class="wrapper">
    <!-- Navbar -->
    <jrdoc:incluir tipo="modulo" nombre="top" />
    </jrdoc:incluir>
    <!-- /.navbar -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content">
        <jrdoc:incluir tipo="mensaje" />
        <jrdoc:incluir tipo="recurso" />
        <!-- /.content -->
      </div>
    </div>
    <jrdoc:incluir tipo="modulo" nombre="pie" />
    </jrdoc:incluir>
    <!-- /.content-wrapper -->
    <!-- Control Sidebar -->
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->
  <?php
  $configs = JrConfiguracion::get_();
  require_once(RUTA_SITIO . 'ServerxAjax.php');
  echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
  ?>
  <script type="text/javascript" src="<?php echo $rutastatic; ?>/libs/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

</body>

</html>