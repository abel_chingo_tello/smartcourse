<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       16/01/2018
 * @copyright   Copyright (C) 2009. Todos los derechos reservados.
 */
require_once(dirname(dirname(__FILE__)).'/config.php');
$__carpeta__ = explode(SD, dirname(__FILE__));
$__carpeta__ = end($__carpeta__);
$__config__ = json_decode(_CONFIG_, true)[$__carpeta__];
define('_mitema_', $__config__["tema"]);
define('_sitio_', $__carpeta__);
define('RUTA_PLANTILLAS', RUTA_BASE. _sitio_. SD . 'plantillas' . SD);
define('RUTA_TEMA', RUTA_PLANTILLAS. _mitema_.SD);
define('RUTA_SITIO', RUTA_BASE. _sitio_. SD);
define('IS_LOGIN', $__config__["is_login"]);
require_once(RUTA_LIBS . 'cls.JrCargador.php');
require_once(RUTA_LIBS . 'jrAdwen' . SD . 'jrFram.php');
require_once(RUTA_LIBS . 'jrAdwen'.SD.'documento'.SD.'cls.JrDocumento.'.'php');
try {
	JrCargador::clase('sys_inc::ConfigSitio',RUTA_BASE);
	JrCargador::clase('sys_inc::Sitio',RUTA_BASE);
	$aplicacion = Sitio::getInstancia();
} catch(Exception $e){
	echo json_encode(array('code'=>'Error','msj'=>'Imposible iniciar la aplicacion: '.$e->getMessage()));
	exit();
}