<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-09-2019
 * @copyright	Copyright (C) 27-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatUgel', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegUgel 
{
	protected $idugel;
	protected $descripcion;
	protected $abrev;
	protected $iddepartamento;
	protected $idprovincia;

	protected $idproyecto=0;
	protected $iddre=0;
	protected $dataUgel;
	protected $oDatUgel;
	private $oNegMindre;

	public function __construct()
	{
		$this->oDatUgel = new DatUgel;
		$this->oNegMindre=new NegMin_dre;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatUgel->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatUgel->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarconinsert($filtros){
		try {
			$result = $this->oDatUgel->buscar($filtros);
			if(empty($result)){
				if(!empty($filtros['descripcion']) && !empty($filtros['idproyecto'])){
					//crear el dre y obtener su id
					$this->descripcion = $filtros['descripcion'];
					$this->abrev = isset($filtros['abrev']) && !empty($filtros['abrev']) ? $filtros['abrev']: '';
					$this->iddepartamento = isset($filtros['iddepartamento']) && !empty($filtros['iddepartamento']) ? $filtros['iddepartamento']: '';;
					$this->idprovincia = isset($filtros['idprovincia']) && !empty($filtros['idprovincia']) ? $filtros['idprovincia']: '';;
					$this->idproyecto=$filtros['idproyecto'];
					$this->iddre=$filtros['iddre'];
					$id = $this->agregar();
					$result = $this->oDatUgel->buscar(array('idugel'=>$id));
				}
			}
			return $result;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatUgel->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatUgel->get($this->idugel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('ugel', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatUgel->iniciarTransaccion('neg_i_Ugel');
			$this->idugel = $this->oDatUgel->insertar($this->descripcion,$this->abrev,$this->iddepartamento,$this->idprovincia,$this->idproyecto,$this->iddre);
			$this->oDatUgel->terminarTransaccion('neg_i_Ugel');	
			return $this->idugel;
		} catch(Exception $e) {	
		    $this->oDatUgel->cancelarTransaccion('neg_i_Ugel');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('ugel', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatUgel->actualizar($this->idugel,$this->descripcion,$this->abrev,$this->iddepartamento,$this->idprovincia,$this->idproyecto,$this->iddre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Ugel', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatUgel->eliminar($this->idugel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdugel($pk){
		try {
			$this->dataUgel = $this->oDatUgel->get($pk);
			if(empty($this->dataUgel)) {
				throw new Exception(JrTexto::_("Ugel").' '.JrTexto::_("not registered"));
			}
			$this->idugel = $this->dataUgel["idugel"];
			$this->descripcion = $this->dataUgel["descripcion"];
			$this->abrev = $this->dataUgel["abrev"];
			$this->iddepartamento = $this->dataUgel["iddepartamento"];
			$this->idprovincia = $this->dataUgel["idprovincia"];
			$this->idproyecto = $this->dataUgel["idproyecto"];
			$this->iddre = $this->dataUgel["iddre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('ugel', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataUgel = $this->oDatUgel->get($pk);
			if(empty($this->dataUgel)) {
				throw new Exception(JrTexto::_("Ugel").' '.JrTexto::_("not registered"));
			}

			return $this->oDatUgel->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function importar($id,$descripcion,$abrev,$iddepartamento,$dre,$idprovincia,$iddre=0)
	{
		$dre=$this->oNegMindre->importar($iddre,$dre,'','',$this->idproyecto);
		$iddre=$dre['_Id'];		
		if(!empty($id)){
			$hay=empty($id)?$this->buscar(array('idugel'=>$id,'idproyecto'=>$this->idproyecto)):array();
			if(!empty($hay[0])) return array('_estado'=>'E','_Id'=>$hay[0]["idugel"]);
		}
		$idugel=$this->oDatUgel->insertar($descripcion,$abrev,$iddepartamento,$idprovincia,$this->idproyecto,$iddre);
		return array('_estado'=>'N','_Id'=>$idugel,'_idimport'=>@$id,'_iddre'=>$iddre);
	}

}