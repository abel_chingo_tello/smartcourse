<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatReportes_resumen', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegReportes_resumen 
{
	protected $idresumen;
	protected $idalumno;
	protected $idlocal;
	protected $fecha_modificacion;
	protected $prog_curso_A1;
	protected $prog_hab_L_A1;
	protected $prog_hab_R_A1;
	protected $prog_hab_W_A1;
	protected $prog_hab_S_A1;
	protected $prog_curso_A2;
	protected $prog_hab_L_A2;
	protected $prog_hab_R_A2;
	protected $prog_hab_W_A2;
	protected $prog_hab_S_A2;
	protected $prog_curso_B1;
	protected $prog_hab_L_B1;
	protected $prog_hab_R_B1;
	protected $prog_hab_W_B1;
	protected $prog_hab_S_B1;
	protected $prog_curso_B2;
	protected $prog_hab_L_B2;
	protected $prog_hab_R_B2;
	protected $prog_hab_W_B2;
	protected $prog_hab_S_B2;
	protected $prog_curso_C1;
	protected $prog_hab_L_C1;
	protected $prog_hab_R_C1;
	protected $prog_hab_W_C1;
	protected $prog_hab_S_C1;
	
	protected $dataReportes_resumen;
	protected $oDatReportes_resumen;	

	public function __construct()
	{
		$this->oDatReportes_resumen = new DatReportes_resumen;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatReportes_resumen->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatReportes_resumen->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatReportes_resumen->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatReportes_resumen->get($this->idresumen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('reportes_resumen', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatReportes_resumen->iniciarTransaccion('neg_i_Reportes_resumen');
			$this->idresumen = $this->oDatReportes_resumen->insertar($this->idalumno,$this->idlocal,$this->fecha_modificacion,$this->prog_curso_A1,$this->prog_hab_L_A1,$this->prog_hab_R_A1,$this->prog_hab_W_A1,$this->prog_hab_S_A1,$this->prog_curso_A2,$this->prog_hab_L_A2,$this->prog_hab_R_A2,$this->prog_hab_W_A2,$this->prog_hab_S_A2,$this->prog_curso_B1,$this->prog_hab_L_B1,$this->prog_hab_R_B1,$this->prog_hab_W_B1,$this->prog_hab_S_B1,$this->prog_curso_B2,$this->prog_hab_L_B2,$this->prog_hab_R_B2,$this->prog_hab_W_B2,$this->prog_hab_S_B2,$this->prog_curso_C1,$this->prog_hab_L_C1,$this->prog_hab_R_C1,$this->prog_hab_W_C1,$this->prog_hab_S_C1);
			$this->oDatReportes_resumen->terminarTransaccion('neg_i_Reportes_resumen');	
			return $this->idresumen;
		} catch(Exception $e) {	
		    $this->oDatReportes_resumen->cancelarTransaccion('neg_i_Reportes_resumen');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('reportes_resumen', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatReportes_resumen->actualizar($this->idresumen,$this->idalumno,$this->idlocal,$this->fecha_modificacion,$this->prog_curso_A1,$this->prog_hab_L_A1,$this->prog_hab_R_A1,$this->prog_hab_W_A1,$this->prog_hab_S_A1,$this->prog_curso_A2,$this->prog_hab_L_A2,$this->prog_hab_R_A2,$this->prog_hab_W_A2,$this->prog_hab_S_A2,$this->prog_curso_B1,$this->prog_hab_L_B1,$this->prog_hab_R_B1,$this->prog_hab_W_B1,$this->prog_hab_S_B1,$this->prog_curso_B2,$this->prog_hab_L_B2,$this->prog_hab_R_B2,$this->prog_hab_W_B2,$this->prog_hab_S_B2,$this->prog_curso_C1,$this->prog_hab_L_C1,$this->prog_hab_R_C1,$this->prog_hab_W_C1,$this->prog_hab_S_C1);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Reportes_resumen', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatReportes_resumen->eliminar($this->idresumen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdresumen($pk){
		try {
			$this->dataReportes_resumen = $this->oDatReportes_resumen->get($pk);
			if(empty($this->dataReportes_resumen)) {
				throw new Exception(JrTexto::_("Reportes_resumen").' '.JrTexto::_("not registered"));
			}
			$this->idresumen = $this->dataReportes_resumen["idresumen"];
			$this->idalumno = $this->dataReportes_resumen["idalumno"];
			$this->idlocal = $this->dataReportes_resumen["idlocal"];
			$this->fecha_modificacion = $this->dataReportes_resumen["fecha_modificacion"];
			$this->prog_curso_A1 = $this->dataReportes_resumen["prog_curso_A1"];
			$this->prog_hab_L_A1 = $this->dataReportes_resumen["prog_hab_L_A1"];
			$this->prog_hab_R_A1 = $this->dataReportes_resumen["prog_hab_R_A1"];
			$this->prog_hab_W_A1 = $this->dataReportes_resumen["prog_hab_W_A1"];
			$this->prog_hab_S_A1 = $this->dataReportes_resumen["prog_hab_S_A1"];
			$this->prog_curso_A2 = $this->dataReportes_resumen["prog_curso_A2"];
			$this->prog_hab_L_A2 = $this->dataReportes_resumen["prog_hab_L_A2"];
			$this->prog_hab_R_A2 = $this->dataReportes_resumen["prog_hab_R_A2"];
			$this->prog_hab_W_A2 = $this->dataReportes_resumen["prog_hab_W_A2"];
			$this->prog_hab_S_A2 = $this->dataReportes_resumen["prog_hab_S_A2"];
			$this->prog_curso_B1 = $this->dataReportes_resumen["prog_curso_B1"];
			$this->prog_hab_L_B1 = $this->dataReportes_resumen["prog_hab_L_B1"];
			$this->prog_hab_R_B1 = $this->dataReportes_resumen["prog_hab_R_B1"];
			$this->prog_hab_W_B1 = $this->dataReportes_resumen["prog_hab_W_B1"];
			$this->prog_hab_S_B1 = $this->dataReportes_resumen["prog_hab_S_B1"];
			$this->prog_curso_B2 = $this->dataReportes_resumen["prog_curso_B2"];
			$this->prog_hab_L_B2 = $this->dataReportes_resumen["prog_hab_L_B2"];
			$this->prog_hab_R_B2 = $this->dataReportes_resumen["prog_hab_R_B2"];
			$this->prog_hab_W_B2 = $this->dataReportes_resumen["prog_hab_W_B2"];
			$this->prog_hab_S_B2 = $this->dataReportes_resumen["prog_hab_S_B2"];
			$this->prog_curso_C1 = $this->dataReportes_resumen["prog_curso_C1"];
			$this->prog_hab_L_C1 = $this->dataReportes_resumen["prog_hab_L_C1"];
			$this->prog_hab_R_C1 = $this->dataReportes_resumen["prog_hab_R_C1"];
			$this->prog_hab_W_C1 = $this->dataReportes_resumen["prog_hab_W_C1"];
			$this->prog_hab_S_C1 = $this->dataReportes_resumen["prog_hab_S_C1"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('reportes_resumen', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataReportes_resumen = $this->oDatReportes_resumen->get($pk);
			if(empty($this->dataReportes_resumen)) {
				throw new Exception(JrTexto::_("Reportes_resumen").' '.JrTexto::_("not registered"));
			}

			return $this->oDatReportes_resumen->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}