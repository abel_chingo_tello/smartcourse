<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRub_rubrica', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRub_rubrica 
{
	protected $id_rubrica;
	protected $titulo;
	protected $descripcion;
	protected $foto;
	protected $tipo_encuestado;
	protected $opcion_publico;
	protected $autor;
	protected $tipo_letra;
	protected $tamanio_letra;
	protected $fecha_creacion;
	protected $tipo_rubrica;
	protected $puntuacion_general;
	protected $tipo_pregunta;
	protected $id_usuario;
	protected $activo;
	protected $registros;
	
	protected $dataRub_rubrica;
	protected $oDatRub_rubrica;	

	public function __construct()
	{
		$this->oDatRub_rubrica = new DatRub_rubrica;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRub_rubrica->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRub_rubrica->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRub_rubrica->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRub_rubrica->get($this->id_rubrica);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_rubrica', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRub_rubrica->iniciarTransaccion('neg_i_Rub_rubrica');
			$this->id_rubrica = $this->oDatRub_rubrica->insertar($this->titulo,$this->descripcion,$this->foto,$this->tipo_encuestado,$this->opcion_publico,$this->autor,$this->tipo_letra,$this->tamanio_letra,$this->fecha_creacion,$this->tipo_rubrica,$this->puntuacion_general,$this->tipo_pregunta,$this->id_usuario,$this->activo,$this->registros);
			$this->oDatRub_rubrica->terminarTransaccion('neg_i_Rub_rubrica');	
			return $this->id_rubrica;
		} catch(Exception $e) {	
		    $this->oDatRub_rubrica->cancelarTransaccion('neg_i_Rub_rubrica');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_rubrica', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRub_rubrica->actualizar($this->id_rubrica,$this->titulo,$this->descripcion,$this->foto,$this->tipo_encuestado,$this->opcion_publico,$this->autor,$this->tipo_letra,$this->tamanio_letra,$this->fecha_creacion,$this->tipo_rubrica,$this->puntuacion_general,$this->tipo_pregunta,$this->id_usuario,$this->activo,$this->registros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rub_rubrica', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRub_rubrica->eliminar($this->id_rubrica);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_rubrica($pk){
		try {
			$this->dataRub_rubrica = $this->oDatRub_rubrica->get($pk);
			if(empty($this->dataRub_rubrica)) {
				throw new Exception(JrTexto::_("Rub_rubrica").' '.JrTexto::_("not registered"));
			}
			$this->id_rubrica = $this->dataRub_rubrica["id_rubrica"];
			$this->titulo = $this->dataRub_rubrica["titulo"];
			$this->descripcion = $this->dataRub_rubrica["descripcion"];
			$this->foto = $this->dataRub_rubrica["foto"];
			$this->tipo_encuestado = $this->dataRub_rubrica["tipo_encuestado"];
			$this->opcion_publico = $this->dataRub_rubrica["opcion_publico"];
			$this->autor = $this->dataRub_rubrica["autor"];
			$this->tipo_letra = $this->dataRub_rubrica["tipo_letra"];
			$this->tamanio_letra = $this->dataRub_rubrica["tamanio_letra"];
			$this->fecha_creacion = $this->dataRub_rubrica["fecha_creacion"];
			$this->tipo_rubrica = $this->dataRub_rubrica["tipo_rubrica"];
			$this->puntuacion_general = $this->dataRub_rubrica["puntuacion_general"];
			$this->tipo_pregunta = $this->dataRub_rubrica["tipo_pregunta"];
			$this->id_usuario = $this->dataRub_rubrica["id_usuario"];
			$this->activo = $this->dataRub_rubrica["activo"];
			$this->registros = $this->dataRub_rubrica["registros"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rub_rubrica', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRub_rubrica = $this->oDatRub_rubrica->get($pk);
			if(empty($this->dataRub_rubrica)) {
				throw new Exception(JrTexto::_("Rub_rubrica").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRub_rubrica->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}