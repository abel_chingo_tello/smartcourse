<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNegReportesgeneral', RUTA_BASE, 'sys_datos');

class NegReportesgeneral
{
    protected $oDatNegReportesgeneral;

    public function __construct()
	{
        $this->oDatNegReportesgeneral = new DatNegReportesgeneral;
    }
    public function buscar($params = []){
        try{
            return $this->oDatNegReportesgeneral->buscar($params);
        }catch(Exception $e){
            throw new Exception("Un error en buscar ".$e->getMessage());
        }
    }
    /**
     * Funcion que procede a insertar un nuevor registro en la tabla reportesgeneral
     * @param Array parametros para busqueda de la base de datos
     * @return Array Resultado de las habilidades de los examenes
     */
    public function insertar($params){
        try{
            if(empty($params)){ throw new Exception("Se requiere parametros para insertar"); }
            return $this->oDatNegReportesgeneral->insertar($params);
        }catch(Exception $e){
            throw new Exception("Un error en getProgresoHabilidadExamen ".$e->getMessage());
        }
    }
}