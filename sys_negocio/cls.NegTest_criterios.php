<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		29-01-2019
 * @copyright	Copyright (C) 29-01-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTest_criterios', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTest_criterios 
{
	protected $idtestcriterio;
	protected $idtest;
	protected $criterio;
	protected $mostrar;
	
	protected $dataTest_criterios;
	protected $oDatTest_criterios;	

	public function __construct()
	{
		$this->oDatTest_criterios = new DatTest_criterios;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTest_criterios->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTest_criterios->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTest_criterios->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTest_criterios->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTest_criterios->get($this->idtestcriterio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test_criterios', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatTest_criterios->iniciarTransaccion('neg_i_Test_criterios');
			$this->idtestcriterio = $this->oDatTest_criterios->insertar($this->idtest,$this->criterio,$this->mostrar);
			$this->oDatTest_criterios->terminarTransaccion('neg_i_Test_criterios');	
			return $this->idtestcriterio;
		} catch(Exception $e) {	
		    $this->oDatTest_criterios->cancelarTransaccion('neg_i_Test_criterios');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test_criterios', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatTest_criterios->actualizar($this->idtestcriterio,$this->idtest,$this->criterio,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatTest_criterios->cambiarvalorcampo($this->idtestcriterio,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Test_criterios', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTest_criterios->eliminar($this->idtestcriterio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtestcriterio($pk){
		try {
			$this->dataTest_criterios = $this->oDatTest_criterios->get($pk);
			if(empty($this->dataTest_criterios)) {
				throw new Exception(JrTexto::_("Test_criterios").' '.JrTexto::_("not registered"));
			}
			$this->idtestcriterio = $this->dataTest_criterios["idtestcriterio"];
			$this->idtest = $this->dataTest_criterios["idtest"];
			$this->criterio = $this->dataTest_criterios["criterio"];
			$this->mostrar = $this->dataTest_criterios["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('test_criterios', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTest_criterios = $this->oDatTest_criterios->get($pk);
			if(empty($this->dataTest_criterios)) {
				throw new Exception(JrTexto::_("Test_criterios").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTest_criterios->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}