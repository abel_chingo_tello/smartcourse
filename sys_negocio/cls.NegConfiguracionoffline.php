<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatConfiguracionoffline', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegConfiguracionoffline 
{
	protected $idconfoffline;
	protected $nombre;
	protected $valor;
	
	protected $dataConfiguracionoffline;
	protected $oDatConfiguracionoffline;	

	public function __construct()
	{
		$this->oDatConfiguracionoffline = new DatConfiguracionoffline;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatConfiguracionoffline->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatConfiguracionoffline->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatConfiguracionoffline->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatConfiguracionoffline->get($this->idconfoffline);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('configuracionoffline', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatConfiguracionoffline->iniciarTransaccion('neg_i_Configuracionoffline');
			$this->idconfoffline = $this->oDatConfiguracionoffline->insertar($this->nombre,$this->valor);
			$this->oDatConfiguracionoffline->terminarTransaccion('neg_i_Configuracionoffline');	
			return $this->idconfoffline;
		} catch(Exception $e) {	
		    $this->oDatConfiguracionoffline->cancelarTransaccion('neg_i_Configuracionoffline');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('configuracionoffline', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatConfiguracionoffline->actualizar($this->idconfoffline,$this->nombre,$this->valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Configuracionoffline', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatConfiguracionoffline->eliminar($this->idconfoffline);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdconfoffline($pk){
		try {
			$this->dataConfiguracionoffline = $this->oDatConfiguracionoffline->get($pk);
			if(empty($this->dataConfiguracionoffline)) {
				throw new Exception(JrTexto::_("Configuracionoffline").' '.JrTexto::_("not registered"));
			}
			$this->idconfoffline = $this->dataConfiguracionoffline["idconfoffline"];
			$this->nombre = $this->dataConfiguracionoffline["nombre"];
			$this->valor = $this->dataConfiguracionoffline["valor"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('configuracionoffline', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataConfiguracionoffline = $this->oDatConfiguracionoffline->get($pk);
			if(empty($this->dataConfiguracionoffline)) {
				throw new Exception(JrTexto::_("Configuracionoffline").' '.JrTexto::_("not registered"));
			}

			return $this->oDatConfiguracionoffline->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}