<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatReportes', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz',RUTA_BASE,'sys_negocio');

require_once (RUTA_BASE."static".SD."libs".SD.'dompdf'.SD.'lib'.SD.'html5lib'.SD.'Parser.php');
require_once (RUTA_BASE."static".SD."libs".SD.'dompdf'.SD.'lib'.SD.'php-font-lib'.SD.'src'.SD.'FontLib'.SD.'Autoloader.php');
require_once (RUTA_BASE."static".SD."libs".SD.'dompdf'.SD.'lib'.SD.'php-svg-lib'.SD.'src'.SD.'autoload.php');
require_once (RUTA_BASE."static".SD."libs".SD.'dompdf'.SD.'src'.SD.'Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

class NegReportes
{
    protected $dompdf;

    public function __construct()
	{
		$this->oDatReportes = new DatReportes;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegReportealumno = new NegReportealumno;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegNotas_quiz = new NegNotas_quiz;
        $this->dompdf = new Dompdf();

    }

    public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
    }
    private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatReportes->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
    }////////// Fin - Metodos magicos //////////
    public function alumnosNotas($filtros){
        try{
            $arr = array();
            $r = $this->oDatReportes->examenes($filtros);
            if(!empty($r)){
                $alumnos = array();
                foreach($r as $value){
                    if(in_array($value['idalumno'],$alumnos)) continue;
                    $alumnos[] = $value['idalumno'];
                    $prepare = array();
                    $keys = array_keys(array_column($r,'idalumno'),$value['idalumno']);
                    $prepare['nombre'] = $value['strnombre'];
                    $prepare['entrada'] = null;
                    $prepare['salida'] = null;
                    foreach($keys as $llave){
                        $r[$llave]['preguntas'] = str_replace('__xRUTABASEx__',$filtros['urlBase'],$r[$llave]['preguntas']);
                        $prepare['entrada'] = intval($r[$llave]['orden']) == 1 ? $r[$llave] : $prepare['entrada'];
                        $prepare['salida'] = intval($r[$llave]['orden']) != 1 ? $r[$llave] : $prepare['salida'];
                    }
                    $arr[] = $prepare;
                }
            }
            return $arr;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
	}
	public function alumnosNotasAutoevaluacion($filtros){
		try{
			$arr = array();
			$r = $this->oDatReportes->examenAutoevaluacion($filtros);
			$sesions = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso'=>$filtros['idcurso']));
			foreach($sesions as $key => $value){
				$jsonEvaluate = json_decode($value['txtjson'],true); 
				if(!isset($jsonEvaluate['options'])){
					unset($sesions[$key]);
				}
			}
			if(!empty($r)){
                $alumnos = array();
                foreach($r as $value){
                    if(in_array($value['idalumno'],$alumnos)) continue;
                    $alumnos[] = $value['idalumno'];
                    $prepare = array();
                    $keys = array_keys(array_column($r,'idalumno'),$value['idalumno']);
                    $prepare['nombre'] = $value['strnombre'];
                    $prepare['sesion'] = null;
                    foreach($keys as $llave){
                        $r[$llave]['preguntas'] = str_replace('__xRUTABASEx__',$filtros['urlBase'],$r[$llave]['preguntas']);
						$arrSearch = array_search($r[$llave]['orden'],array_column($sesions,'orden'));
						$sesionName = ($arrSearch === false) ? 'unknown' : $sesions[$arrSearch]['nombre']; 
						$prepare['sesion'][] = array('nombre'=>$sesionName,'examen'=>$r[$llave]);
                    }
                    $arr['alumnos'][] = $prepare;
				}
            }
			$arr['sesiones'] = $sesions;
			return $arr;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function alumnosProgresos($filtros){
		try{
			$arr = array();
			$alumnos = $this->oDatReportes->matricula($filtros);
			$sesions = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso'=>$filtros['idcurso']));
			$arr['alumnos'] = null;
			$arr['sesiones'] = $sesions;

			if(!empty($alumnos)){
				
				foreach($alumnos as $valueAlumno){
					$inCurso = $this->oNegMatricula->buscar(array('idalumno' => $valueAlumno['idalumno'],'idgrupoaula'=>$filtros['idgrupoaula'], 'estado' => 1, "idproyecto" => $filtros['idproyecto'],'idcurso' =>$filtros['idcurso']));
					if(!empty($inCurso)){
						$totalCurso = 0;
						$progresos = null;
						$totalCurso = $this->oNegReportealumno->getTotalCurso(array('idcurso'=>$filtros['idcurso'],'idalumno'=>$valueAlumno['idalumno'],'fechaDesde'=>$filtros['fechaDesde'],'fechaHasta'=>$filtros['fechaHasta']));
						foreach($sesions as $value){
							$p = 0;
							$jsonParse = json_decode($value['txtjson'],true);
							if(isset($jsonParse['link']) && !isset($jsonParse['options'])){
								$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
								$idexamen = explode('&',$parte)[0];
								$objExamen = $this->oNegNotas_quiz->buscar(array('idrecurso'=>$idexamen,'idalumno'=>$valueAlumno['idalumno'],'idproyecto'=>$filtros['idproyecto'],'fechaDesde'=>$filtros['fechaDesde'],'hastafecha'=>$filtros['fechaHasta']));
								$p = (!empty($objExamen)) ? 100 : 0;
							}else{
								$p = $this->oNegReportealumno->getprogreso(array('idcurso'=>$filtros['idcurso'],'idalumno'=>$valueAlumno['idalumno'],'idsesion'=>$value['idrecurso'],'fechaDesde'=>$filtros['fechaDesde'],'fechaHasta'=>$filtros['fechaHasta']));
							}
							$progresos[] = array('idsesion'=>$value['idrecurso'],'sesion'=> $value['nombre'],'progreso'=> $p);
						}
						$arr['alumnos'][] = array('alumno'=>$valueAlumno['nombre'],'total'=>$totalCurso,'progresos'=> $progresos);
					}
				}
			}			
            return $arr;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function alumnosculminados($filtros){
		try{
			$resultado = array();
			//obtener sesions
			$sesions = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso'=>$filtros['idcurso']));
			$idexamen = 0;
			$key_sesion_examen_final = array_search(count($sesions),array_column($sesions,'orden'));

			if($key_sesion_examen_final != null && $key_sesion_examen_final !== false ){
				$jsonParse = json_decode($sesions[$key_sesion_examen_final]['txtjson'],true);
				if(isset($jsonParse['link']) && !isset($jsonParse['options'])){
					$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
					$idexamen = explode('&',$parte)[0];
				}else{
					throw new Exception("el curso no tiene examen final");					
				}
			}
			$fechaHasta = (!empty($filtros['fechaHasta'])) ? $filtros['fechaHasta'] : null;
			//obtener los alumnos
			$alumnos = $this->oDatReportes->matricula($filtros);
			if(!empty($alumnos)){
				foreach($alumnos as $alumno){
					$totalCurso = $this->oNegReportealumno->getTotalCurso(array('idcurso'=>$filtros['idcurso'],'idalumno'=>$alumno['idalumno']));
					if($totalCurso > 99){
						//buscar el examen final del alumno respecto a la fecha
						$objExamen = $this->oNegNotas_quiz->buscar(array('idrecurso'=>$idexamen,'idalumno'=>$alumno['idalumno'],'idproyecto'=>$filtros['idproyecto'],'fechaDesde'=>$filtros['fechaDesde'],'hastafecha'=>$fechaHasta));
						if(!empty($objExamen)){
							$fechita = $objExamen[0]['regfecha'];
							$resultado[] = array('nombre'=>$alumno['nombre'],'fecha'=>$fechita,'progreso'=>$totalCurso);		
						}
					}
				}
			}
			//comprobar si ha concluido el curso 
			//procesar datos para enviar
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function makeXLS($dir,$data){
        try{
            $pathfile = false;
            return $pathfile;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public function makePDF($_namefile = 'pdf',$_data = '<html><body><table border="1"><thead><tr><th>column1</th><th>column2</th><th>column3</th></tr></thead><tbody><tr><td>valor1</td> <td>valor2</td> <td>valor3</td></tr<></tbody></table></body></html>'){
        try{
            $pathfile = false;

            // instantiate and use the dompdf class
            $this->dompdf = new Dompdf();
            $this->dompdf->loadHtml($_data);
            
            // (Optional) Setup the paper size and orientation
            $this->dompdf->setPaper('A4', 'landscape');
            
            // Render the HTML as PDF
            $this->dompdf->render();
            // Output the generated PDF to Browser
			$output = $this->dompdf->output();
			if(!empty($output)){
				$namefile = $_namefile.date('Y_m_d_i_s').rand(0,100).'.pdf';
				if(file_put_contents(RUTA_BASE."sys_temp".SD.$namefile,$output)){
					$pathfile = RUTA_BASE."sys_temp".SD.$namefile;
				}
			}
            return $pathfile;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
}
?>