<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatModulos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegModulos
{
	protected $idmodulo;
	protected $nombre;
	protected $icono;
	protected $link;
	protected $estado;

	protected $dataModulos;
	protected $oDatModulos;

	public function __construct()
	{
		$this->oDatModulos = new DatModulos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatModulos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			$arrModulosPadre = $this->oDatModulos->buscar($filtros);
			// $arrModulosHijos = array();
			foreach ($arrModulosPadre as $key => $value) {
				if ($value["espadre"] != 0) {
					$arrModulosPadre[$key]["arrhijos"] = array();
					$arrTmp= $this->oDatModulos->buscarModulosHijos($value["idmodulo"], $filtros);
					foreach ($arrTmp as $keyy => $valuee) {
						//  print_r($arrModulosPadre[$key]);
						$nombretmp=str_replace('Mensajes','Messages',$valuee["nombre"]);
						$nombretmp=str_replace('Video Conferencia','Video conference',$nombretmp);
						$nombretmp=str_replace('Foro','Forum',$nombretmp);
						$nombretmp=str_replace('Reporte','Report',$nombretmp);
						$nombretmp=str_replace('Asignar Exámenes','Assign Exams',$nombretmp);
						$nombretmp=str_replace('Categorías','Categories',$nombretmp);
						$nombretmp=str_replace('Menú Empresa','Company menu',$nombretmp);
						$nombretmp=str_replace('Menú','Menu',$nombretmp);
						$arrModulosPadre[$key]["arrhijos"][] = array_replace($valuee, array("nombretraducido" => ucfirst(JrTexto::_($nombretmp))));
					}
					// $arrModulosPadre[$key]["arrhijos"]= $this->oDatModulos->buscarModulosHijos($value["idmodulo"], $filtros);
				}
			}
			return $arrModulosPadre;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatModulos->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatModulos->get($this->idmodulo);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('modulos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatModulos->iniciarTransaccion('neg_i_Modulos');
			$this->idmodulo = $this->oDatModulos->insertar($this->nombre, $this->icono, $this->link, $this->estado);
			$this->oDatModulos->terminarTransaccion('neg_i_Modulos');
			return $this->idmodulo;
		} catch (Exception $e) {
			$this->oDatModulos->cancelarTransaccion('neg_i_Modulos');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('modulos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatModulos->actualizar($this->idmodulo, $this->nombre, $this->icono, $this->link, $this->estado);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Modulos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatModulos->eliminar($this->idmodulo);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmodulo($pk)
	{
		try {
			$this->dataModulos = $this->oDatModulos->get($pk);
			if (empty($this->dataModulos)) {
				throw new Exception(JrTexto::_("Modulos") . ' ' . JrTexto::_("not registered"));
			}
			$this->idmodulo = $this->dataModulos["idmodulo"];
			$this->nombre = $this->dataModulos["nombre"];
			$this->icono = $this->dataModulos["icono"];
			$this->link = $this->dataModulos["link"];
			$this->estado = $this->dataModulos["estado"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('modulos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataModulos = $this->oDatModulos->get($pk);
			if (empty($this->dataModulos)) {
				throw new Exception(JrTexto::_("Modulos") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatModulos->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
