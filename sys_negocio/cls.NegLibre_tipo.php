<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-04-2018
 * @copyright	Copyright (C) 17-04-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatLibre_tipo', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegLibre_tipo 
{
	protected $idtipo;
	protected $nombre;
	protected $tipo_contenido;
	
	protected $dataLibre_tipo;
	protected $oDatLibre_tipo;	

	public function __construct()
	{
		$this->oDatLibre_tipo = new DatLibre_tipo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatLibre_tipo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatLibre_tipo->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatLibre_tipo->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatLibre_tipo->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatLibre_tipo->get($this->idtipo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('libre_tipo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatLibre_tipo->iniciarTransaccion('neg_i_Libre_tipo');
			$this->idtipo = $this->oDatLibre_tipo->insertar($this->nombre,$this->tipo_contenido);
			$this->oDatLibre_tipo->terminarTransaccion('neg_i_Libre_tipo');	
			return $this->idtipo;
		} catch(Exception $e) {	
		    $this->oDatLibre_tipo->cancelarTransaccion('neg_i_Libre_tipo');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('libre_tipo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatLibre_tipo->actualizar($this->idtipo,$this->nombre,$this->tipo_contenido);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Libre_tipo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatLibre_tipo->eliminar($this->idtipo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtipo($pk){
		try {
			$this->dataLibre_tipo = $this->oDatLibre_tipo->get($pk);
			if(empty($this->dataLibre_tipo)) {
				throw new Exception(JrTexto::_("Libre_tipo").' '.JrTexto::_("not registered"));
			}
			$this->idtipo = $this->dataLibre_tipo["idtipo"];
			$this->nombre = $this->dataLibre_tipo["nombre"];
			$this->tipo_contenido = $this->dataLibre_tipo["tipo_contenido"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('libre_tipo', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataLibre_tipo = $this->oDatLibre_tipo->get($pk);
			if(empty($this->dataLibre_tipo)) {
				throw new Exception(JrTexto::_("Libre_tipo").' '.JrTexto::_("not registered"));
			}

			return $this->oDatLibre_tipo->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}