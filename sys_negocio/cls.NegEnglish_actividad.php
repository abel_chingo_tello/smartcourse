<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-03-2017
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEnglish_actividad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_datos::DatGeneral', RUTA_BASE, 'sys_datos');

class NegEnglish_actividad
{
	
	protected $dataExamenes;
	protected $oDatEnglish_actividad;

	public function __construct($BD='')
	{
		$this->oDatEnglish_actividad = new DatEnglish_actividad($BD);
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			$this->oDatEnglish_actividad->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function nintentos($idcurso){
		return 3;
 		/*$usuarioAct = NegSesion::getUsuario();
 		$data=$this->oDatGeneral->buscar(array('nombre'=>$idcurso,'tipo_tabla'=>'DBYnintento_P'.$usuarioAct["idproyecto"]));
 		if(!empty($data[0])){ 			
 			return $data[0]["codigo"]; // numero de intentos DBY 			
 		} return 3;*/
 	}

 	public function fullActividades($filtros=null){
 		$fullActividades=array();
 		$fullEjercicios=array();
 		$condact = array();
 		$filtromet=array();
 		$filtrodet=array();
 		if(!empty($filtros["idactividad"])){
 			$condact["idactividad"]=$filtros["idactividad"];
 		}
 		if(!empty($filtros["nivel"])){
 			$condact["nivel"]=$filtros["nivel"];
 		}
 		if(!empty($filtros["unidad"])){
 			$condact["unidad"]=$filtros["unidad"];
 		}
 		if(!empty($filtros["sesion"])){
 			$condact["sesion"]=$filtros["sesion"];
 		}
 		if(!empty($filtros["idmetodologia"])){
 			$filtromet["idmetodologia"]=$filtros["idmetodologia"];
 		}
 		if(!empty($filtros["idhabilidad"])){
 			$filtrodet["idhabilidad"]=$filtros["idhabilidad"];
 		}
 		if(!empty($filtros["titulodetalle"])){
 			$filtrodet["titulo"]=$filtros["titulodetalle"];
 		}
 		$filtromet['tipo']='M';
 		$metodologias=$this->oDatEnglish_actividad->buscarmetodologia($filtromet);
 		$idet=0;
 		if(!empty($metodologias))
 			foreach ($metodologias as $vmet){
				$condact["metodologia"]=$vmet["idmetodologia"];   //parametro adicional para leer las actividades
				$actividad=$this->oDatEnglish_actividad->buscar($condact);
				if(!empty($actividad)){
					$fullActividades[$vmet["idmetodologia"]]["met"]=$vmet;
					$fullActividades[$vmet["idmetodologia"]]["act"]=$actividad[0];
					foreach ($actividad as $value){
						$filtrodet['idactividad']=$value["idactividad"];
						$this->oDatEnglish_actividad->setLimite(0,100000000);
						$act_det=$this->oDatEnglish_actividad->buscardetalle($filtrodet);
						if(!empty($act_det)){
							$idet++;
							$fullActividades[$vmet["idmetodologia"]]["acts"][$value["idactividad"]]=$act_det;
							$fullActividades[$vmet["idmetodologia"]]["act"]["det"]=$act_det;
							$fullEjercicios[]=$act_det;
						}
					}
					unset($actividad);
				}
			}
		if(!empty($filtros["ejercicios"])){
			return $fullEjercicios;
		}else{
			return $fullActividades;
		}
	}

	public function buscarmetodologia($filtros=array())
	{
		try {
			return $this->oDatEnglish_actividad->buscarmetodologia($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}