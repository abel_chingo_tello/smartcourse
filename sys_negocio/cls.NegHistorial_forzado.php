<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatHistorial_forzado', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegHistorial_forzado 
{
	protected $idhisesion;
	protected $idhistorialsin;
	protected $nombre_archivo;
	protected $fechaentrada;
	protected $estadoexp;
	protected $estadoimp;
	protected $tabla;
	protected $fechamodificacion;
	protected $completo;
	protected $contactualizador;
	protected $estadoimpac;
	
	protected $dataHistorial_forzado;
	protected $oDatHistorial_forzado;	

	public function __construct()
	{
		$this->oDatHistorial_forzado = new DatHistorial_forzado;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatHistorial_forzado->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatHistorial_forzado->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatHistorial_forzado->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatHistorial_forzado->get($this->idhisesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_forzado', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatHistorial_forzado->iniciarTransaccion('neg_i_Historial_forzado');
			$this->idhisesion = $this->oDatHistorial_forzado->insertar($this->idhistorialsin,$this->nombre_archivo,$this->fechaentrada,$this->estadoexp,$this->estadoimp,$this->tabla,$this->fechamodificacion,$this->completo,$this->contactualizador,$this->estadoimpac);
			$this->oDatHistorial_forzado->terminarTransaccion('neg_i_Historial_forzado');	
			return $this->idhisesion;
		} catch(Exception $e) {	
		    $this->oDatHistorial_forzado->cancelarTransaccion('neg_i_Historial_forzado');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_forzado', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatHistorial_forzado->actualizar($this->idhisesion,$this->idhistorialsin,$this->nombre_archivo,$this->fechaentrada,$this->estadoexp,$this->estadoimp,$this->tabla,$this->fechamodificacion,$this->completo,$this->contactualizador,$this->estadoimpac);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Historial_forzado', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatHistorial_forzado->eliminar($this->idhisesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdhisesion($pk){
		try {
			$this->dataHistorial_forzado = $this->oDatHistorial_forzado->get($pk);
			if(empty($this->dataHistorial_forzado)) {
				throw new Exception(JrTexto::_("Historial_forzado").' '.JrTexto::_("not registered"));
			}
			$this->idhisesion = $this->dataHistorial_forzado["idhisesion"];
			$this->idhistorialsin = $this->dataHistorial_forzado["idhistorialsin"];
			$this->nombre_archivo = $this->dataHistorial_forzado["nombre_archivo"];
			$this->fechaentrada = $this->dataHistorial_forzado["fechaentrada"];
			$this->estadoexp = $this->dataHistorial_forzado["estadoexp"];
			$this->estadoimp = $this->dataHistorial_forzado["estadoimp"];
			$this->tabla = $this->dataHistorial_forzado["tabla"];
			$this->fechamodificacion = $this->dataHistorial_forzado["fechamodificacion"];
			$this->completo = $this->dataHistorial_forzado["completo"];
			$this->contactualizador = $this->dataHistorial_forzado["contactualizador"];
			$this->estadoimpac = $this->dataHistorial_forzado["estadoimpac"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('historial_forzado', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataHistorial_forzado = $this->oDatHistorial_forzado->get($pk);
			if(empty($this->dataHistorial_forzado)) {
				throw new Exception(JrTexto::_("Historial_forzado").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHistorial_forzado->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}