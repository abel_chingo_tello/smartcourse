<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRecursos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRecursos 
{
	protected $idrecurso;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $idpersonal;
	protected $titulo;
	protected $texto;
	protected $caratula;
	protected $orden;
	protected $tipo;
	protected $publicado;
	protected $compartido;
	
	protected $dataRecursos;
	protected $oDatRecursos;	

	public function __construct()
	{
		$this->oDatRecursos = new DatRecursos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRecursos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRecursos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRecursos->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRecursos->get($this->idrecurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRecursos->iniciarTransaccion('neg_i_Recursos');
			$this->idrecurso = $this->oDatRecursos->insertar($this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->titulo,$this->texto,$this->caratula,$this->orden,$this->tipo,$this->publicado,$this->compartido);
			$this->oDatRecursos->terminarTransaccion('neg_i_Recursos');	
			return $this->idrecurso;
		} catch(Exception $e) {	
		    $this->oDatRecursos->cancelarTransaccion('neg_i_Recursos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRecursos->actualizar($this->idrecurso,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->titulo,$this->texto,$this->caratula,$this->orden,$this->tipo,$this->publicado,$this->compartido);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Recursos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRecursos->eliminar($this->idrecurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecurso($pk){
		try {
			$this->dataRecursos = $this->oDatRecursos->get($pk);
			if(empty($this->dataRecursos)) {
				throw new Exception(JrTexto::_("Recursos").' '.JrTexto::_("not registered"));
			}
			$this->idrecurso = $this->dataRecursos["idrecurso"];
			$this->idnivel = $this->dataRecursos["idnivel"];
			$this->idunidad = $this->dataRecursos["idunidad"];
			$this->idactividad = $this->dataRecursos["idactividad"];
			$this->idpersonal = $this->dataRecursos["idpersonal"];
			$this->titulo = $this->dataRecursos["titulo"];
			$this->texto = $this->dataRecursos["texto"];
			$this->caratula = $this->dataRecursos["caratula"];
			$this->orden = $this->dataRecursos["orden"];
			$this->tipo = $this->dataRecursos["tipo"];
			$this->publicado = $this->dataRecursos["publicado"];
			$this->compartido = $this->dataRecursos["compartido"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('recursos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRecursos = $this->oDatRecursos->get($pk);
			if(empty($this->dataRecursos)) {
				throw new Exception(JrTexto::_("Recursos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRecursos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}