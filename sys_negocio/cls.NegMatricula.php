<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMatricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegMatricula 
{
	protected $idmatricula;
	protected $idalumno;
	protected $idactividad;
	protected $fechamatricula;
	protected $estado;
	protected $regusuario;
	protected $regfecha;
	
	protected $dataMatricula;
	protected $oDatMatricula;	

	public function __construct()
	{
		$this->oDatMatricula = new DatMatricula;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMatricula->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMatricula->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMatricula->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMatricula->get($this->idmatriculaalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('matricula', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatMatricula->iniciarTransaccion('neg_i_Matricula');
			$this->idmatriculaalumno = $this->oDatMatricula->insertar($this->idmatricula,$this->idalumno,$this->idactividad,$this->fechamatricula,$this->estado,$this->regusuario,$this->regfecha);
			$this->oDatMatricula->terminarTransaccion('neg_i_Matricula');	
			return $this->idmatriculaalumno;
		} catch(Exception $e) {	
		    $this->oDatMatricula->cancelarTransaccion('neg_i_Matricula');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('matricula', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatMatricula->actualizar($this->idmatriculaalumno,$this->idmatricula,$this->idalumno,$this->idactividad,$this->fechamatricula,$this->estado,$this->regusuario,$this->regfecha);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Matricula', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatMatricula->eliminar($this->idmatriculaalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmatriculaalumno($pk){
		try {
			$this->dataMatricula = $this->oDatMatricula->get($pk);
			if(empty($this->dataMatricula)) {
				throw new Exception(JrTexto::_("Matricula").' '.JrTexto::_("not registered"));
			}
			$this->idmatricula = $this->dataMatricula["idmatricula"];
			$this->idalumno = $this->dataMatricula["idalumno"];
			$this->idactividad = $this->dataMatricula["idactividad"];
			$this->fechamatricula = $this->dataMatricula["fechamatricula"];
			$this->estado = $this->dataMatricula["estado"];
			$this->regusuario = $this->dataMatricula["regusuario"];
			$this->regfecha = $this->dataMatricula["regfecha"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('matricula', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataMatricula = $this->oDatMatricula->get($pk);
			if(empty($this->dataMatricula)) {
				throw new Exception(JrTexto::_("Matricula").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMatricula->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}