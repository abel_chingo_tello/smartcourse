<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-03-2017
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEnglish_herramientas', RUTA_BASE);
class NegEnglish_herramientas
{
	
	protected $dataExamenes;
	protected $oDatEnglish_niveles;

	public function __construct($BD='')
	{
		$this->oDatEnglish_herramientas = new DatEnglish_herramientas($BD);
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			$this->oDatEnglish_herramientas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatEnglish_herramientas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function nintentos($idcurso){
		return 3;
 		/*$usuarioAct = NegSesion::getUsuario();
 		$data=$this->oDatGeneral->buscar(array('nombre'=>$idcurso,'tipo_tabla'=>'DBYnintento_P'.$usuarioAct["idproyecto"]));
 		if(!empty($data[0])){ 			
 			return $data[0]["codigo"]; // numero de intentos DBY 			
 		} return 3;*/
 	}

 	/*public function fullActividades($filtros=null){
 		$fullActividades=array();
 		$fullEjercicios=array();
 		$condact = array();
 		$filtromet=array();
 		$filtrodet=array();
 		if(!empty($filtros["idactividad"])){
 			$condact["idactividad"]=$filtros["idactividad"];
 		}
 		if(!empty($filtros["nivel"])){
 			$condact["nivel"]=$filtros["nivel"];
 		}
 		if(!empty($filtros["unidad"])){
 			$condact["unidad"]=$filtros["unidad"];
 		}
 		if(!empty($filtros["sesion"])){
 			$condact["sesion"]=$filtros["sesion"];
 		}
 		if(!empty($filtros["idmetodologia"])){
 			$filtromet["idmetodologia"]=$filtros["idmetodologia"];
 		}
 		if(!empty($filtros["idhabilidad"])){
 			$filtrodet["idhabilidad"]=$filtros["idhabilidad"];
 		}
 		if(!empty($filtros["titulodetalle"])){
 			$filtrodet["titulo"]=$filtros["titulodetalle"];
 		}
 		$filtromet['tipo']='M';
 		$metodologias=$this->oNegMetodologia->buscarmetodologia($filtromet);
 		$idet=0;
 		if(!empty($metodologias))
 			foreach ($metodologias as $vmet){
				$condact["metodologia"]=$vmet["idmetodologia"];   //parametro adicional para leer las actividades
				$actividad=$this->oDatActividades->buscar($condact);
				if(!empty($actividad)){
					$fullActividades[$vmet["idmetodologia"]]["met"]=$vmet;
					$fullActividades[$vmet["idmetodologia"]]["act"]=$actividad[0];
					foreach ($actividad as $value){
						$filtrodet['idactividad']=$value["idactividad"];
						$this->oDatActividad_detalle->setLimite(0,100000000);
						$act_det=$this->oDatActividad_detalle->buscar($filtrodet);
						if(!empty($act_det)){
							$idet++;
							$fullActividades[$vmet["idmetodologia"]]["acts"][$value["idactividad"]]=$act_det;
							$fullActividades[$vmet["idmetodologia"]]["act"]["det"]=$act_det;
							$fullEjercicios[]=$act_det;
						}
					}
					unset($actividad);
				}
			}
		if(!empty($filtros["ejercicios"])){
			return $fullEjercicios;
		}else{
			return $fullActividades;
		}
	}	*/
}