<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRub_pregunta', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRub_pregunta 
{
	protected $id_pregunta;
	protected $nombre;
	protected $otros_datos;
	protected $id_estandar;
	protected $tipo;
	protected $activo;
	
	protected $dataRub_pregunta;
	protected $oDatRub_pregunta;	

	public function __construct()
	{
		$this->oDatRub_pregunta = new DatRub_pregunta;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRub_pregunta->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRub_pregunta->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRub_pregunta->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRub_pregunta->get($this->id_pregunta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_pregunta', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRub_pregunta->iniciarTransaccion('neg_i_Rub_pregunta');
			$this->id_pregunta = $this->oDatRub_pregunta->insertar($this->nombre,$this->otros_datos,$this->id_estandar,$this->tipo,$this->activo);
			$this->oDatRub_pregunta->terminarTransaccion('neg_i_Rub_pregunta');	
			return $this->id_pregunta;
		} catch(Exception $e) {	
		    $this->oDatRub_pregunta->cancelarTransaccion('neg_i_Rub_pregunta');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_pregunta', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRub_pregunta->actualizar($this->id_pregunta,$this->nombre,$this->otros_datos,$this->id_estandar,$this->tipo,$this->activo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rub_pregunta', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRub_pregunta->eliminar($this->id_pregunta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_pregunta($pk){
		try {
			$this->dataRub_pregunta = $this->oDatRub_pregunta->get($pk);
			if(empty($this->dataRub_pregunta)) {
				throw new Exception(JrTexto::_("Rub_pregunta").' '.JrTexto::_("not registered"));
			}
			$this->id_pregunta = $this->dataRub_pregunta["id_pregunta"];
			$this->nombre = $this->dataRub_pregunta["nombre"];
			$this->otros_datos = $this->dataRub_pregunta["otros_datos"];
			$this->id_estandar = $this->dataRub_pregunta["id_estandar"];
			$this->tipo = $this->dataRub_pregunta["tipo"];
			$this->activo = $this->dataRub_pregunta["activo"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rub_pregunta', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRub_pregunta = $this->oDatRub_pregunta->get($pk);
			if(empty($this->dataRub_pregunta)) {
				throw new Exception(JrTexto::_("Rub_pregunta").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRub_pregunta->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}