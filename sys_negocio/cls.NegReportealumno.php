<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		14-11-2016
 * @copyright	Copyright (C) 14-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatReportealumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');

class NegReportealumno 
{
	protected $idnivel;
	protected $id;
	protected $idpadre;
	protected $titulo;
	protected $tipo;	
	protected $idusuario;	
	protected $estado;
	protected $orden;
	protected $imagen;
	protected $pestanas;
	protected $nivel;
	
	protected $dataReportealumno;
	protected $oDatReportealumno;	
	protected $oNegBitacora_alumno_smartbook;
	protected $oNegBitacora_smartbook;
	protected $oNegAcad_cursodetalle;
	protected $oNegNotas_quiz;

	public function __construct()
	{
		$this->oDatReportealumno = new DatReportealumno;
		$this->oNegBitacora_alumno_smartbook = new NegBitacora_alumno_smartbook;
		$this->oNegBitacora_smartbook = new NegBitacora_smartbook;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegNotas_quiz = new NegNotas_quiz;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatComponentes->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatComponentes->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatReportealumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar2($filtros = array())
	{
		try {
			return $this->oDatComponentes->buscar2($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscar_nivel($filtros = array())
	{
		try {
			return $this->oDatComponentes->buscar_nivel($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarActxIdNivel($filtros = array())
	{
		try {
			$unidades = $this->oDatComponentes->buscar($filtros);
			$arrActividades = array();
			if(!empty($unidades)){
				foreach ($unidades as $u) {
					$acts = $this->oDatComponentes->buscar(array(
						'tipo'=>'L',
						'idpadre'=> $u['idnivel'],
					));
					$arrActividades = array_merge($arrActividades, $acts);
				}
			}
			return $arrActividades;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarNiveles($filtros = array())
	{
		try {			
			 $sysnivel=$this->oDatComponentes->buscar($filtros);
			 $niveles=array();
			 foreach ($sysnivel as $nivel){
			 	$niveles[$nivel["idnivel"]]=$nivel;
			 	$filtros["tipo"]='U';
			 	$filtros["idpadre"]=$nivel["idnivel"];
			 	$sysunidad=$this->oDatComponentes->buscar($filtros);
			 	$niveles[$nivel["idnivel"]]["nunidades"]=count($sysunidad);
			 	$nactividades=0;
  			    foreach ($sysunidad as $unidad){
  			    	$niveles[$nivel["idnivel"]]["unidades"][$unidad["idnivel"]]=$unidad;  			    		 	
				 	$filtros["tipo"]='L';
			 		$filtros["idpadre"]=$unidad["idnivel"];
				 	$sysactividad=$this->oDatComponentes->buscar($filtros);	
				 	$nactividades=$nactividades+count($sysactividad);
				 	$niveles[$nivel["idnivel"]]["unidades"][$unidad["idnivel"]]["nactividades"]=count($sysactividad);
	  			    foreach ($sysactividad as $actividad){
					 	$niveles[$nivel["idnivel"]]["unidades"][$unidad["idnivel"]]["actividades"][$actividad["idnivel"]]=$actividad;					 	
					}			 	
				}
				$niveles[$nivel["idnivel"]]["nactividades"]=$nactividades;
			 }
			 return $niveles;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatComponentes->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatComponentes->get($this->idnivel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			
			/*if(!NegSesion::tiene_acceso('tarea', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/

			$this->idnivel = $this->oDatComponentes->insertar($this->id,$this->titulo,$this->orden,$this->estado,$this->idusuario,$this->imagen,$this->pestanas);			
			

			return $this->idnivel;
		} catch(Exception $e) {	
		   //$this->oDatComponentes->cancelarTransaccion('neg_i_Niveles');		
			throw new Exception($e->getMessage());
		}
	}

	public function agregar_unidad()
	{
		try {

			$this->idnivel = $this->oDatComponentes->insertar_unidad($this->idpadre,$this->titulo,$this->orden,$this->estado,$this->idusuario,$this->pestanas,$this->nivel,$this->imagen);

			return $this->idnivel;
		} catch(Exception $e) {	
		   //$this->oDatComponentes->cancelarTransaccion('neg_i_Niveles');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('niveles', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatComponentes->actualizar($this->idnivel,$this->id,$this->titulo,$this->orden,$this->estado,$this->idusuario,$this->imagen,$this->pestanas);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function editar_unidad()
	{
		try {
			
			return $this->oDatComponentes->actualizar_unidad($this->idnivel,$this->idpadre,$this->titulo,$this->orden,$this->estado,$this->idusuario,$this->imagen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('niveles', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatComponentes->eliminar($this->idnivel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnivel($pk){
		try {
			$this->dataComponentes = $this->oDatComponentes->get($pk);
			if(empty($this->dataComponentes)) {
				throw new Exception(JrTexto::_("Niveles").' '.JrTexto::_("not registered"));
			}
			$this->idnivel = $this->dataComponentes["idnivel"];
			$this->nombre = $this->dataComponentes["nombre"];
			$this->tipo = $this->dataComponentes["tipo"];
			$this->idpadre = $this->dataComponentes["idpadre"];
			$this->idpersonal = $this->dataComponentes["idpersonal"];
			$this->estado = $this->dataComponentes["estado"];
			$this->orden = $this->dataComponentes["orden"];
			$this->imagen = $this->dataComponentes["imagen"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('niveles', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataComponentes = $this->oDatComponentes->get($pk);
			if(empty($this->dataComponentes)) {
				throw new Exception(JrTexto::_("Niveles").' '.JrTexto::_("not registered"));
			}

			return $this->oDatComponentes->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function ordenar($frm){
		try {
				if(empty($frm))	return false;
			    $accion=intval($frm["ordenaa"]);
				$orden=intval($frm["ordenactual"])+$accion;		
				$id=$frm["id"];
				if($orden==0) return false;
				$filtro['tipo']=$frm["tipo"];
				$datos=$this->buscar($filtro);
				$i=0;	
				foreach ($datos as $nivel){	
					if($nivel["idnivel"]==$id)
						$this->setCampo($id,'orden',$orden);
					else{
						$i++;
						if($i==$orden) $i++;
						$this->setCampo($nivel["idnivel"],'orden',$i);
					}
				}
				return true;

			} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getprogreso($filtros){
		try{
			$resultado = 0;
			$fecha1 = isset($filtros['fechaDesde']) ? $filtros['fechaDesde'] : null;
			$fecha2 = isset($filtros['fechaHasta']) ? $filtros['fechaHasta'] : null;
			$sesionesActuales = $this->oNegBitacora_alumno_smartbook->buscar(array('idcurso'=>$filtros['idcurso'],'idusuario' => $filtros['idalumno'],'idsesion'=>$filtros['idsesion'],$filtros['idalumno'],'fechaDesde'=>$fecha1,'fechaHasta'=>$fecha2));
			if(!empty($sesionesActuales)){
				$resultado = 100;
			}
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function getTotalCurso($filtros){
		try{
			$resultado 	= 0;
			$examenes 	= 0;
			$fecha1 = isset($filtros['fechaDesde']) ? $filtros['fechaDesde'] : null;
			$fecha2 = isset($filtros['fechaHasta']) ? $filtros['fechaHasta'] : null;
			//buscar todas las sesiones del curso
			$sesiones = $this->oNegAcad_cursodetalle->buscar2(array('idcurso' => $filtros['idcurso']));
			foreach($sesiones as $value){
				$jsonParse = json_decode($value['txtjson'],true);
				if(isset($jsonParse['link']) && !isset($jsonParse['options'])){
					$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
					$idexamen = explode('&',$parte)[0];
					$examen = $this->oNegNotas_quiz->buscar(array('idalumno'=>$filtros['idalumno'],'idrecurso'=>$idexamen,'fechaDesde'=>$fecha1,'hastafecha'=>$fecha2));
					if(!empty($examen)){
						$examenes++;
					}
				}
			}
			$sesionesActuales = $this->oNegBitacora_alumno_smartbook->buscar(array('idcurso'=>$filtros['idcurso'],'idusuario' => $filtros['idalumno'],'fechaDesde'=>$fecha1,'fechaHasta'=>$fecha2));
			//calcular el total
			$resultado = ((count($sesionesActuales) + $examenes) * 100) / count($sesiones);
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
}