<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-02-2020
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_criterios', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_criterios 
{
	protected $idcriterio;
	protected $idcapacidad;
	protected $idcurso;
	protected $nombre;
	protected $estado;
	protected $idproyecto;
	
	protected $dataAcad_criterios;
	protected $oDatAcad_criterios;	

	public function __construct()
	{
		$this->oDatAcad_criterios = new DatAcad_criterios;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_criterios->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {			
			return $this->oDatAcad_criterios->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_criterios->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_criterios->get($this->idcriterio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_criterios', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_criterios->iniciarTransaccion('neg_i_Acad_criterios');
			$this->idcriterio = $this->oDatAcad_criterios->insertar($this->idcapacidad,$this->idcurso,$this->nombre,$this->estado,$this->idproyecto);
			$this->oDatAcad_criterios->terminarTransaccion('neg_i_Acad_criterios');	
			return $this->idcriterio;
		} catch(Exception $e) {	
		    $this->oDatAcad_criterios->cancelarTransaccion('neg_i_Acad_criterios');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_criterios', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_criterios->actualizar($this->idcriterio,$this->idcapacidad,$this->idcurso,$this->nombre,$this->estado,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAcad_criterios->cambiarvalorcampo($this->idcriterio,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_criterios', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_criterios->eliminar($this->idcriterio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcriterio($pk){
		try {
			$this->dataAcad_criterios = $this->oDatAcad_criterios->get($pk);
			if(empty($this->dataAcad_criterios)) {
				throw new Exception(JrTexto::_("Acad_criterios").' '.JrTexto::_("not registered"));
			}
			$this->idcriterio = $this->dataAcad_criterios["idcriterio"];
			$this->idcapacidad = $this->dataAcad_criterios["idcapacidad"];
			$this->idcurso = $this->dataAcad_criterios["idcurso"];
			$this->nombre = $this->dataAcad_criterios["nombre"];
			$this->estado = $this->dataAcad_criterios["estado"];
			$this->idproyecto = $this->dataAcad_criterios["idproyecto"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_criterios', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_criterios = $this->oDatAcad_criterios->get($pk);
			if(empty($this->dataAcad_criterios)) {
				throw new Exception(JrTexto::_("Acad_criterios").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_criterios->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}