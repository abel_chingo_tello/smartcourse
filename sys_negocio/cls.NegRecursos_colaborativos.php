<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		02-12-2020
 * @copyright	Copyright (C) 02-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRecursos_colaborativos', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRecursos_colaborativos 
{
	
	protected $idrecurso;
	protected $idproyecto;
	protected $titulo;
	protected $descripcion;
	protected $file;
	protected $tipo;
	protected $estado;
	protected $idusuario;
	
	protected $dataRecursos_colaborativos;
	protected $oDatRecursos_colaborativos;	

	public function __construct()
	{
		$this->oDatRecursos_colaborativos = new DatRecursos_colaborativos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRecursos_colaborativos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatRecursos_colaborativos->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colaborativos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatRecursos_colaborativos->iniciarTransaccion('neg_i_Recursos_colaborativos');
			$this->idrecurso = $this->oDatRecursos_colaborativos->insertar($this->idproyecto,$this->titulo,$this->descripcion,$this->file,$this->tipo,$this->estado,$this->idusuario);
			$this->oDatRecursos_colaborativos->terminarTransaccion('neg_i_Recursos_colaborativos');	
			return $this->idrecurso;
		} catch(Exception $e) {	
		    $this->oDatRecursos_colaborativos->cancelarTransaccion('neg_i_Recursos_colaborativos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colaborativos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatRecursos_colaborativos->actualizar($this->idrecurso,$this->idproyecto,$this->titulo,$this->descripcion,$this->file,$this->tipo,$this->estado,$this->idusuario);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	
	public function actualizar2($estados,$condicion){
		try {
			return $this->oDatRecursos_colaborativos->actualizar2($estados,$condicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatRecursos_colaborativos->cambiarvalorcampo($this->idrecurso,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Recursos_colaborativos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRecursos_colaborativos->eliminar($this->idrecurso,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecurso($pk){
		try {
			$this->dataRecursos_colaborativos = $this->oDatRecursos_colaborativos->buscar(array('sqlget'=>true,'idrecurso'=>$pk));
			if(empty($this->dataRecursos_colaborativos)) {
				throw new Exception(JrTexto::_("Recursos_colaborativos").' '.JrTexto::_("not registered"));
			}
			$this->idrecurso=$this->dataRecursos_colaborativos["idrecurso"];
			$this->idproyecto = $this->dataRecursos_colaborativos["idproyecto"];
			$this->titulo = $this->dataRecursos_colaborativos["titulo"];
			$this->descripcion = $this->dataRecursos_colaborativos["descripcion"];
			$this->file = $this->dataRecursos_colaborativos["file"];
			$this->tipo = $this->dataRecursos_colaborativos["tipo"];
			$this->estado = $this->dataRecursos_colaborativos["estado"];
			$this->idusuario = $this->dataRecursos_colaborativos["idusuario"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colaborativos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRecursos_colaborativos = $this->oDatRecursos_colaborativos->buscar(array('sqlget'=>true,'idrecurso'=>$pk));
			if(empty($this->dataRecursos_colaborativos)) {
				throw new Exception(JrTexto::_("Recursos_colaborativos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRecursos_colaborativos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}