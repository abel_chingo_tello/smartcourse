<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-04-2017
 * @copyright	Copyright (C) 12-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatActividad_alumno', RUTA_BASE);
/*JrCargador::clase('sys_datos::DatActividad_detalle', RUTA_BASE);
JrCargador::clase('sys_datos::DatActividades', RUTA_BASE);
JrCargador::clase('sys_datos::DatNiveles', RUTA_BASE);*/
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegActividad_alumno
{

	protected $idactalumno;
	protected $iddetalleactividad;
	protected $idalumno;
	protected $idrecurso;
	protected $fecha;
	protected $porcentajeprogreso;
	protected $habilidades;
	protected $estado;
	protected $html_solucion;
	protected $file;
	protected $tipofile;
	protected $idcurso;
	protected $idcc;
	protected $idcursodetalle_sc;
	protected $idproyecto;
	protected $idgrupoauladetalle;

	protected $dataActividad_alumno;
	protected $oDatActividad_alumno;
	protected $oDatActividad_detalle;
	protected $oDatActividades;
	protected $oDatNiveles;

	public function __construct()
	{
		$this->oDatActividad_alumno = new DatActividad_alumno;
		/*$this->oDatActividad_detalle = new DatActividad_detalle;
		$this->oDatActividades = new DatActividades;
		$this->oDatNiveles = new DatNiveles;*/
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatActividad_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////
	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatActividad_alumno->getNumRegistros($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array(), $bdname = null)
	{
		try {
			$this->setLimite(0, 10000);
			return $this->oDatActividad_alumno->buscar($filtros, $bdname);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getProgresoHabilidadesSB($params = ["idalumno", "idrecurso", "bd_se"])
	{
		try {
			$this->setLimite(0, 10000);
			return $this->oDatActividad_alumno->getProgresoHabilidadesSB($params);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getPorcentajeSmartbookAlumno($params)
	{
		try {
			$this->setLimite(0, 10000);
			return $this->oDatActividad_alumno->getPorcentajeSmartbookAlumno($params);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatActividad_alumno->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatActividad_alumno->get($this->idactalumno);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function agregar()
	{
		try {
			//if(!NegSesion::tiene_acceso('actividad_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			//$this->oDatActividad_alumno->iniciarTransaccion('neg_i_Actividad_alumno');
			$this->idactalumno = $this->oDatActividad_alumno->insertar($this->iddetalleactividad, $this->idalumno, $this->idrecurso, $this->fecha, $this->porcentajeprogreso, $this->habilidades, $this->estado, $this->html_solucion, $this->idcurso, $this->idcc, $this->idproyecto,$this->idgrupoauladetalle);





			//$this->oDatActividad_alumno->terminarTransaccion('neg_i_Actividad_alumno');
			return $this->idactalumno;
		} catch (Exception $e) {
			//$this->oDatActividad_alumno->cancelarTransaccion('neg_i_Actividad_alumno');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('actividad_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatActividad_alumno->actualizar($this->idactalumno, $this->iddetalleactividad, $this->idalumno, $this->idrecurso, $this->fecha, $this->porcentajeprogreso, $this->habilidades, $this->estado, $this->html_solucion, $this->idcurso, $this->idcc, $this->idproyecto,$this->idgrupoauladetalle);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			if (!NegSesion::tiene_acceso('Actividad_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access') . '!!');
			}
			return $this->oDatActividad_alumno->eliminar($this->idactalumno);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			if (!NegSesion::tiene_acceso('actividad_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access') . '!!');
			}
			$this->dataActividad_alumno = $this->oDatActividad_alumno->get($pk);
			if (empty($this->dataActividad_alumno)) {
				throw new Exception(JrTexto::_("Actividad_alumno") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatActividad_alumno->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function ultimaActividadxAlum($idalumno)
	{
		try {
			$ultima_activ = $this->oDatActividad_alumno->ultimaActividad($idalumno);
			if (empty($ultima_activ)) return null;
			$det_act = $this->oDatActividad_detalle->get($ultima_activ['iddetalleactividad']);
			$activ = $this->oDatActividades->get($det_act['idactividad']);

			$filtroN = array('idnivel' => $activ['nivel']);
			$filtroU = array('idnivel' => $activ['unidad']);
			$filtroS = array('idnivel' => $activ['sesion']);
			$nivel  = $this->oDatNiveles->buscar($filtroN);
			$unidad = $this->oDatNiveles->buscar($filtroU);
			$sesion = $this->oDatNiveles->buscar($filtroS);

			return array('nivel' => $nivel[0], 'unidad' => $unidad[0], 'sesion' => $sesion[0]);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function countprogreso($filtros = null, $bdname = null)
	{
		try {
			$this->setLimite(0, 10000);
			return $this->oDatActividad_alumno->countprogreso($filtros, $bdname);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function ubicartrimestre($key, $key2, $value, $hab, &$arreglo)
	{

		if ($key == 1) {
			$arreglo[$key][strval($hab)] += $value;
		} elseif ($key == 2) {
			if ($key2 == 0) {
				$arreglo[1][strval($hab)] += $value;
			} else {
				$arreglo[2][strval($hab)] += $value;
			}
		} elseif ($key == 3) {
			$arreglo[2][strval($hab)] += $value;
		} else {
			$arreglo[3][strval($hab)] += $value;
		}
	}
	public function totalactividades($idcurso, $unidades, $contrimestre = true, $bdname = null)
	{
		try {
			$act = array();
			$tri = array();
			$totales = 0;

			$filtros = null;

			if (!is_null($idcurso) && !is_null($unidades)) {
				$tri[1] = array('4' => 0, '5' => 0, '6' => 0, '7' => 0);
				$tri[2] = array('4' => 0, '5' => 0, '6' => 0, '7' => 0);
				$tri[3] = array('4' => 0, '5' => 0, '6' => 0, '7' => 0);
				foreach ($unidades as $key => $value) {
					if (!is_null($value)) {
						$sum4 = 0;
						$sum5 = 0;
						$sum6 = 0;
						$sum7 = 0;
						$sumTri4 = 0;
						$sumTri5 = 0;
						$sumTri6 = 0;
						$sumTri7 = 0;
						foreach ($value as $key2 => $value2) {
							if ($resultado = $this->oDatActividad_alumno->actividadestotal(array('xunidad' => true, 'idcurso' => $idcurso, 'idrecurso' => $value2, 'listen' => true), $bdname)) {
								$sum4 += $resultado[0]['total'];
								$this->ubicartrimestre($key, $key2, $resultado[0]['total'], '4', $tri);
							}
							if ($resultado = $this->oDatActividad_alumno->actividadestotal(array('xunidad' => true, 'idcurso' => $idcurso, 'idrecurso' => $value2, 'read' => true), $bdname)) {
								$sum5 += $resultado[0]['total'];
								$this->ubicartrimestre($key, $key2, $resultado[0]['total'], '5', $tri);
							}
							if ($resultado = $this->oDatActividad_alumno->actividadestotal(array('xunidad' => true, 'idcurso' => $idcurso, 'idrecurso' => $value2, 'write' => true), $bdname)) {
								$sum6 += $resultado[0]['total'];
								$this->ubicartrimestre($key, $key2, $resultado[0]['total'], '6', $tri);
							}
							if ($resultado = $this->oDatActividad_alumno->actividadestotal(array('xunidad' => true, 'idcurso' => $idcurso, 'idrecurso' => $value2, 'speak' => true), $bdname)) {
								$sum7 += $resultado[0]['total'];
								$this->ubicartrimestre($key, $key2, $resultado[0]['total'], '7', $tri);
							}
						}
						$act[$key] = array('4' => $sum4, '5' => $sum5, '6' => $sum6, '7' => $sum7);
					}
				}
			}
			$totales = array('4' => 0, '5' => 0, '6' => 0, '7' => 0);
			foreach ($act as $value) {
				foreach ($value as $k => $v) {
					$totales[$k] += $v;
				}
			}

			// var_dump($act);
			// var_dump($tri);
			// var_dump($totales);
			// exit();
			return array('total' => $totales, 'bimestre' => $act, 'trimestre' => $tri);
		} catch (Exception $e) {

			throw new Exception($e->getMessage());
		}
	}

	/*
	public function set($pk){
		try {
			$this->dataActividad_alumno = $this->oDatActividad_alumno->get($pk);
			if(empty($this->dataActividad_alumno)) {
				throw new Exception(JrTexto::_("Actividad_alumno").' '.JrTexto::_("not registered"));
			}
			$this->idactalumno = $this->dataActividad_alumno["idactalumno"];
			$this->iddetalleactividad = $this->dataActividad_alumno["iddetalleactividad"];
			$this->idalumno = $this->dataActividad_alumno["idalumno"];
			$this->fecha = $this->dataActividad_alumno["fecha"];
			$this->porcentajeprogreso = $this->dataActividad_alumno["porcentajeprogreso"];
			$this->estado = $this->dataActividad_alumno["estado"];
						//falta campos
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function setIdactalumno($idactalumno)
	{
		try {
			$this->idactalumno= NegTools::validar('todo', $idactalumno, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setIddetalleactividad($iddetalleactividad)
	{
		try {
			$this->iddetalleactividad= NegTools::validar('todo', $iddetalleactividad, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setIdalumno($idalumno)
	{
		try {
			$this->idalumno= NegTools::validar('todo', $idalumno, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setFecha($fecha)
	{
		try {
			$this->fecha= NegTools::validar('todo', $fecha, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setPorcentajeprogreso($porcentajeprogreso)
	{
		try {
			$this->porcentajeprogreso= NegTools::validar('todo', $porcentajeprogreso, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setEstado($estado)
	{
		try {
			$this->estado= NegTools::validar('todo', $estado, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
*/
}
