<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatResumen_local', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegResumen_local 
{
	protected $id;
	protected $id_ubigeo;
	protected $iddre;
	protected $idugel;
	protected $idlocal;
	protected $local;
	protected $tipo;
	protected $alumno_ubicacion;
	protected $ubicacion_A1;
	protected $ubicacion_A2;
	protected $ubicacion_B1;
	protected $ubicacion_B2;
	protected $ubicacion_C1;
	protected $entrada_prom;
	protected $alumno_entrada;
	protected $salida_prom;
	protected $alumno_salida;
	protected $examen_b1_prom;
	protected $alumno_examen_b1;
	protected $examen_b2_prom;
	protected $alumno_examen_b2;
	protected $examen_b3_prom;
	protected $alumno_examen_b3;
	protected $examen_b4_prom;
	protected $alumno_examen_b4;
	protected $examen_t1_prom;
	protected $alumno_examen_t1;
	protected $examen_t2_prom;
	protected $alumno_examen_t2;
	protected $examen_t3_prom;
	protected $alumno_examen_t3;
	protected $tiempopv;
	protected $tiempo_exam;
	protected $tiempo_task;
	protected $tiempo_smartbook;
	protected $tiempo_practice;
	protected $prog_A1;
	protected $prog_A2;
	protected $prog_B1;
	protected $prog_B2;
	protected $prog_C1;
	
	protected $dataResumen_local;
	protected $oDatResumen_local;	

	public function __construct()
	{
		$this->oDatResumen_local = new DatResumen_local;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatResumen_local->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatResumen_local->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatResumen_local->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatResumen_local->get($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('resumen_local', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatResumen_local->iniciarTransaccion('neg_i_Resumen_local');
			$this->id = $this->oDatResumen_local->insertar($this->id_ubigeo,$this->iddre,$this->idugel,$this->idlocal,$this->local,$this->tipo,$this->alumno_ubicacion,$this->ubicacion_A1,$this->ubicacion_A2,$this->ubicacion_B1,$this->ubicacion_B2,$this->ubicacion_C1,$this->entrada_prom,$this->alumno_entrada,$this->salida_prom,$this->alumno_salida,$this->examen_b1_prom,$this->alumno_examen_b1,$this->examen_b2_prom,$this->alumno_examen_b2,$this->examen_b3_prom,$this->alumno_examen_b3,$this->examen_b4_prom,$this->alumno_examen_b4,$this->examen_t1_prom,$this->alumno_examen_t1,$this->examen_t2_prom,$this->alumno_examen_t2,$this->examen_t3_prom,$this->alumno_examen_t3,$this->tiempopv,$this->tiempo_exam,$this->tiempo_task,$this->tiempo_smartbook,$this->tiempo_practice,$this->prog_A1,$this->prog_A2,$this->prog_B1,$this->prog_B2,$this->prog_C1);
			$this->oDatResumen_local->terminarTransaccion('neg_i_Resumen_local');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatResumen_local->cancelarTransaccion('neg_i_Resumen_local');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('resumen_local', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatResumen_local->actualizar($this->id,$this->id_ubigeo,$this->iddre,$this->idugel,$this->idlocal,$this->local,$this->tipo,$this->alumno_ubicacion,$this->ubicacion_A1,$this->ubicacion_A2,$this->ubicacion_B1,$this->ubicacion_B2,$this->ubicacion_C1,$this->entrada_prom,$this->alumno_entrada,$this->salida_prom,$this->alumno_salida,$this->examen_b1_prom,$this->alumno_examen_b1,$this->examen_b2_prom,$this->alumno_examen_b2,$this->examen_b3_prom,$this->alumno_examen_b3,$this->examen_b4_prom,$this->alumno_examen_b4,$this->examen_t1_prom,$this->alumno_examen_t1,$this->examen_t2_prom,$this->alumno_examen_t2,$this->examen_t3_prom,$this->alumno_examen_t3,$this->tiempopv,$this->tiempo_exam,$this->tiempo_task,$this->tiempo_smartbook,$this->tiempo_practice,$this->prog_A1,$this->prog_A2,$this->prog_B1,$this->prog_B2,$this->prog_C1);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Resumen_local', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatResumen_local->eliminar($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataResumen_local = $this->oDatResumen_local->get($pk);
			if(empty($this->dataResumen_local)) {
				throw new Exception(JrTexto::_("Resumen_local").' '.JrTexto::_("not registered"));
			}
			$this->id = $this->dataResumen_local["id"];
			$this->id_ubigeo = $this->dataResumen_local["id_ubigeo"];
			$this->iddre = $this->dataResumen_local["iddre"];
			$this->idugel = $this->dataResumen_local["idugel"];
			$this->idlocal = $this->dataResumen_local["idlocal"];
			$this->local = $this->dataResumen_local["local"];
			$this->tipo = $this->dataResumen_local["tipo"];
			$this->alumno_ubicacion = $this->dataResumen_local["alumno_ubicacion"];
			$this->ubicacion_A1 = $this->dataResumen_local["ubicacion_A1"];
			$this->ubicacion_A2 = $this->dataResumen_local["ubicacion_A2"];
			$this->ubicacion_B1 = $this->dataResumen_local["ubicacion_B1"];
			$this->ubicacion_B2 = $this->dataResumen_local["ubicacion_B2"];
			$this->ubicacion_C1 = $this->dataResumen_local["ubicacion_C1"];
			$this->entrada_prom = $this->dataResumen_local["entrada_prom"];
			$this->alumno_entrada = $this->dataResumen_local["alumno_entrada"];
			$this->salida_prom = $this->dataResumen_local["salida_prom"];
			$this->alumno_salida = $this->dataResumen_local["alumno_salida"];
			$this->examen_b1_prom = $this->dataResumen_local["examen_b1_prom"];
			$this->alumno_examen_b1 = $this->dataResumen_local["alumno_examen_b1"];
			$this->examen_b2_prom = $this->dataResumen_local["examen_b2_prom"];
			$this->alumno_examen_b2 = $this->dataResumen_local["alumno_examen_b2"];
			$this->examen_b3_prom = $this->dataResumen_local["examen_b3_prom"];
			$this->alumno_examen_b3 = $this->dataResumen_local["alumno_examen_b3"];
			$this->examen_b4_prom = $this->dataResumen_local["examen_b4_prom"];
			$this->alumno_examen_b4 = $this->dataResumen_local["alumno_examen_b4"];
			$this->examen_t1_prom = $this->dataResumen_local["examen_t1_prom"];
			$this->alumno_examen_t1 = $this->dataResumen_local["alumno_examen_t1"];
			$this->examen_t2_prom = $this->dataResumen_local["examen_t2_prom"];
			$this->alumno_examen_t2 = $this->dataResumen_local["alumno_examen_t2"];
			$this->examen_t3_prom = $this->dataResumen_local["examen_t3_prom"];
			$this->alumno_examen_t3 = $this->dataResumen_local["alumno_examen_t3"];
			$this->tiempopv = $this->dataResumen_local["tiempopv"];
			$this->tiempo_exam = $this->dataResumen_local["tiempo_exam"];
			$this->tiempo_task = $this->dataResumen_local["tiempo_task"];
			$this->tiempo_smartbook = $this->dataResumen_local["tiempo_smartbook"];
			$this->tiempo_practice = $this->dataResumen_local["tiempo_practice"];
			$this->prog_A1 = $this->dataResumen_local["prog_A1"];
			$this->prog_A2 = $this->dataResumen_local["prog_A2"];
			$this->prog_B1 = $this->dataResumen_local["prog_B1"];
			$this->prog_B2 = $this->dataResumen_local["prog_B2"];
			$this->prog_C1 = $this->dataResumen_local["prog_C1"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('resumen_local', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataResumen_local = $this->oDatResumen_local->get($pk);
			if(empty($this->dataResumen_local)) {
				throw new Exception(JrTexto::_("Resumen_local").' '.JrTexto::_("not registered"));
			}

			return $this->oDatResumen_local->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}