<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-11-2017
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatLogro', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegLogro 
{
	protected $id_logro;
	protected $titulo;
	protected $descripcion;
	protected $tipo;
	protected $imagen;
	protected $puntaje;
	protected $estado;
	
	protected $dataLogro;
	protected $oDatLogro;	

	public function __construct()
	{
		$this->oDatLogro = new DatLogro;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;			
			$this->oDatLogro->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatLogro->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatLogro->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatLogro->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatLogro->get($this->id_logro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->oDatLogro->iniciarTransaccion('neg_i_Logro');
			$this->id_logro = $this->oDatLogro->insertar($this->titulo,$this->descripcion,$this->tipo,$this->imagen,$this->puntaje,$this->estado);
			$this->oDatLogro->terminarTransaccion('neg_i_Logro');	
			return $this->id_logro;
		} catch(Exception $e) {	
		    $this->oDatLogro->cancelarTransaccion('neg_i_Logro');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {

			return $this->oDatLogro->actualizar($this->id_logro,$this->titulo,$this->descripcion,$this->tipo,$this->imagen,$this->puntaje,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			return $this->oDatLogro->eliminar($this->id_logro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function eliminar2($idlogro)
	{
		try {
			return $this->oDatLogro->eliminar($idlogro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_logro($pk){
		try {
			$this->dataLogro = $this->oDatLogro->get($pk);
			if(empty($this->dataLogro)) {
				throw new Exception(JrTexto::_("Logro").' '.JrTexto::_("not registered"));
			}
			$this->id_logro = $this->dataLogro["id_logro"];
			$this->titulo = $this->dataLogro["titulo"];
			$this->descripcion = $this->dataLogro["descripcion"];
			$this->tipo = $this->dataLogro["tipo"];
			$this->imagen = $this->dataLogro["imagen"];
			$this->puntaje = $this->dataLogro["puntaje"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			$this->dataLogro = $this->oDatLogro->get($pk);
			if(empty($this->dataLogro)) {
				throw new Exception(JrTexto::_("Logro").' '.JrTexto::_("not registered"));
			}
			return $this->oDatLogro->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}		
}