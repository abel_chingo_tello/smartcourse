<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-02-2020
 * @copyright	Copyright (C) 11-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTareas_mensajes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegTareas_mensajes 
{
	protected $idtareamensaje;
	protected $idtarea;
	protected $idusuario;
	protected $rol;
	protected $mensaje;
	protected $estado;
	
	protected $dataTareas_mensajes;
	protected $oDatTareas_mensajes;	

	public function __construct()
	{
		$this->oDatTareas_mensajes = new DatTareas_mensajes;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTareas_mensajes->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTareas_mensajes->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTareas_mensajes->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTareas_mensajes->get($this->idtareamensaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tareas_mensajes', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatTareas_mensajes->iniciarTransaccion('neg_i_Tareas_mensajes');
			$this->idtareamensaje = $this->oDatTareas_mensajes->insertar($this->idtarea,$this->idusuario,$this->rol,$this->mensaje,$this->estado);
			$this->oDatTareas_mensajes->terminarTransaccion('neg_i_Tareas_mensajes');	
			return $this->idtareamensaje;
		} catch(Exception $e) {	
		    $this->oDatTareas_mensajes->cancelarTransaccion('neg_i_Tareas_mensajes');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tareas_mensajes', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatTareas_mensajes->actualizar($this->idtareamensaje,$this->idtarea,$this->idusuario,$this->rol,$this->mensaje,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Tareas_mensajes', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTareas_mensajes->eliminar($this->idtareamensaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtareamensaje($pk){
		try {
			$this->dataTareas_mensajes = $this->oDatTareas_mensajes->get($pk);
			if(empty($this->dataTareas_mensajes)) {
				throw new Exception(JrTexto::_("Tareas_mensajes").' '.JrTexto::_("not registered"));
			}
			$this->idtareamensaje = $this->dataTareas_mensajes["idtareamensaje"];
			$this->idtarea = $this->dataTareas_mensajes["idtarea"];
			$this->idusuario = $this->dataTareas_mensajes["idusuario"];
			$this->rol = $this->dataTareas_mensajes["rol"];
			$this->mensaje = $this->dataTareas_mensajes["mensaje"];
			$this->estado = $this->dataTareas_mensajes["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('tareas_mensajes', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTareas_mensajes = $this->oDatTareas_mensajes->get($pk);
			if(empty($this->dataTareas_mensajes)) {
				throw new Exception(JrTexto::_("Tareas_mensajes").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTareas_mensajes->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}