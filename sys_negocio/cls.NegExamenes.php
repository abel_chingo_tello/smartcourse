<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-03-2017
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatExamenes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegExamenes 
{
	protected $idexamen;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $titulo;
	protected $descripcion;
	protected $portada;
	protected $fuente;
	protected $fuentesize;
	protected $tipo;
	protected $grupo;
	protected $aleatorio;
	protected $calificacion_por;
	protected $calificacion_en;
	protected $calificacion_total;
	protected $calificacion_min;
	protected $tiempo_por;
	protected $tiempo_total;
	protected $estado;
	protected $idpersonal;
	protected $fecharegistro;
	/*protected $nintento;
	protected $calificacion;*/
	
	protected $dataExamenes;
	protected $oDatExamenes;	
	protected $arrContextOptions;	

	public function __construct()
	{
		$this->arrContextOptions=array( "ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false, ), ); 
		$this->oDatExamenes = new DatExamenes;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatExamenes->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatExamenes->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function buscar($idexamen=null)
	{
		try {
			if(empty($idexamen)){ throw new Exception(JrTexto::_("IdExamen is missing")); }
			$resp = array();
			$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$idexamen.'&pr='.IDPROYECTO, false, stream_context_create($this->arrContextOptions));
            $dataExam = json_decode($dataExam, true);
            if($dataExam['code']==200){
            	$resp = $dataExam['data'];
            }
			return $resp;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
/*
	public function listar()
	{
		try {
			return $this->oDatExamenes->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatExamenes->get($this->idexamen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			//$this->oDatExamenes->iniciarTransaccion('neg_i_Examenes');
			$this->idexamen = $this->oDatExamenes->insertar($this->idnivel,$this->idunidad,$this->idactividad,$this->titulo,$this->descripcion,$this->portada,$this->fuente,$this->fuentesize,$this->tipo,$this->grupo,$this->aleatorio,$this->calificacion_por,$this->calificacion_en,$this->calificacion_total,$this->calificacion_min,$this->tiempo_por,$this->tiempo_total,$this->estado,$this->idpersonal);//,$this->nintento,$this->calificacion
			//$this->oDatExamenes->terminarTransaccion('neg_i_Examenes');	
			return $this->idexamen;
		} catch(Exception $e) {	
		   //$this->oDatExamenes->cancelarTransaccion('neg_i_Examenes');		
			throw new Exception($e->getMessage());
		}
	}
*/
	public function editar()
	{
		try {

			return $this->oDatExamenes->actualizar($this->idexamen,$this->idnivel,$this->idunidad,$this->idactividad,$this->titulo,$this->descripcion,$this->portada,$this->fuente,$this->fuentesize,$this->tipo,$this->grupo,$this->aleatorio,$this->calificacion_por,$this->calificacion_en,$this->calificacion_total,$this->calificacion_min,$this->tiempo_por,$this->tiempo_total,$this->estado,$this->idpersonal);/*,$this->nintento,$this->calificacion*/
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			return $this->oDatExamenes->eliminar($this->idexamen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdexamen($pk){
		try {
			$this->dataExamenes = $this->oDatExamenes->get($pk);
			if(empty($this->dataExamenes)) {
				throw new Exception(JrTexto::_("Examenes").' '.JrTexto::_("not registered"));
			}
			$this->idexamen = $this->dataExamenes["idexamen"];
			$this->idnivel = $this->dataExamenes["idnivel"];
			$this->idunidad = $this->dataExamenes["idunidad"];
			$this->idactividad = $this->dataExamenes["idactividad"];
			$this->titulo = $this->dataExamenes["titulo"];
			$this->descripcion = $this->dataExamenes["descripcion"];
			$this->portada = $this->dataExamenes["portada"];
			$this->fuente = $this->dataExamenes["fuente"];
			$this->fuentesize = $this->dataExamenes["fuentesize"];
			$this->tipo = $this->dataExamenes["tipo"];
			$this->grupo = $this->dataExamenes["grupo"];
			$this->aleatorio = $this->dataExamenes["aleatorio"];
			$this->calificacion_por = $this->dataExamenes["calificacion_por"];
			$this->calificacion_en = $this->dataExamenes["calificacion_en"];
			$this->calificacion_total = $this->dataExamenes["calificacion_total"];
			$this->calificacion_min = $this->dataExamenes["calificacion_min"];
			$this->tiempo_por = $this->dataExamenes["tiempo_por"];
			$this->tiempo_total = $this->dataExamenes["tiempo_total"];
			$this->estado = $this->dataExamenes["estado"];
			$this->idpersonal = $this->dataExamenes["idpersonal"];
			$this->fecharegistro = $this->dataExamenes["fecharegistro"];
			/*$this->nintento = $this->dataExamenes["nintento"];
			$this->calificacion = $this->dataExamenes["calificacion"];*/
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			$this->dataExamenes = $this->oDatExamenes->get($pk);
			if(empty($this->dataExamenes)) {
				throw new Exception(JrTexto::_("Examenes").' '.JrTexto::_("not registered"));
			}
			return $this->oDatExamenes->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	

	public function getResultado($idexamen=null, $identif_persona=null)
	{
		try {
			if(empty($idexamen) && empty($identif_persona)){ throw new Exception(JrTexto::_("Impossible to get exam result of person")); }
			$response = null;
			$resultExam = file_get_contents(URL_SMARTQUIZ.'service/exam_result?pr='.IDPROYECTO.'&idexam='.$idexamen.'&idstudent='.$identif_persona, false, stream_context_create($this->arrContextOptions));
            $resultExam= json_decode($resultExam, true);
            if($resultExam['code']==200){
            	$response = $resultExam['data'];
            }
            return $response;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getResultado_alumno($identif_persona=null){
		try {
			if(empty($identif_persona)){ throw new Exception(JrTexto::_("Impossible to get exam result of person")); }
			$response = null;
			$url = URL_SMARTQUIZ."service/results_student?pr=".IDPROYECTO."&idstudent=".$identif_persona;
			$resultExam = file_get_contents($url, false, stream_context_create($this->arrContextOptions));
            $resultExam= json_decode($resultExam, true);
            if($resultExam['code']==200){
            	$response = $resultExam['data'];
            }
            return $response;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}