<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		26-10-2018
 * @copyright	Copyright (C) 26-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersonal', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
JrCargador::clase('sys_datos::DatPersona_rol', RUTA_BASE);
class NegPersonal 
{
	protected $idpersona;
	protected $tipodoc;
	protected $dni;
	protected $ape_paterno;
	protected $ape_materno;
	protected $nombre;
	protected $fechanac;
	protected $sexo;
	protected $estado_civil;
	protected $ubigeo;
	protected $urbanizacion;
	protected $direccion;
	protected $telefono;
	protected $celular;
	protected $email;
	protected $idugel;
	protected $regusuario;
	protected $regfecha;
	protected $usuario;
	protected $clave;
	protected $token;
	protected $rol;
	protected $foto;
	protected $estado;
	protected $situacion;
	protected $idioma;
	protected $tipousuario;
	protected $idlocal;
	protected $esdemo;
	
	protected $dataPersonal;
	protected $oDatPersonal;
	protected $oDatPersonarol;

	public function __construct()
	{
		$this->oDatPersonal = new DatPersonal;
		$this->oDatPersonarol = new DatPersona_rol;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersonal->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPersonal->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function webService($filtros = array())
	{
		try {
			return $this->oDatPersonal->webService($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersonal->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscar_paracertificado($filtros = array())
	{
		try {
			return $this->oDatPersonal->buscar_paracertificado($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function importar($id,$tipodoc,$dni,$ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idugel,$regusuario,$regfecha,$usuario,$clave,$token,$idrol,$foto,$estado,$situacion,$idioma,$tipousuario,$idiiee,$rol,$ugel,$iiee,$idproyecto,$idempresa)
	{
		//$odre = new DatMin_dre;
		//$oDatrol
		//$this->oDatPersonarol->buscar();
		/*$haydre=$odre->buscar(array('ubigeo'=>$iddre));
		if(empty($haydre[0])){
			$haydre2=$odre->buscar(array('descripcion'=>$dre));
			if(empty($haydre2[0])) $odre->insertar($dre,$iddre,'');
			else $iddre=$haydre2[0]["ubigeo"];
		}

		$oUgel=  new DatUgel;
		$hayugel=$oUgel->buscar(array('idugel'=>$idugel));
		if(empty($hayugel[0])){
			$hayugel2=$oUgel->buscar(array('descripcion'=>$ugel));
			if(empty($hayugel2[0])) $idugel=$oUgel->insertar($ugel,$ugel,$iddre,$ubigeo);
			else $idugel=$hayugel2[0]["idugel"];
		}
		 $this->oDatLocal->importar($idiiee,$iiee,$direccion,$ubigeo,$tipo,$nvacantes,$idugel,$idproyecto);*/

		 $haypersona=array();
		 if(!empty($id)) $haypersona=$this->oDatPersonal->buscar(array('idpersona'=>$id,'sqlsolopersona'=>true));
		if(empty($haypersona[0])){
			$idpersona=$this->oDatPersonal->importar($id,$tipodoc,$dni,$ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idugel,$regusuario,$regfecha,$usuario,$clave,$token,$idrol,$foto,$estado,$situacion,$idioma,$tipousuario,$idiiee);
		}else{
			$idpersona=$haypersona[0]["idpersona"];
		}
		$oDatPersonarol = new DatPersona_rol;
		$hayrol=$oDatPersonarol->buscar(array('idrol'=>$idrol,'idpersonal'=>$idpersona,'idproyecto'=>$idproyecto));
		if(empty($hayrol[0])&&$idpersona!=-1){
			$idempresa=!empty($idempresa)?$idempresa:4;
			$idroldetalle=$oDatPersonarol->insertar($idrol,$idpersona,$idproyecto,$idempresa);
		}
		return $idpersona;
	}

	public function listar()
	{
		try {
			return $this->oDatPersonal->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function borrarhistorial($idpersona)
	{
		try {
			return $this->oDatPersonal->borrarhistorial($idpersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPersonal->get($this->idpersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('personal', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPersonal->iniciarTransaccion('neg_i_Personal');
			$this->esdemo=isset($this->esdemo)?$this->esdemo:0;
			$this->idpersona = $this->oDatPersonal->insertar($this->tipodoc,$this->dni,$this->ape_paterno,$this->ape_materno,$this->nombre,$this->fechanac,$this->sexo,$this->estado_civil,$this->ubigeo,$this->urbanizacion,$this->direccion,$this->telefono,$this->celular,$this->email,$this->idugel,$this->regusuario,$this->regfecha,$this->usuario,$this->clave,$this->token,$this->rol,$this->foto,$this->estado,$this->situacion,$this->idioma,$this->tipousuario,$this->idlocal,$this->esdemo);
			$this->oDatPersonal->terminarTransaccion('neg_i_Personal');	
			return $this->idpersona;
		} catch(Exception $e) {	
		    $this->oDatPersonal->cancelarTransaccion('neg_i_Personal');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('personal', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersonal->actualizar($this->idpersona,$this->tipodoc,$this->dni,$this->ape_paterno,$this->ape_materno,$this->nombre,$this->fechanac,$this->sexo,$this->estado_civil,$this->ubigeo,$this->urbanizacion,$this->direccion,$this->telefono,$this->celular,$this->email,$this->idugel,$this->regusuario,$this->regfecha,$this->usuario,$this->clave,$this->token,$this->rol,$this->foto,$this->estado,$this->situacion,$this->idioma,$this->tipousuario,$this->idlocal,$this->esdemo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function editar_($idpersona, $estados = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('personal', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersonal->actualizar_($idpersona, $estados);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatPersonal->cambiarvalorcampo($this->idpersonal,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Personal', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersonal->eliminar($this->idpersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function eliminarusuario($data){
		try{
			if(empty($data)){ throw new Exception("Data vacio"); }
			return $this->oDatPersonal->eliminarusuario($data);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpersona($pk){
		try {
			$this->dataPersonal = $this->oDatPersonal->get($pk);
			if(empty($this->dataPersonal)) {
				throw new Exception(JrTexto::_("Personal").' '.JrTexto::_("not registered"));
			}
			$this->idpersona = $this->dataPersonal["idpersona"];
			$this->tipodoc = $this->dataPersonal["tipodoc"];
			$this->dni = $this->dataPersonal["dni"];
			$this->ape_paterno = $this->dataPersonal["ape_paterno"];
			$this->ape_materno = $this->dataPersonal["ape_materno"];
			$this->nombre = $this->dataPersonal["nombre"];
			$this->fechanac = $this->dataPersonal["fechanac"];
			$this->sexo = $this->dataPersonal["sexo"];
			$this->estado_civil = $this->dataPersonal["estado_civil"];
			$this->ubigeo = $this->dataPersonal["ubigeo"];
			$this->urbanizacion = $this->dataPersonal["urbanizacion"];
			$this->direccion = $this->dataPersonal["direccion"];
			$this->telefono = $this->dataPersonal["telefono"];
			$this->celular = $this->dataPersonal["celular"];
			$this->email = $this->dataPersonal["email"];
			$this->idugel = $this->dataPersonal["idugel"];
			$this->regusuario = $this->dataPersonal["regusuario"];
			$this->regfecha = $this->dataPersonal["regfecha"];
			$this->usuario = $this->dataPersonal["usuario"];
			$this->clave = $this->dataPersonal["clave"];
			$this->token = $this->dataPersonal["token"];
			$this->rol = $this->dataPersonal["rol"];
			$this->foto = $this->dataPersonal["foto"];
			$this->estado = $this->dataPersonal["estado"];
			$this->situacion = $this->dataPersonal["situacion"];
			$this->idioma = $this->dataPersonal["idioma"];
			$this->tipousuario = $this->dataPersonal["tipousuario"];
			$this->idlocal = $this->dataPersonal["idlocal"];
			$this->esdemo = $this->dataPersonal["esdemo"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('personal', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersonal = $this->oDatPersonal->get($pk);
			if(empty($this->dataPersonal)) {
				throw new Exception(JrTexto::_("Personal").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersonal->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	public function  actualizartabla($tabla,$campo){
		if(!empty($tabla)&&!empty($campo)){
			$this->setLimite(0, 5000);
		 	return $this->oDatPersonal->actualizartabla($tabla,$campo);		
		}
	}
	/*private function setUsuario($usuario)
	{
		try {
			$this->usuario= NegTools::validar('todo', $usuario, false, JrTexto::_("Please enter a valid value"), array("longmax" => 80));
			if($this->oDatPersonal->usuario != $usuario) {
				$existe = $this->oDatPersonal->getxusuario($usuario);
				if(!empty($existe)) {
					throw new Exception(JrTexto::_('The user entered is already in use to personal'));
				}
				$existealumno = $this->oDatAlumno->getxusuario($usuario);
				if(!empty($existe)) {
					throw new Exception(JrTexto::_('The user entered is already in use to student'));
				}
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}*/	
	public function procesarSolicitudCambioClave($usuario,$idproyecto)
	{
		try {
			$this->oDatPersonal = new DatPersonal;
			$usuario = $this->oDatPersonal->getxusuarioemail($usuario);
			if (!empty($usuario)){
				$userrol=$this->oDatPersonarol->buscar(array('idpersona'=>$usuario["idpersona"],'idproyecto'=>$idproyecto));
				if(empty($userrol[0]) && $usuario["tipouser"]='n'){
					return array('code'=>403); // no tiene permiso ne esta empresa 
				}
				$this->token = JrSession::crearRandom();
				$this->oDatPersonal->set($usuario['idpersona'], 'token', $this->token);
				$usuario['token']=$this->token;
				return array('code'=>200,'usuario'=>$usuario); 
				//return $usuario;
			}
			return array('code'=>404); // usuario no existe
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}