<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-03-2017
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatExamen_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegExamen_alumno 
{
	protected $idexaalumno;
	protected $idexamen;
	protected $idalumno;
	protected $preguntas;
	protected $resultado;
	protected $puntajehabilidad;
	protected $puntaje;
	protected $resultadojson;
	protected $tiempoduracion;
	protected $fecha;
	protected $intento;
	
	protected $dataExamen_alumno;
	protected $oDatExamen_alumno;	

	public function __construct()
	{
		$this->oDatExamen_alumno = new DatExamen_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatExamen_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatExamen_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatExamen_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatExamen_alumno->get($this->idexaalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			//$this->oDatExamen_alumno->iniciarTransaccion('neg_i_Examen_alumno');
			$this->idexaalumno = $this->oDatExamen_alumno->insertar($this->idexamen,$this->idalumno,$this->preguntas,$this->resultado,$this->puntajehabilidad,$this->puntaje,$this->resultadojson,$this->tiempoduracion,$this->intento);
			//$this->oDatExamen_alumno->terminarTransaccion('neg_i_Examen_alumno');	
			return $this->idexaalumno;
		} catch(Exception $e) {	
		   //$this->oDatExamen_alumno->cancelarTransaccion('neg_i_Examen_alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			return $this->oDatExamen_alumno->actualizar($this->idexaalumno,$this->idexamen,$this->idalumno,$this->preguntas,$this->resultado,$this->puntajehabilidad,$this->puntaje,$this->resultadojson,$this->tiempoduracion,$this->intento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			return $this->oDatExamen_alumno->eliminar($this->idexaalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdexaalumno($pk){
		try {
			$this->dataExamen_alumno = $this->oDatExamen_alumno->get($pk);
			if(empty($this->dataExamen_alumno)) {
				throw new Exception(JrTexto::_("Examen_alumno").' '.JrTexto::_("not registered"));
			}
			$this->idexaalumno = $this->dataExamen_alumno["idexaalumno"];
			$this->idexamen = $this->dataExamen_alumno["idexamen"];
			$this->idalumno = $this->dataExamen_alumno["idalumno"];
			$this->preguntas = $this->dataExamen_alumno["preguntas"];
			$this->resultado = $this->dataExamen_alumno["resultado"];
			$this->puntajehabilidad = $this->dataExamen_alumno["puntajehabilidad"];
			$this->puntaje = $this->dataExamen_alumno["puntaje"];
			$this->resultadojson = $this->dataExamen_alumno["resultadojson"];
			$this->tiempoduracion = $this->dataExamen_alumno["tiempoduracion"];
			$this->fecha = $this->dataExamen_alumno["fecha"];
			$this->intento = $this->dataExamen_alumno["intento"];
			return $this->dataExamen_alumno;
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			$this->dataExamen_alumno = $this->oDatExamen_alumno->get($pk);
			if(empty($this->dataExamen_alumno)) {
				throw new Exception(JrTexto::_("Examen_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatExamen_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}		
}