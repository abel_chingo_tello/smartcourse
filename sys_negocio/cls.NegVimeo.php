<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		21-05-2020
 * @copyright	Copyright (C) 21-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatVimeo', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegVimeo
{
	protected $static_path;
	protected $vimeo_uri;
	protected $idlog;
	protected $fecha_registro;
	protected $oDatVimeo;

	public function __construct()
	{
		$this->oDatVimeo = new DatVimeo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatVimeo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			$this->setLimite(0, 9999991);
			return $this->oDatVimeo->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function eliminarVideosIngles()
	{
		try {
			$this->setLimite(0, 9999991);
			$arrRegistroRutaVideo = $this->oDatVimeo->listarCursosInglesConStaticPath();
			foreach ($arrRegistroRutaVideo as $i => $registroRutaVideo) {
				$numReg = $i + 1;
				$rutaVideo = RUTA_BASE . $registroRutaVideo["static_path"];
				if (file_exists($rutaVideo)) {
					echo "$numReg Se está por eliminar este archivo: $rutaVideo <br>";
					$success = unlink($rutaVideo);
					if ($success) {
						$this->set_tmp_vimeo_log($registroRutaVideo["idlog"], "estado_post_proceso", "eliminado (" . date("Y-m-d H:i:s"). ")");
						echo "Eliminado: $rutaVideo<br>";
					} else {
						throw new Exception("Cannot delete $rutaVideo");
					}
				} else {
					echo "$numReg No existe este archivo: $rutaVideo <br>";
				}
			}
			exit(0);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	/**
	 * fakeVideosIngles(): Método para crear localmente los videos registrados en bd
	 * para por hacer pruebas de eliminación repetidas veces localmente.
	 */
	public function fakeVideosIngles()
	{
		try {
			$this->setLimite(0, 9999991);
			$arrRegistroRutaVideo = $this->oDatVimeo->listarCursosInglesConStaticPath();
			foreach ($arrRegistroRutaVideo as $i => $registroRutaVideo) {
				$numReg = $i + 1;
				$rutaVideo = RUTA_BASE . $registroRutaVideo["static_path"];
				$rutaVideo=str_replace("\\","/",$rutaVideo);
				if (!file_exists($rutaVideo)) {
					echo "$numReg Se está creando este archivo: $rutaVideo <br>";
					$success=mkdir(dirname($rutaVideo), 0777, true);
					if (!$success) {
						echo "Error al crear directorio<br>";
					} 
					$success = fopen("$rutaVideo", "x");
					if ($success) {
						echo "Creado: $rutaVideo<br>";
					} else {
						throw new Exception("Cannot create $rutaVideo");
					}
				} else {
					echo "$numReg Ya existe este archivo: $rutaVideo <br>";
				}
			}
			exit(0);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getNombreCurso($filtros = array())
	{
		try {
			$this->setLimite(0, 9999991);
			return $this->oDatVimeo->getNombreCurso($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatVimeo->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listarCursoProcesado($params)
	{
		try {
			return $this->oDatVimeo->listarCursoProcesado($params);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listarCursosEmpresa($params)
	{
		try {
			return $this->oDatVimeo->listarCursosEmpresa($params);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listarCursosEmpresaIngles($params)
	{
		try {
			return $this->oDatVimeo->listarCursosEmpresaIngles($params);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function insertarCursoProcesado($params)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Vimeo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			// $this->oDatVimeo->iniciarTransaccion('neg_i_Vimeo');
			$rs = $this->oDatVimeo->insertarCursoProcesado($params);
			// $this->oDatVimeo->terminarTransaccion('neg_i_Vimeo');
			return $rs;
		} catch (Exception $e) {
			// $this->oDatVimeo->cancelarTransaccion('neg_i_Vimeo');
			throw new Exception($e->getMessage());
		}
	}
	public function getXid()
	{
		try {
			return $this->oDatVimeo->get($this->idcron_tapr);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar($params)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Vimeo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			// $this->oDatVimeo->iniciarTransaccion('neg_i_Vimeo');
			$rs = $this->oDatVimeo->insertar($params);
			// $this->oDatVimeo->terminarTransaccion('neg_i_Vimeo');
			return $rs;
		} catch (Exception $e) {
			// $this->oDatVimeo->cancelarTransaccion('neg_i_Vimeo');
			throw new Exception($e->getMessage());
		}
	}
	public function customUpdate($filtros, $update)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Vimeo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatVimeo->customUpdate($filtros, $update);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Vimeo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatVimeo->actualizar($this->idcron_tapr, $this->idproyecto, $this->fecha_hora, $this->estado, $this->idsesion, $this->idpestania, $this->idcurso, $this->idcomplementario, $this->iddocente, $this->nombre, $this->tipo, $this->recurso, $this->tiporecurso);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar($filtros)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Vimeo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatVimeo->eliminar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcron_tapr($pk)
	{
		try {
			$this->dataVimeo = $this->oDatVimeo->get($pk);
			if (empty($this->dataVimeo)) {
				throw new Exception(JrTexto::_("Vimeo") . ' ' . JrTexto::_("not registered"));
			}
			$this->idcron_tapr = $this->dataVimeo["idcron_tapr"];
			$this->idproyecto = $this->dataVimeo["idproyecto"];
			$this->fecha_hora = $this->dataVimeo["fecha_hora"];
			$this->estado = $this->dataVimeo["estado"];
			$this->idsesion = $this->dataVimeo["idsesion"];
			$this->idpestania = $this->dataVimeo["idpestania"];
			$this->idcurso = $this->dataVimeo["idcurso"];
			$this->idcomplementario = $this->dataVimeo["idcomplementario"];
			$this->iddocente = $this->dataVimeo["iddocente"];
			$this->nombre = $this->dataVimeo["nombre"];
			$this->tipo = $this->dataVimeo["tipo"];
			$this->recurso = $this->dataVimeo["recurso"];
			$this->tiporecurso = $this->dataVimeo["tiporecurso"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function set_tmp_vimeo_log($pk, $propiedad, $valor)
	{
		try {
			return $this->oDatVimeo->set_tmp_vimeo_log($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarTarea($filtros = array())
	{
		try {
			return $this->oDatVimeo->buscarTarea($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function agregarTarea($params = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('Vimeo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			// $this->oDatVimeo->iniciarTransaccion('neg_i_Vimeo');
			$this->idcron_tapr = $this->oDatVimeo->insertarTarea($params);
			// $this->oDatVimeo->terminarTransaccion('neg_i_Vimeo');
			return $this->idcron_tapr;
		} catch (Exception $e) {
			$this->oDatVimeo->cancelarTransaccion('neg_i_Vimeo');
			throw new Exception($e->getMessage());
		}
	}
}
