<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		08-05-2017
 * @copyright	Copyright (C) 08-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatConfiguracion_docente', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegConfiguracion_docente 
{
	protected $idconfigdocente;
	protected $iddocente;
	protected $idrol;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $tiempototal;
	protected $tiempoactividades;
	protected $tiempoteacherresrc;
	protected $tiempogames;
	protected $tiempoexamenes;
	protected $escalas;
	
	protected $dataConfiguracion_docente;
	protected $oDatConfiguracion_docente;	

	public function __construct()
	{
		$this->oDatConfiguracion_docente = new DatConfiguracion_docente;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatConfiguracion_docente->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatConfiguracion_docente->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatConfiguracion_docente->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatConfiguracion_docente->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatConfiguracion_docente->get($this->idconfigdocente);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('configuracion_docente', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatConfiguracion_docente->iniciarTransaccion('neg_i_Configuracion_docente');
			$this->idconfigdocente = $this->oDatConfiguracion_docente->insertar($this->iddocente,$this->idrol,$this->idnivel,$this->idunidad,$this->idactividad,$this->tiempototal,$this->tiempoactividades,$this->tiempoteacherresrc,$this->tiempogames,$this->tiempoexamenes,$this->escalas);
			//$this->oDatConfiguracion_docente->terminarTransaccion('neg_i_Configuracion_docente');	
			return $this->idconfigdocente;
		} catch(Exception $e) {	
		   //$this->oDatConfiguracion_docente->cancelarTransaccion('neg_i_Configuracion_docente');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('configuracion_docente', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatConfiguracion_docente->actualizar($this->idconfigdocente,$this->iddocente,$this->idrol,$this->idnivel,$this->idunidad,$this->idactividad,$this->tiempototal,$this->tiempoactividades,$this->tiempoteacherresrc,$this->tiempogames,$this->tiempoexamenes,$this->escalas);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Configuracion_docente', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatConfiguracion_docente->eliminar($this->idconfigdocente);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdconfigdocente($pk){
		try {
			$this->dataConfiguracion_docente = $this->oDatConfiguracion_docente->get($pk);
			if(empty($this->dataConfiguracion_docente)) {
				throw new Exception(JrTexto::_("Configuracion_docente").' '.JrTexto::_("not registered"));
			}
			$this->idconfigdocente = $this->dataConfiguracion_docente["idconfigdocente"];
			$this->iddocente = $this->dataConfiguracion_docente["iddocente"];
			$this->idrol = $this->dataConfiguracion_docente["idrol"];
			$this->idnivel = $this->dataConfiguracion_docente["idnivel"];
			$this->idunidad = $this->dataConfiguracion_docente["idunidad"];
			$this->idactividad = $this->dataConfiguracion_docente["idactividad"];
			$this->tiempototal = $this->dataConfiguracion_docente["tiempototal"];
			$this->tiempoactividades = $this->dataConfiguracion_docente["tiempoactividades"];
			$this->tiempoteacherresrc = $this->dataConfiguracion_docente["tiempoteacherresrc"];
			$this->tiempogames = $this->dataConfiguracion_docente["tiempogames"];
			$this->tiempoexamenes = $this->dataConfiguracion_docente["tiempoexamenes"];
			$this->escalas = $this->dataConfiguracion_docente["escalas"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('configuracion_docente', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataConfiguracion_docente = $this->oDatConfiguracion_docente->get($pk);
			if(empty($this->dataConfiguracion_docente)) {
				throw new Exception(JrTexto::_("Configuracion_docente").' '.JrTexto::_("not registered"));
			}

			return $this->oDatConfiguracion_docente->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setIddocente($iddocente)
	{
		try {
			$this->iddocente= NegTools::validar('todo', $iddocente, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdrol($idrol)
	{
		try {
			$this->idrol= NegTools::validar('todo', $idrol, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdnivel($idnivel)
	{
		try {
			$this->idnivel= NegTools::validar('todo', $idnivel, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdunidad($idunidad)
	{
		try {
			$this->idunidad= NegTools::validar('todo', $idunidad, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdactividad($idactividad)
	{
		try {
			$this->idactividad= NegTools::validar('todo', $idactividad, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTiempototal($tiempototal)
	{
		try {
			$this->tiempototal= NegTools::validar('todo', $tiempototal, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTiempoactividades($tiempoactividades)
	{
		try {
			$this->tiempoactividades= NegTools::validar('todo', $tiempoactividades, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTiempoteacherresrc($tiempoteacherresrc)
	{
		try {
			$this->tiempoteacherresrc= NegTools::validar('todo', $tiempoteacherresrc, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTiempogames($tiempogames)
	{
		try {
			$this->tiempogames= NegTools::validar('todo', $tiempogames, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTiempoexamenes($tiempoexamenes)
	{
		try {
			$this->tiempoexamenes= NegTools::validar('todo', $tiempoexamenes, false, JrTexto::_("Please enter a valid value"), array("longmax" => 64));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setEscalas($escalas)
	{
		try {
			$this->escalas= NegTools::validar('todo', $escalas, false, JrTexto::_("Please enter a valid value"), array("longmax" => 999999999));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}