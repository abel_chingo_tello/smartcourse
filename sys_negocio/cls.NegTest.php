<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		29-01-2019
 * @copyright	Copyright (C) 29-01-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatSuscripciones_web4u', RUTA_BASE, 'sys_datos');
class NegSuscripciones_web4u
{
	protected $dataTest;
	protected $oDatSuscripciones;	

	public function __construct()
	{
		$this->oDatSuscripciones = new DatSuscripciones_web4u;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatSuscripciones->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatSuscripciones->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatSuscripciones->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatSuscripciones->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatSuscripciones->get($this->idtest);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatSuscripciones->iniciarTransaccion('neg_i_Test');
			$this->idtest = $this->oDatSuscripciones->insertar($this->titulo,$this->puntaje,$this->mostrar,$this->imagen);
			$this->oDatSuscripciones->terminarTransaccion('neg_i_Test');	
			return $this->idtest;
		} catch(Exception $e) {	
		    $this->oDatSuscripciones->cancelarTransaccion('neg_i_Test');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatSuscripciones->actualizar($this->idtest,$this->titulo,$this->puntaje,$this->mostrar,$this->imagen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatSuscripciones->cambiarvalorcampo($this->idtest,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Test', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatSuscripciones->eliminar($this->idtest);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtest($pk){
		try {
			$this->dataTest = $this->oDatSuscripciones->get($pk);
			if(empty($this->dataTest)) {
				throw new Exception(JrTexto::_("Test").' '.JrTexto::_("not registered"));
			}
			$this->idtest = $this->dataTest["idtest"];
			$this->titulo = $this->dataTest["titulo"];
			$this->puntaje = $this->dataTest["puntaje"];
			$this->mostrar = $this->dataTest["mostrar"];
			$this->imagen = $this->dataTest["imagen"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('test', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTest = $this->oDatSuscripciones->get($pk);
			if(empty($this->dataTest)) {
				throw new Exception(JrTexto::_("Test").' '.JrTexto::_("not registered"));
			}

			return $this->oDatSuscripciones->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}