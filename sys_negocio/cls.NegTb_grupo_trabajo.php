<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-11-2020
 * @copyright	Copyright (C) 17-11-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTb_grupo_trabajo', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegTb_grupo_trabajo
{
	protected $id_grupo_trabajo;
	protected $idgrupoauladetalle;
	protected $nombre;

	protected $dataTb_grupo_trabajo;
	protected $oDatTb_grupo_trabajo;

	public function __construct()
	{
		$this->oDatTb_grupo_trabajo = new DatTb_grupo_trabajo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatTb_grupo_trabajo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTb_grupo_trabajo->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listarConMatriculas($filtros = array())
	{
		try {
			return $this->oDatTb_grupo_trabajo->listarConMatriculas($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTb_grupo_trabajo->get($this->id_grupo_trabajo);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tb_grupo_trabajo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatTb_grupo_trabajo->iniciarTransaccion('neg_i_Tb_grupo_trabajo');
			$this->id_grupo_trabajo = $this->oDatTb_grupo_trabajo->insertar($this->idgrupoauladetalle, $this->nombre);
			$this->oDatTb_grupo_trabajo->terminarTransaccion('neg_i_Tb_grupo_trabajo');
			return $this->id_grupo_trabajo;
		} catch (Exception $e) {
			$this->oDatTb_grupo_trabajo->cancelarTransaccion('neg_i_Tb_grupo_trabajo');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tb_grupo_trabajo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTb_grupo_trabajo->actualizar($this->id_grupo_trabajo, $this->idgrupoauladetalle, $this->nombre);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Tb_grupo_trabajo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTb_grupo_trabajo->eliminar($this->id_grupo_trabajo);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_grupo_trabajo($pk)
	{
		try {
			$this->dataTb_grupo_trabajo = $this->oDatTb_grupo_trabajo->get($pk);
			if (empty($this->dataTb_grupo_trabajo)) {
				throw new Exception(JrTexto::_("Tb_grupo_trabajo") . ' ' . JrTexto::_("not registered"));
			}
			$this->id_grupo_trabajo = $this->dataTb_grupo_trabajo["id_grupo_trabajo"];
			$this->idgrupoauladetalle = $this->dataTb_grupo_trabajo["idgrupoauladetalle"];
			$this->nombre = $this->dataTb_grupo_trabajo["nombre"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('tb_grupo_trabajo', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTb_grupo_trabajo = $this->oDatTb_grupo_trabajo->get($pk);
			if (empty($this->dataTb_grupo_trabajo)) {
				throw new Exception(JrTexto::_("Tb_grupo_trabajo") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatTb_grupo_trabajo->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
