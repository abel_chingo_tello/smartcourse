<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-11-2017
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_datos::DatNiveles', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_cursodetalleTest 
{
	protected $idcursodetalle;
	protected $idcurso;
	protected $orden;
	protected $idrecurso;
	protected $tiporecurso;
	protected $idlogro;
	protected $idpadre;
	protected $url;	
	protected $color;
	protected $esfinal;

	protected $dataAcad_cursodetalle;
	protected $oDatAcad_cursodetalle;
	protected $oDatNiveles;	

	public function __construct()
	{
		$this->oDatAcad_cursodetalle = new DatAcad_cursodetalle;
		$this->oDatNiveles = new DatNiveles;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;			
			$this->oDatAcad_cursodetalle->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_cursodetalle->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getMaxorden($idcurso,$idpadre)
	{
		try {
			return $this->oDatAcad_cursodetalle->maxorden($idcurso,$idpadre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			$datos=$this->oDatAcad_cursodetalle->buscar($filtros);
			$data=array();
			if(!empty($datos))
			foreach ($datos as $dt){
				$tipo=$dt["tiporecurso"];
				$infonivel=array('N','U','L');
				$infoexam =array('E');
				if(in_array($tipo,$infonivel)){
					$nivel=$this->oDatNiveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo));
					$dt["nombre"]=@$nivel[0]["nombre"];
					$dt["estado"]=@$nivel[0]["estado"];
					$dt["idpersonal"]=@$nivel[0]["idpersonal"];
					$dt["descripcion"]=@$nivel[0]["descripcion"];
					$dt["imagen"]=@$nivel[0]["imagen"];
				}elseif(in_array($tipo,$infoexam)){
					$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
					$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';
					$arrContextOptions=array(
                        "ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false, ),
                    ); 
					$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                    $dataExam = json_decode($dataExam, true);
                    if($dataExam['code']==200){
                        $dt["nombre"] = $dataExam['data']['titulo'];
                        $dt["imagen"] = $dataExam['data']['portada'];
                    }
				}
				if($tipo=='N'){
					$unidades=$this->oDatAcad_cursodetalle->buscar(array('idpadre'=>$dt["idcursodetalle"],'tiporecurso'=>'U'));
					$nexamenes=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$dt["idcursodetalle"],'tiporecurso'=>'E'));
					$nunidad=0;
					$nactividad=0;
					if(!empty($unidades)){
						$nunidad=count($unidades);						
						foreach ($unidades as $unidad){
							$nactividad+=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$unidad["idcursodetalle"],'tiporecurso'=>'L'));
							$nexamenes+=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$unidad["idcursodetalle"],'tiporecurso'=>'E'));
							
						}
					}
					$dt["nunidad"]=$nunidad;
					$dt["nactividad"]=$nactividad;
					$dt["nexamenes"]=$nexamenes;
				}elseif($tipo=='U'){
					$nexamenes=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$dt["idcursodetalle"],'tiporecurso'=>'E'));
					$nactividad=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$dt["idcursodetalle"],'tiporecurso'=>'L'));
					$dt["nactividad"]=$nactividad;
					$dt["nexamenes"]=$nexamenes;
				}
				$data[]=$dt;				
			}
			return $data;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	//mine
	public function getCursosDetalles($filtros = array()){
		try {
			return $this->oDatAcad_cursodetalle->getCursosDetalles($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	//Listan el menu ok  ---------------
	public function verhijos($dt,$id){
		$sd=array();
		$dt2=$dt;
		foreach ($dt as $k =>$ds2){
			$idpadre=$ds2["idpadre"];			
			if($idpadre==$id){			
				unset($dt2[$k]);
				$ds2['hijos']=$this->verhijos($dt2,$ds2["idcursodetalle"]);
				array_push($sd,$ds2);
			}
		}
		return $sd;	
	}
	public function buscarconnivel($filtros,$maxlimit=150000){
		try{
			//var_dump($filtros);
			$this->oDatAcad_cursodetalle->setLimite(0, $maxlimit);
			$dt=$this->oDatAcad_cursodetalle->buscarconnivel($filtros);
			//var_dump($dt);
			$dts=array();
			$dpmay=0;
			$dpmen=1000000000;	
			foreach ($dt as $ds){
				$idpadre=$ds["idpadre"];
				$tipo=$ds["tiporecurso"];
				if($idpadre<=$dpmen) $dpmen=$idpadre;
				if($tipo=='E'||$tipo=='EU'){	
					$ds["imagen"] = '/static/media/imagenes/examen_default.png';
					$arrContextOptions=array("ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false, ),);
					//echo URL_SMARTQUIZ.'service/exam_info?idexam='.$ds['idrecurso'].'&pr='.IDLEARN."<br>";
					$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$ds['idrecurso'].'&pr='.IDLEARN, false, stream_context_create($arrContextOptions));
                    $dataExam = json_decode($dataExam, true);
                    if($dataExam['code']==200){
                        $ds["nombre"] = $dataExam['data']['titulo'];
                        $ds["imagen"] = $dataExam['data']['portada'];
                    }                  
				}
				if(empty($ds["imagen"])) $ds["imagen"]='/static/media/cursos/nofoto.jpg';
				array_push($dts,$ds);
			}

			$td1=$dts;
			//var_dump($td1);
			$dat=array();		
			foreach($td1 as $k=>$ds){ $ipa=$ds["idpadre"];
			//echo " <br>".$dpmen." - - ".$ipa;
				if($dpmen==$ipa){
					unset($dts[$k]);
					$ds['hijos']=$this->verhijos($dts,$ds["idcursodetalle"]);
					array_push($dat,$ds);
				}
			}	
			//var_dump($dat);
			return $dat;
		} catch(Exception $e){
			var_dump($e->Message());
			throw new Exception($e->getMessage());	
		}
	}
	public function getprogresounidad($filtros=null){
		try{
			$this->setLimite(0,10000);
			return $this->oDatAcad_cursodetalle->getprogresounidad($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function unidades($idpadre = 0){
		try{
			//lol
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			if(!empty($datos)){
				$infonivel=array('N','U','L','M');
				foreach ($datos as $dt){
					//hola
				}
			}//end if empty datos
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	/*public function sesiones($idcurso,$idpadre){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscarconnivel(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 			
			if(!empty($datos)){				
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					if($tipo=='E'){
						$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
						$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
						$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                            $dataExam = json_decode($dataExam, true);
                            if($dataExam['code']==200){
                                $dt["nombre"] = $dataExam['data']['titulo'];
                                $dt["imagen"] = $dataExam['data']['portada'];
                            }
					}
					$datos3=$this->sesiones($idcurso,$dt["idcursodetalle"]);
					if(!empty($datos3)){ $dt["hijo"]=$datos3; }
					$datos2[]=$dt;
				}
			}else return null;
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}*/

	public function sesiones($idcurso,$idpadre){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			if(!empty($datos)){
				$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 			
				$infonivel=array('N','U','L','M');
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					if(in_array($tipo,$infonivel)){
						$nivel=$this->oDatNiveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo));
						$dt["nombre"]=@$nivel[0]["nombre"];
						$dt["estado"]=@$nivel[0]["estado"];
						$dt["idpersonal"]=@$nivel[0]["idpersonal"];
						$dt["descripcion"]=@$nivel[0]["descripcion"];
						$dt["imagen"]=@$nivel[0]["imagen"];
					}elseif($tipo=='E'){
						$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
						$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
						$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                            $dataExam = json_decode($dataExam, true);
                            if($dataExam['code']==200){
                                $dt["nombre"] = $dataExam['data']['titulo'];
                                $dt["imagen"] = $dataExam['data']['portada'];
                            }
					}
					$datos3=$this->sesiones($idcurso,$dt["idcursodetalle"]);
					if(!empty($datos3)){ $dt["hijo"]=$datos3; }
					$datos2[]=$dt;
				}
			}else return null;			
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function examenes($idcurso,$idpadre,$ini=true){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 			
			if(!empty($datos)){
				$infonivel=array('N','U','M');
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					if($tipo=='E'){
						$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
						$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
						//$dataExam = file_get_contents(URL_BASE.'smartquiz/service/exam_info?idexam='.$dt['idrecurso'].'&pr=ALL', false, stream_context_create($arrContextOptions));
                        //$dataExam = json_decode($dataExam, true);
                        //if($dataExam['code']==200){
                        //    $dt["nombre"] = $dataExam['data']['titulo'];
                        //    $dt["imagen"] = $dataExam['data']['portada'];
                        //    $dt["idexamen"]=$dt['idrecurso'];
                        //}
                        $dt["idexamen"]=$dt['idrecurso'];
						$datos2[]=$dt;
					}else{
						if($tipo=='M'){
							$json=json_decode($dt["txtjson"], true);
							if(@$json["typelink"]=='smartquiz'){
								$idexamen=substr($json["link"],strrpos($json["link"],'ver/?idexamen=')+14);
								$idexamen=substr($idexamen,0,strrpos($idexamen,'&idproyecto'));
								$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
								$dt["imagen"] = '/static/media/imagenes/examen_default.png';
								$dt["idexamen"]=$idexamen;
								$datos2[]=$dt;
									//$urlquiz=URL_BASE.'smartquiz/service/exam_info?idexam='.$idexamen.'&pr=ALL';
									//$dataExam = file_get_contents($urlquiz, false, stream_context_create($arrContextOptions));
			                            //$dataExam = json_decode($dataExam, true);
			                            /*if($dataExam['code']==200){
			                            	$tipo='E';
			                                $dt["nombre"] = $dataExam['data']['titulo'];
			                                $dt["imagen"] = $dataExam['data']['portada'];
			                                $dt['idexamen']=$idexamen;
			                                $datos2[]=$dt;
			                            }*/
							}else{
								$options=@$json["options"];
								if(!empty($options))
								foreach ($options as $k => $v){
									if(@$v["type"]=='smartquiz'){
										$idexamen=substr($v["link"],strrpos($v["link"],'ver/?idexamen=')+14);
										$idexamen=substr($idexamen,0,strrpos($idexamen,'&idproyecto'));
										$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
										$dt["imagen"] = '/static/media/imagenes/examen_default.png';
										$dt["idexamen"]=$idexamen;
										$datos2[]=$dt;
										//$urlquiz=URL_BASE.'smartquiz/service/exam_info?idexam='.$idexamen.'&pr=ALL';
										//$dataExam = file_get_contents($urlquiz, false, stream_context_create($arrContextOptions));
			                            //$dataExam = json_decode($dataExam, true);
			                            /*if($dataExam['code']==200){
			                            	$tipo='E';
			                                $dt["nombre"] = $dataExam['data']['titulo'];
			                                $dt["imagen"] = $dataExam['data']['portada'];
			                                $dt['idexamen']=$idexamen;
			                                $datos2[]=$dt;
			                            }*/
									}
								}
							}
						}else{ // falta examenes dentro de unidades
							$datos3=$this->examenes($idcurso,$dt["idcursodetalle"],false);
							if(!empty($datos3))
							foreach ($datos3 as $k => $v) {
								$datos2[]=$v;
							}
						}
					}					
				}
			}else return null;
			if($ini==true){

				$totalexamenes=array();
				if(!empty($datos2)){
				 	foreach ($datos2 as $kexa){
				 		$totalexamenes[]=$kexa["idexamen"];
				 	}				 	
				 	$hayvariosexamenes=false;
				 	if(count($totalexamenes)>1){
				 		$hayvariosexamenes=true;
				 	}
				 	$urlquiz=URL_BASE.'smartquiz/service/exam_info?idexam='.implode(",",$totalexamenes).'&pr=ALL';
					$dataExam = file_get_contents($urlquiz, false, stream_context_create($arrContextOptions));
			        $dataExam = json_decode($dataExam, true);
			        $examenessmartquiz=array();
			        if($dataExam['code']==200){
			        	$tmpexamenessmartquiz=$dataExam['data'];
			        	if(!empty($tmpexamenessmartquiz) && $hayvariosexamenes==true) $examenessmartquiz=$dataExam['data'];
			        	else $examenessmartquiz[]=$dataExam['data'];
			        }
			        if(!empty($examenessmartquiz)){
			        	$datos3=array();			        	
			        	foreach ($datos2 as $kexa){
			        		foreach ($examenessmartquiz as $exa){
			        			if($exa["idexamen"]==$kexa["idexamen"]){
			        				$kexa["nombre"] = $exa['titulo'];
	                                $kexa["imagen"] = $exa['portada'];
	                                $datos3[]=$kexa;
			        			}
			        		}
			        	}
			        	$datos2=$datos3;			        	
			        }			        
				}				
			}		
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function soloexamenes($idcurso,$idpadre,$ini=true){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),);
			if(!empty($datos)){	
				$infonivel=array('N','U','M');
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					if($tipo=='E'){
						$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
						$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
						/*$dataExam = file_get_contents(URL_BASE.'/smartquiz/service/exam_info?idexam='.$dt['idrecurso'].'&pr=ALL', false, stream_context_create($arrContextOptions));
                        $dataExam = json_decode($dataExam, true);
                        if($dataExam['code']==200){
                            $dt["nombre"] = $dataExam['data']['titulo'];
                            $dt["imagen"] = $dataExam['data']['portada'];
                            $dt["idexamen"]=$dt['idrecurso'];
                        }*/
                        $dt["idexamen"]=$dt['idrecurso'];
                        $datos2[]=$dt;
					}else{
						if($tipo=='M'){
							$json=json_decode($dt["txtjson"], true);
							if(@$json["typelink"]=='smartquiz'){
								$idexamen=substr($json["link"],strrpos($json["link"],'ver/?idexamen=')+14);
								$idexamen=substr($idexamen,0,strrpos($idexamen,'&idproyecto'));
								$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
								$dt["imagen"] = '/static/media/imagenes/examen_default.png';
								$dt['idexamen']=$idexamen;
			                    $datos2[]=$dt;
									/*$urlquiz=URL_BASE.'/smartquiz/service/exam_info?idexam='.$idexamen.'&pr=ALL';
									$dataExam = file_get_contents($urlquiz, false, stream_context_create($arrContextOptions));
			                            $dataExam = json_decode($dataExam, true);
			                            if($dataExam['code']==200){
			                            	$tipo='E';
			                                $dt["nombre"] = $dataExam['data']['titulo'];
			                                $dt["imagen"] = $dataExam['data']['portada'];
			                                $dt['idexamen']=$idexamen;
			                                $datos2[]=$dt;
			                            }*/
							}else{
								$options=@$json["options"];
								if(!empty($options))
								foreach ($options as $k => $v){
									if(@$v["type"]=='smartquiz'){
										$idexamen=substr($v["link"],strrpos($v["link"],'ver/?idexamen=')+14);
										$idexamen=substr($idexamen,0,strrpos($idexamen,'&idproyecto'));
										$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
										$dt["imagen"] = '/static/media/imagenes/examen_default.png';
										$dt['idexamen']=$idexamen;
			                            $datos2[]=$dt;
										/*$urlquiz=URL_BASE.'/smartquiz/service/exam_info?idexam='.$idexamen.'&pr=ALL';
										$dataExam = file_get_contents($urlquiz, false, stream_context_create($arrContextOptions));
			                            $dataExam = json_decode($dataExam, true);
			                            if($dataExam['code']==200){
			                            	$tipo='E';
			                                $dt["nombre"] = $dataExam['data']['titulo'];
			                                $dt["imagen"] = $dataExam['data']['portada'];
			                                $dt['idexamen']=$idexamen;
			                                $datos2[]=$dt;
			                            }*/
									}
								}
							}
						}else{// falta examenes dentro de unidades
							//$nivel=$this->oDatNiveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo));
							$datos3=$this->soloexamenes($idcurso,$dt["idcursodetalle"],false);
							if(!empty($datos3))
							foreach ($datos3 as $k => $v) {
								$datos2[]=$v;
							}
						}
					}					
				}
			}else return null;
			if($ini==true){
				$totalexamenes=array();
				if(!empty($datos2)){
					$datos2 = array_intersect_key($datos2,array_unique(array_column($datos2,'idcursodetalle')));
				 	foreach ($datos2 as $kexa){
				 		$totalexamenes[]=$kexa["idexamen"];
				 	}				 	
				 	$hayvariosexamenes=false;
				 	if(count($totalexamenes)>1){
				 		$hayvariosexamenes=true;
				 	}

				 	$urlquiz=URL_SMARTQUIZ.'service/exam_info?idexam='.implode(",",$totalexamenes).'&pr=ALL';
					$dataExam = file_get_contents($urlquiz, false, stream_context_create($arrContextOptions));
			        $dataExam = json_decode($dataExam, true);
			        $examenessmartquiz=array();
			        if($dataExam['code']==200){
			        	$tmpexamenessmartquiz=$dataExam['data'];
			        	if(!empty($tmpexamenessmartquiz) && $hayvariosexamenes==true) $examenessmartquiz=$dataExam['data'];
			        	else $examenessmartquiz[]=$dataExam['data'];
			        }
			        if(!empty($examenessmartquiz)){
			        	$datos3=array();			        	
			        	foreach ($datos2 as $kexa){
			        		foreach ($examenessmartquiz as $exa){
			        			if($exa["idexamen"]==$kexa["idexamen"]){
			        				$kexa["nombre"] = $exa['titulo'];
	                                $kexa["imagen"] = $exa['portada'];
	                                $datos3[]=$kexa;
			        			}
			        		}
			        	}
			        	$datos2=$datos3;			        	
			        }			        
				}				
			}
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function sesiones2($idcurso,$idpadre){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			if(!empty($datos)){
				$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 			
				$infonivel=array('N','U','L','M');
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					if(in_array($tipo,$infonivel)){
						$nivel=$this->oDatNiveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo));
						$dt["nombre"]=@$nivel[0]["nombre"];
						$dt["estado"]=@$nivel[0]["estado"];
						$dt["idpersonal"]=@$nivel[0]["idpersonal"];
						$dt["descripcion"]=@$nivel[0]["descripcion"];
						$dt["imagen"]=@$nivel[0]["imagen"];
					}elseif($tipo=='E'){
						/*$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
						$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
						$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                            $dataExam = json_decode($dataExam, true);
                            if($dataExam['code']==200){
                                $dt["nombre"] = $dataExam['data']['titulo'];
                                $dt["imagen"] = $dataExam['data']['portada'];
                            }
                            */
					}
					$datos3=$this->sesiones2($idcurso,$dt["idcursodetalle"]);
					if(!empty($datos3)){ $dt["hijo"]=$datos3; }
					$datos2[]=$dt;
				}
			}else return null;			
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getSoloSesiones($idcurso, $idpadre)
	{
		try {
			$sesiones = $hijos = array();
			$arbol_sesiones = $this->sesiones($idcurso, $idpadre);

			if(!empty($arbol_sesiones)){
				foreach ($arbol_sesiones as $i=>$a) {
					$tipo=$a["tiporecurso"];
					if($tipo=='U' && !empty(@$a['hijo'])){ $hijos = array_merge($hijos, $a['hijo']); } 
					else if($tipo=='L'){ $hijos = $arbol_sesiones; }
				}
				
				/*//Extraer solo las que son Lecciones hijos (sesiones) sin exámenes:
				if(!empty($hijos)){
					foreach ($hijos as $i => $h) {
						if($h["tiporecurso"]=='L'){
							$sesiones[] = $h;
						}
					}
				}*/
				
				//Todas las lecciones y exámenes hijos.
				$sesiones = $hijos; 
			}
			//echo '<pre>'; print_r($sesiones); echo '</pre>';
			return $sesiones;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getTodosPadresXCursoDet($filtros=array(), $orden='')
	{
		/**
		* @param $orden:
		* 'ASC' : a partir del hijo $cursoDetalle obtenido de $filtros tendra dentro un campo para su padre y este 
		*		  último tendrá a su padre y asi sucesivamente hasta obtener el útlimo padre.
		* 'DESC': obtendrá desde el primer padre el cual contendrá a un hijo y este último a su hijo y así  
		* 		  sucesivamente hasta llegar al $cursoDetalle obtenido de $filtros.
		*/
		try {
			$arrTodosPadres = $arrPadresOrdenado = array();

			do {
				$detalle = array();
				$curso_det = $this->oDatAcad_cursodetalle->buscar($filtros);
				if(!empty($curso_det)){ 
					$detalle = $curso_det[0]; 
					$nivel = $this->oDatNiveles->buscar(array('idnivel'=>$detalle["idrecurso"],'tipo'=>$detalle["tiporecurso"]));
					$detalle["nombre"] = @$nivel[0]["nombre"];
					$detalle["descripcion"] = @$nivel[0]["descripcion"];
					$detalle["imagen"] = @$nivel[0]["imagen"];
				}
				$arrTodosPadres[] = $detalle;
				$filtros["idcursodetalle"] = $detalle["idpadre"];
			} while( (int)$filtros["idcursodetalle"]!=0 );

			if($orden=='ASC') {
				$arrPadresOrdenado = $this->ordenarCursoDet_ASC($arrTodosPadres);
			} else if($orden=='DESC') {
				$arrPadresOrdenado = $this->ordenarCursoDet_DESC($arrTodosPadres);
			} else if($orden=='') {
				$arrPadresOrdenado = $arrTodosPadres;
			}

			return $arrPadresOrdenado;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function ordenarCursoDet_ASC($arrTodosPadres=array())
	{
		try {
			if(empty($arrTodosPadres)){ return null; }
			$arrOrdenado = array();

	        for ($i=count($arrTodosPadres)-1; $i>=0; $i--) { 
	        	if($i-1>=0) {
		        	$arrTodosPadres[$i-1]["cursodetalle_padre"] = $arrTodosPadres[$i];
	        	} else {
	        		$arrOrdenado = $arrTodosPadres[$i];
	        	}
	        	unset($arrTodosPadres[$i]);
	        }

			return $arrOrdenado;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function ordenarCursoDet_DESC($arrTodosPadres=array())
	{
		try {
			if(empty($arrTodosPadres)){ return null; }
			$arrOrdenado = array();

	        for ($i=0; $i<count($arrTodosPadres); $i++) { 
	        	if($i+1<count($arrTodosPadres)) {
		        	$arrTodosPadres[$i+1]["cursodetalle_hijo"] = $arrTodosPadres[$i];
	        	} else {
	        		$arrOrdenado = $arrTodosPadres[$i];
	        	}
	        }

			return $arrOrdenado;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar2($filtros=array())
	{
		try {
			return $this->oDatAcad_cursodetalle->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatAcad_cursodetalle->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getNivelPadre($curso_det)
	{
		try {
			if(empty($curso_det)){throw new Exception(JrTexto::_('curso_det missing')); }
			$response = array();
			$padre = $this->oDatAcad_cursodetalle->get($curso_det['idpadre']);
			$nivel_padre = $this->oDatNiveles->get($padre['idrecurso']);
			if(!empty($padre) && !empty($nivel_padre)) {
				$response = array_merge($padre, $nivel_padre);
				$response["idpadre"] = $padre["idpadre"];
			}
			return $response;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_cursodetalle->get($this->idcursodetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregaromodificar($idcursodetalle, $nombre='',$idrecursoPadre='',$idpersonal='',$imagen='',$descripcion='',$ordenrecurso=0,$iscloonado=false,$idcomplementario){
		try {
			$this->oDatAcad_cursodetalle->iniciarTransaccion('neg_i_Acad_cursodetalle');
			$nivel=$this->oDatNiveles->insertoupdate($this->idrecurso, $nombre,$this->tiporecurso,$idrecursoPadre,$idpersonal,1,$ordenrecurso,$imagen,$descripcion,$idcomplementario);
			$idrecurso=$nivel["idnivel"];  
			//$cursodetalle=$this->oDatAcad_cursodetalle->insertoupdate($idcursodetalle,$this->idcurso,$this->orden,$idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre);
			$cursodetalle=$this->oDatAcad_cursodetalle->insertoupdate($idcursodetalle,$this->idcurso,$this->orden,$idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre,$this->color,$this->esfinal,$idcomplementario);
			$this->oDatAcad_cursodetalle->terminarTransaccion('neg_i_Acad_cursodetalle');
			$menu=array_merge($nivel,$cursodetalle);
			return $menu;
			}catch(Exception $e){
		    	$this->oDatAcad_cursodetalle->cancelarTransaccion('neg_i_Acad_cursodetalle');		
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->oDatAcad_cursodetalle->iniciarTransaccion('neg_i_Acad_cursodetalle');
			$this->idcursodetalle = $this->oDatAcad_cursodetalle->insertar($this->idcurso,$this->orden,$this->idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre,$this->color,$this->esfinal);
			$this->oDatAcad_cursodetalle->terminarTransaccion('neg_i_Acad_cursodetalle');	
			return $this->idcursodetalle;
		} catch(Exception $e) {	
		    $this->oDatAcad_cursodetalle->cancelarTransaccion('neg_i_Acad_cursodetalle');		
			throw new Exception($e->getMessage());
		}
	}

	public function agregarvarios($idcurso,$detallecursos,$idpadre=0){
		try {
			$univel=$uunidad=$ulession=$idpadre=$id=$idpadre;			
			foreach($detallecursos as $k=>$v){
				$tipo=$v->tipo;
				$idpadre=($tipo=='N')?0:(($tipo=='U')?$univel:$uunidad);
				$orden=$this->getMaxorden($idcurso,$idpadre);
				$orden++;
				$nombre=$v->nombre;
				$idrecurso=$v->idnivel;
				$color=!empty($v->color)?$v->color:'';
				$esfinal=!empty($v->esfinal)?$v->esfinal:'';
				$id=$this->oDatAcad_cursodetalle->insertar($idcurso,$orden,$idrecurso,$tipo,0,'',$idpadre,$color,$esfinal);
				if($tipo=='N'){
					$univel=$id;
				}elseif($tipo=='U'){
					$uunidad=$id;
				}
				
			}
		} catch(Exception $e) {			    		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			return $this->oDatAcad_cursodetalle->actualizar($this->idcursodetalle,$this->idcurso,$this->orden,$this->idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre,$this->color,$this->esfinal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAcad_cursodetalle->cambiarvalorcampo($this->idcursodetalle,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($idpadre)
	{
		try{				
			$datos2=$this->oDatAcad_cursodetalle->buscar(array("idpadre"=>$idpadre));
			if(!empty($datos2)){
				foreach ($datos2 as $v1){
					$datos2=$this->oDatAcad_cursodetalle->buscar(array("idpadre"=>$v1["idcursodetalle"]));
					if(!empty($datos2)){
						foreach ($datos2 as $dt2){					
							$this->oDatAcad_cursodetalle->eliminar2("idpadre",$dt2["idcursodetalle"]);
						}
					}
					$this->oDatAcad_cursodetalle->eliminar2("idpadre",$v1["idcursodetalle"]);
				}
				$this->oDatAcad_cursodetalle->eliminar2("idpadre",$idpadre);	
			}				
			return $this->oDatAcad_cursodetalle->eliminar2("idcursodetalle",$idpadre);			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function setIdcursodetalle($pk){
		try {
			$this->dataAcad_cursodetalle = $this->oDatAcad_cursodetalle->get($pk);
			if(empty($this->dataAcad_cursodetalle)){throw new Exception(JrTexto::_("Acad_cursodetalle").' '.JrTexto::_("not registered"));}
			$this->idcursodetalle = $this->dataAcad_cursodetalle["idcursodetalle"];
			$this->idcurso = $this->dataAcad_cursodetalle["idcurso"];
			$this->orden = $this->dataAcad_cursodetalle["orden"];
			$this->idrecurso = $this->dataAcad_cursodetalle["idrecurso"];
			$this->tiporecurso = $this->dataAcad_cursodetalle["tiporecurso"];
			$this->idlogro = $this->dataAcad_cursodetalle["idlogro"];
			$this->url = $this->dataAcad_cursodetalle["url"];
			$this->idpadre = $this->dataAcad_cursodetalle["idpadre"];
			$this->color = $this->dataAcad_cursodetalle["color"];
			$this->esfinal = $this->dataAcad_cursodetalle["esfinal"];
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {			
			$this->dataAcad_cursodetalle = $this->oDatAcad_cursodetalle->get($pk);
			if(empty($this->dataAcad_cursodetalle)){throw new Exception(JrTexto::_("cursodetalle").' '.JrTexto::_("not registered"));}
			return $this->oDatAcad_cursodetalle->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function setCampo_($pk, $propiedad, $valor, $tabla){
		try {			
			return $this->oDatAcad_cursodetalle->set_($pk, $propiedad, $valor, $tabla);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function importaramenu($idcurso,$userid,$datos,$tipo,$clonar){
		try {
			$this->oDatAcad_cursodetalle->iniciarTransaccion('import_cursodetalle');
			//$this->oDatAcad_cursodetalle 
			//$this->oDatNiveles
			$padres=array();
			$padresupdate=array();
			if($tipo=='nivel'){
				if($clonar=='no'){ // asignara los registros al curso ...
					if(!empty($datos))
					foreach ($datos as $k => $v){
						$update =false;
						if($v->idpadre==0)$idpadre=0;
						else{
							if(!empty($padres[$v->idnivel])) $idpadre=$padres[$v->idnivel];
							else{
								$idpadre=-1;
								$update=true;
							}
						}
						$idcursodet = $this->oDatAcad_cursodetalle->insertar($idcurso,$v->orden,$v->idnivel,$v->tipo,0,'',$idpadre,'',0);
						$padres[$v->idnivel]=$idcursodet;
						if($update==true){
							$padresupdate[$idcursodet]=$v;
						}						
					}
					if(!empty($padresupdate)){
						foreach ($padresupdate as $k => $v){							
							$this->oDatAcad_cursodetalle->actualizar($k,$idcurso,$v->orden,$v->idnivel,$v->tipo,'0','',$padres[$v->idpadre],'',0);
						}
					}
				}else{//clonara los registros
					
				}
			}else{
				if($clonar=='no'){
					if(!empty($datos))						
					foreach ($datos as $k => $v){						
						$update =false;
						if($v->idpadre==0)$idpadre=0;
						else{
							if(!empty($padres[$v->idcursodetalle])) $idpadre=$padres[$v->idcursodetalle];
							else{
								$idpadre=-1;
								$update=true;
							}
						}
						$data=array();
						$data["idcurso"]=$idcurso;
						$data["idpadre"]=$v->idpadre;
						$idcursodet = $this->oDatAcad_cursodetalle->duplicar($v->idcursodetalle,$data);
						$padres[$v->idcursodetalle]=$idcursodet;
						if($update==true){
							$padresupdate[$idcursodet]=$v;
						}
					}
					if(!empty($padresupdate)){
						foreach ($padresupdate as $k => $v){							
							$this->oDatAcad_cursodetalle->actualizar($k,$idcurso,$v->orden,$v->idnivel,$v->tiporecurso,'0','',$padres[$v->idpadre],'',0);
						}
					}
				}else{//clonara los registros
					//var_dump($datos);
				}
			}


			$this->oDatAcad_cursodetalle->terminarTransaccion('import_cursodetalle');
		} catch(Exception $e) {
			$this->oDatAcad_cursodetalle->cancelarTransaccion('import_cursodetalle');
			throw new Exception($e->getMessage());
		}
	}
	public function eliminarMenu($idcurso,$idcursodetalle,$tabla){
		$id=$idcursodetalle;
		try{
			$this->oDatAcad_cursodetalle->iniciarTransaccion('negeliminar'.$id);
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idpadre"=>$id,"complementario"=>true));
			if(!empty($datos)){
				foreach ($datos as $dt){
					$this->eliminarMenu($dt["idcurso"],$dt["idcursodetalle"],$tabla);
				}
			}
			$aeli=$this->oDatAcad_cursodetalle->buscar(array("idcursodetalle"=>$id));
			if(!empty($aeli[0])){
				$curdet=$aeli[0];
				$idrecurso=$curdet["idrecurso"];
				$tipo=$curdet["tiporecurso"];
				$this->oDatAcad_cursodetalle->eliminar($id,$tabla);
				//falta elimimnar en niveles
			}
			$this->oDatAcad_cursodetalle->terminarTransaccion('negeliminar'.$id);
		}catch(Exception $e) {	
		    $this->oDatAcad_cursodetalle->cancelarTransaccion('negeliminar'.$id);		
			throw new Exception($e->getMessage());
		}
	}

	public function buscarRecurso($filtros){
		try{
			return $this->oDatNiveles->buscar($filtros);
		} catch(Exception $e){
			throw new Exception($e->getMessage());	
		}
	}		
}