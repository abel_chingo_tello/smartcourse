<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotas_pestania', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegNotas_pestania 
{
	protected $idpestania;
	protected $nombre;
	protected $abreviatura;
	protected $tipo_pestania;
	protected $tipo_info;
	protected $info_valor;
	protected $color;
	protected $orden;
	protected $idarchivo;
	protected $idpestania_padre;
	
	protected $dataNotas_pestania;
	protected $oDatNotas_pestania;	

	public function __construct()
	{
		$this->oDatNotas_pestania = new DatNotas_pestania;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNotas_pestania->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotas_pestania->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNotas_pestania->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotas_pestania->get($this->idpestania);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_pestania', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatNotas_pestania->iniciarTransaccion('neg_i_Notas_pestania');
			$this->idpestania = $this->oDatNotas_pestania->insertar($this->nombre,$this->abreviatura,$this->tipo_pestania,$this->tipo_info,$this->info_valor,$this->color,$this->orden,$this->idarchivo,$this->idpestania_padre);
			$this->oDatNotas_pestania->terminarTransaccion('neg_i_Notas_pestania');	
			return $this->idpestania;
		} catch(Exception $e) {	
		    $this->oDatNotas_pestania->cancelarTransaccion('neg_i_Notas_pestania');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_pestania', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotas_pestania->actualizar($this->idpestania,$this->nombre,$this->abreviatura,$this->tipo_pestania,$this->tipo_info,$this->info_valor,$this->color,$this->orden,$this->idarchivo,$this->idpestania_padre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas_pestania', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas_pestania->eliminar($this->idpestania);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpestania($pk){
		try {
			$this->dataNotas_pestania = $this->oDatNotas_pestania->get($pk);
			if(empty($this->dataNotas_pestania)) {
				throw new Exception(JrTexto::_("Notas_pestania").' '.JrTexto::_("not registered"));
			}
			$this->idpestania = $this->dataNotas_pestania["idpestania"];
			$this->nombre = $this->dataNotas_pestania["nombre"];
			$this->abreviatura = $this->dataNotas_pestania["abreviatura"];
			$this->tipo_pestania = $this->dataNotas_pestania["tipo_pestania"];
			$this->tipo_info = $this->dataNotas_pestania["tipo_info"];
			$this->info_valor = $this->dataNotas_pestania["info_valor"];
			$this->color = $this->dataNotas_pestania["color"];
			$this->orden = $this->dataNotas_pestania["orden"];
			$this->idarchivo = $this->dataNotas_pestania["idarchivo"];
			$this->idpestania_padre = $this->dataNotas_pestania["idpestania_padre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('notas_pestania', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotas_pestania = $this->oDatNotas_pestania->get($pk);
			if(empty($this->dataNotas_pestania)) {
				throw new Exception(JrTexto::_("Notas_pestania").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNotas_pestania->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}