<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-04-2020
 * @copyright	Copyright (C) 24-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBolsa_empresas', RUTA_BASE);
JrCargador::clase('sys_datos::DatPersona_rol', RUTA_BASE);
JrCargador::clase('sys_datos::DatProyecto', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegBolsa_empresas 
{
	protected $idempresa;
	protected $nombre;
	protected $rason_social;
	protected $ruc;
	protected $logo;
	protected $direccion;
	protected $telefono;
	protected $representante;
	protected $usuario;
	protected $clave;
	protected $correo;
	protected $estado;
	protected $tipo_matricula=1;
	protected $idpersona=1;
	
	protected $dataBolsa_empresas;
	protected $oDatBolsa_empresas;	
	protected $oDatPersona_rol;
	protected $oDatProyecto;

	public function __construct()
	{
		$this->oDatBolsa_empresas = new DatBolsa_empresas;
		$this->oDatPersona_rol = new DatPersona_rol;
		$this->oDatProyecto = new DatProyecto;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBolsa_empresas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////
	private function buscarproyectos($arr){
		try{
			if(!empty($arr)){
				$idempresas = array_column($arr,'idempresa');
				$rs_proyectos = $this->oDatProyecto->buscar(array('idempresa'=>$idempresas)); 
				if(!empty($rs_proyectos)){
					$c = count($arr); $i=0;
					do{
						$idemp = $arr[$i]['idempresa'];
						//find proyectos
						$arr[$i]['proyectos'] = array_filter($rs_proyectos,function($el) use($idemp){
							return $el['idempresa'] == $idemp;
						});
						++$i;
					}while($i < $c);
				}//endif empty rs_proyecto
			}//endif empty arr
			return $arr;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBolsa_empresas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarempresaxpersona($filtros){
		try{
			$resultado = array();
			if(!empty($filtros['idpersonal'])){
				$rs_idempresa = $this->oDatPersona_rol->buscar(array_merge($filtros,array('groupby'=>'idempresa')));
				$filtroproyecto = array('estado'=> 1);
				if(!empty($rs_idempresa)){
					$idsempresa = array_column($rs_idempresa,'idempresa');
					$filtroproyecto['idempresa'] = $idsempresa;
					$rs_bolsaempresa = $this->oDatBolsa_empresas->buscar($filtroproyecto);
					if(!empty($rs_bolsaempresa)){
						$resultado = $this->buscarproyectos($rs_bolsaempresa);
					}
				}
			}
			return $resultado; 
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function empresas($filtros){
		try{
			$resultado = [];
			$rs_bolsaempresa = $this->oDatBolsa_empresas->buscar($filtros);
			if(!empty($rs_bolsaempresa)){
				$resultado = $this->buscarproyectos($rs_bolsaempresa);
			}
			return $resultado; 
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatBolsa_empresas->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBolsa_empresas->get($this->idempresa);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_empresas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatBolsa_empresas->iniciarTransaccion('neg_i_Bolsa_empresas');
			$this->idempresa = $this->oDatBolsa_empresas->insertar($this->nombre,$this->rason_social,$this->ruc,$this->logo,$this->direccion,$this->telefono,$this->representante,$this->usuario,$this->clave,$this->correo,$this->estado,$this->tipo_matricula,$this->idpersona);
			$this->oDatBolsa_empresas->terminarTransaccion('neg_i_Bolsa_empresas');	
			return $this->idempresa;
		} catch(Exception $e) {	
		    $this->oDatBolsa_empresas->cancelarTransaccion('neg_i_Bolsa_empresas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_empresas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBolsa_empresas->actualizar($this->idempresa,$this->nombre,$this->rason_social,$this->ruc,$this->logo,$this->direccion,$this->telefono,$this->representante,$this->usuario,$this->clave,$this->correo,$this->estado,$this->tipo_matricula,$this->idpersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function editarmultiple($data){
		try{
			if(empty($data)){ throw new Exception("data vacio"); }
			return $this->oDatBolsa_empresas->editarmultiple($data);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatBolsa_empresas->cambiarvalorcampo($this->idempresa,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bolsa_empresas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBolsa_empresas->eliminar($this->idempresa);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdempresa($pk){
		try {
			$this->dataBolsa_empresas = $this->oDatBolsa_empresas->get($pk);
			if(empty($this->dataBolsa_empresas)) {
				throw new Exception(JrTexto::_("Bolsa_empresas").' '.JrTexto::_("not registered"));
			}
			$this->idempresa = $this->dataBolsa_empresas["idempresa"];
			$this->nombre = $this->dataBolsa_empresas["nombre"];
			$this->rason_social = $this->dataBolsa_empresas["rason_social"];
			$this->ruc = $this->dataBolsa_empresas["ruc"];
			$this->logo = $this->dataBolsa_empresas["logo"];
			$this->direccion = $this->dataBolsa_empresas["direccion"];
			$this->telefono = $this->dataBolsa_empresas["telefono"];
			$this->representante = $this->dataBolsa_empresas["representante"];
			$this->usuario = $this->dataBolsa_empresas["usuario"];
			$this->clave = $this->dataBolsa_empresas["clave"];
			$this->correo = $this->dataBolsa_empresas["correo"];
			$this->estado = $this->dataBolsa_empresas["estado"];
			$this->tipo_matricula = $this->dataBolsa_empresas["tipo_matricula"];
			$this->idpersona = $this->dataBolsa_empresas["idpersona"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_empresas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBolsa_empresas = $this->oDatBolsa_empresas->get($pk);
			if(empty($this->dataBolsa_empresas)) {
				throw new Exception(JrTexto::_("Bolsa_empresas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBolsa_empresas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}