<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-05-2020
 * @copyright	Copyright (C) 04-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPortafolio_folder', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegPortafolio_folder 
{
	protected $idfolder;
	protected $idfolder_padre;
	protected $nombre;
	protected $idportafolio;
	
	protected $dataPortafolio_folder;
	protected $oDatPortafolio_folder;	

	public function __construct()
	{
		$this->oDatPortafolio_folder = new DatPortafolio_folder;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPortafolio_folder->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPortafolio_folder->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPortafolio_folder->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPortafolio_folder->get($this->idfolder);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('portafolio_folder', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPortafolio_folder->iniciarTransaccion('neg_i_Portafolio_folder');
			$this->idfolder = $this->oDatPortafolio_folder->insertar($this->idfolder_padre,$this->nombre,$this->idportafolio);
			$this->oDatPortafolio_folder->terminarTransaccion('neg_i_Portafolio_folder');	
			return $this->idfolder;
		} catch(Exception $e) {	
		    $this->oDatPortafolio_folder->cancelarTransaccion('neg_i_Portafolio_folder');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('portafolio_folder', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPortafolio_folder->actualizar($this->idfolder,$this->idfolder_padre,$this->nombre,$this->idportafolio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Portafolio_folder', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPortafolio_folder->eliminar($this->idfolder);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdfolder($pk){
		try {
			$this->dataPortafolio_folder = $this->oDatPortafolio_folder->get($pk);
			if(empty($this->dataPortafolio_folder)) {
				throw new Exception(JrTexto::_("Portafolio_folder").' '.JrTexto::_("not registered"));
			}
			$this->idfolder = $this->dataPortafolio_folder["idfolder"];
			$this->idfolder_padre = $this->dataPortafolio_folder["idfolder_padre"];
			$this->nombre = $this->dataPortafolio_folder["nombre"];
			$this->idportafolio = $this->dataPortafolio_folder["idportafolio"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('portafolio_folder', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPortafolio_folder = $this->oDatPortafolio_folder->get($pk);
			if(empty($this->dataPortafolio_folder)) {
				throw new Exception(JrTexto::_("Portafolio_folder").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPortafolio_folder->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}