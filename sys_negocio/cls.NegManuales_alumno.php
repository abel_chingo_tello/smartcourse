<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-01-2018
 * @copyright	Copyright (C) 17-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatManuales_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegManuales_alumno 
{
	protected $identrega;
	protected $idgrupo;
	protected $idalumno;
	protected $idmanual;
	protected $idaulagrupodetalle;
	protected $comentario;
	protected $fecha;
	protected $estado;
	protected $serie;	
	protected $dataManuales_alumno;
	protected $oDatManuales_alumno;	

	public function __construct()
	{
		$this->oDatManuales_alumno = new DatManuales_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatManuales_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatManuales_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatManuales_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatManuales_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatManuales_alumno->get($this->identrega);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('manuales_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatManuales_alumno->iniciarTransaccion('neg_i_Manuales_alumno');
			$this->identrega = $this->oDatManuales_alumno->insertar($this->idgrupo,$this->idalumno,$this->idmanual,$this->idaulagrupodetalle,$this->comentario,$this->fecha,$this->estado,$this->serie);
			$this->oDatManuales_alumno->terminarTransaccion('neg_i_Manuales_alumno');	
			return $this->identrega;
		} catch(Exception $e) {	
		    $this->oDatManuales_alumno->cancelarTransaccion('neg_i_Manuales_alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('manuales_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatManuales_alumno->actualizar($this->identrega,$this->idgrupo,$this->idalumno,$this->idmanual,$this->idaulagrupodetalle,$this->comentario,$this->fecha,$this->estado,$this->serie);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatManuales_alumno->cambiarvalorcampo($this->identrega,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Manuales_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatManuales_alumno->eliminar($this->identrega);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdentrega($pk){
		try {
			$this->dataManuales_alumno = $this->oDatManuales_alumno->get($pk);
			if(empty($this->dataManuales_alumno)) {
				throw new Exception(JrTexto::_("Manuales_alumno").' '.JrTexto::_("not registered"));
			}
			$this->identrega = $this->dataManuales_alumno["identrega"];
			$this->idgrupo = $this->dataManuales_alumno["idgrupo"];
			$this->idalumno = $this->dataManuales_alumno["idalumno"];
			$this->idmanual = $this->dataManuales_alumno["idmanual"];
			$this->idaulagrupodetalle = $this->dataManuales_alumno["idaulagrupodetalle"];
			$this->comentario = $this->dataManuales_alumno["comentario"];
			$this->fecha = $this->dataManuales_alumno["fecha"];
			$this->estado = $this->dataManuales_alumno["estado"];
			$this->serie = $this->dataManuales_alumno["serie"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('manuales_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataManuales_alumno = $this->oDatManuales_alumno->get($pk);
			if(empty($this->dataManuales_alumno)) {
				throw new Exception(JrTexto::_("Manuales_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatManuales_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}