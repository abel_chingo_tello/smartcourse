<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatVideosconferencia', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegVideosconferencia 
{
	protected $idmensaje;
	
	protected $dataVideosconferencia;
	protected $oDatVideosconferencia;	

	public function __construct()
	{
		$this->oDatVideosconferencia = new DatVideosconferencia;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatVideosconferencia->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatVideosconferencia->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatVideosconferencia->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatVideosconferencia->get($this->idmensaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('mensajes', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatVideosconferencia->iniciarTransaccion('neg_i_Videosconferencia');
			$this->idmensaje = $this->oDatVideosconferencia->insertar();
			$this->oDatVideosconferencia->terminarTransaccion('neg_i_Videosconferencia');	
			return $this->idmensaje;
		} catch(Exception $e) {	
		    $this->oDatVideosconferencia->cancelarTransaccion('neg_i_Videosconferencia');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('mensajes', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatVideosconferencia->actualizar($this->idmensaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Mensajes', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatVideosconferencia->eliminar($this->idmensaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmensaje($pk){
		try {
			$this->dataVideosconferencia = $this->oDatVideosconferencia->get($pk);
			if(empty($this->dataVideosconferencia)) {
				throw new Exception(JrTexto::_("NegVideosconferencia").' '.JrTexto::_("not registered"));
			}
			$this->idmensaje = $this->dataVideosconferencia["idmensaje"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('NegVideosconferencia', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataVideosconferencia = $this->oDatVideosconferencia->get($pk);
			if(empty($this->dataVideosconferencia)) {
				throw new Exception(JrTexto::_("NegVideosconferencia").' '.JrTexto::_("not registered"));
			}

			return $this->oDatVideosconferencia->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}