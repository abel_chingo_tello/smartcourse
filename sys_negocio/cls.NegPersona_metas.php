<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-01-2018
 * @copyright	Copyright (C) 04-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona_metas', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPersona_metas 
{
	protected $idmeta;
	protected $idpersona;
	protected $meta;
	protected $objetivo;
	protected $mostrar;
	
	protected $dataPersona_metas;
	protected $oDatPersona_metas;	

	public function __construct()
	{
		$this->oDatPersona_metas = new DatPersona_metas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona_metas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPersona_metas->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona_metas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPersona_metas->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPersona_metas->get($this->idmeta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_metas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPersona_metas->iniciarTransaccion('neg_i_Persona_metas');
			$this->idmeta = $this->oDatPersona_metas->insertar($this->idpersona,$this->meta,$this->objetivo,$this->mostrar);
			$this->oDatPersona_metas->terminarTransaccion('neg_i_Persona_metas');	
			return $this->idmeta;
		} catch(Exception $e) {	
		    $this->oDatPersona_metas->cancelarTransaccion('neg_i_Persona_metas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_metas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersona_metas->actualizar($this->idmeta,$this->idpersona,$this->meta,$this->objetivo,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatPersona_metas->cambiarvalorcampo($this->idmeta,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona_metas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona_metas->eliminar($this->idmeta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmeta($pk){
		try {
			$this->dataPersona_metas = $this->oDatPersona_metas->get($pk);
			if(empty($this->dataPersona_metas)) {
				throw new Exception(JrTexto::_("Persona_metas").' '.JrTexto::_("not registered"));
			}
			$this->idmeta = $this->dataPersona_metas["idmeta"];
			$this->idpersona = $this->dataPersona_metas["idpersona"];
			$this->meta = $this->dataPersona_metas["meta"];
			$this->objetivo = $this->dataPersona_metas["objetivo"];
			$this->mostrar = $this->dataPersona_metas["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona_metas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona_metas = $this->oDatPersona_metas->get($pk);
			if(empty($this->dataPersona_metas)) {
				throw new Exception(JrTexto::_("Persona_metas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona_metas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}