<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		07-04-2020
 * @copyright	Copyright (C) 07-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEvento_programacion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegEvento_programacion 
{
	protected $id_programacion;
	protected $fecha;
	protected $titulo;
	protected $detalle;
	protected $hora_comienzo;
	protected $hora_fin;
	protected $idpersona;
	protected $idgrupoaula;
	protected $idgrupoauladetalle;
	
	protected $dataEvento_programacion;
	protected $oDatEvento_programacion;	

	public function __construct()
	{
		$this->oDatEvento_programacion = new DatEvento_programacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatEvento_programacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatEvento_programacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatEvento_programacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatEvento_programacion->get($this->id_programacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('evento_programacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatEvento_programacion->iniciarTransaccion('neg_i_Evento_programacion');
			$this->id_programacion = $this->oDatEvento_programacion->insertar($this->fecha,$this->titulo,$this->detalle,$this->hora_comienzo,$this->hora_fin,$this->idpersona,$this->idgrupoaula,$this->idgrupoauladetalle);
			$this->oDatEvento_programacion->terminarTransaccion('neg_i_Evento_programacion');	
			return $this->id_programacion;
		} catch(Exception $e) {	
		    $this->oDatEvento_programacion->cancelarTransaccion('neg_i_Evento_programacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('evento_programacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatEvento_programacion->actualizar($this->id_programacion,$this->fecha,$this->titulo,$this->detalle,$this->hora_comienzo,$this->hora_fin,$this->idpersona,$this->idgrupoaula,$this->idgrupoauladetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Evento_programacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatEvento_programacion->eliminar($this->id_programacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_programacion($pk){
		try {
			$this->dataEvento_programacion = $this->oDatEvento_programacion->get($pk);
			if(empty($this->dataEvento_programacion)) {
				throw new Exception(JrTexto::_("Evento_programacion").' '.JrTexto::_("not registered"));
			}
			$this->id_programacion = $this->dataEvento_programacion["id_programacion"];
			$this->fecha = $this->dataEvento_programacion["fecha"];
			$this->titulo = $this->dataEvento_programacion["titulo"];
			$this->detalle = $this->dataEvento_programacion["detalle"];
			$this->hora_comienzo = $this->dataEvento_programacion["hora_comienzo"];
			$this->hora_fin = $this->dataEvento_programacion["hora_fin"];
			$this->idpersona = $this->dataEvento_programacion["idpersona"];
			$this->idgrupoaula = $this->dataEvento_programacion["idgrupoaula"];
			$this->idgrupoauladetalle = $this->dataEvento_programacion["idgrupoauladetalle"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('evento_programacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataEvento_programacion = $this->oDatEvento_programacion->get($pk);
			if(empty($this->dataEvento_programacion)) {
				throw new Exception(JrTexto::_("Evento_programacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatEvento_programacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}