<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRub_detalle_respuestas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRub_detalle_respuestas 
{
	protected $id_det_rptas;
	protected $id_respuestas;
	protected $id_pregunta;
	protected $respuesta;
	protected $puntaje_obtenido;
	protected $id_dimension;
	
	protected $dataRub_detalle_respuestas;
	protected $oDatRub_detalle_respuestas;	

	public function __construct()
	{
		$this->oDatRub_detalle_respuestas = new DatRub_detalle_respuestas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRub_detalle_respuestas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRub_detalle_respuestas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRub_detalle_respuestas->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRub_detalle_respuestas->get($this->id_det_rptas);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_detalle_respuestas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRub_detalle_respuestas->iniciarTransaccion('neg_i_Rub_detalle_respuestas');
			$this->id_det_rptas = $this->oDatRub_detalle_respuestas->insertar($this->id_respuestas,$this->id_pregunta,$this->respuesta,$this->puntaje_obtenido,$this->id_dimension);
			$this->oDatRub_detalle_respuestas->terminarTransaccion('neg_i_Rub_detalle_respuestas');	
			return $this->id_det_rptas;
		} catch(Exception $e) {	
		    $this->oDatRub_detalle_respuestas->cancelarTransaccion('neg_i_Rub_detalle_respuestas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_detalle_respuestas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRub_detalle_respuestas->actualizar($this->id_det_rptas,$this->id_respuestas,$this->id_pregunta,$this->respuesta,$this->puntaje_obtenido,$this->id_dimension);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rub_detalle_respuestas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRub_detalle_respuestas->eliminar($this->id_det_rptas);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_det_rptas($pk){
		try {
			$this->dataRub_detalle_respuestas = $this->oDatRub_detalle_respuestas->get($pk);
			if(empty($this->dataRub_detalle_respuestas)) {
				throw new Exception(JrTexto::_("Rub_detalle_respuestas").' '.JrTexto::_("not registered"));
			}
			$this->id_det_rptas = $this->dataRub_detalle_respuestas["id_det_rptas"];
			$this->id_respuestas = $this->dataRub_detalle_respuestas["id_respuestas"];
			$this->id_pregunta = $this->dataRub_detalle_respuestas["id_pregunta"];
			$this->respuesta = $this->dataRub_detalle_respuestas["respuesta"];
			$this->puntaje_obtenido = $this->dataRub_detalle_respuestas["puntaje_obtenido"];
			$this->id_dimension = $this->dataRub_detalle_respuestas["id_dimension"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rub_detalle_respuestas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRub_detalle_respuestas = $this->oDatRub_detalle_respuestas->get($pk);
			if(empty($this->dataRub_detalle_respuestas)) {
				throw new Exception(JrTexto::_("Rub_detalle_respuestas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRub_detalle_respuestas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}