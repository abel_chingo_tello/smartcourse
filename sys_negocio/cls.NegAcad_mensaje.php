<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-03-2020
 * @copyright	Copyright (C) 17-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_mensaje', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_mensaje 
{
	protected $id;
	protected $de;
	protected $asunto;
	protected $mensaje;
	protected $fecha_hora;
	protected $estado;
	
	protected $dataAcad_mensaje;
	protected $oDatAcad_mensaje;	

	public function __construct()
	{
		$this->oDatAcad_mensaje = new DatAcad_mensaje;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_mensaje->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_mensaje->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_mensaje->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_mensaje->get($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar($estados, $para)
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_mensaje', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_mensaje->iniciarTransaccion('neg_i_Acad_mensaje');
			$id = $this->oDatAcad_mensaje->insertar($estados, $para);
			$this->oDatAcad_mensaje->terminarTransaccion('neg_i_Acad_mensaje');	
			return $id;
		} catch(Exception $e) {	
		    $this->oDatAcad_mensaje->cancelarTransaccion('neg_i_Acad_mensaje');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar($estados)
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_mensaje', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_mensaje->actualizar($estados);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_mensaje', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_mensaje->eliminar($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataAcad_mensaje = $this->oDatAcad_mensaje->get($pk);
			if(empty($this->dataAcad_mensaje)) {
				throw new Exception(JrTexto::_("Acad_mensaje").' '.JrTexto::_("not registered"));
			}
			$this->id = $this->dataAcad_mensaje["id"];
			$this->de = $this->dataAcad_mensaje["de"];
			$this->asunto = $this->dataAcad_mensaje["asunto"];
			$this->mensaje = $this->dataAcad_mensaje["mensaje"];
			$this->fecha_hora = $this->dataAcad_mensaje["fecha_hora"];
			$this->estado = $this->dataAcad_mensaje["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor, $tabla = 'acad_mensaje'){
		try {
			/*if(!NegSesion::tiene_acceso('acad_mensaje', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			// $this->dataAcad_mensaje = $this->oDatAcad_mensaje->get($pk);
			// if(empty($this->dataAcad_mensaje)) {
			// 	throw new Exception(JrTexto::_("Acad_mensaje").' '.JrTexto::_("not registered"));
			// }

			return $this->oDatAcad_mensaje->set($pk, $propiedad, $valor, $tabla);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}