<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		02-12-2020
 * @copyright	Copyright (C) 02-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatDescargas_asignadas', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegDescargas_asignadas 
{
	
	protected $id;
	protected $idpersona;
	protected $total_asignado;
	protected $codigo_suscripcion;
	protected $idempresa;
	protected $idproyecto;
	//protected $estado;
	//protected $idusuario;

	
	
	protected $dataDescargas_asignadas;
	protected $oDatDescargas_asignadas;	

	public function __construct()
	{
		$this->oDatDescargas_asignadas = new DatDescargas_asignadas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatDescargas_asignadas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try{
			return $this->oDatDescargas_asignadas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Descargas_asignadas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatDescargas_asignadas->iniciarTransaccion('neg_i_Descargas_asignadas');
			$this->id = $this->oDatDescargas_asignadas->insertar($this->idpersona,$this->total_asignado,$this->codigo_suscripcion,$this->idempresa,$this->idproyecto);
			$this->oDatDescargas_asignadas->terminarTransaccion('neg_i_Descargas_asignadas');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatDescargas_asignadas->cancelarTransaccion('neg_i_Descargas_asignadas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Descargas_asignadas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatDescargas_asignadas->actualizar($this->id, $this->idpersona,$this->total_asignado,$this->codigo_suscripcion,$this->idempresa,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	
	public function actualizar2($estados,$condicion){
		try {
			return $this->oDatDescargas_asignadas->actualizar2($estados,$condicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatDescargas_asignadas->cambiarvalorcampo($this->id,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Descargas_asignadas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatDescargas_asignadas->eliminar($this->id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecurso($pk){
		try {
			$this->dataDescargas_asignadas = $this->oDatDescargas_asignadas->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataDescargas_asignadas)) {
				throw new Exception(JrTexto::_("Descargas_asignadas").' '.JrTexto::_("not registered"));
			}
			$this->id=$this->dataDescargas_asignadas["id"];
			$this->idpersona = $this->dataDescargas_asignadas["idpersona"];
			$this->total_asignado = $this->dataDescargas_asignadas["total_asignado"];
			$this->codigo_suscripcion = $this->dataDescargas_asignadas["codigo_suscripcion"];
			$this->idempresa = $this->dataDescargas_asignadas["idempresa"];
			$this->idproyecto = $this->dataDescargas_asignadas["idproyecto"];
			//$this->estado = $this->dataDescargas_asignadas["estado"];
		//	$this->idusuario = $this->dataDescargas_asignadas["idusuario"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('Descargas_asignadas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataDescargas_asignadas = $this->oDatDescargas_asignadas->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataDescargas_asignadas)) {
				throw new Exception(JrTexto::_("Descargas_asignadas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatDescargas_asignadas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}