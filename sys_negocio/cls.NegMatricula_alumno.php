<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		18-04-2017
 * @copyright	Copyright (C) 18-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMatricula_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegMatricula_alumno 
{
	protected $idmatriculaalumno;
	protected $idalumno;
	protected $codigo;
	protected $tipo;
	protected $orden;
	protected $fechamatricula;
	protected $fechacaduca;
	protected $regusuario;
	protected $fecharegistro;
	
	protected $dataMatricula_alumno;
	protected $oDatMatricula_alumno;	

	public function __construct()
	{
		$this->oDatMatricula_alumno = new DatMatricula_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMatricula_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatMatricula_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMatricula_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMatricula_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMatricula_alumno->get($this->idmatriculaalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('matricula_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatMatricula_alumno->iniciarTransaccion('neg_i_Matricula_alumno');
			$this->idmatriculaalumno = $this->oDatMatricula_alumno->insertar($this->idalumno,$this->codigo,$this->tipo,$this->orden,$this->fechamatricula,$this->fechacaduca,$this->regusuario,$this->fecharegistro);
			//$this->oDatMatricula_alumno->terminarTransaccion('neg_i_Matricula_alumno');	
			return $this->idmatriculaalumno;
		} catch(Exception $e) {	
		   //$this->oDatMatricula_alumno->cancelarTransaccion('neg_i_Matricula_alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('matricula_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatMatricula_alumno->actualizar($this->idmatriculaalumno,$this->idalumno,$this->codigo,$this->tipo,$this->orden,$this->fechamatricula,$this->fechacaduca,$this->regusuario,$this->fecharegistro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Matricula_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatMatricula_alumno->eliminar($this->idmatriculaalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmatriculaalumno($pk){
		try {
			$this->dataMatricula_alumno = $this->oDatMatricula_alumno->get($pk);
			if(empty($this->dataMatricula_alumno)) {
				throw new Exception(JrTexto::_("Matricula_alumno").' '.JrTexto::_("not registered"));
			}
			$this->idmatriculaalumno = $this->dataMatricula_alumno["idmatriculaalumno"];
			$this->idalumno = $this->dataMatricula_alumno["idalumno"];
			$this->codigo = $this->dataMatricula_alumno["codigo"];
			$this->tipo = $this->dataMatricula_alumno["tipo"];
			$this->orden = $this->dataMatricula_alumno["orden"];
			$this->fechamatricula = $this->dataMatricula_alumno["fechamatricula"];
			$this->fechacaduca = $this->dataMatricula_alumno["fechacaduca"];
			$this->regusuario = $this->dataMatricula_alumno["regusuario"];
			$this->fecharegistro = $this->dataMatricula_alumno["fecharegistro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('matricula_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataMatricula_alumno = $this->oDatMatricula_alumno->get($pk);
			if(empty($this->dataMatricula_alumno)) {
				throw new Exception(JrTexto::_("Matricula_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMatricula_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setIdalumno($idalumno)
	{
		try {
			$this->idalumno= NegTools::validar('todo', $idalumno, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setCodigo($codigo)
	{
		try {
			$this->codigo= NegTools::validar('todo', $codigo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTipo($tipo)
	{
		try {
			$this->tipo= NegTools::validar('todo', $tipo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setOrden($orden)
	{
		try {
			$this->orden= NegTools::validar('todo', $orden, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFechamatricula($fechamatricula)
	{
		try {
			$this->fechamatricula= NegTools::validar('todo', $fechamatricula, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFechacaduca($fechacaduca)
	{
		try {
			$this->fechacaduca= NegTools::validar('todo', $fechacaduca, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setRegusuario($regusuario)
	{
		try {
			$this->regusuario= NegTools::validar('todo', $regusuario, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFecharegistro($fecharegistro)
	{
		try {
			$this->fecharegistro= NegTools::validar('todo', $fecharegistro, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	


	public function auto_mejora($filtros=[])
	{
		try {
			return $this->oDatMatricula_alumno->auto_mejora($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}