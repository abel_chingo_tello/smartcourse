<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-03-2020
 * @copyright	Copyright (C) 11-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_curso_consulta', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_curso_consulta 
{
	protected $codigo;
	protected $idcurso;
	protected $idcategoria;
	protected $idgrupoaula;
	protected $iddetalle;
	protected $tipocurso;
	protected $idpersona;
	protected $contenido;
	protected $fecha_hora;
	protected $respuesta;
	
	protected $dataAcad_curso_consulta;
	protected $oDatAcad_curso_consulta;	

	public function __construct()
	{
		$this->oDatAcad_curso_consulta = new DatAcad_curso_consulta;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_curso_consulta->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_curso_consulta->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_curso_consulta->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_curso_consulta->get($this->codigo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar($estados = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso_consulta', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_curso_consulta->iniciarTransaccion('neg_i_Acad_curso_consulta');
			$codigo = $this->oDatAcad_curso_consulta->insertar($estados);
			$this->oDatAcad_curso_consulta->terminarTransaccion('neg_i_Acad_curso_consulta');	
			return $codigo;
		} catch(Exception $e) {	
		    $this->oDatAcad_curso_consulta->cancelarTransaccion('neg_i_Acad_curso_consulta');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar($estados = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso_consulta', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_curso_consulta->actualizar($estados);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_curso_consulta', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_curso_consulta->eliminar($this->codigo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCodigo($pk){
		try {
			$this->dataAcad_curso_consulta = $this->oDatAcad_curso_consulta->get($pk);
			if(empty($this->dataAcad_curso_consulta)) {
				throw new Exception(JrTexto::_("Acad_curso_consulta").' '.JrTexto::_("not registered"));
			}
			$this->codigo = $this->dataAcad_curso_consulta["codigo"];
			$this->idcurso = $this->dataAcad_curso_consulta["idcurso"];
			$this->idcategoria = $this->dataAcad_curso_consulta["idcategoria"];
			$this->idgrupoaula = $this->dataAcad_curso_consulta["idgrupoaula"];
			$this->iddetalle = $this->dataAcad_curso_consulta["iddetalle"];
			$this->tipocurso = $this->dataAcad_curso_consulta["tipocurso"];
			$this->idpersona = $this->dataAcad_curso_consulta["idpersona"];
			$this->contenido = $this->dataAcad_curso_consulta["contenido"];
			$this->fecha_hora = $this->dataAcad_curso_consulta["fecha_hora"];
			$this->respuesta = $this->dataAcad_curso_consulta["respuesta"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso_consulta', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_curso_consulta = $this->oDatAcad_curso_consulta->get($pk);
			if(empty($this->dataAcad_curso_consulta)) {
				throw new Exception(JrTexto::_("Acad_curso_consulta").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_curso_consulta->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}