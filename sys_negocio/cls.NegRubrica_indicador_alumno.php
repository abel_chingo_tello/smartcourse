<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-04-2020
 * @copyright	Copyright (C) 24-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRubrica_indicador_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRubrica_indicador_alumno
{
	protected $idrubrica_indicador_alumno;
	protected $idalumno;
	protected $idrubrica_indicador;
	protected $idrubrica;
	protected $idrubrica_instancia;
	protected $idgrupoauladetalle;
	protected $dataRubrica_indicador_alumno;
	protected $oDatRubrica_indicador_alumno;

	public function __construct()
	{
		$this->oDatRubrica_indicador_alumno = new DatRubrica_indicador_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatRubrica_indicador_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////
	public function contar($filtros = array())
	{
		try {
			return $this->oDatRubrica_indicador_alumno->contar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRubrica_indicador_alumno->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarCriterio($filtros = array())
	{
		try {
			return $this->oDatRubrica_indicador_alumno->buscarCriterio($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatRubrica_indicador_alumno->buscar();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRubrica_indicador_alumno->get($this->idrubrica_indicador_alumno);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_indicador_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRubrica_indicador_alumno->iniciarTransaccion('neg_i_Rubrica_indicador_alumno');
			$this->idrubrica_indicador_alumno = $this->oDatRubrica_indicador_alumno->insertar($this->idalumno, $this->idrubrica_indicador, $this->idrubrica, $this->idrubrica_instancia, $this->idgrupoauladetalle);
			$this->oDatRubrica_indicador_alumno->terminarTransaccion('neg_i_Rubrica_indicador_alumno');
			return $this->idrubrica_indicador_alumno;
		} catch (Exception $e) {
			$this->oDatRubrica_indicador_alumno->cancelarTransaccion('neg_i_Rubrica_indicador_alumno');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_indicador_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica_indicador_alumno->actualizar($this->idrubrica_indicador_alumno, $this->idalumno, $this->idrubrica_indicador, $this->idrubrica, $this->idrubrica_instancia, $this->idgrupoauladetalle);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rubrica_indicador_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica_indicador_alumno->eliminar($this->idrubrica_indicador_alumno);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrubrica_indicador_alumno($pk)
	{
		try {
			$this->dataRubrica_indicador_alumno = $this->oDatRubrica_indicador_alumno->get($pk);
			if (empty($this->dataRubrica_indicador_alumno)) {
				throw new Exception(JrTexto::_("Rubrica_indicador_alumno") . ' ' . JrTexto::_("not registered"));
			}
			$this->idrubrica_indicador_alumno = $this->dataRubrica_indicador_alumno["idrubrica_indicador_alumno"];
			$this->idalumno = $this->dataRubrica_indicador_alumno["idalumno"];
			$this->idrubrica_indicador = $this->dataRubrica_indicador_alumno["idrubrica_indicador"];
			$this->idrubrica = $this->dataRubrica_indicador_alumno["idrubrica"];
			$this->idrubrica_instancia = $this->dataRubrica_indicador_alumno["idrubrica_instancia"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_indicador_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRubrica_indicador_alumno = $this->oDatRubrica_indicador_alumno->get($pk);
			if (empty($this->dataRubrica_indicador_alumno)) {
				throw new Exception(JrTexto::_("Rubrica_indicador_alumno") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatRubrica_indicador_alumno->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
