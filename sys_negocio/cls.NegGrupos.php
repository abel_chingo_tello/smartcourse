<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		20-04-2017
 * @copyright	Copyright (C) 20-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatGrupos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegGrupos 
{
	protected $idgrupo;
	protected $iddocente;
	protected $idlocal;
	protected $idambiente;
	protected $idasistente;
	protected $fechainicio;
	protected $fechafin;
	protected $dias;
	protected $horas;
	protected $horainicio;
	protected $horafin;
	protected $valor;
	protected $valor_asi;
	protected $regusuario;
	protected $regfecha;
	protected $matriculados;
	protected $nivel;
	protected $codigo;
	
	protected $dataGrupos;
	protected $oDatGrupos;	

	public function __construct()
	{
		$this->oDatGrupos = new DatGrupos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatGrupos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatGrupos->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatGrupos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatGrupos->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatGrupos->get($this->idgrupo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('grupos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatGrupos->iniciarTransaccion('neg_i_Grupos');
			$this->idgrupo = $this->oDatGrupos->insertar($this->iddocente,$this->idlocal,$this->idambiente,$this->idasistente,$this->fechainicio,$this->fechafin,$this->dias,$this->horas,$this->horainicio,$this->horafin,$this->valor,$this->valor_asi,$this->regusuario,$this->regfecha,$this->matriculados,$this->nivel,$this->codigo);
			//$this->oDatGrupos->terminarTransaccion('neg_i_Grupos');	
			return $this->idgrupo;
		} catch(Exception $e) {	
		   //$this->oDatGrupos->cancelarTransaccion('neg_i_Grupos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('grupos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatGrupos->actualizar($this->idgrupo,$this->iddocente,$this->idlocal,$this->idambiente,$this->idasistente,$this->fechainicio,$this->fechafin,$this->dias,$this->horas,$this->horainicio,$this->horafin,$this->valor,$this->valor_asi,$this->regusuario,$this->regfecha,$this->matriculados,$this->nivel,$this->codigo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Grupos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatGrupos->eliminar($this->idgrupo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdgrupo($pk){
		try {
			$this->dataGrupos = $this->oDatGrupos->get($pk);
			if(empty($this->dataGrupos)) {
				throw new Exception(JrTexto::_("Grupos").' '.JrTexto::_("not registered"));
			}
			$this->idgrupo = $this->dataGrupos["idgrupo"];
			$this->iddocente = $this->dataGrupos["iddocente"];
			$this->idlocal = $this->dataGrupos["idlocal"];
			$this->idambiente = $this->dataGrupos["idambiente"];
			$this->idasistente = $this->dataGrupos["idasistente"];
			$this->fechainicio = $this->dataGrupos["fechainicio"];
			$this->fechafin = $this->dataGrupos["fechafin"];
			$this->dias = $this->dataGrupos["dias"];
			$this->horas = $this->dataGrupos["horas"];
			$this->horainicio = $this->dataGrupos["horainicio"];
			$this->horafin = $this->dataGrupos["horafin"];
			$this->valor = $this->dataGrupos["valor"];
			$this->valor_asi = $this->dataGrupos["valor_asi"];
			$this->regusuario = $this->dataGrupos["regusuario"];
			$this->regfecha = $this->dataGrupos["regfecha"];
			$this->matriculados = $this->dataGrupos["matriculados"];
			$this->nivel = $this->dataGrupos["nivel"];
			$this->codigo = $this->dataGrupos["codigo"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('grupos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataGrupos = $this->oDatGrupos->get($pk);
			if(empty($this->dataGrupos)) {
				throw new Exception(JrTexto::_("Grupos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatGrupos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setIddocente($iddocente)
	{
		try {
			$this->iddocente= NegTools::validar('todo', $iddocente, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdlocal($idlocal)
	{
		try {
			$this->idlocal= NegTools::validar('todo', $idlocal, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdambiente($idambiente)
	{
		try {
			$this->idambiente= NegTools::validar('todo', $idambiente, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdasistente($idasistente)
	{
		try {
			$this->idasistente= NegTools::validar('todo', $idasistente, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFechainicio($fechainicio)
	{
		try {
			$this->fechainicio= NegTools::validar('todo', $fechainicio, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFechafin($fechafin)
	{
		try {
			$this->fechafin= NegTools::validar('todo', $fechafin, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setDias($dias)
	{
		try {
			$this->dias= NegTools::validar('todo', $dias, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setHoras($horas)
	{
		try {
			$this->horas= NegTools::validar('todo', $horas, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setHorainicio($horainicio)
	{
		try {
			$this->horainicio= NegTools::validar('todo', $horainicio, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setHorafin($horafin)
	{
		try {
			$this->horafin= NegTools::validar('todo', $horafin, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setValor($valor)
	{
		try {
			$this->valor= NegTools::validar('todo', $valor, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setValor_asi($valor_asi)
	{
		try {
			$this->valor_asi= NegTools::validar('todo', $valor_asi, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setRegusuario($regusuario)
	{
		try {
			$this->regusuario= NegTools::validar('todo', $regusuario, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setRegfecha($regfecha)
	{
		try {
			$this->regfecha= NegTools::validar('todo', $regfecha, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setMatriculados($matriculados)
	{
		try {
			$this->matriculados= NegTools::validar('todo', $matriculados, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setNivel($nivel)
	{
		try {
			$this->nivel= NegTools::validar('todo', $nivel, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setCodigo($codigo)
	{
		try {
			$this->codigo= NegTools::validar('todo', $codigo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}