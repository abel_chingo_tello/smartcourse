<?php
/**
 * @autor		Sistec Peru
 */

defined('RUTA_RAIZ') or die();

JrCargador::clase('sys_negocio::NegBase', RUTA_BASE);
JrCargador::clase('sys_datos::sistema::DatSis_configuracion', RUTA_BASE);

class NegConfiguracion extends NegBase
{
	protected $oDatConfiguracion;
	protected static $instancia = null;
	protected $configs = array();
	
	public function __construct()
	{
		$this->oDatConfiguracion = new DatSis_configuracion;
		$configs = $this->oDatConfiguracion->buscar();
		
		foreach($configs as $config) {
			$this->configs[$config['config']] = $config['valor'];
		}
		
		parent::__construct($this->oDatConfiguracion);
	}
	
	public static function &getInstancia()
	{
		if(!is_object(self::$instancia)) {
			self::$instancia = new self;
		}
		
		return self::$instancia;
	}
	
	public function listar($autocargar = true)
	{
		try {
			return $this->oDatConfiguracion->buscar($autocargar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function get($config)
	{
		try {
			if(!isset($this->configs[$config])) {
				$this->configs[$config] = $this->oDatConfiguracion->get($config);
			}
			
			return $this->configs[$config];
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function set($config, $valor)
	{
		try {
			return $this->oDatConfiguracion->set($config, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public static function get_($config)
	{
		try {
			$oNegConfig = NegConfiguracion::getInstancia();
			
			return $oNegConfig->get($config);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public static function set_($config, $valor)
	{
		try {
			$oNegConfig = NegConfiguracion::getInstancia();
			return $oNegConfig->set($config, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}