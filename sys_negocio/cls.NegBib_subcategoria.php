<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_subcategoria', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegBib_subcategoria 
{
	protected $id_subcategoria;
	protected $id_categoria;
	protected $descripcion;
	
	protected $dataBib_subcategoria;
	protected $oDatBib_subcategoria;	

	public function __construct()
	{
		$this->oDatBib_subcategoria = new DatBib_subcategoria;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_subcategoria->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_subcategoria->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_subcategoria->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_subcategoria->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_subcategoria->get($this->id_subcategoria);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_subcategoria', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_subcategoria->iniciarTransaccion('neg_i_Bib_subcategoria');
			$this->id_subcategoria = $this->oDatBib_subcategoria->insertar($this->id_categoria,$this->descripcion);
			//$this->oDatBib_subcategoria->terminarTransaccion('neg_i_Bib_subcategoria');	
			return $this->id_subcategoria;
		} catch(Exception $e) {	
		   //$this->oDatBib_subcategoria->cancelarTransaccion('neg_i_Bib_subcategoria');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_subcategoria', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBib_subcategoria->actualizar($this->id_subcategoria,$this->id_categoria,$this->descripcion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_subcategoria', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_subcategoria->eliminar($this->id_subcategoria);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_subcategoria($pk){
		try {
			$this->dataBib_subcategoria = $this->oDatBib_subcategoria->get($pk);
			if(empty($this->dataBib_subcategoria)) {
				throw new Exception(JrTexto::_("Bib_subcategoria").' '.JrTexto::_("not registered"));
			}
			$this->id_subcategoria = $this->dataBib_subcategoria["id_subcategoria"];
			$this->id_categoria = $this->dataBib_subcategoria["id_categoria"];
			$this->descripcion = $this->dataBib_subcategoria["descripcion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_subcategoria', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_subcategoria = $this->oDatBib_subcategoria->get($pk);
			if(empty($this->dataBib_subcategoria)) {
				throw new Exception(JrTexto::_("Bib_subcategoria").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_subcategoria->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}