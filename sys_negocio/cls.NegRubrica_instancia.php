<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-04-2020
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRubrica_instancia', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRubrica_instancia
{
	protected $idrubrica_instancia;
	protected $idrubrica;
	protected $idcursodetalle;
	protected $idpestania;
	protected $idcomplementario;
	protected $idcurso;
	protected $dataRubrica_instancia;
	protected $oDatRubrica_instancia;

	public function __construct()
	{
		$this->oDatRubrica_instancia = new DatRubrica_instancia;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatRubrica_instancia->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRubrica_instancia->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRubrica_instancia->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRubrica_instancia->get($this->idrubrica_instancia);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_instancia', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRubrica_instancia->iniciarTransaccion('neg_i_Rubrica_instancia');
			$this->idrubrica_instancia = $this->oDatRubrica_instancia->insertar($this->idrubrica, $this->idcursodetalle, $this->idpestania, $this->idcomplementario, $this->idcurso);
			$this->oDatRubrica_instancia->terminarTransaccion('neg_i_Rubrica_instancia');
			return $this->idrubrica_instancia;
		} catch (Exception $e) {
			$this->oDatRubrica_instancia->cancelarTransaccion('neg_i_Rubrica_instancia');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_instancia', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica_instancia->actualizar($this->idrubrica_instancia, $this->idrubrica, $this->idcursodetalle, $this->idpestania, $this->idcomplementario, $this->idcurso);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rubrica_instancia', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica_instancia->eliminar($this->idrubrica_instancia);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrubrica_instancia($pk)
	{
		try {
			$this->dataRubrica_instancia = $this->oDatRubrica_instancia->get($pk);
			if (empty($this->dataRubrica_instancia)) {
				throw new Exception(JrTexto::_("Rubrica_instancia") . ' ' . JrTexto::_("not registered"));
			}
			$this->idrubrica_instancia = $this->dataRubrica_instancia["idrubrica_instancia"];
			$this->idrubrica = $this->dataRubrica_instancia["idrubrica"];
			$this->idcursodetalle = $this->dataRubrica_instancia["idcursodetalle"];
			$this->idpestania = $this->dataRubrica_instancia["idpestania"];
			$this->idcomplementario = $this->dataRubrica_instancia["idcomplementario"];
			$this->idcurso = $this->dataRubrica_instancia["idcurso"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_instancia', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRubrica_instancia = $this->oDatRubrica_instancia->get($pk);
			if (empty($this->dataRubrica_instancia)) {
				throw new Exception(JrTexto::_("Rubrica_instancia") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatRubrica_instancia->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
