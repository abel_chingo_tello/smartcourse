<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		14-11-2016
 * @copyright	Copyright (C) 14-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatChat', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegChat 
{
	protected $idchat;
	protected $usuario;
	
	protected $dataChat;
	protected $oDatChat;	

	public function __construct()
	{
		$this->oDatChat = new DatChat;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatChat->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatChat->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatChat->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatChat->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatChat->get($this->idchat);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('chat', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatChat->iniciarTransaccion('neg_i_Chat');
			$this->idchat = $this->oDatChat->insertar($this->usuario);
			//$this->oDatChat->terminarTransaccion('neg_i_Chat');	
			return $this->idchat;
		} catch(Exception $e) {	
		   //$this->oDatChat->cancelarTransaccion('neg_i_Chat');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('chat', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatChat->actualizar($this->idchat,$this->usuario);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Chat', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatChat->eliminar($this->idchat);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdchat($pk){
		try {
			$this->dataChat = $this->oDatChat->get($pk);
			if(empty($this->dataChat)) {
				throw new Exception(JrTexto::_("Chat").' '.JrTexto::_("not registered"));
			}
			$this->idchat = $this->dataChat["idchat"];
			$this->usuario = $this->dataChat["usuario"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('chat', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataChat = $this->oDatChat->get($pk);
			if(empty($this->dataChat)) {
				throw new Exception(JrTexto::_("Chat").' '.JrTexto::_("not registered"));
			}

			return $this->oDatChat->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setUsuario($usuario)
	{
		try {
			$this->usuario= NegTools::validar('todo', $usuario, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}