<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		14-10-2019
 * @copyright	Copyright (C) 14-10-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_datos::DatAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_grupoaula
{
	protected $idgrupoaula;
	protected $nombre;
	protected $tipo;
	protected $comentario;
	protected $nvacantes;
	protected $estado;
	protected $idproyecto;
	protected $oldid;
	protected $dataAcad_grupoaula;
	protected $oDatAcad_grupoauladetalle;
	protected $oDatAcad_grupoaula;
	protected $idgrado = 0;
	protected $idsesion = 0;
	protected $idcategoria = 0;
	protected $idlocal = 0;
	protected $idproducto = 0;
	protected $tipodecurso = 0;
	protected $fecha_inicio;
	protected $tipogrupo = 1; //1 = normal, 2 = sence
	protected $codaccionsence = null;
	protected $idcarrera = null;
	public function __construct()
	{
		$this->oDatAcad_grupoaula = new DatAcad_grupoaula;
		$this->oDatAcad_grupoauladetalle = new DatAcad_grupoauladetalle;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatAcad_grupoaula->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return count($this->oDatAcad_grupoaula->buscar($filtros));
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_grupoaula->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getGrupoAulaRol($filtros = array())
	{
		try {
			return $this->oDatAcad_grupoaula->getGrupoAulaRol($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getAlumnos($filtros = array())
	{
		try {
			return $this->oDatAcad_grupoaula->getAlumnos($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	/**
	 * Funcion que retorna todos los grupos y cursos asociados  de un proyecto y empresa determinado
	 * @param Array $_REQUEST por GET o POST
	 * @return Object JSON
	 */
	public function gruposestudios($filtros = null){
		try{
			$resultado = [];
			$rs_grupos = $this->oDatAcad_grupoaula->buscar($filtros);
			if(!empty($rs_grupos)){
				foreach($rs_grupos as $grupo ){
					$grupo['grupodetalle'] = $this->oDatAcad_grupoauladetalle->buscargruposestudios(['idgrupoaula'=>$grupo['idgrupoaula']]);
					$resultado[] = $grupo;
				}
			}
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public function buscargrupos($filtros = array())
	{
		try {
			return $this->oDatAcad_grupoaula->buscargrupos($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function gruposxmatricula($filtros = array())
	{
		try {
			return $this->oDatAcad_grupoaula->gruposxmatricula($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_grupoaula->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function vacantes($idgrupoaula)
	{
		try {
			return $this->oDatAcad_grupoaula->vacantes($idgrupoaula);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscargruposxproyecto($filtros = array())
	{
		try {
			$dt = $this->oDatAcad_grupoaula->buscar($filtros);
			$grupos = array();
			if (!empty($dt))
				foreach ($dt as $k => $v) {
					$grupos[$k] = $v;
					$filtros["idgrupoaula"] = $v["idgrupoaula"];
					$gad = $this->oDatAcad_grupoauladetalle->buscar($filtros);
					$grupos[$k]['detalle'] = $gad;
				}
			return $grupos;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_grupoaula->get($this->idgrupoaula);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar($complementario = false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_grupoaula', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_grupoaula->iniciarTransaccion('neg_i_Acad_grupoaula');
			$this->idgrupoaula = $this->oDatAcad_grupoaula->insertar($this->nombre, $this->tipo, $this->comentario, $this->nvacantes, $this->estado, $this->idproyecto, $this->idgrado, $this->idsesion, $this->idcategoria, $this->idlocal, $this->fecha_inicio, $complementario, $this->idproducto, $this->tipodecurso,$this->tipogrupo,$this->codaccionsence,$this->idcarrera);
			$this->oDatAcad_grupoaula->terminarTransaccion('neg_i_Acad_grupoaula');
			return $this->idgrupoaula;
		} catch (Exception $e) {
			$this->oDatAcad_grupoaula->cancelarTransaccion('neg_i_Acad_grupoaula');
			throw new Exception($e->getMessage());
		}
	}

	public function editar($complementario)
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_grupoaula', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_grupoaula->actualizar($this->idgrupoaula, $this->nombre, $this->tipo, $this->comentario, $this->nvacantes, $this->estado, $this->idproyecto, $this->idgrado, $this->idsesion, $this->idcategoria, $this->idlocal, $this->fecha_inicio, $complementario,$this->codaccionsence,$this->idcarrera);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function cambiarvalorcampo($campo, $valor)
	{
		try {
			return $this->oDatAcad_grupoaula->cambiarvalorcampo($this->idgrupoaula, $campo, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_grupoaula', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$det = $this->oDatAcad_grupoauladetalle->buscar(array('idgrupoaula' => $this->idgrupoaula));
			if (!empty($det))
				foreach ($det as $rw) {
					$this->oDatAcad_grupoauladetalle->eliminar($rw["idgrupoauladetalle"]);
				}
			return $this->oDatAcad_grupoaula->eliminar($this->idgrupoaula);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminar_($idgrupoaula)
	{
		try {
			return $this->oDatAcad_grupoaula->eliminar_($idgrupoaula);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdgrupoaula($pk)
	{
		try {
			$this->dataAcad_grupoaula = $this->oDatAcad_grupoaula->get($pk);
			if (empty($this->dataAcad_grupoaula)) {
				throw new Exception(JrTexto::_("Acad_grupoaula") . ' ' . JrTexto::_("not registered"));
			}
			$this->idgrupoaula = $this->dataAcad_grupoaula["idgrupoaula"];
			$this->nombre = $this->dataAcad_grupoaula["nombre"];
			$this->tipo = $this->dataAcad_grupoaula["tipo"];
			$this->comentario = $this->dataAcad_grupoaula["comentario"];
			$this->nvacantes = $this->dataAcad_grupoaula["nvacantes"];
			$this->estado = $this->dataAcad_grupoaula["estado"];
			$this->idproyecto = $this->dataAcad_grupoaula["idproyecto"];
			$this->oldid = $this->dataAcad_grupoaula["oldid"];
			$this->idgrado = $this->dataAcad_grupoaula["idgrado"];
			$this->idsesion = $this->dataAcad_grupoaula["idsesion"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_grupoaula', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_grupoaula = $this->oDatAcad_grupoaula->get($pk);
			if (empty($this->dataAcad_grupoaula)) {
				throw new Exception(JrTexto::_("Acad_grupoaula") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatAcad_grupoaula->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
