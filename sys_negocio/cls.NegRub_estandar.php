<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRub_estandar', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRub_estandar 
{
	protected $id_estandar;
	protected $nombre;
	protected $id_rubrica;
	protected $puntuacion;
	protected $id_dimension;
	protected $activo;
	protected $tipo;
	protected $duplicado_id_estandar;
	
	protected $dataRub_estandar;
	protected $oDatRub_estandar;	

	public function __construct()
	{
		$this->oDatRub_estandar = new DatRub_estandar;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRub_estandar->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRub_estandar->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRub_estandar->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRub_estandar->get($this->id_estandar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_estandar', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRub_estandar->iniciarTransaccion('neg_i_Rub_estandar');
			$this->id_estandar = $this->oDatRub_estandar->insertar($this->nombre,$this->id_rubrica,$this->puntuacion,$this->id_dimension,$this->activo,$this->tipo,$this->duplicado_id_estandar);
			$this->oDatRub_estandar->terminarTransaccion('neg_i_Rub_estandar');	
			return $this->id_estandar;
		} catch(Exception $e) {	
		    $this->oDatRub_estandar->cancelarTransaccion('neg_i_Rub_estandar');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_estandar', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRub_estandar->actualizar($this->id_estandar,$this->nombre,$this->id_rubrica,$this->puntuacion,$this->id_dimension,$this->activo,$this->tipo,$this->duplicado_id_estandar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rub_estandar', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRub_estandar->eliminar($this->id_estandar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_estandar($pk){
		try {
			$this->dataRub_estandar = $this->oDatRub_estandar->get($pk);
			if(empty($this->dataRub_estandar)) {
				throw new Exception(JrTexto::_("Rub_estandar").' '.JrTexto::_("not registered"));
			}
			$this->id_estandar = $this->dataRub_estandar["id_estandar"];
			$this->nombre = $this->dataRub_estandar["nombre"];
			$this->id_rubrica = $this->dataRub_estandar["id_rubrica"];
			$this->puntuacion = $this->dataRub_estandar["puntuacion"];
			$this->id_dimension = $this->dataRub_estandar["id_dimension"];
			$this->activo = $this->dataRub_estandar["activo"];
			$this->tipo = $this->dataRub_estandar["tipo"];
			$this->duplicado_id_estandar = $this->dataRub_estandar["duplicado_id_estandar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rub_estandar', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRub_estandar = $this->oDatRub_estandar->get($pk);
			if(empty($this->dataRub_estandar)) {
				throw new Exception(JrTexto::_("Rub_estandar").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRub_estandar->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}