<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-09-2019
 * @copyright	Copyright (C) 12-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatZoom', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegZoom 
{
	protected $idzoom;
	protected $idpersona;
	protected $fecha;
	protected $urlmoderador;
	protected $urlparticipantes;
	protected $titulo;
	protected $agenda;
	protected $estado;
	protected $uuid;
	protected $duracion;
	protected $email = '';
	protected $imagen = '';
	protected $idcursodetalle = 0;
	protected $idpestania = 0;
	protected $idcurso = 0;
	protected $idcomplementario = 0;
	
	protected $dataZoom;
	protected $oDatZoom;	

	public function __construct()
	{
		$this->oDatZoom = new DatZoom;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatZoom->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatZoom->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatZoom->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatZoom->get($this->idzoom);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('zoom', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatZoom->iniciarTransaccion('neg_i_Zoom');
			$this->idzoom = $this->oDatZoom->insertar($this->idpersona,$this->fecha,$this->urlmoderador,$this->urlparticipantes,$this->titulo,$this->agenda,$this->estado,$this->uuid,$this->duracion,$this->imagen, $this->idcursodetalle, $this->idpestania, $this->idcurso, $this->idcomplementario,$this->email);
			$this->oDatZoom->terminarTransaccion('neg_i_Zoom');	
			return $this->idzoom;
		} catch(Exception $e) {	
		    $this->oDatZoom->cancelarTransaccion('neg_i_Zoom');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('zoom', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatZoom->actualizar($this->idzoom,$this->idpersona,$this->fecha,$this->urlmoderador,$this->urlparticipantes,$this->titulo,$this->agenda,$this->estado,$this->uuid,$this->duracion,$this->imagen, $this->idcursodetalle, $this->idpestania, $this->idcurso, $this->idcomplementario,$this->email);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Zoom', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatZoom->eliminar($this->idzoom);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdzoom($pk){
		try {
			$this->dataZoom = $this->oDatZoom->get($pk);
			if(empty($this->dataZoom)) {
				throw new Exception(JrTexto::_("Zoom").' '.JrTexto::_("not registered"));
			}
			$this->idzoom = $this->dataZoom["idzoom"];
			$this->idpersona = $this->dataZoom["idpersona"];
			$this->fecha = $this->dataZoom["fecha"];
			$this->urlmoderador = $this->dataZoom["urlmoderador"];
			$this->urlparticipantes = $this->dataZoom["urlparticipantes"];
			$this->titulo = $this->dataZoom["titulo"];
			$this->agenda = $this->dataZoom["agenda"];
			$this->estado = $this->dataZoom["estado"];
			$this->uuid = $this->dataZoom["uuid"];
			$this->duracion = $this->dataZoom["duracion"];
			$this->email = $this->dataZoom["email"];
			$this->imagen = $this->dataZoom['imagen'];
			$this->idcursodetalle = $this->dataZoom['idcursodetalle'];
			$this->idpestania = $this->dataZoom['idpestania'];
			$this->idcurso = $this->dataZoom['idcurso'];
			$this->idcomplementario = $this->dataZoom['idcomplementario'];
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('zoom', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataZoom = $this->oDatZoom->get($pk);
			if(empty($this->dataZoom)) {
				throw new Exception(JrTexto::_("Zoom").' '.JrTexto::_("not registered"));
			}
			return $this->oDatZoom->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	//////////////FUNCIONES PUBLICAS/////////////////
	public static function requestZoom($token,$dt=array()){
		$curl = curl_init();
		//$pgsize=!empty($dt["pgsize"])?$dt["pgsize"]:30;
		//$pagenun=!empty($dt["pagenun"])?$dt["pagenun"]:1;
		$metodo=!empty($dt["metodo"])?$dt["metodo"]:'GET';
		$post=!empty($dt["datos"])?json_encode($dt["datos"]):array();
		$url=!empty($dt["url"])?$dt["url"]:'users?status=active';
		$url2="https://api.zoom.us/v2/".$url;//."&page_size=".$pgsize."&page_number=".$pagenun;
		curl_setopt($curl, CURLOPT_URL, $url2);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_ENCODING, "");
		curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
		curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, !empty($metodo)?$metodo:'GET');
		if(!empty($post))curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("authorization: Bearer ".$token,"content-type: application/json")); 
		$response = curl_exec($curl);
		$status   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$err = curl_error($curl);
		curl_close($curl);
		if (!empty($err)){
			$err=array('status' => $status,'code'=>'error','msj'=>$err, 'response' => $response,'error'=>$err);
			echo json_encode($err);
			exit(0);		   
		}else{
			return json_decode($response,true);		    
		}
	}
}