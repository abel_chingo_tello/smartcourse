<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		23-12-2016
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatHerramientas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegGame 
{
	protected $idtool;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $idpersonal;
	protected $texto;
	protected $orden;
	protected $tool;
	
	protected $dataHerramientas;
	protected $oDatHerramientas;	

	public function __construct()
	{
		$this->oDatHerramientas = new DatHerramientas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatHerramientas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			if(empty($filtros))
			$filtros["tool"]='G';
			return $this->oDatHerramientas->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			if(empty($filtros))
			$filtros["tool"]='G';
			return $this->oDatHerramientas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatHerramientas->get($this->idtool);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {			
			//$this->oDatVocabulario->iniciarTransaccion('neg_i_Vocabulario');			
			   $res=$this->buscar(array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'G'));
			//$this->oDatLinks->iniciarTransaccion('neg_i_Links');
			if(empty($res[0]))   
				$this->idtool = $this->oDatHerramientas->insertar($this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->texto,$this->orden,'G');
			else{
				$this->idtool=$res[0]["idtool"];
				$this->idtool = $this->oDatHerramientas->actualizar($this->idtool,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->texto,$this->orden,'G');
			}//$this->oDatVocabulario->terminarTransaccion('neg_i_Vocabulario');	
			return $this->idtool;
		} catch(Exception $e) {	
		   //$this->oDatVocabulario->cancelarTransaccion('neg_i_Vocabulario');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('vocabulario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatHerramientas->actualizar($this->idtool,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->texto,$this->orden,'G');
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Vocabulario', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatHerramientas->eliminar($this->idtool);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtool($pk){
		try {
			$this->dataHerramientas = $this->oDatHerramientas->get($pk);
			if(empty($this->dataHerramientas)) {
				throw new Exception(JrTexto::_("Vocabulario").' '.JrTexto::_("not registered"));
			}
			$this->idtool = $this->dataHerramientas["idtool"];
			$this->idnivel = $this->dataHerramientas["idnivel"];
			$this->idunidad = $this->dataHerramientas["idunidad"];
			$this->idactividad = $this->dataHerramientas["idactividad"];
			$this->idpersonal = $this->dataHerramientas["idpersonal"];
			$this->texto = $this->dataHerramientas["texto"];
			$this->orden = $this->dataHerramientas["orden"];
			$this->tool = $this->dataHerramientas["tool"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('vocabulario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataHerramientas = $this->oDatHerramientas->get($pk);
			if(empty($this->dataHerramientas)) {
				throw new Exception(JrTexto::_("Vocabulario").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHerramientas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}	
}