<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::sistema::DatSis_configuracion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegConfiguracion 
{
	protected $config;
	protected $valor;
	protected $autocargar;
	
	protected $dataSis_configuracion;
	protected $oDatSis_configuracion;	

	public function __construct()
	{
		$this->oDatSis_configuracion = new DatSis_configuracion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatSis_configuracion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatSis_configuracion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatSis_configuracion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatSis_configuracion->get($this->config);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('sis_configuracion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatSis_configuracion->iniciarTransaccion('neg_i_Sis_configuracion');
			$this->config = $this->oDatSis_configuracion->insertar($this->valor,$this->autocargar);
			$this->oDatSis_configuracion->terminarTransaccion('neg_i_Sis_configuracion');	
			return $this->config;
		} catch(Exception $e) {	
		    $this->oDatSis_configuracion->cancelarTransaccion('neg_i_Sis_configuracion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('sis_configuracion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatSis_configuracion->actualizar($this->config,$this->valor,$this->autocargar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Sis_configuracion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatSis_configuracion->eliminar($this->config);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setConfig($pk){
		try {
			$this->dataSis_configuracion = $this->oDatSis_configuracion->get($pk);
			if(empty($this->dataSis_configuracion)) {
				throw new Exception(JrTexto::_("Sis_configuracion").' '.JrTexto::_("not registered"));
			}
			$this->config = $this->dataSis_configuracion["config"];
			$this->valor = $this->dataSis_configuracion["valor"];
			$this->autocargar = $this->dataSis_configuracion["autocargar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('sis_configuracion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataSis_configuracion = $this->oDatSis_configuracion->get($pk);
			if(empty($this->dataSis_configuracion)) {
				throw new Exception(JrTexto::_("Sis_configuracion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatSis_configuracion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}