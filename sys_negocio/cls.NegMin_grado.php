<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		16-10-2018
 * @copyright	Copyright (C) 16-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMin_grado', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegMin_grado 
{
	protected $idgrado;
	protected $descripcion;
	protected $abrev;
	
	protected $dataMin_grado;
	protected $oDatMin_grado;	

	public function __construct()
	{
		$this->oDatMin_grado = new DatMin_grado;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMin_grado->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatMin_grado->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMin_grado->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMin_grado->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMin_grado->get($this->idgrado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_grado', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatMin_grado->iniciarTransaccion('neg_i_Min_grado');
			$this->idgrado = $this->oDatMin_grado->insertar($this->descripcion,$this->abrev);
			$this->oDatMin_grado->terminarTransaccion('neg_i_Min_grado');	
			return $this->idgrado;
		} catch(Exception $e) {	
		    $this->oDatMin_grado->cancelarTransaccion('neg_i_Min_grado');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_grado', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatMin_grado->actualizar($this->idgrado,$this->descripcion,$this->abrev);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Min_grado', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatMin_grado->eliminar($this->idgrado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdgrado($pk){
		try {
			$this->dataMin_grado = $this->oDatMin_grado->get($pk);
			if(empty($this->dataMin_grado)) {
				throw new Exception(JrTexto::_("Min_grado").' '.JrTexto::_("not registered"));
			}
			$this->idgrado = $this->dataMin_grado["idgrado"];
			$this->descripcion = $this->dataMin_grado["descripcion"];
			$this->abrev = $this->dataMin_grado["abrev"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('min_grado', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataMin_grado = $this->oDatMin_grado->get($pk);
			if(empty($this->dataMin_grado)) {
				throw new Exception(JrTexto::_("Min_grado").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMin_grado->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}