<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		09-11-2017
 * @copyright	Copyright (C) 09-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_cursoaprendizaje', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_cursoaprendizaje 
{
	protected $idaprendizaje;
	protected $idcurso;
	protected $idcursodetalle;
	protected $idpadre;
	protected $idrecurso;
	protected $descripcion;
	
	protected $dataAcad_cursoaprendizaje;
	protected $oDatAcad_cursoaprendizaje;	

	public function __construct()
	{
		$this->oDatAcad_cursoaprendizaje = new DatAcad_cursoaprendizaje;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_cursoaprendizaje->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_cursoaprendizaje->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_cursoaprendizaje->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_cursoaprendizaje->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_cursoaprendizaje->get($this->idaprendizaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {			
			$this->oDatAcad_cursoaprendizaje->iniciarTransaccion('neg_i_Acad_cursoaprendizaje');
			$this->idaprendizaje = $this->oDatAcad_cursoaprendizaje->insertar($this->idcurso,$this->idcursodetalle,$this->idpadre,$this->idrecurso,$this->descripcion);
			$this->oDatAcad_cursoaprendizaje->terminarTransaccion('neg_i_Acad_cursoaprendizaje');	
			return $this->idaprendizaje;
		} catch(Exception $e) {	
		    $this->oDatAcad_cursoaprendizaje->cancelarTransaccion('neg_i_Acad_cursoaprendizaje');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
						
			return $this->oDatAcad_cursoaprendizaje->actualizar($this->idaprendizaje,$this->idcurso,$this->idcursodetalle,$this->idpadre,$this->idrecurso,$this->descripcion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {			
			return $this->oDatAcad_cursoaprendizaje->eliminar($this->idaprendizaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdaprendizaje($pk){
		try {
			$this->dataAcad_cursoaprendizaje = $this->oDatAcad_cursoaprendizaje->get($pk);
			if(empty($this->dataAcad_cursoaprendizaje)) {
				throw new Exception(JrTexto::_("Acad_cursoaprendizaje").' '.JrTexto::_("not registered"));
			}
			$this->idaprendizaje = $this->dataAcad_cursoaprendizaje["idaprendizaje"];
			$this->idcurso = $this->dataAcad_cursoaprendizaje["idcurso"];
			$this->idcursodetalle = $this->dataAcad_cursoaprendizaje["idcursodetalle"];
			$this->idpadre = $this->dataAcad_cursoaprendizaje["idpadre"];
			$this->idrecurso = $this->dataAcad_cursoaprendizaje["idrecurso"];
			$this->descripcion = $this->dataAcad_cursoaprendizaje["descripcion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('acad_cursoaprendizaje', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataAcad_cursoaprendizaje = $this->oDatAcad_cursoaprendizaje->get($pk);
			if(empty($this->dataAcad_cursoaprendizaje)) {
				throw new Exception(JrTexto::_("Acad_cursoaprendizaje").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_cursoaprendizaje->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}