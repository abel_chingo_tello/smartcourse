<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRub_respuestas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRub_respuestas 
{
	protected $id_respuestas;
	protected $id_rubrica;
	protected $nombre;
	protected $institucion;
	protected $idlocal;
	protected $iddocente;
	protected $fecha_registro;
	protected $activo;
	
	protected $dataRub_respuestas;
	protected $oDatRub_respuestas;	

	public function __construct()
	{
		$this->oDatRub_respuestas = new DatRub_respuestas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRub_respuestas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRub_respuestas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRub_respuestas->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRub_respuestas->get($this->id_respuestas);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_respuestas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRub_respuestas->iniciarTransaccion('neg_i_Rub_respuestas');
			$this->id_respuestas = $this->oDatRub_respuestas->insertar($this->id_rubrica,$this->nombre,$this->institucion,$this->idlocal,$this->iddocente,$this->fecha_registro,$this->activo);
			$this->oDatRub_respuestas->terminarTransaccion('neg_i_Rub_respuestas');	
			return $this->id_respuestas;
		} catch(Exception $e) {	
		    $this->oDatRub_respuestas->cancelarTransaccion('neg_i_Rub_respuestas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_respuestas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRub_respuestas->actualizar($this->id_respuestas,$this->id_rubrica,$this->nombre,$this->institucion,$this->idlocal,$this->iddocente,$this->fecha_registro,$this->activo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rub_respuestas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRub_respuestas->eliminar($this->id_respuestas);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_respuestas($pk){
		try {
			$this->dataRub_respuestas = $this->oDatRub_respuestas->get($pk);
			if(empty($this->dataRub_respuestas)) {
				throw new Exception(JrTexto::_("Rub_respuestas").' '.JrTexto::_("not registered"));
			}
			$this->id_respuestas = $this->dataRub_respuestas["id_respuestas"];
			$this->id_rubrica = $this->dataRub_respuestas["id_rubrica"];
			$this->nombre = $this->dataRub_respuestas["nombre"];
			$this->institucion = $this->dataRub_respuestas["institucion"];
			$this->idlocal = $this->dataRub_respuestas["idlocal"];
			$this->iddocente = $this->dataRub_respuestas["iddocente"];
			$this->fecha_registro = $this->dataRub_respuestas["fecha_registro"];
			$this->activo = $this->dataRub_respuestas["activo"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rub_respuestas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRub_respuestas = $this->oDatRub_respuestas->get($pk);
			if(empty($this->dataRub_respuestas)) {
				throw new Exception(JrTexto::_("Rub_respuestas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRub_respuestas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}