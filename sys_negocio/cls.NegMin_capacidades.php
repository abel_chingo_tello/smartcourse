<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		16-10-2018
 * @copyright	Copyright (C) 16-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMin_capacidades', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegMin_capacidades 
{
	protected $idcapacidad;
	protected $idcompetencia;
	protected $titulo;
	protected $descripcion;
	protected $habilidades;
	
	protected $dataMin_capacidades;
	protected $oDatMin_capacidades;	

	public function __construct()
	{
		$this->oDatMin_capacidades = new DatMin_capacidades;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMin_capacidades->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatMin_capacidades->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMin_capacidades->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMin_capacidades->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMin_capacidades->get($this->idcapacidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_capacidades', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatMin_capacidades->iniciarTransaccion('neg_i_Min_capacidades');
			$this->idcapacidad = $this->oDatMin_capacidades->insertar($this->idcompetencia,$this->titulo,$this->descripcion,$this->habilidades);
			$this->oDatMin_capacidades->terminarTransaccion('neg_i_Min_capacidades');	
			return $this->idcapacidad;
		} catch(Exception $e) {	
		    $this->oDatMin_capacidades->cancelarTransaccion('neg_i_Min_capacidades');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_capacidades', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatMin_capacidades->actualizar($this->idcapacidad,$this->idcompetencia,$this->titulo,$this->descripcion,$this->habilidades);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Min_capacidades', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatMin_capacidades->eliminar($this->idcapacidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcapacidad($pk){
		try {
			$this->dataMin_capacidades = $this->oDatMin_capacidades->get($pk);
			if(empty($this->dataMin_capacidades)) {
				throw new Exception(JrTexto::_("Min_capacidades").' '.JrTexto::_("not registered"));
			}
			$this->idcapacidad = $this->dataMin_capacidades["idcapacidad"];
			$this->idcompetencia = $this->dataMin_capacidades["idcompetencia"];
			$this->titulo = $this->dataMin_capacidades["titulo"];
			$this->descripcion = $this->dataMin_capacidades["descripcion"];
			$this->habilidades = $this->dataMin_capacidades["habilidades"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('min_capacidades', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataMin_capacidades = $this->oDatMin_capacidades->get($pk);
			if(empty($this->dataMin_capacidades)) {
				throw new Exception(JrTexto::_("Min_capacidades").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMin_capacidades->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}