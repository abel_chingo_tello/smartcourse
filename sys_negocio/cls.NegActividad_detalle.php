<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-11-2016
 * @copyright	Copyright (C) 19-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatActividad_detalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegActividad_detalle
{
	protected $iddetalle;
	protected $idactividad;
	protected $pregunta;
	protected $orden;
	protected $url;
	protected $tipo;
	protected $tipo_desarrollo;
	protected $tipo_actividad;
	protected $idhabilidad;
	protected $dataActividad_detalle;
	protected $oDatActividad_detalle;

	public function __construct()
	{
		$this->oDatActividad_detalle = new DatActividad_detalle;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			$this->oDatActividad_detalle->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatActividad_detalle->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatActividad_detalle->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatActividad_detalle->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatActividad_detalle->get($this->iddetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->iddetalle = $this->oDatActividad_detalle->insertar($this->idactividad,$this->pregunta,$this->orden,$this->url,$this->tipo,$this->tipo_desarrollo,$this->tipo_actividad,$this->idhabilidad);
			return $this->iddetalle;
		} catch(Exception $e) {

			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			return $this->oDatActividad_detalle->actualizar($this->iddetalle,$this->idactividad,$this->pregunta,$this->orden,$this->url,$this->tipo,$this->tipo_desarrollo,$this->tipo_actividad,$this->idhabilidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			return $this->oDatActividad_detalle->eliminar($this->iddetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIddetalle($pk){
		try {
			$this->dataActividad_detalle = $this->oDatActividad_detalle->get($pk);
			if(empty($this->dataActividad_detalle)) {
				throw new Exception(JrTexto::_("Actividad_detalle").' '.JrTexto::_("not registered"));
			}
			$this->iddetalle = $this->dataActividad_detalle["iddetalle"];
			$this->idactividad = $this->dataActividad_detalle["idactividad"];
			$this->pregunta = $this->dataActividad_detalle["pregunta"];
			//$this->tipopregunta = $this->dataActividad_detalle["tipopregunta"];
			$this->orden = $this->dataActividad_detalle["orden"];
			$this->url = $this->dataActividad_detalle["url"];
			//$this->estado = $this->dataActividad_detalle["estado"];
			$this->tipo = $this->dataActividad_detalle["tipo"];
			$this->tipo_desarrollo = $this->dataActividad_detalle["tipo_desarrollo"];
			$this->tipo_actividad = $this->dataActividad_detalle["tipo_actividad"];
			$this->idhabilidad = $this->dataActividad_detalle["idhabilidad"];
			//$this->regusuario = $this->dataActividad_detalle["regusuario"];
						//falta campos
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('actividad_detalle', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataActividad_detalle = $this->oDatActividad_detalle->get($pk);
			if(empty($this->dataActividad_detalle)) {
				throw new Exception(JrTexto::_("Actividad_detalle").' '.JrTexto::_("not registered"));
			}

			return $this->oDatActividad_detalle->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	public function  actualizartabla($tabla,$campo,$iddetalle){
		if(!empty($tabla)&&!empty($campo)){
			$this->setLimite(0, 5000);
		 return $this->oDatActividad_detalle->actualizartabla($tabla,$campo,$iddetalle);		
		}
	}
	public function  actualizartabla1($tabla,$campo,$iddetalle){
		if(!empty($tabla)&&!empty($campo)){
			$this->setLimite(0, 5000);
		 return $this->oDatActividad_detalle->actualizartabla1($tabla,$campo,$iddetalle);		
		}
	}
}
