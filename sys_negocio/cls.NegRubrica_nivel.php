<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-04-2020
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRubrica_nivel', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRubrica_nivel 
{
	protected $idrubrica_nivel;
	protected $cualitativo;
	protected $cuantitativo;
	protected $idrubrica;
	
	protected $dataRubrica_nivel;
	protected $oDatRubrica_nivel;	

	public function __construct()
	{
		$this->oDatRubrica_nivel = new DatRubrica_nivel;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRubrica_nivel->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRubrica_nivel->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRubrica_nivel->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRubrica_nivel->get($this->idrubrica_nivel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_nivel', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRubrica_nivel->iniciarTransaccion('neg_i_Rubrica_nivel');
			$this->idrubrica_nivel = $this->oDatRubrica_nivel->insertar($this->cualitativo,$this->cuantitativo,$this->idrubrica);
			$this->oDatRubrica_nivel->terminarTransaccion('neg_i_Rubrica_nivel');	
			return $this->idrubrica_nivel;
		} catch(Exception $e) {	
		    $this->oDatRubrica_nivel->cancelarTransaccion('neg_i_Rubrica_nivel');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_nivel', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRubrica_nivel->actualizar($this->idrubrica_nivel,$this->cualitativo,$this->cuantitativo,$this->idrubrica);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rubrica_nivel', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica_nivel->eliminar($this->idrubrica_nivel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrubrica_nivel($pk){
		try {
			$this->dataRubrica_nivel = $this->oDatRubrica_nivel->get($pk);
			if(empty($this->dataRubrica_nivel)) {
				throw new Exception(JrTexto::_("Rubrica_nivel").' '.JrTexto::_("not registered"));
			}
			$this->idrubrica_nivel = $this->dataRubrica_nivel["idrubrica_nivel"];
			$this->cualitativo = $this->dataRubrica_nivel["cualitativo"];
			$this->cuantitativo = $this->dataRubrica_nivel["cuantitativo"];
			$this->idrubrica = $this->dataRubrica_nivel["idrubrica"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_nivel', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRubrica_nivel = $this->oDatRubrica_nivel->get($pk);
			if(empty($this->dataRubrica_nivel)) {
				throw new Exception(JrTexto::_("Rubrica_nivel").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRubrica_nivel->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}