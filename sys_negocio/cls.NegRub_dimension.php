<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRub_dimension', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRub_dimension 
{
	protected $id_dimension;
	protected $nombre;
	protected $tipo_pregunta;
	protected $escalas_evaluacion;
	protected $tipo;
	protected $otros_datos;
	protected $id_rubrica;
	protected $activo;
	
	protected $dataRub_dimension;
	protected $oDatRub_dimension;	

	public function __construct()
	{
		$this->oDatRub_dimension = new DatRub_dimension;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRub_dimension->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRub_dimension->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRub_dimension->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRub_dimension->get($this->id_dimension);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_dimension', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRub_dimension->iniciarTransaccion('neg_i_Rub_dimension');
			$this->id_dimension = $this->oDatRub_dimension->insertar($this->nombre,$this->tipo_pregunta,$this->escalas_evaluacion,$this->tipo,$this->otros_datos,$this->id_rubrica,$this->activo);
			$this->oDatRub_dimension->terminarTransaccion('neg_i_Rub_dimension');	
			return $this->id_dimension;
		} catch(Exception $e) {	
		    $this->oDatRub_dimension->cancelarTransaccion('neg_i_Rub_dimension');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_dimension', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRub_dimension->actualizar($this->id_dimension,$this->nombre,$this->tipo_pregunta,$this->escalas_evaluacion,$this->tipo,$this->otros_datos,$this->id_rubrica,$this->activo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rub_dimension', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRub_dimension->eliminar($this->id_dimension);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_dimension($pk){
		try {
			$this->dataRub_dimension = $this->oDatRub_dimension->get($pk);
			if(empty($this->dataRub_dimension)) {
				throw new Exception(JrTexto::_("Rub_dimension").' '.JrTexto::_("not registered"));
			}
			$this->id_dimension = $this->dataRub_dimension["id_dimension"];
			$this->nombre = $this->dataRub_dimension["nombre"];
			$this->tipo_pregunta = $this->dataRub_dimension["tipo_pregunta"];
			$this->escalas_evaluacion = $this->dataRub_dimension["escalas_evaluacion"];
			$this->tipo = $this->dataRub_dimension["tipo"];
			$this->otros_datos = $this->dataRub_dimension["otros_datos"];
			$this->id_rubrica = $this->dataRub_dimension["id_rubrica"];
			$this->activo = $this->dataRub_dimension["activo"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rub_dimension', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRub_dimension = $this->oDatRub_dimension->get($pk);
			if(empty($this->dataRub_dimension)) {
				throw new Exception(JrTexto::_("Rub_dimension").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRub_dimension->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}