<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-11-2016
 * @copyright	Copyright (C) 19-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatCorregir', RUTA_BASE);
class NegCorregir 
{

	protected $oDatCorregir;	

	public function __construct()
	{
		$this->oDatCorregir = new DatCorregir;
	}

	public function historial_sesion($desde=0){
		$this->oDatCorregir->historial_sesion($desde);		
	}

	public function valoracion($desde=0){
		$this->oDatCorregir->valoracion($desde);		
	}

	public function notas_quiz($desde=0){
		$this->oDatCorregir->notas_quiz($desde);		
	}
	public function tareas($desde=0){
		$this->oDatCorregir->tareas($desde);		
	}

	public function actividad_alumno($desde=0){
		$this->oDatCorregir->actividad_alumno($desde);		
	}

	public function bitacora_alumno_smartbook($desde=0){
		$this->oDatCorregir->bitacora_alumno_smartbook($desde);		
	}

	public function bitacora_alumno_smartbook_se($desde=0){
		$this->oDatCorregir->bitacora_alumno_smartbook_se($desde);		
	}

	public function transferencias($desde=0){
		$this->oDatCorregir->transferencias($desde);		
	}

	public function eliminarinfouser($idalumno){
		$this->oDatCorregir->eliminarinfouser($idalumno);		
	}
	
}