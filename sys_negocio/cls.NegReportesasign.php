<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatReportesasign', RUTA_BASE, 'sys_datos');

class NegReportesasign
{
    protected $oDatReportesasign;

    public function __construct()
	{
        $this->oDatReportesasign = new DatReportesasign;
    }
    /**
     * Funcion que procede a insertar un nuevor registro en la tabla reportesasign_empresa
     * @param Array parametros para busqueda de la base de datos
     * @return Array Resultado de las habilidades de los examenes
     */
    public function insertarempresa($params){
        try{
            if(empty($params)){ throw new Exception("Se requiere parametros para insertar"); }
            return $this->oDatReportesasign->insertarempresa($params);
        }catch(Exception $e){
            throw new Exception("Un error en insertarempresa ".$e->getMessage());
        }
    }
    /**
     * Funcion que procede a insertar un nuevor registro en la tabla reportesasign_proyecto
     * @param Array parametros para busqueda de la base de datos
     * @return Array Resultado de las habilidades de los examenes
     */
    public function insertarproyecto($params){
        try{
            if(empty($params)){ throw new Exception("Se requiere parametros para insertar"); }
            return $this->oDatReportesasign->insertarproyecto($params);
        }catch(Exception $e){
            throw new Exception("Un error en insertarproyecto ".$e->getMessage());
        }
    }
    /**
     * Busqueda general
     * @param Array parametros para realizar las busquedas
     * @return Array resultado
     */
    public function buscar($params = []){
        try{
            return $this->oDatReportesasign->buscar($params);
        }catch(Exception $e){
            throw new Exception("Un error en buscar ".$e->getMessage());
        }
    }
    /**
     * Buscar asignaciones tanto general como especificos
     * @param Array parametros para realizar las busquedas
     * @return Array resultado
     */
    public function asignaciones($params = []){
        try{
            if(empty($params['groupby'])){ $params['groupby'] = 'idreportegeneral'; }
            return $this->oDatReportesasign->buscar($params);
        }catch(Exception $e){
            throw new Exception("Un error en asignaciones ".$e->getMessage());
        }
    }
    public function delete($id){
        try{
            if(empty($id)){ throw new Exception("Id vacio"); }
            return $this->oDatReportesasign->delete($id);
        }catch(Exception $e){
            throw new Exception("Un error en delete ".$e->getMessage());
        }
    }
}