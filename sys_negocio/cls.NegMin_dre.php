<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-09-2019
 * @copyright	Copyright (C) 27-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMin_dre', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegMin_dre 
{
	protected $iddre;
	protected $descripcion;
	protected $ubigeo;
	protected $opcional;
	protected $idproyecto=0;
	
	protected $dataMin_dre;
	protected $oDatMin_dre;	

	public function __construct()
	{
		$this->oDatMin_dre = new DatMin_dre;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMin_dre->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMin_dre->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarconinsert($filtros){
		try {
			$result = $this->oDatMin_dre->buscarv2($filtros);
			if(empty($result)){
				if(!empty($filtros['descripcion']) && !empty($filtros['idproyecto'])){
					//crear el dre y obtener su id
					$this->descripcion = $filtros['descripcion'];
					$this->ubigeo = (isset($filtros['ubigeo']) && !empty($filtros['ubigeo'])) ? $filtros['ubigeo'] : '0';
					$this->opcional = (isset($filtros['opcional']) && !empty($filtros['opcional'])) ? $filtros['opcional'] : '0';
					$this->idproyecto= $filtros['idproyecto'];
					$id = $this->agregar();
					$result = $this->oDatMin_dre->buscarv2(array('iddre'=>$id));
				}
			}
			return $result;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMin_dre->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMin_dre->get($this->iddre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_dre', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatMin_dre->iniciarTransaccion('neg_i_Min_dre');
			$this->iddre = $this->oDatMin_dre->insertar($this->descripcion,$this->ubigeo,$this->opcional,$this->idproyecto);
			$this->oDatMin_dre->terminarTransaccion('neg_i_Min_dre');	
			return $this->iddre;
		} catch(Exception $e) {	
		    $this->oDatMin_dre->cancelarTransaccion('neg_i_Min_dre');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_dre', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatMin_dre->actualizar($this->iddre,$this->descripcion,$this->ubigeo,$this->opcional,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Min_dre', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatMin_dre->eliminar($this->iddre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function importar($id,$descripcion,$ubigeo,$opcional,$idproyecto=0)
	{
		if(!empty($id)){
			$haydre=$this->buscar(array('iddre'=>$id,'idproyecto'=>$idproyecto));			
			if(!empty($haydre[0])) return array('_estado'=>'E','_Id'=>$haydre[0]["iddre"]);			
		}
		$iddre=$this->oDatMin_dre->insertar($descripcion,$ubigeo,$opcional,$idproyecto);
		return array('_estado'=>'N','_Id'=>$iddre,'_idimport'=>@$id);
	}


	public function setIddre($pk){
		try {
			$this->dataMin_dre = $this->oDatMin_dre->get($pk);
			if(empty($this->dataMin_dre)) {
				throw new Exception(JrTexto::_("Min_dre").' '.JrTexto::_("not registered"));
			}
			$this->iddre = $this->dataMin_dre["iddre"];
			$this->descripcion = $this->dataMin_dre["descripcion"];
			$this->ubigeo = $this->dataMin_dre["ubigeo"];
			$this->opcional = $this->dataMin_dre["opcional"];
			$this->idproyecto = $this->dataMin_dre["idproyecto"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('min_dre', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataMin_dre = $this->oDatMin_dre->get($pk);
			if(empty($this->dataMin_dre)) {
				throw new Exception(JrTexto::_("Min_dre").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMin_dre->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}