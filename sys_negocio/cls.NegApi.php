<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		26-10-2018
 * @copyright	Copyright (C) 26-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_datos::DatApi', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatAcad_categorias', RUTA_BASE, 'sys_datos');

class NegApi 
{
	protected $oDatApi;
	protected $oDatAcad_categorias;

	public function __construct()
	{
		$this->oDatApi = new DatApi;
		$this->oDatAcad_categorias = new DatAcad_categorias;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}
	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatApi->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function _buscargrupos($filtros = array())
	{
		try {
			return $this->oDatApi->_buscargrupos($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function _buscargrupos2($filtros = array())
	{
		try {
			return $this->oDatApi->_buscargrupos2($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function _transferir($datos = array())
	{
		try {
			return $this->oDatApi->_transferir($datos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarmatriculados($filtros = array()){
		try{
			return $this->oDatApi->buscarmatriculados($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function detallecursoxgrupo($filtros = array()){
		try{
			$dt = $this->oDatApi->detallecursoxgrupo($filtros);
			if(!empty($dt)){
				$i = 0; $c = count($dt); $tmp_arr = array();
				do{
					if(!empty($dt[$i]['txtjson'])){
						$dt[$i]['txtjson'] = json_decode($dt[$i]['txtjson'],true);
					}
					$findex = array_search($dt[$i]['idcurso'], array_column($tmp_arr, 'idcurso'));
					if($findex === false){
						$tmp_arr[] = array('nombre'=>$dt[$i]['nombre'],'idcurso'=>$dt[$i]['idcurso'],'detalle'=>array($dt[$i]));
					}else{
						$tmp_arr[$findex]['detalle'][] = $dt[$i];
					}
					++$i;
				}while($i < $c);
				$dt = $tmp_arr;
			}
			return $dt;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function insertgrupoaula($arr){
		try{
			return $this->oDatApi->insertgrupoaula($arr);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function insertgrupoauladetalle($arr,$c = null){
		try{
			return $this->oDatApi->insertgrupoauladetalle($arr,$c);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function InsertMultiple($arr,$_key_,$is_prefijo,$prefijo){
		try{
			return $this->oDatApi->InsertMultiple($arr,$_key_,$is_prefijo,$prefijo);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function _buscarCategorias($filtros = array()){
		try{
			return $this->oDatApi->_buscarCategorias($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function _crearusuariov2($persona,$persona_rol){
		try{
			return $this->oDatApi->_crearusuariov2($persona,$persona_rol);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function editarusuariov1($persona){
		try{
			return $this->oDatApi->editarusuariov1($persona);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function agregarrolv1($persona_rol,$persona = null){
		try{
			return $this->oDatApi->agregarrolv1($persona_rol);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function eliminarrolv1($persona_rol){
		try{
			return $this->oDatApi->eliminarrolv1($persona_rol);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function _smtp($datos){
		try{
			return $this->oDatApi->_smtp($datos);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function _matricular($datos){
		try{
			return $this->oDatApi->_matricular($datos);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function formatearUsuario($personas,$params = []){
		try{
			if(empty($personas)){ throw new Exception("personas esta vacio"); }

			$usuarios = [];
			
			foreach($personas as $value){
				$phone = 0;
				if(isset($value['telefono']) && !empty($value['telefono'])){
					$phone = $value['telefono'];
				}else if(isset($value['celular']) && !empty($value['celular'])){
					$phone = $value['celular'];
				}
				//buscar existencia
				$rs_persona = $this->oDatApi->_buscarPersonal(["dni"=>$value['documento'],"tipodoc"=>$value["tipodoc"]]);
				if(!empty($rs_persona)){
					$prepare = end($rs_persona);
					//buscar roles
					$rs_roles = $this->oDatApi->_buscarRol(["idpersonal"=>$prepare["idpersona"],"idempresa"=>$params["idempresa"], "idproyecto"=>$params["idproyecto"]]);
					$prepare["roles"] = $rs_roles;
				}else{
					//setear
					$prepare = [
						"tipodoc" => $value['tipodoc']
						,'dni'      => $value['documento']
						,'ape_paterno' => $value['apellido1']
						,'ape_materno' => $value['apellido2']
						,'nombre' => $value['nombres']
						,'fechanac' => !empty($value['fechanac']) ? $value['fechanac'] : date("Y-m-d")
						,"regfecha" => date("Y-m-d")
						,'sexo' => $value['sexo']
						,'estado_civil' => 'S'
						,'ubigeo' => ' '
						,'urbanizacion' => ' '
						,'direccion' => ' '
						,'telefono' => $phone
						,'celular' => $phone
						,'email' => $value['email']
						,'regusuario' => '1'
						,'usuario' => $value["usuario"]
						,"clave" => md5($value["usuario"])
						,"token" => md5($value["usuario"])
						,'rol' => 3
						,'foto' => ''
						,'estado' => 1
						,'situacion' => '1'
						,'idioma' => 'ES'
						,'tipousuario' => 'n'
						,'edad_registro' => @$value['edad_registro']
						,'instruccion' => @$value['instruccion']
						,'esdemo' => isset($value['esdemo']) && !empty($value['esdemo']) ? intval($value['esdemo']) : 0
					];
				}
				$usuarios[] = $prepare;
			}

			return $usuarios;
		}catch(Exception $e){
			throw new Exception("error formatearUsuario: ".$e->getMessage()." - ".$e->getLine());
		}
	}
	public function formatearMatricula($datos){
		try{
			if(empty($datos)){ throw new Exception("datos esta vacio"); }

			$prepare = [
				'idgrupoauladetalle' => null
				,"idalumno" => null
				,"idusuario" => null
				,'fecha_registro' => date("Y-m-d")
				,'fecha_matricula' => !empty($datos['fecha_inicio']) ? $datos['fecha_inicio'] : date("Y-m-d")
				,'fecha_vencimiento' => !empty($datos['fecha_fin']) ? $datos['fecha_fin'] : date("Y-m-d")
				,'estado' => 1
				,"pagorecurso" => !empty($datos["pagorecurso"]) ? $datos["pagorecurso"] : 0
			];

			return $prepare;
		}catch(Exception $e){
			throw new Exception("error formatearMatricula: ".$e->getMessage()." - ".$e->getLine());
		}
	}
	public function prepararDatos($datos){
		try{
			//validar datos
			if(empty($datos["personas"])){ throw new Exception("personas no definido"); }
			if(empty($datos["idgrupoauladetalle"])){ throw new Exception("idgrupoauladetalle no definido"); }
			
			if(!is_array($datos["personas"]) && is_string($datos["personas"])){
				$datos["personas"] = json_decode($datos["personas"],true);
			}
			
			//formatear usuario de la tabla personal
			$datos["usuarios"] = $this->formatearUsuario($datos["personas"],$datos);
			//preparar formato para matricula
			$datos["matricula"] = $this->formatearMatricula($datos);
			//obtener grupoauladetalles
			$datos["grupoauladetalle"] = $this->oDatApi->_buscargrupos(["idgrupoauladetalle"=>$datos["idgrupoauladetalle"]]);
			//buscar carreras y niveles
			$datos["categorias"] = $this->oDatAcad_categorias->buscarcompleto(["idproyecto"=>$datos["idproyecto"]]);
			return $datos;
		}catch(Exception $e){
			throw new Exception("error en prepararDatos: ".$e->getMessage()." - ".$e->getLine());
		}
	}
	public function matricularapi($datos){
		try{
			return $this->oDatApi->matricularapi($datos);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function _registrarDocente($datos){
		try{
			return $this->oDatApi->_registrarDocente($datos);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function grupoauladetalle($filtros = array()){
		try{
			return $this->oDatApi->grupoauladetalle($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function locales($filtros = array()){
		try{
			return $this->oDatApi->locales($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function crearrepresentante($datos){
		try{
			if(empty($datos)){ throw new Exception("Datos esta vacio"); }
			return $this->oDatApi->crearrepresentante($datos);
		}catch(Exception $e){
			throw new Exception($e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}	
	}
}