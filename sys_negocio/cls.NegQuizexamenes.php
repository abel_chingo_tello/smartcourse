<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-03-2017
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatQuizexamenes', RUTA_BASE);
class NegQuizexamenes 
{
	protected $idexamen;
	protected $titulo;
	protected $descripcion;
	protected $portada;
	protected $fuente;
	protected $fuentesize;
	protected $grupo;
	protected $aleatorio;
	protected $calificacion_por;
	protected $calificacion_en;
	protected $calificacion_total;
	protected $calificacion_min;
	protected $tiempo_por;
	protected $tiempo_total;
	protected $tipo;
	protected $estado;
	protected $idpersonal;
	protected $fecharegistro;
	protected $nintento;
	protected $calificacion;
	protected $habilidades_todas;
	protected $origen_habilidades;
	protected $fuente_externa;
	protected $dificultad_promedio;
	protected $idproyecto;
	protected $tiene_certificado;
	protected $nombre_certificado;
	protected $idcc_smartcourse;
	protected $idpersona_smartcourse;
	protected $retroceder=1;
	protected $tiempoalinicio=0;
	protected $dataExamenes;
	protected $oDatExamenes;	

	public function __construct()
	{
		$this->oDatExamenes = new DatQuizexamenes;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatExamenes->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $ex) {
			throw new Exception($ex->getMessage());
		}
	}
	
	////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatExamenes->buscar($filtros);
		} catch(Exception $ex) {
			throw new Exception($ex->getMessage());
		}
	}

	public function mostrarPreguntas($filtros=array(), $preguntaspadres=array()){
		try{
			$data = array();
			if(empty($preguntaspadres) && !empty($filtros)){ 
				$newfiltro["idexamen"]=$filtros["idexamen"];
				$orden=!empty($filtros["orden"])?$filtros["orden"]:0;	
				$this->oDatExamenes->setLimite(0,999);		
				$preguntaspadres=$this->oDatExamenes->buscarpreguntas($newfiltro);	
				$resultado=array();		
				$idcontenedor=array();
			}
			foreach ($preguntaspadres as $pregunta){
				$idcont=$pregunta["idcontenedor"];
				$padre=$pregunta["idpadre"]==1?"F":"P";
				if(!in_array($idcont,$idcontenedor)){
					$data[$idcont]=array("F"=>array(),"P"=>array(),'orden'=> $pregunta['orden']);
					array_push($idcontenedor,$idcont);
				}
				array_push($data[$idcont][$padre],$pregunta);
				if($orden==1&&$padre=='P'){ shuffle($data[$idcont]["P"]); }
			}
            if($orden==1){
				shuffle($data);
			} else{
				#verificar si el examen esta ordenado de lo contrario omitir para no afectar examenes anteriores
				$sinorden = array_count_values(array_column($data,'orden'));
				if(empty($sinorden[0])){
					//ordenar registro contenedores
					uasort($data, function($a, $b) {
						return $a['orden'] - $b['orden'];
					});
				}
			}
			return $data;
		}catch(Exception $ex) {
			throw new Exception($ex->getMessage());
		}
	}


	public function getXid()
	{
		try {
			return $this->oDatExamenes->get($this->idexamen);
		} catch(Exception $ex) {
			throw new Exception($ex->getMessage());
		}
	}

	
	public function setIdexamen($pk){
		try {
			$this->dataExamenes = $this->oDatExamenes->get($pk);
			if(empty($this->dataExamenes)) {
				throw new Exception(JrTexto::_("Examenes").' '.JrTexto::_("not registered"));
			}
			$this->idexamen = $this->dataExamenes["idexamen"];
			$this->titulo = $this->dataExamenes["titulo"];
			$this->descripcion = $this->dataExamenes["descripcion"];
			$this->portada = $this->dataExamenes["portada"];
			$this->fuente = $this->dataExamenes["fuente"];
			$this->fuentesize = $this->dataExamenes["fuentesize"];
			$this->aleatorio = $this->dataExamenes["aleatorio"];
			$this->calificacion_por = $this->dataExamenes["calificacion_por"];
			$this->calificacion_en = $this->dataExamenes["calificacion_en"];
			$this->calificacion_total = $this->dataExamenes["calificacion_total"];
			$this->calificacion_min = $this->dataExamenes["calificacion_min"];
			$this->tiempo_por = $this->dataExamenes["tiempo_por"];
			$this->tiempo_total = $this->dataExamenes["tiempo_total"];
			$this->tipo = $this->dataExamenes["tipo"];
			$this->estado = $this->dataExamenes["estado"];
			$this->idpersonal = $this->dataExamenes["idpersonal"];
			$this->fecharegistro = $this->dataExamenes["fecharegistro"];
			$this->nintento = $this->dataExamenes["nintento"];
			$this->calificacion = $this->dataExamenes["calificacion"];
			$this->habilidades_todas = $this->dataExamenes["habilidades_todas"];
			$this->origen_habilidades = $this->dataExamenes["origen_habilidades"];
			$this->fuente_externa = $this->dataExamenes["fuente_externa"];
			$this->dificultad_promedio = $this->dataExamenes["dificultad_promedio"];
			$this->idproyecto = $this->dataExamenes["idproyecto"];
			$this->tiene_certificado = $this->dataExamenes["tiene_certificado"];
			$this->nombre_certificado = $this->dataExamenes["nombre_certificado"];
			$this->retroceder = $this->dataExamenes["retroceder"];
			$this->tiempoalinicio = $this->dataExamenes["tiempoalinicio"];
			//falta campos
		} catch(Exception $ex) {			
			throw new Exception($ex->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			$this->dataExamenes = $this->oDatExamenes->get($pk);
			if(empty($this->dataExamenes)) {
				throw new Exception(JrTexto::_("Examenes").' '.JrTexto::_("not registered"));
			}
			return $this->oDatExamenes->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function guardar($datos){
		try {
			return $this->oDatExamenes->guardar($datos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function persona(){
		try{
			$user = Negsesion::getUsuario();
			$slug = 'PY'.$user["idproyecto"];
			return $this->oDatExamenes->persona($user,$slug);
		}catch(Exception $ex){
			throw new Exception($ex->getMessage());
		}
	}

	public function duplicar($idexamen,$idioma='ES'){
		try {
			$user = Negsesion::getUsuario();
			$slug = 'PY'.$user["idproyecto"];
			return $this->oDatExamenes->duplicar($idexamen,$slug,$user,$idioma);
		}catch(Exception $ex) {
			throw new Exception($ex->getMessage());
		}
	}	

	public  function eliminar($idexamen){
		try{
			return $this->oDatExamenes->eliminar($idexamen);
		}catch(Exception $ex){
			throw new Exception($e->getMessage());
		}
	}
}