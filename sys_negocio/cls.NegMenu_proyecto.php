<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		10-11-2020
 * @copyright	Copyright (C) 10-11-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMenu_proyecto', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegMenu_proyecto 
{
	
	protected $idmenuproyecto;
	protected $idmenu;
	protected $idproyecto;
	protected $idrol;
	protected $orden;
	protected $idpadre;
	protected $insertar;
	protected $modificar;
	protected $eliminar;
	protected $usuario_registro;
	protected $fecha_registro;
	
	protected $dataMenu_proyecto;
	protected $oDatMenu_proyecto;	

	public function __construct()
	{
		$this->oDatMenu_proyecto = new DatMenu_proyecto;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMenu_proyecto->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	private function neworden($menu,$orden=0){
		if(!empty($menu[$orden]))
			$orden=$this->neworden($menu,$orden+1);
		return $orden;
	}

	private function translate($menus){
		$newmenu=array();
		foreach($menus as $key => $value){
			if(!empty($value["idmenu"]))
				$value["strmenu"]=ucfirst(JrTexto::_($value["strmenu"]));
			elseif(!empty($value["insertar"])) 
				$value["insertar"]=ucfirst(JrTexto::_($value["insertar"]));
			//var_dump($value);
			$newmenu[$key]=$value;
		}
		return $newmenu;
	}

	public function buscar($filtros = array())
	{
		try {			
			if(!empty($filtros["enorden"])){
				$idpadretmp=!empty($filtros["idpadre"])?$filtros["idpadre"]:false;
				if(!empty($idpadretmp)) unset($filtros["idpadre"]);
				$datosall=	$this->oDatMenu_proyecto->buscar($filtros);			
				$menus=array();
				if(!empty($datosall)){
					$datosall=$this->translate($datosall);
					$datos2=$datosall; // los principales	
					foreach($datosall as $kd => $vd){						
						if((empty($vd["idpadre"]) && empty($idpadretmp))||(@$vd["idpadre"]==@$idpadretmp)){
							unset($datos2[$kd]);
							$tienehijos=$this->menuhijos($vd["idmenuproyecto"],$datos2);
							if(!empty($tienehijos["hijos"])){
								$datos2=$tienehijos['datos'];
								$vd["hijos"]=$tienehijos['hijos'];
							}
							$orden=$this->neworden($menus,$vd["orden"]);
							$menus[$orden]=$vd;
						}
					}
				}
				return $menus;
			}else {
				$menus= $this->oDatMenu_proyecto->buscar($filtros);
				if(!empty($menus)) $menus=$this->translate($menus);
				return $menus;
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function menuhijos($idpadre,$datos2){
		$datostmp=$datos2;
		$menus=array();
		if(!empty($datos2)){			
			foreach($datos2 as $k => $v){
				if($idpadre==$v["idpadre"]){
					unset($datostmp[$k]);
					$tienehijos=$this->menuhijos($v["idmenuproyecto"],$datostmp);
					if(!empty($tienehijos["hijos"])){
						$datostmp=$tienehijos['datos'];
						$v["hijos"]=$tienehijos['hijos'];						
					}
					$orden=empty($menus[$v["orden"]])?$v["orden"]:(intval($v["orden"])+1);
					$menus[$orden]=$v;
				}				
			}
		}
		return array("hijos"=>$menus,'datos'=>$datostmp);
	}		
	
	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('menu_proyecto', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatMenu_proyecto->iniciarTransaccion('neg_i_Menu_proyecto');
			$this->idmenuproyecto = $this->oDatMenu_proyecto->insertar($this->idmenu,$this->idproyecto,$this->idrol,$this->orden,$this->idpadre,$this->insertar,$this->modificar,$this->eliminar,$this->usuario_registro);
			$this->oDatMenu_proyecto->terminarTransaccion('neg_i_Menu_proyecto');	
			return $this->idmenuproyecto;
		} catch(Exception $e) {	
		    $this->oDatMenu_proyecto->cancelarTransaccion('neg_i_Menu_proyecto');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('menu_proyecto', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatMenu_proyecto->actualizar($this->idmenuproyecto,$this->idmenu,$this->idproyecto,$this->idrol,$this->orden,$this->idpadre,$this->insertar,$this->modificar,$this->eliminar,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatMenu_proyecto->cambiarvalorcampo($this->idmenuproyecto,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Menu_proyecto', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatMenu_proyecto->eliminar($this->idmenuproyecto,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmenuproyecto($pk){
		try {
			$this->dataMenu_proyecto = $this->oDatMenu_proyecto->buscar(array('sqlget'=>true,'idmenuproyecto'=>$pk));
			if(empty($this->dataMenu_proyecto)) {
				throw new Exception(JrTexto::_("Menu_proyecto").' '.JrTexto::_("not registered"));
			}
			$this->idmenuproyecto=$this->dataMenu_proyecto["idmenuproyecto"];
			$this->idmenu = $this->dataMenu_proyecto["idmenu"];
			$this->idproyecto = $this->dataMenu_proyecto["idproyecto"];
			$this->idrol = $this->dataMenu_proyecto["idrol"];
			$this->orden = $this->dataMenu_proyecto["orden"];
			$this->idpadre = $this->dataMenu_proyecto["idpadre"];
			$this->insertar = $this->dataMenu_proyecto["insertar"];
			$this->modificar = $this->dataMenu_proyecto["modificar"];
			$this->eliminar = $this->dataMenu_proyecto["eliminar"];
			$this->usuario_registro = $this->dataMenu_proyecto["usuario_registro"];
			$this->fecha_registro = $this->dataMenu_proyecto["fecha_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('menu_proyecto', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataMenu_proyecto = $this->oDatMenu_proyecto->buscar(array('sqlget'=>true,'idmenuproyecto'=>$pk));
			if(empty($this->dataMenu_proyecto)) {
				throw new Exception(JrTexto::_("Menu_proyecto").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMenu_proyecto->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function guardarorden($datos){
		try {
			return $this->oDatMenu_proyecto->guardarorden($datos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}