<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		16-09-2018
 * @copyright	Copyright (C) 16-09-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotas_quiz', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegNotas_quiz
{
	protected $idnota;
	protected $idcursodetalle;
	protected $idgrupoauladetalle;
	protected $idrecurso;
	protected $idalumno;
	protected $tipo;
	protected $nota;
	protected $notatexto;
	protected $regfecha;
	protected $idproyecto;
	protected $calificacion_en;
	protected $calificacion_total;
	protected $calificacion_min;
	protected $tiempo_total;
	protected $tiempo_realizado;
	protected $calificacion;
	protected $habilidades;
	protected $habilidad_puntaje;
	protected $intento;
	protected $idexamenalumnoquiz;
	protected $idexamenproyecto;
	protected $idexamenidalumno;
	protected $datos;
	protected $preguntas;
	protected $idcurso = 0;
	protected $idcomplementario = 0;
	protected $preguntasoffline = null;

	protected $dataNotas_quiz;
	protected $oDatNotas_quiz;

	public function __construct()
	{
		$this->oDatNotas_quiz = new DatNotas_quiz;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatNotas_quiz->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function buscarNotas($filtros = null)
	{
		try {
			$resultado = array();
			$this->setLimite(0, 100000);
			$result = $this->oDatNotas_quiz->buscarNotas($filtros);
			if (!empty($result)) {
				$letra = 'B';
				$isTri = false;
				$cantidad = 5;
				if (isset($filtros["trimestre"])) {
					if ($filtros["trimestre"] == true) {
						$letra = 'T';
						$isTri = true;
						$cantidad = 4;
					}
				}
				foreach ($result as $key => $value) {
					$value['examen_' . $letra . '1'] = (!empty($value['examen_' . $letra . '1'])) ? intval(floor($value['examen_' . $letra . '1'] * 0.20)) : 0;
					$value['examen_' . $letra . '2'] = (!empty($value['examen_' . $letra . '2'])) ? intval(floor($value['examen_' . $letra . '2'] * 0.20)) : 0;
					$value['examen_' . $letra . '3'] = (!empty($value['examen_' . $letra . '3'])) ? intval(floor($value['examen_' . $letra . '3'] * 0.20)) : 0;
					if (!$isTri) {
						$value['examen_' . $letra . '4'] = (!empty($value['examen_' . $letra . '4'])) ? intval(floor($value['examen_' . $letra . '4'] * 0.20)) : 0;
					}
					$value['examen_salida'] = (!empty($value['examen_salida'])) ? intval(floor($value['examen_salida'] * 0.20)) : 0;
					$prom = ($isTri == false) ? $value['examen_' . $letra . '1'] + $value['examen_' . $letra . '2'] + $value['examen_' . $letra . '3'] + $value['examen_' . $letra . '4'] + $value['examen_salida'] : $value['examen_' . $letra . '1'] + $value['examen_' . $letra . '2'] + $value['examen_' . $letra . '3'] + $value['examen_salida'];
					$value['promedio_' . $letra] = ($prom != 0) ? intval(floor($prom / $cantidad)) : 0;
					$resultado[] = $value;
				}
			}

			return $resultado;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatNotas_quiz->getNumRegistros($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotas_quiz->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getHabilidadesPorExamen($params)
	{ #$params[idexamen,idalumno]
		try {
			return $this->oDatNotas_quiz->getHabilidadesPorExamen($params);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatNotas_quiz->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotas_quiz->get($this->idnota);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/

			$this->oDatNotas_quiz->iniciarTransaccion('neg_i_Notas_quiz');
			$this->idnota = $this->oDatNotas_quiz->insertar($this->idcursodetalle, $this->idrecurso, $this->idalumno, $this->tipo, $this->nota, $this->notatexto, $this->regfecha, $this->idproyecto, $this->calificacion_en, $this->calificacion_total, $this->calificacion_min, $this->tiempo_total, $this->tiempo_realizado, $this->calificacion, $this->habilidades, $this->habilidad_puntaje, $this->intento, $this->idexamenalumnoquiz, $this->idexamenproyecto, $this->idexamenidalumno, $this->datos, $this->preguntas, $this->idcurso, $this->idcomplementario, $this->preguntasoffline,$this->idgrupoauladetalle);
			$this->oDatNotas_quiz->terminarTransaccion('neg_i_Notas_quiz');
			return $this->idnota;
		} catch (Exception $e) {
			$this->oDatNotas_quiz->cancelarTransaccion('neg_i_Notas_quiz');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas_quiz->actualizar($this->idnota, $this->idcursodetalle, $this->idrecurso, $this->idalumno, $this->tipo, $this->nota, $this->notatexto, $this->regfecha, $this->idproyecto, $this->calificacion_en, $this->calificacion_total, $this->calificacion_min, $this->tiempo_total, $this->tiempo_realizado, $this->calificacion, $this->habilidades, $this->habilidad_puntaje, $this->intento, $this->idexamenalumnoquiz, $this->idexamenproyecto, $this->idexamenidalumno, $this->datos, $this->preguntas, $this->idcurso, $this->idcomplementario, $this->preguntasoffline,$this->idgrupoauladetalle);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas_quiz', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas_quiz->eliminar($this->idnota);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnota($pk)
	{
		try {
			$this->dataNotas_quiz = $this->oDatNotas_quiz->get($pk);
			if (empty($this->dataNotas_quiz)) {
				throw new Exception(JrTexto::_("Notas_quiz") . ' ' . JrTexto::_("not registered"));
			}
			$this->idnota = $this->dataNotas_quiz["idnota"];
			$this->idcursodetalle = $this->dataNotas_quiz["idcursodetalle"];
			$this->idrecurso = $this->dataNotas_quiz["idrecurso"];
			$this->idalumno = $this->dataNotas_quiz["idalumno"];
			$this->tipo = $this->dataNotas_quiz["tipo"];
			$this->nota = $this->dataNotas_quiz["nota"];
			$this->notatexto = $this->dataNotas_quiz["notatexto"];
			$this->regfecha = $this->dataNotas_quiz["regfecha"];
			$this->idproyecto = $this->dataNotas_quiz["idproyecto"];
			$this->calificacion_en = $this->dataNotas_quiz["calificacion_en"];
			$this->calificacion_total = $this->dataNotas_quiz["calificacion_total"];
			$this->calificacion_min = $this->dataNotas_quiz["calificacion_min"];
			$this->tiempo_total = $this->dataNotas_quiz["tiempo_total"];
			$this->tiempo_realizado = $this->dataNotas_quiz["tiempo_realizado"];
			$this->calificacion = $this->dataNotas_quiz["calificacion"];
			$this->habilidades = $this->dataNotas_quiz["habilidades"];
			$this->habilidad_puntaje = $this->dataNotas_quiz["habilidad_puntaje"];
			$this->intento = $this->dataNotas_quiz["intento"];
			$this->idexamenalumnoquiz = $this->dataNotas_quiz["idexamenalumnoquiz"];
			$this->idexamenproyecto = $this->dataNotas_quiz["idexamenproyecto"];
			$this->idexamenidalumno = $this->dataNotas_quiz["idexamenidalumno"];
			$this->idcomplementario = $this->dataNotas_quiz["idcomplementario"];
			$this->idgrupoauladetalle = $this->dataNotas_quiz["idgrupoauladetalle"];
			$this->idcurso = $this->dataNotas_quiz["idcurso"];
			$this->datos = $this->dataNotas_quiz['datos'];
			$this->preguntasoffline = $this->dataNotas_quiz['preguntasoffline'];
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotas_quiz = $this->oDatNotas_quiz->get($pk);
			if (empty($this->dataNotas_quiz)) {
				throw new Exception(JrTexto::_("Notas_quiz") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatNotas_quiz->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
