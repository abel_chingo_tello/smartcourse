<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-01-2018
 * @copyright	Copyright (C) 11-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_horariogrupodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_horariogrupodetalle 
{
	protected $idhorario;
	protected $idgrupoauladetalle;
	protected $fecha_inicio;
	protected $fecha_final;
	protected $descripcion;
	protected $color;
	protected $idhorariopadre;
	protected $diasemana;
	
	protected $dataAcad_horariogrupodetalle;
	protected $oDatAcad_horariogrupodetalle;	

	public function __construct()
	{
		$this->oDatAcad_horariogrupodetalle = new DatAcad_horariogrupodetalle;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_horariogrupodetalle->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_horariogrupodetalle->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function maxidpadre($filtros = array())
	{
		try {
			return $this->oDatAcad_horariogrupodetalle->maxidpadre($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_horariogrupodetalle->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_horariogrupodetalle->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_horariogrupodetalle->get($this->idhorario);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_horariogrupodetalle', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_horariogrupodetalle->iniciarTransaccion('neg_i_Acad_horariogrupodetalle');
			$this->idhorario = $this->oDatAcad_horariogrupodetalle->insertar($this->idgrupoauladetalle,$this->fecha_inicio,$this->fecha_final,$this->descripcion,$this->color,$this->idhorariopadre,$this->diasemana);
			$this->oDatAcad_horariogrupodetalle->terminarTransaccion('neg_i_Acad_horariogrupodetalle');	
			return $this->idhorario;
		} catch(Exception $e) {	
		    $this->oDatAcad_horariogrupodetalle->cancelarTransaccion('neg_i_Acad_horariogrupodetalle');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_horariogrupodetalle', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_horariogrupodetalle->actualizar($this->idhorario,$this->idgrupoauladetalle,$this->fecha_inicio,$this->fecha_final,$this->descripcion,$this->color,$this->idhorariopadre,$this->diasemana);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_horariogrupodetalle', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_horariogrupodetalle->eliminar($this->idhorario);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminarxpadre($idhorariopadre)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_horariogrupodetalle', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_horariogrupodetalle->eliminarxpadre($idhorariopadre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdhorario($pk){
		try {
			$this->dataAcad_horariogrupodetalle = $this->oDatAcad_horariogrupodetalle->get($pk);
			if(empty($this->dataAcad_horariogrupodetalle)) {
				throw new Exception(JrTexto::_("Acad_horariogrupodetalle").' '.JrTexto::_("not registered"));
			}
			$this->idhorario = $this->dataAcad_horariogrupodetalle["idhorario"];
			$this->idgrupoauladetalle = $this->dataAcad_horariogrupodetalle["idgrupoauladetalle"];
			$this->fecha_finicio = $this->dataAcad_horariogrupodetalle["fecha_inicio"];
			$this->fecha_final = $this->dataAcad_horariogrupodetalle["fecha_final"];
			$this->descripcion = $this->dataAcad_horariogrupodetalle["descripcion"];
			$this->color = $this->dataAcad_horariogrupodetalle["color"];
			$this->idhorariopadre = $this->dataAcad_horariogrupodetalle["idhorariopadre"];
			$this->diasemana = $this->dataAcad_horariogrupodetalle["diasemana"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_horariogrupodetalle', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_horariogrupodetalle = $this->oDatAcad_horariogrupodetalle->get($pk);
			if(empty($this->dataAcad_horariogrupodetalle)) {
				throw new Exception(JrTexto::_("Acad_horariogrupodetalle").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_horariogrupodetalle->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}