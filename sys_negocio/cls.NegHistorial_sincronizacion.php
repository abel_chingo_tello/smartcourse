<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatHistorial_sincronizacion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegHistorial_sincronizacion 
{
	protected $idhisesion;
	protected $idhistorialsin;
	protected $nombre_archivo;
	protected $fechaentrada;
	protected $estadoexp;
	protected $estadoimp;
	protected $tabla;
	protected $fechamodificacion;
	protected $completo;
	protected $contactualizador;
	protected $estadoimpac;
	
	protected $dataHistorial_sincronizacion;
	protected $oDatHistorial_sincronizacion;	

	public function __construct()
	{
		$this->oDatHistorial_sincronizacion = new DatHistorial_sincronizacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatHistorial_sincronizacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatHistorial_sincronizacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatHistorial_sincronizacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatHistorial_sincronizacion->get($this->idhisesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_sincronizacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatHistorial_sincronizacion->iniciarTransaccion('neg_i_Historial_sincronizacion');
			$this->idhisesion = $this->oDatHistorial_sincronizacion->insertar($this->idhistorialsin,$this->nombre_archivo,$this->fechaentrada,$this->estadoexp,$this->estadoimp,$this->tabla,$this->fechamodificacion,$this->completo,$this->contactualizador,$this->estadoimpac);
			$this->oDatHistorial_sincronizacion->terminarTransaccion('neg_i_Historial_sincronizacion');	
			return $this->idhisesion;
		} catch(Exception $e) {	
		    $this->oDatHistorial_sincronizacion->cancelarTransaccion('neg_i_Historial_sincronizacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_sincronizacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatHistorial_sincronizacion->actualizar($this->idhisesion,$this->idhistorialsin,$this->nombre_archivo,$this->fechaentrada,$this->estadoexp,$this->estadoimp,$this->tabla,$this->fechamodificacion,$this->completo,$this->contactualizador,$this->estadoimpac);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Historial_sincronizacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatHistorial_sincronizacion->eliminar($this->idhisesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdhisesion($pk){
		try {
			$this->dataHistorial_sincronizacion = $this->oDatHistorial_sincronizacion->get($pk);
			if(empty($this->dataHistorial_sincronizacion)) {
				throw new Exception(JrTexto::_("Historial_sincronizacion").' '.JrTexto::_("not registered"));
			}
			$this->idhisesion = $this->dataHistorial_sincronizacion["idhisesion"];
			$this->idhistorialsin = $this->dataHistorial_sincronizacion["idhistorialsin"];
			$this->nombre_archivo = $this->dataHistorial_sincronizacion["nombre_archivo"];
			$this->fechaentrada = $this->dataHistorial_sincronizacion["fechaentrada"];
			$this->estadoexp = $this->dataHistorial_sincronizacion["estadoexp"];
			$this->estadoimp = $this->dataHistorial_sincronizacion["estadoimp"];
			$this->tabla = $this->dataHistorial_sincronizacion["tabla"];
			$this->fechamodificacion = $this->dataHistorial_sincronizacion["fechamodificacion"];
			$this->completo = $this->dataHistorial_sincronizacion["completo"];
			$this->contactualizador = $this->dataHistorial_sincronizacion["contactualizador"];
			$this->estadoimpac = $this->dataHistorial_sincronizacion["estadoimpac"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('historial_sincronizacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataHistorial_sincronizacion = $this->oDatHistorial_sincronizacion->get($pk);
			if(empty($this->dataHistorial_sincronizacion)) {
				throw new Exception(JrTexto::_("Historial_sincronizacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHistorial_sincronizacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}