<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		15-01-2021
 * @copyright	Copyright (C) 15-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatSinlogin_quiz', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegSinlogin_quiz 
{
	protected $idnota;
	protected $idexamen;
	protected $idpersona;
	protected $tipo;
	protected $nota;
	protected $notatexto;
	protected $regfecha;
	protected $idproyecto;
	protected $calificacion_en;
	protected $calificacion_total;
	protected $calificacion_min;
	protected $tiempo_total;
	protected $tiempo_realizado;
	protected $calificacion;
	protected $habilidades;
	protected $habilidad_puntaje;
	protected $intento;
	protected $datos;
	protected $fechamodificacion;
	protected $preguntas;
	
	protected $dataSinlogin_quiz;
	protected $oDatSinlogin_quiz;	

	public function __construct()
	{
		$this->oDatSinlogin_quiz = new DatSinlogin_quiz;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatSinlogin_quiz->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatSinlogin_quiz->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatSinlogin_quiz->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatSinlogin_quiz->get($this->idpersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_quiz', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatSinlogin_quiz->iniciarTransaccion('neg_i_Sinlogin_quiz');
			$this->idpersona = $this->oDatSinlogin_quiz->insertar($this->idpersona,$this->idexamen,$this->tipo,$this->nota,$this->notatexto,$this->idproyecto,$this->calificacion_en,$this->calificacion_total,$this->calificacion_min,$this->tiempo_total,$this->tiempo_realizado,$this->calificacion,$this->habilidades,$this->habilidad_puntaje,$this->intento,$this->datos,$this->preguntas);
			$this->oDatSinlogin_quiz->terminarTransaccion('neg_i_Sinlogin_quiz');	
			return $this->idpersona;
		} catch(Exception $e) {	
		    $this->oDatSinlogin_quiz->cancelarTransaccion('neg_i_Sinlogin_quiz');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_quiz', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatSinlogin_quiz->actualizar($this->idnota,$this->idpersona,$this->idexamen,$this->tipo,$this->nota,$this->notatexto,$this->idproyecto,$this->calificacion_en,$this->calificacion_total,$this->calificacion_min,$this->tiempo_total,$this->tiempo_realizado,$this->calificacion,$this->habilidades,$this->habilidad_puntaje,$this->intento,$this->datos,$this->preguntas);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Sinlogin_quiz', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatSinlogin_quiz->eliminar($this->idpersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnota($pk){
		try {
			var_dump($pk);
			$this->dataSinlogin_quiz = $this->oDatSinlogin_quiz->get($pk);
			if(empty($this->dataSinlogin_quiz)) {
				throw new Exception(JrTexto::_("Sinlogin_quiz").' '.JrTexto::_("not registered "));
			}
			$this->idnota = $this->dataSinlogin_quiz["idnota"];
			$this->idexamen = $this->dataSinlogin_quiz["idexamen"];
			$this->idpersona = $this->dataSinlogin_quiz["idpersona"];
			$this->tipo = $this->dataSinlogin_quiz["tipo"];
			$this->nota = $this->dataSinlogin_quiz["nota"];
			$this->notatexto = $this->dataSinlogin_quiz["notatexto"];
			$this->regfecha = $this->dataSinlogin_quiz["regfecha"];
			$this->idproyecto = $this->dataSinlogin_quiz["idproyecto"];
			$this->calificacion_en = $this->dataSinlogin_quiz["calificacion_en"];
			$this->calificacion_total = $this->dataSinlogin_quiz["calificacion_total"];
			$this->calificacion_min = $this->dataSinlogin_quiz["calificacion_min"];
			$this->tiempo_total = $this->dataSinlogin_quiz["tiempo_total"];
			$this->tiempo_realizado = $this->dataSinlogin_quiz["tiempo_realizado"];
			$this->calificacion = $this->dataSinlogin_quiz["calificacion"];
			$this->habilidades = $this->dataSinlogin_quiz["habilidades"];
			$this->habilidad_puntaje = $this->dataSinlogin_quiz["habilidad_puntaje"];
			$this->intento = $this->dataSinlogin_quiz["intento"];
			$this->datos = $this->dataSinlogin_quiz["datos"];
			$this->fechamodificacion = $this->dataSinlogin_quiz["fechamodificacion"];
			$this->preguntas = $this->dataSinlogin_quiz["preguntas"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_quiz', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataSinlogin_quiz = $this->oDatSinlogin_quiz->get($pk);
			if(empty($this->dataSinlogin_quiz)) {
				throw new Exception(JrTexto::_("Sinlogin_quiz").' '.JrTexto::_("not registered"));
			}

			return $this->oDatSinlogin_quiz->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}