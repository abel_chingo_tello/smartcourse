<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		25-10-2018
 * @copyright	Copyright (C) 25-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatLocal', RUTA_BASE);
JrCargador::clase('sys_datos::DatUgel', RUTA_BASE);
JrCargador::clase('sys_datos::DatMin_dre', RUTA_BASE);
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);

class NegLocal 
{
	protected $idlocal;
	protected $nombre;
	protected $direccion;
	protected $id_ubigeo;
	protected $tipo;
	protected $vacantes;
	protected $idugel;
	protected $idproyecto;
	
	protected $dataLocal;
	protected $oDatLocal;
	private $oNegUgel;

	public function __construct()
	{
		$this->oDatLocal = new DatLocal;
		$this->oNegUgel = new NegUgel;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatLocal->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatLocal->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatLocal->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function infolocal($filtros = null){
		try{
			return $this->oDatLocal->infolocal($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatLocal->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatLocal->get($this->idlocal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('local', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatLocal->iniciarTransaccion('neg_i_Local');
			$this->idlocal = $this->oDatLocal->insertar($this->nombre,$this->direccion,$this->id_ubigeo,$this->tipo,$this->vacantes,$this->idugel,$this->idproyecto);
			$this->oDatLocal->terminarTransaccion('neg_i_Local');	
			return $this->idlocal;
		} catch(Exception $e) {	
		    $this->oDatLocal->cancelarTransaccion('neg_i_Local');		
			throw new Exception($e->getMessage());
		}
	}

	public function importar($iddre,$dre,$idugel,$ugel,$idiiee,$iiee,$direccion,$ubigeo,$tipo,$nvacantes,$idproyecto)
	{
		$this->oNegUgel->idproyecto=$idproyecto;
		$ugel=$this->oNegUgel->importar($idugel,$ugel,'','',$dre,'',$iddre);
		$idugel=$ugel['_Id'];
		$iddre=$ugel['_iddre'];

		if(!empty($idiiee)){
			$hay=empty($idiiee)?$this->buscar(array('idlocal'=>$idiiee,'idproyecto'=>$idproyecto)):array();
			if(!empty($hay[0])) return array('_estado'=>'E','_Id'=>$hay[0]["idlocal"]);
		}
		$idlocal=$this->oDatLocal->insertar($iiee,$direccion,$ubigeo,$tipo,$nvacantes,$idugel,$idproyecto);
		return array('_estado'=>'N','_Id'=>$idlocal,'_idimport'=>$idiiee,'_idugel'=>$idugel,'_iddre'=>$iddre);
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('local', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatLocal->actualizar($this->idlocal,$this->nombre,$this->direccion,$this->id_ubigeo,$this->tipo,$this->vacantes,$this->idugel,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatLocal->cambiarvalorcampo($this->idlocal,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Local', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatLocal->eliminar($this->idlocal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdlocal($pk){
		try {
			$this->dataLocal = $this->oDatLocal->get($pk);

			if(empty($this->dataLocal)) {
				throw new Exception(JrTexto::_("Local").' '.JrTexto::_("not registered"));
			}
			$this->idlocal = $this->dataLocal["idlocal"];
			$this->nombre = $this->dataLocal["nombre"];
			$this->direccion = $this->dataLocal["direccion"];
			$this->id_ubigeo = $this->dataLocal["id_ubigeo"];
			$this->tipo = $this->dataLocal["tipo"];
			$this->vacantes = $this->dataLocal["vacantes"];
			$this->idugel = $this->dataLocal["idugel"];
			$this->idproyecto = $this->dataLocal["idproyecto"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('local', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataLocal = $this->oDatLocal->get($pk);
			if(empty($this->dataLocal)) {
				throw new Exception(JrTexto::_("Local").' '.JrTexto::_("not registered"));
			}

			return $this->oDatLocal->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}