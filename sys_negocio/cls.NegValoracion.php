<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		21-03-2020
 * @copyright	Copyright (C) 21-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatValoracion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegValoracion 
{
	protected $idvaloracion;
	protected $idcurso;
	protected $idrupoaula;
	protected $idcomplementario;
	protected $idalumno;
	protected $puntuacion;
	protected $comentario;
	protected $idgrupoauladetalle;
	
	protected $dataValoracion;
	protected $oDatValoracion;	

	public function __construct()
	{
		$this->oDatValoracion = new DatValoracion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatValoracion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatValoracion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatValoracion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatValoracion->get($this->idvaloracion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar($estados = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('valoracion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatValoracion->iniciarTransaccion('neg_i_Valoracion');
			$idvaloracion = $this->oDatValoracion->insertar($estados);
			$this->oDatValoracion->terminarTransaccion('neg_i_Valoracion');	
			return $idvaloracion;
		} catch(Exception $e) {	
		    $this->oDatValoracion->cancelarTransaccion('neg_i_Valoracion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('valoracion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatValoracion->actualizar($this->idvaloracion,$this->idcurso,$this->idrupoaula,$this->idcomplementario,$this->idalumno,$this->puntuacion,$this->comentario,$this->idgrupoauladetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Valoracion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatValoracion->eliminar($this->idvaloracion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdvaloracion($pk){
		try {
			$this->dataValoracion = $this->oDatValoracion->get($pk);
			if(empty($this->dataValoracion)) {
				throw new Exception(JrTexto::_("Valoracion").' '.JrTexto::_("not registered"));
			}
			$this->idvaloracion = $this->dataValoracion["idvaloracion"];
			$this->idcurso = $this->dataValoracion["idcurso"];
			$this->idrupoaula = $this->dataValoracion["idrupoaula"];
			$this->idcomplementario = $this->dataValoracion["idcomplementario"];
			$this->idalumno = $this->dataValoracion["idalumno"];
			$this->puntuacion = $this->dataValoracion["puntuacion"];
			$this->comentario = $this->dataValoracion["comentario"];
			$this->idgrupoauladetalle = $this->dataValoracion["idgrupoauladetalle"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('valoracion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataValoracion = $this->oDatValoracion->get($pk);
			if(empty($this->dataValoracion)) {
				throw new Exception(JrTexto::_("Valoracion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatValoracion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}