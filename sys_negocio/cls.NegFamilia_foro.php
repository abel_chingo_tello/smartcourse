<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		30-03-2020
 * @copyright	Copyright (C) 30-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatFamilia_foro', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegFamilia_foro 
{
	protected $codigo;
	protected $persona;
	protected $idproyecto;
	protected $idempresa;
	protected $likes;
	protected $like_de;
	protected $contenido;
	protected $fecha_hora;
	protected $respuesta;
	
	protected $dataFamilia_foro;
	protected $oDatFamilia_foro;	

	public function __construct()
	{
		$this->oDatFamilia_foro = new DatFamilia_foro;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatFamilia_foro->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatFamilia_foro->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatFamilia_foro->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatFamilia_foro->get($this->codigo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar($estados = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('familia_foro', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatFamilia_foro->iniciarTransaccion('neg_i_Familia_foro');
			$codigo = $this->oDatFamilia_foro->insertar($estados);
			$this->oDatFamilia_foro->terminarTransaccion('neg_i_Familia_foro');	
			return $codigo;
		} catch(Exception $e) {	
		    $this->oDatFamilia_foro->cancelarTransaccion('neg_i_Familia_foro');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('familia_foro', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatFamilia_foro->actualizar($this->codigo,$this->persona,$this->idproyecto,$this->idempresa,$this->likes,$this->like_de,$this->contenido,$this->fecha_hora,$this->respuesta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Familia_foro', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatFamilia_foro->eliminar($this->codigo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCodigo($pk){
		try {
			$this->dataFamilia_foro = $this->oDatFamilia_foro->get($pk);
			if(empty($this->dataFamilia_foro)) {
				throw new Exception(JrTexto::_("Familia_foro").' '.JrTexto::_("not registered"));
			}
			$this->codigo = $this->dataFamilia_foro["codigo"];
			$this->persona = $this->dataFamilia_foro["persona"];
			$this->idproyecto = $this->dataFamilia_foro["idproyecto"];
			$this->idempresa = $this->dataFamilia_foro["idempresa"];
			$this->likes = $this->dataFamilia_foro["likes"];
			$this->like_de = $this->dataFamilia_foro["like_de"];
			$this->contenido = $this->dataFamilia_foro["contenido"];
			$this->fecha_hora = $this->dataFamilia_foro["fecha_hora"];
			$this->respuesta = $this->dataFamilia_foro["respuesta"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('familia_foro', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataFamilia_foro = $this->oDatFamilia_foro->get($pk);
			if(empty($this->dataFamilia_foro)) {
				throw new Exception(JrTexto::_("Familia_foro").' '.JrTexto::_("not registered"));
			}

			return $this->oDatFamilia_foro->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}