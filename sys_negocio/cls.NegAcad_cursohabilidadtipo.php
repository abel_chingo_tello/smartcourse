<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_cursohabilidadtipo', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_cursohabilidadtipo 
{
	protected $idtipo;
	protected $nombre;
	
	protected $dataAcad_cursohabilidadtipo;
	protected $oDatAcad_cursohabilidadtipo;	

	public function __construct()
	{
		$this->oDatAcad_cursohabilidadtipo = new DatAcad_cursohabilidadtipo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_cursohabilidadtipo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_cursohabilidadtipo->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_cursohabilidadtipo->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_cursohabilidadtipo->get($this->idtipo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursohabilidadtipo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_cursohabilidadtipo->iniciarTransaccion('neg_i_Acad_cursohabilidadtipo');
			$this->idtipo = $this->oDatAcad_cursohabilidadtipo->insertar($this->nombre);
			$this->oDatAcad_cursohabilidadtipo->terminarTransaccion('neg_i_Acad_cursohabilidadtipo');	
			return $this->idtipo;
		} catch(Exception $e) {	
		    $this->oDatAcad_cursohabilidadtipo->cancelarTransaccion('neg_i_Acad_cursohabilidadtipo');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursohabilidadtipo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_cursohabilidadtipo->actualizar($this->idtipo,$this->nombre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_cursohabilidadtipo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_cursohabilidadtipo->eliminar($this->idtipo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtipo($pk){
		try {
			$this->dataAcad_cursohabilidadtipo = $this->oDatAcad_cursohabilidadtipo->get($pk);
			if(empty($this->dataAcad_cursohabilidadtipo)) {
				throw new Exception(JrTexto::_("Acad_cursohabilidadtipo").' '.JrTexto::_("not registered"));
			}
			$this->idtipo = $this->dataAcad_cursohabilidadtipo["idtipo"];
			$this->nombre = $this->dataAcad_cursohabilidadtipo["nombre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursohabilidadtipo', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_cursohabilidadtipo = $this->oDatAcad_cursohabilidadtipo->get($pk);
			if(empty($this->dataAcad_cursohabilidadtipo)) {
				throw new Exception(JrTexto::_("Acad_cursohabilidadtipo").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_cursohabilidadtipo->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}