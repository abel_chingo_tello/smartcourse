<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-03-2017
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatExamenes_preguntas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegExamenes_preguntas 
{
	protected $idpregunta;
	protected $idexamen;
	protected $pregunta;
	protected $descripcion;
	protected $ejercicio;
	protected $idpadre;
	protected $tiempo;
	protected $puntaje;
	protected $template;
	protected $habilidad;
	protected $idpersonal;
	protected $fecharegistro;
	protected $idcontenedor;
	
	protected $dataExamenes_preguntas;
	protected $oDatExamenes_preguntas;	

	public function __construct()
	{
		$this->oDatExamenes_preguntas = new DatExamenes_preguntas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatExamenes_preguntas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatExamenes_preguntas->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatExamenes_preguntas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function mostrarPreguntas($filtros=array(), $preguntaspadres=array()){
		try{
			$data = array();
			if(empty($preguntaspadres) && !empty($filtros)){ 
				$newfiltro["idexamen"]=$filtros["idexamen"];
				$orden=!empty($filtros["orden"])?$filtros["orden"]:0;			
				$preguntaspadres=$this->buscar($newfiltro);	
				$resultado=array();		
				$idcontenedor=array();
			}
			foreach ($preguntaspadres as $pregunta){
				$idcont=$pregunta["idcontenedor"];
				$padre=$pregunta["idpadre"]==1?"F":"P";
				if(!in_array($idcont,$idcontenedor)){
					$data[$idcont]=array("F"=>array(),"P"=>array());
					array_push($idcontenedor,$idcont);
				}
				array_push($data[$idcont][$padre],$pregunta);
				if($orden==1&&$padre=='P')shuffle($data[$idcont]["P"]);
			}
            if($orden==1)shuffle($data);
			return $data;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatExamenes_preguntas->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatExamenes_preguntas->get($this->idpregunta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->idpregunta = $this->oDatExamenes_preguntas->insertar($this->idexamen,$this->pregunta,$this->descripcion,$this->ejercicio,$this->idpadre,$this->tiempo,$this->puntaje,$this->idpersonal,$this->template,$this->habilidad,$this->idcontenedor);
			return $this->idpregunta;
		} catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			$this->oDatExamenes_preguntas->actualizar($this->idpregunta,$this->idexamen,$this->pregunta,$this->descripcion,$this->ejercicio,$this->idpadre,$this->tiempo,$this->puntaje,$this->idpersonal,$this->template,$this->habilidad,$this->idcontenedor);
			return $this->idpregunta;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			return $this->oDatExamenes_preguntas->eliminar($this->idpregunta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function eliminarxcontenedor($pk)
	{
		try {
			$this->oDatExamenes_preguntas->eliminarxcontenedor($pk);
			return $pk;
			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpregunta($pk){
		try {
			$this->dataExamenes_preguntas = $this->oDatExamenes_preguntas->get($pk);
			$this->idpregunta = $this->dataExamenes_preguntas["idpregunta"];
			$this->idexamen = $this->dataExamenes_preguntas["idexamen"];
			$this->pregunta = $this->dataExamenes_preguntas["pregunta"];
			$this->descripcion = $this->dataExamenes_preguntas["descripcion"];
			$this->ejercicio = $this->dataExamenes_preguntas["ejercicio"];
			$this->idpadre = $this->dataExamenes_preguntas["idpadre"];
			$this->tiempo = $this->dataExamenes_preguntas["tiempo"];
			$this->puntaje = $this->dataExamenes_preguntas["puntaje"];
			$this->idpersonal = $this->dataExamenes_preguntas["idpersonal"];
			$this->habilidad = $this->dataExamenes_preguntas["habilidades"];
			$this->template = $this->dataExamenes_preguntas["template"];
			$this->idcontenedor = $this->dataExamenes_preguntas["idcontenedor"];
			$this->fecharegistro = $this->dataExamenes_preguntas["fecharegistro"];
			//return $this->dataExamenes_preguntas;
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			$this->dataExamenes_preguntas = $this->oDatExamenes_preguntas->get($pk);
			if(empty($this->dataExamenes_preguntas)) {
				throw new Exception(JrTexto::_("Examenes_preguntas").' '.JrTexto::_("not registered"));
			}
			return $this->oDatExamenes_preguntas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}