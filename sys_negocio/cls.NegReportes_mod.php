<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatReportes', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatReportes_mod', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatGeneral', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatActividad_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegReporteModular', RUTA_BASE);

class NegReportes_mod
{
    protected $oDatReportes_mod;
    protected $oDatGeneral;
    protected $oDatActividad_alumno;
    protected $oNegReporteModular;
    public function __construct()
	{
        $this->oDatReportes_mod = new DatReportes_mod;
        $this->oDatGeneral = new DatGeneral;
        $this->oDatActividad_alumno = new DatActividad_alumno;
        $this->oNegReporteModular = new NegReporteModular;
    }
    
    /**
     * Funcion que retorna todos los alumnos asociado a un grupodeestudio
     * @param Array parametros para filtrar la busqueda
     * @return Array El resultado de la busqueda 
     */
    public function findAlumnosxgrupo($params = []){
        try{
            $result = [];
            if(empty($params['idgrupoauladetalle'])) throw new Exception("Id grupo aula detalle es requerido");
            
            $rs_alumnos = $this->oDatReportes_mod->findAlumnosxgrupo($params);
            if(!empty($rs_alumnos)){
                $_documentos = $this->oDatGeneral->buscar(['tipo_tabla'=>'tipodocidentidad','mostrar'=>1]);
                $result = array_map(function($v) use($_documentos){
                    $find = array_search($v['tipodocumento'],array_column($_documentos,'codigo'));
                    $v['nombredocumento'] = $find !== false ? $_documentos[$find]['nombre'] : null;
                    return $v;
                },$rs_alumnos);
            }
            return $result;
        }catch(Exception $e){
            throw new Exception("Un error en findAlumnosxgrupo ".$e->getMessage());
        }

    }
    /**
     * Funcion que retorna todos los alumnos asociado a un grupodeestudio
     * @param Array parametros para filtrar la busqueda
     * @return Array El resultado de la busqueda 
     */
    public function findHabilidadxsesion($params = array()){
        try{
            $alumnos = !empty($params['alumnos']) ? $params['alumnos'] : [];
            $result = [];
            #Determinar si se debe buscar alumnos
            if(empty($alumnos)){
                #buscar grupo
                if(!empty($params['idgrupodetalle'])){
                    $alumnos = $this->findAlumnosxgrupo(['idgrupoauladetalle'=>$params['idgrupodetalle']]);
                }else if(!empty($params['idcurso'])){
                    $idcomplementario = !empty($params['idcurso']) ?$params['idcomplementario'] : null;
                    //por hacer
                }else{
                    throw new Exception("Se requiere obtener alumnos para evaluar las habilidades");
                }
            }
            
            if(empty($alumnos)){ throw new Exception("Se requiere obtener alumnos para evaluar las habilidades"); }
            if(empty($params["idcurso"])){ throw new Exception("Se id del curso para evaluar las habilidades"); }
            if(!isset($params["idcomplementario"])){ throw new Exception("Se requiere id complementario para evaluar las habilidades"); }
            if(empty($params["idproyecto"])){ throw new Exception("Se requiere id del proyecto para evaluar las habilidades"); }
            if(empty($params["idrecurso"])){ throw new Exception("Se requiere idrecurso para evaluar las habilidades"); }
            
            $bd_se = !empty($params['bd_se']) ? $params['bd_se'] : NAME_BDSEAdultos;
            $esexamen = !empty($params['esexamen']) ? $params['esexamen'] : false;
            
            foreach($alumnos as $key => $alumno){
                $alumno['Listening'] = 0;
                $alumno['Reading']  = 0;
                $alumno['Writing']  = 0;
                $alumno['Speaking'] = 0;

                #buscar habilidades segun idalumno
                if($esexamen == false){
                    $arrHabilidades = $this->oDatActividad_alumno->getProgresoHabilidadesSB([
                        "idrecurso" => $params["idrecurso"],
                        "idalumno" => $alumno["idalumno"],
                        "idcurso" =>	$params["idcurso"],
                        "idcomplementario" => $params["idcomplementario"],
                        "idproyecto" => $params["idproyecto"],
                        "bd_se" => $bd_se,
                        "idgrupoauladetalle" => $params['idgrupoauladetalle']
                    ]);
                }else{
                    $arrHabilidades = $this->oNegReporteModular->getProgresoHabilidadExamen([
                        "idalumno" => $alumno["idalumno"],
                        "idexamen" => $params["idrecurso"],
                        "idgrupoauladetalle" => $params['idgrupoauladetalle']
                    ]);
                }

                if(!empty($arrHabilidades)){
                    foreach($arrHabilidades as $habilidad){
                        $alumno[$habilidad['skill_name']] = $habilidad['progreso'];
                    }
                }
                $result[] = $alumno;
            }
            return $result;
        }catch(Exception $e){
            throw new Exception("Un error en findHabilidadxsesion ".$e->getMessage());
        }
    }

}