<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_idioma', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegBib_idioma 
{
	protected $id_idioma;
	protected $descripcion;
	
	protected $dataBib_idioma;
	protected $oDatBib_idioma;	

	public function __construct()
	{
		$this->oDatBib_idioma = new DatBib_idioma;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_idioma->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_idioma->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_idioma->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_idioma->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_idioma->get($this->id_idioma);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_idioma', 'add')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_idioma->iniciarTransaccion('neg_i_Bib_idioma');
			$this->id_idioma = $this->oDatBib_idioma->insertar($this->descripcion);
			//$this->oDatBib_idioma->terminarTransaccion('neg_i_Bib_idioma');	
			return $this->id_idioma;
		} catch(Exception $e) {	
		   //$this->oDatBib_idioma->cancelarTransaccion('neg_i_Bib_idioma');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_idioma', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatBib_idioma->actualizar($this->id_idioma,$this->descripcion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_idioma', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_idioma->eliminar($this->id_idioma);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_idioma($pk){
		try {
			$this->dataBib_idioma = $this->oDatBib_idioma->get($pk);
			if(empty($this->dataBib_idioma)) {
				throw new Exception(JrTexto::_("Bib_idioma").' '.JrTexto::_("not registered"));
			}
			$this->id_idioma = $this->dataBib_idioma["id_idioma"];
			$this->descripcion = $this->dataBib_idioma["descripcion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_idioma', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_idioma = $this->oDatBib_idioma->get($pk);
			if(empty($this->dataBib_idioma)) {
				throw new Exception(JrTexto::_("Bib_idioma").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_idioma->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}