<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-05-2017
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class NegTools
{
	public function __construct() {}
	
	public static function validar($tipo, $valor, $obligatorio = true, $msj = false, $params = array())
	{
		$valor = strip_tags(trim($valor));
		$valor_ = self::utf8($valor);
		
		if(false == $obligatorio && empty($valor)) {
			return '';
		} else {
			if('letras' == $tipo) {
				if(preg_match("/^[A-Z��������������' ]+$/i", $valor_)) {
					if(is_numeric(@$params['longmax'])) {
						return substr($valor, 0, $params['longmax']);
					}
					return $valor;
				}
			} elseif('alfanum' == $tipo) {
				if(preg_match("/^[A-Z0-9��������������' ]+$/i", $valor_)) {
					if(is_numeric(@$params['longmax'])) {
						return substr($valor, 0, $params['longmax']);
					}
					
					return $valor;
				}
			} elseif('url' == $tipo) {
				if(!filter_var($valor_, FILTER_VALIDATE_URL) === false) {
					return $valor;
				}				
			} elseif('todo' == $tipo) {
				if(!empty($valor_) && is_numeric(@$params['longmax'])) {
					return substr($valor, 0, $params['longmax']);
				}
			} elseif('usuario' == $tipo) {
				$patron = "/^[a-z0-9.\d_]{".$params['longmin'].",".$params['longmax']."}$/i";
				if(preg_match($patron, $valor_)) {
					return $valor;
				}
			} elseif('num' == $tipo) {
				if(is_numeric($valor_)) {
					return $valor;
				}
			} elseif('email' == $tipo) {
				if(preg_match("/^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$/", $valor_)) {
					return $valor;
				}
			} elseif('long' == $tipo) {
				if(is_numeric(@$params['longmin']) && strlen($valor) >= $params['longmin']) {
					if(is_numeric(@$params['longmax']) && strlen($valor) <= $params['longmax']) {
						if(is_numeric(@$params['longigual']) && strlen($valor) == $params['longigual']) {
							return true;
						}
						return true;
					}
					return true;
				}
			} elseif('igual' == $tipo) {
				if($valor_ == $params['espejo']) {
					return true;
				}
			} else {
				return true;
			}
		}
		
		throw new Exception($msj);
	}
	
	public static function chekFecha($fecha, $formato = 'ymd')
    {
		if(empty($fecha)) {
			return false;
		}		
		$separador_tipo = array("/", "-", ".");		
        foreach ($separador_tipo as $separador) {
			$find = stripos($fecha, $separador);
            if(false !== $find) {
                $separador_usado = $separador;
            }
        }				
        $fecha_array = explode($separador_usado, $fecha);		
        if ($formato == 'mdy') {
            return checkdate($fecha_array[0], $fecha_array[1], $fecha_array[2]);
        } elseif ($formato=="ymd") {
            return checkdate($fecha_array[1], $fecha_array[2], $fecha_array[0]);
        } else {
            return checkdate($fecha_array[1], $fecha_array[0], $fecha_array[2]);
        }
    }	
	public static function cambiarFormatoFecha($fecha, $formato = 'ymd')
    {
		if(empty($fecha)) {
			return false;
		}		
		$separador_tipo = array("/", "-", ".");		
        foreach ($separador_tipo as $separador) {
			$find = stripos($fecha, $separador);
            if(false !== $find) {
                $separador_usado = $separador;
            }
        }				
        $fecha_array = explode($separador_usado, $fecha);		
        if ($formato == 'mdy') {
            return $fecha_array[2] . '-' . $fecha_array[0] . '-' . $fecha_array[1];
        } elseif ($formato == "ymd") {
            return $fecha_array[0] . '-' . $fecha_array[1] . '-' . $fecha_array[2];
        } else {
            return $fecha_array[2] . '-' . $fecha_array[1] . '-' . $fecha_array[0];
        }
    }
	
	public static function cambiarFormatoFechaDeA($fecha, $formato_de = 'ymd', $formato_a = 'y-m-d')
    {
		if(empty($fecha)) {
			return false;
		}		
		if('0000-00-00' == $fecha) {
			return '0000-00-00';
		}		
		$separador_tipo = array("/", "-", ".");		
		if('mdy' == $formato_de) {//[0]y-[1]m-[2]d //formato americano
			$fecha = str_replace($separador_tipo, '/', $fecha);
		} elseif('mdy' == $formato_de) {//[0]d-[1]m-[2]y //formato europeo
			$fecha = str_replace($separador_tipo, '-', $fecha);
		} else {//[0]y-[1]-m-d[2] //iso
			$fecha = str_replace($separador_tipo, '-', $fecha);
		}		
		return date($formato_a, strtotime($fecha));
    }	
	public static function getFormatoFecha($fecha, $formato = 'y-m-d')
    {
		$fecha = new DateTime($fecha);
		return $fecha->format($formato);
    }	
	public static function chekFechaHora($fecha_hora, $solo_fecha = false)
    {
		if (true == $solo_fecha) {
			return (date('Y-m-d', strtotime($fecha_hora)) == $fecha_hora) ? true : false;
		} else {
			return (date('Y-m-d H:i:s', strtotime($fecha_hora)) == $fecha_hora) ? true : false;
		}
    }	
	public static function esHora($hora)
    {
		$patron = "/^([0-1][0-9]|[2][0-3])[\:]([0-5][0-9])[\:]([0-5][0-9])$/";		
		if(preg_match($patron, $hora)) {
			return true;
		}
		return false;
    }	
	public static function checkRangoFecha($f_ini, $f_fin, $evaluar) {
		$f_ini = strtotime($f_ini);
		$f_fin = strtotime($f_fin);
		$evaluar = strtotime($evaluar);		
		return (($evaluar >= $f_ini) && ($evaluar <= $f_fin));
	}	
	public static function diasEntreFechas($fecha_ini, $fecha_fin) {
		$fecha_ini = date('Y-m-d', strtotime($fecha_ini));
		$fecha_fin = date('Y-m-d', strtotime($fecha_fin));		
		$dias = (strtotime($fecha_ini) - strtotime($fecha_fin)) / 86400;
		$dias = abs($dias);
		$dias = floor($dias);
		return $dias;
	}	
	public static function horasEntreFechas($fecha_ini, $fecha_fin) {
		$fecha_ini = new DateTime($fecha_ini);
		$fecha_fin = new DateTime($fecha_fin);
		$diff = $fecha_fin->diff($fecha_ini);		
		$horas = $diff->h + ($diff->i / 60);
		$horas = $horas + ($diff->days * 24);		
		return $horas;
	}	
	public static function codificaurl($cadena, $longmax)
	{
		$url = strtolower($cadena);
		$url = preg_replace("/[^a-z0-9\s-]/", "", $url);
		$url = trim(preg_replace("/[\s-]+/", " ", $url));
		$url = trim(substr($url, 0, $longmax));
		$url = preg_replace("/\s/", "-", $url);		
		return $url;
	}
	
    public static function is_bot(){     
        $bots = array(
            'Googlebot', 'Baiduspider', 'ia_archiver',
            'R6_FeedFetcher', 'NetcraftSurveyAgent', 'Sogou web spider',
            'bingbot', 'Yahoo! Slurp', 'facebookexternalhit', 'PrintfulBot',
            'msnbot', 'Twitterbot', 'UnwindFetchor, Java',
            'urlresolver', 'Butterfly', 'TweetmemeBot', 'Facebot', 'AhrefsBot', 'sitemaps');		
        foreach($bots as $b){
            if(stripos($_SERVER['HTTP_USER_AGENT'], $b ) !== false) return true;     
		}		
        return false;
    }	
	public static function sumarDias($fecha_base, $dias)
	{
		$fecha_base = new DateTime($fecha_base);
		$fecha_base->add(new DateInterval('P'.$dias.'D'));		
		return $fecha_base->format('Y-m-d');
	}	
	public static function codificacion($texto)
	{
		return mb_detect_encoding($texto, 'ASCII,UTF-8,ISO-8859-15');
	}	
	public static function utf8($texto)
	{
		return (self::codificacion($texto) == 'UTF-8') ? utf8_decode($texto) : $texto;
	}	
	public static function getCadUrl($text)
	{	// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// trim
		$text = trim($text, '-');
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		// lowercase
		$text = strtolower($text);		
		if(empty($text)) {
			return 'n-a';
		}		
		return $text;
	}	
	public static function getFechasxNumSemana($semana, $anio)
	{
		//obtenemos el timestamp del primer dia del a�o
		$timestamp = mktime(0, 0, 0, 1, 1, $anio);
		//sumamos el timestamp de la suma de las semanas actuales
		$timestamp += $semana * 7 * 24 * 60 * 60;
		//restamos la posici�n inicial del primer dia del a�o
		$ultimoDia = $timestamp - date("w", mktime(0, 0, 0, 1, 1, $anio))*24*60*60;
		//le restamos los dias que hay hasta llegar al lunes
		$primerDia = $ultimoDia - 86400 * (date('N', $ultimoDia)-1);
		return array('inicio' => date("Y-m-d", $primerDia), 'fin' => date("Y-m-d", $ultimoDia), 'numero' => $semana);
	}	
	public static function date($formato = 'Y-m-d H:i:s', $time_zone = 'America/Lima', $fecha = null, $time_zone_base = 'UTC')
	{
		$formato = empty($formato) ? 'Y-m-d H:i:s' : $formato;
		$time_zone = empty($time_zone) ? 'America/Lima' : $time_zone;
		$fecha = empty($fecha) ? date('Y-m-d H:i:s') : $fecha;
		$time_zone_base = empty($time_zone_base) ? 'UTC' : $time_zone_base;		
		$fecha_actualizada = new DateTime($fecha, new DateTimeZone($time_zone_base));
		$fecha_actualizada->setTimeZone(new DateTimeZone($time_zone));		
		return $fecha_actualizada->format($formato);
	}

	public static function subirImagen($nombre,$img,$ruta,$rename=false,$ancho=100, $alto=100,$ext='png'){
		if(!empty($img)) {
			$dir_fotos = RUTA_BASE . 'static' . SD . 'media' . SD . $ruta . SD;
			$dir_cache = RUTA_BASE . 'sys_cache' . SD;

			if(!is_dir($dir_fotos)) { @mkdir($dir_fotos,0777,true); @chmod($dir_fotos,0777); }
			$data_img = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
            
			if($rename==false) $nom_tmp = $nombre.date('Yhis').'.'.$ext;
			else $nom_tmp=$nombre;
            $solonombre=reset(explode(".", $nom_tmp));
            //sube archivo
			file_put_contents($dir_cache . $nom_tmp, $data_img);
			$ruta_foto = $dir_cache . $nom_tmp;
			if(is_file($ruta_foto)) @chmod($ruta_foto, 0777);
						
			JrCargador::clase('jrAdwen::JrImagen');
			require_once(RUTA_LIBS . 'class_upload' . SD . 'class.upload.php');
			$imagen = new Upload($ruta_foto);
			$imagen->image_convert = $ext;
			JrImagen::crearImagen($imagen, $dir_fotos,$solonombre, $ancho, $alto, false);
			$file=$dir_fotos.$nom_tmp;
			if(is_file($file)){@chmod($file, 0777); @unlink($ruta_foto);}
			$rutaall=SD.'static'.SD.'media'.SD.$ruta.SD.$nom_tmp;
			return  $rutaall; 
		}
	}

	public static function isonline(){
		$ip=$_SERVER['SERVER_ADDR'];
		if($ip==='127.0.0.1') return false;
		else return true;
	}

	public static function conversorSegundosHoras($tiempo_en_segundos) {
		$horas = floor($tiempo_en_segundos / 3600);
		$minutos = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
		$segundos = $tiempo_en_segundos - ($horas * 3600) - ($minutos * 60);
	
		return $horas . ':' . $minutos . ":" . $segundos;
	}

	public static function subirFile($file,$dirmedia,$oldmedia,$typefile,$nombre){
			$dirmedia=!empty($dirmedia)?$dirmedia:'';
            $oldmedia=!empty($oldmedia)?$oldmedia:'';
			$typefile=!empty($typefile)?$typefile:'';
			$nombre=!empty($nombre)?$nombre:'';          
            $newmedia='';
			if(!empty($file['name'])){
				$file=$file;
				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				if($typefile=='html'&&$ext=='html'){
					$newnombre=$file['name'];
				}else $newnombre=(!empty($nombre)?$nombre:(date("Ymdhis").rand(0,100))).'.'.$ext;
				
				$dir=RUTA_BASE.'static'.SD.'media'.SD.str_replace('/', SD, $dirmedia);				
				@mkdir($dir,0777,true);
				@chmod($dir, 0777);
				if(substr($dir,-1)==SD) $dir=substr($dir, 0,-1);
				if(is_file($dir.SD.$newnombre)) @chmod($dir.SD.$newnombre, 0775);				
				if(!is_dir($dir) || !is_writable($dir)) {					
					return array('code'=>'Error','msj'=>'Error de directorio - '.$dir);
				}
				if(move_uploaded_file($file["tmp_name"],$dir.SD.$newnombre)) 
				{	
		  			$rutamedia='static/media/'.$dirmedia.$newnombre;
		  			@chmod($dir.SD.$newnombre, 0775);		  			
		  			$newmedia=URL_BASE.$rutamedia;		  			
		  			if($oldmedia!=''&& $newmedia!=$oldmedia){
		  				$rutaoldmedia=str_replace(URL_BASE,RUTA_BASE,$oldmedia);
		  				if(is_file($rutaoldmedia)) unlink($rutaoldmedia);
		  			}
		  			$staticmediahtml='';
		  			if($ext=='zip'){
		  				$zip = new ZipArchive;
		  				$fileszip = array();	
						if ($zip->open($dir.SD.$newnombre) === TRUE) 
						{
							$next=(strlen('.'.$ext)*-1);
							$nombresinext=substr($newnombre,0,$next);
							$hayfile=false;	
							$validar=true;

							for($i=0; $i<$zip->numFiles; $i++)
						   	{							
								$namefile=$zip->getNameIndex($i); //obtenemos nombre del fichero
								$rutamedia='static/media/'.$dirmedia.$nombresinext."/".$namefile;
								$fileszip['tmp_name'][$i] = $rutamedia; //obtenemos ruta que tendrán los documentos cuando los 
								$fileszip['name'][$i] = $namefile;
								
								$extfile=strtolower(pathinfo($namefile, PATHINFO_EXTENSION));	

								if(($extfile==$typefile && $hayfile==false)||$namefile=='index.html'){
									$hayfile=true;
									$newmedia=URL_BASE.$rutamedia;
									$validar=false;	
									$staticmediahtml=$rutamedia;						
								}
								if($typefile=='html' && ($namefile=='index.html'||$namefile=='default.html')){
									$hayfile=true;
									$newmedia=URL_BASE.$rutamedia;
									$validar=false;	
									$staticmediahtml=$rutamedia;
								}

								if($extfile=="mp3" && $validar && $typefile!='html'){
									$hayfile=true;
									$newmedia=URL_BASE.$dirmedia;							
								}
						   	}
						   	if($hayfile==true){						   		
						   		$zip->extractTo($dir.SD.$nombresinext);
						   		$zip->close();							   		
						   		//unlink($dir. SD.$newnombre);
						   	}else{
						   		$zip->close();
						   		//unlink($dir. SD.$newnombre);
						   		return array('code'=>'Error','msj'=>JrTexto::_('File Zip no valido'));
						   	}
						   	if($typefile=='html'){
						   		$rutamedia=$staticmediahtml;
						   	}
						}
		  			}
		  		}
			}
	
            return array('code'=>200,'msj'=>ucfirst(JrTexto::_('Upload successfully')),'media'=>$newmedia,'oldmedia'=>@$rutaoldmedia,'filesdezip'=>@$fileszip,'rutaStatic'=>@$rutamedia); 
	}  

	public static function enviarcorreo($titulo='',$mensaje='',$destinatario=array()){
		JrCargador::clase('jrAdwen::JrCorreo');
		$oCorreo = new JrCorreo;
		$oCorreo->setAsunto(!empty($titulo)?$titulo:'Correo informativo');		
		$oCorreo->addDestinatario('abelchingo@gmail.com','Abelchingo');
		if(!empty($destinatario))
			foreach ($destinatario as $v) {
				$oCorreo->addDestinatario(@$v['email'],@$v['nombre']);
			}
		$html='<table style="  width: 100% !important;  table-layout: fixed; background: #fff;" align="center">
    <caption><h2 style="text-align: center;">'.(!empty($titulo)?$titulo:'Correo informativo').'</h2></caption>
    <tbody>
        <tr>
            <td>
                <table class="ecxdeviceWidth" style="max-width:600px;" align="center">
                <tbody>
                    <tr><td colspan="3" style="background: #bb5b21; color: #f9f3f3; text-align: center; padding: 2ex;  /* padding: 6ex; */">
                        <br><a href="'.@URL_BASE.'" style="color: #ffe520;"><b>visitanos en Aqui</b></a></td></tr>
                <tr>
                   
                    <td style="padding-bottom:8px;">
                        <table style="color:#353e4a;font-family:Arial,sans-serif;font-size:15px;" align="center" bgcolor="#ffffff" border="0">
                            <tbody>                               
                                <tr>
                                    <td style="line-height:24px;padding-left:20px;padding-right:20px;padding-bottom:10px;">'.$mensaje.'</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr style=" background: #737070;color: #fff;"><td colspan="3">
                    <h2>Necesitas Ayuda?</h2>
                    Escribenos a nuestro correo <b>info@eduktvirtual.com</b><br>
                    Llámanos al <b>+51 1-435-3738</b><br>
                    Contáctanos en Whatsapp al <b>+51 960-177-900</b>
                </td>
                </tr>
                <tr>                    
                    <td colspan="3" style="color: #717175;font-size: 12px;padding: 1ex;text-align: center;background: #ecece9;">
                   Este email se ha generado automáticamente. Por favor, no contestes a este email.</td>                               
                </tr>
            </tbody>
          	</table>
            </td>
        </tr>
    </tbody>
</table>';
		$oCorreo->setMensaje($html);
		$envio=$oCorreo->sendPhpmailer();
		return json_encode(array('code'=>'ok','envio'=>true));
	}

	/**
	 * @autor		Alvaro Alonso Cajusol Vallejos
	 * @fecha		10/01/2020
	 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
	 */

	public static function subirArchivo($files, $directorio, $nombre){
		$dir_subida = RUTA_BASE . 'static' . SD . 'media' . SD . $directorio;
		$array_ = explode('.', $files['name']);
		$nombre_ = $nombre . "." . end($array_);
		$fichero_subido = $dir_subida . SD . $nombre_;
		$dir_ = opendir($dir_subida);
		while ($archivo = readdir($dir_)) {
			if (!is_dir($archivo)) {
				if($archivo == $nombre_){
					unlink($fichero_subido);
				}
			}
		}
		if (move_uploaded_file($files['tmp_name'], $fichero_subido)) {
			return $nombre_; 
		}
		return ""; 
	}

	public static function full_copy($source, $target){
		if(is_dir($source)){
			@mkdir($target);
			$d = dir($source);
			while(FALSE !== ($entry = $d->read())){
				if($entry == '.' || $entry == '..'){
					continue;
				}
				$Entry = $source . '/' . $entry; 
				if(is_dir($Entry)){
					full_copy($Entry, $target . '/' . $entry);
					continue;
				}
				copy($Entry, $target . '/' . $entry);
			}
			$d->close();
		}else {
			copy($source, $target);
		}
	}

	public static function requestHTTP($post_data,$target, $json = true){
		try{
			if (function_exists('curl_init')){				
				$ch = curl_init();
				if(strpos($target,'https') !== false){
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
				}
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL,$target);
				curl_setopt($ch, CURLOPT_POST,1);				
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
				$result=curl_exec($ch);
				curl_close($ch);
				$p = ($json==true) ? json_decode($result,true) : $result ;
				if($p['code'] != 200){
					print_r($result);
					exit();
					throw new Exception($result);
				}
				return $p;
			}else{
				$postdata = http_build_query($post_data);				
				$options=array(
					'http' =>array(
				        'method'  => 'POST',
				        'header'  => 'Content-Type: application/x-www-form-urlencoded',
				        'content' => $postdata
				    ),
				    'ssl'=>array("verify_peer"=>false, "verify_peer_name"=>false,)
				);
				$context= stream_context_create($options);
				$result = file_get_contents($target, false, $context);
				$p = ($json==true) ? json_decode($result,true) : $result ;
				if($p['code'] != 200){
					print_r($result);
					exit();
					throw new Exception($result);
				}
				return $p;
			}
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public static function array2json($data){
		$contenido = array();
		foreach ($data as $key => $valor) {
			$contenido_ = array();
			foreach ($valor as $k => $v) {
				$contenido_[] = '"'.$k.'":"'.$v.'"';
			}
			$contenido[] = $contenido_;
		}
		$contenido_ = array();
		foreach ($contenido as $key => $value) {
			$contenido_[] = '{' . implode(",", $value) . '}';
		}
		return '[' . implode(",", $contenido_) . ']';
	}
	
	public static function SmartFormatoMultiple($info,$usuarios,$key,$prefijo,$is_prefijo){
		try{
			$user = array();
			$matricula = array();
			$roles = array(3);
			$empresa = array('idempresa'=>4,'idproyecto'=>3);
			$phone = '';
			
			if(isset($info['roles']) && !empty($info['roles'])){
				$roles = $info['roles'];
			}
			if(isset($info['empresaproyecto']) &&  !empty($info['empresaproyecto'])){
				$empresa = $info['empresaproyecto'];
			}
			$_usuarios = array();
			if(!empty($usuarios)){
				foreach($usuarios as $value){
					if(isset($value['telefono']) && !empty($value['telefono'])){
						$phone = $value['telefono'];
					}else if(isset($value['celular']) && !empty($value['celular'])){
						$phone = $value['celular'];
					}
					$_fecha = date("Y-m-d H:i:s");
					$user = array(
						'idpersonasks' => $value['idpersona'],
						'idpersona' => $value['idusuario_externo'],
						'tipodoc'=> $value['tipodoc'],
						'dni' => $value['documento'],
						'primer_ape'=>$value['apellido1'],
						'segundo_ape'=>$value['apellido2'],
						'nombre'=>$value['nombres'],
						'telefono' => $phone,
						'email' => $value['email'],
						'usuario' => $value['usuario'],
						'clave' => md5($value['usuario']),
						'clavesinmd5' => $value['usuario'],
						"sexo" => $value["sexo"],
						"fechanac" => $value["fechanac"],
						"edad_registro" => $value["edad_registro"],
						"instruccion" => $value["instruccion"],
						'esdemo' => ((isset($value['esdemo']) && !empty(isset($value['esdemo']))) ? intval($value['esdemo']) : 0)
					);
					$personal = array(
						'idpersonasks'   => $value['idpersona']
						,'idpersona'   => $value['idusuario_externo']
						,'tipodoc'   => $value['tipodoc']
						,'dni'      => $value['documento']
						,'ape_paterno' => $value['apellido1']
						,'ape_materno' => $value['apellido2']
						,'nombre' => $value['nombres']
						,'fechanac' => $value['fechanac']
						,'sexo' => $value['sexo']
						,'estado_civil' => 'S'
						,'ubigeo' => ' '
						,'urbanizacion' => ' '
						,'direccion' => ' '
						,'telefono' => $phone
						,'celular' => $phone
						,'email' => $value['email']
						,'regusuario' => '1'
						,'regfecha' => $_fecha
						,'usuario' => $value['usuario']
						,'clave' => md5($value['usuario'])
						,'clavesinmd5' => $value['usuario']
						,'token' => md5($value['usuario'])
						,'rol' => 3
						,'foto' => ''
						,'estado' => 1
						,'situacion' => '1'
						,'idioma' => 'ES'
						,'tipousuario' => 'n'
						,'edad_registro' => @$value['edad_registro']
						,'instruccion' => $value['instruccion']
						,'esdemo' => isset($value['esdemo']) && !empty($value['esdemo']) ? intval($value['esdemo']) : 0
					);
					if(!empty($value['idlocal'])){ $personal['idlocal'] = $value['idlocal']; }
					if(!empty($value['idugel'])){ $personal['idugel'] = $value['idugel']; }
					$matriculasks = null;
					$matricula = null;
					if(!isset($info['esdocente']) || empty($info['esdocente'])){
						if(isset($info['idgrupoauladetalle']) && !empty($info['idgrupoauladetalle'])){
							if(!is_array($info['idgrupoauladetalle'])){
								$info['idgrupoauladetalle'] = array($info['idgrupoauladetalle']);
							}
							foreach($info['idgrupoauladetalle'] as $grupoauladetalle){
								$matriculasks[] = array(
									'idgrupoaula'=> $info['idgrupoaula'],
									'idgrupoauladetalle'=> $grupoauladetalle,
									'idalumno' => null,
									'fecha_registro' => $_fecha,
									'idusuario' => null,
									'fecha_matricula'=> $info['fecha_inicio'],
									'fecha_vencimiento'=> $info['fecha_vencimiento'],
									'tipomatricula'=>$info['tipomatricula'],
									'pagorecurso'=>$info['pagorecurso']
								);
								$arrPrepareMatricula = [
									'idgrupoaula' => $info['idgrupoaula']
									,'idgrupoauladetalle' => $grupoauladetalle
									,'idalumno' => null
									,'fecha_registro' => $_fecha
									,'idusuario' => null
									,'fecha_matricula' => $info['fecha_inicio']
									,'fecha_vencimiento' => $info['fecha_vencimiento']
									,'tipomatricula' => $info['tipomatricula']
									,'pagorecurso' => (!empty($info['pagorecurso']) ? $info['pagorecurso'] : 0)
									,'idcomplementario' => 0
									,'idcategoria' => 0
									,'estado' => !empty($info["estado_matricula"]) ? $info["estado_matricula"] : 1
								];
								
								if(!empty($value['codigocursosence'])){ $arrPrepareMatricula['codigocursosence'] = $value['codigocursosence']; }

								$matricula[] = $arrPrepareMatricula;
							}
						}//end if isset
					}//end if esdocente
					$personal_rol = array();
					foreach ($roles as $rolesvl) {
						$personal_rol[] = array(
							'idrol'=>$rolesvl
							,'idpersonal'=>null
							,'idproyecto'=>$empresa['idproyecto']
							,'idempresa'=>$empresa['idempresa']
						);
					}
					$_usuarios[] = array(
						'usuario'=>$user,
						'matriculasks'=>$matriculasks,
						'personal'=>$personal,
						'personal_rol'=>$personal_rol,
						'matricula'=>$matricula,
						'idcurso'=>$info['idcurso']
					);
				}//end foreach
			}//end if !empty
			
			return array(
				'usuario'=>$_usuarios,
				'roles' => $roles,
				'empresa'=>$empresa,
				'key'=>$key,
				'is_prefijo'=>$is_prefijo,
				'prefijo'=>$prefijo
			);
		}catch(Exception $e){
			throw new Exception("Hay un error");
		}
	}

	public static function SmartFormatoMultipleDocente($info,$usuarios,$key,$prefijo,$is_prefijo){
		try{
			$user = array();
			$matricula = array();
			$roles = array(3);
			$empresa = array('idempresa'=>4,'idproyecto'=>3);
			$phone = '';
			
			if(isset($info['roles']) && !empty($info['roles'])){
				$roles = $info['roles'];
			}
			if(isset($info['empresaproyecto']) &&  !empty($info['empresaproyecto'])){
				$empresa = $info['empresaproyecto'];
			}
			$_usuarios = array();
			if(!empty($usuarios)){
				foreach($usuarios as $value){
					if(isset($value['telefono']) && !empty($value['telefono'])){
						$phone = $value['telefono'];
					}else if(isset($value['celular']) && !empty($value['celular'])){
						$phone = $value['celular'];
					}
					$_fecha = date("Y-m-d H:i:s");
					$user = array(
						'idpersonasks' => $value['idpersona'],
						'idpersona' => $value['idusuario_externo'],
						'tipodoc'=> $value['tipodoc'],
						'dni' => $value['documento'],
						'primer_ape'=>$value['apellido1'],
						'segundo_ape'=>$value['apellido2'],
						'nombre'=>$value['nombres'],
						'telefono' => $phone,
						'email' => $value['email'],
						'usuario' => $value['usuario'],
						'clave' => md5($value['usuario']),
						'clavesinmd5' => $value['usuario'],
						"sexo" => $value["sexo"],
						"fechanac" => $value["fechanac"],
						// "edad_registro" => $value["edad_registro"],
						"instruccion" => $value["instruccion"],
						'esdemo' => ((isset($value['esdemo']) && !empty(isset($value['esdemo']))) ? intval($value['esdemo']) : 0)
					);
					$personal = array(
						'idpersonasks'   => $value['idpersona']
						,'idpersona'   => $value['idusuario_externo']
						,'tipodoc'   => $value['tipodoc']
						,'dni'      => $value['documento']
						,'ape_paterno' => $value['apellido1']
						,'ape_materno' => $value['apellido2']
						,'nombre' => $value['nombres']
						,'fechanac' => $value['fechanac']
						,'sexo' => $value['sexo']
						,'estado_civil' => 'S'
						,'ubigeo' => ' '
						,'urbanizacion' => ' '
						,'direccion' => ' '
						,'telefono' => $phone
						,'celular' => $phone
						,'email' => $value['email']
						,'regusuario' => '1'
						,'regfecha' => $_fecha
						,'usuario' => $value['usuario']
						,'clave' => md5($value['usuario'])
						,'clavesinmd5' => $value['usuario']
						,'token' => md5($value['usuario'])
						,'rol' => 3
						,'foto' => ''
						,'estado' => 1
						,'situacion' => '1'
						,'idioma' => 'ES'
						,'tipousuario' => 'n'
						// ,'edad_registro' => @$value['edad_registro']
						,'instruccion' => $value['instruccion']
						,'esdemo' => isset($value['esdemo']) && !empty($value['esdemo']) ? intval($value['esdemo']) : 0
					);
					
					if(!empty($value['idlocal'])){ $personal['idlocal'] = $value['idlocal']; }
					if(!empty($value['idugel'])){ $personal['idugel'] = $value['idugel']; }

					$matriculasks = null;
					$matricula = null;
					$personal_rol = array();
					foreach ($roles as $rolesvl) {
						$personal_rol[] = array(
							'idrol'=>$rolesvl
							,'idpersonal'=>null
							,'idproyecto'=>$empresa['idproyecto']
							,'idempresa'=>$empresa['idempresa']
						);
					}
					$_usuarios[] = array(
						'usuario'=>$user,
						'matriculasks'=>$matriculasks,
						'personal'=>$personal,
						'personal_rol'=>$personal_rol,
						'matricula'=>$matricula
						// 'idcurso'=>$info['idcurso']
					);
					
				}//end foreach
			}//end if !empty
			
			return array(
				'usuario'=>$_usuarios,
				'roles' => $roles,
				'empresa'=>$empresa,
				'key'=>$key,
				'is_prefijo'=>$is_prefijo,
				'prefijo'=>$prefijo
			);
		}catch(Exception $e){
			throw new Exception("Hay un error");
		}
	}

	public static function _empty($filtros = array()){
		$resultado = array();
		foreach($filtros as $i => $val){
			if(!empty($val)){
				$resultado[$i] = $val;
			}
		}
		return $resultado;
	}

}