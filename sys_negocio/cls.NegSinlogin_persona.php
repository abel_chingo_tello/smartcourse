<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		15-01-2021
 * @copyright	Copyright (C) 15-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatSinlogin_persona', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegSinlogin_persona 
{
	protected $idpersona;
	protected $nombres;
	protected $telefono;
	protected $correo;
	protected $idproyecto;
	protected $dataSinlogin_persona;
	protected $oDatSinlogin_persona;	
	protected $pais;
	protected $compania;
	protected $ciudad;

	public function __construct()
	{
		$this->oDatSinlogin_persona = new DatSinlogin_persona;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatSinlogin_persona->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatSinlogin_persona->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatSinlogin_persona->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatSinlogin_persona->get($this->idpersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_persona', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatSinlogin_persona->iniciarTransaccion('neg_i_Sinlogin_persona');
			$this->idpersona = $this->oDatSinlogin_persona->insertar($this->nombres,$this->telefono,$this->correo,$this->idproyecto,$this->pais,$this->compania,$this->ciudad);
			$this->oDatSinlogin_persona->terminarTransaccion('neg_i_Sinlogin_persona');	
			return $this->idpersona;
		} catch(Exception $e) {	
		    $this->oDatSinlogin_persona->cancelarTransaccion('neg_i_Sinlogin_persona');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_persona', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatSinlogin_persona->actualizar($this->idpersona,$this->nombres,$this->telefono,$this->correo,$this->idproyecto,$this->pais,$this->compania,$this->ciudad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Sinlogin_persona', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatSinlogin_persona->eliminar($this->idpersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpersona($pk){
		try {
			$this->dataSinlogin_persona = $this->oDatSinlogin_persona->get($pk);
			if(empty($this->dataSinlogin_persona)) {
				throw new Exception(JrTexto::_("Sinlogin_persona").' '.JrTexto::_("not registered"));
			}
			$this->idpersona = $this->dataSinlogin_persona["idpersona"];
			$this->nombres = $this->dataSinlogin_persona["nombres"];
			$this->telefono = $this->dataSinlogin_persona["telefono"];
			$this->correo = $this->dataSinlogin_persona["correo"];
			$this->idproyecto = $this->dataSinlogin_persona["idproyecto"];
			$this->pais = $this->dataSinlogin_persona["pais"];
			$this->compania = $this->dataSinlogin_persona["compania"];
			$this->ciudad = $this->dataSinlogin_persona["ciudad"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_persona', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataSinlogin_persona = $this->oDatSinlogin_persona->get($pk);
			if(empty($this->dataSinlogin_persona)) {
				throw new Exception(JrTexto::_("Sinlogin_persona").' '.JrTexto::_("not registered"));
			}

			return $this->oDatSinlogin_persona->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}