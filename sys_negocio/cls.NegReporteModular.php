<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotas_quiz', RUTA_BASE, 'sys_datos');

class NegReporteModular
{
	protected $oDatNotas_quiz;

	public function __construct()
	{
		$this->oDatNotas_quiz = new DatNotas_quiz;
	}
	/**
	 * Funcion que trae las habilidades de los examenes, funcion extraido en webreportemodular.php
	 * @param Array parametros para busqueda de la base de datos
	 * @return Array Resultado de las habilidades de los examenes
	 */
	public function getProgresoHabilidadExamen($params)
	{
		try {
			$arrHabilidadesTodas = [];

			if (empty($params)) {
				throw new Exception("params esta vacio se requiere parametros");
			}
			if (empty($params['idexamen'])) {
				throw new Exception("idexamen esta vacio se requiere este valor");
			}
			if (empty($params['idalumno'])) {
				throw new Exception("idalumno esta vacio se requiere este valor");
			}

			$arrExamenesHabilidades = $this->oDatNotas_quiz->getHabilidadesPorExamen($params);
			#echo json_encode($arrExamenesHabilidades);exit();

			foreach ($arrExamenesHabilidades as $i => $habilidadExamen) {
				if (!empty($habilidadExamen["habilidades_todas"])) {#puede que esté vacío y no sea un  json
					$habilidades_todas = json_decode($habilidadExamen["habilidades_todas"], true);
					$arrHabilidadesTodas = array_merge($arrHabilidadesTodas, $habilidades_todas);
					$arrHabilidadesTodas = array_unique($arrHabilidadesTodas, SORT_REGULAR);
				}
			}
			#hasta aquí tengo las habilidades unicas de los examenes (sin puntaje)

			foreach ($arrExamenesHabilidades as $i => $habilidadExamen) { #cada examen
				$arrHabilidad_puntaje = json_decode($habilidadExamen["habilidad_puntaje"], true);
				/*
				if ($habilidadExamen["nota"]==null){... no dió el exámen}
				$arrHabilidades_todas= json_decode($habilidadExamen["habilidades_todas"], true);
				if (count($arrHabilidades_todas)>0) {
					# el examen sí tiene habilidades...
				}else{
					# el examen no tiene habilidades...
				}
				*/
				if (count($arrHabilidad_puntaje) > 0) { //sí dió el examen (?)
					#busco el skill_id y le asigno el puntaje
					foreach ($arrHabilidad_puntaje as $key => $habilidadPuntaje) {
						$index = array_search($key, array_column($arrHabilidadesTodas, 'skill_id'));
						if (!isset($arrHabilidadesTodas[$index]["progreso"])) {
							$arrHabilidadesTodas[$index]["progreso"] = 0;
							$arrHabilidadesTodas[$index]["cantidad"] = 0;
						}
						#((float)$progreso * $cantidad + (float)$habilidadConcat["progreso"]) / ($cantidad + 1);
						$habilidad = $arrHabilidadesTodas[$index];
						$arrHabilidadesTodas[$index]["progreso"] = round((float)((float)$habilidad["progreso"] * $habilidad["cantidad"] + (float)$habilidadPuntaje) / ($habilidad["cantidad"] + 1), 2);
						$arrHabilidadesTodas[$index]["cantidad"]++;
					}
				} else { //no dió el examen ó el exámen no tiene habilidades
					/**
					 * 
					 */
					#/*
					#sumamos cero y promediamos a cada habilidad:
					foreach ($arrHabilidadesTodas as $key => $habilidad) {
						if (!isset($arrHabilidadesTodas[$key]["progreso"])) {
							$arrHabilidadesTodas[$key]["progreso"] = 0;
							$arrHabilidadesTodas[$key]["cantidad"] = 0;
						}
						$arrHabilidadesTodas[$key]["progreso"] = round((float)((float)$arrHabilidadesTodas[$key]["progreso"] * $arrHabilidadesTodas[$key]["cantidad"] + 0) / ($arrHabilidadesTodas[$key]["cantidad"] + 1), 2);
						$arrHabilidadesTodas[$key]["cantidad"]++;
					}
					#*/
				}
			}

			return $arrHabilidadesTodas;
		} catch (Exception $e) {
			throw new Exception("Un error en getProgresoHabilidadExamen " . $e->getMessage());
		}
	}
}
