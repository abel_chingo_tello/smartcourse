<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_sesionhtml', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_sesionhtml 
{
	protected $idsesion;
	protected $idcurso;
	protected $idpersonal;
	protected $idcursodetalle;
	protected $html;
	
	protected $dataAcad_sesionhtml;
	protected $oDatAcad_sesionhtml;	

	public function __construct()
	{
		$this->oDatAcad_sesionhtml = new DatAcad_sesionhtml;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_sesionhtml->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_sesionhtml->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_sesionhtml->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_sesionhtml->get($this->idsesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_sesionhtml', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_sesionhtml->iniciarTransaccion('neg_i_Acad_sesionhtml');
			$this->idsesion = $this->oDatAcad_sesionhtml->insertar($this->idcurso,$this->idpersonal,$this->idcursodetalle,$this->html);
			$this->oDatAcad_sesionhtml->terminarTransaccion('neg_i_Acad_sesionhtml');	
			return $this->idsesion;
		} catch(Exception $e) {	
		    $this->oDatAcad_sesionhtml->cancelarTransaccion('neg_i_Acad_sesionhtml');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_sesionhtml', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_sesionhtml->actualizar($this->idsesion,$this->idcurso,$this->idpersonal,$this->idcursodetalle,$this->html);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_sesionhtml', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_sesionhtml->eliminar($this->idsesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdsesion($pk){
		try {
			$this->dataAcad_sesionhtml = $this->oDatAcad_sesionhtml->get($pk);
			if(empty($this->dataAcad_sesionhtml)) {
				throw new Exception(JrTexto::_("Acad_sesionhtml").' '.JrTexto::_("not registered"));
			}
			$this->idsesion = $this->dataAcad_sesionhtml["idsesion"];
			$this->idcurso = $this->dataAcad_sesionhtml["idcurso"];
			$this->idpersonal = $this->dataAcad_sesionhtml["idpersonal"];
			$this->idcursodetalle = $this->dataAcad_sesionhtml["idcursodetalle"];
			$this->html = $this->dataAcad_sesionhtml["html"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_sesionhtml', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_sesionhtml = $this->oDatAcad_sesionhtml->get($pk);
			if(empty($this->dataAcad_sesionhtml)) {
				throw new Exception(JrTexto::_("Acad_sesionhtml").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_sesionhtml->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}