<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-02-2020
 * @copyright	Copyright (C) 11-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTareas_archivosalumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegTareas_archivosalumno 
{
	protected $idtarearecurso;
	protected $idtarea;
	protected $media;
	protected $tipo;
	protected $fecha_registro;
	protected $idalumno;
	
	protected $dataTareas_archivosalumno;
	protected $oDatTareas_archivosalumno;	

	public function __construct()
	{
		$this->oDatTareas_archivosalumno = new DatTareas_archivosalumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTareas_archivosalumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTareas_archivosalumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTareas_archivosalumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTareas_archivosalumno->get($this->idtarearecurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tareas_archivosalumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatTareas_archivosalumno->iniciarTransaccion('neg_i_Tareas_archivosalumno');
			$this->idtarearecurso = $this->oDatTareas_archivosalumno->insertar($this->idtarea,$this->media,$this->tipo,$this->idalumno);
			$this->oDatTareas_archivosalumno->terminarTransaccion('neg_i_Tareas_archivosalumno');	
			return $this->idtarearecurso;
		} catch(Exception $e) {	
		    $this->oDatTareas_archivosalumno->cancelarTransaccion('neg_i_Tareas_archivosalumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tareas_archivosalumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatTareas_archivosalumno->actualizar($this->idtarearecurso,$this->idtarea,$this->media,$this->tipo,$this->idalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Tareas_archivosalumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTareas_archivosalumno->eliminar($this->idtarearecurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtarearecurso($pk){
		try {
			$this->dataTareas_archivosalumno = $this->oDatTareas_archivosalumno->get($pk);
			if(empty($this->dataTareas_archivosalumno)) {
				throw new Exception(JrTexto::_("Tareas_archivosalumno").' '.JrTexto::_("not registered"));
			}
			$this->idtarearecurso = $this->dataTareas_archivosalumno["idtarearecurso"];
			$this->idtarea = $this->dataTareas_archivosalumno["idtarea"];
			$this->media = $this->dataTareas_archivosalumno["media"];
			$this->tipo = $this->dataTareas_archivosalumno["tipo"];
			$this->fecha_registro = $this->dataTareas_archivosalumno["fecha_registro"];
			$this->idalumno = $this->dataTareas_archivosalumno["idalumno"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('tareas_archivosalumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTareas_archivosalumno = $this->oDatTareas_archivosalumno->get($pk);
			if(empty($this->dataTareas_archivosalumno)) {
				throw new Exception(JrTexto::_("Tareas_archivosalumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTareas_archivosalumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}