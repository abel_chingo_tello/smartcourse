<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-02-2021
 * @copyright	Copyright (C) 17-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotificacion_de', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegNotificacion_de 
{
	protected $idnotificacion;
	protected $idpersona;
	protected $tipo;
	protected $idrecurso;
	protected $texto;
	protected $idgrupoauladetalle;
	
	protected $dataNotificacion_de;
	protected $oDatNotificacion_de;	

	public function __construct()
	{
		$this->oDatNotificacion_de = new DatNotificacion_de;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNotificacion_de->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotificacion_de->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNotificacion_de->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotificacion_de->get($this->idnotificacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notificacion_de', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatNotificacion_de->iniciarTransaccion('neg_i_Notificacion_de');
			$this->idnotificacion = $this->oDatNotificacion_de->insertar($this->idpersona,$this->tipo,$this->idrecurso,$this->texto,$this->idgrupoauladetalle);
			$this->oDatNotificacion_de->terminarTransaccion('neg_i_Notificacion_de');	
			return $this->idnotificacion;
		} catch(Exception $e) {	
		    $this->oDatNotificacion_de->cancelarTransaccion('neg_i_Notificacion_de');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notificacion_de', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotificacion_de->actualizar($this->idnotificacion,$this->idpersona,$this->tipo,$this->idrecurso,$this->texto,$this->idgrupoauladetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function actualizar($estados,$condicion)
	{
		try {
			/*if(!NegSesion::tiene_acceso('notificacion_de', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotificacion_de->actualizar2($estados,$condicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatNotificacion_de->cambiarvalorcampo($this->idnotificacion,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notificacion_de', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotificacion_de->eliminar($this->idnotificacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnotificacion($pk){
		try {
			$this->dataNotificacion_de = $this->oDatNotificacion_de->get($pk);
			if(empty($this->dataNotificacion_de)) {
				throw new Exception(JrTexto::_("Notificacion_de").' '.JrTexto::_("not registered"));
			}
			$this->idnotificacion = $this->dataNotificacion_de["idnotificacion"];
			$this->idpersona = $this->dataNotificacion_de["idpersona"];
			$this->tipo = $this->dataNotificacion_de["tipo"];
			$this->idrecurso = $this->dataNotificacion_de["idrecurso"];
			$this->texto = $this->dataNotificacion_de["texto"];
			$this->idgrupoauladetalle=$this->dataNotificacion_de["idgrupoauladetalle"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('notificacion_de', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotificacion_de = $this->oDatNotificacion_de->get($pk);
			if(empty($this->dataNotificacion_de)) {
				throw new Exception(JrTexto::_("Notificacion_de").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNotificacion_de->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function notificar($info){
		try{
			//var_dump($info);
			$user=NegSesion::getUsuario();
			$iduser=$user["idpersona"];
			$idrol=$user["idrol"];
			$idrepresentante=$user["idrepresentante"];
			$tipo=$info["tipo"];
			$para=array();
			if($idrol==3){//desde un alumno
				if(empty($info["iddocente"])){
					JrCargador::clase('sys_datos::DatAcad_grupoauladetalle', RUTA_BASE);
					$oGrupoauladetalle=new DatAcad_grupoauladetalle;
					$grupodetalle=$oGrupoauladetalle->buscar(array('idgrupoauladetalle'=>$info["idgrupoauladetalle"],'sql4'=>true,'orderby'=>'no'));
					$iddocente='';
					if(!empty($grupodetalle[0])){
						$grupodetalle=$grupodetalle[0];
						$iddocente=$grupodetalle["iddocente"];
					}
				}else{
					$iddocente=$info["iddocente"];
				}

				if($tipo=='E'){
					if(!empty($iddocente)){ if(!in_array($iddocente, $para)) array_push($para, $iddocente); }					
					if(!empty($idrepresentante)){ if(!in_array($idrepresentante, $para))  array_push($para, $idrepresentante); }
					if(!empty($iduser)){if(!in_array($iduser, $para))  array_push($para, $iduser); }
					$texto=$info["nota"];
					$tipo=5;
				}
				if(!empty($para))
					$idnotificacion = $this->oDatNotificacion_de->insertar2($iduser,$tipo,$info["idrecurso"],$texto,$info["idgrupoauladetalle"],$para);

			}

			return true;
			//var_dump($user);
			//exit(0);
			//return $this->oDatNotificacion_de->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}		
	}
}