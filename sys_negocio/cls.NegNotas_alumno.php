<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotas_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegNotas_alumno 
{
	protected $idalumno;
	protected $nombres;
	protected $apellidos;
	protected $identificador;
	protected $idarchivo;
	protected $origen;
	protected $fechareg;
	
	protected $dataNotas_alumno;
	protected $oDatNotas_alumno;	

	public function __construct()
	{
		$this->oDatNotas_alumno = new DatNotas_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNotas_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotas_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNotas_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotas_alumno->get($this->idalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatNotas_alumno->iniciarTransaccion('neg_i_Notas_alumno');
			$this->idalumno = $this->oDatNotas_alumno->insertar($this->nombres,$this->apellidos,$this->identificador,$this->idarchivo,$this->origen,$this->fechareg);
			$this->oDatNotas_alumno->terminarTransaccion('neg_i_Notas_alumno');	
			return $this->idalumno;
		} catch(Exception $e) {	
		    $this->oDatNotas_alumno->cancelarTransaccion('neg_i_Notas_alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotas_alumno->actualizar($this->idalumno,$this->nombres,$this->apellidos,$this->identificador,$this->idarchivo,$this->origen,$this->fechareg);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas_alumno->eliminar($this->idalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdalumno($pk){
		try {
			$this->dataNotas_alumno = $this->oDatNotas_alumno->get($pk);
			if(empty($this->dataNotas_alumno)) {
				throw new Exception(JrTexto::_("Notas_alumno").' '.JrTexto::_("not registered"));
			}
			$this->idalumno = $this->dataNotas_alumno["idalumno"];
			$this->nombres = $this->dataNotas_alumno["nombres"];
			$this->apellidos = $this->dataNotas_alumno["apellidos"];
			$this->identificador = $this->dataNotas_alumno["identificador"];
			$this->idarchivo = $this->dataNotas_alumno["idarchivo"];
			$this->origen = $this->dataNotas_alumno["origen"];
			$this->fechareg = $this->dataNotas_alumno["fechareg"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('notas_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotas_alumno = $this->oDatNotas_alumno->get($pk);
			if(empty($this->dataNotas_alumno)) {
				throw new Exception(JrTexto::_("Notas_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNotas_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}