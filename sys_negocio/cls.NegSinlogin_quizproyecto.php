<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-01-2021
 * @copyright	Copyright (C) 19-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatSinlogin_quizproyecto', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegSinlogin_quizproyecto 
{
	protected $idasignacion;
	protected $idquiz;
	protected $idproyecto;
	protected $user_registro;
	protected $fecha_hasta;
	protected $estado;
	
	protected $dataSinlogin_quizproyecto;
	protected $oDatSinlogin_quizproyecto;	

	public function __construct()
	{
		$this->oDatSinlogin_quizproyecto = new DatSinlogin_quizproyecto;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatSinlogin_quizproyecto->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatSinlogin_quizproyecto->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatSinlogin_quizproyecto->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatSinlogin_quizproyecto->get($this->idasignacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_quizproyecto', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatSinlogin_quizproyecto->iniciarTransaccion('neg_i_Sinlogin_quizproyecto');
			$this->idasignacion = $this->oDatSinlogin_quizproyecto->insertar($this->idquiz,$this->idproyecto,$this->user_registro,$this->fecha_hasta,$this->estado);
			$this->oDatSinlogin_quizproyecto->terminarTransaccion('neg_i_Sinlogin_quizproyecto');	
			return $this->idasignacion;
		} catch(Exception $e) {	
		    $this->oDatSinlogin_quizproyecto->cancelarTransaccion('neg_i_Sinlogin_quizproyecto');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_quizproyecto', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatSinlogin_quizproyecto->actualizar($this->idasignacion,$this->idquiz,$this->idproyecto,$this->user_registro,$this->fecha_hasta,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatSinlogin_quizproyecto->cambiarvalorcampo($this->idasignacion,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Sinlogin_quizproyecto', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatSinlogin_quizproyecto->eliminar($this->idasignacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdasignacion($pk){
		try {
			$this->dataSinlogin_quizproyecto = $this->oDatSinlogin_quizproyecto->get($pk);
			if(empty($this->dataSinlogin_quizproyecto)) {
				throw new Exception(JrTexto::_("Sinlogin_quizproyecto").' '.JrTexto::_("not registered"));
			}
			$this->idasignacion = $this->dataSinlogin_quizproyecto["idasignacion"];
			$this->idquiz = $this->dataSinlogin_quizproyecto["idquiz"];
			$this->idproyecto = $this->dataSinlogin_quizproyecto["idproyecto"];
			$this->user_registro = $this->dataSinlogin_quizproyecto["user_registro"];
			$this->fecha_hasta = $this->dataSinlogin_quizproyecto["fecha_hasta"];
			$this->estado = $this->dataSinlogin_quizproyecto["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('sinlogin_quizproyecto', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataSinlogin_quizproyecto = $this->oDatSinlogin_quizproyecto->get($pk);
			if(empty($this->dataSinlogin_quizproyecto)) {
				throw new Exception(JrTexto::_("Sinlogin_quizproyecto").' '.JrTexto::_("not registered"));
			}

			return $this->oDatSinlogin_quizproyecto->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}