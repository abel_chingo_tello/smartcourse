<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-07-2017
 * @copyright	Copyright (C) 19-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatUbigeo', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegUbigeo 
{
	protected $id_ubigeo;
	protected $pais;
	protected $departamento;
	protected $provincia;
	protected $distrito;
	protected $ciudad;
	
	protected $dataUbigeo;
	protected $oDatUbigeo;	

	public function __construct()
	{
		$this->oDatUbigeo = new DatUbigeo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatUbigeo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatUbigeo->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}



	public function buscar($filtros = array())
	{
		try {
			return $this->oDatUbigeo->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getciudad($filtros=null){
		try{
			return $this->getciudad($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatUbigeo->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatUbigeo->get($this->id_ubigeo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {			
			$this->oDatUbigeo->iniciarTransaccion('neg_i_Ubigeo');
			$this->id_ubigeo = $this->oDatUbigeo->insertar($this->pais,$this->departamento,$this->provincia,$this->distrito,$this->ciudad);
			$this->oDatUbigeo->terminarTransaccion('neg_i_Ubigeo');	
			return $this->id_ubigeo;
		} catch(Exception $e) {	
		    $this->oDatUbigeo->cancelarTransaccion('neg_i_Ubigeo');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {						
			return $this->oDatUbigeo->actualizar($this->id_ubigeo,$this->pais,$this->departamento,$this->provincia,$this->distrito,$this->ciudad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {			
			return $this->oDatUbigeo->eliminar($this->id_ubigeo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_ubigeo($pk){
		try {
			$this->dataUbigeo = $this->oDatUbigeo->get($pk);
			if(empty($this->dataUbigeo)) {
				throw new Exception(JrTexto::_("Ubigeo").' '.JrTexto::_("not registered"));
			}
			$this->id_ubigeo = $this->dataUbigeo["id_ubigeo"];
			$this->pais = $this->dataUbigeo["pais"];
			$this->departamento = $this->dataUbigeo["departamento"];
			$this->provincia = $this->dataUbigeo["provincia"];
			$this->distrito = $this->dataUbigeo["distrito"];
			$this->ciudad = $this->dataUbigeo["ciudad"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {			
			$this->dataUbigeo = $this->oDatUbigeo->get($pk);
			if(empty($this->dataUbigeo)) {
				throw new Exception(JrTexto::_("Ubigeo").' '.JrTexto::_("not registered"));
			}
			return $this->oDatUbigeo->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}