<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		30-04-2020
 * @copyright	Copyright (C) 30-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatProyecto', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegProyecto 
{
	protected $idproyecto;
	protected $idempresa;
	protected $jsonlogin;
	protected $fecha;
	protected $idioma;
	protected $nombre='';
	protected $tipo_empresa=1;
	protected $nombreurl='';
	protected $logo='';
	protected $tipo_portal=2;
	protected $correo_servidor;
	protected $correo_puerto;
	protected $correo_email;
	protected $correo_clave;
	
	protected $dataProyecto;
	protected $oDatProyecto;	

	public function __construct()
	{
		$this->oDatProyecto = new DatProyecto;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatProyecto->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatProyecto->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatProyecto->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function identificar($filtros)
	{
		try {
			return $this->oDatProyecto->identificar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatProyecto->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function getXid()
	{
		try {
			return $this->oDatProyecto->get($this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatProyecto->iniciarTransaccion('neg_i_Proyecto');
			$this->idproyecto = $this->oDatProyecto->insertar($this->idempresa,$this->jsonlogin,$this->fecha,$this->idioma,$this->nombre,$this->tipo_empresa,$this->nombreurl,$this->logo,$this->tipo_portal);
			$this->oDatProyecto->terminarTransaccion('neg_i_Proyecto');	
			return $this->idproyecto;
		} catch(Exception $e) {	
		    $this->oDatProyecto->cancelarTransaccion('neg_i_Proyecto');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatProyecto->actualizar($this->idproyecto,$this->idempresa,$this->jsonlogin,$this->fecha,$this->idioma,$this->nombre,$this->tipo_empresa,$this->nombreurl,$this->logo,$this->tipo_portal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function actulizarcampos($id,$campos){
		try{			
			return $this->oDatProyecto->actulizarcampos($id,$campos);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Proyecto', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatProyecto->eliminar($this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdproyecto($pk){
		try {
			$this->dataProyecto = $this->oDatProyecto->get($pk);
			if(empty($this->dataProyecto)) {
				throw new Exception(JrTexto::_("Proyecto").' '.JrTexto::_("not registered"));
			}
			$this->idproyecto = $this->dataProyecto["idproyecto"];
			$this->idempresa = $this->dataProyecto["idempresa"];
			$this->jsonlogin = $this->dataProyecto["jsonlogin"];
			$this->fecha = $this->dataProyecto["fecha"];
			$this->idioma = $this->dataProyecto["idioma"];
			$this->nombre = $this->dataProyecto["nombre"];
			$this->tipo_empresa = $this->dataProyecto["tipo_empresa"];
			$this->logo = $this->dataProyecto["logo"];
			$this->nombreurl = $this->dataProyecto["nombreurl"];
			$this->tipo_portal = $this->dataProyecto["tipo_portal"];
			$this->solocursosprincipales = @$this->dataProyecto["solocursosprincipales"];
			$this->correo_servidor = @$this->dataProyecto["correo_servidor"];
			$this->correo_puerto = @$this->dataProyecto["correo_puerto"];
			$this->correo_email = @$this->dataProyecto["correo_email"];
			$this->correo_clave = @$this->dataProyecto["correo_clave"];			
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('proyecto', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataProyecto = $this->oDatProyecto->get($pk);
			if(empty($this->dataProyecto)) {
				throw new Exception(JrTexto::_("Proyecto").' '.JrTexto::_("not registered"));
			}

			return $this->oDatProyecto->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}