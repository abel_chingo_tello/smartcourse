<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		21-01-2021
 * @copyright	Copyright (C) 21-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRecursos_colabasig_comentario', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRecursos_colabasig_comentario 
{
	protected $idrecursocomentario;
	protected $idasignacion;
	protected $idpersona;
	protected $comentario;
	protected $fecha;
	protected $file;
	protected $idpadre;

	protected $dataRecursos_colabasig_comentario;
	protected $oDatRecursos_colabasig_comentario;	

	public function __construct()
	{
		$this->oDatRecursos_colabasig_comentario = new DatRecursos_colabasig_comentario;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRecursos_colabasig_comentario->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRecursos_colabasig_comentario->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRecursos_colabasig_comentario->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRecursos_colabasig_comentario->get($this->idrecursocomentario);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colabasig_comentario', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRecursos_colabasig_comentario->iniciarTransaccion('neg_i_Recursos_colabasig_comentario');
			$this->idrecursocomentario = $this->oDatRecursos_colabasig_comentario->insertar($this->idasignacion,$this->idpadre,$this->idpersona,$this->comentario,$this->fecha,$this->file);
			$this->oDatRecursos_colabasig_comentario->terminarTransaccion('neg_i_Recursos_colabasig_comentario');	
			return $this->idrecursocomentario;
		} catch(Exception $e) {	
		    $this->oDatRecursos_colabasig_comentario->cancelarTransaccion('neg_i_Recursos_colabasig_comentario');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colabasig_comentario', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRecursos_colabasig_comentario->actualizar($this->idrecursocomentario,$this->idasignacion,$this->idpadre,$this->idpersona,$this->comentario,$this->fecha,$this->file);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function actualizar2($estados,$condicion){
		try {
			return $this->oDatRecursos_colabasig_comentario->actualizar2($estados,$condicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Recursos_colabasig_comentario', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRecursos_colabasig_comentario->eliminar($this->idrecursocomentario);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecursocomentario($pk){
		try {
			$this->dataRecursos_colabasig_comentario = $this->oDatRecursos_colabasig_comentario->get($pk);
			if(empty($this->dataRecursos_colabasig_comentario)) {
				throw new Exception(JrTexto::_("Recursos_colabasig_comentario").' '.JrTexto::_("not registered"));
			}
			$this->idrecursocomentario = $this->dataRecursos_colabasig_comentario["idrecursocomentario"];
			$this->idasignacion = $this->dataRecursos_colabasig_comentario["idasignacion"];
			$this->idpersona = $this->dataRecursos_colabasig_comentario["idpersona"];
			$this->comentario = $this->dataRecursos_colabasig_comentario["comentario"];
			$this->fecha = $this->dataRecursos_colabasig_comentario["fecha"];
			$this->file = $this->dataRecursos_colabasig_comentario["file"];
			$this->idpadre = $this->dataRecursos_colabasig_comentario["idpadre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colabasig_comentario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRecursos_colabasig_comentario = $this->oDatRecursos_colabasig_comentario->get($pk);
			if(empty($this->dataRecursos_colabasig_comentario)) {
				throw new Exception(JrTexto::_("Recursos_colabasig_comentario").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRecursos_colabasig_comentario->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}