<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatHistorial_subidos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegHistorial_subidos 
{
	protected $idsubido;
	protected $nombre_archivo;
	protected $fechasubida;
	protected $tabla;
	protected $descomprimido;
	protected $importacion_total;
	
	protected $dataHistorial_subidos;
	protected $oDatHistorial_subidos;	

	public function __construct()
	{
		$this->oDatHistorial_subidos = new DatHistorial_subidos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatHistorial_subidos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatHistorial_subidos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatHistorial_subidos->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatHistorial_subidos->get($this->idsubido);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_subidos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatHistorial_subidos->iniciarTransaccion('neg_i_Historial_subidos');
			$this->idsubido = $this->oDatHistorial_subidos->insertar($this->nombre_archivo,$this->fechasubida,$this->tabla,$this->descomprimido,$this->importacion_total);
			$this->oDatHistorial_subidos->terminarTransaccion('neg_i_Historial_subidos');	
			return $this->idsubido;
		} catch(Exception $e) {	
		    $this->oDatHistorial_subidos->cancelarTransaccion('neg_i_Historial_subidos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_subidos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatHistorial_subidos->actualizar($this->idsubido,$this->nombre_archivo,$this->fechasubida,$this->tabla,$this->descomprimido,$this->importacion_total);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Historial_subidos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatHistorial_subidos->eliminar($this->idsubido);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdsubido($pk){
		try {
			$this->dataHistorial_subidos = $this->oDatHistorial_subidos->get($pk);
			if(empty($this->dataHistorial_subidos)) {
				throw new Exception(JrTexto::_("Historial_subidos").' '.JrTexto::_("not registered"));
			}
			$this->idsubido = $this->dataHistorial_subidos["idsubido"];
			$this->nombre_archivo = $this->dataHistorial_subidos["nombre_archivo"];
			$this->fechasubida = $this->dataHistorial_subidos["fechasubida"];
			$this->tabla = $this->dataHistorial_subidos["tabla"];
			$this->descomprimido = $this->dataHistorial_subidos["descomprimido"];
			$this->importacion_total = $this->dataHistorial_subidos["importacion_total"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('historial_subidos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataHistorial_subidos = $this->oDatHistorial_subidos->get($pk);
			if(empty($this->dataHistorial_subidos)) {
				throw new Exception(JrTexto::_("Historial_subidos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHistorial_subidos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}