<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-07-2017
 * @copyright	Copyright (C) 31-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_portada', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegBib_portada 
{
	protected $id_portada;
	protected $foto;
	protected $estado;
	
	protected $dataBib_portada;
	protected $oDatBib_portada;	

	public function __construct()
	{
		$this->oDatBib_portada = new DatBib_portada;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_portada->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_portada->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_portada->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_portada->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_portada->get($this->id_portada);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_portada', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_portada->iniciarTransaccion('neg_i_Bib_portada');
			$this->id_portada = $this->oDatBib_portada->insertar($this->foto,$this->estado);
			//$this->oDatBib_portada->terminarTransaccion('neg_i_Bib_portada');	
			return $this->id_portada;
		} catch(Exception $e) {	
		   //$this->oDatBib_portada->cancelarTransaccion('neg_i_Bib_portada');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_portada', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatBib_portada->actualizar($this->id_portada,$this->foto,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatBib_portada->cambiarvalorcampo($this->id_portada,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_portada', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_portada->eliminar($this->id_portada);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_portada($pk){
		try {
			$this->dataBib_portada = $this->oDatBib_portada->get($pk);
			if(empty($this->dataBib_portada)) {
				throw new Exception(JrTexto::_("Bib_portada").' '.JrTexto::_("not registered"));
			}
			$this->id_portada = $this->dataBib_portada["id_portada"];
			$this->foto = $this->dataBib_portada["foto"];
			$this->estado = $this->dataBib_portada["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_portada', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_portada = $this->oDatBib_portada->get($pk);
			if(empty($this->dataBib_portada)) {
				throw new Exception(JrTexto::_("Bib_portada").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_portada->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}