<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-04-2020
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRubrica_competencia', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRubrica_competencia 
{
	protected $idrubrica_competencia;
	protected $idrubrica;
	protected $idcompetencia;
	
	protected $dataRubrica_competencia;
	protected $oDatRubrica_competencia;	

	public function __construct()
	{
		$this->oDatRubrica_competencia = new DatRubrica_competencia;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRubrica_competencia->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRubrica_competencia->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRubrica_competencia->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRubrica_competencia->get($this->idrubrica_competencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_competencia', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRubrica_competencia->iniciarTransaccion('neg_i_Rubrica_competencia');
			$this->idrubrica_competencia = $this->oDatRubrica_competencia->insertar($this->idrubrica,$this->idcompetencia);
			$this->oDatRubrica_competencia->terminarTransaccion('neg_i_Rubrica_competencia');	
			return $this->idrubrica_competencia;
		} catch(Exception $e) {	
		    $this->oDatRubrica_competencia->cancelarTransaccion('neg_i_Rubrica_competencia');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_competencia', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRubrica_competencia->actualizar($this->idrubrica_competencia,$this->idrubrica,$this->idcompetencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rubrica_competencia', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica_competencia->eliminar($this->idrubrica_competencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrubrica_competencia($pk){
		try {
			$this->dataRubrica_competencia = $this->oDatRubrica_competencia->get($pk);
			if(empty($this->dataRubrica_competencia)) {
				throw new Exception(JrTexto::_("Rubrica_competencia").' '.JrTexto::_("not registered"));
			}
			$this->idrubrica_competencia = $this->dataRubrica_competencia["idrubrica_competencia"];
			$this->idrubrica = $this->dataRubrica_competencia["idrubrica"];
			$this->idcompetencia = $this->dataRubrica_competencia["idcompetencia"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_competencia', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRubrica_competencia = $this->oDatRubrica_competencia->get($pk);
			if(empty($this->dataRubrica_competencia)) {
				throw new Exception(JrTexto::_("Rubrica_competencia").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRubrica_competencia->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}