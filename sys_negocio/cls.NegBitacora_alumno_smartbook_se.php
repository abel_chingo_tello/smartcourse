<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		28-11-2019
 * @copyright	Copyright (C) 28-11-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBitacora_alumno_smartbook_se', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegBitacora_alumno_smartbook_se 
{
	protected $idbitacora_smartbook;
	protected $idcurso;
	protected $idsesion;
	protected $idusuario;
	protected $estado;
	protected $regfecha;
	protected $idcurso_sc;
	protected $idproyecto;
	protected $idcc;
	protected $idcursodetalle_sc;
	protected $idgrupoauladetalle;


	protected $datBitacora_alumno_smartbook_se;
	protected $oDatBitacora_alumno_smartbook_se;	

	public function __construct()
	{
		$this->oDatBitacora_alumno_smartbook_se = new DatBitacora_alumno_smartbook_se;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBitacora_alumno_smartbook_se->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBitacora_alumno_smartbook_se->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBitacora_alumno_smartbook_se->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBitacora_alumno_smartbook_se->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBitacora_alumno_smartbook_se->get($this->idbitacora_smartbook);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			//$this->oDatBitacora_alumno_smartbook_se->iniciarTransaccion('neg_i_Bitacora_alumno_smartbook');
			$this->idbitacora_smartbook = $this->oDatBitacora_alumno_smartbook_se->insertar($this->idcurso,$this->idsesion,$this->idusuario,$this->estado,$this->regfecha,$this->idcurso_sc,$this->idproyecto,$this->idcc,$this->idcursodetalle_sc,$this->idgrupoauladetalle);
			//$this->oDatBitacora_alumno_smartbook_se->terminarTransaccion('neg_i_Bitacora_alumno_smartbook');	
			return $this->idbitacora_smartbook;
		} catch(Exception $e) {	
		   // $this->oDatBitacora_alumno_smartbook_se->cancelarTransaccion('neg_i_Bitacora_alumno_smartbook');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {	
			return $this->oDatBitacora_alumno_smartbook_se->actualizar($this->idbitacora_smartbook,$this->idcurso,$this->idsesion,$this->idusuario,$this->estado,$this->regfecha,$this->idcurso_sc,$this->idproyecto,$this->idcc,$this->idcursodetalle_sc,$this->idgrupoauladetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			return $this->oDatBitacora_alumno_smartbook_se->eliminar($this->idbitacora_smartbook);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function multidelete($ids){
		try{
			return $this->oDatBitacora_alumno_smartbook_se->multidelete($ids);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdbitacora_smartbook($pk){
		try {
			$this->datBitacora_alumno_smartbook_se = $this->oDatBitacora_alumno_smartbook_se->get($pk);
			if(empty($this->datBitacora_alumno_smartbook_se)) {
				throw new Exception(JrTexto::_("Bitacora_alumno_smartbook").' '.JrTexto::_("not registered"));
			}
			$this->idbitacora_smartbook = $this->datBitacora_alumno_smartbook_se["idbitacora_smartbook"];
			$this->idcurso = $this->datBitacora_alumno_smartbook_se["idcurso"];
			$this->idsesion = $this->datBitacora_alumno_smartbook_se["idsesion"];
			$this->idusuario = $this->datBitacora_alumno_smartbook_se["idusuario"];
			$this->estado = $this->datBitacora_alumno_smartbook_se["estado"];
			$this->regfecha = $this->datBitacora_alumno_smartbook_se["regfecha"];
			$this->idcurso_sc = $this->datBitacora_alumno_smartbook_se["idcurso_sc"];
			$this->idproyecto = $this->datBitacora_alumno_smartbook_se["idproyecto"];
			$this->idcc = $this->datBitacora_alumno_smartbook_se["idcc"];
			$this->idcursodetalle_sc = $this->datBitacora_alumno_smartbook_se["idcursodetalle_sc"];
			$this->idgrupoauladetalle=$this->datBitacora_alumno_smartbook_se["idgrupoauladetalle"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bitacora_alumno_smartbook', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->datBitacora_alumno_smartbook_se = $this->oDatBitacora_alumno_smartbook_se->get($pk);
			if(empty($this->datBitacora_alumno_smartbook_se)) {
				throw new Exception(JrTexto::_("Bitacora_alumno_smartbook").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBitacora_alumno_smartbook_se->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}