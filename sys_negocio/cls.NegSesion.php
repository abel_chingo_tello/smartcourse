<?php

/**
 * @autor		Abel Chingo Tello , ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersonal', RUTA_BASE);
JrCargador::clase('sys_datos::DatRoles', RUTA_BASE);
JrCargador::clase('sys_datos::DatPersona_rol', RUTA_BASE);
JrCargador::clase('sys_datos::DatBolsa_empresas', RUTA_BASE);
JrCargador::clase('sys_datos::DatAcad_matricula', RUTA_BASE);
JrCargador::clase('jrAdwen::JrSession');
class NegSesion
{
	protected $oDatpersonarol;
	protected $oDatPersona;
	protected $oDatRoles;
	protected $oDatBolsa_empresas;
	protected $oDatAcad_matricula;

	static $keysesion = '_Smartcourse_';

	public function __construct()
	{
		$this->oDatPersona = new DatPersonal;
		$this->oDatpersonarol = new DatPersona_rol;
		$this->oDatRoles = new DatRoles;
		$this->oDatBolsa_empresas = new DatBolsa_empresas;
		$this->oDatAcad_matricula = new DatAcad_matricula;
	}
	public static function existeSesion()
	{
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$idpersona = $sesion->get('idpersona', false, $keysesion);
		if (false === $idpersona) {
			return false;
		} else {
			return true;
		}
	}

	public static function EditarHistorial()
	{
		$sesion = JrSession::getInstancia();
	}

	public static function getUsuario()
	{
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$idpersona = $sesion->get('idpersona', false, $keysesion);
		if (empty($idpersona)) {
			return array('code' => 'Error', 'msj' => JrTexto::_('Please sign in.'));
		}
		return array(
			'idpersona' => $idpersona, 
			'tipodoc' => $sesion->get('tipodoc', false, $keysesion), 
			'apellido1' => $sesion->get('apellido1', false, $keysesion),
			'apellido2' => $sesion->get('apellido2', false, $keysesion),
			'nombre' => $sesion->get('nombre', false, $keysesion),
			'fechanac' => $sesion->get('fechanac', false, $keysesion),
			'dni' => $sesion->get('dni', false, $keysesion), 
			'usuario' => $sesion->get('usuario', false, $keysesion), 
			'email' => $sesion->get('email', false, $keysesion), 'nombre_full' => $sesion->get('nombre_full', false, $keysesion), 'rol' => $sesion->get('rol', false, $keysesion), 'idrol' => $sesion->get('idrol', false, $keysesion), 'roles' => $sesion->get('roles', false, $keysesion), 'rol_original' => $sesion->get('rol_original', false, $keysesion), 'clave' => $sesion->get('clave', false, $keysesion), 'sexo' => $sesion->get('sexo', false, $keysesion), 'idHistorialSesion' => $sesion->get('idHistorialSesion', false, $keysesion), 'fechaentrada' => $sesion->get('fechaentrada', false, $keysesion), 'idproyecto' => $sesion->get('idproyecto', false, $keysesion), 'idempresa' => $sesion->get('idempresa', false, $keysesion), 'idioma' => $sesion->get('idioma', false, $keysesion), 'token' => $sesion->get('token', false, $keysesion), 'foto' => $sesion->get('foto', false, $keysesion), 'tuser' => $sesion->get('tuser', false, $keysesion), 'esdemo' => $sesion->get('esdemo', false, $keysesion), 'tipo_empresa' => $sesion->get('tipo_empresa', false, $keysesion), 'idrepresentante' => $sesion->get('idrepresentante', false, $keysesion), 'emailpadre' => $sesion->get('emailpadre', false, $keysesion)
		);
	}
	public  static function salir()
	{
		$keysesion = self::$keysesion;
		return JrSession::limpiarEspacio($keysesion);
	}
	public function ingresoForzado($datos)
	{
		$_datos = array();
		//chequear si hay usuarios
		if (!empty($datos['usuario']) && !empty($datos['clave'])) {
			$_datos['usuario'] = $datos['usuario'];
			$_datos['clave'] = $datos['clave'];
		} else {
			//buscar un usuario con el idproyecto
			$cond = array('sql2' => 1, 'estado' => 1, 'idproyecto' => $datos['idproyecto']);
			if (!empty($datos['idempresa'])) {
				$cond['idempresa'] = $datos['idempresa'];
			}
			$roles = $this->oDatpersonarol->buscar($cond);
			if (!empty($roles)) {
				$_datos['usuario'] = $roles[0]['usuario'];
				$_datos['clave'] = $roles[0]['clave'];
			}
		}
		if (empty($datos['idempresa'])) {
			$cond['idempresa'] = $datos['idempresa'];
			//buscar id empresa segun idproyecto
			$rs = $this->oDatpersonarol->buscar(array('idproyecto' => $datos['idproyecto']));
			if (empty($rs)) {
				return array('code' => 'Error', 'msj' => JrTexto::_('idproyecto no tiene empresa asignado'), 'islogin' => false);
			}
			$_datos['idempresa'] = $rs[0]['idempresa'];
		} else {
			$_datos['idempresa'] = $datos['idempresa'];
		}

		$_datos['idproyecto'] = $datos['idproyecto'];

		return $this->ingresar($_datos);
	}
	/**
	 * Metodo que cumple la funcion de actualizar las fechas dinamicamente aquellas matriculas que se activan
	 * en el primer login del usuario, o quellas matriculas que se activan en un primer logeo despues de ser matriculado
	 */
	private function actualizarFechas($params){
		try{
			$rs_matricula = $this->oDatAcad_matricula->buscar2(["estado"=>2,"tipomatricula"=>4]);
			if(empty($rs_matricula)){ return false; }
			$fechaActual = date("Y-m-d");
			$rows = []; $row2 = [];
			foreach($rs_matricula as $value){
				//tomar las fechas inicio y vencimiento de la matricula
				$fechainicio = $value["fecha_matricula"];
				$fechafin = $value["fecha_vencimiento"];
				//calcular la diferencia entre fechas (meses) de la matricula
				$date1 = new DateTime($fechainicio);
				$date2 = new DateTime($fechaActual);
				if($date1 == $date2){
					$fechafinActual = $fechafin;
				}else{
					$diferencia = $date1->diff($date2);
					$strDiff = $diferencia->format('%R%a days');
					$fechafinActual = date("Y-m-d",strtotime("{$fechaActual} {$strDiff}"));
				}
				
				//cambiar el estado a 1 de la matricula
				$rows[] = [
					"idmatricula" => $value["idmatricula"]
					,"estado" => 1
					,"fecha_matricula" => $fechaActual
					,"fecha_vencimiento" => $fechafinActual
				];

				$row2[]=[
					"PK" => ["idmatricula_externa" => $value["idmatricula"]]
					,"fecha_inicio" => $fechaActual
					,"fecha_fin" => $fechafinActual
				];
			}//end foreach
			if(!empty($rows)){
				//actualizar las fechas de inicio al actual y modificar la de vencimiento de la matricula
				$this->oDatAcad_matricula->multipleupdate($rows);
				//Inhabilitar en SKS
				$r2 = NegTools::requestHTTP([
					"datos" => json_encode($row2)
				], _URL_SKS_ . "/json/matriculas/updatebypk");
				if (empty($r2) || $r2['code'] != 200) {
					throw new Exception("Ocurrio algo en la peticion json/matriculas/updatebypk");
				}
			}
			return true;
		}catch(Exception $e){
			throw new Exception("Error en actualizarFechas: ". $e->getMessage());
		}
	}
	public function ingresar($datos)
	{
		
		$idpersona = !empty($datos["idpersona"]) ? $datos["idpersona"] : '';
		$usuario = !empty($datos["usuario"]) ? $datos["usuario"] : '';
		$clave = !empty($datos["clave"]) ? $datos["clave"] : '';
		$tocken = !empty($datos["token"]) ? $datos["token"] : '';
		$idproyecto = !empty($datos["idproyecto"]) ? $datos["idproyecto"] : 3;
		if ($idproyecto == 'undefined') $idproyecto = 3;
		$idempresa = !empty($datos["idempresa"]) ? $datos["idempresa"] : 4;
		if ($idempresa == 'undefined') $idempresa = 4;
		$persona = array();
		if(!empty($datos["_user_"])){
			$persona = $this->oDatPersona->buscar(array('_user_' => $datos["_user_"], 'estado' => 1));
		}else if(!empty($idpersona)){
			$persona = $this->oDatPersona->buscar(array('idpersona' => $idpersona, 'estado' => 1));
		} else if (!empty($usuario) && !empty($clave)) {
			$persona = $this->oDatPersona->buscar(array('usuario' => $usuario, 'clave' => $clave, 'estado' => 1));
		} else if (!empty($tocken)) {
			$persona = $this->oDatPersona->buscar(array('token' => $tocken, 'estado' => 1));
			if(!empty($persona[0])){
				$this->oDatPersona->set($persona[0]['idpersona'], 'token', 'enlacedirecto'.date('Y-m-d'));
				$persona[0]['token']='enlacedirecto';
			}				
		} else return array('code' => 'Error', 'msj' => JrTexto::_('Data user incorrect'), 'islogin' => false);

		if (empty($persona)) return array('code' => 'Error', 'msj' => JrTexto::_('Usuario no logueado, Datos de usuario incorrectos'), 'islogin' => false);
		$usuario = $persona[0];
		$roles = $this->oDatpersonarol->buscar(array('idpersonal' => $usuario["idpersona"], 'idproyecto' => $idproyecto, 'idempresa' => $idempresa));
		//var_dump($roles);
		//exit();
		if (empty($roles)) {
			if ($usuario["tipousuario"] == 's') {
				$roles[0] = array('rol' => 'Administrador', 'idrol' => 1, 'idproyecto' => $idproyecto, 'idempresa' => $idempresa);
				$roles[1] = array('rol' => 'Docente', 'idrol' => 2, 'idproyecto' => $idproyecto, 'idempresa' => $idempresa);
				$roles[2] = array('rol' => 'Alumno', 'idrol' => 3, 'idproyecto' => $idproyecto, 'idempresa' => $idempresa);
			} else return array('code' => 'Error', 'msj' => JrTexto::_('Usuario no tiene rol asignado'), 'islogin' => false);
		}

		$empresa = $this->oDatBolsa_empresas->buscar(array("idempresa" => $idempresa))[0];
		$idpersona_ = $empresa["idpersona"];

		$roles_ = $roles[0];
		$rol = $roles_["rol"];
		$idRol = $roles_["idrol"];
		$sesion = JrSession::getInstancia();
		$keysesion = (self::$keysesion);
		if(!isset($_SESSION)) session_start();

		$sesion->set('idpersona', $usuario['idpersona'], $keysesion);
		$sesion->set('tipodoc', $usuario['tipodoc'], $keysesion);
		$sesion->set('emailpadre', $usuario['emailpadre'], $keysesion);
		$sesion->set('dni', $usuario['dni'], $keysesion);
		$sesion->set('usuario', $usuario['usuario'], $keysesion);
		$sesion->set('email', $usuario['email'], $keysesion);
		$sesion->set('rol', $rol, $keysesion);
		$sesion->set('idrol', $idRol, $keysesion); /*Se agregó este campo para se usado en los Controllers necesarios */
		$sesion->set('sexo',  $usuario['sexo'], $keysesion);
		$sesion->set('idproyecto', $idproyecto, $keysesion); /*Se agregó este campo para se usado en los Controllers necesarios */
		$sesion->set('idempresa', $idempresa, $keysesion);
		$sesion->set('idioma',  @$usuario['idioma'], $keysesion);
		$sesion->set('roles', $roles, $keysesion);
		$sesion->set('rol_original', $rol, $keysesion);
		$sesion->set('nombre_full', $usuario['ape_paterno'] . ' ' . $usuario['ape_materno'] . ", " . $usuario['nombre'], $keysesion);
		$sesion->set('clave', $usuario['clave'], $keysesion);
		$sesion->set('token', md5($usuario['usuario'] . $clave), $keysesion);
		$sesion->set('foto', $usuario['foto'], $keysesion);
		$sesion->set('tuser', $usuario['tipousuario'], $keysesion);
		$sesion->set('idrepresentante', $idpersona_, $keysesion);
		$sesion->set('empresa', $empresa, $keysesion);
		$sesion->set('tipo_empresa', false, $keysesion);
		$sesion->set('apellido1', $usuario['ape_paterno'], $keysesion);
		$sesion->set('apellido2', $usuario['ape_materno'], $keysesion);
		$sesion->set('fechanac', $usuario['fechanac'], $keysesion);
		$sesion->set('nombre', $usuario['nombre'], $keysesion);
		
		//buscar si tiene una matricula de tipo 4
		//del tener una matricula 4 actualizar la fecha de vencimiento y de inicio para que contabilice desde su primer login
		$this->actualizarFechas([
			"idpersona" => $usuario["idpersona"]
		]);
		return array('code' => 200, 'islogin' => true);
	}

	public function relogin($datos){
		return $this->ingresar($datos);
	}

	public function cambiar_rol($idrol, $rol, $dni, $idproyecto, $idempresa)
	{
		if (empty($rol) || empty($dni)) return false;
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$user = self::getUsuario();
		$roles = $this->oDatpersonarol->buscar(array('idpersonal' => $dni, 'idproyecto' => $idproyecto, 'idempresa' => $idempresa));
		if ($user["tuser"] == 's') {
			//$hayadmin=false;
			//foreach ($roles as $k => $v) {if($v["idrol"]==1) $hayadmin=true;}
			//if($hayadmin==false){
			$sesion->set('rol', $rol, $keysesion);
			$sesion->set('idrol', $idrol, $keysesion);
			$sesion->set('idempresa', $idempresa, $keysesion);
			$sesion->set('idproyecto', $idproyecto, $keysesion);
			return true;
			//}
			//var_dump($roles);
		}
		if (!empty($roles)) {
			$sesion->set('roles', $roles, $keysesion);
			foreach ($roles as $rol_) {
				$rol = $rol_["rol"];
				$idrol_ = $rol_["idrol"];
				if ($idrol == $idrol_) {
					$sesion->set('rol', $rol, $keysesion);
					$sesion->set('idrol', $idrol_, $keysesion);
					$sesion->set('idempresa', $idempresa, $keysesion);
					$sesion->set('idproyecto', $idproyecto, $keysesion);
					return true;
				}
			}
			return false;
		} else return false;
		//$sesion->set('rol_original', $rol , '__admin_m3c');
	}

	public function verroles($idpersonal, $idproyecto, $idempresa)
	{
		return $this->oDatpersonarol->buscar(array('idpersonal' => $idpersonal, 'idproyecto' => $idproyecto, 'idempresa' => $idempresa));
	}

	public function misrolesallempresas($idpersonal, $idproyecto, $idempresa)
	{
		return $this->oDatpersonarol->buscar(array('idpersonal' => $idpersonal, 'noproyecto' => $idproyecto, 'noempresa' => $idempresa));
	}


	public static function get($prop, $espacio = '')
	{
		//var_dump($this->keysesion,'aaaaaaa');
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$espacio = (!empty($espacio) ? $espacio : $keysesion);
		return $sesion->get($prop, null, $espacio);
	}

	public static function set_($prop, $valor)
	{
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$idpersona = $sesion->get('idpersona', false, $keysesion);
		if (empty($idpersona)) {
			throw new Exception(JrTexto::_('Please sign in.'));
		}
		return $sesion->set($prop, $valor, $keysesion);
	}

	public static function set($prop, $valor, $espacio = '')
	{

		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		return $sesion->set($prop, $valor, (!empty($espacio) ? $espacio : $keysesion));
	}

	public static function tiene_acceso($menu, $accion)
	{
		if (!NegSesion::existeSesion()) {
			return false;
		}
		if ('superadmin' == NegSesion::get('rol')) {
			return true;
		}
		$oDatRoles = new DatRoles;
		$existe_privilegio = $oDatRoles->existe_permiso(NegSesion::get('rol'), $menu, $accion);
		if (!$existe_privilegio) {
			return false;
		}
		return true;
	}

	public static function tieneRol($nombreRol = '')
	{
		$existe = false;
		if (empty($nombreRol)) {
			return $existe;
		}
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$arrRoles = $sesion->get('roles', false, $keysesion);
		foreach ($arrRoles as  $r) {
			if (strtolower($r['rol']) === strtolower($nombreRol)) {
				$existe = true;
				break;
			}
		}
		return $existe;
	}

	public static function getEmpresa($idempresa=0){
		try{
			if(empty($idempresa)){
				$user=NegSesion::getUsuario();
				$idempresa=$user["idempresa"];
			}
			$oDatBolsa_empresas = new DatBolsa_empresas;
			return $oDatBolsa_empresas->buscar(array("idempresa" => $idempresa))[0];

		}catch(Exception $ex){

		}
	}

	public static function setEmpresa($prop,$valor){
		$sesion = JrSession::getInstancia();
		$sesion->set($prop, $valor,'_infoempresa_');
		return $sesion->getAll('_infoempresa_');
	}

}
