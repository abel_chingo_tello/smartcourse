<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-07-2017
 * @copyright	Copyright (C) 04-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_recursos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegBib_recursos 
{
	protected $id_recursos;
	protected $id_personal;
	protected $titulo;
	protected $texto;
	protected $caratula;
	protected $orden;
	protected $tipo;
	protected $publicado;
	protected $compartido;
	protected $estado;
	protected $descripcion;

	
	protected $dataBib_recursos;
	protected $oDatBib_recursos;	

	public function __construct()
	{
		$this->oDatBib_recursos = new DatBib_recursos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_recursos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_recursos->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_recursos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarCambiar($id,$compartido)
	{
		try {
			return $this->oDatBib_recursos->buscarCambiar($id,$compartido);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar($filtros = array())
	{
		try {
			return $this->oDatBib_recursos->listarall($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
    public function newOrder($id_personal){
        try {
			return $this->oDatBib_recursos->newOrder($id_personal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}      
    }

	public function getXid()
	{
		try {
			return $this->oDatBib_recursos->get($this->id_recursos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_recursos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_recursos->iniciarTransaccion('neg_i_Bib_recursos');
            
			$this->id_recursos = $this->oDatBib_recursos->insertar($this->id_personal,$this->titulo,$this->texto,$this->caratula,$this->estado,$this->orden,$this->tipo,$this->publicado,$this->compartido,$this->descripcion);
			//$this->oDatBib_recursos->terminarTransaccion('neg_i_Bib_recursos');	
			return $this->id_recursos;
		} catch(Exception $e) {	
		   //$this->oDatBib_recursos->cancelarTransaccion('neg_i_Bib_recursos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_recursos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}		*/	
			return $this->oDatBib_recursos->actualizar($this->id_recursos,$this->id_personal,$this->titulo,$this->texto,$this->caratula,$this->estado,$this->orden,$this->tipo,$this->publicado,$this->compartido,$this->descripcion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_recursos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_recursos->eliminar($this->id_recursos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
    public function eliminar2($dni,$orden)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_recursos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_recursos->eliminar2($dni,$orden);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecursos($pk){
		try {
			$this->dataBib_recursos = $this->oDatBib_recursos->get($pk);
			if(empty($this->dataBib_recursos)) {
				throw new Exception(JrTexto::_("Bib_recursos").' '.JrTexto::_("not registered"));
			}
			$this->id_recursos = $this->dataBib_recursos["id_recursos"];
			$this->id_personal = $this->dataBib_recursos["id_personal"];
			$this->titulo = $this->dataBib_recursos["titulo"];
			$this->estado = $this->dataBib_recursos["estado"];
			$this->texto = $this->dataBib_recursos["texto"];
			$this->caratula = $this->dataBib_recursos["caratula"];
			$this->orden = $this->dataBib_recursos["orden"];
			$this->tipo = $this->dataBib_recursos["tipo"];
			$this->publicado = $this->dataBib_recursos["publicado"];
			$this->compartido = $this->dataBib_recursos["compartido"];
			$this->descripcion = $this->dataBib_recursos["descripcion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_recursos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_recursos = $this->oDatBib_recursos->get($pk);
			if(empty($this->dataBib_recursos)) {
				throw new Exception(JrTexto::_("Bib_recursos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_recursos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}