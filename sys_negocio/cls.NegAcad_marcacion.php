<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-01-2018
 * @copyright	Copyright (C) 19-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_marcacion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_marcacion 
{
	protected $idmarcacion;
	protected $idgrupodetalle;
	protected $idcurso;
	protected $idhorario;
	protected $fecha;
	protected $horaingreso;
	protected $horasalida;
	protected $iddocente;
	protected $nalumnos;
	protected $observacion;
	
	protected $dataAcad_marcacion;
	protected $oDatAcad_marcacion;	

	public function __construct()
	{
		$this->oDatAcad_marcacion = new DatAcad_marcacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_marcacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_marcacion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_marcacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function mismarcaciones($filtros = array())
	{
		try{
			$this->oDatAcad_marcacion->setLimite(0,1000);
			return $this->oDatAcad_marcacion->mismarcaciones($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_marcacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_marcacion->get($this->idmarcacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_marcacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_marcacion->iniciarTransaccion('neg_i_Acad_marcacion');
			$this->idmarcacion = $this->oDatAcad_marcacion->insertar($this->idgrupodetalle,$this->idcurso,$this->idhorario,$this->fecha,$this->horaingreso,$this->horasalida,$this->iddocente,$this->nalumnos,$this->observacion);
			$this->oDatAcad_marcacion->terminarTransaccion('neg_i_Acad_marcacion');	
			return $this->idmarcacion;
		} catch(Exception $e) {	
		    $this->oDatAcad_marcacion->cancelarTransaccion('neg_i_Acad_marcacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_marcacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_marcacion->actualizar($this->idmarcacion,$this->idgrupodetalle,$this->idcurso,$this->idhorario,$this->fecha,$this->horaingreso,$this->horasalida,$this->iddocente,$this->nalumnos,$this->observacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_marcacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_marcacion->eliminar($this->idmarcacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmarcacion($pk){
		try {
			$this->dataAcad_marcacion = $this->oDatAcad_marcacion->get($pk);
			if(empty($this->dataAcad_marcacion)) {
				throw new Exception(JrTexto::_("Acad_marcacion").' '.JrTexto::_("not registered"));
			}
			$this->idmarcacion = $this->dataAcad_marcacion["idmarcacion"];
			$this->idgrupodetalle = $this->dataAcad_marcacion["idgrupodetalle"];
			$this->idcurso = $this->dataAcad_marcacion["idcurso"];
			$this->idhorario = $this->dataAcad_marcacion["idhorario"];
			$this->fecha = $this->dataAcad_marcacion["fecha"];
			$this->horaingreso = $this->dataAcad_marcacion["horaingreso"];
			$this->horasalida = $this->dataAcad_marcacion["horasalida"];
			$this->iddocente = $this->dataAcad_marcacion["iddocente"];
			$this->nalumnos = $this->dataAcad_marcacion["nalumnos"];
			$this->observacion = $this->dataAcad_marcacion["observacion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_marcacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_marcacion = $this->oDatAcad_marcacion->get($pk);
			if(empty($this->dataAcad_marcacion)) {
				throw new Exception(JrTexto::_("Acad_marcacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_marcacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}