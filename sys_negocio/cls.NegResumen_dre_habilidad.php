<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatResumen_dre_habilidad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegResumen_dre_habilidad 
{
	protected $id;
	protected $id_ubigeo;
	protected $iddre;
	protected $tipo;
	protected $ubicacion_hab_L;
	protected $ubicacion_hab_R;
	protected $ubicacion_hab_W;
	protected $ubicacion_hab_S;
	protected $entrada_hab_L;
	protected $entrada_hab_R;
	protected $entrada_hab_W;
	protected $entrada_hab_S;
	protected $salida_hab_L;
	protected $salida_hab_R;
	protected $salida_hab_W;
	protected $salida_hab_S;
	protected $examen_b1_hab_L;
	protected $examen_b1_hab_R;
	protected $examen_b1_hab_W;
	protected $examen_b1_hab_S;
	protected $examen_b2_hab_L;
	protected $examen_b2_hab_R;
	protected $examen_b2_hab_W;
	protected $examen_b2_hab_S;
	protected $examen_b3_hab_L;
	protected $examen_b3_hab_R;
	protected $examen_b3_hab_W;
	protected $examen_b3_hab_S;
	protected $examen_b4_hab_L;
	protected $examen_b4_hab_R;
	protected $examen_b4_hab_W;
	protected $examen_b4_hab_S;
	protected $examen_t1_hab_L;
	protected $examen_t1_hab_R;
	protected $examen_t1_hab_W;
	protected $examen_t1_hab_S;
	protected $examen_t2_hab_L;
	protected $examen_t2_hab_R;
	protected $examen_t2_hab_W;
	protected $examen_t2_hab_S;
	protected $examen_t3_hab_L;
	protected $examen_t3_hab_R;
	protected $examen_t3_hab_W;
	protected $examen_t3_hab_S;
	protected $prog_hab_A1_L;
	protected $prog_hab_A1_R;
	protected $prog_hab_A1_W;
	protected $prog_hab_A1_S;
	protected $prog_hab_A2_L;
	protected $prog_hab_A2_R;
	
	protected $dataResumen_dre_habilidad;
	protected $oDatResumen_dre_habilidad;	

	public function __construct()
	{
		$this->oDatResumen_dre_habilidad = new DatResumen_dre_habilidad;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatResumen_dre_habilidad->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatResumen_dre_habilidad->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatResumen_dre_habilidad->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatResumen_dre_habilidad->get($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('resumen_dre_habilidad', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatResumen_dre_habilidad->iniciarTransaccion('neg_i_Resumen_dre_habilidad');
			$this->id = $this->oDatResumen_dre_habilidad->insertar($this->id_ubigeo,$this->iddre,$this->tipo,$this->ubicacion_hab_L,$this->ubicacion_hab_R,$this->ubicacion_hab_W,$this->ubicacion_hab_S,$this->entrada_hab_L,$this->entrada_hab_R,$this->entrada_hab_W,$this->entrada_hab_S,$this->salida_hab_L,$this->salida_hab_R,$this->salida_hab_W,$this->salida_hab_S,$this->examen_b1_hab_L,$this->examen_b1_hab_R,$this->examen_b1_hab_W,$this->examen_b1_hab_S,$this->examen_b2_hab_L,$this->examen_b2_hab_R,$this->examen_b2_hab_W,$this->examen_b2_hab_S,$this->examen_b3_hab_L,$this->examen_b3_hab_R,$this->examen_b3_hab_W,$this->examen_b3_hab_S,$this->examen_b4_hab_L,$this->examen_b4_hab_R,$this->examen_b4_hab_W,$this->examen_b4_hab_S,$this->examen_t1_hab_L,$this->examen_t1_hab_R,$this->examen_t1_hab_W,$this->examen_t1_hab_S,$this->examen_t2_hab_L,$this->examen_t2_hab_R,$this->examen_t2_hab_W,$this->examen_t2_hab_S,$this->examen_t3_hab_L,$this->examen_t3_hab_R,$this->examen_t3_hab_W,$this->examen_t3_hab_S,$this->prog_hab_A1_L,$this->prog_hab_A1_R,$this->prog_hab_A1_W,$this->prog_hab_A1_S,$this->prog_hab_A2_L,$this->prog_hab_A2_R);
			$this->oDatResumen_dre_habilidad->terminarTransaccion('neg_i_Resumen_dre_habilidad');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatResumen_dre_habilidad->cancelarTransaccion('neg_i_Resumen_dre_habilidad');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('resumen_dre_habilidad', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatResumen_dre_habilidad->actualizar($this->id,$this->id_ubigeo,$this->iddre,$this->tipo,$this->ubicacion_hab_L,$this->ubicacion_hab_R,$this->ubicacion_hab_W,$this->ubicacion_hab_S,$this->entrada_hab_L,$this->entrada_hab_R,$this->entrada_hab_W,$this->entrada_hab_S,$this->salida_hab_L,$this->salida_hab_R,$this->salida_hab_W,$this->salida_hab_S,$this->examen_b1_hab_L,$this->examen_b1_hab_R,$this->examen_b1_hab_W,$this->examen_b1_hab_S,$this->examen_b2_hab_L,$this->examen_b2_hab_R,$this->examen_b2_hab_W,$this->examen_b2_hab_S,$this->examen_b3_hab_L,$this->examen_b3_hab_R,$this->examen_b3_hab_W,$this->examen_b3_hab_S,$this->examen_b4_hab_L,$this->examen_b4_hab_R,$this->examen_b4_hab_W,$this->examen_b4_hab_S,$this->examen_t1_hab_L,$this->examen_t1_hab_R,$this->examen_t1_hab_W,$this->examen_t1_hab_S,$this->examen_t2_hab_L,$this->examen_t2_hab_R,$this->examen_t2_hab_W,$this->examen_t2_hab_S,$this->examen_t3_hab_L,$this->examen_t3_hab_R,$this->examen_t3_hab_W,$this->examen_t3_hab_S,$this->prog_hab_A1_L,$this->prog_hab_A1_R,$this->prog_hab_A1_W,$this->prog_hab_A1_S,$this->prog_hab_A2_L,$this->prog_hab_A2_R);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Resumen_dre_habilidad', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatResumen_dre_habilidad->eliminar($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataResumen_dre_habilidad = $this->oDatResumen_dre_habilidad->get($pk);
			if(empty($this->dataResumen_dre_habilidad)) {
				throw new Exception(JrTexto::_("Resumen_dre_habilidad").' '.JrTexto::_("not registered"));
			}
			$this->id = $this->dataResumen_dre_habilidad["id"];
			$this->id_ubigeo = $this->dataResumen_dre_habilidad["id_ubigeo"];
			$this->iddre = $this->dataResumen_dre_habilidad["iddre"];
			$this->tipo = $this->dataResumen_dre_habilidad["tipo"];
			$this->ubicacion_hab_L = $this->dataResumen_dre_habilidad["ubicacion_hab_L"];
			$this->ubicacion_hab_R = $this->dataResumen_dre_habilidad["ubicacion_hab_R"];
			$this->ubicacion_hab_W = $this->dataResumen_dre_habilidad["ubicacion_hab_W"];
			$this->ubicacion_hab_S = $this->dataResumen_dre_habilidad["ubicacion_hab_S"];
			$this->entrada_hab_L = $this->dataResumen_dre_habilidad["entrada_hab_L"];
			$this->entrada_hab_R = $this->dataResumen_dre_habilidad["entrada_hab_R"];
			$this->entrada_hab_W = $this->dataResumen_dre_habilidad["entrada_hab_W"];
			$this->entrada_hab_S = $this->dataResumen_dre_habilidad["entrada_hab_S"];
			$this->salida_hab_L = $this->dataResumen_dre_habilidad["salida_hab_L"];
			$this->salida_hab_R = $this->dataResumen_dre_habilidad["salida_hab_R"];
			$this->salida_hab_W = $this->dataResumen_dre_habilidad["salida_hab_W"];
			$this->salida_hab_S = $this->dataResumen_dre_habilidad["salida_hab_S"];
			$this->examen_b1_hab_L = $this->dataResumen_dre_habilidad["examen_b1_hab_L"];
			$this->examen_b1_hab_R = $this->dataResumen_dre_habilidad["examen_b1_hab_R"];
			$this->examen_b1_hab_W = $this->dataResumen_dre_habilidad["examen_b1_hab_W"];
			$this->examen_b1_hab_S = $this->dataResumen_dre_habilidad["examen_b1_hab_S"];
			$this->examen_b2_hab_L = $this->dataResumen_dre_habilidad["examen_b2_hab_L"];
			$this->examen_b2_hab_R = $this->dataResumen_dre_habilidad["examen_b2_hab_R"];
			$this->examen_b2_hab_W = $this->dataResumen_dre_habilidad["examen_b2_hab_W"];
			$this->examen_b2_hab_S = $this->dataResumen_dre_habilidad["examen_b2_hab_S"];
			$this->examen_b3_hab_L = $this->dataResumen_dre_habilidad["examen_b3_hab_L"];
			$this->examen_b3_hab_R = $this->dataResumen_dre_habilidad["examen_b3_hab_R"];
			$this->examen_b3_hab_W = $this->dataResumen_dre_habilidad["examen_b3_hab_W"];
			$this->examen_b3_hab_S = $this->dataResumen_dre_habilidad["examen_b3_hab_S"];
			$this->examen_b4_hab_L = $this->dataResumen_dre_habilidad["examen_b4_hab_L"];
			$this->examen_b4_hab_R = $this->dataResumen_dre_habilidad["examen_b4_hab_R"];
			$this->examen_b4_hab_W = $this->dataResumen_dre_habilidad["examen_b4_hab_W"];
			$this->examen_b4_hab_S = $this->dataResumen_dre_habilidad["examen_b4_hab_S"];
			$this->examen_t1_hab_L = $this->dataResumen_dre_habilidad["examen_t1_hab_L"];
			$this->examen_t1_hab_R = $this->dataResumen_dre_habilidad["examen_t1_hab_R"];
			$this->examen_t1_hab_W = $this->dataResumen_dre_habilidad["examen_t1_hab_W"];
			$this->examen_t1_hab_S = $this->dataResumen_dre_habilidad["examen_t1_hab_S"];
			$this->examen_t2_hab_L = $this->dataResumen_dre_habilidad["examen_t2_hab_L"];
			$this->examen_t2_hab_R = $this->dataResumen_dre_habilidad["examen_t2_hab_R"];
			$this->examen_t2_hab_W = $this->dataResumen_dre_habilidad["examen_t2_hab_W"];
			$this->examen_t2_hab_S = $this->dataResumen_dre_habilidad["examen_t2_hab_S"];
			$this->examen_t3_hab_L = $this->dataResumen_dre_habilidad["examen_t3_hab_L"];
			$this->examen_t3_hab_R = $this->dataResumen_dre_habilidad["examen_t3_hab_R"];
			$this->examen_t3_hab_W = $this->dataResumen_dre_habilidad["examen_t3_hab_W"];
			$this->examen_t3_hab_S = $this->dataResumen_dre_habilidad["examen_t3_hab_S"];
			$this->prog_hab_A1_L = $this->dataResumen_dre_habilidad["prog_hab_A1_L"];
			$this->prog_hab_A1_R = $this->dataResumen_dre_habilidad["prog_hab_A1_R"];
			$this->prog_hab_A1_W = $this->dataResumen_dre_habilidad["prog_hab_A1_W"];
			$this->prog_hab_A1_S = $this->dataResumen_dre_habilidad["prog_hab_A1_S"];
			$this->prog_hab_A2_L = $this->dataResumen_dre_habilidad["prog_hab_A2_L"];
			$this->prog_hab_A2_R = $this->dataResumen_dre_habilidad["prog_hab_A2_R"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('resumen_dre_habilidad', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataResumen_dre_habilidad = $this->oDatResumen_dre_habilidad->get($pk);
			if(empty($this->dataResumen_dre_habilidad)) {
				throw new Exception(JrTexto::_("Resumen_dre_habilidad").' '.JrTexto::_("not registered"));
			}

			return $this->oDatResumen_dre_habilidad->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}