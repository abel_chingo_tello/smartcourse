<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-05-2017
 * @copyright	Copyright (C) 11-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatReporte', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegReporte 
{
	protected $idreporte;
	protected $idusuario;
	protected $idrol;
	protected $tipo;
	protected $id_alumno_grupo;
	public $informacion;
	protected $fechacreacion;
	
	protected $dataReporte;
	protected $oDatReporte;	

	public function __construct()
	{
		$this->oDatReporte = new DatReporte;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatReporte->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatReporte->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatReporte->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarultimo($filtros = array())
	{
		try {
			$this->setLimite(0,1);
			return $this->oDatReporte->buscarultimo($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function buscarDeHoy($filtros = array())
	{
		try {
			$this->setLimite(0,1);
			$res = $this->oDatReporte->buscar($filtros);
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatReporte->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatReporte->get($this->idreporte);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('reporte', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').' at '. __FUNCTION__.'!!');
			}*/
			//$this->oDatReporte->iniciarTransaccion('neg_i_Reporte');
			$this->idreporte = $this->oDatReporte->insertar($this->idusuario,$this->idrol,$this->tipo,$this->id_alumno_grupo,$this->informacion,$this->fechacreacion);
			//$this->oDatReporte->terminarTransaccion('neg_i_Reporte');	
			return $this->idreporte;
		} catch(Exception $e) {	
		   //$this->oDatReporte->cancelarTransaccion('neg_i_Reporte');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('reporte', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').' at '. __FUNCTION__.'!!');
			}	*/		
			return $this->oDatReporte->actualizar($this->idreporte,$this->idusuario,$this->idrol,$this->tipo,$this->id_alumno_grupo,$this->informacion,$this->fechacreacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Reporte', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').' at '. __FUNCTION__.'!!');
			}*/
			return $this->oDatReporte->eliminar($this->idreporte);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdreporte($pk){
		try {
			$this->dataReporte = $this->oDatReporte->get($pk);
			if(empty($this->dataReporte)) {
				throw new Exception(JrTexto::_("Reporte").' '.JrTexto::_("not registered").' at '. __METHOD__);
			}
			$this->idreporte = $this->dataReporte["idreporte"];
			$this->idusuario = $this->dataReporte["idusuario"];
			$this->idrol = $this->dataReporte["idrol"];
			$this->tipo = $this->dataReporte["tipo"];
			$this->id_alumno_grupo = $this->dataReporte["id_alumno_grupo"];
			$this->informacion = $this->dataReporte["informacion"];
			$this->fechacreacion = $this->dataReporte["fechacreacion"];

		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('reporte', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').' at '. __METHOD__.'!!');
			}
			$this->dataReporte = $this->oDatReporte->get($pk);
			if(empty($this->dataReporte)) {
				throw new Exception(JrTexto::_("Reporte").' '.JrTexto::_("not registered").' at '. __METHOD__);
			}

			return $this->oDatReporte->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setIdusuario($idusuario)
	{
		try {
			$this->idusuario= NegTools::validar('todo', $idusuario, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdrol($idrol)
	{
		try {
			$this->idrol= NegTools::validar('todo', $idrol, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTipo($tipo)
	{
		try {
			$this->tipo= NegTools::validar('todo', $tipo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 32));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setId_alumno_grupo($id_alumno_grupo)
	{
		try {
			$this->id_alumno_grupo= NegTools::validar('todo', $id_alumno_grupo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setInformacion($informacion)
	{
		try {
			$this->informacion= NegTools::validar('todo', $informacion, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4294967296));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFechacreacion($fechacreacion)
	{
		try {
			$this->fechacreacion= NegTools::validar('todo', $fechacreacion, false, JrTexto::_("Please enter a valid value"), array("longmax" => 16));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}