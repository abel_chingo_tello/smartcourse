<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-04-2020
 * @copyright	Copyright (C) 24-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRubrica', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRubrica 
{
	protected $idrubrica;
	protected $nombre;
	protected $fecha_creacion;
	protected $idproyecto;
	protected $estado;
	protected $iddocente;
	
	protected $dataRubrica;
	protected $oDatRubrica;	

	public function __construct()
	{
		$this->oDatRubrica = new DatRubrica;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRubrica->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRubrica->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRubrica->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRubrica->get($this->idrubrica);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRubrica->iniciarTransaccion('neg_i_Rubrica');
			$this->idrubrica = $this->oDatRubrica->insertar($this->nombre,$this->fecha_creacion,$this->idproyecto,$this->estado,$this->iddocente);
			$this->oDatRubrica->terminarTransaccion('neg_i_Rubrica');	
			return $this->idrubrica;
		} catch(Exception $e) {	
		    $this->oDatRubrica->cancelarTransaccion('neg_i_Rubrica');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRubrica->actualizar($this->idrubrica,$this->nombre,$this->fecha_creacion,$this->idproyecto,$this->estado,$this->iddocente);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rubrica', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica->eliminar($this->idrubrica);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrubrica($pk){
		try {
			$this->dataRubrica = $this->oDatRubrica->get($pk);
			if(empty($this->dataRubrica)) {
				throw new Exception(JrTexto::_("Rubrica").' '.JrTexto::_("not registered"));
			}
			$this->idrubrica = $this->dataRubrica["idrubrica"];
			$this->nombre = $this->dataRubrica["nombre"];
			$this->fecha_creacion = $this->dataRubrica["fecha_creacion"];
			$this->idproyecto = $this->dataRubrica["idproyecto"];
			$this->estado = $this->dataRubrica["estado"];
			$this->iddocente = $this->dataRubrica["iddocente"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rubrica', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRubrica = $this->oDatRubrica->get($pk);
			if(empty($this->dataRubrica)) {
				throw new Exception(JrTexto::_("Rubrica").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRubrica->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}