<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		21-05-2020
 * @copyright	Copyright (C) 21-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatCron_tarea_proy', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegCron_tarea_proy
{
	protected $idcron_tapr;
	protected $idproyecto;
	protected $fecha_hora;
	protected $estado;
	protected $idsesion;
	protected $idpestania;
	protected $idcurso;
	protected $idcomplementario;
	protected $iddocente;
	protected $nombre;
	protected $tipo;
	protected $recurso;
	protected $tiporecurso;
	protected $idgrupoauladetalle;

	protected $dataCron_tarea_proy;
	protected $oDatCron_tarea_proy;

	public function __construct()
	{
		$this->oDatCron_tarea_proy = new DatCron_tarea_proy;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatCron_tarea_proy->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			$this->setLimite(0,9999991);
			return $this->oDatCron_tarea_proy->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatCron_tarea_proy->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatCron_tarea_proy->get($this->idcron_tapr);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('cron_tarea_proy', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatCron_tarea_proy->iniciarTransaccion('neg_i_Cron_tarea_proy');
			$this->idcron_tapr = $this->oDatCron_tarea_proy->insertar($this->idproyecto, $this->fecha_hora, $this->estado, $this->idsesion, $this->idpestania, $this->idcurso, $this->idcomplementario, $this->iddocente, $this->nombre, $this->tipo, $this->recurso, $this->tiporecurso, $this->idgrupoauladetalle);
			$this->oDatCron_tarea_proy->terminarTransaccion('neg_i_Cron_tarea_proy');
			return $this->idcron_tapr;
		} catch (Exception $e) {
			$this->oDatCron_tarea_proy->cancelarTransaccion('neg_i_Cron_tarea_proy');
			throw new Exception($e->getMessage());
		}
	}
	public function customUpdate($filtros, $update)
	{
		try {
			/*if(!NegSesion::tiene_acceso('cron_tarea_proy', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatCron_tarea_proy->customUpdate($filtros, $update);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('cron_tarea_proy', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatCron_tarea_proy->actualizar($this->idcron_tapr, $this->idproyecto, $this->fecha_hora, $this->estado, $this->idsesion, $this->idpestania, $this->idcurso, $this->idcomplementario, $this->iddocente, $this->nombre, $this->tipo, $this->recurso, $this->tiporecurso);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar($filtros)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Cron_tarea_proy', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatCron_tarea_proy->eliminar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcron_tapr($pk)
	{
		try {
			$this->dataCron_tarea_proy = $this->oDatCron_tarea_proy->get($pk);
			if (empty($this->dataCron_tarea_proy)) {
				throw new Exception(JrTexto::_("Cron_tarea_proy") . ' ' . JrTexto::_("not registered"));
			}
			$this->idcron_tapr = $this->dataCron_tarea_proy["idcron_tapr"];
			$this->idproyecto = $this->dataCron_tarea_proy["idproyecto"];
			$this->fecha_hora = $this->dataCron_tarea_proy["fecha_hora"];
			$this->estado = $this->dataCron_tarea_proy["estado"];
			$this->idsesion = $this->dataCron_tarea_proy["idsesion"];
			$this->idpestania = $this->dataCron_tarea_proy["idpestania"];
			$this->idcurso = $this->dataCron_tarea_proy["idcurso"];
			$this->idcomplementario = $this->dataCron_tarea_proy["idcomplementario"];
			$this->iddocente = $this->dataCron_tarea_proy["iddocente"];
			$this->nombre = $this->dataCron_tarea_proy["nombre"];
			$this->tipo = $this->dataCron_tarea_proy["tipo"];
			$this->recurso = $this->dataCron_tarea_proy["recurso"];
			$this->tiporecurso = $this->dataCron_tarea_proy["tiporecurso"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('cron_tarea_proy', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataCron_tarea_proy = $this->oDatCron_tarea_proy->get($pk);
			if (empty($this->dataCron_tarea_proy)) {
				throw new Exception(JrTexto::_("Cron_tarea_proy") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatCron_tarea_proy->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarTarea($filtros = array())
	{
		try {
			return $this->oDatCron_tarea_proy->buscarTarea($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function agregarTarea($params = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('cron_tarea_proy', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			// $this->oDatCron_tarea_proy->iniciarTransaccion('neg_i_Cron_tarea_proy');
			$this->idcron_tapr = $this->oDatCron_tarea_proy->insertarTarea($params);
			// $this->oDatCron_tarea_proy->terminarTransaccion('neg_i_Cron_tarea_proy');
			return $this->idcron_tapr;
		} catch (Exception $e) {
			$this->oDatCron_tarea_proy->cancelarTransaccion('neg_i_Cron_tarea_proy');
			throw new Exception($e->getMessage());
		}
	}
}
