<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-05-2020
 * @copyright	Copyright (C) 04-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPortafolio_archivo', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegPortafolio_archivo 
{
	protected $idarchivo;
	protected $idfolder;
	protected $nombre;
	protected $ubicacion;
	protected $tipo;
	protected $extension;
	protected $idportafolio;
	
	protected $dataPortafolio_archivo;
	protected $oDatPortafolio_archivo;	

	public function __construct()
	{
		$this->oDatPortafolio_archivo = new DatPortafolio_archivo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPortafolio_archivo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPortafolio_archivo->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPortafolio_archivo->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPortafolio_archivo->get($this->idarchivo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('portafolio_archivo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPortafolio_archivo->iniciarTransaccion('neg_i_Portafolio_archivo');
			$this->idarchivo = $this->oDatPortafolio_archivo->insertar($this->idfolder,$this->nombre,$this->ubicacion,$this->tipo,$this->extension,$this->idportafolio);
			$this->oDatPortafolio_archivo->terminarTransaccion('neg_i_Portafolio_archivo');	
			return $this->idarchivo;
		} catch(Exception $e) {	
		    $this->oDatPortafolio_archivo->cancelarTransaccion('neg_i_Portafolio_archivo');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('portafolio_archivo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPortafolio_archivo->actualizar($this->idarchivo,$this->idfolder,$this->nombre,$this->ubicacion,$this->tipo,$this->extension,$this->idportafolio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Portafolio_archivo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPortafolio_archivo->eliminar($this->idarchivo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdarchivo($pk){
		try {
			$this->dataPortafolio_archivo = $this->oDatPortafolio_archivo->get($pk);
			if(empty($this->dataPortafolio_archivo)) {
				throw new Exception(JrTexto::_("Portafolio_archivo").' '.JrTexto::_("not registered"));
			}
			$this->idarchivo = $this->dataPortafolio_archivo["idarchivo"];
			$this->idfolder = $this->dataPortafolio_archivo["idfolder"];
			$this->nombre = $this->dataPortafolio_archivo["nombre"];
			$this->ubicacion = $this->dataPortafolio_archivo["ubicacion"];
			$this->tipo = $this->dataPortafolio_archivo["tipo"];
			$this->extension = $this->dataPortafolio_archivo["extension"];
			$this->idportafolio = $this->dataPortafolio_archivo["idportafolio"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('portafolio_archivo', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPortafolio_archivo = $this->oDatPortafolio_archivo->get($pk);
			if(empty($this->dataPortafolio_archivo)) {
				throw new Exception(JrTexto::_("Portafolio_archivo").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPortafolio_archivo->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}