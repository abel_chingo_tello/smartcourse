<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_detalle_usuario_estudio', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegBib_detalle_usuario_estudio 
{
	protected $id_detalle;
	protected $id_personal;
	protected $id_estudio;
	protected $tipo;
	
	protected $dataBib_detalle_usuario_estudio;
	protected $oDatBib_detalle_usuario_estudio;	

	public function __construct()
	{
		$this->oDatBib_detalle_usuario_estudio = new DatBib_detalle_usuario_estudio;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_detalle_usuario_estudio->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_detalle_usuario_estudio->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{

		try {
			return $this->oDatBib_detalle_usuario_estudio->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarbiblioteca($filtros = array())
	{
		
		try {
			return $this->oDatBib_detalle_usuario_estudio->buscarbiblioteca($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatBib_detalle_usuario_estudio->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar1($dni)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_estudio', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_estudio->listar1($dni);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_detalle_usuario_estudio->get($this->id_detalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {

			/*if(!NegSesion::tiene_acceso('bib_detalle_usuario_estudio', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_detalle_usuario_estudio->iniciarTransaccion('neg_i_Bib_detalle_usuario_estudio');
			$this->id_detalle = $this->oDatBib_detalle_usuario_estudio->insertar($this->id_personal,$this->id_estudio,$this->tipo);
			//$this->oDatBib_detalle_usuario_estudio->terminarTransaccion('neg_i_Bib_detalle_usuario_estudio');	
			return $this->id_detalle;
		} catch(Exception $e) {	
		   //$this->oDatBib_detalle_usuario_estudio->cancelarTransaccion('neg_i_Bib_detalle_usuario_estudio');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_detalle_usuario_estudio', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBib_detalle_usuario_estudio->actualizar($this->id_detalle,$this->id_personal,$this->id_estudio,$this->tipo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_detalle_usuario_estudio', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_detalle_usuario_estudio->eliminar($this->id_detalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_detalle($pk){
		try {
			$this->dataBib_detalle_usuario_estudio = $this->oDatBib_detalle_usuario_estudio->get($pk);
			if(empty($this->dataBib_detalle_usuario_estudio)) {
				throw new Exception(JrTexto::_("Bib_detalle_usuario_estudio").' '.JrTexto::_("not registered"));
			}
			$this->id_detalle = $this->dataBib_detalle_usuario_estudio["id_detalle"];
			$this->id_personal = $this->dataBib_detalle_usuario_estudio["id_personal"];
			$this->id_estudio = $this->dataBib_detalle_usuario_estudio["id_estudio"];
			$this->tipo = $this->dataBib_detalle_usuario_estudio["tipo"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_detalle_usuario_estudio', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_detalle_usuario_estudio = $this->oDatBib_detalle_usuario_estudio->get($pk);
			if(empty($this->dataBib_detalle_usuario_estudio)) {
				throw new Exception(JrTexto::_("Bib_detalle_usuario_estudio").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_detalle_usuario_estudio->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}