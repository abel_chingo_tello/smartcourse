<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		02-12-2020
 * @copyright	Copyright (C) 02-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatDescargas_consumidas', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegDescargas_consumidas 
{
	
	protected $iddescargaconsumida;
	protected $idasignado;
	protected $archivo;
	protected $idcurso;
	protected $strcurso;
	protected $idcc;
	//protected $idproyecto;
	//protected $idusuario;

	
	protected $dataDescargas_consumidas;
	protected $oDatDescargas_consumidas;	

	public function __construct()
	{
		$this->oDatDescargas_consumidas = new DatDescargas_consumidas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatDescargas_consumidas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try{
			return $this->oDatDescargas_consumidas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Descargas_consumidas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatDescargas_consumidas->iniciarTransaccion('neg_i_Descargas_consumidas');
			$this->iddescargaconsumida = $this->oDatDescargas_consumidas->insertar($this->idasignado,$this->archivo,$this->idcurso,$this->strcurso,$this->idcc);
			$this->oDatDescargas_consumidas->terminarTransaccion('neg_i_Descargas_consumidas');	
			return $this->iddescargaconsumida;
		} catch(Exception $e) {	
		    $this->oDatDescargas_consumidas->cancelarTransaccion('neg_i_Descargas_consumidas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Descargas_consumidas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatDescargas_consumidas->actualizar($this->iddescargaconsumida, $this->idasignado,$this->archivo,$this->idcurso,$this->strcurso,$this->idcc);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	
	public function actualizar2($estados,$condicion){
		try {
			return $this->oDatDescargas_consumidas->actualizar2($estados,$condicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatDescargas_consumidas->cambiarvalorcampo($this->idasignado,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Descargas_consumidas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatDescargas_consumidas->eliminar($this->id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecurso($pk){
		try {
			$this->dataDescargas_consumidas = $this->oDatDescargas_consumidas->buscar(array('sqlget'=>true,'iddescargaconsumida'=>$pk));
			if(empty($this->dataDescargas_consumidas)) {
				throw new Exception(JrTexto::_("Descargas_consumidas").' '.JrTexto::_("not registered"));
			}
			$this->iddescargaconsumida=$this->dataDescargas_consumidas["iddescargaconsumida"];
			$this->idasignado=$this->dataDescargas_consumidas["idasignado"];
			$this->archivo = $this->dataDescargas_consumidas["archivo"];
			$this->idcurso = $this->dataDescargas_consumidas["idcurso"];
			$this->strcurso = $this->dataDescargas_consumidas["strcurso"];
			$this->idcc = $this->dataDescargas_consumidas["idcc"];
			//$this->idproyecto = $this->dataDescargas_consumidas["idproyecto"];
			//$this->estado = $this->dataDescargas_consumidas["estado"];
		//	$this->idusuario = $this->dataDescargas_consumidas["idusuario"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('Descargas_consumidas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataDescargas_consumidas = $this->oDatDescargas_consumidas->buscar(array('sqlget'=>true,'iddescargaconsumida'=>$pk));
			if(empty($this->dataDescargas_consumidas)) {
				throw new Exception(JrTexto::_("Descargas_consumidas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatDescargas_consumidas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}