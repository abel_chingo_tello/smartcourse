<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		09-03-2020
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatForos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegForos
{
	protected $idpublicacion;
	protected $titulo;
	protected $subtitulo;
	protected $contenido;
	protected $estado;
	protected $idusuario;
	protected $rol;
	protected $idcurso;
	protected $idgrupoaula;
	protected $idpersona;
	protected $idproyecto;
	protected $categoria;
	protected $idgrupoauladetalle;
	protected $idpublicacionpadre;
	protected $tipo;
	protected $fecha;
	protected $hora;
	protected $otrosdatos;

	protected $dataForos;
	protected $oDatForos;

	public function __construct()
	{
		$this->oDatForos = new DatForos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatForos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatForos->buscar($filtros);
			// return $this->oDatForos->get($this->idpublicacion,$this->idpersona);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function prueba($filtros = array())
	{
		try {
			return $filtros;
			// return $this->oDatForos->get($this->idpublicacion,$this->idpersona);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar($filtros = array())
	{
		try {
			return $this->oDatForos->listar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getForoAula($filtros = array())
	{
		try {
			return $this->oDatForos->getForoAula($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatForos->get($this->idpublicacion);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('foros', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatForos->iniciarTransaccion('neg_i_Foros');
			// $this->idpublicacion =
			$this->oDatForos->insertar($this->titulo, $this->subtitulo, $this->estado, $this->contenido, $this->idpersona, $this->idgruauladet,$this->idproyecto);
			$this->oDatForos->terminarTransaccion('neg_i_Foros');
			return "ok";
		} catch (Exception $e) {
			$this->oDatForos->cancelarTransaccion('neg_i_Foros');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('foros', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatForos->actualizar($this->idpublicacion, $this->titulo, $this->subtitulo, $this->estado, $this->contenido, $this->idpersona, $this->idgruauladet);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Foros', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatForos->eliminar($this->idpublicacion);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setidpublicacion($pk)
	{
		try {
			$this->dataForos = $this->oDatForos->get($pk);
			if (empty($this->dataForos)) {
				throw new Exception(JrTexto::_("Foros") . ' ' . JrTexto::_("not registered"));
			}
			$this->idpublicacion = $this->dataForos["idpublicacion"];

			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

}
