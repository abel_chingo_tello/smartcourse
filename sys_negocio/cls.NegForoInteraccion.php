<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		09-03-2020
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatForoInteraccion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegForoInteraccion
{
	protected $idpublicacion;
	protected $idforo_interaccion;
	protected $idpuntuacion;
	protected $puntuacion;
	protected $idpersona;
	protected $dataForos;
	protected $oDatForoInteraccion;	

	public function __construct()
	{
		$this->oDatForoInteraccion = new DatForoInteraccion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatForoInteraccion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatForoInteraccion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function promedio($filtros = array())
	{
		try {
			return $this->oDatForoInteraccion->promedio($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatForoInteraccion->listar();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatForoInteraccion->get($this->idpublicacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('foros', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatForoInteraccion->iniciarTransaccion('neg_i_Foros');
			// $this->idpublicacion =
			$this->oDatForoInteraccion->insertar($this->idpublicacion,$this->idforo_interaccion,$this->puntuacion,$this->idpersona);
			$this->oDatForoInteraccion->terminarTransaccion('neg_i_Foros');	
			return "ok";
		} catch(Exception $e) {	
		    $this->oDatForoInteraccion->cancelarTransaccion('neg_i_Foros');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('foros', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatForoInteraccion->actualizar($this->idpublicacion,$this->titulo,$this->texto,$this->estado,$this->idusuario,$this->rol,$this->idcurso,$this->idgrupoaula,$this->idgrupoauladetalle,$this->idpublicacionpadre,$this->tipo,$this->otrosdatos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatForoInteraccion->cambiarvalorcampo($this->idpublicacion,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Foros', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatForoInteraccion->eliminar($this->idpublicacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setidpublicacion($pk){
		try {
			$this->dataForos = $this->oDatForoInteraccion->get($pk);
			if(empty($this->dataForos)) {
				throw new Exception(JrTexto::_("Foros").' '.JrTexto::_("not registered"));
			}
			$this->idpublicacion = $this->dataForos["idpublicacion"];
			// $this->titulo = $this->dataForos["titulo"];
			// $this->texto = $this->dataForos["texto"];
			// $this->estado = $this->dataForos["estado"];
			// $this->idusuario = $this->dataForos["idusuario"];
			// $this->rol = $this->dataForos["rol"];
			// $this->idcurso = $this->dataForos["idcurso"];
			// $this->idgrupoaula = $this->dataForos["idgrupoaula"];
			// $this->idgrupoauladetalle = $this->dataForos["idgrupoauladetalle"];
			// $this->idpublicacionpadre = $this->dataForos["idpublicacionpadre"];
			// $this->tipo = $this->dataForos["tipo"];
			// $this->regfecha = $this->dataForos["regfecha"];
			// $this->otrosdatos = $this->dataForos["otrosdatos"];
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('foros', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataForos = $this->oDatForoInteraccion->get($pk);
			if(empty($this->dataForos)) {
				throw new Exception(JrTexto::_("Foros").' '.JrTexto::_("not registered"));
			}

			return $this->oDatForoInteraccion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
