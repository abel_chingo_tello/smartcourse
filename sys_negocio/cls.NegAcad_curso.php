<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		08-02-2018
 * @copyright	Copyright (C) 08-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursocategoria', RUTA_BASE);
class NegAcad_curso
{
	protected $idcurso;
	protected $nombre;
	protected $imagen;
	protected $descripcion;
	protected $estado;
	protected $fecharegistro;
	protected $idusuario;
	protected $vinculosaprendizajes;
	protected $materialesyrecursos;
	protected $color;
	protected $abreviado;
	protected $autor;
	protected $tipo;
	protected $aniopublicacion;
	protected $txtjson;
	protected $sks;
	protected $oNegBitacora_smartbook;
	protected $dataAcad_curso;
	protected $oDatAcad_curso;

	public function __construct()
	{
		$this->oDatAcad_curso = new DatAcad_curso;
		$this->oNegBitacora_smartbook = new NegBitacora_smartbook;
		$this->oNegAcad_categorias = new NegAcad_categorias;
		$this->oNegAcad_cursocategoria = new NegAcad_cursocategoria;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatAcad_curso->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_curso->getNumRegistros($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_curso->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function todos($filtros = array())
	{
		try {
			$this->setLimite(0, 100000000);
			//var_dump($filtros);
			return $this->oDatAcad_curso->todos($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarcursos($filtros = array())
	{
		try {
			$this->setLimite(0, 100000000);
			//var_dump($filtros);
			return $this->oDatAcad_curso->buscarcursos($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarXproyecto($filtros = array())
	{
		try {
			return $this->oDatAcad_curso->buscarXproyecto($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarxcategoria($filtros = array())
	{
		try {
			$this->oDatAcad_curso->setLimite(0, 10000);
			return $this->oDatAcad_curso->buscarxcategoria($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function duplicarcurso($idcurso, $idcc, $tipo, $idproyecto, $idusuario, $temas, $nombrecurso)
	{
		return $this->oDatAcad_curso->duplicarcurso($idcurso, $idcc, $tipo, $idproyecto, $idusuario, $temas, $nombrecurso);
	}

	public function reordenar($idproyecto, $datos)
	{
		return $this->oDatAcad_curso->reordenar($idproyecto, $datos);
	}

	public function insertcategoriasxcurso($idcat, $idcur)
	{
		return $this->oDatAcad_curso->insertcategoriasxcurso($idcat, $idcur);
	}

	public function eliminarcategoriasxcurso($id)
	{
		return $this->oDatAcad_curso->eliminarcategoriasxcurso($id);
	}

	public function getCursos_Not_In_GrupoAulaDetalle($filtros = array(), $iddocente = 0)
	{
		try {
			if ($iddocente <= 0) {
				throw new Exception(JrTexto::_('Iddocente is missing'));
			}
			return $this->oDatAcad_curso->getCursos_Not_In_GrupoAulaDetalle($filtros, $iddocente);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_curso->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_curso->get($this->idcurso);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getCurso($idcurso)
	{
		try {
			return $this->oDatAcad_curso->getCurso($idcurso);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	//
	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_curso->iniciarTransaccion('neg_i_Acad_curso');
			$this->idcurso = $this->oDatAcad_curso->insertar($this->nombre, $this->imagen, $this->descripcion, $this->estado, $this->fecharegistro, $this->idusuario, $this->vinculosaprendizajes, $this->materialesyrecursos, $this->color, $this->abreviado, $this->autor, $this->aniopublicacion, $this->tipo, $this->sks);
			$this->oDatAcad_curso->terminarTransaccion('neg_i_Acad_curso');
			return $this->idcurso;
		} catch (Exception $e) {
			$this->oDatAcad_curso->cancelarTransaccion('neg_i_Acad_curso');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_curso->actualizar($this->idcurso, $this->nombre, $this->imagen, $this->descripcion, $this->estado, $this->fecharegistro, $this->idusuario, $this->vinculosaprendizajes, $this->materialesyrecursos, $this->color, $this->abreviado, $this->autor, $this->aniopublicacion);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function cambiarvalorcampo($campo, $valor)
	{
		try {
			return $this->oDatAcad_curso->cambiarvalorcampo($this->idcurso, $campo, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminar($tipo = '1')
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_curso', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_curso->eliminar($this->idcurso, $tipo);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminarcurso($datos)
	{
		try {
			return $this->oDatAcad_curso->eliminarcurso($datos);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcurso($pk)
	{
		try {
			$this->dataAcad_curso = $this->oDatAcad_curso->get($pk);
			if (empty($this->dataAcad_curso)) {
				throw new Exception(JrTexto::_("Acad_curso") . ' ' . JrTexto::_("not registered"));
			}
			$this->idcurso = $this->dataAcad_curso["idcurso"];
			$this->nombre = $this->dataAcad_curso["nombre"];
			$this->imagen = $this->dataAcad_curso["imagen"];
			$this->descripcion = $this->dataAcad_curso["descripcion"];
			$this->estado = $this->dataAcad_curso["estado"];
			$this->fecharegistro = $this->dataAcad_curso["fecharegistro"];
			$this->idusuario = $this->dataAcad_curso["idusuario"];
			$this->vinculosaprendizajes = @$this->dataAcad_curso["vinculosaprendizajes"];
			$this->materialesyrecursos = @$this->dataAcad_curso["materialesyrecursos"];
			$this->color = $this->dataAcad_curso["color"];
			$this->abreviado = $this->dataAcad_curso["abreviado"];
			$this->autor = $this->dataAcad_curso["autor"];
			$this->aniopublicacion = $this->dataAcad_curso["aniopublicacion"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_curso = $this->oDatAcad_curso->get($pk);
			if (empty($this->dataAcad_curso)) {
				throw new Exception(JrTexto::_("Acad_curso") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatAcad_curso->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function setCampo_($pk, $propiedad, $valor, $tabla)
	{
		try {
			return $this->oDatAcad_curso->set_($pk, $propiedad, $valor, $tabla);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getProgresoUnidad($sesion, $idCurso, $idpersona, $esComplementario)
	{
		try {
			$sesion["progreso"] = 0;
			//$sesion["nactividad"]=0;
			//$sesion["link"]='';
			// if ($sesion["tiporecurso"] != 'E') {
			$p = null;
			if ($esComplementario) {
				//progreso por cada sesion ($suma_pestanias/$total_pestanias)
				$p = $this->oNegBitacora_smartbook->getProgresoPromedio(array('sql2' => true, 'idcomplementario' => $idCurso, 'idusuario' => $idpersona, 'idsesion' => $sesion['idcursodetalle']));
			} else {
				//progreso por cada sesion ($suma_pestanias/$total_pestanias
				$p = $this->oNegBitacora_smartbook->getProgresoPromedio(array('sql2' => true, 'idcurso' => $idCurso, 'idusuario' => $idpersona, 'idsesion' => $sesion['idcursodetalle']));
			}


			$sesion["progreso"] = $p; //($suma_pestanias/$total_pestanias)
			//$p=number_format((float)$p, 2, '.', ''); 
			//$sesion["link"]=$this->documento->getUrlBase()."/smartbook/ver2/?idcursodetalle=".$sesion["idcursodetalle"]."&idrecurso=".$sesion["idrecurso"];            
			if (!empty($sesion['hijo'])) { //ES MENU CONTENEDOR
				$hijos = array();
				$p = 0;
				$nhijos = count($sesion['hijo']);
				$suma_progreso = 0;
				$j = 0;
				foreach ($sesion['hijo'] as $sesion_hija) {
					if ($j == 0) $nb = false;
					$j++;
					$sesion_hija = $this->getProgresoUnidad($sesion_hija, $idCurso, $idpersona, $esComplementario);
					$progreso_hijo = (float) $sesion_hija["progreso"];
					//if($progreso_hijo<$this->avanzaen) $nb=true;
					$suma_progreso = $suma_progreso + $progreso_hijo;
					$hijos[] = $sesion_hija;
				}
				$p = (float)$suma_progreso / $nhijos;
				$p = $p >= 100 ? 100 : $p;
				//$p=number_format((float)$p, 2, '.', '');                
				$sesion["progreso"] = $p;
				$sesion["hijo"] = $hijos;
			}
			// }
			return $sesion;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function _cursos($filtros)
	{
		try {
			$user = NegSesion::getUsuario();
			$filtros["idproyecto"] = $user['idproyecto'];
			return $this->oDatAcad_curso->cursos($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function guardarcategoria($filtros)
	{
		try {
			$user = NegSesion::getUsuario();
			$idproyecto = $user['idproyecto'];
			if (empty($filtros["idcomplementario"]) && !empty($filtros["idcurso"])) {
				$haycat = $this->oDatAcad_curso->buscarcategoriascursos(array('idcurso' => $filtros["idcurso"], 'idproy' => $idproyecto));
				
				if(empty($filtros["idcategoria"]) && !empty($haycat[0])){
					$this->oDatAcad_curso->eliminarcategoriaxcursoxproyecto($filtros["idcurso"],$idproyecto);
				}
				elseif(!empty($haycat[0])) {				
					$this->oDatAcad_curso->updatecategoriasxcurso($haycat[0]["id"], $filtros["idcategoria"], $filtros["idcurso"]);
				} else {
					$this->oDatAcad_curso->insertcategoriasxcurso($filtros["idcategoria"], $filtros["idcurso"]);
				}

				if(@$filtros["tipo"] == 2){
					$this->oDatAcad_curso->updatecategoriaextension($filtros["idcategoria"], $filtros["idcurso"], 0);
					//$this->oDatAcad_curso->updatecategoriaextension($filtros["idcategoria"], $filtros["idcurso"]);
				}
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function cursos($filtros)
	{
		try {
			$allcursos = array('miscursos' => array(), 'otroscursos' => array());
			//$cursos=$this->oDatAcad_curso->cursos($filtros);    		
			$user = NegSesion::getUsuario();
			$filtros["idproyecto"] = $user['idproyecto'];
			$this->oDatAcad_curso->setLimite(0, 1000000);
			$miscursos = $this->oDatAcad_curso->cursos($filtros);

			$idcursoprincipales = array();
			$idmiscursos = array();
			$configcurso = array();
			if (!empty($miscursos))
				foreach ($miscursos as $k => $v) {
					if ($v["tipo"] == 2) { // solo tipo puede tener complementarios
						$idcursoprincipales[] = $v["idcurso"];
						$configcurso[$v["idcurso"]] = array('configuracion_nota' => $v["configuracion_nota"]);
					}
					$v["idcomplementario"] = 0;
					$allcursos['miscursos'][] = $v;
					$idmiscursos[] = $v["idcurso"];
				}
			$filtrosex = $filtros;
			$filtrosex["idcursoprincipal"] = $idcursoprincipales;
			$filtrosex["complementarios"] = true;
			$this->oDatAcad_curso->setLimite(0, 1000000);
			$miscursosextencion = $this->oDatAcad_curso->cursos($filtrosex);
			if (!empty($miscursosextencion))
				foreach ($miscursosextencion as $k => $v) {
					$dt = $v;
					$dt["idcurso"] = $v["idcursoprincipal"];
					$dt["idcomplementario"] = $v["idcurso"];
					$dt["idproyecto"] = $user['idproyecto'];
					$dt["configuracion_nota"] = @$configcurso[$v["idcursoprincipal"]]["configuracion_nota"];
					$allcursos['miscursos'][] = $dt;
				}
			if (!empty($filtros['todos'])) {
				$filtros2 = array();
				$filtros2['noidcurso'] = $idmiscursos;
				$filtros2['tipo'] = 1;
				$otroscursos = $this->oDatAcad_curso->cursos($filtros2);
				if (!empty($otroscursos))
					foreach ($otroscursos as $k => $v) {
						$v["idcomplementario"] = 0;
						$allcursos['otroscursos'][] = $v;
					}
			}
               
			if (!empty($filtros["allcategorias"])) {
				$this->oNegAcad_categorias->setLimite(0, 1000000);
				$categorias = $this->oNegAcad_categorias->buscar(array('idproyecto' => $user['idproyecto']));
				$idcats = array();
				if (!empty($categorias)) {
					foreach ($categorias as $k => $v) {
						$idcats[] = $v["idcategoria"];
					}
				}
				if (!empty($idcats) && !empty($allcursos["miscursos"])) {
					$this->oNegAcad_cursocategoria->setLimite(0, 1000000);
					$cursoscat = $this->oNegAcad_cursocategoria->buscar(array('idcategoria' => $idcats));
					//var_dump($cursoscat);

					$miscursos_ = array();
					$yaasigno=array();
					if (!empty($cursoscat)) {
						foreach ($allcursos["miscursos"] as $kc => $vc) {
							if ($vc["idcomplementario"] == 0) $vc["idcategoria"] = 0;
							foreach ($cursoscat as $k => $v) {
								if ($vc["idcurso"] == $v["idcurso"] && $vc["idcomplementario"] == 0) {
									if(!empty($v["idproy"])){
										$yaasigno[$v["idcurso"]]=$v["idcategoria"];
									}
									$vc["idcategoria"] = !empty($yaasigno[$v["idcurso"]])?$yaasigno[$v["idcurso"]]:$v["idcategoria"];
								}
							}
							$miscursos_[] = $vc;
						}
						$allcursos["miscursos"] = $miscursos_;
					}
				}
				// var_dump($allcursos["miscursos"]);
				$miscursos_ = array();
				if (!empty($allcursos["miscursos"]))
					foreach ($allcursos["miscursos"] as $kc => $vc) {
						if ($vc["idcomplementario"] == 0 && empty($vc["idcategoria"])) $vc["idcategoria"] = 0;
						$miscursos_[] = $vc;
					}
				$allcursos["miscursos"] = $miscursos_;
			}
			return $allcursos;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function grupos_de_curso($filtros){
		try {
			return $this->oDatAcad_curso->grupos_de_curso($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function validar_y_eliminar($idcurso,$idcc=0,$tipo=1,$accion='si'){
		try{
			$user = NegSesion::getUsuario();
			$idproyecto = $user['idproyecto'];
			if($tipo==1 && $accion=='si'){ //Validar si curso esta en cualquier proyecto				
					// trae el listado de grupos al cual pertenece el curso.
					$haygrupos=$this->oDatAcad_curso->grupos_de_curso(array('idcurso'=>$idcurso,'idcc'=>$idcc,'tipo'=>1)); 
					if(empty($haygrupos)) return $this->oDatAcad_curso->eliminarcurso(array('idcurso'=>$idcurso,'idcc'=>$idcc,'tipo'=>1,'idproyecto'=>$idproyecto));
					else{
						echo json_encode(array('code'=>201,'msj'=>'El curso  tiene asignado grupos de estudio, elimínelos primero'));
						exit(0);
					}
			}else{ // solo elimina el curso del proyecto
				$filtros=array('idcurso'=>$idcurso,'idcc'=>$idcc,'tipo'=>$tipo,'idproyecto'=>$idproyecto);
				$haygrupos=$this->oDatAcad_curso->grupos_de_curso($filtros);
				//var_dump($haygrupos);
				//exit();
				if(empty($haygrupos)){ // quitamos el curso de este proyecto
					if($tipo==1) $filtros["quitardelaempresa"]=true;
					return $this->oDatAcad_curso->eliminarcurso($filtros);
				}else{
					echo json_encode(array('code'=>201,'msj'=>'El curso  tiene asignado grupos de estudio, elimínelos primero'));
					exit(0);	
				}
			}
		}catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
