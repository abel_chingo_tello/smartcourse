<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		07-12-2020
 * @copyright	Copyright (C) 07-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRecursos_colaborativosasignacion', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRecursos_colaborativosasignacion 
{
	
	protected $idrecursoasignacion;
	protected $idrecurso;
	protected $idalumno;
	protected $grupo;
	protected $idgrupoauladetalle;
	protected $nota;
	protected $nota_base;
	protected $iddocente;
	protected $fecha_calificacion;
	protected $titulo;
	protected $respuestas;
	protected $fecha_respuesta;
	protected $idcursodetalle;
	protected $idpestania;
	
	protected $dataRecursos_colaborativosasignacion;
	protected $oDatRecursos_colaborativosasignacion;	

	public function __construct()
	{
		$this->oDatRecursos_colaborativosasignacion = new DatRecursos_colaborativosasignacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRecursos_colaborativosasignacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRecursos_colaborativosasignacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colaborativosasignacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatRecursos_colaborativosasignacion->iniciarTransaccion('neg_i_Recursos_colaborativosasignacion');
			$this->idrecursoasignacion = $this->oDatRecursos_colaborativosasignacion->insertar($this->idrecurso,$this->idalumno,$this->grupo,$this->idgrupoauladetalle,$this->nota,$this->nota_base,$this->iddocente,$this->fecha_calificacion,$this->titulo,$this->respuestas,$this->fecha_respuesta,$this->idcursodetalle,$this->idpestania);
			$this->oDatRecursos_colaborativosasignacion->terminarTransaccion('neg_i_Recursos_colaborativosasignacion');	
			return $this->idrecursoasignacion;
		} catch(Exception $e) {	
		    $this->oDatRecursos_colaborativosasignacion->cancelarTransaccion('neg_i_Recursos_colaborativosasignacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function agregarasignacion($datos){
		try{
			return $this->oDatRecursos_colaborativosasignacion->agregarasignacion($datos);
		}catch(Exception $ex){
			throw new Exception($ex->getMessage());
		}
	}
	

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colaborativosasignacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatRecursos_colaborativosasignacion->actualizar($this->idrecursoasignacion,$this->idrecurso,$this->idalumno,$this->grupo,$this->idgrupoauladetalle,$this->nota,$this->nota_base,$this->iddocente,$this->fecha_calificacion,$this->titulo,$this->respuestas,$this->fecha_respuesta,$this->idcursodetalle,$this->idpestania);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	

	public function actualizar2($estados,$condicion){
		try {
			return $this->oDatRecursos_colaborativosasignacion->actualizar2($estados,$condicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Recursos_colaborativosasignacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRecursos_colaborativosasignacion->eliminar($this->idrecursoasignacion,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecursoasignacion($pk){
		try {
			$this->dataRecursos_colaborativosasignacion = $this->oDatRecursos_colaborativosasignacion->buscar(array('sqlget'=>true,'idrecursoasignacion'=>$pk));
			if(empty($this->dataRecursos_colaborativosasignacion)) {
				throw new Exception(JrTexto::_("Recursos_colaborativosasignacion").' '.JrTexto::_("not registered"));
			}
			$this->idrecursoasignacion=$this->dataRecursos_colaborativosasignacion["idrecursoasignacion"];
			$this->idrecurso = $this->dataRecursos_colaborativosasignacion["idrecurso"];
			$this->idalumno = $this->dataRecursos_colaborativosasignacion["idalumno"];
			$this->grupo = $this->dataRecursos_colaborativosasignacion["grupo"];
			$this->idgrupoauladetalle = $this->dataRecursos_colaborativosasignacion["idgrupoauladetalle"];
			$this->nota = $this->dataRecursos_colaborativosasignacion["nota"];
			$this->nota_base = $this->dataRecursos_colaborativosasignacion["nota_base"];
			$this->iddocente = $this->dataRecursos_colaborativosasignacion["iddocente"];
			$this->fecha_calificacion = $this->dataRecursos_colaborativosasignacion["fecha_calificacion"];
			$this->titulo = $this->dataRecursos_colaborativosasignacion["titulo"];
			$this->respuestas = $this->dataRecursos_colaborativosasignacion["respuestas"];
			$this->fecha_respuesta = $this->dataRecursos_colaborativosasignacion["fecha_respuesta"];
			$this->idcursodetalle = $this->dataRecursos_colaborativosasignacion["idcursodetalle"];
			$this->idpestania = $this->dataRecursos_colaborativosasignacion["idpestania"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('recursos_colaborativosasignacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRecursos_colaborativosasignacion = $this->oDatRecursos_colaborativosasignacion->buscar(array('sqlget'=>true,'idrecursoasignacion'=>$pk));
			if(empty($this->dataRecursos_colaborativosasignacion)) {
				throw new Exception(JrTexto::_("Recursos_colaborativosasignacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRecursos_colaborativosasignacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}