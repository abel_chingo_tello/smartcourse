<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-03-2020
 * @copyright	Copyright (C) 31-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatFamilia_slider', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegFamilia_slider 
{
	protected $codigo;
	protected $idpersona;
	protected $idproyecto;
	protected $idempresa;
	protected $link;
	protected $fecha_hora;
	
	protected $dataFamilia_slider;
	protected $oDatFamilia_slider;	

	public function __construct()
	{
		$this->oDatFamilia_slider = new DatFamilia_slider;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatFamilia_slider->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatFamilia_slider->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatFamilia_slider->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatFamilia_slider->get($this->codigo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar($estados = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('familia_slider', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatFamilia_slider->iniciarTransaccion('neg_i_Familia_slider');
			$codigo = $this->oDatFamilia_slider->insertar($estados);
			$this->oDatFamilia_slider->terminarTransaccion('neg_i_Familia_slider');	
			return $codigo;
		} catch(Exception $e) {	
		    $this->oDatFamilia_slider->cancelarTransaccion('neg_i_Familia_slider');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('familia_slider', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatFamilia_slider->actualizar($this->codigo,$this->idpersona,$this->idproyecto,$this->idempresa,$this->link,$this->fecha_hora);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Familia_slider', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatFamilia_slider->eliminar($this->codigo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCodigo($pk){
		try {
			$this->dataFamilia_slider = $this->oDatFamilia_slider->get($pk);
			if(empty($this->dataFamilia_slider)) {
				throw new Exception(JrTexto::_("Familia_slider").' '.JrTexto::_("not registered"));
			}
			$this->codigo = $this->dataFamilia_slider["codigo"];
			$this->idpersona = $this->dataFamilia_slider["idpersona"];
			$this->idproyecto = $this->dataFamilia_slider["idproyecto"];
			$this->idempresa = $this->dataFamilia_slider["idempresa"];
			$this->link = $this->dataFamilia_slider["link"];
			$this->fecha_hora = $this->dataFamilia_slider["fecha_hora"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('familia_slider', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataFamilia_slider = $this->oDatFamilia_slider->get($pk);
			if(empty($this->dataFamilia_slider)) {
				throw new Exception(JrTexto::_("Familia_slider").' '.JrTexto::_("not registered"));
			}

			return $this->oDatFamilia_slider->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}