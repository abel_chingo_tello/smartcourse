<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		16-10-2018
 * @copyright	Copyright (C) 16-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
// JrCargador::clase('sys_datos::DatMin_competencias', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegMin_competencias 
{
	protected $idcompetencia;
	protected $titulo;
	protected $descripcion;
	protected $estado;
	
	protected $dataMin_competencias;
	// protected $oDatMin_competencias;	

	public function __construct()
	{
		// $this->oDatMin_competencias = new DatMin_competencias;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMin_competencias->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatMin_competencias->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMin_competencias->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function competencias(){
		try{
			$resultado = array();
			$competencias = $this->oDatMin_competencias->competencias(array('estado'=> true));
			if(!empty($competencias)){
				$tmp_competencias =$competencias;
				$i = 0;
				do{
					$tmp_competencias[$i] = array_splice($tmp_competencias[$i], 0,4);
					++$i;
				}while($i < count($tmp_competencias));
				$tmp_competencias = array_unique($tmp_competencias,SORT_REGULAR);

				foreach($tmp_competencias as $c){
					$prepare_capacidades = array();
					$prepare_habilidad = array();
					foreach($competencias as $key => $value){
						if($c['idcompetencia'] == $value['idcompetencia']){
							$prepare_capacidades[] = array('idcapacidad'=> $value['idcapacidad'],'nombre'=>$value['capacidad'],'descripcion' => 'descripcion_capacidad');
						}
					}
					if(strpos($c['habilidad'],'|') === false){
						$prepare_habilidad[] = $c['habilidad'];
					}else{
						$prepare_habilidad = explode('|',$c['habilidad']);
					}
					$resultado[] = array(
						'idcompetencia' => $c['idcompetencia'],
						'competencia' => $c['competencia'],
						'capacidades' => $prepare_capacidades,
						'habilidades' => $prepare_habilidad
					);
				}//end foreach 
			}//end if resultado
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public function capacidades(){
		try{
			$resultado = array();
			$capacidades = $this->oDatUnidad_capacidad->buscar(array());
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMin_competencias->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMin_competencias->get($this->idcompetencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_competencias', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatMin_competencias->iniciarTransaccion('neg_i_Min_competencias');
			$this->idcompetencia = $this->oDatMin_competencias->insertar($this->titulo,$this->descripcion,$this->estado);
			$this->oDatMin_competencias->terminarTransaccion('neg_i_Min_competencias');	
			return $this->idcompetencia;
		} catch(Exception $e) {	
		    $this->oDatMin_competencias->cancelarTransaccion('neg_i_Min_competencias');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_competencias', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatMin_competencias->actualizar($this->idcompetencia,$this->titulo,$this->descripcion,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Min_competencias', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatMin_competencias->eliminar($this->idcompetencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcompetencia($pk){
		try {
			$this->dataMin_competencias = $this->oDatMin_competencias->get($pk);
			if(empty($this->dataMin_competencias)) {
				throw new Exception(JrTexto::_("Min_competencias").' '.JrTexto::_("not registered"));
			}
			$this->idcompetencia = $this->dataMin_competencias["idcompetencia"];
			$this->titulo = $this->dataMin_competencias["titulo"];
			$this->descripcion = $this->dataMin_competencias["descripcion"];
			$this->estado = $this->dataMin_competencias["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('min_competencias', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataMin_competencias = $this->oDatMin_competencias->get($pk);
			if(empty($this->dataMin_competencias)) {
				throw new Exception(JrTexto::_("Min_competencias").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMin_competencias->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}