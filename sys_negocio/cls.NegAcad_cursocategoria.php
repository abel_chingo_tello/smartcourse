
<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-04-2018
 * @copyright	Copyright (C) 17-04-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_cursocategoria', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_cursocategoria 
{
	protected $id;
	protected $idcategoria;
	protected $idsubcategoria;
	protected $idcurso;
	
	protected $dataAcad_cursocategoria;
	protected $oDatAcad_cursocategoria;	

	public function __construct()
	{
		$this->oDatAcad_cursocategoria = new DatAcad_cursocategoria;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_cursocategoria->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_cursocategoria->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_cursocategoria->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	/**
	 * Funcion generica con busqueda relacional para encontrar las asignaciones de categorias en los cursos de un determinado proyecto
	 * @param Array Filtros de busquedas
	 * @return Array El resultado de la consulta SQL
	 */
	public function buscarcompleto($filtros = []){
		try {
			return $this->oDatAcad_cursocategoria->buscarcompleto($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarCursos($filtros = array())
	{
		try {
			return $this->oDatAcad_cursocategoria->buscarCursos($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_cursocategoria->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_cursocategoria->get($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursocategoria', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_cursocategoria->iniciarTransaccion('neg_i_Acad_cursocategoria');
			$this->id = $this->oDatAcad_cursocategoria->insertar($this->idcategoria,$this->idsubcategoria,$this->idcurso);
			$this->oDatAcad_cursocategoria->terminarTransaccion('neg_i_Acad_cursocategoria');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatAcad_cursocategoria->cancelarTransaccion('neg_i_Acad_cursocategoria');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursocategoria', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_cursocategoria->actualizar($this->id,$this->idcategoria,$this->idsubcategoria,$this->idcurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_cursocategoria', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_cursocategoria->eliminar($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataAcad_cursocategoria = $this->oDatAcad_cursocategoria->get($pk);
			if(empty($this->dataAcad_cursocategoria)) {
				throw new Exception(JrTexto::_("Acad_cursocategoria").' '.JrTexto::_("not registered"));
			}
			$this->id = $this->dataAcad_cursocategoria["id"];
			$this->idcategoria = $this->dataAcad_cursocategoria["idcategoria"];
			$this->idsubcategoria = $this->dataAcad_cursocategoria["idsubcategoria"];
			$this->idcurso = $this->dataAcad_cursocategoria["idcurso"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursocategoria', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_cursocategoria = $this->oDatAcad_cursocategoria->get($pk);
			if(empty($this->dataAcad_cursocategoria)) {
				throw new Exception(JrTexto::_("Acad_cursocategoria").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_cursocategoria->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}