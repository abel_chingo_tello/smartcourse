<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-09-2019
 * @copyright	Copyright (C) 12-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatZoomalumnos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegZoomalumnos 
{
	protected $idzoomalumno;
	protected $idzoom;
	protected $idalumno;
	
	protected $dataZoomalumnos;
	protected $oDatZoomalumnos;	

	public function __construct()
	{
		$this->oDatZoomalumnos = new DatZoomalumnos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatZoomalumnos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatZoomalumnos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatZoomalumnos->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatZoomalumnos->get($this->idzoomalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('zoomalumnos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatZoomalumnos->iniciarTransaccion('neg_i_Zoomalumnos');
			$this->idzoomalumno = $this->oDatZoomalumnos->insertar($this->idzoom,$this->idalumno);
			$this->oDatZoomalumnos->terminarTransaccion('neg_i_Zoomalumnos');	
			return $this->idzoomalumno;
		} catch(Exception $e) {	
		    $this->oDatZoomalumnos->cancelarTransaccion('neg_i_Zoomalumnos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('zoomalumnos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatZoomalumnos->actualizar($this->idzoomalumno,$this->idzoom,$this->idalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Zoomalumnos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatZoomalumnos->eliminar($this->idzoomalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdzoomalumno($pk){
		try {
			$this->dataZoomalumnos = $this->oDatZoomalumnos->get($pk);
			if(empty($this->dataZoomalumnos)) {
				throw new Exception(JrTexto::_("Zoomalumnos").' '.JrTexto::_("not registered"));
			}
			$this->idzoomalumno = $this->dataZoomalumnos["idzoomalumno"];
			$this->idzoom = $this->dataZoomalumnos["idzoom"];
			$this->idalumno = $this->dataZoomalumnos["idalumno"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('zoomalumnos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataZoomalumnos = $this->oDatZoomalumnos->get($pk);
			if(empty($this->dataZoomalumnos)) {
				throw new Exception(JrTexto::_("Zoomalumnos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatZoomalumnos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}