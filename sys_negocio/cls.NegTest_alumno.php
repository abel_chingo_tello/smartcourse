<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		06-02-2019
 * @copyright	Copyright (C) 06-02-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTest_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTest_alumno 
{
	protected $idtestalumno;
	protected $idalumno;
	protected $idtest;
	protected $idtestcriterio;
	protected $idtestasigancion;
	protected $puntaje;
	
	protected $dataTest_alumno;
	protected $oDatTest_alumno;	

	public function __construct()
	{
		$this->oDatTest_alumno = new DatTest_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTest_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTest_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTest_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTest_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTest_alumno->get($this->idtestalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatTest_alumno->iniciarTransaccion('neg_i_Test_alumno');
			$this->idtestalumno = $this->oDatTest_alumno->insertar($this->idalumno,$this->idtest,$this->idtestcriterio,$this->idtestasigancion,$this->puntaje);
			$this->oDatTest_alumno->terminarTransaccion('neg_i_Test_alumno');	
			return $this->idtestalumno;
		} catch(Exception $e) {	
		    $this->oDatTest_alumno->cancelarTransaccion('neg_i_Test_alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatTest_alumno->actualizar($this->idtestalumno,$this->idalumno,$this->idtest,$this->idtestcriterio,$this->idtestasigancion,$this->puntaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Test_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTest_alumno->eliminar($this->idtestalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtestalumno($pk){
		try {
			$this->dataTest_alumno = $this->oDatTest_alumno->get($pk);
			if(empty($this->dataTest_alumno)) {
				throw new Exception(JrTexto::_("Test_alumno").' '.JrTexto::_("not registered"));
			}
			$this->idtestalumno = $this->dataTest_alumno["idtestalumno"];
			$this->idalumno = $this->dataTest_alumno["idalumno"];
			$this->idtest = $this->dataTest_alumno["idtest"];
			$this->idtestcriterio = $this->dataTest_alumno["idtestcriterio"];
			$this->idtestasigancion = $this->dataTest_alumno["idtestasigancion"];
			$this->puntaje = $this->dataTest_alumno["puntaje"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('test_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTest_alumno = $this->oDatTest_alumno->get($pk);
			if(empty($this->dataTest_alumno)) {
				throw new Exception(JrTexto::_("Test_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTest_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}