<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-04-2020
 * @copyright	Copyright (C) 24-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRubrica_criterio', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRubrica_criterio 
{
	protected $idrubrica_criterio;
	protected $detalle;
	protected $peso;
	protected $idrubrica;
	protected $idcapacidad;
	
	protected $dataRubrica_criterio;
	protected $oDatRubrica_criterio;	

	public function __construct()
	{
		$this->oDatRubrica_criterio = new DatRubrica_criterio;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRubrica_criterio->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRubrica_criterio->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRubrica_criterio->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRubrica_criterio->get($this->idrubrica_criterio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_criterio', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRubrica_criterio->iniciarTransaccion('neg_i_Rubrica_criterio');
			$this->idrubrica_criterio = $this->oDatRubrica_criterio->insertar($this->detalle,$this->peso,$this->idrubrica,$this->idcapacidad);
			$this->oDatRubrica_criterio->terminarTransaccion('neg_i_Rubrica_criterio');	
			return $this->idrubrica_criterio;
		} catch(Exception $e) {	
		    $this->oDatRubrica_criterio->cancelarTransaccion('neg_i_Rubrica_criterio');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_criterio', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRubrica_criterio->actualizar($this->idrubrica_criterio,$this->detalle,$this->peso,$this->idrubrica,$this->idcapacidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rubrica_criterio', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica_criterio->eliminar($this->idrubrica_criterio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrubrica_criterio($pk){
		try {
			$this->dataRubrica_criterio = $this->oDatRubrica_criterio->get($pk);
			if(empty($this->dataRubrica_criterio)) {
				throw new Exception(JrTexto::_("Rubrica_criterio").' '.JrTexto::_("not registered"));
			}
			$this->idrubrica_criterio = $this->dataRubrica_criterio["idrubrica_criterio"];
			$this->detalle = $this->dataRubrica_criterio["detalle"];
			$this->peso = $this->dataRubrica_criterio["peso"];
			$this->idrubrica = $this->dataRubrica_criterio["idrubrica"];
			$this->idcapacidad = $this->dataRubrica_criterio["idcapacidad"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_criterio', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRubrica_criterio = $this->oDatRubrica_criterio->get($pk);
			if(empty($this->dataRubrica_criterio)) {
				throw new Exception(JrTexto::_("Rubrica_criterio").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRubrica_criterio->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}