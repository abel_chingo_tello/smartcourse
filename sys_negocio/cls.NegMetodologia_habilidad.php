<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		14-11-2016
 * @copyright	Copyright (C) 14-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMetodologia_habilidad', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegMetodologia_habilidad 
{
	protected $idmetodologia;
	protected $nombre;
	protected $tipo;
	protected $estado;
	
	protected $dataMetodologia_habilidad;
	protected $oDatMetodologia_habilidad;	

	public function __construct()
	{
		$this->oDatMetodologia_habilidad = new DatMetodologia_habilidad;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMetodologia_habilidad->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatMetodologia_habilidad->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMetodologia_habilidad->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMetodologia_habilidad->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMetodologia_habilidad->get($this->idmetodologia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('metodologia_habilidad', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatMetodologia_habilidad->iniciarTransaccion('neg_i_Metodologia_habilidad');
			$this->idmetodologia = $this->oDatMetodologia_habilidad->insertar($this->nombre,$this->tipo,$this->estado);
			//$this->oDatMetodologia_habilidad->terminarTransaccion('neg_i_Metodologia_habilidad');	
			return $this->idmetodologia;
		} catch(Exception $e) {	
		   //$this->oDatMetodologia_habilidad->cancelarTransaccion('neg_i_Metodologia_habilidad');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('metodologia_habilidad', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatMetodologia_habilidad->actualizar($this->idmetodologia,$this->nombre,$this->tipo,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatMetodologia_habilidad->cambiarvalorcampo($this->idmetodologia,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Metodologia_habilidad', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatMetodologia_habilidad->eliminar($this->idmetodologia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmetodologia($pk){
		try {
			$this->dataMetodologia_habilidad = $this->oDatMetodologia_habilidad->get($pk);
			if(empty($this->dataMetodologia_habilidad)) {
				throw new Exception(JrTexto::_("Metodologia_habilidad").' '.JrTexto::_("not registered"));
			}
			$this->idmetodologia = $this->dataMetodologia_habilidad["idmetodologia"];
			$this->nombre = $this->dataMetodologia_habilidad["nombre"];
			$this->tipo = $this->dataMetodologia_habilidad["tipo"];
			$this->estado = $this->dataMetodologia_habilidad["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('metodologia_habilidad', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataMetodologia_habilidad = $this->oDatMetodologia_habilidad->get($pk);
			if(empty($this->dataMetodologia_habilidad)) {
				throw new Exception(JrTexto::_("Metodologia_habilidad").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMetodologia_habilidad->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	/*private function setNombre($nombre)
	{
		try {
			$this->nombre= NegTools::validar('todo', $nombre, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTipo($tipo)
	{
		try {
			$this->tipo= NegTools::validar('todo', $tipo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setEstado($estado)
	{
		try {
			$this->estado= NegTools::validar('todo', $estado, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}*/
		
}