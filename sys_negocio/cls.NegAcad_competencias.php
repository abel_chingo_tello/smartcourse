<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-02-2020
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_competencias', RUTA_BASE);
JrCargador::clase('sys_datos::DatAcad_capacidades', RUTA_BASE);
JrCargador::clase('sys_datos::DatAcad_criterios', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_competencias 
{
	protected $idcompetencia;
	protected $idcategoria;
	protected $nombre;
	protected $estado;
	protected $idproyecto;
	
	protected $dataAcad_competencias;
	protected $oDatAcad_competencias;	
	protected $oDatAcad_capacidades;	
	protected $oDatAcad_criterios;	

	public function __construct()
	{
		$this->oDatAcad_competencias = new DatAcad_competencias;
		$this->oDatAcad_capacidades = new DatAcad_capacidades;
		$this->oDatAcad_criterios = new DatAcad_criterios;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_competencias->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_competencias->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	/**
	 * Funcion que se encarga de retornar las competencias junto a sus capacidades y criterios
	 * @param Array Filtro de busqueda en base a la competencia
	 * @return Array resultado de la busqueda
	 */
	public function competencias($filtros = array()){
		try {
			$arrResult = [];
			$this->setLimite(0,1000);
			$rs_competencia = $this->oDatAcad_competencias->buscar($filtros);
			if(!empty($rs_competencia)){
				$filtros_capacidades = [];
				$filtros_criterios = [];
				if(!empty($filtros['idcurso'])){ 
					$filtros_capacidades['idcurso'] = $filtros['idcurso'];
					$filtros_criterios['idcurso'] = $filtros['idcurso'];
				}
				foreach($rs_competencia as $competencia){
					$prepare = [];
					$filtros_capacidades['idcompetencia'] = $competencia['idcompetencia'];
					$rs_capacidades = $this->oDatAcad_capacidades->buscar($filtros_capacidades);
					$competencia['capacidades'] = array();
					if(!empty($rs_capacidades)){
						$competencia['capacidades'] = $rs_capacidades;
						foreach($rs_capacidades as $key_cap => $capacidad){
							$competencia['capacidades'][$key_cap]['criterios'] = array();
							$filtros_criterios['idcapacidad'] = $capacidad['idcapacidad'];
							$rs_criterios = $this->oDatAcad_criterios->buscar($filtros_criterios);
							if(!empty($rs_criterios)){
								$competencia['capacidades'][$key_cap]['criterios'] = $rs_criterios;
							}
						}//endforeach
					}//endif !empty capacidades
					$arrResult[] = $competencia;
				}//endforeach
			}//endif;
			
			return $arrResult;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_competencias->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_competencias->get($this->idcompetencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_competencias', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_competencias->iniciarTransaccion('neg_i_Acad_competencias');
			$this->idcompetencia = $this->oDatAcad_competencias->insertar($this->idcategoria,$this->nombre,$this->estado,$this->idproyecto);
			$this->oDatAcad_competencias->terminarTransaccion('neg_i_Acad_competencias');	
			return $this->idcompetencia;
		} catch(Exception $e) {	
		    $this->oDatAcad_competencias->cancelarTransaccion('neg_i_Acad_competencias');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_competencias', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_competencias->actualizar($this->idcompetencia,$this->idcategoria,$this->nombre,$this->estado,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAcad_competencias->cambiarvalorcampo($this->idcompetencia,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_competencias', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_competencias->eliminar($this->idcompetencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcompetencia($pk){
		try {
			$this->dataAcad_competencias = $this->oDatAcad_competencias->get($pk);
			if(empty($this->dataAcad_competencias)) {
				throw new Exception(JrTexto::_("Acad_competencias").' '.JrTexto::_("not registered"));
			}
			$this->idcompetencia = $this->dataAcad_competencias["idcompetencia"];
			$this->idcategoria = $this->dataAcad_competencias["idcategoria"];
			$this->nombre = $this->dataAcad_competencias["nombre"];
			$this->estado = $this->dataAcad_competencias["estado"];
			$this->idproyecto = $this->dataAcad_competencias["idproyecto"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_competencias', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_competencias = $this->oDatAcad_competencias->get($pk);
			if(empty($this->dataAcad_competencias)) {
				throw new Exception(JrTexto::_("Acad_competencias").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_competencias->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}