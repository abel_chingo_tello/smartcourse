<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-02-2020
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_capacidades', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAcad_capacidades 
{
	protected $idcapacidad;
	protected $idcompetencia;
	protected $idcurso;
	protected $nombre;
	protected $idproyecto;
	
	protected $dataAcad_capacidades;
	protected $oDatAcad_capacidades;	

	public function __construct()
	{
		$this->oDatAcad_capacidades = new DatAcad_capacidades;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_capacidades->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_capacidades->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_capacidades->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_capacidades->get($this->idcapacidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_capacidades', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_capacidades->iniciarTransaccion('neg_i_Acad_capacidades');
			$this->idcapacidad = $this->oDatAcad_capacidades->insertar($this->idcompetencia,$this->idcurso,$this->nombre,$this->idproyecto);
			$this->oDatAcad_capacidades->terminarTransaccion('neg_i_Acad_capacidades');	
			return $this->idcapacidad;
		} catch(Exception $e) {	
		    $this->oDatAcad_capacidades->cancelarTransaccion('neg_i_Acad_capacidades');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_capacidades', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_capacidades->actualizar($this->idcapacidad,$this->idcompetencia,$this->idcurso,$this->nombre,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_capacidades', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_capacidades->eliminar($this->idcapacidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcapacidad($pk){
		try {
			$this->dataAcad_capacidades = $this->oDatAcad_capacidades->get($pk);
			if(empty($this->dataAcad_capacidades)) {
				throw new Exception(JrTexto::_("Acad_capacidades").' '.JrTexto::_("not registered"));
			}
			$this->idcapacidad = $this->dataAcad_capacidades["idcapacidad"];
			$this->idcompetencia = $this->dataAcad_capacidades["idcompetencia"];
			$this->idcurso = $this->dataAcad_capacidades["idcurso"];
			$this->nombre = $this->dataAcad_capacidades["nombre"];
			$this->idproyecto = $this->dataAcad_capacidades["idproyecto"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_capacidades', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_capacidades = $this->oDatAcad_capacidades->get($pk);
			if(empty($this->dataAcad_capacidades)) {
				throw new Exception(JrTexto::_("Acad_capacidades").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_capacidades->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}