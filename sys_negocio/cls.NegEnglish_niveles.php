<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-03-2017
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEnglish_niveles', RUTA_BASE);
class NegEnglish_niveles 
{
	
	protected $dataExamenes;
	protected $oDatEnglish_niveles;	

	public function __construct($BD='')
	{
		$this->oDatEnglish_niveles = new DatEnglish_niveles($BD);
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			$this->oDatEnglish_niveles->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros=array())
	{
		try {
			return $this->oDatEnglish_niveles->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
}