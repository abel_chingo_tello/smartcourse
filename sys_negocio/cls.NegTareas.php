<?php

/**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-02-2020
 * @copyright	Copyright (C) 11-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTareas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegTareas
{
	protected $idtarea;
	public $tipo;
	protected $idcurso;
	protected $idsesion;
	protected $idpestania;
	public $idalumno;
	protected $recurso;
	protected $tiporecurso;
	protected $nota;
	protected $notabaseen;
	protected $iddocente;
	protected $idproyecto;
	public $nombre;
	protected $descripcion;
	protected $fecha_calificacion;
	protected $criterios;
	protected $criterios_puntaje;
	protected $idcomplementario;
	protected $idgrupoauladetalle;

	protected $dataTareas;
	protected $oDatTareas;

	public function __construct()
	{
		$this->oDatTareas = new DatTareas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if (is_array($prop)) {
			foreach ($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if (method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatTareas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	} ////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			$this->setLimite(0,9999990);
			return $this->oDatTareas->buscar($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function eliminarTodo($filtros = array())
	{
		try {
			return $this->oDatTareas->eliminarTodo($filtros);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatTareas->listarall();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTareas->get($this->idtarea);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tareas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatTareas->iniciarTransaccion('neg_i_Tareas');
			$this->idtarea = $this->oDatTareas->insertar($this->tipo, $this->idcurso, $this->idsesion, $this->idpestania, $this->idalumno, $this->recurso, $this->tiporecurso, $this->nota, $this->notabaseen, $this->iddocente, $this->idproyecto, $this->nombre, $this->descripcion, $this->fecha_calificacion, $this->criterios, $this->idcomplementario,$this->idgrupoauladetalle);
			$this->oDatTareas->terminarTransaccion('neg_i_Tareas');
			return $this->idtarea;
		} catch (Exception $e) {
			$this->oDatTareas->cancelarTransaccion('neg_i_Tareas');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tareas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTareas->actualizar($this->idtarea, $this->tipo, $this->idcurso, $this->idsesion, $this->idpestania, $this->idalumno, $this->recurso, $this->tiporecurso, $this->nota, $this->notabaseen, $this->iddocente, $this->idproyecto, $this->nombre, $this->descripcion, $this->fecha_calificacion, $this->criterios, $this->criterios_puntaje,$this->idcomplementario,$this->idgrupoauladetalle);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Tareas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTareas->eliminar($this->idtarea);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtarea($pk)
	{
		try {
			$this->dataTareas = $this->oDatTareas->get($pk);
			if (empty($this->dataTareas)) {
				throw new Exception(JrTexto::_("Tareas") . ' ' . JrTexto::_("not registered"));
			}
			$this->idtarea = $this->dataTareas["idtarea"];
			$this->tipo = $this->dataTareas["tipo"];
			$this->idcurso = $this->dataTareas["idcurso"];
			$this->idsesion = $this->dataTareas["idsesion"];
			$this->idpestania = $this->dataTareas["idpestania"];
			$this->idalumno = $this->dataTareas["idalumno"];
			$this->recurso = $this->dataTareas["recurso"];
			$this->tiporecurso = $this->dataTareas["tiporecurso"];
			$this->nota = $this->dataTareas["nota"];
			$this->notabaseen = $this->dataTareas["notabaseen"];
			$this->iddocente = $this->dataTareas["iddocente"];
			$this->idproyecto = $this->dataTareas["idproyecto"];
			$this->nombre = $this->dataTareas["nombre"];
			$this->descripcion = $this->dataTareas["descripcion"];
			$this->fecha_calificacion = $this->dataTareas["fecha_calificacion"];
			$this->criterios = $this->dataTareas["criterios"];
			$this->criterios_puntaje = $this->dataTareas["criterios_puntaje"];
			$this->idcomplementario = $this->dataTareas["idcomplementario"];
			$this->idgrupoauladetalle = $this->dataTareas["idgrupoauladetalle"];
			//falta campos
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('tareas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTareas = $this->oDatTareas->get($pk);
			if (empty($this->dataTareas)) {
				throw new Exception(JrTexto::_("Tareas") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatTareas->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setUpdateNota($pk, $propiedad, $valor)
	{
		try {
			/*if(!NegSesion::tiene_acceso('tareas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTareas = $this->oDatTareas->get($pk);
			if (empty($this->dataTareas)) {
				throw new Exception(JrTexto::_("Tareas") . ' ' . JrTexto::_("not registered"));
			}

			return $this->oDatTareas->set($pk, $propiedad, $valor);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
