<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_datos::DatSence_cursovalor', RUTA_BASE, 'sys_datos');

class NegSence_cursovalor{
    protected $oDatSence_cursovalor;

    public function __construct(){
        $this->oDatSence_cursovalor = new DatSence_cursovalor;
    }
    public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}
	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatSence_cursovalor->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
    }
    /**
     * Funcion generica para buscar en la tabla especifica sin datos relacionados
     * Por favor no modificar tanto la logica de la función y si es necesaria, delimitar consulta mediante un parametro especifico
     * @param Array Son los filtros del a consulta donde se condiciona la busqueda
     * @return Array El resultado de la consulta
     */
    public function buscar($filtros = null){
        try{
            return $this->oDatSence_cursovalor->buscar($filtros);
        }catch(Exception $e){
            throw new Exception("Error en buscar: ".$e->getMessage());
        }
    }
    /**
     * Funcion generica para insertar registro en la tabla, se puede registrar 1 a N cantidad de registro ya que actua de manera masiva
     * La función esta destinada a insertar como tabla unica, si se requiere hacer un insertado mas complejo, por favor hacer otra función
     * ya que esta función es generica y se puede reutilizar en diferentes modulos del sistema
     * @param Array los registro a procesar
     * @return Integer Retorna el ultimo id insertado
     */
    public function insert($data){
        try{
            if(empty($data)){ throw new Exception("Data esta vacio"); }
            return $this->oDatSence_cursovalor->insert($data);
        }catch(Exception $e){
            throw new Exception("Error en insert: ".$e->getMessage());
        }
    }
    /**
     * Funcion generica para actualizar registro en la tabla, se puede actualizar un unico registro como varios, ya que actua de manera masiva el proceso.
     * La funcion esta destinada solo a actualizar registros sin logica especial, si se requiere un proceso mas complejo, por favor crea otra funcion preferiblemente
     * Este proceso es reutilizado por varios modulos del sistema, y un cambio drastico puede causar fallas
     * @param Array los registro a procesar
     * @param Boolean verifica si es estricto las validaciones, si esta estricto y no posee el identificador del registro, lanzara un throw Exception
     * @return Boolean Resultado en TRUE si se realizo el proceso completamente, FALSE algo ocurrio
     */
    public function update($data,$esEstricto = false){
        try{
            if(empty($data)){ throw new Exception("Data esta vacio"); }
            return $this->oDatSence_cursovalor->update($data,$esEstricto);
        }catch(Exception $e){
            throw new Exception("Error en update: ".$e->getMessage());
        }
    }
    public function delete($data){}
}

?>