<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRecursivo', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRecursivo 
{
	protected $idrecursivo;
	protected $comprobar;
	protected $fechaejecucion;
	protected $fechafinalizacion;
	
	protected $dataRecursivo;
	protected $oDatRecursivo;	

	public function __construct()
	{
		$this->oDatRecursivo = new DatRecursivo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRecursivo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRecursivo->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRecursivo->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRecursivo->get($this->idrecursivo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursivo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRecursivo->iniciarTransaccion('neg_i_Recursivo');
			$this->idrecursivo = $this->oDatRecursivo->insertar($this->comprobar,$this->fechaejecucion,$this->fechafinalizacion);
			$this->oDatRecursivo->terminarTransaccion('neg_i_Recursivo');	
			return $this->idrecursivo;
		} catch(Exception $e) {	
		    $this->oDatRecursivo->cancelarTransaccion('neg_i_Recursivo');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('recursivo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRecursivo->actualizar($this->idrecursivo,$this->comprobar,$this->fechaejecucion,$this->fechafinalizacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Recursivo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRecursivo->eliminar($this->idrecursivo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecursivo($pk){
		try {
			$this->dataRecursivo = $this->oDatRecursivo->get($pk);
			if(empty($this->dataRecursivo)) {
				throw new Exception(JrTexto::_("Recursivo").' '.JrTexto::_("not registered"));
			}
			$this->idrecursivo = $this->dataRecursivo["idrecursivo"];
			$this->comprobar = $this->dataRecursivo["comprobar"];
			$this->fechaejecucion = $this->dataRecursivo["fechaejecucion"];
			$this->fechafinalizacion = $this->dataRecursivo["fechafinalizacion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('recursivo', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRecursivo = $this->oDatRecursivo->get($pk);
			if(empty($this->dataRecursivo)) {
				throw new Exception(JrTexto::_("Recursivo").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRecursivo->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}