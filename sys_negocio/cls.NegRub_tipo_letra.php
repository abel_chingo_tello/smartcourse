<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-09-2019
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRub_tipo_letra', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRub_tipo_letra 
{
	protected $id_tipoletra;
	protected $descripcion;
	protected $activo;
	
	protected $dataRub_tipo_letra;
	protected $oDatRub_tipo_letra;	

	public function __construct()
	{
		$this->oDatRub_tipo_letra = new DatRub_tipo_letra;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRub_tipo_letra->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRub_tipo_letra->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRub_tipo_letra->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRub_tipo_letra->get($this->config);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_tipo_letra', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRub_tipo_letra->iniciarTransaccion('neg_i_Rub_tipo_letra');
			$this->config = $this->oDatRub_tipo_letra->insertar($this->id_tipoletra,$this->descripcion,$this->activo);
			$this->oDatRub_tipo_letra->terminarTransaccion('neg_i_Rub_tipo_letra');	
			return $this->config;
		} catch(Exception $e) {	
		    $this->oDatRub_tipo_letra->cancelarTransaccion('neg_i_Rub_tipo_letra');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rub_tipo_letra', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRub_tipo_letra->actualizar($this->config,$this->id_tipoletra,$this->descripcion,$this->activo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rub_tipo_letra', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRub_tipo_letra->eliminar($this->config);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setConfig($pk){
		try {
			$this->dataRub_tipo_letra = $this->oDatRub_tipo_letra->get($pk);
			if(empty($this->dataRub_tipo_letra)) {
				throw new Exception(JrTexto::_("Rub_tipo_letra").' '.JrTexto::_("not registered"));
			}
			$this->id_tipoletra = $this->dataRub_tipo_letra["id_tipoletra"];
			$this->descripcion = $this->dataRub_tipo_letra["descripcion"];
			$this->activo = $this->dataRub_tipo_letra["activo"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rub_tipo_letra', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRub_tipo_letra = $this->oDatRub_tipo_letra->get($pk);
			if(empty($this->dataRub_tipo_letra)) {
				throw new Exception(JrTexto::_("Rub_tipo_letra").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRub_tipo_letra->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}