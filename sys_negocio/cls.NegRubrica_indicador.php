<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-04-2020
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRubrica_indicador', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRubrica_indicador 
{
	protected $idrubrica_indicador;
	protected $detalle;
	protected $idrubrica;
	protected $idrubrica_criterio;
	protected $idrubrica_nivel;
	protected $idcriterio;
	
	protected $dataRubrica_indicador;
	protected $oDatRubrica_indicador;	

	public function __construct()
	{
		$this->oDatRubrica_indicador = new DatRubrica_indicador;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRubrica_indicador->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRubrica_indicador->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRubrica_indicador->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRubrica_indicador->get($this->idrubrica_indicador);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_indicador', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatRubrica_indicador->iniciarTransaccion('neg_i_Rubrica_indicador');
			$this->idrubrica_indicador = $this->oDatRubrica_indicador->insertar($this->detalle,$this->idrubrica,$this->idrubrica_criterio,$this->idrubrica_nivel,$this->idcriterio);
			$this->oDatRubrica_indicador->terminarTransaccion('neg_i_Rubrica_indicador');	
			return $this->idrubrica_indicador;
		} catch(Exception $e) {	
		    $this->oDatRubrica_indicador->cancelarTransaccion('neg_i_Rubrica_indicador');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_indicador', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatRubrica_indicador->actualizar($this->idrubrica_indicador,$this->detalle,$this->idrubrica,$this->idrubrica_criterio,$this->idrubrica_nivel,$this->idcriterio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rubrica_indicador', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRubrica_indicador->eliminar($this->idrubrica_indicador);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrubrica_indicador($pk){
		try {
			$this->dataRubrica_indicador = $this->oDatRubrica_indicador->get($pk);
			if(empty($this->dataRubrica_indicador)) {
				throw new Exception(JrTexto::_("Rubrica_indicador").' '.JrTexto::_("not registered"));
			}
			$this->idrubrica_indicador = $this->dataRubrica_indicador["idrubrica_indicador"];
			$this->detalle = $this->dataRubrica_indicador["detalle"];
			$this->idrubrica = $this->dataRubrica_indicador["idrubrica"];
			$this->idrubrica_criterio = $this->dataRubrica_indicador["idrubrica_criterio"];
			$this->idrubrica_nivel = $this->dataRubrica_indicador["idrubrica_nivel"];
			$this->idcriterio = $this->dataRubrica_indicador["idcriterio"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rubrica_indicador', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRubrica_indicador = $this->oDatRubrica_indicador->get($pk);
			if(empty($this->dataRubrica_indicador)) {
				throw new Exception(JrTexto::_("Rubrica_indicador").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRubrica_indicador->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}