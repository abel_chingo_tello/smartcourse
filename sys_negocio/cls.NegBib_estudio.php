<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_estudio', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegBib_estudio 
{
	protected $id_estudio;
	protected $paginas;
	protected $codigo_tipo;
	protected $codigo;
	protected $nombre;
	protected $nombre_abreviado;
	protected $precio;
	protected $edicion;
	protected $fec_publicacion;
	protected $fec_creacion;
	protected $fec_modificacion;
	protected $foto;
	protected $archivo;
	protected $link;
	protected $lugar;
	protected $condicion;
	protected $resumen;
	protected $id_idioma;
	protected $id_tipo;
	protected $id_detalle;
	protected $id_setting;
	protected $id_editorial;
	protected $padre;
	protected $orden;
	
	protected $dataBib_estudio;
	protected $oDatBib_estudio;	

	public function __construct()
	{
		$this->oDatBib_estudio = new DatBib_estudio;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_estudio->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscaraudios($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->buscaraudios($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarmaterial($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->buscarmaterial($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarmusica($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->buscarmusica($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarAvanzada($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->buscarAvanzada($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarFavoritos($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->buscarFavoritos($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarDescargados($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->buscarDescargados($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarAudiolibros($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->buscarAudiolibros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function reporte1($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->reporte1($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function reporte2($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->reporte2($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function Audiolibros($filtros = array())
	{
		try {
			return $this->oDatBib_estudio->Audiolibros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatBib_estudio->listarall($this->nombre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_estudio->get($this->id_estudio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_estudio', 'add')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_estudio->iniciarTransaccion('neg_i_Bib_estudio');
			$this->id_estudio = $this->oDatBib_estudio->insertar($this->paginas,$this->codigo_tipo,$this->codigo,$this->nombre,$this->nombre_abreviado,$this->precio,$this->edicion,$this->fec_publicacion,$this->fec_creacion,$this->fec_modificacion,$this->foto,$this->archivo,$this->link,$this->lugar,$this->condicion,$this->resumen,$this->id_idioma,$this->id_tipo,$this->id_detalle,$this->id_setting,$this->id_editorial,$this->padre,$this->orden);
			//$this->oDatBib_estudio->terminarTransaccion('neg_i_Bib_estudio');	
			return $this->id_estudio;
		} catch(Exception $e) {	
		   //$this->oDatBib_estudio->cancelarTransaccion('neg_i_Bib_estudio');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_estudio', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatBib_estudio->actualizar($this->id_estudio,$this->paginas,$this->codigo_tipo,$this->codigo,$this->nombre,$this->nombre_abreviado,$this->precio,$this->edicion,$this->fec_publicacion,$this->fec_creacion,$this->fec_modificacion,$this->foto,$this->archivo,$this->link,$this->lugar,$this->condicion,$this->resumen,$this->id_idioma,$this->id_tipo,$this->id_detalle,$this->id_setting,$this->id_editorial,$this->padre,$this->orden);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($id)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_estudio', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_estudio->eliminar($id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_estudio($pk){
		try {
			$this->dataBib_estudio = $this->oDatBib_estudio->get($pk);
			if(empty($this->dataBib_estudio)) {
				throw new Exception(JrTexto::_("Bib_estudio").' '.JrTexto::_("not registered"));
			}
			$this->id_estudio = $this->dataBib_estudio["id_estudio"];
			$this->paginas = $this->dataBib_estudio["paginas"];
			$this->codigo_tipo = $this->dataBib_estudio["codigo_tipo"];
			$this->codigo = $this->dataBib_estudio["codigo"];
			$this->nombre = $this->dataBib_estudio["nombre"];
			$this->nombre_abreviado = $this->dataBib_estudio["nombre_abreviado"];
			$this->precio = $this->dataBib_estudio["precio"];
			$this->edicion = $this->dataBib_estudio["edicion"];
			$this->fec_publicacion = $this->dataBib_estudio["fec_publicacion"];
			$this->fec_creacion = $this->dataBib_estudio["fec_creacion"];
			$this->fec_modificacion = $this->dataBib_estudio["fec_modificacion"];
			$this->foto = $this->dataBib_estudio["foto"];
			$this->archivo = $this->dataBib_estudio["archivo"];
			$this->link = $this->dataBib_estudio["link"];
			$this->lugar = $this->dataBib_estudio["lugar"];
			$this->condicion = $this->dataBib_estudio["condicion"];
			$this->resumen = $this->dataBib_estudio["resumen"];
			$this->id_idioma = $this->dataBib_estudio["id_idioma"];
			$this->id_tipo = $this->dataBib_estudio["id_tipo"];
			$this->id_detalle = $this->dataBib_estudio["id_detalle"];
			$this->id_setting = $this->dataBib_estudio["id_setting"];
			$this->id_editorial = $this->dataBib_estudio["id_editorial"];
			$this->padre = $this->dataBib_estudio["padre"];
			$this->orden = $this->dataBib_estudio["orden"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_estudio', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_estudio = $this->oDatBib_estudio->get($pk);
			if(empty($this->dataBib_estudio)) {
				throw new Exception(JrTexto::_("Bib_estudio").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_estudio->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}	
	public function  eliminarbookstabla($tabla,$campo){
		if(!empty($tabla)&&!empty($campo)){
			$this->setLimite(0, 5000);
		 return $this->oDatBib_estudio->eliminarbookstabla($tabla,$campo);		
		}
	}
}