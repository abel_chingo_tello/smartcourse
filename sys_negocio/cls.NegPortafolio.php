<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-05-2020
 * @copyright	Copyright (C) 04-05-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPortafolio', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegPortafolio 
{
	protected $idportafolio;
	protected $idalumno;
	protected $idproyecto;
	
	protected $dataPortafolio;
	protected $oDatPortafolio;	

	public function __construct()
	{
		$this->oDatPortafolio = new DatPortafolio;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPortafolio->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPortafolio->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPortafolio->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPortafolio->get($this->idportafolio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('portafolio', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPortafolio->iniciarTransaccion('neg_i_Portafolio');
			$this->idportafolio = $this->oDatPortafolio->insertar($this->idalumno,$this->idproyecto);
			$this->oDatPortafolio->terminarTransaccion('neg_i_Portafolio');	
			return $this->idportafolio;
		} catch(Exception $e) {	
		    $this->oDatPortafolio->cancelarTransaccion('neg_i_Portafolio');		
			throw new Exception($e->getMessage());
		}
	}

	public function guardarEstilo($filtros)
	{
		try {
			return $this->oDatPortafolio->guardarEstilo($filtros);
		} catch(Exception $e) {	
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('portafolio', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPortafolio->actualizar($this->idportafolio,$this->idalumno,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Portafolio', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPortafolio->eliminar($this->idportafolio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdportafolio($pk){
		try {
			$this->dataPortafolio = $this->oDatPortafolio->get($pk);
			if(empty($this->dataPortafolio)) {
				throw new Exception(JrTexto::_("Portafolio").' '.JrTexto::_("not registered"));
			}
			$this->idportafolio = $this->dataPortafolio["idportafolio"];
			$this->idalumno = $this->dataPortafolio["idalumno"];
			$this->idproyecto = $this->dataPortafolio["idproyecto"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('portafolio', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPortafolio = $this->oDatPortafolio->get($pk);
			if(empty($this->dataPortafolio)) {
				throw new Exception(JrTexto::_("Portafolio").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPortafolio->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}