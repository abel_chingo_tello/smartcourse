<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		25-10-2017
 * @copyright	Copyright (C) 25-10-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAgenda', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegAgenda 
{
	protected $id;
	protected $titulo;
	protected $descripcion;
	protected $color;
	protected $fecha_inicio;
	protected $fecha_final;
	protected $alumno_id;
	
	protected $dataAgenda;
	protected $oDatAgenda;	

	public function __construct()
	{
		$this->oDatAgenda = new DatAgenda;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAgenda->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAgenda->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAgenda->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAgenda->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAgenda->get($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('agenda', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAgenda->iniciarTransaccion('neg_i_Agenda');
			$this->id = $this->oDatAgenda->insertar($this->titulo,$this->descripcion,$this->color,$this->fecha_inicio,$this->fecha_final,$this->alumno_id);
			$this->oDatAgenda->terminarTransaccion('neg_i_Agenda');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatAgenda->cancelarTransaccion('neg_i_Agenda');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('agenda', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}		*/	
			return $this->oDatAgenda->actualizar($this->id,$this->titulo,$this->descripcion,$this->color,$this->fecha_inicio,$this->fecha_final,$this->alumno_id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($id)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Agenda', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAgenda->eliminar($id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataAgenda = $this->oDatAgenda->get($pk);
			if(empty($this->dataAgenda)) {
				throw new Exception(JrTexto::_("Agenda").' '.JrTexto::_("not registered"));
			}
			$this->id = $this->dataAgenda["id"];
			$this->titulo = $this->dataAgenda["titulo"];
			$this->descripcion = $this->dataAgenda["descripcion"];
			$this->color = $this->dataAgenda["color"];
			$this->fecha_inicio = $this->dataAgenda["fecha_inicio"];
			$this->fecha_final = $this->dataAgenda["fecha_final"];
			$this->alumno_id = $this->dataAgenda["alumno_id"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('agenda', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAgenda = $this->oDatAgenda->get($pk);
			if(empty($this->dataAgenda)) {
				throw new Exception(JrTexto::_("Agenda").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAgenda->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}