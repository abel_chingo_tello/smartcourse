<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-02-2021
 * @copyright	Copyright (C) 17-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotificacion_para', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegNotificacion_para 
{
	protected $idnotipersona;
	protected $idnotificacion;
	protected $idpersona;
	protected $estado;
	
	protected $dataNotificacion_para;
	protected $oDatNotificacion_para;	

	public function __construct()
	{
		$this->oDatNotificacion_para = new DatNotificacion_para;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNotificacion_para->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotificacion_para->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNotificacion_para->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotificacion_para->get($this->idnotipersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notificacion_para', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatNotificacion_para->iniciarTransaccion('neg_i_Notificacion_para');
			$this->idnotipersona = $this->oDatNotificacion_para->insertar($this->idnotificacion,$this->idpersona,$this->estado);
			$this->oDatNotificacion_para->terminarTransaccion('neg_i_Notificacion_para');	
			return $this->idnotipersona;
		} catch(Exception $e) {	
		    $this->oDatNotificacion_para->cancelarTransaccion('neg_i_Notificacion_para');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notificacion_para', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotificacion_para->actualizar($this->idnotipersona,$this->idnotificacion,$this->idpersona,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function actualizar($estados,$condicion)
	{
		try {
			/*if(!NegSesion::tiene_acceso('notificacion_de', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotificacion_para->actualizar2($estados,$condicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatNotificacion_para->cambiarvalorcampo($this->idnotipersona,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notificacion_para', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotificacion_para->eliminar($this->idnotipersona);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnotipersona($pk){
		try {
			$this->dataNotificacion_para = $this->oDatNotificacion_para->get($pk);
			if(empty($this->dataNotificacion_para)) {
				throw new Exception(JrTexto::_("Notificacion_para").' '.JrTexto::_("not registered"));
			}
			$this->idnotipersona = $this->dataNotificacion_para["idnotipersona"];
			$this->idnotificacion = $this->dataNotificacion_para["idnotificacion"];
			$this->idpersona = $this->dataNotificacion_para["idpersona"];
			$this->estado = $this->dataNotificacion_para["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('notificacion_para', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotificacion_para = $this->oDatNotificacion_para->get($pk);
			if(empty($this->dataNotificacion_para)) {
				throw new Exception(JrTexto::_("Notificacion_para").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNotificacion_para->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}