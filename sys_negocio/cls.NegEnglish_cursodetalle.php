<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-03-2017
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEnglish_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_datos::DatEnglish_niveles', RUTA_BASE);
class NegEnglish_cursodetalle 
{
	
	protected $dataExamenes;
	protected $oDatEnglish_cursodetalle;	

	public function __construct($BD='')
	{
		$this->oDatEnglish_cursodetalle = new DatEnglish_cursodetalle($BD);
		$this->oDatEnglish_Niveles = new DatEnglish_niveles($BD);
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			$this->oDatEnglish_cursodetalle->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros=array())
	{
		try {
			$this->setLimite(0,10000);
			return $this->oDatEnglish_cursodetalle->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarcurso($filtros=array())
	{
		try {
			$this->setLimite(0,10000);
			return $this->oDatEnglish_cursodetalle->buscarcurso($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function copiar($filtros=array())
	{
		try {			
			return $this->oDatEnglish_cursodetalle->copiar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function personaingles($user,$BD){
		try {			
			return $this->oDatEnglish_cursodetalle->personaingles($user,$BD);			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function solosesiones($idcurso,$idpadre,$BD,$nombresesion=''){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatEnglish_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre,'BD'=>$BD));
			$datos2=array();
			if(!empty($datos)){	
				$infonivel=array('L','M');
				$datosreturn=array();
				//$tmpexam=array();
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					$hay=false;
					if(in_array($tipo,$infonivel)){
						$nivel=$this->oDatEnglish_Niveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo,'nombre'=>$nombresesion,'BD'=>$BD));
						if(!empty($nivel[0])){
							$dt["nombre"]=@$nivel[0]["nombre"];
							$dt["estado"]=@$nivel[0]["estado"];
							$dt["idpersonal"]=@$nivel[0]["idpersonal"];
							$dt["descripcion"]=@$nivel[0]["descripcion"];
							$dt["imagen"]=@$nivel[0]["imagen"];
							if(!empty($dt["imagen"])){
								$ipos=stripos($dt["imagen"],'static/');
								if ($ipos !== false) {
									$dt["imagen"]=substr($dt["imagen"], $ipos);
								}
							}
							$hay=true;
						}
					}
					if($hay) $datosreturn[]=$dt;
					$datos3=$this->solosesiones($idcurso,$dt["idcursodetalle"],$BD,$nombresesion);
					if(!empty($datos3)){
						foreach ($datos3 as $s => $d){
							$datosreturn[]=$d;
						}
					}
				}
				$datos2=$datosreturn;
			}else return null;	
			return $datos2;				
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function allsesiones($idpadre,$BD,$nombresesion=''){
		try{
			$datos=$this->oDatEnglish_cursodetalle->buscar(array('idpadre'=>$idpadre,'BD'=>$BD));
			$datos2=array();
			if(!empty($datos)){	
				$infonivel=array('L','M');
				$datosreturn=array();
				//$tmpexam=array();
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					$hay=false;
					if(in_array($tipo,$infonivel)){
						$nivel=$this->oDatEnglish_Niveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo,'nombre'=>$nombresesion,'BD'=>$BD));
						if(!empty($nivel[0])){
							$dt["nombre"]=@$nivel[0]["nombre"];
							$dt["estado"]=@$nivel[0]["estado"];
							$dt["idpersonal"]=@$nivel[0]["idpersonal"];
							$dt["descripcion"]=@$nivel[0]["descripcion"];
							$dt["imagen"]=@$nivel[0]["imagen"];
							if(!empty($dt["imagen"])){
								$ipos=stripos($dt["imagen"],'static/');
								if ($ipos !== false) {
									$dt["imagen"]=substr($dt["imagen"], $ipos);
								}
							}
							$hay=true;
						}
					}
					if($hay) $datosreturn[]=$dt;
					$datos3=$this->allsesiones($dt["idcursodetalle"],$BD,$nombresesion);
					if(!empty($datos3)){
						foreach ($datos3 as $s => $d){
							$datosreturn[]=$d;
						}
					}
				}
				$datos2=$datosreturn;
			}else return null;	
			return $datos2;				
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminarsmartbook($filtros,$bd)
	{
		try {
			return $this->oDatEnglish_cursodetalle->eliminarsmartbook($filtros,$bd);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	/***/
	
}