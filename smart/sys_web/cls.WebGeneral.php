<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		20-04-2019 
 * @copyright	Copyright (C) 20-04-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE);
class WebGeneral extends JrWeb
{
	private $oNegGeneral;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGeneral = new NegGeneral;
			
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('General', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idgeneral"])&&@$_REQUEST["idgeneral"]!='')$filtros["idgeneral"]=$_REQUEST["idgeneral"];
			if(isset($_REQUEST["codigo"])&&@$_REQUEST["codigo"]!='')$filtros["codigo"]=$_REQUEST["codigo"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo_tabla"])&&@$_REQUEST["tipo_tabla"]!='')$filtros["tipo_tabla"]=$_REQUEST["tipo_tabla"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			$this->datos=$this->oNegGeneral->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idgeneral)) {
				$this->oNegGeneral->idgeneral = $idgeneral;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        			$this->oNegGeneral->codigo=@$codigo;
					$this->oNegGeneral->nombre=@$nombre;
					$this->oNegGeneral->tipo_tabla=@$tipo_tabla;
					$this->oNegGeneral->mostrar=@$mostrar;
					
            if($accion=='_add') {
            	$res=$this->oNegGeneral->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('General')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegGeneral->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('General')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegGeneral->__set('idgeneral', $_REQUEST['idgeneral']);
			$res=$this->oNegGeneral->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error',msj=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegGeneral->setCampo($_REQUEST['idgeneral'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}