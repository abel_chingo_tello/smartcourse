<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
class WebPeditar extends JrWeb
{
	private $oNegCurso;
	private $oNegCursodetalle;

	public function __construct()
	{
		parent::__construct();		
        $this->oNegCurso = new NegAcad_curso;
        $this->oNegCursodetalle = new NegAcad_cursodetalle;
	}

	public function defecto(){
		return $this->ver();
	}

	public function ver(){
		try{
			$this->documento->script('tinymce.min', '/libs/tinymce/');
            //$this->documento->script('chi_inputadd', '/libs/tinymce/plugins/chingo/');
			global $aplicacion;
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->idcursodetalle=!empty($_REQUEST["idsesion"])?$_REQUEST["idsesion"]:''; // idcursodetalle
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){
					$this->curso=$curso[0];
					$this->menus=$this->oNegCursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"]));				
				}
			}			
			$this->documento->setTitulo(JrTexto::_('Smartcourse'),true);
			$this->esquema = 'inicio-editar';			
			$this->documento->plantilla =!empty($_REQUEST["plt"])?$_REQUEST["plt"]:'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function buscarCursodetalle($idcurso,$idpadre){
		try{
			if(!empty($idcurso))
				return $this->oNegCursodetalle->buscar(array('idcurso'=>$idcurso,'idpadre'=>$idpadre));
			else
				return '';
			}catch(Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregarhijos($m){
		try{	
			$ht='';
			$mh=!empty($m["hijos"])?true:false;
			if($mh==true){
				$hth='';
				foreach ($m["hijos"] as $mj){
					$hth.=$this->agregarhijos($mj);
				}				
			}
			$mht=!empty($m["html"])?'data-link="'.$m["html"].'"':'';
			$ht='<li class="'.($mh?'hijos':'').'" id="me_'.$m["idcursodetalle"].'" data-idrecurso="'.$m["idrecurso"].'" data-idcursodetalle="'.$m["idcursodetalle"].'" data-idpadre="'.$m["idpadre"].'" data-orden="'.$m["orden"].'" data-tipo="'.$m["tiporecurso"].'" '.$mht.' data-imagen="'.$m["imagen"].'" data-esfinal="'.$m["esfinal"].'" >
				<div>
					<span class="mnombre">'.$m["nombre"].'</span>
					<span class="macciones">
						<i class="mmsubmenu fa fa-sort-down verpopoverm" data-content="Ocultar Submenus" ></i>
						<i class="mmsubmenu fa fa-sort-up verpopoverm" data-content="Mostrar Submenus" ></i>
						<i class="addcontent fa fa-eye verpopoverm" data-content="Ver contenido" data-placement="top"></i>
						<i class="magregar fa fa-plus verpopoverm" data-content="Agregar Submenu" data-placement="top"></i>
						<i class="meditar fa fa-pencil verpopoverm" data-content="Editar Menu" data-placement="top"></i>							
						<i class="meliminar fa fa-trash verpopoverm" data-content="Eliminar" data-placement="top"></i>
						<i class="mmove fa fa-arrows-alt verpopoverm" data-content="Mover Menu" data-placement="right"></i>
						<i class="mcopylink fa fa-link verpopoverm" data-content="Mover Menu" data-placement="right"></i>
						<!--span style="display:inline-block">								
							<i class="mmoveup fa fa-angle-double-up verpopoverm" data-content="Mover Arriba"></i>
							<i class="mmovedown fa fa-angle-double-down verpopoverm" data-content="Mover abajo"></i>
						</span-->
					</span>
				</div>
				'.($mh?('<ul id="me_hijo'.$m["idcursodetalle"].'">'.$hth.'</ul>'):'').'
			</li>';
			return $ht;
		}catch(Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function buscarRecurso($idrecurso,$tipo){	
		try{
			if(!empty($idrecurso))
				return $this->oNegCursodetalle->buscarRecurso(array('idnivel'=>$idrecurso,'tipo'=>$tipo));
			else
				return '';
			}catch(Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function curso(){
		try{
			global $aplicacion;			
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0]))
					$this->curso=$curso[0];
			}
			$this->documento->setTitulo(JrTexto::_('Crear curso'),true);
			$this->esquema = 'curso-formulario';	
			$this->documento->plantilla =!empty($_REQUEST["plt"])?$_REQUEST["plt"]:'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importar(){
		try{
			global $aplicacion;
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0]))
					$this->curso=$curso[0];
			}
			$this->documento->setTitulo(JrTexto::_('Crear curso'),true);
			$this->esquema = 'importar-general';	
			$this->documento->plantilla =!empty($_REQUEST["plt"])?$_REQUEST["plt"]:'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	} 
}