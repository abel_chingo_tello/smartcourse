<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPlanAccion', RUTA_BASE, 'sys_negocio');
class WebPlan_accion extends JrWeb
{
	private $oNegCurso;

	public function __construct()
	{
		parent::__construct();		
        $this->oNegPlanAccion = new NegPlanAccion;
	}

	public function defecto(){
		return $this->principal();
	}

	public function guardarPlan(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			@extract($_POST);
			if(!empty($data) || !empty($idcurso) || !empty($idpersona)){
				parse_str($data, $datosArray);
				for ($i=0; $i < count($datosArray["respuesta"]); $i++) { 
					$datos = array(
						"codigo" => ($i + 1),
						"idcurso" => $idcurso,
						"idpersona" => $idpersona,
						"valor" => $datosArray["respuesta"][$i]
					);
					$this->oNegPlanAccion->agregar($datos);
				}
				echo 200;
			}else{
				echo 300;
			}
		 	exit(0);
        }catch(Exception $e) {
            echo 500;
            exit(0);
        }
	}

	public function actualizarPlan(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			@extract($_POST);
			if(!empty($data) || !empty($idcurso) || !empty($idpersona)){
				parse_str($data, $datosArray);
				for ($i=0; $i < count($datosArray["respuesta"]); $i++) { 
					$this->oNegPlanAccion->actualizar(($i + 1), $idcurso, $idpersona, $datosArray["respuesta"][$i]);
				}
				echo 200;
			}else{
				echo 300;
			}
		 	exit(0);
        }catch(Exception $e) {
            echo $e->getMessage();
            exit(0);
        }
	}

	public function leerPlan(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			@extract($_POST);
			if(!empty($idcurso) || !empty($idpersona)){
				echo json_encode(array("ok" => 200, "data" =>$this->oNegPlanAccion->leer($idcurso, $idpersona)));
			}else{
				echo json_encode(array("ok" => 300, "data" => ""));
			}
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array("ok" => 500, "data" => $e->getMessage()));
            exit(0);
        }
	}

	public function principal(){
		try{
			global $aplicacion;
			@extract($_REQUEST);
			$this->idcurso = $idcurso;
			$this->nombreCurso = $this->oNegCurso->buscar(array("idcurso" => $idcurso))[0]["nombre"];
			// echo $this->nombreCurso;
			$this->preguntas = $this->oNegAporte_Respuesta->listarPreguntas($idcurso);
			$this->esquema = 'preguntas/principal';
			// $this->documento->plantilla ='general';
			$this->usuActivo = NegSesion::getUsuario();
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar(){
		try{
			global $aplicacion;
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
			@extract($_REQUEST);
			$this->idcurso = $idcurso;
			$this->nombreCurso = $this->oNegCurso->buscar(array("idcurso" => $idcurso))[0]["nombre"];
			$this->esquema = 'preguntas/agregar';
			$this->documento->plantilla ='general';
			$this->usuActivo = NegSesion::getUsuario();
			return parent::getEsquema();
			// echo "agregar";
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function registrarPregunta(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			@extract($_POST);
			if(!empty($titulo) || !empty($contenido)){
				if(is_null(@$_FILES["archivo"]['name'])){
					$datos = array(
							"idcurso" => $idcurso,
							"personal" => $personal,
							"titulo" => $titulo,
							"contenido" => $contenido
						);
						$this->oNegAporte_Respuesta->agregar($datos);
				}else{
					$dir_subida = RUTA_MEDIA . 'blog' . SD .'static' . SD .'archivos' . SD;
	            	$fichero_subido = $dir_subida . $_FILES['archivo']['name'];
	            	if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido)) {
	                	$datos = array(
							"idcurso" => $idcurso,
							"personal" => $personal,
							"titulo" => $titulo,
							"contenido" => $contenido,
							"archivo" => $_FILES['archivo']['name']
						);
						$this->oNegAporte_Respuesta->agregar($datos);
	            	}
				}
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('pregunta')).' '.JrTexto::_('resgistrada exitosamente')));
			}else{
				echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('pregunta')).' '.JrTexto::_('faltan datos')));
			}
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	// public function __registrarPregunta($datos){
	// 	$idgrupoauladetalle=@$datos["idgrupoauladetalle"];
	// 	$idalumno=@$datos["idalumno"];
	// 	$idmatricula=@$datos["idmatricula"];
	// 	$estado=!empty($datos["estado"])?$datos["estado"]:1;
	// 	$fecha_matricula=!empty($datos["fecha_matricula"])?$datos["fecha_matricula"]:date('Y-m-d h:i:s');
	// 	$accion='add_';
	// 	$vermatricula='';
	// 	if(!empty($idmatricula)){
	// 		$vermatricula=$this->oNegAcad_matricula->buscar(array('idmatricula'=>$idmatricula));
	// 	}else
	// 		$vermatricula=$this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle,'idalumno'=>$idalumno,'estado'=>1));

	// 	$accion='add_';
	// 	if(!empty($vermatricula)){
	// 		$this->oNegAcad_matricula->idmatricula = $vermatricula[0]["idmatricula"];
	// 		$accion='editar';
	// 	}
	// 	$usuarioAct = NegSesion::getUsuario();
	// 	$this->oNegAcad_matricula->idgrupoauladetalle=$idgrupoauladetalle;
	// 	$this->oNegAcad_matricula->idalumno=$idalumno;
	// 	$this->oNegAcad_matricula->estado=$estado;
	// 	$this->oNegAcad_matricula->idusuario=$usuarioAct["idpersona"];	
 //        $this->oNegAcad_matricula->fecha_registro=date('Y-m-d');
	// 	$this->oNegAcad_matricula->fecha_matricula=$fecha_matricula;
	// 	if($accion=='add_'){
	// 		return $this->oNegAcad_matricula->agregar();
	// 	}else{
	// 		return $this->oNegAcad_matricula->editar();
	// 	}
	// }

	public function __cursosxCategoria($filtros=array()){
		try{
			$this->cursos=$this->oNegCurso->buscarxcategoria($filtros);
			return $this->cursos;
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function __SubCategorias($categorias = array(), $idcat = ""){
		try{
			$subcategorias = array();
			foreach ($categorias as $value) {
				if($value["idpadre"] === $idcat){
					$subcategorias[] = $value;
				}
			}
			return $subcategorias;
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function __buscarxPry($pry){
		try{
			return $this->oNegAcad_categorias->buscarPry($pry);
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function __cursosxMatricula($pry, $alu){
		try{
			// $this->datos=$this->oNegAcad_matricula->buscar($filtros);
			$filtros=array();
			$filtros["idproyecto"] = $pry;
			$filtros["idalumno"] = $alu;
			return $this->oNegAcad_matricula->buscar($filtros);
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function buscarxcategoriajson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegCurso->buscarxcategoria($filtros);
			if(empty($this->datos)) echo json_encode(array('code'=>'ok','data'=>''));
			else{
				$cat=array();
				foreach ($this->datos as $cur){
					if(empty($cat[$cur["idcategoria"]])){
						$cat[$cur["idcategoria"]]=array();
						$cat[$cur["idcategoria"]]["nombre"]=$cur["categoria"];
						$cat[$cur["idcategoria"]]["hijos"]=array();
					}
					$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
					$imgtmp=substr(RUTA_MEDIA,0,-1).$cur["imagen"];                 
                    $cur["imagen"]=is_file($imgtmp)?URL_MEDIA.$cur["imagen"]:$imgcursodefecto; 
					$cat[$cur["idcategoria"]]["hijos"][$cur["idcurso"]]=$cur;
				}
				$this->datos=$cat;
			}
			echo json_encode(array('code'=>'ok','data'=>json_encode($this->datos)));
			exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function verlistado()
	{
		try{			
			global $aplicacion;	
			$filtro=array();
			@extract($_REQUEST);
			$this->cursos=$this->oNegCurso->buscar($filtro); // agregar los filtros de busqueda
			$vista=!empty($_REQUEST["view"])?$_REQUEST["view"]:'vista01';
			$this->esquema = 'listado-cursos-'.$vista;
			$this->usuActivo = NegSesion::getUsuario();
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function silabus(){
		try{
			global $aplicacion;
			$this->usuActivo = NegSesion::getUsuario();
			$this->documento->plantilla ='general';
			$rolActual=$this->usuActivo["idrol"];
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));
			$this->esquema = 'silabus';
			$this->idcurso=$idcurso;
			if(!empty($idcurso)){	
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));	
				if(!empty($curso[0])){			
					JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
					$cursodetalle=new NegAcad_cursodetalle;
					$this->curso=$curso[0];
					$this->idcurso=$this->curso["idcurso"];
					$menushtml=$cursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"],'htmlnotnull'=>true),1);
					$this->menushtml=!empty($menushtml[0])?$menushtml[0]:'';
					$this->categoriasdelcurso=$this->oNegCurso->buscarxcategoria(array('idcurso'=>$this->curso["idcurso"],'solocategoria'=>true));
					$this->menus=$this->oNegCursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"]));
				}
			}
			return parent::getEsquema();	
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	} 

	/*public function vercursoautor(){
		try{
			global $aplicacion;
			$this->usuActivo = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('Curso'),true);
			$this->esquema = 'curso-ver-autor';
			$this->documento->plantilla ='general';
			//var_dump($this->usuActivo);
			$rolActual=$this->usuActivo["idrol"];
			if($rolActual==3){ exit('no tieme suficientes permisos');}
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){					
					JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
					$cursodetalle=new NegAcad_cursodetalle;
					$this->curso=$curso[0];
					$menushtml=$cursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"],'htmlnotnull'=>true),1);
					$this->menushtml=!empty($menushtml[0])?$menushtml[0]:'';
					if(empty($this->menushtml)) return $aplicacion->redir('../recursos/listar/'.$idcurso);
				}  else {$this->esquema='error/404';}
			} else {$this->esquema='error/404';}	
			return parent::getEsquema();	
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}*/

	public function vercurso(){
		try{			
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Curso'),true);
			$this->esquema = 'curso-ver';
			$this->documento->plantilla ='general';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){
					JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
					$cursodetalle=new NegAcad_cursodetalle;
					$this->curso=$curso[0];
					$this->menus=$cursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"]));					
					//$this->menushtml=!empty($menushtml[0])?$menushtml[0]:'';
					return parent::getEsquema();					
				} 
			} 
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function vermenuupdate(){
		try{
			$this->documento->plantilla = 'blanco';
			global $aplicacion;
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->menus=$this->oNegCursodetalle->buscarconnivel(array('idcurso'=>$idcurso));
			$this->esquema = 'menu-ver';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}