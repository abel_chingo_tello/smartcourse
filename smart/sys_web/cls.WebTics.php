<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebTics extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto(){
		return $this->ver();
	}

	public function ver(){
		try{		
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();			
			$this->documento->setTitulo(JrTexto::_('SmartTics'));			
			$idactividad=!empty($_REQUEST["idactividad"])?$_REQUEST["idactividad"]:'';
			$met=!empty($_REQUEST["met"])?$_REQUEST["met"]:'';
			$idsesion=!empty($_REQUEST["idsesion"])?$_REQUEST["idsesion"]:'';
			$this->link =	URL_RAIZ."actividad/ver/?idactividad=" .$idactividad."&met=".$met."&idsesion=".$idsesion;
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco'; 			 	
			$this->esquema = 'tics/ver';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		//$paginaext="https://www.abacoeducacion.org/web/smartquiz/examenes/resolver/?idexamen=$idexa&id=$idalu&pr=edukt2017&u=$nomalu";
	}

	public function buscar(){
		try{			
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();	
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco';	
			$this->documento->setTitulo(JrTexto::_('SmartTics'));			
			$this->esquema = 'tics/buscar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}	
}