<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');*/
class WebWidgets extends JrWeb
{
	//private $oNegEmpresas;

	public function __construct()
	{
		parent::__construct();		
        //$this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;*/
	}

	public function defecto(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Widget'),true);
			$this->esquema = 'widget/defecto';
			$this->documento->plantilla =!empty($_REQUEST["plt"])?$_REQUEST["plt"]:'blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		
	}	
}