<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-04-2017 
 * @copyright	Copyright (C) 04-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');

// JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegConfiguracion_docente', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegHerramientas', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
class WebHistorial_sesion extends JrWeb
{
	private $oNegHistorial_sesion;
    private $oNegGrupo_matricula;
    private $oNegGrupos;
    private $oNegConfiguracion_docente;
    private $oNegResources;
    private $oNegHerramientas;
    protected $oNegExamenes;
    protected $oNegExamenes_preguntas;
    protected $oNegAcad_cursodetalle;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
        // $this->oNegGrupo_matricula = new NegGrupo_matricula;
        // $this->oNegGrupos = new NegGrupos;
        // $this->oNegConfiguracion_docente = new NegConfiguracion_docente;
        // $this->oNegResources = new NegResources;
        // $this->oNegHerramientas = new NegHerramientas;
        // $this->oNegExamenes = new NegExamenes;
        // $this->oNegExamenes_preguntas = new NegExamenes_preguntas;
        // $this->oNegCursodetalle = new NegAcad_cursodetalle;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Historial_sesion', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			
			
			$this->datos=$this->oNegHistorial_sesion->buscar();

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Historial_sesion'), true);
			$this->esquema = 'historial_sesion-list';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Historial_sesion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/

			$usuarioAct = NegSesion::getUsuario();
            $tipousuario=$usuarioAct["idrol"]==1?'M':($usuarioAct["idrol"]==2?'D':($usuarioAct["idrol"]==3?'A':P));
			$this->oNegHistorial_sesion->tipousuario = $tipousuario;
			$this->oNegHistorial_sesion->idusuario = $usuarioAct['idpersona'];
            $this->oNegHistorial_sesion->lugar = $_POST['lugar'];
			$this->oNegHistorial_sesion->idcurso = !empty($_POST['idcurso'])?$_POST['idcurso']:null;
			$this->oNegHistorial_sesion->fechaentrada = $_POST['fechaentrada'];
            $this->oNegHistorial_sesion->fechasalida = $_POST['fechaentrada'];
            $this->oNegHistorial_sesion->idproyecto = $usuarioAct['idproyecto'];
            $this->oNegHistorial_sesion->idcc = !empty($_POST['idcc'])?$_POST['idcc']:null;
			$idHistSesion = $this->oNegHistorial_sesion->agregar();

			$data=array('code'=>'ok','data'=>['idhistorialsesion'=>$idHistSesion]);
            echo json_encode($data);
            exit();
			/* $this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Historial_sesion').' /'.JrTexto::_('New'), true);
			return $this->form(); */
		} catch(Exception $ex) {
			/* return $aplicacion->error(JrTexto::_($e->getMessage())); */
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
		}
	}

	public function editar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Historial_sesion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$usuarioAct = NegSesion::getUsuario();
			$this->oNegHistorial_sesion->idhistorialsesion = $_POST['idhistorialsesion'];
			$this->oNegHistorial_sesion->fechasalida = $_POST['fechasalida'];
			$resp = $this->oNegHistorial_sesion->editar();
			$data=array('code'=>'ok','data'=>['idhistorialsesion'=>$_POST['idhistorialsesion']]);
            echo json_encode($data);
            exit();

			/*$this->frmaccion='Editar';
			$this->oNegHistorial_sesion->idhistorialsesion = @$_GET['id'];
			$this->datos = $this->oNegHistorial_sesion->dataHistorial_sesion;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Historial_sesion').' /'.JrTexto::_('Edit'), true);
			return $this->form();*/
		} catch(Exception $e) {
			/*return $aplicacion->error(JrTexto::_($e->getMessage()));*/
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
    }
    public function editar2()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Historial_sesion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$usuarioAct = NegSesion::getUsuario();
            if(empty($_POST)|| empty($_POST['idhistorialsesion'])){                
                $data=array('code'=>'Error','data'=>'data imompleta historial sesion');
                echo json_encode($data);
                exit(0);
            }
			$this->oNegHistorial_sesion->idhistorialsesion = $_POST['idhistorialsesion'];
			$this->oNegHistorial_sesion->fechasalida = $_POST['fechasalida'];
			$this->oNegHistorial_sesion->tipousuario = $_POST['tipousuario'];
			$this->oNegHistorial_sesion->idusuario = $_POST['idusuario'];
			$this->oNegHistorial_sesion->lugar = $_POST['lugar'];
            $this->oNegHistorial_sesion->fechaentrada = $_POST['fechaentrada'];
            
			$resp = $this->oNegHistorial_sesion->editar();
			$data=array('code'=>'ok','data'=>['idhistorialsesion'=>$_POST['idhistorialsesion']]);
            echo json_encode($data);
            exit();
		} catch(Exception $e) {
			/*return $aplicacion->error(JrTexto::_($e->getMessage()));*/
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegHistorial_sesion->idhistorialsesion = @$_GET['id'];
			$this->datos = $this->oNegHistorial_sesion->dataHistorial_sesion;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Historial_sesion').' /'.JrTexto::_('see'), true);
			$this->esquema = 'historial_sesion-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// private function form()
	// {
	// 	try {
	// 		global $aplicacion;	
			
	// 		//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
	// 		$this->esquema = 'historial_sesion-frm';
	// 		$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
	// 		return parent::getEsquema();
	// 	} catch(Exception $e) {
	// 		return $aplicacion->error(JrTexto::_($e->getMessage()));
	// 	}
	// }

    // private function toSeconds($string='')
    // {
    //     if($string==='') return 0;
    //     $part = explode(':', $string);
    //     return ((int)$part[0]*60*60) + ((int)$part[1]*60) + ((int) $part[2]);
    // }

    // private function toHHMMSS($segundos=0)
    // {
    //     $segs = (int)$segundos;
    //     $horas = floor($segs / 3600);
    //     $minutos = floor(($segs-($horas*3600)) / 60);
    //     $segundos = $segs - ($horas*3600) - ($minutos*60);

    //     if($horas<10){ $horas='0'.$horas; }
    //     if($minutos<10){ $minutos='0'.$minutos; }
    //     if($segundos<10){ $segundos='0'.$segundos; }

    //     return $horas.':'.$minutos.':'.$segundos;
    // }

    // private function sumartiempos($time1,$time2)
    // {
    //     $x = (is_string($time1))?new DateTime($time1):$time1;
    //     $y = (is_string($time2))?new DateTime($time2):$time2;
    //     $tinit1 = new DateTime('00-00-00 00:00:00');
    //     $tinit2 = new DateTime('00-00-00 00:00:00');

    //     $interval1 = $tinit1->diff($x) ;
    //     $interval2 = $tinit2->diff($y) ;

    //     $e = new DateTime('00:00');
    //     $f = clone $e;
    //     $e->add($interval1);
    //     $e->add($interval2);
        
    //     $total = $f->diff($e)->format("%Y-%M-%D %H:%I:%S");
    //     return $total;
    // }

    // private function getConfigDocentexNivel($filtros=array()){
    //     if(empty($filtros)) return array();
    //     try {
    //         $config_doc=$this->oNegConfiguracion_docente->buscar($filtros);
    //         if(empty($config_doc)){
    //             $config_doc = array(
    //                 0=>array(
    //                     'tiempototal'=>'00:00:00',
    //                     'tiempoactividades'=>'00:00:00',
    //                     'tiempogames'=>'00:00:00',
    //                     'tiempoteacherresrc'=>'00:00:00',
    //                     'tiempoexamenes'=>'00:00:00',
    //                 ),
    //             );
    //         }
    //         /**** calculando tiempos x cantidad de Teacher resouorces ****/
    //         $cant_TResrc=$this->oNegResources->getNumRegistros(array('idnivel'=> $filtros['idnivel'], 'idunidad'=> $filtros['idunidad'], 'idactividad'=> $filtros['idactividad'],'tipo'=>'P'));
    //         $time_TR = 0;
    //         for ($i=1; $i <= $cant_TResrc; $i++) {
    //             $time_TR+=$this->toSeconds($config_doc[0]['tiempoteacherresrc']);
    //         }
    //         $config_doc[0]['tiempoteacherresrc'] = $this->toHHMMSS($time_TR);

    //         /**** calculando tiempos x cantidad de Games ****/
    //         $cant_Games=$this->oNegHerramientas->getNumRegistros(array('idnivel'=> $filtros['idnivel'], 'idunidad'=> $filtros['idunidad'], 'idactividad'=> $filtros['idactividad'],'tool'=>'G'));
    //         $time_G = 0;
    //         for ($i=1; $i <= $cant_Games; $i++) {
    //             $time_G+=$this->toSeconds($config_doc[0]['tiempogames']);
    //         }
    //         $config_doc[0]['tiempogames'] = $this->toHHMMSS($time_G);
            
    //         /**** calculando tiempos x Examenes ****/
    //         $examenes = array();
    //         $sumaTExam = '00-00-00 00:00:00';
    //         $exams_curso_det = $this->oNegCursodetalle->buscar(array("idcurso"=>$this->idCurso,"tiporecurso"=>'E'));
    //         if(!empty($exams_curso_det)){
    //             foreach ($exams_curso_det as $cd) {
    //                 $exam = $this->oNegExamenes->buscar( $cd["idrecurso"] );
    //                 if(!empty($exam)){ $examenes[] = $this->oNegExamenes->buscar( $cd["idrecurso"] ); }
    //             }
    //         }
    //         foreach ($examenes as $exam) {
    //             if($exam['tiempo_por']=='E'){
    //                 $sumaTExam = $this->sumartiempos($sumaTExam, '00-00-00 '.$exam['tiempo_total']);
    //             }else{
    //                 $preguntas = $this->oNegExamenes_preguntas->buscar(array(
    //                     'idexamen'=> $exam['idexamen'],
    //                 ));
    //                 $sumaTPreg = '00-00-00 00:00:00';
    //                 foreach ($preguntas as $preg) {
    //                     $sumaTPreg = $this->sumartiempos($sumaTPreg, '00-00-00 '.$preg['tiempo']);
    //                 }
    //                 $sumaTExam = $this->sumartiempos($sumaTExam, $sumaTPreg);
    //             }
    //         }
    //         $fecha_hora = explode(' ', $sumaTExam);
    //         $config_doc[0]['tiempoexamenes'] = end($fecha_hora);
    //         return $config_doc[0];
    //     } catch (Exception $e) {
    //         throw new Exception(JrTexto::_('Error getting Config Docente: ').$e->getMessage());
    //     }
    // }

	// public function tiempoxalumno($idalumno=null, $ultima_act=null)
    // {
	// 	/* informe de Tiempo para "docente/panelcontrol.php"  */
    //     $this->documento->plantilla = 'returnjson';
	// 	try{
    //         global $aplicacion;
    //         if(empty($_REQUEST["id"]) && $idalumno==null && empty($_REQUEST["ultima_actividad"]) && $ultima_act==null ) {
    //             throw new Exception(JrTexto::_('Error in filtering'));
    //         }
    //         $usuarioAct = NegSesion::getUsuario();
    //         $this->idCurso = !empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
    //         $ultima_actividad=($ultima_act==null)?json_decode($_REQUEST["ultima_actividad"],true):$ultima_act;
    //         $filtros2 = array(
    //             #'iddocente' => $usuarioAct["dni"],
    //             #'idrol' => $usuarioAct["idrol"],
    //             'idnivel'=> $ultima_actividad['idnivel'],
    //             'idunidad'=> $ultima_actividad['idunidad'],
    //             'idactividad'=> $ultima_actividad['idactividad'],
    //             'idcurso'=> $this->idCurso,
    //         );
    //         $config_doc=$this->getConfigDocentexNivel($filtros2);
            
    //         $filtros=array(
    //             "idusuario"=>($idalumno==null)? $_REQUEST["id"] : $idalumno,
    //             "tipousuario"=>'A',
    //             'idcurso'=> $this->idCurso,
    //         );
    //         $historial_sesion=$this->oNegHistorial_sesion->buscar($filtros);
            
    //         $timeP = new DateTime('00:00:00'); //en toda la Plataforma
    //         $timeA = new DateTime('00:00:00'); //en Actividades
    //         $timeG = new DateTime('00:00:00'); //en Games
    //         $timeTR= new DateTime('00:00:00'); //en Teacher Resrc.
    //         $timeE = new DateTime('00:00:00'); //en Examenes
    //         $tiempoTotal = clone $timeP;
    //         $tiempoActiv = clone $timeA;
    //         $tiempoGames = clone $timeG;
    //         $tiempoTRsrc = clone $timeTR;
    //         $tiempoExams = clone $timeE;
    //         // var_dump($historial_sesion);
    //         foreach ($historial_sesion as $value) {
    //             if($value['fechasalida']!=null){
    //                 $in = new DateTime($value['fechaentrada']);
	//             	$out = new DateTime($value['fechasalida']);
	//             	$diferencia = $in->diff($out);
	            	
	// 	        	//if($value['lugar']=='P'){ $timeP->add($diferencia); }
	// 	        	if($value['lugar']=='A'){ $timeA->add($diferencia); }
	// 	        	if($value['lugar']=='G'){ $timeG->add($diferencia); }
	// 	        	if($value['lugar']=='TR'){ $timeTR->add($diferencia); }
	// 	        	if($value['lugar']=='E'){ $timeE->add($diferencia); }
    //         	}
    //         }
            
    //         //$tTotal = $tiempoTotal->diff($timeP)->format("%Y-%M-%D %H:%I:%S");
    //         $tActiv = $tiempoActiv->diff($timeA)->format("%Y-%M-%D %H:%I:%S");
    //         $tGames = $tiempoGames->diff($timeG)->format("%Y-%M-%D %H:%I:%S");
    //         $tTResrc= $tiempoTRsrc->diff($timeTR)->format("%Y-%M-%D %H:%I:%S");
    //         $tExams = $tiempoExams->diff($timeE)->format("%Y-%M-%D %H:%I:%S");
            
    //         // var_dump($timeTR);
    //         // var_dump($tTResrc);

    //         $sumaTiempos = $this->sumartiempos($this->sumartiempos($tActiv, $tGames), $this->sumartiempos($tTResrc, $tExams));

    //         $tiempo = [
    //         	'total'        => $sumaTiempos,
    //         	'actividades'  => $tActiv,
    //         	'juegos'       => $tGames,
    //         	'teacherresrc' => $tTResrc,
    //         	'examenes'     => $tExams,
    //         ];
            
    //         if($idalumno!=null){
    //             return $tiempo;
    //         }
    //         $data=array('code'=>'ok','data'=>$tiempo, 'config_docente'=>$config_doc);
    //         echo json_encode($data);
    //         return parent::getEsquema();
    //     }catch(Exception $ex){
    //         $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
    //         echo json_encode($data);
    //     }
	// }

    // public function tiempoxgrupo()
    // {
    //     $this->documento->plantilla = 'returnjson';
    //     try {
    //         global $aplicacion;
    //         if(empty($_POST["id"]) && empty($_POST["ultima_actividad"])) {
    //             throw new Exception(JrTexto::_('Error in filtering'));
    //         }
    //         $this->idCurso = empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
    //         $usuarioAct = NegSesion::getUsuario();
    //         $ultima_actividad = json_decode($_POST["ultima_actividad"],true);
    //         $filtros2 = array(
    //             #'iddocente' => $usuarioAct["dni"],
    //             #'idrol' => $usuarioAct["idrol"],
    //             'idnivel'=> $ultima_actividad['idnivel'],
    //             'idunidad'=> $ultima_actividad['idunidad'],
    //             'idactividad'=> $ultima_actividad['idactividad'],
    //         );
    //         $config_doc=$this->getConfigDocentexNivel($filtros2);
    //         $tiempoProm=array();
    //         $filtros=array();
    //         $filtros['idgrupo']=$_POST['id'];
    //         $filtros['estado']=1;
    //         $tiempoProm=[
    //             'total'=> new DateTime('00:00:00'),
    //             'actividades' => new DateTime('00:00:00'),
    //             'juegos' => new DateTime('00:00:00'),
    //             'teacherresrc' => new DateTime('00:00:00'),
    //             'examenes' => new DateTime('00:00:00'),
    //         ];

    //         $alum_grupo = $this->oNegGrupo_matricula->buscar($filtros);
    //         foreach ($alum_grupo as $al) {
    //             $arrTiempos = $this->tiempoxalumno($al['idalumno'], $ultima_actividad);

    //             $t = new DateTime('00-00-00 00:00:00');
    //             $ac = clone $t;
    //             $ju = clone $t;
    //             $tr = clone $t;
    //             $ex = clone $t;
    //             $total = new DateTime($arrTiempos['total']);
    //             $activ = new DateTime($arrTiempos['actividades']);
    //             $juego = new DateTime($arrTiempos['juegos']);
    //             $trsrc = new DateTime($arrTiempos['teacherresrc']);
    //             $exams = new DateTime($arrTiempos['examenes']);

    //             $interval_T = $t->diff($total);
    //             $interval_A = $ac->diff($activ);
    //             $interval_J = $ju->diff($juego);
    //             $interval_TR = $tr->diff($trsrc);
    //             $interval_E = $ex->diff($exams);

    //             $tiempoProm['total']->add($interval_T);
    //             $tiempoProm['actividades']->add($interval_A);
    //             $tiempoProm['juegos']->add($interval_J);
    //             $tiempoProm['teacherresrc']->add($interval_TR);
    //             $tiempoProm['examenes']->add($interval_E);
                
    //         }

    //         foreach ($tiempoProm as $key => $value) {
    //             $date = new DateTime('00:00:00');
    //             $tiempoProm[$key] = $value->diff($date)->format("%Y-%M-%D %H:%I:%S");
    //         }

    //         $data=array('code'=>'ok','data'=>$tiempoProm, 'config_docente'=>$config_doc);
    //         echo json_encode($data);
    //         return parent::getEsquema();
    //     } catch (Exception $ex) {
    //         $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
    //         echo json_encode($data);
    //     }
    // }

    // public function tiempoxaula()
    // {
    //     $this->documento->plantilla = 'returnjson';
    //     try {
    //         global $aplicacion;
    //         if(empty($_POST["id"])) {
    //             throw new Exception(JrTexto::_('Error in filtering'));
    //         }
    //         $usuarioAct = NegSesion::getUsuario();
    //         $filtros2 = array(
    //             'iddocente' => $usuarioAct["dni"],
    //             'idrol' => $usuarioAct["rol"],
    //         );
    //         $config_doc=$this->oNegConfiguracion_docente->buscar($filtros2);
    //         $filtros = array();
    //         $usuarioAct = NegSesion::getUsuario();
    //         $filtros['iddocente'] = $usuarioAct['dni'];
    //         $filtros['idambiente'] = $_POST['id'];
    //         $grupos = $this->oNegGrupos->buscar($filtros);
    //         foreach ($grupos as $g) {
    //             $arrTiempos = $this->tiempoxgrupo($g['idgrupo']);
    //             var_dump($arrTiempos);
    //         }
    //         $data=array('code'=>'ok','data'=>$tiempoProm, 'config_docente'=>$config_doc);
    //         echo json_encode($data);
    //         return parent::getEsquema();
    //     } catch (Exception $ex) {
    //         $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
    //         echo json_encode($data);
    //     }
    // }
	// // ========================== Funciones xajax ========================== //
	// public function xSaveHistorial_sesion(&$oRespAjax = null, $args = null)
	// {
	// 	if(is_a($oRespAjax, 'xajaxResponse')) {
	// 		try {
	// 			if(empty($args[0])) { return;}
	// 			$frm = $args[0];
				
	// 			if(!empty($frm['pkIdhistorialsesion'])) {
	// 				$this->oNegHistorial_sesion->idhistorialsesion = $frm['pkIdhistorialsesion'];
	// 			}
				
	// 			$this->oNegHistorial_sesion->__set('tipousuario',@$frm["txtTipousuario"]);
	// 				$this->oNegHistorial_sesion->__set('idusuario',@$frm["txtIdusuario"]);
	// 				$this->oNegHistorial_sesion->__set('lugar',@$frm["txtLugar"]);
	// 				$this->oNegHistorial_sesion->__set('fechaentrada',@$frm["txtFechaentrada"]);
	// 				$this->oNegHistorial_sesion->__set('fechasalida',@$frm["txtFechasalida"]);
					
	// 			   if(@$frm["accion"]=="Nuevo"){
	// 								    $res=$this->oNegHistorial_sesion->agregar();
	// 				}else{
	// 								    $res=$this->oNegHistorial_sesion->editar();
	// 			    }
	// 				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegHistorial_sesion->idhistorialsesion);
	// 			else{
	// 				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
	// 				$oRespAjax->setReturnValue(false);
	// 			}
							
	// 		} catch(Exception $e) {
	// 			$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
	// 		} 
	// 	}
	// }

	// public function xGetxIDHistorial_sesion(&$oRespAjax = null, $args = null)
	// {
	// 	if(is_a($oRespAjax, 'xajaxResponse')) {
	// 		try {
	// 			if(empty($args[0])) { return;}
	// 			$pk = $args[0];
	// 			$this->oNegHistorial_sesion->__set('idhistorialsesion', $pk);
	// 			$this->datos = $this->oNegHistorial_sesion->dataHistorial_sesion;
	// 			$res=$this->oNegHistorial_sesion->getXid();
	// 			if(!empty($res))
	// 				$oRespAjax->setReturnValue($res);
	// 			else{
	// 			$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
	// 			$oRespAjax->setReturnValue(false);
	// 			}
	// 		} catch(Exception $e) {
	// 			$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
	// 			$oRespAjax->setReturnValue(false);
	// 		} 
	// 	}
	// }
	// public function xEliminar(&$oRespAjax = null, $args = null)
	// {
	// 	if(is_a($oRespAjax, 'xajaxResponse')) {
	// 		try {
	// 			if(empty($args[0])) { return;}
	// 			$pk = $args[0];
	// 			$this->oNegHistorial_sesion->__set('idhistorialsesion', $pk);
	// 			$res=$this->oNegHistorial_sesion->eliminar();
	// 			if(!empty($res))
	// 				$oRespAjax->setReturnValue($res);
	// 			else{
	// 				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
	// 				$oRespAjax->setReturnValue(false);
	// 			}
	// 		} catch(Exception $e) {
	// 			$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
	// 			$oRespAjax->setReturnValue(false);
	// 		} 
	// 	}
	// }

	     
}