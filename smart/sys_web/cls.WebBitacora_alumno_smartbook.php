<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-02-2018 
 * @copyright	Copyright (C) 23-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE, 'sys_negocio');

class WebBitacora_alumno_smartbook extends JrWeb
{
	private $oNegBitacora_alumno_smartbook;
	private $oNegBitacora_smartbook;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBitacora_alumno_smartbook = new NegBitacora_alumno_smartbook;
		$this->oNegBitacora_smartbook = new NegBitacora_smartbook;
	}

	public function defecto(){
		return $this->listado();
	}
/*
	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bitacora_alumno_smartbook', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idbitacora_smartbook"])&&@$_REQUEST["idbitacora_smartbook"]!='')$filtros["idbitacora_smartbook"]=$_REQUEST["idbitacora_smartbook"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idsesion"])&&@$_REQUEST["idsesion"]!='')$filtros["idsesion"]=$_REQUEST["idsesion"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			
			$this->datos=$this->oNegBitacora_alumno_smartbook->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bitacora_alumno_smartbook'), true);
			$this->esquema = 'bitacora_alumno_smartbook-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bitacora_alumno_smartbook', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Bitacora_alumno_smartbook').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bitacora_alumno_smartbook', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegBitacora_alumno_smartbook->idbitacora_smartbook = @$_GET['id'];
			$this->datos = $this->oNegBitacora_alumno_smartbook->dataBitacora_alumno_smartbook;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bitacora_alumno_smartbook').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bitacora_alumno_smartbook-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/
	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bitacora_alumno_smartbook', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			$usuarioAct = NegSesion::getUsuario();
			if(isset($_REQUEST["idbitacora_smartbook"])&&@$_REQUEST["idbitacora_smartbook"]!='')$filtros["idbitacora_smartbook"]=$_REQUEST["idbitacora_smartbook"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idsesion"])&&@$_REQUEST["idsesion"]!='')$filtros["idsesion"]=$_REQUEST["idsesion"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$usuarioAct["dni"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
						
			$this->datos=$this->oNegBitacora_alumno_smartbook->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarsession(){
		try{
			global $aplicacion;
			
			$usuarioAct = NegSesion::getUsuario();
			
			if(empty($_POST) && !isset($_POST['idcurso'])){
                echo json_encode(array('code'=>'Error','msj'=>'data incomplete'));
                exit(0);            
			}

			//search course
			$busqueda = $this->oNegBitacora_alumno_smartbook->buscar(array('idcurso' => $_POST['idcurso'],'idusuario'=>@$usuarioAct['idpersona']));
			if(!empty($busqueda)){
				echo json_encode(array('code'=>'ok','message'=>'Bitacora_alumno_smartbook exist')); 				
				exit(0);
			}

			$estado = (isset($_POST['estado'])) ? $_POST['estado'] : 'P';

			$this->oNegBitacora_alumno_smartbook->idcurso=$_POST['idcurso'];
			$this->oNegBitacora_alumno_smartbook->idsesion=$_POST['idsesion'];
			$this->oNegBitacora_alumno_smartbook->idusuario=@$usuarioAct['idpersona']; //@$txtIdusuario;
			$this->oNegBitacora_alumno_smartbook->estado=$estado;
			$this->oNegBitacora_alumno_smartbook->regfecha= @date('Y-m-d H:i:s'); //@$txtRegfecha;

			$res=$this->oNegBitacora_alumno_smartbook->agregar();

			echo json_encode(array('code'=>'ok','message'=>'Bitacora_alumno_smartbook saved successfully','newid'=>$res)); 
			exit();
		}catch(Exception $e){
			// echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
            // exit(0);
		}
	}
	public function guardarprogreso(){
		try{
			global $aplicacion;
			
			$usuarioAct = NegSesion::getUsuario();
			$txtIdcurso = $_POST['idcurso'];
			$txtIdsesion = $_POST['idsesion'];
			$Idbitacora_smartbook = $_POST['Idbitacora_smartbook'];
			$txtPestania = 'div_sesion';
			$txtTotal_Pestanias= 1;
			$txtProgreso = 100;
			$txtOtros_datos = '';

			$this->oNegBitacora_smartbook->idcurso=@$txtIdcurso;
			$this->oNegBitacora_smartbook->idsesion=@$txtIdsesion;
			$this->oNegBitacora_smartbook->idusuario=@$usuarioAct['idpersona'];
			$this->oNegBitacora_smartbook->idbitacora_alum_smartbook=@$Idbitacora_smartbook;
			$this->oNegBitacora_smartbook->pestania=@$txtPestania;
			$this->oNegBitacora_smartbook->total_pestanias=@$txtTotal_Pestanias;
			$this->oNegBitacora_smartbook->fechahora=date('Y-m-d H:i:s');
			$this->oNegBitacora_smartbook->progreso=@$txtProgreso;
			$this->oNegBitacora_smartbook->otros_datos= @str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$txtOtros_datos);


			$res=$this->oNegBitacora_smartbook->agregar();

			echo json_encode(array('code'=>'ok','message'=>'Bitacora_alumno_smartbook saved successfully','newid'=>$res)); 
			exit();
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','message'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function xGuardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdbitacora_smartbook)) {
				$this->oNegBitacora_alumno_smartbook->idbitacora_smartbook = @$pkIdbitacora_smartbook;
				$accion='_edit';
			}
			$usuarioAct = NegSesion::getUsuario();

			$this->oNegBitacora_alumno_smartbook->idcurso=@$txtIdcurso;
			$this->oNegBitacora_alumno_smartbook->idsesion=@$txtIdsesion;
			$this->oNegBitacora_alumno_smartbook->idusuario=@$usuarioAct['idpersona']; //@$txtIdusuario;
			$this->oNegBitacora_alumno_smartbook->estado=@$txtEstado;
			$this->oNegBitacora_alumno_smartbook->regfecha= @date('Y-m-d H:i:s'); //@$txtRegfecha;
					
            if($accion=='_add') {
            	$res=$this->oNegBitacora_alumno_smartbook->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Bitacora_alumno_smartbook')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegBitacora_alumno_smartbook->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Bitacora_alumno_smartbook')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveBitacora_alumno_smartbook(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdbitacora_smartbook'])) {
					$this->oNegBitacora_alumno_smartbook->idbitacora_smartbook = $frm['pkIdbitacora_smartbook'];
				}
				
				$this->oNegBitacora_alumno_smartbook->idcurso=@$frm["txtIdcurso"];
				$this->oNegBitacora_alumno_smartbook->idsesion=@$frm["txtIdsesion"];
				$this->oNegBitacora_alumno_smartbook->idusuario=@$frm["txtIdusuario"];
				$this->oNegBitacora_alumno_smartbook->estado=@$frm["txtEstado"];
				$this->oNegBitacora_alumno_smartbook->regfecha=@$frm["txtRegfecha"];
				
				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegBitacora_alumno_smartbook->agregar();
				}else{
					$res=$this->oNegBitacora_alumno_smartbook->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBitacora_alumno_smartbook->idbitacora_smartbook);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBitacora_alumno_smartbook(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBitacora_alumno_smartbook->__set('idbitacora_smartbook', $pk);
				$this->datos = $this->oNegBitacora_alumno_smartbook->dataBitacora_alumno_smartbook;
				$res=$this->oNegBitacora_alumno_smartbook->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBitacora_alumno_smartbook->__set('idbitacora_smartbook', $pk);
				$res=$this->oNegBitacora_alumno_smartbook->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegBitacora_alumno_smartbook->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}