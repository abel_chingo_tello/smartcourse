<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-02-2018 
 * @copyright	Copyright (C) 09-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
/*JrCargador::clase('sys_negocio::NegAcad_sesionhtml', RUTA_BASE, 'sys_negocio');*/
class WebAcad_cursodetalle extends JrWeb
{
	private $oNegAcad_cursodetalle;
	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		/*$this->oNegSesionhtml = new NegAcad_sesionhtml;*/
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_cursodetalle', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros = array();
			if (isset($_REQUEST["idcursodetalle"]) && @$_REQUEST["idcursodetalle"] != '') $filtros["idcursodetalle"] = $_REQUEST["idcursodetalle"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["orden"]) && @$_REQUEST["orden"] != '') $filtros["orden"] = $_REQUEST["orden"];
			if (isset($_REQUEST["idrecurso"]) && @$_REQUEST["idrecurso"] != '') $filtros["idrecurso"] = $_REQUEST["idrecurso"];
			if (isset($_REQUEST["tiporecurso"]) && @$_REQUEST["tiporecurso"] != '') $filtros["tiporecurso"] = $_REQUEST["tiporecurso"];
			if (isset($_REQUEST["idlogro"]) && @$_REQUEST["idlogro"] != '') $filtros["idlogro"] = $_REQUEST["idlogro"];
			if (isset($_REQUEST["url"]) && @$_REQUEST["url"] != '') $filtros["url"] = $_REQUEST["url"];
			if (isset($_REQUEST["idpadre"]) && @$_REQUEST["idpadre"] != '') $filtros["idpadre"] = $_REQUEST["idpadre"];
			if (isset($_REQUEST["color"]) && @$_REQUEST["color"] != '') $filtros["color"] = $_REQUEST["color"];
			if (isset($_REQUEST["esfinal"]) && @$_REQUEST["esfinal"] != '') $filtros["esfinal"] = $_REQUEST["esfinal"];
			$this->datos = $this->oNegAcad_cursodetalle->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Acad_cursodetalle'), true);
			$this->esquema = 'acad_cursodetalle-list';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function vermenus()
	{
		try {
			global $aplicacion;
			$idcursodetalle = !empty($_REQUEST["idcursodetalle"]) ? $_REQUEST["idcursodetalle"] : '';
			if (!empty($idcursodetalle)) {
				$cursodet = $this->oNegAcad_cursodetalle->buscar(array('idcursodetalle' => $idcursodetalle));
				if (!empty($cursodet[0])) {
					$this->cursodet = $cursodet[0];
					$recurso = $this->oNegAcad_cursodetalle->buscarRecurso(array('idnivel' => $this->cursodet["idrecurso"], 'tipo' => $this->cursodet["tiporecurso"]));
					if ($recurso[0]) $this->recurso = $recurso[0];
				}
			} else {
				$this->idpadre = !empty($_REQUEST["idpadre"]) ? $_REQUEST["idpadre"] : 0;
				$this->idrecursopadre = !empty($_REQUEST["idrecursopadre"]) ? $_REQUEST["idrecursopadre"] : 0;
			}
			$this->documento->setTitulo(JrTexto::_('Crear Menu'), true);
			$this->esquema = 'menu-formulario';
			$this->documento->plantilla = !empty($_REQUEST["plt"]) ? $_REQUEST["plt"] : 'general';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_cursodetalle', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_cursodetalle') . ' /' . JrTexto::_('New'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_cursodetalle', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Editar';
			$this->oNegAcad_cursodetalle->idcursodetalle = @$_GET['id'];
			$this->datos = $this->oNegAcad_cursodetalle->dataAcad_cursodetalle;
			$this->pk = @$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_cursodetalle') . ' /' . JrTexto::_('Edit'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_cursodetalle-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_cursodetalle', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idcursodetalle"]) && @$_REQUEST["idcursodetalle"] != '') $filtros["idcursodetalle"] = $_REQUEST["idcursodetalle"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["orden"]) && @$_REQUEST["orden"] != '') $filtros["orden"] = $_REQUEST["orden"];
			if (isset($_REQUEST["idrecurso"]) && @$_REQUEST["idrecurso"] != '') $filtros["idrecurso"] = $_REQUEST["idrecurso"];
			if (isset($_REQUEST["tiporecurso"]) && @$_REQUEST["tiporecurso"] != '') $filtros["tiporecurso"] = $_REQUEST["tiporecurso"];
			if (isset($_REQUEST["idlogro"]) && @$_REQUEST["idlogro"] != '') $filtros["idlogro"] = $_REQUEST["idlogro"];
			if (isset($_REQUEST["url"]) && @$_REQUEST["url"] != '') $filtros["url"] = $_REQUEST["url"];
			if (isset($_REQUEST["idpadre"]) && @$_REQUEST["idpadre"] != '') $filtros["idpadre"] = $_REQUEST["idpadre"];
			if (isset($_REQUEST["color"]) && @$_REQUEST["color"] != '') $filtros["color"] = $_REQUEST["color"];
			if (isset($_REQUEST["esfinal"]) && @$_REQUEST["esfinal"] != '') $filtros["esfinal"] = $_REQUEST["esfinal"];

			$this->datos = $this->oNegAcad_cursodetalle->buscar($filtros);
			echo json_encode(array('code' => 'ok', 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function buscarjsonconnivel()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_cursodetalle', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros = array();
			if (isset($_REQUEST["idcursodetalle"]) && @$_REQUEST["idcursodetalle"] != '') $filtros["idcursodetalle"] = $_REQUEST["idcursodetalle"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["orden"]) && @$_REQUEST["orden"] != '') $filtros["orden"] = $_REQUEST["orden"];
			if (isset($_REQUEST["idrecurso"]) && @$_REQUEST["idrecurso"] != '') $filtros["idrecurso"] = $_REQUEST["idrecurso"];
			if (isset($_REQUEST["tiporecurso"]) && @$_REQUEST["tiporecurso"] != '') $filtros["tiporecurso"] = $_REQUEST["tiporecurso"];
			if (isset($_REQUEST["idlogro"]) && @$_REQUEST["idlogro"] != '') $filtros["idlogro"] = $_REQUEST["idlogro"];
			if (isset($_REQUEST["url"]) && @$_REQUEST["url"] != '') $filtros["url"] = $_REQUEST["url"];
			if (isset($_REQUEST["idpadre"]) && @$_REQUEST["idpadre"] != '') $filtros["idpadre"] = $_REQUEST["idpadre"];
			if (isset($_REQUEST["color"]) && @$_REQUEST["color"] != '') $filtros["color"] = $_REQUEST["color"];
			if (isset($_REQUEST["esfinal"]) && @$_REQUEST["esfinal"] != '') $filtros["esfinal"] = $_REQUEST["esfinal"];

			$this->datos = $this->oNegAcad_cursodetalle->buscarconnivel($filtros);
			echo json_encode(array('code' => 'ok', 'data' => $this->datos));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardarMenu()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$iscloonado = false;
			$newimagen = $rutaimagen = '';
			if (@$accimagen == 'sg') { //solo guardar
				$rutaimagen = $imagen;
				if (is_file(RUTA_BASE . @$oldimagen)) @unlink(RUTA_BASE . @$oldimagen);
				$newimagen = URL_BASE . $rutaimagen;
			} else if (!empty($_FILES['imagen']['name'])) {
				$file = $_FILES["imagen"];
				$ext = strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$newfoto = 'logo_' . $idcursodetalle . date("Ymdhis") . '.' . $ext;
				$dir = RUTA_BASE . '/static' . SD . 'media' . SD . 'cursos' . SD . "curso_" . $idcurso . SD . "sesion" . $idcursodetalle;
				if (!file_exists($dir)) {
					@mkdir($dir, 0777, true);
				}
				if (move_uploaded_file($file["tmp_name"], $dir . SD . $newfoto)) {
					if ($icc != 0) {
						$rutaimagen = '/static/media/cursos_complementarios/curso_complementario_' . $idcurso . "/sesion" . $idcursodetalle . "/" . $newfoto;
					} else {
						$rutaimagen = '/static/media/cursos/curso_' . $idcurso . "/sesion" . $idcursodetalle . "/" . $newfoto;
					}
					if (is_file(RUTA_BASE . $oldimagen)) unlink(RUTA_BASE . $oldimagen);
					$newimagen = URL_BASE . $rutaimagen;
				}
			} else {
				$rutaimagen = @$oldimagen;
			}
			$this->oNegAcad_cursodetalle->idcurso = @$idcurso;
			$this->oNegAcad_cursodetalle->orden = @$orden;
			$this->oNegAcad_cursodetalle->idrecurso = @$idrecurso;
			$this->oNegAcad_cursodetalle->tiporecurso = @$tiporecurso;
			$this->oNegAcad_cursodetalle->idlogro = @$idlogro;
			$this->oNegAcad_cursodetalle->url = @$url;
			$this->oNegAcad_cursodetalle->idpadre = @$idpadre;
			$this->oNegAcad_cursodetalle->color = @$color;
			$this->oNegAcad_cursodetalle->esfinal = @$esfinal;
			$this->oNegAcad_cursodetalle->espadre = @$espadre;
			$usuarioAct = NegSesion::getUsuario();
			if (!isset($espadre)) {
				$espadre = 0;
			}
			$newid = $this->oNegAcad_cursodetalle->agregaromodificar(@$idcursodetalle, @$nombre, @$idrecursopadre, @$usuarioAct['idpersona'], $rutaimagen, $descripcion, $idrecursoorden, $iscloonado, $icc, $espadre);
			echo json_encode(array('code' => 'ok', 'msj' => ucfirst(JrTexto::_('Menu')) . ' ' . JrTexto::_('saved successfully'), 'newid' => $newid, 'imagen' => $newimagen, 'rutaimagen' => $rutaimagen));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	/*public function guardarSesionhtml()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$idcursodetalle = !empty($idcursodetalle) ? $idcursodetalle : 0;
			$this->oNegSesionhtml->idcurso = $idcurso;
			$this->oNegSesionhtml->idpersonal = 1;
			$this->oNegSesionhtml->idcursodetalle = $idcursodetalle;
			$dir1 = RUTA_MEDIA . SD . 'static' . SD . 'media' . SD . 'cursos' . SD . 'curso_' . $idcurso;
			if (!file_exists($dir1)) {
				@mkdir($dir1, 0777, true);
			}
			//$dir2=$dir1.SD.'det_'.$idcursodetalle;
			//if(!file_exists($dir2)){@mkdir($dir2, 0777, true);}

			$archivo = 'static/media/cursos/curso_' . $idcurso . '/ses_' . $idcursodetalle . ".html";
			$rutfile = $dir1 . '/ses_' . $idcursodetalle . ".html";
			if (file_exists($rutfile)) @unlink($rutfile);
			$fp = @fopen($rutfile, "a");
			$write = fputs($fp, $html);
			fclose($fp);
			@chmod($rutfile, 0777);
			$this->oNegSesionhtml->html = $archivo;
			if ($idcursodetalle > 0)
				$newid = $this->oNegSesionhtml->update();
			else $newid = 0;
			echo json_encode(array('code' => 'ok', 'msj' => ucfirst(JrTexto::_('saved successfully')), 'newid' => $newid, 'archivo' => $archivo));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}*/

	public function uploadmedia()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$dirmedia = !empty($dirmedia) ? $dirmedia : '';
			$oldmedia = !empty($oldmedia) ? $oldmedia : '';
			$typefile = !empty($typefile) ? $typefile : '';
			$newmedia = '';
			if (!empty($_FILES['media']['name'])) {
				$file = $_FILES["media"];
				$ext = strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				if ($typefile == 'html' && $ext == 'html') {
					$newnombre = $_FILES['media']['name'];
				} else $newnombre = date("Ymdhis") . rand(0, 100) . '.' . $ext;

				$dir = RUTA_MEDIA . '/static' . SD . 'media' . SD . 'cursos' . SD . $dirmedia;
				if (!file_exists($dir)) {
					@mkdir($dir, 0777, true);
				}
				if (move_uploaded_file($file["tmp_name"], $dir . SD . $newnombre)) {
					$rutamedia = '/static/media/cursos/' . $dirmedia . $newnombre;
					$newmedia = URL_MEDIA . $rutamedia;
					if ($oldmedia != '' && $newmedia != $oldmedia) {
						$rutaoldmedia = str_replace(URL_MEDIA, RUTA_MEDIA, $oldmedia);
						if (is_file($rutaoldmedia)) unlink($rutaoldmedia);
					}
					if ($ext == 'zip') {
						$zip = new ZipArchive;
						$fileszip = array();
						if ($zip->open($dir . SD . $newnombre) === TRUE) {
							$next = (strlen('.' . $ext) * -1);
							$nombresinext = substr($newnombre, 0, $next);
							$hayfile = false;
							for ($i = 0; $i < $zip->numFiles; $i++) {
								$namefile = $zip->getNameIndex($i); //obtenemos nombre del fichero
								$rutamedia = '/static/media/cursos/' . $dirmedia . $nombresinext . "/" . $namefile;
								$fileszip['tmp_name'][$i] = $rutamedia; //obtenemos ruta que tendrán los documentos cuando los descomprimamos
								$fileszip['name'][$i] = $namefile;
								$extfile = strtolower(pathinfo($namefile, PATHINFO_EXTENSION));
								if ($extfile == $typefile && $hayfile == false) {
									$hayfile = true;
									$newmedia = URL_MEDIA . $rutamedia;
								}
							}
							if ($hayfile == true) {
								$zip->extractTo($dir . SD . $nombresinext);
								$zip->close();
								//unlink($dir. SD.$newnombre);
							} else {
								$zip->close();
								//unlink($dir. SD.$newnombre);
								echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('File Zip no valido')));
								exit(0);
							}
						}
					}
				}
			}
			echo json_encode(array('code' => 'ok', 'msj' => ucfirst(JrTexto::_('Upload successfully')), 'media' => $newmedia, 'oldmedia' => @$rutaoldmedia, 'filesdezip' => @$fileszip));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function eliminarMenu()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$arrIdChilds = json_decode($arrIdChilds);
			if (!empty($idcurso) && !empty($idcursodetalle)) {
				$idcomplementario = 0;
				if (isset($_REQUEST["icc"]) && @$_REQUEST["icc"] != '') {
					$idcomplementario = $_REQUEST["icc"];
				}

				if ($idcomplementario != 0) {
					$this->oNegAcad_cursodetalle->eliminarMenu($idcomplementario, $idcursodetalle, "acad_cursodetalle_complementario", true);
					@chmod(RUTA_BASE . "static" . SD . "media" . SD . "cursos_complementarios" . SD . "curso_complementario_" . $idcurso . SD . "ses_" . $idcursodetalle, 777);
					@unlink(RUTA_BASE . "static" . SD . "media" . SD . "cursos_complementarios" . SD . "curso_complementario_" . $idcurso . SD . "ses_" . $idcursodetalle);
					foreach ($arrIdChilds as $key => $idcursodetalleChild) {
						@chmod(RUTA_BASE . "static" . SD . "media" . SD . "cursos_complementarios" . SD . "curso_complementario_" . $idcurso . SD . "ses_" . $idcursodetalleChild, 777);
						@unlink(RUTA_BASE . "static" . SD . "media" . SD . "cursos_complementarios" . SD . "curso_complementario_" . $idcurso . SD . "ses_" . $idcursodetalleChild);
					}
				} else {
					$this->oNegAcad_cursodetalle->eliminarMenu($idcurso, $idcursodetalle, "acad_cursodetalle");
					@chmod(RUTA_BASE . "static" . SD . "media" . SD . "cursos" . SD . "curso_" . $idcurso . SD . "ses_" . $idcursodetalle, 777);
					@unlink(RUTA_BASE . "static" . SD . "media" . SD . "cursos" . SD . "curso_" . $idcurso . SD . "ses_" . $idcursodetalle);
					foreach ($arrIdChilds as $key => $idcursodetalleChild) {
						@chmod(RUTA_BASE . "static" . SD . "media" . SD . "cursos" . SD . "curso_" . $idcurso . SD . "ses_" . $idcursodetalleChild, 777);
						@unlink(RUTA_BASE . "static" . SD . "media" . SD . "cursos" . SD . "curso_" . $idcurso . SD . "ses_" . $idcursodetalleChild);
					}
				}
			}
			echo json_encode(array('code' => 'ok', 'msj' => ucfirst(JrTexto::_('Menu')) . ' ' . JrTexto::_('saved successfully'), 'newid' => @$newid));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}


	public function importarjson()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$datos = json_decode($dataimport);
			$usuarioAct = NegSesion::getUsuario();
			$userid = @$usuarioAct['idpersona'];
			$this->oNegAcad_cursodetalle->importaramenu($idcurso, $userid, $datos, $tipo, $clonado);
			echo json_encode(array('code' => 'ok', 'msj' => JrTexto::_('Import successfully')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function reordenar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			$datos = json_decode($dataorden);
			$usuarioAct = NegSesion::getUsuario();
			$userid = @$usuarioAct['idpersona'];
			if (!empty($datos))
				foreach ($datos as $i => $v) {
					$this->oNegAcad_cursodetalle->setCampo(intval($v), 'orden', intval($i));
				}
			echo json_encode(array('code' => 'ok', 'msj' => JrTexto::_('Datos ordenados correctamente')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function setCampojson()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if (empty($_POST)) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('data incomplete')));
				exit(0);
			}
			@extract($_POST);
			if (!empty($id) && !empty($campo) && isset($valor)) {
				$this->oNegAcad_cursodetalle->setCampo($id, $campo, $valor);
			}
			echo json_encode(array('code' => '200', 'msj' => 'ok'));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if (is_a($oRespAjax, 'xajaxResponse')) {
			try {

				if (empty($args[0])) {
					return;
				}
				$this->oNegAcad_cursodetalle->setCampo($args[0], $args[1], $args[2]);
				$oRespAjax->setReturnValue(true);
			} catch (Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			}
		}
	}
}
