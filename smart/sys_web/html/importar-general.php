<style type="text/css">
	.queimportar{ padding-left: 1.5ex; }
	.queimportar .itemme{			
		    margin: 1ex 0px;
		    border-top: 1px solid #ccc;
		    padding-right: 0px;
	}
	.queimportar .icon{ position: absolute; right: 0px; top: 1.3ex }
</style>
<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-4">
        <div class="form-group">
          <label>Que desea importar a su menu </label>
            <div class="cajaselect">
              <select id="tipoimport" class="form-control">              	
              	<option value="curso">Curso</option>
                <option value="nivel">Nivel - version antigua</option>
              </select>
            </div>
        </div>
     </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
    	<div class="form-group">
          <label>El contenido se  </label>
            <div class="cajaselect">
              <select id="esclonado" class="form-control">              	
              	<option value="no">Asignara</option>
                <option value="si">Clonara</option>
              </select>
            </div>
        </div>
    </div>
</div>
<div class="row text-left">
	<div class="col-xs-12 col-sm-6 col-md-4 queimportar" id="queimportar">
		Nombre de curso
	</div>
	<div class="col-12 text-center">
		<hr>
		<button type="button" class="btncancelar btn btn-warning"><i class=" fa fa-close"></i> Cancelar</button>
		<button type="button" id="btnimportardatos" class="btn btn-primary"><i class=" fa fa-save"></i> Guardar seleccionados </button>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(ev){
		var dtinfoimport={};	
		var mostrarcursos=function(en,idcurso,idpadre){
			if(en.length==0) return;
			let $cl=$('#cargando_clone').clone();
			$cl.removeAttr('style').find('span').css({'fontSize':'2.5em'});
			en.html($cl.html());
			var url=_sysUrlBase_;
			if(idcurso==0 && idpadre==0){
				url+= '/acad_curso/buscarjson';
				fromdata={};				
			}else {
				url+='/acad_cursodetalle/buscarjsonconnivel';
				fromdata={idcurso:idcurso,idpadre:idpadre};				
			}
			//dtinfoimport=new Array();
			__sysajax({
				url:url,				
				type:'json',
				fromdata:fromdata,
				showmsjok:false,
				callback:function(rs){									
					let cur=rs.data;					
					let htmltmp='';					
					$.each(cur,function(i,v){					
						if(v.idcursodetalle!=undefined){
							idpad=idcur=v.idcursodetalle;	
							txt='det';
							chk='checked';
							tr=v.tiporecurso;
							tienehijos=v.nhijos>0?true:false;
							//v.tipo='det';
							dtinfoimport[idcur]=v;
							sinchk='custom-control-label';
						}else{
							tienehijos=true;
							idcur=v.idcurso;
							txt='cur';
							idpad=0;
							chk='disabled="disabled" style="display:none"';
							tr='N';
							//v.tipo='cur';
							sinchk='';
							
						}																
						let icon=((tr!='L'&&tr!='E')||tienehijos==true)?'<i class="fa fa-hand-o-down btnimporthijo" data-donde="idtmp_cur'+(txt+idcur)+'" data-tipo="curso" data-idpadre="'+idpad+'" data-idcurso="'+(v.idcurso)+'"></i>':'';
						htmltmp+=	'<div class="col-auto itemme" id="itemme'+(txt+idcur)+'">'
									+ '<span class="custom-checkbox">'
				       				+  '<input type="checkbox" class="custom-control-input" '+chk+' id="chk_'+(txt+idcur)+'" value="'+(idcur)+'">'
				       				+	'<label class="'+sinchk+'" class="" for="chk_'+(txt+idcur)+'" style="padding:0px 1ex;"> ' + (v.nombre) + ' </label>'
				      				+ '</span><span class="icon">'+icon+'</span><div class="mihijodet" id="idtmp_cur'+(txt+idcur)+'"></div>'
				      				+'</div>';
					})
					if(htmltmp=='') en.siblings('.icon').remove();
					en.html(htmltmp);
					//en.sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9});
				}
			});
		}
		var mostrarniveles=function(en,ipadre){
			if(en.length==0) return;
			let idpadre=ipadre||0;
			let $cl=$('#cargando_clone').clone();
			$cl.removeAttr('style').find('span').css({'fontSize':'2.5em'});
			en.html($cl.removeAttr('style').html());
			//dtinfoimport=new Array();
			__sysajax({
				url:_sysUrlBase_+'/niveles/buscarjson',
				fromdata:{idpadre:idpadre},
				type:'json',
				showmsjok:false,			
				callback:function(rs){
					let cur=rs.data;					
					let htmltmp='';
					$.each(cur,function(i,v){						
						tienehijos=v.nhijos>0?true:false;
						dtinfoimport[v.idnivel]=v;	
						let addicon=(tienehijos==true)?'<i class="fa fa-hand-o-down btnimporthijo" data-idpadre="'+v.idnivel+'" data-donde="idtmp_niv'+v.idnivel+'" data-tipo="nivel"></i>':'';
						htmltmp+=	'<div class="col-auto itemme" id="itemme'+(txt+idcur)+'">'
									+ '<span class="custom-checkbox">'
				       				+  '<input type="checkbox" checked class="custom-control-input" id="chk_'+(v.idnivel)+'" value="'+(v.idnivel)+'">'
				       				+	'<label class="custom-control-label" for="chk_'+(v.idnivel)+'" style="padding:0px 1ex">' + (v.nombre) + '</label>'
				      				+ '</span><span class="icon">'+addicon+'</span><div class="mihijodet" id="idtmp_niv'+v.idnivel+'"></div>'
				      				+'</div>';
				     });					
					if(htmltmp=='') en.siblings('.icon').remove();
					en.html(htmltmp);
					//en.sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9});
				}
			});
		}
		$('#queimportar').on('click','.btnimporthijo',function(ev){
			ev.preventDefault();
			let io =$(this);
			let en =$(this).attr('data-donde');
			td=io.closest('.itemme').siblings('.mostrandodet');
			if(td.length){
				td.removeClass('mostrandodet');
				td.children('.icon').children('.fa-hand-o-up').removeClass('fa-hand-o-up').addClass('fa-hand-o-down');
				td.children('.mihijodet').fadeOut();
			}
			if(io.hasClass('datoscargados')){
				if(io.hasClass('fa-hand-o-up')){
					io.removeClass('fa-hand-o-up').addClass('fa-hand-o-down');
					io.closest('.itemme').removeClass('mostrandodet');
					$('#'+en).animateCss({'de':'out','hide':true,'animationName':'fadeOut'}).hide(1000);
				}else{					
					io.removeClass('fa-hand-o-down').addClass('fa-hand-o-up');
					io.closest('.itemme').addClass('mostrandodet');
					$('#'+en).animateCss({'de':'in','showme':true,'animationName':'zoomIn'});
					$('#'+en).show();
				}
				return false;
			}else{				
				io.closest('.itemme').addClass('mostrandodet');				
				io.addClass('datoscargados').removeClass('fa-hand-o-down').addClass('fa-hand-o-up');
			}
			
			let tipo=$(this).attr('data-tipo');
			let idpadre=$(this).attr('data-idpadre');			
			if(tipo=='curso') mostrarcursos($('#'+en),parseInt(io.attr('data-idcurso')),parseInt(io.attr('data-idpadre')));
			else mostrarniveles($('#'+en),idpadre);			
			ev.stopPropagation();
		});

		$('#btnimportardatos').on('click',function(ev){
			chk=$('.queimportar').find('input:checked');
			let infoimport={};
			$.each(chk,function(i,v){
				idinfo=JSON.parse($(v).val());
				infoimport[idinfo]=dtinfoimport[idinfo];				
			});
			let idcurso=<?php echo $_REQUEST["idcurso"];?>;			
			let data=__formdata('');
			data.append('idcurso',idcurso);
			data.append('tipo',$('#tipoimport').val());
			data.append('dataimport',JSON.stringify(infoimport));
			data.append('clonado',$('#esclonado').val());
			//console.log(dts);
			__sysajax({
				url:_sysUrlBase_+'/acad_cursodetalle/importarjson',
				fromdata:data,
				type:'json',
				showmsjok:true,			
				callback:function(rs){					
					if(_isFunction(cargarmenus))
						cargarmenus(idcurso);
					else
					window.location.href = window.location.href;
					//$('.m1 li:first-child').trigger('click');				
				}
			});

		})
		
		$('#tipoimport').change(function(ev){
			let tp=$(this).val();
			dtinfoimport={};
			if(tp=='curso')	mostrarcursos($('#queimportar'),0,0);
			else mostrarniveles($('#queimportar'),0);
		})
		$('#tipoimport').trigger('change');
		//$('#queimportar').sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9});
		/*$('#mindice').find('ul').each(function(i,v){
			$('ul#'+$(v).attr('id')).sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9});
		});*/
	})
</script>
