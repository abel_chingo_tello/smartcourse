<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$cursodetalle=!empty($this->cursodet)?$this->cursodet:'';
$idcurso=!empty($curso["idcurso"])?$curso["idcurso"]:'';
$recurso=!empty($this->recurso)?$this->recurso:'';
$fileurl=is_file(RUTA_MEDIA.@$recurso["imagen"])?URL_MEDIA.@$recurso["imagen"]:URL_MEDIA.'/static/media/cursos/nofoto.jpg';
//var_dump($_REQUEST);
?>
<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-12" style="padding: 1em;">
				<form id="frmdatosmenu" method="post" target="" enctype="multipart/form-data">
				<input type="hidden" name="idcurso" 		id="idcurso" value="<?php echo @$_REQUEST["idcurso"]; ?>">
				<input type="hidden" name="idcursodetalle" 	id="idcursodetalle" value="<?php echo @$cursodetalle["idcursodetalle"]; ?>">
				<input type="hidden" name="idpadre" 		id="idpadre" value="<?php echo !empty($cursodetalle["idpadre"])?$cursodetalle["idpadre"]:$this->idpadre; ?>">
				<input type="hidden" name="orden" 			id="orden" value="<?php echo !empty($cursodetalle["orden"])?$cursodetalle["orden"]:0; ?>">

				<input type="hidden" name="idrecurso" 		id="idrecurso" value="<?php echo @$recurso["idnivel"]; ?>">
				<input type="hidden" name="idrecursopadre" 	id="idrecursopadre" value="<?php echo !empty($recurso["idpadre"])?$recurso["idpadre"]:$this->idrecursopadre; ?>">
				<input type="hidden" name="idrecursoorden" 	id="idrecursoorden" value="<?php echo !empty($recurso["orden"])?$recurso["orden"]:0; ?>">

				<input type="hidden" name="tiporecurso" id="tiporecurso" value="<?php echo !empty($cursodetalle["tiporecurso"])?$cursodetalle["tiporecurso"]:'M'; ?>">
				<input type="hidden" name="idlogro" id="idlogro" value="<?php echo !empty($cursodetalle["idlogro"])?$cursodetalle["idlogro"]:'0'; ?>">				
				<input type="hidden" name="oldimagen" id="oldimagen" value="<?php echo !empty($recurso["imagen"])?$recurso["imagen"]:''; ?>">
				<input type="hidden" name="accmenu" id="accmenu" value="<?php echo !empty($_REQUEST["acc"])?$_REQUEST["acc"]:'add'?>">
				<input type="hidden" id="esfinal" name="esfinal" value="1" >		
				<div class="card text-center">				 
				  <div class="card-body">
				    <h5 class="card-title"><?php echo JrTexto::_('Datos del tema');?></h5>
				    <div class="row">
				    	<div class="col-12 col-sm-6 col-md-6">
				    		<div class="row">
					    		<div class="col-12 form-group">
						            <label style=""><?php echo JrTexto::_('Titulo');?></label> 
						            <input type="text" name="nombre" id="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Nombre 01"))?>" value="<?php echo @$recurso["nombre"]; ?>">					           
						        </div>
						        <div class="col-12 form-group">
						            <label style=""><?php echo JrTexto::_('Descripción');?></label> 
						            <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del menu/nivel/unidad/lección"))?>"><?php echo @$recurso["descripcion"]; ?></textarea>
						        </div>
						        <div class="col-6 form-group text-left ">		        	
						            <label style=""><?php echo JrTexto::_('Color');?> </label> 
						            <input type="" name="color" value="transparent" class="form-control" >			       
						        </div> 
                            </div>		       
				    	</div>
				    	<div class="col-12 col-sm-6 col-md-6">
				    		<div class="row">
					    		<div class="col-12 form-group text-center">
						            <label style="float: none"><?php echo JrTexto::_('Portada');?></label> 
						            <div style="position: relative;" class="frmchangeimage">
						                <div class="toolbarmouse text-center">                  
						                  	<div class="toolbarmouse text-center">                  
							                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
							                </div>
						                </div>
						                <div id="sysfileimagen<?php echo $idgui; ?>">
						                	<img id="verimg<?php echo $idgui; ?>" src="<?php echo $fileurl; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
                                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">
						                </div>
						                <!--div id="sysfileimagen<?php echo $idgui; ?>">
						                	<img id="verimg<?php echo $idgui; ?>" src="<?php echo $fileurl; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
						                </div-->
						                <small>Tamaño 250px x 360px</small>                
						            </div>				           
						        </div>
					        </div>				    		
				    	</div>
				    </div>				   
				  </div>
				  <div class="card-footer text-muted">
				  	<button type="button" class="btncancelar btn btn-warning"><i class=" fa fa-close"></i> Cancelar</button>
				    <button type="submit" class="btn btn-primary btnguardarmenu"><i class=" fa fa-save"></i> Guardar </button>
				  </div>
				</div>
				</form>
			</div>
		</div>
	</div>	
</div>

