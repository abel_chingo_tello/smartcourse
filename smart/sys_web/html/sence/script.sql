ALTER TABLE `bolsa_empresas` 
ADD COLUMN `RutOtec` varchar(10) NULL DEFAULT NULL COMMENT 'RUT OTEC sin puntos y con digito verificador, en formato' AFTER `idpersona`,
ADD COLUMN `Token` varchar(36) NULL DEFAULT NULL COMMENT 'Token del OTEC.' AFTER `RutOtec`,
ADD COLUMN `CodSense` varchar(10) NULL DEFAULT -1 COMMENT 'Código SENCE del curso.' AFTER `Token`,
ADD COLUMN `LineaCapacitacion` tinyint(0) NULL DEFAULT 3 COMMENT 'Identificador de la línea de capacitación.\r\n3 = Impulsa Personas.' AFTER `CodSense`;

ALTER TABLE `acad_curso`
ADD COLUMN `CodigoCurso` varchar(50) NULL DEFAULT -1 COMMENT 'Identificador del curso, mínimo 8 caracteres.' AFTER `avance_secuencial`;