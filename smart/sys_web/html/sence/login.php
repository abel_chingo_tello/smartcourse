<?php
defined('RUTA_BASE') or die();
$idgui = uniqid();
?>
<div id="wrapper">
    <div id="login" class="animate form ">
        <section class="login_content text-center">
            <img src="<?php echo $this->documento->getUrlStatic(); ?>/sence.png" class="img-fluid img-responsive" alt="_logo_" id="logo">
            <form id="frmlogin" name = "formPost" method="post" action="<?php echo $this->rutaSense; ?>IniciarSesion">
                <h1><?php echo JrTexto::_('Log In'); ?></h1>
                <input type="hidden" name="RutOtec" value="<?php echo $this->RutOtec; ?>" />
                <input type="hidden" name="Token" value="<?php echo $this->Token; ?>" />
                <input type="hidden" name="CodSence" value="<?php echo $this->CodSense; ?>" />
                <input type="hidden" name="CodigoCurso" value="<?php echo $this->CodigoCurso; ?>" />
                <input type="hidden" name="LineaCapacitacion" value="<?php echo $this->LineaCapacitacion; ?>" />
                <input type="hidden" name="UrlRetoma" value="<?php echo $this->urlActual; ?>" />
                <input type="hidden" name="UrlError" value="<?php echo $this->urlActual; ?>" />
                <input type="hidden" name="RunAlumno" value="<?php echo $this->RunAlumno; ?>" />
                <input type="hidden" name="IdSesionAlumno" value="<?php echo $this->IdSesionAlumno; ?>" />
                <div class="clearfix"></div>
                <!-- <div class="separator">
                    <a target="_blank" href="https://cus.sence.cl/Account/Registrar" class="to_recuperar"> <?php echo JrTexto::_('Registrar CS'); ?></a>
                    <a target="_blank" href="https://cus.sence.cl/Account/RecuperarClave" class="to_recuperar"> <?php echo JrTexto::_('Recuperar CS'); ?></a>
                    <a target="_blank" href="https://cus.sence.cl/Account/CambiarClave" class="to_recuperar"> <?php echo JrTexto::_('Cambiar CS'); ?></a>
                    <a target="_blank" href="https://cus.sence.cl/Account/ActualizarDatos" class="to_recuperar"> <?php echo JrTexto::_('Actualizar Datos'); ?></a>
                    <div class="clearfix"></div>
                    <br>
                </div> -->
            </form>
        </section>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#frmlogin").submit();
    });
</script>