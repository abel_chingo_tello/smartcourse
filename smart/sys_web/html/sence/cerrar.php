<?php
defined('RUTA_BASE') or die();
$idgui = uniqid();
?>
<?php if(!empty($this->mensaje) && !empty($this->Haysence)):?>
<!--
	FORMULARIO que se dispara o ejecuta en el boton de cerrar curso, funcion ubicada en el modulo "top" que compone la PLANTILLA
-->
<form id="frmCerrarSence" name = "formPost" method="post" action="<?php echo @$this->rutaSense; ?>CerrarSesion">
	<input type="hidden" name="RutOtec" value="<?php echo @$this->RutOtec; ?>" />
	<input type="hidden" name="Token" value="<?php echo @$this->Token; ?>" />
	<input type="hidden" name="CodSence" value="<?php echo @$this->CodSense; ?>" />
	<input type="hidden" name="CodigoCurso" value="<?php echo @$this->CodigoCurso; ?>" />
	<input type="hidden" name="LineaCapacitacion" value="<?php echo @$this->LineaCapacitacion; ?>" />
	<input type="hidden" name="RunAlumno" value="<?php echo @$this->RunAlumno; ?>" />
	<input type="hidden" name="IdSesionAlumno" value="<?php echo @$this->IdSesionAlumno; ?>" />
	<input type="hidden" name="IdSesionSence" value="<?php echo @$this->IdSesionSence; ?>" />
	<input type="hidden" name="UrlRetoma" value="<?php echo @$this->urlCerrar; ?>" />
	<input type="hidden" name="UrlError" value="<?php echo @$this->urlCerrar; ?>" />
</form>
<?php endif; ?>
<script>
    const msj = <?= !empty($this->mensaje) ? 'true' : 'false'; ?>;
    const cerrarventana = ()=>{
        close();
        window.open('','_parent','');
        window.close();
    }
    if(msj){
        Swal.fire({
            icon: 'warning',
            title: 'Expiro el timpo de cierre de sesión',
            text:'¿Desea ir nueva mente a la ventana de cerrar sesión de SENCE?',
            showCancelButton:true,
            confirmButtonText: `Cerrar sesión de SENCE`,
            cancelButtonText: `Salir`,
        }).then((result) => {
            if(result.value){
                const select = $("#frmCerrarSence")
                if(select != null && select.length > 0){
                    select.submit();
                }
            }else{
                cerrarventana()
            }
        });
    }else{
        cerrarventana()
    }
</script>