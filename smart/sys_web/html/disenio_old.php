<?php 
	$rustatic=$this->documento->getUrlStatic();
	$idgui=uniqid();
	$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
	$idcursodet=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:0;
	$fileurl=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
?>
<style type="text/css">
	body{ overflow: hidden;}
	#mainpage,.seepage{ width: 100%; height: 100%; overflow: auto; }	
	.islayout.active{background: transparent;}
	#accionesedicion, .hide , .islayout ._acccont, .controlsdisenio, ._subirfileedit .toolbaredit, ._subirvisoredit .toolbaredit { display: none }
	#layoutver{position: fixed; bottom: 0.1ex;}
	#layoutver span, .savecambios { padding: 1ex 1ex 0.2ex 1ex; border-radius: 10px 10px 0 0; color: #fbfbfb; cursor: pointer;}
	iframe{ border: 0px; width: 100%; height: 100%; }
	.addnegrita{ font-weight: bold !important; }
	.addcursiva{ font-style: italic !important; }
	.addsubrayado{ text-decoration: underline !important; }
	#pagecontroler{position:  relative; height:  100%; width:  calc(100% + 30px); overflow:hidden; }	
	#mainpage { overflow-y: auto; }
	.seepage > ._cont, .widgetcont{margin: 0px;width: 100%;height: 100%;}
	.btnnavegacionPage{ display: none; position: absolute; right: 63px; top:0.5ex;}
	::-webkit-scrollbar { width: 10px;}
	/*::-webkit-scrollbar { width: 10px;}*/
	::-webkit-scrollbar-thumb { background: #888;}
	::-webkit-scrollbar-thumb:hover { background: #222; }
</style>
<link rel="stylesheet" type="text/css" id="linkviewen" href="<?php echo $this->documento->getUrlTema();?>/css/disenio.css">
<div class="row viewendisenio" id="pagecontroler">
	<?php if(!empty($this->sesion)){
		if(file_exists(RUTA_MEDIA.$this->sesion["html"])) require_once(RUTA_MEDIA.$this->sesion["html"]);
	}else {?>
	<div id="mainpage" data-npage="1"></div>
	<?php } ?>
	<div id="layoutver" class="viewendisenio" >
		<span data-class="viewendisenio" title="Diseño" data-placement="bottom" data-content="Ver vista de Diseño" class="btn-info verpopover">Vista Diseño</span>
		<span data-class="viewenautor" title="Autor" data-placement="bottom" data-content="Ver como un Autor" class="btn-warning verpopover">Vista Autor</span>
		<span data-class="viewenalumno" title="Alumno" data-placement="bottom" data-content="Ver como un Alumno" class="btn-success verpopover">Vista Alumno</span>		
	</div>
	<div id="accionesedicion">
		<div class="btn-group-vertical btn-group-sm">
		  <button type="button" class="btn btn-success verpopover savecambios" title="Guardar" data-content="Guarda los cambios para este menu"><i class="fa fa-save"></i> Guardar</button>
		  <!--button type="button" class="btn btn-warning verpopover" data-content="Importar desde una plantilla"><i class="fa fa-upload"></i> Importar</button>
		  <button type="button" class="btn btn-info verpopover" data-content="Espotar a plantilla"><i class="fa fa-download"></i> Exportar</button-->
		</div>		
	</div>	
	<div class="controlsdisenio  bg-primary" style="height: 100%; overflow-y: auto;">		
		<div class="showherramientas">
			<!--div class="col-6 cicon ciconlayout"><img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/layout.gif"></div-->			
			<div class="cicon col-12 ciconconfig verpopover" data-placement="bottom" title="Herramientas" data-content="Mostrar o ocultar herramientas"><img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/config.gif">
			</div>						
		</div>
		<div class="iconconfig">
			<hr>
			<div class="col-12 cicon verpopover" data-placement="bottom" title="Indice" data-content="Agregar Indice" data-acc="addindice">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/menu.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="bottom" title="Texto" data-content="Agregar texto" data-acc="texto">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/texto.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="bottom" title="Imagen" data-content="Agregar una imagen" data-acc="imagen">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/imagen.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="Audio" data-content="Agregar un audio" data-acc="audio">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/audio.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="Video" data-content="agregar un video" data-acc="video">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/video.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="PDF" data-content="Agregar un archivo pdf" data-acc="pdf">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/pdf.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="HTML" data-content="Agregar contenido HTML - seleccione un zip si incluye mas contenido" data-acc="html">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/html.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="HTML" data-content="Agregar SWF - archivo Flash" data-acc="swf">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/swf.gif"></div>
			<hr>
			<div class="col-12 cicon verpopover" data-placement="top" title="Widgets" data-content="Agregar contenedores de menu" data-acc="widgets">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/iwidget.png"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="SmartTic" data-content="Agregar ejecicios interactivos " data-acc="smartic">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/ismarttic.png"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="Juegos" data-content="Agregar juegos interactivos" data-acc="game">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/igame.png"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="SmartQuiz" data-content="Agregar Examenes" data-acc="smartquiz">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/ismartquiz.gif"></div>
			<hr>
			<div class="col-12 cicon verpopover" data-placement="top" title="Diseño en columnas" data-content="Dividir pagina en columnas" data-acc="2collay">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/layout3.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="Diseño en filas" data-content="Dividir pagina en filas" data-acc="2rowlay">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/layout2.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="Disenio grilla" data-content="Diseñar distribucion" data-acc="grilla">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/grilla.gif"></div>
			<div class="col-12 cicon verpopover" data-placement="top" title="Pagina" data-content="Agregar una nueva pagina" data-acc="1rowlay">
				<img class="img-fluid img-responsive" src="<?php echo $rustatic; ?>/media/image/icons/layout1.gif"></div>			
		</div>
	</div>	
</div>
<div id="iconclonar<?php echo $idgui; ?>" style="display: none;">
	<div id="rowlay">
		<div class="col-12 islayout">
			<div class="row _cont"></div>
			<div class="_acccont">
				<span class="btn-group btn-group-sm btn-primary">
					<span class="btn name">Contenedor </span> <span>: </span>
					<span class="btn layouttxtleft" title="Alinear a la Izquierda"><i class="fa fa-align-left"></i></span>
					<span class="btn layouttxtcent" title="Alinear al centro"><i class="fa fa-align-center"></i></span>
					<span class="btn layouttxtrigh" title="Alinear a la derecha"><i class="fa fa-align-right"></i></span>
					<span class="btn layouttxtjust" title="Justificar"><i class="fa fa-align-justify"></i></span>
					<span class="btn layouttxtnegr" title="Texto en Negrita"><i class="fa fa-bold"></i></span>
					<span class="btn layouttxtcurs" title="Texto en cursiva"><i class="fa fa-italic"></i></span>
					<span class="btn layouttxtsubr" title="Texto subrayado"><i class="fa fa-underline"></i></span>
					<span class="btn layouterase" title="Borrar todo el contenido"><i class="fa fa-eraser"></i></span>
					<span class="btn layoutcopy" title="Copiar Contenido"><i class="fa fa-copy"></i></span>
					<span class="btn layoutpaste" title="Pegar Contenido"><i class="fa fa-paste"></i></span>								
					<span class="btn layoutedit" title="Editar Propiedaes"><i class="fa fa-pencil"></i></span>
					<span class="btn layouteli" title="Eliminar Layout"><i class="fa fa-trash"></i></span>
				</span>
			</div>
		</div>			
	</div>	
	<div id="clon_texto">		
		<div class="col-12 text-center _subirfileedit">	            
            <div style="position: relative; width: 100%; height: 100%; display: flex;  align-items: center; justify-content: center;" >
                <div class="toolbaredit btn-group text-center"  data-clean="_txtedit">   
                	<!--button type="button" class="btn btn-sm"><i class="fa fa-cloud-upload"></i></button-->
					<button type="button" class="_btneditar_ btn btn-sm"><i class="fa fa-pencil"></i></button> 
					<button type="button" class="_btneliminarcontenido_ btn btn-sm"><i class="fa fa-trash"></i></button>
                </div>	                
                <div class="txtedit text-left" style="width: 100%;"> Click Aqui para modificar el texto </div>
            </div>				           
        </div>       
	</div>
	<div id="clon_media">		
		<div class="col-12 text-center _subirfileedit">	            
            <div style="position: relative; width: 100%; height: 100%; display: flex;  align-items: center; justify-content: center;" >
                <div class="toolbaredit btn-group text-center" data-clean="_filemedia">   
                	<button type="button" class="_btnsubirfile_ btn btn-sm"><i class="fa fa-cloud-upload"></i></button>
					<button type="button" class="_btneditar_ btn btn-sm"><i class="fa fa-pencil"></i></button>
					<button type="button" class="_btneliminarcontenido_ btn btn-sm"><i class="fa fa-trash"></i></button>
					<input type="file" class="filetmp" style="display: none;" >
                </div> 
                <audio class="filemedia" src="" controls="true" style="max-width: 100%; max-height: 100%;"></audio>
                <video class="filemedia" src="" controls="true" style="width: 100%;  max-width: 100%; max-height: 100%;"></video>
                <img src="<?php echo $rustatic; ?>/media/image/icons/imagen2.gif" class="tmpmedia img-responsive img-fluid center-block">
            </div>				           
        </div>      
	</div>
	<div id="clon_file">		
		<div class="col-12 text-center _subirvisoredit" style="padding: 0px;">	            
            <div style="position: relative; width: 100%; height: 100%; display: flex;  align-items: center; justify-content: center;" >
                <div class="toolbaredit btn-group text-center" data-clean="_tmpmediapage">   
                	<button type="button" class="_btnsubirvisor_ btn btn-sm"><i class="fa fa-cloud-upload"></i></button>
					<button type="button" class="_btneditar_ btn btn-sm"><i class="fa fa-pencil"></i></button>
					<button type="button" class="_btneliminarcontenido_ btn btn-sm"><i class="fa fa-trash"></i></button>
					<input type="file" class="filetmp" style="display: none;" >
                </div>               
                <iframe  src="" class="filemedia" style="width: 100%;"></iframe>             
                <img src="<?php echo $rustatic; ?>/media/image/icons/pdf2.gif" class="img-responsive img-fluid center-block">
            </div>				           
        </div>        
	</div>	
  	<div id="clone_progressup" class="text-center" style="position: absolute; right: 1%; left: 1%; top:1%; bottom: 1%; background: #afaca99c;">	 
  		<div style="position:relative; top: 45%;">
	  		<i class="fa fa-spinner fa-spin" style="font-size: 8em; color: #461d19;"></i>  		
	  		<div class="progress">  			
	    		<div id="progressavance" class=" progress-bar progress-bar-striped progress-bar-animated focus active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
	    			<div><?php echo JrTexto::_('loading').'...'; ?><span id="cantidadupload">0%</span></div>
	    		</div>
    		</div>
		</div>
	</div>
	<div id="clone_editlayout">
		<div class="row">				
			<div class="col-12">
				<div class="row">
		    		<div class="col-12 form-group">
			            <label style="" class="labelnombre"><?php echo JrTexto::_('Nombre del Layout');?></label> 
			            <input type="text" name="layoutname" class="layoutname form-control">
			        </div>
			        <div class="col-4 form-group">
			            <label style=""><?php echo JrTexto::_('Tipo de fuente');?></label>
			            <div class="cajaselect">
                		<select name="layouttipofuente" class="layouttipofuente form-control">
			            	<option values="Arial, Helvetica, sans-serif">Arial, Helvetica, sans-serif</option>
							<option values="‘Arial Black’, Gadget, sans-serif">‘Arial Black’, Gadget, sans-serif</option>
							<option values="‘Bookman Old Style’, serif">‘Bookman Old Style’, serif</option>
							<option values="‘Comic Sans MS’, cursive">‘Comic Sans MS’, cursive</option>
							<option values="Courier, monospace">Courier, monospace</option>
							<option values="‘Courier New’, Courier, monospace">‘Courier New’, Courier, monospace</option>
							<option values="Garamond, serif">Garamond, serif</option>
							<option values="Georgia, serif">Georgia, serif</option>
							<option values="Impact, Charcoal, sans-serif">Impact, Charcoal, sans-serif</option>
							<option values="‘Lucida Console’, Monaco, monospace">‘Lucida Console’, Monaco, monospace</option>
							<option values="‘Lucida Sans Unicode’, ‘Lucida Grande’, sans-serif">‘Lucida Sans Unicode’, ‘Lucida Grande’, sans-serif</option>
							<option values="‘MS Sans Serif’, Geneva, sans-serif">‘MS Sans Serif’, Geneva, sans-serif</option>
							<option values="‘MS Serif’, ‘New York’, sans-serif">‘MS Serif’, ‘New York’, sans-serif</option>
							<option values="‘Palatino Linotype’, ‘Book Antiqua’, Palatino, serif">‘Palatino Linotype’, ‘Book Antiqua’, Palatino, serif</option>
							<option values="Symbol, sans-serif">Symbol, sans-serif</option>
							<option values="Tahoma, Geneva, sans-serif">Tahoma, Geneva, sans-serif</option>
							<option values="‘Times New Roman’, Times, serif">‘Times New Roman’, Times, serif</option>
							<option values="‘Trebuchet MS’, Helvetica, sans-serif">‘Trebuchet MS’, Helvetica, sans-serif</option>
							<option values="Verdana, Geneva, sans-serif">Verdana, Geneva, sans-serif</option>
							<option values="Webdings, sans-serif">Webdings, sans-serif</option>
							<option values="Wingdings, ‘Zapf Dingbats’, sans-serif">Wingdings, ‘Zapf Dingbats’, sans-serif</option>
			            </select>
			            </div>
			        </div>
			        <div class="col-4 form-group">
			            <label style=""><?php echo JrTexto::_('Tamaño de fuente');?></label>
			            <div class="cajaselect">
                		<select name="layoutsizefuente" class="layoutsizefuente form-control">
			            	<option values="8px">8px</option>
							<option values="10px">10px</option>
							<option values="12px" selected="selected">12px</option>
							<option values="14px">14px</option>
							<option values="16px">16px</option>
							<option values="18px">18px</option>
							<option values="20px">20px</option>
							<option values="24px">24px</option>
							<option values="26px">26px</option>
							<option values="28px">28px</option>
							<option values="30px">30px</option>
							<option values="0.5em">0.5em</option>
							<option values="1em">1em</option>
							<option values="1.5em">1.5em</option>
							<option values="2em">2em</option>
							<option values="2.5em">2.5em</option>
							<option values="3em">3em</option>
							<option values="3.5em">3.5em</option>
							<option values="4em">4em</option>
							<option values="5em">5em</option>
							<option values="6em">6em</option>
							<option values="7em">7em</option>
							<option values="8em">8em</option>
							<option values="9em">9em</option>
							<option values="10em">10em</option>
			            </select>
			            </div>			            
			        </div>
			        <div class="col-4 form-group nopage">
			            <label style=""><?php echo JrTexto::_('Ancho');?></label> 
			            <div class="cajaselect">
                		<select name="layoutancho" class="layoutancho form-control">
			            	<option values="10%">10%</option>
							<option values="15%">15%</option>
							<option values="20%">20%</option>
							<option values="25%">25%</option>
							<option values="30%">30%</option>
							<option values="45%">45%</option>
							<option values="50%">50%</option>
							<option values="55%">55%</option>
							<option values="60%">60%</option>
							<option values="65%">65%</option>
							<option values="70%">70%</option>
							<option values="85%">85%</option>
							<option values="90%">90%</option>
							<option values="100%" selected="selected">100%</option>
			            </select>
			            </div>			            
			        </div>
			        <div class="col-4 form-group nopage">
			            <label style=""><?php echo JrTexto::_('Alto');?></label> 
			            <div class="cajaselect">
                		<select name="layoutalto" class="layoutalto form-control">
			            	<option values="10%">10%</option>
							<option values="15%">15%</option>
							<option values="20%">20%</option>
							<option values="25%">25%</option>
							<option values="30%">30%</option>
							<option values="45%">45%</option>
							<option values="50%">50%</option>
							<option values="55%">55%</option>
							<option values="60%">60%</option>
							<option values="65%">65%</option>
							<option values="70%">70%</option>
							<option values="85%">85%</option>
							<option values="90%">90%</option>
							<option values="100%" selected="selected">100%</option>
			            </select>
			            </div>			            
			        </div>
			        <div class="col-4 form-group text-left">
			            <label style=""><?php echo JrTexto::_('Color de Texto');?> </label>  
			            <input type="color" name="layoutcolortexto" class="layoutcolortexto sincolor" data-color="#007bff" style="margin-left: 1ex; display:none;" value=""><br>
			            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
				        <a style="cursor:pointer;" class="chkoption fa fa-check-circle"><span> Si</span></a>
			        </div>
			        <div class="col-4 form-group text-left ">
			            <label style=""><?php echo JrTexto::_('Color de fondo');?> </label>  
			            <input type="color" name="layoutcolorfondo" class="layoutcolorfondo sincolor" data-color="#ffffff" style="margin-left: 1ex; display:none;" value=""><br>
			            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
				        <a style="cursor:pointer;" class="chkoption fa fa-check-circle"><span> Si</span></a>
			        </div>
			        <div class="col-4 form-group text-center ">	           
			            <label style="float: none"><?php echo JrTexto::_('Imagen de fondo');?></label> 
			            <div style="position: relative;" class="frmchangeimage">
			                <div class="toolbarmouse text-center">	                                  
			                  <span class="btn subirimg" style="background: #e2e2e285;"><i class="fa fa-pencil" style="color:#000"></i></span>	                  
			                </div>
			                <input type="file" class="input-file-invisible" name="imagen" accept="image/x-png,image/gif,image/jpeg" style="display:none; width:1px; height: 1px;">
			                <img src="<?php echo $fileurl; ?>" class="imgfondo img-responsive center-block" style="max-width:100px; max-height:100px;">			                
			            </div>
			        </div>
			        <div class="col-12 form-group">
			            <label style=""><?php echo JrTexto::_('Estilos CSS');?></label> 
			            <textarea rows="3" style="width: 100%" class="layoutstyle form-control"></textarea>
			        </div>
			    </div>
			</div>			
			<div class="col-12 text-center"><hr>
				<a href="javascript:void(0)" class="aplicarcss btn btn-warning"><i class="fa fa-save"></i> Guardar</a>
				<a href="javascript:void(0)" class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</a>
			</div>
		</div>
	</div>
	<div id="clone_editmenuwitget">
		<div class="row">
			<div class="col-12 form-group">
				<label style=""><?php echo JrTexto::_('Texto');?></label> 
			    <input type="text" class="texto form-control">
			</div>
			<div class="col-3 form-group">
	            <label style=""><?php echo JrTexto::_('Tipo de fuente');?></label>
	            <div class="cajaselect">    
		            <select name="tipofuente" class="tipofuente form-control">
		            	<option values="Arial, Helvetica, sans-serif">Arial, Helvetica, sans-serif</option>
						<option values="‘Arial Black’, Gadget, sans-serif">‘Arial Black’, Gadget, sans-serif</option>
						<option values="‘Bookman Old Style’, serif">‘Bookman Old Style’, serif</option>
						<option values="‘Comic Sans MS’, cursive">‘Comic Sans MS’, cursive</option>
						<option values="Courier, monospace">Courier, monospace</option>
						<option values="‘Courier New’, Courier, monospace">‘Courier New’, Courier, monospace</option>
						<option values="Garamond, serif">Garamond, serif</option>
						<option values="Georgia, serif">Georgia, serif</option>
						<option values="Impact, Charcoal, sans-serif">Impact, Charcoal, sans-serif</option>
						<option values="‘Lucida Console’, Monaco, monospace">‘Lucida Console’, Monaco, monospace</option>
						<option values="‘Lucida Sans Unicode’, ‘Lucida Grande’, sans-serif">‘Lucida Sans Unicode’, ‘Lucida Grande’, sans-serif</option>
						<option values="‘MS Sans Serif’, Geneva, sans-serif">‘MS Sans Serif’, Geneva, sans-serif</option>
						<option values="‘MS Serif’, ‘New York’, sans-serif">‘MS Serif’, ‘New York’, sans-serif</option>
						<option values="‘Palatino Linotype’, ‘Book Antiqua’, Palatino, serif">‘Palatino Linotype’, ‘Book Antiqua’, Palatino, serif</option>
						<option values="Symbol, sans-serif">Symbol, sans-serif</option>
						<option values="Tahoma, Geneva, sans-serif">Tahoma, Geneva, sans-serif</option>
						<option values="‘Times New Roman’, Times, serif">‘Times New Roman’, Times, serif</option>
						<option values="‘Trebuchet MS’, Helvetica, sans-serif">‘Trebuchet MS’, Helvetica, sans-serif</option>
						<option values="Verdana, Geneva, sans-serif">Verdana, Geneva, sans-serif</option>
						<option values="Webdings, sans-serif">Webdings, sans-serif</option>
						<option values="Wingdings, ‘Zapf Dingbats’, sans-serif">Wingdings, ‘Zapf Dingbats’, sans-serif</option>
		            </select>
	            </div>
	        </div>
	        <div class="col-3 form-group">
	            <label style=""><?php echo JrTexto::_('Tamaño de fuente');?></label>
	            <div class="cajaselect">
            		<select name="sizefuente" class="sizefuente form-control">
		            	<option values="8px">8px</option>
						<option values="10px">10px</option>
						<option values="12px" selected="selected">12px</option>
						<option values="14px">14px</option>
						<option values="16px">16px</option>
						<option values="18px">18px</option>
						<option values="20px">20px</option>
						<option values="24px">24px</option>
						<option values="26px">26px</option>
						<option values="28px">28px</option>
						<option values="30px">30px</option>
						<option values="0.5em">0.5em</option>
						<option values="1em">1em</option>
						<option values="1.5em">1.5em</option>
						<option values="2em">2em</option>
						<option values="2.5em">2.5em</option>
						<option values="3em">3em</option>
						<option values="3.5em">3.5em</option>
						<option values="4em">4em</option>
						<option values="5em">5em</option>
						<option values="6em">6em</option>
						<option values="7em">7em</option>
						<option values="8em">8em</option>
						<option values="9em">9em</option>
						<option values="10em">10em</option>
		            </select>
		        </div>
	        </div>
	        <div class="col-3 form-group text-left">
	            <label style=""><?php echo JrTexto::_('Color de Texto');?> </label>  
	            <input type="color" class="colortexto sincolor" data-color="#007bff" style="margin-left: 1ex; display:none;" value=""><br>
	            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
		        <a style="cursor:pointer;" class="chkcambiarcolor fa fa-check-circle"><span> Si</span></a>
	        </div>
	        <div class="col-3 form-group text-left ">
	            <label style=""><?php echo JrTexto::_('Color de fondo');?> </label>  
	            <input type="color" class="colorfondo sincolor" data-color="#ffffff" style="margin-left: 1ex; display:none;" value=""><br>
	            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
		        <a style="cursor:pointer;" class="chkcambiarcolor fa fa-check-circle"><span> Si</span></a>
	        </div>
	        <div class="col-6 form-group text-center ">	           
	            <label style="float: none"><?php echo JrTexto::_('Imagen de fondo');?></label> 
	            <div style="position: relative;" class="frmchangeimage">
	                <div class="toolbarmouse text-center">	                                  
	                  <span class="btn subirimg" style="background: #e2e2e285;"><i class="fa fa-pencil" style="color:#000"></i></span>	                  
	                </div>
	                <input type="file" class="input-file-invisible" name="imagen" accept="image/x-png,image/gif,image/jpeg" style="width:1px; height: 1px;">
	                <img src="<?php echo $fileurl; ?>" class="img-responsive center-block" style="max-width:150px; max-height:150px;">
	                <small>Tamaño 200px x 200px</small> 
	            </div>
	        </div>
	        <div class="col-6 form-group text-left">
	            <label style=""><?php echo JrTexto::_('Enlace A');?> </label>  
	            <div class="cajaselect">
	            <select name="link" class="link form-control"></select>
	            </div>
	             <input type="text" class="otrolink form-control" placeholder="Copie o escria el enlace" style="display: none;">          
	        </div>	
			<div class="col-12 text-center"><hr>
				<!--a href="javascript:void(0)" class="aplicarcss btn btn-warning"><i class="fa fa-save"></i> Guardar</a-->
				<a href="javascript:void(0)" class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</a>
			</div>
		</div>
	</div>
	<div id="clone_pagenavegar">
		<div class="btn-group btnnavegacionPage">
		  <button type="button" class="btnirlistado btn btn-primary" title="Salir del cursos"><i class="fa fa-close"></i></button>
		  <button type="button" class="btnircurso btn btn-primary" title="Ir al inicio del curso"><i class="fa fa-bars"></i></button>		  		  
		  <button type="button" class="btnirmenu btn btn-primary" title="Ir al inicio de esta sesion"><i class="fa fa-home"></i></button>		  
		  <button type="button" class="btnirback btn btn-primary" title="Ir a la Pagina anterior"><i class="fa fa-angle-double-left"></i></button>
		  <button type="button" class="btnirnext btn btn-primary" title="Ir a la siguiente Pagina"><i class="fa fa-angle-double-right"></i></button>
		</div>
	</div>
	<div id="clone_grilla">
		<div class="row">				
			<div class="col-12">
				<div class="row">		    		
			        <div class="col-4 form-group">
			            <label style=""><?php echo JrTexto::_('Número de Filas');?></label>
			            <div class="cajaselect">
                		<select name="nfil" class="nfil form-control">
			            	<option values="1">1</option>
							<option values="2">2</option>
							<option values="3">3</option>
							<option values="4">4</option>
							<option values="5">5</option>
							<option values="6">6</option>
							<option values="7">7</option>
							<option values="8">8</option>
							<option values="9">9</option>
							<option values="10">10</option>	
							<option values="11">11</option>
							<option values="12">12</option>						
			            </select>
			            </div>
			        </div>
			        <div class="col-4 form-group">
			            <label style=""><?php echo JrTexto::_('Número de columnas');?></label>
			            <div class="cajaselect">
                		<select name="ncol" class="ncol form-control">
			            	<option values="1">1</option>
							<option values="2">2</option>
							<option values="3">3</option>
							<option values="4">4</option>
							<option values="5">5</option>
							<option values="6">6</option>													
			            </select>
			            </div>			            
			        </div>
			    </div>
			</div>			
			<div class="col-12 text-center"><hr>
				<a href="javascript:void(0)" class="aplicarcss btn btn-warning"><i class="fa fa-save"></i> Guardar</a>
				<a href="javascript:void(0)" class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		let tmpidcuro='<?php echo $idcurso;?>';
		let tmpiddetalle='<?php echo $idcurso;?>';		
		if(__isEmpty($('#mainpage'))){
			$('#mainpage').append($('#clone_pagenavegar').html());		
			$addlayout=$('#iconclonar<?php echo $idgui; ?>').find('#rowlay').clone();
			$addlayout.children('.islayout').addClass('seepage').attr('data-nlayout',1).attr('id','page'+__idgui());
			$addlayout.find('span.name').text(' Pagina 01 ');			
			$('#mainpage .btnnavegacionPage').before($addlayout.html()).attr('data-npage',2);			
		}else{
			$('#mainpage .btnnavegacionPage').remove();
			$('#mainpage').append($('#clone_pagenavegar').html());
		}

		$('.menucircleitem').click(function(){
			let page=$(this).attr('data-link')||'';
			if(page!='')__mostrarPage(page);
		})

		var mainpagescoll=function(cls){
			if(cls!='viewendisenio') $('#mainpage').css('width','100%');
			else{
				if($('.iconconfig').hasClass('hide')){
					$('#mainpage').css('width','100%');
				}else{
					$('#mainpage').css('width','calc(100% - 62px)');
				}
			}
		}
		mainpagescoll('viewendisenio');	

		$('.savecambios').click(function(){
			$page=$('#mainpage').clone();
			var html=$('<div>').append($page.css({'width':''})).html();
			var data=__formdata('');
			data.append('idcurso','<?php echo $idcurso;?>');
			data.append('idcursodetalle','<?php echo $idcursodet;?>');
			data.append('html',html);
			//data.append('typefile',type);
			__sysajax({ 
				fromdata:data,
            	url:_sysUrlBase_+'/acad_cursodetalle/guardarSesionhtml',            	
        	});
		});
		/*$('.ciconlayout').click(function(){			
			//$('.iconlayout').show();
			//$('.iconconfig').hide();
		});*/
		$('.ciconconfig').click(function(){
			$tolls=$(this).closest('.showherramientas').siblings();
			$tolls.toggleClass('hide');
			if($tolls.hasClass('hide')){
				$(this).closest('.controlsdisenio').css('height','auto');
				$('#accionesedicion').css('right','1ex');
				$('#mainpage').css('width','100%');
			}else{
				$(this).closest('.controlsdisenio').css('height','100vh');
				$('#accionesedicion').css('right','65px');
				$('#mainpage').css('width','calc(100% - 62px)');
			}
		});
		
		$('#layoutver').on('click','span',function(ev){
			sp=$(this);
			lv=sp.closest('#layoutver');
			cls=sp.attr('data-class');
			lv.removeAttr('class').addClass(cls);
			ev.stopPropagation();
			$('#linkviewen').attr('href',_sysUrlBase_+'/frontend/plantillas/tema1/css/'+(cls.substring(6,100))+'.css');
			$('#pagecontroler').removeClass('viewendisenio viewenautor viewenalumno').addClass(cls);	
			mainpagescoll(cls);
		})

		var changealing=function(il,cl){
			if(il.length<1) return;
			cssaling=il.attr('data-txtaling')||'';
			_cont=il.children('._cont');			
			if(cssaling==cl){
				_cont.removeClass(cssaling);
				cl='';
				il.attr('data-txtaling','');				
			}else{
				_cont.removeClass(cssaling).addClass(cl);
				il.attr('data-txtaling',cl);
			}
			if(_cont.attr('tplplantilla')=='texto'){_cont.children('div').children('div').children('.txtedit').removeClass(cssaling).addClass(cl);}
		}

		
		$('#pagecontroler').on('click','.islayout',function(ev){
			$pv=$('#pagecontroler');
			if($pv.hasClass("viewendisenio")){
				$('.islayout').removeClass('active');
				$layout=$(this);
				$layout.addClass('active');
				ev.stopPropagation();
			}
		}).on('click','.layouttxtleft',function(ev){
			$islayout=$(this).closest('.islayout');
			changealing($islayout,'text-left');			
		}).on('click','.layouttxtcent',function(ev){
			$islayout=$(this).closest('.islayout');
			changealing($islayout,'text-center');			
		}).on('click','.layouttxtrigh',function(ev){
			$islayout=$(this).closest('.islayout');
			changealing($islayout,'text-right');			
		}).on('click','.layouttxtjust',function(ev){
			$islayout=$(this).closest('.islayout');
			changealing($islayout,'text-justify');			
		}).on('click','.layouttxtnegr',function(ev){
			$islayout=$(this).closest('.islayout');
			$islayout.children('._cont').toggleClass('addnegrita');
		}).on('click','.layouttxtcurs',function(ev){
			$islayout=$(this).closest('.islayout');
			$islayout.children('._cont').toggleClass('addcursiva');
		}).on('click','.layouttxtsubr',function(ev){
			$islayout=$(this).closest('.islayout');
			$islayout.children('._cont').toggleClass('addsubrayado');			
		}).on('click','.layouterase',function(ev){
			$islayout=$(this).closest('.islayout');
			$islayout.children('._cont').html('');
		}).on('click','.layoutcopy',function(ev){
			$islayout=$(this).closest('.islayout');
			$copyhtml=$islayout.children('._cont').html();
		}).on('click','.layoutpaste',function(ev){
			$islayout=$(this).closest('.islayout');
			$islayout.children('._cont').html($copyhtml);
			$copyhtml='';
		}).on('click','.layouteli',function(ev){
			$layout=$(this).closest('.islayout');
			$mainpage=$layout.parent('#mainpage');
			if($mainpage.length==1){ // si es pagina principal
				$layout.remove();
				if(__isEmpty($mainpage)) $mainpage.html('');
				return false;
			}
			$mainpage=$layout.parent('.divcol');
			if($mainpage.length==1){
				pad=$mainpage.parent();
				$layout.remove();
				if(__isEmpty($mainpage)) $mainpage.remove();
				if(__isEmpty(pad))pad.remove();
				return false;
			}
			$mainpage=$layout.parent('.row').parent('.isrows');
			if($mainpage.length==1){
				pad=$mainpage.parent();				
				$mainpage.remove();
				if(__isEmpty(pad)) pad.html('');
				return;
			}

			$mainpage=$layout.parent('.row').parent('.iscolums');
			if($mainpage.length==1){
				pad=$layout.parent();				
				$layout.remove();
				if(__isEmpty(pad)) pad.remove();
				if(__isEmpty($mainpage)) $mainpage.remove();
				return false;
			}
		}).on('click','.layoutedit',function(ev){

			$thislayout=$(this);
			$islayout=$(this).closest('.islayout');
			var $md=__sysmodal({titulo:'',html:$('#clone_editlayout').html()});
			$md.find('input.layoutname').val($(this).siblings('.name').text());	
			let ispage=$islayout.hasClass('seepage')?true:false;
			$md.find('label.labelnombre').text('Nombre de'+(ispage?' la Página':'l Layout'));
			$md.find('input.layoutname').on('change',function(){
				$thislayout.siblings('.name').text($(this).val());
			});
			if(ispage){
				$md.find('.nopage').hide();
			}

			var mdancho=$islayout.attr('data-ancho')||'';
			var mdalto=$islayout.attr('data-alto')||'';
			var mdcolorfondo=$islayout.attr('data-colorfondo')||'';
			var mdfuente=$islayout.attr('data-fuente')||'';
			var mdfuentezise=$islayout.attr('data-fuentezise')||'';
			var mdcolortexto=$islayout.attr('data-colortexto')||'';
			var mdstyle=$islayout.attr('data-style')||'';
			var mdfondo=$islayout.attr('data-fondo')||'';
			
			var _loadstyletmp=function(){				
				var style='';
		    	if(mdancho!=''){ txt=mdancho.replace(/[0-9]/gi,'');	style=style+' max-width:'+(txt!=''?mdancho:mdancho+'%')+'; ';	}
		    	if(mdalto!=''){	txt=mdalto.replace(/[0-9]/gi,''); style=style+' min-height:'+(txt!=''?mdalto:mdalto+'%')+'; ';	}
		    	if(mdcolorfondo!=''){ style=style+' background:'+(mdcolorfondo)+'; '; }
		    	if(mdfuente!=''){style=style+' font-family:'+(mdfuente)+'; '; 	}
		    	if(mdfuentezise!=''){txt=mdfuentezise.replace(/[0-9]/gi,''); style=style+' font-size:'+(txt!=''?mdfuentezise:mdfuentezise+'px')+'; '; 	}
		    	if(mdcolortexto!=''){ style=style+' color:'+(mdcolortexto)+'; '; }
		    	mdstyle=style;
		    	$md.find('textarea.layoutstyle').val(mdstyle);
		    	$islayout.attr('style',mdstyle);		    	
		    	$islayout.attr('data-style',mdstyle);
			}
			$md.find('select.layoutancho').val(mdancho);
			$md.find('select.layoutalto').val(mdalto);
			$md.find('select.layouttipofuente').val(mdfuente);
			$md.find('select.layoutsizefuente').val(mdalto);
			$md.find('textarea.layoutstyle').val(mdstyle);
			if(mdfondo!=''){
				$md.find('img.imgfondo').attr('src',mdfondo);
				$md.find('img.imgfondo').attr('data-oldmedia',mdfondo);
			}
			if(mdcolorfondo!=''){
				$md.find('input.layoutcolorfondo').val(mdcolorfondo).show().siblings('.chkoption').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
				_loadstyletmp();
			}
			if(mdcolortexto!=''){
				$md.find('input.layoutcolortexto').val(mdcolortexto).show().siblings('.chkoption').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
				_loadstyletmp();
			}

			$md.find('select.layoutancho').on('change',function(){
				mdancho=$(this).val();	$islayout.attr('data-ancho',mdancho); _loadstyletmp();
			});			
			
			$md.find('select.layoutalto').on('change',function(){
				mdalto=$(this).val(); $islayout.attr('data-alto',mdalto); _loadstyletmp();
			});			
			
			$md.find('input.layoutcolorfondo').on('change',function(){
				mdcolorfondo=$(this).hasClass('sincolor')?'':$(this).val();	$islayout.attr('data-colorfondo',mdcolorfondo); _loadstyletmp();
			});			
			
			$md.find('select.layouttipofuente').on('change',function(){
				mdfuente=$(this).val(); $islayout.attr('data-fuente',mdfuente); _loadstyletmp();
			});			
			
			$md.find('select.layoutsizefuente').on('change',function(){
				mdfuentezise=$(this).val();	$islayout.attr('data-fuentezise',mdfuentezise); _loadstyletmp();
			});			
						
			$md.find('input.layoutcolortexto').on('change',function(){
				mdcolortexto=$(this).hasClass('sincolor')?'':$(this).val();	$islayout.attr('data-colortexto',mdcolortexto);	 _loadstyletmp();			
			});

			$md.find('.chkoption').on('click',function(ev){ 
		    	var input= $(this).siblings('input');   
		      	if($(this).hasClass('fa-circle-o')){
		        	$('span',this).text(' <?php echo JrTexto::_("Si");?>');	
		        	input.addClass('sincolor');
		       		input.attr('data-color',input.val()).val('').hide();
		       		input.trigger('change');
		        	$(this).removeClass('fa-circle-o').addClass('fa-check-circle');
		      	}else{
		        	$('span',this).text(' <?php echo JrTexto::_("No");?>');
		        	input.removeClass('sincolor');
		        	input.val(input.attr('data-color')).show();
		        	input.trigger('change');
		        	$(this).addClass('fa-circle-o').removeClass('fa-check-circle');
		      	}
		    });

		    $md.on('click','.subirimg',function(ev){	    	
		    	ev.preventDefault();
		    	ev.stopPropagation();
		    	var btn=$(this);
		    	var $file1=btn.parent().siblings('input[type="file"]');
		    	var $tmpmedia=btn.parent().siblings('img');	
		    	$file1.off('change');
		    	$file1.on('change',function(){
		    		var reader = new FileReader();
					reader.onload = function(filetmp){
						var filelocal = filetmp.target.result;
						$tmpmedia.attr('src',filelocal);
					}				
					reader.readAsDataURL(this.files[0]);
					var $idprogress=$('#clone_progressup').clone(true);
					var iduploadtmp='idup'+Date.now()+Math.floor((Math.random() * 100) + 1);
					$idprogress.attr('id',iduploadtmp);
					$(this).closest('.frmchangeimage').append($idprogress);
					tmpolmedia=$tmpmedia.attr('data-olmedia')||'';
					var data=__formdata('');
					data.append('media',this.files[0]);
					data.append('dirmedia','curso_'+tmpidcuro+'/');
					data.append('oldmedia',tmpolmedia);
					//data.append('typefile',type);
					__sysajax({ 
						fromdata:data,
	                	url:_sysUrlBase_+'/acad_cursodetalle/uploadmedia',
	                	iduploadtmp: '#'+iduploadtmp,
	                	callback:function(rs){                		
	                		if(rs.code=='ok'){
	                			if(rs.media!=''){
	                				$tmpmedia.attr('src',rs.media);
	                				$tmpmedia.attr('data-oldmedia',rs.media);
	                				$islayout.css({'background-image':'url('+rs.media+')'});                				
	                				$islayout.attr('data-fondo',rs.media);	                				
	                			} 
	                		}
	                	}
	            	});			
		    	});
		    	$file1.trigger('click');		    	
		    });

		    $md.find('.aplicarcss').on('click',function(ev){
		    	mdstyle=$md.find('textarea.layoutstyle').val();
		    	$islayout.attr('style',mdstyle);
		    	$md.modal('hide');
		    });
		}).on('click','.txtedit',function(ev){//solo autor	
			haytxt=$(this).children('textarea').length;
			$pv=$('#pagecontroler');
			if($pv.hasClass("viewenautor")&& haytxt==0){
				$(this).closest('._subirfileedit').addClass('nohover');
				txtaqui=$(this);
				htmledit=txtaqui.html();
				idtmp='idtmp'+Date.now();
				html='<textarea id="'+idtmp+'" style="width:100%; height:100%;" row="10">'+htmledit+'</textarea>';
				txtaqui.html(html);
				__initEditor(idtmp,'savehtml');
				ev.stopPropagation();
			}
		}).on('click','._btneditar_',function(ev){
			let _cont=$(this).closest('._cont');
			let tpl=_cont.attr('tplplantilla');
			if(tpl=='texto'){
				txtaqui=$(this).closest('.toolbaredit').siblings('.txtedit');
				haytxt=txtaqui.children('textarea').length;
				if(haytxt==0){
					idtmp='idtmp'+Date.now();
					htmledit=txtaqui.html();
					html='<textarea id="'+idtmp+'" style="width:100%" row="10">'+htmledit+'</textarea>';
					txtaqui.html(html);
					$(this).closest('._subirfileedit').addClass('nohover');
					__initEditor(idtmp,'savehtml');			
				}
				ev.stopPropagation();
			}/*else{				
				let tmpid=_cont.attr('id')||__idgui();
				_cont.attr('id',tmpid);
				__initEditor(tmpid,'savehtml');
			}*/
		}).on('click','._btnsubirfile_',function(ev){
			var tpl=$(this).closest('._cont').attr('tplplantilla');
			var $file=$(this).siblings('input[type="file"]');
			var type=$file.attr('data-type');
				$tmpmedia=$(this).closest('._subirfileedit').children('div').children(type);
			$file.off('change');
			$file.on('change',function(){				
				if(type=='audio'||type=='video'){
					$tmpmedia.attr('src',window.URL.createObjectURL(this.files[0]));
				}else{
					var reader = new FileReader();
					reader.onload = function(filetmp){
						var filelocal = filetmp.target.result;
						$tmpmedia.attr('src',filelocal);
					}				
					reader.readAsDataURL(this.files[0]);
				}
				var $idprogress=$('#clone_progressup').clone(true);
				var iduploadtmp='idup'+Date.now()+Math.floor((Math.random() * 100) + 1);
				$idprogress.attr('id',iduploadtmp);
				$(this).closest('._cont').append($idprogress);
				tmpolmedia=$tmpmedia.attr('data-olmedia')||'';				
				var data=__formdata('');
				data.append('media',this.files[0]);
				data.append('dirmedia','curso_<?php echo $idcurso; ?>/');
				data.append('oldmedia',tmpolmedia);
				data.append('typefile',type);
				__sysajax({ 
					fromdata:data,
                	url:_sysUrlBase_+'/acad_cursodetalle/uploadmedia',
                	iduploadtmp: '#'+iduploadtmp,
                	callback:function(rs){                		
                		if(rs.code=='ok'){
                			if(rs.media!=''){
                				$tmpmedia.attr('src',rs.media); 
                				$tmpmedia.attr('data-olmedia',rs.media);
                			} 
                		}
                	}
            	});
			});	
			$file.trigger('click');
		}).on('click','._btnsubirvisor_',function(ev){
			var tpl=$(this).closest('._cont').attr('tplplantilla');
			var $file=$(this).siblings('input[type="file"]');
			var type=$file.attr('data-type');
			$tmpmedia=$(this).closest('._subirvisoredit').children('div').children('iframe');
			$file.off('change');
			$file.on('change',function(){
				//$(this).closest('._subirvisoredit').css({'padding':'0px;'});
				//$(this).closest('.islayout').css({'padding':'0px;'});
				var $idprogress=$('#clone_progressup').clone(true);
				var iduploadtmp='idup'+Date.now()+Math.floor((Math.random() * 100) + 1);
				$idprogress.attr('id',iduploadtmp);
				$(this).closest('._cont').append($idprogress);				
				tmpolmedia=$tmpmedia.attr('data-olmedia')||'';
				var data=__formdata('');
				data.append('media',this.files[0]);
				data.append('dirmedia','curso_<?php echo $idcurso; ?>/');
				data.append('oldmedia',tmpolmedia);
				data.append('typefile',type);
				__sysajax({ 
					fromdata:data,
                	url:_sysUrlBase_+'/acad_cursodetalle/uploadmedia',
                	iduploadtmp: '#'+iduploadtmp,
                	callback:function(rs){
                		if(rs.code=='ok'){
                			if(rs.media!=''){
                				if(tpl=='html') $tmpmedia.attr('data-src','');
                				else $tmpmedia.attr('data-src',_sysUrlBase_+'/plantillas/'+tpl);
                				$tmpmedia.attr('src','');
                				console.log($tmpmedia);
                				//$tmpmedia.children('iframe').attr('src',rs.media);
                				$tmpmedia.attr('data-olmedia',rs.media);
                			} 
                		}
                	}
            	});

			});
			$file.trigger('click');
		}).on('click','._btneliminarcontenido_',function(ev){
			$tb=$(this).closest('.toolbaredit');
			cl=$tb.attr('data-clean');
			if(cl=='_tmpmediapage'){
				$tb.siblings('.tmpmediapage').attr('src','').children('iframe').attr('src',''); // falta limpiar de la base de datos
			}else if(cl=='_filemedia'){
				type=$(this).siblings('input[type="file"]').attr('data-type');
				if(type!='img')	$tb.siblings('.filemedia').attr('src','');
				else $tb.siblings('img').attr('src','<?php echo $rustatic; ?>/media/image/icons/imagen2.gif');
			}else if(cl=='_txtedit'){
				$tb.siblings('.txtedit').text('Click Aqui para modificar el texto');
			}

			ev.stopPropagation();
		})
		
		let __hijosmenu=function(men,tipo){
			if(men.length>0){				
				var  indice=men.children('li');
				if(tipo=='json'){
					var menujson={};
					$.each(indice,function(i,v){
						$men=$(v);
						menujson[$men.attr('data-idcursodetalle')]={orden:$men.attr('data-orden'),padre:$men.attr('data-idpadre'),idrecurso:$men.attr('data-idrecurso'),link:$men.attr('data-link'),imagen:$men.attr('data-imagen'),hijos:__hijosmenu($men.children('ul'),'json')};
					})
					return menujson;
				}else if(tipo='lihtml'){
					var li='';					
					$.each(indice,function(i,v){
						$men=$(v);
						link=$men.attr('data-link')||'';
						li+='<li style="display:inline-flex"><a ';
						if(link!=undefined&&link!='') li+=' href="javascript:void(0)" data-link="'+link+'"';						
						li+='> '+$men.children('div').children('.mnombre').text()+'</a>';
						let _hijos=$men.children('ul');
						if($men.length>0) li+='<ul style="display:flex">'+__hijosmenu(_hijos,'lihtml')+'</ul>';
						li+='</li>';						
					})
					return li;
				}
			}return '';
		}
		
		var itmpfile=0;		
		$('.cicon').click(function(ev){
			var div=$(this);
			if(div.hasClass('ciconlayout')) return false;
			else if(div.hasClass('ciconconfig')) return false;			
			else{
				var acc=div.attr('data-acc');
				var $layout=$('.islayout.active');
				if($layout.length==0&&acc!='1rowlay'){ alert('seleccione layout');
					return false;
				}
				var incont=$layout.children('._cont');
				if(acc=='addindice'){
					$thislayout=$(this);
					$islayout=$(this).closest('.islayout');
					let indice=$('#mindice').children('li');
					var menujson={};
					var html='';
					//if(indice.length>1)
						/*$.each(indice,function(i,v){							
							$men=$(v);
							if(!$men.hasClass('paraclonar'))
							menujson[$men.attr('data-idcursodetalle')]={orden:$men.attr('data-orden'),padre:$men.attr('data-idpadre'),idrecurso:$men.attr('data-idrecurso'),link:$men.attr('data-link'),imagen:$men.attr('data-imagen'),hijos:__hijosmenu($men.children('ul'),'json')};
						})*/
						/*html='<ul style="display:flex">';
						$.each(indice,function(i,v){							
							$men=$(v);
							li='';
							if(!$men.hasClass('paraclonar')){
								link=$men.attr('data-link')||'';
								li+='<li style="display:inline-flex"><a ';
								if(link!=undefined&&link!='') li+=' href="javascript:void(0)" data-link="'+link+'"';
								li+='> '+$men.children('div').children('.mnombre').text()+'</a>';
								let _hijos=$men.children('ul');
								if($men.length>0) li+='<ul style="display:flex">'+__hijosmenu(_hijos,'lihtml')+'</ul>';
								li+='</li>'
							}
							html+=li;							
						})
						html+='</ul>';*/

						html+='<div class="slick-items container text-center"><div class="row">';
						$.each(indice,function(i,v){
							//console.log(v);						
							$men=$(v);
							li='';							
							if(!$men.hasClass('paraclonar')){
								link=$men.attr('data-link')||'';
								img=$men.attr('data-imagen')||'/static/media/cursos/nofoto.jpg';
								li+='<div class="slick-item col-md-3 col-sm-4 col-xs-6 text-center hvr-float"><div class="rounded-bottom" style="border: 1px solid #fff; margin:1ex 1.5ex; padding:  0px; -webkit-box-shadow:0px 0px 10px 2px #746c6c ; -moz-box-shadow:0px 0px 10px 2px #746c6c ; box-shadow:0px 0px 10px 2px #746c6c ;"><a ';
								if(link!=undefined&&link!='') li+=' href="javascript:void(0)" data-link="'+link+'"';
								li+='> ';
								li+='<div style="height:150px; overflow:hidden"> <img src="'+img+'" style="min-width:100%; min-height:100%"> </div>';
								li+='<div class="text-center" style="height:50px; overflow:hidden">'+$men.children('div').children('.mnombre').text()+'</div>';
								//let _hijos=$men.children('ul');
								//if($men.length>0) li+='<ul style="display:flex">'+__hijosmenu(_hijos,'lihtml')+'</ul>';
								li+='</a></div></div>'
							}
							html+=li;							
						})
						html+='</div></div>';
						
						/*			
							<div  style="margin: 0.25ex 0.5ex; border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);">
							   <a href="<?php //echo $this->documento->getUrlBase(); ?>/recursos/listar/<?php //echo $nivel["idnivel"] ?>" style="text-decoration: none; color:#000;"> 
								<div></div>
								<div class="namelevel text-center"><?php //echo $nivel["nombre"]; ?></div>
								<div class="txtinfolevel">
									<div><strong><?php //echo JrTexto::_('Total') ?>:</strong><span> <?php //echo $nivel["nunidades"]; ?> </span><?php //echo JrTexto::_('Units') ?></div>
									<div><strong><?php //echo JrTexto::_('Total') ?>:</strong><span> <?php //echo $nivel["nactividades"]; ?> </span> <?php //echo JrTexto::_('Activities') ?></div>
								</div>
								</a>
							</div>*/
                           

					//html='<script type="text/javascript">var menuall='+JSON.stringify(menujson)+'; console.log(menuall); <\/script>';
					incont.html(html);
					//var slikitems=$('.slick-items').slick(optionslike);

				}else if(acc=='grilla'){
					$thislayout=$(this);
					$islayout=$(this).closest('.islayout');
					var $md=__sysmodal({titulo:'',html:$('#clone_grilla').html()});

					let $addlayout=$('#iconclonar<?php echo $idgui; ?>').find('#rowlay').clone();
					$md.find('.aplicarcss').on('click',function(ev){
						let nfilas=$md.find('.nfil').val();
						let ncolum=$md.find('.ncol').val();
						let html='<div class="col-12 isrows">';
						let addcol=ncolum==1?'col-12':(ncolum==2?' col-md-6 cols-sm-12':(ncolum==3?'col-md-4 cols-sm-6':(ncolum==4?'col-md-3 cols-sm-12':('col-'+parseInt(12/ncolum)))));
						for(i=1;i<=nfilas;i++){
							html+='<div class="row divcol">';
							for(j=1;j<=ncolum;j++){
								html+='<div class="'+addcol+' islayout scollcss" style="overflow:auto;">'+$addlayout.find('.islayout').html()+'</div>';
							}
							html+='</div>';							
						}
						html+='</div>';
						incont.append(html);
						$md.modal('hide');				    	
			    	});
				}else if(acc=='1rowlay'||acc=='2collay'||acc=='2rowlay'){
					$addlayout=$('#iconclonar<?php echo $idgui; ?>').find('#rowlay').clone();
					var nlayout=$('#mainpage').attr('data-nlayout')||1;
					nl=parseInt(nlayout);
					if(acc=='1rowlay'){
						$addlayout.children('.islayout').addClass('seepage').attr('data-nlayout',1).attr('id','page'+__idgui());
						npage=parseInt($('#mainpage').attr('data-npage')||0);
						$addlayout.find('span.name').text(' Pagina '+npage);
						npage++;
						$('#mainpage .btnnavegacionPage').before($addlayout.html()).attr('data-npage',npage);
					}
					else if(acc=='2collay'){
						$addlayout.find('.islayout').attr('id','lyt'+__idgui());
						$addlayout.find('.islayout').removeClass('col-12').addClass('col-6').find('.name').text('Columna '+(nl++));
						var html=$addlayout.html();
						$addlayout.find('.islayout').attr('id','lyt'+__idgui());
						$addlayout.find('.name').text('Columna '+(nl++));
						incont.append('<div class="col-12 iscolums"><div class="row">'+html+$addlayout.html()+'</div></div>');
					}else if(acc=='2rowlay'){
						$addlayout.find('.islayout').attr('id','lyt'+__idgui());
						$addlayout.find('.name').text('Fila '+(nl++)); incont.append('<div class="col-12 isrows"><div class="row">'+$addlayout.html()+'</div></div>');
						$addlayout.find('.islayout').attr('id','lyt'+__idgui());
						$addlayout.find('.name').text('Fila '+(nl++)).attr('id','lyt'+__idgui()); incont.append('<div class="col-12 isrows"><div class="row">'+$addlayout.html()+'</div></div>');
					}
					$('#mainpage').attr('data-nlayout',nl);
				}else{
					if(acc=='texto'){						
						txt=$('#iconclonar<?php echo $idgui; ?> #clon_texto').clone(true);
						incont.attr('tplplantilla','texto');
						incont.html(txt.html());
					}else if(acc=='audio'||acc=='video'|| acc=='imagen'){
						var mediacl=$('#iconclonar<?php echo $idgui; ?> #clon_media').clone(true);
						mediacl.find('.filetmp').attr('data-type',acc);
						if(acc=='imagen'){ 
							mediacl.find('.filetmp').attr('data-type','img');
							mediacl.find('audio').remove(); 
							mediacl.find('video').remove(); 
							mediacl.find('.filetmp').attr('accept','image/x-png, image/gif, image/jpeg, image/*');
						}else if(acc=='audio'){	mediacl.find('img').attr('src',_sysUrlStatic_+'/media/image/icons/audio2.gif'); mediacl.find('video').remove(); mediacl.find('.filetmp').attr('accept','audio/mp3');
						}else if(acc=='video'){ mediacl.find('img').attr('src',_sysUrlStatic_+'/media/image/icons/video2.gif'); mediacl.find('audio').remove(); mediacl.find('.filetmp').attr('accept','video/mp4, video/*');}
						mediacl.find('.filetmp').attr('id','id'+acc+Date.now()+(itmpfile++));
						incont.attr('tplplantilla',acc);
						incont.html(mediacl.html());
					}else if(acc=='pdf'||acc=='ppt'||acc=='html'||acc=='swf'){
						let idttmp=acc+Date.now()+(itmpfile++);
						incont.attr('tplplantilla',acc);
						incont.attr('data-cargado','no');
						incont.attr('data-cargaren','iframe'+idttmp);
						var clone=$('#iconclonar<?php echo $idgui; ?> #clon_file').clone(true);
						clone.find('iframe').attr('id','iframe'+idttmp);
						accept='application/pdf , application/zip';
						if(acc!='pdf'){
							clone.find('img').attr('src',_sysUrlStatic_+'/media/image/icons/'+acc+'2.gif');
							accept='.'+acc+' , application/zip';
							if(accept=='html') accept+=', .htm';
							else if(accept=='xls')accept+=', .xls, .xlsx';
							else if(accept=='ppt')accept+=', .ppt, .pptx';
							else if(accept=='swf')accept+=', .swf, .fla'
						}
						clone.find('.filetmp').attr('id','id'+idttmp).attr('accept',accept).attr('data-type',acc);
						incont.html(clone.html());
						$layout.css('padding','0px');
					}else if(acc=='widgets'){
						incont.attr('tplplantilla',acc);
						incont.html('<div class="widgetjson"></div><div class="widgetcont"></div>');
						$widcont=incont.children('.widgetcont');
						var dt={data:{idcurso:<?php echo $idcurso; ?>,idcursodetalle:<?php echo $idcursodet; ?>},url:'/widgets/'};
						__cargarWidget(dt,$widcont);
					}
				}				
			}
		});

		$('body').on('click','.chkcambiarcolor',function(ev){
	    	var input= $(this).siblings('input');   
	      	if($(this).hasClass('fa-circle-o')){
	        	$('span',this).text(' <?php echo JrTexto::_("Si");?>');	
	        	input.addClass('sincolor');
	       		input.attr('data-color',input.val()).val('').hide();
	       		input.trigger('change');
	        	$(this).removeClass('fa-circle-o').addClass('fa-check-circle');
	      	}else{
	        	$('span',this).text(' <?php echo JrTexto::_("No");?>');
	        	input.removeClass('sincolor');
	        	input.val(input.attr('data-color')).show();
	        	input.trigger('change');
	        	$(this).addClass('fa-circle-o').removeClass('fa-check-circle');
	      	}
	      	ev.stopPropagation();
	    })

		$('.verpopover').popover({ trigger: 'hover',delay:100});
	})
</script>