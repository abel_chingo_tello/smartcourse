<?php
$usuarioAct = NegSesion::getUsuario();
$rol = $usuarioAct['idrol'];

?>
<style type="text/css">
    .x_panel {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
    padding: 10px 17px;
    display: inline-block;
    background: #fff;
    border: 1px solid #E6E9ED;
    -moz-column-break-inside: avoid;
    column-break-inside: avoid;
    opacity: 1;
    transition: all .2s ease;
}
</style>
<div class="clearfix"><br></div>
<div class="container">
    <!--div class="paris-navbar-container row">
        <div class="col-md-12 col-sm-12">    
            <div class="x_panel paris-navbar-panel">      
                <div class="x_content paris-navbar">        
                    <div class="col-md-8 paris-left">           
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" onclick="history.back()" >
                            <i class="fa fa-reply"></i> Volver</button>          
                            <label class="paris-divisor"><i class="fa fa-chevron-right"></i></label>
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" >Foro</button>
                    </div>            
                </div>
            </div>
        </div>
    </div-->
    <div class="row">
        <div class="col-md-12">
            <div class="alert"></div>
            <div class="alert alert-info" role="alert">
              <h4 class="alert-heading"><?php echo JrTexto::_('Important information!');?></h4>
              <p>
                <?php 
                  $textomensaje=JrTexto::_('Your subscription plan allows you to download _ncantidaddescarga_ digital books (Workbooks). he has currently made _ncantidaddescargados_ of _ncantidaddescarga_ possible downloads')."<br>"; 
                  $textomensaje.=JrTexto::_('Please note this information before starting the download process').".";
                  $textomensaje=str_replace('_ncantidaddescargados_', '<strong>'.$this->recurso["candidad_descargados"].'</strong>', $textomensaje);
                  $textomensaje=str_replace('_ncantidaddescarga_', '<strong>'.$this->recurso["cantidad_descarga"].'</strong>', $textomensaje);
                echo $textomensaje;?>.</p>
              <hr>
              <p class="mb-0"><?php echo JrTexto::_('Thanks for your understanding');?>.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="text-center col-md-12"><h2><?php echo ucfirst(@$this->recurso["titulo"]); ?></h2></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row mx-auto">
                <div class="col-lg-12 col-md-12 mx-auto div-foro">
                    <div class="post-preview"> 
                        <p><?php echo @$this->recurso["descripcion"]; ?></p>
                        <!--p class="post-meta">Publicado por   <i><b><?php //echo $this->empresa["nombre"]." : <small>".$this->recurso["struser"]."</small>"; ?></b></i> </p-->
                    </div>
                    <hr>
                </div>
            </div>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row mx-auto">
                <div class="col-lg-12 col-md-12 mx-auto">
                    <div class="post-preview"> 
                        <p><?php echo JrTexto::_('Download files') ?></p>
                        <?php 
                            if(!empty($this->recurso["archivos"]))
                                foreach ($this->recurso["archivos"] as $key => $value) {
                                    ?>
                                    <div class="file-item-subido col-md-2" style="text-align: center; border: 1px solid #ccc;  border-radius: 1ex; position:relative; margin: 1ex auto;">
                                         <a href="#" class="btndescargar" target="_blank" style="font-size: 4em; cursor:pointer;" file="<?php echo URL_BASE.$value; ?>"><i class="fa fa-file "></i></a>
                                         <small class="" style="display: block; cursor:pointer" ><?php echo JrTexto::_('click to download'); ?></small>
                                        </div>
                        <?php } ?>                        
                    </div>
                    <hr>
                </div>
                <div class="col-md-6 text-center  mx-auto" id="infodescarga"  style="margin: auto; display: none;">
                    <div class="progress" style="background: #e2a93f99; border-radius: 1ex; height: 30px;">
                        <div id="descarga_progress" class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 0%"  aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"><small id="progress-text">0%</small></div>
                    </div>
                   
                    <div class="row">
                        <div class="col-md-6 col-ms-12">
                            <h5><?php echo JrTexto::_('Download speed'); ?></h5>
                            <p id="descarga_velocidad">150 kB/s</p>
                        </div>
                        <div class="col-md-6 col-ms-12">
                            <h5><?php echo JrTexto::_('Time remaining'); ?></h5>
                            <p id="descarga_tiemporestante">1H 6m 5s</p>
                        </div>
                    </div>
                    <div><a id="save-file" style="display: none;"></a></div>
                </div>
            </div>
        </div>        
    </div>    
</div>
<script type="text/javascript">
    var save_file= document.getElementById('save-file');
    var progress = document.getElementById("descarga_progress");
    var progressText = document.getElementById("progress-text");
    var descarga_velocidad = document.getElementById("descarga_velocidad");
    var descarga_tiemporestante = document.getElementById("descarga_tiemporestante");
    var infodescarga = document.getElementById("infodescarga");
    var tipomatricula=<?php echo !empty($this->recurso["tipomatricula"])?$this->recurso["tipomatricula"]:2; ?>;

    var iniciardescarga=function(){// verificar si puede descargar        
        return new Promise((resolve, reject)=>{
                if(tipomatricula!=3) return resolve(true);
                Swal.showLoading();
                $.post(_sysUrlBase_ + 'json/descargascontroladas/verificar',{
                    'idgrupoauladetalle': '<?php echo $this->recurso["idgrupoauladetalle"]?>',
                    'idcurso': '<?php echo $this->recurso["idcurso"]?>',
                    'idcc': '<?php echo $this->recurso["idcc"]?>',
                    'strcurso': '<?php echo $this->recurso["strcurso"]?>',
                    'idpestania': '<?php echo $this->recurso["idpestania"]?>',
                    'idcursodetalle': '<?php echo $this->recurso["idcursodetalle"]?>',
                    'idempresa': '<?php echo $this->recurso["idempresa"]?>',
                    'idproyecto': '<?php echo $this->recurso["idproyecto"]?>',
                }, function(data){
                    Swal.close();
                    console.log(data);
                    //validar descargas;
                    if(data.code == 200){
                        let tienepermiso=data.tienepermiso;
                        if(tienepermiso==false || tienepermiso=='' || tienepermiso=='false'){
                            Swal.fire({
                              icon: 'error',
                              title: 'Oops...',
                              text: data.msj,
                              //footer: '<a href>Why do I have this issue?</a>'
                            })
                            return resolve(false);
                        }
                        return  resolve(data.tienepermiso);
                    }else{
                        Swal.fire({
                              icon: 'error',
                              title: 'Oops...',
                              text: data.msj,
                              //footer: '<a href>Why do I have this issue?</a>'
                            })
                       return  resolve(false);
                    }

                }, 'json');
            });
    }


    var terminardescarga=function(){
        return new Promise((resolve, reject)=>{
        save_file.click();
        save_file.setAttribute('style','display:none');
        infodescarga.setAttribute('style','display:none');

        //solo si quiere volver a descargarlo
        infodescarga.setAttribute('descargando','false');
        if(tipomatricula!=3) return resolve(true);
            //Swal.showLoading();
            $.post(_sysUrlBase_ + 'json/descargas_consumidas/guardar',{
                'idgrupoauladetalle': '<?php echo $this->recurso["idgrupoauladetalle"]?>',
                'idcurso': '<?php echo $this->recurso["idcurso"]?>',
                'idcc': '<?php echo $this->recurso["idcc"]?>',
                'strcurso': '<?php echo $this->recurso["strcurso"]?>',
                'idpestania': '<?php echo $this->recurso["idpestania"]?>',
                'idcursodetalle': '<?php echo $this->recurso["idcursodetalle"]?>',
                'idempresa': '<?php echo $this->recurso["idempresa"]?>',
                'idproyecto': '<?php echo $this->recurso["idproyecto"]?>',
                'archivo': '<?php echo $this->recurso["idproyecto"]?>',
            }, function(data){
                //Swal.close();
                //console.log(data);
                //validar descargas;
                if(data.code == 200){
                    /*let tienepermiso=data.tienepermiso;
                    if(tienepermiso==false || tienepermiso=='' || tienepermiso=='false'){
                        Swal.fire({
                          icon: 'error',
                          title: 'Oops...',
                          text: data.msj,
                          //footer: '<a href>Why do I have this issue?</a>'
                        })
                        return resolve(false);
                    }
                    return  resolve(data.tienepermiso);*/
                }else{
                   return reject(false); 
                }

            }, 'json');
        });
    }

    document.querySelector('.btndescargar').addEventListener('click', function(ev){
        ev.preventDefault();
        if((infodescarga.getAttribute('descargando')||'false')=='true') return false;
        infodescarga.setAttribute('descargando','true');        
        iniciardescarga().then((rs)=>{
            if(rs==true){
                progress.setAttribute('aria-valuenow',0);
                progress.setAttribute('style','width:'+0+'%');
                infodescarga.setAttribute('style','display:inline-block');       
                progressText.innerHTML =  "0%";
                var fileName=this.getAttribute('file').split('?')[0];       
                request = new XMLHttpRequest();
                request.responseType = 'blob';
                request.open('get', fileName, true);
                request.send();

                var startTime = new Date().getTime();
                request.onreadystatechange = function() {
                    if(this.readyState == 4 && this.status == 200) {
                        var obj = window.URL.createObjectURL(this.response);
                        let nfile=fileName.lastIndexOf('/');
                        fileName=fileName.substring(nfile+1);
                        save_file.setAttribute('href', obj);
                        save_file.setAttribute('download', fileName);
                        setTimeout(function() {
                            window.URL.revokeObjectURL(obj);
                        }, 60 * 1000);
                    }
                };      
     
                request.onprogress = function(e) {
                    progress.max = e.total;
                    progress.value = e.loaded;
             
                    var percent_complete = (e.loaded / e.total) * 100;
                    percent_complete = Math.floor(percent_complete);     
                    progressText.innerHTML = percent_complete + "%";
                    progress.setAttribute('aria-valuenow',percent_complete);
                    progress.setAttribute('style','width:'+percent_complete+'%');
                    if(percent_complete==100){
                        save_file.setAttribute('style','display:block');
                        setTimeout(function(){ terminardescarga(); },1000);
                    }

                    var duration = (new Date().getTime()-startTime)/1000;
                    var bps = e.loaded / duration;
                    var kbps = bps / 1024;
                    kbps = Math.floor(kbps); 
                    descarga_velocidad.innerHTML = kbps + " KB / s";

                    var time=(e.total - e.loaded) /bps;
                    var segundos=Math.floor(time % 60);
                    var minutes=Math.floor(time/60);
                    var horas=Math.floor(time/3600);
                    descarga_tiemporestante.innerHTML=horas+':'+minutes+':'+segundos;
                };
            }else{
                infodescarga.setAttribute('descargando','false');
            }
        }).catch((ex)=>{            
            console.log(ex);
        });
    });
   
</script>

