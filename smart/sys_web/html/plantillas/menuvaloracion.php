<style type="text/css">
    .lead{
        font-size: 2em;
    }
    .lead i{
        cursor:pointer;
        padding: 1ex;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form id="FrmValoracion" class="jumbotron">
                <h1 class="display-4 text-center"><?php echo JrTexto::_('Hello, rate this course');?>!</h1>
                <br>
                <p class="lead text-center"><i class="fa fa-star" data-value="1"></i><i class="fa fa-star"  data-value="2"></i><i class="fa fa-star"  data-value="3"></i><i class="fa fa-star"  data-value="4"></i><i class="fa fa-star"  data-value="5"></i></p>
                <hr class="my-4">
                <p><?php echo JrTexto::_('Leave us your comment');?></p>
                <div class="form-group">
                <label for="exampleFormControlTextarea1"><?php echo JrTexto::_('Your comment');?></label>
                <textarea id="txtcomentario_" class="form-control" id="exampleFormControlTextarea1" rows="4" style="resize: none;" required=""<?php echo $this->idrol == "3" ? '' : ' disabled=""'; ?>></textarea>
                </div>
                <?php if($this->idrol == "3"): ?>
                <button id="enviarValoracion" type="submit" class="btn btn-primary btn-lg pull-right"><i class="fa fa-send"></i> <?php echo JrTexto::_('Send');?> </button>
                <?php endif; ?>
            </form>
        </div>        
    </div>
</div>
<?php if($this->idrol == "3"): ?>
<script type="text/javascript">
    var puntuacion = 0, activado = true;
    $(document).ready(function(i,v){
        $.post("<?php echo URL_BASE; ?>json/valoracion", 
        {
            'idalumno': <?php echo $this->idalumno; ?>,
            'idcurso': <?php echo $this->idcurso; ?>,
            'idgrupoaula': <?php echo $this->idgrupoaula; ?>,
            'idcomplementario': <?php echo $this->idcomplementario; ?>,
            'idgrupoauladetalle': <?php echo !empty($this->idgrupoauladetalle)?$this->idgrupoauladetalle:-1; ?>
        }
        ,function(data){
            if(data.code === 200){
                if(data.data.length > 0){
                    activado = false;
                    var v = data["data"][0]["puntuacion"];
                    for (var i = 1; i <= v; i++) {
                        $('i.fa-star[data-value="'+i+'"]').css({color:'#F00'});
                    }
                    $("#txtcomentario_").val(data["data"][0]["comentario"]).attr("readonly", "");
                    $("#enviarValoracion").attr("disabled", "");
                }
            }
        }, 'json');
    });
    $('i.fa-star').on('click',function(i,v){
        if(activado){
            var v=$(this).attr('data-value');
            puntuacion = v;
            for (var i = v; i < 6; i++) {
                $('i.fa-star[data-value="'+i+'"]').css({color:'#000'});
            }
            for (var i = 1; i <= v; i++) {
                $('i.fa-star[data-value="'+i+'"]').css({color:'#F00'});
            }
        }
    });
    $("#FrmValoracion").submit(function(evt){ 
        evt.preventDefault();
        if(puntuacion<1){
            Swal.fire('Warning', '<?php echo JrTexto::_('You have to rate the course, rating from 1 to 5 stars') ?>', 'warning');
            return false;
        }

        $.post("<?php echo URL_BASE; ?>json/valoracion/guardar", 
        {
            'idalumno': <?php echo $this->idalumno; ?>,
            'idcurso': <?php echo $this->idcurso; ?>,
            'idgrupoaula': <?php echo $this->idgrupoaula; ?>,
            'idcomplementario': <?php echo $this->idcomplementario; ?>,
            'idgrupoauladetalle': <?php echo !empty($this->idgrupoauladetalle)?$this->idgrupoauladetalle:-1; ?>,
            'puntuacion': puntuacion,
            'comentario': $("#txtcomentario_").val()
        }
        ,function(data){
            if(data.code === 200){
                activado = false;
                $("#txtcomentario_").attr("readonly", "");
                $("#enviarValoracion").attr("disabled", "");
                swal("<?php echo JrTexto::_('Success');?>", "<?php echo JrTexto::_('Thank you for your feedback');?>", "success");
            }
        }, 'json');
    });
</script>
<?php endif; ?>