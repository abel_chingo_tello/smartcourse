<style type="text/css">
	.title01 { background: white; padding: 0.25rem 1.25em; border-radius: 0.25em; border-bottom: 1px solid #c5c6c7; }
	.icon-circle { background: #eaecef; padding: 6px 8px; border-radius: 50%; }
	.tdAction { padding-top: 0.5em!important; }
	#allrooms td { text-align: center; }
	.room-card { width: 100%; max-width: 14.5rem; }
	.image-preview-content { /*max-width: 320px;*/ width: 100%; border:1px solid #d0d0d0; text-align: center; }
	.image-preview-content img { width:100%; height:auto; max-height: 100%; }
	@media (max-width: 992px){
		.container-especial{ width: 100%; } 
	}
</style>
<!-- https://cdn.clipart.email/67b7ec26148891e99db61be71577983e_zoom-zoom-call-hd-png-download-transparent-png-image-pngitem_860-779.png -->
<div class="container container-especial">
	<h3 class="my-2 title01"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAMAAABOo35HAAAAA3NCSVQICAjb4U/gAAAAjVBMVEVHcEwYatwZbeQZZNYYcOc1heksdOAXcecUZ/0jeelBjfIlatoYYdMWYNMYYdMXYNMMU75Gju1Kke1JkO01g+sac+j////y9PUZcugYWrz9/v4Na+fw8vQbefUaaeFLlvYKUbgugOzc5fH3+f5PgcwYZNaZwPO/wsaFsevK0ttnoe12nda6vsMaYMW4vMG6LJILAAAAFXRSTlMA/P6jsUYv/gP+/RXQW+1+8olov9nI4pUHAAAQnklEQVR4nO2dCXPiOBCFwQRkbAiVwyAgIYHEmWQy7P//edvdksFcviRZvt7UVu3OJsb+eP3U8iH3ep06derUqVOnTp06depUVK5N2T74TKrQrlZkN67qFJLrj0hDOxr5tC9VxHXE5I+Gj48PT0/Pz5735r3Zk/f89DDqVQxXxMn1hwjJE4A8b+ttQWNbgs+G3Xh69KuDS4Lyhw9Pz55gBHyCgKEc5jjMlvCTg7H39vxYCVoClDsCPxEnpBTtKg/wj11xDjsy3r49jWzTEqR8AQo4SUzcNqFTccbZ2PMe7ZMaPcZB8YhTpXDBXnG2fXuwh4pIoaUOoMSOWQZzXbBzbAy0bFQimYo85W2DChbepWAHWfD2WD4t/ECfSKGlROVVHRaYC2kNS6ZFpoo8xStvqaOA1tjzyyQFqNzHpxip+sBCWtvyYgtR+Q/PUH51iKkL4ZjoldRuIarRg/eGpgrqhyoga70+lQGLUMGcbyvKr46ssINg2xKsRaio/uppKiHoH16Np9YJqvqywpnP67NxVP6D1wBUWId8bLJ7gLByH5uBKiBrGWxMYcPD5zdCxWwfqQ4ZDC0RVltCVXtXkdjrwBAs2CpUYNCIApQyBUt0Vs2pQBJ7vTMBC4L9QVag7SPUKCOw0FbPVIENCSspE7Bgew20FUo/LBfTKmggKv2waBBEW7HmsdINi5J93EhbBbphUQk2Ma2ENDtLlmCjBsGjNMKiUbCZyS6lDxZshkbBJia7lDZYbs9/ohK0fUQGpQsWRPvzlgWNZqULltsbeo3tGA7SA8uFYbDZcUXSAiti1dCO4SA9znr0GtxdHaUF1kM7WGmBJVjZPpISpAzLRVa8FayUYQGrbVtYqcLCcbA1rBRhEauGt+0xKcFqGSslWDjHCdoxDgopwIK5c7tYKcBye+7za6tYKTnr6bUVfftRCrBax6owLGpGW8aqKCxoGrasrjceF1YxWDAQbnmLGiypos5q20BIKgTLFeFue99LVxFYFFhtC3dUAVgysNrHqlgZtrMIi8CCDqt13ahUblhubwgdVitZFYDlPgetDKwgPyxZhLZ3245ywmpzERZwVnuLMC+sVhdhTljYjra3CHOXIbajrWWVCxbMCdtchDlhtTrdg1ywRLq3mFUuWP6Yt3NOGCk7LLc3aLmxssOSbYPt/bWqHLDuXlud7kF2WGCscduNlQNWZ6yssDpjoTLD6oyVFZZGY9UZeFZYAz3GEpswsGx0KcvCZizD0VjHE194LGy9ZsFWu3C7a9NXMzPB0jMrxDtJ1sz7/trtPrRrt/v69gL4HkziygbLvVe+ZYZQed+7+Xy+muvXCje6+94axZUFljiPpbYLPOBr7wuPaPWxMqIP5PXxtV2bo5Uts55f1YzFebAOvldoAJQBZx22/PHNjE33M8Bye8Ox2qK18Ltrb0e1YoZUxIs2vwNzmVngLBMs7BsUhKz+Gkd1wLX6eFub8VaWMvTHqt/U+htj3TSqCNd8/tcMrXRYyvHOkVUJtorTMuOtLM5Si3eONSgOoRTRR314JmilwoJpYaAS7zxg2495eazmVPDzXWBgTMwAC7t3FbFduayEt75SrZV/Opkhs5SqUARWSXl1pAUfl1aI2Cbn7PfTYEGTpVSFARu/lGysuSjEn3UiKmyTvTcvVwebDguqUCWxhLHKZSW85SXQwtbvbbdYLH62OWillyFUoVLfsCu5BgUskVpJrP4uFi8vL6tdDlopsFTHQvj+5oYmgym0oH3Y3oraIyuglcA0NyzFsXD9VX4NClirm318nNXLYqevDN07xRMOVqqQaN2qwxNWAGuc+fjSYPmB0ryQeWW3DTFY1z1zykojLJoXqjTCYqZjCdbV0DrzlVZYA7UqpMbBjq43D3FWC81l6Ko1DpTvlmiJhE9gpdlZ2DioncqyNRjegHXqK73OUo4sy7C+z2AJVi8xaXWW4gllm7DmF7DOfaU5s1Qjq1LOOs8r3aOhr3oNrkLOitfgQr+z3N5QMbIq5KwYq4UJZ+HEUPGqfWWcddGL6nfWnWJkVcZZMVaLo7MWWkdD1XyvirPOewYDzlJuSavirJO8MuMsDfleDWdd7UU1Owv7d9Vrb1Vw1oWvzDhLtX+vhLMufWVmNFQeDO07i9PtA6e+MjIa4mCoKPvO4pe+MpJZypOdSjjr0lcGnCWugtUXlnTW+vfnwlj6naWjc7DurPH69w/BWph21qPi7TN2YZGzgNW/cpz1oDwYWnfW7x8Jy7izBrWGhc76/POnLGfdKXcOlmH9/Aew/ivDWXj/TM0za/dfWc6im40aAKscZ+HTTQ2AVY6zfHVWVYBVjrNGGlaargCsMpwFsx0Nyz9VAFYZzsKpofqTvhWAVZKzVK+DVQNWOc4aqjfwVYBVjrOGnbNaB6tzVg5YnbNywOqclQNW56wcsPQ5izUfVkkdfDNgldTBN2Mirc9ZSWU40vBiWsunlf/pdBZLhFX7sw6/n3/+6bu6kwCLTv7VHdb7759/2pzFE8+U3tca1gphLX+1joaDhHPwdb+6A7De5eV7HZmVBKs3qTUs4Syg9XMKqHhmBa+z27Duan1jiHAW0lppc9Y06fJ9A5wFtL5X594q6qxEWMoJb/leB4J1pKXqLD66DWtW6xtD8C6acLm86q0izuIscEa3EsvtTWvvrHV/edVbBZ1179+GpWG+Y/v+rOCcVnFnQQM/SXjacOQo31Rq21nBBa3CzmLYwN+Wq97CW3cWv+qtQs5K7EmpK1WkZd1Z/JJWcWcl9KQ6egfrzrpGq9CTrDypzZK9Q31hzaPHUa54q4CzOOPsZpsV9Q51f97wOq38zoLB8HbnQL2D8nBoH9ZVWgWcBVU4uc2qJ85o1faB8tjzhre9lcNZKYOhhuGwCrASvJXdWTx5MNQyHNpd5ej4jPQtb+VxFk8YDLUkvFiC2hasv7Gn7696K5ez+jen0ceEV2FFK7PZYXW6JNQtb2V2FuZ72lLBqglPK29agjWPr8x2zVsv2Z3F0/OdHt9RqkNasdvW2psv/diqFDe9lRlWYr5HPbzS+rehxaU3f8L4ycsb3soMy0no3wWsoepbWPeWhkNc1PV3f3Km98bMJ9OirsAzqX8XUmxLebD/tLVc8Gr+GZ5yuEprlb5wvKjCDK9kUA2tcGlnIWqswvczWNdoLRafWV7gAIZJiSz10ILMev+1ssQ5XTXsn2M4ofW7AKS7z/cMCzIwxjcpkUWd1kbtZR/95f6l/EKkt1gslxewTlP+8/f7d/++5Kmw8LGctC6LcE1UrIU7ZsFaK2Gs5eYSw4m3UMsssOAHUrosYS18qlwBFoOdKvuFH+KtDIDh2rL4cW8JZXJW8sQwgjVVaR4A8x7sPi+XFn7YAuprf/VrvqCVCosDgPTGAeWrvNoQdjZcUiGWSIs+6hOMdT4Y3qCVAVYQZH8ba2FryXx4/yqPlnjPFt0S4twIkFNa1/0XF8tWhVEdFmRFu7E/0irtxWrECilcx3BCy8lQhTxMPD1zVocKEU83ZwAtPAjTtOQ3Im41umwczmjto59Kc1bAk0+/n9Zh4fEQfpHLFlCcfjD+MsjV6uXzfZlorIgW74d9lsoK4z1bFR7Gw4K0eGQtGBN/TL5lNCI1n3/txV1ZCcaS32K0h6mweJhpLCRcKnXIqXsgWstfetWvQcHmf4StUowlaaHSqwbMl+1l273oprbCsODPRkYp4PpZSRsY4DSfv3x9vktWy/T6yngAMBY6GatQzA85K26t+DD9DvOxr92LgUL8ePn5+twfUNFMR8v7DbF7z9aRSk2U6vAQW3I+ttx/6td+/34kJfpRPazwT4Z54dFaU54SACkfF6cVTWA1a3mifnpuZ917xlmYenYmLtVW65yWafWPg50qqxxNVmStgdLy3fHTIiVo74g2SgssFrDs8S5gjUKFiJe0WEnmCjP0Ttl3HGDd50BFtCZqX5Wois3ePKo9C7L0Tln3G2bFTurJ93NYUydQsVZ0AE5olNc+ZIFWVmAsJ+scOqaJ6pOaXMQe34Th3gCx/T7sO4FWVGJa6OToGw7WUuke5EcftgABqFlyu1pRobG4k69vELRclcb08OnRfEy/eMaJXq5tFjKWJmuJPaDjopeta/xHO6iguLHIWtzE6+QrLDJW5vMNZqxVF0VDYQFYUWq1hxb1WAUSS1rLIWe2RGisTUFjoSZtKkScFW4KGoustWGtsZaqsXq9u9ZYi2bQxY0lTj6gtdpAi0MVbnKdTb6kNWiJtaht2OQ6j3Up/74VtDil+0SNFbYPTH2KWHkhrL5Sugu1oX0QRaiQ7pG12pDx2Lv3ldI9ojXjGs7VVFpUhKrpLnE1vRA1FaGw1ih0mlyI6KsghCLUAAsL0QkafGZLYxH26FyN09xC1FiEBAsLsam0aE4YahgJj7RmG9bM84Acr46G2opQaNDM2OIisHJegk6WC3NEJzB0TcumRGApzgkvaY3CTfNiSwaW+pzwnNZs4zQttuhSXxjqDSyhgdOwkMdw1x1YkbDbalLIc06sBvpJiZDf0K28to9Sj+RAOHFNGOsY8o2gxcX1Z+3hHsHCS2P9htCSrMKhGVa9aEhsBC3RNGju3M81EA1E3WlJVkYGwjgt0UDUm5Zkpe1Uw3XBxieb2tOiJ+YNNQ2ntNza0yqLFbVbNaclWU16hgMrolXn3IpYmWlGr9Gqrbd4uaxqTYt60TJZxWjVbZ5IrFh/c1dGXp3Qcmo3q+aH/qpEVrKDqBstOn/llM6KPm0As+o6BVfEalY2q/rRksMgnkS2IKA1C2sT8zFWZdsq0pRo1SC4ON3lDi2DoXN9GeT2RhjzlS9FHsXVRM+tMkVpQQtR+eCKSrD8YfCclgwupv3hP22iEuQi2m2yok+f3le5FI8laC+u4rhkKVYy58X9sFiC5c0GkxSVIq+euYStYBS0X4KRRClKc1UJF6fHJfsVKcGj/MGmauaKbBWGFiY4SaqeufjRVkO8PlwpuWQupyq4BCoH0mpWjWQ/lTAX1aKu5ZmKo4oqkNKqeqzoXgh/EFagFgUq7K3uK5ZWccF+wWTRci3yYwUO/Oqy6h2CnlnDJVHJCqwyqh7tnTuzhiuOqjJtaJLcnoiu8nHFUN3P3Dqw6onoAlwbwqV/LaIEUgdU1Q6rC5WLS6AK6onKJXfd08honhduHj8DRsDNpG6oUIjLh6iX4WVqfTaxQFtUf+FkWpesOhPicqeT8GAvA7gEKmmqcDDs1RMVCXd8iNWI6aXbXzwiRaaC+hv1aowKhTvvz9Be+D4Ero1XRArLj0yF9VdvVCiax44GMA3aOAd/KQW+WKKSXizIqPwaYKqj6EufQjk6VI8Hg+UnJn9NLAbAAFREqimoUGQvfzqAenSEweiA83iMH0RXIZwDqZqOf4kiXu5oNgGDSWBxj928Y4IfMcnZUwRqMB0dNtw8icPypwAMHXYgFnPNTQmWDnHahPd3s5F73GRDJQ/OH84G1IFR7LN4nUXPBh0Q0v9kwk7ICR3lx7bVbLnSE6MpEAOPCWZQnI6DS0wHkg4LHAZ/1e8LSlh4khOmVCtISR0O1kdkwOw+DCNiJPpXwQjMNBnMpkMqPPi9VoGK5MaO2vdHw+l0NgNwMcF/T6ejkX8A1EpOMblxZrLCTv/u8qdaL/e2bO9ap06dOnXq1KlTp06d6qj/Ac6W680Ev27NAAAAAElFTkSuQmCC" class="img-fluid" style="height:30px;"> Videoconferencias con ZOOM </h3>
	<?php if($this->idrol == 3): ?>
		<!--VISTA DE ALUMNOS-->
		<h1>Vista de alumno</h1>
		<div class="row justify-content-center">
			<div class="col-sm-9" id="listAlumno">
				<div class="card">
					<div class="card-header">
						<h5 class="mb-0"><i class="fa fa-users icon-circle" aria-hidden="true"></i>&nbsp;Mi sala</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-5">
								<div style=" width: 82%; height: 184px; background: #d0d0d0; margin: auto;"></div>
							</div>
							<div class=" col-sm-7">
								<div class="form-group">
									<label>Nombre de la sala</label>
									<h4>PEPEPEP</h4>
								</div>
								<div class="form-group">
									<label>Descripción</label>
									<textarea class="form-control" readonly>Some quick example text to build on the card title and make up the bulk of the card's content.</textarea>
								</div>
								<div class="form-group">
									<label>Fecha de inicio</label>
									<ol>
										<li>2020-03-01 12:30:01</li>
										<li>2020-03-02 12:30:01</li>
									</ol>
								</div>
								<div class="form-group">
									<label>Enlace de la reunión</label>
									<div class="input-group mb-3">
									  <input type="text" class="form-control" placeholder="link" aria-label="Recipient's username" aria-describedby="basic-addon2" value="https://zoomcom.com/" readonly>
									  <div class="input-group-append">
									    <button class="btn btn-outline-secondary" type="button">Copiar enlace</button>
									  </div>
									</div>
								</div>
								<div class="form-group">
									<div class="text-center">
										<a class="btn btn-primary disabled" href="javascript:void(0)">Ir a la sala</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php else: ?>
		<!--VISTA DE DOCENTE / ADMINISTRADOR-->
		<div class="row justify-content-center">
			<!-- START CREATE VIDEO CONFERENCE -->
			<div class="col-sm-9" id="listDocente" style="display: none;">
				<div class="card">
					<div class="card-header">
						<h5 class="mb-0"><i class="fa fa-users icon-circle" aria-hidden="true"></i>&nbsp;Mi sala <span class="float-right"><button class="btn btn-warning on-editroom"><i class="fa fa-pencil-square" aria-hidden="true"></i> Editar sala</button></span></h5>
					</div>
					<div class="card-body">
						<div class="d-none">
							<label>Salas para hoy</label>
							<div class="row">
								<div class="col-sm-4">
									<div class="card text-white bg-info mb-3" style="max-width: 18rem;">
									  	<div class="card-body">
										    <h5 class="card-title">Info card title</h5>
										    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
										    <a href="javascript:void(0)" class="btn btn-light">Ir a la reunión</a>
									    </div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="card text-white bg-info mb-3" style="max-width: 18rem;">
									  	<div class="card-body">
										    <h5 class="card-title">Info card title</h5>
										    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
										    <a href="javascript:void(0)" class="btn btn-light">Ir a la reunión</a>
									    </div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
		    					<div class="image-preview-content">
  									<img id="previewIMG" src="<?php echo $this->documento->getUrlStatic(); ?>/media/nofoto.jpg" style="max-width:200px;" />
  								</div>
							</div>
							<div class=" col-sm-7">
								<div class="form-group">
									<label>Nombre de la sala</label>
									<h4 class="tituloroom">Unknown</h4>
								</div>
								<div class="form-group">
									<label>Descripción</label>
									<pre class="descroom"></pre>
								</div>
								<div class="form-group">
									<label>Fecha de inicio</label>
									<ol class="fecharoom">
										<li>Unknown</li>
									</ol>
								</div>
								<div class="form-group">
									<label>Enlace de la reunión</label>
									<div class="input-group mb-3">
									  <input type="text" class="form-control linkroom" placeholder="link" aria-label="Recipient's username" aria-describedby="basic-addon2" value="https://zoomcom.com/" readonly>
									  <!-- <div class="input-group-append">
									    <button class="btn btn-outline-secondary" type="button">Copiar enlace</button>
									  </div> -->
									</div>
								</div>
								<div class="form-group">
									<div class="text-center">
										<a class="btn btn-primary disabled enlaceroom" href="javascript:void(0)" target="_blank">Ir a la sala</a>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="row">
							<div class="col-sm-4" style="border-right: 1px solid #c6c7c8;">
								<label>Reuniones Activas</label>
								<div class="table-responsive">
									<table class="table" id="allrooms">
										<thead>
											<th>Nombre</th>
											<th>Fecha</th>
											<th>Action</th>
										</thead>
										<tbody>
											<tr>
												<td>Mi reunion 1</td>
												<td>2020/01/02 10:00:51</td>
												<td class="tdAction">
													<div class="dropdown">
														<a href="javascript:void(0)" class="btn btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
														<div class="dropdown-menu">
															<a class="dropdown-item" href="#">Ir</a>
															<a class="dropdown-item" href="#">Editar</a>
															<a class="dropdown-item" href="#">Eliminar</a>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-sm-8">
								<label>Busqueda</label>
								<div class="row border-bottom ">
									<div class="col-sm-5">
										<label>Fecha</label>
										<div class="form-row">
											<div class="col-7">
												<input type="date" class="form-control" />
											</div>
											<div class="col-auto">
												<button type="button" class="btn btn-light">Reset</button>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>grupos</label>
											<select class="form-control">
												<option value="0">Seleccionar grupo...</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="card room-card">
										  <img class="card-img-top" src="https://cdn.clipart.email/67b7ec26148891e99db61be71577983e_zoom-zoom-call-hd-png-download-transparent-png-image-pngitem_860-779.png" alt="Card image cap">
										  <div class="card-body">
										    <h5 class="card-title">Nombre de la reunion</h5>
										    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
										    <div class="form-row">
										    	<div class="col-auto">
										    		<a href="javascript:void(0)" class="btn btn-primary">Ir a la reunión</a>
										    	</div>
										    	<div class="col-2">
												    <div class="dropdown">
														<a href="javascript:void(0)" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
														<div class="dropdown-menu">
															<a class="dropdown-item" href="#">Editar</a>
															<a class="dropdown-item" href="#">Eliminar</a>
														</div>
													</div>
												  </div>
										    	</div>
										    </div>
										</div>
									</div>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
			<!-- START CREATE AND EDIT VIDEO CONFERENCE -->
			<div class="col-sm-9" id="makeRoom-alert" style="display: none;">
				<div class="alert alert-primary">
					<p class="h4"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;No hay reuniones programada</p>
				</div>
			</div>
			<div class="col-sm-9" id="makeRoom" style="display: none;">
				<div class="card">
					<div class="card-header">
						<h5 class="mb-0"><i class="fa fa-video-camera icon-circle" aria-hidden="true"></i> &nbsp;<span class="text-creareditar">Crear sala</span></h5>
					</div>
		  			<div class="card-body">

		  				<input type="hidden" id="idcursodetalle" value="<?php echo !empty($this->idcursodetalle) ? $this->idcursodetalle:0; ?>">
		  				<input type="hidden" id="idcurso" value="<?php echo !empty($this->idcurso) ? $this->idcurso:0; ?>">
		  				<input type="hidden" id="idpestana" value="<?php echo !empty($this->idpestana) ? $this->idpestana:0; ?>">
		  				<input type="hidden" id="idpersona" value="<?php echo !empty($this->idalumno) ? $this->idalumno:0; ?>">
		  				<input type="hidden" id="idcomplementario" value="<?php echo !empty($this->idcomplementario) ? $this->idcomplementario:0; ?>">
		    			<div class="row">
		    				<div class="col-sm-6">
		    					<div class="form-group">
		    						<label>Nombre de la sala</label>
		    						<input type="text" class="form-control data-required" id="npt_name" placeholder="Nombre de la reunión..." />
		    					</div>
		    					<div class="form-group">
		    						<label>Descripci&oacute;n de la sala</label>
		    						<textarea class="form-control" id="txt_desc" placeholder="Descripcion..." rows="4"></textarea>
		    					</div>
		    				</div>
		    				<div class="col-sm-6">
		    					<div class="form-group">
		    						<label>Imagen de la sala</label>
		    						<!-- image-preview-filename input [CUT FROM HERE]-->
									<div class="input-group image-preview">
		  								<div class="image-preview-content">
		  									<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/nofoto.jpg" style="max-width:200px;" />
		  								</div>
										<input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
										<span class="input-group-btn">
											<!-- image-preview-clear button -->
											<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
												<i class="fa fa-times"></i> Eliminar
											</button>
											<!-- image-preview-input -->
											<div class="btn btn-default image-preview-input">
												<i class="fa fa-folder-open"></i>
												<span class="image-preview-input-title">Buscar</span>
												<input type="file" id="logoroom" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
											</div>
										</span>
									</div><!-- /input-group image-preview [TO HERE]--> 
		    					</div>
		    				</div>
		    				<div class="col-sm-6">
		    					<label>Fechas de la sala</label>
		    					<div class="form-group">
		    						<label>Fecha y hora de inicio</label>
		    						<div class="form-row">
		    							<div class="col-6">
		    								<input type="date" id="npt_date" class="form-control data-required" />
		    							</div>
		    							<div class="col-auto">
		    								<input type="time" id="npt_time" class="form-control data-required" value="<?php echo date('H:i:s'); ?>" />
		    							</div>
		    						</div>
		    					</div>
		    					<div class="form-check">
								    <input type="checkbox" class="form-check-input" id="exampleCheck1">
								    <label class="form-check-label" for="exampleCheck1">¿Requieres una segunda reunión?</label>
								</div>
		    					<div id="fechas2reunion" class="form-group pt-2" style="display: none;">
		    						<p class="alert alert-info">Se asignara la misma sala en otra fecha indicada como segunda videoconferencia</p>
		    						<label>Fecha y hora de la segunda sala</label>
		    						<div class="form-row">
		    							<div class="col-6">
		    								<input type="date" id="npt_date2" class="form-control" />
		    							</div>
		    							<div class="col-auto">
		    								<input type="time" id="npt_time2" class="form-control" />
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    				<div class="col-sm-6">
		    					<div class="form-group">
		    						<label>Email del anfitrión de la sala</label>
		    						<input type="email" id="npt_email" class="form-control" placeholder="<?php echo !empty($this->usuario) && !empty($this->usuario['email']) ? $this->usuario['email'] : '' ?>">
		    					</div>
		    					<div class="form-group">
		    						<label>Duración de la sala (En horas)</label>
		    						<input type="number" class="form-control" id="npt_duracion" min="1" value="1" placeholder="1" />
		    					</div>
		    				</div>
		    				<div class="col-sm-12 text-center py-2">
		    					<div class="row justify-content-center text-center">
		    						<div class="col-3">
		    							<button type="button" class="btn btn-warning on-cancel">Cancelar</button>
		    						</div>
		    						<div class="col-3">
		    							<button type="button" class="btn btn-primary on-create">Guardar</button>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
		  			</div>
				</div>
			</div>
		</div>

	<?php endif; ?>
</div>

<script type="text/javascript">
	// var allroomsTable;
	var INIT_ROOMS = <?php echo !empty($this->rooms) ? json_encode($this->rooms) : '[]' ?>;
	var INIT_ROOM = <?php echo !empty($this->room) ? json_encode($this->room) : '[]' ?>;
	var INIT_USER = <?php echo !empty($this->usuario) ? json_encode($this->usuario) : '[]' ?>;
	var DEFAULT_EMAIL = '<?php echo !empty($this->usuario) && !empty($this->usuario['email']) ? $this->usuario['email'] : '' ?>';
	var IDPERSONA = <?php echo !empty($this->idalumno) ? $this->idalumno:0; ?>;
	var IDCURSO = <?php echo !empty($this->idcurso) ? $this->idcurso:0; ?>;
	var IDCURSODETALLE = <?php echo !empty($this->idcursodetalle) ? $this->idcursodetalle:0; ?>; 
	var IDPESTANA = <?php echo !empty($this->idpestana) ? $this->idpestana:0; ?>; 
	var IDCOMPLEMENTARIO = <?php echo !empty($this->idcomplementario) ? $this->idcomplementario:0; ?>;
	var GRUPOAULADETALLES = <?php echo !empty($this->grupoauladetalles) ? json_encode($this->grupoauladetalles) : '[]' ?>;
	var CURRENT_FECHA = new Date('<?php echo date("Y-m-d H:i:s") ?>');
	var IDZOOM = 0;
	var IDZOOM2 = 0;
	var state = 1; /* 1- list docente, 2- crear room, 3- editar, 4 list alumno */
	var flagmake = false,flagmake2 = false;

	function mostrarcreacion(){
		try{
			state = 2;
			$('.text-creareditar').text('Crear sala');
			$('#makeRoom').find('input,textarea').val(''); //reset all inputs
			$('#exampleCheck1').prop('checked',false);

			$('#makeRoom-alert,#makeRoom').show();
			$('.on-cancel').closest('div').hide();
		}catch(error){
			console.error("Error syntax: ",error);
		}
	}
	function mostraredicion(){
		try{
			var fechaArr = INIT_ROOM.fecha.split(' ');
			var _duracion = Math.floor(parseInt(INIT_ROOM.duracion) / 60);

			state = 3;
			_duracion = _duracion < 1 ? 1 : _duracion;

			$('.text-creareditar').text('Editar sala');
			//initialize values
			$('#npt_name').val(INIT_ROOM.titulo);
			$('#txt_desc').val(INIT_ROOM.agenda);
			if(INIT_ROOM.imagen != '' && INIT_ROOM.imagen != null){
				$('.image-preview-content img').attr('src', String.prototype.concat(_sysUrlStatic_,INIT_ROOM.imagen ));
			}else{
				$('.image-preview-content img').attr('src', String.prototype.concat(_sysUrlStatic_,'/media/nofoto.jpg' ));
			}
			$('#npt_date').val(fechaArr[0]);
			$('#npt_time').val(fechaArr[1]);
			if(INIT_ROOM.fechas_salas != null && Object.keys(INIT_ROOM.fechas_salas).length > 0){
				var fecha2Arr = INIT_ROOM.fechas_salas[1].split(' ');;
				$('#exampleCheck1').prop('checked',true);
				$('#fechas2reunion').show();
				$('#npt_date').val(fecha2Arr[0]);
				$('#npt_time').val(fecha2Arr[1]);
			}
			$('#npt_email').val(((INIT_ROOM.email == '' || INIT_ROOM.email == null) ? DEFAULT_EMAIL : INIT_ROOM.email));
			$('#npt_duracion').val(_duracion);

			//draw formulario
			$('#makeRoom-alert').hide();
			$('#listDocente').hide('slow', function() {
				$('#makeRoom').show('fast');
				$('.on-cancel').closest('div').show();
			});
			
		}catch(error){
			console.error("Error syntax: ",error);
		}
	}
	function mostrarsala(){
		try{
			if(Object.keys(INIT_ROOMS).length == 0){
				mostrarcreacion(); //go to make
				return false;
			}
			let fecharoom;
			$('#listDocente').show('fast');

			state = 1;
			IDZOOM = INIT_ROOM.idzoom;

			if(INIT_ROOM.imagen != '' && INIT_ROOM.imagen != null){
				$('#previewIMG').attr('src', String.prototype.concat(_sysUrlStatic_,INIT_ROOM.imagen) );
			}
			$('.tituloroom').text(INIT_ROOM.titulo);
			$('.descroom').html(INIT_ROOM.agenda);
			$('.fecharoom').html('');
			$('.linkroom').val(INIT_ROOM.urlparticipantes);
			$('.enlaceroom').attr('href',INIT_ROOM.urlparticipantes);
			//validar si hay segunda reunion
			if(INIT_ROOM.fechas_salas != null && Object.keys(INIT_ROOM.fechas_salas).length > 0 ){
				IDZOOM2 = INIT_ROOMS[1];
				for(var f of INIT_ROOM.fechas_salas){
					$('.fecharoom').append('<li>'+f+'</li>');
				}
			} else{
				$('.fecharoom').append('<li>'+INIT_ROOM.fecha+'</li>');
			}
			//Verificar si las fechas coincide para habilitar el boton
			$('.fecharoom').find('li').each(function(index, el) {
				fecharoom = new Date($(this).text());
				if(CURRENT_FECHA == fecharoom){
					$('.enlaceroom').removeClass('disabled');
				}
			});
			return true;
		}catch(error){
			console.error("Error syntax: ",error);
		}
	}
	function initevent(){
		/**
		* Image preview event
		*/
		$('.image-preview-clear').click(function(){
			$('.image-preview-content').html("").hide();
			$('.image-preview-filename').val("");
			$('.image-preview-clear').hide();
			$('.image-preview-input #logoroom').val("");
			$(".image-preview-input-title").text("Buscar"); 
		}); 
		$(".image-preview-input #logoroom").change(function (){     
			var img = $('<img/>', {
				id: 'dynamic',
				style:'max-width:175px;max-height:170px;',
			});      
			var file = this.files[0];
			var reader = new FileReader();
			// Set preview image into the popover data-content
			reader.onload = function (e) {
				$(".image-preview-input-title").text("Cambiar");
				$(".image-preview-clear").show();
				$(".image-preview-filename").val(file.name);            
				img.attr('src', e.target.result);
				$(".image-preview-content").html($(img)[0].outerHTML).show();
			}        
			reader.readAsDataURL(file);
		});
		/*--------------------------*/
		$('body').on('click','.on-editroom',function(){
			mostraredicion()
		});
		$('body').on('change','#exampleCheck1',function(){
			if($(this).is(':checked') == true){
				$('#fechas2reunion').show();
			}else{
				$('#fechas2reunion').hide();
			}
		});
		$('body').on('click','.on-create',function(){
			var _continuar =  true; //para validar
			var POSTDATA = new Object;
			if(flagmake == true){
				return false;
			}
			//validar campos requeridos
			$('#makeRoom').find('.data-required').each(function(index, el) {
				$(this).css('border',"");
				if($(this).val() == 0 || $(this).val() == ''){
					$(this).css('border',"1.5px solid red");
					_continuar = false;
				}
			});
			if(_continuar == false){
				Swal.fire('Espera!','Faltan campos por definir en el formulario', 'warning');
				return false;
			}
			
			//preparar datos
			let hour2min = ($('#npt_duracion').val() == '') ? 60 : parseInt($('#npt_duracion').val()) * 60 ;
			let form_data = new FormData(); // added
			let _URL = "json/videosconferencia/crearsala";
			let _email = $('#npt_email').val() == '' ? DEFAULT_EMAIL : $('#npt_email').val();

			if(state == 3){
				form_data.set('idzoom',IDZOOM);
				_URL = "json/videosconferencia/editarsala";
			}

			form_data.set('idpersona',IDPERSONA);
			form_data.set('email',_email);
			form_data.set('fecha',$('#npt_date').val());
			form_data.set('time',$('#npt_time').val());
			form_data.set('titulo',$('#npt_name').val());
			form_data.set('agenda',$('#txt_desc').val());
			form_data.set('duracion',hour2min);
			form_data.set('imagen',$('#logoroom')[0].files[0]);
			form_data.set('idcursodetalle',IDCURSODETALLE);
			form_data.set('idpestania',IDPESTANA);
			form_data.set('idcurso',IDCURSO);
			form_data.set('idcomplementario',IDCOMPLEMENTARIO);
			form_data.set('idgrupoauladetalle',GRUPOAULADETALLES);
			
			$.post(String.prototype.concat(_sysUrlBase_,"json/videosconferencia/verifyuser"), {email: _email}, function(resp, textStatus, xhr) {
				console.log(resp)
				if(resp.code == 200){ //yes, the user is exist
					flagmake = true;
					$.ajax({
			            url: String.prototype.concat(_sysUrlBase_,_URL),
			            type: 'POST',
			            data: form_data,
			            dataType:'json',
			            contentType: false,
			            processData: false,
			            success: function(data){
			            	console.log(data);
			                if(data.code == 200){
								INIT_ROOMS = [];
								INIT_ROOM = data.data[0];
								INIT_ROOMS.push(data.data[0]);
								flagmake = false;

								mostrar_notificacion("Room","Guardo correctamente",'success');
								$('#makeRoom,#makeRoom-alert').hide('slow', function() {
									mostrarsala();
								});
							}else{
								console.error("en la creacion de la sala ",data.msj);
								Swal.fire('Ops!',data.msj, 'error');
							}
			            },
			        }).fail(function(data){
						Swal.fire('Ops!','Ocurrio algun error del sistema', 'error');
					});
					
					if($('#exampleCheck1').is(':checked') == true){
						if(state == 3){
							form_data.set('idzoom',IDZOOM2);
						}
						form_data.set('fecha',$('#npt_date2').val());
						form_data.set('time',$('#npt_time2').val());
						console.log("guarde 2");
						if(flagmake2 == false){
							flagmake2 = true;
							$.ajax({
					            url: String.prototype.concat(_sysUrlBase_,_URL),
					            type: 'POST',
					            data: form_data,
					            dataType:'json',
					            contentType: false,
					            processData: false,
					            success: function(data2){
					                if(data2.code == 200){
										INIT_ROOMS.push(data2.data[0]);
										INIT_ROOM.fechas_salas = [INIT_ROOM.fecha,data2.data[0].fecha];
										$('.fecharoom').html('<li>'+INIT_ROOM.fecha+'</li><li>'+data2.data[0].fecha+'</li>');
										mostrar_notificacion("Room","Guardo correctamente la segunda sala",'success');
										flagmake2 = false;
									}else{
										console.error("en la creacion de la sala ",data2.msj);
										Swal.fire('Ops!',data2.msj, 'error');
									}
					            },
					        }).fail(function(data2){
								Swal.fire('Ops!','Ocurrio algun error del sistema', 'error');
							});
						}
					}//endif
				}else{
					Swal.fire('Ops!',resp.msj, 'warning');		
				}
				
			},'json');
		});
		$('body').on('click','.on-cancel',function(){
			state = 1;
			$('#makeRoom-alert').hide();

			$('#makeRoom').hide('slow', function() {
				mostrarsala();
			});
		});
	}

	$(document).ready(function(){
		initevent();
		setTimeout(function(){
			$('#npt_time,#npt_time2').val('<?php echo date('H:i:s') ?>');
			$('#npt_date,#npt_date2').val('<?php echo date('Y-m-d') ?>');
		},500);
		$('.container-especial').closest('#contentpages').css('overflow','auto');

		//determinar que vista mostrar
		if(Object.keys(INIT_ROOMS).length > 0){
			state = 1;
			mostrarsala();
		}else{
			mostrarcreacion();
		}

		// allroomsTable  = $('#allrooms').DataTable({
		// 	"lengthChange": false,
		// 	"pageLength": 15
		// });
	});
</script>