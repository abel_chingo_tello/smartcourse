<?php

$this->link = explode(".txt?id", $this->link)[0];
$this->link = explode("/static/", $this->link)[1];
$this->link = explode("/", $this->link);
$this->link = implode(SD, $this->link);
$this->link = RUTA_BASE . "static" . SD . $this->link . ".php";
// var_dump($this->link);
if(is_file($this->link)){
    include_once $this->link;    
}else {
    exit($this->link . " No Existe");
}
$enlaces = json_decode($enlaces, true);

foreach ($enlaces as $key => $value) {
    // if (!empty($value["base64"])) {
    //     if ($value["base64"] == 1) {
    //         $value["titulo"] = base64_decode($value["titulo"], true);
    //     }
    // }
    $value["titulo"] = base64_decode($value["titulo"], true);
    $enlaces[$key] = $value;
}
// $enlace = utf8_encode($enlaces);
// var_dump($enlaces);
?>
<style>
    .my-col {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 20px;
    }

    .separate {
        /* margin-bottom: 20px; */
        margin-top: 40px;
    }

    h1 {
        margin-bottom: 20px;
    }

    /* -- FIXING -- */
    .lib {
        cursor: pointer;
        overflow: hidden;
        max-width: 300px;
        height: 320px;
        max-height: 320px;
        transition: max-height 2000ms ease-in-out;
        position: relative;
        width: 100%;
    }

    .card-title {
        overflow: hidden;
        max-height: 30px;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 100%;
        transition: max-height 2000ms ease-in-out;
    }

    /* .card:hover {
        opacity: 1;
        background-color: rgba(0,0,0,0.5);
    } */

    .lib:hover .mask {
        opacity: 1;
    }

    .active .mask2 {
        opacity: 1 !important;
    }

    .lib .mask {
        position: absolute;
        opacity: 0;
        background-color: rgba(0, 0, 0, 0.5);
        transition: all 0.4s ease-in-out;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .lib .mask2 {
        position: absolute;
        opacity: 0;
        background-color: rgba(57, 122, 240, 0.5);
        transition: all 0.4s ease-in-out;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    /* .card:hover {
        max-height: 100vh;
        white-space: normal;
        transition: max-height 2000ms ease-in-out;
    } */

    .lib-img-top {
        height: 100%;
        width: 100%;
    }

    /* .card:hover>.card-body>.card-title {
        white-space: normal;
        position: absolute;
        bottom: 0px;
        left: 0px;
        padding: 10px;
        padding-left: 20px;
        backdrop-filter: saturate(180%) blur(20px) !important;
        background-color: rgba(255, 255, 255, 0.56) !important;
        width: 100%;
        max-height: 100vh;

    } */


    .row.separate {
        align-items: start;
    }

    #contentpages {
        overflow: auto !important;
    }

    /* -- FIXING -- */
</style>
<div class="container-fluid">
    <!-- <center>
        <h1>Enlaces de Interés</h1>
    </center> -->
    <div class="row separate" id="viewlibrery" style="display: none;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title"></h5>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" onclick="viewLibreryClose()"><i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <!-- <img src="${enlace.img}" class="lib-img-top" alt="..."> -->
                </div>
            </div>
        </div>
    </div>
    <div class="row separate">
        <?php foreach ($enlaces as $i => $enlace) : ?>
            <div class="my-col col-md-4 col-sm-6 col-xs-12">
                <div class="card lib lib-<?php echo $i ?>" onclick="viewLibrery(this)" tipo="<?php echo $enlace['tipo'] ?>" link="<?php echo $enlace['link'] ?>">
                    <div class="card-header">
                        <h5 class="card-title"><?php echo $enlace['titulo'] ?></h5>
                    </div>
                    <div class="card-body">
                        <img src="<?php echo URL_BASE . $enlace['img'] ?>" class="lib-img-top" alt="...">
                    </div>
                    <div class="mask"></div>
                    <div class="mask2"></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<script>
    $("#contentpages").removeClass("embed-responsive-16by9");

    function viewLibreryClose() {
        $("#viewlibrery").find(".card-body").html("");
        $('#viewlibrery').hide()
    }

    function viewLibrery(e) {
        $(".lib").removeClass("active");
        $(e).addClass("active");
        $("#viewlibrery").show();
        $('#contentpages').animate({
            scrollTop: 0
        }, 'slow');
        $("#viewlibrery").find(".card-title").html($(e).find(".card-title").html());
        var el = $(e);
        typelink = el.attr('tipo') || '';
        link_ = el.attr('link') || '';
        if (typelink != "") {
            if (el.length == 0) return;
            if (typelink != "enlaceyoutube") {
                link_ = link_.split("?id=")[0];
                link_ = _sysUrlBase_ + link_;
            }
            __sysajax({
                fromdata: {
                    link: link_
                },
                type: 'html',
                showmsjok: false,
                async: false,
                url: _sysUrlSitio_ + '/plantillas/' + typelink,
                callback: function(rs) {
                    if (typelink == 'imagen') $("#viewlibrery").find(".card-body").removeClass('embed-responsive embed-responsive-16by9');
                    else $("#viewlibrery").find(".card-body").addClass('embed-responsive embed-responsive-16by9');
                    $("#viewlibrery").find(".card-body").html(rs);
                    $("iframe").on('load', function(ev) {
                        agregarFuncionCronometro();
                    });
                    $("iframe").siblings("div").remove();
                }
            });
        } else {
            var sincontenido = `
            <div class="row">
                <div class="col-md-12">
                    <h3 class="no-mensajes">Sin contenido asignado ...</h3>
                </div>
            </div>
            `;
            $("#viewlibrery").find(".card-body").removeClass("embed-responsive embed-responsive-16by9");
            $("#viewlibrery").find(".card-body").html(sincontenido);
        }
    }
</script>