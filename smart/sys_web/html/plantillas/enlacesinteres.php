<?php

$this->link = explode(".txt?id", $this->link)[0];
$this->link = explode("/static/", $this->link)[1];
$this->link = explode("/", $this->link);
$this->link = implode(SD, $this->link);
$this->link = RUTA_BASE . "static" . SD . $this->link . ".php";
// var_dump($this->link);
include_once $this->link;

$enlaces = json_decode($enlaces, true);

foreach ($enlaces as $key => $value) {
    if (!empty($value["base64"])) {
        if ($value["base64"] == 1) {
            $value["titulo"] = base64_decode($value["titulo"], true);
        }
    }
    $enlaces[$key] = $value;
}
// $enlace = utf8_encode($enlaces);
// var_dump($enlaces); 
?>
<style>
    .my-col {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 20px;
    }

    .separate {
        /* margin-bottom: 20px; */
        margin-top: 40px;
    }

    h1 {
        margin-bottom: 20px;
    }

    /* -- FIXING -- */
    .card {
        overflow: hidden;
        max-width: 22.2vw;
        height: 230px;
        max-height: 230px;
        transition: max-height 2000ms ease-in-out;
        position: relative;
    }

    .card-title {
        overflow: hidden;
        max-height: 30px;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 100%;
        transition: max-height 2000ms ease-in-out;
    }

    .card:hover {
        max-height: 100vh;
        white-space: normal;
        transition: max-height 2000ms ease-in-out;
    }


    .card:hover>.card-body>.card-title {
        white-space: normal;
        position: absolute;
        bottom: 0px;
        left: 0px;
        padding: 10px;
        padding-left: 20px;
        backdrop-filter: saturate(180%) blur(20px) !important;
        background-color: rgba(255, 255, 255, 0.56) !important;
        width: 100%;
        max-height: 100vh;

    }


    .row.separate {
        align-items: start;
    }

    #contentpages {
        overflow: auto !important;
    }

    /* -- FIXING -- */
</style>
<div class="container-fluid">
    <center>
        <h1>Enlaces de Interés</h1>
    </center>
    <div class="row separate">
        <?php foreach ($enlaces as $key => $enlace) : ?>
            <div class="my-col col-md-4">
                <a target="_blank" href="<?php echo $enlace['link'] ?>" class="">
                    <div class="card">
                        <img src="<?php echo URL_BASE . $enlace['img'] ?>" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $enlace['titulo'] ?></h5>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>