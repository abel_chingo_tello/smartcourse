<?php
$usuarioAct = NegSesion::getUsuario();
$rol = $usuarioAct['idrol'];

?>
<style type="text/css">
    .x_panel {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
    padding: 10px 17px;
    display: inline-block;
    background: #fff;
    border: 1px solid #E6E9ED;
    -moz-column-break-inside: avoid;
    column-break-inside: avoid;
    opacity: 1;
    transition: all .2s ease;
}
</style>
<div class="clearfix"><br></div>
<div class="container-fluid">
    <div class="paris-navbar-container row">
        <div class="col-md-12 col-sm-12">    
            <div class="x_panel paris-navbar-panel">      
                <div class="x_content paris-navbar">        
                    <div class="col-md-8 paris-left">           
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" onclick="history.back()" >
                            <i class="fa fa-reply"></i> Volver</button>          
                            <label class="paris-divisor"><i class="fa fa-chevron-right"></i></label>
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" >Foro</button>
                    </div>            
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="text-center col-md-12"><h2>Foro : <?php echo ucfirst($this->recurso["titulo"]); ?></h2></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row mx-auto">
                <div class="col-lg-12 col-md-12 mx-auto div-foro">
                    <div class="post-preview"> 
                        <p><?php echo $this->recurso["descripcion"]; ?></p>
                        <p class="post-meta">Publicado por   <i><b><?php echo $this->empresa["nombre"]." : <small>".$this->recurso["struser"]."</small>"; ?></b></i> </p>
                    </div>
                    <hr>
                </div>
            </div>
        </div>        
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="alert"></div>
            <div class="alert alert-info" role="alert">
              <h4 class="alert-heading"><?php echo JrTexto::_('Error');?>!</h4>
              <p><?php echo JrTexto::_('Sorry only one student can answer this resource and only teacher can rate it');?>.</p>
              <hr>
              <p class="mb-0"><?php echo JrTexto::_('thanks for your understanding');?>.</p>
            </div>
        </div>
    </div>
</div>