<?php
$usuarioAct = NegSesion::getUsuario();
$rol = $usuarioAct['idrol'];
$idgui_=date('YmdHis');
?>
<style type="text/css">
    .x_panel {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
    padding: 10px 17px;
    display: inline-block;
    background: #fff;
    border: 1px solid #E6E9ED;
    -moz-column-break-inside: avoid;
    column-break-inside: avoid;
    opacity: 1;
    transition: all .2s ease;
}
 .media,
  .mb-4 {
    background-color: #ffffff66;
    ;
    padding: 15px;
    border-radius: 10px;
  }

  .media:hover,
  .mb-4:hover {
    background-color: white;
  }

  .widgets {
    display: none;
  }

  .autor {
    /* font-style: italic; */
  }

  .fecha {
    font-style: italic;
  }
  .contenido{
    font-size: 15px;
  }


</style>

<div class="clearfix"><br></div>
<div class="container-fluid">
    <!--div class="paris-navbar-container row">
        <div class="col-md-12 col-sm-12">    
            <div class="x_panel paris-navbar-panel">      
                <div class="x_content paris-navbar">        
                    <div class="col-md-8 paris-left">           
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" onclick="history.back()" >
                            <i class="fa fa-reply"></i> Volver</button>          
                            <label class="paris-divisor"><i class="fa fa-chevron-right"></i></label>
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" >Foro</button>
                    </div>            
                </div>
            </div>
        </div>
    </div-->
    <div class="row">
      <div class="col-lg-1 col-sm-12"></div>
      <div id="post" class="col-lg-10 col-sm-12">
            <h1 class="mt-4"><?php echo ucfirst($this->recurso["titulo"]); ?></h1>
            <p class="lead">
              Publicado por
              <a class="autor" href="#"><i><b><?php echo $this->empresa["nombre"]." <br><strong>Docente : </strong><small>".$this->recurso["struser"]."</small>"; ?></b></i></a>
            </p>
            <hr>            
              <div class="contenido">
                <?=htmlspecialchars_decode($this->recurso["descripcion"]) ?>
                <?php if(!empty($this->tema)){ ?>
                 <br><strong>Tema : </strong> <?php echo $this->tema;?> 
                <?php }?>
              </div>              
            <hr> 
            
              <?php 
                $respuesta='';
                if(!empty($this->haygrupo["respuestas"])){
                  if(is_file(RUTA_BASE.$this->haygrupo["respuestas"])){
                    require_once(RUTA_BASE.$this->haygrupo["respuestas"]);
                    //$respuesta;
                  }
                }
                $participantes='';
                if(!empty($this->wikiamigos)) 
                foreach ($this->wikiamigos as $key => $alu){
                   $participantes.='<span>'.str_replace(',', '', $alu["stralumno"]).'<b>,</b> </span>';
                } ?>

                <?php if(!empty($this->haygrupo["nota"])){ ?> 
                <div id="lista_comentarios" style="    border: 1px solid #ccc; border-radius: 1ex; padding: 1em;">            
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <p class="lead"><a class="autor" href="#"><i><b><strong>Participante(s) : </strong><small><?php echo $participantes; ?></small></b></i></a></p>  
                      </div>                     
                        <?php $notaBase=($this->haygrupo['nota']*$this->formulaNota["calificacion_total"])/$this->haygrupo["nota_base"];?>
                        <div class="col-md-3 col-sm-5 col-xs-12 text-right">
                          <h5>Nota : <strong><?php echo $notaBase; ?></strong></h5>
                        </div>                      
                    </div>                    
                    <div class="contenido">
                      <?php echo $respuesta; ?>                        
                    </div>
                    <p class="fecha">Ultima actualización  <?php echo $this->haygrupo['fecha_respuesta']; ?></p>
                  </div>
                </div>
                <?php } else{     ?>
            <div class="card my-4">
              <h5 class="card-header">Participante(s): <small><?php echo $participantes; ?></small></h5>
              <div class="card-body">
                <form id="frmGuardarcomentario_wiki">
                  <input type="hidden" name="idrecursoasignacion" id="idrecursoasignacion" value="<?php echo $this->idrecursoasignacion; ?>">                  
                  <div class="form-group">
                    <textarea id="contenido_comentario<?php echo $idgui_; ?>" name="contenido_comentario" required="required" class="form-control" rows="10"><?php echo @$respuesta;?></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary frmGuardarcomentario_wikibtn"><i class="fa fa-send"></i> Enviar</button>
                </form>
              </div>
            </div>
           <?php } ?>
            <!-- Comment with nested comments -->
            
          </div>
    </div>
</div>


<script type="text/javascript">
  $(function(){ 

    if($('#contenido_comentario<?php echo $idgui_; ?>').length>0)
    tinymce.init({
      relative_urls : false,
      remove_script_host : false,
      convert_urls : true,
      convert_newlines_to_brs : true,
      menubar: false,
      //statusbar: false,
      verify_html : false,
      content_css : [_sysUrlBase_+'/static/tema/css/bootstrap.min.css'],
      selector: '#contenido_comentario<?php echo $idgui_; ?>',
      height: 400,
      paste_auto_cleanup_on_paste : true,
      paste_preprocess : function(pl, o) {
          var html='<div>'+o.content+'</div>';
          var txt =$(html).text();
          o.content = txt;
          },
      paste_postprocess : function(pl, o) {
          o.node.innerHTML = o.node.innerHTML;
          o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
          },
      plugins:[" textcolor paste lists advlist" ],  // chingoinput chingoimage chingoaudio chingovideo
      toolbar: ' | undo redo  |  removeformat |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor fontsizeselect| ', //chingoinput chingoimage chingoaudio chingovideo,
      advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",
      setup: function (editor){
          editor.on('change', function () {
              tinymce.triggerSave();
          });
      }  
    });



    $('.frmGuardarcomentario_wikibtn').on('click',function(ev){
      ev.preventDefault();
      Swal.fire({
          // title: 'Cargando...',
          allowOutsideClick: false,
          allowEscapeKey: false,
          onBeforeOpen: () => {
              Swal.showLoading();
          },
      })
      //console.log($(this).attr('id'));
      //let frmid=document.getElementById($(this).attr('id'));
      //var formData = new FormData(frmid);
      let formData={
        'comentario':$('#frmGuardarcomentario_wiki').find('textarea[name="contenido_comentario"]').val(),
        'idrecursoasignacion':$('#frmGuardarcomentario_wiki').find('#idrecursoasignacion').val(),
      }

     /* let idfrm=document.getElementById('');
      let formData=new FormData(idfrm);*/


      $.ajax({
        url: _sysUrlBase_+'json/recursos_colaborativosasignacion/guardar_respuestawiki/',
        type: "POST",
        data: formData,
        dataType: "json",
        success: function(rs){
          Swal.close();
          if(rs.code==200){
            Swal.fire({
              type: 'success',//warning (!)//success
              icon: 'success',//warning (!)//success             
              text: rs.msj,
              //footer: config.footer,
              allowOutsideClick: false,
              closeOnClickOutside: false
            })
          }else{
            Swal.fire({
              type: 'error',//warning (!)//success
                icon: 'error',//warning (!)//success
                title: 'Oops...',
                text: rs.msj,
                //footer: config.footer,
                allowOutsideClick: false,
                closeOnClickOutside: false
            })
          }
          console.log(response);
        },
        error: function() {
          Swal.close();
          console.log('Error');
        },
      });
      return false;
    })

  });
</script>