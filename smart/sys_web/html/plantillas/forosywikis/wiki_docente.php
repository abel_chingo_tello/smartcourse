<?php
$usuarioAct = NegSesion::getUsuario();
$rol = $usuarioAct['idrol'];
?>
<style type="text/css">
    .x_panel {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
    padding: 10px 17px;
    display: inline-block;
    background: #fff;
    border: 1px solid #E6E9ED;
    -moz-column-break-inside: avoid;
    column-break-inside: avoid;
    opacity: 1;
    transition: all .2s ease;
}
 .media,
  .mb-4 {
    background-color: #ffffff66;
    ;
    padding: 15px;
    border-radius: 10px;
  }

  .media:hover,
  .mb-4:hover {
    background-color: white;
  }

  .widgets {
    display: none;
  }

  .autor {
    /* font-style: italic; */
  }

  .fecha {
    font-style: italic;
  }
  .contenido{
    font-size: 15px;
  }
  .plnforo{
    border:1px solid #ccc;
    border-radius: 1ex; 
    -webkit-box-shadow: 0px 6px 6px 1px rgba(120,190,227,0.42);
    -moz-box-shadow: 0px 6px 6px 1px rgba(120,190,227,0.42);
    box-shadow: 0px 6px 6px 1px rgba(120,190,227,0.42);
  }
  .pnlerror{
    border:1px solid #ccc;
    border-radius: 1ex; 
    padding: 1em;
    text-align: center;
    -webkit-box-shadow: 0px 6px 6px 1px rgba(252,168,0,1);
-moz-box-shadow: 0px 6px 6px 1px rgba(252,168,0,1);
box-shadow: 0px 6px 6px 1px rgba(252,168,0,1);
  }
  .calificarrespuesta_foro{
    cursor:pointer;
  }


</style>
<div class="clearfix"><br></div>
<div class="container-fluid">
    <div class="paris-navbar-container row">
        <div class="col-md-12 col-sm-12">    
            <div class="x_panel paris-navbar-panel">      
                <div class="x_content paris-navbar">        
                    <div class="col-md-8 paris-left">           
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" onclick="history.back()" >
                            <i class="fa fa-reply"></i> Volver</button>          
                            <label class="paris-divisor"><i class="fa fa-chevron-right"></i></label>
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" >Foro</button>
                    </div>            
                </div>
            </div>
        </div>
    </div>


    <div class="row">
      <div class="col-lg-1 col-sm-12"></div>
      <div id="post" class="col-lg-10 col-sm-12">
            <h1 class="mt-4"><?php echo ucfirst($this->recurso["titulo"]); ?></h1>
            <p class="lead">
              Publicado por
              <a class="autor" href="#"><i><b><?php echo $this->empresa["nombre"]." <br><strong>Docente : </strong><small>".$this->recurso["struser"]."</small>"; ?></b></i></a>
            </p>
            <hr>            
              <div class="contenido">
                <?=htmlspecialchars_decode($this->recurso["descripcion"]) ?>                
              </div>
              <p class="fecha">Publicado el <?php echo $this->recurso['fecha_creacion']; ?></p> <hr><hr>
            <?php $nohay=true; if(!empty($this->respuestas))
            foreach ($this->respuestas as $k => $r){ 
                if(!empty($r["tema"])){ ?>
                 <br><strong>Tema: </strong> <?php echo $r["tema"];?> 
                <?php }else{?>
                  <br><strong>Grupo: </strong> <?php echo $k;?> 
                <?php }
                $participantes='';
                $respuesta='';
                $nota=0;
                $notaBase=20;
                $fecha_respuesta='';
                $idasignacion=-1;
                if(!empty($r["respuestas"])){
                  foreach ($r["respuestas"] as $key => $alu){
                    $participantes.='<span>'.str_replace(',', '', $alu["stralumno"]).'<b>,</b> </span>';
                    if(empty($respuesta)){
                      $nota=$alu["nota"];
                      $nota_base=$alu["nota_base"];
                      $fecha_respuesta=$alu['fecha_respuesta'];
                      $idasignacion=$alu['idrecursoasignacion'];
                      if(is_file(RUTA_BASE.$alu["respuestas"])){
                        require_once(RUTA_BASE.$alu["respuestas"]);
                      }      
                    } 
                  }
                } 
                if(!empty($respuesta)){
                ?>
                <div id="lista_comentarios" style="border: 1px solid #ccc; border-radius: 1ex; padding: 1em;"> 
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <p class="lead"><a class="autor" href="#"><i><b><strong>Participante(s) : </strong><small><?php echo $participantes; ?></small></b></i></a></p>  
                      </div>                      
                        <div class="col-md-3 col-sm-5 col-xs-12 text-right infopnl" idasignacion="<?php echo $idasignacion?>" nota_maxima="<?php echo @$this->formulaNota["calificacion_total"]; ?>">
                          <?php if(!empty($nota)){ 
                            $notaActual=($nota*$this->formulaNota["calificacion_total"])/$nota_base;?>                            
                              <h6>Nota : <strong class="calificarrespuesta_wiki aquinota" nota="<?php echo $notaActual;?>" ><?php echo $notaActual; ?></strong></h6>                            
                          <?php }else{ ?>
                            <h6><span style="display: none">Nota</span> <span class="calificarrespuesta_wiki aquinota" nota="0">(Calificar Wiki)</span><h6>
                          <?php } ?>
                        </div>                      
                    </div>                    
                    <div class="contenido">
                      <?php echo $respuesta; ?>                        
                    </div>
                    <p class="fecha">Ultima actualización  <?php echo $fecha_respuesta; ?></p>
                  </div>
                </div><?php }else{?>
                   <div class="container">
                    <div class="row">
                      <div class="col-md-12 pnlerror text-center">
                        <h5>
                          <!--strong><?php echo JrTexto::_('Information') ?></strong--> 
                          <?php echo JrTexto::_('There are no comments because no student has responded yet'); ?>.
                         </h5>                    
                      </div>                  
                    </div>
                  </div>
                <?php } ?>  
                <?php } ?> 
                <hr>                   
          </div>
    </div>
</div>
<script type="text/javascript">
  $(function(){ 
    $('.calificarrespuesta_wiki').on('click',function(ev){      
      ev.preventDefault();

      let nota=$(this).attr('nota')||0;
      var infopnl=$(this).closest('.infopnl');
      var idasignacion=infopnl.attr('idasignacion')||'';
      let nota_maxima=infopnl.attr('nota_maxima')||'20';

      Swal.fire({
        title: 'Registra nota hasta '+nota_maxima+' como maximo',
        input: 'number',
        inputValue: nota,
        inputAttributes: {
          autocapitalize: 'off',
          min:0,
          max:nota_maxima,
        },
        allowOutsideClick: false,
        allowEscapeKey: false,
        showCancelButton: true,
        confirmButtonText: 'Guardar',
        showLoaderOnConfirm: true,
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {      
        if(result.dismiss=="cancel") return false;
        if(result.value==''|| result.value=='0') return false;

        Swal.fire({
          // title: 'Cargando...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => { Swal.showLoading(); },
        })

        let notaobtenida=result.value;
        var frm = new FormData();
        frm.append("nota",notaobtenida);
        frm.append("idrecursoasignacion",idasignacion);
        frm.append("nota_base",nota_maxima);
        frm.append("tipo",'wiki');
     
        fetch(_sysUrlBase_+'json/recursos_colaborativosasignacion/guardar_nota/',{method:'POST',body:frm})
        .then(response => response.json()).then(rs=>{        
            if(rs.code==200){
              Swal.fire({
                type: 'success',//warning (!)//success
                icon: 'success',//warning (!)//success             
                text: rs.msj,
                //footer: config.footer,
                allowOutsideClick: false,
                closeOnClickOutside: false
              })
              infopnl.find('.calificarrespuesta_foro').attr('nota',notaobtenida);
              infopnl.find('.calificarrespuesta_foro.aquinota').text(notaobtenida);
            }else{
              Swal.fire({
                type: 'error',//warning (!)//success
                  icon: 'error',//warning (!)//success
                  title: 'Oops...',
                  text: rs.msj,
                  //footer: config.footer,
                  allowOutsideClick: false,
                  closeOnClickOutside: false
              })
            }
        }).catch(e=>{        
        console.log(e);  
          Swal.fire({
            type: 'error',//warning (!)//success
              icon: 'error',//warning (!)//success
              title: 'Oops...',
              text: 'error',
              //footer: config.footer,
              allowOutsideClick: false,
              closeOnClickOutside: false
          })
        })
      })      
      return false;
    })
  });
</script>