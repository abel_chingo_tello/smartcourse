<?php
$usuarioAct = NegSesion::getUsuario();
$rol = $usuarioAct['idrol'];
$idgui_=date('YmdHis');
?>
<style type="text/css">
  .x_panel {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
    padding: 10px 17px;
    display: inline-block;
    background: #fff;
    border: 1px solid #E6E9ED;
    -moz-column-break-inside: avoid;
    column-break-inside: avoid;
    opacity: 1;
    transition: all .2s ease;
  }
  .media,
  .mb-4 {
    background-color: #ffffff66;
    ;
    padding: 15px;
    border-radius: 10px;
  }

  .media:hover,
  .mb-4:hover {
    background-color: white;
  }

  .widgets {
    display: none;
  }

  .autor {
    /* font-style: italic; */
  }

  .fecha {
    font-style: italic;
  }
  .contenido{
    font-size: 15px;
  }
  .btnverrespuestas{
    color: blue !important;
    font-weight: bold;
    border-right: 1px solid #ccc;
    padding: 0.25ex 1ex;
  }
  .btnresponder,.btneditarforo{
    color: blue !important;
  }
  .pnlforo{
    -webkit-box-shadow: 10px 9px 6px -7px rgba(91,126,194,1);
    -moz-box-shadow: 10px 9px 6px -7px rgba(91,126,194,1);
    box-shadow: 10px 9px 6px -7px rgba(91,126,194,1);
    border: 1px solid #e7e1e1; 
    padding: 1ex; 
    border-radius: 1ex; 
    margin-bottom: 1ex;
  }
  .pnlforo .pnlforo, .pnlforo .btnresponder{
    padding-left: 1ex;
  }
  .misrespuestas{
    padding-right: 1px;
  }
  .existenota .btneditarforo, .existenota .btnresponder{
    display: none;
  }

</style>
<div class="clearfix"><br></div>
<div class="container-fluid">
    <div class="paris-navbar-container row">
        <div class="col-md-12 col-sm-12">    
            <div class="x_panel paris-navbar-panel">      
                <div class="x_content paris-navbar">        
                    <div class="col-md-8 paris-left">           
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" onclick="history.back()" >
                            <i class="fa fa-reply"></i> Volver</button>          
                            <label class="paris-divisor"><i class="fa fa-chevron-right"></i></label>
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" >Foro</button>
                    </div>            
                </div>
            </div>
        </div>
    </div>


    <div class="row">
      <div class="col-lg-1 col-sm-12"></div>
      <div id="post" class="col-lg-10 col-sm-12">
            <h1 class="mt-4"><?php echo ucfirst($this->recurso["titulo"]); ?></h1>
            <p class="lead">
              Publicado por
              <a class="autor" href="#"><i><b><?php echo $this->empresa["nombre"]." <br><strong>Docente : </strong><small>".$this->recurso["struser"]."</small>"; ?></b></i></a>
            </p>
            <hr>            
              <div class="contenido">
                <?=htmlspecialchars_decode($this->recurso["descripcion"]) ?>
                <?php if(!empty($this->tema)){ ?>
                 <br><strong>Tema : </strong> <?php echo $this->tema;?> 
                <?php }?>
              </div>
              <p class="fecha">Publicado el <?php echo $this->recurso['fecha_creacion']; ?></p>
            <hr> 
            <div id="lista_comentarios">
              <?php 
              $hay=false;
               $hayperosinrespuesta=false;
              if($this->forosamigos){
                foreach ($this->forosamigos as $key => $value){ 
                 // var_dump($value);
                  $next=false;                 
                  if($value["idalumno"]==$this->user["idpersona"]){
                    $next=true;
                    $hay=true;
                    if(empty($value["respuestas"]) && empty($value["fecha_respuesta"])){
                      $hayperosinrespuesta=true;
                    }
                    //var_dump($hay);
                  }else{
                    if(empty($value["respuestas"]) && empty($value["fecha_respuesta"])){
                      continue;
                    }
                  }
                 ?>
                  <div class="col-md-12 pnlforo pnlforoini <?php echo !empty($value["nota"])?'existenota':'';?> " id="pnlforo<?php echo $value["idrecursoasignacion"]; ?>" idrecursoasignacion="<?php echo $value["idrecursoasignacion"] ?>" style="display:<?php echo empty($value["respuestas"])&&empty($value["fecha_respuesta"])?'none':'';  ?>">
                    <div class="row">
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <p class="lead"><a class="autor" href="#"><i><b><strong>Alumno : </strong><small class="stralumno"><?php echo $value['stralumno']; ?></small></b></i></a></p>  
                      </div>
                      <div class="col-md-3 col-sm-5 col-xs-12 text-right respuestaeditar">
                      <?php if(!empty($value["nota"])){ 
                        $notaBase=($value['nota']*$this->formulaNota["calificacion_total"])/$value["nota_base"];?>                        
                          <h5>Nota : <strong><?php echo $notaBase; ?></strong></h5>                        
                      <?php }else{ if($hay==true){ ?>
                        <h5><a href="#" class="btneditarforo" title="Editar"><i class="fa fa-pencil"></i></a></h5>
                      <?php }} ?>
                      </div>
                    </div>
                    <div class="row respuestacontenido">
                      <div class="col-md-12 contenido" id="contenido<?php echo $value["idrecursoasignacion"]; ?>">
                        <?php echo $value["respuestas"]; ?>                        
                      </div>
                    </div>
                    <hr>
                    <div class="row respuestafecha">                      
                      <div class="col-md-6 text-left">
                        <p class="fecha" >Publicado el <span id="fecha<?php echo $value["idrecursoasignacion"]; ?>"><?php echo $value['fecha_respuesta']; ?></span></p>
                      </div>
                      <div class="col-md-6 text-right">
                        <a href="#" class="btnverrespuestas"><span><?php echo JrTexto::_('Ver Respuestas');?></span> ( <strong class="numero_mensaje"><?php echo $value["numero_mensaje"]; ?></strong> ) </a>
                        <a href="#" class="btnresponder"><?php echo JrTexto::_('Responder');?></a>
                      </div>
                    </div>
                    <div class="row pnlrespuestas">
                      <div class="col-md-12 misrespuestas"></div>
                    </div>
                  </div>                   
                 <?php } }  ?>
            </div>

            <div id="respuestaclone" style="display: none">
               <div class="col-md-12 pnlforo">
                    <div class="row respuestadatos">
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <p class="lead"><a class="autor" href="#"><i><b><strong>Alumno : </strong><small class="stralumno"><?php echo $this->user['nombre_full']; ?></small></b></i></a></p>  
                      </div>
                      <div class="col-md-3 col-sm-5 col-xs-12 text-right respuestaeditar">                      
                        <h5><a href="#" class="btneditarforo" idrecursoasignacion="<?php echo$this->idrecursoasignacion; ?>" title="Editar"><i class="fa fa-pencil"></i></a></h5>                     
                      </div>
                    </div>
                    <div class="row respuestacontenido">
                      <div class="col-md-12 contenido">                        
                      </div>
                    </div>
                    <hr>
                    <div class="row respuestafecha">                      
                        <div class="col-md-6 text-left">
                          <p class="fecha">Publicado el <span><?php echo date('Y-m-d'); ?></span></p>
                        </div>
                        <div class="col-md-6 text-right">
                          <a href="#" class="btnverrespuestas"><span><?php echo JrTexto::_('Ver Respuestas');?></span> ( <strong class="numero_mensaje">0</strong> ) </a>
                          <a href="#" class="btnresponder"><?php echo JrTexto::_('Responder');?></a>
                        </div>
                    </div>
                    <div class="row pnlrespuestas">
                      <div class="col-md-12 misrespuestas"></div>
                    </div>
                </div> 
            </div>    

            <!-- Comments Form -->
            <div class="card my-4 frmcomentario_foro" style="<?php  echo ($hay==true&&$hayperosinrespuesta==false?'display: none;':'') ?>" idnuevo="<?php echo $this->idrecursoasignacion; ?>">
              <h5 class="card-header">Deja un comentario: <small><?php echo $this->user["nombre_full"];?></small></h5>
              <div class="card-body">
                <form id="frmGuardarcomentario">                  
                  <input type="hidden" name="idrecursoasignacion" id="idrecursoasignacion" value="<?php echo $this->idrecursoasignacion; ?>">
                  <input type="hidden" name="esrespuesta" id="esrespuesta" value="no">
                  <input type="hidden" name="idpadre" id="idpadre" value="">
                  <input type="hidden" name="idrecursocomentario" id="idrecursocomentario" value="">                  
                  <div class="form-group">
                    <textarea name="contenido_comentario" id="contenido_comentario<?php echo $idgui_; ?>" required="required" class="form-control" rows="3">Escribe tu comentario</textarea>
                    <small style="color: #897d7d; float: right;">Puedes volver a editar tu comentario hasta que el docente lo evalué</small>
                  </div>
                  <button  class="btncancel btn btn-secondary" style="display: none;"><i class="fa fa-refresh"></i> Cancelar</button>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Enviar</button>
                </form>
              </div>
            </div>
            <!-- Comment with nested comments -->    

          </div>
    </div>
</div>
<script type="text/javascript">
  $(function(){

    tinymce.init({
      relative_urls : false,
      remove_script_host : false,
      convert_urls : true,
      convert_newlines_to_brs : true,
      menubar: false,
      //statusbar: false,
      verify_html : false,
      content_css : [_sysUrlBase_+'/static/tema/css/bootstrap.min.css'],
      selector: '#contenido_comentario<?php echo $idgui_; ?>',
      height: 250,
      paste_auto_cleanup_on_paste : true,
      paste_preprocess : function(pl, o) {
          var html='<div>'+o.content+'</div>';
          var txt =$(html).text();
          o.content = txt;
          },
      paste_postprocess : function(pl, o) {
          o.node.innerHTML = o.node.innerHTML;
          o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
          },
      plugins:[" textcolor paste lists advlist link image | media" ],  // chingoinput chingoimage chingoaudio chingovideo
      toolbar: ' | undo redo  |  removeformat | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor fontsizeselect | link image media', //chingoinput chingoimage chingoaudio chingovideo,
      advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",
      setup: function (editor){
          editor.on('change', function () {
              tinymce.triggerSave();
          });
      }  
    });


    $('#frmGuardarcomentario').on('click','.btncancel',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
      $('#lista_comentarios').show();
      $('.frmcomentario_foro').hide();
      $(this).hide();
    }).on('submit',function(ev){
      ev.preventDefault();
      var $soyelfrm=$(this);
      Swal.fire({
          // title: 'Cargando...',
          allowOutsideClick: false,
          allowEscapeKey: false,
          onBeforeOpen: () => {
              Swal.showLoading();
          },
      })
      //console.log($(this).attr('id'));
      //let frmid=document.getElementById($(this).attr('id'));
      //var formData = new FormData(frmid);
      let idrecursoasignacion_=$(this).find('#idrecursoasignacion').val()||'';
      let commentario=$(this).find('#contenido_comentario<?php echo $idgui_; ?>').val();
      let esrespuesta=$(this).find('#esrespuesta').val()||'no';
      let idrecursocomentario=$(this).find('#idrecursocomentario').val()||'';
      let idpadre=$(this).find('#idpadre').val()||'';
      let datafrm={
        'comentario':commentario,
        'idrecursoasignacion':idrecursoasignacion_,
        'esrespuesta':esrespuesta,
        'idrecursocomentario':idrecursocomentario,
        'idpadre':idpadre,
      }
      $('#lista_comentarios').show();
      $.ajax({
        url: _sysUrlBase_+'json/recursos_colaborativosasignacion/guardar_respuesta/',
        type: "POST",
        data: datafrm,
        dataType: "json",
        success: function(rs){
          Swal.close();
          if(rs.code==200){
            Swal.fire({
              type: 'success',//warning (!)//success
              icon: 'success',//warning (!)//success             
              text: rs.msj,
              //footer: config.footer,
              allowOutsideClick: false,
              closeOnClickOutside: false
            })


            let $panelpadre=$('#lista_comentarios');
            let mycommet='';
            if(esrespuesta=='no'){
              mycommet = $panelpadre.children('#pnlforo'+idrecursoasignacion_);
              //console.log(mycommet,idrecursoasignacion_);
              if(idrecursoasignacion_=='' && mycommet.length==0){ // viene desde nuevo
                mycommet=$('#respuestaclone').clone();
                mycommet.find('.fecha').children('span').text(rs.fecha);
                mycommet.find(".fecha").attr('id','fecha'+idrecursoasignacion);
                mycommet.find('.contenido').attr('id','contenido'+idrecursoasignacion_).html(commentario);
                mycommet.find('.btneditarforo').attr('idrecursoasignacion',idrecursoasignacion_);
                mycommet.find('.pnlforo').attr('id','pnlforo'+idrecursoasignacion_).attr('idrecursoasignacion',idrecursoasignacion_);
                $panelpadre.show();
                $panelpadre.append(mycommet.html());
              }else{ // viene desde editar
                mycommet.children('.respuestafecha').find('.fecha').children('span').text(rs.fecha);
                mycommet.children('.respuestacontenido').children(".contenido").html(commentario);
                mycommet.show();
              }
            }else{ // es una respuesta
              let inforespuesta=rs.respuesta;
              mycommet = $('#pnlrespuesta'+inforespuesta.idrecursocomentario)
              if(idrecursocomentario=='' && mycommet.length==0){ // es una respuesta nueva
                mycommet=$('#respuestaclone').clone();
                mycommet.find('.fecha').children('span').text(rs.fecha);
                mycommet.find(".fecha").attr('id','fecha'+idrecursoasignacion_);
                mycommet.find('.contenido').attr('id','contenido'+idrecursoasignacion_).html(commentario||'');
                mycommet.find('.btneditarforo').attr('idrecursoasignacion',idrecursoasignacion_);
                mycommet.find('.pnlforo').attr('id','pnlrespuesta'+inforespuesta.idrecursocomentario);
                mycommet.find('.pnlforo').attr('idrecursoasignacion',idrecursoasignacion_);
                mycommet.find('.pnlforo').attr('idrecursocomentario',inforespuesta.idrecursocomentario);
                mycommet.find('.pnlforo').addClass('esrespuesta');

                mycommet.find('.pnlforo').attr('idpadre',inforespuesta.idpadre);
                $panelpadre=$('#pnlforo'+idrecursoasignacion_);
                $panelpadre.show();
                if(inforespuesta.idpadre!='null' && inforespuesta.idpadre!=''){
                  if($('#pnlrespuesta'+inforespuesta.idpadre).length){
                    $panelpadre=$('#pnlrespuesta'+inforespuesta.idpadre);
                  }
                }
                //$panelpadre.children('.pnlrespuestas').children('.misrespuestas').append(mycommet.html());
                //let n=parseInt($panelpadre.children('.respuestafecha').find('.numero_mensaje').text())+1;                
               // $panelpadre.children('.respuestafecha').find('.numero_mensaje').text(n);
                var haypadre=1;
                let i=0;
                do{
                  let n=parseInt($panelpadre.children('.respuestafecha').find('.numero_mensaje').text())+1;                
                  $panelpadre.children('.respuestafecha').find('.numero_mensaje').text(n);
                  i++;
                  if($panelpadre.length==0 || $panelpadre.hasClass('pnlforoini')) haypadre=0;
                  $panelpadre=$panelpadre.parent().closest('.pnlforo');
                }while(haypadre==1);
             }else{ // editar una respuesta 
                mycommet.children('.respuestacontenido').children(".contenido").html(commentario);
                mycommet.children('.respuestafecha').find('.fecha').children('span').text(rs.fecha);
                mycommet.show();
              }
            }
          $('.frmcomentario_foro').hide();
          }else{
            Swal.fire({
              type: 'error',//warning (!)//success
                icon: 'error',//warning (!)//success
                title: 'Oops...',
                text: rs.msj,
                //footer: config.footer,
                allowOutsideClick: false,
                closeOnClickOutside: false
            })
          }
        },
        error: function() {
          Swal.close();
          console.log('Error');
        },
      });
      return false;
    })

    $('#lista_comentarios').on('click','.btneditarforo',function(ev){
        $('.frmcomentario_foro').show();
        $('.frmcomentario_foro').find('.btncancel').show();
        let pnlforo=$(this).closest('.pnlforo');
        //pnlforo.hide();
        Swal.fire({
          // title: 'Cargando...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => { Swal.showLoading(); },
        })
       
        $frmcom=$('.frmcomentario_foro');
        let idasignacion=pnlforo.attr('idrecursoasignacion')||-1;
        $('#lista_comentarios').hide();

        var frm = new FormData();
        frm.append("idrecursoasignacion",idasignacion);
        frm.append("tipo",'foro');
        frm.append("sqlget",'1');
        let url=_sysUrlBase_+'json/recursos_colaborativosasignacion/';     
        if(pnlforo.hasClass('esrespuesta')){
          frm.append("idpadre",pnlforo.attr('idpadre')||'');
          frm.append("idrecursocomentario",pnlforo.attr('idrecursocomentario')||'');
          url=_sysUrlBase_+"json/recursos_colabasig_comentario/";
        }
        fetch(url,{method:'POST',body:frm})
        .then(response => response.json()).then(rs=>{
          Swal.close();        
            if(rs.code==200){
              rs=rs.data;
              if(pnlforo.hasClass('esrespuesta')){
                 $frmcom.find('#idrecursoasignacion').val(rs.idasignacion);
                 tinymce.get("contenido_comentario<?php echo $idgui_; ?>").setContent(rs.comentario||'');
                 $frmcom.find('input#idpadre').val(rs.idpadre||'');
                 $frmcom.find('input#idrecursocomentario').val(rs.idrecursocomentario||'');
                 $frmcom.find('input#esrespuesta').val('si');
              }else{
                $frmcom.find('#idrecursoasignacion').val(idasignacion);
                $frmcom.find('input#idrecursocomentario').val('');
                $frmcom.find('input#idpadre').val('');
                tinymce.get("contenido_comentario<?php echo $idgui_; ?>").setContent(rs.respuestas||'');
                $frmcom.find('input#esrespuesta').val('no');
              }
               //$frmcom.find('#').val(notaobtenida);
              // $frmcom.find('.idrecursocomentario').attr('nota',notaobtenida);
              // $frmcom.find('.calificarrespuesta_foro').attr('nota',notaobtenida);
              // $frmcom.find('.calificarrespuesta_foro').attr('nota',notaobtenida);
            }else{
              Swal.fire({
                type: 'error',//warning (!)//success
                  icon: 'error',//warning (!)//success
                  title: 'Oops...',
                  text: rs.msj,
                  //footer: config.footer,
                  allowOutsideClick: false,
                  closeOnClickOutside: false
              })
            }
        }).catch(e=>{        
          Swal.fire({
            type: 'error',//warning (!)//success
              icon: 'error',//warning (!)//success
              title: 'Oops...',
              text: 'Error',
              //footer: config.footer,
              allowOutsideClick: false,
              closeOnClickOutside: false
          })
        })
    }).on('click','.btnresponder',function(ev){
      $('#lista_comentarios').hide();
      let frm=$('.frmcomentario_foro');
      let pnlforo=$(this).closest('.pnlforo');
      frm.find('input#idrecursoasignacion').val(pnlforo.attr('idrecursoasignacion'));
      frm.find('input#esrespuesta').val('si');
      if(pnlforo.hasClass('esrespuesta')){
        frm.find('input#idpadre').val(pnlforo.attr('idrecursocomentario')||''); 
        frm.find('input#idrecursocomentario').val(pnlforo.attr(''));      
      }
      //frm.find('input#idrecursoasignacion').val();
      tinymce.get("contenido_comentario<?php echo $idgui_; ?>").setContent('');
      frm.show();
    }).on('click','.btnverrespuestas',function(ev){
      let pnlforo=$(this).closest('.pnlforo');
      let idrecursoasignacion=pnlforo.attr('idrecursoasignacion');
      let idrecursocomentario=pnlforo.attr('idrecursocomentario')||'';
      let idpadre=pnlforo.attr('idpadre')||'';
      if($(this).hasClass('ocultar')){
        $(this).children('span').text('<?php echo JrTexto::_('Mostrar Mensajes') ?>');
        $(this).removeClass('ocultar');
        $(this).addClass('yacargo');
        pnlforo.children('.pnlrespuestas').hide();
      }else{
        $(this).addClass('ocultar');
        $(this).children('span').text('<?php echo JrTexto::_('Ocular Mensajes') ?>');
        pnlforo.children('.pnlrespuestas').show();
      }
      if($(this).hasClass('yacargo')) return false;
      if(parseInt($(this).children('.numero_mensaje').text())==0) return false;    


      Swal.fire({
        // title: 'Cargando...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        onBeforeOpen: () => { Swal.showLoading(); },
      })

      //let notaobtenida=result.value;
      var frm = new FormData();
      //frm.append("nota",notaobtenida);
      frm.append("idrecursoasignacion",idrecursoasignacion);
      if(pnlforo.hasClass('esrespuesta')) {
        frm.append("idrecursocomentario",'');
        frm.append("idpadre",idrecursocomentario);        
      }else{
        frm.append("idrecursocomentario",idrecursocomentario);
        frm.append("idpadre",'null');
      }
     
      fetch(_sysUrlBase_+'json/recursos_colabasig_comentario/',{method:'POST',body:frm})
        .then(response => response.json()).then(rs=>{
          Swal.close();       
          if(rs.code==200){ 
              let mycommet=$('#respuestaclone').clone();
              let iduser='<?php echo $this->user["idpersona"]; ?>';
            $.each(rs.data,function(i,d){
              if(iduser!=d.idalumno){
                mycommet.find('.respuestaeditar').hide();
              }
              mycommet.find(".fecha").attr('id','fecha'+d.idasignacion).text('Publicado el '+d.fecha);
              mycommet.find('.contenido').attr('id','contenido'+d.idasignacion).html(d.comentario||'');
              mycommet.find('.btneditarforo').attr('idrecursoasignacion',d.idasignacion);
              mycommet.find('.pnlforo').attr('id','pnlrespuesta'+d.idrecursocomentario);
              mycommet.find('.pnlforo').attr('idrecursoasignacion',d.idasignacion);
              mycommet.find('.pnlforo').attr('idrecursocomentario',d.idrecursocomentario);
              mycommet.find('.pnlforo').attr('idpadre',d.idpadre||'');
              mycommet.find('.pnlforo').addClass('esrespuesta');
              mycommet.find(".stralumno").text(d.stralumno||'');
              mycommet.find(".numero_mensaje").text(d.numero_mensaje||0);
              pnlforo.children('.pnlrespuestas').children('.misrespuestas').append(mycommet.html());
            })          
          }else{
            Swal.fire({
              type: 'error',//warning (!)//success
                icon: 'error',//warning (!)//success
                title: 'Oops...',
                text: rs.msj,
                //footer: config.footer,
                allowOutsideClick: false,
                closeOnClickOutside: false
            })
          }
        }).catch(e=>{        
          console.log(e);  
          Swal.fire({
            type: 'error',//warning (!)//success
              icon: 'error',//warning (!)//success
              title: 'Oops...',
              text: 'error',
              //footer: config.footer,
              allowOutsideClick: false,
              closeOnClickOutside: false
          })
        })
    })
  });
</script>