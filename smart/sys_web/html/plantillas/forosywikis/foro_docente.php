<?php
$usuarioAct = NegSesion::getUsuario();
$rol = $usuarioAct['idrol'];
$idgui_=date('YmdHis');
?>
<style type="text/css">
.x_panel {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
    padding: 10px 17px;
    display: inline-block;
    background: #fff;
    border: 1px solid #E6E9ED;
    -moz-column-break-inside: avoid;
    column-break-inside: avoid;
    opacity: 1;
    transition: all .2s ease;
  }
  .media,
  .mb-4 {
    background-color: #ffffff66;
    ;
    padding: 15px;
    border-radius: 10px;
  }

  .media:hover,
  .mb-4:hover {
    background-color: white;
  }

  .widgets {
    display: none;
  }

  .autor {
    /* font-style: italic; */
  }

  .fecha {
    font-style: italic;
  }
  .contenido{
    font-size: 15px;
  }
  .btnverrespuestas{
    color: blue !important;
    font-weight: bold;
    border-right: 1px solid #ccc;
    padding: 0.25ex 1ex;
  }
  .btnresponder,.btneditarforo{
    color: blue !important;
  }
  .pnlforo{
    -webkit-box-shadow: 10px 9px 6px -7px rgba(91,126,194,1);
    -moz-box-shadow: 10px 9px 6px -7px rgba(91,126,194,1);
    box-shadow: 10px 9px 6px -7px rgba(91,126,194,1);
    border: 1px solid #e7e1e1; 
    padding: 1ex; 
    border-radius: 1ex; 
    margin-bottom: 1ex;
  }
  .pnlforo .pnlforo, .pnlforo .btnresponder{
    padding-left: 1ex;
  }
  .misrespuestas{
    padding-right: 1px;
  }
  .existenota .btneditarforo, .existenota .btnresponder{
    display: none;
  }
  .respuestaeditar h6{
    cursor: pointer;
  }
</style>
<div class="clearfix"><br></div>
<div class="container-fluid">
    <div class="paris-navbar-container row">
        <div class="col-md-12 col-sm-12">    
            <div class="x_panel paris-navbar-panel">      
                <div class="x_content paris-navbar">        
                    <div class="col-md-8 paris-left">           
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" onclick="history.back()" >
                            <i class="fa fa-reply"></i> Volver</button>          
                            <label class="paris-divisor"><i class="fa fa-chevron-right"></i></label>
                        <button type="button" class="paris-item btn btn-default btn-sm " style="border:0;" >Foro</button>
                    </div>            
                </div>
            </div>
        </div>
    </div>


    <div class="row">
      <div class="col-lg-1 col-sm-12"></div>
      <div id="post" class="col-lg-10 col-sm-12">
            <h1 class="mt-4"><?php echo ucfirst($this->recurso["titulo"]); ?></h1>
            <p class="lead">
              Publicado por
              <a class="autor" href="#"><i><b><?php echo $this->empresa["nombre"]." <br><strong>Docente : </strong><small>".$this->recurso["struser"]."</small>"; ?></b></i></a>
            </p>
            <hr>            
              <div class="contenido">
                <?=htmlspecialchars_decode($this->recurso["descripcion"]) ?>
                <?php if(!empty($this->tema)){ ?>
                 <br><strong>Tema : </strong> <?php echo $this->tema;?> 
                <?php }?>                
              </div>
              <p class="fecha">Publicado el <?php echo $this->recurso['fecha_creacion']; ?></p> <hr>
              <div id="lista_comentarios">
            <?php $nohay=true; if(!empty($this->respuestas))
            foreach ($this->respuestas as $k => $r){ 
                if(!empty($this->respuestas["tema"])) { ?>
                 <br><strong>Tema : </strong> <?php echo $this->tema;?> 
                <?php } ?>
                
                  <?php                   
                  if(!empty($r["respuestas"])){
                    foreach ($r["respuestas"] as $key => $value) { if(!empty($value["respuestas"])){ $nohay=false; ?>

                        <div class="col-md-12 pnlforo pnlforoini <?php echo !empty($value["nota"])?'existenota':'';?> " id="pnlforo<?php echo $value["idrecursoasignacion"]; ?>" idrecursoasignacion="<?php echo $value["idrecursoasignacion"] ?>">
                          <div class="row">
                            <div class="col-md-9 col-sm-7 col-xs-12">
                              <p class="lead"><a class="autor" href="#"><i><b><strong>Alumno : </strong><small class="stralumno"><?php echo $value['stralumno']; ?></small></b></i></a></p>  
                            </div>

                            <div class="col-md-3 col-sm-5 col-xs-12 text-right respuestaeditar infopnl" idasignacion="<?php echo $value["idrecursoasignacion"] ?>" nota_maxima="<?php echo @$this->formulaNota["calificacion_total"]; ?>">
                            <?php if(!empty($value["nota"])){ 
                              $notaBase=($value['nota']*$this->formulaNota["calificacion_total"])/$value["nota_base"];?>                        
                                <h5>Nota : <strong class="calificarrespuesta_foro aquinota" nota="<?php echo $notaBase;?>"><?php echo $notaBase; ?></strong></h5>
                            <?php }else{ ?>
                              <h6><span style="display: none">Nota</span> <span class="calificarrespuesta_foro aquinota" nota="0">(Calificar Foro)</span></h6>
                            <?php } ?>
                            </div>
                          </div> 
                          <div class="row respuestacontenido">
                            <div class="col-md-12 contenido" id="contenido<?php echo $value["idrecursoasignacion"]; ?>">
                              <?php echo $value["respuestas"]; ?>                        
                            </div>
                          </div>
                          <hr>
                          <div class="row respuestafecha">                      
                            <div class="col-md-6 text-left">
                              <p class="fecha" >Publicado el <span id="fecha<?php echo $value["idrecursoasignacion"]; ?>"><?php echo $value['fecha_respuesta']; ?></span></p>
                            </div>
                            <div class="col-md-6 text-right">
                              <a href="#" class="btnverrespuestas"><span><?php echo JrTexto::_('Ver Respuestas');?></span> ( <strong class="numero_mensaje"><?php echo $value["numero_mensaje"]; ?></strong> ) </a>
                              <!--a href="#" class="btnresponder"><?php //echo JrTexto::_('Responder');?></a-->
                            </div>
                          </div>
                          <div class="row pnlrespuestas">
                            <div class="col-md-12 misrespuestas"></div>
                          </div>                          
                       </div>
                     <?php } } } ?>
                </div>

              <div id="respuestaclone" style="display: none">
               <div class="col-md-12 pnlforo">
                    <div class="row respuestadatos">
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <p class="lead"><a class="autor" href="#"><i><b><strong>Alumno : </strong><small class="stralumno"><?php echo $this->user['nombre_full']; ?></small></b></i></a></p>  
                      </div>
                      <div class="col-md-3 col-sm-5 col-xs-12 text-right respuestaeditar">                      
                        <h5><a href="#" class="btneditarforo" idrecursoasignacion="<?php echo$this->idrecursoasignacion; ?>" title="Editar"><i class="fa fa-pencil"></i></a></h5>                     
                      </div>
                    </div>
                    <div class="row respuestacontenido">
                      <div class="col-md-12 contenido">                        
                      </div>
                    </div>
                    <hr>
                    <div class="row respuestafecha">                      
                        <div class="col-md-6 text-left">
                          <p class="fecha">Publicado el <span><?php echo date('Y-m-d'); ?></span></p>
                        </div>
                        <div class="col-md-6 text-right">
                          <a href="#" class="btnverrespuestas"><span><?php echo JrTexto::_('Ver Respuestas');?></span> ( <strong class="numero_mensaje">0</strong> ) </a>
                          <a href="#" class="btnresponder"><?php echo JrTexto::_('Responder');?></a>
                        </div>
                    </div>
                    <div class="row pnlrespuestas">
                      <div class="col-md-12 misrespuestas"></div>
                    </div>
                </div> 
            </div>



            <?php } if($nohay){?>
              <div class="container">
                <div class="row">
                  <div class="col-md-12 pnlerror text-center">
                    <h5>
                      <!--strong><?php echo JrTexto::_('Information') ?></strong--> 
                      <?php echo JrTexto::_('There are no comments because no student has responded yet'); ?>.
                     </h5>                    
                  </div>                  
                </div>
              </div>
            <?php } ?>           
          </div>
    </div>
</div>
<script type="text/javascript">
  $(function(){ 
    $('.calificarrespuesta_foro').on('click',function(ev){      
      ev.preventDefault();

      let nota=$(this).attr('nota')||0;
      var infopnl=$(this).closest('.infopnl');
      var idasignacion=infopnl.attr('idasignacion')||'';
      let nota_maxima=infopnl.attr('nota_maxima')||'20';
      Swal.fire({
        title: 'Registra nota hasta '+nota_maxima+' como maximo',
        input: 'number',
        inputValue: nota,
        inputAttributes: {
          autocapitalize: 'off',
          min:0,
          max:nota_maxima,
        },
        allowOutsideClick: false,
        allowEscapeKey: false,
        showCancelButton: true,
        confirmButtonText: 'Guardar',
        showLoaderOnConfirm: true,
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {       
        if(result.dismiss=="cancel") return false;
        if(result.value==''|| result.value=='0') return false;
        Swal.fire({
          // title: 'Cargando...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => { Swal.showLoading(); },
        })

        let notaobtenida=result.value;
        var frm = new FormData();
        frm.append("nota",notaobtenida);
        frm.append("idrecursoasignacion",idasignacion);
        frm.append("nota_base",nota_maxima);
        frm.append("tipo",'foro');
     
        fetch(_sysUrlBase_+'json/recursos_colaborativosasignacion/guardar_nota/',{method:'POST',body:frm})
        .then(response => response.json()).then(rs=>{        
            if(rs.code==200){
              Swal.fire({
                type: 'success',//warning (!)//success
                icon: 'success',//warning (!)//success             
                text: rs.msj,
                //footer: config.footer,
                allowOutsideClick: false,
                closeOnClickOutside: false
              })
              infopnl.find('.calificarrespuesta_foro').attr('nota',notaobtenida);
              infopnl.find('.calificarrespuesta_foro.aquinota').text(notaobtenida);
              infopnl.find('.calificarrespuesta_foro.aquinota').siblings('span').show();
            }else{
              Swal.fire({
                type: 'error',//warning (!)//success
                  icon: 'error',//warning (!)//success
                  title: 'Oops...',
                  text: rs.msj,
                  //footer: config.footer,
                  allowOutsideClick: false,
                  closeOnClickOutside: false
              })
            }
        }).catch(e=>{        
          Swal.fire({
            type: 'error',//warning (!)//success
              icon: 'error',//warning (!)//success
              title: 'Oops...',
              text: 'error',
              //footer: config.footer,
              allowOutsideClick: false,
              closeOnClickOutside: false
          })
        })
      })      
      return false;
    })

    $('#lista_comentarios').on('click','.btnverrespuestas',function(ev){
      let pnlforo=$(this).closest('.pnlforo');
      let idrecursoasignacion=pnlforo.attr('idrecursoasignacion');
      let idrecursocomentario=pnlforo.attr('idrecursocomentario')||'';
      let idpadre=pnlforo.attr('idpadre')||'';
      if($(this).hasClass('ocultar')){
        $(this).children('span').text('<?php echo JrTexto::_('Mostrar Mensajes') ?>');
        $(this).removeClass('ocultar');
        $(this).addClass('yacargo');
        pnlforo.children('.pnlrespuestas').hide();
      }else{
        $(this).addClass('ocultar');
        $(this).children('span').text('<?php echo JrTexto::_('Ocular Mensajes') ?>');
        pnlforo.children('.pnlrespuestas').show();
      }
      if($(this).hasClass('yacargo')) return false;
      if(parseInt($(this).children('.numero_mensaje').text())==0) return false;    


      Swal.fire({
        // title: 'Cargando...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        onBeforeOpen: () => { Swal.showLoading(); },
      })

      //let notaobtenida=result.value;
      var frm = new FormData();
      //frm.append("nota",notaobtenida);
      frm.append("idrecursoasignacion",idrecursoasignacion);
      if(pnlforo.hasClass('esrespuesta')) {
        frm.append("idrecursocomentario",'');
        frm.append("idpadre",idrecursocomentario);        
      }else{
        frm.append("idrecursocomentario",idrecursocomentario);
        frm.append("idpadre",'null');
      }
     
      fetch(_sysUrlBase_+'json/recursos_colabasig_comentario/',{method:'POST',body:frm})
        .then(response => response.json()).then(rs=>{
          Swal.close();       
          if(rs.code==200){ 
              let mycommet=$('#respuestaclone').clone();
              let iduser='<?php echo $this->user["idpersona"]; ?>';
            $.each(rs.data,function(i,d){
              if(iduser!=d.idalumno){
                mycommet.find('.respuestaeditar').hide();
              }
              mycommet.find(".fecha").attr('id','fecha'+d.idasignacion).text('Publicado el '+d.fecha);
              mycommet.find('.contenido').attr('id','contenido'+d.idasignacion).html(d.comentario||'');
              mycommet.find('.btneditarforo').attr('idrecursoasignacion',d.idasignacion);
              mycommet.find('.pnlforo').attr('id','pnlrespuesta'+d.idrecursocomentario);
              mycommet.find('.pnlforo').attr('idrecursoasignacion',d.idasignacion);
              mycommet.find('.pnlforo').attr('idrecursocomentario',d.idrecursocomentario);
              mycommet.find('.pnlforo').attr('idpadre',d.idpadre||'');
              mycommet.find('.pnlforo').addClass('esrespuesta');
              mycommet.find(".stralumno").text(d.stralumno||'');
              mycommet.find(".numero_mensaje").text(d.numero_mensaje||0);
              pnlforo.children('.pnlrespuestas').children('.misrespuestas').append(mycommet.html());
            })          
          }else{
            Swal.fire({
              type: 'error',//warning (!)//success
                icon: 'error',//warning (!)//success
                title: 'Oops...',
                text: rs.msj,
                //footer: config.footer,
                allowOutsideClick: false,
                closeOnClickOutside: false
            })
          }
        }).catch(e=>{        
          console.log(e);  
          Swal.fire({
            type: 'error',//warning (!)//success
              icon: 'error',//warning (!)//success
              title: 'Oops...',
              text: 'error',
              //footer: config.footer,
              allowOutsideClick: false,
              closeOnClickOutside: false
          })
        })
    })



  });
</script>