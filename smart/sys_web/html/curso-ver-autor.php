<style type="text/css">	
	.islayout.active{background: transparent;}
	.islayout ._acccont, .controlsdisenio, ._subirfileedit .toolbaredit, ._subirvisoredit .toolbaredit { display: none }
	#layoutver{position: absolute; bottom: -1px;}
	#layoutver span, .savecambios { padding: 1ex 1ex 0.2ex 1ex; border-radius: 10px 10px 0 0; color: #fbfbfb; cursor: pointer;}
	iframe{ border: 0px; width: 100%; height: 100%; }
	iframe{ border: 0px; width: 100%; height: 100%; }
	.addnegrita{ font-weight: bold !important; }
	.addcursiva{ font-style: italic !important; }
	.addsubrayado{ text-decoration: underline !important; }
	#mainpage{ width: 100%; height:100vh;}
	#mainpage .seepage{ width: 100%; height:100%; display: none; }
	#mainpage .seepage.mostrarpagina{display: block;}
	.seepage > ._cont, .widgetcont{margin: 0px;width: 100%;height: 100%; padding: 0px;}
	.seepage, ._cont > ._subirfileedit{ padding: 0px; }
	.btnnavegacionPage{ /*display: none;*/ position: absolute; right: 63px; top:0.5ex;}
</style>
<link rel="stylesheet" type="text/css" id="linkviewen" href="<?php echo $this->documento->getUrlTema();?>/css/autor.css">
<div class="row" id="pagecontroler" style="position:  relative; height:  100vh; ">
	<?php 
	 $html=!empty($this->menushtml["html"])?$this->menushtml["html"]:'noexistearchivo';
	 if(file_exists(RUTA_MEDIA.$html)) require_once(RUTA_MEDIA.$html);
	 ?>
</div>
<script type="text/javascript">
	$(document).ready(function(ev){
		$('#pagecontroler').on('click','.btnircurso',function(ev){
			window.location=window.location;
		}).on('click','.btnirlistado',function(ev){
			redir(_sysUrlBase_);
		}).on('click','.btnirmenu',function(ev){
			$page=$(this).closest('#mainpage').find('.seepage');
			idpage=$page.first().attr('id')||'';			
			if(idpage!='')__mostrarPage(idpage);
		}).on('click','.menucircleitem',function(ev){
			let page=$(this).attr('data-linkid')||'';
			if(page!='')__mostrarPage(page);
		}).on('click','.btnircurso',function(ev){
			window.location=window.location;
		}).on('click','.btnirback',function(ev){
			console.log('test');
			$page=$(this).closest('#mainpage').find('.seepage.mostrarpagina').prev('.seepage');
			idpage=$page.attr('id')||'';			
			if(idpage!='')__mostrarPage(idpage);
		}).on('click','.btnirnext',function(ev){
			console.log('btnirnext')
			$page=$(this).closest('#mainpage').find('.seepage.mostrarpagina').next('.seepage');
			idpage=$page.attr('id')||'';			
			if(idpage!='')__mostrarPage(idpage);
		}).on('click','[data-link]',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			obj=$(this);
			url=_sysUrlBase_+'/../'+obj.attr('data-link');
			donde='pagecontroler';			
			__sysajax({
				url:url,
				type:'html',
				donde:$('#'+donde),		
				callback:function(rs){
					$('#'+donde).html(rs);
					__initPage();
				}
			});
			//console.log(url);

		})
		$( ".btnnavegacionPage" ).draggable();
	});	
</script>