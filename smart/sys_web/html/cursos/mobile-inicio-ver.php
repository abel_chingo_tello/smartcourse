<?php
$idgui = uniqid();
$imgcursodefecto = URL_BASE . 'static/media/cursos/nofoto.jpg';
$fileurl = is_file(RUTA_BASE . @$curso["imagen"]) ? URL_MEDIA . @$curso["imagen"] : $imgcursodefecto;
$bloqueodeexamenes = false;
$notabloqueoexamenes = false;
if (!empty($this->proyecto)) { // solo rol alumno
	if (!empty($this->proyecto["jsonlogin"])) $jsontmp = json_decode($this->proyecto["jsonlogin"], true);
	if (!empty($jsontmp['bloqueodeexamenes']))
		if ($jsontmp['bloqueodeexamenes'] == 'no') $bloqueodeexamenes = false;
		else {
			$bloqueodeexamenes = $jsontmp['bloqueodeexamenes']; // si == todos menos el examen de entrada; sf==solo examen final
			$notabloqueoexamenes = intval($jsontmp['notabloqueoexamenes']);
		}
} ?>
<style type="text/css">
	/* mine */
	/* .carousel.slide,
	.carousel-inner,
	.carousel-item.active,
	#vimeo-multiopt {
		height: 100%;
	} */

	.tab .cont-vimeo-player iframe {
		top: 0;
		bottom: 0;
		left: 0;
		width: 100%;
		height: calc(100vh - 122px);
		border: 0;
	}

	.cont-vimeo-player iframe {
		top: 0;
		bottom: 0;
		left: 0;
		width: 100%;
		height: calc(100vh - 60px);
		border: 0;
	}

	/* mine */
	.content-wrapper {
		max-height: calc(100vh - 60px) !important;
		overflow: auto !important;
	}

	._vercontenido_.disabled {
		opacity: 0.5;
		filter: alpha(opacity=50);
		cursor: no-drop;
	}

	._vercontenido_.disabled a {
		cursor: no-drop;
	}

	._vercontenido_.terminado:after {
		content: "\f00c";
		font-family: FontAwesome;
		font-style: normal;
		font-weight: normal;
		color: #57e678;
		font-size: 15px;
		padding-right: 0.25ex;
		right: 0px;
		position: absolute;
		top: 0px;
	}

	._btnversesion._seleccionado {
		font-style: italic;
	}

	._btnversesion.terminado:after {
		content: "\f00c";
		font-family: FontAwesome;
		font-style: normal;
		font-weight: normal;
		color: #57e678;
		font-size: 20px;
		padding-right: 0.35ex;
		right: 0px;
		position: absolute;
		bottom: 0px;
	}

	.marcopage {
		width: 100% !important;
	}

	.listaSesiones>li>a.active {
		border-bottom: solid;
		background-color: #d5d5d573;
	}

	#previewcurso{
		z-index: 0
	}

	#contentpages {
		/* height: calc(100vh - 120px); */
	}

	#contentpages.imagenfondo {
		background-repeat: no-repeat;
		background-position: center;
		background-size: auto;
	}

	.view {
		position: relative;
		/*box-shadow:1px 1px 2px #e6e6e6;*/
		cursor: default
	}

	.view .mask,
	.view .content {
		position: absolute;
		width: 100%;
		overflow: hidden;
		top: 0;
		left: 0
	}

	.view img {
		display: block;
		position: relative
	}

	.view .tools {
		color: #fff;
		text-align: center;
		position: relative;
		font-size: 17px;
		background: rgba(32, 29, 29, 0.89);
		/*margin:43px 0 0 0*/
	}

	.view .tools i {
		font-size: 1.5em;
		padding-top: 1.3ex;
	}

	.mask.no-caption .tools {
		margin: 90px 0 0 0
	}

	/*.view .tools a{display:inline-block;color:#FFF;font-size:18px;font-weight:400;padding:0 4px}*/
	/*.view p{font-family:Georgia, serif;font-style:italic;font-size:12px;position:relative;color:#fff;padding:10px 20px 20px;text-align:center}*/
	/*.view a.info{display:inline-block;text-decoration:none;padding:7px 14px;background:#000;color:#fff;text-transform:uppercase;box-shadow:0 0 1px #000}*/
	/*.view-first img{transition:all 0.2s linear}*/
	.view-first .mask {
		opacity: 0;
		background-color: rgba(0, 0, 0, 0.5);
		transition: all 0.4s ease-in-out
	}

	.view-first .tools {
		transform: translateY(-100px);
		opacity: 0;
		transition: all 0.2s ease-in-out
	}

	/*.view-first p{transform:translateY(100px);opacity:0;transition:all 0.2s linear}*/
	.view-first:hover img {
		transform: scale(1.1)
	}

	.view-first:hover .mask {
		opacity: 1
	}

	.view-first:hover .tools,
	.view-first:hover p {
		opacity: 1;
		transform: translateY(0px)
	}

	/*.view-first:hover p{transition-delay:0.1s}*/
	._P0 {
		margin: 0px;
		padding: 0px;
	}

	#chat #header-chat {
		background-color: #555;
		color: #20f1f14d;
		padding: 10px;
		text-align: center;
		text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.6);
	}

	#chat #mensajes {
		padding: 10px;
		height: 400px;
		width: 100%;
		overflow: hidden;
		overflow-y: scroll
	}

	#chat #mensajes .mensaje-autor {
		margin-bottom: 20px;
		float: left;
	}

	#chat #mensajes .mensaje-autor img,
	#chat #mensajes .mensaje-amigo img {
		display: inline-block;
		vertical-align: top;
		max-width: 50px;
		max-height: 50px;
	}

	#chat #mensajes .mensaje-autor .contenido {
		background-color: #20f1f14d;
		border-radius: 5px;
		box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);
		display: inline-block;
		font-size: 13px;
		padding: 15px;
		vertical-align: top;
	}

	#chat #mensajes .mensaje-autor .fecha {
		color: #777;
		font-style: italic;
		font-size: 13px;
		text-align: right;
		margin-right: 35px;
		margin-top: 10px;
	}

	#chat #mensajes .mensaje-autor .flecha-izquierda {
		display: inline-block;
		margin-right: -6px;
		margin-top: 10px;
		width: 0;
		height: 0;
		border-top: 0px solid transparent;
		border-bottom: 15px solid transparent;
		border-right: 15px solid #20f1f14d;
	}

	#chat #mensajes .mensaje-amigo {
		margin-bottom: 20px;
		float: right;
	}

	#chat #mensajes .mensaje-amigo .contenido {
		background-color: #3990BF;
		border-radius: 5px;
		color: white;
		display: inline-block;
		font-size: 13px;
		padding: 15px;
		vertical-align: top;
	}

	#chat #mensajes .mensaje-amigo .flecha-derecha {
		display: inline-block;
		margin-left: -6px;
		margin-top: 10px;
		width: 0;
		height: 0;
		border-top: 0px solid transparent;
		border-bottom: 15px solid transparent;
		border-left: 15px solid #3990BF;
	}

	#chat #mensajes .mensaje-amigo img,
	#chat #mensajes .mensaje-autor img {
		border-radius: 5px;
	}

	#chat #mensajes .mensaje-amigo .fecha {
		color: #777;
		font-style: italic;
		font-size: 13px;
		text-align: left;
		margin-top: 10px;
	}

	._vercontenido_ a.disabled {
		cursor: no-drop;
	}

	._vercontenido_ a {
		cursor: pointer;
	}


	::-webkit-scrollbar {
		width: 8px;
	}

	.heartbox,
	.commentbox {
		font-size: 1em;
		border: 1px solid #c3c3c3;
	}

	.heartbox i {
		cursor: pointer;
	}

	.post {
		padding: 5px;
		border: 1px solid #cfcfcf;
		border-radius: 0.5em;
	}

	.isliked {
		color: #e06363;
		font-size: 1.5em;
	}

	.mt-2>ul>li>a {
		display: flex;
		justify-content: center;
		/* align-items: center;  //Esto centra los nùmeros y iconos*/
	}

	.mt-2>.nav-icon {
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.mt-2>ul>li>a>b {
		line-height: 1.15;
		width: 1.6rem;
		text-align: center;
	}

	.content-wrapper {
		max-height: calc(100vh - 60px) !important;
		overflow: auto !important;
	}

	.modalsense {
		font-family: 'Source Sans Pro , Arial';
		font-size: 1.1em;
		padding: 1ex;
		width: 100%;
	}

	.modalsense>div {
		padding: 0.5ex;
	}

	.modalsense strong {
		color: #c12b2b;
	}

	.modalsense b {
		font-size: 1.2em;
		color: #114a7b;
	}

	.modalsenseopen {
		position: absolute;
		left: 1.5em;
		color: #2b8bbb;
		cursor: pointer;
	}
</style>

<link rel="stylesheet" href="<?= URL_RAIZ ?>static/libs/othersLibs/mine/css/mine.css">
<script src="<?= URL_RAIZ ?>static/libs/othersLibs/mine/js/mine.js"></script>
<script src="<?= URL_RAIZ ?>static/libs/othersLibs/mine/js/mine2.js"></script>
<script src="<?= URL_RAIZ ?>static/libs/vimeo/player.js"></script>

<link rel="stylesheet" href="<?= URL_RAIZ ?>static/libs/othersLibs/mine/css/soft-rw-theme-paris.css">
<link rel="stylesheet" href="<?= URL_RAIZ ?>static/libs/othersLibs/foro/clean-blog-plantilla.min.css">

<link rel="stylesheet" href="<?= URL_RAIZ ?>static/libs/othersLibs/conferencias/conferencias.css">
<script src="<?= URL_RAIZ ?>static/libs/othersLibs/conferencias/conferencias.js"></script>
<script src="<?= URL_RAIZ ?>static/libs/othersLibs/fecha-limite-tapr/fecha-limite-tapr.js?v=1"></script>


<link rel="stylesheet" href="<?= URL_RAIZ ?>static/libs/othersLibs/rubrica/mine-rubrica.css">
<script src="<?= URL_RAIZ ?>static/libs/othersLibs/rubrica/mine-rubrica.js"></script>

<link rel="stylesheet" href="<?= URL_RAIZ ?>static/libs/othersLibs/submenu/mine-submenu.css">
<script src="<?= URL_RAIZ ?>static/libs/othersLibs/submenu/mine-submenu.js"></script>

<link rel="stylesheet" href="<?= URL_RAIZ ?>static/libs/othersLibs/crud-curso/mine-crud-curso.css">
<script src="<?= URL_RAIZ ?>static/libs/othersLibs/crud-curso/mine-crud-curso.js"></script>
<script src="<?= URL_RAIZ ?>static/libs/moment/moment.js"></script>

<input type="hidden" id="idpersona__" value="<?php echo $this->usuActivo['idpersona']; ?>">
<input type="hidden" id="urlraiz__" value="<?php echo URL_RAIZ; ?>">
<div class="row">
	<div class="col-md-12 px-0" id="previewcurso">
		<div class="row tabstop" id="bookplantilla">
			<!-- <div class="row marcopage arriba"> -->
			<div class="marcopage arriba">
				<nav id="fq-nav" class="main-header navbar navbar-expand-md navbar-light navbar-white" style="margin-left: 0; width: 100%;">
					<div class="container-fluid">
						<a class="navbar-brand">
						<span class="brand-text font-weight-light" id="tituloSesion"></span>
						</a>
						<button class="navbar-toggler order-1 collapsed tooltipstatic border-0" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" style="position:relative;">
						<span class="asistentetooltip " data-placement="buttom" >
							<span class="navbar-toggler-icon background-points"></span>
						</span>
						<div class="tooltip fade show bs-tooltip-bottom" role="tooltip" id="tooltip576779" x-placement="bottom" style="position: absolute;will-change: transform;top: -20px;left: -8px;transform: translate3d(5px, 51px, 0px);z-index: 999999;"><div class="arrow" style="left: 21px;"></div><div class="tooltip-inner">Inicia Aquí</div></div>
						</button>
						<div class="menuSesion navbar-collapse order-3 collapse" id="navbarCollapse">
							<ul class="listaSesiones navbar-nav ml-auto"></ul>
						</div>
					</div>
				</nav>
				<div class="col-md-12 imagenfondo" id="contentpages"></div>
				<?php
				// if($this->curso_["tipo"] == "2"){
				?>
				<form id="frmAddConsulta">
					<div class="modal fade" id="mdAddConsulta" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Agregar a Consulta</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-md-12">
											<p>Contenido de la Consulta <font color="red">(*)</font>
												<textarea type="text" id="txtcontenidoc" name="txtcontenidoc" class="form-control input-sm" required="" rows="3" style="resize: none;"></textarea>
											</p>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary" aria-hidden="true"><i class="fa fa-save"> </i> Guardar</button>
									<button id="btncerrarc" type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-close"> </i> Cerrar</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div id="foro" class="card collapsed-card" style="margin-bottom: 0;">
					<div class="card-header">
						<h3 class="card-title">Preguntas y Respuestas</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-outline-primary btn-xs" id="btn-ac" data-toggle="modal" data-target="#mdAddConsulta">
								<i class="fas fa-plus-circle"></i> Hacer una Pregunta
							</button>
							<button type="button" class="btn btn-tool btn-collapse" data-card-widget="collapse">
								<i class="fas fa-plus"></i>
							</button>
						</div>
					</div>

					<div class="card-body" style="display: none;">
						<div class="col-md-12">
							<div style="overflow-y: scroll; max-height: 500px;" id="listarConsulta"></div>
						</div>
					</div>
				</div>
				<?php
				// }
				?>
			</div>
		</div>
		<button type="button" id="NextContentButton" class="btn btn-block btn-success" style="display: none;">Siguiente contenido</button>
	</div>
</div>
<div id="esrecursopagado" style="display: none;">
	<div style="text-align: center; font-size: 2em;
    position: absolute;
    width: 100%;
    background-color: white;
    height: 100%;">
		<br><br>
		<i class="fa fa-dollar" style="font-size: 3em"></i><br><br>
		Este recurso es Pagado
	</div>
</div>
<style>
	.social-bar {
		position: absolute;
		right: 0;
		top: 0;
		font-size: 2rem;
		display: flex;
		flex-direction: column;
		align-items: flex-end;
		z-index: 1000;
		background-color: #534d4d;
	}

	.icon {
		color: white !important;
		text-decoration: none;
		padding: .7rem;
		display: flex;
		transition: .5s all;
		width: 52px;
	}

	.icon:first-child {
		border-radius: 1rem 0 0 0;
	}

	.icon:last-child {
		border-radius: 0 0 0 1rem;
	}

	.icon:hover {
		color: white;
		/* padding-right: 4rem; */
		border-radius: 1rem 0 0 1rem;
		box-shadow: .5rem rgba(0, 0, 0, 0.42) 0 0;
	}
</style>
<div id="preloading" style="display: none;">
	<div class="x_panel">
		<div class="x_title">
			<br>
			<center>
				<h2>La página está cargando</h2>
			</center>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br>
			<center>
				<img src="<?php echo URL_BASE . $this->logo_emp; ?>" width="20%" height="20%">
			</center>
			<br>
			<center>
				<i class="fa fa-refresh fa-spin" style="font-size: 60px;"></i>
			</center>
		</div>
	</div>
</div>
<div id="estareaoproyecto" style="display: none">
	<div class="social-bar">
		<i title="Subir Archivo y Enviar Mensajes" class="icon _navbar_ fa fa-pencil-square-o btnsubirfilealumno" data-container="body" data-toggle="popover" data-placement="left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." idcursopestania="" idcursodetalle="" style="cursor: pointer; display: none;" alt="subir Archivo y enviar mensajes"></i>
		<i title="Descargar Material" class="icon _logo_ fa fa-cloud-download btndescargartrabajo" idcursopestania="" idcursodetalle="" style="cursor: pointer;" alt="Descargar Material"></i>
		<i id="rubrica-icon" onclick="modalVer()" title="Ver Rúbrica" class="my-hide icon _logo_ fa fa-table btn-ver-rubrica" idcursopestania="" idcursodetalle="" style="cursor: pointer;"></i>
	</div>
</div>
<div style="display: none;">
	<div id="subirtarea">
		<div class="x_panel">
			<div class="x_title">
				<h2 style="font-size: 22px;"><i class="fa fa-tasks estarea"></i> <small class="estarea"><?php echo ucfirst(JrTexto::_('Tasks')); ?></small> <span class="estareaproyecto" style="display: none;">/</span>
					<i class="fa fa-folder esproyecto"></i> <small class="esproyecto"><?php echo ucfirst(JrTexto::_('Projects')); ?></small></h2>

				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<ul class="nav nav-tabs bar_tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active idtab isa" id="frmsubirarchivo_tab" data-toggle="tab" role="tab" href="#frmsubirarchivo">Subir Archivo</a>
					</li>
					<li class="nav-item">
						<a class="nav-link idtab isa" id="listadomensajes-tab" data-toggle="tab" role="tab" href="#listadomensajes">Mensajes</a>
					</li>
					<li class="nav-item">
						<a class="nav-link idtab isa" id="listadoarchivos-tab" data-toggle="tab" role="tab" href="#listadoarchivos">Archivos subidos</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade show active idtab" id="frmsubirarchivo" role="tabpanel" aria-labelledby="frmsubirarchivo_tab">
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-info" role="alert">
									Click en el tipo de archivo que deseas enviarle a tu docente, escribele un mensaje y enviale dando click en enviar
								</div>
							</div>
							<div class="col-md-12">
								<form class="row" id="frmarchivosmultimedia" method="post" target="" enctype="multipart/form-data">
									<div class="col-md-6 form-group">
										<label><?php echo JrTexto::_("Tipo de Archivo") ?></label>
										<br>
										<div class="btn-group" role="group" aria-label="">
											<button type="button" class="btn btn-secondary btntipofile" title="texto"><i class="fa fa-font"></i></button>
											<button type="button" class="btn btn-secondary btntipofile" title="imagen"><i class="fa fa-photo"></i></button>
											<button type="button" class="btn btn-secondary btntipofile" title="audio"><i class="fa fa-file-audio-o"></i></button>
											<button type="button" class="btn btn-secondary btntipofile" title="video"><i class="fa fa-video"></i></button>
											<button type="button" class="btn btn-secondary btntipofile" title="documento"><i class="fa fa-file-o"></i></button>
											<button type="button" class="btn btn-secondary btntipofile" title="html"><i class="fa fa-html5"></i></button>
											<button type="button" class="btn btn-secondary btntipofile" title="link"><i class="fa fa-link"></i></button>
										</div>
									</div>
									<div class="col-md-6" id="aquitipofile"></div>
									<div class="col-md-12 form-group">
										<label style=""><?php echo JrTexto::_('Mensaje'); ?> <i class="fa fa-comment"></i> </label>
										<textarea id="_mensaje_" class="form-control" rows="4" placeholder="<?php echo  ucfirst(JrTexto::_("Mensaje")) ?>" value="" style="resize: none;"></textarea>
									</div>
								</form>
							</div>
							<div class="col-md-12 text-center">
								<button class="btnguardartarea btn btn-primary "> <i class="fa fa-envelope-o"></i> Enviar Mensaje</button>
							</div>
						</div>
					</div>
					<div class="tab-pane fade idtab mensajeenviados" id="listadomensajes" role="tabpanel" aria-labelledby="listadomensajes-tab">
						<div id="chat">
							<div id="mensajes">
								<h2 class="text-center">No tiene Mensajes enviados</h2>
							</div>
						</div>
						<div id="caja-mensaje">
							<textarea id="_mensaje2_" class="form-control" rows="2" placeholder="<?php echo  ucfirst(JrTexto::_("Mensaje")) ?>" value="" style="resize: none;"></textarea>
							<div class="text-center">
								<button style="margin: 1ex" class="btn btn-primary btnsendmensaje"> Enviar Mensaje → </button>
							</div>
						</div>
					</div>
					<div class="tab-pane table-responsive fade idtab archivosenviados" id="listadoarchivos" role="tabpanel" aria-labelledby="listadoarchivos-tab">
						<table class="table  table-striped">
							<thead>
								<tr>
									<th>Archivo</th>
									<th>Fecha</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="3">Aun no a enviado archivos de este trabajo</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12 text-center" style="padding: 1ex;">
					<button class="salirmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar </button>
				</div>
			</div>
		</div>
	</div>
	<!-- (yp) -->
	<div id="multioptionsvideos">
		<div class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" style="text-align: center;"></div>
			<ol class="carousel-indicators" style="position: relative;"></ol>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" style="height: 50px; margin: auto; background-color: #19a2b8; width: 50px; border-radius: 2.5em; ">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only"><?php echo JrTexto::_('Previous'); ?></span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style="height: 50px; margin: auto; background-color: #19a2b8; width: 50px; border-radius: 2.5em;">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only"><?php echo JrTexto::_('Next'); ?></span>
			</a>
		</div>
	</div>
	<div id="showvideoconferencia" class="my-hide">
		<div id="fix-md" class="row soft-rw  my-center cont-md-conf">
			<div class="cont-conferencia col-sm-12  my-font-all">
				<!-- <div class=""> -->
				<div class="sec-conf col-sm-12 col-md-8 my-mg-center my-card my-shadow conf-head ">
					<i class="fa fa-video-camera" aria-hidden="true"> </i>
					<label id="input-conf-titulo" for="input-conf-titulo"> Tema de la conferencia</label>
				</div>
				<!-- </div> -->
				<div id="cont-conf" class="sec-conf col-sm-12 col-md-8 my-mg-center my-card my-shadow">


					<div class="row col-md-12">
						<?php if ($this->rolActual == 2) { ?>
							<div class="form-group col-sm-12">
								<label for="input-conf-link_mod" class="col-sm-12">Enlace a la conferencia <small><i>(moderador)</i></small>:</label>
								<a class="col-sm-12" target="_blank" id="input-conf-link_mod" href="#"></a>
							</div>
						<?php } ?>
						<div class="form-group col-sm-12">
							<label for="input-conf-link_visitor" class="col-sm-12">Enlace a la conferencia <small class="my-hide"><i>(alumno)</i></small>:</label>
							<a class="col-sm-12" target="_blank" id="input-conf-link_visitor" href="#"></a>
						</div>

						<div class="form-group col-sm-12 col-md-6 ">
							<label for="fecha">Fecha:</label>
							<div>
								<i class="fa fa-calendar" aria-hidden="true"> </i> <span required type="date" name="fecha" id="input-conf-fecha" placeholder=""></span>
							</div>
						</div>
						<div class="form-group col-sm-12 col-md-6 ">
							<label for="fecha">Hora de inicio:</label>
							<div><i class="fa fa-clock-o" aria-hidden="true"> </i> <span required type="time" name="fecha" id="input-conf-hora_comienzo" placeholder=""></span></div>
						</div>

						<div class="form-group col-sm-12 col-md-12 ">
							<label for="input-conf-detalle">Descripción:</label>
							<div placeholder="Descripción de la conferencia..." required name="" id="input-conf-detalle" rows="2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="pnltabladeaudios">
		<div class="text-center"><br>
			<h2 class="titulo"></h2>
		</div>
		<div class="table-responsive" style="padding: 2em">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Audio</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div style="display: none" id="mostrarinfosesnse">
	<?php if (!empty($this->sence) || !empty($this->idsence_assigncursos)) { ?>
		<img src="<?php echo URL_BASE; ?>static/sence.png" style="max-width: 90%; cursor:pointer">
	<?php } ?>
</div>
<!-- Modal Ver Rubrica-->
<div class="modal fade my-font-all soft-rw" id="md-ver-rubrica" tabindex="-1" role="dialog" aria-labelledby="md-nueva-rubricaLabel" aria-hidden="true">
	<div class="modal-dialog md-auto-w" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-title">
					RUBRICA
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body modal-no-scroll">
				<div id="main-cont" class="row col-sm-12">
					<div class="my-loader my-color-green"></div>
				</div>
				<div class="my-hide row col-sm-12">
					<div id="row-cont-rubrica" class="col-sm-12 row-cont-rubrica">
						<div class="cont-rubrica my-card my-shadow">
							<h3 onclick="modalRubrica(this)" class="text-center my-full-w">Titulo de rubrica</h3>
							<div id="out-cont" class="out-cont">
								<div id="body-rubrica" class="body-cont-rubrica rubrica-black tema-4">
									<div id="rubrica-code" class="cod-rubrica">
										<div class="code-text">Código</div>
										<div class="code-num"></div>
									</div>
									<div id="cont-btn-crit" onclick="modalCriterio(this)" class="cont-btn-crit">
										<div class="criterio-text">Detalle</div>
										<div class="criterio-num">5%</div>
									</div>
									<div id="cont-btn-nivel" onclick="modalNivel(this)" name="cont-btn-nivel" class="cont-btn-nivel">
										<div class="nivel-text">Excelente</div>
										<div class="nivel-num">20</div>
									</div>
									<div id="cont-btn-indicador" onclick="modalIndicador(this)" class="cont-btn-indicador">
										<div class="indicador-text">Detalle</div>
									</div>
									<div id="btn-new-indicador" onclick="modalIndicador(this)" class="btn-new cont-btn-indic">
										<span class="btn-icon">+</span>
										<span class="btn-text">Indicador</span>
									</div>
									<div id="btn-new-criterio" onclick="modalCriterio(this)" class="cont-btn-crit btn-new">
										<span class="btn-icon">+</span>
										<span class="btn-text">Criterio</span>
									</div>
									<div id="btn-new-nivel" onclick="modalNivel(this)" class="cont-btn-nivel btn-new">
										<span class="btn-icon">+</span>
										<span class="btn-text">Nivel</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="cont-nota-rubrica" class="col-sm-12 my-center my-hide">
					<label for="nota-tp-rubrica" style="padding-right: 7px;">Tu Nota: </label> <span id="nota-tp-rubrica"> 20</span>
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12  col-md-6">
						<div id="autor-btn" class="my-hide">
							<button type="button" class="btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
							<button id="btn-save" type="submit" form="form-add" class="btn btn-primary">Guardar</button>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div id="default-btn" class="my-hide">
							<button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
					<div id="new-btn" class="col-sm-12 my-hide">
						<div class="row my-block">
							<div class="my-float-r">
								<button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
								<button type="submit" form="form-add-rubrica" class="btn btn-primary">Crear</button>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in show" id="md-material" tabindex="-1" role="dialog"  aria-hidden="true" style="z-index: 999991;">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-title">
					Material de Ayuda
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body modal-no-scroll">
				<iframe width="100%" id="materialidiframe" height="450" src="" autoplay=true frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="border: none;" class="embed-responsive-item"></iframe>
			</div>
			<div class="modal-footer">				
				<div class="container">
				<div class="row">
					<div class="col-sm-12  col-md-6 text-left">
						<label class="nomostraralinicio" modal="md-material" style="cursor: pointer; font-size: 20px;" ><i class="fa fa-check-square" aria-hidden="true"></i> Mostrar al iniciar el curso  </label>
					</div>
					<div class="col-sm-12 col-md-6 text-right">
						<div id="default-btn" class="">
							<button type="button" class="btncerrarmodal btn btn-secondary my-float-r" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
						</div>
					</div>			
					</div>		
				</div>
			
			</div>
		</div>
	</div>
</div>
</div>

<!-- mine -->
<script type="text/javascript">
	const cursoJSON=<?= $this->curso ?>;
	const __oCurso = cursoJSON.datos;
	__oCurso.idgrupoauladetalle = <?php echo !empty($this->idgrupoauladetalle) ? $this->idgrupoauladetalle : 0; ?>;
	const __idalumno = <?= $this->usuActivo['idpersona'] ?>;
	const __idcomplementario = <?php echo $this->idcomplementario; ?>;
	const __idcurso = <?php echo $this->idcurso; ?>;
	const __idgrupoauladetalle = <?php echo !empty($this->idgrupoauladetalle) ? $this->idgrupoauladetalle : 0; ?>;
	const __tipomatricula=<?php echo !empty($this->matricula["tipomatricula"])?$this->matricula["tipomatricula"]:2;?>;
	const oSubmenu = new Submenu;
	oSubmenu.launchInicioVer();
	const oMine = new Mine;
	const oCrudCurso = new CrudCurso;
	var _idalumno = <?= $this->usuActivo['idpersona'] ?>;
	var HIDE_fq_nav = false
	oCrudCurso.launchInicioVer();
	var idgrupoauladetalle = <?php echo !empty($this->idgrupoauladetalle) ? $this->idgrupoauladetalle : 0; ?>;
	$(document).ready(() => {
		fixCss();
		setInterval(() => {
			fixCss();
		}, 1500);
		window.addEventListener('message', function(e) {
			if(e.data != null && e.data.modulo != null){
				const data = e.data
				const modulo = data.modulo
				console.log(e,modulo)
				switch(modulo){
					case "smartbook":{
						$("#fq-nav").hide()
						HIDE_fq_nav = true
						if(data.vista != null && data.vista == "menu"){
							$("#fq-nav").show()
							HIDE_fq_nav = false
						}
					}break;
				}
			}
		});
	})

	function fixCss() {
		setTimeout(() => {
			let tmpAttr = $('#contentpages').attr('style');
			let fixStyle = '';
			let nav = $('#fq-nav');
			let cont = nav.parent();			
			if(tmpAttr!=undefined)
			if (!tmpAttr.includes('height: calc(100vh -')) {
				if (nav.css('display') == 'none') { //menus
					if (cont.hasClass('tab')) {
						cont.removeClass('tab');
					}

					fixStyle = ' height: calc(100vh - 60px) !important; ';
				} else { //pestañas
					if (!cont.hasClass('tab')) {
						cont.addClass('tab');
					}
					fixStyle = ' height: calc(100vh - 117px) !important; ';
				}
				$('#contentpages').attr('style', tmpAttr + fixStyle)
			}
		}, 2000);
	}

	// verificar si existe sesion 
	var _tiempo = 300000; // cada 5 minutos;
	var _tiempologueofn = function() {
		__sysajax({
			showmsjok: false,
			url: _sysUrlBase_ + 'json/sesion/estoylogueado',
			async: false,
			callback: function(rs) {
				if (rs.msj == false) {
					JSON.parse(localStorage.getItem("conteo"));
					clearInterval(_tiempologueo);
					Swal.fire('Advertencia', 'Su sesion se ha cerrado', 'warning').then((result) => {
						close();
						window.open('', '_parent', '');
						window.close();
						window.location.href = _sysUrlSitio_;
					});
				}
			}
		})
	};
	var _tiempologueo = setInterval(_tiempologueofn, _tiempo);
</script>
<!-- mine -->
<script type="text/javascript">
	//var cursoJSON = <?php //echo $this->curso; ?>;
	var cursoDATOS = __oCurso;
	//console.log('curso',__oCurso);
	var cursoCategoria = cursoJSON["categorias"][0];
	var idproyecto = <?php echo $this->usuActivo["idproyecto"]; ?>;
	var _iddetalle_ = 0;
	var _idpestania_ = 0;
	var icon = null;
	var notificar_vencimiento_curso=<?php echo !empty($this->notificarvencimiento)?'true':'false'; ?>;

	function agregarLike(obj, codigo, idusuario) {
		$.post(_sysUrlBase_ + "json/acad_curso_consulta/agregarlike", {
			'codigo': codigo,
			'idpersona': idusuario,
			'idgrupoauladetalle': __idgrupoauladetalle
		}, function(datosJSON) {
			if (datosJSON.code == 200) {
				icon = obj.closest('.heartbox').find('i');
				obj.text(datosJSON.likes);
				if (icon != null && typeof icon != 'undefined' && icon.hasClass('isliked')) {
					icon.attr('class', 'fa fa-heart-o');
				} else {
					icon.attr('class', 'fa fa-heart isliked');
				}
			} else {
				console.error(datosJSON.msj);
			}
		}, "json");
	}

	function checkLike(str) {
		var arr = [];
		var index = -1;
		var _idusuario = $('#idpersona__').val();
		if (str == null || str == '') {
			return 'fa fa-heart-o';
		}
		arr = str.split(',');
		index = arr.findIndex((el) => el == _idusuario);

		if (index != -1) {
			return 'fa fa-heart isliked';
		}
		return 'fa fa-heart-o';
	}

	function listarConsultas(iddetalle, idpestania) {
		_iddetalle_ = iddetalle;
		_idpestania_ = idpestania;
		$.post(_sysUrlBase_ + "json/acad_curso_consulta", {
			'sql': "1",
			'idcurso': "<?php echo $this->idcurso; ?>",
			'idcategoria': cursoCategoria["idcategoria"],
			'idgrupoaula': '<?php echo $this->idgrupoaula; ?>',
			'iddetalle': _iddetalle_,
			'idpestania': _idpestania_,
			'tipocurso': '<?php echo (empty($this->idcomplementario) ? "1" : "2"); ?>',
			'idcomplementario': __idcomplementario,
			'idgrupoauladetalle': __idgrupoauladetalle
		}, function(datosJSON) {
			if (datosJSON.code === 200) {
				var html = "",
					contador = 0,
					ultimo;
				if (Object.keys(datosJSON.data).length > 0) {
					$.each(datosJSON.data, function(i, item) {
						if (item.padre === "p" && i > 0) {
							html += '</div></div>';

							html += '<div class="input-group col-md-12">';
							html += '  <input id="input-' + ultimo + '" type="text" class="form-control rounded-0" placeholder="Responder">';
							html += '  <span class="input-group-append">';
							html += '    <button id="btnc_' + ultimo + '" onclick="responder(' + ultimo + ')" type="button" class="btn btn-info btn-flat"><i class="fa fa-chevron-right"></i></button>';
							html += '  </span>';
							html += '</div>';

							html += '</div>';
						}
						if (item.padre === "p") {
							ultimo = item.codigo_consulta;
							html += '<div class="post card collapsed-card">';

							//preguntas
							html += '<div class="card-header"><span data-card-widget="collapse" style="cursor:pointer; font-size: 15px;"><b>' + item.nombre + ' ' + item.ape_paterno + '</b> [' + item.fecha + ']</span><span class="badge badge-light ml-2 heartbox" data-cod="' + item.codigo + '"><span class="nlikes">' + item.likes + '</span> <i class="' + checkLike(item.like_de) + '" aria-hidden="true"></i></span><span class="badge badge-light ml-2 commentbox"><span class="ncomment">' + item.hijos + '</span> <i class="fa fa-commenting-o" aria-hidden="true"></i></span> <button type="button" class="btn btn-tool float-right" data-card-widget="collapse"><i class="fas fa-plus"></i></button>';
							html += '<p style="word-wrap: break-word;">' + item.contenido + '</p></div><div class="card-body">';
							html += '<div style="margin-left: 20px;">';
						} else {
							//respuestas
							html += '<span style="font-size: 15px;"><b>' + item.nombre + ' ' + item.ape_paterno + '</b> [' + item.fecha + ']</span><span class="badge badge-light ml-2 heartbox" data-cod="' + item.codigo + '"><span class="nlikes">' + item.likes + '</span> <i class="' + checkLike(item.like_de) + '" aria-hidden="true"></i></span>';
							html += '<p style="word-wrap: break-word;">' + item.contenido + '</p>';
						}
						contador++;
					});

					if (contador > 0) {
						html += '</div></div>';

						html += '<div class="input-group col-md-12">';
						html += '  <input id="input-' + ultimo + '" type="text" class="form-control rounded-0" placeholder="Responder">';
						html += '  <span class="input-group-append">';
						html += '    <button id="btnc_' + ultimo + '" onclick="responder(' + ultimo + ')" type="button" class="btn btn-info btn-flat"><i class="fa fa-chevron-right"></i></button>';
						html += '  </span>';
						html += '</div>';

						html += '</div>';
					}
					html += `
						
						<hr/>
						<div class="row justify-content-center">
							<div class="col-sm-12 text-center">
								<h4 style="color:#50555a;">¿Tienes alguna duda?. <span class="h5">Te podemos ayudar, escribe tu pregunta aquí</span></h4>
							</div>
							<div class="col-md-6">
								<p>Contenido de la Consulta <font color = "red">(*)</font>
									<textarea type="text" id="txtcontenidoc2" name="txtcontenidoc2" class="form-control input-sm" required="" rows="2" style="resize: none;"></textarea>
								</p>
								<div class="text-center">
									<button type="button" class="btn btn-primary on-save"><?php echo JrTexto::_("Save"); ?></button>
								</div>
							</div>
						</div>
					`;
					$("#listarConsulta").html(html);
				} else {
					$("#listarConsulta").html(`
						<hr/>
						<div class="row justify-content-center">
							<div class="col-sm-12 text-center">
								<h4 style="color:#50555a;">¿Tienes alguna duda?. <span class="h5">Te podemos ayudar, escribe tu pregunta aquí</span></h4>
							</div>
							<div class="col-md-6">
								<p>Contenido de la Consulta <font color = "red">(*)</font>
									<textarea type="text" id="txtcontenidoc2" name="txtcontenidoc2" class="form-control input-sm" required="" rows="2" style="resize: none;"></textarea>
								</p>
								<div class="text-center">
									<button type="button" class="btn btn-primary on-save"><?php echo JrTexto::_("Save"); ?></button>
								</div>
							</div>
						</div>
					`);
				}
			}
		}, "json");
	}

	function responder(id) {
		var input = "#input-" + id;
		$.post(_sysUrlBase_ + "json/acad_curso_consulta/guardar", {
			'idcurso': __idcurso,
			'idcategoria': cursoCategoria["idcategoria"],
			'idgrupoaula': '<?php echo $this->idgrupoaula; ?>',
			'iddetalle': _iddetalle_,
			'idpestania': _idpestania_,
			'tipocurso': '<?php echo (empty($this->idcomplementario) ? "1" : "2"); ?>',
			'contenido': $(input).val(),
			'idcomplementario': __idcomplementario,
			'idgrupoauladetalle': __idgrupoauladetalle,
			'respuesta': id
		}, function(resultado) {
			if (resultado.code === 200) {
				$(input).val("");
				listarConsultas(_iddetalle_, _idpestania_);
			}
		}, 'json');
	}

	$('body').on('click', '.on-save', function() {
		$("#txtcontenidoc").val($('#txtcontenidoc2').val());
		$("#frmAddConsulta").trigger('submit');
	});

	$("#frmAddConsulta").submit(function(evento) {
		evento.preventDefault();

		$.post(_sysUrlBase_ + "json/acad_curso_consulta/guardar", {
			'idcurso': "<?php echo $this->idcurso; ?>",
			'idcategoria': cursoCategoria["idcategoria"],
			'idgrupoaula': '<?php echo $this->idgrupoaula; ?>',
			'iddetalle': _iddetalle_,
			'idpestania': _idpestania_,
			'tipocurso': '<?php echo (empty($this->idcomplementario) ? "1" : "2"); ?>',
			'idcomplementario': __idcomplementario,
			'idgrupoauladetalle': __idgrupoauladetalle,
			'contenido': $("#txtcontenidoc").val()
		}, function(resultado) {
			var datosJSON = resultado;
			if (datosJSON.code === 200) {
				swal("Exito", datosJSON.msj, "success");
				$("#btncerrarc").click();
				listarConsultas(_iddetalle_, _idpestania_);
			}
		}, 'json');
	});

	var oIdHistorial = {
		'examen': 0,
		'sesion': 0
	}
	var rolActual = <?php echo !empty($this->rolActual) ? $this->rolActual : 0 ?>;
	var infocurso = <?php echo $this->curso; ?>;
	var controlaravance = rolActual == 3 ? true : false;
	var returnlink = "<?php echo $this->returnlink; ?>";
	var nombreusuario = "<?php echo $this->usuarionombre; ?>";
	var datosdelcurso = infocurso.datos || {};
	var temas = infocurso.temas || {};
	var chkjson = datosdelcurso.txtjson == '' || datosdelcurso.txtjson == '1' ? {} : datosdelcurso.txtjson;
	var jsoncurso = _isJson(chkjson) ? JSON.parse(chkjson) : {};
	var idcurso = parseInt(datosdelcurso.idcurso || 0);
	var datenow = '<?php echo date('Y-m-d'); ?>';
	var imgdefecto = "<?php echo $imgcursodefecto; ?>";
	var urlmedia = '<?php echo URL_BASE; ?>';
	var pasoscurso = 10;
	var plantilla = jsoncurso.plantilla || {
		id: 0,
		nombre: 'blanco'
	};
	var estructura = jsoncurso.estructura || {
		'font-family': 'arial',
		'font-size': '12px',
		color: 'rgba(0,0,0,1)',
		'background-color': 'rgba(0,0,0,0)',
		'background-image': '',
		image: imgdefecto
	};
	var estilopagina = jsoncurso.estilopagina || {
		'font-family': 'arial',
		'font-size': '12px',
		color: 'rgba(0,0,0,1)',
		'background-color': 'rgba(0,0,0,0)',
		'background-image': '',
		image: imgdefecto
	};
	var infoportada = jsoncurso.infoportada || {
		titulo: '',
		descripcion: '',
		image: imgdefecto
	};
	var infoindice = jsoncurso.infoindice || 'top';
	var infoavance = jsoncurso.infoavance || 0;
	var curtemadefault = {
		tipo: '#showpaddcontenido',
		imagenfondo: '',
		infoavancetema: 0,
		colorfondo: 'rgba(0,0,0,0)'
	};
	var curtema = {
		index: 0,
		idtema: 0,
		txtjson: {
			tipo: '#showpaddcontenido',
			imagenfondo: '',
			infoavancetema: 0,
			colorfondo: 'rgba(0,0,0,0)'
		}
	};
	var infoavancetema = 0;
	var curtemaidpadre = 0;
	var curtemaindex = 0;
	var curtemaoption = {};
	var _sysUrlRaiz_ = '<?php echo URL_BASE; ?>';
	var _host = '<?php echo URL_BASE; ?>';
	var url_media = '<?php echo URL_BASE; ?>';
	var dandoidexamen = '';
	var tieneDocente = <?php echo !empty($this->tieneDocente) ? 'true' : 'false'; ?>;
	var pagoRecurso = <?php echo !empty($this->pagoRecurso) ? 'true' : 'false'; ?>;
	var bloqueodeexamenes = <?php echo !empty($bloqueodeexamenes) ? "'" . $bloqueodeexamenes . "'" : 'false'; ?>;
	var notabloqueoexamenes = <?php echo !empty($notabloqueoexamenes) ? $notabloqueoexamenes : 'false'; ?>;
	//console.log('notabloqueoexamenes',bloqueodeexamenes,notabloqueoexamenes); 
	var __menu__ = false;

	function registrarFecha(_lugar) {
		var now = new Date();
		var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
		$.ajax({
			url: _sysUrlBase_ + 'json/historial_sesion/guardarhistorialsesion',
			type: 'POST',
			dataType: 'json',
			data: {
				'lugar': _lugar,
				'idcurso': __idcurso,
				'idcc': __idcomplementario,
				'idgrupoauladetalle': __idgrupoauladetalle,
			},
		}).done(function(resp) {
			if (resp.code == 200) {
				if (_lugar == 'E') {
					oIdHistorial.examen = resp.data.idhistorialsesion;
				} else {
					oIdHistorial.sesion = resp.data.idhistorialsesion;
				}
				// console.log(oIdHistorial.examen);
			} else {
				mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error');
			}
		}).fail(function(xhr, textStatus, errorThrown) {
			alert(xhr.responseText);
		});
		return 0;
	}

	function editarHistoriaSesion(id = null, type = 1) {
		var _id = id != null ? id : 0;
		$.ajax({
				url: _sysUrlBase_ + 'json/historial_sesion/guardarhistorialsesion',
				async: false,
				type: 'POST',
				dataType: 'json',
				data: {
					'idhistorialsesion': _id
				},
			})
			.done(function(resp) {
				if (resp.code == 200) {
					switch (type) {
						case 1:
							oIdHistorial.examen = resp.data.idhistorialsesion;
							break;
						case 2:
							oIdHistorial.sesion = resp.data.idhistorialsesion;
							break;
					}
				} else {
					return false;
				}
			})
			.fail(function(xhr, textStatus, errorThrown) {
				return false;
			});
	}

	$(window).on('beforeunload', function() {
		if (oIdHistorial.examen != 0) {
			editarHistoriaSesion(oIdHistorial.examen);
		}
		if (oIdHistorial.sesion != 0) {
			editarHistoriaSesion(oIdHistorial.sesion);
		}
	});

	var tabladeaudios = function(link) {
		var _linkjson_ = link;
		var html = $('#pnltabladeaudios').clone();
		let txtbloquearaudios='';
		if(__tipomatricula==3){ // solo bloquear para los de tipo suscripcion;
			txtbloquearaudios='controlslist="nodownload"';
		}
		if (_linkjson_ != '') {
			_linkjson_ = atob(_linkjson_);
			_linkjson_ = JSON.parse(_linkjson_);
			if (_linkjson_.titulo != '' && _linkjson_.titulo != undefined)
				html.find('h2.titulo').text(_linkjson_.titulo);
			if (_linkjson_.menu != undefined)
				if (_linkjson_.menu.length)
					$.each(_linkjson_.menu, function(i, a){
						html.find('tbody').append('<tr><td>' + (i + 1) + '</td><td>' + a.nombre + '</td><td><audio controls src="' + _sysUrlBase_ + a.link + '" '+txtbloquearaudios+'></audio></td></tr>');
					})
		}
		return html;
	}

	var tablamultioptions = function(_linkmenujson_) {
		return new Promise(async (resolve, reject) => {
			var html = $('#multioptionsvideos').clone();
			for (let i = 0; i < _linkmenujson_.length; i++) {
				const v = _linkmenujson_[i];
				if (v.tipo == 'video') {
					if (v.hasOwnProperty('ubicacion') &&
						v.ubicacion == "vimeo"
					) {
						let rs = await oMine.postData({
							'url': _sysUrlBase_ + 'json/Vimeo/isAvailable',
							data: {
								params: {
									uri: v.link
								}
							},
							showloading: false
						})
						if (rs.ubicacion == "vimeo") {
							html.find('ol').append('<li data-target="#carouselExampleIndicators" data-slide-to="' + i + '" class="' + (i == 0 ? 'active' : '') + '" style="width:100px;"><video class="embed-responsive-item d-block w-100" src="' + _sysUrlBase_ + v.link + '"></video></li>');
							let tmp = `<div class="carousel-item ${ (i == 0 ? 'active' : '')}">
						<div class="cont-vimeo-player" data-vimeo-id="${rs.link}" 
							data-vimeo-byline="false"
							data-vimeo-autoplay="false"
							data-vimeo-title="false" id="vimeo-multiopt"></div>
						</div>`;
							html.find('div.carousel-inner').append(tmp);
						} else{

							html.find('ol').append('<li data-target="#carouselExampleIndicators" data-slide-to="' + i + '" class="' + (i == 0 ? 'active' : '') + '" style="width:100px;"><video class="embed-responsive-item d-block w-100" src="' + rs.link + '"></video></li>');
							html.find('div.carousel-inner').append('<div class="carousel-item ' + (i == 0 ? 'active' : '') + '"><video class="embed-responsive-item" src="' + rs.link + '" controls ' + (i == 0 ? 'autoplay' : '') + ' style="width:80%; max-width:80%;"></video></div>');
						}

					} else { //curso no procesado con vimeo
						html.find('ol').append('<li data-target="#carouselExampleIndicators" data-slide-to="' + i + '" class="' + (i == 0 ? 'active' : '') + '" style="width:100px;"><video class="embed-responsive-item d-block w-100" src="' + _sysUrlBase_ + v.link + '" oncontextmenu="return false;" controlslist="nodownload"></video></li>');
						html.find('div.carousel-inner').append('<div class="carousel-item ' + (i == 0 ? 'active' : '') + '"><video class="embed-responsive-item" src="' + _sysUrlBase_ + v.link + '" controls ' + (i == 0 ? 'autoplay' : '') + ' style="width:80%; max-width:80%;" oncontextmenu="return false;" controlslist="nodownload"></video></div>');
					}
				} else if (v.tipo == 'imagen') {
					html.find('ol').append('<li data-target="#carouselExampleIndicators" data-slide-to="' + i + '" style="width:100px;"><img class="img-responsive d-block w-100" src="' + _sysUrlBase_ + v.link + '"/></li>');
					html.find('div.carousel-inner').append('<div class="carousel-item ' + (i == 0 ? 'active' : '') + '"><img class="img-responsive" src="' + _sysUrlBase_ + v.link + '"style="max-height:80%; max-width:80%;"/></div>');
				} else if(v.tipo =='word' || v.tipo =='excel' || v.tipo =='power point'){
					html.find('ol').append('<li data-target="#carouselExampleIndicators" data-slide-to="' + i + '" style="width:auto; text-indent:0; height: auto;    background-color: transparent;"><span class="btn btn-danger"><i class="fa fa-file-o"></i> <br>' + v.nombre + '</span></li>');
					html.find('div.carousel-inner').append('<div class="carousel-item ' + (i == 0 ? 'active' : '') + '" style="position: relative; width: 100%; padding: 0; overflow: hidden; height:80vh;"><iframe src="https://docs.google.com/gview?url=' + _sysUrlBase_ + v.link + '&amp;embedded=true" style="height:85%; width:100%; "/></div>');
					/*<iframe src="https://docs.google.com/gview?url=https://sagita.localhost.com/static/media/cursos/curso_854/ses_4785/file_4785_1612471261176.docx?id=20210204154103&amp;embedded=true" width="100%" height="450" style="border: none;" class="embed-responsive-item"></iframe>*/
				}else if(v.tipo =='enlacevimeo'){
					let rs = await oMine.postData({
						'url': String.prototype.concat("https://vimeo.com/api/oembed.json?url=",v.link),
						method: 'GET',
						showloading: false
					})
					if(rs != null && rs.video_id != null){
						html.find('ol').append('<li data-target="#carouselExampleIndicators" data-slide-to="' + i + '" class="' + (i == 0 ? 'active' : '') + '" style="width:100px;"><video class="embed-responsive-item d-block w-100" src=""></video></li>');
							let tmp = `<div class="carousel-item ${ (i == 0 ? 'active' : '')}">
								<div class="cont-vimeo-player" data-vimeo-id="${rs.video_id}" 
									data-vimeo-byline="false"
									data-vimeo-autoplay="false"
									data-vimeo-title="false" id="vimeo-multiopt"></div>
								</div>`;
						html.find('div.carousel-inner').append(tmp);
					}
					// console.log("pintar los reproductores vimeo",rs)
				}else {
					html.find('ol').append('<li data-target="#carouselExampleIndicators" data-slide-to="' + i + '" style="width:auto; text-indent:0; height: auto;    background-color: transparent;"><span class="btn btn-danger"><i class="fa fa-file-o"></i> <br>' + v.nombre + '</span></li>');
					html.find('div.carousel-inner').append('<div class="carousel-item ' + (i == 0 ? 'active' : '') + '" style="position: relative; width: 100%; padding: 0; overflow: hidden; height:80vh;"><iframe src="' + _sysUrlBase_ + v.link + '" style="height:85%; width:100%; "/></div>');
				}

			}
			html.children().attr('id', 'carouselExampleIndicators');
			resolve(html);
		})
	}

	var datosvideoconferencia = function(_linkjson_) {
		let conferencia = JSON.parse(atob(_linkjson_));
		let htmlTmp = $('#showvideoconferencia').clone();
		let html = htmlTmp;
		html.find('#input-conf-titulo').html(conferencia.titulo);
		if (rolActual == 2 && conferencia.link_mod != '') {
			html.find('#input-conf-link_mod').attr('href', conferencia.link_mod);
			html.find('#input-conf-link_mod').html(conferencia.link_mod);
		} else {
			html.find('#input-conf-link_mod').parent().addClass('my-hide');
		}
		html.find('#input-conf-link_visitor').attr('href', conferencia.link_visitor);
		html.find('#input-conf-link_visitor').html(conferencia.link_visitor);
		html.find('#input-conf-fecha').html(conferencia.fecha);
		html.find('#input-conf-hora_comienzo').html(conferencia.hora_comienzo);
		html.find('#input-conf-detalle').html(conferencia.detalle);
		return html;
	}

	// var htmlforos= function(link) {
	// 	let html = 'aqui se muestra ...';
	// 	return html;
	// }

	var cargarrefresh = false;

	$(document).ready(function(ev) {
		$('#infosense').html($('#mostrarinfosesnse').html());
		<?php if (!empty($this->sence) || !empty($this->idsence_assigncursos)) : ?>
			<?php if (empty($this->idsence_assigncursos)) { ?>
				$('#infosense').on('click', 'img', function(ev) {
					let idaccion = '<?php echo !empty($this->sence["CodigoCurso"]) ? $this->sence["CodigoCurso"] : '123456789'; ?>';
					let codigocursosense = 'SENCE-<?php echo !empty($this->sence["CodSence"]) ? $this->sence["CodSence"] : 'XXXXXXX' ?>';
					let showmodalsense = localStorage.getItem('showmodalsense') || {
						mostra: 'true'
					};
					let htmltarea = `<div class="modalsense">
					<div><b>Gestor, aquí los usuarios se integrarán con sence</b></div>
					<div>El Código SENCE configurado para el curso <b>${__oCurso.nombre||'Nombre del curso'} </b> es : <strong>${codigocursosense}</strong>.  </div>
					<div>Recuerda asignar el ID de Acción en el nombre del grupo  de tus participantes, así : <strong>${idaccion}</strong> </div>
					<div><b>No</b> existen grupos de becarios configurados.</div>
					<div><b>No</b> se enviarán correos de alerta en caso de errores. </div>
					<div><b>No</b> se pedirá el cierre de sesión al participante. </div>
					<div><br><label class="modalsenseopen" style="position: absolute;left: 1.5em;"> 
							<i class="clsmodal fa ${showmodalsense=='si'?'fa-check-circle':'fa-circle-o'}"></i> 
							<span>Mostrar éste mensaje al cargar curso </span>
						</label></div>
					</div>`;
					var dt = {
						titulo: 'Integración Sence',
						header: true,
						html: htmltarea,
						footer: true,
						tam: 'xs'
					};
					//console.log('dt',dt);
					var md = __sysmodal(dt);
					md.on('click', '.modalsenseopen', function(ev) {
						let i = $(this).children('i.clsmodal');
						if (i.hasClass('fa-check-circle')) {
							i.addClass('fa-circle-o').removeClass('fa-check-circle');
							localStorage.setItem('showmodalsense', 'no');
						} else {
							i.addClass('fa-check-circle').removeClass('fa-circle-o');
							localStorage.setItem('showmodalsense', 'si');
						}
					})
				})
			<?php } ?>

			var resizesense = function() {
				let heightmenu = $('#mostrarmodulos').height();
				let heightbody = $('body').height();
				if (heightmenu > heightbody - 150)
					$("#infosense").css({
						position: 'relative'
					});
				else $("#infosense").css({
					position: 'absolute'
				});
			}
			$(window).resize(function() {
				resizesense();
			})
			let showmodalsense = localStorage.getItem('showmodalsense') || 'si';
			if (showmodalsense == 'si') $("#infosense img").trigger("click");
			setTimeout(function() {
				resizesense()
			}, 3000);
		<?php endif;
		$fechascurso = '';
		if (!empty($this->matricula)) {
			$fechascurso = '<br><div style="color: white; margin-top: -4px; font-weight: 400; font-size: 14px; text-shadow: black 0.1em 0.1em 0.2em;"> Inicio : ' . date("m-d-Y", strtotime($this->matricula["fecha_registro"])) . ' - Final ' . date("m-d-Y", strtotime($this->matricula["fecha_vencimiento"])) . ' </div>';
		}
		?>
		// console.log(cursoDATOS)
		$("#nombreCursoTop").html(cursoDATOS.nombre + ' <?php echo $fechascurso; ?>');
		var bookplantilla = $('#bookplantilla');
		var marcopage = bookplantilla.children('.marcopage');
		var contentpages = marcopage.children('#contentpages');
		var addcontent = $('#showpaddcontenido._addcontenido #addcontentclone').clone();
		$('#editlistadoopcion #aquitambienaddcontent').append(addcontent);
		var _diseniarcurso = function() {
			var previewcurso = $('#previewcurso');
			var mainestilo = previewcurso.children('#mainestilo');
			var bookplantilla = previewcurso.children('#bookplantilla');
			bookplantilla.css(estructura);
			disenioestiloP_(estilopagina);
		}
		var _diseniarIndice = function(mostrar) {
			// return null;
			_show = mostrar || false; //no sirve de nada...
			var bookplantilla = $('#previewcurso').children('#bookplantilla'); //no lo usa aquí??
			////////////////////////////////////////////////////
			oSubmenu.drawFunction = (arrMenu, target, eshijo = false, indexPadre = '') => {
				if (arrMenu.length > 0) {
					var j = 0;
					var nexamen = 0;
					var nsesion = 0;
					var secactive = 0;
					$.each(arrMenu, function(i, v) {
						nsesion++;
						txtjson = v.txtjson == '' ? '{}' : v.txtjson;
						var jsontmp = JSON.parse(txtjson);
						var txti = '';
						var esexamen = false;
						txtjson = v.txtjson == '' ? '{}' : v.txtjson;
						var jsontmp = JSON.parse(txtjson);
						var txti = '';
						var esexamen = false;
						var icon = jsontmp.icon || '';
						if (icon == '' || icon == '#') {
							if (jsontmp.typelink != 'smartquiz') {
								j++;
								txti = '<b  style="font-size: 1.25em;">' + (indexPadre != '' ? (indexPadre + '.') : '') + j + '.</b> '; //Esta es el número de orden de los menus
							} else {
								txti = '<i class="nav-icon fas fa fa-list fa-fw"></i> ';
							}
						} else { //en vez de numero de orden, le pone icono
							txti = '<i class="nav-icon fas fa ' + icon + ' fa-fw"></i> ';
						}
						if (jsontmp.typelink == 'smartquiz') { //si es examen lo cuenta
							esexamen = true;
							nexamen++;
						}
						var dataExtra = "";
						var classExtra = "";
						if (esexamen) {
							dataExtra = ' esexamen="' + nexamen + '"';
							classExtra = ' esexamen';
						} else {
							dataExtra = ' essesion="' + j + '"';
							classExtra = ' essesion';
						}
						var html = '';
						var necesitadocente = '';
						var esrecursopagado = '';
						var estarea = '';
						var esproyecto = '';
						var _vermenu = jsontmp._vermenu || 'si';
						if (rolActual == 3) {
							necesitadocente = jsontmp.necesitadocente == 'si' ? 'necesitadocente' : '';
							esrecursopagado = jsontmp.esrecursopagado == 'si' ? 'esrecursopagado' : '';
							estarea = jsontmp.estarea == 'si' ? 'estarea' : '';
							esproyecto = jsontmp.esproyecto == 'si' ? 'esproyecto' : '';
						}
						if (pagoRecurso == true) esrecursopagado = '';
						var _linktmp = (jsontmp.link || '');
						var _linktypetmp = (jsontmp.typelink || '');
						if (_linktypetmp == 'smartenglish') {
							_linktmp = _linktmp + '&idcursoexterno=' + __idcurso + '&icc=' + __idcomplementario + '&idgrupoauladetalle=' + __idgrupoauladetalle;
							_linktmp = '/smartbook' + _linktmp.substring(_linktmp.lastIndexOf("/ver/?"));
							_linktypetmp = 'html';
							$(".home-course-section-1").hide()
						}


						html += '<li id="mnuli' + (eshijo ? 'Sub' : '') + v.orden + '" class="nav-item ' + `${v.espadre == 1?' has-treeview ':' '} ` + ' view view-first ' + necesitadocente + ' ' + esrecursopagado + ' ' + estarea + ' ' + esproyecto + ` "${v.espadre == 0?` onclick="contenidoCurso(this, ${eshijo ? `'Sub'+` : ``}${v.orden})" `:` `} ` + ' data-link="' + _linktmp + '" data-type="' + _linktypetmp + '" data-imagenfondopagina="' + v.imagen + '">';
						html += '<a  ' + `${v.espadre == 1?`espadre="${v.espadre}"`:``}` + ' id="mnu' + (eshijo ? 'Sub' : '') + v.orden + '" class=" mnus nav-link _btnversesion' + classExtra + ' ' + necesitadocente + ' ' + esrecursopagado + ' ' + estarea + ' ' + esproyecto + ' " data-iddetalle="' + v.idcursodetalle + '" idnumero="' + nsesion + '"' + dataExtra + ' data-link="' + _linktmp + '" data-type="' + _linktypetmp + '" criterios="' + jsontmp.criterios + '" data-mostrarpregunta="' + jsontmp.mostrarpregunta + '">';
						html += txti;
						html += '<p style="white-space: normal"> &nbsp;&nbsp;' + v.nombre + `${v.espadre == 1?' <i class="right fas fa-angle-left"></i> ':' '}` + '</p>';
						html += '</a>';
						/*if(necesitadocente!=''&&rolActual==3)
						html += '<div class="tools necesitadocente text-center" style="position:absolute; top:0px; width:100%; left:0px; height:100%;"><i class="fa fa-graduation-cap"></i></div>';*/
						//mine

						if (v.espadre == 1) {
							html += `
								<ul id="submenu${v.orden}" class="nav nav-treeview">
								</ul>
							`;
						}

						//mine
						if (jsontmp.esrecursopagado == 'si' && rolActual == 3 && pagoRecurso == false)
							html += '<div class="tools  esrecursopagado text-center" style="position:absolute; top:0px; width:100%; left:0px; height:100%;"><i class="fa fa-dollar"></i><br>Necesita Pagar este Recurso</div>';
						html += '</li>';
						//INSERTA uno por uno
						if (((necesitadocente != '' && tieneDocente == true) || necesitadocente == '') && _vermenu == 'si') {
							//.esexamen nunca existe y está de más??
							$(target).append(html).find('.esexamen').attr('totalexamen', nexamen);
							// mnu_active(v.orden);
						}
						if (v.espadre == 1 && v.hijos.length > 0) {
							oSubmenu.drawFunction(v.hijos, `#submenu${v.orden}`, true, (icon == '' || icon == '#') ? j : '');
						}
					});
					$.each(arrMenu, function(i, v) {
						var url_ = window.location.href.split("#");
						if (url_[1] != undefined && cargarrefresh == false) {
							cargarrefresh = true;
							var idurlsel = window.atob(url_[1]);
							if (url_.length > 2)
								var idurlsel = window.atob(url_[2]);
							mnu_active(idurlsel);
						}
					});
					if (cargarrefresh == false) {
						mnu_active(0);
					}
				}
			}
			oSubmenu.drawFunction(temas, "#mostrarmodulos");
			console.log('temas', temas);
			////////////////////////////////////////////////////
			continuarSesion();
		}
		//activar los menus
		var continuarSesion = function() {

			if (controlaravance == true) {
				oCrudCurso.isBlocked().then((bloquear) => {
					if (bloquear) {

						$("._btnversesion").parent().attr("style", 'cursor: no-drop !important;');
						$('._btnversesion[idnumero!=1][espadre!=1]').addClass('disabled').attr('disabled', 'disabled');
					}
				})
				__sysajax({
					fromdata: {
						idcurso: __idcurso,
						idusuario: 'yo',
						idcomplementario: __idcomplementario,
						idgrupoauladetalle: __idgrupoauladetalle,
					},
					showmsjok: false,
					url: _sysUrlBase_ + 'json/bitacora_alumno_smartbook/',
					async: false,
					callback: function(rs) {
						oCrudCurso.isBlocked().then((bloquear) => {
							if (bloquear) {

								var dt = rs.data;
								var haypendiente = false;
								var hayterminado = false;
								$.each(dt, function(i, v) {
									if (v.estado == 'T') {
										hayterminado = true;
										if (v.idsesion == -1) v.idsesion = 'Encuesta';
										$('._btnversesion[data-iddetalle="' + v.idsesion + '"]').removeClass('disabled').removeAttr('disabled').addClass('terminado');
									} else {
										haypendiente = true;
										$('._btnversesion[data-iddetalle="' + v.idsesion + '"]').removeClass('disabled').removeAttr('disabled')
									}
								})
								if (haypendiente == false && hayterminado == true) {
									$('._btnversesion.disabled').first().removeClass('disabled').removeAttr('disabled').parent().removeAttr("style");

									//$('._btnversesion.terminado').last().next().removeClass('disabled').removeAttr('disabled');
									//$('._btnversesion.terminado').last().parent().next().children().removeClass('disabled').removeAttr('disabled').removeAttr("style");
								}
							}
						})
					}
				})
			}
		}
		//activar las pestanias
		var continuarPestanias = function(_men) {
			if (controlaravance == true) {
				oCrudCurso.isBlocked().then((bloquear) => {
					if (bloquear) {

						_men.siblings().addClass('disabled').attr('disabled', 'disabled');
						_men.removeClass('disabled').removeAttr('disabled');
					}
				})
				__sysajax({
					fromdata: {
						idcurso: idcurso,
						idusuario: 'yo',
						idsesion: _men.attr('idcursodetallepadre'),
						idcomplementario: __idcomplementario,
						idgrupoauladetalle: __idgrupoauladetalle,
					},
					showmsjok: false,
					url: _sysUrlBase_ + 'json/bitacora_smartbook/',
					async: false,
					callback: function(rs) {
						oCrudCurso.isBlocked().then((bloquear) => {
							if (bloquear) {

								var dt = rs.data;
								var haypendiente = false;
								var hayterminado = false;
								$.each(dt, function(i, v) {
									if (v.progreso == 100) {
										var _tmpmen = $('._vercontenido_[data-id="' + v.idsesionB + '"]');
										if (_tmpmen.length == 0) _tmpmen = $('._vercontenido_[data-nombre="' + v.pestania + '"]');
										if (_tmpmen.length) _tmpmen.removeClass('disabled faltante').removeAttr('disabled').addClass('terminado').attr('progreso', v.progreso);
									} else {
										var _tmpmen = $('._vercontenido_[data-id="' + v.idsesionB + '"]');
										if (_tmpmen.length == 0) _tmpmen = $('._vercontenido_[data-nombre="' + v.pestania + '"]');
										if (_tmpmen.length) _tmpmen.removeClass('terminado').addClass('disabled').attr('disabled', 'disabled').attr('progreso', v.progreso);
									}
								})
								$('._vercontenido_.disabled').eq(0).removeClass('disabled').removeAttr('disabled');
								var pest = _men.siblings('._vercontenido_');
								var termino = true;
								$.each(pest, function(i, v) {
									if (!$(v).hasClass('terminado')) termino = false;
								})
								//console.log("termino",termino);
								if (termino) {
									$('._btnversesion.disabled').first().removeClass('disabled').removeAttr('disabled').parent().removeAttr("style");
									//$('._btnversesion.terminado').last().next().removeClass('disabled').removeAttr('disabled');
									//$('._btnversesion.terminado').last().parent().next().children().removeClass('disabled').removeAttr('disabled').removeAttr("style");
								}
							}
						})
					}
				})
			}
		}

		var disenioestiloP_ = function(estilopagina) {
			var previewcurso = $('#previewcurso');
			var bookplantilla = previewcurso.children('#bookplantilla');
			var marcopage = bookplantilla.children('.marcopage');
			marcopage.css(estilopagina);
		}

		var buscartema2_ = function(itemas, buscar, idpadre) {
			idpadre = idpadre || 0;
			var encontrado = false;
			var rdt = {
				dato: {},
				encontrado: false
			};
			if (itemas.length > 0) {
				itemas.forEach(function(cur, i, v) {
					if (encontrado == false) {
						cur.index = i;
						if (cur.idcursodetalle == buscar.idet && buscar.por == 'idet') {
							encontrado = true;
							cur.index = i;
							if (cur.txtjson == '') cur.txtjson = curtemadefault;
							rdt = {
								dato: cur,
								encontrado: true
							};
						} else if (i == buscar.index && buscar.por == 'index' && cur.idpadre == idpadre) {
							encontrado = true;
							cur.index = i;
							if (cur.txtjson == '') cur.txtjson = curtemadefault;
							rdt = {
								dato: cur,
								encontrado: true
							};
						} else {
							var hijos = cur.hijos || [];
							if (hijos.length > 0) {
								dt = buscartema2_(hijos, buscar, idpadre);
								if (dt.encontrado == true) {
									encontrado = true;
									rdt = {
										dato: dt.dato,
										encontrado: true
									};
								}
							}
						}
					}
				})
			}
			return rdt;
		}

		$('#showindice').find('.btnplantillaindice[data-nombre="' + infoindice + '"]').children('a').addClass('btn-success');

		$('body').on('mouseover', '.heartbox i', function() {
			if (!$(this).hasClass('isliked')) {
				$(this).attr('class', 'fa fa-heart');
				$(this).css('color', '#e06363');
				$(this).css('font-size', '1.5em');
			}
		}).on('click', '.heartbox i', function(e) {
			//set el corazon
			var codigo = $(this).closest('.heartbox').data('cod');
			var _objeto = $(this).closest('.heartbox').find('.nlikes');
			agregarLike(_objeto, codigo, $('#idpersona__').val());
		}).on('mouseout', '.heartbox i', function() {
			if (!$(this).hasClass('isliked')) {
				$(this).attr('class', 'fa fa-heart-o');
				$(this).css('font-size', '');
				$(this).css('color', '');
			}
		});

		$('#previewcurso').on('click', '._vercontenido_', function(ev) {
			ev.preventDefault();
			var menu = $(this);
			var typelink = menu.attr('data-type') || '';
			var _idpestana = menu.data('id');
			var _idgrupoauladetalleArr = _idgrupoauladetalleArr = <?php echo !empty($this->idgrupoauladetalleArr) ? json_encode($this->idgrupoauladetalleArr) : '[]'; ?>; //fix
			
			if (typelink != "multioptions") {
				contentpages.removeClass('embed-responsive embed-responsive-16by9').addClass('_P0').attr("style", "");
				contentpages.html($("#preloading").html());
				// return false;
			}
			//console.log('a',menu);		
			if (menu.hasClass('esrecursopagado') && rolActual == 3) {
				return false;
			}
			// return false;
			var link = menu.attr('data-link');
			let ubicacion = menu.attr('data-ubicacion');

			var img = menu.attr('data-imagenfondo') || '';
			img = img == '/static/media/nofoto.jpg' ? '' : _sysUrlBase_ + img;

			var mostrarpregunta = menu.attr('data-mostrarpregunta') || 'no';
			if (mostrarpregunta == "si") {
				$("#foro").addClass("collapsed-card").show();
				$("#foro").children(".card-header").children(".card-tools").children(".btn-collapse").children("i").attr("class", "fas fa-plus");
				$("#foro").children(".card-body").hide();
			} else {
				$("#foro").hide();
			}

			var cssadd = {
				'background-image': 'url(' + img + ')',
				'color': (menu.attr('data-color') || ''),
				'background-color': (menu.attr('data-colorfondopagina') || '')
			};
			var colorfondo = '';
			var imagenfondo = '';
			var lastindex = link.lastIndexOf("static/");
			var tipoexamen = '';
			var progreso = 100;
			if (menu.hasClass('disabled')) return false;
			$(".mnuses").removeClass("active");
			menu.children("a").addClass("active");
			var loc = window.location.href.split("#");
			var linkurl = "#" + window.btoa(menu.data("id"));
			// _iddetalle_ = menu.data("id");
			listarConsultas(menu.attr("idcursodetallepadre"), menu.data("id"));
			// window.location.href = "#" + loc[1] + "#" + window.btoa(menu.data("id"));
			//console.log(lastindex,typelink);
			var _urltmp_ = '';
			clearInterval(_tiempologueo);
			_tiempologueo = setInterval(_tiempologueofn, _tiempo);
			console.log(typelink);
			if (lastindex == -1 && typelink == 'smartquiz') {
				clearInterval(_tiempologueo);
				//contentpages.removeClass('embed-responsive embed-responsive-16by9');
				link = _sysUrlSitio_ + link.substring(link.lastIndexOf("/quiz/"));
				var idexamen = link.substring(link.indexOf('ver/?idexamen=') + 14);
				idexamen = idexamen.substring(0, idexamen.indexOf('&idproyecto'));
				dandoidexamen = "exa" + idexamen;
				document.cookie = "dandoidexamen=" + dandoidexamen + "; path=/";
				tipoexamen = '&tipoexamen=A&tipo=A&idcursodetalle=' + menu.attr('idcursodetallepadre') + '&examencurso=1&idcc=' + __idcomplementario + '&idpestania=' + menu.attr('data-id') + '&idgrupoauladetalle=' + __idgrupoauladetalle;
				var urltmpreturn = _sysUrlBase_ + 'notas_quiz/resultados/?idcurso=<?php echo $this->idcurso; ?>&idexamen=' + idexamen + '&iddetalle=' + menu.attr('idcursodetallepadre') + tipoexamen;
				//console.log("abel tipoexamen",tipoexamen);
				if (!menu.hasClass('terminado')) {
					if (bloqueodeexamenes == false) {
						estadoprogreso = 'T';
						progreso = 100;
					} else if (bloqueodeexamenes == 'si') {
						estadoprogreso = 'P';
						progreso = 50;
						tipoexamen += '&urlback=' + btoa(urltmpreturn);
						__menu__ = menu;
					}
				}
				if (oIdHistorial.examen == 0) {
					registrarFecha('E');
				} else {
					editarHistoriaSesion(oIdHistorial.examen);
				}
				//console.log('comprobar si termino examen');
				comprobarsiterminoexamen_();
				var idexamen_ = '?idexamen=' + idexamen;
				var urltmp_ = idexamen_ + '&pr=PY' + idproyecto;
				//urltmp_+='&id=<?php echo $this->usuActivo["idpersona"] ?>';
				//urltmp_+='&u=<?php echo $this->usuActivo["usuario"] ?>';
				//urltmp_+='&p=<?php echo $this->usuActivo["clave"] ?>';
				urltmp_ += '&idcursodetalle=' + menu.attr('idcursodetallepadre');
				urltmp_ += '&tipo=A';
				var idcurso_ = '&idcurso=' + __idcurso;
				idcurso_ += '&tipoexamen=A';
				idcurso_ += '&examencurso=1';
				idcurso_ += '&idpestania=' + menu.attr('data-id');
				urltmp_ += idcurso_ + '&idcc=' + __idcomplementario + '&idgrupoauladetalle=' + __idgrupoauladetalle;
				//urltmp_+='&urlback=';
				//urltmp_+='&callback='+_sysUrlBase_+'notas_quiz/guardarnotas_quiz/'+idexamen_+idcurso_;
				var lastindex = link.indexOf("?");
				if (lastindex == -1) {
					link = link + '?idcurso=' + tipoexamen;
				} else link = link + '&idcurso=' + __idcurso + tipoexamen + idcurso_;
				_urltmp_ = _sysUrlBase_ + 'quiz/examenes/resolver/' + urltmp_ + `&idioma=${__oCurso.idioma}`;
				link=_urltmp_;
				typelink = 'html';
			} else {
				link = _sysUrlBase_ + link.substring(lastindex);
				if (oIdHistorial.sesion == 0) {
					registrarFecha('S');
				} else {
					editarHistoriaSesion(oIdHistorial.sesion, 2);
				}
				var lastindex = link.indexOf("?");
				if (lastindex == -1) {
					link = link + '?idcurso=' + idcurso + tipoexamen;
				} else link = link + '&idcurso=' + idcurso + tipoexamen;

			}
			var _selmenu = $('._btnversesion[data-iddetalle="' + menu.attr('idcursodetallepadre') + '"]');
			var _idbitacora_alum_smartbook = _selmenu.attr('idbitacoraalumno') || -1;
			//if (_idbitacora_alum_smartbook != -1) {
			totalpestanias = menu.parent().children('._vercontenido_').length;
			__sysajax({
				fromdata: {
					'idcurso': __idcurso,
					'idbitacora_alum_smartbook': _idbitacora_alum_smartbook,
					'pestania': menu.attr('data-nombre'),
					'idsesionB': menu.attr('data-id'),
					'total_pestanias': totalpestanias,
					'progreso': progreso,
					'idsesion': menu.attr('idcursodetallepadre'),
					'idcomplementario': __idcomplementario,
					'idgrupoauladetalle': __idgrupoauladetalle,
				},
				showmsjok: false,
				url: _sysUrlBase_ + 'json/bitacora_smartbook/guardar/',
				async: false,
				callback: function(rs) {
					var postdata = {};
					termino = rs.termino;
					progreso = rs.progreso;
					if (termino == true) {
						_selmenu.attr('progreso', 100).addClass('terminado');
						_selmenu.next().removeClass('disabled').removeAttr('disabled');
					}
					if (link != '' && typelink != '') {
						if (typelink == "pdf") {
							link = menu.attr('data-link');
							lastindex = link.lastIndexOf("static/");
							link = _sysUrlBase_ + link.substring(lastindex);
						} else if (typelink == 'multioptions') {
							var _linkjson_ = menu.attr('data-link') || '';
							console.log("elelelel",_linkjson_)
							if (_linkjson_ != '') {
								_linkjson_ = atob(_linkjson_);
								_linkjson_ = JSON.parse(_linkjson_);
								var mostrarcomo = _linkjson_.mostrarcomo;
								if (mostrarcomo == 'slider') {
									(async () => {
										var html = await tablamultioptions(_linkjson_.menus);
										contentpages.html(html.html()).removeClass('embed-responsive embed-responsive-16by9');

										contentpages.find('ol.carousel-indicators').remove();
										menu.addClass('terminado');
										menu.attr('progreso', rs.progreso);
										let arrPlayers = contentpages.find('.cont-vimeo-player');
										if (arrPlayers.length > 0) {
											for (let index = 0; index < arrPlayers.length; index++) {
												const player = arrPlayers[index];
												new Vimeo.Player(player);
											}
										}
										continuarPestanias(menu);
									})();
									return;
								} else {
									menu.addClass('terminado');
									menu.attr('progreso', rs.progreso);
									_linkmenujson_ = _linkjson_.menus;
									if (loc.length > 2) {
										var idurlsel = window.atob(loc[2]);
										$.each(_linkmenujson_, function(i, v){
											//console.log(menu,_selmenu);											
											//console.log(idurlsel,menu.attr('id')+'_'+i);								
											if ('mnuse' + idurlsel == menu.attr('id')) {
												link = _sysUrlBase_ + $('#mnuse' + idurlsel + '_0').children('a').attr('link') || '';
												typelink = $('#mnuse' + idurlsel + '_0').children('a').attr('tipo') || '';
												linkurl = "#" + window.btoa('mnuse' + idurlsel + '_0');
												console.log("hererhehehrehre");
											} else if (idurlsel == menu.attr('id') + '_' + i) {
												var txtid = '#' + idurlsel;
												typelink = $('#' + idurlsel).children('a').attr('tipo') || '';
												link = $('#' + idurlsel).children('a').attr('link') || '';
												if (typelink != "enlaceyoutube") {
													link = _sysUrlBase_ + link;
												}
												linkurl = "#" + window.btoa(idurlsel);
												console.log("pepepepepeppe");
											}
											//console.log(v);
										})
										//contentpages.removeClass('embed-responsive embed-responsive-16by9');
									}
									continuarPestanias(menu);
									menu = menu.children('ul').children('li').eq(0).children('a').trigger('click');
									return;
								}
							}
						} else if (typelink == 'imagen') {
							contentpages.removeClass('embed-responsive embed-responsive-16by9');
						} else if (typelink == 'enlaceyoutube' || typelink == 'enlacevimeo') {
							link = menu.attr('data-link');
						} else if (typelink == "enlacesinteres") {
							contentpages.removeClass('embed-responsive embed-responsive-16by9');
						} else if (typelink == 'menuvaloracion' || typelink == 'zoom' || typelink == 'enlacecolaborativo') {
							contentpages.removeClass('embed-responsive embed-responsive-16by9');
							link = "<?php echo $this->usuActivo['idpersona']; ?>-<?php echo $this->usuActivo['idrol']; ?>-<?php echo $this->idcurso; ?>-<?php echo $this->idgrupoaula; ?>-" + __idcomplementario + '-' + __idgrupoauladetalle;
							_idcursodetalle = menu.attr('idcursodetallepadre');
							postdata.idcursodetalle = _idcursodetalle;
							postdata.idpestana = _idpestana;
						} else if (typelink == 'tabladeaudios') {
							_link = menu.attr('data-link');
							var html = tabladeaudios(_link);
							contentpages.html(html.html()).removeClass('embed-responsive embed-responsive-16by9');
							menu.addClass('terminado');
							menu.attr('progreso', rs.progreso);
							continuarPestanias(menu);
							return;
						} else if (typelink == 'videoconferencia') {
							_link = menu.attr('data-link');
							var html = datosvideoconferencia(_link);
							contentpages.html(html.html()).removeClass('embed-responsive embed-responsive-16by9');
							menu.addClass('terminado');
							menu.attr('progreso', rs.progreso);
							continuarPestanias(menu);
							return;
						/*} else if (typelink == 'foros') {
							_link = menu.attr('data-link');
							var html = htmlforos(_link);
							contentpages.html(html.html()).removeClass('embed-responsive embed-responsive-16by9');
							menu.addClass('terminado');
							menu.attr('progreso', rs.progreso);
							continuarPestanias(menu);
							return;*/
						} else if (typelink == 'foro'|| typelink=='wiki'){							
							postdata={
								idrecurso:menu.attr('data-link'),								
								tiporecurso:typelink,
								idcurso:__oCurso.idcurso,
								nombrecurso:__oCurso.nombre,
								idcursoprincipal:__oCurso.idcursoprincipal,
								idgrupoauladetalle:__idgrupoauladetalle,
								idcc:__idcomplementario,
								idcursodetalle:menu.attr('idcursodetallepadre'),
								idpestania:_idpestana,
								tipo:__oCurso.tipo
							}
							typelink='foroywiki';
							cssadd={
								'background-color':'#fff'
							}							
						} else if (typelink == 'smartenglish') {
							_idcursodetalle = menu.attr('idcursodetallepadre');
							link = _sysUrlBase_ + menu.attr('data-link') + '&idcursodetalle_sc=' + _idcursodetalle + '&idpestania=' + _idpestana + '&idgrupoauladetalle=' + __idgrupoauladetalle;
							_urltmp_=link;
							contentpages.removeClass('embed-responsive embed-responsive-16by9');
							$(".home-course-section-1").hide()
							//console.log('smartenglish_03',link);
						} else if (typelink=='descargacontrolada'){
							var _urltmp_ = menu.attr('data-link') || '';
							postdata={
								link:_urltmp_,
								idcurso:__oCurso.idcurso,
								nombrecurso:__oCurso.nombre,
								idcursoprincipal:__oCurso.idcursoprincipal,
								idgrupoauladetalle:__idgrupoauladetalle,
								idcc:__idcomplementario,
								idcursodetalle:menu.attr('idcursodetallepadre'),
								idpestania:_idpestana,
								tipo:__oCurso.tipo,
								tipomatricula:__tipomatricula,
							}

							cssadd={
								'background-color':'#fff'
							}
						}

						window.location.href = "#" + loc[1] + linkurl;
						_urltmp_ = _urltmp_ != '' && _urltmp_ != null && _urltmp_!=undefined ? _urltmp_ : link;
						if (typelink == 'video' &&	ubicacion == 'vimeo' ) {
							postdata.mydata = {
								link: menu.attr('data-link'),
								ubicacion: ubicacion
							};
							postdata.mydata = JSON.stringify(postdata.mydata);
							postdata.mytype = "json";
						} else {
							postdata.link = _urltmp_;
						}

						//PESTAÑA PESTANIA		
						console.log('PESTANIA',typelink,postdata);				
						__sysajax({
							fromdata: postdata,
							type: 'html',
							showmsjok: false,
							url: _sysUrlSitio_ + '/plantillas/' + typelink,
							callback: function(rs) {
								// if(typelink=='video'||typelink=='html'||typelink=='smartenglish'){
								// 	contentpages.addClass('embed-responsive embed-responsive-16by9').removeClass('_P0');
								// }	
								if($("#NextContentButton").css("display") != "none"){
									let elementNavLink = menu.find(".mnuses.nav-link")
									let arrMenuLink = $("#fq-nav .listaSesiones .mnuses.nav-link")
									let NextContentButton = $("#NextContentButton")
									let indexTO = $("#fq-nav .listaSesiones .mnuses.nav-link").index(elementNavLink)
									const maxLength = arrMenuLink.length - 1
									
									if(indexTO >= maxLength){
										NextContentButton.hide();
									}
									NextContentButton.data("index",indexTO)
								}							
								if (typelink == 'video' || typelink == 'html' || typelink == 'smartenglish') {
									contentpages.attr("class", "col-md-12 embed-responsive embed-responsive-16by9").removeClass('_P0');
									menu.addClass('terminado');
								} else if (typelink == "enlacesinteres") {
									contentpages.attr("class", "col-md-12 embed-responsive").removeClass('_P0');
									menu.addClass('terminado');
								} else if (typelink == "menuvaloracion" || typelink == "zoom" || typelink == "foro" || typelink == "enlacecolaborativo") {
									contentpages.attr("class", "col-md-12 embed-responsive").removeClass('_P0');
									menu.addClass('terminado');
								} else if (typelink == 'imagen' || typelink=='foroywiki' || typelink=='descargacontrolada' || typelink=='audio'  ) {
									contentpages.removeClass('embed-responsive embed-responsive-16by9');
								} else {
									contentpages.attr("class", "col-md-12 embed-responsive embed-responsive-16by9").removeClass('_P0');
								}
								if (menu.hasClass('esrecursopagado') && rolActual == 3) {
									contentpages.html($('#esrecursopagado').html()).removeClass('embed-responsive embed-responsive-16by9').addClass('_P0');
									// return false;
								}
								contentpages.html(rs).css(cssadd);
								if(typelink == "enlacevimeo"){
									$.get(String.prototype.concat("https://vimeo.com/api/oembed.json?url=",postdata.link),{},function(resp){
										contentpages.find("#VideoVimeoContainer").attr("style","")
										contentpages.find("#VideoVimeoContainer").html(resp.html)
									},"json")
								}
								if (img != '') {
									contentpages.addClass('imagenfondo');
								} else contentpages.removeClass('imagenfondo');
								menu.attr('progreso', progreso);
								continuarPestanias(menu);
								if((typelink=='video' || typelink=='imagen') &&(menu.hasClass('estarea') || menu.hasClass('esproyecto'))){
									try{
										agregarFuncionCronometro();
										console.log('Observacion// en video e imagen no se puede poner como tarea  o proyecto ', typelink); // no se acta
									}catch(ex){console.log(ex)}									
								}else{
									$("iframe").on('load', function(ev){
										agregarFuncionCronometro();
										if (rolActual == 3 && (menu.hasClass('estarea') || menu.hasClass('esproyecto'))) {
											var estarea = menu.hasClass('estarea') ? 'estarea' : '';
											var esproyecto = menu.hasClass('esproyecto') ? 'esproyecto' : '';
											var nombre = (_selmenu.text() || '').trim().replace('  ', '');
											if (nombre != '') nombre += ' - ';
											nombre += menu.text();
											nombre = nombre.replace(/^[0-9]\./g, '');
											let instanciaRubrica = {
												idcursodetalle: menu.attr('idcursodetallepadre'),
												idpestania: menu.attr('data-id'),
												idcomplementario: __idcomplementario,
												idgrupoauladetalle: __idgrupoauladetalle,
											}
											postData(_sysUrlBase_ + 'json/rubrica_instancia', instanciaRubrica).then((arrInstancia) => {
												if (arrInstancia.length > 0) {
													_instancia = arrInstancia[0];
													$('#rubrica-icon').removeClass('my-hide');
													postData(_sysUrlBase_ + 'json/rubrica/listado', {
															idrubrica: arrInstancia[0].idrubrica
														})
														.then((arrRubrica) => {
															postData(_sysUrlBase_ + 'json/tareas', {
																idcurso: __idcurso,
																idcomplementario: __idcomplementario,
																idalumno: __idalumno,
																idsesion: _instancia.idcursodetalle,
																idpestania: _instancia.idpestania,
																idgrupoauladetalle: __oCurso.idgrupoauladetalle
															}).then((arrTareas) => {
																if (arrTareas.length > 0) {
																	let tarea = arrTareas[0];
																	if (tarea.fecha_calificacion != null) {
																		$('#cont-nota-rubrica').removeClass('my-hide');
																		$('#nota-tp-rubrica').html(tarea.nota)
																	}
																}
															})
															renderCalificarRubrica(arrRubrica, document, mode = 1)
														})

												}
											});

											$('#estareaoproyecto').find('i').attr('idcursodetalle', menu.attr('idcursodetallepadre')).attr('idcursopestania', menu.attr('data-id') || 0).addClass(estarea + ' ' + esproyecto).attr('data-link', menu.attr('data-link')).attr('data-tipolink', menu.attr('data-type')).attr('data-nombre', nombre).attr('data-descripcion', menu.attr('data-descripcion') || '').attr('criterios', menu.attr('criterios') || '').attr('data-nombre', __oCurso.nombre + ' - ' + menu.attr('data-nombre'));
											$('#rubrica-icon').attr('data-link', '');
											$('#estareaoproyecto').find('.btnsubirfilealumno').show();
											$('#estareaoproyecto').find('.btndescargartrabajo').show();
											contentpages.append($('#estareaoproyecto').html());
										} else {
											if (typelink == 'pdf') {
												$('#estareaoproyecto').find('i').attr('idcursodetalle', menu.attr('idcursodetallepadre')).attr('idcursopestania', menu.attr('data-id') || 0).addClass(estarea + ' ' + esproyecto).attr('data-link', menu.attr('data-link')).attr('data-tipolink', menu.attr('data-type')).attr('data-nombre', nombre).attr('data-descripcion', menu.attr('data-descripcion') || '').attr('criterios', menu.attr('criterios') || '').attr('data-nombre', __oCurso.nombre + ' - ' + menu.attr('data-nombre'));
												$('#estareaoproyecto').find('.btndescargartrabajo').show();
												$('#estareaoproyecto').find('.btnsubirfilealumno').hide();
												contentpages.append($('#estareaoproyecto').html());
											}
										}
									})//endif iframe load event
								}
							}//endif callback
						})
					}
				}
			})
			//}
		});
		_diseniarcurso();
		_diseniarIndice({}, false);
		var contab = 0;
		// working
		$('#contentpages').on('click', ' i.btnsubirfilealumno', function(ev) {
			var htmltarea = $('#subirtarea').clone(true);
			htmltarea.find('.idtab').each(function(i, v) {
				if ($(v).hasClass('isa')) $(v).attr('href', $(v).attr('href') + contab);
				$(v).attr('id', $(v).attr('id') + contab);
			})
			var el = $(this);
			var tipotarea = '';
			if (el.hasClass('estarea') && el.hasClass('esproyecto')) {
				htmltarea.find('.estareaproyecto').show();
				tipotarea = 'TP';
			} else if (el.hasClass('estarea')) {
				htmltarea.find('.esproyecto').remove();
				tipotarea = 'T';
			} else if (el.hasClass('esproyecto')) {
				htmltarea.find('.estarea').remove();
				tipotarea = 'P';
			}
			const flujoSubirTarea = () => {
				var dt = {
					html: htmltarea
				};
				//console.log('dt',dt);
				var md = __sysmodal(dt);
				var _cargarmensajesyarchivos = function() {
					var formData = new FormData();
					// formData.append('idcurso', parseInt(idcurso));
					formData.append('idcurso', parseInt(__idcurso));
					formData.append('idsesion', parseInt(el.attr('idcursodetalle')));
					formData.append('idpestania', parseInt(el.attr('idcursopestania') || '0'));
					formData.append('tipo', tipotarea);
					formData.append('idcomplementario', __idcomplementario);
					formData.append('idgrupoauladetalle', __idgrupoauladetalle);
					__sysajax({
						fromdata: formData,
						url: _sysUrlBase_ + 'json/tareas/archivosalumnoymensajes',
						showmsjok: false,
						callback: function(rs) {
							$pnlmensajes = md.find('.mensajeenviados #mensajes');
							if (rs.mensajes) {
								$pnlmensajes.html('');
								var yo = '<?php echo $this->usuActivo["idpersona"] ?>';
								$.each(rs.mensajes, function(i, v) {
									var fotouser = (v.foto == '' || v.foto == null) ? 'static/media/usuarios/user_avatar.jpg' : v.foto;
									var ifoto = fotouser.lastIndexOf('static/');
									if (ifoto == -1) {
										fotouser = _sysUrlBase_ + 'static/media/usuarios/user_avatar.jpg';
									} else fotouser = _sysUrlBase_ + fotouser.substring(ifoto);
									if (v.idusuario == yo) {
										var msj = '<div class="mensaje-autor"><img src="' + fotouser + '" alt="" class="foto img-responsive"><div class="flecha-izquierda"></div><div class="contenido"><b>' + v.usuario + '</b><br>' + v.mensaje + '</div><div class="fecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
									} else {
										msj = '<div class="mensaje-amigo" ><div class="contenido">' + v.mensaje + '</div><div class="flecha-derecha"></div><img src="' + fotouser + '" alt="" class="foto img_responsive"><div class="fecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
									}
									$pnlmensajes.append(msj);
								})
							}
							$pnlarchivos = md.find('.archivosenviados table tbody');
							if (rs.archivos) {
								$pnlarchivos.html('');
								$.each(rs.archivos, function(i, v) {
									var descripcion = v.media.split("/trabajos/")[1].split("?")[0];
									var html = '<tr><td><a href="' + _sysUrlBase_ + v.media + '" target="_blank" download class="btn btn-sm"> <i class="fa fa-download"></i> ' + descripcion + '</a></td><td>' + v.fecha_registro + '</td><td class="text-center"> <i class="fa fa-trash borrararchivosubido" style="cursor:pointer" id="' + v.idtarearecurso + '"></i></tr>';
									$pnlarchivos.append(html);
								})
							}
						}
					})
				}
				var _guardartarea = function(solomsj = false) {
					var link = el.attr('data-link');
					var tipolink = el.attr('data-tipolink');
					if (tipolink == 'smartquiz') link = _sysUrlSitio_ + link.substring(link.lastIndexOf("/quiz/"));
					else link = _sysUrlBase_ + link.substring(link.lastIndexOf("static/"));
					var tipomedia = md.find('#infolink').attr('tipolink') || '';
					if (tipomedia == 'texto' || tipomedia == 'link') mediainfo = md.find('#infolink').attr('value') || '';
					else mediainfo = md.find('#infolink').val() || '';
					if (mediainfo == undefined) mediainfo = '';
					$mensaje = md.find('#_mensaje_').val() || '';
					if (mediainfo != '' && $mensaje != '') {
						$mensaje = '<a href="' + _sysUrlBase_ + mediainfo + '" class="btn btn-primary btn-xs" download="" target="_blank"><i class="fa fa-download"></i> File</a><br>' + $mensaje;
					}
					var formData = new FormData();
					formData.append('tipo', tipotarea);
					//formData.append('idcurso', parseInt(idcurso));
					formData.append('idcurso', parseInt(__idcurso));
					formData.append('idsesion', parseInt(el.attr('idcursodetalle')));
					formData.append('idpestania', parseInt(el.attr('idcursopestania') || '0'));
					formData.append('recurso', link);
					formData.append('tiporecurso', tipolink);
					formData.append('iddocente', iddocente);
					formData.append('nombre', el.attr('data-nombre') || '');
					formData.append('descripcion', el.attr('data-descripcion') || '');
					formData.append('media', mediainfo);
					formData.append('tipomedia', tipomedia);
					formData.append('criterios', el.attr('criterios') || '');
					formData.append('idcomplementario', __idcomplementario);
					formData.append('idgrupoauladetalle', __idgrupoauladetalle);
					if (solomsj) {
						formData.append('solomensaje', true);
						formData.append('mensaje', md.find('#_mensaje2_').val() || '');
						md.find('#_mensaje2_').val('');
					} else {
						formData.append('mensaje', $mensaje);
						md.find('#_mensaje_').val('');
					}
					__sysajax({
						fromdata: formData,
						url: _sysUrlBase_ + 'json/tareas/guardar',
						callback: function(rs) {
							_cargarmensajesyarchivos();
						}
					})
				}
				md.on('click', '.salirmodal', function(ev) {
					__cerrarmodal(md, true);
				}).on('click', '.btnguardartarea', function(ev) {
					_guardartarea(false);
				}).on('click', '.btntipofile', function(ev) {
					var el_ = $(this);
					el_.siblings().removeClass('btn-primary').addClass('btn-secondary');
					el_.addClass('btn-primary').removeClass('btn-secondary');
					var title = (el_.attr('title') || 'pdf').toLowerCase();
					var aquitipofile = md.find('#aquitipofile');
					if (title == 'texto') {
						aquitipofile.html('<br><textarea id="infolink" tipolink="texto" class="form-control" rows="5" placeholder="Copie o escriba su texto de repuesta"  style="resize: none;"></textarea>');
					} else if (title == 'imagen') {
						var idimagen = __idgui();
						aquitipofile.html('<input type="hidden" id="infolink" tipolink="imagen" ><div><img src="" class="img-responsive" id="' + idimagen + '" style="max-width:130px; max-height:130px;"></div>');
						$img = aquitipofile.find('#' + idimagen);
						__subirfile_({
							file: $img,
							typefile: 'imagen',
							uploadtmp: true,
							guardar: true,
							dirmedia: 'trabajos/'
						}, function(rs) {
							//console.log(rs);
							let f = rs.media.replace(url_media, '');
							aquitipofile.find('#infolink').attr('value', f);
							$img.attr('src', url_media + f);
						});

					} else if (title == 'video') {
						var idimagen = __idgui();
						aquitipofile.html('<input type="hidden" id="infolink"  tipolink="video"><div><video controls src="" class="img-responsive" id="' + idimagen + '" style="max-width:130px; max-height:130px;"></div>');
						$img = aquitipofile.find('#' + idimagen);
						__subirfile_({
							file: $img,
							typefile: 'video',
							uploadtmp: true,
							guardar: true,
							dirmedia: 'trabajos/'
						}, function(rs) {
							let f = rs.media.replace(url_media, '');
							aquitipofile.find('#infolink').attr('value', f);
							$img.attr('src', url_media + f);
						});

					} else if (title == 'audio') {
						var idimagen = __idgui();
						aquitipofile.html('<input type="hidden" id="infolink"  tipolink="audio"><div><audio controls src="" class="img-responsive" id="' + idimagen + '"></div>');
						$img = aquitipofile.find('#' + idimagen);
						__subirfile_({
							file: $img,
							typefile: 'audio',
							uploadtmp: true,
							guardar: true,
							dirmedia: 'trabajos/'
						}, function(rs) {
							let f = rs.media.replace(url_media, '');
							aquitipofile.find('#infolink').attr('value', f);
							$img.attr('src', url_media + f);
						});

					} else if (title == 'html') {
						var idimagen = __idgui();
						aquitipofile.html('<input type="hidden" id="infolink"  tipolink="html"><div><a target="_blank" src="img" class="img-responsive" id="' + idimagen + '">Link subido</a></div>');
						$img = aquitipofile.find('#' + idimagen);
						__subirfile_({
							file: $img,
							typefile: 'html',
							uploadtmp: true,
							guardar: true,
							dirmedia: 'trabajos/'
						}, function(rs) {
							let f = rs.media.replace(url_media, '');
							aquitipofile.find('#infolink').attr('value', f);
							$img.attr('href', url_media + f);
						});
					} else if (title == 'documento') {
						var idimagen = __idgui();
						aquitipofile.html('<input type="hidden" id="infolink"  tipolink="documento"><div><a target="_blank" src="img" class="img-responsive" id="' + idimagen + '">Documento subido</a></div>');
						$img = aquitipofile.find('#' + idimagen);

						__subirfile_({
							file: $img,
							typefile: 'documento',
							uploadtmp: true,
							guardar: true,
							dirmedia: 'trabajos/'
						}, function(rs) {
							let f = rs.media.replace(url_media, '');
							aquitipofile.find('#infolink').attr('value', f);
							$img.attr('href', url_media + f);
							Swal.close();
						}, true);

					} else if (title == 'link') {
						aquitipofile.html('<br><input id="infolink"  tipolink="link" value="" placeholder="Copie o escriba el enlace " class="form-control"> ');
					}
				}).on('click', '.borrararchivosubido', function(ev) {
					var el_ = $(this);
					var formData = new FormData();
					formData.append('idtarearecurso', parseInt(el_.attr('id')));
					__sysajax({
						fromdata: formData,
						url: _sysUrlBase_ + 'json/tareas_archivosalumno/eliminar',
						showmsjok: false,
						callback: function(rs) {
							el_.closest('tr').remove();
						}
					})
				}).on('click', '.btnsendmensaje', function(ev) {
					_guardartarea(true);
				})
				_cargarmensajesyarchivos();
				contab++;
			}
			let idsesion = el.attr('idcursodetalle');
			let idpestania = el.attr('idcursopestania');
			
			if (parseInt(idsesion) > parseInt(idpestania)) { //no hay pestañas
				idpestania = 0
			} else {
				idpestania = idpestania;
			}
			let cronTarPr = {
				idcurso: __idcurso,
				idcomplementario: __idcomplementario || 0,
				idsesion,
				idpestania,
				idgrupoauladetalle: __idgrupoauladetalle,
			};
			const fechaTarea = new FechaTarea({
				flujo: flujoSubirTarea
			});
			// la funcion de bloqueo no se activara para  cursos de tipo 01 o que el complementario =0

			//if(__idcomplementario>0)
				fechaTarea.bloquearTarea(cronTarPr);
		}).on('click', '.carousel-indicators,video', function(ev) {
			$(this).closest('.carousel').find('video').each(function(i, v) {
				$(v).trigger('pause');
			})
		})

		$('.content').on('click', 'i.btndescargartrabajo', function(ev) {
			var _link = $(this).attr('data-link') || '';
			_link = _link.replace(_sysUrlBase_, '');
			console.log(_link);
			if (_link == '') {
				//$(this).hide();
			} else {
				var nom = ($(this).attr('data-nombre') || 'sin nombre').replace(/ /g, '');
				// var nombrearchivo=nom+'.'+_link.substring(_link.lastIndexOf('.'));
				var ext = _link.split(".");
				ext = ext[ext.length - 1]
				ext = ext.split("?");
				var nombrearchivo = nom + '.' + ext[0];
				var save = document.createElement('a');
				save.href = _sysUrlBase_ + _link;
				save.target = '_blank';
				save.download = nombrearchivo;
				save.click()
			}
		})

		$('.marcopage').on('click', '._vercontenidomenu_', function(ev) {
			ev.preventDefault();
			ev.stopPropagation();
			var el = $(this);
			typelink = el.attr('tipo') || '';
			link_ = el.attr('link') || '';
			var demultioptions=el.hasClass('demultioptions')?true:false;
			var verificarsiesworkbook=((el.closest('ul').parent('li').attr('data-nombre')||'').toLowerCase()=='workbook'?true:false);
			if (el.length == 0) return;
			// console.log("asd")
			if (typelink != "enlaceyoutube") {
				if(typelink == "enlacevimeo"){
					link_ = link_;
				}else{
					link_ = _sysUrlBase_ + link_;
				}
			}
			var loc = window.location.href.split("#");
			let ubicacion = el.attr('ubicacion');
			mypostdata = {};
			if (typelink == 'video' &&
				ubicacion == 'vimeo'
			) {
				mypostdata.mydata = {
					link: el.attr('link'),
					ubicacion: ubicacion
				};
				mypostdata.mydata = JSON.stringify(mypostdata.mydata);
				mypostdata.mytype = "json";
			} else {
				mypostdata.link = link_;
			}

			if(typelink=='word' || typelink=='excel' || typelink=='power point'){
				$('#contentpages').html('<iframe src="https://docs.google.com/gview?url=' + link_ + '&amp;embedded=true" style="height:85%; width:100%; "/>');
				$("iframe").on('load', function(ev) {
					agregarFuncionCronometro();
				})
				return;
			}
			
		   console.log('MENUS DE PESTANIA',typelink);
			__sysajax({
				fromdata: mypostdata,
				type: 'html',
				showmsjok: false,
				async: false,
				url: _sysUrlSitio_ + '/plantillas/' + typelink,
				callback: function(rs) {
					// console.log("2_2");
					// console.log(el, el.closest('li').attr('id'));
					window.location.href = "#" + loc[1] + "#" + window.btoa(el.closest('li').attr('id'));
					//console.log('aqui',rs);
					$('#contentpages').html(rs);
					if(typelink == "enlacevimeo"){
						$.get(String.prototype.concat("https://vimeo.com/api/oembed.json?url=",mypostdata.link),{},function(resp){
							$('#contentpages').find("#VideoVimeoContainer").attr("style","")
							$('#contentpages').find("#VideoVimeoContainer").html(resp.html)
						},"json")
					}
					//$("iframe").attr("onload", "agregarFuncionCronometro();");
					if (typelink != "enlaceyoutube") {
						if (typelink == 'imagen') $('#contentpages').removeClass('embed-responsive embed-responsive-16by9');
						else $('#contentpages').addClass('embed-responsive embed-responsive-16by9');

						if(demultioptions==true && __tipomatricula==3 && verificarsiesworkbook==true){
							$('#estareaoproyecto').find('i').attr('data-link', '').attr('data-tipolink', '').attr('data-nombre', __oCurso.nombre + ' - ' + el.text());
							$('#estareaoproyecto').find('.btndescargartrabajo').hide();
						}else{
							$('#estareaoproyecto').find('i').attr('data-link', link_).attr('data-tipolink', typelink).attr('data-nombre', __oCurso.nombre + ' - ' + el.text());
							$('#estareaoproyecto').find('.btndescargartrabajo').show();
						}
						//
						$('#contentpages').append($('#estareaoproyecto').html());
					}
					$("iframe").on('load', function(ev) {
						agregarFuncionCronometro();
					})
				}
			});
			//$('#contentpages').html();
		})

		$('body').on('click','.tooltipstatic',function(ev){
			$(this).children('.tooltip').hide();
		})
		$("#NextContentButton").on("click",function(){
			const buttonElement = $(this)
			const arrMenuLink = $("#fq-nav .listaSesiones .mnuses.nav-link")
			const maxLength = arrMenuLink.length - 1
			let indexMenu = buttonElement.data("index") || "0";
			if(indexMenu == -1){
				indexMenu = 1
				arrMenuLink.eq(indexMenu).click();
			}else{
				indexMenu = parseInt(indexMenu) + 1
				if(indexMenu >= maxLength){
					buttonElement.hide();
					indexMenu = maxLength
				}
			}
			buttonElement.data("index",indexMenu)
			arrMenuLink.eq(indexMenu).click();
		})
	}) //end document.ready()

	var iddocente = parseInt(<?php echo !empty($this->idDocente) ? $this->idDocente : '0'; ?>);
	var previewcurso = $('#previewcurso');
	var bookplantilla = previewcurso.children('#bookplantilla');
	var marcopage = bookplantilla.children('.marcopage');
	var contentpages = marcopage.children('#contentpages');

	var mostrarPortada = function() {
		$(".home-course-section-1").show()
		$('#foro').hide();

		window.location.href = "#";
		$(".mnus").removeClass("active");
		$("#contentpages").attr("class", "col-md-12");
		$(".menuSesion").parent().parent().hide();
		var tmp = infoportada;
		var img2 = (tmp.imagen || '').replace(url_media, '');
		var imagen = (img2 == 'undefined' || img2 == '' || img2 == 'static/media/nofoto.jpg') ? '' : ('<img src="' + url_media + img2 + '" class="img img-responsive img-thumbnail" style="max-height:200px; max-width:200px;">');
		if (imagen === "") {
			imagen = '<img src="' + url_media + 'static/cursodefault.png" class="img img-responsive img-thumbnail" style="max-height:200px; max-width:200px;">';
		}
		var htmlimagen = ''; //imagen != '' ? '<div class="col-12 tituloPortada">' + imagen + '</div>' : '';
		var html = "";
		html += '<div class="row text-center">';
		html += '<div class="col-md-12 align-middle" style="padding-right:2em">';
		html += '<br>';
		if (tmp.titulo != "") {
			html += '<center>';
			html += '<div style="background: rgba(255,255,255, 0.7); border-radius: 5px; padding: 15px; width: 50%;">';
			html += '<h1 style="word-wrap: break-word;">' + tmp.titulo + '</h1>';
			html += '</div>';
			html += '</center>';
		}
		if (tmp.descripcion != "") {
			html += '<br><br>';
			html += '<div style="background: rgba(255,255,255, 0.7); border-radius: 5px; padding: 15px; margin:1ex 4em;">';
			html += '<h5 style="margin: 0;">' + tmp.descripcion + '</h5>';
			html += '<br>';
			html += '</div>';
			html += '<br>';
		}
		//html += htmlimagen;
		var autor = infocurso.datos.autor || '';
		//html += autor!=''<br><b>Autor: ' + ( + '</b><hr>';
		html += '</div>';
		img2 = (tmp.imagenfondo || '').replace(url_media, '');
		var imagenfondo = (img2 == 'undefined' || img2 == '' || img2 == 'static/media/nofoto.jpg') ? '' : (url_media + img2);
		if (imagenfondo === "") {
			imagenfondo = url_media + 'static/fondodefault.png';
		}
		var cssadd = {
			'background-size': 'cover',
			//'background-position': 'center center',
			'background-repeat': 'no-repeat',
			'background-image': 'url(' + imagenfondo + ')',
			'background-color': tmp.colorfondo || '',
			'font-family': tmp.tipotexto || ''
		};
		contentpages.css(cssadd);
		contentpages.html(html);
	}

	var buscartemaContenido = function(itemas, buscar, idpadre) {
		idpadre = idpadre || 0;
		var encontrado = false;
		var rdt = {
			dato: {},
			encontrado: false
		};
		if (itemas.length > 0) {
			itemas.forEach(function(cur, i, v) {
				if (encontrado == false) {
					cur.index = i;
					if (cur.idcursodetalle == buscar.idet && buscar.por == 'idet') {
						encontrado = true;
						cur.index = i;
						if (cur.txtjson == '') cur.txtjson = curtemadefault;
						rdt = {
							dato: cur,
							encontrado: true
						};
					} else if (i == buscar.index && buscar.por == 'index' && cur.idpadre == idpadre) {
						encontrado = true;
						cur.index = i;
						if (cur.txtjson == '') cur.txtjson = curtemadefault;
						rdt = {
							dato: cur,
							encontrado: true
						};
					} else {
						var hijos = cur.hijos || [];
						if (hijos.length > 0) {
							dt = buscartemaContenido(hijos, buscar, idpadre);
							if (dt.encontrado == true) {
								encontrado = true;
								rdt = {
									dato: dt.dato,
									encontrado: true
								};
							}
						}
					}
				}
			})
		}
		return rdt;
	}

	var addTabContenido = function(tmpsesion) {

		var tmpcurtema = {};
		var vercurtema = curtemadefault;
		var mostrartab = false;
		if (tmpsesion != undefined) {
			tmpcurtema = tmpsesion;
			vercurtema = tmpsesion.txtjson;
		} else {
			tmpcurtema = curtema;
			vercurtema = curtema.txtjson;
		}
		contentpages.html('');
		//console.log(tmpsesion);
		if (vercurtema.tipo == '#showpaddopciones') {
			$(".menuSesion").parent().parent().show();
			var options = vercurtema.options || {};
			var hijos = tmpcurtema.hijos || {};
			var pnlmenu = $('.menuSesion').children('.listaSesiones');
			pnlmenu.html("");
			if (hijos.length) {
				$.each(hijos, function(i, v) {
					var li = '<li id="mnuse' + v.id + '" data-iddetalle="' + v.idcursodetalle + '" class="_btnversesion nav-item hvr-pop" tipo="pestaniahijo" ><a class="mnuses nav-link" href="javascript:void(0)">' + v.nombre + '</a></li>';
					pnlmenu.append(li);
				})
			} else {

				$.each(options, function(i, v) {
					img2 = (v.imagenfondo || '').replace(url_media, '');
					img2 = img2 == '/static/media/nofoto.jpg' ? '' : img2;
					var imagen = (img2 == 'undefined' || img2 == '') ? '' : (url_media + img2);
					var nom = ((v.nombre || 'sinnombre').replace(/ /g, "")).trim().toLowerCase();
					/*	1 = autoevaluacion	 */
					var necesitadocente = '';
					var esrecursopagado = '';
					var estarea = '';
					var esproyecto = '';
					if (rolActual == 3) {
						necesitadocente = v.necesitadocente == 'si' ? 'necesitadocente' : '';
						esrecursopagado = v.esrecursopagado == 'si' ? 'esrecursopagado' : '';
						estarea = v.estarea == 'si' ? 'estarea' : '';
						esproyecto = v.esproyecto == 'si' ? 'esproyecto' : '';
					}

					if (pagoRecurso == true) esrecursopagado = '';
					var typepestania = (v.nombre.toLowerCase() == 'autoevaluación' || v.nombre.toLowerCase() == 'autoevaluacion') ? 1 : 0;
					_tienesubmenu = v.type;
					_tmpmenuli = '';
					_mostrarcomo = '';
					__menuslink = '';
					if (_tienesubmenu == 'multioptions') {
						try {
							link = v.link;
							link = atob(link);
							link = JSON.parse(link);
							_mostrarcomo = link.mostrarcomo || 'slider';
							if (_mostrarcomo == 'menu') {
								_tmpmenuli = 'dropdown';
								__menuslink = link.menus;
							}
						} catch (ex) {
							console.log(ex)
						}
					}

					var _linktmp = (v.link || '');
					var _linktypetmp = (v.type || '');
					if (_linktypetmp == 'smartenglish') {
						_linktmp = _linktmp + '&idcursoexterno=' + __idcurso + '&icc=' + __idcomplementario + '&idgrupoauladetalle=' + __idgrupoauladetalle;
						_linktmp = 'smartbook' + _linktmp.substring(_linktmp.lastIndexOf("/ver/?"));
						$(".home-course-section-1").hide()
					}

					var li = '<li criterios="' + v.criterios + '" id="mnuse' + v.id + '" idrecurso="' + tmpcurtema.idrecurso + '" idcursodetallepadre="' + tmpcurtema.idcursodetalle + '" data-id="' + v.id + '" data-nombre="' + nom + '"  data-imagenfondo="' + (v.imagenfondo || '') + '" tipo="pestania" data-imagenfondopagina="' + (v.imagenfondopagina || '') + '" data-color="' + v.color + '" data-colorfondopagina="' + (v.colorfondopagina || '') + '" data-colorfondo="' + v.colorfondo + '" data-mostrarpregunta="' + v.mostrarpregunta + '"  data-ubicacion="' + v.ubicacion + '" data-link="' + _linktmp + '" data-type="' + _linktypetmp + '" class=" ' + (_tmpmenuli == 'dropdown' ? '_vercontenido_ dropdown' : '_vercontenido_ nav-item view view-first hvr-grow') + '    ' + necesitadocente + ' ' + esrecursopagado + '  ' + estarea + '  ' + esproyecto + ' " data-typepestania="' + typepestania + '" style="position: relative;"><a class="mnuses nav-link ' + necesitadocente + ' ' + esrecursopagado + '  ' + estarea + '  ' + esproyecto + ' ' + (_tmpmenuli != '' ? ' dropdown-toggle" data-mostrarpregunta="' + v.mostrarpregunta + '" data-toggle="dropdown" ' : ' "') + ' style="color: black;"> ' + v.nombre + '</a>';
					if (_tmpmenuli != '' && _mostrarcomo == 'menu') {
						li += '<ul class="dropdown-menu" role="menu" x-placement="bottom-start" style="font-size: 12px; font-family: Arial;">';
						$.each(__menuslink, function(i, vl) {
							li += '<li id="mnuse' + v.id + '_' + i + '"><a class="dropdown-item _vercontenidomenu_ demultioptions" tipo="' + vl.tipo + '" href="#" link="' + vl.link + '" ubicacion="' + vl.ubicacion + '">' + vl.nombre + '</a></li>';
						})
						li += '</li></ul>';
					}

					if (v.esrecursopagado == 'si' && rolActual == 3 && pagoRecurso == false)
						li += '<div class="tools esrecursopagado text-center" style="font-size:1.5ex; position:absolute; top:0px; width:100%; left:0px; height:100%;"><i class="fa fa-dollar"></i><br>Pagame</div>';

					li += '</li>';


					var _vermenu = v._vermenu || 'si';

					if (((necesitadocente != '' && tieneDocente == true) || necesitadocente == '') && _vermenu == 'si') {
						pnlmenu.append(li);
						var url_ = window.location.href.split("#");
						if (url_.length > 2) {
							var idurlsel = window.atob(url_[2]);
							//console.log(idurlsel,pnlmenu.find('#'+idurlsel));
							if (pnlmenu.find('#' + idurlsel).length) {
								pnlmenu.find('#' + idurlsel).trigger('click');
								//pnlmenu.find('#'+idurlsel).children('a').trigger('click');
							} else {
								mnuse_active(v.id);
							}
						} else mnuse_active(v.id);
					}
				})
			}
		}
	}

	var continuarSesionContenido = function() {
		//console.log("continuarsesion",controlaravance);
		if (controlaravance == true) {
			oCrudCurso.isBlocked().then((bloquear) => {
				if (bloquear) {
					$("._btnversesion").parent().attr("style", 'cursor: no-drop !important;');
					$('._btnversesion[idnumero!=1][espadre!=1]').addClass('disabled').attr('disabled', 'disabled').attr("style", 'cursor: no-drop !important;');
				}
			})
			__sysajax({
				fromdata: {
					idcurso: __idcurso,
					idusuario: 'yo',
					idcomplementario: __idcomplementario,
					idgrupoauladetalle: __idgrupoauladetalle
				},
				showmsjok: false,
				url: _sysUrlBase_ + 'json/bitacora_alumno_smartbook/',
				async: false,
				callback: function(rs) {
					oCrudCurso.isBlocked().then((bloquear) => {
						if (bloquear) {
							var dt = rs.data;
							var haypendiente = false;
							var hayterminado = false;
							$.each(dt, function(i, v) {
								if (v.estado == 'T') {
									hayterminado = true;
									if (v.idsesion == -1) v.idsesion = 'Encuesta';
									$('._btnversesion[data-iddetalle="' + v.idsesion + '"]').removeClass('disabled').removeAttr('disabled').addClass('terminado').removeAttr("style");
									$('._btnversesion[data-iddetalle="' + v.idsesion + '"]').parent().removeAttr("style");
								} else {
									haypendiente = true;
									$('._btnversesion[data-iddetalle="' + v.idsesion + '"]').removeClass('disabled').removeAttr('disabled').removeAttr("style");
									$('._btnversesion[data-iddetalle="' + v.idsesion + '"]').parent().removeAttr("style");
								}
							})
							if (haypendiente == false && hayterminado == true) {
								$('._btnversesion.disabled').first().removeClass('disabled').removeAttr('disabled').parent().removeAttr("style");
								//$('._btnversesion.terminado').last().next().removeClass('disabled').removeAttr('disabled'). removeAttr("style");
								//$('._btnversesion.terminado').last().parent().next().children().removeClass('disabled').removeAttr('disabled').removeAttr("style");
							}
						}
					})
				}
			})
		}
	}

	function readCookie_(name) {
		return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
	}

	var contarcokie = 0;
	var comprobarsiterminoexamen_ = function() {
		var dandoexamen2 = readCookie_('dandoidexamen');
		contarcokie++;
		//console.log("eeeeeeeeeee",__menu__,dandoexamen2,dandoidexamen);
		if (dandoidexamen == dandoexamen2)
			setTimeout(function() {
				comprobarsiterminoexamen_()
			}, contarcokie < 5 ? 3000 : 1500);
		else {
			//console.log(__menu__,dandoexamen2,'_'+dandoidexamen+'_ok_');
			if (dandoexamen2 == '_' + dandoidexamen + '_ok_') {
				if (__menu__.length > 0) { //activar menu
					__menu__.addClass('terminado');
					oCrudCurso.isBlocked().then((bloquear) => {
						if (bloquear) {

							__menu__.next().removeClass('disabled').removeAttr('disabled').addClass('active');
							__menu__.next().children().removeClass('disabled').removeAttr('disabled').removeAttr("style");
						}
					})

				}

			}
		}
	}

	function contenidoCurso(ev, id, mnuse = "") {
		contentpages.removeClass('embed-responsive embed-responsive-16by9').addClass('_P0').attr("style", "");
		var imagenfondo = $(ev).attr('data-imagenfondopagina') || '';
		imagenfondo = imagenfondo.indexOf("nofoto") >= 0 ? '' : imagenfondo;
		if(HIDE_fq_nav){
			$("#fq-nav").show()
			HIDE_fq_nav = false
		}
		$("#NextContentButton").hide();
		if (imagenfondo == '') contentpages.html($("#preloading").html()).removeClass('imagenfondo');
		else {
			contentpages.css({
				'background-image': 'url(' + _sysUrlBase_ + imagenfondo + ')'
			}).addClass('imagenfondo');
		}
		if (typeof ev == 'string' && ev.includes("Sub")) {
			let li = $(ev).parent().parent();
			li.addClass("menu-open").children("ul").show();
		}
		var _selmenu = $(ev).children("a");
		var _idcursodetalle = typeof _selmenu.data('idcursodetallepadre') != 'undefined' ? _selmenu.data('idcursodetallepadre') : _selmenu.data('iddetalle');

		if (_selmenu.hasClass('disabled')) return false;
		if (_selmenu.hasClass('esrecursopagado') && rolActual == 3) {
			return false;
		}
		$(".mnus").removeClass("active");
		$("#mnu" + id).addClass("active");
		window.location.href = "#" + window.btoa(id) + mnuse;
		//$("#contentpages").css("background-image", "");
		var iddet = _selmenu.attr('data-iddetalle');
		tmpsesion = buscartemaContenido(temas, {
			por: 'idet',
			idet: iddet
		}, 0).dato;
		var _link = '';
		var estadoprogreso = 'P';
		var _urltmp_ = '';
		if (iddet == 'Encuesta') {
			$(".menuSesion").parent().parent().hide();
			_link = 'https://rubricas.eduktvirtual.com/smarquality/dar_examen.php?id_rubrica=38&idalumno=' + 1 + '&idcurso=' + idcurso + '&idemp=' + 1;
			estadoprogreso = 'T';
		} else if (tmpsesion.idcursodetalle != undefined) {
			$(".menuSesion").parent().parent().hide();
			try {
				if (typeof(tmpsesion.txtjson) == 'string') tmpsesion.txtjson = JSON.parse(tmpsesion.txtjson);
			} catch (ex) {}
			if (Object.keys(tmpsesion.txtjson.options).length == 0) { //menus simples
				estadoprogreso = 'T';
				var _link = tmpsesion.txtjson.link;
				var typelink = tmpsesion.txtjson.typelink || '';
				var lastindex = _link.lastIndexOf("static/");
				var tipoexamen = '';
				var nsesion = '';
				//console.log(tmpsesion);
				clearInterval(_tiempologueo);
				_tiempologueo = setInterval(_tiempologueofn, _tiempo);
				console.log(typelink);
				if (lastindex == -1 && typelink == 'smartquiz') {
					clearInterval(_tiempologueo);
					_link = _sysUrlBase_ + 'smart' + _link.substring(_link.lastIndexOf("/quiz/"));
					var nex = _selmenu.attr('esexamen') || 1;
					var texa = _selmenu.attr('totalexamen') || 1;
					var idexamen = _link.substring(_link.indexOf('ver/?idexamen=') + 14);
					idexamen = idexamen.substring(0, idexamen.indexOf('&idproyecto'));
					dandoidexamen = "exa" + idexamen;
					document.cookie = "dandoidexamen=" + dandoidexamen + "; path=/";
					var urltmpreturn = _sysUrlBase_ + 'notas_quiz/resultados/?idcurso=' + __idcurso + '&idexamen=' + idexamen + '&iddetalle=' + iddet + '&idcursodetalle=' + iddet + '&examencurso=1&idcc=' + __idcomplementario + '&idgrupoauladetalle=' + __idgrupoauladetalle;
					__menu__ = _selmenu.parent();

					if (nex == texa) {
						_tipoexamen = 'F';
						tipoexamen = '&tipoexamen=F&idcursodetalle=' + iddet + '&idcc=' + __idcomplementario + '&tipo=F&idgrupoauladetalle=' + __idgrupoauladetalle+'&urlback=' + btoa(urltmpreturn + '&tipo=F&idgrupoauladetalle=' + __idgrupoauladetalle);
						estadoprogreso = 'P';
					} else if (nex == 1) {
						_tipoexamen = 'E';
						tipoexamen = '&tipoexamen=E&idcursodetalle=' + iddet + '&idcc=' + __idcomplementario + '&tipo=E&idgrupoauladetalle=' + __idgrupoauladetalle+'&urlback=' + btoa(urltmpreturn + '&tipo=E&idgrupoauladetalle=' + __idgrupoauladetalle);
						estadoprogreso = 'T';
					} else {
						if (bloqueodeexamenes == false) estadoprogreso = 'T';
						else if (bloqueodeexamenes == 'si') estadoprogreso = 'P';
						_tipoexamen = nex;
						tipoexamen = '&tipoexamen=' + nex + '&tipo=' + nex + '&idcc=' + __idcomplementario + '&idcursodetalle=' + iddet + '&idgrupoauladetalle=' + __idgrupoauladetalle + '&urlback=' + btoa(urltmpreturn + '&tipo=' + nex);
					}

					if (oIdHistorial.examen == 0) {
						registrarFecha('E');
					} else {
						editarHistoriaSesion(oIdHistorial.examen);
					}
					comprobarsiterminoexamen_();
					var lastindex = _link.indexOf("?");
					if (lastindex == -1) {
						_link = _link + '?idcurso=' + __idcurso + tipoexamen;
					} else _link = _link + '&idcurso=' + __idcurso + tipoexamen;

					var idexamen_ = '?idexamen=' + idexamen;
					var urltmp_ = idexamen_ + '&pr=PY' + idproyecto;
					urltmp_ += '&idcursodetalle=' + iddet;
					urltmp_ += '&tipo=' + _tipoexamen;
					var idcurso_ = '&idcurso=' + __idcurso;
					idcurso_ += '&tipoexamen=' + _tipoexamen;
					idcurso_ += '&examencurso=1';
					idcurso_ += '&idpestania=0';
					urltmp_ += idcurso_ + '&idcc=' + __idcomplementario;
					_urltmp_ = _sysUrlBase_ + 'quiz/examenes/resolver/' + urltmp_ + `&idioma=${__oCurso.idioma}&idgrupoauladetalle=${__idgrupoauladetalle}`;
					typelink = 'html';
					_link=_urltmp_;
				} else {
					_link = _sysUrlBase_ + _link.substring(lastindex);
					tipoexamen = '&nsesion=' + (_selmenu.attr('essesion') || 1) + '&tsesion=' + (_selmenu.parent().find('essesion').length);
					var lastindex = _link.indexOf("?");
					if (lastindex == -1) {
						_link = _link + '?idcurso=' + idcurso + tipoexamen;
					} else _link = _link + '&idcurso=' + idcurso + tipoexamen;
				}

				
			}
		}
		if (_selmenu.attr('data-link') != "") {
			estadoprogreso = 'T';
		}
		__sysajax({
			fromdata: {
				idcurso: __idcurso,
				idsesion: (iddet != 'Encuesta' ? iddet : -1),
				estado: estadoprogreso,
				idcomplementario: __idcomplementario,
				idgrupoauladetalle: __idgrupoauladetalle,
			},
			showmsjok: false,
			url: _sysUrlBase_ + 'json/bitacora_alumno_smartbook/guardar/',
			//async:false,
			callback: function(rs) {
				if (iddet == 'Encuesta') {
					tmpsesion.txtjson = {
						tipo: '#showpaddcontenido'
					};
					tmpsesion.idcursodetalle = -1;
					typelink = 'html'
				};
				$('._btnversesion[data-iddetalle="' + iddet + '"]').addClass('_seleccionado ' + (iddet == 'Encuesta' ? 'terminado' : '')).attr('idbitacoraalumno', rs.newid);
				$('._btnversesion[data-iddetalle="' + iddet + '"]').siblings().removeClass('_seleccionado');
				//console.log(tmpsesion.idcursodetalle);
				if (tmpsesion.idcursodetalle != undefined) {
					if (Object.keys(tmpsesion.txtjson.options).length == 0) {
						var _idgrupoauladetalleArr = _idgrupoauladetalleArr = <?php echo !empty($this->idgrupoauladetalleArr) ? json_encode($this->idgrupoauladetalleArr) : '[]'; ?>; //fix
						var postdata = {}; //formdata
						if (typelink == 'imagen' ||typelink == 'audio' ) {
							contentpages.removeClass('embed-responsive embed-responsive-16by9');
						} else if (typelink == 'enlaceyoutube' || typelink == 'enlacevimeo') {
							_link = _selmenu.attr('data-link'); 
						} else if (typelink == "enlacesinteres") {
							contentpages.removeClass('embed-responsive embed-responsive-16by9');
						} else if (typelink == 'menuvaloracion' || typelink == 'zoom' || typelink == 'enlacecolaborativo') {
							contentpages.removeClass('embed-responsive embed-responsive-16by9');
							_link = "<?php echo $this->usuActivo['idpersona']; ?>-<?php echo $this->usuActivo['idrol']; ?>-<?php echo $this->idcurso; ?>-<?php echo $this->idgrupoaula; ?>-" + __idcomplementario + '-' + __idgrupoauladetalle;
							postdata.idgrupoauladetalleArr = _idgrupoauladetalleArr;
							postdata.idcursodetalle = _idcursodetalle;
						} else if (typelink == 'tabladeaudios') {
							_link = _selmenu.attr('data-link');
							var html = tabladeaudios(_link);
							contentpages.html(html.html()).removeClass('embed-responsive embed-responsive-16by9');
							continuarSesionContenido();
							return;
						} else if (typelink == 'multioptions') {
							(async () => {
								_link = _selmenu.attr('data-link');
								if (_link != '') {
									_linkjson_ = atob(_link);
									_linkjson_ = JSON.parse(_linkjson_);
									var html = await tablamultioptions(_linkjson_.menus);
									contentpages.html(html.html()).removeClass('embed-responsive embed-responsive-16by9');
									contentpages.find('ol.carousel-indicators').remove();
									let arrPlayers = contentpages.find('.cont-vimeo-player');
									if (arrPlayers.length > 0) {
										for (let index = 0; index < arrPlayers.length; index++) {
											const player = arrPlayers[index];
											new Vimeo.Player(player);
										}

									}
									continuarSesionContenido();
								}
							})();
							return;
						} else if (typelink == 'videoconferencia') {
							_link = _selmenu.attr('data-link');
							if (_link != '') {
								var html = datosvideoconferencia(_link);
								contentpages.html(html.html()).removeClass('embed-responsive embed-responsive-16by9');
								contentpages.find('ol.carousel-indicators').remove();
								continuarSesionContenido();
								if (mostrarpregunta == "si") {
									listarConsultas(_selmenu.attr('data-iddetalle'), 0);
									$("#foro").addClass("collapsed-card").show();
									$("#foro").children(".card-header").children(".card-tools").children(".btn-collapse").children("i").attr("class", "fas fa-plus");
									$("#foro").children(".card-body").hide();
								} else {
									$("#foro").hide();
								}
							}
							return;
						/*} else if (typelink == 'foros') {
							_link = _selmenu.attr('data-link');
							if (_link != '') {
								var html = htmlforos(_link);
								contentpages.html(html.html()).removeClass('embed-responsive embed-responsive-16by9');
								contentpages.find('ol.carousel-indicators').remove();
								continuarSesionContenido();
								if (mostrarpregunta == "si") {
									listarConsultas(_selmenu.attr('data-iddetalle'), 0);
									$("#foro").addClass("collapsed-card").show();
									$("#foro").children(".card-header").children(".card-tools").children(".btn-collapse").children("i").attr("class", "fas fa-plus");
									$("#foro").children(".card-body").hide();
								} else {
									$("#foro").hide();
								}
							}
							return;*/
						} else if (typelink == 'foro' || typelink == 'wiki'){							
							postdata={
								idrecurso:_selmenu.attr('data-link'),								
								tiporecurso:typelink,
								idcurso:__oCurso.idcurso,
								nombrecurso:__oCurso.nombre,
								idcursoprincipal:__oCurso.idcursoprincipal,
								idgrupoauladetalle:__idgrupoauladetalle,
								idcc:__idcomplementario,
								idcursodetalle:_idcursodetalle,
								idpestania:0,
								tipo:__oCurso.tipo
							}
							typelink='foroywiki';
							contentpages.removeClass('embed-responsive embed-responsive-16by9 _P0').css({'background-color':'#fff'});
						} else if (typelink == 'smartenglish') {
							_link = _selmenu.attr('data-link') + '&idcursoexterno=' + __idcurso + '&icc=' + __idcomplementario+'&idcursodetalle_sc=' + _idcursodetalle + '&idpestania=0';
							_link = _sysUrlBase_ + 'smartbook' + _link.substring(_link.lastIndexOf("/ver/?"));
							contentpages.removeClass('embed-responsive embed-responsive-16by9');
							$(".home-course-section-1").hide()
							//console.log('smartenglish_2',_link);
						} else if (typelink=='descargacontrolada'){
							var _urltmp_ = _selmenu.attr('data-link') || '';
							postdata={
								link:_urltmp_,
								idcurso:__oCurso.idcurso,
								nombrecurso:__oCurso.nombre,
								idcursoprincipal:__oCurso.idcursoprincipal,
								idgrupoauladetalle:__idgrupoauladetalle,
								idcc:__idcomplementario,
								idcursodetalle:_idcursodetalle,
								idpestania:0,
								tipo:__oCurso.tipo,
								tipomatricula:__tipomatricula
							}												
							typelink='descargacontrolada';
							contentpages.removeClass('embed-responsive embed-responsive-16by9 _P0').css({'background-color':'#fff'});
						}
						var mostrarpregunta = _selmenu.attr('data-mostrarpregunta') || 'no';
						if (mostrarpregunta == "si") {
							listarConsultas(_selmenu.attr('data-iddetalle'), 0);
							$("#foro").addClass("collapsed-card").show();
							$("#foro").children(".card-header").children(".card-tools").children(".btn-collapse").children("i").attr("class", "fas fa-plus");
							$("#foro").children(".card-body").hide();
						} else {
							$("#foro").hide();
						}

						if (_link != '' && typelink != '') {
							continuarSesionContenido();

							_urltmp_ = _urltmp_ != '' && _urltmp_ != null && _urltmp_!=undefined ? _urltmp_ : _link;
							if (tmpsesion.txtjson.typelink == 'video' && tmpsesion.txtjson.hasOwnProperty('ubicacion') && tmpsesion.txtjson.ubicacion == 'vimeo' ) {
								postdata.mydata = JSON.stringify(tmpsesion.txtjson);
								postdata.mytype = "json";
							} else {
								postdata.link = _urltmp_;
							}

							//MENUS
							__sysajax({
								fromdata: postdata,
								type: 'html',
								showmsjok: false,
								async: false,
								url: _sysUrlSitio_ + '/plantillas/' + typelink,
								callback: function(rs) {
									//console.log('aqui',rs);
									console.log(typelink);
									if (typelink == 'video' || typelink == 'html' || typelink == 'smartenglish' ) {
										contentpages.attr("class", "col-md-12 embed-responsive embed-responsive-16by9 imagenfondo").removeClass('_P0');

									} else if (typelink == "enlacesinteres") {
										contentpages.attr("class", "col-md-12 embed-responsive imagenfondo").removeClass('_P0');
									} else if (typelink == "menuvaloracion" || typelink == 'zoom' || typelink == 'enlacecolaborativo' || typelink == 'foro') {
										contentpages.attr("class", "col-md-12 embed-responsive imagenfondo").removeClass('_P0');
									} else if (typelink == 'imagen'|| typelink == 'audio') {
										contentpages.removeClass('embed-responsive embed-responsive-16by9');
									} else if(typelink == 'foroywiki' || typelink=='descargacontrolada'){										
										contentpages.removeClass("col-md-12 embed-responsive embed-responsive-16by9 imagenfondo _P0");
									} else {
										contentpages.attr("class", "col-md-12 embed-responsive embed-responsive-16by9 imagenfondo").removeClass('_P0');
									}
									if (_selmenu.hasClass('esrecursopagado') && rolActual == 3) {
										contentpages.html($('#esrecursopagado').html()).removeClass('embed-responsive embed-responsive-16by9').addClass('_P0');
									}
									contentpages.html(rs);
									if(typelink == "enlacevimeo"){
										$.get(String.prototype.concat("https://vimeo.com/api/oembed.json?url=",postdata.link),{},function(resp){
											if(resp.html != null){
												contentpages.find("#VideoVimeoContainer").attr("style","")
												contentpages.find("#VideoVimeoContainer").html(resp.html);
											}
										},"json")
									}
									//$("iframe").attr("onload", "agregarFuncionCronometro();");

									$("iframe").on('load', function(ev) {
										agregarFuncionCronometro();
										if (rolActual == 3 && (_selmenu.hasClass('estarea') || _selmenu.hasClass('esproyecto'))) {
											var estarea = _selmenu.hasClass('estarea') ? 'estarea' : '';
											var esproyecto = _selmenu.hasClass('esproyecto') ? 'esproyecto' : '';
											//console.log(_selmenu);
											let instanciaRubrica = {
												idcursodetalle: _selmenu.attr('data-iddetalle'),
												idcomplementario: <?= $this->idcomplementario ?>,
											}

											postData(_sysUrlBase_ + 'json/rubrica_instancia', instanciaRubrica).then((arrInstancia) => {
												if (arrInstancia.length > 0) {
													_instancia = arrInstancia[0];
													$('#rubrica-icon').removeClass('my-hide');
													postData(_sysUrlBase_ + 'json/rubrica/listado', {
															idrubrica: arrInstancia[0].idrubrica
														})
														.then((arrRubrica) => {
															postData(_sysUrlBase_ + 'json/tareas', {
																idcurso: __idcurso,
																idcomplementario: __idcomplementario,
																idalumno: __idalumno,
																idsesion: _instancia.idcursodetalle,
																idpestania: _instancia.idpestania,
																idgrupoauladetalle: __oCurso.idgrupoauladetalle
															}).then((arrTareas) => {
																if (arrTareas.length > 0) {
																	let tarea = arrTareas[0];
																	if (tarea.fecha_calificacion != null) {
																		$('#cont-nota-rubrica').removeClass('my-hide');
																		$('#nota-tp-rubrica').html(tarea.nota)
																	}
																} else {
																	$('#cont-nota-rubrica').addClass('my-hide');
																	$('#nota-tp-rubrica').html("")
																}
															})
															renderCalificarRubrica(arrRubrica, document, mode = 1)
														})

												}
											});
											$('#estareaoproyecto').find('i').attr('idcursodetalle', _selmenu.attr('data-iddetalle')).attr('idcursopestania', 0).addClass(estarea + ' ' + esproyecto).attr('data-link', _selmenu.attr('data-link')).attr('data-tipolink', _selmenu.attr('data-type')).attr('data-nombre', __oCurso.nombre + ' - ' + _selmenu.text()).attr('data-descripcion', _selmenu.attr('data-descripcion') || '').attr('criterios', _selmenu.attr('criterios') || '');
											$('#rubrica-icon').attr('data-link', '');
											$('#estareaoproyecto').find('.btnsubirfilealumno').show();
											$('#estareaoproyecto').find('.btndescargartrabajo').show();
											contentpages.append($('#estareaoproyecto').html());
										} else {
											if (typelink == 'imagen' || typelink == 'pdf') {
												$('#estareaoproyecto').find('i').attr('idcursodetalle', _selmenu.attr('data-iddetalle')).attr('idcursopestania', 0).addClass(estarea + ' ' + esproyecto).attr('data-link', _selmenu.attr('data-link')).attr('data-tipolink', _selmenu.attr('data-type')).attr('data-nombre', __oCurso.nombre + ' - ' + _selmenu.text()).attr('data-descripcion', _selmenu.attr('data-descripcion') || '').attr('criterios', _selmenu.attr('criterios') || '');
												$('#estareaoproyecto').find('.btnsubirfilealumno').hide();
												$('#estareaoproyecto').find('.btndescargartrabajo').show();
												contentpages.append($('#estareaoproyecto').html());
											}

										}
										// $("div[role=toolbar]").css("opacity", "0");
										// document.querySelector('div[role=toolbar]').style.opacity = 0;
									})
								}
							});
						}
					} else if ('#showpaddopciones') {
						$("#NextContentButton").data("index","-1").show();
						$("#tituloSesion").html('<i class="far fa-bookmark"></i> ' + _selmenu.text());
						addTabContenido(tmpsesion);
					}
				}
				if(isMobile.any()!=null ||($('body').width()<=580 && $('body').hasClass('sidebar-open'))){
					$('body').removeClass('sidebar-open').addClass('sidebar-collapse');
				}
			}
		})
		fixCss();
	}

	var _idiomauser=_sysIdioma_;
    console.log(notificar_vencimiento_curso);
	if(notificar_vencimiento_curso){
		console.log('porvencer');
		try{
			var textos_a_traducir={
				EN:{
					'tu curso esta por vencer pronto':`Your course is about to expire`,
					'dale click en el siguiente link para':`Click on the link to`,	
					'renovar ahora':`RENEW NOW`,		
				},
				PT:{
					'tu curso esta por vencer pronto':`Seu curso vai expirar em breve`,
					'dale click en el siguiente link para':`Clique no seguinte link para`,	
					'renovar ahora':`RENOVAR AGORA`,	
				}
			}

			function _t(texto,idioma){// function que traduce los textos
				//let idioma=infoWeb.idioma;
				if(idioma=='ES') return texto;
				if(textos_a_traducir[idioma]==undefined) return texto;	
				if(textos_a_traducir[idioma][texto.toLowerCase()]==undefined) return texto;
				return textos_a_traducir[idioma][texto.toLowerCase()];
			}
			__sysajax({
				fromdata: {					
					idmatricula:'<?php echo @$this->matricula["idmatricula"]; ?>',
					idpersona: '<?php echo @$this->matricula["idalumno"]; ?>',
				},
				showmsjok: false,
				url: '<?php echo _URL_SKS_; ?>/json/persona_externa/obtenerpais',
				async: false,
				callback: function(rs) {
					//console.log('coooooooooooooooo',rs);
					if(rs.code==200){
						let pais=(rs.data.abreviacion||'PE').toLowerCase();
						let linkpvcompra='https://web.smartenglish4u.com/';
						let __idioma='ES';
						//quitar esta linea 
						//pais='br';
						if(pais=='usa') linkpvcompra+='planes-ingles-online-eeuu';
						else if(pais=='mex') linkpvcompra+='planes-ingles-online-mexico';
						else if(pais=='pe') linkpvcompra+='planes-ingles-online-peru';
						else if(pais=='br'){
							linkpvcompra+='pt/planos-ingles-online-brasil/';
							__idioma='PT';
						}
						linkpvcompra=_sysUrlBase_+'codigos/renovarsuscripcion/?idmatricula=<?php echo @$this->matricula["idmatricula"]; ?>';
						_idiomauser=__idioma;
						var renotificar=function(){
							Swal.fire({
							  position: 'top-end',
							  icon: 'warning',
							  type:'warning',
							  title: _t('Tu curso esta por vencer pronto',__idioma),
							  //text: 'tu curso se vence hoy',
							  html:`${_t('Dale click en el siguiente link para',__idioma)} <a href="${linkpvcompra}" target="_blank"><b>${_t('RENOVAR AHORA',__idioma)}</b</a> `,
							  showConfirmButton: false,
							  timer: 60000,
							  showCloseButton: true,
							}).then(()=>{
								let a=document.createElement('a');
								a.href=linkpvcompra;
								a.target='_blank';
								a.click();
								setTimeout(function(){renotificar()},60000*10);	
							})			
						}
						renotificar();
					}
				}
			})
		}catch(ex){
			console.log(ex);
		}	
	}else{
		//consultar solo para saber el idioma de registro
		try{
			<?php if(!empty($this->matricula["idmatricula"])){?>
			__sysajax({
				fromdata: {					
					idmatricula:'<?php echo @$this->matricula["idmatricula"]; ?>',
					idpersona: '<?php echo @$this->matricula["idalumno"]; ?>',
				},
				showmsjok: false,
				url: '<?php echo _URL_SKS_; ?>/json/persona_externa/obtenerpais',
				async: false,
				callback: function(rs) {
					let __idioma='EN';
					if(rs.code==200){
						let linkpvcompra='https://web.smartenglish4u.com/';
						let pais=(rs.data.abreviacion||'PE').toLowerCase();						
						if(pais=='usa') linkpvcompra+='planes-ingles-online-eeuu';						
						else if(pais=='br'){						
							__idioma='PT';
						}
						_idiomauser=__idioma;						
					}
				}
			})
			<?php } ?>
		}catch(ex){
			console.log(ex);
		}
	}


	var materialdeayuda=function(){
		let linkvideo='https://www.youtube.com/embed/birhE9bXJww';		
		if(isMobile.any()==null){//no es mobile
			if(_idiomauser=='PT') linkvideo='https://www.youtube.com/embed/Q84OXiSgiDk';
		}else{ // si es smobile			
			linkvideo='https://www.youtube.com/embed/guCuv52_Hac';
			if(_idiomauser=='PT') linkvideo='https://www.youtube.com/embed/guCuv52_Hac';
		}
		let marcado=localStorage.getItem('show_modal_material')||'1';
		$modalmaterial=$('#md-material');
		$modalmaterial.find('.modal-body iframe').attr('src',linkvideo);
		let iimodal=$modalmaterial.find('.nomostraralinicio i');
		if(marcado=='0')
			iimodal.removeClass('fa-check-square').addClass('fa-square');
		else{ 
			if(!iimodal.hasClass('fa-check-square'))
				iimodal.removeClass('fa-square').addClass('fa-check-square');
		}
		$modalmaterial.modal({'show':true,'backdrop':'static'});
	}

	$('#md-material').on('click','.nomostraralinicio',function(ev){
		let mostaralinicio=$(this).children('i');
		if(mostaralinicio.hasClass('fa-check-square')){
			mostaralinicio.removeClass('fa-check-square').addClass('fa-square');
			localStorage.setItem('show_modal_material', '0');
		}else{
			localStorage.setItem('show_modal_material', '1');
			mostaralinicio.removeClass('fa-square').addClass('fa-check-square');
		}
	}).on('hide.bs.modal',function(){
		//console.log('cerrando');
		//player.stopVideo();
		$('#md-material').find('#materialidiframe').attr('src','');
	}).on('hidden.bs.modal',function(){
		//console.log('cerrando2');
		$('#md-material').find('.modal-body iframe').attr('src','');
	})

	$('body').on('click','.mostrar_material_ayuda',function(ev){
		materialdeayuda();
	})

	var proyectos_empresa=[46,27];
	var cursosSmartenglis=[];
	//723,742,746,747,748,749  - Blended Adulto
	cursosSmartenglis=cursosSmartenglis.concat([723,742,746,747,748,749]);
	//778,762,763 - Masterclass
	cursosSmartenglis=cursosSmartenglis.concat([778,762,763]);
	//894-899 - smart english autoestudio
	cursosSmartenglis=cursosSmartenglis.concat([894,895,896,897,898,899]);
	//915,916,917,918,919,920,921 - Smartenglish Amazon
	cursosSmartenglis=cursosSmartenglis.concat([915,916,917,918,919,920,921]);
	//908,909.910,911,912,913  - Blended Adolescente
	cursosSmartenglis=cursosSmartenglis.concat([908,909.910,911,912,913]);
	let idcursotmp=parseInt(__oCurso.idcurso);	
	if(cursosSmartenglis.includes(idcursotmp) && proyectos_empresa.includes(idproyecto)){ // condicionar solo cursos de una empresa;
		if((localStorage.getItem('show_modal_material')||'1')=='1'){
			setTimeout(() => { materialdeayuda(); }, 2000);
		}	
		$('.mostrar_material_ayuda').show();
	}else{
		$('._navbar_.enmobil').removeClass('enmobil');		
		$('ul.ulmobil').removeClass('ulmobil');
	}
</script>

<?php if (!empty($this->sence)) : ?>
	<script>
		sence = true; /* OJO: VARIABLE QUE SE DECLARA EN LA PLANTILLA*/
	</script>
	<!-- FORMULARIO que se dispara o ejecuta en el boton de cerrar curso, funcion ubicada en el modulo "top" que compone la PLANTILLA -->
	<form id="frmCerrarSence" name="formPost" method="post" action="<?php echo @$this->rutaSense; ?>CerrarSesion">
		<input type="hidden" name="RutOtec" value="<?php echo @$this->RutOtec; ?>" />
		<input type="hidden" name="Token" value="<?php echo @$this->Token; ?>" />
		<input type="hidden" name="CodSence" value="<?php echo @$this->CodSense; ?>" />
		<input type="hidden" name="CodigoCurso" value="<?php echo @$this->CodigoCurso; ?>" />
		<input type="hidden" name="LineaCapacitacion" value="<?php echo @$this->LineaCapacitacion; ?>" />
		<input type="hidden" name="RunAlumno" value="<?php echo @$this->RunAlumno; ?>" />
		<input type="hidden" name="IdSesionAlumno" value="<?php echo @$this->IdSesionAlumno; ?>" />
		<input type="hidden" name="IdSesionSence" value="<?php echo @$this->sence['IdSesionSence']; ?>" />
		<input type="hidden" name="UrlRetoma" value="<?php echo @$this->urlCerrar; ?>" />
		<input type="hidden" name="UrlError" value="<?php echo @$this->urlCerrar; ?>" />
	</form>
<?php endif; ?>