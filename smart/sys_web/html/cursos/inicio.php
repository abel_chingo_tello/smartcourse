<?php 
$idgui=uniqid(); 
$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
$fileurl=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:$imgcursodefecto;
?>
<style type="text/css">
#contentpages{
	padding: 0px;
}
	#ultabs{ background: #ecf2f9e0; padding: 0.25ex; }	
	#ultabs li b.number{ font-size: 1.5em; }
	#ultabs li.active{ background:#047bfc;}
	#ultabs li.active a { color:#f7f1f1; }
	#ultabs li.active:after { border-left: 16px solid #047bfc; }
	#ultabs li.visto{ background:#047bfc;}
	#ultabs li.visto a { color:#f7f1f1; }
	#ultabs li.visto:after { border-left: 16px solid #047bfc; }
	.vh0{ padding: 0px; }
	#paso02 .paso2tabshow02 , #paso02 .pasotematabshow, ._opcionespestania{ display: none }
	#paso02 .paso2tabshow02.active , #paso02 .pasotematabshow.active, ._opcionespestania.active { display: flex; }
	.cicon{	font-size: 2em;  padding: 0.25ex 0.5ex;  text-align: center;	position: relative;}
	.cicon i{ padding: 0.5ex 1ex;  width: 100%;  text-align: center; }
	.cicon i span{ font-size: 15px; }
	/**/
ul.menumaintop{ display: inline-block; margin-bottom: 0px; padding-left:1ex; padding-top: 1ex; }
ul.menumaintop li{display: inline-block; padding-left: 1ex;}

.showindicepage{max-width: 440px; font-size: 1em; background: #2480dd; color: #333; }
ul.menumaintop ~ .showindicepage{ display: none; }	
ul.menumaintop.active ~ .showindicepage{ position: absolute; top:35px; left:35px; display: block; z-index: 9999; }

.indicetop{ margin: 1px; padding: 1px; padding-bottom: 2px; }
.indicetop > div{ padding: 5px; font-size: 14px; color: #000000cf; background: #fdf9f93d; border: 1px solid #f9f7f7a8; font-weight: bold; cursor: pointer;}

.indicetop .pagevisto:before{ content: "\f00c"; font-family: FontAwesome; font-style: normal; font-weight: normal; color:#28a745;
    font-size: 16px; padding-right: 0.5ex;}


.circular-menu{	margin: 0 auto;	position: relative;}
.menucircle{
	width: 100%;
  	height: 100%;
  	opacity: 0;
    -webkit-transform: scale(0);
  	-moz-transform: scale(0);
  	transform: scale(0);
  	-webkit-transition: all 0.4s ease-out;
  	-moz-transition: all 0.4s ease-out;
  	transition: all 0.4s ease-out;
  	margin: 0 auto;
  	position: relative;
}

.open.menucircle{
  	opacity: 1;
  	-webkit-transform: scale(1);
  	-moz-transform: scale(1);
  	transform: scale(1);
  	width: 100%;
  	height: 100%;
}

.menucircle .menucircleitem{
  	text-decoration: none;
  	background: #eee;
  	border-radius: 50%;
  	color: #bbb;
  	padding: 1ex;
  	display: block;
  	position: absolute;
  	text-align: center;
  	display: flex;
  	justify-content: center;
  	align-content: center;
  	flex-direction: column;
  	z-index: 2;
  	background-size: 100% 100% !important;
}

.menucircle .menucircleitem:hover{
    background: #ccc;
    border-radius: 50%;
    color: #fff;
    padding: 1ex;
    display: flex;
    justify-content: center;
    align-content: center;
    flex-direction: column;
    z-index: 3;
    background-size: 100% 100%;
}
.menucircle ~ .openmenucircle{
	position: absolute;
	cursor: pointer;
	text-decoration: none;
	text-align: center;
	color: #444;
	border-radius: 50%;
  	display: block;
  	padding: 1ex;
  	background: #dde;
  	display: flex;
    justify-content: center;
    align-content: center;
  	flex-direction: column;
  	background-size: 100% 100%;
}

.menucircle ~ .openmenucircle:hover{opacity: 0.75; background-size: 100% 100%;}

</style>
  <link rel="stylesheet" href="<?php echo $this->documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.css">
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.min.js"></script>
<div class="row">
	<div class="col-12 pasotabs active" id="paso01" >
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="margin-bottom: 1.5ex; margin-top: 1ex;">
					<div class="progress">
						<div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-value="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">% completado</div>
					  	<div class="progress-bar bg-warning" role="progressbar" aria-value="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
				</div>
				<div class="col-md-12">
			        <div class="wizard">
			            <ul class="nav nav-wizard" id="ultabs">
			                <li class="active"><a href="#pasoinfo" data-toggle="tab"> <b class="number">1.</b> Datos del curso </a></li>
			                <li><a href="#pasocate" data-toggle="tab"> <b class="number">2.</b> Categorias </a></li>
			                <li><a href="#pasoestruct" data-toggle="tab"> <b class="number">3.</b> Estructura </a></li>
			                <li><a href="#pasosilabus" data-toggle="tab"> <b class="number">4.</b> Sesiones </a></li>
			                <!--li><a  href="#plantilla" data-toggle="tab"> <b class="number">4.</b> Plantilla </a></li-->
			                <!--li><a  href="#contenido" data-toggle="tab"> <b class="number">4.</b> Contenido </a></li-->
			            </ul>
			            <div class="tab-content shadow" id="panelesultabs">			            	
			            	<div class="tab-pane active " role="tabpanel" id="pasoinfo">
			            		<form id="frmdatosdelcurso" method="post" target="" enctype="multipart/form-data">
			            		<input type="hidden" name="idcurso" id="idcurso" value="0">	
			            		<div class="card shadow">
			            			<div class="card-body">	
			            				<div class="row">
			            					<div class="col-6">
			            						<div class="row">
										    		<div class="col-12 form-group">
											            <label style=""><?php echo JrTexto::_('Nombre del curso');?></label> 
											            <input type="text" autofocus name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Curso 01"))?>" >					           
											        </div>
											        <div class="col-6 col-sm-12 form-group">							        	
											            <label style=""><?php echo JrTexto::_('Autor');?></label> 
											            <input type="text" name="autor" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Apellidos y Nombres del autor "))?>">					           
											        </div>
											        <div class="col-6 col-sm-12 form-group">
											            <label style=""><?php echo JrTexto::_('Fecha de publicación');?></label> 
											            <input type="date" name="aniopublicacion" required="required"  class="form-control" placeholder="<?php echo date('Y-m-d'); ?>" >
											        </div>
											        <div class="col-12 form-group">
											            <label style=""><?php echo JrTexto::_('Reseña');?></label> 
											            <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso"))?>"></textarea>
											        </div>										       							        
											        <div class="col-6 col-sm-12 form-group text-left ">		        	
											            <label style=""><?php echo JrTexto::_('Color');?> </label> 
											            <input type="" name="color" value="rgb(0,0,0,0)" class="vercolor form-control" >			       
											        </div>
											    </div>
			            					</div>
			            					<div class="col-6 text-center form-group"> 
			            						<label style="float: none"><?php echo JrTexto::_('Imagen');?></label> 
									            <div style="position: relative;" class="frmchangeimage">
									                <div class="toolbarmouse text-center">                  
									                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
									                  <span class="btn resetfile"><i class="fa fa-trash"></i></span>
									                </div>			               
								                	<img id="verimg" src="<?php echo $imgcursodefecto; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
						                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">			               
									                <small>Tamaño 250px x 360px</small>                
									            </div>
			            					</div>
			            				</div>			            				
			            			</div>
			            			<div class="card-footer text-center">
			            				<button type="button" onclick="redir(_sysUrlBase_+'/');" class="btn btn-warning"><i class=" fa fa-undo"></i> Regresar al listado</button>
									    <button type="submit" class="btn btn-primary" data-pclase="tab-pane" data-showp="#pasocate" ><i class=" fa fa-save"></i> Guardar y continuar</button>
			            			</div>
			            		</div>
			            	</form>		            		
			            	</div>
			            	<div class="tab-pane" role="tabpanel" id="pasocate"></div>
			            	<div class="tab-pane" role="tabpanel" id="pasoestruct">
			            		<div class="card text-center" id="cardestruct">
									<div class="card-body">
								    	<div class="row btnmanagermenu">
								    		<div class="col-12">
								    			<div class="row">
								    				<div class="col-xs-12 col-sm-4 col-sm-4 btnplantilla" data-nombre="blanco" data-idplantilla="0">
								    					<h4>Blanco</h4>
														<a href="javascript:void(0)" class="btn">
															<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/blanco.jpg" class="border img-thumbnail img-responsive">
														</a>
													</div>
								    				<div class="col-xs-12 col-sm-4 col-sm-4 btnplantilla" data-nombre="libro" data-idplantilla="1">
								    					<h4>Libro</h4>
														<a href="javascript:void(0)" class="btn" >
															<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/libro.jpg" class="border img-thumbnail img-responsive">
														</a>
													</div>
													<div class="col-xs-12 col-sm-4 col-sm-4 btnplantilla" data-nombre="marco" data-idplantilla="2">
														<h4>Marco</h4>
														<a href="javascript:void(0)" class="btn">
															<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/marco.jpg" class="border img-thumbnail img-responsive">
														</a>
													</div>                 				
								    			</div>
								    		</div>
								    		<div class="col-12">
								    			<div class="card-footer text-muted">
												  	<button type="button" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
												    <button type="button" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>
												</div>
								    		</div>
								    	</div>
								    </div>
								</div>
			            	</div>
			            	<div class="tab-pane " role="tabpanel" id="pasosilabus"></div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
	<div class="col-12 pasotabs" id="paso02" style="display: none; ">
		<div class="row">
			<div class="col-md-4" style="background: #bbd7ec;">
				<div class="row">
					<div class="col-md-12">
						<div class="progress" style="margin-top: 1ex;">
							<div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">% completado</div>
						  	<div class="progress-bar bg-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>					
				</div>
				<div class="row paso2tabshow02 active" id="showestiloestructura">
					<div class="col-12 text-center">
						<h3>Estilo de Estructura</h3>
					</div>
					<div class="col-md-6 col-sm-8 col-xs-12 form-group">
			            <label style="">Tipo de Letra</label> 
			            <input type="text" name="font" class="form-control" value="Arial">							        
					</div>
					<div class="col-md-6 col-sm-4 col-xs-12 form-group">
			            <label style="">Tamaño de letra (px)</label>
			            <input type="number" name="fontsize" class="form-control" value="12">							        
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group ">						
			            <label style=""><?php echo JrTexto::_('Color de texto');?> </label> 
			            <input type="" name="color" value="rgb(0,0,0,1)" class="vercolor colortexto form-control">			        	
			        </div>
			        <div class="col-md-6 col-sm-6 col-xs-12 form-group text-left ">		        	
			            <label style=""><?php echo JrTexto::_('Color de Fondo');?> </label> 
			            <input type="" name="colorfondo" value="rgb(0,0,0,0)" class="vercolor colorfondo form-control" >			       
			        </div>
			        <div class="col-md-12 form-group text-center">
			            <label style="float: none">Imagen de Fondo</label> 
			            <div style="position: relative;" class="frmchangeimage">
			                <div class="toolbarmouse text-center">                  
			                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
			                  <span class="btn resetfile"><i class="fa fa-trash"></i></span>
			                </div>			               
		                	<img id="verimg" src="<?php echo $imgcursodefecto; ?>" class="img-responsive center-block reset" style="width:60%; height:200px;">
                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">			               
			                <!--small>Tamaño 250px x 360px</small-->
			            </div>				           
			        </div>
			        <div class="col-12 text-center text-muted">
					  	<button type="button" data-showp="#paso01" data-pclase="pasotabs" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
					    <button type="button" data-showp="#showestilopage" data-pclase="paso2tabshow02" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>	
		    		</div>					
	    		</div>
	    		<div class="row paso2tabshow02" id="showestilopage">
					<div class="col-12 text-center">
						<h3>Estilo de Pagina</h3>
					</div>
					<div class="col-md-6 col-sm-8 col-xs-12 form-group">
			            <label style="">Tipo de Letra</label> 
			            <input type="text" name="font" class="form-control" value="Arial">							        
					</div>
					<div class="col-md-6 col-sm-4 col-xs-12 form-group">
			            <label style="">Tamaño de letra (px)</label>
			            <input type="number" name="fontsize" class="form-control" value="12">							        
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group ">						
			            <label style=""><?php echo JrTexto::_('Color de texto');?> </label> 
			            <input type="" name="color" value="rgba(0,0,0,1)" class="vercolor colortexto form-control">			        	
			        </div>
			        <div class="col-md-6 col-sm-6 col-xs-12 form-group text-left ">		        	
			            <label style=""><?php echo JrTexto::_('Color de Fondo');?> </label> 
			            <input type="" name="colorfondo" value="rgba(0,0,0,0)" class="vercolor colorfondo form-control" >			       
			        </div>
			        <div class="col-md-12 form-group text-center">
			            <label style="float: none">Imagen fondo de Paginas</label> 
			            <div style="position: relative;" class="frmchangeimage">
			                <div class="toolbarmouse text-center">                  
			                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
			                  <span class="btn resetfile"><i class="fa fa-trash"></i></span>
			                </div>			               
		                	<img id="verimg" src="<?php echo $imgcursodefecto; ?>" class="img-responsive center-block reset" style="width:60%; height:200px;">
                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">			               
			                <!--small>Tamaño 250px x 360px</small-->
			            </div>				           
			        </div>
			        <div class="col-12 text-center text-muted">
					  	<button type="button" data-showp="#showestiloestructura" data-pclase="paso2tabshow02" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
					    <button type="button" data-showp="#showportada" data-pclase="paso2tabshow02" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>	
		    		</div>
	    		</div>
	    		<div class="row paso2tabshow02" id="showportada">
					<div class="col-12 text-center">
						<h3>Información de Portada</h3>
					</div>
					<div class="col-12 form-group">
			            <label style="">Titulo</label> 
			            <textarea name="titulo" class="form-control"></textarea>
					</div>					
					<div class="col-12 form-group text-left ">		        	
			            <label style="">Descripción</label> 
			            <textarea name="descripcion" class="form-control"></textarea>
			        </div>
			        
			        <div class="col-12 form-group text-center">
			            <label style="float: none"><?php echo JrTexto::_('Portada');?></label> 
			            <div style="position: relative;" class="frmchangeimage">
			                <div class="toolbarmouse text-center">                  
			                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
			                  <span class="btn resetfile"><i class="fa fa-trash"></i></span>
			                </div>			               
		                	<img id="verimg" src="<?php echo $imgcursodefecto; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">			               
			                <!--small>Tamaño 250px x 360px</small-->
			            </div>				           
			        </div>
			        <div class="col-12 text-center text-muted">
					  	<button type="button" data-showp="#showestilopage" data-pclase="paso2tabshow02" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
					    <button type="button" data-showp="#showindice" data-pclase="paso2tabshow02" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>	
		    		</div>					
	    		</div>
	    		<div class="row paso2tabshow02" id="showindice">
					<div class="col-12 text-center">
						<h3>Posicion de Indice</h3>
					</div>
					<div class="col-xs-12 col-sm-6 col-sm-6 btnplantillaindice active text-center" data-nombre="top" >
    					<h6>Indice flotante</h6>
						<a href="javascript:void(0)" class="btn">
							<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/indice/top.gif" class="border img-thumbnail img-responsive">
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-sm-6 btnplantillaindice text-center" data-nombre="enpagina">
    					<h6>Indice en Pagina</h6>
						<a href="javascript:void(0)" class="btn">
							<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/indice/enpage.gif" class="border img-thumbnail img-responsive">
						</a>
					</div>
					
					<div class="col-12 text-center text-muted" style="margin-top: 1ex;">
					  	<button type="button" data-showp="#showportada" data-pclase="paso2tabshow02" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
					    <button type="button" data-showp="#showtemaidedit" data-pclase="paso2tabshow02" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>	
		    		</div>		
	    		</div>
	    		<div class="row paso2tabshow02" id="showtemaidedit">
	    			<div class="col-12 text-center">
						<div class="progress" style="margin-bottom: 1.5ex; margin-top: 1ex;">
							<div class="progresstema progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0% completado</div>
							<div class="progress-bar bg-warning" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
	    			</div>
					<div class="col-12 text-center"><h3 class="nombretema">Tema 001</h3></div>
					<div class="col-12 pasotematabshow active" id="addtemacontenido">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12 form-group text-center">
					            <label style="float: none"><?php echo JrTexto::_('Imagen de Fondo');?></label> 
					            <div style="position: relative;" class="frmchangeimage">
					                <div class="toolbarmouse text-center">                  
					                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
					                  <span class="btn resetfile"><i class="fa fa-trash"></i></span>
					                </div>			               
				                	<img id="verimg" src="<?php echo $imgcursodefecto; ?>" class="img-responsive center-block" style="width:60%; max-height:150px;">
		                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">		               
					                <!--small>Tamaño 250px x 360px</small-->
					            </div>				           
					        </div>
							<div class="col-md-6 col-sm-6 col-xs-12 form-group text-left ">		        	
					            <label style=""><?php echo JrTexto::_('Color de fondo');?> </label> 
					            <input type="" name="fondocolor" value="rgba(0,0,0,0)" class="vercolor form-control" >			       
					        </div>
							<div class="col-md-12">						  					
								<a href="javascript:void(0)" data-showp="#showpaddcontenido" data-pclase="pasotematabshow" class="btn btn-block btn-success active btntemaaddinfo text-center" style="margin-bottom: 1.5ex;"><h6>Agregar Contenido</h6></a>
							</div>
							<div class="col-md-12">						  					
								<a href="javascript:void(0)" data-showp="#showpaddopciones" data-pclase="pasotematabshow" class="btn btn-block btn-secondary btntemaaddinfo active text-center" style="margin-bottom: 1.5ex;"><h6>Agregar Opciones</h6></a>
							</div>
							<div class="col-12 text-center text-muted">
							  	<button type="button" data-showp="#showindice" data-pclase="paso2tabshow02" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
							    <button type="button" data-showp="_elegir_" data-pclase="pasotematabshow" data-elegir="btntemaaddinfo" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>	
				    		</div>							
						</div>
					</div>					
					<div class="col-12 pasotematabshow _addopciones" id="showpaddopciones">
						<div class="row _opcionespestania active" id="opciontipomenu">
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<h6>Pestañas Arriba</h6>
								<a href="javascript:void(0)" data-pestania="arriba" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania active btn-success">
									<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/indice/oparriba.png" class="border img-thumbnail img-responsive">
								</a>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<h6>Pestañas Derecha</h6>
								<a href="javascript:void(0)" data-pestania="derecha" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
									<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/indice/opderecha.png" class="border img-thumbnail img-responsive">
								</a>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<h6>Pestañas Abajo</h6>
								<a href="javascript:void(0)" data-pestania="abajo" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
									<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/indice/opabajo.png" class="border img-thumbnail img-responsive">
								</a>
							</div>
							<div class="col-12"><br></div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<h6>Circulos</h6>
								<a href="javascript:void(0)"  data-pestania="circulo" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
									<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/indice/opcirculo.png" class="border img-thumbnail img-responsive">
								</a>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<h6>Menus</h6>
								<a href="javascript:void(0)"  data-pestania="menus" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
									<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/indice/opmenu.png" class="border img-thumbnail img-responsive">
								</a>
							</div>
							<div class="col-12 text-center text-muted">
							  	<button type="button" data-showp="#addtemacontenido" data-pclase="pasotematabshow" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
							    <button type="button" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" data-elegir="btnopcionespestania" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>	
				    		</div>
						</div>
						<div class="col-12 _opcionespestania" id="opcionlistadopestanias">
							<div class="row" style="width: 100%;">
								<div class="col-12 table-responsive" id="verlistadoopciones">								
									<table class="table table-striped">
										<tr class="nocontar"><th colspan="2" class="text-center">Opciones</th></tr>
									</table>
									<div class="col-md-12 text-center" style="margin: 1.5ex auto;"><button type="button" id="btnaddpestania"  class=" btn btn-warning"><i class="fa fa-plus"></i> Agregar </button></div>
									<div class="col-12 text-center text-muted">
									  	<button type="button" data-showp="#opciontipomenu" data-pclase="_opcionespestania" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
									    <button type="button" data-showp="#showppublicar" data-pclase="paso2tabshow02" data-elegir="btnopcionespestania" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>	
						    		</div>								
								</div>
								<div class="col-12" style="display: none;" id="editlistadoopcion">
									<div class="row">
										<div class="col-12 form-group">
								            <label style=""><?php echo JrTexto::_('Nombre de opcion');?></label> 
								            <input type="text" name="nombre" class="form-control" placeholder="Objetivos/sesion/video/manual" >			           
								        </div>
								        <div class="col-md-6 col-sm-6 col-xs-12 form-group ">						
								            <label style=""><?php echo JrTexto::_('Color de texto');?> </label> 
								            <input type="" name="color" value="rgba(0,0,0,1)" class="vercolor colortexto form-control">			        	
								        </div>
								        <div class="col-md-6 col-sm-6 col-xs-12 form-group text-left ">		        	
								            <label style=""><?php echo JrTexto::_('Color de Fondo');?> </label> 
								            <input type="" name="colorfondo" value="rgba(0,0,0,0)" class="vercolor colorfondo form-control" >			       
								        </div>									
									</div>
									<div class="row" >										
										<div class="col-12" id="aquitambienaddcontent">
											
										</div>
										<div class="col-12 text-center text-muted">
										  	<button type="button" id="editpestaniacancel" class="btn btn-warning"><i class="fa fa-cancel"></i> Cancelar </button>
										    <button type="button" id="editpestaniasave" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>	
							    		</div>
									</div>								
								</div>								
				    		</div>
						</div>						
					</div>
					<div class="col-12 pasotematabshow _addcontenido" id="showpaddcontenido">
						<div class="col-md-12">
		        			<div class="row" id="addcontentclone">
		        				<div class="col-md-12 text-left ">		        	
						            <br><label style="">Seleccione plantilla</label> <br>
						            <hr>			       
						        </div>
								<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="texto" title="Texto" data-content="Agregar texto" data-acc="texto">				
									<i class="fa btn btn-block btn-primary fa-text-width"> <!--br><span>Texto</span--></i> 
								</div>				
								<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="imagen" title="Imagen" data-content="Agregar una imagen" data-acc="imagen">
									<i class="fa btn btn-block btn-primary fa-file-image-o"> <!--br><span>Imagen</span--></i> 
								</div>
								<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="audio" title="Audio" data-content="Agregar un audio" data-acc="audio">
									<i class="fa btn btn-block btn-primary fa-file-audio-o"> <!--br><span>Audio</span--></i> 
								</div>
								<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="video" title="Video" data-content="agregar un video" data-acc="video">
									<i class="fa btn btn-block btn-primary  fa-file-movie-o"><!--br><span>Video</span--></i> 
								</div>
								<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="pdf" title="PDF" data-content="Agregar un archivo pdf" data-acc="pdf">
									<i class="fa btn btn-block btn-primary  fa-file-pdf-o"> <!--br><span>PDF</span--></i> 
								</div>
								<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="html" title="HTML" data-content="Agregar contenido HTML - seleccione un zip si incluye mas contenido" data-acc="html">
									<i class="fa btn btn-block btn-primary fa-file-code-o"> <!--br><span>Html</span--></i> 
								</div>
								<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="flash" title="Flash" data-content="Agregar SWF - archivo Flash" data-acc="flash">
									<i class="fa btn btn-block btn-primary fa-file-archive-o"></i> 
								</div>				
								<div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticlock" title="SmartTic Look" data-content="Look" data-acc="smarticlock">
									<i class="fa btn btn-block btn-primary fa-cogs"> <span> L</span></i> 
								</div>
								<div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticlookPractice" title="SmartTic Practice" data-content="Practice" data-acc="smarticlookPractice">
									<i class="fa btn btn-block btn-primary fa-cogs"> <span> P</span></i>
								</div>
								<div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticDobyyourselft" title="SmartTic Do by yourselft" data-content="Do it by yourself" data-acc="smarticDobyyourselft">
									<i class="fa btn btn-block btn-primary fa-cogs"> <span> D</span></i> 
								</div>
								<div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="game" title="Juegos" data-content="Agregar juegos interactivos" data-acc="game">
									<i class="fa btn btn-block btn-primary fa-gamepad"> <!--span>Juegos</span--></i> 
								</div>
								<div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="smartquiz" title="SmartQuiz" data-content="Agregar Examenes" data-acc="smartquiz">
									<i class="fa btn btn-block btn-primary fa-tasks"> <!--br><span>SmartQuiz</span--></i> 
								</div>				
							</div>
							<div class="row" style="margin-top: 1ex;">								
								<div class="col-12 text-center text-muted" >
									<button type="button" data-showp="#addtemacontenido" data-pclase="pasotematabshow" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
							    	<button type="button" data-showp="#showppublicar" data-pclase="paso2tabshow02" data-elegir="btnopcionespestania" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>
				    			</div>
							</div>
						</div>		
					</div>		
	    		</div>
	    		<div class="row paso2tabshow02 publicar" id="showppublicar" style="margin-top: 1em;">
					<div class="col-md-12">						  					
						<a href="javascript:void(0)" class="btn btn-block btn-warning btnpublicar text-center" style="margin-bottom: 1.5ex;"><h3>Publicar</h3></a>				
					</div>
					<div class="col-12 text-center text-muted">
						<button type="button" data-showp="#showtemaidedit" data-pclase="paso2tabshow02" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
						<button type="button" class="btn btn-primary btniralistado"><i class="fa fa-arrow-right"></i> Finalizar</button>	
				    </div>						
	    		</div>	    		
			</div>
			<div class="col-md-8" id="previewcurso" style="height:calc(100vh - 1px); margin: 0px; padding: 0px auto;">
				<link id="mainestilo" rel="stylesheet" type="text/css" data-tmpurl="<?php echo $this->documento->getUrlTema()."/css/"?>" href="<?php echo $this->documento->getUrlTema()."/css/smartbook.css"; ?>">				
				<div class="row tabstop" id="bookplantilla"  >
					<ul class="menumaintop">
				      <!--li><a class="noalumno btnconfigurarSesion" href="javascript:void(0)"><i class="fa fa-cog fa-2x"></i></a></li-->
				      <li><a class="btnshowexit" href="javascript:void(0)" data-link=""><i class="fa fa-sign-out fa-2x"></i></a></li>
				      <li><a class="btnshowsesiones" href="javascript:void(0)"><i class="fa fa-th fa-2x"></i></a></li>
				      <li><a class="btnshowhome" href="javascript:void(0)"><i class="fa fa-home fa-2x"></i></a></li>
				      <li class="nombrecurso"></li>				      	    	    
				    </ul>
				    <div class="showindicepage">
				    	<div class="pnlsesions text-center">
							<h2>Sesiones</h2>
							<div class="row indicetop">
								<div class="cloneindice col-md-6 text-left hvr-glow _btnversesion" hidden data-iddetalle="547"></div>								
							</div>
						</div>
				    </div>					
					<div class="marcopage">
						<div class="menubooktop">						
							<ul class="nav nav-tabs pull-right sysmenumain">						    	
							</ul>
						</div>
						<div class="anillado"></div>
						<div class="col-12" id="contentpages" style="padding: 0px"></div>
					</div>
					<?php // var_dump($_REQUEST); ?>
				</div>
			</div>
		</div>
	</div>	
</div>
<script type="text/javascript">
	var infocurso=<?php echo $this->curso; ?>;
	var nombreusuario='<?php echo $this->usuarionombre;?>';
	var datosdelcurso=infocurso.datos||{};
	var temas=infocurso.temas||{};
	var chkjson=datosdelcurso.txtjson==''||datosdelcurso.txtjson=='1'?{}:datosdelcurso.txtjson;
	var jsoncurso=_isJson(chkjson)?JSON.parse(chkjson):{};
	var idcurso=parseInt(datosdelcurso.idcurso||0);
	var datenow='<?php echo date('Y-m-d'); ?>';
	var imgdefecto='<?php echo $imgcursodefecto; ?>';
	var urlmedia='<?php echo URL_MEDIA; ?>';
	var pasoscurso=10;
	var plantilla=jsoncurso.plantilla||{id:0,nombre:'blanco'};
	var estructura=jsoncurso.estructura||{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:imgdefecto};
	var estilopagina=jsoncurso.estilopagina||{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:imgdefecto};
	var infoportada=jsoncurso.infoportada||{titulo:'',descripcion:'',image:imgdefecto};
	var infoindice=jsoncurso.infoindice||'top';
	var infoavance=jsoncurso.infoavance||0;	
	var curtemadefault={tipo:'#showpaddcontenido',imagenfondo:'',infoavancetema:0,colorfondo:'rgba(0,0,0,0)'};
	var curtema={index:0,idtema:0,txtjson:{tipo:'#showpaddcontenido',imagenfondo:'',infoavancetema:0,colorfondo:'rgba(0,0,0,0)'}};
	var infoavancetema=0;
	var curtemaidpadre=0;
	var curtemaindex=0;
	
	var curtemaoption={};
	var _sysUrlRaiz_='<?php echo URL_RAIZ;?>';

	$(document).ready(function(ev){
		var bookplantilla=$('#bookplantilla');				
		var marcopage=bookplantilla.children('.marcopage');				
		var contentpages=marcopage.children('#contentpages');
		let addcontent=$('#showpaddcontenido._addcontenido #addcontentclone').clone();
		$('#editlistadoopcion #aquitambienaddcontent').append(addcontent);

		let updateprogress=function(newval){
			let vactual=$('.progresscurso').first().attr('aria-value');
			vactual=parseFloat(vactual);
			newval=parseFloat(newval);
			if(vactual<newval){
				$('.progresscurso').first().attr('aria-value',newval);
				$('.progresscurso').css({width:newval+'%'}).text('Curso '+newval+'% completado');
				$('.progresscurso').siblings().css({width:(100 - newval)+'%'});
				//console.log(newval);
			}
		}
		let updateprogresscurso=function(reset){
			reset=reset||false;
			let vactual=$('.progresstema').first().attr('aria-value')||0;			
			if(vactual<infoavancetema||reset==true){
				let citem=temas.length;
				$('.progresstema').attr('aria-value',infoavancetema).css({width:infoavancetema+'%'}).text('Sesion '+infoavancetema+'% completado');
				$('.progresstema').siblings().css({width:(100 - infoavancetema)+'%'});	
			}
		}
		let _backtab=function(){
			let curpa=$('.pasotabs.active');
			let curpaso=curpa.attr('id');
			if(curpaso=='paso01'){
				let curtab=$('#ultabs').find('a.active.show');
				let curtaba=curtab.attr('href');
				let liback=curtab.closest('li').prev();
				liback.children('a').tab('show');				
			}
		}
		let _nexttab=function(){
			let curpa=$('.pasotabs.active');
			let curpaso=curpa.attr('id');
			if(curpaso=='paso01'){
				let curtab=$('#ultabs').find('li.active').last();
				let curtaba=curtab.children('a').attr('href');
				let lastlia=$('#ultabs').find('li').last().children('a').attr('href');
				if(curtaba!=lastlia){
					let nextli=curtab.next('li');
					nextli.children('a').tab('show');			
				}else{
					$('#paso01').removeClass('active').fadeOut();
					$('#paso02').addClass('active').fadeIn();					
				}				
			}
		}		
		let _actualizardatos=function(){
		}
		let _updatejsoncurso=function(){			
			let data=new FormData();
			data.append('id',idcurso);
			data.append('campo','txtjson');
			data.append('valor',JSON.stringify(jsoncurso));
			__sysajax({
				fromdata:data,
				showmsjok:false,
        	    url:_sysUrlBase_+'/acad_curso/setCampojson'});
		}

		/// acciones pasainfo
		$('#paso01').on('submit','#pasoinfo #frmdatosdelcurso',function(ev){
			ev.preventDefault();
			let frm=$(this);
			var data=__formdata('frmdatosdelcurso');
			let reset=$('#pasoinfo').find('img#verimg').hasClass('reset')?1:0;
			data.append('resetimagen',reset);
			__sysajax({
				fromdata:data,
	            url:_sysUrlBase_+'/acad_curso/guardarAcad_curso',
	            callback:function(rs){	            	       	
	            	$('#pasoinfo').find('input[name="idcurso"]').val(rs.newid);
	            	infocurso.datos.idcurso=rs.newid;
	            	idcurso=parseInt(rs.newid);
	            	infocurso.datos.nombre=$('#pasoinfo').find('input[name="nombre"]').val();
	            	infocurso.datos.autor=$('#pasoinfo').find('input[name="autor"]').val();
	            	infocurso.datos.descripcion=$('#pasoinfo').find('textarea[name="descripcion"]').val();
	            	infocurso.datos.aniopublicacion=$('#pasoinfo').find('input[name="aniopublicacion"]').val();
	            	infocurso.datos.color=$('#pasoinfo').find('input[name="color"]').val();
	            	let imagen=rs.newimagen;
	            	if(imagen!=''){
	            		$('#pasoinfo').find('img#verimg').attr('src',imagen);
	            		infocurso.datos.imagen=imagen;
	            	}
	            	$.extend(datosdelcurso,infocurso.datos);
	            	nombreusuario=infocurso.datos.autor;
	            	infoavance=10;
	            	jsoncurso.infoavance=infoavance;
	            	_updatejsoncurso();
	            	updateprogress(infoavance);
	            	let pnltmp=$('#paso01 .wizard');
	            	let ul=pnltmp.find('#ultabs');
	            	let pclase=".tab-pane"
	            	let showp="#pasocate";
	            	//ul.find('li.active').removeClass('active');
	            	_initpnlcategorias();
	            	ul.find('a[href="'+showp+'"]').closest('li').addClass('active');
	            	pnltmp.find('#panelesultabs').children(pclase).removeClass('active');
	            	pnltmp.find('#panelesultabs').children(showp).addClass('active');	            	
	            }
			});
			return false;
		}).on('click','.btnaddfile',function(ev){
			ev.preventDefault();
			let pnltmpimg=$(this).closest('.frmchangeimage');
			__subirfile({
				file:pnltmpimg.find('img'),
				typefile:'imagen',				
				fntmp:function(filev){
					filev.name="imagen";
					filev.style="display:none";					
					pnltmpimg.find('input[name="imagen"]').replaceWith(filev);
					pnltmpimg.find('img#verimg').removeClass('reset');
				}
			})
			return false;
		}).on('click','.resetfile',function(ev){
			ev.preventDefault();
			$('#pasoinfo').find('img#verimg').attr('src',imgdefecto).addClass('reset');
		});

		//acciones pasacategorias
		$('#pasocate').on('click','.btnback',function(ev){
			ev.preventDefault();
			let pnltmp=$('#paso01 .wizard');
        	let ul=pnltmp.find('#ultabs');
        	let pclase=".tab-pane"
        	let showp="#pasoinfo";
			ul.find('a[href="'+showp+'"]').closest('li').addClass('active');
	        pnltmp.find('#panelesultabs').children(pclase).removeClass('active');
	        pnltmp.find('#panelesultabs').children(showp).addClass('active');
		}).on('click','.btnnext',function(ev){
			ev.preventDefault();
			infoavance=20;
	        jsoncurso.infoavance=infoavance;
	        _updatejsoncurso();
			updateprogress(infoavance);
			let pnltmp=$('#paso01 .wizard');
        	let ul=pnltmp.find('#ultabs');
        	let pclase=".tab-pane"
        	let showp="#pasoestruct";
			ul.find('a[href="'+showp+'"]').closest('li').addClass('active');
	        pnltmp.find('#panelesultabs').children(pclase).removeClass('active');
	        pnltmp.find('#panelesultabs').children(showp).addClass('active');
		}).on('click','.addcategoria',function(ev){
			ev.preventDefault();
			let pnlcat=$('#pasocate');
			let trclone=pnlcat.find('.trclone').clone(true);
			let cat=pnlcat.find('select#categoria');
			let scat=pnlcat.find('select#subcategoria');
			let idcate=0;
			let idscat=parseInt(scat.val()||0);
			let txt='';
			if(idscat>0){
				txt=cat.find('option:selected').text()+' -> '+scat.find('option:selected').text();
				idcate=idscat;
				txtcat=scat.find('option:selected').text();
			}else{
				txt=cat.find('option:selected').text();
				idcate=parseInt(cat.val());
				txtcat=cat.find('option:selected').text();
			}			
			__sysajax({
            	fromdata:{idcat:idcate,idcur:idcurso,acc:'I'},				
				showmsjok:false,
	            url:_sysUrlBase_+'/acad_curso/guardarcategorias',
	            callback:function(rs){          	
	            	let id=rs.data;
	            	let pnlcont=$('#pasocate');
	            	let tbcategorias=pnlcont.find('.tablecategorias'); 	
	            	let trclone=tbcategorias.find('.trclone').clone(true);
					trclone.removeClass('trclone');
					trclone.children('td:first').text(txt);
					trclone.attr('idcursocategoria',id);
					tbcategorias.append(trclone);
					infocurso.categorias.push({idcursocategoria:id+'',categoria:txtcat,idcategoria:idcate+''});
				}	            
			});					
		}).on('click','.removetcat',function(ev){
			let tr=$(this).closest('tr');
			let idcat=tr.attr('idcursocategoria');
			 __sysajax({
            	fromdata:{id:idcat,acc:'D'},
				showmsjok:false,
	            url:_sysUrlBase_+'/acad_curso/guardarcategorias',
	            callback:function(rs){	            	
	            	tr.remove();	            	
	            	if(infocurso.categorias.length>0)
	            		infocurso.categorias.forEach(function(currvalue, index, arr){
	            			if(infocurso.categorias[index].idcursocategoria==idcat){
						      infocurso.categorias.splice(index, index);
						    }	            			
	            		})
	            }	            
			});
		});

		//acciones estructuras
		$('#pasoestruct').on('click','.btnbacktab',function(ev){
			ev.preventDefault();
			let pnltmp=$('#paso01 .wizard');
        	let ul=pnltmp.find('#ultabs');
        	let pclase=".tab-pane"
        	let showp="#pasocate";
			ul.find('a[href="'+showp+'"]').closest('li').addClass('active');
	        pnltmp.find('#panelesultabs').children(pclase).removeClass('active');
	        pnltmp.find('#panelesultabs').children(showp).addClass('active');
		}).on('click','.btnnexttab',function(ev){
			ev.preventDefault();
			infoavance=30;
	        jsoncurso.infoavance=infoavance;
	        _updatejsoncurso();
			updateprogress(infoavance);
			_initpnlSesiones();
			let pnltmp=$('#paso01 .wizard');
        	let ul=pnltmp.find('#ultabs');
        	let pclase=".tab-pane"
        	let showp="#pasosilabus";        	
			ul.find('a[href="'+showp+'"]').closest('li').addClass('active');
	        pnltmp.find('#panelesultabs').children(pclase).removeClass('active');
	        pnltmp.find('#panelesultabs').children(showp).addClass('active');
		}).on('click','.btnplantilla',function(ev){
			ev.preventDefault();
			if(!$(this).children('a').hasClass('btn-success')){
				$(this).children('a').addClass('btn-success')
				$(this).siblings().children('a').removeClass('btn-success');
				plantilla={id:$(this).attr('data-idplantilla'),nombre:$(this).attr('data-nombre')};
				jsoncurso.plantilla=plantilla;
				infocurso.datos.txtjson=jsoncurso;
				$('')
				_updatejsoncurso();							
			}
		})

		//acciones silabos. 
		$('#pasosilabus').on('click','.btnbacktab',function(ev){
			ev.preventDefault();
			let pnltmp=$('#paso01 .wizard');
        	let ul=pnltmp.find('#ultabs');
        	let pclase=".tab-pane"
        	let showp="#pasoestruct";
			ul.find('a[href="'+showp+'"]').closest('li').addClass('active');
	        pnltmp.find('#panelesultabs').children(pclase).removeClass('active');
	        pnltmp.find('#panelesultabs').children(showp).addClass('active');
		}).on('click','.btnnexttab',function(ev){
			ev.preventDefault();
			infoavance=40;
	        jsoncurso.infoavance=infoavance;
	        _updatejsoncurso();
			updateprogress(infoavance);	
			_diseniarcurso();
	        $('#paso01').removeClass('active').fadeOut();
	        $('#paso02').addClass('active').fadeIn();
		}).on('click','.creartema',function(ev){
			ev.preventDefault();
	    	_editdatostema({});
		}).on('click','.importartemas',function(ev){	
		}).on('click','ul#mindice i.magregar',function(ev){
			let $li=$(this).closest('li');
			let idcursodetalle=$li.attr('data-idcursodetalle');
			let item=_buscartemas(temas,idcursodetalle);
			_editdatostema({idpadre:idcursodetalle,idrecursopadre:item.idrecurso,accmenu:'addsubitem'});
		}).on('click','ul#mindice i.meditar',function(ev){
			let $li=$(this).closest('li');
			let idpadre=$li.attr('data-idpadre');
			let idcursodetalle=$li.attr('data-idcursodetalle');
			let hayidcurso=false;
			let datos=_buscartemas(infocurso.temas,idcursodetalle);
			datos.dato.accmenu='edit';
			_editdatostema(datos.dato);
		}).on('click','ul#mindice i.meliminar',function(ev){
			$li=$(this).closest('li');
			$(this).popover('dispose');
			__sysajax({
				url:_sysUrlBase_+'/acad_cursodetalle/eliminarMenu',
				fromdata:{
					idcurso:idcurso,
					idcursodetalle:$li.attr('data-idcursodetalle'),
					idrecurso:$li.attr('data-idrecurso'),
					tiporecurso:$li.attr('data-tipo'),
				},
				type:'html',
				showmsjok:true,
				callback:function(rs){
					$li.remove();					
					_buscartemaremove(infocurso.temas,$li.attr('data-idcursodetalle'));
					temas=infocurso.temas;
				}
			});			
		}).on('click','.btncancelar',function(ev){
			$('#pasosilabus').find('#paneldisenio').hide(0);
	    	$('#pasosilabus').find('#plnindice').show(0);
	    	$('#pasosilabus').find('.card-footer').show(0);
	    	$('#pasosilabus').find('.btnmanagermenu').show(0);	    	
		}).on('click','.btnaddfile',function(ev){
			ev.preventDefault();
			let pnltmpimg=$(this).closest('.frmchangeimage');
			__subirfile({
				file:pnltmpimg.find('img'),
				typefile:'imagen',				
				fntmp:function(filev){
					filev.name="imagen";
					filev.style="display:none";	
					//let pnlcont=$('#pasosilabus').find('#paneldisenio');				
					pnltmpimg.find('input[name="imagen"]').replaceWith(filev);
					pnltmpimg.find('img#verimg').removeClass('reset');
				}
			})
			return false;
		}).on('click','.resetfile',function(ev){
			ev.preventDefault();
			let pnlcont=$('#pasosilabus').find('#paneldisenio');
			pnlcont.find('img#verimg').attr('src',imgdefecto).addClass('reset');
		}).on('submit','#frmdatosdeltema',function(ev){
			ev.preventDefault();
			let frm=$(this);
			let esfinal=frm.find('#esfinal').val()||0;
			if(parseInt(esfinal)==0)frm.find('#tiporecurso').val('M');
			else frm.find('#tiporecurso').val('S');
			let acc=frm.find('input[name="accmenu"]').val();
			var data=__formdata('frmdatosdeltema');
			let reset=$('#pasoinfo').find('img#verimg').hasClass('reset')?1:0;
			let idpadre=frm.find('#idpadre').val()||0;
			data.append('resetimagen',reset);
			__sysajax({
				fromdata:data,
        	    url:_sysUrlBase_+'/acad_cursodetalle/guardarMenu',
            	callback:function(rs){            
            		if(rs.code=='ok'){            			
	            		var dt=rs.newid;          		
	            		let tematmp={
	            			idcursodetalle:dt.idcursodetalle+"",
							idrecurso:dt.idnivel+"",							
							idpadre:dt.idpadre+"",
							idrecursopadre:dt.idnivelpadre+"",
							orden:dt.orden+"",
							tiporecurso:frm.find('#tiporecurso').val(),
							imagen:dt.imagen||'',
							efinal:esfinal+"",
							nombre:frm.find('input[name="nombre"]').val(),
							ordenrecurso:dt.ordennivel+"",
							descripcion:frm.find('textarea[name="descripcion"]').val(),
							color:frm.find('input[name="color"]').val(),
							hijos:[],
							index:dt.orden												
	            		}
	            		let pnltmp=$('#pasosilabus').find('ul#mindice');
	            		if(acc=='additem'){
	            			infocurso.temas.push(tematmp);
	            			temas=infocurso.temas;
	            			addtemas(pnltmp,[tematmp]);
	            		}else if(acc=='edit'){	            			
	            			_modificartema(infocurso.temas,dt.idcursodetalle,tematmp,true);
	            			let nmenu=pnltmp.find('#me_'+dt.idcursodetalle);
	            			nmenu.children('div').children('span.mnombre').text(frm.find('input[name="nombre"]').val());
							nmenu.attr('data-idcursodetalle',dt.idcursodetalle);
							nmenu.attr('data-idpadre',dt.idpadre);
							nmenu.attr('data-orden',dt.orden);
							nmenu.attr('data-idrecurso',dt.idnivel);
							nmenu.attr('id','me_'+dt.idcursodetalle);
							nmenu.attr('data-imagen',dt.imagen);
							nmenu.attr('data-esfinal',esfinal);	            				            			
	            		}else if(acc=='addsubitem'){
	            			$li=$('#mindice li#me_'+dt.idpadre);
	            			liul=$li.children('ul');
	            			if(liul.length==0){
	            				$li.append('<ul></ul>');
	            				liul=$li.children('ul');
	            			}
	            			addtemas(liul,[tematmp]);
	            			_modificartema(infocurso.temas,dt.idpadre,tematmp,false);	            			
	            		}
	            		$('#paneldisenio').fadeOut(0);
						$('#plnindice').fadeIn('fast');
						$('.card-footer').fadeIn('fast');
						$('.btnmanagermenu').fadeIn('fast');
						//console.log(infocurso.temas);
					}
	        	}
			});
			return false;
		})

		var _editdatostema=function(curtema){
			$('#pasosilabus').find('#plnindice').hide(0);
	    	$('#pasosilabus').find('.card-footer').hide(0);
	    	$('#pasosilabus').find('#paneldisenio').show(0);
	    	$('#pasosilabus').find('.btnmanagermenu').hide(0);

			let pnlcont=$('#pasosilabus').find('#paneldisenio');
			pnlcont.find('.card-footer').show(0);
			let tema={idcurso:datosdelcurso.idcurso,nombre:'',idpadre:0,orden:0,idrecurso:0,idrecursopadre:0,idrecursoorden:0,tiporecurso:'S',idlogro:0,esfinal:0,
					nombre:'',descripcion:'',imagen:'',color:'',accmenu:'additem'};						
			$.extend(tema,curtema);			
			if(tema.imagen==''||tema.imagen==undefined)tema.imagen=imgdefecto;
			else{				
				let mm=new RegExp(urlmedia, "gi");						
				tema.imagen.replace(mm,'');				
				tema.imagen=urlmedia+tema.imagen;				
			}
			
			pnlcont.find('input[name="idcurso"]').val(tema.idcurso);
			pnlcont.find('input[name="idcursodetalle"]').val(tema.idcursodetalle);
			pnlcont.find('input[name="idpadre"]').val(tema.idpadre);
			pnlcont.find('input[name="orden"]').val(tema.orden);
			pnlcont.find('input[name="idrecurso"]').val(tema.idrecurso);
			pnlcont.find('input[name="idrecursopadre"]').val(tema.idrecursopadre);
			pnlcont.find('input[name="idrecursoorden"]').val(tema.idrecursoorden);
			pnlcont.find('input[name="tiporecurso"]').val(tema.tiporecurso);
			pnlcont.find('input[name="idlogro"]').val(tema.idlogro);
			pnlcont.find('input[name="esfinal"]').val(tema.esfinal);
			pnlcont.find('input[name="nombre"]').val(tema.nombre);
			pnlcont.find('textarea[name="descripcion"]').val(tema.descripcion);
			pnlcont.find('img#verimg').attr('src',tema.imagen);
			pnlcont.find('input[name="color"]').val(tema.color);
			pnlcont.find('input[name="color"]').minicolors({opacity: true,format:'rgb'});
			pnlcont.find('input[name="color"]').minicolors('settings',{value:tema.color});
			pnlcont.find('input[name="accmenu"]').val(tema.accmenu);
		}
		
		let _reordenar=function(dt){
			let item=dt.obj||'';
			let max=dt.max||0;
			let min=dt.min||0;
			let res=dt.res||0;
			let items=dt.items||'';
			if(item==''||item==undefined||items=='') return ;
			$hijos=item.children(items);
			let ordenmen=min;
			var datosupdate={};
			$.each($hijos,function(i,v){
				let hi=$(v);
				if(!hi.hasClass('paraclonar')){
					if(i>=min && i<=max){
						hi.attr('data-orden',ordenmen).removeAttr('data-oldindex');
						datosupdate[ordenmen]=hi.attr('data-idcursodetalle');
						ordenmen++;
					}		
				}
			})
			__sysajax({
				url:_sysUrlBase_+'/acad_cursodetalle/reordenar',
				fromdata:{idcurso:idcurso , dataorden:JSON.stringify(datosupdate)},
				type:'json',
				showmsjok:true
			});
		}
		var _fnordenar=function(){
			$('#plnindice').find('ul').each(function(i,v){
				$('ul#'+$(v).attr('id')).disableSelection();
				$('ul#'+$(v).attr('id')).sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9,
					start:function(ev,ui){
						var li=$(ui.item);
						var pos=parseInt(li.index()+1);
						if(li.closest('ul').attr('id')=='mindice') pos--;
						li.addClass('enmovimiento').attr('data-oldindex',pos);
					},
					update:function(ev,ui){				
						let li=$(ui.item);
						li.removeClass('enmovimiento');
						let pos=parseInt(li.index()+1);
						let posant=parseInt(li.attr('data-oldindex')||1);
						if(li.closest('ul').attr('id')=='mindice') {pos--; }
						ul=li.closest('ul');
						_reordenar({obj:ul,min:((pos<posant)?pos:posant),max:((pos>posant)?pos:posant),items:'li'})			
					}
				});
			});
			$('.verpopover').popover({ trigger: 'hover',delay:100});
		}
		var addtemas=function(donde,temas){
			$.each(temas,function(i,t){
				//console.log(t);
				let dt={hijos:{},imagen:imgdefecto};
				$.extend(dt,t);				
				//if(.hijos==undefined) t.hijos={};
				let hijos=dt.hijos||[];
				let liclonado=$('#plnindice').children('li.paraclonar').clone(true);
				liclonado.removeAttr('style').removeClass('paraclonar');
				liclonado.attr('id','me_'+dt.idcursodetalle);
				liclonado.attr('data-idrecurso',dt.idrecurso);
				liclonado.attr('data-idcursodetalle',dt.idcursodetalle);
				liclonado.attr('data-idpadre',dt.idpadre);
				liclonado.attr('data-orden',dt.orden);
				liclonado.attr('data-tipo',dt.tiporecurso);
				liclonado.attr('data-imagen',dt.imagen);
				liclonado.attr('data-esfinal',dt.esfinal);
				liclonado.children('div').children('span.mnombre').text(dt.nombre);
				
				if(hijos.length){
					liclonado.addClass('hijos');
					liclonado.append('<ul id="me_hijo'+dt.idcursodetalle+'"></ul>');					
					addtemas(liclonado.find('#me_hijo'+dt.idcursodetalle),hijos);
				}
				donde.append(liclonado);
			})
			_fnordenar();
		}
		let _buscartemaremove=function(itemas,idcurdet){
			let icont=false;
			if(itemas.length>0){				
				itemas.forEach(function(cur,i,v){
					if(icont==false)
					if(itemas[i].idcursodetalle==idcurdet){		
						itemas.splice(i, i);
						icont=true;
						return true;
					}else{
						let ic=_buscartemaremove(cur.hijos,idcurdet);
						if(ic==true){
							icont=true;
							return true;
						}
					}
				})
			}
		}
		let _modificartema=function(itemas,iddet,dt,edit){
			let icont=false;
			if(itemas!=undefined)
			if(itemas.length>0){				
				itemas.forEach(function(cur,i,v){
					if(icont==false)
					if(itemas[i].idcursodetalle==iddet){
						if(edit) $.extend(itemas[i],dt);
						else{
							if(itemas[i]['hijos']==undefined)itemas[i]['hijos']=[];
							//console.log('eee',itemas[i]['hijos'],cur,i);
							itemas[i]['hijos'].push(dt);
						}
						icont=true;
						return true;
					}else{
						let ic=_modificartema(itemas[i].hijos,iddet,dt,edit);
						if(ic==true){
							icont=true;
							return true;
						}
					}
				})
			}
		}
		let _diseniarcurso=function(){
			let previewcurso=$('#previewcurso');
			let mainestilo=previewcurso.children('#mainestilo');
			let bookplantilla=previewcurso.children('#bookplantilla');			
			mainestilo.attr('href',mainestilo.attr('data-tmpurl')+plantilla.nombre+'.css?idtmp'+__idgui());
			bookplantilla.css(estructura);
			disenioestiloP_(estilopagina);
		}
		let _diseniarPortada=function(){
			let tmp=infoportada;			
			let previewcurso=$('#previewcurso');			
			let bookplantilla=previewcurso.children('#bookplantilla');
			let marcopage=bookplantilla.children('.marcopage');
			let contentPage=marcopage.children('#contentpages');
			let imagen=tmp.imagen!=''&&tmp.imagen!=imgdefecto?'<div class="col-12 tituloPortada"><img src="'+tmp.image+'" class="img im-responsive img-thumbnail" style="max-height:350px; max-width:350px;"></div>':'';
			let tmptitulo=tmp.titulo==''?datosdelcurso.nombre:tmp.titulo; 
			let descripcion=tmp.descripcion==''?datosdelcurso.descripcion:tmp.descripcion;
			html='<div class="row text-center">';
			html+='<div class="col-12 align-middle"><br><br><br><h2>'+tmptitulo+'</h2>';
			html+='<br><br><h5>'+tmp.descripcion+'</h5><br><hr><br>';
			html+=imagen;
			html+='<br><br><b>Autor: '+nombreusuario+'</b><br><hr>';
			html+='</div>';
			contentPage.html(html);
		}
		let _diseniarIndice=function(mostrar){
			_show=mostrar||false;			
			let bookplantilla=$('#previewcurso').children('#bookplantilla');
			let mainmenutop=bookplantilla.children('.menumaintop');
			let btnshowsesiones=mainmenutop.find('.btnshowsesiones').attr('data-menuen',infoindice);
			let pnltmp=bookplantilla.children('.showindicepage').children('.pnlsesions').children('.indicetop');
			if(temas.length>0){
				pnltmp.children('.cloneindice').siblings().remove();
				i=1;				
				$.each(temas,function(i,v){ i++;
					let indiceclon=pnltmp.children('.cloneindice').clone(true);
					indiceclon.removeClass('cloneindice').removeAttr('hidden').attr('data-iddetalle',v.idcursodetalle);
					indiceclon.text(i+'. '+v.nombre);
					pnltmp.append(indiceclon);							
				})				
			}
			if(mostrar==true)
            if(infoindice=='top'){
            	mainmenutop.removeClass('active').addClass('active');
            	contentpages.html('');
            }else if(infoindice=='enpagina'){
            	mainmenutop.removeClass('active');            	
				let pnltmp=bookplantilla.children('.showindicepage').clone(true);
				contentpages.html('<div class="row" style="margin-top:1em;"><div class="col-md-3 col-sm-12 col-xs-12"></div><div id="addindice" class="col-md-6 col-sm-12 col-xs-12"></div><div class="col-md-3 col-sm-12 col-xs-12"></div></div>');
				pnltmp.find('._btnversesion').removeClass('col-md-6').addClass('col-12');
				contentpages.find('#addindice').append(pnltmp);
            }
		}
		let disenioestiloP_=function(estilopagina){
			let previewcurso=$('#previewcurso');
			let bookplantilla=previewcurso.children('#bookplantilla');
			let marcopage=bookplantilla.children('.marcopage');
			marcopage.css(estilopagina);
		}

		let __disenioaddcontenido=function(){
			let vercurtema=curtema.txtjson;			
			if(vercurtema.tipo=='#showpaddcontenido'){
				let link=vercurtema.link;
				let typelink=vercurtema.typelink||'';
				if(link!=''&&typelink!=''){
					let data=new FormData();					
					__sysajax({
						fromdata:{link:link},
						type:'html',
						showmsjok:false,
	        	    	url:_sysUrlBase_+'/plantillas/'+typelink,
	        	    	callback:function(rs){
	        	    		contentpages.html(rs);
	        	    	}
	        	    })
				}
   			}
		}

		let __disenioaddpestanias=function(tmpsesion){
			let tmpcurtema={};
			let vercurtema=curtemadefault;			
			if(tmpsesion!=undefined){				
				tmpcurtema=tmpsesion;
				try{
						if(typeof(tmpsesion.txtjson)=='string' )vercurtema=JSON.parse(tmpsesion.txtjson);
						vercurtema=tmpsesion.txtjson;				
				}catch(ex){}									
			}else{
				tmpcurtema=curtema;
				vercurtema=curtema.txtjson;
			}
			contentpages.html('');			
			if(vercurtema.tipo=='#showpaddopciones'){
				let tipofile=vercurtema.tipofile;
				marcopage.removeClass('arriba abajo derecha circulo menus').addClass(tipofile);
				let options=vercurtema.options||{};
				let hijos=tmpcurtema.hijos||{};				
				if(tipofile=='arriba'||tipofile=='derecha'||tipofile=='abajo'){					
					let pnlmenu=marcopage.children('.menubooktop').children('ul.sysmenumain');
					pnlmenu.find('li').remove();					
					if(hijos.length){						
						$.each(hijos,function(i,v){							
							let li='<li data-iddetalle="'+v.idcursodetalle+'" class="_btnversesion nav-item hvr-grow" ><a class="nav-link" href="javascript:void(0)">'+v.nombre+'</a></li>';
							pnlmenu.append(li);
						})
					}else{
						$.each(options,function(i,v){
							let li='<li data-color="'+v.color+'" data-colorfondo="'+v.colorfondo+'" data-link="'+v.link+'" data-type="'+v.type+'" class="_vercontenido_ nav-item hvr-grow" style="background-color:'+v.colorfondo+'"><a class="nav-link" style="color:'+v.color+'" href="javascript:void(0)">'+v.nombre+'</a></li>';
							pnlmenu.append(li);
						})
					}
				}else if(tipofile=='menus'){
					let html='<div class="row" style="margin-top:1em;"><div class="col-md-3 col-sm-12 col-xs-12"></div>';
					html+='<div class="col-md-6 col-sm-12 col-xs-12"><div class="showindicepage">';
					if(hijos.length){
						html+='<div class="pnlsesions text-center"><h3>Sesiones :'+tmpcurtema.nombre+'</h3></div>';
						$.each(hijos,function(i,v){
							html+='<div class="row indicetop"><div data-iddetalle="'+v.idcursodetalle+'" class="_btnversesion col-12 text-left hvr-glow" >'+i+'. '+v.nombre+'</div></div>';
						})
					}else{
						html+='<div class="pnlsesions text-center"><h3>Opciones :'+tmpcurtema.nombre+'</h3></div>';					
						$.each(options,function(i,v){
							html+='<div class="row indicetop"><div data-color="'+v.color+'" data-colorfondo="'+v.colorfondo+'" data-link="'+v.link+'" data-type="'+v.type+'" class="_vercontenido_ col-12 text-left hvr-glow" style="color:'+v.color+'">'+i+'. '+v.nombre+'</div></div>';						
						})
					}
					html+='</div></div></div><div class="col-md-3 col-sm-12 col-xs-12"></div></div>';
					contentpages.html(html);			
				}else if(tipofile=='circulo'){
					let idgui=__idgui();
					let html='<div class="widgetcont" style="height: calc(100vh - 100px);"><div class="row"><div class="col-12 text-center"><h3>'+tmpcurtema.nombre+'</h3></div></div>';
					html+='<div class="row" style="margin: 0px; padding: 0px;"><div class="col-12"><div class="nav circular-menu"><div class="menucircle open" id="menucircle'+idgui+'"> ';
					if(hijos.length){						
						$.each(hijos,function(i,v){
							let colorfondo=v.colorfondo||'rgba(0,0,0,0)';
							html+='<div data-iddetalle="'+v.idcursodetalle+'" class="_btnversesion menucircleitem" style="background-color:'+colorfondo+'; color:'+v.color+'"><span>'+(i+1)+'. '+v.nombre+'</span></div>';	

						})
					}else{
						$.each(options,function(i,v){
							let colorfondo=v.colorfondo||'rgba(0,0,0,0)';
							html+='<div data-color="'+v.color+'" data-colorfondo="'+colorfondo+'" data-link="'+v.link+'" data-type="'+v.type+'" class="_vercontenido_ menucircleitem" style="background-color:'+colorfondo+'; color:'+v.color+'"><span>'+(i+1)+'. '+v.nombre+'</span></div>';					
						})
					}
					html+='</div><span class="openmenucircle"><span>Menus</span></span>';
					html+='</div></div></div></div';
					contentpages.html(html);
					var opt={idcurso:idcurso,idcursodetalle:tmpcurtema.idtema}
					setTimeout(function(){$('#menucircle'+idgui).menucircle(opt);},450);
				}				
			}
		}

		let _updatejsontema=function(){			
			curtema.txtjson.infoavancetema=infoavancetema;
			updatetema_(temas,curtema.idcursodetalle,curtema);
			let data=new FormData();
			data.append('tb','sesionhtml');
			data.append('id',parseInt(curtema.idcursodetalle));
			data.append('campo','txtjson');
			data.append('valor',JSON.stringify(curtema.txtjson));
			__sysajax({
				fromdata:data,
				showmsjok:false,
        	    url:_sysUrlBase_+'/acad_cursodetalle/setCampojson'});
		}

		let __asiganacionfile=function(it){			
			it.siblings('.cicon').removeClass('active').children('i').removeClass('btn-success btn-primary').addClass('btn-primary');
			it.addClass('active').children('i').addClass('btn-success');
			if(curtema.txtjson.tipo=='#showpaddcontenido'){
				infoavancetema=100;
				curtema.txtjson.infoavancetema=infoavancetema;
				curtema.txtjson.link=it.attr('data-link');
				curtema.txtjson.typelink=it.attr('data-type');
				_updatejsontema();
				updateprogresscurso();
				__disenioaddcontenido();
			}			
		}

		var __addexamen=function(obj,fncall){
			//let md=__sysmodal({titulo:'Examenes',html:$('#config_plantilla').html()});
			obj.off('addassesment');
			let lstmp='btnaddexamen'+__idgui();
			obj.addClass(lstmp);
			let url=_sysUrlBase_+'/quiz/buscar/?plt=modal&fcall='+lstmp;
			let md=__sysmodal({titulo:'Quiz',url:url});
			obj.on('addassesment',function(ev){
				$(this).off(lstmp);
				$(this).removeClass(lstmp);
				var idexamen=$(this).attr('data-idexamen');
				let link=_sysUrlBase_+'/quiz/ver/?idexamen='+idexamen;
				if(_isFunction(fncall)) fncall(link);	
				//__cargarplantilla({url:link,donde:'contentpages'});			
			})
		}
		var __addgame=function(obj,fncall){
			obj.off('addgame');
			let lstmp='addgame'+__idgui();
			obj.addClass(lstmp);
			let url=_sysUrlBase_+'/games/buscar/?plt=modal&fcall='+lstmp;		
			let md=__sysmodal({titulo:'Games',url:url});
			obj.on('addgame',function(ev){
				$(this).off(lstmp);
				$(this).removeClass(lstmp);
				var idgame=$(this).attr('data-idgame');
				let link=_sysUrlRaiz_+'game/ver/?idgame='+idgame+'&tmp='+lstmp;
				if(_isFunction(fncall)) fncall(link); 		
				//__cargarplantilla({url:link,donde:'contentpages'});			
			})
		}
		var __addtics=function(obj,met,fncall){
			obj.off('addtics');
			let lstmp='addtics'+__idgui();
			obj.addClass(lstmp);
			let url=_sysUrlBase_+'/tics/buscar/?plt=modal&fcall='+lstmp+'&met='+met;
			let md=__sysmodal({titulo:'Games',url:url});
			obj.on('addtics',function(ev){
				$(this).off(lstmp);
				$(this).removeClass(lstmp);
				let idtic=$(this).attr('data-idtic'); // idactividad
				let idsesion=$(this).attr('data-idsesion');
				let link=_sysUrlRaiz_+'actividad/ver/?idactividad='+idtic+"&met="+met+"&idsesion="+idsesion+'&tmp='+lstmp;	
				obj.attr('data-link',link);
				if(_isFunction(fncall)) fncall(link); 
				//__cargarplantilla({url:link,donde:'contentpages'});			
			})
		}

		var _buscartemas=function(itemas,idcurdet){
			let curt={};
			let icont=false;
			let rdt={dato:{},icon:false};	
			if(itemas.length>0){				
				itemas.forEach(function(cur,i,v){
					if(icont==false){																
						if(parseInt(cur.idcursodetalle)==parseInt(idcurdet)){
							icont=true;
							rdt={dato:cur,icont:true};
						}else{							
							let hijos=cur.hijos||[];
							dt=_buscartemas(hijos,idcurdet);
							if(dt.icont==true){
								icont=true;
								rdt={dato:dt.dato,icont:true};
							}
						}
					}
				});
			}
			return rdt;			
		}

		let __temamostrar=function(){				
			let pnledittema=$('#showtemaidedit');
			$('.paso2tabshow02').removeClass('active');
			pnledittema.addClass('active');
			pnledittema.find('.pasotematabshow').removeClass('active');
			pnledittema.find('#addtemacontenido').addClass('active');
			let txttemajson=curtemadefault;
			let tmptema=curtema;
			if(curtema.txtjson!='') txttemajson=_isJson(curtema.txtjson)?JSON.parse(curtema.txtjson):curtemadefault;

			let colorfondo=txttemajson.colorfondo==''?'rgba(0,0,0,0)':txttemajson.colorfondo;
			let imagenfondo=txttemajson.imagenfondo;
			curtema.idtema=curtema.idcursodetalle;
			curtema.txtjson=txttemajson;			
			infoavancetema=txttemajson.infoavancetema;			
			updatetema_(temas,curtema.idcursodetalle,curtema);

			pnledittema.find('h3.nombretema').text(curtema.nombre);			
			let addcont=pnledittema.find('#addtemacontenido');
			addcont.find('input[name="fondocolor"]').val(colorfondo);
			addcont.find('input[name="fondocolor"]').minicolors({opacity: true,format:'rgb'});			
			addcont.find('input[name="fondocolor"]').minicolors('settings',{value:colorfondo});						
			addcont.find('img#verimg').attr('src',imagenfondo!=''?imagenfondo:imgdefecto);

			addcont.find('.btntemaaddinfo').removeClass('active btn-success').addClass('btn-secondary');
			addcont.find('.btntemaaddinfo[data-showp="'+txttemajson.tipo+'"]').addClass('active btn-success').removeClass('btn-secondary');
			disenioestiloP_({'background-color':colorfondo,'background-image':'url('+imagenfondo+')'})			
			$('#previewcurso #bookplantilla .marcopage #contentpages').html('');
			updateprogresscurso();
			if(curtema.txtjson.tipo=='#showpaddcontenido') __disenioaddcontenido();
			else if(curtema.txtjson.tipo=='#showpaddopciones') __disenioaddpestanias();
		}

		var buscartema2_=function(itemas,buscar,idpadre){			
			idpadre=idpadre||0;
			let encontrado=false;
			let rdt={dato:{},encontrado:false};
			if(itemas.length>0){			
				itemas.forEach(function(cur,i,v){					
					if(encontrado==false){
						cur.index=i;
						if(cur.idcursodetalle==buscar.idet && buscar.por=='idet'){
							encontrado=true;
							cur.index=i;
							if(cur.txtjson=='')cur.txtjson=curtemadefault;							
							rdt={dato:cur,encontrado:true};
						}else if(i==buscar.index && buscar.por=='index' && cur.idpadre==idpadre){
							encontrado=true;
							cur.index=i;
							if(cur.txtjson=='')cur.txtjson=curtemadefault;
							rdt={dato:cur,encontrado:true};
						}else{
							let hijos=cur.hijos||[];							
							if(hijos.length>0){								
								dt=buscartema2_(hijos,buscar,idpadre);								
								if(dt.encontrado==true){
									encontrado=true;
									rdt={dato:dt.dato,encontrado:true};
								}
							}
						}
					}
				})
			}
			return rdt;
		}

		var updatetema_=function(itemas,iddet,tmptema){
			let encontrado=false;
			let rdt={dato:itemas,encontrado:false};			
			if(itemas.length>0){
				itemas.forEach(function(cur,i,v){
					if(encontrado==false){
						if(cur.idcursodetalle==iddet){
							encontrado=true;							
							$.extend(itemas[i],tmptema);							
							rdt.dato=itemas;
							rdt.encontrado=true;
						}else{
							let hijos=cur.hijos||[];
							dt=updatetema_(hijos,iddet,tmptema);
							if(dt.encontrado==true){
								itemas[i].hijos=dt.dato;															
								rdt.dato=itemas;
								rdt.encontrado=true;		
							}
						}
					}
				})
			}			
			return rdt;
		}

		var _vertemapadreohermano=function(tmpcurtema){
			curtemaidpadre=tmpcurtema.idpadre;
			curtemaindex=tmpcurtema.index+1;
			let tmpcurpadre=buscartema2_(temas,{por:'idet',idet:curtemaidpadre},curtemaindex).dato;
			curtemaidpadre=tmpcurpadre.idpadre;
			curtemaindex=tmpcurpadre.index+1;
			if(tmpcurpadre.idcursodetalle!=undefined){
				let curtema2=buscartema2_(temas,{por:'index',index:curtemaindex},curtemaidpadre).dato;
				if(curtema2.idcursodetalle==undefined)
					curtema2=_vertemapadreohermano(tmpcurpadre);
				return curtema2;
			} else tmpcurpadre;
		}
		
		var _siguienteteam=function(){
			let tmpcurtema=curtema;
			curtemaindex=tmpcurtema.index;
			curtemaidpadre=tmpcurtema.idpadre;
			let hijos=tmpcurtema.hijos||[];
			let otrotema=false;
			if(hijos.length){
				curtema=hijos[0];				
				curtemaindex=curtema.index;
				curtemaidpadre=curtema.idpadre;				
				otrotema=true;
			}else{
				curtemaindex++;
				let tmpcurtema2=buscartema2_(temas,{por:'index',index:curtemaindex},curtemaidpadre).dato;
				if(tmpcurtema2.idcursodetalle==undefined){
					if(curtemaidpadre==0) return false;					
					let tmpcurtema3=_vertemapadreohermano(tmpcurtema);
					if(tmpcurtema3.idcursodetalle==undefined) otrotema=false;
					else {
						curtema=tmpcurtema3;						
						otrotema=true;
					}
				}else{
					curtema=tmpcurtema2;
					otrotema=true;
				}
			}			
			return otrotema;
		}

		var _backtema=function(){
			curtemaindex=curtema.index;
			curtemaidpadre=curtema.idpadre;
			if(curtemaindex==0 && curtemaidpadre==0) return false;
			curtemaindex--;
			if(curtemaindex>=0){
				tmpcurtema=buscartema2_(temas,{por:'index',index:curtemaindex},curtemaidpadre).dato;
				if(tmpcurtema.idcursodetalle==undefined) return false;
				else{
					curtema=tmpcurtema;
					return true;
				}
			}else{				
				curtema=buscartema2_(temas,{por:'idet',idet:curtema.idpadre},curtema.idpadre).dato;				
				if(curtema.idcursodetalle==undefined) return false;
				else{					
					return true;
				}
			}
		}

		$('#paso02').on('click','.btnbacktab',function(ev){
			ev.preventDefault();
			let btn=$(this);
			let showp=btn.attr('data-showp');
			let clasep=btn.attr('data-pclase');
			let itemas=temas.length-1;
			let nindex=curtema.index||0;
			if(showp=='#showtemaidedit'){
				$(this).closest('.publicar').removeClass('active');
				$(showp).find('.pasotematabshow').removeClass('active');
				$(showp).find('.pasotematabshow').first().addClass('active');
			}else if(showp=='#showindice'){
				let tmpback=_backtema();				
				$(this).closest('.'+clasep).removeClass('active').fadeOut();
				if(tmpback==true){					
					__temamostrar();
					return false;
				}					 
			}else{
				$(this).closest('.'+clasep).removeClass('active').fadeOut();				
			}
			$(showp).addClass('active').fadeIn();
			
		}).on('nexttab','.btnnexttab',function(ev){
			ev.preventDefault();
			let btn=$(this);
			let showp=btn.attr('data-showp');
			let clasep=btn.attr('data-pclase');
			let eleclasep=$(this).closest('.'+clasep)
			eleclasep.removeClass('active');			
			if(showp!='_elegir_'){
				$(showp).addClass('active');
			}else{
				let elegir=btn.attr('data-elegir');
				let hayelegir=eleclasep.find('.'+elegir+'.active');
				if(hayelegir){
					showp=hayelegir.attr('data-showp');
					$(showp).addClass('active');
				}
			}
		}).on('click','.btnaddfile',function(ev){
			ev.preventDefault();
			let tmpplnimg=$(this).closest('.frmchangeimage');
			__subirfile({
				file:tmpplnimg.find('img'),
				typefile:'imagen',
				guardar:true,
				dirmedia:'curso_'+idcurso+'/',			
				fntmp:function(filev){
					filev.name="imagen";
					filev.style="display:none";					
					tmpplnimg.find('input[name="imagen"]').replaceWith(filev);
					tmpplnimg.find('img#verimg').removeClass('reset').trigger('changeimage');
				}
			})
			return false;
		}).on('click','.resetfile',function(ev){
			ev.preventDefault();
			let tmpplnimg=$(this).closest('.frmchangeimage').find('img#verimg');
			tmpplnimg.attr('src',imgdefecto).addClass('reset').trigger('changeimage');
		}).on('blur','#showestiloestructura input[name="font"]',function(ev){
			estructura['font-family']=$(this).val();
			_diseniarcurso();
		}).on('change','#showestiloestructura input[name="fontsize"]',function(ev){
			estructura['font-size']=$(this).val()+'px';
			_diseniarcurso();
		}).on('change','#showestiloestructura input[name="color"]',function(ev){
			estructura.color=$(this).val();
			_diseniarcurso();
		}).on('change','#showestiloestructura input[name="colorfondo"]',function(ev){
			estructura['background-color']=$(this).val();
			_diseniarcurso();
		}).on('changeimage','#showestiloestructura #verimg',function(ev){
			let img=$(this).attr('src')||'';
			if(img==imgdefecto) img='';			
			if(img!='')estructura['background-image']='url('+img+')';
			else estructura['background-image']='none';
			estructura.image=img;
			_diseniarcurso();
		}).on('click','#showestiloestructura .btnnexttab',function(ev){
			let tmppnlcont=$('#showestiloestructura');
			let img=tmppnlcont.find('img#verimg').attr('src')||'';
			if(img==imgdefecto) img='';
			estructura={
				'font-family':tmppnlcont.find('input[name="font"]').val(),
				'font-size':tmppnlcont.find('input[name="fontsize"]').val()+'px',
				color:tmppnlcont.find('input[name="color"]').val(),
				'background-color':tmppnlcont.find('input[name="colorfondo"]').val(),
				'background-image':(img!='')?('url('+img+')'):'',
				image:img
			};
			infoavance=50;
			jsoncurso.estructura=estructura;
			infocurso.datos.txtjson=jsoncurso;			
	        jsoncurso.infoavance=infoavance;	       
			updateprogress(infoavance);
			_diseniarcurso();
			_updatejsoncurso();
			$(this).trigger('nexttab');
		}).on('blur','#showestilopage input[name="font"]',function(ev){
			estilopagina['font-family']=$(this).val();
			_diseniarcurso();
		}).on('change','#showestilopage input[name="fontsize"]',function(ev){
			estilopagina['font-size']=$(this).val()+'px';
			_diseniarcurso();
		}).on('change','#showestilopage input[name="color"]',function(ev){
			estilopagina.color=$(this).val();
			_diseniarcurso();
		}).on('change','#showestilopage input[name="colorfondo"]',function(ev){
			estilopagina['background-color']=$(this).val();
			_diseniarcurso();
		}).on('changeimage','#showestilopage #verimg',function(ev){
			let img=$(this).attr('src')||'';
			if(img==imgdefecto) img='';			
			if(img!='')estilopagina['background-image']='url('+img+')';
			else estilopagina['background-image']='none';
			estilopagina.image=img;
			_diseniarcurso();
		}).on('click','#showestilopage .btnnexttab',function(ev){
			let tmppnlcont=$('#showestilopage');
			let img=tmppnlcont.find('img#verimg').attr('src')||'';
			if(img==imgdefecto) img='';
			estilopagina={
					'font-family':tmppnlcont.find('input[name="font"]').val(),
					'font-size':tmppnlcont.find('input[name="fontsize"]').val()+'px',
					color:tmppnlcont.find('input[name="color"]').val(),
					'background-color':tmppnlcont.find('input[name="colorfondo"]').val(),
					'background-image':(img!='')?('url('+img+')'):'',
					imagen:img
				};
			infoavance=60;
			jsoncurso.infoavance=infoavance;
			jsoncurso.estilopagina=estilopagina;
			infocurso.datos.txtjson=jsoncurso;			
			infoportada.descripcion=infoportada.descripcion==''?infocurso.datos.descripcion:infoportada.descripcion;
			infoportada.titulo=infoportada.titulo==''?infocurso.datos.nombre:infoportada.titulo;
			infoportada.image=(infoportada.image==''||infoportada.image==imgdefecto)?infocurso.datos.imagen:infoportada.image;
			infoportada.autor=infoportada.autor==''?infocurso.datos.autor:infoportada.autor;			
			let pnlportada=$('#showportada');			
			let img3=infoportada.image==''?imgdefecto:infoportada.image;						
			pnlportada.find('img#verimg').attr('src',img3);
			pnlportada.find('textarea[name="titulo"]').val(infoportada.titulo);
			pnlportada.find('textarea[name="descripcion"]').val(infoportada.descripcion);
			updateprogress(infoavance);
			_diseniarcurso();
			_updatejsoncurso();	
			_diseniarPortada();
			$(this).trigger('nexttab');
		}).on('change','#showportada textarea[name="titulo"]',function(ev){
			infoportada.titulo=$(this).val();
			jsoncurso.infoportada=infoportada;
			_diseniarPortada();
		}).on('change','#showportada textarea[name="descripcion"]',function(ev){
			infoportada.descripcion=$(this).val();
			jsoncurso.infoportada=infoportada;
			_diseniarPortada();
		}).on('changeimage','#showportada #verimg',function(ev){
			let img=$(this).attr('src')||imgdefecto;
			if(img==imgdefecto) img='';
			infoportada.image=img;
			jsoncurso.infoportada=infoportada;
			_diseniarPortada();
		}).on('click','#showportada .btnnexttab',function(ev){
			let tmppnlcont=$('#showportada');
			let img=tmppnlcont.find('img#verimg').attr('src')||'';
			if(img==imgdefecto) img='';
			infoportada={
					titulo:tmppnlcont.find('textarea[name="titulo"]').val(),
					descripcion:tmppnlcont.find('textarea[name="descripcion"]').val(),				
					image:img
				};
			infoavance=70;
			jsoncurso.infoportada=infoportada;
			jsoncurso.infoavance=infoavance;
			infocurso.datos.txtjson=jsoncurso;
			_diseniarPortada();
			_updatejsoncurso();
			updateprogress(infoavance);
			$(this).trigger('nexttab');
			_diseniarIndice(true);
		}).on('click','#showindice .btnplantillaindice',function(ev){
			let tmppnl=$('#showindice');
			tmppnl.find('.btnplantillaindice').removeClass('active').children('a').removeClass('btn-success');
			$(this).addClass('active').children('a').addClass('btn-success');
			infoindice=$(this).attr('data-nombre');
			infoavance=80;
			jsoncurso.infoindice=infoindice;
			infocurso.datos.txtjson=jsoncurso;
			_diseniarIndice(true);
			updateprogress(infoavance);			
		}).on('click','#showindice .btnnexttab',function(ev){
			if(temas.length>0){		
				infoindice=$('#showindice .btnplantillaindice.active').attr('data-nombre');
				infoavance=80;				
				jsoncurso.infoindice=infoindice;
				infocurso.datos.txtjson=jsoncurso;	
				jsoncurso.infoavance=infoavance;
				bookplantilla.children('ul.menumaintop').removeClass('active');
				updateprogress(infoavance);
				_updatejsoncurso();				
				let tmpcurtema=buscartema2_(temas,{por:'index',index:curtemaindex},curtemaidpadre).dato;				
				if(tmpcurtema.idcursodetalle==undefined){
					alert('Agregue Sesiones en el paso 4');
				}else{
					curtema=tmpcurtema;					
					__temamostrar();
				}
			}else{
				alert('Agregue Sesiones en el paso 4');
			}
		}).on('change','#showtemaidedit #addtemacontenido input[name="fondocolor"]',function(ev){
			let colorfondo=$(this).val();
			curtema.txtjson.colorfondo=colorfondo;
			updatetema_(temas,curtema.idcursodetalle,curtema);			
			disenioestiloP_({'background-color':colorfondo});
		}).on('changeimage','#showtemaidedit #addtemacontenido #verimg',function(ev){
			let img=$(this).attr('src')||imgdefecto;
			curtema.txtjson.imagenfondo=img;
			if(img==imgdefecto) img='';
				disenioestiloP_({'background-image':'url('+img+')'});
				updatetema_(temas,curtema.idcursodetalle,curtema);				
		}).on('click','#showtemaidedit #addtemacontenido .btnnexttab',function(ev){ //aqui empieza los temas 
			let tmppnl=$('#showtemaidedit #addtemacontenido .btntemaaddinfo.active');			
			let imagenfondo=$('#showtemaidedit #addtemacontenido #verimg').attr('src')||imgdefecto;
			let showp=tmppnl.attr('data-showp');
			curtema.txtjson.colorfondo=$('#showtemaidedit #addtemacontenido input[name="fondocolor"]').val();
			imagenfondo=imagenfondo!=imgdefecto?imagenfondo:'';
			curtema.txtjson.imagenfondo=imagenfondo;
			curtema.txtjson.tipo=showp;

			if(showp=='#showpaddcontenido'){
				infoavancetema=curtema.txtjson.infoavancetema||50;
				updateprogresscurso(true);
				$(showp).find('.cicon').removeClass('active').children('i').removeClass('btn-success').addClass('btn-primary');
				let typelink=curtema.txtjson.typelink||'';
				let link=curtema.txtjson.link||'';
				if(typelink!='' && link!=''){
					let cicon=$(showp).find('.cicon[data-type="'+typelink+'"]');
					cicon.addClass('active').children('i').addClass('btn-success');
					cicon.attr('data-link',link);
					cicon.attr('data-oldmedia',link);
				}
			}else{
				infoavancetema=curtema.txtjson.infoavancetema||30;
				updateprogresscurso(true);
				let tipofile=curtema.txtjson.tipofile||'';
				let showpaddopciones=$('#showpaddopciones');
				let opciontipomenu=showpaddopciones.children('#opciontipomenu');
				 showpaddopciones.children('#opcionlistadopestanias').removeClass('active');
				 opciontipomenu.addClass('active');
				 opciontipomenu.find('.btnopcionespestania.active').removeClass('active btn-success');
				 opciontipomenu.find('.btnopcionespestania[data-pestania="'+tipofile+'"]').addClass('active btn-success');
			}								
			curtema.txtjson.infoavancetema=infoavancetema;			
			_updatejsontema();	
			disenioestiloP_({'background-color':curtema.color});
			if(imagenfondo!='')disenioestiloP_({'background-image':'url('+imagenfondo+')'});
			$(this).attr('data-showp',showp).trigger('nexttab');	
			__temamostrar		
		}).on('click','#showtemaidedit #addtemacontenido .btntemaaddinfo',function(ev){
			$(this).closest('#addtemacontenido').find('a').removeClass('btn-success active').addClass('btn-secondary');
			$(this).addClass('btn-success active');
			let showp=$(this).attr('data-showp');
			let imagenfondo=$('#showtemaidedit #addtemacontenido #verimg').attr('src')||imgdefecto;
			curtema.txtjson.colorfondo=$('#showtemaidedit #addtemacontenido input[name="fondocolor"]').val();
			imagenfondo=imagenfondo!=imgdefecto?imagenfondo:'';
			curtema.txtjson.imagenfondo=imagenfondo;
			curtema.txtjson.tipo=showp;
			updatetema_(temas,curtema.idcursodetalle,curtema);
		}).on('click','#showtemaidedit #showpaddcontenido .btnnexttab',function(ev){
			ev.preventDefault();
			let opttmp=$('#showpaddcontenido').find('.cicon.active');
			if(opttmp.length==1){
				curtema.txtjson.link=opttmp.attr('data-link')||'';
				curtema.txtjson.typelink=opttmp.attr('data-type')||'';
				curtema.txtjson.infoavancetema=100;
				_updatejsontema();				
			}
			let nexttema=_siguienteteam();
			if(nexttema==false){
				infoavance=100;
				updateprogress(100);
				_updatejsoncurso();				
				$(this).trigger('nexttab');
			}else{
				__temamostrar();			
				updateprogresscurso(true);
				$(this).closest('.pasotematabshow').removeClass('active');
				$(this).closest('.pasotematabshow').siblings('#addtemacontenido').addClass('active').children('i').addClass('btn-success');
			}
		}).on('click','#showtemaidedit #showpaddopciones #opciontipomenu .btnopcionespestania',function(ev){
			$(this).closest('#showpaddopciones').find('.btnopcionespestania').removeClass('active btn-success');
			$(this).addClass('active btn-success');
			let tipopestania=$(this).attr('data-pestania');			
			if(!marcopage.hasClass('tipopestania')){
				marcopage.removeClass('arriba derecha abajo circulo menus').addClass(tipopestania).attr('data-tipopestania',tipopestania);
			}
			infoavancetema=60;
			curtema.txtjson.tipofile=tipopestania;
			curtema.txtjson.infoavancetema=infoavancetema;
			updatetema_(temas,curtema.idcursodetalle,curtema);					
			updateprogresscurso();
			__disenioaddpestanias();
		}).on('click','#showtemaidedit #showpaddopciones #opciontipomenu .btnnexttab',function(ev){	// aqui faltan los hijos
			let tipopestania=$('#showtemaidedit #showpaddopciones #opciontipomenu .btnopcionespestania.active').attr('data-pestania')||'';
			infoavancetema=curtema.txtjson.infoavancetema||60;
			curtema.txtjson.infoavancetema=infoavancetema;
			curtema.txtjson.tipofile=tipopestania;
			let pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
			let edittmp=pnltmp.children('#editlistadoopcion');
			let pnltable=$('#verlistadoopciones').children('table').children('tbody');
			let options=curtema.txtjson.options||'';
			pnltable.children('tr.nocontar').siblings('tr').remove();
			console.log(curtema);
			if(options!='')				
				$.each(options,function(i,v){
					let trclone='<tr data-id="'+v.id+'" data-link="'+v.link+'" data-type="'+v.type+'" data-color="'+v.color+'" data-colorfondo="'+v.colorfondo+'"><td class="nombre">'+v.nombre+'</td><td><span class="temadetedit" style="padding:0.5ex;"><i class=" fa fa-pencil btn btn-sm btn-warning "></i></span><span class="temadetremove" style="padding:0.5ex;"><i class="btn btn-sm btn-danger fa fa-trash"></i></span></td></tr>';
					pnltable.append(trclone);
				})
			updateprogresscurso();
			_updatejsontema();			
			$(this).trigger('nexttab');
		}).on('click','#showtemaidedit #showpaddopciones #opcionlistadopestanias .btnnexttab',function(ev){	
			infoavancetema=100;
			curtema.txtjson.infoavancetema=infoavancetema;
			updateprogresscurso(true);
			_updatejsontema(100);
			console.log(curtema);
			let nexttema=_siguienteteam();
			if(nexttema==false){
				infoavance=100;
				jsoncurso.infoavance=infoavance;
				updateprogress(100);
				_updatejsoncurso();
				$(this).trigger('nexttab');
			}else{
				__temamostrar();
				$(this).closest('.pasotematabshow').removeClass('active');
				$(this).closest('.pasotematabshow').siblings('#addtemacontenido').addClass('active').children('i').addClass('btn-success');
			}			
		}).on('click','#showtemaidedit #showpaddopciones #opcionlistadopestanias #btnaddpestania',function(ev){	
			let pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
			pnltmp.children('div').fadeOut().hide();
			let edittmp=pnltmp.children('#editlistadoopcion');			
			edittmp.attr('data-idedit',__idgui());
			edittmp.find('input[name="nombre"]').val('');
			edittmp.find('input[name="color"]').val('rgba(0,0,0,1)');
			edittmp.find('input[name="color"]').minicolors('settings',{value:'rgba(0,0,0,1)'});
			edittmp.find('input[name="colorfondo"]').val('rgba(255, 198, 83,1)');
			edittmp.find('input[name="colorfondo"]').minicolors('settings',{value:'rgba(255, 198, 83,1)'});
			edittmp.find('.cicon').removeClass('active').children('i').removeClass('btn-success').addClass('btn-primary');
			edittmp.fadeIn().show();
		}).on('click','#showtemaidedit #showpaddopciones #opcionlistadopestanias #verlistadoopciones .temadetedit',function(ev){
			let pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
			let edittmp=pnltmp.children('#editlistadoopcion');
			pnltmp.children('div').fadeOut().hide();
			let tr=$(this).closest('tr');
			edittmp.attr('data-idedit',tr.attr('data-id'));
			edittmp.find('input[name="nombre"]').val(tr.children('.nombre').text());
			edittmp.find('input[name="color"]').val(tr.attr('data-color'));
			edittmp.find('input[name="color"]').minicolors('settings',{value:tr.attr('data-color')});
			edittmp.find('input[name="colorfondo"]').val(tr.attr('data-colorfondo'));
			edittmp.find('input[name="colorfondo"]').minicolors('settings',{value:tr.attr('data-colorfondo')});
			edittmp.find('.cicon').removeClass('active').children('i').removeClass('btn-success');
			edittmp.find('.cicon[data-type="'+tr.attr('data-type')+'"]').addClass('active').children('i').addClass('btn-success');
			edittmp.find('.cicon[data-type="'+tr.attr('data-type')+'"]').attr('data-link',tr.attr('data-link'));
			edittmp.fadeIn().show();
		}).on('click','#showtemaidedit #showpaddopciones #opcionlistadopestanias #verlistadoopciones .temadetremove',function(ev){			
			$(this).closest('tr').remove();
			let pnltable=$('#verlistadoopciones').children('table').children('tbody');
			let txtoptionjson={};
			pnltable.find('tr').each(function(i,v){
				tmptr=$(v);
				if(i>0)
				txtoptionjson[i]={
					nombre:tmptr.children('td.nombre').text(),
					id:tmptr.attr('data-id'),
					link:tmptr.attr('data-link'),
					type:tmptr.attr('data-type'),
					color:tmptr.attr('data-color'),
					colorfondo:tmptr.attr('data-colorfondo')
				}
			})
			/*let curt=curtema.txtjson;
			let txtjson={	
				options:txtoptionjson,
				tipo:'#showpaddopciones',
				tipofile:curt.tipofile,
				imagenfondo:curt.imagenfondo,
				infoavancetema:curt.infoavancetema
			};*/
			curtema.txtjson.options=txtoptionjson;			
			__disenioaddpestanias();
			_updatejsontema();
		}).on('click','#showtemaidedit #showpaddopciones #opcionlistadopestanias #editlistadoopcion #editpestaniacancel',function(ev){		
			let pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
			pnltmp.children('div').fadeOut().hide();
			pnltmp.children('#verlistadoopciones').fadeIn().show();
		}).on('click','#showtemaidedit #showpaddopciones #opcionlistadopestanias #editlistadoopcion #editpestaniasave',function(ev){	
			let pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
			let edittmp=pnltmp.children('#editlistadoopcion');
			let pnltable=$('#verlistadoopciones').children('table').children('tbody');
			let btnfileactive=edittmp.find('.cicon.active');
			let option={
				id:edittmp.attr('data-idedit'),
				nombre:edittmp.find('input[name="nombre"]').val(),
				color:edittmp.find('input[name="color"]').val(),
				colorfondo:edittmp.find('input[name="colorfondo"]').val(),
				link:btnfileactive.attr('data-link')||'',
				type:btnfileactive.attr('data-type')||''
			}
			let trexite=pnltable.children('tr[data-id="'+option.id+'"]');
			if(trexite.length==0){
				let trclone='<tr data-id="'+option.id+'" data-link="'+option.link+'" data-type="'+option.type+'" data-color="'+option.color+'" data-colorfondo="'+option.colorfondo+'"><td class="nombre">'+option.nombre+'</td><td><span class="temadetedit" style="padding:0.5ex;"><i class=" fa fa-pencil btn btn-sm btn-warning "></i></span><span class="temadetremove" style="padding:0.5ex;"><i class="btn btn-sm btn-danger fa fa-trash"></i></span></td></tr>';
				pnltable.append(trclone);
			}else {
				trexite.attr('data-link',option.link);
				trexite.attr('data-type',option.type);
				trexite.attr('data-color',option.color);
				trexite.attr('data-colorfondo',option.colorfondo);
				trexite.children('td.nombre').text(option.nombre);
			}

			let txtoptionjson={};
			pnltable.find('tr').each(function(i,v){
				tmptr=$(v);
				if(i>0)
				txtoptionjson[i]={
					nombre:tmptr.children('td.nombre').text(),
					id:tmptr.attr('data-id'),
					link:tmptr.attr('data-link'),
					type:tmptr.attr('data-type'),
					color:tmptr.attr('data-color'),
					colorfondo:tmptr.attr('data-colorfondo')
				}
			})
			curtema.txtjson.options=txtoptionjson;
			/*let curt=curtema.txtjson;
			let txtjson={	
				options:txtoptionjson,
				tipo:'#showpaddopciones',
				tipofile:curt.tipofile,
				imagenfondo:curt.imagenfondo,
				infoavancetema:curt.infoavancetema
			};*/			
			_updatejsontema();
			pnltmp.children('div').fadeOut().hide();
			pnltmp.children('#verlistadoopciones').fadeIn().show();
			__disenioaddpestanias();
		}).on('click','#showtemaidedit .cicon',function(ev){
			ev.preventDefault();
		    let it=$(this);
		    let type=it.attr('data-type')||'';
		    let datalink=it.attr('data-link')||'';
		    //if(type===''||type==datatype) return false;
		    it.attr('data-oldmedia',datalink)
		    it.siblings().removeAttr('data-oldmedia').children('i.btn-success').removeClass('btn-success').addClass('btn-primary');	
		    let _cont=$('#bookplantilla').children('#contentpages');
		    switch (type){
		    	case 'smarticlock':
			    	__addtics(it,1,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		it.attr('data-link',link);
			    		it.attr('data-type','smarticlock');
			    		it.attr('data-oldmedia',link);				    		
						__asiganacionfile(it);
			    	});
			        break;
			    case 'smarticlookPractice':
			    	__addtics(it,2,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		it.attr('data-link',link);
			    		it.attr('data-type','smarticlookPractice');
			    		it.attr('data-oldmedia',link);				    		
						__asiganacionfile(it);
			    	});
			        break;
			    case 'smarticDobyyourselft':
			    	__addtics(it,3,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		it.attr('data-link',link);
			    		it.attr('data-type','smarticDobyyourselft');
			    		it.attr('data-oldmedia',link);				    		
						__asiganacionfile(it); 
			    	});
			        break;
			    case 'game':
			    	__addgame(it,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		it.attr('data-link',link);
			    		it.attr('data-type','game');
			    		it.attr('data-oldmedia',link);				    		
						__asiganacionfile(it); 
			    	});
			        break;
			    case 'smartquiz':
			    	__addexamen(it,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		it.attr('data-link',link);
			    		it.attr('data-type','smartquiz');
			    		it.attr('data-oldmedia',link);				    		
						__asiganacionfile(it);
			    	});
			        break;			    
				case 'pdf':
					__subirfile({ file:it.children('i'), typefile:'pdf',guardar:true,dirmedia:'curso_'+idcurso+'/'},
						function(rs){
							it.attr('data-type','pdf');
			    			it.attr('data-link',rs.media);
			    			it.attr('data-oldmedia',rs.media);				    		
							__asiganacionfile(it);
						});
				    break;
				case 'html':
					__subirfile({ file:it.children('i'), typefile:'html',guardar:true,dirmedia:'curso_'+idcurso+'/'},
						function(rs){
							it.attr('data-type','html');
			    			it.attr('data-link',rs.media);
			    			it.attr('data-oldmedia',rs.media);				    		
							__asiganacionfile(it);
						});
				    break;
				case 'flash':
					__subirfile({ file:it.children('i'), typefile:'flash',guardar:true,dirmedia:'curso_'+idcurso+'/'},
						function(rs){
							it.attr('data-type','flash');
			    			it.attr('data-link',rs.media);
			    			it.attr('data-oldmedia',rs.media);				    		
							__asiganacionfile(it);
						});
				    break;
				case 'audio':
					__subirfile({ file:it.children('i'), typefile:'audio',guardar:true,dirmedia:'curso_'+idcurso+'/'},
						function(rs){
							it.attr('data-type','audio');
			    			it.attr('data-link',rs.media);
			    			it.attr('data-oldmedia',rs.media);				    		
							__asiganacionfile(it);
						});			    	
				    break;
				case 'video':
					__subirfile({ file:it.children('i'), typefile:'video',guardar:true,dirmedia:'curso_'+idcurso+'/'},
						function(rs){
							console.log(rs);
							it.attr('data-type','video');
			    			it.attr('data-link',rs.media);
			    			it.attr('data-oldmedia',rs.media);				    		
							__asiganacionfile(it);
						});		    	
				    break;
				case 'imagen':
					__subirfile({ file:it.children('i'), typefile:'imagen',guardar:true,dirmedia:'curso_'+idcurso+'/'},
						function(rs){
							it.attr('data-type','imagen');
			    			it.attr('data-link',rs.media);
			    			it.attr('data-oldmedia',rs.media);				    		
							__asiganacionfile(it);
						});
			        break;				 
				case 'texto':
						it.children('i').addClass('btn-success').removeClass('btn-primary');
						it.attr('data-idinfotmp','txt'+__idgui());				       
				        break;
				}
			ev.stopPropagation();
		}).on('click','#showppublicar .btnpublicar',function(ev){
			window.open(_sysUrlBase_+'/cursos/ver/?idcurso='+idcurso, '_blank');
		}).on('click','#showppublicar .btniralistado',function(ev){
			window.location.href=_sysUrlBase_+'/';
		})
		
		//$('#ultabs').find('a:first').tab('show');
		$('input.vercolor').minicolors({opacity: true,format:'rgb'});
		$('#pasoestruct').find('.btnplantilla[data-nombre="'+plantilla.nombre+'"]').children('a').addClass('btn-success');
		$('#showindice').find('.btnplantillaindice[data-nombre="'+infoindice+'"]').children('a').addClass('btn-success');
		updateprogress(infoavance);

		$('#paso01').on('show.bs.tab','#ultabs a',function(){
			let ahref=$(this).attr('href');
			if(ahref!='#pasoinfo'&&idcurso>0){
				if(ahref=='#pasocate'){
					$(this).closest('li').addClass('active');
					updateprogress(10);
					_initpnlcategorias();
				}else if(ahref=='#pasoestruct'){
					$(this).closest('li').addClass('active');
					updateprogress(20);
				}else if(ahref=='#pasosilabus'){
					$(this).closest('li').addClass('active');
					updateprogress(30);
					_initpnlSesiones();
				}
			}else if(idcurso==0){
				return false;
			}
		});

		/* Para manejo de la pagina */
		$('ul.menumaintop').on('click','.btnshowsesiones',function(ev){
			ev.preventDefault();
			let tipomenu=$(this).attr('data-menuen')||'top';
			if(tipomenu=='top')	
				$(this).closest('ul').toggleClass('active');
			else{
				$(this).closest('ul').removeClass('active');
				let bookplantilla=$('#previewcurso').children('#bookplantilla');
				let pnltmp=bookplantilla.children('.showindicepage').clone(true);
				let marcopage=bookplantilla.children('.marcopage');
				let contentPage=marcopage.children('#contentpages');
				contentPage.html('<div class="row" style="margin-top:1em;"><div class="col-md-3 col-sm-12 col-xs-12"></div><div id="addindice" class="col-md-6 col-sm-12 col-xs-12"></div><div class="col-md-3 col-sm-12 col-xs-12"></div></div>');
				pnltmp.find('._btnversesion').removeClass('col-md-6').addClass('col-12');
				contentPage.find('#addindice').append(pnltmp);
			}
			ev.stopPropagation();
		}).on('click','.btnshowhome',function(ev){
			ev.preventDefault();			
			_diseniarPortada();
			ev.stopPropagation();
		})

		$('#previewcurso').on('click','._vercontenido_',function(ev){
			ev.preventDefault();
			let menu=$(this);
			let link=menu.attr('data-link');
			let typelink=menu.attr('data-type')||'';
			if(link!=''&&typelink!='')
			__sysajax({
				fromdata:{link:link},
				type:'html',
				showmsjok:false,
    	    	url:_sysUrlBase_+'/plantillas/'+typelink,
    	    	callback:function(rs){
    	    		contentpages.html(rs);
    	    	}
    	    })
		}).on('click','._btnversesion',function(ev){
			ev.preventDefault();			
		    let iddet=$(this).attr('data-iddetalle');
		    tmpsesion=buscartema2_(temas,{por:'idet',idet:iddet},0).dato;		   
		    if(tmpsesion.idcursodetalle!=undefined){		    	
		    	try{if(typeof(tmpsesion.txtjson)=='string')tmpsesion.txtjson=JSON.parse(tmpsesion.txtjson);}catch(ex){}		    	 
				if(tmpsesion.txtjson.tipo=='#showpaddcontenido'){
					let link=tmpsesion.txtjson.link;
					let typelink=tmpsesion.txtjson.typelink||'';
					if(link!=''&&typelink!='')
					__sysajax({
						fromdata:{link:link},
						type:'html',
						showmsjok:false,
	        	    	url:_sysUrlBase_+'/plantillas/'+typelink,
	        	    	callback:function(rs){
	        	    		contentpages.html(rs);
	        	    	}
	        	    });
	   			}else if('#showpaddopciones')
		    	__disenioaddpestanias(tmpsesion);
		    }
		})

		$(document).on('click','body',function(ev){
			$('#bookplantilla').children('.menumaintop').removeClass('active');
		})
		
		var _initpnlcurso=function(){
			$.extend(datosdelcurso,infocurso.datos||{});		
			let pnlcont=$('#panelesultabs').children('#pasoinfo');
			let rimg=datosdelcurso.imagen?(urlmedia+datosdelcurso.imagen):imgdefecto;
			pnlcont.find('input[name="idcurso"]').val(datosdelcurso.idcurso||0);			
			pnlcont.find('input[name="nombre"]').val(datosdelcurso.nombre||'');
			pnlcont.find('input[name="autor"]').val(datosdelcurso.autor||nombreusuario);
			pnlcont.find('textarea[name="descripcion"]').val(datosdelcurso.descripcion||'');			
			pnlcont.find('input[name="aniopublicacion"]').val(datosdelcurso.aniopublicacion||datenow);
			pnlcont.find('img#verimg').attr('src',rimg);
			pnlcont.find('input[name="color"]').val(datosdelcurso.color);			
			pnlcont.find('input[name="color"]').minicolors('settings',{value:datosdelcurso.color});
		}

		var _initpnlcategorias=function(){
			let pnlcont=$('#panelesultabs').children('#pasocate');					
			if(__isEmpty(pnlcont)){
				__sysajax({donde:pnlcont,url:_sysUrlBase_+'/cursos/paso1categorias',mostrarcargando:true,type:'html',callback:function(rs){
						$.extend(datosdelcurso,infocurso.datos||{});											
						pnlcont.html(rs);
						let catego=infocurso.categorias;
						if(categorias.length>0 && catego.length>0){
							let tbcategorias=pnlcont.find('.tablecategorias');
							$.each(catego,function(i,v){							
								$.each(categorias,function(ii,vv){									
									if(v.idcategoria==vv.idcategoria){										
										let trclone=pnlcont.find('.trclone').clone(true);
										trclone.removeClass('trclone');
										trclone.children('td').first().text(v.categoria);
										trclone.attr('idcursocategoria',v.idcursocategoria);
										tbcategorias.append(trclone);
									}else if(vv.hijos.length>0){
										$.each(vv.hijos,function(hi,hv){
											if(hv.idcategoria==v.idcategoria){
												let trclone=pnlcont.find('.trclone').clone(true);
												trclone.removeClass('trclone');
												trclone.children('td:first').text(vv.nombre +' -> '+v.categoria);
												trclone.attr('idcursocategoria',v.idcursocategoria);
												tbcategorias.append(trclone);
											}
										})
									}
								});
							});
						}
					}
				});
			}
		}
		var _initpnlSesiones=function(){
			$.extend(datosdelcurso,infocurso.datos||{});
			let pnlcont=$('#panelesultabs').children('#pasosilabus');
			if(__isEmpty(pnlcont)){				
				__sysajax({donde:pnlcont,url:_sysUrlBase_+'/cursos/paso1temas',mostrarcargando:true,type:'html',callback:function(rs){
						$.extend(temas,infocurso.temas||{});									
						pnlcont.html(rs);
						let dt=pnlcont.find('#mindice');
						addtemas(dt,temas);
					}
				});
			}
		}
		var _initpnlEstructura=function(){
			let pnlestruc=$('#showestiloestructura');
			let img=estructura.image||'';			
			if(img!='')	pnlestruc.find('img#verimg').attr('src',img);
			pnlestruc.find('input[name="fontsize"]').val(estructura['font-size'].replace('px',''));
			pnlestruc.find('input[name="color"]').val(estructura.color);
			pnlestruc.find('input[name="color"]').minicolors('settings',{value:estructura.color});
			pnlestruc.find('input[name="font"]').val(estructura['font-family']);
			pnlestruc.find('input[name="colorfondo"]').val(estructura['background-color']);
			pnlestruc.find('input[name="colorfondo"]').minicolors('settings',{value:estructura['background-color']});

			let pnlestilopage=$('#showestilopage');
			let img2=estilopagina.image||'';
			if(img2!='') pnlestilopage.find('img#verimg').attr('src',img2);
			pnlestilopage.find('input[name="fontsize"]').val(estilopagina['font-size'].replace('px',''));
			pnlestilopage.find('input[name="color"]').val(estilopagina.color);
			pnlestilopage.find('input[name="color"]').minicolors('settings',{value:estructura.color});
			pnlestilopage.find('input[name="font"]').val(estilopagina['font-family']);
			pnlestilopage.find('input[name="colorfondo"]').val(estilopagina['background-color']);
			pnlestilopage.find('input[name="colorfondo"]').minicolors('settings',{value:estructura['background-color']});
			//estilopagina
			_diseniarcurso();
			let pnlportada=$('#showportada');
			let img3=infoportada.image||imgdefecto;			
			pnlportada.find('img#verimg').attr('src',img3);
			pnlportada.find('textarea[name="titulo"]').val(infoportada.titulo);
			pnlportada.find('textarea[name="descripcion"]').val(infoportada.descripcion);
		}
		_initpnlcurso();
		_diseniarIndice({},false);
		_initpnlEstructura();

	})
</script>