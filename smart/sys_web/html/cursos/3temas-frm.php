<?php 
$idgui=uniqid(); 
$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
$fileurl=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:$imgcursodefecto;
?>
<form id="frmdatosdeltema" method="post" target="" enctype="multipart/form-data">
	<input type="hidden" name="idcurso" 		id="idcurso" value="0">
	<input type="hidden" name="idcursodetalle" 	id="idcursodetalle" value="0">
	<input type="hidden" name="idpadre" 		id="idpadre" value="0">
	<input type="hidden" name="orden" 			id="orden" value="0">

	<input type="hidden" name="idrecurso" 		id="idrecurso" value="0">
	<input type="hidden" name="idrecursopadre" 	id="idrecursopadre" value="0">
	<input type="hidden" name="idrecursoorden" 	id="idrecursoorden" value="0">

	<input type="hidden" name="tiporecurso" id="tiporecurso" value="0">
	<input type="hidden" name="idlogro" id="idlogro" value="0">				
	<input type="hidden" name="oldimagen" id="oldimagen" value="0">
	<input type="hidden" name="accmenu" id="accmenu" value="0">
	<input type="hidden" id="esfinal" name="esfinal" value="1" >

	<div class="card text-center shadow">				 
	  <div class="card-body">
	    <!--h5 class="card-title"><?php //echo JrTexto::_('Datos generales del curso');?></h5-->
	    <div class="row">
	    	<div class="col-12 col-sm-6 col-md-6">
	    		<div class="row">
		    		<div class="col-12 form-group">
			            <label style=""><?php echo JrTexto::_('Nombre del Tema(Sesión)');?></label> 
			            <input type="text" autofocus name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Tema 001"))?>" >					           
			        </div>
			       
			        
			        <div class="col-12 form-group">
			            <label style=""><?php echo JrTexto::_('Descripción');?></label> 
			            <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del tema"))?>"></textarea>
			        </div>
			       							        
			        <div class="col-6 form-group text-left ">		        	
			            <label style=""><?php echo JrTexto::_('Color');?> </label> 
			            <input type="" name="color" value="transparent" class="form-control" >			       
			        </div>
			        <div>
			        	
			        </div>		       
                </div>		       
	    	</div>
	    	<div class="col-12 col-sm-6 col-md-6">
	    		<div class="row">
		    		<div class="col-12 form-group text-center">
			            <label style="float: none"><?php echo JrTexto::_('Portada');?></label> 
			            <div style="position: relative;" class="frmchangeimage">
			                <div class="toolbarmouse text-center">                  
			                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
			                  <span class="btn resetfile"><i class="fa fa-trash"></i></span>
			                </div>			               
		                	<img id="verimg" src="<?php echo $imgcursodefecto; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">			               
			                <small>Tamaño 250px x 360px</small>                
			            </div>				           
			        </div>
		        </div>
	    	</div>
	    </div>	    			   
	  </div>
	  <div class="card-footer text-muted">
	  	<button type="button" class="btncancelar btn btn-warning"><i class=" fa fa-close"></i> Cancelar</button>
		<button type="submit" class="btn btn-primary btnguardartema"><i class=" fa fa-save"></i> Guardar Tema</button>
	  </div>
	</div>
</form>
