<?php 
$idgui=uniqid(); 
$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
$fileurl=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:$imgcursodefecto;
?>
<form id="frmdatosdelcurso" method="post" target="" enctype="multipart/form-data">
	<input type="hidden" name="idcurso" id="idcurso" value="<?php echo @$idcurso; ?>">
	<div class="card text-center shadow">				 
	  <div class="card-body">
	    <!--h5 class="card-title"><?php //echo JrTexto::_('Datos generales del curso');?></h5-->
	    <div class="row">
	    	<div class="col-12 col-sm-6 col-md-6">
	    		<div class="row">
	    		<div class="col-md-12 form-group">
		            <label style=""><?php echo JrTexto::_('Nombre del curso');?></label> 
		            <input type="text" autofocus name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Curso 01"))?>" >					           
		        </div>
		        <div class="col-md-6 col-sm-12 form-group">							        	
		            <label style=""><?php echo JrTexto::_('Autor');?></label> 
		            <input type="text" name="autor" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Apellidos y Nombres del autor "))?>">					           
		        </div>
		        <div class="col-md-6 col-sm-12 form-group">
		            <label style=""><?php echo JrTexto::_('Fecha de publicación');?></label> 
		            <input type="date" name="aniopublicacion" required="required"  class="form-control" placeholder="<?php echo date('Y-m-d'); ?>" >
		        </div>
		        <div class="col-md-12 form-group">
		            <label style=""><?php echo JrTexto::_('Reseña');?></label> 
		            <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso"))?>"></textarea>
		        </div>
		       							        
		        <div class="col-md-6 col-sm-12 form-group text-left ">		        	
		            <label style=""><?php echo JrTexto::_('Color');?> </label> 
		            <input type="" name="color" value="transparent" class="form-control" >			       
		        </div>
		        <div>
		        	
		        </div>
		        <!--div class="col-6 form-group text-right">
			        <label style="float: none;"><?php echo JrTexto::_('Publicado');?> </label>              
			        <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$curso["estado"]==1?"fa-check-circle":"fa-circle-o";?>">
			        	<span> <?php echo JrTexto::_(@$curso["estado"]==1?"Si":"No");?></span>
			        	<input type="hidden" id="estado<?php echo $idgui; ?>" name="estado" value="<?php echo !empty($curso["estado"])?$curso["estado"]:0;?>" > 
			        </a>
                </div-->
                </div>		       
	    	</div>
	    	<div class="col-12 col-sm-6 col-md-6">
	    		<div class="row">
		    		<div class="col-12 form-group text-center">
			            <label style="float: none"><?php echo JrTexto::_('Portada');?></label> 
			            <div style="position: relative;" class="frmchangeimage">
			                <div class="toolbarmouse text-center">                  
			                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
			                  <span class="btn resetfile"><i class="fa fa-trash"></i></span>
			                </div>			               
		                	<img id="verimg" src="<?php echo $imgcursodefecto; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">			               
			                <small>Tamaño 250px x 360px</small>                
			            </div>				           
			        </div>
		        </div>				    		
	    	</div>
	    </div>
	    <div class="row">
	    	<div class="col-12">
	    		
	    	</div>
	    </div>			   
	  </div>
	  <div class="card-footer text-muted">
	  	<button type="button" onclick="redir(_sysUrlBase_+'/');" class="btn btn-warning"><i class=" fa fa-undo"></i> Regresar al listado</button>
	    <button type="submit" class="btn btn-primary"><i class=" fa fa-save"></i> Guardar y continuar</button>
	  </div>
	</div>
</form>
