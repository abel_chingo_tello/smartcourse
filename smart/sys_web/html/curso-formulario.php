<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$curso=!empty($this->curso)?$this->curso:'';
$idcurso=!empty($curso["idcurso"])?$curso["idcurso"]:'';
$fileurl=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:URL_MEDIA.'/static/media/cursos/nofoto.jpg';
//var_dump($curso);
?>
<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-12" style="padding: 1em;">
				<form id="frm<?php echo $idgui;?>" method="post" target="" enctype="multipart/form-data">
				<input type="hidden" name="idcurso" id="idcurso<?php echo $idgui; ?>" value="<?php echo @$idcurso; ?>">
				<div class="card text-center">				 
				  <div class="card-body">
				    <h5 class="card-title"><?php echo JrTexto::_('Datos generales del curso');?></h5>
				    <div class="row">
				    	<div class="col-12 col-sm-6 col-md-6">
				    		<div class="row">
				    		<div class="col-12 form-group">
					            <label style=""><?php echo JrTexto::_('Nombre del curso');?></label> 
					            <input type="text" name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Curso 01"))?>" value="<?php echo @$curso["nombre"]; ?>">					           
					        </div>
					        <div class="col-12 form-group">
					            <label style=""><?php echo JrTexto::_('Descripción');?></label> 
					            <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso"))?>"><?php echo @$curso["descripcion"]; ?></textarea>
					        </div>
					        
					        <div class="col-6 form-group text-left ">
					            <label style=""><?php echo JrTexto::_('Color');?> </label>  
					            <input type="color" name="color" id="color<?php echo $idgui; ?>" class="<?php echo empty($curso["color"])?'sincolor':''; ?>" data-color="#007bff" style="margin-left: 1ex; <?php echo empty($curso["color"])?'display:none;':''; ?>" value="<?php echo @$curso["color"]; ?>"><br>
					            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
						        <a style="cursor:pointer;" class="chkoption fa  <?php echo @$curso["color"]==''?"fa-check-circle":"fa-circle-o";?>" data-value="<?php echo @$curso["color"];?>">
						        	<span> <?php echo JrTexto::_(@$curso["color"]==''?"Si":"No");?></span>						        	
						        </a>
					        </div>

					        <div class="col-6 form-group text-right">
						        <label style="float: none;"><?php echo JrTexto::_('Publicado');?> </label>              
						        <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$curso["estado"]==1?"fa-check-circle":"fa-circle-o";?>">
						        	<span> <?php echo JrTexto::_(@$curso["estado"]==1?"Si":"No");?></span>
						        	<input type="hidden" id="estado<?php echo $idgui; ?>" name="estado" value="<?php echo !empty($curso["estado"])?$curso["estado"]:0;?>" > 
						        </a>
                            </div>
                            </div>		       
				    	</div>
				    	<div class="col-12 col-sm-6 col-md-6">
				    		<div class="row">
					    		<div class="col-12 form-group text-center">
						            <label style="float: none"><?php echo JrTexto::_('Portada');?></label> 
						            <div style="position: relative;" class="frmchangeimage">
						                <div class="toolbarmouse text-center">                  
						                  <span class="btn btnaddfile">
						                  	<input type="file" class="input-file-invisible" name="imagen" id="imagen<?php echo $idgui; ?>" accept="image/x-png,image/gif,image/jpeg"><i class="fa fa-pencil"></i></span>
						                </div>
						                <div id="sysfileimagen<?php echo $idgui; ?>">
						                	<img id="verimg<?php echo $idgui; ?>" src="<?php echo $fileurl; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
						                </div>
						                <small>Tamaño 250px x 360px</small>                
						            </div>				           
						        </div>
					        </div>				    		
				    	</div>
				    </div>				   
				  </div>
				  <div class="card-footer text-muted">
				  	<button type="button" onclick="redir(_sysUrlBase_+'/');" class="btn btn-warning"><i class=" fa fa-list"></i> Regresar al listado</button>
				    <button type="submit" class="btn btn-primary"><i class=" fa fa-save"></i> Guardar y continuar</button>
				  </div>
				</div>
				</form>
			</div>
		</div>
	</div>	
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frm<?php echo $idgui;?>').on('submit',function(ev){
			ev.preventDefault();			
			var data=__formdata('frm<?php echo $idgui;?>');
			if($('#color<?php echo $idgui; ?>').hasClass('sincolor')) data.append('color','');
			__sysajax({
				fromdata:data,
                url:_sysUrlBase_+'/acad_curso/guardarAcad_curso',
                callback:function(rs){
                	var imagen=rs.newimagen;
                	$('#idcurso<?php echo $idgui; ?>').val(rs.newid);
                	if($('input#idcursoprincipal').length) $('input#idcursoprincipal').val(rs.newid);
                	if(imagen!=''){
                		$('#verimg<?php echo $idgui; ?>').attr('src',imagen);
                	}
                	//__cargarplantilla({url:'/peditar',data:{idcurso:rs.newid}});
                	redir(_sysUrlBase_+'/defecto/silabus/?idcurso='+rs.newid);
                }
			});
			return false;
		});
		$('.chkformulario').on('click',function(ev){
		    if($(this).hasClass('fa-circle-o')){
		        $('span',this).text(' <?php echo JrTexto::_("Si");?>');
		        $('input',this).val(1);
		        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
		    }else{
		        $('span',this).text(' <?php echo JrTexto::_("No");?>');
		        $('input',this).val(0);
		        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
		    }      
		});

		$('.chkoption').on('click',function(ev){ 
	    	var input= $(this).siblings('input');   
	      	if($(this).hasClass('fa-circle-o')){
	        	$('span',this).text(' <?php echo JrTexto::_("Si");?>');	
	        	input.addClass('sincolor');
	       		input.attr('data-color',input.val()).val('').hide();
	        	$(this).removeClass('fa-circle-o').addClass('fa-check-circle');
	      	}else{
	        	$('span',this).text(' <?php echo JrTexto::_("No");?>');
	        	input.removeClass('sincolor');
	        	input.val(input.attr('data-color')).show();		        	
	        	$(this).addClass('fa-circle-o').removeClass('fa-check-circle');
	      	}      
	    });	
		var inputimagen<?php echo $idgui; ?> = document.getElementById('imagen<?php echo $idgui; ?>');  
		inputimagen<?php echo $idgui; ?>.addEventListener('change', addfileimagen<?php echo $idgui; ?>, false);
		function addfileimagen<?php echo $idgui; ?>(ev) {
		  var file=ev.target.files[0];
		  var tipo='imagen';
		  var donde=$('#sysfileimagen<?php echo $idgui; ?>');
		  var reader=new FileReader();
		  reader.onload=function(ev_){
		    var filetmp=document.getElementById('verimg<?php echo $idgui; ?>');          
		    filetmp.style.width='60%';
		    filetmp.style.height='200px';
		    filetmp.classList.add("img-responsive");
		          filetmp.classList.add("center-block");
		          filetmp.src=ev_.target.result;
		    if(filetmp!='') donde.html('').append(filetmp);
		  }
		  reader.readAsDataURL(file);
		}
	})
</script>
