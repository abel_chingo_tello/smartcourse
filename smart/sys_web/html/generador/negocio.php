<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$nuevalista='';
$txtradiolistadosimple='';
echo '<?php
'; ?>
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		<?php echo date("d-m-Y") ?>

 * @copyright	Copyright (C) <?php echo date("d-m-Y") ?>. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::Dat<?php echo  $tb ?>', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
<?php
$x='';
$getid='';
$settxt='';
		$strcampos='';
		//echo implode(',', $this->campos);
		foreach ($this->campos as $campo) {
			if(@$frm["tipo_".$campo]=='fk'){
						if(!empty($frm["tipo2_".$campo])){
							$tb2=$frm["tipo2_".$campo];							
							if(!empty($frm["tipofkid_".$campo])&&!empty($frm["tipofkver_".$campo])){
								$fkid=$frm["tipofkid_".$campo];

$nuevalista.='

	/****************************** FK '.$tb2.' ***************************/
	/*public function listar'.substr($tb2,4).'(){
		try {
			return $this->oDat'.$tb.'->listar'.substr($tb2,4).'();			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listarX'.$campo.'(){
		try {
			return $this->oDat'.$tb.'->listarX'.$campo.'($this->'.$fkid.');
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}*/
	';
							}
						}
					}elseif((@$frm["tipo_".$campo]=='radiobutton'||  @$frm["tipo_".$campo]=='checkbox' || @$frm["tipo_".$campo]=='combobox')&&$frm["tipo2_".$campo]=='simple'){
						$txtradiolistadosimple='
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDat'.$tb.'->cambiarvalorcampo($this->'.$this->pk.',$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						';
					}


					if($this->pk!=$campo){
					$strcampos.=',$this->'.$campo;			
$settxt.='private function set'.ucfirst($campo).'($'.strtolower($campo).')
	{
		try {
			$this->'.$campo.'= NegTools::validar(\'todo\', $'.strtolower($campo).', false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	';}

	$getid.='$this->'.$campo.' = $this->data'.$tb.'["'.$campo.'"];
			';
$x.= "protected $".$campo.";
	";

	}
?>
class Neg<?php echo  $tb ?> 
{
	<?php  echo $x; ?>

	protected $data<?php echo  $tb ?>;
	protected $oDat<?php echo  $tb ?>;	

	public function __construct()
	{
		$this->oDat<?php echo  $tb ?> = new Dat<?php echo  $tb ?>;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDat<?php echo $tb; ?>->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDat<?php echo $tb; ?>->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDat<?php echo $tb; ?>->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDat<?php echo $tb; ?>->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDat<?php echo $tb; ?>->get($this-><?php echo $this->pk; ?>);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('<?php echo strtolower($this->tabla); ?>', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDat<?php echo $tb; ?>->iniciarTransaccion('neg_i_<?php echo $tb; ?>');
			$this-><?php echo $this->pk ?> = $this->oDat<?php echo $tb ?>->insertar(<?php echo substr($strcampos, 1) ?>);
			$this->oDat<?php echo $tb; ?>->terminarTransaccion('neg_i_<?php echo $tb; ?>');	
			return $this-><?php echo $this->pk ?>;
		} catch(Exception $e) {	
		    $this->oDat<?php echo $tb; ?>->cancelarTransaccion('neg_i_<?php echo $tb; ?>');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('<?php echo strtolower($this->tabla); ?>', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDat<?php echo $tb; ?>->actualizar($this-><?php echo $this->pk; ?><?php echo $strcampos?>);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	<?php echo $txtradiolistadosimple; ?>

	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('<?php echo $tb; ?>', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDat<?php echo $tb; ?>->eliminar($this-><?php echo $campopk; ?>);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function set<?php echo ucfirst($this->pk); ?>($pk){
		try {
			$this->data<?php echo  $tb ?> = $this->oDat<?php echo $tb; ?>->get($pk);
			if(empty($this->data<?php echo  $tb ?>)) {
				throw new Exception(JrTexto::_("<?php echo $tb; ?>").' '.JrTexto::_("not registered"));
			}
			<?php echo $getid; ?>
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('<?php echo strtolower($this->tabla); ?>', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->data<?php echo $tb; ?> = $this->oDat<?php echo $tb; ?>->get($pk);
			if(empty($this->data<?php echo  $tb ?>)) {
				throw new Exception(JrTexto::_("<?php echo $tb; ?>").' '.JrTexto::_("not registered"));
			}

			return $this->oDat<?php echo  $tb ?>->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	<?php //echo $settxt;?>
	<?php //echo $nuevalista; ?>

}