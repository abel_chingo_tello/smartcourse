<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$txtcamposave='';
$txtcamposavexajax='';
$file=false;
$fileimg=false;
$jsradiolistadosimple=false;
$addjs='';
$addfk='';
$addtools=null;
$jsfileimg=null;
$txtcamposaveimgnuevo=null;
$txtcamposaveimgeditar=null;
$txtcamposaveajax=null;
$txtaddfiltros=null;
//fk variables
$txtaddclase=null;
$txtadddec=null;
$txtadddec2=null;
foreach ($campos as $campo) {	
	if(@$frm["tipo_".$campo]=='file') {
		if($frm["tipo2_".$campo]=='imagen'){
			if($fileimg==false){
				$jsfileimg.='
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/cropper.min.js");
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/main3.js");
				';
				$fileimg=true;
			}

			$addtools='JrCargador::clase("sys_negocio::sistema::NegTools", RUTA_SITIO, "sys_negocio/sistema");';
			$txtcamposaveimgnuevo.='
						$txt'.ucfirst($campo).'=NegTools::subirImagen("'.$campo.'_",@$frm["txt'.ucfirst($campo).'"], "'.strtolower($tb).'",false,100,100 );
						$this->oNeg'.$tb."->__set('".$campo."',".'@$txt'.ucfirst($campo).');
						';
			$txtcamposaveimgeditar.='
						$archivo=basename($frm["txt'.ucfirst($campo).'_old"]);
						if(!empty($frm["txt'.ucfirst($campo).'"])) $txt'.ucfirst($campo).'=NegTools::subirImagen("'.$campo.'_",@$frm["txt'.ucfirst($campo).'"], "'.strtolower($tb).'",false,100,100 );
						$this->oNeg'.$tb."->__set('".$campo."',".'@$txt'.ucfirst($campo).');
						@unlink(RUTA_SITIO . SD ."static/media/'.strtolower($tb).'/".$archivo);
						';

		}else{
			if($campo!=$campopk){
				$txtcamposave.='$this->oNeg'.$tb."->".$campo.'=@$txt'.ucfirst($campo).';
					';
				$txtcamposaveajax.='$this->oNeg'.$tb."->".$campo.'=@frm["txt'.ucfirst($campo).'"];
					';

			}
			
		}

	}else{
		if(@$frm["tipo_".$campo]=='fk'&&@$frm["tipofkcomo_".$campo]=='system'){
			if($campo!=$campopk){
				$txtcamposave.='$this->oNeg'.$tb."->".$campo.'=@$this->usuario["idusuario"];
					';

				$txtcamposaveajax.='$this->oNeg'.$tb."->".$campo.'=@$this->usuario["idusuario"];
					';

			}		
		}else{
			if($campo!=$campopk){
				$txtcamposave.='$this->oNeg'.$tb."->".$campo.'=@$txt'.ucfirst($campo).';
					';
				$txtcamposaveajax.='$this->oNeg'.$tb."->".$campo.'=@$frm["txt'.ucfirst($campo).'"];
					';
				
			}

		
		}
		$txtaddfiltros.='if(isset($_REQUEST["'.$campo.'"])&&@$_REQUEST["'.$campo.'"]!=\'\')$filtros["'.$campo.'"]=$_REQUEST["'.$campo.'"];
			';

	}

	if((@$frm["tipo_".$campo]=='radiobutton'||  @$frm["tipo_".$campo]=='checkbox' || @$frm["tipo_".$campo]=='combobox')&& $frm["tipo2_".$campo]=='simple'){
		$jsradiolistadosimple=true;
	}
	if(@$frm["tipo_".$campo]=='date'&& @$frm["tipo2_".$campo]=='user'){
		$addjs.='
		$this->documento->script(\'jquery.datetimepicker\', \'/libs/datetimepicker-master/\');
		$this->documento->stylesheet(\'jquery.datetimepicker\', \'/libs/datetimepicker-master/\');
		';
	}
	if(@$frm["tipo_".$campo]=='textArea'){
		if(@$frm["tipo2_".$campo]=='tinymce'){
			$addjs.='
			$this->documento->script(null, \'//tinymce.cachefly.net/4.0/tinymce.min.js\');
			';
		}
		if(@$frm["tipo2_".$campo]=='chkeditor'){
			$addjs.='
			$this->documento->script(null, \'//cdn.ckeditor.com/4.5.1/standard-all/ckeditor.js\');
			$this->documento->script(null, \'//cdn.ckeditor.com/4.5.1/standard-all/samples/old/assets/uilanguages/languages.js\');
			';
		}
	}

	if(@$frm["tipo_".$campo]=='fk'){
		$tb2=$frm["tipo2_".$campo];
		if(@$frm["tipofkcomo_".$campo]=='system'){
			$addfk.='
			$this->fk'.$campo.'=$this->usuario;';
		}else{
		$addfk.='$this->fk'.$campo.'=$this->oNeg'.ucfirst($tb2).'->buscar();
			';
		}
		$txtaddclase.='JrCargador::clase(\'sys_negocio::Neg'.ucfirst($tb2).'\', RUTA_BASE, \'sys_negocio\');
';
		$txtadddec.='private $oNeg'.ucfirst($tb2).';
	';
		$txtadddec2.='$this->oNeg'.ucfirst($tb2).' = new Neg'.ucfirst($tb2).';
	';

		


	}



}
echo '<?php
'; ?>
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		<?php echo date("d-m-Y") ?>
 
 * @copyright	Copyright (C) <?php echo date("d-m-Y") ?>. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::Neg<?php echo  $tb ?>', RUTA_BASE, 'sys_negocio');
<?php echo $txtaddclase; ?>
class Web<?php echo  $tb ?> extends JrWeb
{
	private $oNeg<?php echo  $tb ?>;
	<?php echo $txtadddec;?>
	
	public function __construct()
	{
		parent::__construct();		
		$this->oNeg<?php echo  $tb ?> = new Neg<?php echo  $tb ?>;
		<?php echo $txtadddec2; ?>
		
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('<?php echo $tb; ?>', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			<?php echo $txtaddfiltros; ?>

			$this->datos=$this->oNeg<?php echo  $tb ?>->buscar($filtros);
			<?php echo $addfk; ?>
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('<?php echo $tb ?>'), true);
			$this->esquema = '<?php echo $this->tabla; ?>-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('<?php echo $tb; ?>', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('<?php echo $tb; ?>').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('<?php echo $tb; ?>', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNeg<?php echo  $tb ?>-><?php echo $campopk; ?> = @$_GET['id'];
			$this->datos = $this->oNeg<?php echo  $tb ?>->data<?php echo  $tb ?>;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('<?php echo $tb; ?>').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			<?php echo $addfk; ?>

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = '<?php echo $this->tabla; ?>-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('<?php echo $tb; ?>', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			<?php echo $txtaddfiltros; ?>
			
			$this->datos=$this->oNeg<?php echo  $tb ?>->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar<?php echo  $tb ?>(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pk<?php echo ucfirst($campopk); ?>)) {
				$this->oNeg<?php echo $tb;?>-><?php echo $campopk; ?> = $frm['pk<?php echo ucfirst($campopk); ?>'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	           	<?php echo $addtools;?>

				<?php echo $txtcamposave; ?>

            if($accion=='_add') {
            	$res=$this->oNeg<?php echo $tb;?>->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('<?php echo $tb;?>')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNeg<?php echo $tb;?>->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('<?php echo $tb;?>')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSave<?php echo  $tb ?>(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pk<?php echo ucfirst($campopk); ?>'])) {
					$this->oNeg<?php echo $tb;?>-><?php echo $campopk; ?> = $frm['pk<?php echo ucfirst($campopk); ?>'];
				}
				<?php echo $addtools;?>

				<?php echo $txtcamposaveajax; ?>

				   if(@$frm["accion"]=="Nuevo"){
					<?php echo $txtcamposaveimgnuevo; ?>
				    $res=$this->oNeg<?php echo $tb;?>->agregar();
					}else{
					<?php echo $txtcamposaveimgeditar;?>
				    $res=$this->oNeg<?php echo $tb;?>->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNeg<?php echo $tb;?>-><?php echo $campopk; ?>);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxID<?php echo $tb; ?>(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNeg<?php echo $tb; ?>->__set('<?php echo $campopk; ?>', $pk);
				$this->datos = $this->oNeg<?php echo  $tb ?>->data<?php echo  $tb ?>;
				$res=$this->oNeg<?php echo $tb;?>->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNeg<?php echo $tb; ?>->__set('<?php echo $campopk; ?>', $pk);
				$res=$this->oNeg<?php echo $tb;?>->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNeg<?php echo $tb; ?>->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}