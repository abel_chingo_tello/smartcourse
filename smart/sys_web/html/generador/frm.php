<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$datoslistadoheader='';
$datoslistado='';
$addjschk=false;
$jsdate=false;
$jsdatetime=false;
$jstime=false;
$jsverchkeditor=false; 
$jsverwysihtml5 =false;
$jsvertinymce=false;
$jsfile=false;
$jschkformulario=false;
$jschkformulariomultiple=false;
$addimgsave=null;
$jsdatetxt=null;
$jsdatetimetxt=null;
?>
<?php
echo '<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->datos)) $frm=$this->datos;
$file=\'<img src="\'.$this->documento->getUrlStatic().\'/media/nofoto.jpg" class="img-responsive center-block">\';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
';
?>
<?php echo '<?php if(!$ismodal){?>';?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo '<?php echo $this->documento->getUrlBase();?>';?>"><i class="fa fa-home"></i>&nbsp;<?php echo '<?php echo JrTexto::_("Home"); ?>';?></a></li>
    <!--li><a href="<?php echo '<?php echo $this->documento->getUrlBase();?>';?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo '<?php echo JrTexto::_("Academic"); ?>';?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo '<?php echo JrTexto::_("'.$tb.'"); ?>';?></li>
  </ol>
</nav>
<?php echo '<?php } ?>';?>
<div class="row"  id="vent-<?php echo '<?php echo $idgui;?>'; ?>"  >
  <div class="col">
    <div id="msj-interno"></div>
    <form method="post" id="frm-<?php echo '<?php echo $idgui;?>'; ?>"  target="" enctype="" class="form-horizontal form-label-left" >
    <input type="hidden" name="<?php echo ucfirst($campopk); ?>" id="<?php echo $campopk.'<?php echo $idgui; ?>'; ?>" value="<?php echo '<?php echo $this->pk;?>';?>">
    <input type="hidden" name="accion" id="accion<?php echo '<?php echo $idgui; ?>';?>" value="<?php echo '<?php echo JrTexto::_($this->frmaccion);?>';?>">
    <div class="row">
          <?php if(!empty($campos))
          foreach ($campos as $campo){
            $Campo=ucfirst($campo);
            $idcampo=$campo.'<?php echo $idgui; ?>';
            $verenlistado='_chk'.$campo;
            $tipo=@$frm['tipo_'.$campo];
            if($campo!=$campopk){
              //if($tipo!='fk'){
                if(@$frm["tipo2_".$campo]!='system'){ 
            ?><div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo "<?php echo JrTexto::_('".str_replace('_', ' ',$Campo)."');?>"; ?> <span class="required"> (*) </span></label>              
              <?php }
//////////////////////////////////////////////------------text -------------------
              if($tipo=='text'||$tipo=='double'||$tipo=='decimal'||$tipo=='money'){
              ?>  <input type="text"  id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" required="required" class="form-control" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">
              <?php 

//////////////////////////////////////////////------------email -------------------
              }elseif($tipo=='email'){
              ?>  <input type="email" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" required="required" class="form-control" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">
              <?php 

//////////////////////////////////////////////------------date -------------------
              }elseif($tipo=='date'){
                    if($frm["tipo2_".$campo]=='user'){$jsdate=true;
                      $jsdatetxt.="
  $('#".$idcampo."').datetimepicker({ /*lang:'es', timepicker:false,*/ format:'YYYY/MM/DD'}); 
  ";
              ?>  <input name="<?php echo $idcampo ?>" class="verdate form-control" required="required" type="date" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
              <?php }elseif($frm["tipo2_".$campo]=='system'){
              ?>  <input type="hidden" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("Y/m/d") ?>' ?>">

              <?php } 

//////////////////////////////////////////////------------datetime -------------------
              }elseif($tipo=='datetime'){
                    if($frm["tipo2_".$campo]=='user'){$jsdatetime=true;
  $jsdatetimetxt.="                   
  $('#".$idcampo."').datetimepicker({ /*lang:'es', timepicker:false,*/ format:'YYYY/MM/DD HH:mm:ss'}); 
  ";  
              
              ?>  <input id="<?php echo $idcampo ?>" name="<?php echo $campo ?>"  class="form-control" required="required" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
              <?php }elseif($frm["tipo2_".$campo]=='system'){
              ?>  <input type="hidden" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("Y/m/d H:i:s"); ?>' ?>">

              <?php }

//////////////////////////////////////////////------------time -------------------
              }elseif($tipo=='time'){
                      if($frm["tipo2_".$campo]=='user'){$jstime=true;
 $jsdatetimetxt.="                   
  $('#".$idcampo."').datetimepicker({ /*lang:'es', timepicker:false,*/ format:'HH:mm:ss'}); 
  ";
                ?>  <input name="<?php echo $idcampo ?>" id="<?php echo $campo ?>" class="form-control" required="required" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
                <?php }elseif($frm["tipo2_".$campo]=='system'){
                ?>  <input type="hidden"  id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("H:i") ?>' ?>">

                <?php }

//////////////////////////////////////////////------------combobox -------------------
              }elseif($tipo=='combobox'){
              ?> <select id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control" required>
              <option value=""><?php echo '<?php JrTexto::_("----");?>';?></option>
                <?php if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); ?>

              <option value=<?php echo $valoresopt[0]; ?>><?php echo "<?php echo JrTexto::_(".$valoresopt[0].");?>"; ?></option>
              <option value=<?php echo $valoresopt[1]; ?>><?php echo "<?php echo JrTexto::_(".$valoresopt[1].");?>"; ?></option>
                  <?php }else{ ?>
              <option value="1"><?php echo "<?php echo JrTexto::_('Active');?>"; ?></option>
              <option value="0"><?php echo "<?php echo JrTexto::_('Inactive');?>"; ?></option>
                  <?php } ?>
                <?php }else{ 
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ ?> 
              <option value="<?php echo $i;?>"> <?php echo "<?php echo JrTexto::_('".$i."');?> ".$i; ?></option>
              <?php } }
              ?> 
              </select>
              <?php  

//////////////////////////////////////////////------------checkbox -------------------
              }elseif($tipo=='checkbox'){
                $jschkformulario=true;
                if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); 
                  $jschkformulario=true;
              ?> 
              <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>"
                data-valueno=<?php echo $valoresopt[1];?> data-value2="1">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[0].':'.$valoresopt[1].');?>'; ?></span>
                 <input type="hidden" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" > 
                 </a>
                  <?php }else{ ?>
                  <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]==1?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>"
                data-valueno="0" data-value2="<?php echo '<?php echo @$frm["'.$campo.'"]==1?1:0;?>';?>">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]==1?"Active":"Inactive");?>'; ?></span>
                 <input type="hidden" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>';?>" > 
                 </a>
                  <?php } ?>
                <?php }else{ 
                     $jschkformulariomultiple=true;
                    if($frm["dattype".$campo]=="enum"){ 
                      $txtchk=str_replace("'", "", substr($frm["coltype".$campo], 5,-1));
                      $valoresopt=@explode(",",$txtchk); 
                      for($i=0;$i<count($valoresopt);$i++){                       
                       ?>

                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo $valoresopt[$i]; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("'.$valoresopt[$i].'");?>'; ?></span>                 
                 </a><br>
                  <?php    }
                      
                  }else{
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ 
              ?> 
                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo $i; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("Opcion - '.$i.'");?>'; ?></span>                 
                 </a><br>
              <?php 
                  }  
                  }?>
                  <input type="hidden" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>" >
                  <?php
                }                    

//////////////////////////////////////////////------------radiobutton -------------------
              }elseif($tipo=='radiobutton'){                
                if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                  $jschkformulario=true;
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); 
                  $jschkformulario=true;

              ?>  
                <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>"  data-valueno=<?php echo $valoresopt[1];?> data-value2="<?php echo '<?php echo @$frm["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[1].':'.$valoresopt[0].';?>';?>">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[0].':'.$valoresopt[1].');?>'; ?></span>
                 <input type="hidden" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" > 
                 </a>
                  <?php }else{ ?>
                  <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]==1?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>"  data-valueno="0" data-value2="<?php echo '<?php echo @$frm["'.$campo.'"]==1?1:0;?>';?>">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]==1?"Activo":"Inactivo");?>'; ?></span>
                 <input type="hidden" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" > 
                 </a>
                  <?php } ?>
                <?php }else{ 
                 $jschkformulariomultiple=true;
                    if($frm["dattype".$campo]=="enum"){ 
                      $txtchk=str_replace("'", "", substr($frm["coltype".$campo], 5,-1));
                      $valoresopt=@explode(",",$txtchk); 
                      for($i=0;$i<count($valoresopt);$i++){                       
                       ?>
  <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo $valoresopt[$i]; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("'.$valoresopt[$i].'");?>'; ?></span>
                 
                 </a><br>
                  <?php    }
                      
                  }else{
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ 
              ?> 
                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo $i; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("Opcion - '.$i.'");?>'; ?></span>
                 
                 </a><br>
              <?php 
                  }  
                  }?>
                  <input type="hidden" id="<?php echo $idcampo ?>"  name="<?php echo $campo ?>" value="<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" >
                  <?php } 
                   
//////////////////////////////////////////////------------Number -------------------
              }elseif($tipo=='int'||$tipo=='tinyint'||$tipo=='bigint'){
              ?> <input type="number" id="<?php echo $idcampo ?>"  name="<?php echo $campo ?>" step="1" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:1;?>'; ?>"  min="1" class="form-control col-md-7 col-xs-12" required />
              <?php 

//////////////////////////////////////////////------------password -------------------
              }elseif($tipo=='password'){
                if($frm["tipo2_".$campo]=='simple'){
              ?> <input type="password" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control" required />    
              <?php  }else{ 
              ?> <input type="password" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control" required /> 
              <?php  }

//////////////////////////////////////////////------------file -------------------
              }elseif($tipo=='file'){
                $jsfile=true; 
  $tmptipofile='*';
  $tmpfileadd='';
  $tmptipofileadd='img';
if($frm["tipo2_".$campo]=='imagen'){
  $tmptipofile='image/*';
  $tmpfileadd='filetmp.classList.add("img-responsive");
          filetmp.classList.add("center-block");
          filetmp.src=ev_.target.result;
          ';
}elseif($frm["tipo2_".$campo]=='video'){
  $tmptipofileadd='video';
  $tmptipofile='video/*';
  $tmpfileadd='filetmp.controls=true;
            filetmp.src=ev_.target.result;';
}elseif($frm["tipo2_".$campo]=='audio'){
  $tmptipofileadd='audio';
  $tmptipofile='audio/*';
  $tmpfileadd='filetmp.controls=true;
            filetmp.src=ev_.target.result;';
}elseif($frm["tipo2_".$campo]=='documentos'){
  $tmptipofile='application/*';
}

$addimgsave="
$('#".$idcampo."').click(function(ev){  
    $(this).attr('accept','".$tmptipofile."');   
    //if(tipo=='pdf') $(this).attr('accept','application/pdf');
    //else $(this).attr('accept','application/*');
})

var input".$idcampo." = document.getElementById('".$idcampo."');  
input".$idcampo.".addEventListener('change', addfile".$idcampo.", false);
function addfile".$idcampo."(ev) {
  var file=ev.target.files[0];
  var tipo='".$frm["tipo2_".$campo]."';
  var donde=$('#sysfile".$idcampo."');
  var reader=new FileReader();      
  reader.onload=function(ev_){
    var filetmp=document.createElement('".$tmptipofileadd."');          
    filetmp.style.width='60%';
    filetmp.style.height='200px';
    ".$tmpfileadd."
    if(filetmp!='') donde.html('').append(filetmp);
  }
  reader.readAsDataURL(file);
}
  ";
                  ?>
                <div style="position: relative;" class="frmchangeimage">
                <div class="toolbarmouse text-center">                  
                  <span class="btn btnaddfile"><input type="file" class="input-file-invisible" name="<?php echo $campo ?>" id="<?php echo $idcampo ?>"><i class="fa fa-pencil"></i></span>
                </div>
                <div id="sysfile<?php echo $idcampo ?>"><?php echo '<?php echo @$file; ?>';?></div>                
              </div>               
                
<?php
//////////////////////////////////////////////------------textArea -------------------
               }elseif($tipo=='textArea'){
                if($frm["tipo2_".$campo]=='simple'){
              ?> <textarea id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='wysihtml5'){
              $jsverwysihtml5 =true;
              ?> <textarea id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="verwysihtml5 form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='chkeditor'){
              $jsverchkeditor=true; 
              $js.='CKEDITOR.replace("txt'.$campo.'");'
              ?> <textarea id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="verchkeditor form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='tinymce'){
              $jsvertinymce=true; 
              ?> <textarea id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="vertinymce form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }

//////////////////////////////////////////////------------foreign key -------------------
              }elseif($tipo=='fk'){
                if($frm["tipofkcomo_".$campo]=='combobox'){
              ?><select id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control">
               <option value=""><?php echo "<?php echo JrTexto::_('Seleccione'); ?>"; ?></option>
              <?php
              echo '<?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') { ?>';
              echo '<option value="<?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]?>" <?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]==@$frm["'.$campo.'"]?"selected":""; ?> ><?php echo $fk'.$campo.'["'.$frm["tipofkver_".$campo].'"] ?></option>';
              echo '<?php } ?>';
              ?>
              </select>
              <?php }elseif($frm["tipofkcomo_".$campo]=='radiobutton'){
                echo '
              <?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                 ?>';
              ?><input type="radio" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" >
              <?php 
              echo ' <?php } ?>';
              }elseif($frm["tipofkcomo_".$campo]=='checkbox'){
                echo '
              <?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                 ?>';
              ?><input type="checkbox" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control">
              <?php echo ' <?php } ?>';
              }elseif($frm["tipofkcomo_".$campo]=='text'){
              ?><input type="text" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control">
              <?php }elseif($frm["tipofkcomo_".$campo]=='listado'){
              ?><input type="button" id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control listarfk">
              <?php }elseif($frm["tipofkcomo_".$campo]=='system'){
              ?><?php echo '<?php echo $this->fk'.$campo.'["usuario"];?>';?>
              <?php }

//////////////////////////////////////////////------------otraopcion -------------------
              }elseif($frm["tipofkcomo_".$campo]=='money'){?>
              <input type="text"  id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control" value="<?php echo '<?php echo @$frm["txt'.$Campo.'"] ?>'?>">
              <?php }else{
              ?>   <input type="text"  id="<?php echo $idcampo ?>" name="<?php echo $campo ?>" class="form-control " value="<?php echo '<?php echo @$frm["txt'.$Campo.'"] ?>'?>">
              <?php } if(@$frm["tipo2_".$campo]!='system'){ 
              ?>
            </div>
            <?php }
            }
          }?>

    </div>
        <div class="row"> 
            <div class="col-12 form-group text-center">
              <hr>
              <button id="btn-save<?php echo $tb ?>" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo "<?php echo ucfirst(JrTexto::_('Save'));?>"; ?> </button>
              <button type="button" class="btn btn-warning btn-close" data-dismiss="modal" href="<?php echo "<?php echo JrAplicacion::getJrUrl(array('".strtolower($tb)."'))?>"; ?>"> <i class=" fa fa-repeat"></i> <?php echo "<?php echo ucfirst(JrTexto::_('Cancel'));?>"; ?></button>              
            </div>
        </div>
        </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
  <?php echo $jsdatetxt; ?>
  <?php echo $jsdatetimetxt; ?>
  <?php if($jsvertinymce==true){?>
    tinymce.init({
    selector: ".vertinymce",
     plugins: [
        "link image"
    ],
    toolbar: " bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image",
    language_url:"../../static/libs/tinymce/tinymce_idioma/es.js"
  });
  <?php } ?>
  <?php if($jsverchkeditor ==true){?>

  <?php } ?>
  <?php if($jsverwysihtml5==true){?>
    $(".verwysihtml5").wysihtml5();
  <?php } ?>
  <?php echo @$js; ?>

$('#frm-<?php echo '<?php echo $idgui;?>'; ?>').bind({    
     submit: function(ev){
      ev.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', '<?php echo strtolower($tb);?>', 'save<?php echo $tb?>', xajax.getFormValues('frm-<?php echo '<?php echo $idgui;?>'; ?>'));
      if(res){
       <?php echo '<?php if(!empty($fcall)){ ?>'; ?> if(typeof <?php echo '<?php echo $fcall?>';?> == 'function'){
          <?php echo '<?php echo $fcall?>';?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else <?php echo '<?php } ?>';?>  return redir('<?php echo '<?php echo JrAplicacion::getJrUrl(array("'.$tb.'"))?>';?>');
      }
     }
  }); 
  <?php if($jsfile==true){ echo @$addimgsave; } ?>

});

<?php if($jschkformulariomultiple){?>
      $('.chkformulariomultiple').click(function(){
        var v1=$(this).attr('data-value');
        if($(this).hasClass("fa-circle-o")){
          $(this).removeClass('fa-circle-o').addClass(' fa-check-circle ');
        }else{
          $(this).removeClass('fa-check-circle').addClass(' fa-circle-o ');
        }

      });
<?php }?>
<?php if($jschkformulario){?>
$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo "<?php echo JrTexto::_(\"Active\");?>"; ?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo "<?php echo JrTexto::_(\"Inactive\");?>"; ?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });
<?php }?>
</script>