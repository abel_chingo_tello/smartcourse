<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='blanco'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .breadcrumb {background-color:rgb(64, 113, 191); padding: 1ex;  }
  .breadcrumb a{ color: #fff; }
  .breadcrumb li.active{ color:#d2cccc; }
  .panel-user{ padding:0.4ex; border: 1px solid rgba(90, 137, 248, 0.41); position: relative; margin-bottom: 1ex; }
  .panel-user .item{ height:150px; overflow: auto; width: 100%;}
  .panel-user .item img{ max-height: 100%; max-width: 100%; }
  .panel-user .pnlacciones{ display: none;}
  .panel-user.active .pnlacciones{ display: block; top: 0px;     position: absolute; width: 100%;}
  .panel-user .nombre{ text-align: center; font-size: 1em; max-height: 60px; overflow: hidden;}
  .form-group{ margin-bottom: 0px; }
  .input-group { margin-top: 0px; }
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_('SmartEnglish'); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<form name="frmbuscar" id="frmbuscar<?php echo $idgui; ?>" >
<div class="row" id="filtros">
  <div class="col-md-6" class="form-group">
    <select name="plataforma" id="plataforma" class="form-control">
      <?php foreach ($this->PV as $k => $v) {?>
      <option value="<?php echo $k; ?>" link="<?php echo $v["link"]; ?>"><?php echo $v["nombre"]; ?></option>
        
      <?php } ?>
      <!--option value="PVADU_1" link="http://localhost/smart/smartenglish/">smartEnglish Adultos</option-->
    </select>
  </div>
  <div class="col-md-6" class="form-group">
    <select name="idcurso" id="idcurso" class="form-control">
      <option value="1">A1</option>
    </select>
  </div>
  <div class="col-md-9 form-group">
    <div class="input-group">
      <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
      <span class="input-group-addon btn btnbuscar btn btn-success" style="margin-top: 0px;"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
    </div>
  </div>
  <div class="col-md-3 form-group">
    <button class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</button>
  </div>
</div>
</form>
<div class="row">
  <div class="col-md-12 table-responsive" id="datos">
    <table class="table table-striped table-hover">
      <thead style="display: none"><tr><th></th></tr></thead>
      <tbody id="aquicarganlassesiones"><tr><td></td></tr></tbody>      
    </table>
  </div>
  <div id="cargando" style="display: none;  width: 100%; text-align: center;  font-size: 19px; font-weight: bold;">Cargando...</div> 
</div>