<iframe id="webview" frameborder="0" src="<?php echo $this->link; ?>" width="100%" height="100%" style="border:0px;"type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all" allowmicrophone="yes" allow="geolocation; microphone; <?php echo $this->link; ?>"></iframe>
<script type="text/javascript">
    document.getElementById("webview").addEventListener("MSWebViewPermissionRequested", managePermissionRequest)
    function managePermissionRequest(e) {
            if (e.permissionRequest.type == "media") {
                    e.permissionRequest.allow();
            }
    }
</script>