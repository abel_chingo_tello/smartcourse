<div class="container">
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron">
                <h1><strong>Opss!</strong></h1>
                <?php if($this->info["code"]==201){ ?>
                    <div class="disculpa"><?php echo JrTexto::_('Usted no se encuentra matriculado a este curso')?>.</div>
                <?php }else{ ?>
                    <div class="disculpa"><?php echo JrTexto::_('Información del Matricula')?>.<br>
                        curso: <strong> <?php echo @$this->matricula["idcurso"]."_".@$this->matricula["idcomplementario"].":".@$this->matricula["strcurso"]; ?></strong><br>
                        Matricula: <br><strong> <?php echo @$this->matricula["idmatricula"].": ".(@$this->matricula["estado"]=='1'?'Activa':'Inactiva'); ?></strong><br>
                        Fecha de vencimiento de Matricula: <br><strong> <?php echo @$this->matricula["fecha_vencimiento"]; ?></strong><br>
                        <small>Comuniquese con el coordinador de su empresa</small>
                        <br><br>
                        <br>Alumno: <br><strong> <?php echo @$this->matricula["idalumno"].": ".@$this->matricula["stralumno"]; ?></strong><br>
                        Grupo aula: <br><strong> <?php echo @$this->matricula["idgrupoauladetalle"].": ".@$this->matricula["strgrupoaula"]; ?></strong><br>

                    </div>
                <?php } ?>
                <div class=""><br>
                    <a class="btn btn-default close" href="javascript:salir();"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Go back')?></a>
                </div>
            </div>
        </div>
	</div>
</div>
<script type="text/javascript">
    function salir(){        
        window.open('','_parent',''); 
        window.close();
    }
</script>