<?php
defined('RUTA_BASE') or die();
?>
<div class="container">
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
<?php /*
        	<div id="wp-excepcion-error" class="wp-excepcion">
                <h3>Opss!</h3>
                <div class="disculpa"><?php echo JrTexto::_('Lo sentimos, no hemos encontrado la dirección solicitada')?>.</div>
            </div>
*/ ?>
            <div class="jumbotron">
                <h1><strong>Opss!</strong></h1>
                
                <div class="disculpa"><?php echo JrTexto::_('Sorry, we could not find the requested address')?>.</div>

                <div class=""><br>
                    <a class="btn btn-default" href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Go back')?></a>
                </div>
            </div>
        </div>
	</div>
</div>