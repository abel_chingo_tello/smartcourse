<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$curso=!empty($this->curso)?$this->curso:'';
$idcurso=!empty($this->idcurso)?$this->idcurso:'0';
$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
$fileurl=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:$imgcursodefecto;
$miid=$this->usuActivo["idpersona"];
$midni=$this->usuActivo["dni"];
$mirol=$this->usuActivo["idrol"];
$catcurso=array();
$catcursoinfo=array();
if(!empty($this->categoriasdelcurso)){
	foreach ($this->categoriasdelcurso as $cat){
		array_push($catcurso,$cat["idcategoria"]);
		$catcursoinfo[$cat["idcategoria"]]=$cat;
	}
}
?>
<style type="text/css">
	#mindice{ height: calc(100%); overflow-y: hidden;}
	#mindice:hover{overflow-y: auto; }

	#mindice, #mindice ul{list-style-type: decimal-leading-zero; }
	#mindice li{border-top:1px solid #878e9838;	/*padding: 0.4ex 1ex;*/	width: 100%;}
	#mindice li > div{content: " ";	font-size: 17px;padding: 1ex 0ex;}
	#mindice li ul li > div{padding-left: 1.5ex;}
	#mindice li ul li ul li > div{padding-left: 3ex;}
	#mindice li ul li ul li ul li > div{padding-left: 4.5ex;}
	#mindice li ul li ul li ul li ul li> div{padding-left: 6ex;}
	#mindice li ul li ul li ul li ul li ul li> div{padding-left: 7.5ex;}
	#mindice li ul li ul li ul li ul li ul li ul li> div{padding-left: 9ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 10.5ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 12ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 13.5ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 15ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 16.5ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 17ex;}
	#mindice li > div span.macciones{float: right;}
	#mindice li > div span.macciones i{cursor: pointer;}
	#mindice li > div span.macciones i.mmsubmenu{display: none;}
	#mindice li.hijos > div ~ ul{display: none;}
	#mindice li.hijos.active > div ~ ul{display: block;}	
	#mindice li.hijos.active > div span.macciones i.mmsubmenu.fa-sort-down{ display: none;}
	#mindice li.hijos.active > div span.macciones i.mmsubmenu.fa-sort-up{ display: inline-block;}
	#mindice li.hijos > div span.macciones i.mmsubmenu.fa-sort-down{ display: inline-block;}
	#mindice li.hijos > div span.macciones i.mmsubmenu.fa-sort-up{ display: none;}
	#mindice li span.macciones i.mmovedown, #mindice li span.macciones i.mmoveup{display:block !important;}	
	#mindice li span.macciones i.mmoveup{margin-top: -1.1ex; }
	#mindice > li:nth-child(2) > div  span.macciones i.mmoveup{display: none !important;}
	#mindice > li:last-child > div span.macciones i.mmovedown{display: none !important;}
	#mindice li ul li:nth-child(2) span.macciones i.mmoveup{margin-top: -1.1ex; display: block !important;}
	#mindice li ul li:first-child > div span.macciones i.mmoveup{ display: none !important;}
	#mindice li ul li:last-child > div span.macciones i.mmovedown{ display: none !important;}
	#toolbarcurso .fa { color:#FFF !important; }
	#mindice li .macciones{ display: none; }
	#mindice li:hover > div > .macciones{ display: block; }
	#ultabs{ background: #ecf2f9e0; padding: 0.25ex; }	
	#ultabs li b.number{ font-size: 1.5em; }
	#ultabs li.active{ background:#047bfc;}
	#ultabs li.active a { color:#f7f1f1; }
	#ultabs li.active:after { border-left: 16px solid #047bfc; }
	#ultabs li.visto{ background:#047bfc;}
	#ultabs li.visto a { color:#f7f1f1; }
	#ultabs li.visto:after { border-left: 16px solid #047bfc; }
	
</style>
<div class="row">
	<div class="col-md-12">
		<div class="container">
	<div class="row">
		<div class="col-md-12">
        <div class="wizard">
            <ul class="nav nav-wizard" id="ultabs">
                <li class="active"><a href="#info" data-toggle="tab"> <b class="number">1.</b> Datos del curso </a></li>
                <li><a  href="#cate" data-toggle="tab"> <b class="number">2.</b> Categorias </a></li>
                <li><a  href="#silabus" data-toggle="tab"> <b class="number">3.</b> Silabus </a></li>
                <!--li><a  href="#plantilla" data-toggle="tab"> <b class="number">4.</b> Plantilla </a></li-->
                <li><a  href="#contenido" data-toggle="tab"> <b class="number">4.</b> Contenido </a></li>
            </ul>
            <div class="tab-content shadow">
                <div class="tab-pane active " role="tabpanel" id="info">
                    <form id="frm<?php echo $idgui;?>" method="post" target="" enctype="multipart/form-data">
						<input type="hidden" name="idcurso" id="idcurso<?php echo $idgui; ?>" value="<?php echo @$idcurso; ?>">
						<div class="card text-center shadow">				 
						  <div class="card-body">
						    <!--h5 class="card-title"><?php //echo JrTexto::_('Datos generales del curso');?></h5-->
						    <div class="row">
						    	<div class="col-12 col-sm-6 col-md-6">
						    		<div class="row">
						    		<div class="col-12 form-group">
							            <label style=""><?php echo JrTexto::_('Nombre del curso');?></label> 
							            <input type="text" name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Curso 01"))?>" value="<?php echo @$curso["nombre"]; ?>">					           
							        </div>
							        <div class="col-6 form-group">							        	
							            <label style=""><?php echo JrTexto::_('Autor');?></label> 
							            <input type="text" name="autor" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Apellidos y Nombres del autor "))?>" value="<?php echo !empty($curso["autor"])?$curso["autor"]:$this->usuActivo["nombre_full"]; ?>">					           
							        </div>
							        <div class="col-6 form-group">
							            <label style=""><?php echo JrTexto::_('Año de publicación');?></label> 
							            <input type="text" name="aniopublicacion" required="required"  class="form-control" placeholder="<?php echo date('Y'); ?>" value="<?php echo !empty($curso["aniopublicacion"])?$curso["aniopublicacion"]:date('Y'); ?>">
							        </div>
							        <div class="col-12 form-group">
							            <label style=""><?php echo JrTexto::_('Reseña');?></label> 
							            <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso"))?>"><?php echo @$curso["descripcion"]; ?></textarea>
							        </div>
							       							        
							        <div class="col-6 form-group text-left ">
							            <label style=""><?php echo JrTexto::_('Color');?> </label>  
							            <input type="color" name="color" id="color<?php echo $idgui; ?>" class="<?php echo empty($curso["color"])?'sincolor':''; ?>" data-color="#007bff" style="margin-left: 1ex; <?php echo empty($curso["color"])?'display:none;':''; ?>" value="<?php echo @$curso["color"]; ?>"><br>
							            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
								        <a style="cursor:pointer;" class="chkoption fa  <?php echo @$curso["color"]==''?"fa-check-circle":"fa-circle-o";?>" data-value="<?php echo @$curso["color"];?>">
								        	<span> <?php echo JrTexto::_(@$curso["color"]==''?"Si":"No");?></span>						        	
								        </a>
							        </div>
							        <div class="col-6 form-group text-right">
								        <label style="float: none;"><?php echo JrTexto::_('Publicado');?> </label>              
								        <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$curso["estado"]==1?"fa-check-circle":"fa-circle-o";?>">
								        	<span> <?php echo JrTexto::_(@$curso["estado"]==1?"Si":"No");?></span>
								        	<input type="hidden" id="estado<?php echo $idgui; ?>" name="estado" value="<?php echo !empty($curso["estado"])?$curso["estado"]:0;?>" > 
								        </a>
		                            </div>
		                            </div>		       
						    	</div>
						    	<div class="col-12 col-sm-6 col-md-6">
						    		<div class="row">
							    		<div class="col-12 form-group text-center">
								            <label style="float: none"><?php echo JrTexto::_('Portada');?></label> 
								            <div style="position: relative;" class="frmchangeimage">
								                <div class="toolbarmouse text-center">                  
								                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
								                </div>
								                <div id="sysfileimagen<?php echo $idgui; ?>">
								                	<img id="verimg<?php echo $idgui; ?>" src="<?php echo $fileurl; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
                                                    <input type="file" class="input-file-invisible" name="imagen" style="display: none;">
								                </div>
								                <small>Tamaño 250px x 360px</small>                
								            </div>				           
								        </div>
							        </div>				    		
						    	</div>
						    </div>
						    <div class="row">
						    	<div class="col-12">
						    		
						    	</div>
						    </div>			   
						  </div>
						  <div class="card-footer text-muted">
						  	<button type="button" onclick="redir(_sysUrlBase_+'/');" class="btn btn-warning"><i class=" fa fa-undo"></i> Regresar al listado</button>
						    <button type="submit" class="btn btn-primary"><i class=" fa fa-save"></i> Guardar y continuar</button>
						  </div>
						</div>
					</form>
                </div>
                <div class="tab-pane" role="tabpanel" id="cate">
                	<div class="card text-center" id="categoriascurso">
						<div class="card-body">
	                    	<div class="row">
						        <div class="col-12 form-group"><label style=""><?php echo JrTexto::_('Categorias');?></label></div>
					            <?php if(!empty($this->categorias))
			            				foreach ($this->categorias as $cat){
			            				$marcado=in_array($cat["idcategoria"], $catcurso)?true:false;
			            				$dt='';
			            				if($marcado){
			            					$icurcat=$catcursoinfo[$cat["idcategoria"]]["idcursocategoria"];
			            					$dt='data-idcategoriacurso="'.$icurcat.'"';
			            				} ?>
			            			<div class="col-3 form-group text-left">
				            			<a style="cursor:pointer;" <?php echo $dt; ?> data-idcategoria="<?php echo $cat["idcategoria"] ?>" class="selcategoria">
				            				<i class="fa  <?php echo $marcado==true?"fa-check-circle":"fa-circle-o";?>"></i><span> <?php echo $cat['nombre']; ?></span>
								        </a>
		                            </div>
			            		<?php }?>
							</div>
                		</div>
                		<div class="card-footer text-muted">
						  	<button type="button" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
						    <button type="button" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>
						</div>
                	</div> 
                </div>
                <div class="tab-pane" role="tabpanel" id="silabus">
                	<div class="card text-center" id="cardsilabo">
                		<div class="card-body">
	                    	<div class="row btnmanagermenu">
	                    		<div class="col-12">
	                    			<div class="row">
	                    				<div class="col-xs-12 col-sm-3">
											<a href="javascript:void(0)" class="importarmenu btn btn-block bg-primary" style="color:#fff">
												<div><i class="fa fa-cloud-upload fa-2x "></i></div>												
												<p class="bolder">Importar Silabo.</p>
											</a>
										</div>
	                    				<div class="col-xs-12 col-sm-3">
											<a href="javascript:void(0)" class="crearmenu btn btn-block bg-warning" style="color:#fff">
												<div><i class="fa fa-plus fa-2x "></i></div>
												<p class="bolder">Nuevo Tema</p>												
											</a>
										</div>                 				
	                    			</div>
	                    		</div>
	                    		<div class="col-12">
	                    			<hr>
	                    		</div>
	                    	</div>
	                    	<div class="row" id no >
	                    		<div class="col-12" id="paneldisenio" style="max-height: 450px ; overflow: auto;">
	                    		</div>	
								<div class="col-12 text-left" id="plnindice">
									<li class="paraclonar" style="display: none">
										<div><span class="mnombre">Friends</span>
										<span class="macciones">
											<i class="mmsubmenu fa fa-sort-down verpopoverm" data-content="Ocultar Submenus" ></i>
											<i class="mmsubmenu fa fa-sort-up verpopoverm" data-content="Mostrar Submenus" ></i>
											<i class="addcontent fa fa-eye verpopoverm" data-content="Ver contenido" data-placement="top"></i>
											<i class="magregar fa fa-plus verpopoverm" data-content="Agregar Submenu" data-placement="top"></i>
											<i class="meditar fa fa-pencil verpopoverm" data-content="Editar Menu" data-placement="top"></i>
											<i class="meliminar fa fa-trash verpopoverm" data-content="Eliminar" data-placement="top"></i>
											<i class="mmove fa fa-arrows-alt verpopoverm" data-content="Mover Menu" data-placement="right"></i>
											<!--i class="mcopylink fa fa-link verpopoverm" data-content="Copiar link" data-placement="right"></i-->
											<!--span style="display:inline-block">								
												<i class="mmoveup fa fa-angle-double-up verpopoverm" data-content="Mover Arriba" ></i>
												<i class="mmovedown fa fa-angle-double-down verpopoverm" data-content="Mover abajo"  ></i>
											</span-->
										</span>
										</div>
									</li>
									<ul id="mindice">
										
									</ul>
								</div>
							</div>
                		</div>
                		<div class="card-footer text-muted">
						  	<button type="button" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
						    <button type="button" class="btnnexttabs btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>
						</div>
                    </div>
                </div>
                <!--div class="tab-pane" role="tabpanel" id="plantilla">
                	<div class="card text-center" id="categoriascurso">
						<div class="card-body">
	                    	<div class="row">
						        <div class="col-12 form-group"><h3 style=""><?php //echo JrTexto::_('Plantillas del curso');?></h3></div>
						        <div class="col-4 col-xs-12 col-sm-4">
									<a href="javascript:void(0)" class="editarcontenido btn btn-block bg-success" style="color:#fff">
										<div><i class="fa fa-address-book-o fa-2x "></i></div>												
										<p class="bolder">Eduktvirtual</p>
									</a>
								</div>
								<div class="col-4 col-xs-12 col-sm-4">
									<a href="javascript:void(0)" class="editarcontenido btn btn-block bg-success" style="color:#fff">
										<div><i class="fa fa-address-book-o fa-2x "></i></div>												
										<p class="bolder">Book</p>
									</a>
								</div>
					           
							</div>
                		</div>
                		<div class="card-footer text-muted">
						  	<button type="button" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
						    <button type="button" class="btnnexttabs btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>
						</div>
                	</div> 
                </div-->
                <div class="tab-pane" role="tabpanel" id="contenido">
                	<div class="card text-center" id="categoriascurso">
						<div class="card-body">
	                    	<div class="row">
	                    		<div class="col-4 col-xs-12 col-sm-4"></div>
	                    		<div class="col-4 col-xs-12 col-sm-4">
									<a href="javascript:void(0)" class="editarcontenido btn btn-block bg-success" style="color:#fff">
										<div><i class="fa fa-address-book-o fa-2x "></i></div>	
										<p class="bolder">Ver o Editar contenido</p>
									</a>
								</div>
								<div class="col-4 col-xs-12 col-sm-4"></div>
							</div>
                		</div>                		
                	</div> 
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
   </div>
</div>
	</div>	
</div>

<script type="text/javascript">
var cargarmenus=function(idcurso){
	__cargarplantilla({
		url:'/defecto/vermenuupdate/',
		donde:'mindice',
		data:{idcurso:idcurso}
	});
	$('#paneldisenio').hide(0);
	$('#plnindice').show(0);
	$('.card-footer').show(0);
	$('.btnmanagermenu').show(0);
}

$(document).ready(function(ev){
	var idcurso=parseInt('<?php echo $idcurso; ?>');
	var urlmedia='<?php echo URL_MEDIA; ?>';
	let mentabs=$('#ultabs');
	if(idcurso==0) mentabs.find('li').nextAll().children('a').addClass('disabled');	
	mentabs.on('click','a[data-toggle="tab"]',function(ev){
		$(this).closest('li').addClass('active');
		$(this).tab('show');

	})

	mentabs.on('show.bs.tab','a[data-toggle="tab"]',function(ev){
		//console.log('aaa');
		//ev.preventDefault();
		$(this).removeClass('disabled');
		li=$(this).closest('li');
		li.siblings('li').removeClass('active');
		li.addClass('active');
		li.prevAll().removeClass('visto').addClass('visto');
		li.nextAll().removeClass('visto');		
	})
	cargarmenus(idcurso);
	
	$('#frm<?php echo $idgui;?>').on('submit',function(ev){
		ev.preventDefault();			
		var data=__formdata('frm<?php echo $idgui;?>');
		if($('#color<?php echo $idgui; ?>').hasClass('sincolor')) data.append('color','');
		__sysajax({
			fromdata:data,
            url:_sysUrlBase_+'/acad_curso/guardarAcad_curso',
            callback:function(rs){
            	console.log(rs);
            	var imagen=rs.newimagen;
            	$('#idcurso<?php echo $idgui; ?>').val(rs.newid);
            	idcurso=rs.newid;
            	if(imagen!=''){
            		$('#verimg<?php echo $idgui; ?>').attr('src',imagen);
            	}
            	let liactive=$('#ultabs').find('li.active');  
            	linext=liactive.next().children('a');
            	linext.removeClass('disabled').tab('show');
            	idlinext=linext.attr('href');
            	//console.log(idlinext);
            	$('.tab-content').children().removeClass('active');
            	$('.tab-content').children(idlinext).addClass('active');
            	//$(linext).trigger('click');
            	console.log(linext);
            }
		});
		//return false;
	});

	$('.btnbacktab').on('click',function(ev){
		let liactive=$('#ultabs').find('li.active');
		liprev=liactive.prev().children('a');
		liprev.removeClass('disabled').tab('show');
        idlinext=liprev.attr('href');
        $('.tab-content').children().removeClass('active');
        $('.tab-content').children(idlinext).addClass('active');        
	});

	$('.btnnexttabs').on('click',function(ev){
		let liactive=$('#ultabs').find('li.active');
		linext=liactive.next().children('a');
		linext.removeClass('disabled').tab('show');
        idlinext=linext.attr('href');
        $('.tab-content').children().removeClass('active');
        $('.tab-content').children(idlinext).addClass('active');  
	});

    $('.selcategoria').on('click',function(ev){    	
    	let it=$(this);
    	let idcat=it.attr("data-idcategoria")||0;		
		let id=it.attr('data-idcategoriacurso')||0;
		let icon=$(this).children('i');
    	if(icon.hasClass('fa-circle-o')){
    		icon.removeClass('fa-circle-o').addClass('fa-refresh fa-spin');      
            __sysajax({
            	fromdata:{idcat:idcat,idcur:idcurso,acc:'I'},				
				showmsjok:false,
	            url:_sysUrlBase_+'/acad_curso/guardarcategorias',
	            callback:function(rs){	            	
	            	let id=rs.data;
	            	it.attr('data-idcategoriacurso',id);
	            	icon.removeClass('fa-refresh fa-spin').addClass('fa-check-circle');	            	
	            }	            
			});
        }else{
        	icon.removeClass('fa-check-circle').addClass('fa-refresh fa-spin');
            __sysajax({
            	fromdata:{id:id,acc:'D'},
				showmsjok:false,
	            url:_sysUrlBase_+'/acad_curso/guardarcategorias',
	            callback:function(rs){
	            	it.removeAttr('data-idcategoriacurso');
	            	icon.addClass('fa-circle-o').removeClass('fa-refresh fa-spin');
	            }	            
			});

        }
    })

    $('.tab-content').on('click','.chkoption',function(ev){ 
        var input= $(this).siblings('input');   
        if($(this).hasClass('fa-circle-o')){
            $('span',this).text(' <?php echo JrTexto::_("Si");?>'); 
            input.addClass('sincolor');
            input.attr('data-color',input.val()).val('').hide();
            $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
        }else{
            $('span',this).text(' <?php echo JrTexto::_("No");?>');
            input.removeClass('sincolor');
            input.val(input.attr('data-color')).show();                 
            $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
        }
    }).on('click','.chkformulario',function(ev){
    	if($(this).hasClass('fa-circle-o')){
            $('span',this).text(' <?php echo JrTexto::_("Si");?>');
            $('input',this).val(1);
            $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
        }else{
            $('span',this).text(' <?php echo JrTexto::_("No");?>');
            $('input',this).val(0);
            $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
        }
    }).on('click','.btnaddfile',function(ev){    
        let obj=$(this).closest('.frmchangeimage');
        let attr={
            accept:"image/x-png,image/gif,image/jpeg",
            dirmedia:'',           
            typefile:'imagen',
            guardar:false,
            file:obj.find('img'),
            inputfile:obj.find('input')
        }
        __subirmediaaweb(obj,attr,function(rs){console.log(rs);});
    });

    /* Menus */
    $('#cardsilabo').on('click','.crearmenu',function(ev){
    	ev.preventDefault();
    	$('#plnindice').hide(0);
    	$('.card-footer').hide(0);
    	$('#paneldisenio').show(0);
    	$('.btnmanagermenu').hide(0);
    	__cargarplantilla({
			url:'/acad_cursodetalle/vermenus/',
			donde:'paneldisenio',
			data:{
				idcurso:idcurso,
				endonde:'mindice',
				idpadre:0,
				acc:'additem'
				}
		});
    }).on('click','.importarmenu',function(ev){
    	$('#paneldisenio').show(0);
		$('#plnindice').hide(0);
		$('.card-footer').hide(0);
		$('.btnmanagermenu').hide(0);
    	__cargarplantilla({
			url:'/peditar/importar/',
			donde:'paneldisenio',
			data:{idcurso:idcurso}
		});
    }).on('click','.btncancelar',function(ev){
    	$('#paneldisenio').hide(0);
    	$('#plnindice').show(0);
    	$('.card-footer').show(0);
    	$('.btnmanagermenu').show(0);
    }).on('click','.btnguardarmenu',function(ev){
		ev.preventDefault();
		let frm=$('#frmdatosmenu');
		var esfinal=frm.find('#esfinal').val()||0;
		if(parseInt(esfinal)==0)frm.find('#tiporecurso').val('M');
		else frm.find('#tiporecurso').val('S');
		var data=__formdata('frmdatosmenu');
		if(frm.find('#color').hasClass('sincolor')) data.append('color','');
		var idpadre=frm.find('#idpadre').val()||0;
			__sysajax({
			fromdata:data,
            url:_sysUrlBase_+'/acad_cursodetalle/guardarMenu',
            callback:function(rs){            
            	if(rs.code=='ok'){
            		var dt=rs.newid; 
            		//Agregando el menu
            		var acc=$('#accmenu').val();
            		var menu=$('#mindice');

            		if(acc!='edit'){
						var nmenu=$('#plnindice').children('.paraclonar').clone(); // selecionamos li 
						nmenu.removeClass('paraclonar').removeAttr('style');						
					}else if(acc=='edit'){
						var nmenu=menu.find('#me_'+dt.idcursodetalle);
					}
					if(parseInt(esfinal)==0) nmenu.removeClass('esfinal');
					else  if(!nmenu.hasClass('esfinal'))nmenu.addClass('esfinal');

					imgsubido='';
					if(rs.imagen!=''){
            			imgsubido=rs.imagen;
            			$('#verimg<?php echo $idgui; ?>').attr('src',imgsubido);
            			imgsubido=imgsubido.replace(urlmedia, '');
            		}
					
					nmenu.find('.mnombre').text(frm.find('#nombre').val());
					nmenu.attr('data-idcursodetalle',dt.idcursodetalle);
					nmenu.attr('data-idpadre',dt.idpadre);
					nmenu.attr('data-orden',dt.orden);
					nmenu.attr('data-idrecurso',dt.idnivel);
					nmenu.attr('id','me_'+dt.idcursodetalle);
					nmenu.attr('data-imagen',imgsubido);
					nmenu.attr('data-esfinal',esfinal);	
					$('#paneldisenio').fadeOut(0);
					$('#plnindice').fadeIn('fast');
					$('.card-footer').fadeIn('fast');
					$('.btnmanagermenu').fadeIn('fast');
					if(acc=='edit') return false;
            		if(acc=='additem'){
						menu.append(nmenu);
						$('.verpopoverm').popover({ trigger: 'hover',delay:100});
						$('#mindice').sortable({cursor:"move"});
					}else{
						$li=$('#mindice li#me_'+dt.idpadre);
						if(!$li.hasClass('hijos')){
							$li.addClass('hijos');
							var ul=document.createElement('ul');
							ul.id='me_hijo'+dt.idcursodetalle; //temporal
							$(ul).append(nmenu);
							$li.append($(ul));
							$('#'+ul.id).sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9});
						}else{
							$li.children('ul').append(nmenu);
							$li.closest('ul').sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9});
						}
						if(!$li.hasClass('active')) $li.addClass('active');
						try{$('.verpopoverm').popover({ trigger: 'hover',delay:100});}catch(ex){};							
					}


				}
        	}
		});			
    })

    $('.editarcontenido').click(function(ev){
    	redir(_sysUrlBase_+"/peditar/?idcurso="+idcurso);
    })

    $('ul#mindice').on('click','li.hijos div span:first-child',function(ev){			
		$(this).closest('li').toggleClass('active');
	}).on('click','i.magregar',function(ev){
		$('#paneldisenio').show(0);
		$('#plnindice').hide(0);
		$('.card-footer').hide(0);	
		$('.btnmanagermenu').hide(0);
		$li=$(this).closest('li');
		__cargarplantilla({
			url:'/acad_cursodetalle/vermenus/',
			donde:'paneldisenio',
			data:{
				idcurso:idcurso,
				endonde:'mindice',
				idpadre:$li.attr('data-idcursodetalle'),
				idrecursopadre:$li.attr('data-idrecurso'),
				acc:'addsubitem'
				}
		});			
	}).on('click','i.meditar',function(ev){
		$('#paneldisenio').show(0);
		$('#plnindice').hide(0);
		$('.card-footer').hide(0);
		$('.btnmanagermenu').hide(0);
		$li=$(this).closest('li');
		__cargarplantilla({
			url:'/acad_cursodetalle/vermenus/',
			donde:'paneldisenio',
			data:{
				endonde:'mindice',
				idcurso:idcurso,
				idcursodetalle:$li.attr('data-idcursodetalle'),					
				idpadre:$li.attr('data-idpadre')||0,
				idrecurso:$li.attr('data-idrecurso'),
				acc:'edit'
				}
		});				
	}).on('click','i.meliminar',function(ev){
		$li=$(this).closest('li');
		$(this).popover('dispose');
		__sysajax({
			url:_sysUrlBase_+'/acad_cursodetalle/eliminarMenu',
			fromdata:{
				idcurso:idcurso,
				idcursodetalle:$li.attr('data-idcursodetalle'),
				idrecurso:$li.attr('data-idrecurso'),
				tiporecurso:$li.attr('data-tipo'),
			},
			type:'html',
			showmsjok:true,
			callback:function(rs){
				$li.remove();
			}
		});		
	}).on('click','i.addcontent',function(){
		let idcursodetalle=$(this).closest('li').attr('data-idcursodetalle')||'';
		redir(_sysUrlBase_+"/peditar/?idcurso="+idcurso+'&idsesion='+idcursodetalle);
		/*__cargarplantilla({
			url:'/disenio/',
			donde:'paneldisenio',
			data:{
				plantilla:'ninguno',
				idcurso:'<?php //echo @$idcurso; ?>',
				idcursodetalle:
				//endonde:'mindice',
				//menupadre:idli,
				}
		});*/
		//$(this).		
	})
		
	let _reordenar=function(dt){
		let item=dt.obj||'';
		let max=dt.max||0;
		let min=dt.min||0;
		let res=dt.res||0;
		let items=dt.items||'';
		if(item==''||item==undefined||items=='') return ;
		$hijos=item.children(items);
		let ordenmen=min;
		var datosupdate={};
		$.each($hijos,function(i,v){
			let hi=$(v);
			if(!hi.hasClass('paraclonar')){
				if(i>=min && i<=max){
					hi.attr('data-orden',ordenmen).removeAttr('data-oldindex');
					datosupdate[ordenmen]=hi.attr('data-idcursodetalle');
					ordenmen++;
				}		
			}
		})
		let idcurso=<?php echo $_REQUEST["idcurso"];?>;
		__sysajax({
			url:_sysUrlBase_+'/acad_cursodetalle/reordenar',
			fromdata:{idcurso:idcurso , dataorden:JSON.stringify(datosupdate)},
			type:'json',
			showmsjok:true
		});
	}
	
	$('#mindice').disableSelection();
	$('#mindice').sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9,
		start:function(ev,ui){
			var li=$(ui.item);
			var pos=parseInt(li.index()+1);				
			if(li.closest('ul').attr('id')=='mindice') pos--;
			li.addClass('enmovimiento').attr('data-oldindex',pos);
		},
		update:function(ev,ui){
			let li=$(ui.item);			
			li.removeClass('enmovimiento');
			let pos=parseInt(li.index()+1);
			let posant=parseInt(li.attr('data-oldindex')||1);
			if(li.closest('ul').attr('id')=='mindice') {pos--;}
			ul=li.closest('ul');
			_reordenar({obj:ul,min:((pos<posant)?pos:posant),max:((pos>posant)?pos:posant),items:'li'});
		}
	});

	$('#mindice').find('ul').each(function(i,v){
		$('ul#'+$(v).attr('id')).disableSelection();
		$('ul#'+$(v).attr('id')).sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9,
			start:function(ev,ui){
				var li=$(ui.item);
				var pos=parseInt(li.index()+1);
				if(li.closest('ul').attr('id')=='mindice') pos--;
				li.addClass('enmovimiento').attr('data-oldindex',pos);
			},
			update:function(ev,ui){				
				let li=$(ui.item);
				li.removeClass('enmovimiento');
				let pos=parseInt(li.index()+1);
				let posant=parseInt(li.attr('data-oldindex')||1);
				if(li.closest('ul').attr('id')=='mindice') {pos--; }
				ul=li.closest('ul');
				_reordenar({obj:ul,min:((pos<posant)?pos:posant),max:((pos>posant)?pos:posant),items:'li'})			
			}
		});
	});
	$('.verpopover').popover({ trigger: 'hover',delay:100});
});
</script>