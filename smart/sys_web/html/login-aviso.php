<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if($ismodal){ ?>
<span style="float: right; color: #777; font-size: 14px;" class="close cerrarmodal" data-dismiss="modal"><i class="fa fa-close"></i></span>
<?php } ?>
<style type="text/css">
    .titulobt{    
        position: absolute;
        width: 100%;
        bottom: 1ex;
        color:#fff;
    }
    .icon2{
        position: absolute;
        bottom: 0px;
        font-size: 2em;
        color: #1acc44;
    }
</style>
    <div class="row" style="font-family: 'Roboto Slab',serif;"> 
        <div class="col-12 text-center" style="font-size: 36px; color #0963c3; ">
            <h1><?php echo JrTexto::_('Asociate con nosotros');?></h1>
            <hr>
        </div>

        <div class="col-12 col-sm-6 text-center btn-pnl btn " style="font-size: 20px; ">
            <div class="hvr-float-shadow bg-primary btncancel<?php echo $idgui; ?>" style="width: 100%; height: 150px;" >
                <div class="imagen" style="position: relative;">               
                   <span class="fa fa-key text-primary" style="position: absolute; bottom: 10px; left: 40%; font-size: 2em; color: #1acc44;"></span>
                   <i class="fa fa-user" style=" font-size: 5em;  color: #fff; padding:  0.1ex;"></i> 
                </div>
                <div class="titulobt">Inicia sesion</div>
            </div>
        </div>
        <div class="col-12 col-sm-6 text-center btn-pnl btn " style="font-size: 20px;  ">
            <a href="<?php echo $this->documento->getUrlBase(); ?>/defecto/crearempresa">
            <div class="hvr-float-shadow bg-success" style="width: 100%; height: 150px;" >
                <div class="imagen" style="position: relative;">               
                   <span class="fa fa-pencil text-success" style="position: absolute; bottom: 10px; right: 40%; font-size: 2em; color: #1acc44;"></span>
                   <i class="fa fa-user" style=" font-size: 5em;  color: #fff; padding:  0.1ex;"></i>
                </div>
                <div class="titulobt">Registrate Aqui</div>
            </div>
            </a>
        </div> 
        <div class="col-12 form-group text-center text-info">
            <hr>
            Es Necesario que estes registrado para poder publicar una oferta laboral...
            <!--button type="submit" class="btn btn-primary"><i class="fa fa-user"></i><i class="fa fa-key"></i> <?php echo JrTexto::_('Iniciar Sesion')?></button>
            <button type="button" class="btn btn-warning "><i class="fa fa-refresh"></i> <?php echo JrTexto::_('Cancelar')?></button><br-->            
        </div>      
    </div>

<script type="text/javascript">
$(document).ready(function(){
    <?php if($ismodal){?>
        $('.btncancel<?php echo $idgui; ?>').click(function(ev){
            $(this).closest('.modal').modal('hide');
            $('.venloginopen').trigger('click');
        });
    <?php } ?>
});
</script>