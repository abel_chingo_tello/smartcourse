<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$cursos=!empty($this->cursos)?$this->cursos:'';
$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
$miid=$this->usuActivo["idpersona"];
$midni=$this->usuActivo["dni"];
$mirol=$this->usuActivo["idrol"];
?>
<div class="container">
	<div class="row" id="listadocursos">		
		<div class="col-12 col-sm-4 col-md-3 col-lg-2 padding1ex">
            <div class="card hvr-grow vercurso" >
              <div class="card_img" ><img  src="<?php echo @$imgcursodefecto; ?>"></div>            
              <div class="card_body">
                <div class="card_title text-center">Crea tu nuevo curso</div>
                <div class="card_texto">Crea Tu primer curso desde esta opcion de una forma rapida</div>               
              </div>
            </div>
        </div>
		<?php 
		if(!empty($cursos))
		foreach ($cursos as $cur){ 				
			$idusuario=$cur["idcurso"];				
			$imgtmp=substr(RUTA_MEDIA,0,-1).$cur["imagen"];					
			$img=is_file($imgtmp)?URL_MEDIA.$cur["imagen"]:$imgcursodefecto;?>	
			<div class="col-12 col-sm-4 col-md-3 col-lg-2 padding1ex">
                <div class="card hvr-grow vercurso " data-idcurso="<?php echo $cur["idcurso"]; ?>"  >
                  <div class="card_img" ><img  src="<?php echo @$img; ?>"></div>            
                  <div class="card_body">
                    <div class="card_title text-center"><?php echo $cur["nombre"]; ?></div>
                    <div class="card_texto"><?php echo !empty($cur["descripcion"])?$cur["descripcion"]:'---'; ?></div>
                    <?php if($mirol!=3){?> <div class="card_acciones" >
                        <?php if($idusuario==$miid||$miid==$midni||$mirol=='1'){?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-info editcursoautor verpopover" data-placement="top" data-content="Editar como Autor"><i class="fa fa-pencil"></i><i class="fa fa-graduation-cap"></i> </a>
                        <?php } if($mirol=='1'){?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-primary editcursodisenio verpopover" data-placement="top" data-content="Editar como Administrador"><i class="fa fa-pencil"></i><i class="fa fa-cubes"></i></a>
                        <?php } if($idusuario==$miid||$miid==$midni||$mirol=='1'){?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-danger elimcurso verpopover" data-placement="top" data-content="Eliminar mi curso"><i class="fa fa-trash"></i> </a>
                        <?php } ?>
                    </div>
                    <?php } ?>
                  </div>
                </div>
            </div>            
		<?php }	?>
	</div>	
</div>