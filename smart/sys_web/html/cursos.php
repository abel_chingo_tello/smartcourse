<?php 
$validarxyz = 0;
foreach ($_GET as $key => $value) {
    if($key === "xyz"){
        $validarxyz = 1;
    } 
}
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$cursos=!empty($this->cursos)?$this->cursos:'';
$imgcursodefecto=URL_BASE.'static/media/cursos/nofoto.jpg';
$miid=$this->usuActivo["idpersona"];
$midni=$this->usuActivo["dni"];
$mirol=$this->usuActivo["idrol"];
$categorias=$this->categorias;
// var_dump($this->usuActivo);
if($validarxyz === 1){
    // $cat_subcat = array("5", "13", "14", "15");
    $cat_subcat = $this->__buscarxPry($this->usuActivo["idproyecto"]);
    // var_dump($cat_subcat);
    if($mirol === 3 || $mirol === "3"){
        $cursosMatriculados=$this->__cursosxMatricula($this->usuActivo["idproyecto"], $miid);
        // var_dump($cursosMatriculados);
    }
}
?>
<style type="text/css">
*{font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";}
    body{ overflow-x: hidden; }
    .fa-search.girar{ transform:rotate(90deg);  -ms-transform:rotate(90deg); /* IE 9 */   -webkit-transform:rotate(90deg); /* Opera, Chrome, and Safari */   }
    .menucategorias{  max-width: 100%;  }
    .padding1ex{ padding: 10px 1ex; !important; }
    .card.vercurso{ width: 100%; cursor: pointer; }
    .card_img{display: block;  position: relative;}
    .card_img>img{ width: 100%; opacity: 1; -webkit-filter: sepia(.1) grayscale(.1) saturate(.8);  filter: sepia(.1) grayscale(.1) saturate(.8); display: block;
    height: 122px; overflow: hidden;  position: relative;}   
    .card:hover{outline: -webkit-focus-ring-color auto 5px;}
    .card .card_body > .card_acciones{ display: none; }
    .card:hover .card_body > .card_acciones {display: block;} 
    .card_acciones{  text-align: center;  position: absolute; width: 100%; bottom: 0px;  background: #9c9a9a5c;  margin: 0px -1ex;  padding: 0.5ex;  }
    .card_acciones.top{ bottom: auto; top: 0px;  }
    <?php 
    if($validarxyz === 1){
        echo '.card_body{ padding: 1ex;}';
        echo '.card_title , .card_texto{word-wrap: break-word; text-overflow: ellipsis; white-space: normal; font-weight: 600; min-height: 42px;  font-size: 15px;  color: #29303b;  margin-bottom: 10px; -webkit-line-clamp: 2; line-clamp: 2; -webkit-box-orient: vertical; } ';
    } else {
        echo '.card_body{ padding: 1ex; position: relative; }';
        echo '.card_title , .card_texto{ overflow: hidden; display: -webkit-box!important; text-overflow: ellipsis; white-space: normal; font-weight: 600; height: 36px; min-height: 42px;  font-size: 15px;  color: #29303b;  margin-bottom: 10px; -webkit-line-clamp: 2; line-clamp: 2; -webkit-box-orient: vertical; } ';
    }
    ?>
    .card_texto{ font-size: 13px;   color: #686f7a}
    .slick-slider{margin-bottom: 0px;}
    
    .menucat, ul, li, a  {margin: 0; padding: 0px }
    .menucat{ z-index: 99; }
    .menucat > a{ display: block; padding:1ex 1.5ex; }
    .menucat{padding-right: 1ex;}    
    .menucat a {text-decoration: none; color:#fff; cursor: pointer;}
    .menucat > ul {
        display: none;
        list-style: none;
        /*width: 9em;*/    
        position: absolute;
        z-index: 9999999;
        background: #047bfc;
        margin: 0px;       
        padding: 0px;   
    }
    .menucat.active > ul, .menucat:hover > ul{ display: block; }
    .menucat > ul li{min-width: 150px; border: 1px solid #fff; border-bottom: 0px;}
    .menucat > ul a{ display: block; padding:1ex 1.5ex; width: 100%;}
    .menucat > ul li:hover > a:hover:before{
        content: "\f00c";
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        color: #ffcd6e;
        font-size: 16px;
        padding-right: 0.5ex;
    }
    .menucat > ul li ul{ display: none; list-style: none; position: absolute;  left: 100%;  background: #047bfc; top: 0px }
    .menucat > ul li:hover > ul{ display: block; }
    .menucat > ul li{ position: relative; }    
</style>
<?php 
if($validarxyz === 1 || $validarxyz === "1"){
    if($mirol === "1" || $mirol === "10"){
?>
<div class="row bg-primary" style="padding: 1ex">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3 style="color: white;"><i class="fa fa-th"></i> Listado de Cursos [SmartCourse]</h3>
            </div> 
            <div cla class="col-md-4">
                <a class="btn btn-warning btn-block" href="https://clases.sytes.net/app/proyecto/cursos/crear"><i class="fa fa-plus"></i> Agregar curso</a>
            </div>
        </div>
    </div>
</div>
<?php 
    }

}else{
?>
<div class="row bg-primary" style="padding: 1ex">
    <div class="container">
        <div class="row">        
            <div cla class="col-md-9">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="menucat hvr-bounce-to-bottom">
                            <a class="toggle " href="#"><i class="fa fa-th"></i> Todas las categorias</a>
                            <ul>
                            <?php if(!empty($categorias))
                                foreach ($categorias as $cat){ 
                            ?>
                                <li> <a class="hvr-sweep-to-right" href="#"><?php echo $cat["nombre"] ?></a></li>
                            <?php
                                } 
                            ?>                                                            
                            </ul>
                        </div>
                    </div>
                    <input type="text" id="txttextoabuscar" class="form-control" placeholder="¿ Que curso estas Buscando ?">
                    <div class="input-group-append"><button class="btn btn-info btnbuscarcurso"> Buscar <i class="fa fa-search girar"></i> </button></div>
                </div>
            </div>
                <div cla class="col-md-3 text-center">
                    <a class="btn btn-warning" href="<?php echo $this->documento->getUrlBase(); ?>/cursos/?idcurso=0"><i class="fa fa-plus"></i> Agregar curso</a>
                </div>
        </div>
    </div>  
</div>
<?php
}
?>
<div class="row">
<div class="container-fluid" id="listadodecursos" style="margin: 0;padding: 0;">
<?php 
if(!empty($categorias))
    // var_dump($categorias);
    $catsclass = array();
foreach ($categorias as $cat): 
    $filtros=array();
    $filtros['idcategoria']=$cat['idcategoria'];
    $cursos=$this->__cursosxCategoria($filtros);
    $ncursos=count($cursos);
    $ncursos2 = 1;
    $subcat = $this->__SubCategorias($categorias, $cat["idcategoria"]);
    $nsubcat = count($subcat);
    // var_dump($subcat);
    if($ncursos == 0 && $nsubcat === 0){
        continue;
    }
    if($validarxyz === 1 || $validarxyz === "1"){
        $idc = $cat["idcategoria"];
        $validarid = 0;
        foreach ($cat_subcat as $val) {
            if($idc === $val){
                $validarid = 1;
            }
        }
        if($validarid === 1){
            $catsclass[] = ".cat" . $idc;
            $scriptsxyz = array();
            $catsids = array();
            if($nsubcat > 0){
                $filtros = array();
                $filtros['idpadre']=$cat['idcategoria'];
                $cursos=$this->__cursosxCategoria($filtros);
                // var_dump($cursos);
            }
            if($mirol === 3 || $mirol === "3"){
                // $cursosMatriculados=$this->__cursosxMatricula($this->usuActivo["idproyecto"], $miid);
                // var_dump($cursosMatriculados);
                $cursos_temp = array();
                foreach ($cursos as $temp1) {
                    foreach ($cursosMatriculados as $temp2) {
                        if($temp1["idcurso"] === $temp2["idcurso"]){
                            $cursos_temp[] = $temp1;
                        }
                    }
                }
                $cursos = $cursos_temp;
                $ncursos2 = count($cursos);
                // echo $ncursos2;
            }
            if($ncursos2 > 0){
            //     echo "alvaro";
            // }
    ?>
    <div class="row">
        <div class="col-md-12">          
            <div class="panel mostrarcollapse activeshow" style="border: 1px solid #dad7d7;   margin-bottom:0px;  ">
                <div class="panel-heading bg-primary">
                    <h3 class="panel-title"><!-- Los mejores cursos de:  --><?php echo $cat["nombre"]; ?> 
                        <?php 
                        if($ncursos > 0){
                            echo '<span class="badge badge-light">';
                            echo $ncursos;
                            echo '</span>';
                        } else{
                            echo '<span class="badge badge-light">';
                            echo '<select id="cboSubCat_' . $idc . '" class="form-control input-sm" onchange="cboSubCat(this)">';
                            echo '<option value="cat' . $idc . '">Todas las categorías</option>';
                            $catsids[] = array(
                                "id" => '#contcur_' . $idc,
                                "clase" => '.cat' . $idc
                            );
                            foreach ($subcat as $val) {
                                $cont_curs = 0;
                                foreach ($cursos as $cur) {
                                    if($cur["idcategoria"] === $val["idcategoria"]){
                                        $cont_curs++;
                                    }
                                }
                                if($cont_curs > 0){    
                                    echo '<option value="subcat' . $val["idcategoria"] . '">' . $val["nombre"] . '</option>';
                                }
                            }
                            echo '</select>';
                            echo '</span>';
                            echo '&nbsp;<span id="contcur_' . $idc . '" class="badge badge-light">';
                            echo $ncursos;
                            echo '</span>';
                        }
                        ?>
                    </h3>
                </div>
                <div class="panel-body">            
                   <div class="row">
                        <div class="col-md-12">                            
                        <div class="slick-items">
                        <?php     
                        if(!empty($cursos)){
                            $cnivel=count($cursos);
                            $array_idcursos = array();
                            $icur=0;
                            foreach ($cursos as $cur){ $icur++;
                            $idusuario=$cur["idusuario"]; 
                            $imgtmp=substr(RUTA_BASE,0,-1).$cur["imagen"];                 
                            $img=is_file($imgtmp)?URL_BASE.$cur["imagen"]:$imgcursodefecto;
                            $valida_idcurso = 0;
                            foreach ($array_idcursos as $arr_idcur) {
                                if($arr_idcur === $cur["idcurso"]){
                                    $valida_idcurso = 1;
                                }
                            }
                            if($valida_idcurso === 0){
                                $array_idcursos[] = $cur["idcurso"];
                                // var_dump($array_idcursos);
                            ?>
                                <div class="slick-item cur<?php echo $cur["idcurso"]; ?> cat<?php echo $idc; ?> subcat<?php echo $cur["idcategoria"]; ?>" style="padding: 0.25ex;">
                                    <div style="margin: 0.25ex 0.5ex; border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);" >
                                        <div class="card vercurso " data-idcurso="<?php echo $cur["idcurso"]; ?>">
                                            <div class="card_img" ><img  src="<?php echo @$img; ?>"></div>            
                                            <div class="card_body">
                                                <div class="card_title text-center"><?php echo $cur["nombre"]; ?></div>
                                                <!-- <div class="card_texto"><?php echo !empty($cur["descripcion"])?$cur["descripcion"]:'---'; ?></div> -->
                                                <?php if($mirol!=2 && $mirol!=3){?> 
                                                    <div class="card_acciones" data="<?php echo 'mi rol'.$mirol." --usuario".$idusuario." --miid".$miid." --midni".$midni ?>" >
                                                        <?php if($mirol=='1'||$idusuario==$miid){?>
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-primary editcursodisenio verpopover" data-placement="top" data-content="Editar  curso"><i class="fa fa-pencil"></i><!--i class="fa fa-cubes"></i--></a>
                                                        <?php 
                                                    } 
                                                    if($idusuario==$miid||$miid==$midni||$mirol=='1'){?>
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-danger elimcurso verpopover" data-placement="top" data-content="Eliminar mi curso"><i class="fa fa-trash"></i> </a>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php 
                        } else {
                            $scriptsxyz[] = array(
                                "clase" => ".cur" . $cur["idcurso"] ,
                                "add" => "subcat" . $cur["idcategoria"]
                            );
                        }
                        }
                    }
                    ?>
                            </div>
                        </div>
                    </div>
                </div>                              
            </div>
        </div>      
    </div>
<?php 
}
        foreach ($scriptsxyz as $script) {
            ?>
            <script type="text/javascript">
                $("<?php echo $script['clase']; ?>").addClass("<?php echo $script['add']; ?>");
            </script>
            <?php
        }
        //$catsids
        foreach ($catsids as $script) {
            ?>
            <script type="text/javascript">
                $("<?php echo $script['id']; ?>").html($("<?php echo $script['clase']; ?>").toArray().length);
            </script>
            <?php
        }
        }
        
    } else {
        ?>
        <div class="row">
        <div class="col-md-12">          
            <div class="panel mostrarcollapse activeshow" style="border: 1px solid #dad7d7;   margin-bottom:0px;  ">
                <div class="panel-heading bg-primary">
                    <h3 class="panel-title">Los mejores cursos de: <?php echo $cat["nombre"]; ?> <span class="badge badge-light"><?php echo $ncursos; ?></span></h3>
                </div>
                <div class="panel-body">            
                   <div class="row">
                        <div class="col-md-12">                            
                        <div class="slick-items">
                            <?php if($mirol!=3){ ?>
                            <div class="slick-item" style="padding: 0.25ex;" >
                                <div style="margin: 0.25ex 0.5ex; border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);" >
                                    <div class="card vercurso " data-idcurso="0" data-idcategoria="<?php echo $cat["idcategoria"]; ?>">
                                        <div class="card_img" ><img  src="<?php echo @$imgcursodefecto; ?>"></div>            
                                        <div class="card_body">
                                            <div class="card_title text-center">Crea tu nuevo curso ahora</div>
                                            <div class="card_texto">Crea Tu primer curso desde esta opcion de una forma rapida</div>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        <?php     
                        }                    
                        if(!empty($cursos)){
                            $cnivel=count($cursos);
                            $icur=0;
                            foreach ($cursos as $cur){ $icur++;
                            $idusuario=$cur["idusuario"]; 
                            $imgtmp=substr(RUTA_BASE,0,-1).$cur["imagen"];                 
                            $img=is_file($imgtmp)?URL_BASE.$cur["imagen"]:$imgcursodefecto; ?>
                                <div class="slick-item" style="padding: 0.25ex;">
                                    <div style="margin: 0.25ex 0.5ex; border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);" >
                                        <div class="card vercurso " data-idcurso="<?php echo $cur["idcurso"]; ?>">
                                            <div class="card_img" ><img  src="<?php echo @$img; ?>"></div>            
                                            <div class="card_body">
                                                <div class="card_title text-center"><?php echo $cur["nombre"]; ?></div>
                                                <div class="card_texto"><?php echo !empty($cur["descripcion"])?$cur["descripcion"]:'---'; ?></div>
                                                <?php if($mirol!=3){?> 
                                                    <div class="card_acciones" data="<?php echo 'mi rol'.$mirol." --usuario".$idusuario." --miid".$miid." --midni".$midni ?>" >
                                                        <?php if($mirol=='1'||$idusuario==$miid){?>
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-primary editcursodisenio verpopover" data-placement="top" data-content="Editar  curso"><i class="fa fa-pencil"></i><!--i class="fa fa-cubes"></i--></a>
                                                        <?php } if($idusuario==$miid||$miid==$midni||$mirol=='1'){?>
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-danger elimcurso verpopover" data-placement="top" data-content="Eliminar mi curso"><i class="fa fa-trash"></i> </a>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }}?>
                            </div>
                        </div>
                    </div>
                </div>                              
            </div>
        </div>      
    </div>
        <?php
    }
endforeach 
?>
</div></div>
<div class="row" id="aquicarganloscursos">

</div>
<script type="text/javascript">
$.fn.menu1= function(opt){    
    let op={};
        $.extend(op,opt);
    return this.each(function(){
        ele=$(this);
        ele.on('click','a',function(ev){
            ev.preventDefault();
            let el=$(this);
            if(el.hasClass('toggle')) ele.toggleClass('active');
            else el.toggleClass('active','active');    
            ev.stopPropagation();        
        })
        $(document).click(function(ev){
           if(ele.hasClass('active')) ele.removeClass('active');
        })
    });
}

function cboSubCat(e){
    var clase1 = ".cat" + $(e).attr("id").split("_")[1];
    $(clase1).hide();
    var clase2 = "." + $(e).val();
    $(clase2).show();
    var id = "#contcur_" + $(e).attr("id").split("_")[1];
    $(id).html($(clase2).toArray().length);
    $(clase1).parent().parent().parent().slick("slickGoTo", 0, true);
};
  
$(document).ready(function(ev){
    $('.menucat').menu1();
    let mirol='<?php echo $mirol;?>';
    var optionslike={
        //dots: true,
        infinite: false,
        //speed: 300,
        //adaptiveHeight: tru
        navigation: false,
        centerPadding: '60px',
        slidesToShow: 5,
        responsive:[
            { breakpoint: 1200, settings: {slidesToShow: 5, slidesToScroll: 5} },
            { breakpoint: 992, settings: {slidesToShow: 4, slidesToScroll: 4 } },
            { breakpoint: 880, settings: {slidesToShow: 3, slidesToScroll: 3 } },
            { breakpoint: 720, settings: {slidesToShow: 2, slidesToScroll: 2 } },
            { breakpoint: 320, settings: {slidesToShow: 1, slidesToScroll: 1 /*,arrows: false, centerPadding: '40px',*/} }       
        ]
    };
        var slikitems=$('.slick-items').slick(optionslike);
        $('#listadodecursos').on('click','.panel.mostrarcollapse .panel-title',function(ev){
            ev.preventDefault();
            if(ev.target.nodeName !== "SELECT"){
                let ac=$(this).closest('.panel.mostrarcollapse');
                ac.toggleClass('activeshow');
                if(!ac.hasClass('activeshow')) ac.children('.panel-body').fadeOut('fast');
                else ac.children('.panel-body').fadeIn('fast');
            }
             ev.stopPropagation();
        });
        var __buscarcurso=function(){
            let txt=$('#txttextoabuscar').val()||'';
            __sysajax({
                fromdata:{texto:txt},
                url:_sysUrlBase_+'/defecto/buscarxcategoriajson',
                showmsjok:false,
                callback:function(rs){
                    if(rs.data!=''){
                        let cat=JSON.parse(rs.data);
                        html='';
                        $.each(cat,function(i,v){
                            html+='<div class="row"><div class="col-md-12"><div class="panel mostrarcollapse activeshow" style="border: 1px solid #dad7d7; margin-bottom:0px;"><div class="panel-heading bg-primary"><h3 class="panel-title"> Los mejores cursos de: ';
                            html+=v.nombre;
                            html+=' <span class="badge badge-light">'+Object.keys(v.hijos).length+'</span></h3></div><div class="panel-body"><div class="row">';
                            html+='<div class="col-md-12"><div class="slick-items">';
                            if(mirol!='3'){
                                html+='<div class="slick-item" style="padding: 0.25ex;" ><div style="margin: 0.25ex 0.5ex; border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);" >';
                                html+='<div class="card vercurso " data-idcurso="0" data-idcategoria="'+i+'"><div class="card_img" >';
                                html+='<img  src="<?php echo $imgcursodefecto;?>"></div><div class="card_body">';
                                html+='<div class="card_title text-center">Crea tu nuevo curso ahora</div> <div class="card_texto">Crea Tu primer curso desde esta opcion de una forma rapida</div></div></div></div></div>';
                            }
                            $.each(v.hijos,function(j,h){                                
                                html+='<div class="slick-item" style="padding: 0.25ex;"><div style="margin: 0.25ex 0.5ex; border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);" ><div class="card vercurso " data-idcurso="'+h.idcurso+'"> <div class="card_img"><img  src="'+h.imagen+'"></div> <div class="card_body"><div class="card_title text-center">';
                                html+=h.nombre+'</div><div class="card_texto">'+h.descripcion+'</div>';
                                // console.log(mirol);
                                if(mirol!='3'){
                                    html+='<div class="card_acciones" ><a href="javascript:void(0);" class="btn btn-sm btn-primary editcursodisenio verpopover" data-placement="top" data-content="Editar  curso"><i class="fa fa-pencil"></i><!--i class="fa fa-cubes"></i--></a> <a href="javascript:void(0);" class="btn btn-sm btn-danger elimcurso verpopover" data-placement="top" data-content="Eliminar mi curso"><i class="fa fa-trash"></i> </a></div>';
                                }
                                html+='</div></div> </div></div>';
                            })
                            html+='</div></div> </div></div></div> </div></div>';
                       });
                        $('#listadodecursos').html(html);
                        var slikitems=$('.slick-items').slick(optionslike);
                    }else{
                        alert('no hay datos');
                    }                     
                }
            });           
        }
        $('.btnbuscarcurso').click(function(ev){
            ev.preventDefault();
            __buscarcurso();
        })
       // __buscarcurso();

        $('#listadodecursos').on('click','.vercurso',function(ev){
            ev.preventDefault();
            ev.stopPropagation();  
            // console.log('aaa');
            idcurso=$(this).attr('data-idcurso')||0;
            var urlxyz = "";
            if(idcurso==0||idcurso=="0"){
                urlxyz = _sysUrlBase_+'/cursos/?idcurso=0';    
            }else{
                urlxyz = _sysUrlBase_+'/cursos/ver/?idcurso='+idcurso;
            }
            if(<?php echo $validarxyz; ?> === 1){
                window.open(urlxyz, '_blank');
            }else{
                redir(urlxyz);
            }
            
       /* }).on('click','.editcursoautor',function(ev){
            ev.preventDefault();
            idcurso=$(this).closest('.vercurso').attr('data-idcurso')||0;
            redir(_sysUrlBase_+'/defecto/vercursoautor/?idcurso='+idcurso);
            ev.stopPropagation();*/
        }).on('click','.editcursodisenio',function(ev){
            if(<?php echo $validarxyz; ?> === 1){
                ev.preventDefault();
                var id=$(this).closest('.vercurso').attr('data-idcurso')||0;
                redir('https://clases.sytes.net/app/proyecto/cursos/crear?idcurso='+id);
                ev.stopPropagation();
            }else{
                ev.preventDefault();
                idcurso=$(this).closest('.vercurso').attr('data-idcurso')||0;
                redir(_sysUrlBase_+'/cursos?idcurso='+idcurso);
                ev.stopPropagation();
            }
        }).on('click','.elimcurso',function(ev){
            ev.preventDefault();            
            let $vercurso=$(this).closest('.vercurso');
            let idcurso=$vercurso.attr('data-idcurso')||0;
            __sysajax({
                fromdata:{idcurso:idcurso},
                url:_sysUrlBase_+'/acad_curso/eliminar',
                callback:function(rs){                    
                    $vercurso.animateCss({de:'out','callback':function(){$vercurso.parent().parent().remove();}})
                }
            });
            ev.stopPropagation();
            
        })

        $('.verpopover').popover({ trigger: 'hover',delay:100});
    });
</script>
<?php
    if(@$validarid === 1){
        foreach ($catsclass as $element) {
            ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    // console.log("aqui");
                    var maxheightxyz = 0;
                    $.each($("<?php echo $element; ?>"), function(i, item){
                        if(maxheightxyz < $(item).height()){
                            maxheightxyz = $(item).height();
                        }
                    })
                    $("<?php echo $element; ?>").children().children().attr("style", "height: " + maxheightxyz + "px;");
                });
            </script>
            <?php
        }
    }
?>