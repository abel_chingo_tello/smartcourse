<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$curso=!empty($this->curso)?$this->curso:'';
$idcursodetalle=!empty($this->idcursodetalle)?$this->idcursodetalle:'0';
$idcurso=!empty($curso["idcurso"])?$curso["idcurso"]:'';
$imgdefault=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
$img=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:$imgdefault;
$urlmedia=URL_MEDIA;
$haycontenidoinicial='';
if(file_exists(RUTA_MEDIA.'static'.SD.'media'.SD.'cursos'.SD.'curso_'.$idcurso.SD.'ses_0.html')) {
	$haycontenidoinicial=URL_MEDIA.'/static/media/cursos/curso_'.$idcurso."/ses_0.html";
}
?>
<style type="text/css">
body{ margin: 0px; }
.anillado{
	background-image: url(<?php echo $urlmedia; ?>/static/media/cursos/anillado.png);
    background-size: contain;
    height: calc(100%);
    position: absolute;
    width: 2em;
    margin-left: -2em;
}
.tabtop{ position: relative; width: 100%; height: 50px; }
.tabtop ul{ position:  absolute; right:  0px;  top: 1.2ex;}
ul.toolbar1{ display: inline-block; margin-bottom: 0px; padding-left:1ex; padding-top: 1ex; }
ul.toolbar1 li{display: inline-block; padding-left: 1ex;}


ul.toolbar1 ~ .showindicepage{ display: none; width: 340px; font-size: 1em; background: #2480dd; color: #333; }	
ul.toolbar1.active ~ .showindicepage{ position: absolute; left:50px; display: block; z-index: 9999; }
#indicetop{ margin: 1px; padding: 1px; padding-bottom: 2px; }
#indicetop > div{ padding: 5px; font-size: 14px; color: #000000cf; background: #fdf9f93d; border: 1px solid #f9f7f7a8; font-weight: bold; cursor: pointer;}
#indicetop .pagevisto:before{
	content: "\f00c";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
   /* text-decoration: inherit;
--adjust as necessary--*/
    color:#28a745;
    font-size: 16px;
    padding-right: 0.5ex;
     /*position: absolute;
   top: 10px;
    left: 0px;*/
}
.pnledittabmenu a{min-width: 150px; background-size: 100% 100%;	border: 0px;}

.menubooktop >ul{border-bottom: 0px;} 
.contentpages .showindicepage{background: #fff3f300;}
.modal{ color: #756c6c;}
</style>
<div class="row">
	<div class="col-md-6" style="height: 50px;">		
	    <ul class="toolbar1">
	      <li><a class="noalumno btnconfigurarSesion" href="javascript:void(0)"><i class="fa fa-cog fa-2x"></i></a></li>
	      <li><a class=" btnshowsesiones" href="javascript:void(0)"><i class="fa fa-th fa-2x"></i></a></li>
	      <li><a class="iconsesion" href="javascript:void(0)" data-link="<?php echo $haycontenidoinicial; ?>"><i class="fa fa-home fa-2x"></i></a></li>
	      <li class="nombrecurso">"<?php echo $curso["nombre"]; ?>"</li>
	      <li class="nombresesion"></li>	    	    
	    </ul>
	    <div class="showindicepage">
	    	<div class="pnlsesions text-center">
				<h2>Sesiones</h2>
				<div class="row" id="indicetop">
				</div>
			</div>
	    </div>
	</div>
	<div class="col-md-12" id="paneldisenio" style="height:calc(100vh - 52px);  overflow-y: auto;">
	</div>
	<div id="curdetalletmp" style="display: none;"></div>
</div>
<div id="iconclonar<?php echo $idgui; ?>" style="display: none;">
	<div id="clone_editmenuwitget">
		<div class="row">
			<div class="col-9">
				<div class="row">
					<div class="col-6 form-group">
						<label style=""><?php echo JrTexto::_('Texto');?></label> 
					    <input type="text" class="texto form-control">
					</div>
					<div class="col-3 form-group">
			            <label style=""><?php echo JrTexto::_('Fuente');?></label>
			            <div class="cajaselect">    
				            <select name="tipofuente" class="tipofuente form-control">
				            	<option values="Arial, Helvetica, sans-serif">Arial, Helvetica, sans-serif</option>
								<option values="‘Arial Black’, Gadget, sans-serif">‘Arial Black’, Gadget, sans-serif</option>
								<option values="‘Bookman Old Style’, serif">‘Bookman Old Style’, serif</option>
								<option values="‘Comic Sans MS’, cursive">‘Comic Sans MS’, cursive</option>
								<option values="Courier, monospace">Courier, monospace</option>
								<option values="‘Courier New’, Courier, monospace">‘Courier New’, Courier, monospace</option>
								<option values="Garamond, serif">Garamond, serif</option>
								<option values="Georgia, serif">Georgia, serif</option>
								<option values="Impact, Charcoal, sans-serif">Impact, Charcoal, sans-serif</option>
								<option values="‘Lucida Console’, Monaco, monospace">‘Lucida Console’, Monaco, monospace</option>
								<option values="‘Lucida Sans Unicode’, ‘Lucida Grande’, sans-serif">‘Lucida Sans Unicode’, ‘Lucida Grande’, sans-serif</option>
								<option values="‘MS Sans Serif’, Geneva, sans-serif">‘MS Sans Serif’, Geneva, sans-serif</option>
								<option values="‘MS Serif’, ‘New York’, sans-serif">‘MS Serif’, ‘New York’, sans-serif</option>
								<option values="‘Palatino Linotype’, ‘Book Antiqua’, Palatino, serif">‘Palatino Linotype’, ‘Book Antiqua’, Palatino, serif</option>
								<option values="Symbol, sans-serif">Symbol, sans-serif</option>
								<option values="Tahoma, Geneva, sans-serif">Tahoma, Geneva, sans-serif</option>
								<option values="‘Times New Roman’, Times, serif">‘Times New Roman’, Times, serif</option>
								<option values="‘Trebuchet MS’, Helvetica, sans-serif">‘Trebuchet MS’, Helvetica, sans-serif</option>
								<option values="Verdana, Geneva, sans-serif">Verdana, Geneva, sans-serif</option>
								<option values="Webdings, sans-serif">Webdings, sans-serif</option>
								<option values="Wingdings, ‘Zapf Dingbats’, sans-serif">Wingdings, ‘Zapf Dingbats’, sans-serif</option>
				            </select>
			            </div>
			        </div>
			        <div class="col-3 form-group">
			            <label style=""><?php echo JrTexto::_('Tamaño');?></label>
			            <div class="cajaselect">
		            		<select name="sizefuente" class="sizefuente form-control">
				            	<option values="8px">8px</option>
								<option values="10px">10px</option>
								<option values="12px" selected="selected">12px</option>
								<option values="14px">14px</option>
								<option values="16px">16px</option>
								<option values="18px">18px</option>
								<option values="20px">20px</option>
								<option values="24px">24px</option>
								<option values="26px">26px</option>
								<option values="28px">28px</option>
								<option values="30px">30px</option>
								<option values="0.5em">0.5em</option>
								<option values="1em">1em</option>
								<option values="1.5em">1.5em</option>
								<option values="2em">2em</option>
								<option values="2.5em">2.5em</option>
								<option values="3em">3em</option>
								<option values="3.5em">3.5em</option>
								<option values="4em">4em</option>
								<option values="5em">5em</option>
								<option values="6em">6em</option>
								<option values="7em">7em</option>
								<option values="8em">8em</option>
								<option values="9em">9em</option>
								<option values="10em">10em</option>
				            </select>
				        </div>
					</div>
					<div class="col-6 form-group text-left">
			            <label style=""><?php echo JrTexto::_('Color de Texto');?> </label>  
			            <input type="color" class="colortexto sincolor" data-color="#ffffff" style="margin-left: 1ex; display:none;" value="#ffffff"><br>
			            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
				        <a style="cursor:pointer;" class="chkcambiarcolor fa fa-check-circle"><span> Si</span></a>
			        </div>
			        <div class="col-6 form-group text-left ">
			            <label style=""><?php echo JrTexto::_('Color de fondo');?> </label>  
			            <input type="color" class="colorfondo sincolor" data-color="#007bff" style="margin-left: 1ex; display:none;" value="#007bff"><br>
			            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
				        <a style="cursor:pointer;" class="chkcambiarcolor fa fa-check-circle"><span> Si</span></a>
			        </div>
	        	</div>
			</div>	
	        <div class="col-3 form-group text-center ">	           
	            <label style="float: none"><?php echo JrTexto::_('Imagen de fondo');?></label> 
	            <div style="position: relative;" class="frmchangeimage">
	                <div class="toolbarmouse text-center">	                                  
	                  <span class="btn subirimg" style="background: #e2e2e285;"><i class="fa fa-pencil" style="color:#000"></i></span>	                  
	                </div>	                
	                <img src="<?php echo $imgdefault; ?>" class="imagenfondo img-responsive center-block" style="max-width:150px; max-height:150px;">
	                <small>Tamaño 200px x 200px</small> 
	            </div>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-12">
	        	<div class="row">
	        	<div class="col-2 cicon verpopover" data-placement="top" data-type="indice" title="Indice" data-content="Agregar Indice" data-acc="addindice">
	        		<i class="btn btn-primary fa fa-list"> <!--br><span>Indice</span--></i> 
	        	</div>
				<div class="col-2 cicon verpopover" data-placement="top" data-type="texto" title="Texto" data-content="Agregar texto" data-acc="texto">					
					<i class="fa btn  btn-primary fa-text-width"> <!--br><span>Texto</span--></i> 
				</div>
				<div class="col-2 cicon verpopover" data-placement="top" data-type="widgets" title="Widgets" data-content="Agregar Opciones 2" data-acc="widgets">				
					<i class="btn  btn-primary fa fa-cubes"> <!--br><span>Texto</span--></i> 
				</div>
				<div class="col-2 cicon verpopover" data-placement="top" data-type="imagen" title="Imagen" data-content="Agregar una imagen" data-acc="imagen">
					<i class="fa btn btn-primary fa-file-image-o"> <!--br><span>Imagen</span--></i> 
				</div>
				<div class="col-2 cicon verpopover" data-placement="top" data-type="audio" title="Audio" data-content="Agregar un audio" data-acc="audio">
					<i class="fa btn btn-primary fa-file-audio-o"> <!--br><span>Audio</span--></i> 
				</div>
				<div class="col-2 cicon verpopover" data-placement="top" data-type="video" title="Video" data-content="agregar un video" data-acc="video">
					<i class="fa btn btn-primary  fa-file-movie-o"><!--br><span>Video</span--></i> 
				</div>
				<div class="col-2 cicon verpopover" data-placement="top" data-type="pdf" title="PDF" data-content="Agregar un archivo pdf" data-acc="pdf">
					<i class="fa btn btn-primary  fa-file-pdf-o"> <!--br><span>PDF</span--></i> 
				</div>
				<div class="col-2 cicon verpopover" data-placement="top" data-type="html" title="HTML" data-content="Agregar contenido HTML - seleccione un zip si incluye mas contenido" data-acc="html">
					<i class="fa btn btn-primary fa-file-code-o"> <!--br><span>Html</span--></i> 
				</div>
				<!--div class="col-2 cicon verpopover" data-placement="top" data-type="flash" title="Flash" data-content="Agregar SWF - archivo Flash" data-acc="swf">
					<i class="fa btn btn-primary fa-file-archive-o"> <br><span>Flash</span></i> 
				</div-->				
				<div class="col-2 cicon verpopover " data-placement="top" data-type="smarticlock" title="SmartTic Look" data-content="Look" data-acc="smarticlock">
					<i class="fa btn btn-primary fa-cogs"> <span> L</span></i> 
				</div>
				<div class="col-2 cicon verpopover " data-placement="top" data-type="smarticlookPractice" title="SmartTic Practice" data-content="Practice" data-acc="smarticlookPractice">
					<i class="fa btn btn-primary fa-cogs"> <span> P</span></i>
				</div>
				<div class="col-2 cicon verpopover " data-placement="top" data-type="smarticDobyyourselft" title="SmartTic Do by yourselft" data-content="Do it by yourself" data-acc="smarticDobyyourselft">
					<i class="fa btn btn-primary fa-cogs"> <span> D</span></i> 
				</div>
				<div class="col-2 cicon verpopover " data-placement="top" data-type="game" title="Juegos" data-content="Agregar juegos interactivos" data-acc="game">
					<i class="fa btn btn-primary fa-gamepad"> <!--span>Juegos</span--></i> 
				</div>
				<div class="col-2  cicon verpopover " data-placement="top" data-type="smartquiz" title="SmartQuiz" data-content="Agregar Examenes" data-acc="smartquiz">
					<i class="fa btn btn-primary fa-tasks"> <!--br><span>SmartQuiz</span--></i> 
				</div>				
				</div>
	        </div>
	       
	    </div>
		<div class="row">	        	
			<div class="col-12 text-center"><hr>
				<a href="javascript:void(0)" class="guardarcambios btn btn-warning"><i class="fa fa-save"></i> Aplicar cambios</a>
				<a href="javascript:void(0)" class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</a>
			</div>
		</div>
	</div>
	
	<div class="row" id="config_plantilla">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6 form-group">
		            <label style=""><?php echo JrTexto::_('Seleccione vistas');?></label>
		            <div class="cajaselect">    
			            <select class="menuvista form-control">
			            	<option value="blanco" selected="true">Vista blanco</option>									
							<option value="smartbook">Vista Book</option>						
			            </select>
		            </div>
		        </div>
		        <div class="col-md-6 form-group">
		            <label style=""><?php echo JrTexto::_('Menu');?></label>
		            <div class="cajaselect">    
			            <select class="menuposicion form-control">
			            	<option value="left-right" selected="true">Arriba</option>									
							<option value="bootom-top">Derecha</option>
							<!--option value="left-top">Izquierda</option-->
							<!--option value="bootom-middle">Derecha - Centro</option-->
							<!--option value="bootom-bootom">Derecha - Abajo</option-->
							<option value="bootom-left">Abajo - izquierda</option>
							<!--option value="bootom-center">Abajo - Centro</option-->
							<option value="bootom-right">Abajo - Derecha</option>
							
							<!--option value="left-middle">Izquierda - Centro</option-->
							<!--option value="left-bootom">Izquierda - Abajo</option-->
			            </select>
		            </div>
		        </div>
		        <div class="col-md-12"><hr></div>		
			</div>			
			<div class="row">
				<div class="col-md-12"><h5>Estilo de Fondo</h5></div>
				<div class="col-4 form-group text-left">
		            <label style=""><?php echo JrTexto::_('Color de Texto');?> </label>  
		            <input type="color" class="maincolortexto sincolor" data-color="#007bff" style="margin-left: 1ex; display:none;" value=""><br>
		            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
			        <a style="cursor:pointer;" class="chkcambiarcolor fa fa-check-circle"><span> Si</span></a>
		        </div>
		        <div class="col-4 form-group text-left ">
		            <label style=""><?php echo JrTexto::_('Color de fondo');?> </label>  
		            <input type="color" class="maincolorfondo sincolor" data-color="#ffffff" style="margin-left: 1ex; display:none;" value=""><br>
		            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
			        <a style="cursor:pointer;" class="chkcambiarcolor fa fa-check-circle"><span> Si</span></a>
		        </div>
		        <div class="col-4 form-group text-center ">	           
		            <label style="float: none"><?php echo JrTexto::_('Imagen de fondo');?></label> 
		            <div style="position: relative;" class="frmchangeimage">
		                <div class="toolbarmouse text-center">	                                  
		                  <span class="btn subirimg" style="background: #e2e2e285;"><i class="fa fa-pencil" style="color:#000"></i></span>
		                  <span class="btn borrarimagenfondo" style="background: #e2e2e285;"><i class="fa fa-trash" style="color:#000"></i></span>               
		                </div>		                
		                <img src="<?php echo $imgdefault; ?>" class="mainimagenfondo img-responsive center-block" style="max-width:100px; max-height:100px;">			                
		            </div>
		        </div>
			</div>
			<div class="row">
				<div class="col-md-12"><hr><h5>Estilo de Pagina</h5></div>
				<div class="col-4 form-group text-left">
		            <label style=""><?php echo JrTexto::_('Color de Texto');?> </label>  
		            <input type="color" class="pagecolortexto sincolor" data-color="#007bff" style="margin-left: 1ex; display:none;" value=""><br>
		            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
			        <a style="cursor:pointer;" class="chkcambiarcolor fa fa-check-circle"><span> Si</span></a>
		        </div>
		        <div class="col-4 form-group text-left ">
		            <label style=""><?php echo JrTexto::_('Color de fondo');?> </label>  
		            <input type="color" class="pagecolorfondo sincolor" data-color="#ffffff" style="margin-left: 1ex; display:none;" value=""><br>
		            <label style="float: none;"><?php echo JrTexto::_('Transparente');?> </label>              
			        <a style="cursor:pointer;" class="chkcambiarcolor fa fa-check-circle"><span> Si</span></a>
		        </div>
		        <div class="col-4 form-group text-center ">	           
		            <label style="float: none"><?php echo JrTexto::_('Imagen de fondo');?></label> 
		            <div style="position: relative;" class="frmchangeimage">
		                <div class="toolbarmouse text-center">	                                  
		                  <span class="btn subirimg" style="background: #e2e2e285;"><i class="fa fa-pencil" style="color:#000"></i></span> 
		                  <span class="btn borrarimagenfondo" style="background: #e2e2e285;"><i class="fa fa-trash" style="color:#000"></i></span>              
		                </div>		               
		                <img src="<?php echo $imgdefault; ?>" class="pageimagenfondo img-responsive center-block" style="max-width:100px; max-height:100px;">
		            </div>
		        </div>
			</div>		
		</div>
		<div class="col-12 text-center">
			<button class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> Cerrar</button>
		</div>	
	</div>
	<div id="clone_tplindice">
		<div class="row">
			<div class="col-md-12 txtedit" style="width: 100%; min-height: 30px;">
				Escriba Aqui su titulo, texto o informacion
			</div>
			<div class="col-md-12 showindicepage"></div>	
			<div class="col-md-12 noalumno text-center">
				<button class="btnsubirimagenfondo btn btn-primary"> Subir Imagen de Fondo</button>
				<button class="borrarimagenfondo btn btn-warning">Borrar imagen</button>
				<hr>
				<button class="btnguardarplantilla btn btn-success">Guardar plantilla</button>
			</div>		
		</div>
	</div>
	<div id="clone_tpltexto">
		<div class="row">
			<div class="col-md-12 txtedit" style="width: 100%; min-height: 30px;">Escriba Aqui su titulo, texto o informacion</div>
			<div class="col-md-12 noalumno text-center">
				<button class="btnsubirimagenfondo btn btn-primary"> Subir Imagen de Fondo</button>
				<button class="borrarimagenfondo btn btn-warning">Borrar imagen</button>
				<hr>
				<button class="btnguardarplantilla btn btn-success">Guardar plantilla</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var $copyhtml='';
	var idcurso=parseInt('<?php echo $idcurso; ?>');
	var iddetalle=parseInt('<?php echo $idcursodetalle; ?>');
	var _sysUrlMedia_='<?php echo URL_MEDIA; ?>';
	var _sysUrlRaiz_='<?php echo URL_RAIZ;?>';
	var imgdefault='<?php echo $imgdefault;?>';

	var __subirmediamdini=function(obj,type,typefile){
    	var oldmedia=obj.attr('data-oldmedia')||'';
    	var file=document.createElement('input');
	    	file.type='file';
	    	file.accept=type;
	    	file.id='file_'+__idgui();
	    	file.addEventListener('change',function(ev){
	    		let $idprogress=$('#clone_progressup').clone(true);
				let iduploadtmp='idup'+__idgui();
				$idprogress.attr('id',iduploadtmp);
				obj.append($idprogress);							
				var data=__formdata('');
				data.append('media',this.files[0]);
				data.append('dirmedia','curso_'+idcurso+'/');
				data.append('oldmedia',oldmedia);
				data.append('typefile',typefile);
				__sysajax({ 
					fromdata:data,
                	url:_sysUrlBase_+'/acad_cursodetalle/uploadmedia',
                	iduploadtmp: '#'+iduploadtmp,
                	callback:function(rs){
                		if(rs.code=='ok'){
                			if(rs.media!=''){
                				__cargarplantilla({url:'/plantillas/'+typefile,donde:'contentpages',data:{link:rs.media}});
                				//obj.attr('data-oldmedia',rs.media).children('i').addClass('btn-success').removeClass('btn-primary'); 
                			} 
                		}
                	}
            	});
	    	});
	    file.click();
	}

	var __addexamen=function(obj,fncall){
		//let md=__sysmodal({titulo:'Examenes',html:$('#config_plantilla').html()});
		obj.off('addassesment');
		let lstmp='btnaddexamen'+__idgui();
		obj.addClass(lstmp);
		let url=_sysUrlBase_+'/quiz/buscar/?plt=modal&fcall='+lstmp;
		let md=__sysmodal({titulo:'Quiz',url:url});
		obj.on('addassesment',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			var idexamen=$(this).attr('data-idexamen');
			let link=_sysUrlBase_+'/quiz/ver/?idexamen='+idexamen;
			if(_isFunction(fncall)) fncall(link);	
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}
	var __addgame=function(obj,fncall){
		obj.off('addgame');
		let lstmp='addgame'+__idgui();
		obj.addClass(lstmp);
		let url=_sysUrlBase_+'/games/buscar/?plt=modal&fcall='+lstmp;		
		let md=__sysmodal({titulo:'Games',url:url});
		obj.on('addgame',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			var idgame=$(this).attr('data-idgame');
			let link=_sysUrlRaiz_+'game/ver/?idgame='+idgame+'&tmp='+lstmp;
			if(_isFunction(fncall)) fncall(link); 		
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}

	var __addtics=function(obj,met,fncall){
		obj.off('addtics');
		let lstmp='addtics'+__idgui();
		obj.addClass(lstmp);
		let url=_sysUrlBase_+'/tics/buscar/?plt=modal&fcall='+lstmp+'&met='+met;
		let md=__sysmodal({titulo:'Games',url:url});
		obj.on('addtics',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			let idtic=$(this).attr('data-idtic'); // idactividad
			let idsesion=$(this).attr('data-idsesion');
			let link=_sysUrlRaiz_+'actividad/ver/?idactividad='+idtic+"&met="+met+"&idsesion="+idsesion+'&tmp='+lstmp;	
			obj.attr('data-link',link);
			if(_isFunction(fncall)) fncall(link); 
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}

	let __subirmediamd=function(obj,type,typefile,fncall){
		$('audio').trigger('pause');
		$('video').trigger('pause');
    	var oldmedia=obj.attr('data-oldmedia')||'';
    	var file=document.createElement('input');
	    	file.type='file';
	    	file.accept=type;
	    	file.id='file_'+__idgui();
	    	file.addEventListener('change',function(ev){
	    		let $idprogress=$('#clone_progressup').clone(true);
				let iduploadtmp='idup'+__idgui();
				$idprogress.attr('id',iduploadtmp);
				obj.append($idprogress);							
				var data=__formdata('');
				data.append('media',this.files[0]);
				data.append('dirmedia','curso_'+idcurso+'/');
				data.append('oldmedia',oldmedia);
				data.append('typefile',typefile);
				__sysajax({ 
					fromdata:data,
                	url:_sysUrlBase_+'/acad_cursodetalle/uploadmedia',
                	iduploadtmp: '#'+iduploadtmp,
                	callback:function(rs){
                		if(rs.code=='ok'){
                			if(rs.media!=''){
                			   if(_isFunction(fncall)) fncall(rs);             						                				
                			}
                		}
                	}
            	});
	    	});
	    	file.click();
    }

	var __paneltools=function(obj){
		let item=obj.item;
		let txt=obj.texto||'';
		let fncall=obj.fncall;
		$('audio').trigger('pause');
		$('video').trigger('pause');	
		//
		let fuente=item.attr('data-fuente')||'';
		let zise=item.attr('data-zise')||'';
		let colortexto=item.attr('data-colortexto')||'';
		let colorfondo=item.attr('data-colorfondo')||'';
		let imagenfondo=item.attr('data-imagenfondo')||'';
		let datatype=item.attr('data-type')||'';
		let datalink=item.attr('data-link')||'';
		console.log(colorfondo,colortexto);
		let md=__sysmodal({titulo:'',html:$('#clone_editmenuwitget').html()});
		md.find('input.texto').val(txt.trim());
		md.find('select.tipofuente').val(fuente.trim());
		md.find('select.sizefuente').val(zise.trim());
		if(colortexto!='')
		md.find('input.colortexto').removeClass('sincolor').val(colortexto).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
			
		if(colorfondo!='')
		md.find('input.colorfondo').removeClass('sincolor').val(colorfondo).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
			
		if(imagenfondo!='')md.find('img.imagenfondo').attr('src',imagenfondo).attr('data-oldmedia',imagenfondo);
		md.find('input.texto').on('change',function(){txt=$(this).val()});
		md.find('select.tipofuente').on('change',function(){fuente= $(this).val();});
		md.find('select.sizefuente').on('change',function(){zise=$(this).val();});
		md.find('input.colortexto').on('change',function(){colortexto=$(this).hasClass('sincolor')?'':$(this).val();});
		md.find('input.colorfondo').on('change',function(){colorfondo=$(this).hasClass('sincolor')?'':$(this).val();});
		md.find('.chkcambiarcolor').on('click',function(ev){
			let input= $(this).siblings('input');
			if($(this).hasClass('fa-circle-o')){
	        	$('span',this).text(' Si');	
	        	input.addClass('sincolor');
	       		input.hide().trigger('change');
	        	$(this).removeClass('fa-circle-o').addClass('fa-check-circle');
	      	}else{
	        	$('span',this).text(' No');
	        	input.removeClass('sincolor');
	        	input.val(input.attr('data-color')).show();
	        	input.trigger('change');
	        	$(this).addClass('fa-circle-o').removeClass('fa-check-circle');
	      	}
	      	ev.stopPropagation();});
		md.on('click','.subirimg',function(ev){
			let img=$(this).closest('.frmchangeimage').find('img');
			__subirmediamd($(this),'image/x-png, image/gif, image/jpeg, image/*','imagen',function(rs){
				img.attr('src',rs.media);
				img.attr('data-oldmedia',rs.media);
				imagenfondo=rs.media;
			});
		});
		md.find('.cicon[data-type="'+datatype+'"]').attr('data-oldmedia',datalink).children('i').removeClass('btn-primary').addClass('btn-success');
		md.on('click','.cicon',function(ev){
			ev.preventDefault();
		    let it=$(this);
		    let type=it.attr('data-type')||'';
		    //if(type===''||type==datatype) return false;
		    it.attr('data-oldmedia',datalink)
		    it.siblings().removeAttr('data-oldmedia').children('i.btn-success').removeClass('btn-success').addClass('btn-primary');		    	
		    datatype=type;
		    let _cont=$('#bookplantilla').children('#contentpages');
		    switch (type){
		    	case 'smarticlock':
			    	__addtics(it,1,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;
			    case 'smarticlookPractice':
			    	__addtics(it,2,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;
			    case 'smarticDobyyourselft':
			    	__addtics(it,3,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;
			    case 'game':
			    	__addgame(it,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;
			    case 'smartquiz':
			    	__addexamen(it,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;			    
				case 'pdf':
				   	__subirmediamd(it,'application/pdf , application/zip','pdf',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=rs.media;
			    		it.attr('data-oldmedia',rs.media);
				   	});				    	
				    break;
				case 'html':
				   	__subirmediamd(it,'.html , .htm , application/zip','html',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=rs.media;	
			    		it.attr('data-oldmedia',rs.media);		    		
				   	});				    	
				    break;
				case 'flash':
					__subirmediamd(it,'.swf , .flv  , application/zip','flash',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
				   		it.attr('data-oldmedia',rs.media);
			    		datalink=rs.media;
				   	});
				    break;
				case 'audio':
				   	__subirmediamd(it,'audio/mp3','audio',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=rs.media;
			    		it.attr('data-oldmedia',rs.media);
			    		item.attr('data-idinfotmp','aud'+__idgui());
				   	});				    	
				    break;
				case 'video':
				   	__subirmediamd(it,'video/mp4, video/*','video',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=rs.media;
			    		it.attr('data-oldmedia',rs.media);
				   	});				    	
				    break;
				case 'imagen':
				    	__subirmediamd(it,'image/x-png, image/gif, image/jpeg, image/*','imagen',function(rs){
				    		it.children('i').addClass('btn-success').removeClass('btn-primary');
				    		item.attr('data-idinfotmp','img'+__idgui());
					   		it.attr('data-oldmedia',rs.media);
				    		datalink=rs.media;				    		
				    	});
				        break;
				 case 'indice':
				 		it.children('i').addClass('btn-success').removeClass('btn-primary');
				 		item.attr('data-idinfotmp','indice'+__idgui());
			    		datalink='';
				        break;
				case 'texto':
						it.children('i').addClass('btn-success').removeClass('btn-primary');
						item.attr('data-idinfotmp','txt'+__idgui());
				        datalink='';
				        break;

				case 'widgets':
						it.children('i').addClass('btn-success').removeClass('btn-primary');
						item.attr('data-idinfotmp','widgets'+__idgui());
			    		datalink='';
				    	/*_cont.html('<div class="widgetjson"></div><div class="widgetcont"></div>');
						$widcont=_cont.children('.widgetcont');
						var dt={data:{idcurso:idcurso,idcursodetalle:iddetalle},url:'/widgets/'};
						__cargarWidget(dt,$widcont);*/
				        break;
							 
				}
			ev.stopPropagation();
		}).on('click','.guardarcambios',function(ev){		    
		   	if(_isFunction(fncall)) fncall({
		    	texto:txt,				
				fuente:fuente,
				zise:zise,
				colortexto:colortexto,
				colorfondo:colorfondo,
				imagenfondo:imagenfondo,
				datatype:datatype,
				datalink:datalink
		    }); 
		   	md.modal('hide');
	    }).on('click','.cerrarmodal',function(){
			md.modal('hide');
		})
		md.find('.verpopover').popover({ trigger: 'hover',delay:100});
	}

	var _guardarhtml=function(obj){
		$('audio').trigger('pause');
		$('video').trigger('pause');
	    let stpl=$('#contentpages').find('.savetemplatedatainfo');
	    if(stpl.length){
	    	let id=stpl.attr('id');
	    	let info=stpl.find('.tplinfotmp');
	    	let datainfo={};
	    	$.each(info,function(i,v){
	    		vid=$(this).attr('data-texto');
	    		datainfo[vid]=$(this).html();
	    	})
	    	$('[data-idinfotmp="'+id+'"]').attr('data-datainfo',JSON.stringify(datainfo));
	    }

		let bookplantilla=$('#bookplantilla').clone(false);
		bookplantilla.find('audio').trigger('pause');
		bookplantilla.find('video').trigger('pause');
		let estilo=$('#mainestilo').attr('data-tmpurl')||'';
		let namecss=(bookplantilla.attr('data-vistatlp')||'smartbook')+'.css';
		bookplantilla.attr('data-estilo',estilo+namecss);

		let page=$('<div></div>');
		page.append(bookplantilla);		
		var html=page.html();
		var data=__formdata('');
		data.append('idcurso',idcurso);
		data.append('idcursodetalle',iddetalle);
		data.append('html',html);
		__sysajax({ 
			fromdata:data,
        	url:_sysUrlBase_+'/acad_cursodetalle/guardarSesionhtml',
        	callback:function(rs){
        		$('._btnversesion[data-iddetalle="'+iddetalle+'"]').attr('data-link','/'+rs.archivo);
        	}
    	});
	}

	$(document).ready(function(){
		var idcurso='<?php echo @$idcurso; ?>';
		var tmpmenu=<?php echo json_encode($this->menus); ?>;		
		let __updateindicetop=function(menu){
			let imen=1;
			let html='';
			$.each(tmpmenu,function(i,m){
				let link=m["html"]||'';
				let lin=(link!=undefined&&link!='')?' data-link="/'+link+'"':'';				
				html+='<div class="col-md-6 text-left hvr-glow _btnversesion" '+lin+' data-iddetalle="'+m["idcursodetalle"]+'">' +imen+'. '+ m["nombre"]+'</div>';				
				imen++;
			});
			$('#indicetop').html(html);
		}
		__updateindicetop();
		$('ul.toolbar1').on('click','.btnshowsesiones',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			$(this).closest('.toolbar1').toggleClass('active');
		}).on('click','.iconsesion',function(ev){
			ev.preventDefault();			
			let link=$(this).attr('data-link')||'';
			if(link==''){						
				var itema=$(this);
				__paneltools({
					item:itema,
					texto:'',
					fncall:function(rs){						
						if(rs.colorfondo=='') itema.attr('data-colorfondo','transparent').css({'background-color':'transparent'});					
						else  itema.attr('data-colorfondo',rs.colorfondo).css({'background-color':rs.colorfondo});
						if(rs.colortexto!='') itema.attr('data-colorfondo',rs.colortexto).css({'color':rs.colortexto});
						if(rs.imagenfondo.trim()!='')itema.css({'background-image':'url('+rs.imagenfondo+')','background-size':'100% 100%','min-width':'150px','border':'0px'}).attr('data-imagenfondo',rs.imagenfondo);
						else itema.css({'background-image':'none'});
						if(rs.datalink.trim()!='')itema.attr('data-link',rs.datalink);
						if(rs.datatype.trim()!='')itema.attr('data-type',rs.datatype);
						if(rs.fuente.trim()!='')itema.attr('data-fuente',rs.fuente).css({'font-family':rs.fuente});
						if(rs.zise.trim()!='')itema.attr('data-zise',rs.zise).css({'font-size':rs.zise})
						itema.attr('data-donde','contentpages');
						itema.trigger('click');
					}
				});
				ev.stopPropagation();
			}else{
				__sysajax({
					url:link,
					fromdata:{mostrarcargando:false},
					type:'html',
					donde:$('#bookplantilla'),		
					callback:function(rs){						
						let cont=$('<div>'+rs+'</div>');
						let tpl=cont.find('#bookplantilla');
						let cssfile=tpl.attr('data-cssfile')||'';
						let imgfondo=tpl.attr('data-imagenfondo')||'';
						let textocolor=tpl.attr('data-colortexto')||'';
						let fondocolor=tpl.attr('data-colorfondo')||'';
						if(cssfile!='')	$('#mainestilo').attr('href',$('#mainestilo').attr('data-tmpurl')+cssfile);
						if(imgfondo!='')$('body').css({'background-image':'url('+imgfondo+')'});
						else $('body').css({'background-image':'none'});
						if(textocolor!='')$('body').css({'color':textocolor});
						if(fondocolor!='')$('body').css({'background-color':fondocolor});

						$('#bookplantilla').html(tpl.html());
					}
				});	
			}
			ev.stopPropagation();
			//$('#mindice').find('li').first().next().find('.addcontent').trigger('click');			
		}).on('contextmenu','.iconsesion',function(ev){			
			ev.preventDefault();	
			$('audio , video').trigger('pause');		
			var itema=$(this);
			__paneltools({
				item:itema,
				texto:'',
				fncall:function(rs){						
					if(rs.colorfondo=='') itema.attr('data-colorfondo','transparent').css({'background-color':'transparent'});					
					else  itema.attr('data-colorfondo',rs.colorfondo).css({'background-color':rs.colorfondo});
					if(rs.colortexto!='') itema.attr('data-colorfondo',rs.colortexto).css({'color':rs.colortexto});
					if(rs.imagenfondo.trim()!='')itema.css({'background-image':'url('+rs.imagenfondo+')','background-size':'100% 100%','min-width':'150px','border':'0px'}).attr('data-imagenfondo',rs.imagenfondo);
					else itema.css({'background-image':'none'});
					if(rs.datalink.trim()!='')itema.attr('data-link',rs.datalink);
					if(rs.datatype.trim()!='')itema.attr('data-type',rs.datatype);
					if(rs.fuente.trim()!='')itema.attr('data-fuente',rs.fuente).css({'font-family':rs.fuente});
					if(rs.zise.trim()!='')itema.attr('data-zise',rs.zise).css({'font-size':rs.zise})
					itema.attr('data-donde','contentpages');
					itema.trigger('click');
				}
			});
			ev.stopPropagation();
		}).on('click','.btnconfigurarSesion',function(ev){
			let tpl=$('#bookplantilla');
			let page=tpl.children('#contentpages');
			let posmenu=tpl.attr('data-oldmenuposicion')||'left-right';
			let vista=tpl.attr('data-vistatlp')||'smartbook';
			let mainimagenfondo=tpl.attr('data-imagenfondo')||'';
			let maincolortexto=tpl.attr('data-colortexto')||'';
			let maincolorfondo=tpl.attr('data-colorfondo')||'';
			let pageimagenfondo=page.attr('data-imagenfondo')||'';
			let pagecolortexto=page.attr('data-colortexto')||'';
			let pagecolorfondo=page.attr('data-colorfondo')||'';
			let md=__sysmodal({titulo:'',html:$('#config_plantilla').html()});
			md.find('.menuposicion').val(posmenu);
			md.find('.menuvista').val(vista);
			if(maincolortexto!=''){
			md.find('input.maincolortexto').removeClass('sincolor').val(maincolortexto).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
			}

			if(pagecolortexto!='')
			md.find('input.pagecolortexto').removeClass('sincolor').val(pagecolortexto).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
			
			if(maincolorfondo!='')
			md.find('input.maincolorfondo').removeClass('sincolor').val(maincolorfondo).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
			if(pagecolorfondo!='')
			md.find('input.pagecolorfondo').removeClass('sincolor').val(pagecolorfondo).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
			
			if(mainimagenfondo!='')md.find('img.mainimagenfondo').attr('src',mainimagenfondo).attr('data-oldmedia',mainimagenfondo);
			if(pageimagenfondo!='')md.find('img.pageimagenfondo').attr('src',pageimagenfondo).attr('data-oldmedia',pageimagenfondo);

			md.on('change','input.maincolortexto',function(){
				maincolortexto=$(this).hasClass('sincolor')?'':$(this).val();				
				tpl.attr('data-colortexto',maincolortexto);
				tpl.closest('body').css({'color':maincolortexto});
			}).on('change','input.maincolorfondo',function(){
				maincolorfondo=$(this).hasClass('sincolor')?'':$(this).val();
				if(maincolorfondo!=''){
					tpl.closest('body').css({'background-color':maincolorfondo});
					tpl.attr('data-colorfondo',maincolorfondo);
				}else{
					tpl.closest('body').css({'background-color':'none'});
					tpl.attr('data-colorfondo','none');
				}			
			}).on('change','input.pagecolortexto',function(){
				pagecolortexto=$(this).hasClass('sincolor')?'':$(this).val();
				page.attr('data-colortexto',pagecolortexto);
				page.css({'color':pagecolortexto});
			}).on('change','input.pagecolorfondo',function(){
				pagecolorfondo=$(this).hasClass('sincolor')?'':$(this).val();
				page.attr('data-colorfondo',pagecolorfondo);
				page.css({'background-color':pagecolorfondo});
			});

			md.on('change','.menuvista',function(ev){
				ev.preventDefault();
				ev.stopPropagation();
				let typ=$(this).val()||'smartbook';
				cssgeneral=$('#mainestilo');
				link=cssgeneral.attr('data-tmpurl');
				if(typ==''){					
					cssgeneral.attr('href',link+'blanco.css');	
					tpl.attr('data-cssfile','blanco.css');				
				}else{
					cssgeneral.attr('href',link+typ+'.css');
					tpl.attr('data-cssfile',typ+'.css');
				}
				$('#contentpages').attr('data-tmpcss',typ);
				tpl.attr('data-vistatlp',typ);
			}).on('change','.menuposicion',function(ev){
				let pos=$(this).val();				
				tpl.removeClass(tpl.attr('data-oldmenuposicion')).addClass(pos).attr('data-oldmenuposicion',pos);			
			}).on('click','.chkcambiarcolor',function(ev){
				let input= $(this).siblings('input');
				if($(this).hasClass('fa-circle-o')){
		        	$('span',this).text(' Si');	
		        	input.addClass('sincolor');
		       		input.hide().trigger('change');
		        	$(this).removeClass('fa-circle-o').addClass('fa-check-circle');
		      	}else{
		        	$('span',this).text(' No');
		        	input.removeClass('sincolor');
		        	input.val(input.attr('data-color')).show();
		        	input.trigger('change');
		        	$(this).addClass('fa-circle-o').removeClass('fa-check-circle');
		      	}
		      	ev.stopPropagation();				
		    }).on('click','.subirimg',function(ev){
		    	let it=$(this).closest('.frmchangeimage').find('img');		    	
		    	__subirmediamd(it,'image/x-png, image/gif, image/jpeg, image/*','imagen',function(rs){		    		
		    		it.attr('data-oldmedia',rs.media);
		    		it.attr('src',rs.media);		    		
		    		if(it.hasClass('mainimagenfondo')){
		    			tpl.closest('body').css({'background-image':'url('+rs.media+')'});
						tpl.attr('data-imagenfondo',rs.media);
		    		}else{
		    			page.css({'background-image':'url('+rs.media+')'});
		    			page.attr('data-imagenfondo',rs.media);
		    		}
		    	});
		    }).on('click','.cerrarmodal',function(){
		    	md.modal('hide');
		    }).on('click','.borrarimagenfondo',function(ev){
		    	let it=$(this).closest('.frmchangeimage').find('img');
		    	it.attr('data-oldmedia',$(this).attr('src'));
		    	it.attr('src',imgdefault);
		    	if(it.hasClass('mainimagenfondo')){
	    			tpl.closest('body').css({'background-image':'none'});
					tpl.removeAttr('data-imagenfondo');
	    		}else{
	    			page.css({'background-image':'none'});
	    			page.removeAttr('data-imagenfondo');
	    		}
		    })
		})

		$('body').on('click','._btnversesion',function(ev){
			ev.preventDefault();
			$('audio').trigger('pause');
			$('video').trigger('pause');
			obj=$(this);
			obj.addClass('pagevisto')
			url=obj.attr('data-link')||'';
			donde=$(this).attr('data-donde')||'contentpages';
			iddetalle=$(this).attr('data-iddetalle');
			if(url==''){
				var data=__formdata('');
				data.append('plantilla','ninguno');
				data.append('idcurso',idcurso);
				data.append('idsesion',iddetalle);
				data.append('plt','blanco');				
				__sysajax({
					url:_sysUrlBase_+'/disenio/',
					fromdata:data,
					type:'html',
					donde:$('#bookplantilla'),		
					callback:function(rs){
						let tplold=$('#bookplantilla');						
						let cont=$('<div>'+rs+'</div>');
						let tpl=cont.find('#bookplantilla');
						let cssfile=tpl.attr('data-cssfile')||tplold.attr('data-cssfile')||'';
						let imgfondo=tpl.attr('data-imagenfondo')||tplold.attr('data-imagenfondo')||'';
						let textocolor=tpl.attr('data-colortexto')||tplold.attr('data-colortexto')||'';
						let fondocolor=tpl.attr('data-colorfondo')||tplold.attr('data-colorfondo')||'';

						if(cssfile!=''){
							$('#mainestilo').attr('href',$('#mainestilo').attr('data-tmpurl')+cssfile);
							tplold.attr('data-cssfile',cssfile);
						}
						if(imgfondo!=''){
							tplold.attr('data-imagenfondo',imgfondo)
							$('body').css({'background-image':'url('+imgfondo+')'});
						}else {
							tplold.removeAttr('data-imagenfondo')
							$('body').css({'background-image':'none'});
						}
						if(textocolor!=''){
							tplold.attr('data-colortexto',textocolor);
							$('body').css({'color':textocolor});
						}
						if(fondocolor!=''){
							tplold.attr('data-colorfondo',fondocolor)
							$('body').css({'background-color':fondocolor});
						}
						$('#bookplantilla').html(tpl.html());
					}
				});				
				return false;
			}
			__sysajax({
					url:_sysUrlMedia_+url,
					fromdata:{mostrarcargando:false},
					type:'html',
					donde:$('#bookplantilla'),		
					callback:function(rs){						
						let cont=$('<div>'+rs+'</div>');
						let tpl=cont.find('#bookplantilla');
						let cssfile=tpl.attr('data-cssfile')||'';
						let imgfondo=tpl.attr('data-imagenfondo')||'';
						let textocolor=tpl.attr('data-colortexto')||'';
						let fondocolor=tpl.attr('data-colorfondo')||'';
						if(cssfile!='')	$('#mainestilo').attr('href',$('#mainestilo').attr('data-tmpurl')+cssfile);
						if(imgfondo!='')$('body').css({'background-image':'url('+imgfondo+')'});
						else $('body').css({'background-image':'none'});
						if(textocolor!='')$('body').css({'color':textocolor});
						if(fondocolor!='')$('body').css({'background-color':fondocolor});
						$('#bookplantilla').html(tpl.html());
						//$('#bookplantilla').find('.menubooktop ul li:first a').trigger('click');
					}
				});
			ev.stopPropagation();
		})

		__cargarplantilla({
			url:'/disenio/',
			donde:'paneldisenio',
			data:{
				plantilla:'ninguno',
				idcurso:idcurso,
				idcursodetalle:iddetalle
				},
			fncall:function(rs){
				tpl=$('#bookplantilla');
				cssfile=tpl.attr('data-cssfile')||'';
				imgfondo=tpl.attr('data-imagenfondo')||'';
				textocolor=tpl.attr('data-colortexto')||'';
				fondocolor=tpl.attr('data-colorfondo')||'';
				if(cssfile!='') {
					$('#mainestilo').attr('href',$('#mainestilo').attr('data-tmpurl')+cssfile);
					$('body').css({'background-image':'url('+imgfondo+')'});
					$('body').css({'background-color':fondocolor});
					$('body').css({'color':textocolor});
				}
			}
		});
		$('body').click(function(){
			$('.toolbar1').removeClass('active');
		})
		
		$('.verpopover').popover({ trigger: 'hover',delay:100});
	});
</script>