<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		20-04-2019 
 * @copyright	Copyright (C) 20-04-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE);
class WebUbigeo extends JrWeb
{
	private $oNegUbigeo;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegUbigeo = new NegUbigeo;
				
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ubigeo', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["pais"])&&@$_REQUEST["pais"]!='')$filtros["pais"]=$_REQUEST["pais"];
			if(isset($_REQUEST["departamento"])&&@$_REQUEST["departamento"]!='')$filtros["departamento"]=$_REQUEST["departamento"];
			if(isset($_REQUEST["provincia"])&&@$_REQUEST["provincia"]!='')$filtros["provincia"]=$_REQUEST["provincia"];
			if(isset($_REQUEST["distrito"])&&@$_REQUEST["distrito"]!='')$filtros["distrito"]=$_REQUEST["distrito"];
			if(isset($_REQUEST["ciudad"])&&@$_REQUEST["ciudad"]!='')$filtros["ciudad"]=$_REQUEST["ciudad"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["solo"])&&@$_REQUEST["solo"]!='')$filtros["solo"]=$_REQUEST["solo"];
			$this->datos=$this->oNegUbigeo->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$Id_ubigeo)) {
				$this->oNegUbigeo->id_ubigeo = $Id_ubigeo;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        			$this->oNegUbigeo->pais=@$txtPais;
					$this->oNegUbigeo->departamento=@$txtDepartamento;
					$this->oNegUbigeo->provincia=@$txtProvincia;
					$this->oNegUbigeo->distrito=@$txtDistrito;
					$this->oNegUbigeo->ciudad=@$txtCiudad;
					
            if($accion=='_add') {
            	$res=$this->oNegUbigeo->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Ubigeo')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegUbigeo->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Ubigeo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegUbigeo->__set('id_ubigeo', $_REQUEST['id_ubigeo']);
			$res=$this->oNegUbigeo->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error',msj=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegUbigeo->setCampo($_REQUEST['id_ubigeo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>'Valor Actualizado'));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}