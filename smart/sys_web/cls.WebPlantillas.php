<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegZoom', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegForos', RUTA_BASE, 'sys_negocio');
require_once(RUTA_BASE . "json" . SD . "sys_web" . SD . "cls.WebVimeo.php");
class WebPlantillas extends JrWeb
{
	private $oNegZoom;
	private $oNegForos;
	private $oWebVimeo;
	public function __construct()
	{
		parent::__construct();
		$this->oNegZoom = new NegZoom;
		$this->oNegForos = new NegForos;
		$this->oWebVimeo = null;
	}

	public function defecto()
	{
		return $this->ver();
	}

	public function ver()
	{
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Smartcourse'), true);
			$this->esquema = 'inicio-plantillas';
			$this->documento->plantilla = !empty($_REQUEST["plt"]) ? $_REQUEST["plt"] : 'general';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function enlacesinteres()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/enlacesinteres';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function smartlibrery()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/smartlibrery';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function enlaceyoutube()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/enlaceyoutube';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function enlacevimeo()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/enlacevimeo';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function menuvaloracion()
	{
		try {
			global $aplicacion;
			$link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$link = explode("-", $link);

			$this->idalumno = $link[0];
			$this->idrol = $link[1];
			$this->idcurso = $link[2];
			$this->idgrupoaula = $link[3];
			$this->idcomplementario = $link[4];
			$this->idgrupoauladetalle = empty($link[5])?-1:$link[5];
			$this->esquema = 'plantillas/menuvaloracion';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function zoom()
	{
		try {
			global $aplicacion;
			$this->usuario = NegSesion::getUsuario();
			$link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$link = explode("-", $link);
			$arrIdgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalleArr"]) ? $_REQUEST["idgrupoauladetalleArr"] : array();
			$idcursodetalle = !empty($_REQUEST["idcursodetalle"]) ? $_REQUEST["idcursodetalle"] : 0;
			$idpestana = !empty($_REQUEST["idpestana"]) ? $_REQUEST["idpestana"] : 0;
			//fix de envio de datos
			if (!is_array($arrIdgrupoauladetalle) && !empty($arrIdgrupoauladetalle)) {
				$arrIdgrupoauladetalle = explode(',', $arrIdgrupoauladetalle);
			}
			/*-------------------------------------*/


			$this->idalumno = $link[0];
			$this->idrol = $link[1];
			$this->idcurso = $link[2];
			$this->idgrupoaula = $link[3];
			$this->idcomplementario = $link[4];
			$this->grupoauladetalles = $arrIdgrupoauladetalle;
			$this->idcursodetalle = $idcursodetalle;
			$this->idpestana = $idpestana;

			$filtros = array();

			if (!empty($this->idcursodetalle)) {
				$filtros['idcursodetalle'] = $this->idcursodetalle;
			}
			if (!empty($this->idcurso)) {
				$filtros['idcurso'] = $this->idcurso;
			}
			if (!empty($this->idcomplementario)) {
				$filtros['idcursodetalle'] = $this->idcomplementario;
			}
			if (!empty($this->idalumno)) {
				$filtros['idpersona'] = $this->idalumno;
			}
			if (!empty($this->idpestana)) {
				$filtros['idpestania'] = $this->idpestana;
			}
			$filtros['fecha'] = date('Y-m-d H:s:i');
			//Buscar si hay salas
			// var_dump($filtros);exit();
			$this->rooms = $this->oNegZoom->buscar($filtros);
			$this->room = !empty($this->rooms) ? $this->rooms[0] : array();
			if (count($this->rooms) > 1) {
				$this->room['fechas_salas'] = array($this->rooms[0]['fecha'], $this->rooms[1]['fecha']);
			}
			$this->esquema = 'plantillas/zoom';
			$this->documento->plantilla = 'blanco';

			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function foro()
	{
		try {
			global $aplicacion;
			$datos = !empty($_REQUEST["parametros"]) ? $_REQUEST["parametros"] : '';
			$usuarioAct = NegSesion::getUsuario();
			$confNotaMax = $datos['confNotaMax'];
			$confNotaMin = $datos['confNotaMin'];
			$tipoCalif = $datos['tipoCalif'];
			$filtros = array();
			//$filtros["idpublicacion"] = $datos['idPublicacionForo'] != '' ? intval($datos['idPublicacionForo']) : $datos['idPublicacionForo'];
			$filtros["idpublicacion"] = $datos['idPublicacionForo'];
			$filtros["idpersona"] = $usuarioAct['idpersona'];


			$this->data = $this->oNegForos->buscar($filtros)[0];
			$this->data["confNotaMax"] = $confNotaMax;
			$this->data["confNotaMin"] = $confNotaMin;
			$this->data["tipoCalif"] = $tipoCalif;

			$this->esquema = 'plantillas/foro';
			$this->documento->plantilla = 'blanco';
			// var_dump($_REQUEST);
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function enlacecolaborativo()
	{
		try {
			global $aplicacion;
			$link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$link = explode("-", $link);
			$this->idalumno = $link[0];
			$this->idrol = $link[1];
			$this->idcurso = $link[2];
			$this->idgrupoaula = $link[3];
			$this->idcomplementario = $link[4];
			$this->esquema = 'plantillas/enlacecolaborativo';
			// var_dump($_REQUEST);
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function pdf()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/pdf';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function flash()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/flash';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function html()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function SmartQuiz()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function smarticlock()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function smartenglish()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function smarticlookPractice()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function smarticDobyyourselft()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function game()
	{
		try {
			global $aplicacion;
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function video()
	{
		try {
			global $aplicacion;

			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->mydata = !empty($_REQUEST["mydata"]) ? $_REQUEST["mydata"] : null;
			$this->mytype = !empty($_REQUEST["mytype"]) ? $_REQUEST["mytype"] : null;
			$this->tipoVideo = "static";
			if ($this->mytype != '') {
				if ($this->mytype == "json") {
					$this->mydata = json_decode($this->mydata, true);
					if ($this->mydata["ubicacion"] == "vimeo") {
						$this->oWebVimeo = WebVimeo::getInstance();
						$oRs = $this->oWebVimeo->isAvailable([
							"uri" => $this->mydata["link"]
						], true);
						if ($oRs["ubicacion"] == "vimeo") {
							$this->tipoVideo = "vimeo";
						}
						$this->link = $oRs["link"];
					}
				}
			}
			$this->esquema = 'plantillas/video';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function audio()
	{
		try {
			global $aplicacion;
			$this->idtmp = !empty($_REQUEST["idtmp"]) ? $_REQUEST["idtmp"] : '';
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/audio';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function imagen()
	{
		try {
			global $aplicacion;
			//var_dump($_REQUEST);
			$this->idimagen = !empty($_REQUEST["idtmp"]) ? $_REQUEST["idtmp"] : '';
			$this->link = !empty($_REQUEST["link"]) ? $_REQUEST["link"] : '';
			$this->esquema = 'plantillas/imagen';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function indice()
	{
		try {
			global $aplicacion;
			$this->idtmp = !empty($_REQUEST["idtmp"]) ? $_REQUEST["idtmp"] : '';
			$this->esquema = 'plantillas/indice';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function texto()
	{
		try {
			global $aplicacion;
			$this->idtmp = !empty($_REQUEST["idtmp"]) ? $_REQUEST["idtmp"] : '';
			$this->esquema = 'plantillas/texto';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function widgets()
	{
		try {
			global $aplicacion;
			$this->idtmp = !empty($_REQUEST["idtmp"]) ? $_REQUEST["idtmp"] : '';
			$this->esquema = 'plantillas/widget';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function portada1()
	{
		try {
			global $aplicacion;
			//var_dump($_REQUEST);
			$this->idimagen = !empty($_REQUEST["idtmp"]) ? $_REQUEST["idtmp"] : '';
			$this->esquema = 'plantillas/portada1';
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function foroywiki(){
		global $aplicacion;
		try{
			//$this->documento->stylesheet('buttons.dataTables.min', '/libs/tinymce.10/extensions/Buttons/css/');
			$tiporecurso=!empty($_REQUEST["tiporecurso"])?$_REQUEST["tiporecurso"]:'foro';
			$idrecurso=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:'0';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$idcursoprincipal=!empty($_REQUEST["idcursoprincipal"])?$_REQUEST["idcursoprincipal"]:'';
			$idgrupoauladetalle=!empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:'';
			$idcc=!empty($_REQUEST["idcc"])?$_REQUEST["idcc"]:'';
			$idpestania=!empty($_REQUEST["idpestania"])?$_REQUEST["idpestania"]:'';
			$idcursodetalle=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:'';
			$tipo=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:1;
			if($tipo==2){$idcurso=$idcursoprincipal;}
			$this->idgrupoauladetalle=$idgrupoauladetalle;


			$this->refresh='?tiporecurso='.$tiporecurso.'&idrecurso='.$idrecurso.'&idcurso='.$idcurso.'&idcursoprincipal='.$idcursoprincipal.'&idgrupoauladetalle='.$idgrupoauladetalle.'&idcc='.$idcc.'&idpestania='.$idpestania.'&idcursodetalle='.$idcursodetalle;


			JrCargador::clase('sys_negocio::NegRecursos_colaborativos', RUTA_BASE, 'sys_negocio');			
			$this->user=NegSesion::getUsuario();
			$this->empresa=NegSesion::getEmpresa();
			$idrol=$this->user["idrol"];

			$oRecursos= new NegRecursos_colaborativos;
			$this->recurso=$oRecursos->buscar(array('idrecurso'=>$idrecurso,'idproyecto'=>$this->user["idproyecto"],'sqlget'=>true,'sqlconuser'=>true));			

			JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
			$this->oNegCursoDetalle = new NegAcad_cursodetalle;

			$formulacurso=$this->oNegCursoDetalle->geformula($idcurso, $idcc);
			$this->formulaNota=array();

			if(!empty($formulacurso)){
                if($formulacurso["tipocalificacion"]!='A'){ //no aplica formula para alfanumerico                    
                    $this->formulaNota['calificacion_min']=$formulacurso["mincal"];
                    $this->formulaNota['calificacion_total']=$formulacurso["maxcal"];                    
                }else{
                    $maxformulacurso=0;
                    if(!empty($formulacurso['puntuacion']))
                        foreach ($formulacurso['puntuacion'] as $k => $v) {                           
                            if($maxformulacurso<$v["maxcal"]) $maxformulacurso=$v["maxcal"];
                        }
                    else{
                        $maxformulacurso=$formulacurso["maxcal"];                        
                    }
                    $this->formulaNota['calificacion_total']=$maxformulacurso;
                }
            }else{
            	$this->formulaNota['calificacion_total']=20;
            }

     
			//var_dump($this->recurso);			
			if($idrol==3){
				$this->esquema = 'plantillas/forosywikis/'.$tiporecurso."_alumno";
				//leer grupo de alumnos
				JrCargador::clase('sys_negocio::NegRecursos_colaborativosasignacion', RUTA_BASE, 'sys_negocio');
				$oRecursosalumno = new NegRecursos_colaborativosasignacion;	
				//var_dump(array('idrecurso'=>$idrecurso,'idpestania'=>!empty($idpestania)?$idpestania:'null','idgrupoauladetalle'=>$idgrupoauladetalle,'idcursodetalle'=>$idcursodetalle,'sqlget'=>true,'idalumno'=>$this->user["idpersona"]));
				$this->haygrupo=$oRecursosalumno->buscar(array('idrecurso'=>$idrecurso,'idpestania'=>!empty($idpestania)?$idpestania:'null','idgrupoauladetalle'=>$idgrupoauladetalle,'idcursodetalle'=>$idcursodetalle,'sqlget'=>true,'idalumno'=>$this->user["idpersona"]));
				$this->tema='';
				if(!empty($this->haygrupo)){
					$this->idrecursoasignacion=$this->haygrupo['idrecursoasignacion'];
					$this->respuestas=$this->haygrupo["respuestas"];
					$this->fecha_respuesta=$this->haygrupo["respuestas"];
					if(!empty($this->haygrupo["tema"])){ //si hay tema
						$this->tema=$this->haygrupo["tema"];
					}
				}else{
					$oRecursosalumno->idrecurso=@$idrecurso;
		  			$oRecursosalumno->idalumno=$this->user["idpersona"];
		  			$oRecursosalumno->grupo= $tiporecurso=='foro'?1:date('YmdHis');
		  			$oRecursosalumno->idgrupoauladetalle=@$idgrupoauladetalle;
		  			$oRecursosalumno->nota=0;
		  			$oRecursosalumno->nota_base=0;
		  			$oRecursosalumno->iddocente='null';
		  			$oRecursosalumno->fecha_calificacion='null';
		  			$oRecursosalumno->titulo='null';
		  			$oRecursosalumno->respuestas='null';
		  			$oRecursosalumno->fecha_respuesta='null';
		  			$oRecursosalumno->idcursodetalle=@$idcursodetalle;
		  			$oRecursosalumno->idpestania=@$idpestania;
		  			//$oRecursosalumno->fecha_creacion=date('Y-m-d');
		  			$this->idrecursoasignacion=$oRecursosalumno->agregar();
		  			//var_dump($this->idrecursoasignacion);
				}
				$cond=array('idrecurso'=>$idrecurso,'idpestania'=>!empty($idpestania)?$idpestania:'null','idgrupoauladetalle'=>$idgrupoauladetalle,'idcursodetalle'=>$idcursodetalle,'grupo'=>@$this->haygrupo["grupo"],'orderby'=>' fecha_respuesta ASC ');
				//var_dump($cond);
				if($tiporecurso=='foro'){
					//if(empty($this->haygrupo["nota"])) $cond['noidalumno']=$this->user["idpersona"];
					//var_dump(expression)
					$this->forosamigos=$oRecursosalumno->buscar($cond);
					//var_dump($this->forosamigos);
				}else{//es wiki
					$cond=array('idrecurso'=>$idrecurso,'idpestania'=>$idpestania,'idgrupoauladetalle'=>$idgrupoauladetalle,'idcursodetalle'=>$idcursodetalle,'grupo'=>@$this->haygrupo["grupo"],'orderby'=>' fecha_respuesta ASC ');
					$this->wikiamigos=$oRecursosalumno->buscar($cond); //solo es una misma wiki por todos los del grupo.
					//var_dump($this->wikiamigos);
				}				
			}elseif($idrol==2){

				JrCargador::clase('sys_negocio::NegRecursos_colaborativosasignacion', RUTA_BASE, 'sys_negocio');
				$oRecursosalumno = new NegRecursos_colaborativosasignacion;	
				$this->respuestas=$oRecursosalumno->buscar(array('idrecurso'=>$idrecurso,'idpestania'=>!empty($idpestania)?$idpestania:'null','idgrupoauladetalle'=>$idgrupoauladetalle,'idcursodetalle'=>$idcursodetalle));
				

				$rpt=array();
				$thispk=0;
				if(!empty($this->respuestas)){
					foreach ($this->respuestas as $key => $value) {					
						if(empty($rpt[$value["grupo"]])){
							$thispk=$value["grupo"];
							$rpt[$value["grupo"]]=array('tema'=>$value["tema"],'respuestas'=>[]);
						}
						$rpt[$value["grupo"]]['respuestas'][]=$value;
					}
				}
				$this->ngrupos=count($rpt);
				$this->respuestas=$rpt;
				//if($this->ngrupos==1) $this->respuestas=$rpt[$thispk];
				
				/*JrCargador::clase('sys_negocio::NegRecursos_colaborativosasignacion', RUTA_BASE, 'sys_negocio');
				$oRecursosalumno = new NegRecursos_colaborativosasignacion;	
				$this->haygrupo=$oRecursosalumno->buscar(array('idrecurso'=>$idrecurso,'pestania'=>$idpestania,'idgrupoauladetalle'=>$idgrupoauladetalle,'idcursodetalle'=>$idcursodetalle,'sqlget'=>true,'idalumno'=>$this->user["idpersona"]));*/


				$this->esquema = 'plantillas/forosywikis/'.$tiporecurso."_docente";
			}else{
				/*JrCargador::clase('sys_negocio::NegRecursos_colaborativosasignacion', RUTA_BASE, 'sys_negocio');
				$oRecursosalumno = new NegRecursos_colaborativosasignacion;	
				$this->haygrupo=$oRecursosalumno->buscar(array('idrecurso'=>$idrecurso,'pestania'=>$idpestania,'idgrupoauladetalle'=>$idgrupoauladetalle,'idcursodetalle'=>$idcursodetalle,'sqlget'=>true));*/

				$this->esquema = 'plantillas/forosywikis/'.$tiporecurso;
			}
			//var_dump($this->recurso);
			//var_dump($this->uempresa);
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		}catch(Exception $ex){
			return $aplicacion->error(JrTexto::_($ex->getMessage()));
		}
	}


	public function descargacontrolada(){
		try {
			global $aplicacion;
			$infodatosdescarga=array();
	
			if(!empty($_REQUEST["link"])){
				$link=RUTA_BASE.'static'.SD.'media'.SD.$_REQUEST["link"];
				if(file_exists($link)){
					@chmod($link, 0777);
					include_once($link);
				}
			}
			$this->recurso=$infodatosdescarga;
			$this->recurso["idcurso"]=@$_REQUEST["idcurso"];
			$this->recurso["idcc"]=@$_REQUEST["idcc"];
			$this->recurso["strcurso"]=@$_REQUEST["nombrecurso"];
			$this->recurso["idpestania"]=@$_REQUEST["idpestania"];
			$this->recurso["idgrupoauladetalle"]=@$_REQUEST["idgrupoauladetalle"];
			$this->recurso["idcursodetalle"]=@$_REQUEST["idcursodetalle"];
			$this->recurso["tipomatricula"]=@$_REQUEST["tipomatricula"];
			$this->esquema = 'plantillas/descargarcontrolada';
			$this->documento->plantilla = 'blanco';		

			$this->user=NegSesion::getUsuario();
			$this->recurso["idempresa"]=$this->user["idempresa"];
			$this->recurso["idproyecto"]=$this->user["idproyecto"];
			if($this->user["idrol"]==3 && $this->recurso["tipomatricula"]==3){
				JrCargador::clase('sys_negocio::NegDescargas_asignadas', RUTA_BASE);
				$oNegDescargas_asignadas = new NegDescargas_asignadas;
				$filtros=array();
				$filtros["idpersona"]=$this->user["idpersona"];
				$filtros["idempresa"]=$this->user["idempresa"];
				$filtros["idproyecto"]=$this->user["idproyecto"];
				$filtros["contarconsumidas"]=1;
				$filtros["sqlget"]=1;


				$alldescargas=$oNegDescargas_asignadas->buscar($filtros);				
				if(!empty($alldescargas)){
					$this->recurso["cantidad_descarga"]=$alldescargas["total_descargas"];
					$this->recurso["candidad_descargados"]=$alldescargas["total_consumidas"];
				}else{
					$this->recurso["cantidad_descarga"]=0;
					$this->recurso["candidad_descargados"]=0;	
				}
				//var_dump($this->user);
			}else{
				$this->recurso["cantidad_descarga"]=1;
				$this->recurso["candidad_descargados"]=0;
			}


			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}

	}
}
