<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_sesionhtml', RUTA_BASE, 'sys_negocio');
class WebDisenio extends JrWeb
{
	private $oNegCurso;

	public function __construct()
	{
		parent::__construct();		
        $this->oNegCurso = new NegAcad_curso;
        $this->oNegCursodetalle = new NegAcad_cursodetalle;
		$this->oNegSesionhtml = new NegAcad_sesionhtml;
	}

	public function defecto(){
		return $this->ver();
	}

	public function ver(){
		try{
			global $aplicacion;			
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$idcursodet=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:'';
			$this->curso=$this->cursoDet=$this->sesion='';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){
					$this->curso=$curso[0];
					$cursodet=$this->oNegCursodetalle->buscar(array('idcurso'=>$idcurso,'idcursodetalle'=>$idcursodet));
					if(!empty($cursodet[0])){
						$this->cursoDet=$cursodet[0];
						$sesion=$this->oNegSesionhtml->buscar(array('idcurso'=>$idcurso,'idcursodetalle'=>$idcursodet));
						if(!empty($sesion[0]))
							$this->sesion=$sesion[0];
					}
				}
			}
			//var_dump($this->curso);
			$this->documento->setTitulo(JrTexto::_('Diseño'),true);
			$this->esquema = 'disenio';			
			$this->documento->plantilla =!empty($_REQUEST["plt"])?$_REQUEST["plt"]:'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}	
}