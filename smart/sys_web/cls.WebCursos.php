 <?php

	/**
	 * @autor		Generador Abel Chingo Tello, ACHT
	 * @fecha		21-05-2017 
	 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
	 */
	defined('RUTA_BASE') or die();
	/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');*/
	JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE, 'sys_negocio');
	JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
	JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
	JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
	JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
	JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
	JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
	JrCargador::clase('sys_negocio::NegSence_usuario', RUTA_BASE, 'sys_negocio');
	JrCargador::clase('sys_negocio::NegSence_cursovalor', RUTA_BASE, 'sys_negocio');
	JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
	JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');

	class WebCursos extends JrWeb
	{
		private $oNegCurso;
		private $oNegBolsa_empresas;
		private $oNegSence_usuario;
		private $oNegSence_cursovalor;
		private $oNegPersonal;

		public function __construct()
		{
			parent::__construct();
			$this->oNegAcad_categorias = new NegAcad_categorias;
			$this->oNegCursodetalle = new NegAcad_cursodetalle;
			$this->oNegCurso = new NegAcad_curso;
			$this->oNegProyecto = new NegProyecto;
			$this->oNegBolsa_empresas = new NegBolsa_empresas;
			$this->oNegSence_usuario = new NegSence_usuario;
			$this->oNegSence_cursovalor = new NegSence_cursovalor;
			$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
			$this->oNegPersonal = new NegPersonal;
		}

		public function agregarhijos($m)
		{
			try {
				$ht = '';
				$mh = !empty($m["hijos"]) ? true : false;
				if ($mh == true) {
					$hth = '';
					foreach ($m["hijos"] as $mj) {
						$hth .= $this->agregarhijos($mj);
					}
				}
				$mht = !empty($m["html"]) ? 'data-link="' . $m["html"] . '"' : '';
				$ht = '<li class="' . ($mh ? 'hijos' : '') . '" id="me_' . $m["idcursodetalle"] . '" data-idrecurso="' . $m["idrecurso"] . '" data-idcursodetalle="' . $m["idcursodetalle"] . '" data-idpadre="' . $m["idpadre"] . '" data-orden="' . $m["orden"] . '" data-tipo="' . $m["tiporecurso"] . '" ' . $mht . ' data-imagen="' . $m["imagen"] . '" data-esfinal="' . $m["esfinal"] . '" >
				<div>
					<span class="mnombre">' . $m["nombre"] . '</span>
					<span class="macciones">
						<i class="mmsubmenu fa fa-sort-down verpopoverm" data-content="Ocultar Submenus" ></i>
						<i class="mmsubmenu fa fa-sort-up verpopoverm" data-content="Mostrar Submenus" ></i>
						<i class="addcontent fa fa-eye verpopoverm" data-content="Ver contenido" data-placement="top"></i>
						<i class="magregar fa fa-plus verpopoverm" data-content="Agregar Submenu" data-placement="top"></i>
						<i class="meditar fa fa-pencil verpopoverm" data-content="Editar Menu" data-placement="top"></i>							
						<i class="meliminar fa fa-trash verpopoverm" data-content="Eliminar" data-placement="top"></i>
						<i class="mmove fa fa-arrows-alt verpopoverm" data-content="Mover Menu" data-placement="right"></i>
						<!--i class="mcopylink fa fa-link verpopoverm" data-content="Mover Menu" data-placement="right"></i-->
						<!--span style="display:inline-block">								
							<i class="mmoveup fa fa-angle-double-up verpopoverm" data-content="Mover Arriba"></i>
							<i class="mmovedown fa fa-angle-double-down verpopoverm" data-content="Mover abajo"></i>
						</span-->
					</span>
				</div>
				' . ($mh ? ('<ul id="me_hijo' . $m["idcursodetalle"] . '">' . $hth . '</ul>') : '') . '
			</li>';
				return $ht;
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

	public function defecto(){
		try {
			global $aplicacion;
			$this->esquema = 'cursos/inicio';
			if (JrTools::ismobile()) {
				$this->documento->plantilla = 'mobile/dashboard';
				//$this->esquema = 'mobile/principal';
				 echo "is mobile success";
			}else{
				$this->documento->plantilla = 'general';
			}

				$this->usuActivo = NegSesion::getUsuario();
				$rolActual = $this->usuActivo["idrol"];
				$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
				$this->usuarionombre = $this->usuActivo["nombre_full"];
				$curso = array('idcurso' => 0);
				$categoriasdelcurso = array();
				$temas = array();
				if (!empty($idcurso)) {
					$curso = $this->oNegCurso->buscar(array('idcurso' => $idcurso));
					if (!empty($curso[0])) {
						$curso = $curso[0];
						$categoriasdelcurso = $this->oNegCurso->buscarxcategoria(array('idcurso' => $curso["idcurso"], 'solocategoria' => true));
						$temas = $this->oNegCursodetalle->buscarconnivel(array('idcurso' => $curso["idcurso"]));
					}
				}
				if (JrTools::ismobile()) {
					$this->documento->plantilla = 'mobile/dashboard';
					//$this->esquema = 'mobile/login';
					// echo "is mobile success";
				}
			

			$this->curso = json_encode(array('datos' => $curso, 'categorias' => $categoriasdelcurso, 'temas' => $temas));
			//$this->categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));			
			//$this->idcurso=$idcurso;
			
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
		private function sence_login($curso)
		{
			try {
				$rolActual = $this->usuActivo["idrol"];
				if ($rolActual ==  3) {
					if (!empty($this->empresa["RutOtec"]) && !empty($this->empresa["Token"])) {
						//buscar rut de personal, si esta vacio, agarrar el dni
						$rut_usuario = @$this->usuActivo["dni"];
						$rs_personal = $this->oNegPersonal->buscar(['sqlsolopersona' => true, 'idpersona' => $this->usuActivo["idpersona"]]);
						if (!empty($rs_personal)) {
							$ruttmp = end($rs_personal)['rut'];
							$rut_usuario = !empty($ruttmp) ? $ruttmp : $rut_usuario;
						}
						$this->rutaSense = "https://sistemas.sence.cl/rce" . (SENCE_TEST ? "test" : "") . "/Registro/";
						$this->RutOtec = @$this->empresa["RutOtec"];
						$this->Token = @$this->empresa["Token"];
						$this->LineaCapacitacion = @$this->empresa["LineaCapacitacion"];
						$this->CodSense = !empty($curso['CodSense']) ? $curso['CodSense'] : (SENCE_TEST ? '-1' : '');
						$this->CodigoCurso = !empty($curso["CodigoCurso"]) ? $curso["CodigoCurso"] : (SENCE_TEST ? '-1' : '');
						$this->RunAlumno = SENCE_TEST ? "10626548-8" : @$rut_usuario;
						$this->IdSesionAlumno = @$this->usuActivo["usuario"];

						$sence = @NegSesion::get("sence");
						$this->sence = @$sence[$this->CodigoCurso];

						$idsence_usuario = @$this->sence['idsence_usuario'];
						if (empty($this->sence)) {
							//insertar nuevo id
							$rs = $this->oNegSence_usuario->insert([
								[
									'idpersona' => $this->usuActivo["idpersona"], 'idempresa' => $this->usuActivo["idempresa"], 'idproyecto' => $this->usuActivo["idproyecto"], 'idrol' => $this->usuActivo["idrol"], 'idcurso' => $curso['idcurso'], 'codigocurso' => $this->CodigoCurso, 'codusuario' => ($this->usuActivo["usuario"] . $this->usuActivo["clave"] . $this->usuActivo["idpersona"]), 'codigosence' => $this->CodSense, 'tipo' => 1, 'url' => $_SERVER['REQUEST_URI'], 'url_cierre' => "/" . CARPETA_RAIZ . "smart/cursos/sence_cerrar", 'fecha_creacion' => date('Y-m-d H:i:s'), 'fecha_actualizacion' => date('Y-m-d H:i:s')
								]
							]);
							if (empty($rs)) {
								throw new Exception("Algo ocurrio al aperturar el registro en base de datos de la sesión de sence");
							}
							$idsence_usuario = $rs;
						}

						if (empty($idsence_usuario)) {
							throw new Exception("El identificador de la base de datos no esta definido");
						}

						$this->urlActual = URL_BASE . "sesion/sence/?i={$idsence_usuario}&t=1";
						$this->urlCerrar = URL_BASE . "sesion/sence/?i={$idsence_usuario}&t=2";

						if (empty($this->sence) || (isset($this->sence["GlosaError"]) && (int)$this->sence["GlosaError"] != 210)) {
							$this->documento->setTitulo("Sesión Sense", false);
							$this->documento->plantilla = 'login';
							$this->esquema = 'sence/login';
							return true;
						}
						return false;
					}
				}
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
		}

		public function sence_cerrar()
		{
			try {
				global $aplicacion;
				// NegSesion::set("sence", $sence);
				$this->mensaje = !empty($_REQUEST['msj']) ? true : false;
				$this->Haysence = false;
				if ($this->mensaje == true && !empty($_REQUEST['id'])) {
					$idsence_usuario = $_REQUEST['id'];
					$rs = $this->oNegSence_usuario->busquedacompleta(['id' => $idsence_usuario]);
					if (!empty($rs)) {
						$row = end($rs);
						$this->rutaSense = "https://sistemas.sence.cl/rce" . (SENCE_TEST ? "test" : "") . "/Registro/";
						$this->RutOtec = $row["RutOtec"];
						$this->Token = $row["Token"];
						$this->LineaCapacitacion = $row["LineaCapacitacion"];
						$this->CodSense = $row["CodSence"];
						$this->CodigoCurso = $row["CodigoCurso"];
						$this->RunAlumno = SENCE_TEST ? "10626548-8" : $row["RunAlumno"];
						$this->IdSesionAlumno = $row['IdSesionAlumno'];
						$this->IdSesionSence = $row['IdSesionSence'];
						$this->urlCerrar = URL_BASE . "sesion/sence/?i={$idsence_usuario}&t=2";

						$this->Haysence = true;
					}
				}
				$this->documento->setTitulo("Sesión Sense", false);
				$this->documento->plantilla = 'login';
				$this->esquema = 'sence/cerrar';
				return parent::getEsquema();
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

	public function ver()
	{
		try {
			global $aplicacion;
			// if(!empty($_POST)){
			// 	var_dump($_POST);exit();
			// }
			// $this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			// $this->documento->stylesheet('soft-rw-theme-paris', '/libs/othersLibs/mine/css/');
			// $this->documento->script('mine', '/libs/othersLibs/mine/js/');
			// $this->documento->stylesheet('conferencias', '/libs/othersLibs/conferencias/');
			// $this->documento->script('conferencias', '/libs/othersLibs/conferencias/');
			// $this->documento->script('fecha-limite-tapr', '/libs/othersLibs/fecha-limite-tapr/');
			$this->sence = null;
			$this->esquema = 'cursos/inicio-ver';
			$this->documento->plantilla = 'general';
			$this->usuActivo = NegSesion::getUsuario();
			$this->empresa = $this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuActivo["idempresa"]))[0];
			$this->logo_emp = $this->empresa["logo"];
				
				
				
				$this->documento->plantilla = 'general';
				$this->esquema = 'cursos/inicio-ver';
				
				// $plataforma = !empty($_REQUEST["pl_"]) ? $_REQUEST["pl_"] : '';
				// if ($plataforma === "ai") {
				// 	NegSesion::sesionanonimo();
				// 	$this->usuActivo = NegSesion::getUsuario();
				// } else {
				//$this->usuActivo = NegSesion::getUsuario();
				// }
				//$this->empresa = $this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuActivo["idempresa"]))[0];
				//$this->logo_emp = $this->empresa["logo"];

				$rolActual = $this->usuActivo["idrol"];
				$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';

				$this->idcomplementario = !empty($_REQUEST["icc"]) ? $_REQUEST["icc"] : 0;
				if ($this->idcomplementario == 'undefined') $this->idcomplementario = 0;
				$iduser = !empty($_REQUEST["iu_"]) ? $_REQUEST["iu_"] : '';
				$this->usuarionombre = $this->usuActivo["nombre_full"];
				$curso = array('idcurso' => 0);
				$categoriasdelcurso = array();
				$temas = array();
				$this->tieneDocente = true;
				$this->pagoRecurso = true;
				$_codsence = -1;
				$_codaccionsence = -1;

				//$this->idDocente=0;
				if (!empty($idcurso)) {
					if (!empty($this->idcomplementario)) {
						$curso = $this->oNegCurso->buscar(array("complementario" => true, "idcomplementario" => $this->idcomplementario, 'idcurso' => $idcurso));
					} else {
						$curso = $this->oNegCurso->buscar(array('idcurso' => $idcurso));
					}

					if (!empty($curso[0])) {
						$curso = $curso[0];
						// var_dump($curso);exit();
						if (!empty($this->idcomplementario)) {
							$categoriasdelcurso = $this->oNegCurso->buscarxcategoria(array('idcurso' => $curso["idcursoprincipal"], 'solocategoria' => true));
						} else {
							$categoriasdelcurso = $this->oNegCurso->buscarxcategoria(array('idcurso' => $curso["idcurso"], 'solocategoria' => true));
						}
						if (!empty($this->idcomplementario)) {
							$temas = $this->oNegCursodetalle->buscarconnivel(array('idcurso' => $curso["idcurso"], "complementario" => true));
						} else {
							$temas = $this->oNegCursodetalle->buscarconnivel(array('idcurso' => $curso["idcurso"]));
						}
						$this->idgrupoaula = 0;
						$this->idgrupoauladetalleArr = array();
						$matricula = new NegAcad_matricula;
						if ($rolActual == 3) {							
							if (JrTools::ismobile()) {								
								$this->documento->plantilla = 'mobile/general';
								$this->esquema = 'cursos/mobile-inicio-ver';
							}

							$this->proyecto = $this->oNegProyecto->buscar(array('idproyecto' => $this->usuActivo["idproyecto"]));
							if (!empty($this->proyecto[0])) {
								$this->proyecto = $this->proyecto[0];
							}
							$filtroscurso = array('idcurso' => $idcurso, "idcomplementario" => $this->idcomplementario, 'idalumno' => $this->usuActivo["idpersona"], 'idproyecto' => $this->usuActivo["idproyecto"]);
							$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : 0;
							if (!empty($idgrupoauladetalle)) {
								if ($idgrupoauladetalle > 0) {
									$filtroscurso["idgrupoauladetalle"] = $idgrupoauladetalle;
									if (!empty($_REQUEST["idmatricula"])) $filtroscurso["idmatricula"] = $_REQUEST["idmatricula"];
									$haymatricula = $matricula->cursos($filtroscurso);
									if (!empty($haymatricula["miscursos"])) {
										$haymatricula = $haymatricula["miscursos"];
										$_codsence = $haymatricula[0]['codsence'];
										$_codaccionsence = $haymatricula[0]['codigocursosence']; // codigo por alumno
										$fecha = date("Y-m-d", strtotime($haymatricula[0]["fecha_vencimiento"]));
										$fechaActual = date('Y-m-d');
										$this->matricula = $haymatricula[0];
										$this->notificarvencimiento = false;
										if ($haymatricula[0]["estado"] != 1 || $fecha < $fechaActual) {
											$this->info = array('code' => 200, 'msj' => 'Su matricula  ya no esta activa');
											$this->esquema = 'error/curso';
										} else {
											$fecha = date("Y-m-d", strtotime($fecha . "- 3 days"));
											if ($fecha <= $fechaActual && !empty($this->matricula["notificar"])) {
												$this->notificarvencimiento = true;
											}
										}
										$curso["CodigoCurso"] = $_codaccionsence; // codigo por alumno
										$curso["CodSense"] = $_codsence;


										if (!empty($_codsence) && $this->sence_login($curso)) {
											return parent::getEsquema();
										}
										// var_dump($this->sence);exit();
										$this->idgrupoaula = $haymatricula[0]["idgrupoaula"];
										$this->idgrupoauladetalle = $haymatricula[0]["idgrupoauladetalle"];
										$this->tieneDocente = !empty($haymatricula[0]["iddocente"]) ? true : false;
										$this->idDocente = !empty($haymatricula[0]["iddocente"]) ? $haymatricula[0]["iddocente"] : false;
										$this->pagoRecurso = !empty($haymatricula[0]["pagorecurso"]) ? true : false;
										$this->idgrupoauladetalleArr = array_values(array_column($haymatricula, 'idgrupoauladetalle'));
									} else {
										$this->info = array('code' => 201, 'msj' => 'Matricula al curso no existe');
										$this->esquema = 'error/curso';
									}
								}
							} else {
								//Alumno de Mela no jodas no podras hackearnos --stas cojudo ....
								$this->info = array('code' => 201, 'msj' => 'Matricula al curso no existe');
								$this->esquema = 'error/404';
							}

							//var_dump($this->documento->plantilla);
							//var_dump($this->esquema);
						} else if ($rolActual == 2) {
							$idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"]) ? $_REQUEST["idgrupoauladetalle"] : 0;
							if (!empty($idgrupoauladetalle)) {
								$filtros = array('idcurso' => $idcurso, "idcomplementario" => $this->idcomplementario, 'iddocente' => $this->usuActivo["idpersona"], 'idproyecto' => $this->usuActivo["idproyecto"], 'idgrupoauladetalle' => $idgrupoauladetalle);
								$haymatricula = $matricula->buscar($filtros);
								if (!empty($haymatricula[0]["idsence_assigncursos"])) {
									$this->idsence_assigncursos = true;
								}
								$datos_grupoauladetalle = $this->oNegAcad_grupoauladetalle->buscar(array('idgrupoauladetalle' => $idgrupoauladetalle, 'iddocente' => $this->usuActivo["idpersona"]));
								if (!empty($datos_grupoauladetalle[0])) {
									$det = $datos_grupoauladetalle[0];
									$fechaActual = date('Y-m-d');
									$fecha = date("Y-m-d", strtotime($det["fecha_final"]));
									$ndiasextras = !empty($det["dias_extradocente"]) ? $det["dias_extradocente"] : 30;
									$fecha_condias_extra = date("Y-m-d", strtotime($fecha . "+ " . $ndiasextras . " days"));
									//echo "fecha actual : ".$fechaActual."<br> Fecha vencimiento con dias extra : ".$fecha_condias_extra."<br><hr><br>";
									if ($fecha_condias_extra < $fechaActual) {
										$this->info = array('code' => 201, 'msj' => 'Usted ya no tiene acceso a este curso su fecha de acceso ya vencio');
										$this->msj = $this->info['msj'];
										$this->esquema = 'error/general';
									}
								} else {
									$this->info = array('code' => 201, 'msj' => 'Usted no tiene Asignado este curso');
									$this->msj = $this->info['msj'];
									$this->esquema = 'error/general';
								}
							} else {
								$this->info = array('code' => 201, 'msj' => 'Usted no tiene Asignado este curso');
								$this->msj = $this->info['msj'];
								$this->esquema = 'error/general';
							}
							$this->idgrupoauladetalle = $idgrupoauladetalle;
							$this->idgrupoauladetalleArr = array_values(array_column($haymatricula, 'idgrupoauladetalle'));
							$this->idgrupoaula = @$curso["idgrupoaula"];
							if (empty($this->idgrupoaula) && !empty($haymatricula[0])) {
								$this->idgrupoaula = $haymatricula[0]["idgrupoaula"];
							}
							if (empty($this->idgrupoaula) && !empty($idgrupoauladetalle)) {
								$grupoauladetalle = $this->oNegAcad_grupoauladetalle->buscar(array('idgrupoauladetalle' => $idgrupoauladetalle, 'sql4' => true, 'orderby' => 'idgrupoauladetalle'));
								$this->idgrupoaula = $grupoauladetalle[0]["idgrupoaula"];
							}
						}
						$this->curso_ = $curso;
						// var_dump($this->curso_);exit();
					} else return $aplicacion->redir('../');
				} else return $aplicacion->redir('../');
				if (empty($categoriasdelcurso)) {
					// $categoriasdelcurso = array();
					$categoriasdelcurso[] = array("idcategoria" => 0);
				}
				$this->curso = json_encode(array('datos' => $curso, 'categorias' => $categoriasdelcurso, 'temas' => $temas));
				// var_dump($categoriasdelcurso);exit();
				// $this->curso_ = $curso;
				$this->returnlink = !empty($_REQUEST["returnlink"]) ? $_REQUEST["returnlink"] : $this->documento->getUrlBase();
				$this->rolActual = $rolActual;
				//$this->categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));
				// var_dump(empty($this->sence));exit();
				$this->idcurso = $idcurso;
				return parent::getEsquema();
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

		public function paso1()
		{
			try {
				global $aplicacion;
				$this->esquema = 'cursos/1datosdelcurso';
				$this->documento->plantilla = 'blanco';
				return parent::getEsquema();
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

		public function paso1categorias()
		{
			try {
				global $aplicacion;
				$this->esquema = 'cursos/2categorias';
				$this->documento->plantilla = 'blanco';
				$this->usuActivo = NegSesion::getUsuario();
				$categorias = $this->oNegAcad_categorias->buscar(array('idpadre' => 0, 'idproyecto' => $this->usuActivo["idproyecto"]));
				$adcategoria = array();
				if (!empty($categorias))
					foreach ($categorias as $cat) {
						$cat['hijos'] = $this->oNegAcad_categorias->buscar(array('idpadre' => $cat["idcategoria"], 'idproyecto' => $this->usuActivo["idproyecto"]));
						array_push($adcategoria, $cat);
					}
				$this->categorias = $adcategoria;
				$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : 0;
				$this->categoriasdelcurso = array();
				if (!empty($idcurso)) {
					$this->categoriasdelcurso = $this->oNegCurso->buscarxcategoria(array('idcurso' => $idcurso, 'solocategoria' => true));
				}
				$returnjson = !empty($_REQUEST["rjson"]) ? $_REQUEST["rjson"] : false;
				if ($returnjson == 'si') {
					echo json_encode(array('code' => 200, 'categorias' => $this->categorias, 'categoriascurso' => $this->categoriasdelcurso));
					exit(0);
				}
				return parent::getEsquema();
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

		public function paso1temas()
		{
			try {
				global $aplicacion;
				$this->esquema = 'cursos/3temas';
				$this->documento->plantilla = 'blanco';
				/*$categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));
			$adcategoria=array();
			if(!empty($categorias))
				foreach($categorias as $cat){
					$cat['hijos']=$this->oNegAcad_categorias->buscar(array('idpadre'=>$cat["idcategoria"]));					
					array_push($adcategoria,$cat);
				}*/
				//$this->categorias=$adcategoria;
				return parent::getEsquema();
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

		public function paso1addtemas()
		{
			try {
				global $aplicacion;
				$this->esquema = 'cursos/3temas-frm';
				$this->documento->plantilla = 'blanco';
				return parent::getEsquema();
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

		/*funciones json*/
		public function jsontemas()
		{
			try {
				global $aplicacion;
				$this->documento->plantilla = 'blanco';
				$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : 0;
				$filtros = array();
				$filtros["idcurso"] = $idcurso;
				$temas = $this->oNegCursodetalle->buscarconnivel($filtros);
				echo json_encode(array('code' => 200, 'data' => $temas));
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

		public function jsoncategorias()
		{
			try {
				global $aplicacion;
				$this->documento->plantilla = 'blanco';
				$filtros = array();
				$filtros["idpadre"] = isset($_REQUEST["idcatpadre"]) ? $_REQUEST["idcatpadre"] : 0;
				//if(!empty($_REQUEST["idcategoria"]))
				$categorias = $this->oNegAcad_categorias->buscar($filtros);
				echo json_encode(array('code' => 200, 'data' => $categorias));
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

		public function jsoncursos($filtros = array())
		{
			try {
				global $aplicacion;
				$this->documento->plantilla = 'blanco';
				$filtros = array();
				$this->cursos = $this->oNegCurso->buscarxcategoria($filtros);
				return $this->cursos;
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}

		public function jsonbuscarxcategoria()
		{
			$this->documento->plantilla = 'blanco';
			try {
				global $aplicacion;
				$filtros = array();
				if (isset($_REQUEST["idcategoria"]) && @$_REQUEST["idcategoria"] != '') $filtros["idcategoria"] = $_REQUEST["idcategoria"];
				if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
				$this->datos = $this->oNegCurso->buscarxcategoria($filtros);
				if (empty($this->datos)) echo json_encode(array('code' => 'ok', 'data' => ''));
				else {
					$cat = array();
					foreach ($this->datos as $cur) {
						if (empty($cat[$cur["idcategoria"]])) {
							$cat[$cur["idcategoria"]] = array();
							$cat[$cur["idcategoria"]]["nombre"] = $cur["categoria"];
							$cat[$cur["idcategoria"]]["hijos"] = array();
						}
						$imgcursodefecto = URL_MEDIA . '/static/media/cursos/nofoto.jpg';
						$imgtmp = substr(RUTA_MEDIA, 0, -1) . $cur["imagen"];
						$cur["imagen"] = is_file($imgtmp) ? URL_MEDIA . $cur["imagen"] : $imgcursodefecto;
						$cat[$cur["idcategoria"]]["hijos"][$cur["idcurso"]] = $cur;
					}
					$this->datos = $cat;
				}
				echo json_encode(array('code' => 200, 'data' => $this->datos));
				exit(0);
			} catch (Exception $e) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_($e->getMessage())));
				exit(0);
			}
		}
		// vistas para proyectos.
		public function listar()
		{
			try {
				global $aplicacion;
				$this->esquema = 'cur_proyecto/inicio';
				$this->documento->plantilla = 'general';
				return parent::getEsquema();
			} catch (Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}
		}
	}
