<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebEnglish extends JrWeb
{
	public $PV=array('PVADU_1'=>array('link'=>URL_SEAdultos,'nombre'=>'Plataforma Adultos'),'PVADO_1'=>array('link'=>URL_SEAdolecentes,'nombre'=>'Plataforma Adolescentes'));
	//PVADU_1" link="http://localhost/smart/smartenglish/
	
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto(){
		return $this->ver();
	}

	public function ver(){
		try{			
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();		
			$this->documento->setTitulo(JrTexto::_('SmartEnglish'));		
			$idcurso='&idcurso='.(!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0);
			$idcursoexterno='&idcursoexterno='.(!empty($_REQUEST["idcursoexterno"])?$_REQUEST["idcursoexterno"]:0);
			$idcursodetalle='&idcursodetalle='.(!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:0);
			$idrecurso='&idrecurso='.(!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:0);
			$idpersona='?idusuario='.$usuarioAct["idpersona"];
			$token='&token='.$usuarioAct["token"];
			$usuario='&usuario='.$usuarioAct["usuario"];
			$idproyecto='&idproyecto='.$usuarioAct["idproyecto"];
			$idempresa='&idempresa='.$usuarioAct["idempresa"];
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco';

			$_PV=!empty($_REQUEST['PV'])?$_REQUEST['PV']:'PVADU_1';
			$PVsel=!empty($this->PV[$_PV])?$this->PV[$_PV]:$this->PV['PVADU_1'];
			$this->link =	$PVsel["link"]."servicelogin/actividad/".$idpersona.$token.$usuario.$idproyecto.$idempresa.$idcurso.$idcursodetalle.$idrecurso.$idcursoexterno;			
			$this->esquema = 'smartenglish/ver';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		//$paginaext="https://www.abacoeducacion.org/web/smartquiz/examenes/resolver/?idexamen=$idexa&id=$idalu&pr=edukt2017&u=$nomalu";
	}

	public function buscar(){
		try{			
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();		
			$this->documento->setTitulo(JrTexto::_('SmartEnglish'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco';			
			$this->esquema = 'smartenglish/buscar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}