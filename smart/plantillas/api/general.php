<?php defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';$rutastatic=$documento->getUrlStatic();?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google" content="notranslate" />
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_inicio.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js?vs=<?php echo $verion; ?>"></script>
    <!--script src="<?php //echo $documento->getUrlStatic()?>/js/audio/mespeak/mespeak.js"></script-->

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/sweetalert/sweetalert2.all.min.js"></script> 
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js?vs=<?php echo $verion; ?>"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/inicio.js?vs=<?php echo $verion; ?>"></script>
    <script> 
            var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>';
            var _sysUrlSitio_ = '<?php echo $documento->getUrlSitio()?>';
            var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';</script>
  
    <jrdoc:incluir tipo="cabecera" />
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
</head>
<body >
<!--jrdoc:incluir tipo="modulo" nombre="preload" /></jrdoc:incluir-->
<jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
<div class="">
    <jrdoc:incluir tipo="mensaje" />
    <jrdoc:incluir tipo="recurso" />
</div>
<jrdoc:incluir tipo="modulo" nombre="footer" posicion="man"/></jrdoc:incluir>
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<jrdoc:incluir tipo="docsJs" />
<!-- <script type="text/javascript" src="<?php //echo $documento->getUrlStatic()?>/tema/js/jquery.ui.touch.min.js?vs=<?php //echo $verion; ?>"></script> -->
</body>
</html>
