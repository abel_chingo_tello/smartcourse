<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS);
JrCargador::clase('modulos::top::Top', RUTA_TEMA);
$oMod = new Top;
echo $oMod->mostrar(@$atributos['posicion']);