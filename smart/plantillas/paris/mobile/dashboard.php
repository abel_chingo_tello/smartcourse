<?php
defined('_BOOT_') or die('');
$version = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $documento->getUrlStatic();
?>
<!DOCTYPE html>

<html>

<head>
    <jrdoc:incluir tipo="modulo" nombre="metadata" />
    </jrdoc:incluir>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/fontawesome-free/css/all.min.css">
    <link href="<?php echo $rutastatic; ?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $rutastatic; ?>/libs/pnotify/pnotify.min.css" rel="stylesheet">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->

    <link rel="stylesheet" href="<?php echo $rutastatic; ?>/libs/datatable1.10/media/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo $rutastatic; ?>/libs/datatable1.10/extensions/Buttons/css/buttons.dataTables.min.css">
    <script type="text/javascript" src="<?php echo $rutastatic; ?>/libs/sweetalert/sweetalert2.all.min.js"></script>
    <!-- jQuery -->
    <script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!--script type="text/javascript" src="<?php //echo URL_BASE; ?>static/libs/poppers/dist/umd/popper.min.js"></script-->
    <!-- AdminLTE App -->
    <script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/dist/js/adminlte_curso.js?vs=<?php echo $version; ?>"></script>
    <script src="<?php echo $rutastatic; ?>/js/funciones.js?vs=<?php echo $version; ?>"></script>
    <script src="<?php echo $rutastatic; ?>/js/smartcourse.js?vs=<?php echo $version; ?>"></script>
    <script type="text/javascript" src="<?php echo $rutastatic; ?>/libs/sweetalert/sweetalert2.all.min.js"></script>
    <script src="<?php echo $rutastatic; ?>/libs/pnotify/pnotify.min.js"></script>
    <script src="<?php echo $rutastatic; ?>/libs/tinymce54/tinymce.min.js"></script>

    <script>
        var _sysUrlBase_ = '<?php echo $documento->getUrlBase() ?>';
        var sitio_url_base = _sysUrlSitio_ = '<?php echo $documento->getUrlSitio() ?>';
        var _sysIdioma_ = '<?php echo $documento->getIdioma() ?>';
        var _sysUrlStatic_ = '<?php echo $rutastatic; ?>';
        var _menus_ = new Array();
        var sence = false;
    </script>
    <link rel="stylesheet" href="<?php echo URL_BASE ?>smart/plantillas/paris/mobile/assets/css/index.css" />

</head>
<!-- Main Sidebar Container -->
<jrdoc:incluir tipo="modulo" nombre="sideleft" posicion="sideleft_mobile" />
</jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="top" posicion="top_mobile" />
</jrdoc:incluir>
<!-- Content Wrapper. Contains page content -->
 
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content">
        <jrdoc:incluir tipo="mensaje" />
        <jrdoc:incluir tipo="recurso" />
        <!-- /.content -->
    </div>
</div>
 <div class="home-course-section-1">
    <div id="wf-mask-background-course"></div>
    <div class="grid">
        <div class="">
            <p class="white txt-name-course" id="nombreCursoTop"></p>
        </div>
        <div class="txt-right">
            <p class="white txt-date-course" id="fechaCursoTop">Inicio:02-09-2021</p>
            <!-- <p class="white txt-date-course">Fin:02-09-2021</p> -->
        </div>
    </div>
    <div class="content-blank bg-default-course">
    <jrdoc:incluir tipo="mensaje" />
    <jrdoc:incluir tipo="recurso" />
    </div>
</div> 
<jrdoc:incluir tipo="modulo" nombre="pie" />
</jrdoc:incluir>
<!-- ./wrapper -->

<!-- AdminLTE for demo purposes -->

<script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/dist/js/demo.js?vs=<?php echo $version; ?>"></script>
<script src="<?php echo $rutastatic; ?>/libs/datatable1.10/media/js/jquery.dataTables.min.js?vs=<?php echo $version; ?>"></script>
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<script>
    // $(document).ready(function() {
    //     $("._menu_").addClass(py.paris.menu);
    //     $("._logo_").addClass(py.paris.logo);
    //     $("._texto_").addClass(py.paris.texto);
    //     $("._navbar_").addClass(py.paris.navbar);
    // });

    // $(window).bind('hashchange', function (e) {
    //   mnu_active();
    // });
</script>

<script src="<?php echo URL_BASE ?>smart/plantillas/paris/mobile/assets/js/index.js"></script>
<script type="text/javascript" src="<?php echo $rutastatic; ?>/js/asistentetooltip.js?vs=<?php echo $version; ?>"></script>
<?php
$user = NegSesion::getUsuario();
$empresasconchat = array(27, 46);
//var_dump($this->empresa);
if(in_array($user["idempresa"], $empresasconchat)) { ?>
    <script defer src="https://widget.tochat.be/bundle.js?key=742ebb89-adcc-4f34-a930-0cd897ef6328"></script>
<?php } ?>
<style>
    .bs-tooltip-auto[x-placement^=bottom] .arrow::before,
    .bs-tooltip-bottom .arrow::before {
        border-bottom-color: #f40505;
    }
    .tooltip-inner {
        color: #fff;
        background-color: #f40505;
    }
</style>
</body>

</html>