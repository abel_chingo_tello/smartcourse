let sidebar = document.getElementById("sidebar");
let itemChild = document.getElementsByClassName('item-parent');
if (sidebar) {
    sidebar.style.display = "none";
}

// console.log('itemChild',itemChild);
for (let i = 0; i <= itemChild.length - 1; i++) {
    // console.log(itemChild.length)
    // console.log('PRIMER HIJ',itemChild[i].firstElementChild)
    itemChild[i].firstElementChild.onclick = () => toogleItem(itemChild[i]);
}


function opensidebar() {

    sidebar.classList.remove("sidebar-close");
    sidebar.style.display = "block";
}

function closesidebar() {

    sidebar.classList.add("sidebar-close");
    setTimeout(() => {
        sidebar.style.display = "none";
    }, 700)
}


// ==================  MODAL ===================

function openModal(id) {
    let idModal = document.getElementById(id);
    console.log(id);
    idModal.style.display = "block";
    idModal.classList.toggle("display");

}

function openModaldiferent(id, event) {
    let idModal = document.getElementById(id);
    console.log(id);
    //idModal.style.display = "block";
    idModal.classList.toggle("display");
    console.log(event.currentTarget.firstElementChild)
    event.currentTarget.firstElementChild.classList.toggle("rotate-icon-plus")
}

function openModalDropdown(id, iddrop) {
    let idModal = document.getElementById(id);
    let iddropdown = document.getElementById(iddrop);

    console.log(idModal);
    console.log(iddropdown)
    iddropdown.classList.toggle("display");
    idModal.classList.toggle("display");

}

function closeModal(id) {
    let idModal = document.getElementById(id);
    // console.log(id);
    // idModal.style.display ="none";
    idModal.classList.toggle("display");


}

// ==================  LIST ITEM  ===================

function toogleItem(nodeItem) {
    let ul_items = nodeItem.lastElementChild
    let icon_arrow = nodeItem.querySelectorAll(".icon-toogle");
    icon_arrow.forEach(element => {
        // console.log('2element',element)
        element.classList.toggle("icon-toogle-active");
    });

    // console.log("lastChild =>",ul_items)
    ul_items.style.marginLeft = '15px'
    ul_items.classList.toggle("visible");
    nodeItem.classList.toggle("select-item");


}


var progressbar = document.getElementsByClassName("progress");
console.log(progressbar + typeof (progressbar));


for (let i = 0; i < progressbar.length; i++) {
    //console.log(progressbar[i])
    let element = progressbar[i];
    let valueMin = parseInt(element.ariaValueMin);
    let valueMax = parseInt(element.ariaValueMax);
    let valueNow = parseInt(element.ariaValueNow);
    //console.log(valueMin, valueMax, valueNow);
    let width = valueMin;
    //console.log(typeof (valueMax));
    if (valueNow <= valueMax && valueNow >= valueMin) {
        //  console.log("valueNow", valueNow)
        width = valueNow;
    }
    console.log("width", width);
    let valueNowPorcentage = calcularPorcentaje(valueMax, width);
    console.log(valueNowPorcentage + "%");
    element.style.width = valueNowPorcentage + "%";
}

function calcularPorcentaje(max, now) {
    // console.log("max", max);
    // console.log("now", now);
    // console.log("valor=", (now * 100) / max);
    return (now * 100) / max;
}


// ================== TABS ===============
function openOptiontab(evt, optionName) {

    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";

    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
        tablinks[i].firstElementChild.style.display = "block"
    }
    document.getElementById(optionName).style.display = "block";
    evt.currentTarget.className += " active";
    evt.currentTarget.firstElementChild.style.display = "none"
    closeTab("tab", "btn-tab-action");
}
// ================== BUTTON SHOW TABS ===============
function showTab(idTab, event) {
    console.log(event.target, "eventtt");
    iElement = event.target;

    document.getElementById(idTab).classList.toggle("show_tab");
    if (iElement.classList.contains("icon-points")) {
        iElement.className = iElement.className.replace("icon-points", "icon-close");
    } else {
        iElement.className = iElement.className.replace("icon-close", "icon-points");
    }

    // setTimeout(()=>{
    //     console.log("dentro del timers")
    //     document.getElementById(idTab).className.replace("show_tab","");
    // },4000);  
}


function closeTab(idTab, idbtn) {
    iElement = document.getElementById(idbtn);
    console.log("close tab=>", iElement);
    document.getElementById(idTab).classList.toggle("show_tab");
    if (iElement.classList.contains("icon-points")) {
        iElement.className = iElement.className.replace("icon-points", "icon-close");
    } else {
        iElement.className = iElement.className.replace("icon-close", "icon-points");
    }
}
// ================== TEXT ELLIPSIS ===============
let arrayEllipsis = document.getElementsByClassName("text-ellipsis");
for (let index = 0; index < arrayEllipsis.length; index++) {
    const element = arrayEllipsis[index];
    element.addEventListener("click", () => {
        toogleEllipsis(element)
    })
}

function toogleEllipsis(el) {
    el.classList.toggle("no-text-ellipsis");
    el.style.Opacity = ".2"
}