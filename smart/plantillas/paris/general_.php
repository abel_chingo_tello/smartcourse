<?php 
defined('_BOOT_') or die(''); $version=!empty(_version_)?_version_:'1.0';
$rutastatic=$documento->getUrlStatic();
?>
<!DOCTYPE html>
<html>
  <head>
    <jrdoc:incluir tipo="modulo" nombre="metadata" /></jrdoc:incluir>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $rutastatic;?>/tema/paris/adminLTE3/fontawesome-free/css/all.min.css">
    <link href="<?php echo $rutastatic;?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $rutastatic; ?>/libs/pnotify/pnotify.min.css" rel="stylesheet">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $rutastatic;?>/tema/paris/adminLTE3/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/sweetalert/sweetalert2.all.min.js"></script>
    <!-- jQuery -->
    <script src="<?php echo $rutastatic;?>/tema/paris/adminLTE3/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $rutastatic;?>/tema/paris/adminLTE3/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $rutastatic;?>/tema/paris/adminLTE3/dist/js/adminlte_curso.js?vs=<?php echo $version; ?>"></script>
    <script src="<?php echo $rutastatic; ?>/js/funciones.js?vs=<?php echo $version; ?>"></script>
    <script src="<?php echo $rutastatic; ?>/js/smartcourse.js?vs=<?php echo $version; ?>"></script>
    <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/sweetalert/sweetalert2.all.min.js"></script>
    <script src="<?php echo $rutastatic; ?>/libs/pnotify/pnotify.min.js"></script> 
    <script>
        var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>';
        var sitio_url_base=_sysUrlSitio_ = '<?php echo $documento->getUrlSitio()?>'; 
        var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
        var _sysUrlStatic_ = '<?php echo $rutastatic;?>';
        var _menus_ = new Array();
    </script>
    <style>
    /* .main-sidebar{
      height: calc(100vh - 120px);
      overflow-y: auto;
    } */
    .sidebar::-webkit-scrollbar-track
    {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
      background-color: #F5F5F5;
    }

    .sidebar::-webkit-scrollbar
    {
      width: 6px;
      background-color: #F5F5F5;
    }

    .sidebar::-webkit-scrollbar-thumb
    {
      background-color: #6c757d;
    }
    
    body::-webkit-scrollbar-track
    {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
      background-color: #F5F5F5;
    }

    body::-webkit-scrollbar
    {
      width: 6px;
      background-color: #F5F5F5;
    }

    body::-webkit-scrollbar-thumb
    {
      background-color: #6c757d;
    }
    </style>
  </head>
  <!-- hold-transition sidebar-mini accent-warning -->
  <body class="hold-transition sidebar-mini _texto_">
    <div class="wrapper">
      <!-- Navbar -->
      <jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <jrdoc:incluir tipo="modulo" nombre="sideleft" /></jrdoc:incluir>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content">
          <jrdoc:incluir tipo="mensaje" />
          <jrdoc:incluir tipo="recurso" />       
          <!-- /.content -->
        </div>
      </div>
      <jrdoc:incluir tipo="modulo" nombre="pie" /></jrdoc:incluir>
      <!-- /.content-wrapper -->
      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $rutastatic;?>/tema/paris/adminLTE3/dist/js/demo.js?vs=<?php echo $version; ?>"></script>
    <?php
        $configs = JrConfiguracion::get_();
        require_once(RUTA_SITIO . 'ServerxAjax.php');
        echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
    ?>
    <script>
    $(document).ready(function(){
      $("._menu_").addClass(py.paris.menu);
      $("._logo_").addClass(py.paris.logo);
      $("._texto_").addClass(py.paris.texto);
      $("._navbar_").addClass(py.paris.navbar);
    });

    // $(window).bind('hashchange', function (e) {
    //   mnu_active();
    // });
    </script>
    <div class="modal fade" id="modalclone" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h5 class="modal-title" id="modaltitle" style="width: 100%"></h5>
            <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>       
          </div>
          <div class="modal-body" id="modalcontent">
            <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $documento->getUrlStatic() ?>/media/cargando.gif"><br><span style="color: #006E84;"><?php echo JrTexto::_("Loading");?></span></div>
          </div>
          <div class="modal-footer" id="modalfooter">        
            <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
          </div>
        </div>
      </div>
    </div>
    <div style="display: none;">
      <div class="progress" id="clone_progressup" style="position: absolute; width: calc(100% - 20px); top: 0px; height: 6px; margin: 0 auto; background-color: #f0db61; ">
        <div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width: 0%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
    </div>
  </body>
</html>