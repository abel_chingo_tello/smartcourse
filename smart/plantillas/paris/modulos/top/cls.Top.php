<?php
/**
 * @autor		Alvaro Alonso Cajusol Vallejos
 * @fecha		10/01/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_RAIZ') or die();
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_RAIZ);
JrCargador::clase('sys_negocio::NegRoles', RUTA_RAIZ);
class Top extends JrModulo
{
	protected $oNegConfig;
	protected $oNegRoles;
	protected $oNegPaginas;
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegRoles =new NegRoles();
		$this->usuario = NegSesion::getUsuario();
		$this->modulo = 'top';
	}
	
	public function mostrar($html=null)
	{
		try {
			// if (@$html == 'top_mobile' ) {
				 
			// 	$this->esquema = $html;
			// 	return $this->getEsquema();
			// }		
			if(empty($html)){
				$this->esquema = 'top';
			}else $this->esquema = $html;			
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}