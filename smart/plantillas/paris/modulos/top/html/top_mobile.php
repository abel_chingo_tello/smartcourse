<?php
defined('_BOOT_') or die('');
$verion = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $this->documento->getUrlStatic();
$idrol = $this->usuario["idrol"];
?>
<style>
@media (min-width: 320px) and (max-width: 767px) {
  .enmobil._navbar_{
    flex-flow: wrap;
  }
  ul.ulmobil{
    width: auto;
    margin: 0;
    padding: 0;
  }  
  #md-material .btncerrarmodal{
    margin-top: 2ex;
  }
  #nombreCursoTop{
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    max-width: 230px;
    min-height:46px;
  }
  #contentpages > div > div > center > div > h1 {
    font-size: 16px;
  }
  .marcopage > nav > div > a.navbar-brand{
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    max-width: 220px;
  }
}
</style>

<nav class="main-header navbar navbar-expand _navbar_ enmobil">
<!-- Left navbar links -->

<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link asistentetooltip tooltip1" data-widget="pushmenu" href="#" onclick="sideLeft(this, 1)" data-toggle="tooltip" data-placement="top" title="<?php echo JrTexto::_('Start Here'); ?>"><i class="icon icon-menu"></i></a>
  </li>
</ul>
<!-- SEARCH FORM -->
<!-- <form class="form-inline ml-3">
  <div class="input-group input-group-sm">
    <input class="form-control form-control-navbar" type="search" placeholder="Buscar" aria-label="Search">
    <div class="input-group-append">
      <button class="btn btn-navbar" type="submit">
        <i class="fas fa-search"></i>
      </button>
    </div>
  </div>
</form> -->

<!-- Right navbar links -->
<ul class="navbar-nav ml-auto ulmobil">
  <!-- Notifications Dropdown Menu -->
  <li class="nav-item mostrar_material_ayuda" title="<?php echo JrTexto::_('Tutorial'); ?>"  data-toggle="tooltip" data-placement="top" style="display:none" >
    <a class="nav-link" style="cursor: pointer;"  >
      <i class="fas fa-question-circle"></i>
    </a>
  </li>
  <li class="nav-item" title="<?php echo JrTexto::_('Home'); ?>"  data-toggle="tooltip" data-placement="top" >
    <a class="nav-link" style="cursor: pointer;" onclick="mostrarPortada()"  >
      <i class="icon icon-home"></i>
    </a>
  </li>
  <li class="nav-item" title="<?php echo JrTexto::_('Exit'); ?>" data-toggle="tooltip" data-placement="top" >
    <a class="nav-link" style="cursor: pointer;" onclick="cerrarCurso()">
      <i class="icon icon-exit"></i>
    </a>
  </li>
</ul>
</nav>
<div class="form-inline home-course-section-1 ml-3" style="margin-left: 0 !important;">
    <span id="nombreCursoTop" class="nav-link" style="font-weight: bold; white-space: nowrap;"></span>
</div>
<script type="text/javascript">

function sideLeft(boton, op){
  if(op === 1){
    $("._btnversesion p").css("white-space", "");
    $(boton).attr("onclick", "sideLeft(this, 2)");
  } else if(op === 2){
    $("._btnversesion p").css("white-space", "normal");
    $(boton).attr("onclick", "sideLeft(this, 1)");
  }
}

<?php 
if($idrol==3 || $idrol==2){
?>
$(window).mousemove(function(){
  localStorage.setItem("conteo", localStorage.getItem("conteoInicial"));
});
// var body = document.getElementsByTagName("body")[0];
// body.addEventListener('mousemove', function(){
//   localStorage.setItem("conteo", localStorage.getItem("conteoInicial"));
// });
<?php
}
?>

function agregarFuncionCronometro(){
  <?php 
  if($idrol==3 || $idrol==2){
  ?>
  var html_ = "";
    // html_ += $('iframe').contents().find("body").html();
    html_ += '<script>';
    html_ += 'var body = document.getElementsByTagName("body")[0];';
    html_ += 'body.addEventListener("mousemove", function(){';
    html_ += 'localStorage.setItem("conteo", localStorage.getItem("conteoInicial"));';
    html_ += '});';
    // html_ += '$(window).mousemove(function(){';
    // html_ += '  localStorage.setItem("conteo", localStorage.getItem("conteoInicial"))';
    // html_ += '})';
    html_ += '<\/script>';
    // $('iframe').contents().find("body").html(html_);
    $('iframe').contents().find("body").append(html_);
  <?php
  }
  ?>
}

function cerrarCurso(){
  Swal.fire({
        title: '<?php echo ucfirst(JrTexto::_('Confirm'));?>',
        text: '<?php echo ucfirst(JrTexto::_('are you sure to close the course tab?')); ?>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '<?php echo JrTexto::_('Accept');?>',
        cancelButtonText: '<?php echo JrTexto::_('Cancel');?>',
        }).then((result) => {
            if (result.value) {
              if(sence){
                $("#frmCerrarSence").submit();
              } else {
                close();
                window.open('','_parent',''); 
                try{
                  window.close();
                }catch(e){console.log(e)}
                try{
                  self.close();
                }catch(e){console.log(e)}

                window.location.href=_sysUrlBase_
              }
              // if(returnlink!=''||returnlink!=undefined)redir(returnlink);
            }
          });
}
</script>