<?php
/**
 * @autor		Alvaro Alonso Cajusol Vallejos
 * @fecha		10/01/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_RAIZ') or die();
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_RAIZ);
JrCargador::clase('sys_negocio::NegRoles', RUTA_RAIZ);
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMenu_proyecto', RUTA_BASE, 'sys_negocio');

class Sideleft extends JrModulo
{
	protected $oNegConfig;
	protected $oNegRoles;
	protected $oNegPaginas;
	protected $oNegProyecto;
	protected $oNegBolsa_empresas;
	protected $oNegPersonal;
	protected $oNegMenu_proyecto;

	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegRoles =new NegRoles();
		$this->oNegProyecto = new NegProyecto;
		$this->oNegBolsa_empresas = new NegBolsa_empresas;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegMenu_proyecto= new NegMenu_proyecto();
		$this->usuario = NegSesion::getUsuario();
		$this->modulo = 'sideleft';
	}
	
	public function mostrar($html=null)
	{
		try {

			$this->oNegProyecto->idproyecto = $this->usuario["idproyecto"];
			$this->proyecto = $this->oNegProyecto->dataProyecto;
			$this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuario["idempresa"]))[0];
			$this->nomempresa = $this->empresa["nombre"];
			$this->logo_emp = $this->empresa["logo"];
			$idpersona = $this->usuario["idpersona"];	
			$this->oNegPersonal->idpersona = $idpersona;
			$this->datos = $this->oNegPersonal->dataPersonal;
			$this->menus = $this->oNegMenu_proyecto->buscar(array('idproyecto'=>$this->usuario["idproyecto"],'idrol'=>$this->usuario["idrol"],'enorden'=>true));

			///var_dump($this->menus);
			if(@$html=='sideleft_mobile' && empty($this->menus)){
				if(!empty($this->proyecto["jsonlogin"])){
					$infologin=json_decode($this->proyecto["jsonlogin"],true);
					if(!empty($infologin["menus"])){
						$this->menus=$infologin["menus"];
					}
				}
				JrCargador::clase('sys_negocio::NegModulos', RUTA_BASE);
				$oNegModulos = new NegModulos;
				$datos_=$oNegModulos->buscar(array('estado'=>1));	

				//bloqueo por rol
				$menusbloqueos=array(
							1=>array("13","14","29","31","32","33","37"),
							2=>array("2","20","24","34","31","37"),
							3=>array("1","2","7","20","24","29","30","32","33","34")
						);
				$bloqueomenu=!empty($menusbloqueos[$this->usuario["idrol"]])?$menusbloqueos[$this->usuario["idrol"]]:array();

				//var_dump($this->usuario["idrol"]);
				//var_dump($bloqueomenu);
				//echo '<br><br>';
				//exit();
				foreach($this->menus as $kmen => $men){
					$estaasignado=false;
					foreach ($datos_ as $kmod => $mod){
						if($estaasignado==false && $mod["idmodulo"]==$men["id"] && !in_array($mod["idmodulo"],$bloqueomenu)){
							$estaasignado=true;
							$this->menus[$kmen]["espadre"]=$mod["espadre"];
							$this->menus[$kmen]["arrhijos"]=!empty($mod["arrhijos"])?$mod["arrhijos"]:false;
							$nombretmp=str_replace('Mis notas','My records',$mod["nombre"]);
							$nombretmp=str_replace('Comunidad','Community',$nombretmp);
							$nombretmp=str_replace('Configuración','Setting',$nombretmp);
							$nombretmp=str_replace('Exámenes Externos','External exams',$nombretmp);
							$nombretmp=str_replace('Mi portafolio','My portfolio',$nombretmp);
							$nombretmp=str_replace('Currículo','Curriculum',$nombretmp);
							$nombretmp=str_replace('Familia','Family',$nombretmp);
							$nombretmp=str_replace('Eventos','Events',$nombretmp);
							$nombretmp=str_replace('Participantes','Participants',$nombretmp);
							$nombretmp=str_replace('Revisar Exámenes','Review Exams',$nombretmp);
							$nombretmp=str_replace('Revisar Smartbook','Review Smartbook',$nombretmp);
							$nombretmp=str_replace('Activación de códigos','Activation of codes',$nombretmp);
							$nombretmp=str_replace('Foros','Forums',$nombretmp);
							
							$this->menus[$kmen]["nombretraducido"]=ucfirst(JrTexto::_($mod["nombre"]));
							unset($datos_[$kmod]);
							//if($estaasignado) $this->datos[] = array_replace($mod, array("nombretraducido" => ));
						}
					}
					if($estaasignado==false) unset($this->menus[$kmen]);
				}				
				$this->esquema = $html;
				return $this->getEsquema();
			}			
			if(!empty($this->menus)){				
				$html='sideleft_menu';
				$this->esquema=$html;	
			}
			if(empty($html)){				
				$this->esquema = 'sideleft';
			}else $this->esquema = $html;		
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}