<?php
defined('_BOOT_') or die('');
$verion = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $this->documento->getUrlStatic();

$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
$imgdefecto = $rutastatic . "/media/web/nofoto.jpg";
$imgfoto = $rutastatic . "/media/usuarios/user_avatar.jpg";
$jsonlogin = '{logo1:""}';
$usu = '';
if (!empty($this->usuario)) $usu = $this->usuario;
if (!empty($this->proyecto)) {
  $py = $this->proyecto;
  $jsonlogin = $py["jsonlogin"];
}
$frm = !empty($this->datos) ? $this->datos : "";
$fotouser = !empty($frm["foto"]) ? $frm["foto"] : 'static/media/usuarios/user_avatar.jpg';
$ifoto = strripos($fotouser, "static/");
if ($ifoto != '') $fotouser = $this->documento->getUrlBase() . substr($fotouser, $ifoto);
if ($ifoto == '') {
  $ifoto = strripos($fotouser, "/");
  $ifoto2 = strripos($fotouser, ".");
  if ($ifoto != '' && $ifoto2 != '') $fotouser = $this->documento->getUrlBase() . 'static/media/usuarios' . substr($fotouser, $ifoto);
  elseif ($ifoto2 != '') $fotouser = $this->documento->getUrlBase() . 'static/media/usuarios/' . $fotouser;
  else $fotouser = $this->documento->getUrlBase() . 'static/media/usuarios/user_avatar.jpg';
}
?>

<style>
  .nav-sidebar>.nav-item {
    cursor: pointer !important;
  }

  .nav-sidebar>.nav-item>.nav-link {
    cursor: pointer !important;
  }

  ._btnversesion>p {
    /* white-space: normal !important; */
    /* min-width: 250px; */
    width: 100% !important;
  }

  @media (min-width: 992px) {

    .sidebar-mini.sidebar-collapse .main-sidebar.sidebar-focused,
    .sidebar-mini.sidebar-collapse .main-sidebar:hover ._btnversesion>p {
      white-space: normal !important;
    }
  }

  .sidebar {
    max-height: calc(100vh - 4rem);
  }
</style>

<aside class="main-sidebar elevation-4 _menu_">
  <!-- Brand Logo -->
  <a href="#" class="brand-link _logo_">
    <img src="<?php echo URL_BASE . $this->logo_emp; ?>" class="brand-image img-circle elevation-1" style="height: 2.1rem; width: 2.1rem; background-color: white;">
    <span class="brand-text font-weight-light"><strong style="font-weight: bold; font-size: 13px; "><?php echo $this->nomempresa; ?></strong></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" id="mostrarmodulos"></ul>
      <div id="infosense" style="  max-width: 90%;    position: absolute;      bottom: 0px;"></div>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>

<script>
  var py = JSON.parse(`<?php echo !empty($jsonlogin)?str_replace('\n','<br>',$jsonlogin):'{}';?>`);
  var url_media = '<?php echo URL_MEDIA; ?>';
  var imgdefecto = '<?php echo $imgdefecto; ?>';
  var fotodefecto = '<?php echo $imgfoto ?>';
  var jsonproyecto = py;
  var idproyecto = parseInt('<?php echo $py["idproyecto"]; ?>');
  var idempresa = parseInt('<?php echo $py["idempresa"]; ?>');
  var dtuserlogin = <?php echo json_encode($this->usuario); ?>;
  var idrol = parseInt(dtuserlogin.idrol);
  _menus_ = py.menus;
  if (py.paris == undefined) {
    py.paris = new Object();
    py.paris.navbar = "navbar-white navbar-light";
    py.paris.texto = "";
    py.paris.menu = "sidebar-dark-primary";
    py.paris.logo = "";
  }

  function mnu_active(idmenu = null) {
    var url_ = window.location.href.split("#");
    if (url_.length > 1) {
      if (url_[1] !== "") {
        // var id = parseInt(window.atob(url_[1]));
        var id = (window.atob(url_[1]));
        // if (idmenu == id) {//si existe en arrTemas??
        if (url_.length > 2) {
          if (url_[2] !== "") { //pestaña?
            contenidoCurso("#mnuli" + id, id, "#" + url_[2]);
          }
        } else { //menu?
          contenidoCurso("#mnuli" + id, id);
        }
        // }
      } else {
        mostrarPortada();
      }
    } else {
      mostrarPortada();
    }
  }

  function mnuse_active(idmenu = null) {
    var url_ = window.location.href.split("#");
    if (url_.length > 2) {
      if (url_[2] !== "") {
        var id = parseInt(window.atob(url_[2]));
        if (idmenu == id) {
          console.log(idmenu,id);
          $("#mnuse" + id).click()
        }
      } else {
        var first = $('.menuSesion').children('.listaSesiones').children('li:first-child');
        first.trigger('click');
        window.location.href = "#" + url_[1] + "#" + window.btoa(first.data("id"));
      }
    } else {
      var first = $('.menuSesion').children('.listaSesiones').children('li:first-child');
      first.trigger('click');
      window.location.href = "#" + url_[1] + "#" + window.btoa(first.data("id"));
    }
  }
</script>