<link href="<?php echo $this->documento->getUrlTema()?>/css/preload.css" rel="stylesheet">
<style>
#loader-wrapper .loader-section{
    background: #000000dd !important;
}
</style>
<div id="loader-wrapper">
    <div id="loader">
    	<!-- <img src="<?php echo $this->documento->getUrlStatic()?>/media/imagenes/loading.gif" class="img-fluid"> -->
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<script type="text/javascript">
$(document).ready(function() {    
    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 10);    
});
</script>