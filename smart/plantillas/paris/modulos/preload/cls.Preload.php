<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class Preload extends JrModulo
{
	public function __construct()
	{
		parent::__construct();		
		$this->modulo = 'preload';
	}
	
	public function mostrar($html=null)
	{
		try {
			$this->usuarioAct = NegSesion::getUsuario();			
			$this->esquema = 'preload'.(!empty($html)?('-'.$html):'');
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}