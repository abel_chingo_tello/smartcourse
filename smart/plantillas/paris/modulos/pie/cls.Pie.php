<?php
/**
 * @autor		Alvaro Alonso Cajusol Vallejos
 * @fecha		10/01/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_RAIZ') or die();
class Pie extends JrModulo
{
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		//$this->oNegServicios = new NegServicios;
		$this->modulo = 'pie';
		//$this->oNegPaginas = new NegPaginas();
	}
	
	public function mostrar($html=null)
	{
		try{		
			$this->esquema = 'pie'.$html;				
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}