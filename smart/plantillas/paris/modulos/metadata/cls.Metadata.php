<?php
/**
 * @autor		Alvaro Alonso Cajusol Vallejos
 * @fecha		10/01/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_RAIZ') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
class Metadata extends JrModulo
{
	protected $oNegConfig;
	protected $oNegRoles;
	protected $oNegPaginas;
	protected $oNegProyecto;
	protected $oNegBolsa_empresas;
	protected $oNegPersonal;

	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegBolsa_empresas = new NegBolsa_empresas;
		$this->usuario = NegSesion::getUsuario();
		$this->modulo = 'metadata';
	}
	
	public function mostrar($html=null)
	{
		try {
			$this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuario["idempresa"]))[0];
			$this->nomempresa = $this->empresa["nombre"];
			$this->logo_emp = $this->empresa["logo"];

			if(empty($html)){
				$this->esquema = 'metadata';
			}else $this->esquema = $html;			
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}