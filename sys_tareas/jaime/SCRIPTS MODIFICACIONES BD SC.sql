27/10/2020
ALTER TABLE `aceglob4_smart_course`.`cron_tarea_proy` 
ADD COLUMN `idgrupoauladetalle` BIGINT(20) NULL DEFAULT NULL AFTER `tiporecurso`,
ADD INDEX `cron_idgrupoauladetalle_idx` (`idgrupoauladetalle` ASC);
