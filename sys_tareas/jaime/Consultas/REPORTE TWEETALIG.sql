
	select historial.fechaentrada,
		#personal
		personal.idpersona, concat(personal.ape_paterno , ' ',personal.ape_materno,', ',personal.nombre ) nombre,
		#matricula
		acad_matricula.fecha_matricula, acad_matricula.fecha_vencimiento,
		#grupoauladetalle
		acad_grupoauladetalle.nombre nombre_GAD,acad_grupoauladetalle.fecha_inicio fecha_inicio_GAD,acad_grupoauladetalle.fecha_final fecha_final_GAD,
		#grupoaula
		acad_grupoaula.nombre nombre_grupoaula, acad_grupoaula.fecha_inicio fecha_inicio_grupoaula, acad_grupoaula.fecha_fin fecha_fin_grupoaula,
		acad_grupoauladetalle.idcurso, acad_grupoauladetalle.idcomplementario
	from acad_matricula
		inner join personal on acad_matricula.idalumno=personal.idpersona
		inner join acad_grupoauladetalle on acad_matricula.idgrupoauladetalle=acad_grupoauladetalle.idgrupoauladetalle
		inner join acad_grupoaula on acad_grupoaula.idgrupoaula=acad_grupoauladetalle.idgrupoaula	
        inner join  (select fechaentrada,idusuario,idcurso,idcc from historial_sesion where idproyecto=9) historial
			on historial.idcurso=acad_grupoauladetalle.idcurso
            and historial.idcc=acad_grupoauladetalle.idcomplementario
            and historial.idusuario=personal.idpersona
            #and historial.fechaentrada>"2020-09-30"
	where acad_grupoaula.idproyecto=9

#group by acad_grupoauladetalle.idgrupoauladetalle