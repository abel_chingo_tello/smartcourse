select proyecto.idproyecto,proyecto.nombreurl,acad_curso.nombre curso_principal, acad_curso_complementario.nombre curso_complementario,
	personal.idpersona,personal.email,personal.usuario, personal.clave,concat(personal.ape_paterno , ' ',personal.ape_materno,', ',personal.nombre ) nombre,
    docente.usuario docente_acc,docente.clave docente_pswd,
    tareas.* 
from tareas
inner join proyecto on tareas.idproyecto=proyecto.idproyecto
inner join personal on tareas.idalumno=personal.idpersona
inner join personal docente on tareas.iddocente=docente.idpersona
inner join acad_curso on tareas.idcurso=acad_curso.idcurso
left join acad_curso_complementario on tareas.idcomplementario=acad_curso_complementario.idcurso
where tareas.idproyecto=7
order by personal.idpersona