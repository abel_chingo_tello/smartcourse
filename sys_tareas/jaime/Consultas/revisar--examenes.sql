#DOC7
select personal.idpersona, concat(personal.ape_paterno , ' ',personal.ape_materno,', ',personal.nombre) nombre_alumno,
notas_quiz.fechamodificacion fecha
FROM personal
inner join notas_quiz on personal.idpersona=notas_quiz.idalumno 
WHERE notas_quiz.idrecurso=1621
	AND notas_quiz.idproyecto=27
	AND notas_quiz.idcurso=711
	AND notas_quiz.idcomplementario=501
GROUP BY personal.idpersona
ORDER BY personal.ape_paterno ASC
#and notas_quiz.idcomplementario=503