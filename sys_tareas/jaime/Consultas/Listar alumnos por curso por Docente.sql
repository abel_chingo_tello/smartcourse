#OBTENER CUENTAS DE ALUMNOS POR DOCENTE
use `aceglob4_smart_course`;
select concat(docente.ape_paterno , ' ',docente.ape_materno,', ',docente.nombre ) nombre_docente, 
acad_curso_complementario.nombre nombre_curso_c ,
alumno.usuario, alumno.clave,
concat(alumno.ape_paterno , ' ',alumno.ape_materno,', ',alumno.nombre ) nombre_alumno
from
personal docente
inner join acad_grupoauladetalle on acad_grupoauladetalle.iddocente=docente.idpersona
inner join acad_curso_complementario on acad_grupoauladetalle.idcomplementario=acad_curso_complementario.idcurso
inner join acad_matricula on acad_matricula.idgrupoauladetalle=acad_grupoauladetalle.idgrupoauladetalle
inner join personal alumno on acad_matricula.idalumno=alumno.idpersona
where docente.usuario = "DONHK0000005"
############
	and alumno.ape_paterno like "%Castillo%" 
	and alumno.ape_materno like "%Valverde%"
	#and alumno.nombre like "%Jean%"
############
order by acad_grupoauladetalle.idgrupoauladetalle asc
;