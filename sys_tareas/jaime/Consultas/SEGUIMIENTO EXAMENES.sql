select * from (
	select proyecto.nombreurl,acad_curso.nombre curso_principal, acad_curso_complementario.nombre curso_complementario, count(idrecurso) total_examenes,
			personal.idpersona,personal.usuario, personal.clave, concat(personal.ape_paterno , ' ',personal.ape_materno,', ',personal.nombre ) nombre,
			notas_quiz.nota, notas_quiz.idrecurso, notas_quiz.idcurso, notas_quiz.idcomplementario, notas_quiz.preguntas, notas_quiz.idgrupoauladetalle
	from notas_quiz
		inner join personal on notas_quiz.idalumno=personal.idpersona
		inner join proyecto on notas_quiz.idproyecto=proyecto.idproyecto
		inner join acad_curso on notas_quiz.idcurso=acad_curso.idcurso
		left join acad_curso_complementario on notas_quiz.idcomplementario=acad_curso_complementario.idcurso
	where proyecto.idproyecto=7
	group by personal.usuario
	) v
order by total_examenes desc