SELECT GROUP_CONCAT(DISTINCT CONCAT(table_name,'.',COLUMN_NAME ) SEPARATOR ', ')
FROM information_schema.columns
WHERE table_schema='aceglob4_smart_course'
AND table_name IN 
	('min_unidad_capacidad'
 #   ,'proyecto_cursos'
	)
AND COLUMN_NAME NOT in ('oldid','oldido','oldidoo')