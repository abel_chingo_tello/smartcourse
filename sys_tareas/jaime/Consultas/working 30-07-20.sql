SELECT vista.sesion, ROUND(avg(vista.progreso),2)progreso_smartbook FROM (
	SELECT  actividades.sesion,actividad_detalle.iddetalle, actividad_detalle.tipo,
			ifnull(SUM(actividad_alumno.porcentajeprogreso)/count(actividad_detalle.iddetalle),0) progreso
	FROM `aceglob4_skscourses_english_adultos`.actividades
		INNER JOIN `aceglob4_skscourses_english_adultos`.actividad_detalle 
			ON actividad_detalle.idactividad=actividades.idactividad
            #AND actividades.sesion =197
		LEFT JOIN  actividad_alumno actividad_alumno 
			ON actividad_alumno.iddetalleactividad=actividad_detalle.iddetalle
			AND actividad_alumno.idproyecto=1#dinámico
			AND actividad_alumno.idcurso=205#dinámico
			AND actividad_alumno.idcc=18#dinámico
			AND actividad_alumno.idalumno='207'#dinámico
	WHERE actividades.sesion =197#dinámico para el idrecurso smartbook
		AND actividades.metodologia in (2,3)#fijo para DBY & PRACTICE
	GROUP BY actividades.metodologia
) vista