use `aceglob4_smart_course`;
select proyecto.idproyecto, bolsa_empresas.nombre,

#personal.idpersona,persona_rol.idrol,personal.nombre, personal.ape_paterno, personal.ape_materno,personal.dni,personal.usuario,personal.clave
acad_matricula.idmatricula,
 personal.idpersona,concat(personal.ape_paterno , ' ',personal.ape_materno,', ',personal.nombre ) nombre_alumna,
 ifnull(acad_curso_complementario.nombre,acad_curso.nombre) curso
from bolsa_empresas
	inner join proyecto on proyecto.idempresa=bolsa_empresas.idempresa
	inner join persona_rol on proyecto.idproyecto=persona_rol.idproyecto
	inner join personal on persona_rol.idpersonal=personal.idpersona
    inner JOIN acad_matricula on acad_matricula.idalumno=personal.idpersona    
    left join acad_grupoauladetalle on acad_grupoauladetalle.idgrupoauladetalle=acad_matricula.idgrupoauladetalle
	left join acad_curso_complementario on acad_grupoauladetalle.idcomplementario=acad_curso_complementario.idcurso
    left join acad_curso on acad_grupoauladetalle.idcurso=acad_curso.idcurso

   # left join tareas on tareas.idalumno=personal.idpersona
   # left join tareas_archivosalumno on tareas_archivosalumno.idalumno=personal.idpersona
   # left join tareas_mensajes on tareas_mensajes.idtarea=tareas.idtarea
   # left join notas_quiz on notas_quiz.idalumno=personal.idpersona
where #proyecto.idproyecto=7 and
#bolsa_empresas.nombre like "%tweetalig%"
#and persona_rol.idrol=3
#and
# personal.usuario='TR1104417557'
personal.nombre like "%maria%" and
personal.ape_paterno like "%lopez%"
and personal.idpersona not in (447)
group by acad_matricula.idmatricula
#and personal.usuario not in ("admin","emmy","admineiger","LR1","LM47067438","VA76869895","DS46421771","WS77388057","MQ45118297","LM47000000")
#and personal.regfecha="2020-06-05"
#and personal.idpersona not in (3202,3201,4312)
;