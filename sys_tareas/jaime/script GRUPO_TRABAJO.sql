CREATE TABLE IF NOT EXISTS tb_grupo_trabajo(
    id_grupo_trabajo INT UNSIGNED NOT NULL AUTO_INCREMENT,
    idgrupoauladetalle bigint(20) NOT NULL,
    FOREIGN KEY (idgrupoauladetalle) REFERENCES acad_grupoauladetalle(idgrupoauladetalle)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    nombre VARCHAR(150) NOT NULL,
    PRIMARY KEY (id_grupo_trabajo),
    INDEX `idx_tb_grupo_trabajo_id_grupo_trabajo` (id_grupo_trabajo),
    INDEX `idx_tb_grupo_trabajo_acad_grupoauladetalle_idgrupoauladetalle` (idgrupoauladetalle)
) ENGINE = InnoDB;

ALTER TABLE `acad_matricula` ADD COLUMN `id_grupo_trabajo` INT UNSIGNED NULL AFTER `codigocursosence`,  
    ADD CONSTRAINT  `FK_acad_matricula_tb_grupo_trabajo` FOREIGN KEY (id_grupo_trabajo) REFERENCES tb_grupo_trabajo(id_grupo_trabajo)
        ON DELETE SET NULL
        ON UPDATE CASCADE;