UPDATE acad_curso_complementario
	inner join acad_grupoauladetalle on acad_grupoauladetalle.idgrupoaula=acad_curso_complementario.idgrupoaula
									and acad_curso_complementario.idcursoprincipal=acad_grupoauladetalle.idcurso
	inner join acad_curso on acad_curso.idcurso=acad_grupoauladetalle.idcurso
	inner join proyecto_cursos on proyecto_cursos.idcurso=acad_curso.idcurso
	inner join proyecto on proyecto.idproyecto=proyecto_cursos.idproyecto
SET acad_grupoauladetalle.idcomplementario=acad_curso_complementario.idcurso;
