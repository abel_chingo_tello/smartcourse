-- MySQL Script generated by MySQL Workbench
-- Mon May  4 15:41:05 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `portafolio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `portafolio` (
  `idportafolio` INT NOT NULL AUTO_INCREMENT,
  `idalumno` BIGINT(20) NOT NULL,
  `idproyecto` INT(11) NULL,
  PRIMARY KEY (`idportafolio`),
  INDEX `fk_portafolio_personal1_idx` (`idalumno` ASC))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `portafolio_folder`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `portafolio_folder` (
  `idfolder` INT NOT NULL AUTO_INCREMENT,
  `idfolder_padre` INT NULL,
  `nombre` VARCHAR(500) NOT NULL,
  `idportafolio` INT NOT NULL,
  PRIMARY KEY (`idfolder`),
  INDEX `fk_portafolio_folder_portafolio_folder_idx` (`idfolder_padre` ASC),
  INDEX `fk_portafolio_folder_portafolio1_idx` (`idportafolio` ASC))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `portafolio_archivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `portafolio_archivo` (
  `idarchivo` INT NOT NULL AUTO_INCREMENT,
  `idfolder` INT NOT NULL,
  `nombre` VARCHAR(500) NOT NULL,
  `ubicacion` TEXT NULL,
  `tipo` VARCHAR(100) NULL,
  `extension` VARCHAR(50) NULL,
  `idportafolio` INT NOT NULL,
  PRIMARY KEY (`idarchivo`),
  INDEX `fk_portafolio_archivo_portafolio_folder1_idx` (`idfolder` ASC),
  INDEX `fk_portafolio_archivo_portafolio1_idx` (`idportafolio` ASC))
ENGINE = MyISAM;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
