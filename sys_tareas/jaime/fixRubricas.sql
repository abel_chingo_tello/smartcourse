ALTER TABLE `bdskscourses-10-09-2020`.`rubrica_instancia` 
ADD COLUMN `idcurso` BIGINT(20) NOT NULL AFTER `idcomplementario`,
CHANGE COLUMN `idcursodetalle` `idcursodetalle` INT(11) NOT NULL ,
CHANGE COLUMN `idpestania` `idpestania` VARCHAR(45) CHARACTER SET 'latin1' NOT NULL DEFAULT 0 ,
CHANGE COLUMN `idcomplementario` `idcomplementario` BIGINT(20) NOT NULL DEFAULT 0 ;

ALTER TABLE `bdskscourses-10-09-2020`.`rubrica_indicador_alumno` 
ADD COLUMN `idgrupoauladetalle` BIGINT(20) NOT NULL AFTER `idrubrica_instancia`;
