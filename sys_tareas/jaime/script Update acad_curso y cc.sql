ALTER TABLE `acad_curso` ADD `avance_secuencial` TINYINT(1) NULL DEFAULT '0' AFTER `sks`;

ALTER TABLE `acad_curso_complementario` ADD `avance_secuencial` TINYINT(1) NULL DEFAULT '0' AFTER `tipo`;