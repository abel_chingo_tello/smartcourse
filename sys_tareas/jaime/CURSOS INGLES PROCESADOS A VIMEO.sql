select CURSOS_ELIMINAR.*, tmp_vimeo_log.static_path,tmp_vimeo_log.estado,tmp_vimeo_log.fecha_registro
from tmp_vimeo_log
	right join (
		SELECT * FROM (
			select 	tmp_curso_procesado.idcurso, tmp_curso_procesado.idcomplementario, 
					acad_curso.nombre, tmp_curso_procesado.fecha_registro,
					tmp_curso_procesado.videos_procesados 
			from tmp_curso_procesado
				inner join acad_curso 
				on tmp_curso_procesado.idcurso=acad_curso.idcurso 
				and tmp_curso_procesado.idcomplementario=0
			UNION
			select 	tmp_curso_procesado.idcurso, tmp_curso_procesado.idcomplementario,
					acad_curso_complementario.nombre, tmp_curso_procesado.fecha_registro,
					tmp_curso_procesado.videos_procesados 
			from tmp_curso_procesado
				inner join acad_curso_complementario 
					on tmp_curso_procesado.idcomplementario=acad_curso_complementario.idcurso 
					and tmp_curso_procesado.idcurso=acad_curso_complementario.idcursoprincipal
		) CURSOS_ELIMINAR
		WHERE 	nombre LIKE '%A1%' OR 
				nombre LIKE '%A2%' OR
				nombre LIKE '%B1%' OR
				nombre LIKE '%B2%' OR
				nombre LIKE '%C1%'
		ORDER BY nombre ASC
	) CURSOS_ELIMINAR
		on tmp_vimeo_log.idcurso=CURSOS_ELIMINAR.idcurso 
		and tmp_vimeo_log.idcomplementario=CURSOS_ELIMINAR.idcomplementario
order by idcurso desc