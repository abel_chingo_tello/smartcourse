-- Tue Apr  7 18:41:32 2020


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `evento_programacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `evento_programacion` (
  `id_programacion` INT NOT NULL,
  `fecha` DATE NOT NULL,
  `titulo` VARCHAR(250) NOT NULL,
  `detalle` TEXT NOT NULL,
  `hora_comienzo` TIME NOT NULL,
  `hora_fin` TIME NOT NULL,
  `idpersona` BIGINT(20) NOT NULL,
  `idgrupoaula` INT(11) NULL,
  `idgrupoauladetalle` BIGINT(20) NULL,
  PRIMARY KEY (`id_programacion`),
  INDEX `fk_evento_programacion_acad_grupoaula1_idx` (`idgrupoaula` ASC),
  INDEX `fk_evento_programacion_acad_grupoauladetalle1_idx` (`idgrupoauladetalle` ASC),
  INDEX `fk_evento_programacion_personal1_idx` (`idpersona` ASC),
  CONSTRAINT `fk_evento_programacion_acad_grupoaula1`
    FOREIGN KEY (`idgrupoaula`)
    REFERENCES `acad_grupoaula` (`idgrupoaula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_evento_programacion_acad_grupoauladetalle1`
    FOREIGN KEY (`idgrupoauladetalle`)
    REFERENCES `acad_grupoauladetalle` (`idgrupoauladetalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_evento_programacion_personal1`
    FOREIGN KEY (`idpersona`)
    REFERENCES `personal` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = MyISAM;

INSERT INTO `modulos` (`idmodulo`, `nombre`, `icono`, `link`, `estado`, `modulopadre`) VALUES ('27', 'Eventos', 'fa fa-calendar-check-o', 'eventos', '1', '0');
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
