ALTER TABLE `aceglob4_smart_course`.actividad_alumno ADD INDEX idx_nombre(iddetalleactividad, idalumno,idcurso,idcc,idproyecto);

ALTER TABLE `aceglob4_skscourses_english_adultos`.actividad_detalle ADD INDEX idx_idactividad(idactividad);
ALTER TABLE `aceglob4_skscourses_english_adultos`.actividad_detalle ADD INDEX idx_iddetalle(iddetalle);

ALTER TABLE `aceglob4_skscourses_english_adultos`.actividades ADD INDEX idx_idactividad(idactividad);
ALTER TABLE `aceglob4_skscourses_english_adultos`.actividades ADD INDEX idx_sesion_metodologia(sesion,metodologia);

ALTER TABLE `aceglob4_skscourses_english_adolescentes`.actividad_detalle ADD INDEX idx_idactividad(idactividad);
ALTER TABLE `aceglob4_skscourses_english_adolescentes`.actividad_detalle ADD INDEX idx_iddetalle(iddetalle);

ALTER TABLE `aceglob4_skscourses_english_adolescentes`.actividades ADD INDEX idx_idactividad(idactividad);
ALTER TABLE `aceglob4_skscourses_english_adolescentes`.actividades ADD INDEX idx_sesion_metodologia(sesion,metodologia);