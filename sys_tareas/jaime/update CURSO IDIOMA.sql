ALTER TABLE `acad_curso` ADD `idioma` VARCHAR(45) NULL DEFAULT 'ES' AFTER `sks`;

ALTER TABLE `acad_curso_complementario` ADD `idioma` VARCHAR(45) NULL DEFAULT 'ES' AFTER `tipo`;