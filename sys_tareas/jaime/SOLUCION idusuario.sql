UPDATE  acad_matricula
	inner join acad_grupoauladetalle on acad_grupoauladetalle.idgrupoauladetalle=acad_matricula.idgrupoauladetalle
	inner join acad_curso on acad_curso.idcurso=acad_grupoauladetalle.idcurso
	inner join proyecto_cursos on proyecto_cursos.idcurso=acad_curso.idcurso
	inner join proyecto on proyecto.idproyecto=proyecto_cursos.idproyecto
	inner join bolsa_empresas on proyecto.idempresa=bolsa_empresas.idempresa
set acad_matricula.idusuario=bolsa_empresas.idpersona;
