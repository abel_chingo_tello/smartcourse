ALTER TABLE `acad_competencias` ADD `idproyecto` INT  NULL AFTER `estado`;
ALTER TABLE `acad_capacidades` ADD `idproyecto` INT  NULL AFTER `nombre`;
ALTER TABLE `acad_criterios` ADD `idproyecto` INT  NULL AFTER `estado`;

UPDATE `acad_criterios` SET `idproyecto` = '7';
UPDATE `acad_competencias` SET `idproyecto` = '7';
UPDATE `acad_capacidades` SET `idproyecto` = '7';
INSERT INTO `modulos` VALUES ('20', 'MINEDU', 'fa fa-handshake-o', '#', '1' ,0);
INSERT INTO `modulos` (`idmodulo`, `nombre`, `icono`, `link`, `estado`, `modulopadre`) VALUES ('21', 'Competencias', 'fa fa-bank', 'acad_competencias', '1', '20'), ('22', 'Capacidades', 'fa fa-bank', 'acad_capacidades', '1', '20'), ('23', 'Criterios', 'fa fa-bank', 'acad_criterios', '1', '20');

#26-03-2020
ALTER TABLE `personal` ADD `emailpadre` VARCHAR(250) NULL AFTER `esdemo`;