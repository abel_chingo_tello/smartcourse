<?php
header('Content-Type: application/json');
if(empty($_POST)){
    echo json_encode(array('code'=>'Error','msj'=>'data incomplete'));
    exit();            
}
@extract($_POST);
$servidor=!empty($servidor)?$servidor:'localhost';
$basedatos=!empty($basedatos)?$basedatos:'database1';
$usuario=!empty($usuario)?$usuario:'root';
$clave=!empty($clave)?$clave:'';
// validar conexion	
	try{
		mysqli_report(MYSQLI_REPORT_STRICT);
		$mysqli = new mysqli($servidor, $usuario, $clave, $basedatos);		
	}catch(Exception $ex){
		echo json_encode(array('code'=>'Error','msj'=>'Fallo al conectar a MySQL'));
    	exit();
	}
if(empty($accion)){
    echo json_encode(array('code'=>'Error','msj'=>'Falto Activar Accion'));
    exit();
}else if($accion=='testconexion'){
  echo json_encode(array('code'=>200,'msj'=>'Conexion Correcta'));
  exit();   
}else if($accion=='importarsql'){// sobreescribir data_base
    $mysqli->set_charset('utf8');
    $query = '';
    $sqlScript = file('sistema.sql');
    foreach ($sqlScript as $line)   {
            
            $startWith = substr(trim($line), 0 ,2);
            $endWith = substr(trim($line), -1 ,1);
            
            if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
                    continue;
            }
                    
            $query = $query . $line;
            if ($endWith == ';') {
                    mysqli_query($mysqli,$query) or die('<div class="error-response sql-import-response">Problem in executing the SQL query <b>' . $query. '</b></div>');
                    $query= '';             
            }
    }
    echo json_encode(array('code'=>200,'msj'=>'Importación de Base de datos Correcta'));
    exit();
}else if($accion=='rescribiendoarchivos'){// sobreescribir archivos de configuración
    $file = file("../sys_datos/cls.DatBase.php");
    array_splice($file, 17, 0); // borra la tercer linea del array
    $file[17]='             $this->oBD = BDMySQLI::getInstancia(\''.$servidor.'\',\''.$basedatos.'\',\''.$usuario.'\',\''.$clave.'\');
    ';
    $archivo = fopen("../sys_datos/cls.DatBase.php", "w+b");   
     foreach($file as $linea ) // Guardar los cambios en el archivo:
         fwrite($archivo, $linea);
    fclose($archivo);

    $file = file("../admin/ini_app.php");
    array_splice($file, 19, 0); // borra la tercer linea del array
    $dirfile=$_SERVER['REQUEST_URI'];
    $dirfile = substr($dirfile, 0, strpos($dirfile, "sys_tareas"));
    //preg_replace ('/ By. * /', '', $ variable);
    $file[11]='define(\'_sitio_\',\'admin\');
    ';
    $file[22]='define(\'URL_BASE\',_HOST_.\''.$dirfile.'\');
    ';
    $archivo = fopen("../admin/ini_app.php", "w+b");   
     foreach($file as $linea ) // Guardar los cambios en el archivo:
         fwrite($archivo, $linea);
    fclose($archivo);
    $file[11]='define(\'_sitio_\',\'json\');
    ';
    $archivo = fopen("../json/ini_app.php", "w+b");   
     foreach($file as $linea ) // Guardar los cambios en el archivo:
         fwrite($archivo, $linea);
    fclose($archivo);    
    $file[11]='define(\'_sitio_\',\'frontend\');
    ';
    $file[26]='define(\'IS_LOGIN\',false);
    ';
    $archivo = fopen("../frontend/ini_app.php", "w+b");   
     foreach($file as $linea ) // Guardar los cambios en el archivo:
         fwrite($archivo, $linea);
    fclose($archivo);
    @copy("../_.htaccess","../.htaccess");   
    @copy("../okindex.php","../index.php");

    echo json_encode(array('code'=>200,'msj'=>'Se configuraron los archivos'));
    exit();
}


?>