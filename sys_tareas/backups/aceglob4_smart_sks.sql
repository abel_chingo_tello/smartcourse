-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 02-04-2020 a las 16:26:26
-- Versión del servidor: 5.6.41-84.1
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aceglob4_smart_sks`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acceso`
--

CREATE TABLE `acceso` (
  `id` bigint(20) NOT NULL,
  `idrepresentante` bigint(20) NOT NULL DEFAULT '0',
  `idproyectoplataforma` bigint(20) DEFAULT '0',
  `idempresaplataforma` bigint(20) DEFAULT '0',
  `idproducto_curso` bigint(20) NOT NULL DEFAULT '0',
  `estado` int(11) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `acceso`
--

INSERT INTO `acceso` (`id`, `idrepresentante`, `idproyectoplataforma`, `idempresaplataforma`, `idproducto_curso`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 1, 3, 4, 51, 1, '2020-02-06 17:22:32', '2020-02-06 17:54:55'),
(2, 1, 3, 4, 52, 1, '2020-02-06 17:23:00', '2020-02-06 17:54:59'),
(3, 1, 3, 4, 53, 1, '2020-02-06 17:23:08', '2020-02-06 17:55:00'),
(4, 1, 3, 4, 54, 1, '2020-02-06 17:23:19', '2020-02-06 17:55:01'),
(5, 1, 3, 4, 55, 1, '2020-02-06 17:23:27', '2020-02-06 17:55:02'),
(6, 2, 7, 10, 24, 1, '2020-02-07 11:53:32', '2020-02-11 11:57:02'),
(7, 2, 7, 10, 25, 1, '2020-02-07 11:53:42', '2020-02-11 11:57:13'),
(8, 2, 7, 10, 26, 1, '2020-02-11 11:57:26', '2020-02-11 11:57:26'),
(9, 2, 7, 10, 27, 1, '2020-02-11 11:57:34', '2020-02-11 11:57:34'),
(10, 2, 7, 10, 28, 1, '2020-02-11 11:57:42', '2020-02-11 11:57:42'),
(11, 2, 7, 10, 29, 1, '2020-02-11 11:57:51', '2020-02-11 11:57:51'),
(12, 2, 7, 10, 2, 1, '2020-02-11 11:57:51', '2020-02-11 11:57:51'),
(13, 2, 7, 10, 6, 1, '2020-02-11 11:57:51', '2020-02-11 11:57:51'),
(14, 2, 7, 10, 22, 1, '2020-02-11 11:57:51', '2020-02-11 11:57:51'),
(15, 2, 7, 10, 31, 1, '2020-02-11 11:57:51', '2020-02-11 11:57:51'),
(16, 2, 7, 10, 61, 1, '2020-02-25 13:01:35', '2020-02-25 13:01:35'),
(17, 2, 7, 10, 62, 1, '2020-02-25 13:01:35', '2020-02-25 13:01:35'),
(18, 3, 5, 6, 18, 1, '2020-02-28 12:53:01', '2020-02-28 12:53:01'),
(19, 3, 5, 6, 25, 1, '2020-02-28 13:03:25', '2020-02-28 13:03:25'),
(20, 3, 5, 6, 26, 1, '2020-02-28 13:03:25', '2020-02-28 13:03:25'),
(21, 3, 5, 6, 28, 1, '2020-02-28 13:03:25', '2020-02-28 13:03:25'),
(22, 3, 5, 6, 47, 1, '2020-02-28 13:03:25', '2020-02-28 13:03:25'),
(23, 3, 5, 6, 48, 1, '2020-02-28 13:03:25', '2020-02-28 13:03:25'),
(24, 3, 5, 6, 49, 1, '2020-02-28 13:03:25', '2020-02-28 13:03:25'),
(25, 3, 5, 6, 61, 1, '2020-02-28 13:03:25', '2020-02-28 13:03:25'),
(26, 3, 5, 6, 63, 1, '2020-02-28 13:29:30', '2020-02-28 13:29:30'),
(27, 3, 5, 6, 64, 1, '2020-02-28 13:29:30', '2020-02-28 13:29:30'),
(28, 3, 5, 6, 65, 1, '2020-02-28 13:29:30', '2020-02-28 13:29:30'),
(29, 2, 7, 10, 63, 1, '2020-02-25 13:01:35', '2020-02-25 13:01:35'),
(30, 2, 7, 10, 66, 1, '2020-03-25 09:53:45', '2020-03-25 09:53:45'),
(31, 2, 7, 10, 67, 1, '2020-03-28 09:47:28', '2020-03-28 09:47:28'),
(32, 4, 22, 23, 29, 1, '2020-03-31 19:47:29', '2020-04-01 08:05:50'),
(33, 4, 22, 23, 66, 1, '2020-03-25 09:53:45', '2020-03-25 09:53:45'),
(34, 4, 22, 23, 61, 1, '2020-03-25 09:53:45', '2020-04-01 07:43:01'),
(35, 4, 22, 23, 68, 1, '2020-04-02 10:36:21', '2020-04-02 10:36:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambito_representantes`
--

CREATE TABLE `ambito_representantes` (
  `id` bigint(20) NOT NULL,
  `idregion` bigint(20) DEFAULT NULL,
  `idpais` bigint(20) DEFAULT NULL,
  `iddepartamento` bigint(20) DEFAULT NULL,
  `idprovincia` bigint(20) DEFAULT NULL,
  `idciudad` bigint(20) DEFAULT NULL,
  `idrepresentante` bigint(20) DEFAULT '0',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `tabla` varchar(50) NOT NULL,
  `id` text NOT NULL,
  `sql_str` longtext NOT NULL,
  `descripcion` text NOT NULL,
  `valor` int(11) DEFAULT NULL,
  `razon` varchar(150) DEFAULT NULL,
  `data` text,
  `persona` bigint(20) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`tabla`, `id`, `sql_str`, `descripcion`, `valor`, `razon`, `data`, `persona`, `fecha_hora`) VALUES
('matriculas', '4', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, idinscripcion, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, id) VALUES(\'2\', \'7\', 30, \'2\', \'3\', \'4\', \'2\', \'2020-02-22\', \'2020-03-22\', \'1\', 559, 4)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"4\",\"id_matricula\":[4]}', 3, '2020-02-22 22:01:49'),
('matricula_cuota_depago', '0', 'INSERT INTO matricula_cuota_depago (idmatricula, numero, descripcion, monto, tipo, url, fecha_vencimiento, fecha_registro, estado, id) VALUES(4, 1, \'Material (100.00%)\', 40, 1, \'https://skscourses.com/json/\', \'2020-02-22\', \'2020-02-22 15:01:49\', \'1\', 3)', 'Se registro 0 Estudiantes', 0, 'Estudiantes', '{\"estado\":1,\"os_curso\":1,\"id_matricula\":[]}', 3, '2020-02-22 22:01:49'),
('matriculas', '5', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, id) VALUES(\'2\', 9, 31, \'3\', \'6\', \'2\', \'2020-02-24\', \'2020-03-24\', \'1\', 560, 5)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[5]}', 3, '2020-02-25 00:56:01'),
('docentes', '3', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 10, 32, \'3\', \'2\', \'2020-02-24\', \'2020-03-24\', \'1\', 3)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[3]}', 3, '2020-02-25 01:14:59'),
('docentes', '4', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 11, 33, \'3\', \'2\', \'2020-02-24\', \'2020-03-24\', \'1\', 4)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[4]}', 3, '2020-02-25 01:16:40'),
('docentes', '5', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 12, 34, \'3\', \'2\', \'2020-02-24\', \'2020-03-24\', \'1\', 5)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[5]}', 3, '2020-02-25 01:16:48'),
('matriculas', '6', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, idinscripcion, id) VALUES(\'2\', \'13\', 35, \'3\', \'6\', \'2\', \'2020-02-25\', \'2020-03-25\', \'1\', 561, \'4\', 6)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[6]}', 3, '2020-02-25 22:34:03'),
('matriculas', '7', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, id) VALUES(\'2\', 14, 36, \'3\', \'6\', \'2\', \'2020-02-25\', \'2020-03-26\', \'1\', 562, 7)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[7]}', 3, '2020-02-25 23:03:14'),
('matriculas', '8', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, id) VALUES(\'2\', 15, 37, \'3\', \'6\', \'2\', \'2020-02-25\', \'2020-03-25\', \'1\', 563, 8)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[8]}', 3, '2020-02-25 23:10:16'),
('matriculas', '14', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, id) VALUES(\'2\', 21, 43, \'3\', \'6\', \'2\', \'2020-02-25\', \'2020-03-25\', \'1\', 569, 14)', 'Se registro 6 Estudiantes', 6, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[9,10,11,12,13,14]}', 3, '2020-02-26 01:30:24'),
('matriculas', '20', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, id) VALUES(\'2\', 27, 49, \'3\', \'6\', \'2\', \'2020-02-26\', \'2020-03-26\', \'1\', 575, 20)', 'Se registro 6 Estudiantes', 6, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[15,16,17,18,19,20]}', 3, '2020-02-26 16:03:24'),
('matriculas', '21', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, id) VALUES(\'2\', 28, 52, \'3\', \'6\', \'2\', \'2020-02-26\', \'2020-03-26\', \'1\', 587, 21)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[21]}', 3, '2020-02-27 01:29:05'),
('docentes', '6', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 29, 53, \'3\', \'2\', \'2020-02-26\', \'2020-03-26\', \'1\', 6)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[6]}', 3, '2020-02-27 01:32:37'),
('matriculas', '22', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, idinscripcion, id) VALUES(\'2\', \'30\', 54, \'3\', \'6\', \'2\', \'2020-02-26\', \'2020-03-26\', \'1\', 588, \'5\', 22)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[22]}', 3, '2020-02-27 02:31:56'),
('', '0', '', 'Se registro 0 Estudiantes', 0, 'Estudiantes', '{\"estado\":1,\"os_curso\":1,\"id_matricula\":[]}', 3, '2020-02-27 02:45:48'),
('matriculas', '23', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, id) VALUES(\'2\', 31, 55, \'3\', \'2\', \'2\', \'2020-02-26\', \'2020-03-26\', \'1\', 589, 23)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"2\",\"id_matricula\":[23]}', 3, '2020-02-27 03:06:14'),
('matriculas', '24', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, idmatricula_externa, idinscripcion, id) VALUES(\'2\', \'32\', 56, \'3\', \'4\', \'2\', \'2020-02-27\', \'2020-03-27\', \'1\', 590, \'6\', 24)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"4\",\"id_matricula\":[24]}', 3, '2020-02-27 15:41:27'),
('matricula_pago_detalle', '0', 'INSERT INTO matricula_pago_detalle (pago, cuota, descripcion, monto, fecha_pago, id) VALUES(21, 40, \'Material\', 60, \'2020-02-27 08:41:27\', 36)', 'Se registro 0 Estudiantes', 0, 'Estudiantes', '{\"estado\":1,\"os_curso\":1,\"id_matricula\":[]}', 3, '2020-02-27 15:41:28'),
('bitacora', '0', 'INSERT INTO bitacora (tabla, id, sql_str, descripcion, persona, fecha_hora, valor, data, razon) VALUES(\'matricula_pago_detalle\', 0, \'INSERT INTO matricula_pago_detalle (pago, cuota, descripcion, monto, fecha_pago, id) VALUES(21, 40, \\\'Material\\\', 60, \\\'2020-02-27 08:41:27\\\', 36)\', \'Se registro 0 Estudiantes\', \'3\', \'2020-02-27 08:41:28\', 0, \'{\\\"estado\\\":1,\\\"os_curso\\\":1,\\\"id_matricula\\\":[]}\', \'Estudiantes\')', 'Se registro 0 Estudiantes', 0, 'Estudiantes', '{\"estado\":1,\"os_curso\":1,\"id_matricula\":[]}', 3, '2020-02-27 15:41:28'),
('persona', '4', 'INSERT INTO persona (idpersona, tipodoc, dni, primer_ape, segundo_ape, nombre, sexo, telefono, email, usuario, clave, idrepresentante, estado) VALUES(4, \'1\', \'12345678\', \'Principal\', \'Lima\', \'Contador\', \'M\', \'999888777\', \'jpsvenegasc@gmail.com\', \'CP12345678\', \'25d55ad283aa400af464c76d713c07ad\', \'2\', \'1\')', 'Se registro un usuario: CP12345678', NULL, NULL, '{\"idpersona\":4,\"usuario\":\"CP12345678\",\"idrol\":\"5\"}', 3, '2020-02-27 21:30:21'),
('matriculas', '25', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'2\', 33, 58, \'3\', \'11\', \'2\', \'2020-02-28\', \'2020-03-28\', \'1\', \'23\', 591, 25)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"11\",\"id_matricula\":[25]}', 3, '2020-02-28 23:54:22'),
('matriculas', '26', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, idinscripcion, id) VALUES(\'2\', \'33\', \'58\', \'3\', \'6\', \'2\', \'2020-02-28\', \'2020-03-28\', \'1\', \'23\', 592, \'7\', 26)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[26]}', 3, '2020-02-28 23:56:33'),
('docentes', '7', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 36, 60, \'5\', \'2\', \'2020-03-04\', \'2020-04-04\', \'1\', 7)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[7]}', 5, '2020-03-05 00:24:41'),
('docentes', '8', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 37, 61, \'5\', \'2\', \'2020-03-04\', \'2020-04-04\', \'1\', 8)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[8]}', 5, '2020-03-05 00:52:51'),
('docentes', '9', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 39, 62, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 9)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[9]}', 5, '2020-03-05 17:14:05'),
('docentes', '10', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 40, 63, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 10)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10]}', 5, '2020-03-05 19:16:59'),
('docentes', '11', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 41, 64, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 11)', 'Se registro 2 Docentes', 2, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11]}', 5, '2020-03-05 19:16:59'),
('docentes', '12', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 42, 65, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 12)', 'Se registro 3 Docentes', 3, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12]}', 5, '2020-03-05 19:16:59'),
('docentes', '13', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 43, 66, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 13)', 'Se registro 4 Docentes', 4, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13]}', 5, '2020-03-05 19:16:59'),
('docentes', '14', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 44, 67, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 14)', 'Se registro 5 Docentes', 5, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14]}', 5, '2020-03-05 19:16:59'),
('docentes', '15', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 45, 68, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 15)', 'Se registro 6 Docentes', 6, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15]}', 5, '2020-03-05 19:16:59'),
('docentes', '16', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 46, 69, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 16)', 'Se registro 7 Docentes', 7, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16]}', 5, '2020-03-05 19:16:59'),
('docentes', '17', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 47, 70, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 17)', 'Se registro 8 Docentes', 8, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16,17]}', 5, '2020-03-05 19:16:59'),
('docentes', '18', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 48, 71, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 18)', 'Se registro 9 Docentes', 9, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16,17,18]}', 5, '2020-03-05 19:16:59'),
('docentes', '19', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 49, 72, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 19)', 'Se registro 10 Docentes', 10, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16,17,18,19]}', 5, '2020-03-05 19:16:59'),
('docentes', '20', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 50, 73, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 20)', 'Se registro 11 Docentes', 11, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16,17,18,19,20]}', 5, '2020-03-05 19:16:59'),
('docentes', '21', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 51, 74, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 21)', 'Se registro 12 Docentes', 12, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16,17,18,19,20,21]}', 5, '2020-03-05 19:16:59'),
('docentes', '22', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 52, 75, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 22)', 'Se registro 13 Docentes', 13, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16,17,18,19,20,21,22]}', 5, '2020-03-05 19:16:59'),
('docentes', '23', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 53, 76, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 23)', 'Se registro 14 Docentes', 14, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16,17,18,19,20,21,22,23]}', 5, '2020-03-05 19:16:59'),
('docentes', '24', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 54, 77, \'5\', \'2\', \'2020-03-05\', \'2020-04-05\', \'1\', 24)', 'Se registro 15 Docentes', 15, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]}', 5, '2020-03-05 19:16:59'),
('matriculas', '56', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 84, 110, \'5\', \'14\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'12\', 634, 56)', 'Se registro 30 Estudiantes', 30, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"14\",\"id_matricula\":[27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56]}', 5, '2020-03-05 21:58:29'),
('matriculas', '86', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'84\', \'110\', \'5\', \'18\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'12\', 664, 86)', 'Se registro 30 Estudiantes', 30, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"18\",\"id_matricula\":[57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86]}', 5, '2020-03-05 21:58:36'),
('matriculas', '116', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'84\', \'110\', \'5\', \'21\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'12\', 694, 116)', 'Se registro 30 Estudiantes', 30, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"21\",\"id_matricula\":[87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116]}', 5, '2020-03-05 21:58:42'),
('matriculas', '136', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 104, 130, \'5\', \'15\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'12\', 714, 136)', 'Se registro 20 Estudiantes', 20, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"15\",\"id_matricula\":[117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136]}', 5, '2020-03-05 22:55:18'),
('matriculas', '156', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'104\', \'130\', \'5\', \'19\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'12\', 734, 156)', 'Se registro 20 Estudiantes', 20, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"19\",\"id_matricula\":[137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156]}', 5, '2020-03-05 22:55:22'),
('matriculas', '176', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'104\', \'130\', \'5\', \'22\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'12\', 754, 176)', 'Se registro 20 Estudiantes', 20, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"22\",\"id_matricula\":[157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176]}', 5, '2020-03-05 22:55:27'),
('matriculas', '186', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 114, 140, \'5\', \'15\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'13\', 764, 186)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"15\",\"id_matricula\":[177,178,179,180,181,182,183,184,185,186]}', 5, '2020-03-05 23:09:46'),
('matriculas', '196', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'114\', \'140\', \'5\', \'19\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'13\', 774, 196)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"19\",\"id_matricula\":[187,188,189,190,191,192,193,194,195,196]}', 5, '2020-03-05 23:09:48'),
('matriculas', '206', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'114\', \'140\', \'5\', \'22\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'13\', 784, 206)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"22\",\"id_matricula\":[197,198,199,200,201,202,203,204,205,206]}', 5, '2020-03-05 23:09:50'),
('matriculas', '216', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 124, 150, \'5\', \'13\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'15\', 794, 216)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"13\",\"id_matricula\":[207,208,209,210,211,212,213,214,215,216]}', 5, '2020-03-05 23:17:02'),
('matriculas', '226', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'124\', \'150\', \'5\', \'17\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'15\', 804, 226)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"17\",\"id_matricula\":[217,218,219,220,221,222,223,224,225,226]}', 5, '2020-03-05 23:17:05'),
('matriculas', '236', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'124\', \'150\', \'5\', \'23\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'15\', 814, 236)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"23\",\"id_matricula\":[227,228,229,230,231,232,233,234,235,236]}', 5, '2020-03-05 23:17:07'),
('matriculas', '246', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 134, 160, \'5\', \'13\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'16\', 824, 246)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"13\",\"id_matricula\":[237,238,239,240,241,242,243,244,245,246]}', 5, '2020-03-05 23:20:32'),
('matriculas', '256', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'134\', \'160\', \'5\', \'17\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'16\', 834, 256)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"17\",\"id_matricula\":[247,248,249,250,251,252,253,254,255,256]}', 5, '2020-03-05 23:20:35'),
('matriculas', '266', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'134\', \'160\', \'5\', \'23\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'16\', 844, 266)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"23\",\"id_matricula\":[257,258,259,260,261,262,263,264,265,266]}', 5, '2020-03-05 23:20:37'),
('matriculas', '276', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 144, 171, \'5\', \'14\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'13\', 856, 276)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"14\",\"id_matricula\":[267,268,269,270,271,272,273,274,275,276]}', 5, '2020-03-06 00:48:30'),
('matriculas', '286', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'144\', \'171\', \'5\', \'18\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'13\', 866, 286)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"18\",\"id_matricula\":[277,278,279,280,281,282,283,284,285,286]}', 5, '2020-03-06 00:48:33'),
('matriculas', '296', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'144\', \'171\', \'5\', \'21\', \'2\', \'2020-03-09\', \'2020-12-31\', \'1\', \'13\', 876, 296)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"21\",\"id_matricula\":[287,288,289,290,291,292,293,294,295,296]}', 5, '2020-03-06 00:48:35'),
('matriculas', '306', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 154, 181, \'5\', \'14\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'11\', 886, 306)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"14\",\"id_matricula\":[297,298,299,300,301,302,303,304,305,306]}', 5, '2020-03-06 00:50:13'),
('matriculas', '307', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'145\', \'172\', \'5\', \'18\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'11\', 887, 307)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"18\",\"id_matricula\":[307]}', 5, '2020-03-06 00:50:15'),
('matriculas', '308', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'145\', \'172\', \'5\', \'21\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'11\', 888, 308)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"21\",\"id_matricula\":[308]}', 5, '2020-03-06 00:50:15'),
('matriculas', '318', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 164, 191, \'5\', \'14\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 898, 318)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"14\",\"id_matricula\":[309,310,311,312,313,314,315,316,317,318]}', 5, '2020-03-06 00:51:47'),
('matriculas', '328', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'164\', \'191\', \'5\', \'18\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 908, 328)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"18\",\"id_matricula\":[319,320,321,322,323,324,325,326,327,328]}', 5, '2020-03-06 00:51:49'),
('matriculas', '338', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'164\', \'191\', \'5\', \'21\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 918, 338)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"21\",\"id_matricula\":[329,330,331,332,333,334,335,336,337,338]}', 5, '2020-03-06 00:51:52'),
('matriculas', '36', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 64, 171, \'5\', \'14\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'13\', 856, 36)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"14\",\"id_matricula\":[27,28,29,30,31,32,33,34,35,36]}', 5, '2020-03-06 16:57:52'),
('matriculas', '46', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'64\', \'171\', \'5\', \'18\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'13\', 866, 46)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"18\",\"id_matricula\":[37,38,39,40,41,42,43,44,45,46]}', 5, '2020-03-06 16:57:54'),
('matriculas', '56', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'64\', \'171\', \'5\', \'21\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'13\', 876, 56)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"21\",\"id_matricula\":[47,48,49,50,51,52,53,54,55,56]}', 5, '2020-03-06 16:57:56'),
('matriculas', '66', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 74, 181, \'5\', \'15\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 886, 66)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"15\",\"id_matricula\":[57,58,59,60,61,62,63,64,65,66]}', 5, '2020-03-06 17:01:16'),
('matriculas', '76', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'74\', \'181\', \'5\', \'19\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 896, 76)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"19\",\"id_matricula\":[67,68,69,70,71,72,73,74,75,76]}', 5, '2020-03-06 17:01:18'),
('matriculas', '86', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'74\', \'181\', \'5\', \'22\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 906, 86)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"22\",\"id_matricula\":[77,78,79,80,81,82,83,84,85,86]}', 5, '2020-03-06 17:01:20'),
('matriculas', '96', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 84, 191, \'5\', \'15\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'13\', 916, 96)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"15\",\"id_matricula\":[87,88,89,90,91,92,93,94,95,96]}', 5, '2020-03-06 17:03:02'),
('matriculas', '106', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'84\', \'191\', \'5\', \'19\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'13\', 926, 106)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"19\",\"id_matricula\":[97,98,99,100,101,102,103,104,105,106]}', 5, '2020-03-06 17:03:05'),
('matriculas', '116', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'84\', \'191\', \'5\', \'22\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'13\', 936, 116)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"22\",\"id_matricula\":[107,108,109,110,111,112,113,114,115,116]}', 5, '2020-03-06 17:03:07'),
('matriculas', '126', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 94, 201, \'5\', \'13\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'15\', 946, 126)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"13\",\"id_matricula\":[117,118,119,120,121,122,123,124,125,126]}', 5, '2020-03-06 17:04:24'),
('matriculas', '136', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'94\', \'201\', \'5\', \'17\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'15\', 956, 136)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"17\",\"id_matricula\":[127,128,129,130,131,132,133,134,135,136]}', 5, '2020-03-06 17:04:26'),
('matriculas', '146', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'94\', \'201\', \'5\', \'23\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'15\', 966, 146)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"23\",\"id_matricula\":[137,138,139,140,141,142,143,144,145,146]}', 5, '2020-03-06 17:04:28'),
('matriculas', '156', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 104, 211, \'5\', \'13\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'16\', 976, 156)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"13\",\"id_matricula\":[147,148,149,150,151,152,153,154,155,156]}', 5, '2020-03-06 17:06:01'),
('matriculas', '166', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'104\', \'211\', \'5\', \'17\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'16\', 986, 166)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"17\",\"id_matricula\":[157,158,159,160,161,162,163,164,165,166]}', 5, '2020-03-06 17:06:03'),
('matriculas', '176', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'104\', \'211\', \'5\', \'23\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'16\', 996, 176)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"23\",\"id_matricula\":[167,168,169,170,171,172,173,174,175,176]}', 5, '2020-03-06 17:06:05'),
('matriculas', '186', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 114, 221, \'5\', \'14\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'11\', 1006, 186)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"14\",\"id_matricula\":[177,178,179,180,181,182,183,184,185,186]}', 5, '2020-03-06 17:07:42'),
('matriculas', '196', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'114\', \'221\', \'5\', \'18\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'11\', 1016, 196)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"18\",\"id_matricula\":[187,188,189,190,191,192,193,194,195,196]}', 5, '2020-03-06 17:07:45'),
('matriculas', '206', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'114\', \'221\', \'5\', \'21\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'11\', 1026, 206)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"21\",\"id_matricula\":[197,198,199,200,201,202,203,204,205,206]}', 5, '2020-03-06 17:07:47'),
('matriculas', '216', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', 124, 231, \'5\', \'14\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 1036, 216)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"14\",\"id_matricula\":[207,208,209,210,211,212,213,214,215,216]}', 5, '2020-03-06 17:08:55'),
('matriculas', '226', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'124\', \'231\', \'5\', \'18\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 1046, 226)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"18\",\"id_matricula\":[217,218,219,220,221,222,223,224,225,226]}', 5, '2020-03-06 17:08:57'),
('matriculas', '236', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'3\', \'124\', \'231\', \'5\', \'21\', \'2\', \'2020-03-03\', \'2020-12-31\', \'1\', \'12\', 1056, 236)', 'Se registro 10 Estudiantes', 10, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"21\",\"id_matricula\":[227,228,229,230,231,232,233,234,235,236]}', 5, '2020-03-06 17:08:59'),
('matriculas', '237', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'2\', 125, 232, \'3\', \'6\', \'2\', \'2020-03-09\', \'2020-04-09\', \'1\', \'24\', 1057, 237)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[237]}', 3, '2020-03-09 15:33:22'),
('docentes', '25', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'3\', \'1\', 126, 233, \'5\', \'2\', \'2020-03-13\', \'2020-04-13\', \'1\', 25)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[25]}', 5, '2020-03-13 18:56:05'),
('matriculas', '238', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'2\', 127, 234, \'3\', \'11\', \'2\', \'2020-03-14\', \'2020-04-22\', \'1\', \'24\', 1058, 238)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"11\",\"id_matricula\":[238]}', 3, '2020-03-14 17:03:49'),
('', '0', '', 'Se registro 0 Estudiantes', 0, 'Estudiantes', '{\"estado\":1,\"os_curso\":1,\"id_matricula\":[]}', 3, '2020-03-14 17:10:37'),
('matriculas', '239', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, idinscripcion, id) VALUES(\'2\', \'127\', \'234\', \'3\', \'11\', \'2\', \'2020-12-31\', \'2020-04-14\', \'1\', \'23\', 1063, \'12\', 239)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"11\",\"id_matricula\":[239]}', 3, '2020-03-14 17:17:02'),
('docentes', '26', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 128, 235, \'3\', \'2\', \'2020-03-18\', \'2020-04-18\', \'1\', 26)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[26]}', 3, '2020-03-18 17:04:57'),
('docentes', '27', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 129, 236, \'3\', \'2\', \'2020-03-18\', \'2020-04-18\', \'1\', 27)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[27]}', 3, '2020-03-18 22:34:24'),
('matriculas', '240', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, idinscripcion, id) VALUES(\'2\', \'130\', 237, \'3\', \'11\', \'2\', \'2020-07-18\', \'2020-04-18\', \'1\', \'29\', 1064, \'13\', 240)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"11\",\"id_matricula\":[240]}', 3, '2020-03-19 01:22:54'),
('matriculas', '241', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, idinscripcion, id) VALUES(\'2\', \'131\', 237, \'3\', \'11\', \'2\', \'2020-07-18\', \'2020-04-18\', \'1\', \'24\', 1064, \'14\', 241)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"11\",\"id_matricula\":[241]}', 3, '2020-03-19 02:36:32'),
('matriculas', '242', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, estado, edad_registro, idmatricula_externa, idinscripcion, id) VALUES(\'2\', \'131\', \'237\', \'3\', \'6\', \'2\', \'1\', \'24\', 1066, \'14\', 242)', 'Se registro 1 Estudiantes', 1, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[242]}', 3, '2020-03-20 01:26:10'),
('', '0', '', 'Se registro 0 Estudiantes', 0, 'Estudiantes', '{\"estado\":1,\"os_curso\":1,\"id_matricula\":[]}', 3, '2020-03-23 21:45:12'),
('docentes', '28', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'4\', \'1\', 132, 239, \'6\', \'2\', \'2020-03-31\', \'2020-05-01\', \'1\', 28)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[28]}', 6, '2020-04-01 04:34:04'),
('docentes', '29', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 133, 240, \'3\', \'2\', \'2020-04-01\', \'2020-05-01\', \'1\', 29)', 'Se registro 1 Docentes', 1, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[29]}', 3, '2020-04-01 16:09:45'),
('docentes', '30', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 134, 241, \'3\', \'2\', \'2020-04-01\', \'2020-05-01\', \'1\', 30)', 'Se registro 2 Docentes', 2, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[29,30]}', 3, '2020-04-01 16:09:45'),
('docentes', '31', 'INSERT INTO docentes (idrepresentante, idproducto, idpersona, idusuario_externo, registrado_por, tipo_control, fecha_inicio, fecha_fin, estado, id) VALUES(\'2\', \'1\', 135, 242, \'3\', \'2\', \'2020-04-01\', \'2020-05-01\', \'1\', 31)', 'Se registro 3 Docentes', 3, 'Docentes', '{\"estado\":1,\"os_curso\":null,\"id_matricula\":[29,30,31]}', 3, '2020-04-01 16:09:45'),
('matriculas', '245', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'2\', \'134\', 245, \'3\', \'6\', \'2\', \'2020-04-01\', \'2020-05-01\', \'1\', \'29\', 1070, 245)', 'Se registro 3 Estudiantes', 3, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"6\",\"id_matricula\":[243,244,245]}', 3, '2020-04-01 16:47:51'),
('matriculas', '248', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'2\', \'134\', \'241\', \'3\', \'8\', \'2\', \'2020-04-01\', \'2020-05-01\', \'1\', \'29\', 1073, 248)', 'Se registro 3 Estudiantes', 3, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"8\",\"id_matricula\":[246,247,248]}', 3, '2020-04-01 16:47:52'),
('matriculas', '251', 'INSERT INTO matriculas (idrepresentante, idpersona, idusuario_externo, registrado_por, os_curso, tipo_control, fecha_inicio, fecha_fin, estado, edad_registro, idmatricula_externa, id) VALUES(\'2\', \'134\', \'241\', \'3\', \'11\', \'2\', \'2020-04-01\', \'2020-05-01\', \'1\', \'29\', 1076, 251)', 'Se registro 3 Estudiantes', 3, 'Estudiantes', '{\"estado\":\"1\",\"os_curso\":\"11\",\"id_matricula\":[249,250,251]}', 3, '2020-04-01 16:47:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` bigint(20) NOT NULL,
  `idregion` bigint(20) DEFAULT NULL,
  `idprovincia` bigint(20) NOT NULL DEFAULT '0',
  `nombre` varchar(155) NOT NULL DEFAULT '0',
  `abreviacion` varchar(50) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuota_depago`
--

CREATE TABLE `cuota_depago` (
  `id` bigint(20) NOT NULL,
  `politica_depago` bigint(20) DEFAULT NULL,
  `idrepresentante` bigint(20) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT '0.00',
  `tipo` tinyint(4) DEFAULT '1' COMMENT '1 - nueva orden, 2 - añadir a la orden',
  `tipo_control` tinyint(4) DEFAULT '1' COMMENT '1 - orden de servicio, 2 - pagos por matriculas',
  `fecha_vencimiento` date DEFAULT NULL,
  `fecha_registro` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `estado` tinyint(4) DEFAULT '1' COMMENT '1 - pendiente, 2 -pagado, 3 -cancelado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `cuota_depago`
--

INSERT INTO `cuota_depago` (`id`, `politica_depago`, `idrepresentante`, `numero`, `monto`, `tipo`, `tipo_control`, `fecha_vencimiento`, `fecha_registro`, `estado`) VALUES
(1, 2, 2, 1, '0.00', 1, 1, '2020-02-07', '2020-02-17 01:54:21', 1),
(4, NULL, 2, 1, '48.00', 1, 2, '2020-02-17', '2020-02-17 16:32:00', 2),
(5, NULL, 2, 1, '24.00', 1, 2, '2020-02-17', '2020-02-17 22:08:57', 2),
(6, NULL, 2, 1, '12.00', 1, 2, '2020-02-21', '2020-02-21 17:53:47', 2),
(7, NULL, 2, 1, '12.00', 1, 2, '2020-04-06', '2020-02-21 22:19:59', 1),
(8, NULL, 2, 1, '12.00', 1, 2, '2020-04-06', '2020-02-21 22:23:18', 1),
(9, NULL, 2, 1, '24.00', 1, 2, '2020-04-07', '2020-02-22 22:01:49', 1),
(10, NULL, 2, 1, '36.00', 1, 2, '2020-04-09', '2020-02-25 00:56:02', 1),
(11, NULL, 2, 1, '18.00', 1, 2, '2020-04-10', '2020-02-25 22:34:03', 1),
(12, NULL, 2, 1, '36.00', 1, 2, '2020-04-10', '2020-02-25 23:03:14', 1),
(13, NULL, 2, 1, '36.00', 1, 2, '2020-04-10', '2020-02-25 23:10:16', 1),
(14, NULL, 2, 1, '216.00', 1, 2, '2020-04-10', '2020-02-26 01:30:26', 1),
(15, NULL, 2, 1, '216.00', 1, 2, '2020-04-11', '2020-02-26 16:03:25', 1),
(16, NULL, 2, 1, '36.00', 1, 2, '2020-04-11', '2020-02-27 01:29:05', 1),
(17, NULL, 2, 1, '18.00', 1, 2, '2020-04-11', '2020-02-27 02:31:56', 1),
(18, NULL, 2, 1, '18.00', 1, 2, '2020-04-11', '2020-02-27 02:45:48', 1),
(19, NULL, 2, 1, '36.00', 1, 2, '2020-04-11', '2020-02-27 03:06:14', 1),
(20, NULL, 2, 1, '54.00', 1, 2, '2020-04-12', '2020-02-27 15:41:28', 1),
(21, NULL, 2, 1, '36.00', 1, 2, '2020-04-13', '2020-02-28 23:54:22', 1),
(22, NULL, 2, 1, '18.00', 1, 2, '2020-04-13', '2020-02-28 23:56:33', 1),
(23, NULL, 3, 1, '450.09', 1, 2, '2020-12-30', '2020-03-05 21:58:48', 1),
(24, NULL, 3, 1, '300.06', 1, 2, '2020-12-30', '2020-03-05 22:55:31', 1),
(25, NULL, 3, 1, '150.03', 1, 2, '2020-12-30', '2020-03-05 23:09:52', 1),
(26, NULL, 3, 1, '150.03', 1, 2, '2020-12-30', '2020-03-05 23:17:09', 1),
(27, NULL, 3, 1, '150.03', 1, 2, '2020-12-30', '2020-03-05 23:20:39', 1),
(28, NULL, 3, 1, '150.03', 1, 2, '2020-12-30', '2020-03-06 00:48:37', 1),
(29, NULL, 3, 1, '150.03', 1, 2, '2020-12-30', '2020-03-06 00:50:16', 1),
(30, NULL, 3, 1, '150.03', 1, 2, '2020-12-30', '2020-03-06 00:51:54', 1),
(31, NULL, 3, 1, '150.03', 1, 2, '2020-12-31', '2020-03-06 16:57:58', 1),
(32, NULL, 3, 1, '150.03', 1, 2, '2020-12-31', '2020-03-06 17:01:22', 1),
(33, NULL, 3, 1, '150.03', 1, 2, '2020-12-31', '2020-03-06 17:03:09', 1),
(34, NULL, 3, 1, '150.03', 1, 2, '2020-12-31', '2020-03-06 17:04:30', 1),
(35, NULL, 3, 1, '150.03', 1, 2, '2020-12-31', '2020-03-06 17:06:07', 1),
(36, NULL, 3, 1, '150.03', 1, 2, '2020-12-31', '2020-03-06 17:07:49', 1),
(37, NULL, 3, 1, '150.03', 1, 2, '2020-12-31', '2020-03-06 17:09:01', 1),
(38, NULL, 2, 1, '36.00', 1, 2, '2020-04-23', '2020-03-09 15:33:22', 1),
(39, NULL, 2, 1, '36.00', 1, 2, '2020-04-28', '2020-03-14 17:03:50', 1),
(40, NULL, 2, 1, '36.00', 1, 2, '2020-04-28', '2020-03-14 17:10:37', 1),
(41, NULL, 2, 1, '18.00', 1, 2, '2020-04-28', '2020-03-14 17:17:02', 1),
(42, NULL, 2, 1, '18.00', 1, 2, '2020-05-02', '2020-03-19 01:22:54', 1),
(43, NULL, 2, 1, '18.00', 1, 2, '2020-05-02', '2020-03-19 02:36:32', 1),
(44, NULL, 2, 1, '18.00', 1, 2, '2020-05-03', '2020-03-20 01:26:11', 1),
(45, NULL, 2, 1, '18.00', 1, 2, '2020-05-07', '2020-03-23 21:45:12', 1),
(46, NULL, 2, 1, '324.00', 1, 2, '2020-05-16', '2020-04-01 16:47:53', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` bigint(20) NOT NULL COMMENT 'llave primaria de la tabla',
  `idexterno` int(11) DEFAULT '0' COMMENT 'id del curso en las distintas paltaformas',
  `idproducto_url` bigint(20) NOT NULL DEFAULT '0' COMMENT 'id del proyecto de sks',
  `idgrupo` int(11) DEFAULT '0',
  `nombre` varchar(255) NOT NULL DEFAULT '0' COMMENT 'nombre del curso',
  `descripcion` text,
  `tipocurso` tinyint(4) DEFAULT '1',
  `estado` tinyint(4) DEFAULT '1' COMMENT 'estado del curso, 1 habilitado, 0 inhabilitado',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `idexterno`, `idproducto_url`, `idgrupo`, `nombre`, `descripcion`, `tipocurso`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 119, 1, 0, 'Access 2010 Básico', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(2, 180, 1, 0, 'APLICATIVOS GENERALES', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(3, 44, 1, 0, 'Ardora', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(4, 43, 1, 0, 'Cmap Tools', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(5, 85, 1, 0, 'Como aumentar la productividad a traves de gente motivada - Jim cathcart', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(6, 175, 1, 0, 'DISEÑO WEB - HTML5', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(7, 46, 1, 0, 'Edilim', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(8, 45, 1, 0, 'EducaPlay', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(9, 121, 1, 0, 'Enlace de Datos', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(10, 122, 1, 0, 'Enlace de Datos Intermedio', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(11, 115, 1, 0, 'Excel 2010 Avanzado', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(12, 113, 1, 0, 'Excel 2010 Básico', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(13, 114, 1, 0, 'Excel 2010 Intermedio', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(14, 62, 1, 0, 'Excel 2013 Básico', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(15, 65, 1, 0, 'Excel 2013 Financiero', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(16, 63, 1, 0, 'Excel 2013 Intermedio/Avanzado', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(17, 64, 1, 0, 'Excel 2013 Tablas Dinámicas', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(18, 72, 1, 0, 'Excel 2016 Avanzado', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(19, 47, 1, 0, 'ExeLearning', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(20, 48, 1, 0, 'FreeMind', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(21, 49, 1, 0, 'Geogebra', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(22, 174, 1, 0, 'INTERNET Y REDES SOCIALES', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(23, 50, 1, 0, 'Jclic', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(24, 177, 1, 0, 'MICROSOFT EXCEL 2016 (Base de Datos)', 'Base de Datos', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(25, 70, 1, 0, 'MICROSOFT EXCEL 2016 (Nivel Básico)', 'Nivel Básico', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(26, 71, 1, 0, 'MICROSOFT EXCEL 2016 (Nivel Intermedio)', 'Nivel Intermedio', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(27, 173, 1, 0, 'MICROSOFT POWER POINT 2016', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(28, 179, 1, 0, 'MICROSOFT PROJECT 2016', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(29, 196, 1, 0, 'Windows 10', '', 1, 1, '2020-02-06 09:42:28', '2020-03-19 18:22:26'),
(30, 123, 1, 0, 'Outlook 2010 Básico', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(31, 178, 1, 0, 'PAQUETES CONTABLES', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(32, 51, 1, 0, 'Prezi', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(33, 52, 1, 0, 'QuizFaber', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(34, 53, 1, 0, 'RoboMind', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(35, 32, 1, 0, 'SCI - Banco de la Nación', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(36, 54, 1, 0, 'Scratch', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(37, 26, 1, 0, 'Sistema de Control Interno', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(38, 170, 1, 0, 'Sistema de Control Interno', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(39, 55, 1, 0, 'ThatQuiz', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(40, 56, 1, 0, 'WebQuest', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(41, 67, 1, 0, 'Windows 8.1', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(42, 112, 1, 0, 'Word 2010 Avanzado', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(43, 110, 1, 0, 'Word 2010 Básico', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(44, 111, 1, 0, 'Word 2010 Intermedio', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(45, 58, 1, 0, 'Word 2013 Básico', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(46, 59, 1, 0, 'Word 2013 Intermedio', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(47, 75, 1, 0, 'Word 2016 Avanzado', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(48, 68, 1, 0, 'Word 2016 Básico', '', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(49, 69, 1, 0, 'Word 2016 Intermedio', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(50, 57, 1, 0, 'XMind', 'Edumax', 1, 1, '2020-02-06 09:42:28', '2020-02-06 09:42:28'),
(51, 31, 2, 0, 'A1', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(52, 35, 2, 0, 'A2', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(53, 36, 2, 0, 'B1', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(54, 38, 2, 0, 'B2', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(55, 39, 2, 0, 'C1', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(56, 42, 2, 0, 'Comdata', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(57, 50, 2, 0, 'El arte de las ventas', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(58, 52, 2, 0, 'Prueba', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(59, 32, 2, 0, 'SCI - Banco de la Nación', '', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(60, 26, 2, 0, 'Sistema de Control Interno', 'El Sistema de Control Interno permite prevenir\r\nriesgos, irregularidades y actos de corrupción en\r\nlas entidades públicas, funcionando de manera interdependiente a través de cada elemento organizacional.', 1, 1, '2020-02-06 09:42:29', '2020-02-06 09:42:29'),
(61, 172, 1, 0, 'MICROSOFT WORD 2016', NULL, 1, 1, '2020-02-25 08:55:16', '2020-02-25 08:55:16'),
(62, 176, 1, 0, 'BASE DE DATOS SQL SERVER (Nivel Básico)', NULL, 1, 1, '2020-02-25 08:55:16', '2020-02-25 08:55:16'),
(63, 73, 1, 0, 'Power Point 2016 Básico', NULL, 1, 1, '2020-02-28 13:11:20', '2020-02-28 13:11:20'),
(64, 74, 1, 0, 'Power Point 2016 Intermedio', NULL, 1, 1, '2020-02-28 13:19:04', '2020-02-28 13:19:04'),
(65, 76, 1, 0, 'Power Point 2016 Avanzado', NULL, 1, 1, '2020-02-28 13:19:04', '2020-02-28 13:19:04'),
(66, 0, 1, 0, 'Activador eduktmaestro', NULL, 0, 1, '2020-03-25 09:50:59', '2020-03-25 09:51:26'),
(67, 201, 1, 0, 'Prueba eduktmaestro', '', 2, 1, '2020-03-28 10:47:28', '2020-03-28 10:47:28'),
(68, 202, 1, 0, 'Matemática', '', 2, 1, '2020-04-02 11:36:21', '2020-04-02 11:36:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` bigint(20) NOT NULL,
  `idpais` bigint(20) NOT NULL DEFAULT '0',
  `idregion` bigint(20) DEFAULT NULL,
  `nombre` varchar(155) NOT NULL DEFAULT '0',
  `abreviacion` varchar(50) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `idpais`, `idregion`, `nombre`, `abreviacion`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 11, NULL, 'Amazonas', NULL, '2019-09-13 16:03:12', '2020-02-06 12:07:57'),
(2, 11, NULL, 'Áncash', NULL, '2019-09-13 16:03:12', '2020-02-06 12:08:01'),
(3, 11, NULL, 'Apurímac', NULL, '2019-09-13 16:03:12', '2020-02-06 12:08:03'),
(4, 11, NULL, 'Arequipa', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:05'),
(5, 11, NULL, 'Ayacucho', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:05'),
(6, 11, NULL, 'Cajamarca', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:06'),
(7, 11, NULL, 'Callao', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:07'),
(8, 11, NULL, 'Cusco', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:07'),
(9, 11, NULL, 'Huancavelica', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:09'),
(10, 11, NULL, 'Huánuco', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:08'),
(11, 11, NULL, 'Ica', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:10'),
(12, 11, NULL, 'Junín', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:11'),
(13, 11, NULL, 'La Libertad', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:11'),
(14, 11, NULL, 'Lambayeque', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:14'),
(15, 11, NULL, 'Lima', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:13'),
(16, 11, NULL, 'Loreto', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:15'),
(17, 11, NULL, 'Madre de Dios', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:16'),
(18, 11, NULL, 'Moquegua', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:16'),
(19, 11, NULL, 'Pasco', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:17'),
(20, 11, NULL, 'Piura', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:18'),
(21, 11, NULL, 'Puno', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:19'),
(22, 11, NULL, 'San Martín', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:20'),
(23, 11, NULL, 'Tacna', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:21'),
(24, 11, NULL, 'Tumbes', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:22'),
(25, 11, NULL, 'Ucayali', NULL, '2019-09-13 16:03:13', '2020-02-06 12:08:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docentes`
--

CREATE TABLE `docentes` (
  `id` bigint(20) NOT NULL,
  `idproducto` bigint(20) DEFAULT NULL,
  `idrepresentante` bigint(20) NOT NULL DEFAULT '0',
  `registrado_por` bigint(20) DEFAULT NULL,
  `idpersona` bigint(20) DEFAULT NULL,
  `idusuario_externo` bigint(20) NOT NULL DEFAULT '0',
  `os_curso` bigint(20) DEFAULT NULL,
  `tipo_control` tinyint(4) DEFAULT NULL COMMENT '1 es por licencias y 2 es por saldo',
  `estado` tinyint(4) DEFAULT '1' COMMENT '0=inactivo usuario normal,1=activo, 2=usuario demo, -2= usuario inactivo demo',
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `docentes`
--

INSERT INTO `docentes` (`id`, `idproducto`, `idrepresentante`, `registrado_por`, `idpersona`, `idusuario_externo`, `os_curso`, `tipo_control`, `estado`, `fecha_inicio`, `fecha_fin`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, NULL, 2, NULL, 6, 48, 2, 2, 1, '2020-02-17', '2020-03-17', '2020-02-17 16:53:08', '2020-02-17 16:53:08'),
(2, NULL, 2, NULL, 6, 47, 3, 2, 1, '2020-02-17', '2020-03-17', '2020-02-17 16:53:09', '2020-02-17 16:53:09'),
(3, 1, 2, 3, 10, 32, NULL, 2, 1, '2020-02-24', '2020-03-24', '2020-02-24 16:14:59', '2020-02-24 16:14:59'),
(4, 1, 2, 3, 11, 33, NULL, 2, 1, '2020-02-24', '2020-03-24', '2020-02-24 16:16:40', '2020-02-24 16:16:40'),
(5, 1, 2, 3, 12, 34, NULL, 2, 1, '2020-02-24', '2020-03-24', '2020-02-24 16:16:48', '2020-02-24 16:16:48'),
(6, 1, 2, 3, 29, 53, NULL, 2, 1, '2020-02-26', '2020-03-26', '2020-02-26 16:32:37', '2020-02-26 16:32:37'),
(7, 1, 3, 5, 36, 60, NULL, 2, 1, '2020-03-04', '2020-04-04', '2020-03-04 15:24:41', '2020-03-04 15:24:41'),
(8, 1, 3, 5, 37, 61, NULL, 2, 1, '2020-03-04', '2020-04-04', '2020-03-04 15:52:51', '2020-03-04 15:52:51'),
(9, 1, 3, 5, 39, 62, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 08:14:05', '2020-03-06 14:16:04'),
(10, 1, 3, 5, 40, 63, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(11, 1, 3, 5, 41, 64, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(12, 1, 3, 5, 42, 65, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(13, 1, 3, 5, 43, 66, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(14, 1, 3, 5, 44, 67, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(15, 1, 3, 5, 45, 68, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(16, 1, 3, 5, 46, 69, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(17, 1, 3, 5, 47, 70, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(18, 1, 3, 5, 48, 71, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(19, 1, 3, 5, 49, 72, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(20, 1, 3, 5, 50, 73, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(21, 1, 3, 5, 51, 74, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(22, 1, 3, 5, 52, 75, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(23, 1, 3, 5, 53, 76, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(24, 1, 3, 5, 54, 77, NULL, 2, 1, '2020-03-05', '2020-04-05', '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(25, 1, 3, 5, 126, 233, NULL, 2, 1, '2020-03-13', '2020-04-13', '2020-03-13 11:56:05', '2020-03-13 11:56:05'),
(26, 1, 2, 3, 128, 235, NULL, 2, 1, '2020-03-18', '2020-04-18', '2020-03-18 10:04:57', '2020-03-18 10:04:57'),
(27, 1, 2, 3, 129, 236, NULL, 2, 1, '2020-03-18', '2020-04-18', '2020-03-18 15:34:24', '2020-03-18 15:34:24'),
(28, 1, 4, 6, 132, 239, NULL, 2, 1, '2020-03-31', '2020-05-01', '2020-03-31 21:34:04', '2020-03-31 21:34:04'),
(29, 1, 2, 3, 133, 240, NULL, 2, 1, '2020-04-01', '2020-05-01', '2020-04-01 09:09:45', '2020-04-01 09:09:45'),
(30, 1, 2, 3, 134, 241, NULL, 2, 1, '2020-04-01', '2020-05-01', '2020-04-01 09:09:45', '2020-04-01 09:09:45'),
(31, 1, 2, 3, 135, 242, NULL, 2, 1, '2020-04-01', '2020-05-01', '2020-04-01 09:09:45', '2020-04-01 09:09:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT '0' COMMENT 'nombre de la empresa',
  `rason_social` varchar(255) NOT NULL DEFAULT '0' COMMENT 'rason social de la empresa',
  `ruc` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Identificación tributaria',
  `direccion` text,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL COMMENT 'correo coorporativo',
  `idpais` bigint(20) NOT NULL,
  `iddepartamento` bigint(20) NOT NULL,
  `idprovincia` bigint(20) NOT NULL,
  `idciudad` bigint(20) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipoempresa` tinyint(4) DEFAULT '1',
  `isSede` tinyint(4) DEFAULT '0',
  `prefijo_usu` varchar(5) DEFAULT 'USU',
  `prefijo_usu_doc` varchar(5) DEFAULT 'DOC',
  `is_prefijo` tinyint(4) DEFAULT '1',
  `color_principal` varchar(50) DEFAULT NULL,
  `colores` text,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `nombre`, `rason_social`, `ruc`, `direccion`, `telefono`, `email`, `idpais`, `iddepartamento`, `idprovincia`, `idciudad`, `estado`, `fecha_creacion`, `fecha_actualizacion`, `tipoempresa`, `isSede`, `prefijo_usu`, `prefijo_usu_doc`, `is_prefijo`, `color_principal`, `colores`, `logo`) VALUES
(1, 'edumax', 'edumax SACC', '999999999', 'Av Escalada, Surco, Lima', '999999999', 'alex.mamud@hotmail.com', 11, 14, 125, NULL, 1, '2019-09-20 10:48:15', '2019-12-10 17:46:21', 1, 0, 'USU', 'DOC', 1, NULL, NULL, NULL),
(2, 'Eiger', 'Eiger', '20145698713', 'Somos una CORPORACIÓN EDUCATIVA Y CULTURAL reconocida oficialmente por el Ministerio de Educación, Ministerio de Cultura, Ugeles Educativas y otras In', '987654321', 'server@eiger.com', 11, 15, 128, NULL, 1, '2019-10-23 11:42:48', '2020-03-18 09:21:05', 1, 0, 'USU', 'DOC', 1, 'rgb(60, 141, 188)', '{\"colorsecundario\":\"rgb(91, 190, 206)\",\"colortexto\":\"rgb(248, 249, 250)\"}', '1e72e563b4cb88c064a802ec6bd9015c.jpg'),
(3, 'San Fabian Smart School', 'IE San Fabian', '10000001', 'Av. Los Precursores 891, San Miguel 15088, Peru', '999555666', 'sanfabian@skscourses.com', 11, 15, 128, NULL, 1, '2019-10-23 11:42:48', '2020-02-14 16:40:31', 1, 0, 'USU', 'DOC', 1, NULL, NULL, NULL),
(4, 'EduktMaestro', 'EduktMaestro', '20145698713', NULL, '942171837', 'abelchingo@gmail.com', 11, 15, 128, NULL, 1, '2020-03-31 19:28:32', '2020-03-31 19:28:32', 1, 0, 'ESTEM', 'DOCEM', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_persona`
--

CREATE TABLE `empresa_persona` (
  `idpersona` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `fecha_registro` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `empresa_persona`
--

INSERT INTO `empresa_persona` (`idpersona`, `idempresa`, `fecha_registro`) VALUES
(2, 1, '2020-02-07 17:38:51'),
(3, 2, '2020-02-07 10:30:22'),
(4, 2, '2020-02-27 14:30:21'),
(5, 3, '2020-02-28 09:23:28'),
(6, 4, '2020-03-31 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_sede`
--

CREATE TABLE `empresa_sede` (
  `id` bigint(20) NOT NULL,
  `idempresa` bigint(20) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL COMMENT 'nombre de la empresa',
  `rason_social` varchar(255) NOT NULL COMMENT 'rason social de la empresa',
  `ruc` varchar(255) NOT NULL COMMENT 'Identificación tributaria',
  `direccion` text,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL COMMENT 'correo coorporativo',
  `idpais` bigint(20) NOT NULL,
  `iddepartamento` bigint(20) NOT NULL,
  `idprovincia` bigint(20) NOT NULL,
  `idciudad` bigint(20) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE `inscripcion` (
  `id` bigint(20) NOT NULL,
  `codigo` varchar(20) DEFAULT NULL,
  `idpersona` bigint(20) DEFAULT NULL,
  `idempresa_sede` bigint(20) DEFAULT NULL,
  `idcategoria_externa` bigint(20) DEFAULT NULL,
  `id_padre_externo` bigint(20) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `registrado_por` bigint(20) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `inscripcion`
--

INSERT INTO `inscripcion` (`id`, `codigo`, `idpersona`, `idempresa_sede`, `idcategoria_externa`, `id_padre_externo`, `fecha_inicio`, `fecha_fin`, `registrado_por`, `fecha_creacion`, `estado`) VALUES
(1, 'INS0001P0004', 4, NULL, 16, 15, '2020-02-21', '2020-05-21', 3, '2020-02-21 00:00:00', 1),
(2, 'INS0001P0007', 7, NULL, 16, 15, '2020-02-22', '2020-05-22', 3, '2020-02-22 00:00:00', 1),
(3, 'INS0001P0008', 8, NULL, 16, 15, '2020-02-24', '2020-05-24', 3, '2020-02-24 00:00:00', 1),
(4, 'INS0001P0013', 13, NULL, 16, 15, '2020-02-25', '2020-05-25', 3, '2020-02-25 00:00:00', 1),
(5, 'INS0001P0030', 30, NULL, 16, 15, '2020-02-26', '2020-05-26', 3, '2020-02-26 00:00:00', 1),
(6, 'INS0001P0032', 32, NULL, 16, 15, '2020-02-27', '2020-05-27', 3, '2020-02-27 00:00:00', 1),
(7, 'INS0001P0033', 33, NULL, 16, 15, '2020-02-28', '2020-05-28', 3, '2020-02-28 00:00:00', 1),
(8, 'INS0001P0034', 34, NULL, 16, 15, '2020-02-28', '2020-05-28', 3, '2020-02-28 00:00:00', 1),
(9, 'INS0001P0035', 35, NULL, 16, 15, '2020-03-02', '2020-06-02', 3, '2020-03-02 00:00:00', 1),
(10, 'INS0001P0038', 38, NULL, 16, 15, '2020-03-04', '2020-06-04', 3, '2020-03-04 00:00:00', 1),
(11, 'INS0001P0125', 125, 0, 16, 15, '2020-03-09', '2020-06-09', 3, '2020-03-09 00:00:00', 1),
(12, 'INS0001P0127', 127, NULL, 16, 15, '2020-03-14', '2020-06-30', 3, '2020-03-14 00:00:00', 1),
(13, 'INS0001P0130', 130, NULL, 16, 15, '2020-03-18', '2020-06-18', 3, '2020-03-18 00:00:00', 1),
(14, 'INS0001P0131', 131, NULL, 16, 15, '2020-03-18', '2020-06-18', 3, '2020-03-18 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion_pago`
--

CREATE TABLE `inscripcion_pago` (
  `id` bigint(20) NOT NULL,
  `numorden` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alumno` bigint(20) DEFAULT NULL,
  `idempresa_sede` bigint(20) DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `fecha_hora_registro` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `registrado_por` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` text COLLATE utf8_unicode_ci,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `inscripcion_pago`
--

INSERT INTO `inscripcion_pago` (`id`, `numorden`, `alumno`, `idempresa_sede`, `monto`, `fecha_hora_registro`, `registrado_por`, `file`, `comentario`, `estado`) VALUES
(1, 'PI0001P0004', 4, NULL, '100.00', '2020-02-21 17:49:23', '3', NULL, 'Pago por Inscripción', 1),
(2, 'PI0001P0007', 7, NULL, '100.00', '2020-02-22 22:01:04', '3', NULL, 'Pago por Inscripción', 1),
(3, 'PI0001P0008', 8, NULL, '100.00', '2020-02-24 22:47:14', '3', NULL, 'Pago por Inscripción', 1),
(4, 'PI0001P0013', 13, NULL, '100.00', '2020-02-25 22:32:15', '3', NULL, 'Pago por Inscripción', 1),
(5, 'PI0001P0030', 30, NULL, '100.00', '2020-02-27 02:27:13', '3', NULL, 'Pago por Inscripción', 1),
(6, 'PI0001P0032', 32, NULL, '100.00', '2020-02-27 15:40:45', '3', NULL, 'Pago por Inscripción', 1),
(7, 'PI0001P0033', 33, NULL, '100.00', '2020-02-28 23:56:10', '3', NULL, 'Pago por Inscripción', 1),
(8, 'PI0001P0034', 34, NULL, '100.00', '2020-02-29 00:44:04', '3', NULL, 'Pago por Inscripción', 1),
(9, 'PI0001P0035', 35, NULL, '100.00', '2020-03-02 22:39:33', '3', NULL, 'Pago por Inscripción', 1),
(10, 'PI0001P0038', 38, NULL, '100.00', '2020-03-05 01:34:28', '3', NULL, 'Pago por Inscripción', 1),
(11, 'PI0001P0125', 125, 0, '100.00', '2020-03-09 22:41:38', '3', NULL, 'Pago por Inscripción', 1),
(12, 'PI0001P0127', 127, NULL, '100.00', '2020-03-14 17:12:04', '3', NULL, 'Pago por Inscripción', 1),
(13, 'PI0001P0130', 130, NULL, '100.00', '2020-03-19 01:22:33', '3', NULL, 'Pago por Inscripción', 1),
(14, 'PI0001P0131', 131, NULL, '100.00', '2020-03-19 02:36:17', '3', NULL, 'Pago por Inscripción', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion_pago_detalle`
--

CREATE TABLE `inscripcion_pago_detalle` (
  `id` bigint(20) NOT NULL,
  `pago` bigint(20) DEFAULT NULL,
  `idinscripcion` bigint(20) DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `monto` decimal(10,2) DEFAULT NULL,
  `fecha_pago` timestamp NULL DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `inscripcion_pago_detalle`
--

INSERT INTO `inscripcion_pago_detalle` (`id`, `pago`, `idinscripcion`, `descripcion`, `monto`, `fecha_pago`, `estado`) VALUES
(1, 1, 1, 'Pago por Inscripción', '100.00', '2020-02-21 17:49:23', 1),
(2, 2, 2, 'Pago por Inscripción', '100.00', '2020-02-22 22:01:04', 1),
(3, 3, 3, 'Pago por Inscripción', '100.00', '2020-02-24 22:47:14', 1),
(4, 4, 4, 'Pago por Inscripción', '100.00', '2020-02-25 22:32:15', 1),
(5, 5, 5, 'Pago por Inscripción', '100.00', '2020-02-27 02:27:13', 1),
(6, 6, 6, 'Pago por Inscripción', '100.00', '2020-02-27 15:40:45', 1),
(7, 7, 7, 'Pago por Inscripción', '100.00', '2020-02-28 23:56:10', 1),
(8, 8, 8, 'Pago por Inscripción', '100.00', '2020-02-29 00:44:04', 1),
(9, 9, 9, 'Pago por Inscripción', '100.00', '2020-03-02 22:39:33', 1),
(10, 10, 10, 'Pago por Inscripción', '100.00', '2020-03-05 01:34:28', 1),
(11, 11, 11, 'Pago por Inscripción', '100.00', '2020-03-09 22:41:38', 1),
(12, 12, 12, 'Pago por Inscripción', '100.00', '2020-03-14 17:12:04', 1),
(13, 13, 13, 'Pago por Inscripción', '100.00', '2020-03-19 01:22:33', 1),
(14, 14, 14, 'Pago por Inscripción', '100.00', '2020-03-19 02:36:17', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculas`
--

CREATE TABLE `matriculas` (
  `id` bigint(20) NOT NULL,
  `idmatricula_externa` bigint(20) DEFAULT NULL,
  `idempresa_sede` bigint(20) DEFAULT NULL,
  `idinscripcion` bigint(20) DEFAULT NULL,
  `idrepresentante` bigint(20) NOT NULL DEFAULT '0',
  `registrado_por` bigint(20) DEFAULT NULL,
  `idpersona` bigint(20) DEFAULT NULL,
  `idusuario_externo` bigint(20) NOT NULL DEFAULT '0',
  `os_curso` bigint(20) DEFAULT NULL,
  `tipo_control` tinyint(4) DEFAULT '2' COMMENT '1 es por licencias y 2 es por saldo',
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1',
  `edad_registro` tinyint(4) DEFAULT NULL COMMENT 'Edad en la cual se registro el estudiante',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `matriculas`
--

INSERT INTO `matriculas` (`id`, `idmatricula_externa`, `idempresa_sede`, `idinscripcion`, `idrepresentante`, `registrado_por`, `idpersona`, `idusuario_externo`, `os_curso`, `tipo_control`, `fecha_inicio`, `fecha_fin`, `estado`, `edad_registro`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(4, 559, NULL, 2, 2, 3, 7, 30, 4, 2, '2020-02-22', '2020-03-22', 1, NULL, '2020-02-22 13:01:49', '2020-02-22 13:01:49'),
(5, 560, NULL, NULL, 2, 3, 9, 31, 6, 2, '2020-02-24', '2020-03-24', 1, NULL, '2020-02-24 15:56:01', '2020-02-24 15:56:01'),
(6, 561, NULL, 4, 2, 3, 13, 35, 6, 2, '2020-02-25', '2020-03-25', 1, NULL, '2020-02-25 13:34:03', '2020-02-25 13:34:03'),
(7, 562, NULL, NULL, 2, 3, 14, 36, 6, 2, '2020-02-25', '2020-03-26', 1, NULL, '2020-02-25 14:03:14', '2020-02-25 14:03:14'),
(8, 563, NULL, NULL, 2, 3, 15, 37, 6, 2, '2020-02-25', '2020-03-25', 1, NULL, '2020-02-25 14:10:16', '2020-02-25 14:10:16'),
(9, 564, NULL, NULL, 2, 3, 16, 38, 6, 2, '2020-02-25', '2020-03-25', 0, NULL, '2020-02-25 16:30:24', '2020-03-06 13:29:18'),
(10, 565, NULL, NULL, 2, 3, 17, 39, 6, 2, '2020-02-25', '2020-03-25', 1, NULL, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(11, 566, NULL, NULL, 2, 3, 18, 40, 6, 2, '2020-02-25', '2020-03-25', 1, NULL, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(12, 567, NULL, NULL, 2, 3, 19, 41, 6, 2, '2020-02-25', '2020-03-25', 1, NULL, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(13, 568, NULL, NULL, 2, 3, 20, 42, 6, 2, '2020-02-25', '2020-03-25', 1, NULL, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(14, 569, NULL, NULL, 2, 3, 21, 43, 6, 2, '2020-02-25', '2020-03-25', 1, NULL, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(15, 570, NULL, NULL, 2, 3, 22, 44, 6, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(16, 571, NULL, NULL, 2, 3, 23, 45, 6, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(17, 572, NULL, NULL, 2, 3, 24, 46, 6, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(18, 573, NULL, NULL, 2, 3, 25, 47, 6, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(19, 574, NULL, NULL, 2, 3, 26, 48, 6, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(20, 575, NULL, NULL, 2, 3, 27, 49, 6, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(21, 587, NULL, NULL, 2, 3, 28, 52, 6, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 16:29:05', '2020-02-26 16:29:05'),
(22, 588, NULL, 5, 2, 3, 30, 54, 6, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 17:31:56', '2020-02-26 17:31:56'),
(23, 589, NULL, NULL, 2, 3, 31, 55, 2, 2, '2020-02-26', '2020-03-26', 1, NULL, '2020-02-26 18:06:14', '2020-02-26 18:06:14'),
(24, 590, NULL, 6, 2, 3, 32, 56, 4, 2, '2020-02-27', '2020-03-27', 1, NULL, '2020-02-27 06:41:27', '2020-02-27 06:41:27'),
(25, 591, NULL, NULL, 2, 3, 33, 58, 11, 2, '2020-02-28', '2020-03-28', 1, 23, '2020-02-28 14:54:22', '2020-02-28 14:54:22'),
(26, 592, NULL, 7, 2, 3, 33, 58, 6, 2, '2020-02-28', '2020-03-28', 1, 23, '2020-02-28 14:56:33', '2020-02-28 14:56:33'),
(27, 847, NULL, NULL, 3, 5, 55, 162, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(28, 848, NULL, NULL, 3, 5, 56, 163, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(29, 849, NULL, NULL, 3, 5, 57, 164, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(30, 850, NULL, NULL, 3, 5, 58, 165, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(31, 851, NULL, NULL, 3, 5, 59, 166, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(32, 852, NULL, NULL, 3, 5, 60, 167, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(33, 853, NULL, NULL, 3, 5, 61, 168, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(34, 854, NULL, NULL, 3, 5, 62, 169, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(35, 855, NULL, NULL, 3, 5, 63, 170, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(36, 856, NULL, NULL, 3, 5, 64, 171, 14, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(37, 857, NULL, NULL, 3, 5, 55, 162, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(38, 858, NULL, NULL, 3, 5, 56, 163, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(39, 859, NULL, NULL, 3, 5, 57, 164, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(40, 860, NULL, NULL, 3, 5, 58, 165, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(41, 861, NULL, NULL, 3, 5, 59, 166, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(42, 862, NULL, NULL, 3, 5, 60, 167, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(43, 863, NULL, NULL, 3, 5, 61, 168, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(44, 864, NULL, NULL, 3, 5, 62, 169, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(45, 865, NULL, NULL, 3, 5, 63, 170, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(46, 866, NULL, NULL, 3, 5, 64, 171, 18, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:54', '2020-03-06 07:57:54'),
(47, 867, NULL, NULL, 3, 5, 55, 162, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(48, 868, NULL, NULL, 3, 5, 56, 163, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(49, 869, NULL, NULL, 3, 5, 57, 164, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(50, 870, NULL, NULL, 3, 5, 58, 165, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(51, 871, NULL, NULL, 3, 5, 59, 166, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(52, 872, NULL, NULL, 3, 5, 60, 167, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(53, 873, NULL, NULL, 3, 5, 61, 168, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(54, 874, NULL, NULL, 3, 5, 62, 169, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(55, 875, NULL, NULL, 3, 5, 63, 170, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(56, 876, NULL, NULL, 3, 5, 64, 171, 21, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 07:57:56', '2020-03-06 07:57:56'),
(57, 877, NULL, NULL, 3, 5, 65, 172, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(58, 878, NULL, NULL, 3, 5, 66, 173, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(59, 879, NULL, NULL, 3, 5, 67, 174, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(60, 880, NULL, NULL, 3, 5, 68, 175, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(61, 881, NULL, NULL, 3, 5, 69, 176, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(62, 882, NULL, NULL, 3, 5, 70, 177, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(63, 883, NULL, NULL, 3, 5, 71, 178, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(64, 884, NULL, NULL, 3, 5, 72, 179, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(65, 885, NULL, NULL, 3, 5, 73, 180, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(66, 886, NULL, NULL, 3, 5, 74, 181, 15, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(67, 887, NULL, NULL, 3, 5, 65, 172, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(68, 888, NULL, NULL, 3, 5, 66, 173, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(69, 889, NULL, NULL, 3, 5, 67, 174, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(70, 890, NULL, NULL, 3, 5, 68, 175, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(71, 891, NULL, NULL, 3, 5, 69, 176, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(72, 892, NULL, NULL, 3, 5, 70, 177, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(73, 893, NULL, NULL, 3, 5, 71, 178, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(74, 894, NULL, NULL, 3, 5, 72, 179, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(75, 895, NULL, NULL, 3, 5, 73, 180, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(76, 896, NULL, NULL, 3, 5, 74, 181, 19, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:18', '2020-03-06 08:01:18'),
(77, 897, NULL, NULL, 3, 5, 65, 172, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(78, 898, NULL, NULL, 3, 5, 66, 173, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(79, 899, NULL, NULL, 3, 5, 67, 174, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(80, 900, NULL, NULL, 3, 5, 68, 175, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(81, 901, NULL, NULL, 3, 5, 69, 176, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(82, 902, NULL, NULL, 3, 5, 70, 177, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(83, 903, NULL, NULL, 3, 5, 71, 178, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(84, 904, NULL, NULL, 3, 5, 72, 179, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(85, 905, NULL, NULL, 3, 5, 73, 180, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(86, 906, NULL, NULL, 3, 5, 74, 181, 22, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:01:20', '2020-03-06 08:01:20'),
(87, 907, NULL, NULL, 3, 5, 75, 182, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(88, 908, NULL, NULL, 3, 5, 76, 183, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(89, 909, NULL, NULL, 3, 5, 77, 184, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(90, 910, NULL, NULL, 3, 5, 78, 185, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:03', '2020-03-06 08:03:03'),
(91, 911, NULL, NULL, 3, 5, 79, 186, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:03', '2020-03-06 08:03:03'),
(92, 912, NULL, NULL, 3, 5, 80, 187, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:03', '2020-03-06 08:03:03'),
(93, 913, NULL, NULL, 3, 5, 81, 188, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:03', '2020-03-06 08:03:03'),
(94, 914, NULL, NULL, 3, 5, 82, 189, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:03', '2020-03-06 08:03:03'),
(95, 915, NULL, NULL, 3, 5, 83, 190, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:03', '2020-03-06 08:03:03'),
(96, 916, NULL, NULL, 3, 5, 84, 191, 15, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:03', '2020-03-06 08:03:03'),
(97, 917, NULL, NULL, 3, 5, 75, 182, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(98, 918, NULL, NULL, 3, 5, 76, 183, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(99, 919, NULL, NULL, 3, 5, 77, 184, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(100, 920, NULL, NULL, 3, 5, 78, 185, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(101, 921, NULL, NULL, 3, 5, 79, 186, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(102, 922, NULL, NULL, 3, 5, 80, 187, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(103, 923, NULL, NULL, 3, 5, 81, 188, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(104, 924, NULL, NULL, 3, 5, 82, 189, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(105, 925, NULL, NULL, 3, 5, 83, 190, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(106, 926, NULL, NULL, 3, 5, 84, 191, 19, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:05', '2020-03-06 08:03:05'),
(107, 927, NULL, NULL, 3, 5, 75, 182, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(108, 928, NULL, NULL, 3, 5, 76, 183, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(109, 929, NULL, NULL, 3, 5, 77, 184, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(110, 930, NULL, NULL, 3, 5, 78, 185, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(111, 931, NULL, NULL, 3, 5, 79, 186, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(112, 932, NULL, NULL, 3, 5, 80, 187, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(113, 933, NULL, NULL, 3, 5, 81, 188, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(114, 934, NULL, NULL, 3, 5, 82, 189, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(115, 935, NULL, NULL, 3, 5, 83, 190, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(116, 936, NULL, NULL, 3, 5, 84, 191, 22, 2, '2020-03-03', '2020-12-31', 1, 13, '2020-03-06 08:03:07', '2020-03-06 08:03:07'),
(117, 937, NULL, NULL, 3, 5, 85, 192, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(118, 938, NULL, NULL, 3, 5, 86, 193, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(119, 939, NULL, NULL, 3, 5, 87, 194, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(120, 940, NULL, NULL, 3, 5, 88, 195, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(121, 941, NULL, NULL, 3, 5, 89, 196, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(122, 942, NULL, NULL, 3, 5, 90, 197, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(123, 943, NULL, NULL, 3, 5, 91, 198, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(124, 944, NULL, NULL, 3, 5, 92, 199, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(125, 945, NULL, NULL, 3, 5, 93, 200, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(126, 946, NULL, NULL, 3, 5, 94, 201, 13, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(127, 947, NULL, NULL, 3, 5, 85, 192, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(128, 948, NULL, NULL, 3, 5, 86, 193, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(129, 949, NULL, NULL, 3, 5, 87, 194, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(130, 950, NULL, NULL, 3, 5, 88, 195, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(131, 951, NULL, NULL, 3, 5, 89, 196, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(132, 952, NULL, NULL, 3, 5, 90, 197, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(133, 953, NULL, NULL, 3, 5, 91, 198, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(134, 954, NULL, NULL, 3, 5, 92, 199, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(135, 955, NULL, NULL, 3, 5, 93, 200, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(136, 956, NULL, NULL, 3, 5, 94, 201, 17, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:26', '2020-03-06 08:04:26'),
(137, 957, NULL, NULL, 3, 5, 85, 192, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(138, 958, NULL, NULL, 3, 5, 86, 193, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(139, 959, NULL, NULL, 3, 5, 87, 194, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(140, 960, NULL, NULL, 3, 5, 88, 195, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(141, 961, NULL, NULL, 3, 5, 89, 196, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(142, 962, NULL, NULL, 3, 5, 90, 197, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(143, 963, NULL, NULL, 3, 5, 91, 198, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(144, 964, NULL, NULL, 3, 5, 92, 199, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(145, 965, NULL, NULL, 3, 5, 93, 200, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(146, 966, NULL, NULL, 3, 5, 94, 201, 23, 2, '2020-03-03', '2020-12-31', 1, 15, '2020-03-06 08:04:28', '2020-03-06 08:04:28'),
(147, 967, NULL, NULL, 3, 5, 95, 202, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(148, 968, NULL, NULL, 3, 5, 96, 203, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(149, 969, NULL, NULL, 3, 5, 97, 204, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(150, 970, NULL, NULL, 3, 5, 98, 205, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(151, 971, NULL, NULL, 3, 5, 99, 206, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(152, 972, NULL, NULL, 3, 5, 100, 207, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(153, 973, NULL, NULL, 3, 5, 101, 208, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(154, 974, NULL, NULL, 3, 5, 102, 209, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(155, 975, NULL, NULL, 3, 5, 103, 210, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(156, 976, NULL, NULL, 3, 5, 104, 211, 13, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(157, 977, NULL, NULL, 3, 5, 95, 202, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(158, 978, NULL, NULL, 3, 5, 96, 203, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(159, 979, NULL, NULL, 3, 5, 97, 204, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(160, 980, NULL, NULL, 3, 5, 98, 205, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(161, 981, NULL, NULL, 3, 5, 99, 206, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(162, 982, NULL, NULL, 3, 5, 100, 207, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(163, 983, NULL, NULL, 3, 5, 101, 208, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(164, 984, NULL, NULL, 3, 5, 102, 209, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(165, 985, NULL, NULL, 3, 5, 103, 210, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(166, 986, NULL, NULL, 3, 5, 104, 211, 17, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:03', '2020-03-06 08:06:03'),
(167, 987, NULL, NULL, 3, 5, 95, 202, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(168, 988, NULL, NULL, 3, 5, 96, 203, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(169, 989, NULL, NULL, 3, 5, 97, 204, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(170, 990, NULL, NULL, 3, 5, 98, 205, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(171, 991, NULL, NULL, 3, 5, 99, 206, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(172, 992, NULL, NULL, 3, 5, 100, 207, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(173, 993, NULL, NULL, 3, 5, 101, 208, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(174, 994, NULL, NULL, 3, 5, 102, 209, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(175, 995, NULL, NULL, 3, 5, 103, 210, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(176, 996, NULL, NULL, 3, 5, 104, 211, 23, 2, '2020-03-03', '2020-12-31', 1, 16, '2020-03-06 08:06:05', '2020-03-06 08:06:05'),
(177, 997, NULL, NULL, 3, 5, 105, 212, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(178, 998, NULL, NULL, 3, 5, 106, 213, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(179, 999, NULL, NULL, 3, 5, 107, 214, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(180, 1000, NULL, NULL, 3, 5, 108, 215, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(181, 1001, NULL, NULL, 3, 5, 109, 216, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(182, 1002, NULL, NULL, 3, 5, 110, 217, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(183, 1003, NULL, NULL, 3, 5, 111, 218, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(184, 1004, NULL, NULL, 3, 5, 112, 219, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(185, 1005, NULL, NULL, 3, 5, 113, 220, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(186, 1006, NULL, NULL, 3, 5, 114, 221, 14, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(187, 1007, NULL, NULL, 3, 5, 105, 212, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(188, 1008, NULL, NULL, 3, 5, 106, 213, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(189, 1009, NULL, NULL, 3, 5, 107, 214, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(190, 1010, NULL, NULL, 3, 5, 108, 215, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(191, 1011, NULL, NULL, 3, 5, 109, 216, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(192, 1012, NULL, NULL, 3, 5, 110, 217, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(193, 1013, NULL, NULL, 3, 5, 111, 218, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(194, 1014, NULL, NULL, 3, 5, 112, 219, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(195, 1015, NULL, NULL, 3, 5, 113, 220, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(196, 1016, NULL, NULL, 3, 5, 114, 221, 18, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:45', '2020-03-06 08:07:45'),
(197, 1017, NULL, NULL, 3, 5, 105, 212, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(198, 1018, NULL, NULL, 3, 5, 106, 213, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(199, 1019, NULL, NULL, 3, 5, 107, 214, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(200, 1020, NULL, NULL, 3, 5, 108, 215, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(201, 1021, NULL, NULL, 3, 5, 109, 216, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(202, 1022, NULL, NULL, 3, 5, 110, 217, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(203, 1023, NULL, NULL, 3, 5, 111, 218, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(204, 1024, NULL, NULL, 3, 5, 112, 219, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(205, 1025, NULL, NULL, 3, 5, 113, 220, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(206, 1026, NULL, NULL, 3, 5, 114, 221, 21, 2, '2020-03-03', '2020-12-31', 1, 11, '2020-03-06 08:07:47', '2020-03-06 08:07:47'),
(207, 1027, NULL, NULL, 3, 5, 115, 222, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(208, 1028, NULL, NULL, 3, 5, 116, 223, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(209, 1029, NULL, NULL, 3, 5, 117, 224, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(210, 1030, NULL, NULL, 3, 5, 118, 225, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(211, 1031, NULL, NULL, 3, 5, 119, 226, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(212, 1032, NULL, NULL, 3, 5, 120, 227, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(213, 1033, NULL, NULL, 3, 5, 121, 228, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(214, 1034, NULL, NULL, 3, 5, 122, 229, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(215, 1035, NULL, NULL, 3, 5, 123, 230, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(216, 1036, NULL, NULL, 3, 5, 124, 231, 14, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(217, 1037, NULL, NULL, 3, 5, 115, 222, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(218, 1038, NULL, NULL, 3, 5, 116, 223, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(219, 1039, NULL, NULL, 3, 5, 117, 224, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(220, 1040, NULL, NULL, 3, 5, 118, 225, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(221, 1041, NULL, NULL, 3, 5, 119, 226, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(222, 1042, NULL, NULL, 3, 5, 120, 227, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(223, 1043, NULL, NULL, 3, 5, 121, 228, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(224, 1044, NULL, NULL, 3, 5, 122, 229, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(225, 1045, NULL, NULL, 3, 5, 123, 230, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(226, 1046, NULL, NULL, 3, 5, 124, 231, 18, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:57', '2020-03-06 08:08:57'),
(227, 1047, NULL, NULL, 3, 5, 115, 222, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(228, 1048, NULL, NULL, 3, 5, 116, 223, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(229, 1049, NULL, NULL, 3, 5, 117, 224, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(230, 1050, NULL, NULL, 3, 5, 118, 225, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(231, 1051, NULL, NULL, 3, 5, 119, 226, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(232, 1052, NULL, NULL, 3, 5, 120, 227, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(233, 1053, NULL, NULL, 3, 5, 121, 228, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(234, 1054, NULL, NULL, 3, 5, 122, 229, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(235, 1055, NULL, NULL, 3, 5, 123, 230, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(236, 1056, NULL, NULL, 3, 5, 124, 231, 21, 2, '2020-03-03', '2020-12-31', 1, 12, '2020-03-06 08:08:59', '2020-03-06 08:08:59'),
(237, 1057, NULL, NULL, 2, 3, 125, 232, 6, 2, '2020-03-09', '2020-04-09', 1, 24, '2020-03-09 08:33:22', '2020-03-09 08:33:22'),
(238, 1058, NULL, NULL, 2, 3, 127, 234, 11, 2, '2020-03-14', '2020-04-22', 1, 24, '2020-03-14 10:03:49', '2020-03-14 10:03:49'),
(239, 1063, NULL, 12, 2, 3, 127, 234, 11, 2, '2020-12-31', '2020-04-14', 1, 23, '2020-03-14 10:17:02', '2020-03-14 10:17:02'),
(240, 1064, NULL, 13, 2, 3, 130, 237, 11, 2, '2020-07-18', '2020-04-18', 1, 29, '2020-03-18 18:22:54', '2020-03-18 18:22:54'),
(241, 1064, NULL, 14, 2, 3, 131, 237, 11, 2, '2020-07-18', '2020-04-18', 1, 24, '2020-03-18 19:36:32', '2020-03-18 19:36:32'),
(242, 1066, NULL, 14, 2, 3, 131, 237, 6, 2, NULL, NULL, 1, 24, '2020-03-19 18:26:10', '2020-03-19 18:26:10'),
(243, 1068, NULL, NULL, 2, 3, 133, 243, 6, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:51', '2020-04-01 09:47:51'),
(244, 1069, NULL, NULL, 2, 3, 135, 244, 6, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:51', '2020-04-01 09:47:51'),
(245, 1070, NULL, NULL, 2, 3, 134, 245, 6, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:51', '2020-04-01 09:47:51'),
(246, 1071, NULL, NULL, 2, 3, 133, 240, 8, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:52', '2020-04-01 09:47:52'),
(247, 1072, NULL, NULL, 2, 3, 135, 242, 8, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:52', '2020-04-01 09:47:52'),
(248, 1073, NULL, NULL, 2, 3, 134, 241, 8, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:52', '2020-04-01 09:47:52'),
(249, 1074, NULL, NULL, 2, 3, 133, 240, 11, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:53', '2020-04-01 09:47:53'),
(250, 1075, NULL, NULL, 2, 3, 135, 242, 11, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:53', '2020-04-01 09:47:53'),
(251, 1076, NULL, NULL, 2, 3, 134, 241, 11, 2, '2020-04-01', '2020-05-01', 1, 29, '2020-04-01 09:47:53', '2020-04-01 09:47:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matricula_cuota_depago`
--

CREATE TABLE `matricula_cuota_depago` (
  `id` bigint(20) NOT NULL,
  `idmatricula` bigint(20) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `monto` decimal(10,2) DEFAULT '0.00',
  `tipo` tinyint(4) DEFAULT '1' COMMENT '0 - otros, 1 - recurso',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `fecha_registro` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `estado` tinyint(4) DEFAULT '1' COMMENT '1 - pendiente, 2 -pagado, 3 -cancelado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `matricula_cuota_depago`
--

INSERT INTO `matricula_cuota_depago` (`id`, `idmatricula`, `numero`, `descripcion`, `monto`, `tipo`, `url`, `fecha_vencimiento`, `fecha_registro`, `estado`) VALUES
(3, 4, 1, 'Material (100.00%)', '60.00', 1, 'https://skscourses.com/json/', '2020-02-22', '2020-02-22 20:04:24', 1),
(4, 5, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-24', '2020-02-25 00:56:01', 2),
(5, 5, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-24', '2020-02-25 00:56:01', 2),
(6, 6, 1, 'Material', '60.00', 1, 'https://skscourses.com/json/', '2020-02-25', '2020-02-25 22:34:03', 2),
(7, 7, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-25', '2020-02-25 23:03:14', 2),
(8, 7, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-25 21:15:31', 2),
(9, 8, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-25', '2020-02-25 23:10:16', 2),
(10, 8, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-25', '2020-02-25 23:10:16', 2),
(11, 9, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-25', '2020-02-26 01:30:24', 2),
(12, 9, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-25', '2020-02-26 01:30:24', 2),
(13, 10, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-25', '2020-02-26 01:30:25', 2),
(14, 10, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-25', '2020-02-26 01:30:25', 2),
(15, 11, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-25', '2020-02-26 01:30:25', 2),
(16, 11, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-25', '2020-02-26 01:30:25', 2),
(17, 12, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-25', '2020-02-26 01:30:25', 2),
(18, 12, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-25', '2020-02-26 01:30:25', 2),
(19, 13, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-25', '2020-02-26 01:30:25', 2),
(20, 13, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-25', '2020-02-26 01:30:25', 2),
(21, 14, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-25', '2020-02-26 01:30:26', 2),
(22, 14, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-25', '2020-02-26 01:30:26', 2),
(23, 15, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-26', '2020-02-26 16:03:24', 2),
(24, 15, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-26 16:03:24', 2),
(25, 16, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-26', '2020-02-26 16:03:24', 2),
(26, 16, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-26 16:03:24', 2),
(27, 17, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-26', '2020-02-26 16:03:24', 2),
(28, 17, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-26 16:03:24', 2),
(29, 18, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-26', '2020-02-26 16:03:24', 2),
(30, 18, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-26 16:03:24', 2),
(31, 19, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-26', '2020-02-26 16:03:25', 2),
(32, 19, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-26 16:03:25', 2),
(33, 20, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-26', '2020-02-26 16:03:25', 2),
(34, 20, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-26 16:03:25', 2),
(35, 21, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-26', '2020-02-27 01:29:05', 2),
(36, 21, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-27 01:29:05', 1),
(37, 22, 1, 'Material', '60.00', 1, 'https://skscourses.com/json/', '2020-02-26', '2020-02-27 02:31:56', 2),
(38, 23, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-26', '2020-02-27 03:06:14', 2),
(39, 23, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-26', '2020-02-27 03:06:14', 2),
(40, 24, 1, 'Material', '60.00', 1, 'https://skscourses.com/json/', '2020-02-27', '2020-02-27 15:41:27', 2),
(41, 25, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-02-28', '2020-02-28 23:54:22', 2),
(42, 25, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-03-28', '2020-02-28 23:54:22', 2),
(43, 26, 1, 'Material', '60.00', 1, 'https://skscourses.com/json/', '2020-02-28', '2020-02-28 23:56:33', 2),
(44, 27, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:52', 2),
(45, 28, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:52', 2),
(46, 29, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:52', 2),
(47, 30, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:53', 2),
(48, 31, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:53', 2),
(49, 32, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:53', 2),
(50, 33, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:53', 2),
(51, 34, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:53', 2),
(52, 35, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:54', 2),
(53, 36, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:54', 2),
(54, 37, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:54', 2),
(55, 38, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:54', 2),
(56, 39, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:55', 2),
(57, 40, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:55', 2),
(58, 41, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:55', 2),
(59, 42, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:55', 2),
(60, 43, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:55', 2),
(61, 44, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:55', 2),
(62, 45, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:56', 2),
(63, 46, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:56', 2),
(64, 47, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:56', 2),
(65, 48, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:56', 2),
(66, 49, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:57', 2),
(67, 50, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:57', 2),
(68, 51, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:57', 2),
(69, 52, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:57', 2),
(70, 53, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:57', 2),
(71, 54, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:58', 2),
(72, 55, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:58', 2),
(73, 56, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 16:57:58', 2),
(74, 57, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:16', 2),
(75, 58, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:16', 2),
(76, 59, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:16', 2),
(77, 60, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:16', 2),
(78, 61, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:16', 2),
(79, 62, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:17', 2),
(80, 63, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:17', 2),
(81, 64, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:17', 2),
(82, 65, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:17', 2),
(83, 66, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:17', 2),
(84, 67, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:18', 2),
(85, 68, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:18', 2),
(86, 69, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:18', 2),
(87, 70, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:18', 2),
(88, 71, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:19', 2),
(89, 72, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:19', 2),
(90, 73, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:19', 2),
(91, 74, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:19', 2),
(92, 75, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:19', 2),
(93, 76, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:19', 2),
(94, 77, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:20', 2),
(95, 78, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:20', 2),
(96, 79, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:20', 2),
(97, 80, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:20', 2),
(98, 81, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:21', 2),
(99, 82, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:21', 2),
(100, 83, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:21', 2),
(101, 84, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:21', 2),
(102, 85, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:21', 2),
(103, 86, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:01:22', 2),
(104, 87, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:02', 2),
(105, 88, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:03', 2),
(106, 89, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:03', 2),
(107, 90, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:03', 2),
(108, 91, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:03', 2),
(109, 92, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:03', 2),
(110, 93, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:04', 2),
(111, 94, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:04', 2),
(112, 95, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:04', 2),
(113, 96, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:04', 2),
(114, 97, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:05', 2),
(115, 98, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:05', 2),
(116, 99, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:05', 2),
(117, 100, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:05', 2),
(118, 101, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:05', 2),
(119, 102, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:06', 2),
(120, 103, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:06', 2),
(121, 104, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:06', 2),
(122, 105, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:06', 2),
(123, 106, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:06', 2),
(124, 107, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:07', 2),
(125, 108, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:07', 2),
(126, 109, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:07', 2),
(127, 110, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:07', 2),
(128, 111, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:08', 2),
(129, 112, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:08', 2),
(130, 113, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:08', 2),
(131, 114, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:08', 2),
(132, 115, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:08', 2),
(133, 116, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:03:09', 2),
(134, 117, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:24', 2),
(135, 118, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:24', 2),
(136, 119, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:24', 2),
(137, 120, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:24', 2),
(138, 121, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:24', 2),
(139, 122, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:25', 2),
(140, 123, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:25', 2),
(141, 124, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:25', 2),
(142, 125, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:25', 2),
(143, 126, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:25', 2),
(144, 127, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:26', 2),
(145, 128, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:26', 2),
(146, 129, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:26', 2),
(147, 130, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:26', 2),
(148, 131, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:26', 2),
(149, 132, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:27', 2),
(150, 133, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:27', 2),
(151, 134, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:27', 2),
(152, 135, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:27', 2),
(153, 136, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:27', 2),
(154, 137, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:28', 2),
(155, 138, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:28', 2),
(156, 139, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:28', 2),
(157, 140, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:28', 2),
(158, 141, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:29', 2),
(159, 142, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:29', 2),
(160, 143, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:29', 2),
(161, 144, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:29', 2),
(162, 145, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:29', 2),
(163, 146, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:04:30', 2),
(164, 147, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:01', 2),
(165, 148, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:01', 2),
(166, 149, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:01', 2),
(167, 150, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:01', 2),
(168, 151, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:01', 2),
(169, 152, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:02', 2),
(170, 153, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:02', 2),
(171, 154, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:02', 2),
(172, 155, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:02', 2),
(173, 156, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:02', 2),
(174, 157, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:03', 2),
(175, 158, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:03', 2),
(176, 159, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:03', 2),
(177, 160, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:03', 2),
(178, 161, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:04', 2),
(179, 162, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:04', 2),
(180, 163, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:04', 2),
(181, 164, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:04', 2),
(182, 165, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:04', 2),
(183, 166, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:05', 2),
(184, 167, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:05', 2),
(185, 168, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:05', 2),
(186, 169, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:05', 2),
(187, 170, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:06', 2),
(188, 171, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:06', 2),
(189, 172, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:06', 2),
(190, 173, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:06', 2),
(191, 174, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:06', 2),
(192, 175, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:07', 2),
(193, 176, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:06:07', 2),
(194, 177, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:42', 2),
(195, 178, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:43', 2),
(196, 179, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:43', 2),
(197, 180, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:43', 2),
(198, 181, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:43', 2),
(199, 182, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:43', 2),
(200, 183, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:44', 2),
(201, 184, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:44', 2),
(202, 185, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:44', 2),
(203, 186, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:44', 2),
(204, 187, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:45', 2),
(205, 188, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:45', 2),
(206, 189, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:45', 2),
(207, 190, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:45', 2),
(208, 191, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:45', 2),
(209, 192, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:46', 2),
(210, 193, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:46', 2),
(211, 194, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:46', 2),
(212, 195, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:46', 2),
(213, 196, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:46', 2),
(214, 197, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:47', 2),
(215, 198, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:47', 2),
(216, 199, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:47', 2),
(217, 200, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:47', 2),
(218, 201, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:47', 2),
(219, 202, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:48', 2),
(220, 203, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:48', 2),
(221, 204, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:48', 2),
(222, 205, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:48', 2),
(223, 206, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:07:48', 2),
(224, 207, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:55', 2),
(225, 208, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:55', 2),
(226, 209, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:55', 2),
(227, 210, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:55', 2),
(228, 211, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:55', 2),
(229, 212, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:56', 2),
(230, 213, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:56', 2),
(231, 214, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:56', 2),
(232, 215, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:56', 2),
(233, 216, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:56', 2),
(234, 217, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:57', 2),
(235, 218, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:57', 2),
(236, 219, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:57', 2),
(237, 220, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:57', 2),
(238, 221, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:58', 2),
(239, 222, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:58', 2),
(240, 223, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:58', 2),
(241, 224, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:58', 2),
(242, 225, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:58', 2),
(243, 226, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:59', 2),
(244, 227, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:59', 2),
(245, 228, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:59', 2),
(246, 229, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:08:59', 2),
(247, 230, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:09:00', 2),
(248, 231, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:09:00', 2),
(249, 232, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:09:00', 2),
(250, 233, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:09:00', 2),
(251, 234, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:09:00', 2),
(252, 235, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:09:01', 2),
(253, 236, 1, 'Acceso + Recursos', '16.67', 1, 'https://skscourses.com/json/', '2020-03-03', '2020-03-06 17:09:01', 2),
(254, 237, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-03-09', '2020-03-09 15:33:22', 2),
(255, 237, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-04-09', '2020-03-09 15:33:22', 2),
(256, 238, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-03-14', '2020-03-14 17:03:49', 2),
(257, 238, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-04-22', '2020-03-14 17:03:49', 2),
(258, 239, 1, 'Material', '60.00', 1, 'https://skscourses.com/json/', '2020-12-31', '2020-03-14 17:17:02', 2),
(259, 240, 1, 'Material', '60.00', 1, 'https://skscourses.com/json/', '2020-07-18', '2020-03-19 01:22:54', 2),
(260, 241, 1, 'Material', '60.00', 1, 'https://skscourses.com/json/', '2020-07-18', '2020-03-19 02:36:32', 2),
(261, 242, 1, 'Material', '60.00', 1, 'https://skscourses.com/json/', '2020-04-23', '2020-03-20 01:26:10', 2),
(262, 243, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:51', 2),
(263, 243, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:51', 2),
(264, 244, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:51', 2),
(265, 244, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:51', 2),
(266, 245, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:52', 2),
(267, 245, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:52', 2),
(268, 246, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:52', 2),
(269, 246, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:52', 2),
(270, 247, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:52', 2),
(271, 247, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:52', 2),
(272, 248, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:52', 2),
(273, 248, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:52', 2),
(274, 249, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:53', 2),
(275, 249, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:53', 2),
(276, 250, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:53', 2),
(277, 250, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:53', 2),
(278, 251, 1, 'Acceso al Curso', '84.00', 0, 'https://skscourses.com/json/', '2020-04-01', '2020-04-01 16:47:53', 2),
(279, 251, 2, 'Material', '36.00', 1, 'https://skscourses.com/json/', '2020-05-01', '2020-04-01 16:47:53', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matricula_pago`
--

CREATE TABLE `matricula_pago` (
  `id` bigint(20) NOT NULL,
  `numorden` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alumno` bigint(20) DEFAULT NULL,
  `idempresa_sede` bigint(20) DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `fecha_hora_registro` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `registrado_por` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` text COLLATE utf8_unicode_ci,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `matricula_pago`
--

INSERT INTO `matricula_pago` (`id`, `numorden`, `alumno`, `idempresa_sede`, `monto`, `fecha_hora_registro`, `registrado_por`, `file`, `comentario`, `estado`) VALUES
(1, 'P0001A0009', 9, NULL, '120.00', '2020-02-24 22:56:01', '3', NULL, NULL, 1),
(2, 'P0001A0013', 13, NULL, '60.00', '2020-02-25 22:34:03', '3', NULL, NULL, 1),
(3, 'P0001A0014', 14, NULL, '84.00', '2020-02-25 23:03:14', '3', NULL, NULL, 1),
(4, 'P0001A0015', 15, NULL, '120.00', '2020-02-25 21:10:16', '3', NULL, NULL, 1),
(5, 'P0002A0014', 14, NULL, '36.00', '2020-02-25 21:15:31', '3', '', NULL, 1),
(6, 'P0001A0016', 16, NULL, '120.00', '2020-02-25 23:30:24', '3', NULL, NULL, 1),
(7, 'P0001A0017', 17, NULL, '120.00', '2020-02-25 23:30:25', '3', NULL, NULL, 1),
(8, 'P0001A0018', 18, NULL, '120.00', '2020-02-25 23:30:25', '3', NULL, NULL, 1),
(9, 'P0001A0019', 19, NULL, '120.00', '2020-02-25 23:30:25', '3', NULL, NULL, 1),
(10, 'P0001A0020', 20, NULL, '120.00', '2020-02-25 23:30:25', '3', NULL, NULL, 1),
(11, 'P0001A0021', 21, NULL, '120.00', '2020-02-25 23:30:26', '3', NULL, NULL, 1),
(12, 'P0001A0022', 22, NULL, '120.00', '2020-02-26 14:03:24', '3', NULL, NULL, 1),
(13, 'P0001A0023', 23, NULL, '120.00', '2020-02-26 14:03:24', '3', NULL, NULL, 1),
(14, 'P0001A0024', 24, NULL, '120.00', '2020-02-26 14:03:24', '3', NULL, NULL, 1),
(15, 'P0001A0025', 25, NULL, '120.00', '2020-02-26 14:03:24', '3', NULL, NULL, 1),
(16, 'P0001A0026', 26, NULL, '120.00', '2020-02-26 14:03:25', '3', NULL, NULL, 1),
(17, 'P0001A0027', 27, NULL, '120.00', '2020-02-26 14:03:25', '3', NULL, NULL, 1),
(18, 'P0001A0028', 28, NULL, '84.00', '2020-02-27 01:29:05', '3', NULL, NULL, 1),
(19, 'P0001A0030', 30, NULL, '60.00', '2020-02-27 02:31:56', '3', NULL, NULL, 1),
(20, 'P0001A0031', 31, NULL, '120.00', '2020-02-27 01:06:14', '3', NULL, NULL, 1),
(21, 'P0001A0032', 32, NULL, '60.00', '2020-02-27 15:41:27', '3', NULL, NULL, 1),
(22, 'P0001A0033', 33, NULL, '120.00', '2020-02-28 21:54:22', '3', NULL, NULL, 1),
(23, 'P0002A0033', 33, NULL, '60.00', '2020-02-28 23:56:33', '3', NULL, NULL, 1),
(24, 'P0001A0055', 55, NULL, '16.67', '2020-03-06 16:57:52', '5', NULL, NULL, 1),
(25, 'P0001A0056', 56, NULL, '16.67', '2020-03-06 16:57:52', '5', NULL, NULL, 1),
(26, 'P0001A0057', 57, NULL, '16.67', '2020-03-06 16:57:52', '5', NULL, NULL, 1),
(27, 'P0001A0058', 58, NULL, '16.67', '2020-03-06 16:57:53', '5', NULL, NULL, 1),
(28, 'P0001A0059', 59, NULL, '16.67', '2020-03-06 16:57:53', '5', NULL, NULL, 1),
(29, 'P0001A0060', 60, NULL, '16.67', '2020-03-06 16:57:53', '5', NULL, NULL, 1),
(30, 'P0001A0061', 61, NULL, '16.67', '2020-03-06 16:57:53', '5', NULL, NULL, 1),
(31, 'P0001A0062', 62, NULL, '16.67', '2020-03-06 16:57:53', '5', NULL, NULL, 1),
(32, 'P0001A0063', 63, NULL, '16.67', '2020-03-06 16:57:54', '5', NULL, NULL, 1),
(33, 'P0001A0064', 64, NULL, '16.67', '2020-03-06 16:57:54', '5', NULL, NULL, 1),
(34, 'P0002A0055', 55, NULL, '16.67', '2020-03-06 16:57:54', '5', NULL, NULL, 1),
(35, 'P0002A0056', 56, NULL, '16.67', '2020-03-06 16:57:54', '5', NULL, NULL, 1),
(36, 'P0002A0057', 57, NULL, '16.67', '2020-03-06 16:57:55', '5', NULL, NULL, 1),
(37, 'P0002A0058', 58, NULL, '16.67', '2020-03-06 16:57:55', '5', NULL, NULL, 1),
(38, 'P0002A0059', 59, NULL, '16.67', '2020-03-06 16:57:55', '5', NULL, NULL, 1),
(39, 'P0002A0060', 60, NULL, '16.67', '2020-03-06 16:57:55', '5', NULL, NULL, 1),
(40, 'P0002A0061', 61, NULL, '16.67', '2020-03-06 16:57:55', '5', NULL, NULL, 1),
(41, 'P0002A0062', 62, NULL, '16.67', '2020-03-06 16:57:55', '5', NULL, NULL, 1),
(42, 'P0002A0063', 63, NULL, '16.67', '2020-03-06 16:57:56', '5', NULL, NULL, 1),
(43, 'P0002A0064', 64, NULL, '16.67', '2020-03-06 16:57:56', '5', NULL, NULL, 1),
(44, 'P0003A0055', 55, NULL, '16.67', '2020-03-06 16:57:56', '5', NULL, NULL, 1),
(45, 'P0003A0056', 56, NULL, '16.67', '2020-03-06 16:57:56', '5', NULL, NULL, 1),
(46, 'P0003A0057', 57, NULL, '16.67', '2020-03-06 16:57:57', '5', NULL, NULL, 1),
(47, 'P0003A0058', 58, NULL, '16.67', '2020-03-06 16:57:57', '5', NULL, NULL, 1),
(48, 'P0003A0059', 59, NULL, '16.67', '2020-03-06 16:57:57', '5', NULL, NULL, 1),
(49, 'P0003A0060', 60, NULL, '16.67', '2020-03-06 16:57:57', '5', NULL, NULL, 1),
(50, 'P0003A0061', 61, NULL, '16.67', '2020-03-06 16:57:57', '5', NULL, NULL, 1),
(51, 'P0003A0062', 62, NULL, '16.67', '2020-03-06 16:57:58', '5', NULL, NULL, 1),
(52, 'P0003A0063', 63, NULL, '16.67', '2020-03-06 16:57:58', '5', NULL, NULL, 1),
(53, 'P0003A0064', 64, NULL, '16.67', '2020-03-06 16:57:58', '5', NULL, NULL, 1),
(54, 'P0001A0065', 65, NULL, '16.67', '2020-03-06 17:01:16', '5', NULL, NULL, 1),
(55, 'P0001A0066', 66, NULL, '16.67', '2020-03-06 17:01:16', '5', NULL, NULL, 1),
(56, 'P0001A0067', 67, NULL, '16.67', '2020-03-06 17:01:16', '5', NULL, NULL, 1),
(57, 'P0001A0068', 68, NULL, '16.67', '2020-03-06 17:01:16', '5', NULL, NULL, 1),
(58, 'P0001A0069', 69, NULL, '16.67', '2020-03-06 17:01:16', '5', NULL, NULL, 1),
(59, 'P0001A0070', 70, NULL, '16.67', '2020-03-06 17:01:17', '5', NULL, NULL, 1),
(60, 'P0001A0071', 71, NULL, '16.67', '2020-03-06 17:01:17', '5', NULL, NULL, 1),
(61, 'P0001A0072', 72, NULL, '16.67', '2020-03-06 17:01:17', '5', NULL, NULL, 1),
(62, 'P0001A0073', 73, NULL, '16.67', '2020-03-06 17:01:17', '5', NULL, NULL, 1),
(63, 'P0001A0074', 74, NULL, '16.67', '2020-03-06 17:01:17', '5', NULL, NULL, 1),
(64, 'P0002A0065', 65, NULL, '16.67', '2020-03-06 17:01:18', '5', NULL, NULL, 1),
(65, 'P0002A0066', 66, NULL, '16.67', '2020-03-06 17:01:18', '5', NULL, NULL, 1),
(66, 'P0002A0067', 67, NULL, '16.67', '2020-03-06 17:01:18', '5', NULL, NULL, 1),
(67, 'P0002A0068', 68, NULL, '16.67', '2020-03-06 17:01:18', '5', NULL, NULL, 1),
(68, 'P0002A0069', 69, NULL, '16.67', '2020-03-06 17:01:19', '5', NULL, NULL, 1),
(69, 'P0002A0070', 70, NULL, '16.67', '2020-03-06 17:01:19', '5', NULL, NULL, 1),
(70, 'P0002A0071', 71, NULL, '16.67', '2020-03-06 17:01:19', '5', NULL, NULL, 1),
(71, 'P0002A0072', 72, NULL, '16.67', '2020-03-06 17:01:19', '5', NULL, NULL, 1),
(72, 'P0002A0073', 73, NULL, '16.67', '2020-03-06 17:01:19', '5', NULL, NULL, 1),
(73, 'P0002A0074', 74, NULL, '16.67', '2020-03-06 17:01:19', '5', NULL, NULL, 1),
(74, 'P0003A0065', 65, NULL, '16.67', '2020-03-06 17:01:20', '5', NULL, NULL, 1),
(75, 'P0003A0066', 66, NULL, '16.67', '2020-03-06 17:01:20', '5', NULL, NULL, 1),
(76, 'P0003A0067', 67, NULL, '16.67', '2020-03-06 17:01:20', '5', NULL, NULL, 1),
(77, 'P0003A0068', 68, NULL, '16.67', '2020-03-06 17:01:20', '5', NULL, NULL, 1),
(78, 'P0003A0069', 69, NULL, '16.67', '2020-03-06 17:01:21', '5', NULL, NULL, 1),
(79, 'P0003A0070', 70, NULL, '16.67', '2020-03-06 17:01:21', '5', NULL, NULL, 1),
(80, 'P0003A0071', 71, NULL, '16.67', '2020-03-06 17:01:21', '5', NULL, NULL, 1),
(81, 'P0003A0072', 72, NULL, '16.67', '2020-03-06 17:01:21', '5', NULL, NULL, 1),
(82, 'P0003A0073', 73, NULL, '16.67', '2020-03-06 17:01:21', '5', NULL, NULL, 1),
(83, 'P0003A0074', 74, NULL, '16.67', '2020-03-06 17:01:22', '5', NULL, NULL, 1),
(84, 'P0001A0075', 75, NULL, '16.67', '2020-03-06 17:03:03', '5', NULL, NULL, 1),
(85, 'P0001A0076', 76, NULL, '16.67', '2020-03-06 17:03:03', '5', NULL, NULL, 1),
(86, 'P0001A0077', 77, NULL, '16.67', '2020-03-06 17:03:03', '5', NULL, NULL, 1),
(87, 'P0001A0078', 78, NULL, '16.67', '2020-03-06 17:03:03', '5', NULL, NULL, 1),
(88, 'P0001A0079', 79, NULL, '16.67', '2020-03-06 17:03:03', '5', NULL, NULL, 1),
(89, 'P0001A0080', 80, NULL, '16.67', '2020-03-06 17:03:03', '5', NULL, NULL, 1),
(90, 'P0001A0081', 81, NULL, '16.67', '2020-03-06 17:03:04', '5', NULL, NULL, 1),
(91, 'P0001A0082', 82, NULL, '16.67', '2020-03-06 17:03:04', '5', NULL, NULL, 1),
(92, 'P0001A0083', 83, NULL, '16.67', '2020-03-06 17:03:04', '5', NULL, NULL, 1),
(93, 'P0001A0084', 84, NULL, '16.67', '2020-03-06 17:03:04', '5', NULL, NULL, 1),
(94, 'P0002A0075', 75, NULL, '16.67', '2020-03-06 17:03:05', '5', NULL, NULL, 1),
(95, 'P0002A0076', 76, NULL, '16.67', '2020-03-06 17:03:05', '5', NULL, NULL, 1),
(96, 'P0002A0077', 77, NULL, '16.67', '2020-03-06 17:03:05', '5', NULL, NULL, 1),
(97, 'P0002A0078', 78, NULL, '16.67', '2020-03-06 17:03:05', '5', NULL, NULL, 1),
(98, 'P0002A0079', 79, NULL, '16.67', '2020-03-06 17:03:05', '5', NULL, NULL, 1),
(99, 'P0002A0080', 80, NULL, '16.67', '2020-03-06 17:03:06', '5', NULL, NULL, 1),
(100, 'P0002A0081', 81, NULL, '16.67', '2020-03-06 17:03:06', '5', NULL, NULL, 1),
(101, 'P0002A0082', 82, NULL, '16.67', '2020-03-06 17:03:06', '5', NULL, NULL, 1),
(102, 'P0002A0083', 83, NULL, '16.67', '2020-03-06 17:03:06', '5', NULL, NULL, 1),
(103, 'P0002A0084', 84, NULL, '16.67', '2020-03-06 17:03:06', '5', NULL, NULL, 1),
(104, 'P0003A0075', 75, NULL, '16.67', '2020-03-06 17:03:07', '5', NULL, NULL, 1),
(105, 'P0003A0076', 76, NULL, '16.67', '2020-03-06 17:03:07', '5', NULL, NULL, 1),
(106, 'P0003A0077', 77, NULL, '16.67', '2020-03-06 17:03:07', '5', NULL, NULL, 1),
(107, 'P0003A0078', 78, NULL, '16.67', '2020-03-06 17:03:07', '5', NULL, NULL, 1),
(108, 'P0003A0079', 79, NULL, '16.67', '2020-03-06 17:03:08', '5', NULL, NULL, 1),
(109, 'P0003A0080', 80, NULL, '16.67', '2020-03-06 17:03:08', '5', NULL, NULL, 1),
(110, 'P0003A0081', 81, NULL, '16.67', '2020-03-06 17:03:08', '5', NULL, NULL, 1),
(111, 'P0003A0082', 82, NULL, '16.67', '2020-03-06 17:03:08', '5', NULL, NULL, 1),
(112, 'P0003A0083', 83, NULL, '16.67', '2020-03-06 17:03:08', '5', NULL, NULL, 1),
(113, 'P0003A0084', 84, NULL, '16.67', '2020-03-06 17:03:09', '5', NULL, NULL, 1),
(114, 'P0001A0085', 85, NULL, '16.67', '2020-03-06 17:04:24', '5', NULL, NULL, 1),
(115, 'P0001A0086', 86, NULL, '16.67', '2020-03-06 17:04:24', '5', NULL, NULL, 1),
(116, 'P0001A0087', 87, NULL, '16.67', '2020-03-06 17:04:24', '5', NULL, NULL, 1),
(117, 'P0001A0088', 88, NULL, '16.67', '2020-03-06 17:04:24', '5', NULL, NULL, 1),
(118, 'P0001A0089', 89, NULL, '16.67', '2020-03-06 17:04:24', '5', NULL, NULL, 1),
(119, 'P0001A0090', 90, NULL, '16.67', '2020-03-06 17:04:25', '5', NULL, NULL, 1),
(120, 'P0001A0091', 91, NULL, '16.67', '2020-03-06 17:04:25', '5', NULL, NULL, 1),
(121, 'P0001A0092', 92, NULL, '16.67', '2020-03-06 17:04:25', '5', NULL, NULL, 1),
(122, 'P0001A0093', 93, NULL, '16.67', '2020-03-06 17:04:25', '5', NULL, NULL, 1),
(123, 'P0001A0094', 94, NULL, '16.67', '2020-03-06 17:04:25', '5', NULL, NULL, 1),
(124, 'P0002A0085', 85, NULL, '16.67', '2020-03-06 17:04:26', '5', NULL, NULL, 1),
(125, 'P0002A0086', 86, NULL, '16.67', '2020-03-06 17:04:26', '5', NULL, NULL, 1),
(126, 'P0002A0087', 87, NULL, '16.67', '2020-03-06 17:04:26', '5', NULL, NULL, 1),
(127, 'P0002A0088', 88, NULL, '16.67', '2020-03-06 17:04:26', '5', NULL, NULL, 1),
(128, 'P0002A0089', 89, NULL, '16.67', '2020-03-06 17:04:26', '5', NULL, NULL, 1),
(129, 'P0002A0090', 90, NULL, '16.67', '2020-03-06 17:04:27', '5', NULL, NULL, 1),
(130, 'P0002A0091', 91, NULL, '16.67', '2020-03-06 17:04:27', '5', NULL, NULL, 1),
(131, 'P0002A0092', 92, NULL, '16.67', '2020-03-06 17:04:27', '5', NULL, NULL, 1),
(132, 'P0002A0093', 93, NULL, '16.67', '2020-03-06 17:04:27', '5', NULL, NULL, 1),
(133, 'P0002A0094', 94, NULL, '16.67', '2020-03-06 17:04:27', '5', NULL, NULL, 1),
(134, 'P0003A0085', 85, NULL, '16.67', '2020-03-06 17:04:28', '5', NULL, NULL, 1),
(135, 'P0003A0086', 86, NULL, '16.67', '2020-03-06 17:04:28', '5', NULL, NULL, 1),
(136, 'P0003A0087', 87, NULL, '16.67', '2020-03-06 17:04:28', '5', NULL, NULL, 1),
(137, 'P0003A0088', 88, NULL, '16.67', '2020-03-06 17:04:28', '5', NULL, NULL, 1),
(138, 'P0003A0089', 89, NULL, '16.67', '2020-03-06 17:04:29', '5', NULL, NULL, 1),
(139, 'P0003A0090', 90, NULL, '16.67', '2020-03-06 17:04:29', '5', NULL, NULL, 1),
(140, 'P0003A0091', 91, NULL, '16.67', '2020-03-06 17:04:29', '5', NULL, NULL, 1),
(141, 'P0003A0092', 92, NULL, '16.67', '2020-03-06 17:04:29', '5', NULL, NULL, 1),
(142, 'P0003A0093', 93, NULL, '16.67', '2020-03-06 17:04:29', '5', NULL, NULL, 1),
(143, 'P0003A0094', 94, NULL, '16.67', '2020-03-06 17:04:30', '5', NULL, NULL, 1),
(144, 'P0001A0095', 95, NULL, '16.67', '2020-03-06 17:06:01', '5', NULL, NULL, 1),
(145, 'P0001A0096', 96, NULL, '16.67', '2020-03-06 17:06:01', '5', NULL, NULL, 1),
(146, 'P0001A0097', 97, NULL, '16.67', '2020-03-06 17:06:01', '5', NULL, NULL, 1),
(147, 'P0001A0098', 98, NULL, '16.67', '2020-03-06 17:06:01', '5', NULL, NULL, 1),
(148, 'P0001A0099', 99, NULL, '16.67', '2020-03-06 17:06:01', '5', NULL, NULL, 1),
(149, 'P0001A0100', 100, NULL, '16.67', '2020-03-06 17:06:02', '5', NULL, NULL, 1),
(150, 'P0001A0101', 101, NULL, '16.67', '2020-03-06 17:06:02', '5', NULL, NULL, 1),
(151, 'P0001A0102', 102, NULL, '16.67', '2020-03-06 17:06:02', '5', NULL, NULL, 1),
(152, 'P0001A0103', 103, NULL, '16.67', '2020-03-06 17:06:02', '5', NULL, NULL, 1),
(153, 'P0001A0104', 104, NULL, '16.67', '2020-03-06 17:06:02', '5', NULL, NULL, 1),
(154, 'P0002A0095', 95, NULL, '16.67', '2020-03-06 17:06:03', '5', NULL, NULL, 1),
(155, 'P0002A0096', 96, NULL, '16.67', '2020-03-06 17:06:03', '5', NULL, NULL, 1),
(156, 'P0002A0097', 97, NULL, '16.67', '2020-03-06 17:06:03', '5', NULL, NULL, 1),
(157, 'P0002A0098', 98, NULL, '16.67', '2020-03-06 17:06:03', '5', NULL, NULL, 1),
(158, 'P0002A0099', 99, NULL, '16.67', '2020-03-06 17:06:04', '5', NULL, NULL, 1),
(159, 'P0002A0100', 100, NULL, '16.67', '2020-03-06 17:06:04', '5', NULL, NULL, 1),
(160, 'P0002A0101', 101, NULL, '16.67', '2020-03-06 17:06:04', '5', NULL, NULL, 1),
(161, 'P0002A0102', 102, NULL, '16.67', '2020-03-06 17:06:04', '5', NULL, NULL, 1),
(162, 'P0002A0103', 103, NULL, '16.67', '2020-03-06 17:06:04', '5', NULL, NULL, 1),
(163, 'P0002A0104', 104, NULL, '16.67', '2020-03-06 17:06:05', '5', NULL, NULL, 1),
(164, 'P0003A0095', 95, NULL, '16.67', '2020-03-06 17:06:05', '5', NULL, NULL, 1),
(165, 'P0003A0096', 96, NULL, '16.67', '2020-03-06 17:06:05', '5', NULL, NULL, 1),
(166, 'P0003A0097', 97, NULL, '16.67', '2020-03-06 17:06:05', '5', NULL, NULL, 1),
(167, 'P0003A0098', 98, NULL, '16.67', '2020-03-06 17:06:06', '5', NULL, NULL, 1),
(168, 'P0003A0099', 99, NULL, '16.67', '2020-03-06 17:06:06', '5', NULL, NULL, 1),
(169, 'P0003A0100', 100, NULL, '16.67', '2020-03-06 17:06:06', '5', NULL, NULL, 1),
(170, 'P0003A0101', 101, NULL, '16.67', '2020-03-06 17:06:06', '5', NULL, NULL, 1),
(171, 'P0003A0102', 102, NULL, '16.67', '2020-03-06 17:06:06', '5', NULL, NULL, 1),
(172, 'P0003A0103', 103, NULL, '16.67', '2020-03-06 17:06:07', '5', NULL, NULL, 1),
(173, 'P0003A0104', 104, NULL, '16.67', '2020-03-06 17:06:07', '5', NULL, NULL, 1),
(174, 'P0001A0105', 105, NULL, '16.67', '2020-03-06 17:07:42', '5', NULL, NULL, 1),
(175, 'P0001A0106', 106, NULL, '16.67', '2020-03-06 17:07:43', '5', NULL, NULL, 1),
(176, 'P0001A0107', 107, NULL, '16.67', '2020-03-06 17:07:43', '5', NULL, NULL, 1),
(177, 'P0001A0108', 108, NULL, '16.67', '2020-03-06 17:07:43', '5', NULL, NULL, 1),
(178, 'P0001A0109', 109, NULL, '16.67', '2020-03-06 17:07:43', '5', NULL, NULL, 1),
(179, 'P0001A0110', 110, NULL, '16.67', '2020-03-06 17:07:43', '5', NULL, NULL, 1),
(180, 'P0001A0111', 111, NULL, '16.67', '2020-03-06 17:07:44', '5', NULL, NULL, 1),
(181, 'P0001A0112', 112, NULL, '16.67', '2020-03-06 17:07:44', '5', NULL, NULL, 1),
(182, 'P0001A0113', 113, NULL, '16.67', '2020-03-06 17:07:44', '5', NULL, NULL, 1),
(183, 'P0001A0114', 114, NULL, '16.67', '2020-03-06 17:07:44', '5', NULL, NULL, 1),
(184, 'P0002A0105', 105, NULL, '16.67', '2020-03-06 17:07:45', '5', NULL, NULL, 1),
(185, 'P0002A0106', 106, NULL, '16.67', '2020-03-06 17:07:45', '5', NULL, NULL, 1),
(186, 'P0002A0107', 107, NULL, '16.67', '2020-03-06 17:07:45', '5', NULL, NULL, 1),
(187, 'P0002A0108', 108, NULL, '16.67', '2020-03-06 17:07:45', '5', NULL, NULL, 1),
(188, 'P0002A0109', 109, NULL, '16.67', '2020-03-06 17:07:45', '5', NULL, NULL, 1),
(189, 'P0002A0110', 110, NULL, '16.67', '2020-03-06 17:07:46', '5', NULL, NULL, 1),
(190, 'P0002A0111', 111, NULL, '16.67', '2020-03-06 17:07:46', '5', NULL, NULL, 1),
(191, 'P0002A0112', 112, NULL, '16.67', '2020-03-06 17:07:46', '5', NULL, NULL, 1),
(192, 'P0002A0113', 113, NULL, '16.67', '2020-03-06 17:07:46', '5', NULL, NULL, 1),
(193, 'P0002A0114', 114, NULL, '16.67', '2020-03-06 17:07:46', '5', NULL, NULL, 1),
(194, 'P0003A0105', 105, NULL, '16.67', '2020-03-06 17:07:47', '5', NULL, NULL, 1),
(195, 'P0003A0106', 106, NULL, '16.67', '2020-03-06 17:07:47', '5', NULL, NULL, 1),
(196, 'P0003A0107', 107, NULL, '16.67', '2020-03-06 17:07:47', '5', NULL, NULL, 1),
(197, 'P0003A0108', 108, NULL, '16.67', '2020-03-06 17:07:47', '5', NULL, NULL, 1),
(198, 'P0003A0109', 109, NULL, '16.67', '2020-03-06 17:07:47', '5', NULL, NULL, 1),
(199, 'P0003A0110', 110, NULL, '16.67', '2020-03-06 17:07:48', '5', NULL, NULL, 1),
(200, 'P0003A0111', 111, NULL, '16.67', '2020-03-06 17:07:48', '5', NULL, NULL, 1),
(201, 'P0003A0112', 112, NULL, '16.67', '2020-03-06 17:07:48', '5', NULL, NULL, 1),
(202, 'P0003A0113', 113, NULL, '16.67', '2020-03-06 17:07:48', '5', NULL, NULL, 1),
(203, 'P0003A0114', 114, NULL, '16.67', '2020-03-06 17:07:48', '5', NULL, NULL, 1),
(204, 'P0001A0115', 115, NULL, '16.67', '2020-03-06 17:08:55', '5', NULL, NULL, 1),
(205, 'P0001A0116', 116, NULL, '16.67', '2020-03-06 17:08:55', '5', NULL, NULL, 1),
(206, 'P0001A0117', 117, NULL, '16.67', '2020-03-06 17:08:55', '5', NULL, NULL, 1),
(207, 'P0001A0118', 118, NULL, '16.67', '2020-03-06 17:08:55', '5', NULL, NULL, 1),
(208, 'P0001A0119', 119, NULL, '16.67', '2020-03-06 17:08:55', '5', NULL, NULL, 1),
(209, 'P0001A0120', 120, NULL, '16.67', '2020-03-06 17:08:56', '5', NULL, NULL, 1),
(210, 'P0001A0121', 121, NULL, '16.67', '2020-03-06 17:08:56', '5', NULL, NULL, 1),
(211, 'P0001A0122', 122, NULL, '16.67', '2020-03-06 17:08:56', '5', NULL, NULL, 1),
(212, 'P0001A0123', 123, NULL, '16.67', '2020-03-06 17:08:56', '5', NULL, NULL, 1),
(213, 'P0001A0124', 124, NULL, '16.67', '2020-03-06 17:08:56', '5', NULL, NULL, 1),
(214, 'P0002A0115', 115, NULL, '16.67', '2020-03-06 17:08:57', '5', NULL, NULL, 1),
(215, 'P0002A0116', 116, NULL, '16.67', '2020-03-06 17:08:57', '5', NULL, NULL, 1),
(216, 'P0002A0117', 117, NULL, '16.67', '2020-03-06 17:08:57', '5', NULL, NULL, 1),
(217, 'P0002A0118', 118, NULL, '16.67', '2020-03-06 17:08:57', '5', NULL, NULL, 1),
(218, 'P0002A0119', 119, NULL, '16.67', '2020-03-06 17:08:58', '5', NULL, NULL, 1),
(219, 'P0002A0120', 120, NULL, '16.67', '2020-03-06 17:08:58', '5', NULL, NULL, 1),
(220, 'P0002A0121', 121, NULL, '16.67', '2020-03-06 17:08:58', '5', NULL, NULL, 1),
(221, 'P0002A0122', 122, NULL, '16.67', '2020-03-06 17:08:58', '5', NULL, NULL, 1),
(222, 'P0002A0123', 123, NULL, '16.67', '2020-03-06 17:08:58', '5', NULL, NULL, 1),
(223, 'P0002A0124', 124, NULL, '16.67', '2020-03-06 17:08:59', '5', NULL, NULL, 1),
(224, 'P0003A0115', 115, NULL, '16.67', '2020-03-06 17:08:59', '5', NULL, NULL, 1),
(225, 'P0003A0116', 116, NULL, '16.67', '2020-03-06 17:08:59', '5', NULL, NULL, 1),
(226, 'P0003A0117', 117, NULL, '16.67', '2020-03-06 17:08:59', '5', NULL, NULL, 1),
(227, 'P0003A0118', 118, NULL, '16.67', '2020-03-06 17:09:00', '5', NULL, NULL, 1),
(228, 'P0003A0119', 119, NULL, '16.67', '2020-03-06 17:09:00', '5', NULL, NULL, 1),
(229, 'P0003A0120', 120, NULL, '16.67', '2020-03-06 17:09:00', '5', NULL, NULL, 1),
(230, 'P0003A0121', 121, NULL, '16.67', '2020-03-06 17:09:00', '5', NULL, NULL, 1),
(231, 'P0003A0122', 122, NULL, '16.67', '2020-03-06 17:09:00', '5', NULL, NULL, 1),
(232, 'P0003A0123', 123, NULL, '16.67', '2020-03-06 17:09:01', '5', NULL, NULL, 1),
(233, 'P0003A0124', 124, NULL, '16.67', '2020-03-06 17:09:01', '5', NULL, NULL, 1),
(234, 'P0001A0125', 125, 0, '120.00', '2020-03-09 14:33:22', '3', NULL, NULL, 1),
(235, 'P0001A0127', 127, NULL, '120.00', '2020-03-14 16:03:49', '3', NULL, NULL, 1),
(236, 'P0002A0127', 127, NULL, '60.00', '2020-03-14 17:17:02', '3', NULL, NULL, 1),
(237, 'P0001A0130', 130, NULL, '60.00', '2020-03-19 01:22:54', '3', NULL, NULL, 1),
(238, 'P0001A0131', 131, NULL, '60.00', '2020-03-19 02:36:32', '3', NULL, NULL, 1),
(239, 'P0002A0131', 131, NULL, '60.00', '2020-03-20 01:26:10', '3', NULL, NULL, 1),
(240, 'P0001A0133', 133, NULL, '120.00', '2020-04-01 15:47:51', '3', NULL, NULL, 1),
(241, 'P0001A0135', 135, NULL, '120.00', '2020-04-01 15:47:51', '3', NULL, NULL, 1),
(242, 'P0001A0134', 134, NULL, '120.00', '2020-04-01 15:47:52', '3', NULL, NULL, 1),
(243, 'P0002A0133', 133, NULL, '120.00', '2020-04-01 15:47:52', '3', NULL, NULL, 1),
(244, 'P0002A0135', 135, NULL, '120.00', '2020-04-01 15:47:52', '3', NULL, NULL, 1),
(245, 'P0002A0134', 134, NULL, '120.00', '2020-04-01 15:47:52', '3', NULL, NULL, 1),
(246, 'P0003A0133', 133, NULL, '120.00', '2020-04-01 15:47:53', '3', NULL, NULL, 1),
(247, 'P0003A0135', 135, NULL, '120.00', '2020-04-01 15:47:53', '3', NULL, NULL, 1),
(248, 'P0003A0134', 134, NULL, '120.00', '2020-04-01 15:47:53', '3', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matricula_pago_detalle`
--

CREATE TABLE `matricula_pago_detalle` (
  `id` bigint(20) NOT NULL,
  `pago` bigint(20) DEFAULT NULL,
  `cuota` bigint(20) DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `monto` decimal(10,2) DEFAULT NULL,
  `fecha_pago` timestamp NULL DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `matricula_pago_detalle`
--

INSERT INTO `matricula_pago_detalle` (`id`, `pago`, `cuota`, `descripcion`, `monto`, `fecha_pago`, `estado`) VALUES
(1, 1, 4, 'Acceso al Curso', '84.00', '2020-02-25 00:56:01', 1),
(2, 1, 5, 'Material', '36.00', '2020-02-25 00:56:01', 1),
(3, 2, 6, 'Material', '60.00', '2020-02-25 22:34:03', 1),
(4, 3, 7, 'Acceso al Curso', '84.00', '2020-02-25 23:03:14', 1),
(5, 4, 9, 'Acceso al Curso', '84.00', '2020-02-25 23:10:16', 1),
(6, 4, 10, 'Material', '36.00', '2020-02-25 23:10:16', 1),
(7, 5, 8, 'Material', '36.00', '2020-02-25 23:15:31', 1),
(8, 6, 11, 'Acceso al Curso', '84.00', '2020-02-26 01:30:24', 1),
(9, 6, 12, 'Material', '36.00', '2020-02-26 01:30:24', 1),
(10, 7, 13, 'Acceso al Curso', '84.00', '2020-02-26 01:30:25', 1),
(11, 7, 14, 'Material', '36.00', '2020-02-26 01:30:25', 1),
(12, 8, 15, 'Acceso al Curso', '84.00', '2020-02-26 01:30:25', 1),
(13, 8, 16, 'Material', '36.00', '2020-02-26 01:30:25', 1),
(14, 9, 17, 'Acceso al Curso', '84.00', '2020-02-26 01:30:25', 1),
(15, 9, 18, 'Material', '36.00', '2020-02-26 01:30:25', 1),
(16, 10, 19, 'Acceso al Curso', '84.00', '2020-02-26 01:30:25', 1),
(17, 10, 20, 'Material', '36.00', '2020-02-26 01:30:25', 1),
(18, 11, 21, 'Acceso al Curso', '84.00', '2020-02-26 01:30:26', 1),
(19, 11, 22, 'Material', '36.00', '2020-02-26 01:30:26', 1),
(20, 12, 23, 'Acceso al Curso', '84.00', '2020-02-26 16:03:24', 1),
(21, 12, 24, 'Material', '36.00', '2020-02-26 16:03:24', 1),
(22, 13, 25, 'Acceso al Curso', '84.00', '2020-02-26 16:03:24', 1),
(23, 13, 26, 'Material', '36.00', '2020-02-26 16:03:24', 1),
(24, 14, 27, 'Acceso al Curso', '84.00', '2020-02-26 16:03:24', 1),
(25, 14, 28, 'Material', '36.00', '2020-02-26 16:03:24', 1),
(26, 15, 29, 'Acceso al Curso', '84.00', '2020-02-26 16:03:24', 1),
(27, 15, 30, 'Material', '36.00', '2020-02-26 16:03:24', 1),
(28, 16, 31, 'Acceso al Curso', '84.00', '2020-02-26 16:03:25', 1),
(29, 16, 32, 'Material', '36.00', '2020-02-26 16:03:25', 1),
(30, 17, 33, 'Acceso al Curso', '84.00', '2020-02-26 16:03:25', 1),
(31, 17, 34, 'Material', '36.00', '2020-02-26 16:03:25', 1),
(32, 18, 35, 'Acceso al Curso', '84.00', '2020-02-27 01:29:05', 1),
(33, 19, 37, 'Material', '60.00', '2020-02-27 02:31:56', 1),
(34, 20, 38, 'Acceso al Curso', '84.00', '2020-02-27 03:06:14', 1),
(35, 20, 39, 'Material', '36.00', '2020-02-27 03:06:14', 1),
(36, 21, 40, 'Material', '60.00', '2020-02-27 15:41:27', 1),
(37, 22, 41, 'Acceso al Curso', '84.00', '2020-02-28 23:54:22', 1),
(38, 22, 42, 'Material', '36.00', '2020-02-28 23:54:22', 1),
(39, 23, 43, 'Material', '60.00', '2020-02-28 23:56:33', 1),
(40, 24, 44, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:52', 1),
(41, 25, 45, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:52', 1),
(42, 26, 46, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:52', 1),
(43, 27, 47, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:53', 1),
(44, 28, 48, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:53', 1),
(45, 29, 49, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:53', 1),
(46, 30, 50, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:53', 1),
(47, 31, 51, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:53', 1),
(48, 32, 52, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:54', 1),
(49, 33, 53, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:54', 1),
(50, 34, 54, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:54', 1),
(51, 35, 55, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:54', 1),
(52, 36, 56, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:55', 1),
(53, 37, 57, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:55', 1),
(54, 38, 58, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:55', 1),
(55, 39, 59, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:55', 1),
(56, 40, 60, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:55', 1),
(57, 41, 61, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:55', 1),
(58, 42, 62, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:56', 1),
(59, 43, 63, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:56', 1),
(60, 44, 64, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:56', 1),
(61, 45, 65, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:57', 1),
(62, 46, 66, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:57', 1),
(63, 47, 67, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:57', 1),
(64, 48, 68, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:57', 1),
(65, 49, 69, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:57', 1),
(66, 50, 70, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:57', 1),
(67, 51, 71, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:58', 1),
(68, 52, 72, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:58', 1),
(69, 53, 73, 'Acceso + Recursos', '16.67', '2020-03-06 16:57:58', 1),
(70, 54, 74, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:16', 1),
(71, 55, 75, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:16', 1),
(72, 56, 76, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:16', 1),
(73, 57, 77, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:16', 1),
(74, 58, 78, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:16', 1),
(75, 59, 79, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:17', 1),
(76, 60, 80, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:17', 1),
(77, 61, 81, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:17', 1),
(78, 62, 82, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:17', 1),
(79, 63, 83, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:17', 1),
(80, 64, 84, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:18', 1),
(81, 65, 85, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:18', 1),
(82, 66, 86, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:18', 1),
(83, 67, 87, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:18', 1),
(84, 68, 88, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:19', 1),
(85, 69, 89, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:19', 1),
(86, 70, 90, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:19', 1),
(87, 71, 91, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:19', 1),
(88, 72, 92, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:19', 1),
(89, 73, 93, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:20', 1),
(90, 74, 94, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:20', 1),
(91, 75, 95, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:20', 1),
(92, 76, 96, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:20', 1),
(93, 77, 97, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:20', 1),
(94, 78, 98, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:21', 1),
(95, 79, 99, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:21', 1),
(96, 80, 100, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:21', 1),
(97, 81, 101, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:21', 1),
(98, 82, 102, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:21', 1),
(99, 83, 103, 'Acceso + Recursos', '16.67', '2020-03-06 17:01:22', 1),
(100, 84, 104, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:03', 1),
(101, 85, 105, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:03', 1),
(102, 86, 106, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:03', 1),
(103, 87, 107, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:03', 1),
(104, 88, 108, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:03', 1),
(105, 89, 109, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:03', 1),
(106, 90, 110, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:04', 1),
(107, 91, 111, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:04', 1),
(108, 92, 112, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:04', 1),
(109, 93, 113, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:04', 1),
(110, 94, 114, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:05', 1),
(111, 95, 115, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:05', 1),
(112, 96, 116, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:05', 1),
(113, 97, 117, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:05', 1),
(114, 98, 118, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:05', 1),
(115, 99, 119, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:06', 1),
(116, 100, 120, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:06', 1),
(117, 101, 121, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:06', 1),
(118, 102, 122, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:06', 1),
(119, 103, 123, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:06', 1),
(120, 104, 124, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:07', 1),
(121, 105, 125, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:07', 1),
(122, 106, 126, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:07', 1),
(123, 107, 127, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:07', 1),
(124, 108, 128, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:08', 1),
(125, 109, 129, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:08', 1),
(126, 110, 130, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:08', 1),
(127, 111, 131, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:08', 1),
(128, 112, 132, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:08', 1),
(129, 113, 133, 'Acceso + Recursos', '16.67', '2020-03-06 17:03:09', 1),
(130, 114, 134, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:24', 1),
(131, 115, 135, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:24', 1),
(132, 116, 136, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:24', 1),
(133, 117, 137, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:24', 1),
(134, 118, 138, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:24', 1),
(135, 119, 139, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:25', 1),
(136, 120, 140, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:25', 1),
(137, 121, 141, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:25', 1),
(138, 122, 142, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:25', 1),
(139, 123, 143, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:25', 1),
(140, 124, 144, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:26', 1),
(141, 125, 145, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:26', 1),
(142, 126, 146, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:26', 1),
(143, 127, 147, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:26', 1),
(144, 128, 148, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:27', 1),
(145, 129, 149, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:27', 1),
(146, 130, 150, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:27', 1),
(147, 131, 151, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:27', 1),
(148, 132, 152, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:27', 1),
(149, 133, 153, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:27', 1),
(150, 134, 154, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:28', 1),
(151, 135, 155, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:28', 1),
(152, 136, 156, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:28', 1),
(153, 137, 157, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:28', 1),
(154, 138, 158, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:29', 1),
(155, 139, 159, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:29', 1),
(156, 140, 160, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:29', 1),
(157, 141, 161, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:29', 1),
(158, 142, 162, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:29', 1),
(159, 143, 163, 'Acceso + Recursos', '16.67', '2020-03-06 17:04:30', 1),
(160, 144, 164, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:01', 1),
(161, 145, 165, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:01', 1),
(162, 146, 166, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:01', 1),
(163, 147, 167, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:01', 1),
(164, 148, 168, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:01', 1),
(165, 149, 169, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:02', 1),
(166, 150, 170, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:02', 1),
(167, 151, 171, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:02', 1),
(168, 152, 172, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:02', 1),
(169, 153, 173, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:02', 1),
(170, 154, 174, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:03', 1),
(171, 155, 175, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:03', 1),
(172, 156, 176, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:03', 1),
(173, 157, 177, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:03', 1),
(174, 158, 178, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:04', 1),
(175, 159, 179, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:04', 1),
(176, 160, 180, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:04', 1),
(177, 161, 181, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:04', 1),
(178, 162, 182, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:04', 1),
(179, 163, 183, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:05', 1),
(180, 164, 184, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:05', 1),
(181, 165, 185, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:05', 1),
(182, 166, 186, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:05', 1),
(183, 167, 187, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:06', 1),
(184, 168, 188, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:06', 1),
(185, 169, 189, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:06', 1),
(186, 170, 190, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:06', 1),
(187, 171, 191, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:06', 1),
(188, 172, 192, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:07', 1),
(189, 173, 193, 'Acceso + Recursos', '16.67', '2020-03-06 17:06:07', 1),
(190, 174, 194, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:42', 1),
(191, 175, 195, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:43', 1),
(192, 176, 196, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:43', 1),
(193, 177, 197, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:43', 1),
(194, 178, 198, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:43', 1),
(195, 179, 199, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:43', 1),
(196, 180, 200, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:44', 1),
(197, 181, 201, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:44', 1),
(198, 182, 202, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:44', 1),
(199, 183, 203, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:44', 1),
(200, 184, 204, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:45', 1),
(201, 185, 205, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:45', 1),
(202, 186, 206, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:45', 1),
(203, 187, 207, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:45', 1),
(204, 188, 208, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:45', 1),
(205, 189, 209, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:46', 1),
(206, 190, 210, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:46', 1),
(207, 191, 211, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:46', 1),
(208, 192, 212, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:46', 1),
(209, 193, 213, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:46', 1),
(210, 194, 214, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:47', 1),
(211, 195, 215, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:47', 1),
(212, 196, 216, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:47', 1),
(213, 197, 217, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:47', 1),
(214, 198, 218, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:47', 1),
(215, 199, 219, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:48', 1),
(216, 200, 220, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:48', 1),
(217, 201, 221, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:48', 1),
(218, 202, 222, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:48', 1),
(219, 203, 223, 'Acceso + Recursos', '16.67', '2020-03-06 17:07:48', 1),
(220, 204, 224, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:55', 1),
(221, 205, 225, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:55', 1),
(222, 206, 226, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:55', 1),
(223, 207, 227, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:55', 1),
(224, 208, 228, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:56', 1),
(225, 209, 229, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:56', 1),
(226, 210, 230, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:56', 1),
(227, 211, 231, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:56', 1),
(228, 212, 232, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:56', 1),
(229, 213, 233, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:56', 1),
(230, 214, 234, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:57', 1),
(231, 215, 235, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:57', 1),
(232, 216, 236, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:57', 1),
(233, 217, 237, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:57', 1),
(234, 218, 238, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:58', 1),
(235, 219, 239, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:58', 1),
(236, 220, 240, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:58', 1),
(237, 221, 241, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:58', 1),
(238, 222, 242, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:58', 1),
(239, 223, 243, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:59', 1),
(240, 224, 244, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:59', 1),
(241, 225, 245, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:59', 1),
(242, 226, 246, 'Acceso + Recursos', '16.67', '2020-03-06 17:08:59', 1),
(243, 227, 247, 'Acceso + Recursos', '16.67', '2020-03-06 17:09:00', 1),
(244, 228, 248, 'Acceso + Recursos', '16.67', '2020-03-06 17:09:00', 1),
(245, 229, 249, 'Acceso + Recursos', '16.67', '2020-03-06 17:09:00', 1),
(246, 230, 250, 'Acceso + Recursos', '16.67', '2020-03-06 17:09:00', 1),
(247, 231, 251, 'Acceso + Recursos', '16.67', '2020-03-06 17:09:00', 1),
(248, 232, 252, 'Acceso + Recursos', '16.67', '2020-03-06 17:09:01', 1),
(249, 233, 253, 'Acceso + Recursos', '16.67', '2020-03-06 17:09:01', 1),
(250, 234, 254, 'Acceso al Curso', '84.00', '2020-03-09 15:33:22', 1),
(251, 234, 255, 'Material', '36.00', '2020-03-09 15:33:22', 1),
(252, 235, 256, 'Acceso al Curso', '84.00', '2020-03-14 17:03:49', 1),
(253, 235, 257, 'Material', '36.00', '2020-03-14 17:03:49', 1),
(254, 236, 258, 'Material', '60.00', '2020-03-14 17:17:02', 1),
(255, 237, 259, 'Material', '60.00', '2020-03-19 01:22:54', 1),
(256, 238, 260, 'Material', '60.00', '2020-03-19 02:36:32', 1),
(257, 239, 261, 'Material', '60.00', '2020-03-20 01:26:10', 1),
(258, 240, 262, 'Acceso al Curso', '84.00', '2020-04-01 16:47:51', 1),
(259, 240, 263, 'Material', '36.00', '2020-04-01 16:47:51', 1),
(260, 241, 264, 'Acceso al Curso', '84.00', '2020-04-01 16:47:51', 1),
(261, 241, 265, 'Material', '36.00', '2020-04-01 16:47:51', 1),
(262, 242, 266, 'Acceso al Curso', '84.00', '2020-04-01 16:47:52', 1),
(263, 242, 267, 'Material', '36.00', '2020-04-01 16:47:52', 1),
(264, 243, 268, 'Acceso al Curso', '84.00', '2020-04-01 16:47:52', 1),
(265, 243, 269, 'Material', '36.00', '2020-04-01 16:47:52', 1),
(266, 244, 270, 'Acceso al Curso', '84.00', '2020-04-01 16:47:52', 1),
(267, 244, 271, 'Material', '36.00', '2020-04-01 16:47:52', 1),
(268, 245, 272, 'Acceso al Curso', '84.00', '2020-04-01 16:47:52', 1),
(269, 245, 273, 'Material', '36.00', '2020-04-01 16:47:52', 1),
(270, 246, 274, 'Acceso al Curso', '84.00', '2020-04-01 16:47:53', 1),
(271, 246, 275, 'Material', '36.00', '2020-04-01 16:47:53', 1),
(272, 247, 276, 'Acceso al Curso', '84.00', '2020-04-01 16:47:53', 1),
(273, 247, 277, 'Material', '36.00', '2020-04-01 16:47:53', 1),
(274, 248, 278, 'Acceso al Curso', '84.00', '2020-04-01 16:47:53', 1),
(275, 248, 279, 'Material', '36.00', '2020-04-01 16:47:53', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `codigo_menu` bigint(20) NOT NULL,
  `icono` varchar(30) COLLATE utf8_unicode_ci DEFAULT 'fa-circle-o',
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `nivel` int(11) NOT NULL DEFAULT '1',
  `orden` int(11) NOT NULL DEFAULT '0',
  `archivo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_padre` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`codigo_menu`, `icono`, `nombre`, `descripcion`, `estado`, `nivel`, `orden`, `archivo`, `id_padre`) VALUES
(1, 'fa-folder-open', 'Administrativo', '', 'A', 1, 1, '', NULL),
(2, 'fa-user', 'Representantes', '', 'A', 2, 1, 'representantes', 1),
(3, 'fa-toggle-on', 'Accesos a Productos', '', 'A', 2, 2, 'acceso', 1),
(4, 'fa-gears', 'Mantenimiento', '', 'A', 1, 4, '', NULL),
(5, 'fa-list', 'Menú', '', 'A', 2, 1, 'menu', 4),
(6, 'fa-toggle-on', 'Permisos', '', 'A', 2, 2, 'menu/permisos', 4),
(7, 'fa-clone', 'Licencias', '', 'A', 2, 0, 'licencias', 1),
(8, 'fa-graduation-cap', 'Pedagógico', '', 'A', 1, 2, '', NULL),
(10, 'fa-random', 'Transferir Estudiante', '', 'A', 2, 3, 'matriculas/reasignar', 8),
(11, 'fa-cubes', 'Grupo de Estudio', '', 'A', 2, 2, 'externa/grupoAulaList', 8),
(12, 'fa-credit-card', 'Pago Representante', '', 'A', 2, 0, 'pago', 1),
(13, 'fa-line-chart', 'Reportes', '', 'A', 1, 3, '', NULL),
(14, 'fa-file-o', 'Orden de Servicio', '', 'A', 2, 0, 'reportes/ordenservicio', 13),
(15, 'fa-money', 'Moneda', '', 'A', 2, 0, 'moneda', 4),
(16, 'fa-male', 'Usuarios', '', 'A', 2, 0, 'persona/usuarios', 1),
(17, 'fa-tags', 'Roles', '', 'A', 2, 0, 'roles', 4),
(18, 'fa-pie-chart', 'Seguimiento', 'Reporte de seguimiento de los usuarios de los representantes', 'A', 2, 0, 'reportes/seguimiento', 13),
(19, 'fa-briefcase', 'Registrar Docente', '', 'A', 2, 1, 'docentes', 8),
(24, 'fa-circle-o', 'Aprobar Matrícula', '', 'A', 2, 0, 'matriculas/aprobarmatricula', 13),
(25, 'fa-circle-o', 'Matriculas', '', 'A', 2, 3, 'reportes/matriculas', 13),
(26, 'fa-credit-card', 'Pago Estudiante', '', 'A', 2, 0, 'pago/alumnos', 1),
(27, 'fa-graduation-cap', 'Matrícula', '', 'A', 2, 3, '', 8),
(28, 'fa-book', 'Libre', '', 'A', 3, 1, 'matriculas/libre', 27),
(29, 'fa-database', 'Carrera', '', 'A', 3, 2, 'matriculas/carrera', 27),
(30, 'fa-line-chart', 'Ingresos', '', 'A', 2, 0, 'reportes/pagos', 13),
(31, 'fa-bar-chart-o', 'Ventas', '', 'A', 2, 0, 'reportes/ventas', 13),
(32, 'fa-calculator', 'Configurar', '', 'A', 2, 0, 'empresa/configurar', 1),
(33, 'fa-bars', 'Listado', '', 'A', 3, 3, 'matriculas', 27),
(34, 'fa-sliders', 'Promociones', '', 'A', 2, 0, 'empresa/promocion', 1),
(35, 'fa-building-o', 'Sedes', '', 'A', 2, 0, '/empresa/sedes', 1),
(36, 'fa-book', 'Libre', '', 'A', 3, 1, 'matriculas/registrolibre', 27);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_accesos`
--

CREATE TABLE `menu_accesos` (
  `menu` bigint(20) NOT NULL,
  `rol` int(11) NOT NULL,
  `acceso` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `agregar` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `modificar` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `eliminar` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `imprimir` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `menu_accesos`
--

INSERT INTO `menu_accesos` (`menu`, `rol`, `acceso`, `agregar`, `modificar`, `eliminar`, `imprimir`) VALUES
(1, 1, '1', '1', '1', '1', '1'),
(1, 2, '1', '1', '1', '1', '1'),
(1, 3, '1', '1', '1', '1', '1'),
(1, 4, '1', '1', '1', '1', '1'),
(1, 5, '1', '1', '1', '1', '1'),
(1, 6, '1', '1', '1', '1', '1'),
(2, 1, '1', '1', '1', '1', '1'),
(2, 2, '1', '1', '1', '1', '1'),
(2, 3, '1', '1', '1', '1', '1'),
(2, 4, '0', '1', '1', '1', '1'),
(2, 5, '0', '1', '1', '1', '1'),
(2, 6, '1', '1', '1', '1', '1'),
(3, 1, '1', '1', '1', '1', '1'),
(3, 2, '1', '1', '1', '1', '1'),
(3, 3, '1', '1', '1', '1', '1'),
(3, 4, '0', '1', '1', '1', '1'),
(3, 5, '0', '1', '1', '1', '1'),
(3, 6, '1', '1', '1', '1', '1'),
(4, 1, '1', '1', '1', '1', '1'),
(4, 2, '1', '1', '1', '1', '1'),
(4, 3, '1', '1', '1', '1', '1'),
(4, 4, '0', '1', '1', '1', '1'),
(4, 5, '1', '1', '1', '1', '1'),
(4, 6, '1', '1', '1', '1', '1'),
(5, 1, '1', '1', '1', '1', '1'),
(5, 2, '1', '1', '1', '1', '1'),
(5, 3, '1', '1', '1', '1', '1'),
(5, 4, '0', '1', '1', '1', '1'),
(5, 5, '1', '1', '1', '1', '1'),
(5, 6, '1', '1', '1', '1', '1'),
(6, 1, '1', '1', '1', '1', '1'),
(6, 2, '1', '1', '1', '1', '1'),
(6, 3, '1', '1', '1', '1', '1'),
(6, 4, '0', '1', '1', '1', '1'),
(6, 5, '1', '1', '1', '1', '1'),
(6, 6, '1', '1', '1', '1', '1'),
(7, 1, '1', '1', '1', '1', '1'),
(7, 2, '1', '1', '1', '1', '1'),
(7, 3, '1', '1', '1', '1', '1'),
(7, 4, '0', '1', '1', '1', '1'),
(7, 5, '0', '1', '1', '1', '1'),
(7, 6, '1', '1', '1', '1', '1'),
(8, 1, '1', '1', '1', '1', '1'),
(8, 2, '1', '1', '1', '1', '1'),
(8, 3, '1', '1', '1', '1', '1'),
(8, 4, '1', '1', '1', '1', '1'),
(8, 5, '1', '1', '1', '1', '1'),
(8, 6, '1', '1', '1', '1', '1'),
(10, 1, '1', '1', '1', '1', '1'),
(10, 2, '1', '1', '1', '1', '1'),
(10, 3, '1', '1', '1', '1', '1'),
(10, 4, '0', '1', '1', '1', '1'),
(10, 5, '1', '1', '1', '1', '1'),
(10, 6, '1', '1', '1', '1', '1'),
(11, 1, '1', '1', '1', '1', '1'),
(11, 2, '1', '1', '1', '1', '1'),
(11, 3, '1', '1', '1', '1', '1'),
(11, 4, '1', '1', '1', '1', '1'),
(11, 5, '1', '1', '1', '1', '1'),
(11, 6, '1', '1', '1', '1', '1'),
(12, 1, '1', '1', '1', '1', '1'),
(12, 2, '1', '1', '1', '1', '1'),
(12, 3, '1', '1', '1', '1', '1'),
(12, 4, '0', '1', '1', '1', '1'),
(12, 5, '0', '1', '1', '1', '1'),
(12, 6, '1', '1', '1', '1', '1'),
(13, 1, '1', '1', '1', '1', '1'),
(13, 2, '1', '1', '1', '1', '1'),
(13, 3, '1', '1', '1', '1', '1'),
(13, 4, '1', '1', '1', '1', '1'),
(13, 5, '1', '1', '1', '1', '1'),
(13, 6, '1', '1', '1', '1', '1'),
(14, 1, '1', '1', '1', '1', '1'),
(14, 2, '1', '1', '1', '1', '1'),
(14, 3, '1', '1', '1', '1', '1'),
(14, 4, '1', '1', '1', '1', '1'),
(14, 5, '1', '1', '1', '1', '1'),
(14, 6, '1', '1', '1', '1', '1'),
(15, 1, '1', '1', '1', '1', '1'),
(15, 2, '1', '1', '1', '1', '1'),
(15, 3, '1', '1', '1', '1', '1'),
(15, 4, '0', '1', '1', '1', '1'),
(15, 5, '1', '1', '1', '1', '1'),
(15, 6, '1', '1', '1', '1', '1'),
(16, 1, '1', '1', '1', '1', '1'),
(16, 2, '1', '1', '1', '1', '1'),
(16, 3, '1', '1', '1', '1', '1'),
(16, 4, '1', '1', '1', '1', '1'),
(16, 5, '0', '1', '1', '1', '1'),
(16, 6, '1', '1', '1', '1', '1'),
(17, 1, '1', '1', '1', '1', '1'),
(17, 2, '1', '1', '1', '1', '1'),
(17, 3, '1', '1', '1', '1', '1'),
(17, 4, '0', '1', '1', '1', '1'),
(17, 5, '1', '1', '1', '1', '1'),
(17, 6, '1', '1', '1', '1', '1'),
(18, 1, '1', '1', '1', '1', '1'),
(18, 2, '1', '1', '1', '1', '1'),
(18, 3, '1', '1', '1', '1', '1'),
(18, 4, '1', '1', '1', '1', '1'),
(18, 5, '1', '1', '1', '1', '1'),
(18, 6, '1', '1', '1', '1', '1'),
(19, 1, '1', '1', '1', '1', '1'),
(19, 2, '1', '1', '1', '1', '1'),
(19, 3, '1', '1', '1', '1', '1'),
(19, 4, '1', '1', '1', '1', '1'),
(19, 5, '1', '1', '1', '1', '1'),
(19, 6, '1', '1', '1', '1', '1'),
(24, 1, '1', '1', '1', '1', '1'),
(24, 2, '1', '1', '1', '1', '1'),
(24, 3, '1', '1', '1', '1', '1'),
(24, 4, '0', '1', '1', '1', '1'),
(24, 5, '1', '1', '1', '1', '1'),
(24, 6, '1', '1', '1', '1', '1'),
(25, 1, '1', '1', '1', '1', '1'),
(25, 2, '1', '1', '1', '1', '1'),
(25, 3, '1', '1', '1', '1', '1'),
(25, 4, '1', '1', '1', '1', '1'),
(25, 5, '1', '1', '1', '1', '1'),
(25, 6, '1', '1', '1', '1', '1'),
(26, 1, '1', '1', '1', '1', '1'),
(26, 2, '1', '1', '1', '1', '1'),
(26, 3, '1', '1', '1', '1', '1'),
(26, 4, '1', '1', '1', '1', '1'),
(26, 5, '1', '1', '1', '1', '1'),
(26, 6, '1', '1', '1', '1', '1'),
(27, 1, '1', '1', '1', '1', '1'),
(27, 2, '1', '1', '1', '1', '1'),
(27, 3, '1', '1', '1', '1', '1'),
(27, 4, '1', '1', '1', '1', '1'),
(27, 5, '1', '1', '1', '1', '1'),
(27, 6, '1', '1', '1', '1', '1'),
(28, 1, '1', '1', '1', '1', '1'),
(28, 2, '1', '1', '1', '1', '1'),
(28, 3, '1', '1', '1', '1', '1'),
(28, 4, '0', '1', '1', '1', '1'),
(28, 5, '1', '1', '1', '1', '1'),
(28, 6, '1', '1', '1', '1', '1'),
(29, 1, '1', '1', '1', '1', '1'),
(29, 2, '1', '1', '1', '1', '1'),
(29, 3, '1', '1', '1', '1', '1'),
(29, 4, '1', '1', '1', '1', '1'),
(29, 5, '1', '1', '1', '1', '1'),
(29, 6, '1', '1', '1', '1', '1'),
(30, 1, '1', '1', '1', '1', '1'),
(30, 2, '1', '1', '1', '1', '1'),
(30, 3, '1', '1', '1', '1', '1'),
(30, 4, '1', '1', '1', '1', '1'),
(30, 5, '1', '1', '1', '1', '1'),
(30, 6, '1', '1', '1', '1', '1'),
(31, 1, '1', '1', '1', '1', '1'),
(31, 2, '1', '1', '1', '1', '1'),
(31, 3, '1', '1', '1', '1', '1'),
(31, 4, '1', '1', '1', '1', '1'),
(31, 5, '1', '1', '1', '1', '1'),
(31, 6, '1', '1', '1', '1', '1'),
(32, 1, '1', '1', '1', '1', '1'),
(32, 2, '1', '1', '1', '1', '1'),
(32, 3, '1', '1', '1', '1', '1'),
(32, 4, '1', '1', '1', '1', '1'),
(32, 5, '1', '1', '1', '1', '1'),
(32, 6, '1', '1', '1', '1', '1'),
(33, 1, '1', '1', '1', '1', '1'),
(33, 2, '1', '1', '1', '1', '1'),
(33, 3, '1', '1', '1', '1', '1'),
(33, 4, '1', '1', '1', '1', '1'),
(33, 5, '1', '1', '1', '1', '1'),
(33, 6, '1', '1', '1', '1', '1'),
(34, 1, '1', '1', '1', '1', '1'),
(34, 2, '1', '1', '1', '1', '1'),
(34, 3, '1', '1', '1', '1', '1'),
(34, 4, '1', '1', '1', '1', '1'),
(34, 5, '1', '1', '1', '1', '1'),
(34, 6, '1', '1', '1', '1', '1'),
(35, 1, '1', '1', '1', '1', '1'),
(35, 2, '1', '1', '1', '1', '1'),
(35, 3, '1', '1', '1', '1', '1'),
(35, 4, '1', '1', '1', '1', '1'),
(35, 5, '1', '1', '1', '1', '1'),
(35, 6, '1', '1', '1', '1', '1'),
(36, 1, '1', '1', '1', '1', '1'),
(36, 2, '1', '1', '1', '1', '1'),
(36, 3, '1', '1', '1', '1', '1'),
(36, 4, '1', '1', '1', '1', '1'),
(36, 5, '1', '1', '1', '1', '1'),
(36, 6, '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moneda`
--

CREATE TABLE `moneda` (
  `id` bigint(11) NOT NULL,
  `nombre` varchar(100) NOT NULL DEFAULT '0',
  `codigo` varchar(50) DEFAULT NULL,
  `signo` char(5) DEFAULT NULL,
  `precio_cambio` decimal(10,2) DEFAULT '0.00' COMMENT 'precio del cambio a moneda local',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_atualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `moneda`
--

INSERT INTO `moneda` (`id`, `nombre`, `codigo`, `signo`, `precio_cambio`, `fecha_creacion`, `fecha_atualizacion`) VALUES
(1, 'Nuevo sol peruano', 'PEN', 'S/', '3.50', '2019-09-17 14:51:39', '2020-02-07 12:42:32'),
(2, 'Dolar estadounidense', 'USD', '$', '1.00', '2019-09-17 14:54:44', '2020-02-07 12:42:45'),
(3, 'Euro', 'EUR', '€', '1.25', '2019-09-17 14:55:55', '2020-02-07 12:42:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion`
--

CREATE TABLE `notificacion` (
  `id` bigint(100) NOT NULL,
  `iduser` bigint(100) NOT NULL DEFAULT '0',
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `descripcion` text COLLATE utf8_unicode_ci,
  `icono` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '2' COMMENT '0=inactivo, 1= leido, 2 = sin leer',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_servicio`
--

CREATE TABLE `orden_servicio` (
  `id` bigint(20) NOT NULL,
  `numorden` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idrepresentante` bigint(20) DEFAULT NULL,
  `tipomoneda` bigint(20) DEFAULT NULL,
  `is_saldo` tinyint(4) DEFAULT '1',
  `is_licencia` tinyint(4) DEFAULT '0',
  `saldo` decimal(10,2) DEFAULT '0.00',
  `precio` decimal(10,2) DEFAULT '0.00',
  `licEstudiantes` bigint(20) DEFAULT '0' COMMENT 'cantidad de licencias estuadiantes en total',
  `licDocentes` bigint(20) DEFAULT '0' COMMENT 'cantidad de licencias docentes en total',
  `contrato_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opcional_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ganancia` decimal(10,2) DEFAULT '70.00',
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `dias_prorroga` int(11) DEFAULT '7',
  `estado` tinyint(4) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `orden_servicio`
--

INSERT INTO `orden_servicio` (`id`, `numorden`, `idrepresentante`, `tipomoneda`, `is_saldo`, `is_licencia`, `saldo`, `precio`, `licEstudiantes`, `licDocentes`, `contrato_file`, `opcional_file`, `ganancia`, `fecha_inicio`, `fecha_fin`, `dias_prorroga`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 'OS0001R0001', 1, 1, 1, 0, '10000.00', '0.00', 0, 0, '', NULL, '70.00', '2020-02-06', '2021-02-06', 7, 1, '2020-02-07 10:59:36', '2020-02-07 10:59:36'),
(2, 'OS0001R0002', 2, 1, 1, 0, '0.00', '0.00', 0, 0, '', NULL, '70.00', '2020-02-04', '2021-02-04', 45, 1, '2020-02-21 11:05:08', '2020-02-21 11:05:08'),
(3, 'OS0001R0003', 3, 1, 1, 0, '0.00', '0.00', 0, 0, '', NULL, '70.00', '2020-02-28', '2021-02-28', 300, 1, '2020-02-28 14:19:37', '2020-02-28 14:19:37'),
(4, 'OS0001R0004', 4, 1, 1, 0, '0.00', '0.00', 0, 0, '', NULL, '70.00', '2020-02-04', '2021-02-04', 45, 1, '2020-02-21 11:05:08', '2020-02-21 11:05:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_servicio_curso`
--

CREATE TABLE `orden_servicio_curso` (
  `id` bigint(20) NOT NULL,
  `os_producto` bigint(20) DEFAULT NULL,
  `idproducto_curso` bigint(20) DEFAULT NULL,
  `lic_estudiantes` int(11) DEFAULT '0',
  `lic_docentes` int(11) DEFAULT '0',
  `mat_estudiantes` int(11) DEFAULT '0',
  `mat_docentes` int(11) DEFAULT '0',
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `orden_servicio_curso`
--

INSERT INTO `orden_servicio_curso` (`id`, `os_producto`, `idproducto_curso`, `lic_estudiantes`, `lic_docentes`, `mat_estudiantes`, `mat_docentes`, `estado`) VALUES
(1, 2, 24, 0, 0, 0, 0, 1),
(2, 2, 25, 0, 0, 0, 0, 1),
(3, 2, 26, 0, 0, 0, 0, 1),
(4, 2, 27, 0, 0, 0, 0, 1),
(5, 2, 28, 0, 0, 0, 0, 1),
(6, 2, 29, 0, 0, 0, 0, 1),
(7, 2, 2, 0, 0, 0, 0, 1),
(8, 2, 6, 0, 0, 0, 0, 1),
(9, 2, 22, 0, 0, 0, 0, 1),
(10, 2, 31, 0, 0, 0, 0, 1),
(11, 2, 61, 0, 0, 0, 0, 1),
(12, 2, 62, 0, 0, 0, 0, 1),
(13, 3, 18, 0, 0, 0, 0, 1),
(14, 3, 25, 0, 0, 0, 0, 1),
(15, 3, 26, 0, 0, 0, 0, 1),
(16, 3, 28, 0, 0, 0, 0, 1),
(17, 3, 47, 0, 0, 0, 0, 1),
(18, 3, 48, 0, 0, 0, 0, 1),
(19, 3, 49, 0, 0, 0, 0, 1),
(20, 3, 61, 0, 0, 0, 0, 1),
(21, 3, 63, 0, 0, 0, 0, 1),
(22, 3, 64, 0, 0, 0, 0, 1),
(23, 3, 65, 0, 0, 0, 0, 1),
(24, 2, 63, 0, 0, 0, 0, 1),
(25, 4, 67, 0, 0, 0, 0, 1),
(26, 5, 29, 0, 0, 0, 0, 1),
(27, 5, 61, 0, 0, 0, 0, 1),
(28, 6, 68, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_servicio_producto`
--

CREATE TABLE `orden_servicio_producto` (
  `id` bigint(20) NOT NULL,
  `orden_servicio` bigint(20) DEFAULT NULL,
  `idproducto` bigint(20) DEFAULT NULL,
  `precio_base_inscripcion` decimal(10,2) DEFAULT '100.00',
  `precio_base_curso` decimal(10,2) DEFAULT NULL,
  `precio_base_docente` decimal(10,2) DEFAULT '0.00',
  `precio_inscripcion` decimal(10,2) DEFAULT '100.00',
  `precio_curso` decimal(10,2) DEFAULT NULL,
  `precio_docente` decimal(10,2) DEFAULT '0.00',
  `precio_curso_libre` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ganancia` decimal(10,2) DEFAULT '70.00',
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `orden_servicio_producto`
--

INSERT INTO `orden_servicio_producto` (`id`, `orden_servicio`, `idproducto`, `precio_base_inscripcion`, `precio_base_curso`, `precio_base_docente`, `precio_inscripcion`, `precio_curso`, `precio_docente`, `precio_curso_libre`, `ganancia`, `estado`) VALUES
(1, 1, 2, '100.00', '70.00', '90.00', '100.00', '70.00', '90.00', '0.00', '70.00', 1),
(2, 2, 1, '100.00', '60.00', '0.00', '100.00', '60.00', '0.00', '120.00', '70.00', 1),
(3, 3, 1, '20.00', '5.00', '0.00', '20.00', '20.00', '0.00', '16.67', '70.00', 1),
(4, 2, 5, '100.00', '60.00', '0.00', '100.00', '60.00', '0.00', '120.00', '70.00', 1),
(5, 4, 1, '100.00', '60.00', '0.00', '100.00', '60.00', '0.00', '120.00', '70.00', 1),
(6, 4, 5, '100.00', '60.00', '0.00', '100.00', '60.00', '0.00', '120.00', '70.00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `id` bigint(20) NOT NULL,
  `numorden` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `representante` bigint(20) DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `fecha_hora_registro` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `registrado_por` bigint(20) DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` text COLLATE utf8_unicode_ci,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`id`, `numorden`, `representante`, `monto`, `fecha_hora_registro`, `registrado_por`, `file`, `comentario`, `estado`) VALUES
(1, 'P0001R0002', 2, '48.00', '2020-02-17 07:00:00', 3, '', 'Pago por mátriculas de alumnos (creación de usuarios)', 1),
(2, 'P0002R0002', 2, '24.00', '2020-02-17 07:00:00', 3, '', 'Pago por mátriculas de alumnos (creación de usuarios)', 1),
(3, 'P0003R0002', 2, '12.00', '2020-02-21 07:00:00', 3, '', 'Pago por mátriculas de alumnos (creación de usuarios)', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_detalle`
--

CREATE TABLE `pago_detalle` (
  `id` bigint(20) NOT NULL,
  `pago` bigint(20) DEFAULT NULL,
  `cuota` bigint(20) DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `monto` decimal(10,2) DEFAULT NULL,
  `fecha_pago` timestamp NULL DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `pago_detalle`
--

INSERT INTO `pago_detalle` (`id`, `pago`, `cuota`, `descripcion`, `monto`, `fecha_pago`, `estado`) VALUES
(1, 1, 4, 'Pago por mátriculas de alumnos (creación de usuarios)', '48.00', '2020-02-17 07:00:00', 1),
(2, 2, 5, 'Pago por mátriculas de alumnos (creación de usuarios)', '24.00', '2020-02-17 07:00:00', 1),
(3, 3, 6, 'Pago por mátriculas de alumnos (creación de usuarios)', '12.00', '2020-02-21 07:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(50) NOT NULL DEFAULT '0',
  `abreviacion` varchar(50) DEFAULT NULL,
  `idregion` bigint(20) DEFAULT NULL,
  `prefijo_telefono` varchar(10) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `nombre`, `abreviacion`, `idregion`, `prefijo_telefono`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 'Argentina', 'AR', NULL, '+54', '2019-09-13 14:32:55', '2020-02-08 07:32:05'),
(2, 'Bolivia', 'BO', NULL, '+591', '2019-09-13 14:38:46', '2020-02-08 07:32:05'),
(3, 'Brasil', 'BR', NULL, '+55', '2019-09-13 14:40:43', '2020-02-08 07:32:05'),
(4, 'Chile', 'CHI', NULL, '+56', '2019-09-13 14:41:14', '2020-02-08 07:32:05'),
(5, 'Colombia', 'CO', NULL, '+57', '2019-09-13 14:42:06', '2020-02-08 07:32:05'),
(6, 'Costa Rica', 'CSR', NULL, '+506', '2019-09-13 14:42:39', '2020-02-08 07:32:05'),
(7, ' Ecuador', 'ECU', NULL, '+593', '2019-09-13 14:43:12', '2020-02-08 07:32:05'),
(8, 'México', 'MEX', NULL, '+52', '2019-09-13 14:43:34', '2020-02-08 07:32:05'),
(9, ' Panamá', 'PAN', NULL, '+507', '2019-09-13 14:43:46', '2020-02-08 07:32:05'),
(10, ' Paraguay', 'PAR', NULL, '+595', '2019-09-13 14:43:57', '2020-02-08 07:32:05'),
(11, ' Perú', 'PE', NULL, '+51', '2019-09-13 14:44:13', '2020-02-08 07:32:05'),
(12, 'Uruguay', 'URU', NULL, '+598', '2019-09-13 14:44:25', '2020-02-08 07:32:05'),
(13, 'Venezuela', 'VE', NULL, '+58', '2019-09-13 14:44:41', '2020-02-08 07:32:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idpersona` bigint(20) NOT NULL,
  `tipodoc` bigint(20) NOT NULL DEFAULT '0' COMMENT 'tipo de documento (1= personal, 2= pasaporte)',
  `dni` bigint(20) NOT NULL DEFAULT '0' COMMENT 'numero del documento',
  `primer_ape` varchar(155) DEFAULT NULL COMMENT 'primer apellido',
  `segundo_ape` varchar(155) NOT NULL COMMENT 'segundo apellido',
  `nombre` varchar(255) NOT NULL,
  `sexo` char(1) NOT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `estado` tinyint(4) DEFAULT '1' COMMENT 'estado del usuario, activo e inactivo',
  `imagen` varchar(250) DEFAULT NULL,
  `idexterno` bigint(20) DEFAULT NULL COMMENT 'estudiantes de los productos',
  `fechanac` date DEFAULT NULL,
  `instruccion` text,
  `idrepresentante` bigint(20) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `tipodoc`, `dni`, `primer_ape`, `segundo_ape`, `nombre`, `sexo`, `telefono`, `image`, `email`, `usuario`, `clave`, `estado`, `imagen`, `idexterno`, `fechanac`, `instruccion`, `idrepresentante`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 1, 43831104, 'Super', 'Administrador', '', 'M', '', '1.jpg', 'abelchigno@gmail.com', 'admin', 'f3f1c26545d2424e5bbc7bf12a8f2dc6', 1, NULL, NULL, NULL, NULL, 1, '2019-09-12 10:26:42', '2020-02-21 17:46:05'),
(2, 2, 80239035, 'Seco', 'Meza', 'Emmy Antonio', 'M', '948404468', NULL, 'emmyseco@gmail.com', 'emmy', '91548f86c1032a1a7e9198da1340864e', 1, '', NULL, NULL, NULL, NULL, '2019-09-20 10:48:15', '2020-02-11 11:48:03'),
(3, 1, 43000000, 'EIGER', 'LIMA', 'ADMINISTRADOR', 'M', '159753456', NULL, 'admin@eiger.edu.pe', 'admineiger', '1cc74683694e0e0f32fa2fc9559b6234', 1, '', NULL, NULL, NULL, NULL, '2019-10-23 11:42:48', '2020-02-28 10:06:45'),
(4, 1, 12345678, 'Principal', 'Lima', 'Contador', 'M', '999888777', NULL, 'jpsvenegasc@gmail.com', 'CP12345678', '25d55ad283aa400af464c76d713c07ad', 1, NULL, NULL, NULL, NULL, 2, '2020-02-27 12:30:21', '2020-02-27 12:30:21'),
(5, 1, 43831001, 'Admin', 'Pedagogico', 'San Fabian', 'M', '', NULL, 'admin@sanfabian.com', 'adminsf', 'cfec1ae13ca61cceb6ab31fceac68440', 1, '', NULL, NULL, NULL, 1, '2020-02-28 10:08:18', '2020-02-28 10:08:18'),
(6, 1, 30000001, 'EDUKT', 'MAESTRO', 'ADMIN', 'M', NULL, NULL, 'admineduktmaestro@email.com', 'admineduktmaestro', 'fb60704f4ab935ff8024fda838a194bc', 1, NULL, NULL, '2020-03-31', NULL, NULL, '2020-03-31 19:23:17', '2020-03-31 19:23:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_externa`
--

CREATE TABLE `persona_externa` (
  `idpersona` bigint(20) NOT NULL,
  `tipodoc` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'tipo de documento (1= personal, 2= pasaporte)',
  `dni` bigint(20) NOT NULL DEFAULT '0' COMMENT 'numero del documento',
  `primer_ape` varchar(155) DEFAULT NULL COMMENT 'primer apellido',
  `segundo_ape` varchar(155) NOT NULL COMMENT 'segundo apellido',
  `nombre` varchar(255) NOT NULL,
  `sexo` char(1) NOT NULL,
  `fechanac` date DEFAULT NULL,
  `instruccion` text,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `idpais` bigint(20) DEFAULT '1',
  `iddepartamento` bigint(20) DEFAULT '1',
  `idprovincia` bigint(20) DEFAULT '1',
  `idciudad` bigint(20) DEFAULT '1',
  `estado` tinyint(4) DEFAULT '1' COMMENT 'estado del usuario, activo e inactivo',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `persona_externa`
--

INSERT INTO `persona_externa` (`idpersona`, `tipodoc`, `dni`, `primer_ape`, `segundo_ape`, `nombre`, `sexo`, `fechanac`, `instruccion`, `telefono`, `email`, `usuario`, `clave`, `idpais`, `iddepartamento`, `idprovincia`, `idciudad`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(2, 1, 20000001, 'Mamud', 'Mamud', 'Alex', 'M', NULL, NULL, NULL, 'amamud@hotmail.com', NULL, NULL, 1, 1, 1, 1, 1, '2020-02-12 16:35:39', '2020-02-12 16:35:39'),
(3, 1, 45278963, 'paterno1', 'materno1', 'nombre1', 'M', NULL, NULL, '', 'correo1@sks.com', 'np45278963', 'afb301a7deacc48b14162f46b62d3d1b', 1, 1, 1, 1, 1, '2020-02-15 22:12:24', '2020-02-15 22:12:24'),
(4, 1, 52147963, 'paterno2', 'materno2', 'nombre2', 'M', NULL, NULL, '', 'correo2@sks.com', 'np52147963', '14476733c02537fcf69fcd079ba04647', 1, 1, 1, 1, 1, '2020-02-15 22:12:24', '2020-02-15 22:12:24'),
(5, 1, 25469135, 'cajusol', 'asdsa', 'alvaro', 'M', NULL, NULL, '', '123@sks.com', 'ac25469135', 'd4b60640c932eb5689d39bd3975ba69b', 1, 1, 1, 1, 1, '2020-02-17 15:08:56', '2020-02-17 15:08:56'),
(6, 1, 15746854, 'paterno docente', 'materno docente', 'docente nombre', 'M', NULL, NULL, '', 'docente@sks.com', 'dp15746854', '4e8849d9e63449dd1a571280e4b1e7d7', 1, 1, 1, 1, 1, '2020-02-17 16:51:55', '2020-02-17 16:51:55'),
(7, 1, 42851936, 'Quiroz', 'Ojeda', 'Julio', '', NULL, NULL, NULL, 'ojeda@sks.com', 'JQ42851936', 'd4211b20f68702375f7006871d23f75e', 1, 1, 1, 1, 1, '2020-02-22 13:01:04', '2020-02-22 13:01:49'),
(8, 1, 12345678, 'Venegas', 'C', 'Juan Pablo', '', NULL, NULL, NULL, 'jpsvenegasc@hotmail.com', 'JV12345678', '12345678', 1, 1, 1, 1, 1, '2020-02-24 13:47:14', '2020-02-24 13:47:14'),
(9, 1, 9277083, 'Gomez', 'Hernandez', 'Juan Apolinar', 'M', NULL, NULL, '', 'juangomezh@hotmail.com', 'JG09277083', '66e8fe44ab893a700d9b620e730c884f', 1, 1, 1, 1, 1, '2020-02-24 15:56:01', '2020-02-24 15:56:01'),
(10, 1, 46874848, 'doc123', 'doc123', 'doc123', 'M', NULL, NULL, '', 'doc123@sks.com', 'dd46874848', '23fac545232de381df4e73863bf07241', 1, 1, 1, 1, 1, '2020-02-24 16:14:59', '2020-02-24 16:14:59'),
(11, 1, 23423432, 're', 're', 'tr', 'M', NULL, NULL, '', 'jh@sks.com', 'tr23423432', '726784b1a8c22c54054674ef3bdb4153', 1, 1, 1, 1, 1, '2020-02-24 16:16:40', '2020-02-24 16:16:40'),
(12, 1, 12345679, 'Eiger', 'Lima', 'Docente', 'M', NULL, NULL, '', 'juanpablo.venegas@eduktvirtual.com', 'DE12345679', 'defac44447b57f152d14f30cea7a73cb', 1, 1, 1, 1, 1, '2020-02-24 16:16:48', '2020-02-24 16:16:48'),
(13, 1, 9901904, 'Quintana', 'Olivas', 'Marco', '', NULL, NULL, NULL, 'quintana1974@hotmail.com', 'MQ09901904', '90e996eda5f739695c57892a0766b8af', 1, 1, 1, 1, 1, '2020-02-25 13:32:15', '2020-02-25 13:34:03'),
(14, 1, 64766747, 'Zapata', 'Gonzales', 'Julian', 'M', NULL, NULL, '', 'juanpablo.venegas@eduktvirtual.com', 'JZ64766747', '96f824be2a43e8fd3fce6e52991e6618', 1, 1, 1, 1, 1, '2020-02-25 14:03:14', '2020-02-25 14:03:14'),
(15, 1, 75467478, 'Mata', 'Perez', 'Carlos', 'M', NULL, NULL, '', 'juanpablo.venegas@eduktvirtual.com', 'CM75467478', '4eb0420c8b9f16cbaf44d0fa483b38d8', 1, 1, 1, 1, 1, '2020-02-25 14:10:16', '2020-02-25 14:10:16'),
(16, 1, 58468915, 'alu2_1', 'alu2_1', 'alu2_1', 'M', NULL, NULL, '', 'alu2_1@gmail.com', 'aa58468915', '20c4e5a6f5b482ec503ad1912abe6240', 1, 1, 1, 1, 1, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(17, 1, 58468916, 'alu2_3', 'alu2_3', 'alu2_3', 'M', NULL, NULL, '', 'alu2_3@gmail.com', 'aa58468916', '6c5901e3c3a3e7fac244e4ff01ec7838', 1, 1, 1, 1, 1, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(18, 1, 58468917, 'alu2_4', 'alu2_4', 'alu2_4', 'M', NULL, NULL, '', 'alu2_4@gmail.com', 'aa58468917', 'f233159f5a6777de25fc2505976785f2', 1, 1, 1, 1, 1, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(19, 1, 58468918, 'alu2_5', 'alu2_5', 'alu2_5', 'M', NULL, NULL, '', 'alu2_5@gmail.com', 'aa58468918', '19734d411f521fb8bb666b1b9e0d8ee0', 1, 1, 1, 1, 1, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(20, 1, 58468919, 'alu2_6', 'alu2_6', 'alu2_6', 'M', NULL, NULL, '', 'alu2_6@gmail.com', 'aa58468919', '692ffb43755c329e7a39e5e1439a8ac8', 1, 1, 1, 1, 1, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(21, 1, 58468920, 'alu2_7', 'alu2_7', 'alu2_7', 'M', NULL, NULL, '', 'alu2_7@gmail.com', 'aa58468920', '91f65eec88c1283f20eb38093adfef87', 1, 1, 1, 1, 1, '2020-02-25 16:30:24', '2020-02-25 16:30:24'),
(22, 1, 58468921, 'alu2_1', 'alu2_1', 'alu2_1', 'M', NULL, NULL, '', 'alu2_1@gmail.com', 'aa58468921', '10e98c79546611a5df8802fc5f702bbf', 1, 1, 1, 1, 1, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(23, 1, 58468922, 'alu2_3', 'alu2_3', 'alu2_3', 'M', NULL, NULL, '', 'alu2_3@gmail.com', 'aa58468922', 'd4361167722b5c662d7399559f1a15e7', 1, 1, 1, 1, 1, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(24, 1, 58468923, 'alu2_4', 'alu2_4', 'alu2_4', 'M', NULL, NULL, '', 'alu2_4@gmail.com', 'aa58468923', '633c37ece93877d45052a408a94fc14d', 1, 1, 1, 1, 1, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(25, 1, 58468924, 'alu2_5', 'alu2_5', 'alu2_5', 'M', NULL, NULL, '', 'alu2_5@gmail.com', 'aa58468924', 'ca9dbeac1c9603b96f02beeee3c6c565', 1, 1, 1, 1, 1, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(26, 1, 58468925, 'alu2_6', 'alu2_6', 'alu2_6', 'M', NULL, NULL, '', 'alu2_6@gmail.com', 'aa58468925', '6b476ab45afa463265b36deacc7d4086', 1, 1, 1, 1, 1, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(27, 1, 58468926, 'alu2_7', 'alu2_7', 'alu2_7', 'M', NULL, NULL, '', 'alu2_7@gmail.com', 'aa58468926', '461fcef43061853877e7d479930c5a28', 1, 1, 1, 1, 1, '2020-02-26 07:03:24', '2020-02-26 07:03:24'),
(28, 1, 56465464, 'kjasdjask', 'hdkajsd', 'hjdf', 'M', '2019-07-16', 'algo', '', 'aghsdf@sks.com', 'hk56465464', 'f21d52b5fd587630b39954156cdd10f3', 1, 1, 1, 1, 1, '2020-02-26 16:29:05', '2020-02-26 16:29:05'),
(29, 1, 56445668, 'daksdh', 'daksjdh', 'askdhaskd', 'M', '2014-02-04', 'asgdhkujasvg', '', 'ghasdhjas@sks.com', 'ad56445668', '0c33e0ecff961a421cbdba379e9e9a28', 1, 1, 1, 1, 1, '2020-02-26 16:32:37', '2020-02-26 16:32:37'),
(30, 1, 45646854, 'ni', 'to', 'ju', 'M', '2020-02-06', 'fahuduoaskjdoa', NULL, 'asd@sks.com', 'jn45646854', '5a137cd1192514d125ddcd794cffb030', 1, 1, 1, 1, 1, '2020-02-26 17:27:13', '2020-02-26 17:31:56'),
(31, 1, 56456456, 'kjhkjsdhasjkdh', 'kjhasdkjhasjkd', 'hjkjhkjkj', 'M', '2020-02-04', 'adsgasdha', '', 'kljsdsa@aka.com', 'hk56456456', '1c8695e194b43dc277a6a2cb720bf0d0', 1, 1, 1, 1, 1, '2020-02-26 18:06:14', '2020-02-26 18:06:14'),
(32, 1, 43800000, 'abel', 'abelito', 'demo ', 'M', '2020-02-27', 'culaui', NULL, 'abel@hotmail.com', 'da43800000', '6422257149da88fc41f76988315a74d7', 1, 1, 1, 1, 1, '2020-02-27 06:40:45', '2020-02-27 06:41:27'),
(33, 1, 12121201, 'Carrasquero', 'prueba', 'Andres prueba', 'M', '1996-10-10', 'Ing de sistemas', '', 'emmyseco@gmail.com', 'AC12121201', '98d1c0fd089f5ddc41cd4007e6561c2f', 1, 1, 1, 1, 1, '2020-02-28 14:54:22', '2020-02-28 14:54:22'),
(34, 1, 9901904, 'Quintana', 'Olivas', 'Marco Antonio', 'M', '1974-11-17', 'superior completo', NULL, 'quintana1974@hotmail.com', 'MQ09901904', '09901904', 1, 1, 1, 1, 1, '2020-02-28 15:44:04', '2020-02-28 15:44:04'),
(35, 1, 23232366, '', '', '', 'M', '0000-00-00', '', NULL, '', '23232366', '23232366', 1, 1, 1, 1, 1, '2020-03-02 13:39:33', '2020-03-02 13:39:33'),
(36, 1, 11111111, 'Perez', 'Perez', 'Luis', 'M', '1980-01-01', 'Universitario', '', 'bancesluis@hotmail.com', 'LP11111111', '1bbd886460827015e5d605ed44252251', 1, 1, 1, 1, 1, '2020-03-04 15:24:41', '2020-03-04 15:24:41'),
(37, 1, 22222222, 'Diaz', 'Velasquez', 'Degeneer', 'F', '1998-12-24', 'Universitaria', '', 'ddiaz@eduktvirtual.com', 'DD22222222', 'bae5e3208a3c700e3db642b6631e95b9', 1, 1, 1, 1, 1, '2020-03-04 15:52:51', '2020-03-04 15:52:51'),
(38, 1, 84964561, '156kjnknjk', 'fghvgjhbjj', '1651jnknj', 'M', '2020-03-13', 'olp', NULL, 'lnmlk@algo.com', '1184964561', '84964561', 1, 1, 1, 1, 1, '2020-03-04 16:34:29', '2020-03-04 16:34:29'),
(39, 1, 54649849, 'docente', 'probando', 'nuevo', 'M', '2019-11-11', '', '', 'docentenuevo@sks.com', 'nd54649849', '889305eba109e3483826357267214615', 1, 1, 1, 1, 1, '2020-03-05 08:14:05', '2020-03-05 08:14:05'),
(40, 1, 87654321, 'Bocanegra ', 'Aguilar', 'Luz María', 'F', '1978-03-01', 'Universitario', '', 'luzma1777@hotmail.com', 'LB87654321', '5e8667a439c68f5145dd2fcbecf02209', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(41, 1, 87654322, 'Quiroz', 'De Rivera', 'Iris ', 'F', '1978-04-01', 'Universitario', '', 'irisquirozderivera@gmail.com', 'IQ87654322', '47e1201326892d22f76921d17e049ef5', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(42, 1, 87654323, 'Gutierrez', 'Zambrano', 'Rosalia', 'F', '1979-05-02', 'Universitario', '', 'rosaliasmartschool@gmail.com', 'RG87654323', 'ee42a533836108c815950704de93c4d0', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(43, 1, 87654324, 'Gutierrez', 'Yactayo', 'Melisa', 'F', '1998-08-03', 'Universitario', '', 'kameguya.0694@gmail.com', 'MG87654324', '0698a38810bdc2f12e8596fe250fcdaa', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(44, 1, 87654325, 'Guzmán ', 'Farías', 'Lisbeth Del C', 'F', '1976-06-03', 'Universitario', '', 'lisbeth268@hotmail.com', 'LG87654325', 'db6315e85e7109f27658d99a113b2f22', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(45, 1, 87654326, 'Mesones', 'Hornaza', 'Ana María', 'F', '1975-05-05', 'Universitario', '', 'anamariamesones@hotmail.com', 'AM87654326', 'f58bd0f8143f508923c3dc4b0390a0fa', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(46, 1, 87654327, 'Duque', 'Gutierrez', 'Laura Mercedes', 'F', '1977-07-06', 'Universitario', '', 'laura_duque14@hotmail.com', 'LD87654327', '7c0c5c5eb8b0c188d8f65ed8118e157f', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(47, 1, 87654328, 'Chacón', 'Chacón', 'Gabriel ', 'M', '1979-09-04', 'Universitario', '', 'gabrielchacon702@gmail.com', 'GC87654328', '9a6320e78e7625baf8269177ed7bb3f8', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(48, 1, 87654329, 'Carrasco', 'Villavicencio', 'Carmen', 'F', '1999-06-05', 'Universitario', '', 'carmenhjk@hotmail.com', 'CC87654329', '384e7e7531ef207788aa43b27a511c00', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(49, 1, 87654330, 'Saturno', 'Rivera', 'Briner', 'M', '1980-10-07', 'Universitario', '', 'pegaso1976@hotmail.com', 'BS87654330', '56cd2d71a489baba8b521400e34474c4', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(50, 1, 87654331, 'Martínez ', 'Martínez', 'Norah', 'F', '1996-11-08', 'Universitario', '', 'noraeventos1@gmail.com', 'NM87654331', 'dba43f95723cb18ce9db813e248bb5c3', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(51, 1, 87654332, 'Portocarrero ', 'Fuentes', 'Keylla ', 'F', '1981-12-10', 'Universitario', '', 'kportocarrerofuentes@gmail.com', 'KP87654332', '3eb9bbddbdd3e4db29ad5a79230090fd', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(52, 1, 87654333, 'Llerena', 'Llerena', 'Flor ', 'F', '1995-12-05', 'Universitario', '', 'angelrossmarry@hotmail.com', 'FL87654333', '0f04762e9fa94550e05a4a77e86c30fa', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(53, 1, 87654334, 'Peña', 'Vidalon', 'Paul ', 'M', '1983-03-04', 'Universitario', '', 'paulmax71@hotmail.com', 'PP87654334', '26e3899d2cbdc3cc2528a0842932ef2d', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(54, 1, 87654335, 'Mori', 'Guerra', 'Carol ', 'F', '1979-05-10', 'Universitario', '', 'cmorig2912@gmail.com', 'CM87654335', '1e372ab0badab00b63f3d81f704182cf', 1, 1, 1, 1, 1, '2020-03-05 10:16:59', '2020-03-05 10:16:59'),
(55, 1, 9000001, 'F', 'L', 'S', 'M', '2007-01-01', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000001', '450fd1292a9b0b1769c25c2d1b469b65', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(56, 1, 9000002, 'F', 'L', 'S', 'M', '2007-01-02', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000002', '086fe2431073c6024095b7ed125033d0', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(57, 1, 9000003, 'F', 'L', 'S', 'M', '2007-01-03', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000003', 'e0680a4fb475fc055d4bcafedafd00f7', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(58, 1, 9000004, 'F', 'L', 'S', 'M', '2007-01-04', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000004', '869b76d2b98221a2a2297f5433a34e2b', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(59, 1, 9000005, 'F', 'L', 'S', 'M', '2007-01-05', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000005', 'b4ec5ace9803fb519d4a3f166eb7a9b9', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(60, 1, 9000006, 'F', 'L', 'S', 'M', '2007-01-06', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000006', '1e6ed641edbb7c20d8b19c5ce244c0a6', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(61, 1, 9000007, 'F', 'L', 'S', 'M', '2007-01-07', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000007', 'd5ade21f6ac322b6fb8175f7d0b9dfcd', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(62, 1, 9000008, 'F', 'L', 'S', 'M', '2007-01-08', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000008', 'f7158477ccc2d6c97123f0f374392cc5', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(63, 1, 9000009, 'F', 'L', 'S', 'M', '2007-01-09', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000009', '21d2c969dbd649a8099c9f40de26d2f9', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(64, 1, 9000010, 'F', 'L', 'S', 'M', '2007-01-10', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF9000010', '266c18f0219fc630b4a2169880016bd8', 1, 1, 1, 1, 1, '2020-03-06 07:57:52', '2020-03-06 07:57:52'),
(65, 1, 10000031, 'F', 'L', 'S', 'M', '2008-04-01', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000031', '634624302112d8404a32cc4e2ebdc69e', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(66, 1, 10000032, 'F', 'L', 'S', 'M', '2008-04-02', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000032', '78495fd47847084870e50d48bdf9a805', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(67, 1, 10000033, 'F', 'L', 'S', 'M', '2008-04-03', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000033', 'b94fd5df84f69bfea86f7cf26549a6eb', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(68, 1, 10000034, 'F', 'L', 'S', 'M', '2008-04-04', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000034', '30ca3346de968dd2aa2419adcc3597cd', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(69, 1, 10000035, 'F', 'L', 'S', 'M', '2008-04-05', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000035', '1954581e8547c14a1a00060bac594baf', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(70, 1, 10000036, 'F', 'L', 'S', 'M', '2008-04-06', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000036', '9cf66eb85be54d0aa174306875c62eb6', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(71, 1, 10000037, 'F', 'L', 'S', 'M', '2008-04-07', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000037', '3a99499e25e50194734bf0be0bf02099', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(72, 1, 10000038, 'F', 'L', 'S', 'M', '2008-04-08', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000038', 'e873a4007c7bc964d157287774e937e4', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(73, 1, 10000039, 'F', 'L', 'S', 'M', '2008-04-09', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000039', '15ded7c99c26348474c239043cfcbf12', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(74, 1, 10000040, 'F', 'L', 'S', 'M', '2008-04-10', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF10000040', '9e440a7ffa9a30a994471379ed086dc0', 1, 1, 1, 1, 1, '2020-03-06 08:01:16', '2020-03-06 08:01:16'),
(75, 1, 11000001, 'F', 'L', 'S', 'M', '2007-05-11', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000001', '52cd1528d229668e5750785babf7f0b5', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(76, 1, 11000002, 'F', 'L', 'S', 'M', '2007-05-12', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000002', '3880d65a03a4110d0212efce6eed7bc3', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(77, 1, 11000003, 'F', 'L', 'S', 'M', '2007-05-13', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000003', '41be15d0ff0447151addd7d6303fb082', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(78, 1, 11000004, 'F', 'L', 'S', 'M', '2007-05-14', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000004', 'be30a6f5f06662c1369573155bede6ad', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(79, 1, 11000005, 'F', 'L', 'S', 'M', '2007-05-15', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000005', '3335c740729cb493018d8aa36e539244', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(80, 1, 11000006, 'F', 'L', 'S', 'M', '2007-05-16', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000006', '62e4022db94fd5639501c41bae5d078e', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(81, 1, 11000007, 'F', 'L', 'S', 'M', '2007-05-17', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000007', '8c0e39bc3cd9c232cc6411d70e7dc5a8', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(82, 1, 11000008, 'F', 'L', 'S', 'M', '2007-05-18', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000008', '6dd6105e74926a991e1aada443f6b2af', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(83, 1, 11000009, 'F', 'L', 'S', 'M', '2007-05-19', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000009', 'bdc6827dbcf3f5ad9d526bc7b467f99d', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(84, 1, 11000010, 'F', 'L', 'S', 'M', '2007-05-20', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF11000010', '2b2f44e27637395b1febfc64bde011de', 1, 1, 1, 1, 1, '2020-03-06 08:03:02', '2020-03-06 08:03:02'),
(85, 1, 12000001, 'F ', 'L', 'S ', 'M', '2005-06-01', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000001', 'aaeaa13558947e6aa075c560091f6f9f', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(86, 1, 12000002, 'F ', 'L', 'S ', 'M', '2005-06-02', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000002', '11aefec6ca1b20e38d0930f4beff1d20', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(87, 1, 12000003, 'F ', 'L', 'S ', 'M', '2005-06-03', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000003', 'e316a787ac81e590e3883361a80a796a', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(88, 1, 12000004, 'F ', 'L', 'S ', 'M', '2005-06-04', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000004', '55d0cecb80d85d0ce9f57104fa78d999', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(89, 1, 12000005, 'F ', 'L', 'S ', 'M', '2005-06-05', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000005', '78ab3bc8e63ad86ebd9d332c60acec9f', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(90, 1, 12000006, 'F ', 'L', 'S ', 'M', '2005-06-06', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000006', '11cea16196437d288a9218ee7035e968', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(91, 1, 12000007, 'F ', 'L', 'S ', 'M', '2005-06-07', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000007', '11255f7a9c0fc6cd928483343f07b0fa', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(92, 1, 12000008, 'F ', 'L', 'S ', 'M', '2005-06-08', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000008', '11336748b0871851256dcffb0f77211f', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(93, 1, 12000009, 'F ', 'L', 'S ', 'M', '2005-06-09', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000009', 'c10e8ba659592b73c699d09c8fa4f7b5', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(94, 1, 12000010, 'F ', 'L', 'S ', 'M', '2005-06-10', 'Estudiante', '', 'info@eduktvirtual.com', 'SF12000010', 'd0f17833426eac3809c6646c093380ab', 1, 1, 1, 1, 1, '2020-03-06 08:04:24', '2020-03-06 08:04:24'),
(95, 1, 13000001, 'F ', 'L', 'S ', 'M', '2004-07-01', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000001', 'b7df90e35eb4ea640f1c704777e9e26f', 1, 1, 1, 1, 1, '2020-03-06 08:06:00', '2020-03-06 08:06:00'),
(96, 1, 13000002, 'F ', 'L', 'S ', 'M', '2004-07-02', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000002', '4fa6f9d59d66e82f719b270c142db695', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(97, 1, 13000003, 'F ', 'L', 'S ', 'M', '2004-07-03', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000003', 'fedf6b603531a8958e5654cd0e53b60c', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(98, 1, 13000004, 'F ', 'L', 'S ', 'M', '2004-07-04', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000004', '9cb52ab0d077f907959029bff2e933cf', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(99, 1, 13000005, 'F ', 'L', 'S ', 'M', '2004-07-05', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000005', 'c759dfb08b5e1d1197984af8e010f633', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(100, 1, 13000006, 'F ', 'L', 'S ', 'M', '2004-07-06', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000006', 'f23b30e7550771f6d076a955f374d91f', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(101, 1, 13000007, 'F ', 'L', 'S ', 'M', '2004-07-07', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000007', 'b9b17412eeb3b6b766ce083807448419', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(102, 1, 13000008, 'F ', 'L', 'S ', 'M', '2004-07-08', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000008', '6d21fbd67fd5f2467cb7945dff300c86', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(103, 1, 13000009, 'F ', 'L', 'S ', 'M', '2004-07-09', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000009', '0121bc9c1eeac5958f0bee30d536a368', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(104, 1, 13000010, 'F ', 'L', 'S ', 'M', '2004-07-10', 'Estudiante', '', 'info@eduktvirtual.com', 'SF13000010', '3b6dda25cf1b73beccbea6e0cf85d816', 1, 1, 1, 1, 1, '2020-03-06 08:06:01', '2020-03-06 08:06:01'),
(105, 1, 70000001, 'F', 'L', 'S', 'M', '2009-01-11', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000001', '2a1171e59234655319c85d3395593990', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(106, 1, 70000002, 'F', 'L', 'S', 'M', '2009-01-12', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000002', '9b26639c56dfac446a4ad06df56620cf', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(107, 1, 70000003, 'F', 'L', 'S', 'M', '2009-01-13', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000003', '3298ad3ef8ffcd6ebe4d4de940a67c68', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(108, 1, 70000004, 'F', 'L', 'S', 'M', '2009-01-14', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000004', '1b3aadbc45bccfe3db60e2e6d08a85f6', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(109, 1, 70000005, 'F', 'L', 'S', 'M', '2009-01-15', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000005', '93da19438870d34eb942d536042d2fc6', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(110, 1, 70000006, 'F', 'L', 'S', 'M', '2009-01-16', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000006', 'c0ec13ca5ed514cbeda32c2624b2632a', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(111, 1, 70000007, 'F', 'L', 'S', 'M', '2009-01-17', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000007', 'b6323c38afbcbd49d13c5ea5ef356a81', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(112, 1, 70000008, 'F', 'L', 'S', 'M', '2009-01-18', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000008', 'b6626bf61cd55f3d227252a9cb049a13', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(113, 1, 70000009, 'F', 'L', 'S', 'M', '2009-01-19', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000009', 'a59cdfa38ea883710ad5d1f313d973f5', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(114, 1, 70000010, 'F', 'L', 'S', 'M', '2009-01-20', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF70000010', '6f8096af4069f564fa72ebded0957e64', 1, 1, 1, 1, 1, '2020-03-06 08:07:42', '2020-03-06 08:07:42'),
(115, 1, 80000001, 'F', 'L', 'S', 'M', '2008-01-01', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000001', 'b9e9173c656c0832472360f793a9ca82', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(116, 1, 80000002, 'F', 'L', 'S', 'M', '2008-01-02', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000002', '358fa7dd014124bc90241e094a6e4bf4', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(117, 1, 80000003, 'F', 'L', 'S', 'M', '2008-01-03', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000003', 'a54358e7dd731a69164614d08cd989d9', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(118, 1, 80000004, 'F', 'L', 'S', 'M', '2008-01-04', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000004', 'c4f4652a87563258354ac5cc9f373a0f', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(119, 1, 80000005, 'F', 'L', 'S', 'M', '2008-01-05', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000005', 'd8e398117e6c0ae0497b9e52a01e526c', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(120, 1, 80000006, 'F', 'L', 'S', 'M', '2008-01-06', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000006', '11931938a528b524acb3e97d549c309f', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(121, 1, 80000007, 'F', 'L', 'S', 'M', '2008-01-07', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000007', 'cd3416b483b903be247743a3f60d0ecb', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(122, 1, 80000008, 'F', 'L', 'S', 'M', '2008-01-08', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000008', '765d7849547679a5e72da8d0b2a7ee88', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(123, 1, 80000009, 'F', 'L', 'S', 'M', '2008-01-09', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000009', 'bff49b13450043e41ef5e8945a6f41b1', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(124, 1, 80000010, 'F', 'L', 'S', 'M', '2008-01-10', 'Estudiante ', '', 'info@eduktvirtual.com', 'SF80000010', '6f06018e41cd90ba3dff40e939c0201a', 1, 1, 1, 1, 1, '2020-03-06 08:08:55', '2020-03-06 08:08:55'),
(125, 1, 25010048, 'seco', 'meza', 'emmy', 'M', '1996-03-10', 'Ing', '', 'emmyseco@gmail.com', 'es25010048', '3c505074919cae26141bf8b945f193b6', 1, 1, 1, 1, 1, '2020-03-09 08:33:22', '2020-03-09 08:33:22'),
(126, 1, 7, 'Gutierrez', 'Docente', 'Renzo', 'M', '1978-02-13', 'Universitario', '', 'rjgz2012@gmail.com', 'RG00000007', '400e70d06ac952d601f3c690f005628e', 1, 1, 1, 1, 1, '2020-03-13 11:56:05', '2020-03-13 11:56:05'),
(127, 1, 10000001, 'prueba2', 'prueba2', 'prueba', 'M', '1996-10-03', 'desarrollador', '', 'prueba2@sks.com', '000000000125', 'c75dffabc63d778e5f1c6a003db4c84e', 1, 1, 1, 1, 1, '2020-03-14 10:03:49', '2020-03-14 10:03:49'),
(128, 1, 54654654, 'nuevodas', 'nuevoasd', 'nuevoads', 'M', '1995-10-10', '', '', 'asd@sks.com', 'DOC000000001', 'a12af6e4d282d706cfd3ac65687621b5', 1, 1, 1, 1, 1, '2020-03-18 10:04:57', '2020-03-18 10:04:57'),
(129, 1, 65467684, 'docente de prueba', 'docente de prueba', 'docente de prueba', 'M', '1990-10-10', '', '', 'doc@sks.com', 'DOC000000002', 'b259ccfba155e2fd3c8e37c85b9e4973', 1, 1, 1, 1, 1, '2020-03-18 15:34:24', '2020-03-18 15:34:24'),
(130, 1, 43000013, 'Eiger', 'Alumno', 'Alumno', 'M', '1990-10-10', '', NULL, 'alumnoeiger@sks.com', 'USU000000001', '4c62c3c88d5de9416633a95694d86366', 1, 1, 1, 1, 1, '2020-03-18 18:22:33', '2020-03-18 18:22:33'),
(131, 1, 54654644, 'ALUMNO', 'EIGER', 'EIGER', 'M', '1995-10-10', '', NULL, 'EIGER@SKS.COM', 'alumnoeiger', '1cc74683694e0e0f32fa2fc9559b6234', 1, 1, 1, 1, 1, '2020-03-18 19:36:17', '2020-03-18 19:37:41'),
(132, 1, 30000002, 'DOCENTE', 'EDUKT', 'MAESTRO', 'M', '2020-03-10', '', '', 'docenteeduktmaestro@gmail.com', 'docenteeduktmaestro', 'b7321c4c2622b8379a8f3c8bbc9cb96f', 1, 1, 1, 1, 1, '2020-03-31 21:34:04', '2020-03-31 21:40:28'),
(133, 1, 64646545, 'nuevo', 'nuevo', 'Angie', 'M', '1990-10-10', '', '', 'anha16@hotmail.com', 'DOC000000004', '3e7f806c4087d18d950cd68d74395934', 1, 1, 1, 1, 1, '2020-04-01 09:09:44', '2020-04-01 09:09:44'),
(134, 1, 45645646, 'apellido pat', 'apellido mat', 'Carolina', 'F', '1990-10-10', '', '', 'carolinagonzalezp212@hotmail.com', 'DOC000000005', '3a5a4989a3b163bd9c5b92a88c0b9a7d', 1, 1, 1, 1, 1, '2020-04-01 09:09:44', '2020-04-01 09:09:44'),
(135, 1, 45456465, 'apellido', 'materno', 'Miluska', 'F', '1990-10-10', '', '', 'mngomez_sy@outlook.com', 'DOC000000006', '0cd2fe5dc01ce7363cfc50a3c39dab8c', 1, 1, 1, 1, 1, '2020-04-01 09:09:45', '2020-04-01 09:09:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_rol`
--

CREATE TABLE `persona_rol` (
  `id` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL DEFAULT '0' COMMENT 'id del rol especifico ',
  `idpersona` bigint(20) NOT NULL DEFAULT '0' COMMENT 'id del personal especifico',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `persona_rol`
--

INSERT INTO `persona_rol` (`id`, `idrol`, `idpersona`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 1, 1, '2019-09-12 10:26:42', '2020-01-09 16:00:26'),
(2, 2, 2, '2019-09-20 10:48:15', '2020-01-09 06:31:02'),
(3, 4, 3, '2019-10-23 11:42:48', '2020-02-11 11:42:39'),
(4, 5, 4, '2020-02-27 12:30:21', '2020-02-27 12:30:21'),
(5, 4, 5, '2020-02-28 14:36:17', '2020-02-28 14:36:17'),
(6, 4, 6, '2020-03-31 19:25:07', '2020-03-31 19:25:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `politica_depago`
--

CREATE TABLE `politica_depago` (
  `id` bigint(20) NOT NULL,
  `idorden_servicio` bigint(20) DEFAULT NULL,
  `tipopago` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=contado, 2=credito',
  `cuotas` int(11) NOT NULL DEFAULT '0' COMMENT 'cantidad de cuotas',
  `tipoduracion` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=quincenal, 2= mensual,3 = bimestral,4=trimestre,5=cuatrimestre,6=semestre,7=anual',
  `duracion` decimal(10,1) NOT NULL DEFAULT '0.0' COMMENT 'cantidad de meses ',
  `dia_corte` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Indica el dia de cada cobro de los plazos',
  `estado` tinyint(4) DEFAULT '1',
  `fecha_cobro` date DEFAULT NULL COMMENT 'fecha opcional, si se elige al contado',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `politica_depago`
--

INSERT INTO `politica_depago` (`id`, `idorden_servicio`, `tipopago`, `cuotas`, `tipoduracion`, `duracion`, `dia_corte`, `estado`, `fecha_cobro`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 1, 1, 1, 0, '0.0', 0, 1, '2020-02-07', '2020-02-07 11:05:24', '2020-02-07 11:05:24'),
(2, 2, 1, 1, 0, '0.0', 0, 1, '2020-02-07', '2020-02-11 11:45:18', '2020-02-11 11:45:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio_promocion`
--

CREATE TABLE `precio_promocion` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idempresa` bigint(20) DEFAULT NULL,
  `tipo_matricula` tinyint(4) DEFAULT '1',
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `precio_promocion`
--

INSERT INTO `precio_promocion` (`id`, `nombre`, `idempresa`, `tipo_matricula`, `estado`) VALUES
(1, 'Normal', 2, 2, 1),
(2, 'Normal', 2, 1, 1),
(3, 'Total', 3, 2, 1),
(4, 'Total', 3, 1, 1),
(5, 'Pago Total', 4, 2, 1),
(6, 'Promocion 2', 2, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio_promocion_detalle`
--

CREATE TABLE `precio_promocion_detalle` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentaje` decimal(10,2) DEFAULT NULL,
  `is_obligatorio` tinyint(4) DEFAULT '1',
  `is_recurso` tinyint(4) DEFAULT '0',
  `idprecio_promocion` bigint(20) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `precio_promocion_detalle`
--

INSERT INTO `precio_promocion_detalle` (`id`, `nombre`, `porcentaje`, `is_obligatorio`, `is_recurso`, `idprecio_promocion`, `estado`) VALUES
(1, 'Acceso al Curso', '70.00', 1, 0, 1, 1),
(2, 'Material', '30.00', 0, 1, 1, 1),
(3, 'Material', '100.00', 0, 1, 2, 1),
(4, 'Acceso + Recursos', '100.00', 1, 1, 3, 1),
(5, 'Acceso al Curso', '100.00', 0, 1, 4, 1),
(6, 'Pago Total', '100.00', 1, 1, 5, 1),
(7, 'Acceso al Curso', '50.00', 1, 0, 6, 1),
(8, 'Material', '20.00', 0, 1, 6, 1),
(9, 'Libros de trabrajo', '30.00', 0, 1, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT '0',
  `descripcion` text,
  `logo` varchar(255) DEFAULT NULL,
  `idproducto_url` bigint(20) DEFAULT '1',
  `idioma` varchar(5) DEFAULT 'ES',
  `precio_dolar` decimal(10,2) DEFAULT '40.00' COMMENT 'precio por curso',
  `precio_docente` decimal(10,2) DEFAULT '40.00' COMMENT 'precio por docente',
  `estado` tinyint(4) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipodecurso` int(11) DEFAULT '1' COMMENT '1= son los cursos de la edumax, 2= cursos del cliente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `logo`, `idproducto_url`, `idioma`, `precio_dolar`, `precio_docente`, `estado`, `fecha_creacion`, `fecha_actualizacion`, `tipodecurso`) VALUES
(1, 'EduktVirtual', NULL, 'static/media/proyectos/eduktlogo.png', 1, 'ES', '12.00', '20.00', 1, '2019-09-18 17:05:19', '2020-02-16 08:48:58', 1),
(2, 'SmartEnglish Blended', NULL, 'static/media/proyectos/logo_smartenglish.png', 2, 'EN', '60.00', '60.00', 1, '2019-09-18 17:05:45', '2020-02-16 08:49:04', 1),
(3, 'SmartEnglish Academy', NULL, 'static/media/proyectos/logo_smartenglish.png', 2, 'EN', '60.00', '60.00', 1, '2019-09-18 17:06:06', '2020-02-16 08:49:08', 1),
(4, 'SmartEnglish Licencias', NULL, 'static/media/proyectos/logo_smartenglish.png', 2, 'EN', '60.00', '60.00', 1, '2019-09-18 17:06:06', '2020-02-16 08:49:11', 1),
(5, 'EduktMaestro', NULL, 'static/media/proyectos/eduktlogo.png', 1, 'ES', '12.00', '20.00', 1, '2020-03-24 16:56:00', '2020-03-28 07:38:51', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_cursos`
--

CREATE TABLE `producto_cursos` (
  `id` bigint(11) NOT NULL,
  `idcurso` bigint(11) DEFAULT NULL COMMENT 'id del curso en SKS',
  `idproducto` bigint(11) DEFAULT NULL COMMENT 'id del producto',
  `estado` tinyint(11) DEFAULT '1' COMMENT 'estado del registro 1activo, 0 innactivo',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `producto_cursos`
--

INSERT INTO `producto_cursos` (`id`, `idcurso`, `idproducto`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 1, 1, 1, '2020-02-09 09:58:32', '2020-02-09 09:58:32'),
(2, 2, 1, 1, '2020-02-09 10:04:28', '2020-02-09 10:04:28'),
(3, 3, 1, 1, '2020-02-09 10:04:28', '2020-02-09 10:04:28'),
(4, 4, 1, 1, '2020-02-09 10:04:28', '2020-02-09 10:04:28'),
(5, 5, 1, 1, '2020-02-09 10:04:28', '2020-02-09 10:04:28'),
(6, 6, 1, 1, '2020-02-09 10:04:28', '2020-02-09 10:04:28'),
(7, 7, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(8, 8, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(9, 9, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(10, 10, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(11, 11, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(12, 12, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(13, 13, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(14, 14, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(15, 15, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(16, 16, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(17, 17, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(18, 18, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(19, 19, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(20, 20, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(21, 21, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(22, 22, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(23, 23, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(24, 24, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(25, 25, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(26, 26, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(27, 27, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(28, 28, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(29, 29, 1, 1, '2020-02-09 10:04:29', '2020-02-09 10:04:29'),
(30, 30, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(31, 31, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(32, 32, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(33, 33, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(34, 34, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(35, 35, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(36, 36, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(37, 37, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(38, 38, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(39, 39, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(40, 40, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(41, 41, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(42, 42, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(43, 43, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(44, 44, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(45, 45, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(46, 46, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(47, 47, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(48, 48, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(49, 49, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(50, 50, 1, 1, '2020-02-09 10:04:30', '2020-02-09 10:04:30'),
(51, 51, 2, 1, '2020-02-09 10:05:53', '2020-02-09 10:05:53'),
(52, 52, 2, 1, '2020-02-09 10:05:57', '2020-02-09 10:05:57'),
(53, 53, 2, 1, '2020-02-09 10:06:04', '2020-02-09 10:06:04'),
(54, 54, 2, 1, '2020-02-09 10:06:09', '2020-02-09 10:06:09'),
(55, 55, 2, 1, '2020-02-09 10:06:18', '2020-02-09 10:06:18'),
(56, 51, 3, 1, '2020-02-09 10:06:33', '2020-02-09 10:06:33'),
(57, 52, 3, 1, '2020-02-09 10:06:41', '2020-02-09 10:06:41'),
(58, 51, 4, 1, '2020-02-09 10:06:49', '2020-02-09 10:06:49'),
(59, 52, 4, 1, '2020-02-09 10:06:56', '2020-02-09 10:06:56'),
(60, 53, 4, 1, '2020-02-09 10:07:03', '2020-02-09 10:07:03'),
(61, 61, 1, 1, '2020-02-25 08:55:36', '2020-02-25 08:55:36'),
(62, 62, 1, 1, '2020-02-25 08:55:36', '2020-02-25 08:55:36'),
(63, 63, 1, 1, '2020-02-25 08:55:36', '2020-02-25 08:55:36'),
(64, 64, 1, 1, '2020-02-25 08:55:36', '2020-02-25 08:55:36'),
(65, 65, 1, 1, '2020-02-25 08:55:36', '2020-02-25 08:55:36'),
(66, 66, 5, 1, '2020-03-25 09:53:41', '2020-03-25 09:53:41'),
(67, 67, 5, 1, '2020-03-28 09:47:28', '2020-03-28 09:47:28'),
(68, 68, 5, 1, '2020-04-02 10:36:21', '2020-04-02 10:36:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_url`
--

CREATE TABLE `producto_url` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `producto_url`
--

INSERT INTO `producto_url` (`id`, `nombre`, `url`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 'smartcurse', 'https://skscourses.com/json/', 1, '2020-02-21 16:12:55', '2020-02-21 16:12:55'),
(2, 'smartenglish', 'https://smartenglish.eduktvirtual.com/json/', 1, '2020-02-21 16:15:08', '2020-02-21 16:15:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincias`
--

CREATE TABLE `provincias` (
  `id` bigint(20) NOT NULL,
  `idregion` bigint(20) DEFAULT NULL,
  `iddepartamento` bigint(20) NOT NULL DEFAULT '0',
  `nombre` varchar(155) NOT NULL DEFAULT '0' COMMENT 'Nombre de la provincia',
  `abreviacion` varchar(50) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `provincias`
--

INSERT INTO `provincias` (`id`, `idregion`, `iddepartamento`, `nombre`, `abreviacion`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, NULL, 1, 'Chachapoyas', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:17'),
(2, NULL, 1, 'Bagua', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:20'),
(3, NULL, 1, 'Bongará', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:18'),
(4, NULL, 1, 'Condorcanqui', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:21'),
(5, NULL, 1, 'Luya', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:22'),
(6, NULL, 1, 'Rodríguez', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:23'),
(7, NULL, 1, 'Utcubamba', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:23'),
(8, NULL, 2, 'Huaraz', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:24'),
(9, NULL, 2, 'Aija', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:24'),
(10, NULL, 2, 'Antonio Raymond', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:25'),
(11, NULL, 2, 'Asunción', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:26'),
(12, NULL, 2, 'Bolognesi', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:26'),
(13, NULL, 2, 'Carhuaz', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:28'),
(14, NULL, 2, 'Carlos Fermín Fitzcarrald', NULL, '2019-09-13 17:46:14', '2020-02-06 12:42:28'),
(15, NULL, 2, 'Casma', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:29'),
(16, NULL, 2, 'Corongo', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:29'),
(17, NULL, 2, 'Huari', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:30'),
(18, NULL, 2, 'Huarmey', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:31'),
(19, NULL, 2, 'Huaylas', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:33'),
(20, NULL, 2, 'Mariscal Luzuriaga', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:32'),
(21, NULL, 2, 'Ocros', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:34'),
(22, NULL, 2, 'Pallasca', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:34'),
(23, NULL, 2, 'Pomabamba', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:35'),
(24, NULL, 2, 'Recuay', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:36'),
(25, NULL, 2, 'Santa', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:36'),
(26, NULL, 2, 'Sihuas', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:37'),
(27, NULL, 2, 'Yungay', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:38'),
(28, NULL, 3, 'Abancay', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:39'),
(29, NULL, 3, 'Andahuaylas', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:39'),
(30, NULL, 3, 'Antabamba', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:40'),
(31, NULL, 3, 'Aymaraes', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:41'),
(32, NULL, 3, 'Cotabambas', NULL, '2019-09-13 17:46:15', '2020-02-06 12:42:41'),
(33, NULL, 3, 'Chicheros', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(34, NULL, 3, 'Grau', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(35, NULL, 4, 'Arequipa', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(36, NULL, 4, 'Camaná', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(37, NULL, 4, 'Caraveli', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(38, NULL, 4, 'Castilla', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(39, NULL, 4, 'Caylloma', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(40, NULL, 4, 'Condesuyos', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(41, NULL, 4, 'Islay', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(42, NULL, 4, 'La Unión', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(43, NULL, 5, 'Huamanga', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(44, NULL, 5, 'Cangallo', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(45, NULL, 5, 'Huancasancos', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(46, NULL, 5, 'Huanta', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(47, NULL, 5, 'La Mar', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(48, NULL, 5, 'Lucanas', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(49, NULL, 5, 'Parinacochas', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(50, NULL, 5, 'Páucar del Sara Sara', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(51, NULL, 5, 'Sucre', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(52, NULL, 5, 'Víctor Fajardo', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(53, NULL, 5, 'Vilcashuaman', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(54, NULL, 6, 'Cajamarca', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(55, NULL, 6, 'Cajabamba', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(56, NULL, 6, 'Celendín', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(57, NULL, 6, 'Chota', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(58, NULL, 6, 'Contumazá', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(59, NULL, 6, 'Cutervo', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(60, NULL, 6, 'Hualgayoc', NULL, '2019-09-13 17:46:15', '2020-02-06 12:44:06'),
(61, NULL, 6, 'Jaén', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(62, NULL, 6, 'San Ignacio', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(63, NULL, 6, 'San Marcos', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(64, NULL, 6, 'San Miguel', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(65, NULL, 6, 'San Pablo', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(66, NULL, 6, 'Santa Cruz', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(67, NULL, 7, 'Callao', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(68, NULL, 8, 'Cusco', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(69, NULL, 8, 'Acomayo', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(70, NULL, 8, 'Anta', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(71, NULL, 8, 'Calca', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(72, NULL, 8, 'Canas', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(73, NULL, 8, 'Canchis', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(74, NULL, 8, 'Chumbivilcas', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(75, NULL, 8, 'Espinar', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(76, NULL, 8, 'La Convención', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(77, NULL, 8, 'Paruro', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(78, NULL, 8, 'Paucartambo', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(79, NULL, 8, 'Quispicanchi', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(80, NULL, 8, 'Urubamba', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(81, NULL, 9, 'Huancavelica', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(82, NULL, 9, 'Acobamba', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(83, NULL, 9, 'Angaraes', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(84, NULL, 9, 'Castrovirreyna', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(85, NULL, 9, 'Churcampa', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(86, NULL, 9, 'Huaytará', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(87, NULL, 9, 'Tayacaja', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(88, NULL, 10, 'Huanuco', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(89, NULL, 10, 'Ambo', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(90, NULL, 10, 'Dos de Mayo', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(91, NULL, 10, 'Huacaybamba', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(92, NULL, 10, 'Huamalíes', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(93, NULL, 10, 'Leoncio Prado', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(94, NULL, 10, 'Marañón', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(95, NULL, 10, 'Pachitea', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(96, NULL, 10, 'Puerto Inca', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(97, NULL, 10, 'Lauricocha', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(98, NULL, 10, 'Yarowilca', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(99, NULL, 11, 'Ica', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(100, NULL, 11, 'Chincha', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(101, NULL, 11, 'Nazca', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(102, NULL, 11, 'Palpa', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(103, NULL, 11, 'Pisco', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(104, NULL, 12, 'Huancayo', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(105, NULL, 12, 'Concepción', NULL, '2019-09-13 17:46:16', '2020-02-06 12:44:06'),
(106, NULL, 12, 'Chanchamayo', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(107, NULL, 12, 'Jauja', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(108, NULL, 12, 'Junín', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(109, NULL, 12, 'Satipo', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(110, NULL, 12, 'Tarma', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(111, NULL, 12, 'Yauli', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(112, NULL, 12, 'Chupaca', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(113, NULL, 13, 'Trujillo', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(114, NULL, 13, 'Ascope', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(115, NULL, 13, 'Bolívar', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(116, NULL, 13, 'Chepén', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(117, NULL, 13, 'Julcán', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(118, NULL, 13, 'Otuzco', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(119, NULL, 13, 'Pacasmayo', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(120, NULL, 13, 'Pataz', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(121, NULL, 13, 'Sanchez Carrión', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(122, NULL, 13, 'Santiago de Chuco', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(123, NULL, 13, 'Gran Chimú', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(124, NULL, 13, 'Virú', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(125, NULL, 14, 'Chiclayo', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(126, NULL, 14, 'Ferreñafe', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(127, NULL, 14, 'Lambayeque', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(128, NULL, 15, 'Lima', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(129, NULL, 15, 'Barranca', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(130, NULL, 15, 'Cajatambo', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(131, NULL, 15, 'Canta', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(132, NULL, 15, 'Cañete', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(133, NULL, 15, 'Huaral', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(134, NULL, 15, 'Huarochirí', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(135, NULL, 15, 'Huaura', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(136, NULL, 15, 'Oyón', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(137, NULL, 15, 'Yauyos', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(138, NULL, 16, 'Maynas', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(139, NULL, 16, 'Alto Amazonas', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(140, NULL, 16, 'Loreto', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(141, NULL, 16, 'Mariscal Ramón Castilla', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(142, NULL, 16, 'Requena', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(143, NULL, 16, 'Ucayali', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(144, NULL, 16, 'Datem de Marañón', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(145, NULL, 16, 'Putumayo', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(146, NULL, 17, 'Tambopata', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(147, NULL, 17, 'Manu', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(148, NULL, 17, 'Tahuamanu', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(149, NULL, 18, 'Mariscal Nieto', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(150, NULL, 18, 'General Sánchez Cerro', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(151, NULL, 18, 'Ilo', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(152, NULL, 19, 'Pasco', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(153, NULL, 19, 'Daniel Alcides Carrión', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(154, NULL, 19, 'Oxapampa', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(155, NULL, 20, 'Piura', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(156, NULL, 20, 'Ayabaca', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(157, NULL, 20, 'Huancabamba', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(158, NULL, 20, 'Morropón', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(159, NULL, 20, 'Paita', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(160, NULL, 20, 'Sullana', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(161, NULL, 20, 'Talara', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(162, NULL, 20, 'Sechura', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(163, NULL, 21, 'Puno', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(164, NULL, 21, 'Azángaro', NULL, '2019-09-13 17:46:17', '2020-02-06 12:44:06'),
(165, NULL, 21, 'Carabaya', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(166, NULL, 21, 'Chucuito', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(167, NULL, 21, 'El Collao', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(168, NULL, 21, 'Huacané', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(169, NULL, 21, 'Lampa', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(170, NULL, 21, 'Melgar', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(171, NULL, 21, 'Moho', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(172, NULL, 21, 'San Antonio de Putiña', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(173, NULL, 21, 'San Román', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(174, NULL, 21, 'Sandía', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(175, NULL, 21, 'Yunguyo', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(176, NULL, 22, 'Moyobamba', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(177, NULL, 22, 'Bellavista', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(178, NULL, 22, 'El Dorado', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(179, NULL, 22, 'Huallaga', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(180, NULL, 22, 'Lamas', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(181, NULL, 22, 'Mariscal Cáceres', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(182, NULL, 22, 'Picota', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(183, NULL, 22, 'Rioja', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(184, NULL, 22, 'San Martín', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(185, NULL, 22, 'Tocache', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(186, NULL, 23, 'Tacna', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(187, NULL, 23, 'Candarave', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(188, NULL, 23, 'Jorge Basadre', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(189, NULL, 23, 'Tarata', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(190, NULL, 24, 'Tumbes', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(191, NULL, 24, 'Contralmirante Villar', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(192, NULL, 24, 'Zarumilla', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(193, NULL, 25, 'Coronel Portillo', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(194, NULL, 25, 'Atalaya', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(195, NULL, 25, 'Padre Abad', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06'),
(196, NULL, 25, 'Purús', NULL, '2019-09-13 17:46:18', '2020-02-06 12:44:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regiones`
--

CREATE TABLE `regiones` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(155) NOT NULL COMMENT 'Nombre de la región',
  `abreviacion` varchar(50) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `regiones`
--

INSERT INTO `regiones` (`id`, `nombre`, `abreviacion`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 'Cusco-Apurímac', NULL, '2019-09-13 15:47:59', '2019-09-13 15:47:59'),
(2, 'Sur Andina', NULL, '2019-09-13 15:48:37', '2019-09-13 15:48:37'),
(3, 'Ica-Ayacucho-Huancavelica', NULL, '2019-09-13 15:50:43', '2019-09-13 15:50:43'),
(4, 'Nor-Centro-Oriental', NULL, '2019-09-13 15:50:54', '2019-09-13 15:50:54'),
(5, 'Norte', NULL, '2019-09-13 15:51:02', '2019-09-13 15:51:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representantes`
--

CREATE TABLE `representantes` (
  `id` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `idambito` bigint(20) DEFAULT '0',
  `idpadre` bigint(20) DEFAULT NULL,
  `tipo` int(11) DEFAULT '1' COMMENT 'Diferente tipo de representacion, R, RA y RM',
  `estado` tinyint(4) DEFAULT '1',
  `idclon` bigint(20) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `representantes`
--

INSERT INTO `representantes` (`id`, `idpersona`, `idambito`, `idpadre`, `tipo`, `estado`, `idclon`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 2, NULL, NULL, 2, 1, NULL, '2019-09-20 10:48:15', '2020-02-10 16:12:42'),
(2, 3, NULL, 1, 3, 1, NULL, '2019-10-23 11:42:48', '2020-02-28 10:17:30'),
(3, 5, NULL, 1, 3, 1, NULL, '2020-02-28 10:18:46', '2020-02-28 10:19:04'),
(4, 6, NULL, 1, 3, 1, NULL, '2019-10-23 11:42:48', '2020-02-28 10:17:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idrol` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT '0',
  `abreviatura` char(50) DEFAULT NULL COMMENT 'Opcional, abreviación del rol',
  `estado` int(11) DEFAULT '1',
  `imagen` varchar(250) DEFAULT NULL,
  `id_padre` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idrol`, `nombre`, `abreviatura`, `estado`, `imagen`, `id_padre`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 'Super Administrador', 'SUPER ADMIN', 1, NULL, NULL, '2019-09-12 10:26:41', '2020-01-10 17:59:27'),
(2, 'Representante Master', 'RM', 1, '', 1, '2019-09-12 10:26:41', '2020-01-09 07:09:36'),
(3, 'Representante', 'R', 1, '', 2, '2019-09-12 10:26:41', '2020-01-09 06:56:08'),
(4, 'Representante Independiente', 'IN', 1, '', 3, '2019-09-12 10:26:41', '2020-01-24 16:33:25'),
(5, 'Contador', 'CON', 1, NULL, 1, '2020-01-09 06:54:08', '2020-01-13 10:25:32'),
(6, 'Coordinador', 'COO', 1, NULL, 1, '2020-02-28 15:20:06', '2020-02-28 15:20:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `session`
--

CREATE TABLE `session` (
  `persona` bigint(20) NOT NULL,
  `IPaddress` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `ubigeo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `navegador` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sistOpe` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora_ingreso` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `session`
--

INSERT INTO `session` (`persona`, `IPaddress`, `ubigeo`, `navegador`, `sistOpe`, `fecha`, `hora_ingreso`) VALUES
(1, '181.176.108.113', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-22', '21:58:52'),
(3, '181.64.105.52', 'Trujillo - La Libertad - Peru', 'Google Chrome', 'Windows 10', '2020-02-23', '02:22:52'),
(1, '181.176.96.63', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-23', '10:37:36'),
(1, '201.240.148.13', 'Cajamarca - Cajamarca - Peru', 'Google Chrome', 'Windows 10', '2020-02-23', '12:21:29'),
(3, '201.240.148.13', 'Cajamarca - Cajamarca - Peru', 'Google Chrome', 'Windows 10', '2020-02-23', '12:22:02'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-02-24', '11:18:45'),
(3, '190.239.139.150', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:30:39'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:31:42'),
(2, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:33:36'),
(2, '190.239.139.150', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:34:28'),
(2, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:34:29'),
(2, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:35:08'),
(2, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:37:53'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:39:56'),
(1, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:40:13'),
(3, '190.239.139.150', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:44:44'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:45:16'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '15:50:12'),
(2, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-02-24', '16:28:57'),
(1, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '17:02:41'),
(3, '190.239.139.150', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '17:12:20'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '17:57:56'),
(3, '190.239.139.150', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '18:06:55'),
(3, '190.239.139.150', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-24', '18:37:57'),
(3, '190.239.139.253', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-25', '14:59:49'),
(3, '190.239.139.141', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-25', '15:25:04'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-25', '15:50:16'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-25', '18:24:57'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-26', '08:59:14'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-02-26', '17:20:56'),
(3, '190.239.139.6', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-27', '12:33:18'),
(1, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-27', '15:17:39'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-02-27', '16:06:35'),
(3, '209.45.48.29', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-27', '17:17:12'),
(3, '190.239.139.249', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-28', '09:32:58'),
(4, '190.239.139.249', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-28', '09:38:34'),
(3, '190.239.139.249', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-02-28', '10:11:08'),
(1, '179.7.48.63', 'Lima - Lima - Peru', 'Safari', 'iPhone', '2020-02-28', '10:24:32'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-02-28', '16:23:36'),
(5, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-28', '16:36:28'),
(5, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-28', '16:55:34'),
(3, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-28', '17:17:26'),
(3, '209.45.48.29', 'Lima - Lima - Peru', 'Microsoft Edge', 'Windows 10', '2020-02-28', '17:23:39'),
(5, '179.6.46.227', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-02-28', '17:42:34'),
(3, '209.45.48.29', 'Lima - Lima - Peru', 'Microsoft Edge', 'Windows 10', '2020-03-02', '14:19:08'),
(5, '190.239.139.215', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-03-03', '17:38:18'),
(5, '190.239.139.128', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-03-04', '16:43:38'),
(5, '190.239.139.128', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-03-04', '17:51:31'),
(5, '190.239.139.128', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-03-04', '18:08:07'),
(3, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-04', '18:33:49'),
(5, '190.239.139.128', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-03-05', '09:09:18'),
(5, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-03-05', '11:03:02'),
(5, '190.239.139.128', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-03-05', '11:24:02'),
(5, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-05', '17:40:17'),
(5, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-06', '09:56:21'),
(1, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-03-06', '09:56:28'),
(5, '190.239.139.191', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-03-06', '12:11:27'),
(3, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-09', '09:18:48'),
(1, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-09', '11:24:02'),
(3, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-09', '11:25:27'),
(5, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-09', '11:36:58'),
(3, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-03-09', '12:03:45'),
(3, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-09', '16:02:16'),
(5, '190.239.139.248', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-03-13', '12:51:06'),
(5, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-03-13', '13:04:37'),
(1, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-14', '10:03:34'),
(3, '179.6.47.70', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 8', '2020-03-14', '10:07:43'),
(3, '181.176.97.82', 'Tacna - Tacna - Peru', 'Google Chrome', 'Windows 10', '2020-03-15', '07:53:12'),
(3, '181.176.110.154', 'Tacna - Tacna - Peru', 'Google Chrome', 'Windows 8', '2020-03-18', '08:55:33'),
(3, '181.176.110.154', 'Tacna - Tacna - Peru', 'Google Chrome', 'Windows 8', '2020-03-18', '20:34:24'),
(3, '201.240.183.38', 'Chongoyape - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-03-19', '10:30:22'),
(3, '201.230.169.254', 'Piura - Piura - Peru', 'Google Chrome', 'Windows 10', '2020-03-28', '08:45:15'),
(3, '181.176.121.106', 'Tacna - Tacna - Peru', 'Google Chrome', 'Windows 8', '2020-03-28', '10:40:22'),
(6, '201.230.169.254', 'Piura - Piura - Peru', 'Google Chrome', 'Windows 10', '2020-03-31', '20:52:34'),
(5, '179.7.58.39', 'Lima - Lima - Peru', 'Google Chrome', 'Android', '2020-04-01', '08:44:53'),
(6, '181.176.101.4', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-04-01', '09:09:38'),
(3, '181.176.101.4', 'Lima - Lima - Peru', 'Google Chrome', 'Windows 10', '2020-04-01', '09:42:26'),
(3, '190.233.79.189', 'Chiclayo - Lambayeque - Peru', 'Google Chrome', 'Windows 10', '2020-04-01', '09:42:43'),
(3, '181.176.96.255', 'Puno - Puno - Peru', 'Google Chrome', 'Windows 8', '2020-04-02', '10:14:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiporepresentantes`
--

CREATE TABLE `tiporepresentantes` (
  `id` int(11) NOT NULL,
  `idrol` int(11) DEFAULT '0' COMMENT 'Id del rol asociado',
  `ambito` int(1) DEFAULT '0' COMMENT '1= true, 0 = false',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `tiporepresentantes`
--

INSERT INTO `tiporepresentantes` (`id`, `idrol`, `ambito`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 2, 1, '2019-09-16 15:01:21', '2020-01-09 04:37:54'),
(2, 3, 1, '2019-09-16 15:02:12', '2020-01-09 06:57:53'),
(3, 4, 0, '2019-09-16 15:02:30', '2020-01-09 06:57:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitud` tinyint(4) DEFAULT '8',
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id`, `nombre`, `longitud`, `estado`) VALUES
(1, 'DNI', 8, 1),
(2, 'Pasaporte', 11, 1),
(3, 'Otros', 15, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_docentes`
--

CREATE TABLE `tmp_docentes` (
  `id` bigint(20) NOT NULL,
  `codigo` varchar(20) DEFAULT NULL,
  `idrepresentante` bigint(20) NOT NULL DEFAULT '0',
  `idorden_servicio` bigint(255) DEFAULT NULL,
  `idproducto` bigint(255) DEFAULT NULL,
  `fileData` varchar(100) DEFAULT NULL,
  `fileComprobante` varchar(100) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1' COMMENT '1 - por aprobar, 2 - aprobado, 3 - anulada',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `tmp_docentes`
--

INSERT INTO `tmp_docentes` (`id`, `codigo`, `idrepresentante`, `idorden_servicio`, `idproducto`, `fileData`, `fileComprobante`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 'TMPD0001R0002', 2, 2, 1, 'TMPD0001R0002.php', '', 2, '2020-02-17 12:56:43', '2020-02-17 16:53:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_matriculas`
--

CREATE TABLE `tmp_matriculas` (
  `id` bigint(20) NOT NULL,
  `codigo` varchar(20) DEFAULT NULL,
  `idrepresentante` bigint(20) NOT NULL DEFAULT '0',
  `idorden_servicio` bigint(255) DEFAULT NULL,
  `idproducto` bigint(255) DEFAULT NULL,
  `fileData` varchar(100) DEFAULT NULL,
  `fileComprobante` varchar(100) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1' COMMENT '1 - por aprobar, 2 - aprobado, 3 - anulada',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `tmp_matriculas`
--

INSERT INTO `tmp_matriculas` (`id`, `codigo`, `idrepresentante`, `idorden_servicio`, `idproducto`, `fileData`, `fileComprobante`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 'TMP0001R0002', 2, 2, 1, 'TMP0001R0002.php', 'TMP0001R0002.pdf', 3, '2020-02-12 16:02:07', '2020-02-15 07:29:31'),
(2, 'TMP0002R0002', 2, 2, 1, 'TMP0002R0002.php', '', 3, '2020-02-13 15:02:26', '2020-02-17 09:38:14'),
(3, 'TMP0003R0002', 2, 2, 1, 'TMP0003R0002.php', '', 2, '2020-02-14 18:02:01', '2020-02-17 09:32:00'),
(4, 'TMP0004R0002', 2, 2, 1, 'TMP0004R0002.php', '', 2, '2020-02-17 09:02:48', '2020-02-17 15:08:57'),
(5, 'TMP0005R0002', 2, 2, 1, 'TMP0005R0002.php', '', 1, '2020-02-17 13:02:14', '2020-02-17 13:01:14'),
(6, 'TMP0006R0002', 2, 2, 1, 'TMP0006R0002.php', '', 2, '2020-02-21 10:02:46', '2020-02-21 10:53:47');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acceso`
--
ALTER TABLE `acceso`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idcurso` (`idproducto_curso`) USING BTREE,
  ADD KEY `idrepresentante` (`idrepresentante`) USING BTREE;

--
-- Indices de la tabla `ambito_representantes`
--
ALTER TABLE `ambito_representantes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idrepresentante` (`idrepresentante`) USING BTREE,
  ADD KEY `idregion` (`idregion`) USING BTREE,
  ADD KEY `idpais` (`idpais`) USING BTREE,
  ADD KEY `iddepartamento` (`iddepartamento`) USING BTREE,
  ADD KEY `idprovincia` (`idprovincia`) USING BTREE,
  ADD KEY `idciudad` (`idciudad`) USING BTREE;

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD KEY `persona` (`persona`) USING BTREE;

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idprovincia` (`idprovincia`) USING BTREE,
  ADD KEY `idregion` (`idregion`) USING BTREE;

--
-- Indices de la tabla `cuota_depago`
--
ALTER TABLE `cuota_depago`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `politica_depago` (`politica_depago`) USING BTREE;

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idproducto` (`idproducto_url`) USING BTREE;

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idpais` (`idpais`) USING BTREE,
  ADD KEY `idregion` (`idregion`) USING BTREE;

--
-- Indices de la tabla `docentes`
--
ALTER TABLE `docentes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `iddocente` (`idpersona`) USING BTREE,
  ADD KEY `os_curso` (`os_curso`) USING BTREE,
  ADD KEY `idproducto` (`idproducto`),
  ADD KEY `registrado_por` (`registrado_por`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idpais` (`idpais`) USING BTREE,
  ADD KEY `iddepartamento` (`iddepartamento`) USING BTREE,
  ADD KEY `idprovincia` (`idprovincia`) USING BTREE,
  ADD KEY `idciudad` (`idciudad`) USING BTREE;

--
-- Indices de la tabla `empresa_persona`
--
ALTER TABLE `empresa_persona`
  ADD PRIMARY KEY (`idpersona`,`idempresa`) USING BTREE,
  ADD KEY `idempresa` (`idempresa`) USING BTREE;

--
-- Indices de la tabla `empresa_sede`
--
ALTER TABLE `empresa_sede`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idpais` (`idpais`) USING BTREE,
  ADD KEY `iddepartamento` (`iddepartamento`) USING BTREE,
  ADD KEY `idprovincia` (`idprovincia`) USING BTREE,
  ADD KEY `idciudad` (`idciudad`) USING BTREE,
  ADD KEY `idempresa` (`idempresa`);

--
-- Indices de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idpersona` (`idpersona`) USING BTREE,
  ADD KEY `registrado_por` (`registrado_por`) USING BTREE;

--
-- Indices de la tabla `inscripcion_pago`
--
ALTER TABLE `inscripcion_pago`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `alumno` (`alumno`) USING BTREE;

--
-- Indices de la tabla `inscripcion_pago_detalle`
--
ALTER TABLE `inscripcion_pago_detalle`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `pago` (`pago`) USING BTREE,
  ADD KEY `cuota` (`idinscripcion`) USING BTREE;

--
-- Indices de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `matriculas_ibfk_1` (`idpersona`) USING BTREE,
  ADD KEY `idrepresentante` (`idrepresentante`) USING BTREE,
  ADD KEY `os_curso` (`os_curso`) USING BTREE,
  ADD KEY `idinscripcion` (`idinscripcion`) USING BTREE,
  ADD KEY `idempresa_sede` (`idempresa_sede`);

--
-- Indices de la tabla `matricula_cuota_depago`
--
ALTER TABLE `matricula_cuota_depago`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `politica_depago` (`idmatricula`) USING BTREE;

--
-- Indices de la tabla `matricula_pago`
--
ALTER TABLE `matricula_pago`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `alumno` (`alumno`) USING BTREE;

--
-- Indices de la tabla `matricula_pago_detalle`
--
ALTER TABLE `matricula_pago_detalle`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `pago` (`pago`) USING BTREE,
  ADD KEY `cuota` (`cuota`) USING BTREE;

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`codigo_menu`) USING BTREE,
  ADD KEY `id_padre` (`id_padre`) USING BTREE;

--
-- Indices de la tabla `menu_accesos`
--
ALTER TABLE `menu_accesos`
  ADD PRIMARY KEY (`menu`,`rol`) USING BTREE,
  ADD KEY `rol` (`rol`) USING BTREE,
  ADD KEY `menu` (`menu`) USING BTREE;

--
-- Indices de la tabla `moneda`
--
ALTER TABLE `moneda`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `notificacion`
--
ALTER TABLE `notificacion`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `iduser` (`iduser`) USING BTREE;

--
-- Indices de la tabla `orden_servicio`
--
ALTER TABLE `orden_servicio`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idrepresentante` (`idrepresentante`) USING BTREE,
  ADD KEY `tipomoneda` (`tipomoneda`) USING BTREE;

--
-- Indices de la tabla `orden_servicio_curso`
--
ALTER TABLE `orden_servicio_curso`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `os_producto` (`os_producto`) USING BTREE,
  ADD KEY `idproducto_curso` (`idproducto_curso`) USING BTREE;

--
-- Indices de la tabla `orden_servicio_producto`
--
ALTER TABLE `orden_servicio_producto`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `orden_servicio` (`orden_servicio`) USING BTREE,
  ADD KEY `idproducto` (`idproducto`) USING BTREE;

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `representante` (`representante`) USING BTREE,
  ADD KEY `registrado_por` (`registrado_por`) USING BTREE;

--
-- Indices de la tabla `pago_detalle`
--
ALTER TABLE `pago_detalle`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `pago` (`pago`) USING BTREE,
  ADD KEY `cuota` (`cuota`) USING BTREE;

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idregion` (`idregion`) USING BTREE;

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`) USING BTREE,
  ADD KEY `idpersona` (`idpersona`) USING BTREE,
  ADD KEY `tipodoc` (`tipodoc`) USING BTREE;

--
-- Indices de la tabla `persona_externa`
--
ALTER TABLE `persona_externa`
  ADD PRIMARY KEY (`idpersona`) USING BTREE,
  ADD KEY `idpersona` (`idpersona`) USING BTREE;

--
-- Indices de la tabla `persona_rol`
--
ALTER TABLE `persona_rol`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idpersona` (`idpersona`) USING BTREE,
  ADD KEY `idrol` (`idrol`) USING BTREE;

--
-- Indices de la tabla `politica_depago`
--
ALTER TABLE `politica_depago`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idorden_servicio` (`idorden_servicio`) USING BTREE;

--
-- Indices de la tabla `precio_promocion`
--
ALTER TABLE `precio_promocion`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idempresa` (`idempresa`) USING BTREE;

--
-- Indices de la tabla `precio_promocion_detalle`
--
ALTER TABLE `precio_promocion_detalle`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idprecio_promocion` (`idprecio_promocion`) USING BTREE;

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idurl` (`idproducto_url`) USING BTREE;

--
-- Indices de la tabla `producto_cursos`
--
ALTER TABLE `producto_cursos`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idcurso` (`idcurso`) USING BTREE,
  ADD KEY `idproducto` (`idproducto`) USING BTREE;

--
-- Indices de la tabla `producto_url`
--
ALTER TABLE `producto_url`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `provincias`
--
ALTER TABLE `provincias`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `iddepartamento` (`iddepartamento`) USING BTREE;

--
-- Indices de la tabla `regiones`
--
ALTER TABLE `regiones`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `representantes`
--
ALTER TABLE `representantes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idpadre` (`idpadre`) USING BTREE,
  ADD KEY `idclon` (`idclon`) USING BTREE,
  ADD KEY `idpersona` (`idpersona`) USING BTREE;

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idrol`) USING BTREE;

--
-- Indices de la tabla `session`
--
ALTER TABLE `session`
  ADD KEY `persona` (`persona`) USING BTREE;

--
-- Indices de la tabla `tiporepresentantes`
--
ALTER TABLE `tiporepresentantes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idrol` (`idrol`) USING BTREE;

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `tmp_docentes`
--
ALTER TABLE `tmp_docentes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `matriculas_ibfk_1` (`codigo`) USING BTREE,
  ADD KEY `idrepresentante` (`idrepresentante`) USING BTREE,
  ADD KEY `idorden_servicio` (`idorden_servicio`) USING BTREE,
  ADD KEY `idproducto` (`idproducto`) USING BTREE;

--
-- Indices de la tabla `tmp_matriculas`
--
ALTER TABLE `tmp_matriculas`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `matriculas_ibfk_1` (`codigo`) USING BTREE,
  ADD KEY `idrepresentante` (`idrepresentante`) USING BTREE,
  ADD KEY `idorden_servicio` (`idorden_servicio`) USING BTREE,
  ADD KEY `idproducto` (`idproducto`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acceso`
--
ALTER TABLE `acceso`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `ambito_representantes`
--
ALTER TABLE `ambito_representantes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuota_depago`
--
ALTER TABLE `cuota_depago`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'llave primaria de la tabla', AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `docentes`
--
ALTER TABLE `docentes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `empresa_sede`
--
ALTER TABLE `empresa_sede`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inscripcion_pago_detalle`
--
ALTER TABLE `inscripcion_pago_detalle`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=339;

--
-- AUTO_INCREMENT de la tabla `matricula_cuota_depago`
--
ALTER TABLE `matricula_cuota_depago`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=356;

--
-- AUTO_INCREMENT de la tabla `matricula_pago_detalle`
--
ALTER TABLE `matricula_pago_detalle`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=352;

--
-- AUTO_INCREMENT de la tabla `moneda`
--
ALTER TABLE `moneda`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `notificacion`
--
ALTER TABLE `notificacion`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pago_detalle`
--
ALTER TABLE `pago_detalle`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `persona_rol`
--
ALTER TABLE `persona_rol`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `politica_depago`
--
ALTER TABLE `politica_depago`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `producto_cursos`
--
ALTER TABLE `producto_cursos`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `provincias`
--
ALTER TABLE `provincias`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT de la tabla `regiones`
--
ALTER TABLE `regiones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `representantes`
--
ALTER TABLE `representantes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tiporepresentantes`
--
ALTER TABLE `tiporepresentantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acceso`
--
ALTER TABLE `acceso`
  ADD CONSTRAINT `acceso_ibfk_2` FOREIGN KEY (`idrepresentante`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `acceso_ibfk_3` FOREIGN KEY (`idproducto_curso`) REFERENCES `producto_cursos` (`id`);

--
-- Filtros para la tabla `ambito_representantes`
--
ALTER TABLE `ambito_representantes`
  ADD CONSTRAINT `ambito_representantes_ibfk_1` FOREIGN KEY (`idrepresentante`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `ambito_representantes_ibfk_2` FOREIGN KEY (`idregion`) REFERENCES `regiones` (`id`),
  ADD CONSTRAINT `ambito_representantes_ibfk_3` FOREIGN KEY (`idpais`) REFERENCES `pais` (`id`),
  ADD CONSTRAINT `ambito_representantes_ibfk_4` FOREIGN KEY (`iddepartamento`) REFERENCES `departamentos` (`id`),
  ADD CONSTRAINT `ambito_representantes_ibfk_5` FOREIGN KEY (`idprovincia`) REFERENCES `provincias` (`id`),
  ADD CONSTRAINT `ambito_representantes_ibfk_6` FOREIGN KEY (`idciudad`) REFERENCES `ciudad` (`id`);

--
-- Filtros para la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD CONSTRAINT `bitacora_ibfk_1` FOREIGN KEY (`persona`) REFERENCES `persona` (`idpersona`);

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`idprovincia`) REFERENCES `provincias` (`id`),
  ADD CONSTRAINT `ciudad_ibfk_2` FOREIGN KEY (`idregion`) REFERENCES `regiones` (`id`);

--
-- Filtros para la tabla `cuota_depago`
--
ALTER TABLE `cuota_depago`
  ADD CONSTRAINT `cuota_depago_ibfk_1` FOREIGN KEY (`politica_depago`) REFERENCES `politica_depago` (`id`);

--
-- Filtros para la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `cursos_ibfk_1` FOREIGN KEY (`idproducto_url`) REFERENCES `producto_url` (`id`);

--
-- Filtros para la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD CONSTRAINT `departamentos_ibfk_1` FOREIGN KEY (`idpais`) REFERENCES `pais` (`id`),
  ADD CONSTRAINT `departamentos_ibfk_2` FOREIGN KEY (`idregion`) REFERENCES `regiones` (`id`);

--
-- Filtros para la tabla `docentes`
--
ALTER TABLE `docentes`
  ADD CONSTRAINT `docentes_ibfk_1` FOREIGN KEY (`idpersona`) REFERENCES `persona_externa` (`idpersona`),
  ADD CONSTRAINT `docentes_ibfk_2` FOREIGN KEY (`os_curso`) REFERENCES `orden_servicio_curso` (`id`),
  ADD CONSTRAINT `docentes_ibfk_3` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `docentes_ibfk_4` FOREIGN KEY (`registrado_por`) REFERENCES `persona` (`idpersona`);

--
-- Filtros para la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `empresa_ibfk_1` FOREIGN KEY (`idpais`) REFERENCES `pais` (`id`),
  ADD CONSTRAINT `empresa_ibfk_2` FOREIGN KEY (`iddepartamento`) REFERENCES `departamentos` (`id`),
  ADD CONSTRAINT `empresa_ibfk_3` FOREIGN KEY (`idprovincia`) REFERENCES `provincias` (`id`),
  ADD CONSTRAINT `empresa_ibfk_4` FOREIGN KEY (`idciudad`) REFERENCES `ciudad` (`id`);

--
-- Filtros para la tabla `empresa_persona`
--
ALTER TABLE `empresa_persona`
  ADD CONSTRAINT `empresa_persona_ibfk_1` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`),
  ADD CONSTRAINT `empresa_persona_ibfk_2` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`id`);

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `inscripcion_ibfk_1` FOREIGN KEY (`idpersona`) REFERENCES `persona_externa` (`idpersona`),
  ADD CONSTRAINT `inscripcion_ibfk_2` FOREIGN KEY (`registrado_por`) REFERENCES `persona` (`idpersona`);

--
-- Filtros para la tabla `inscripcion_pago`
--
ALTER TABLE `inscripcion_pago`
  ADD CONSTRAINT `inscripcion_pago_ibfk_1` FOREIGN KEY (`alumno`) REFERENCES `persona_externa` (`idpersona`);

--
-- Filtros para la tabla `inscripcion_pago_detalle`
--
ALTER TABLE `inscripcion_pago_detalle`
  ADD CONSTRAINT `inscripcion_pago_detalle_ibfk_1` FOREIGN KEY (`pago`) REFERENCES `inscripcion_pago` (`id`),
  ADD CONSTRAINT `inscripcion_pago_detalle_ibfk_2` FOREIGN KEY (`idinscripcion`) REFERENCES `inscripcion` (`id`);

--
-- Filtros para la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD CONSTRAINT `matriculas_ibfk_1` FOREIGN KEY (`idpersona`) REFERENCES `persona_externa` (`idpersona`),
  ADD CONSTRAINT `matriculas_ibfk_2` FOREIGN KEY (`idrepresentante`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `matriculas_ibfk_3` FOREIGN KEY (`os_curso`) REFERENCES `orden_servicio_curso` (`id`),
  ADD CONSTRAINT `matriculas_ibfk_4` FOREIGN KEY (`idinscripcion`) REFERENCES `inscripcion` (`id`);

--
-- Filtros para la tabla `matricula_cuota_depago`
--
ALTER TABLE `matricula_cuota_depago`
  ADD CONSTRAINT `matricula_cuota_depago_ibfk_1` FOREIGN KEY (`idmatricula`) REFERENCES `matriculas` (`id`);

--
-- Filtros para la tabla `matricula_pago`
--
ALTER TABLE `matricula_pago`
  ADD CONSTRAINT `matricula_pago_ibfk_1` FOREIGN KEY (`alumno`) REFERENCES `persona_externa` (`idpersona`);

--
-- Filtros para la tabla `matricula_pago_detalle`
--
ALTER TABLE `matricula_pago_detalle`
  ADD CONSTRAINT `matricula_pago_detalle_ibfk_1` FOREIGN KEY (`pago`) REFERENCES `matricula_pago` (`id`),
  ADD CONSTRAINT `matricula_pago_detalle_ibfk_2` FOREIGN KEY (`cuota`) REFERENCES `matricula_cuota_depago` (`id`);

--
-- Filtros para la tabla `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`id_padre`) REFERENCES `menu` (`codigo_menu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `menu_accesos`
--
ALTER TABLE `menu_accesos`
  ADD CONSTRAINT `menu_accesos_ibfk_1` FOREIGN KEY (`rol`) REFERENCES `roles` (`idrol`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `menu_accesos_ibfk_2` FOREIGN KEY (`menu`) REFERENCES `menu` (`codigo_menu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `notificacion`
--
ALTER TABLE `notificacion`
  ADD CONSTRAINT `notificacion_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `persona` (`idpersona`);

--
-- Filtros para la tabla `orden_servicio`
--
ALTER TABLE `orden_servicio`
  ADD CONSTRAINT `orden_servicio_ibfk_1` FOREIGN KEY (`idrepresentante`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `orden_servicio_ibfk_2` FOREIGN KEY (`tipomoneda`) REFERENCES `moneda` (`id`);

--
-- Filtros para la tabla `orden_servicio_curso`
--
ALTER TABLE `orden_servicio_curso`
  ADD CONSTRAINT `orden_servicio_curso_ibfk_1` FOREIGN KEY (`os_producto`) REFERENCES `orden_servicio_producto` (`id`),
  ADD CONSTRAINT `orden_servicio_curso_ibfk_2` FOREIGN KEY (`idproducto_curso`) REFERENCES `producto_cursos` (`id`);

--
-- Filtros para la tabla `orden_servicio_producto`
--
ALTER TABLE `orden_servicio_producto`
  ADD CONSTRAINT `orden_servicio_producto_ibfk_1` FOREIGN KEY (`orden_servicio`) REFERENCES `orden_servicio` (`id`),
  ADD CONSTRAINT `orden_servicio_producto_ibfk_2` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`representante`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `pago_ibfk_2` FOREIGN KEY (`registrado_por`) REFERENCES `persona` (`idpersona`);

--
-- Filtros para la tabla `pago_detalle`
--
ALTER TABLE `pago_detalle`
  ADD CONSTRAINT `pago_detalle_ibfk_1` FOREIGN KEY (`pago`) REFERENCES `pago` (`id`),
  ADD CONSTRAINT `pago_detalle_ibfk_2` FOREIGN KEY (`cuota`) REFERENCES `cuota_depago` (`id`);

--
-- Filtros para la tabla `pais`
--
ALTER TABLE `pais`
  ADD CONSTRAINT `pais_ibfk_1` FOREIGN KEY (`idregion`) REFERENCES `regiones` (`id`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`tipodoc`) REFERENCES `tipo_documento` (`id`);

--
-- Filtros para la tabla `persona_rol`
--
ALTER TABLE `persona_rol`
  ADD CONSTRAINT `persona_rol_ibfk_1` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`),
  ADD CONSTRAINT `persona_rol_ibfk_2` FOREIGN KEY (`idrol`) REFERENCES `roles` (`idrol`);

--
-- Filtros para la tabla `politica_depago`
--
ALTER TABLE `politica_depago`
  ADD CONSTRAINT `politica_depago_ibfk_1` FOREIGN KEY (`idorden_servicio`) REFERENCES `orden_servicio` (`id`);

--
-- Filtros para la tabla `precio_promocion`
--
ALTER TABLE `precio_promocion`
  ADD CONSTRAINT `precio_promocion_ibfk_1` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`id`);

--
-- Filtros para la tabla `precio_promocion_detalle`
--
ALTER TABLE `precio_promocion_detalle`
  ADD CONSTRAINT `precio_promocion_detalle_ibfk_1` FOREIGN KEY (`idprecio_promocion`) REFERENCES `precio_promocion` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`idproducto_url`) REFERENCES `producto_url` (`id`);

--
-- Filtros para la tabla `producto_cursos`
--
ALTER TABLE `producto_cursos`
  ADD CONSTRAINT `producto_cursos_ibfk_1` FOREIGN KEY (`idcurso`) REFERENCES `cursos` (`id`),
  ADD CONSTRAINT `producto_cursos_ibfk_2` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `provincias`
--
ALTER TABLE `provincias`
  ADD CONSTRAINT `provincias_ibfk_1` FOREIGN KEY (`iddepartamento`) REFERENCES `departamentos` (`id`);

--
-- Filtros para la tabla `representantes`
--
ALTER TABLE `representantes`
  ADD CONSTRAINT `representantes_ibfk_1` FOREIGN KEY (`idpadre`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `representantes_ibfk_2` FOREIGN KEY (`idclon`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `representantes_ibfk_3` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`);

--
-- Filtros para la tabla `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`persona`) REFERENCES `persona` (`idpersona`);

--
-- Filtros para la tabla `tiporepresentantes`
--
ALTER TABLE `tiporepresentantes`
  ADD CONSTRAINT `tiporepresentantes_ibfk_1` FOREIGN KEY (`idrol`) REFERENCES `roles` (`idrol`);

--
-- Filtros para la tabla `tmp_docentes`
--
ALTER TABLE `tmp_docentes`
  ADD CONSTRAINT `tmp_docentes_ibfk_1` FOREIGN KEY (`idrepresentante`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `tmp_docentes_ibfk_2` FOREIGN KEY (`idorden_servicio`) REFERENCES `orden_servicio` (`id`),
  ADD CONSTRAINT `tmp_docentes_ibfk_3` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `tmp_matriculas`
--
ALTER TABLE `tmp_matriculas`
  ADD CONSTRAINT `tmp_matriculas_ibfk_2` FOREIGN KEY (`idrepresentante`) REFERENCES `representantes` (`id`),
  ADD CONSTRAINT `tmp_matriculas_ibfk_3` FOREIGN KEY (`idorden_servicio`) REFERENCES `orden_servicio` (`id`),
  ADD CONSTRAINT `tmp_matriculas_ibfk_4` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
