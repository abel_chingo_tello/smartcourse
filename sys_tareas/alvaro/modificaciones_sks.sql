SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------

/* 22-03-2020 */

ALTER TABLE `cursos` 
ADD COLUMN `tipocurso` tinyint NULL DEFAULT 1 AFTER `descripcion`;

/* 23-03-2020 */

INSERT INTO `orden_servicio_curso`(`id`, `os_producto`, `idproducto_curso`, `lic_estudiantes`, `lic_docentes`, `mat_estudiantes`, `mat_docentes`, `estado`) VALUES (24, 2, 63, 0, 0, 0, 0, 1);
INSERT INTO `acceso`(`id`, `idrepresentante`, `idproyectoplataforma`, `idempresaplataforma`, `idproducto_curso`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES (29, 2, 7, 10, 63, 1, '2020-02-25 13:01:35', '2020-02-25 13:01:35');

/* 24-03-2020 */

INSERT INTO `productos`(`id`, `nombre`, `descripcion`, `logo`, `idproducto_url`, `idioma`, `precio_dolar`, `precio_docente`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES (5, 'EduktMaestro', NULL, 'static/media/proyectos/eduktlogo.png', 1, 'ES', 12.00, 20.00, 1, '2020-03-24 16:56:00', '2020-03-24 16:56:00');
INSERT INTO `orden_servicio_producto`(`id`, `orden_servicio`, `idproducto`, `precio_base_inscripcion`, `precio_base_curso`, `precio_base_docente`, `precio_inscripcion`, `precio_curso`, `precio_docente`, `precio_curso_libre`, `estado`) VALUES (4, 2, 5, 100.00, 60.00, 0.00, 100.00, 60.00, 0.00, 120.00, 1);

/* 25-03-2020 */

ALTER TABLE `productos`
  ADD COLUMN `tipodecurso` INT(11) NULL DEFAULT '1' COMMENT '1= son los cursos de la edumax, 2= cursos del cliente' AFTER `fecha_actualizacion`;

INSERT INTO `cursos`(`id`, `idexterno`, `idproducto_url`, `idgrupo`, `nombre`, `descripcion`, `tipocurso`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES (66, 0, 1, 0, 'Activador eduktmaestro', NULL, 0, 1, '2020-03-25 09:50:59', '2020-03-25 09:51:26');

INSERT INTO `producto_cursos`(`id`, `idcurso`, `idproducto`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES (66, 66, 5, 1, '2020-03-25 09:53:41', '2020-03-25 09:53:41');

UPDATE productos SET tipodecurso = 2 WHERE id = 5;

/* 06-03-2020 */

ALTER TABLE `orden_servicio_producto` 
ADD COLUMN `ganancia` decimal(10, 2) NULL DEFAULT 70.00 AFTER `precio_curso_libre`;

SET FOREIGN_KEY_CHECKS = 1;
