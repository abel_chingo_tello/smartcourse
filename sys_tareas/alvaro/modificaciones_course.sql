SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------

DROP TABLE IF EXISTS `acad_curso_complementario`;
CREATE TABLE `acad_curso_complementario`  (
  `idcurso` int(1) NOT NULL,
  `nombre` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL,
  `estado` int(1) NOT NULL,
  `fecharegistro` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idusuario` bigint(20) NOT NULL,
  `vinculosaprendizajes` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL,
  `materialesyrecursos` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL,
  `color` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `objetivos` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL,
  `certificacion` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL,
  `costo` float NOT NULL DEFAULT 0,
  `silabo` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL,
  `e_ini` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL,
  `pdf` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `abreviado` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `e_fin` int(11) NULL DEFAULT NULL,
  `aniopublicacion` date NULL DEFAULT '0000-00-00',
  `autor` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT '',
  `txtjson` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `formula_evaluacion` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `idgrupoaula` bigint(20) NULL DEFAULT NULL,
  `tipo` tinyint(4) NULL DEFAULT 2,
  PRIMARY KEY (`idcurso`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `acad_cursodetalle_complementario`;
CREATE TABLE `acad_cursodetalle_complementario`  (
  `idcursodetalle` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `orden` bigint(20) NOT NULL,
  `idrecurso` int(11) NOT NULL,
  `tiporecurso` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Examen, nivel , rubrica,etc',
  `idlogro` int(11) NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `idpadre` smallint(6) NOT NULL,
  `color` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `esfinal` tinyint(4) NULL DEFAULT 0,
  `txtjson` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idcursodetalle`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

ALTER TABLE `acad_curso` 
ADD COLUMN `tipo` tinyint NULL DEFAULT 1 AFTER `formula_evaluacion`;

ALTER TABLE `acad_matricula` 
ADD COLUMN `tipocurso` tinyint NULL DEFAULT 1 AFTER `tipomatricula`;

ALTER TABLE `acad_grupoaula` 
ADD COLUMN `idcategoria` int NULL DEFAULT 0 AFTER `idsesion`;

ALTER TABLE `acad_matricula` 
ADD COLUMN `idcategoria` int NULL DEFAULT 0 AFTER `tipocurso`;

ALTER TABLE `acad_curso_complementario`
ADD COLUMN `idcategoria` int NULL DEFAULT 0 AFTER `idgrupoaula`;

ALTER TABLE `bitacora_alumno_smartbook` 
ADD COLUMN `tipo` tinyint NULL DEFAULT 1 AFTER `oldido`;

ALTER TABLE `bitacora_smartbook` 
ADD COLUMN `tipo` tinyint NULL DEFAULT 1 AFTER `oldidoo`;

ALTER TABLE `acad_curso_complementario` 
ADD COLUMN `idcursoprincipal` bigint NULL DEFAULT 0 AFTER `formula_evaluacion`;

ALTER TABLE `acad_grupoaula` 
ADD COLUMN `idlocal` int NULL DEFAULT 0 AFTER `idcategoria`;

ALTER TABLE `acad_grupoaula` 
ADD COLUMN `fecha_inicio` date NULL AFTER `idlocal`,
ADD COLUMN `fecha_fin` date NULL AFTER `fecha_inicio`;

DROP TABLE IF EXISTS `acad_curso_consulta`;
CREATE TABLE `acad_curso_consulta`  (
  `codigo` bigint(20) NOT NULL AUTO_INCREMENT,
  `idcurso` bigint(20) NOT NULL,
  `idcategoria` bigint(20) NOT NULL,
  `idgrupoaula` bigint(20) NULL DEFAULT NULL,
  `iddetalle` bigint(20) NULL DEFAULT NULL,
  `tipocurso` tinyint(4) NULL DEFAULT 1,
  `idpersona` bigint(20) NOT NULL,
  `contenido` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_hora` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `respuesta` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

ALTER TABLE `acad_curso_consulta` 
ADD COLUMN `likes` int NULL DEFAULT 0 AFTER `idpersona`;

ALTER TABLE `acad_curso_consulta` 
ADD COLUMN `like_de` text NULL AFTER `likes`;

ALTER TABLE `proyecto` 
ADD COLUMN `tipo_portal` tinyint NULL DEFAULT 1 COMMENT '1-eiger,2-campus' AFTER `nombreurl`;

DROP TABLE IF EXISTS `niveles_complementario`;
CREATE TABLE `niveles_complementario`  (
  `idnivel` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tipo` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'N= nivel, U= unidad, L=lesson',
  `idpadre` int(11) NOT NULL,
  `idpersonal` bigint(20) NOT NULL DEFAULT 0,
  `estado` int(11) NOT NULL COMMENT '1 activo , 0 inactivo',
  `orden` int(11) NOT NULL DEFAULT 0,
  `imagen` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `descripcion` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`idnivel`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

ALTER TABLE `acad_matricula` 
CHANGE COLUMN `tipocurso` `idcomplementario` bigint NULL DEFAULT 0 AFTER `tipomatricula`;

UPDATE acad_matricula SET idcomplementario = 0;

ALTER TABLE `bitacora_alumno_smartbook` 
CHANGE COLUMN `tipo` `idcomplementario` bigint NULL DEFAULT 0 AFTER `oldido`;

UPDATE bitacora_alumno_smartbook SET idcomplementario = 0;

ALTER TABLE `bitacora_smartbook` 
CHANGE COLUMN `tipo` `idcomplementario` bigint NULL DEFAULT 0 AFTER `oldidoo`;

UPDATE bitacora_smartbook SET idcomplementario = 0;

/* 17-03-2020 */

  INSERT INTO `modulos`(`idmodulo`, `nombre`, `icono`, `link`, `estado`) VALUES (16, 'Mensajes', 'fa fa-envelope', 'mensajes', 1);

  DROP TABLE IF EXISTS `acad_mensaje`;
  CREATE TABLE `acad_mensaje`  (
    `id` bigint(20) NOT NULL,
    `idpersona` bigint(20) NULL DEFAULT NULL,
    `asunto` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
    `mensaje` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
    `fecha_hora` timestamp NULL DEFAULT NULL,
    `estado` tinyint NULL DEFAULT 1,
    PRIMARY KEY (`id`) USING BTREE
  ) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

  ALTER TABLE .`acad_mensaje` 
  CHANGE COLUMN `idpersona` `de` bigint(20) NULL DEFAULT NULL AFTER `id`;

  DROP TABLE IF EXISTS `acad_mensaje_para`;
  CREATE TABLE `acad_mensaje_para`  (
    `idmensaje` bigint(20) NOT NULL,
    `para` bigint(20) NOT NULL,
    PRIMARY KEY (`idmensaje`, `para`) USING BTREE
  ) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

  ALTER TABLE `acad_mensaje_para` 
  ADD COLUMN `leido` tinyint NULL DEFAULT 0 AFTER `para`;

  INSERT INTO `modulos`(`idmodulo`, `nombre`, `icono`, `link`, `estado`) VALUES (17, 'Foro', 'fa fa-users', 'foros', 1);

  ALTER TABLE `acad_mensaje_para` 
  ADD COLUMN `id` bigint(0) NOT NULL AUTO_INCREMENT FIRST,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`id`) USING BTREE;

  /* 19-03-2020 */

CREATE TABLE IF NOT EXISTS `foro_categorias` (
  `idcategoria` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `estado` INT NOT NULL,
  PRIMARY KEY (`idcategoria`))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `foro_publicacion` (
  `idpublicacion` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(200) NOT NULL,
  `subtitulo` VARCHAR(200) NULL,
  `estado` INT NOT NULL,
  `contenido` MEDIUMTEXT NOT NULL,
  `fecha_hora` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idgrupoauladetalle` INT NULL,
  `idpersona` INT NOT NULL,
  `idcategoria` INT NULL,
  PRIMARY KEY (`idpublicacion`),
  INDEX `fk_foro_publicacion_tmp_usuario1_idx` (`idpersona` ASC),
  INDEX `fk_foro_publicacion_foro_categorias1_idx` (`idcategoria` ASC),
  INDEX `fk_foro_publicacion_tmp_grupo1_idx` (`idgrupoauladetalle` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `foro_comentario` (
  `idcomentario` INT NOT NULL AUTO_INCREMENT,
  `contenido` TEXT NOT NULL,
  `idpublicacion` INT NOT NULL,
  `idcomentariopadre` INT NULL DEFAULT 0,
  `estado` INT NOT NULL,
  `idpersona` INT NOT NULL,
  `fecha_hora` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcomentario`),
  INDEX `fk_comentario_foro_idx` (`idpublicacion` ASC),
  INDEX `fk_foro_comentario_foro_comentario1_idx` (`idcomentariopadre` ASC),
  INDEX `fk_foro_comentario_tmp_usuario1_idx` (`idpersona` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `foro_interaccion` (
  `idforo_interaccion` INT NOT NULL AUTO_INCREMENT,
  `puntuación` INT NULL,
  `idpersona` INT NOT NULL,
  `idpublicacion` INT NOT NULL,
  PRIMARY KEY (`idforo_interaccion`),
  INDEX `fk_foro_interaccion_tmp_usuario1_idx` (`idpersona` ASC),
  INDEX `fk_foro_interaccion_foro_publicacion1_idx` (`idpublicacion` ASC))
ENGINE = MyISAM;

/* 20-03-2020 */

ALTER TABLE `tareas` 
ADD COLUMN `idcomplementario` bigint NULL DEFAULT 0 AFTER `criterios_puntaje`;

/* 21-03-2020 */

DROP TABLE IF EXISTS `valoracion`;
CREATE TABLE `valoracion`  (
  `idvaloracion` bigint(20) NOT NULL,
  `idcurso` bigint(20) NULL DEFAULT NULL,
  `idgrupoaula` bigint(20) NULL DEFAULT NULL,
  `idcomplementario` bigint(20) NULL DEFAULT NULL,
  `idalumno` bigint(20) NULL DEFAULT NULL,
  `puntuacion` int(11) NULL DEFAULT NULL,
  `comentario` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`idvaloracion`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

ALTER TABLE `acad_curso_consulta` 
ADD COLUMN `idcomplementario` bigint NULL DEFAULT 0 AFTER `idcurso`,
ADD COLUMN `idpestania` bigint NULL DEFAULT 0 AFTER `iddetalle`;

/* 22-03-2020 */

ALTER TABLE `modulos` ADD `modulopadre` INT NOT NULL DEFAULT '0' AFTER `estado`;
INSERT INTO `modulos` (`idmodulo`, `nombre`, `icono`, `link`, `estado`, `modulopadre`) VALUES ('19', 'Comunidad', 'fa-comments-o', '#!', '1', '0');
UPDATE `modulos` SET `modulopadre` = '19' WHERE `modulos`.`idmodulo` = 16;
UPDATE `modulos` SET `modulopadre` = '19' WHERE `modulos`.`idmodulo` = 17;
UPDATE `modulos` SET `modulopadre` = '19' WHERE `modulos`.`idmodulo` = 18;
UPDATE `modulos` SET `link` = '' WHERE `modulos`.`idmodulo` = 19;

/* 23-03-2020 */

ALTER TABLE `bolsa_empresas` 
ADD COLUMN `idpersona` bigint NULL DEFAULT 1 COMMENT 'id del representante en la tabla persona' AFTER `tipo_matricula`;

UPDATE bolsa_empresas SET idpersona = 2 WHERE idempresa = 10;
UPDATE bolsa_empresas SET idpersona = 50 WHERE idempresa = 6;

/* 24-03-2020 */

ALTER TABLE `acad_curso` 
ADD COLUMN `sks` tinyint NULL DEFAULT 1 COMMENT '0 - no se registra en sks, 1 - se registra en sks' AFTER `tipo`;

UPDATE acad_curso SET sks = 0 WHERE idcurso = 181;
UPDATE acad_curso SET sks = 0 WHERE idcurso = 197;
UPDATE acad_curso SET sks = 0 WHERE idcurso = 198;
UPDATE acad_curso SET sks = 0 WHERE idcurso = 199;

ALTER TABLE `acad_grupoaula`
  ADD COLUMN `tipoproducto` TINYINT NULL DEFAULT '1' COMMENT '1=grupo de producto en general, 2= grupo de eduktmaestro. Esto funciona a la hora de tener un cliente con los dos productos activado edukt y eduktmaestro' AFTER `fecha_fin`;

ALTER TABLE `acad_grupoaula` 
CHANGE COLUMN `tipoproducto` `idproducto` bigint NULL DEFAULT 1 COMMENT 'idproducto de sks' AFTER `fecha_fin`;

ALTER TABLE `acad_grupoaula` 
ADD COLUMN `tipodecurso` tinyint NULL DEFAULT 1 AFTER `idproducto`;

ALTER TABLE `personal` ADD `emailpadre` VARCHAR(250) NULL AFTER `esdemo`;

ALTER TABLE `acad_grupoauladetalle` 
MODIFY COLUMN `fecha_inicio` date NULL DEFAULT NULL AFTER `nombre`,
MODIFY COLUMN `fecha_final` date NULL DEFAULT NULL AFTER `fecha_inicio`;

/* 30-03-2020 */

DROP TABLE IF EXISTS `familia_foro`;
CREATE TABLE `familia_foro`  (
  `codigo` bigint(20) NOT NULL,
  `persona` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `idproyecto` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `likes` int(11) NULL DEFAULT 0,
  `like_de` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `contenido` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_hora` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `respuesta` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

/* 31-03-2020 */

DROP TABLE IF EXISTS `familia_slider`;
CREATE TABLE `familia_slider`  (
  `codigo` bigint(20) NOT NULL,
  `idpersona` bigint(20) NULL DEFAULT NULL,
  `idproyecto` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_hora` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

INSERT INTO `familia_slider` VALUES (1, 2, 7, 10, '1.jpg', '2020-03-31 08:56:46');
INSERT INTO `familia_slider` VALUES (2, 2, 7, 10, '2.png', '2020-03-31 08:57:10');

INSERT INTO `aceglob4_smart_course`.`modulos`(`idmodulo`, `nombre`, `icono`, `link`, `estado`, `modulopadre`) VALUES (24, 'Familia', 'fa fa-paint-brush', '', 1, 0);
INSERT INTO `aceglob4_smart_course`.`modulos`(`idmodulo`, `nombre`, `icono`, `link`, `estado`, `modulopadre`) VALUES (25, 'Slider', 'fa fa-image', 'familia_slider', 1, 24);
INSERT INTO `aceglob4_smart_course`.`modulos`(`idmodulo`, `nombre`, `icono`, `link`, `estado`, `modulopadre`) VALUES (26, 'Foro', 'fa fa-users', 'familia_foro', 1, 24);

/* 03-04-2020 */
ALTER TABLE `tareas` ADD COLUMN `fecha_creacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `idcomplementario`;

SET FOREIGN_KEY_CHECKS = 1;
