--SMARTQUIZ 20/03/2020

ALTER TABLE `examenes`
	ADD COLUMN `idcc_smartcourse` INT NULL DEFAULT NULL COMMENT 'id del curso complementario del smartcourse' AFTER `nombre_certificado`,
	ADD COLUMN `idpersona_smartcourse` INT NULL DEFAULT NULL COMMENT 'id del curso de la persona del smartcourse' AFTER `idcc_smartcourse`;
--SMARTCOURSE 24/03/2020
ALTER TABLE `acad_grupoaula`
	ADD COLUMN `tipoproducto` TINYINT NULL DEFAULT '1' COMMENT '1=grupo de producto en general, 2= grupo de eduktmaestro. Esto funciona a la hora de tener un cliente con los dos productos activado edukt y eduktmaestro' AFTER `fecha_fin`;

-- SMARTCOURSE 03/04/2020
ALTER TABLE `tareas`
	ADD COLUMN `fecha_creacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `idcomplementario`;
-- SMARTCOURSE 28/04/2020
ALTER TABLE `zoom`
	ADD COLUMN `email` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Email del anfitrión' AFTER `duracion`;

	ALTER TABLE `zoom`
	ADD COLUMN `imagen` VARCHAR(255) NULL DEFAULT NULL COMMENT 'ruta en el static' AFTER `email`,
	ADD COLUMN `idcursodetalle` INT(11) NULL DEFAULT NULL COMMENT 'sesion' AFTER `imagen`,
	ADD COLUMN `idpestania` INT(11) NULL DEFAULT NULL AFTER `idcursodetalle`,
	ADD COLUMN `idcurso` INT(11) NULL DEFAULT NULL AFTER `idpestania`,
	ADD COLUMN `idcomplementario` INT(11) NULL DEFAULT NULL AFTER `idcurso`;