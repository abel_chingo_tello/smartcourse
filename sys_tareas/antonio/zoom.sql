-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 18-03-2020 a las 11:46:34
-- Versión del servidor: 5.6.41-84.1
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aceglob4_smart_english`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zoom`
--

CREATE TABLE `zoom` (
  `idzoom` int(11) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `fecha` datetime NOT NULL,
  `urlmoderador` varchar(300) NOT NULL,
  `urlparticipantes` varchar(1200) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `agenda` text NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `uuid` text NOT NULL,
  `duracion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `zoom`
--
ALTER TABLE `zoom`
  ADD PRIMARY KEY (`idzoom`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
