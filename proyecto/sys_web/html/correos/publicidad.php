<?php
defined('RUTA_BASE') or die();
?>
<style>
table {
border-collapse:collapse;
}

table[class="ecxdeviceWidth"] {
width:600px;
max-width:600px;
}

table[class="ecxbtn"] {
font-size:21px;
}

table[class="ecxWidthTableInt_I"] {
max-width:425px !important;
width:100% !important;
text-align:left !important;
}

table[class="ecxWidthTableInt_D"] {
width:100% !important;
max-width:90px !important;
text-align:right !important;
}

@media only screen and (max-width: 600px), screen and (max-device-width: 600px) {
table[class="ecxdeviceWidth"] {
width:440px !important;
padding:0 !important;
}
table[class="ecxbtn"] {
font-size:18px !important;
}
table[class="ecxWidthTableInt_I"] {
width:440px !important;
}
table[class="ecxWidthTableInt_D"] {
width:440px !important;
text-align:left !important;
}
}

@media only screen and (max-width: 479px), screen and (max-device-width: 479px) {
table[class="ecxdeviceWidth"] {
width:280px !important;
padding:0 !important;
}
table[class="ecxWidthTableInt_I"] {
width:280px !important;
text-align:left !important;
}
table[class="ecxWidthTableInt_D"] {
width:280px !important;
text-align:left !important;
}
}
</style>

<table style="  width: 100% !important;  table-layout: fixed; background: #fff;" align="center">
    <caption><h1 style="text-align: center;">Correo de publicidad EduktVirtual</h1></caption>
    <tbody>
        <tr>
            <td>
                <table class="ecxdeviceWidth" style="max-width:600px;" align="center">
                <tbody>
                    <tr><td colspan="3" style="
    background: #bb5b21;
    color: #f9f3f3;
    text-align: center;
    padding: 2ex;
    /* padding: 6ex; */
">Tenemos muchos cursos para ti visitanos en <a href="http://eduktvirtual.com" style="color: #ffe520;"><b>Cursos virtuales</b></a></td></tr>
                <tr>
                    <?php 
                    $i=0;
                    if(!empty($this->cursos))
                        foreach ($this->cursos as $cur){ $i++;?>
                           <td><a href="http://eduktvirtual.com" style="color: #000000;"><img src="<?php echo $this->documento->getUrlBase().'/../alumno/images/'.$cur["idcurso"];?>.png" style=""><div style="text-align: center"><b><?php echo $cur["nombre"];?></b></div></a></td>
                        <?php
                         if($i==3){
                            echo '</tr><tr>';
                            $i=0;
                         }

                         } ?>
                    <td style="padding-bottom:8px;">
                        <table style="color:#353e4a;font-family:Arial,sans-serif;font-size:15px;" align="center" bgcolor="#ffffff" border="0">
                            <tbody>
                                <tr>
                                <td style="color:#1e80b6;padding-top:20px;padding-bottom:10px;padding-left:20px;padding-right:20px;">
                                    <?php echo $this->pasarHtml($this->para)?></td>
                                </tr>
                                <tr>
                                    <td style="line-height:24px;padding-left:20px;padding-right:20px;padding-bottom:10px;">
                                        <?php echo $this->mensaje /*$this->pasarHtml($this->mensaje)*/ ; ?>
                                  	</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr style=" background: #737070;color: #fff;"><td colspan="3">
                    <h2>Necesitas Ayuda?</h2>
                    Escribenos a nuestro correo <b>info@eduktvirtual.com</b><br>
                    Llámanos al <b>+51 1-435-3738</b><br>
                    Contáctanos en Whatsapp al <b>+51 960-177-900</b>
                </td>
                </tr>
                <tr>                    
                    <td colspan="3" style="color: #717175;font-size: 12px;padding: 1ex;text-align: center;background: #ecece9;">
                    <?php echo $this->pasarHtml(JrTexto::_('Este email se ha generado automáticamente. Por favor, no contestes a este email'))?>.</td>
                               
                </tr>
            </tbody>
          	</table>
            </td>
        </tr>
    </tbody>
</table>