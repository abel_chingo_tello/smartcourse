<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;
?>
<style>
     .btn-categoria{
        border-bottom: 1px solid;
     }
     .btn-categoria:last-child{
        border-bottom: none;
     }
 </style>
 <!-- ===============  HOME    =================== -->
 <div class="home-container svg-home">
     <div class="mask-bg"></div>
     <ul class="sidebar-menu">
         <li class="sidebar-menu-item">
             <a href="" class="bluedark"><span class="icon icon-graduation bg-bluedark"></span> <b><?php echo JrTexto::_('Courses') ?></b></a>
         </li>
     </ul>
     <div class="container-cursos">
        <?php if(!empty($this->categorias)): ?>
            <?php foreach ($this->categorias as $k=> $v): ?>
                <?php if(empty($v["estado"])) continue; ?>
                <?php if(!empty($v["tienecursos"])):?>
                    <button class="accordion btn-categoria"><?= $v["nombre"]; ?></button>
                    <div class="panel">
                        <?php if(!empty($v["hijos"])) 
                         foreach($v["hijos"] as $ks=> $vs): ?>
                            <?php if(empty($vs["estado"])) continue; ?>
                            <?php if(!empty($vs["cursos"])): ?>
                                <button class="accordion accordion-child-bg"><?=$vs["nombre"]?></button>
                                <div class="panel">
                                    <?php foreach($vs["cursos"] as $kcur =>$cur): ?>
                                        <?php
                                            $haycursosamostrar=true ;
                                            $img=$cur["imagen"];
                                            $posimg=strripos($img, 'static/');
                                            if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                                            else $img=URL_BASE.substr($img, $posimg);
                                            $_tipo = empty($cur['tipo']) ? 1 : $cur['tipo'];
                                            $_idcomplementario = empty($cur['idcomplementario']) ? 0 : $cur['idcomplementario'];
                                        ?>
                                        <a href="<?=URL_BASE?>smart/cursos/ver/?idcurso=<?=$cur['idcurso']?>&icc=<?=$_idcomplementario;?>&idgrupoauladetalle=<?=@$cur['idgrupoauladetalle'];?>&idmatricula=<?=@$cur["idmatricula"];?>" target="_blank" class="course-container">
                                            <div class="course-desc">
                                                <p class="course-txt-title"><?php echo !empty($cur['strcurso'])?$cur['strcurso']:@$cur["nombre"]?></p>
                                                <div class="course-separator"></div>
                                                <?php if(!empty($cur["strgrupoaula"])){?>
                                                <p class="course-txt-desc">
                                                    <?=JrTexto::_("Group")?>: <?=@$cur["strgrupoaula"]?>
                                                </p>
                                                <?php } ?>
                                            </div>
                                            <div class="course-img">
                                                <img class="img-course" src="<?=@$img?>" alt="">
                                            </div>
                                            
                                        </a>
                                    <?php endforeach;?>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            <?php endforeach;?>
        <?php endif; ?> 
        <!--END CATEGORIAS-->
        <!--START CURSOS LIBRE-->
        <?php if(!empty($this->haycursoslibres)): ?>
            <button class="accordion btn-categoria"><?php echo JrTexto::_('Free Courses'); ?></button>
            <div class="panel">
                <?php foreach($this->haycursoslibres as $kl => $cur): ?>
                    <?php
                        $haycursosamostrar=true;
                        $img=$cur["imagen"];
                        $posimg=strripos($img, 'static/');
                        if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                        else $img=URL_BASE.substr($img, $posimg);    
                        $_tipo = empty($cur['tipo']) ? 1 : $cur['tipo'];
                        $_idcomplementario = empty($cur['idcomplementario']) ? 0 : $cur['idcomplementario'];
                    ?>
                    <a href="<?=URL_BASE?>smart/cursos/ver/?idcurso=<?=$cur['idcurso']?>&icc=<?=$_idcomplementario;?>&idgrupoauladetalle=<?=@$cur['idgrupoauladetalle'];?>&idmatricula=<?=@$cur["idmatricula"];?>" target="_blank" class="course-container">
                        <div class="course-desc">
                            <p class="course-txt-title"><?=$cur['strcurso']?></p>
                            <div class="course-separator"></div>
                            <?php if(!empty($cur["strgrupoaula"])){?>
                            <p class="course-txt-desc">                                
                                <?=JrTexto::_("Group")?>: <?=$cur["strgrupoaula"]?>
                            </p>
                            <?php } ?>
                        </div>
                        <div class="course-img">
                            <img class="img-course" src="<?=$img?>" alt="">
                        </div>
                        
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php if(@$haycursosamostrar==false): ?>
            <div class="col-md-12">
                <div style="text-align: center; font-size: 2em;
                    position: relative;
                    width: 100%;
                    background-color: white;
                    height: 100%;">
                        <br><br>
                        <i class="fa fa-info" style="font-size: 3em"></i><br><br>
                        <?php echo ucfirst(JrTexto::_('You have no courses currently assigned')); ?>.
                    </div>
            </div>
            <div class="clearfix"></div>
        <?php endif; ?>
     </div>
 </div>