<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;
?>
<!--div class="row justify-content-end">
	<div class="col-sm-12 text-right">
		<div class="btn-group" role="group" aria-label="Basic example">
		  <button type="button" id="btn-agregar" class="btn btn-success" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/crear" >
		  	<i class="fa fa-plus"></i> <?php echo JrTexto::_('New course');?>
		  </button>
		  <?php if($this->curusuario["tuser"]=='s'){?>
		  <button type="button" id="btn-asignar" class="btn btn-warning" alt="<?php echo JrTexto::_('Order');?>" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/asignar"><i class="fa fa-location-arrow"></i> <?php echo ucfirst(JrTexto::_('Add'))." ".ucfirst(JrTexto::_('Course'));?> </button>
			<?php } ?>
		  <button type="button" id="btn-ordenar" class="btn btn-secondary" alt="<?php echo ucfirst(JrTexto::_('Order'));?>" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/ordenar"><i class="fa fa-sort-amount-desc"></i> <?php echo ucfirst(JrTexto::_('Order'));?> </button>
		</div>
	</div>
</div-->

<div class="card card-secondary">
	<div class="card-header">
		<h4 class="card-title"><i class="fa fa-location-arrow"></i> Asignación de cursos</h4>
	</div>
	<div class="card-body" id="listadoasignarcursos">
		<div class="row">
			<?php if(!empty($this->datos)){
				foreach ($this->datos as $key => $cur){
					$img=!empty($cur["imagen"])?$cur["imagen"]:$imgdefecto;
					$ipost=stripos($img, 'static/');
					if($ipost!==false){
						$img=substr($img, $ipost);
					}

					$img=URL_BASE.$img;
					/*if (is_readable(RUTA_BASE.$img)) $img=URL_BASE.$img;
					else $img=URL_BASE.$imgdefecto;*/

					?>
					<div class="col-md-3 col-sm-4 col-xs-12 col-12"> <div class="card card-outline card-primary" id="card<?php echo $cur["idcurso"]; ?>">
				          <div class="card-header">
				            <h4 class="card-title"><?php echo !empty($cur["nombre"])?$cur["nombre"]:'Sin Nombre'; ?></h4>
				            <!--div class="card-tools">
				              <button type="button" class="btn btn-tool text-secondary" data-card-widget="collapse"><i class="fa fa-minus"></i>
				              </button>
				            </div-->
				          </div>
				          <div class="card-body" style="display: block;">
							<div class="image view view-first item-curso" idcurso="<?php echo $cur["idcurso"]; ?>"  >
								<img class="img-curso" src="<?php echo $img; ?>" />
								<div class="mask btnvercurso">
									<div class="tools tools-bottom">									
										<a class="a-mask btnasignar" data-toggle="popover" data-content="Agregar o asigna éste curso a la empresa"><i class="fa fa-location-arrow"></i> <br> <?php echo ucfirst(JrTexto::_('Add'));?></a>
										<a class="a-mask btneliminar" data-toggle="popover" data-content="Elimina el curso de todas las empresas"><i class="fa fa-trash"></i> <br> <?php echo ucfirst(JrTexto::_('Delete'));?></a>
									</div>
									<div><h4 ><?php echo !empty($cur["nombre"])?$cur["nombre"]:'Sin Nombre'; ?></h4></div>
								</div>
			          		</div>
						</div>
						<!--div class="card-footer">
							<label for="">${el.strgrupoaula}</label>
						</div-->
					</div></div>
				<?php } ?>
			<?php }else{?>

			<?php }?> 
		</div>

		<?php
		 //var_dump($this->datos);
		 ?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover({
		    container: 'body',
		    trigger:'hover',
		    placement:'bottom'
		})
		$('#listadoasignarcursos').on('click','.btnasignar',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			Swal.showLoading();
			var el=$(this).closest('.item-curso');
			var id=el.attr('idcurso');
		    var tipo=1
		    var sks=$(this).attr('sks');
		    var data=new FormData()
		    data.append('idcurso',id);
		    data.append('tipo',tipo);
		    data.append('sks',1);		    
		    __sysAyax({ 
		      fromdata:data,
		        url:_sysUrlBase_+'json/proyecto_cursos/guardar',
		        callback:function(data){
			        Swal.close();
					if(data.code == 200){
						el.closest('.card').parent().remove();
						Swal.fire({								
							text: data.msj||'curso Asignado Correctamente a esta empresa',
							type: 'success',								
							//confirmButtonText: '<?php echo JrTexto::_('Aceptar');?>',								
						})
					}else{
						Swal.fire({								
							text: data.msj||'Error Inesperado inténtelo mas tarde',
							type: 'warning',								
							//confirmButtonText: '<?php echo JrTexto::_('Aceptar');?>',								
						})
					}
		        }
		    });

		}).on('click','.btneliminar',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			var el=$(this).closest('.item-curso');
			var id=el.attr('idcurso');			
			Swal.fire({
				title: '<?php echo JrTexto::_('¿Seguro de eliminar curso?');?>',
				text: '<?php echo JrTexto::_('También se eliminará todo lo relacionado al mismo'); ?>',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: '<?php echo JrTexto::_('Aceptar');?>',
				cancelButtonText: '<?php echo JrTexto::_('Cancelar');?>',
			}).then((result) => {
				if (result.value){
					Swal.showLoading();
					// console.log($(this).closest(".visorCursos").parent())
					$.post(_sysUrlBase_ + 'json/acad_curso/eliminar',{
						'idcurso': id,
						'tipo': 1,
						'eliminar': 'si',
						'idcc':0
					}, function(data){						
						Swal.close();
						if(data.code == 200){
							el.closest('.card').parent().remove();
						}else{
							Swal.fire({								
								text: data.msj||'Error Inesperado inténtelo mas tarde',
								type: 'warning',								
								confirmButtonText: '<?php echo JrTexto::_('Aceptar');?>',
							})
						}
					}, 'json');
				}
			});
		}).on('click','.btnvercurso',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			var el=$(this).closest('.item-curso');
			var id=el.attr('idcurso');
			window.open(_sysUrlBase_+'smart/cursos/ver/?idcurso='+id+'&icc=0&tipo=1', '_blank');
		})
	})
</script>