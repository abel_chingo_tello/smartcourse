<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;
?>
<style type="text/css">
    .thumbnail{
        height: auto !important;
        /* padding: 1em; */
        padding: 0 !important;
    }
    .view-first{
        height: 100% !important;
    }
    .img-curso{
        width: 100% !important;
        display: block !important;
    }
    .img-curso{
        text-align: center;
        margin: auto;
        min-height: 115px;
        min-width: 115px;
    }
    .mask{
        height: 100% !important;
    }
    .tools-bottom{
        background-color: #464646 !important;
        margin: 0 !important;
    }
    .categoria_ {
        font-weight: bold;
        margin: 0px;
        background: #a1e5f0;
        padding: 1ex 1ex 1ex 2ex;
        font-size: x-large;
        font-family: initial;
    }
    .subcategoria_{
        transform: rotate(-90deg);
        -webkit-transform: rotate(-90deg);
        text-align: center;
        vertical-align: middle !important;
        font-size: 20px;
        font-weight: bold;
        padding: 0 !important;
        max-width: 120px;
    }
    .subcategoria2_{     
        font-size: 20px;
        font-weight: bold;    
        padding-left: 2.5em !important;
        display: none;
    }

    .curso-lock{
        opacity: 1 !important;
    }
    .curso-lock > i{
        color: white;
        font-size: 45px;
        padding-top: 30%;
    }
    .item-curso{
        cursor: pointer !important; 
    }
    .titulo-p{
        float: none !important;
        font-size: 24px !important;
        font-weight: bold !important;
        text-align: center !important;
        width: 100% !important;
    }
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="titulo-p-2"><?php echo JrTexto::_('My Courses');?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    <button type="button" id="btncambiarvista" class="btn btn-success" data-toggle="popover" title="<?php echo JrTexto::_('Change view'); ?>" data-content="<?php echo JrTexto::_('Change modules or subcategories from vertical to horizontal'); ?>"> <i class="fa fa-toggle-on" title="" ></i> </button>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style=" padding: 1.5ex;">            
                <div class="row" id="lscursos">

               <div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                <?php 
                $bloquear=false;
                $ncursos=0;
                $aprovo=false;
                $open=true;
                $haycursosamostrar=false;
                //var_dump($this->categorias);
                if(!empty($this->categorias)){
                foreach ($this->categorias as $k=> $v){ 
                    if(empty($v["estado"])) continue;                  
                     if(!empty($v["tienecursos"])){ 
                        ?>
                        <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="headingThree<?php echo $k; ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree<?php echo $k; ?>" aria-expanded="<?php echo $open==true?'true':'false'; ?>" aria-controls="collapseThree" style="font-family: Arial; padding: 0px; color:#111;">
                              <div class="panel-title categoria_"><i class="fa fa-angle-right" ></i> <?php echo $v["nombre"]; ?></div>
                            </a>
                        <div id="collapseThree<?php echo $k; ?>" class="panel-collapse collapse <?php echo $open==true?'show':''; ?>" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body" style="border: 0px;">
                        <table class="table table-striped">                           
                            <tbody>
                        <?php 
                        $open=false;
                            foreach ($v["hijos"] as $ks=> $vs){ 
                                if(empty($vs["estado"])) continue;
                                if(!empty($vs["cursos"]) ){ ?>
                                    <tr><th class="subcategoria2_" colspan="2"><?php echo $vs["nombre"]; ?></th></tr>
                                <tr>
                                    <td class="subcategoria_"><?php echo $vs["nombre"]; ?></td>
                                    <td>
                                        <div class="row cursosdecategoria">                            
                                        <?php 
                                        foreach($vs["cursos"] as $kcur =>$cur){ $haycursosamostrar=true ;
                                            $img=$cur["imagen"];
                                            $posimg=strripos($img, 'static/');
                                            if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                                            else $img=URL_BASE.substr($img, $posimg);                                            
                                            ?>
                                            <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                                                <div class="thumbnail">
                                                    <div class="image view view-first item-curso" idgrupoauladetalle="<?php echo @$cur['idgrupoauladetalle'];?>" idcurso="<?php echo $cur['idcurso'];?>" idmatricula="<?php echo @$cur["idmatricula"]; ?>" idcomplementario="<?php echo @$cur['idcomplementario'];?>" tipo="<?php echo @$cur['tipo'];?>" idcategoria="<?php echo @$cur['idcategoria'];?>">
                                                        <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $cur['strcurso'];?>" />
                                                        <div style="font-size: 11px; margin-top: 0px;  "><?php echo $cur["strgrupoaula"]."<br>".$cur["strcurso"]; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        </div>
                                    </td>
                                </tr>
                        <?php }
                    }
                    if(!empty($v["cursos"]) ){ ?>
                                <tr>                                    
                                    <td colspan="2">
                                        <div class="row cursosdecategoria">                            
                                        <?php 
                                        foreach($v["cursos"] as $kcur =>$cur){
                                            $img=$cur["imagen"];
                                            $posimg=strripos($img, 'static/');
                                            if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                                            else $img=URL_BASE.substr($img, $posimg);
                                            $haycursosamostrar=true;
                                            ?>
                                            <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                                                <div class="thumbnail">
                                                    <div class="image view view-first item-curso" idgrupoauladetalle="<?php echo @$cur['idgrupoauladetalle'];?>"  idcurso="<?php echo $cur['idcurso'];?>" idmatricula="<?php echo @$cur["idmatricula"]; ?>" idcomplementario="<?php echo @$cur['idcomplementario'];?>" tipo="<?php echo @$cur['tipo'];?>" idcategoria="<?php echo @$cur['idcategoria'];?>" >
                                                        <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $cur['strcurso'];?>" />                                                         
                                                        <div style="font-size: 11px; margin-top: 0px;  "><?php echo $cur["strgrupoaula"]."<br>".$cur["strcurso"]; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        </div>
                                    </td>
                                </tr>
                        <?php }

                     ?>
                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                    <?php } ?>
                <?php }
                }
                if(!empty($this->haycursoslibres)){ ?>
                    <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="headingThreea" data-toggle="collapse" data-parent="#accordion1" href="#collapseThreea" aria-expanded="<?php echo $open==true?'true':'false'; ?>" aria-controls="collapseThree" style="font-family: Arial; padding: 0px; color:#111;">
                              <div class="panel-title categoria_"><i class="fa fa-angle-right" ></i> <?php echo JrTexto::_('Free Courses'); ?></div>
                            </a>
                        <div id="collapseThreea" class="panel-collapse collapse <?php echo $open==true?'show':''; ?>" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body" style="border: 0px;">
                            <div class="row">
                    <?php  
                    $open=false;
                    foreach ($this->haycursoslibres as $kl => $cur){ $haycursosamostrar=true; 
                         $img=$cur["imagen"];
                         $posimg=strripos($img, 'static/');
                         if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                         else $img=URL_BASE.substr($img, $posimg);

                        ?>                            
                        <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                            <div class="thumbnail">
                                <div class="image view view-first item-curso"  idgrupoauladetalle="<?php echo @$cur['idgrupoauladetalle'];?>"    idcurso="<?php echo $cur['idcurso'];?>" idmatricula="<?php echo @$cur["idmatricula"]; ?>" idcomplementario="<?php echo @$cur['idcomplementario'];?>" tipo="<?php echo @$cur['tipo'];?>" style="padding: 1ex" idcategoria="<?php echo @$cur['idcategoria'];?>" >
                                    <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $cur['strcurso'];?>" />
                                    <div style="font-size: 11px; margin-top: 0px;  "><?php echo $cur["strgrupoaula"]."<br>".$cur["strcurso"]; ?></div>
                                </div>
                            </div>
                        </div>    
                        <?php }  ?>                           
                        </div>
                        </div>
                        </div>
                    </div>
                <?php }
                if($haycursosamostrar==false){ ?>
                    <div class="col-md-12">
                        <div style="text-align: center; font-size: 2em;
                            position: relative;
                            width: 100%;
                            background-color: white;
                            height: 100%;">
                                <br><br>
                                <i class="fa fa-info" style="font-size: 3em"></i><br><br>
                                <?php echo ucfirst(JrTexto::_('You have no courses currently assigned')); ?>.
                            </div>
                    </div>
                    <div class="clearfix"></div>
                  <?php } ?>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        container: 'body',
        trigger:'hover',
        placement:'bottom'
    })
    $('#lscursos').on('click','.item-curso',function(ev){
        ev.preventDefault();
        var id=$(this).attr('idcurso');
        var idcc='&icc='+($(this).attr('idcomplementario')||0);
        var idgrupoauladetalle='&idgrupoauladetalle='+($(this).attr('idgrupoauladetalle')||0);
        var idmatricula='&idmatricula='+($(this).attr('idmatricula')||'');
        window.open(_sysUrlBase_+'smart/cursos/ver/?idcurso='+id+idcc+idgrupoauladetalle+idmatricula, '_blank');
    }) 
    $('.accordion > .panel > .panel-heading').click(function(ev){
        if($(this).hasClass('collapsed')){ // se abre 
            let panel=$(this).parent().siblings('.panel');
            panel.children('.panel-heading').addClass('collapsed');
            panel.children('.show').removeClass('show').addClass('collapsed');
        }
    }) 
    $('#btncambiarvista').click(function(ev){
        if($(this).children('i').hasClass('fa-toggle-on')){
            $('#btncambiarvista').children('i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
            $('.subcategoria2_').show();
            $('.subcategoria_').hide(); 
            localStorage.setItem('mostrarmodulosverical',true);   
        }else{
            $('#btncambiarvista').children('i').removeClass('fa-toggle-off').addClass('fa-toggle-on');
            $('.subcategoria_').show();
            $('.subcategoria2_').hide();
            localStorage.setItem('mostrarmodulosverical',false);
        }
    })
    var mostrarmodulosverical=localStorage.getItem('mostrarmodulosverical')||false;
    if(mostrarmodulosverical=='true'){
        $('#btncambiarvista').children('i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
        $('.subcategoria2_').show();
        $('.subcategoria_').hide();
    }
});
</script>