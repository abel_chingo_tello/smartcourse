<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;
?>
<style type="text/css">
    .thumbnail{
        height: auto !important;
        /* padding: 1em; */
        padding: 0 !important;
    }
    .view-first{
        height: 100% !important;
    }
    .img-curso{
        width: 100% !important;
        display: block !important;
        text-align: center;
        cursor: pointer !important; 
    }
    .img-curso img{
        text-align: center;
        margin: auto;
    }
    .mask{
        height: 100% !important;
    }
    .tools-bottom{
        background-color: #464646 !important;
        margin: 0 !important;
    }
    .categoria_ {
        font-weight: bold;
        margin: 0px;
        background: #a1e5f0;
        padding: 1ex 1ex 1ex 2ex;
        font-size: x-large;
        font-family: initial;
    }
    .subcategoria_{
        transform: rotate(-90deg);
        -webkit-transform: rotate(-90deg);
        text-align: center;
        vertical-align: middle !important;
        font-size: 20px;
        font-weight: bold;
        padding: 0 !important;
        max-width: 130px;
    }
    .curso-lock{
        opacity: 1 !important;
    }
    .curso-lock > i{
        color: white;
        font-size: 45px;
        padding-top: 30%;
    }   
    .titulo-p{
        float: none !important;
        font-size: 24px !important;
        font-weight: bold !important;
        text-align: center !important;
        width: 100% !important;
    }
    .tools a { cursor: pointer; } 
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="titulo-p-2"><?php echo JrTexto::_('My Courses');?> <?php if(@$this->solocursosprincipales!=2){?> <small style="cursor: pointer" class="showasignados">
                    <i class="fa fa-check-square"></i> Solo mostrar cursos asignados
                </small><?php } ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    <button id="btn-agregar" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/crear" type="button" class="btn btn-success">
                        <i class="fa fa-plus"></i> <?php echo JrTexto::_('New course');?>
                    </button>
                    <button id="btn-ordenar" title="ordenar" alt="ordenar" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/ordenar" type="button" class="btn btn-secondary">
                        <i class="fa fa-sort-amount-desc"></i>
                    </button>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
          
            <div class="x_content" style=" padding: 1.5ex;">            
                <div class="row" id="lscursos">

               <div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                <?php 
                $bloquear=false;
                $ncursos=0;
                $aprovo=false;
                $open=true;
                $haycursosamostrar=false;
                //var_dump($this->categorias);
                if(!empty($this->categorias)){
                foreach ($this->categorias as $k=> $v){
                     if(!empty($v["tienecursos"])){ 
                        $haycursosamostrar=true;
                       // var_dump($v)
                        ?>
                        <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="headingThree<?php echo $k; ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree<?php echo $k; ?>" aria-expanded="<?php echo $open==true?'true':'false'; ?>" aria-controls="collapseThree" style="font-family: Arial; padding: 0px; color:#111;">
                              <div class="panel-title categoria_"><i class="fa fa-angle-right" ></i> <?php echo $v["nombre"]; ?></div>
                            </a>
                        <div id="collapseThree<?php echo $k; ?>" class="panel-collapse collapse <?php echo $open==true?'show':''; ?>" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body" style="border: 0px;">
                        <table class="table table-striped">                           
                            <tbody>
                        <?php 
                        $open=false;
                            foreach ($v["hijos"] as $ks=> $vs){   
                                if(!empty($vs["cursos"]) ){ ?>
                                <tr>
                                    <td class="subcategoria_"><?php echo $vs["nombre"]; ?></td>
                                    <td>
                                        <div class="row cursosdecategoria">                            
                                        <?php 
                                        foreach($vs["cursos"] as $kcur =>$cur){
                                            $img=$cur["imagen"];
                                            $posimg=strripos($img, 'static/');
                                            if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                                            else $img=URL_BASE.substr($img, $posimg);
                                            $haycursosamostrar=true;
                                            $lock=true;
                                            if(!empty($this->cursodesbloqueados[$this->curusuario["idpersona"]]))
                                                if(in_array($cur["idcurso"], $this->cursodesbloqueados[$this->curusuario["idpersona"]])) $lock=false;                                            
                                            ?>
                                            <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                                                <div class="thumbnail">
                                                    <div class="image view view-first item-curso <?php echo $lock==true?'lock':''; ?>" idcurso="<?php echo $cur['idcurso'];?>" idcomplementario="<?php echo @$cur['idcomplementario'];?>" tipo="<?php echo @$cur['tipo'];?>" idcategoria="<?php echo @$cur['idcategoria'];?>">
                                                        <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $cur['nombre'];?>" />
                                                            <!--div class="mask">
                                                                <div class="tools tools-bottom" >
                                                                <?php if(@$kv['tipo']==2||$this->curusuario["tuser"]=='s'){?>
                                                                    <a class="a-mask edit" title="Editar curso"><i class="fa fa-pencil"></i></a>
                                                                <?php }?>
                                                                <?php if(@$kv['idcomplementario']==0){?>
                                                                    <a class="a-mask mirarcategoria"  title="Categoria curso"><i class="fa fa-cc"></i></a>
                                                                <?php } ?>
                                                                <a class="a-mask formular"  title="Formula evaluación"><i class="fa fa-calculator"></i></a>
                                                                <?php if(@$kv['tipo']==2||$this->curusuario["tuser"]=='s'){?>
                                                                <a class="a-mask remove" title="Eliminar curso"><i class="fa fa-trash"></i></a>
                                                                <?php } ?>
                                                                <?php if($this->curusuario["tuser"]=='s'){?>
                                                                    <a class="a-mask copy" title="Copiar curso"><i class="fa fa-copy"></i></a>
                                                                <?php }?>
                                                                </div>
                                                            </div-->
                                                            <?php if(!empty($lock)){ ?><div class="mask curso-lock"><i class="fa fa-lock"></i></div><?php } ?>
                                                        <div style="font-size: 11px; margin-top: 0px;   text-transform: capitalize;"  id="nombrecurso"><?php echo $cur["nombre"]; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        </div>
                                    </td>
                                </tr>
                        <?php }
                    }
                    if(!empty($v["cursos"]) ){ ?>
                                <tr>                                    
                                    <td colspan="2">
                                        <div class="row cursosdecategoria">                            
                                        <?php 
                                        foreach($v["cursos"] as $kcur =>$cur){
                                            $img=$cur["imagen"];
                                            $posimg=strripos($img, 'static/');
                                            if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                                            else $img=URL_BASE.substr($img, $posimg);
                                            $haycursosamostrar=true;
                                            $lock=true;
                                            if(!empty($this->cursodesbloqueados[$this->curusuario["idpersona"]]))
                                                if(in_array($cur["idcurso"], $this->cursodesbloqueados[$this->curusuario["idpersona"]])) $lock=false;
                                            ?>
                                            <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                                                <div class="thumbnail">
                                                    <div class="image view view-first item-curso <?php echo $lock==true?'lock':''; ?>" idcurso="<?php echo $cur['idcurso'];?>" idcomplementario="<?php echo @$cur['idcomplementario'];?>" tipo="<?php echo @$cur['tipo'];?>" idcategoria="<?php echo @$cur['idcategoria'];?>">
                                                        <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $cur['nombre'];?>" />
                                                        <!--div class="mask">
                                                            <div class="tools tools-bottom" >
                                                                <?php if(@$kv['tipo']==2||$this->curusuario["tuser"]=='s'){?>
                                                                    <a class="a-mask edit" title="Editar curso"><i class="fa fa-pencil"></i></a>
                                                                <?php }?>
                                                                <?php if(@$kv['idcomplementario']==0){?>
                                                                    <a class="a-mask mirarcategoria"  title="Categoria curso"><i class="fa fa-cc"></i></a>
                                                                <?php } ?>
                                                                <a class="a-mask formular"  title="Formula evaluación"><i class="fa fa-calculator"></i></a>
                                                                <?php if(@$kv['tipo']==2||$this->curusuario["tuser"]=='s'){?>
                                                                <a class="a-mask remove" title="Eliminar curso"><i class="fa fa-trash"></i></a>
                                                                <?php } ?>
                                                                <?php if($this->curusuario["tuser"]=='s'){?>
                                                                    <a class="a-mask copy" title="Copiar curso"><i class="fa fa-copy"></i></a>
                                                                <?php }?>
                                                            </div>
                                                        </div-->
                                                        <?php if(!empty($lock)){ ?><div class="mask curso-lock"><i class="fa fa-lock"></i></div><?php } ?>
                                                        <div style="font-size: 11px; margin-top: 0px;   text-transform: capitalize;" id="nombrecurso"><?php echo $cur["nombre"]; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        </div>
                                    </td>
                                </tr>
                        <?php }

                     ?>
                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                    <?php } ?>
                <?php }
                }
                if(!empty($this->haycursoslibres)){ ?>
                    <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="headingThreea" data-toggle="collapse" data-parent="#accordion1" href="#collapseThreea" aria-expanded="<?php echo $open==true?'true':'false'; ?>" aria-controls="collapseThree" style="font-family: Arial; padding: 0px; color:#111;">
                              <div class="panel-title categoria_"><i class="fa fa-angle-right" ></i> Cursos Libres</div>
                            </a>
                        <div id="collapseThreea" class="panel-collapse collapse <?php echo $open==true?'show':''; ?>" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body" style="border: 0px;">
                            <div class="row">
                    <?php  
                    $open=false;
                    foreach ($this->haycursoslibres as $kl => $kv){ $haycursosamostrar=true; 
                         $img=$kv["imagen"];
                         $posimg=strripos($img, 'static/');
                         if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                         else $img=URL_BASE.substr($img, $posimg);
                         $lock=true;
                        if(!empty($this->cursodesbloqueados[$this->curusuario["idpersona"]]))
                            if(in_array($kv["idcurso"], $this->cursodesbloqueados[$this->curusuario["idpersona"]])) $lock=false;

                        ?>                            
                        <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                            <div class="thumbnail">
                                <div class="image view view-first item-curso <?php echo $lock==true?'lock':''; ?>"  idcurso="<?php echo $kv['idcurso'];?>" idcomplementario="<?php echo @$kv['idcomplementario'];?>" tipo="<?php echo @$kv['tipo'];?>" style="padding: 1ex"  idcategoria="<?php echo @$kv['idcategoria'];?>">
                                    <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $kv['nombre'];?>" />
                                    <!--div class="mask">
                                        <div class="tools tools-bottom" >
                                           <?php if(@$kv['tipo']==2||$this->curusuario["tuser"]=='s'){?>
                                                <a class="a-mask edit" title="Editar curso"><i class="fa fa-pencil"></i></a>
                                            <?php }?>
                                            <?php if(@$kv['idcomplementario']==0){?>
                                                <a class="a-mask mirarcategoria"  title="Categoria curso"><i class="fa fa-cc"></i></a>
                                            <?php } ?>
                                            <a class="a-mask formular"  title="Formula evaluación"><i class="fa fa-calculator"></i></a>
                                            <?php if(@$kv['tipo']==2||$this->curusuario["tuser"]=='s'){?>
                                            <a class="a-mask remove" title="Eliminar curso"><i class="fa fa-trash"></i></a>
                                            <?php } ?>
                                            <?php if($this->curusuario["tuser"]=='s'){?>
                                                <a class="a-mask copy" title="Copiar curso"><i class="fa fa-copy"></i></a>
                                            <?php }?>
                                        </div>
                                    </div-->
                                    <?php if(!empty($lock)){ ?><div class="mask curso-lock"><i class="fa fa-lock"></i></div><?php } ?>
                                    <div style="font-size: 11px; margin-top: 0px;   text-transform: capitalize;"  id="nombrecurso" ><?php echo $kv["nombre"]; ?></div>
                                </div>
                            </div>
                        </div>    
                        <?php }  ?>                           
                        </div>
                        </div>
                        </div>
                    </div>
                <?php }
                if($haycursosamostrar==false){ ?>
                    <div class="row">
                        <div style="text-align: center; font-size: 2em;
                            /*position: absolute;*/
                            width: 100%;
                            background-color: white;
                            height: 100%;">
                                <br><br>
                                <i class="fa fa-info" style="font-size: 3em"></i><br><br>
                                No tiene cursos actualmente.
                            </div>
                    </div>
                  <?php } ?>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="display: none">
    <div id="shopnlcategorias"> 
        <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoaula" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
        <div class="col-md-6 col-sm-6 col-xs-12 pnlcat">   
          <label>Nivel / Carrera / Categoria</label>         
          <div class="select-ctrl-wrapper select-azul">
          <select id="idcategoria" name="idcategoria"  class=" form-control selchangecat pnlidcategoria0">
              <option value="0">Unico - Curso Libre</option>
              <?php if(!empty($this->categoriasProyecto))
              foreach ($this->categoriasProyecto as $k=>$v){ if($v["idpadre"]==0){?>
                <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
              <?php } } ?>
          </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 pnlcat" style="display:none">   
          <label>Sub Nivel / modulo / Sub categoria</label>         
          <div class="select-ctrl-wrapper select-azul">
          <select id="nomsel" name="nomsel" class=" form-control selchangecat pnlidcategoria1">
              <option value="0">Seleccione</option>
              <?php if(!empty($this->categoriasProyecto))
              foreach ($this->categoriasProyecto as $k=>$v){
                if($v["idpadre"]!=0){?>
                <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
              <?php } } ?>                
          </select>
          </div>
        </div>
        <div class="clearfix"></div>           
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" tb="acad_grupoaula" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
</div>
<div style="display: none">
    <div id="pnlcopiarcursos">
        <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoaula" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
            <div class="row">               
                <div class="col-md-6 col-xs-12 pnlaccion">   
                  <label>Seleccione Accion</label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="accion" name="accion"  class=" form-control selchangecat pnlidcategoria0">
                        <option value="copiar">Copiar</option> 
                        <option value="asignar">Asignar</option>
                  </select>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12 pnlempresa">   
                  <label>Seleccione Empresa / Proyecto</label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="idproyecto" name="idproyecto"  class=" form-control selchangecat pnlidcategoria0">
                    <?php if(!empty($this->proyectos))
                         foreach ($this->proyectos as $k => $v){?>
                            <option value="<?php echo $v['idproyecto']; ?>"><?php echo $v['idproyecto'].": ".$v['empresa']; ?></option>
                         <?php } ?>
                  </select>
                  </div>
                </div>
                <div class="col-md-12 col-xs-12 ">   
                  <label>Copiar como : </label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="como" name="como"  class=" form-control selchangecat pnlidcategoria0">
                        <option value="1">Autoestudio - Tipo 1</option> 
                        <option value="2">Curso oficial de la empresa - Tipo 2 </option>                     
                  </select>
                  </div>
                </div>
                <div class="clearfix"></div>           
                  <hr>
                  <div class="col-md-12 form-group text-center">
                    <button id="btn-save" tb="acad_grupoaula" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
                    <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){    
    $('#lscursos').on('click','.item-curso .edit',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso');
        var idcc='&icc='+(dt.attr('idcomplementario')||0);
        idcc+='&tipo='+(dt.attr('tipo')||1);
        idcc+='&cat='+(dt.attr('idcategoria')||0);
        window.location.href=_sysUrlSitio_+'/cursos/crear?idcurso='+id+idcc;
    }).on('click','.item-curso .mirarcategoria',function(ev){
        ev.preventDefault();
        ev.stopPropagation();        
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso')||0;
        var idcc=dt.attr('idcomplementario')||0; 
        var tipo=dt.attr('tipo')||1; 
        var idcat=dt.attr('idcategoria')||1;    
        var titulo = '<?php echo JrTexto::_(" Categorias de Curso"); ?>';
        var frm=$('#shopnlcategorias').clone();
        var _md = __sysmodal({'html': frm,'titulo': titulo});
        _md.on('change','select',function(ev){
            var el=$(this);            
            if(el.hasClass('pnlidcategoria0')){
                if(el.val()>0){
                    _md.find('select.pnlidcategoria0').attr('id','selcat').attr('name','selcat');
                    _md.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria');
                    _md.find('select.pnlidcategoria1').children('option').first().nextAll().hide();
                    _md.find('select.pnlidcategoria1').children('option[padre="'+el.val()+'"]').show();
                    _md.find('select.pnlidcategoria1').parent().parent().show();
                }else{
                    _md.find('select.pnlidcategoria1').attr('id','selcat').attr('name','selcat');
                    _md.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria');
                    _md.find('select.pnlidcategoria1').parent().parent().hide();
                }
            }
        }).on('click','#btn-save',function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var data=new FormData()
            data.append('idcurso',id);
            data.append('idcomplementario',idcc);
            data.append('idcategoria',_md.find('select#idcategoria').val());
            data.append('tipo',tipo);

            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/acad_curso/guardarcategoria',
                callback:function(rs){
                  if(rs.code==200){
                    window.location.reload();
                  }
                }
            });
        })

        var pnl1=_md.find('select.pnlidcategoria0').find('option[value="'+idcat+'"]');
        if(pnl1.length>0){
            _md.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria').val(idcat);
            _md.find('select.pnlidcategoria1').attr('id','selcat').attr('name','selcat');
            _md.find('select.pnlidcategoria1').parent().parent().hide();
        }else{
            var pnl2=_md.find('select.pnlidcategoria1').find('option[value="'+idcat+'"]');
            if(pnl2.length>0){
                _md.find('select.pnlidcategoria0').attr('id','selcat').attr('name','selcat').val(pnl2.attr('padre')||0);
                _md.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria').val(idcat);
                _md.find('select.pnlidcategoria1').children('option').first().nextAll().hide();
                _md.find('select.pnlidcategoria1').children('option[padre="'+(pnl2.attr('padre')||0)+'"]').show();
                _md.find('select.pnlidcategoria1').parent().parent().show();
            }
        }        
    }).on('click','.item-curso .copy',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso')||0;
        var idcc=dt.attr('idcomplementario')||0;
        var tipo=dt.attr('tipo')||1;  
        var nombrecurso = dt.find("#nombrecurso").text();
        var titulo = '<?php echo JrTexto::_(" Copiar  o Asignar curso"); ?>: '  +nombrecurso;
        var frm=$('#pnlcopiarcursos').clone();
        if(tipo==2){
            titulo = '<?php echo JrTexto::_(" Copiar curso"); ?>: ' +nombrecurso;
            pnlaccion =frm.find('.pnlaccion').hide();
            frm.find('select#accion option[value="copiar"]').attr('selected','selected').siblings('option').removeAttr('selected');
            frm.find('.pnlempresa').removeClass('col-md-6 col-md-12').addClass('col-md-12');
        }
        var _md = __sysmodal({'html': frm,'titulo': titulo,zindex:true}); 
        _md.on('click','#btn-save',function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if($(this).hasClass('disabled')) return;
            $(this).addClass('disabled');
            var el=$(this);
            Swal.showLoading()
            var data=new FormData()
            data.append('idcurso',id);
            data.append('idcomplementario',idcc);
            data.append('accion',_md.find('select#accion').val());
            data.append('idproyecto',_md.find('select#idproyecto').val());
            data.append('tipo',tipo);
            data.append('como',_md.find('select#como').val());
              __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/acad_curso/copiar',
                showmsjok:true,
                callback:function(rs){ 
                    el.removeClass('disabled');
                    //window.location.reload();
                }               
            });
              setTimeout(function(){_md.find('#btn-save').removeClass('disabled');},3000);
        }).on('change','#accion',function(ev){
            var acc=$(this).val();
            if(acc=='asignar'){
                _md.find('select#como option[value="1"]').attr('selected','selected').siblings('option').removeAttr('selected');
                _md.find('select#como').parent().parent().hide();
            }else{
                _md.find('select#como').parent().parent().show();
            }
            _md.find('#btn-save').removeClass('disabled');
        }).on('change','#como',function(ev){
            _md.find('#btn-save').removeClass('disabled');
        })
        return false;     
    })

    $('#lscursos').on('click','.item-curso .remove',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
            var id=dt.attr('idcurso');
            var tipo=dt.attr('tipo');
            Swal.fire({
                title: '<?php echo JrTexto::_('¿Seguro de eliminar curso?');?>',
                text: '<?php echo JrTexto::_('También se eliminará todo lo relacionado al mismo'); ?>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<?php echo JrTexto::_('Aceptar');?>',
                cancelButtonText: '<?php echo JrTexto::_('Cancelar');?>',
            }).then((result) => {
                if (result.value) {
                    // console.log($(this).closest(".visorCursos").parent())
                    $.post(_sysUrlBase_ + 'json/acad_curso/eliminar',{
                        'idcurso': id,
                        'tipo': tipo,
                        'eliminar': 'si'
                    }, function(data){
                        if(data.code == 200){
                            location.reload();
                        }
                    }, 'json');
                }
            });
    })

    
    $('#lscursos').on('click','.item-curso .formular',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso');
        id+='&icc='+(dt.attr('idcomplementario')||0);
        id+='&tipo='+(dt.attr('tipo')||1);
        id+='&idcategoria='+(dt.attr('idcategoria')||0);        
        window.location.href=_sysUrlBase_+'/cursos/formular/?idcurso='+id;
    })



    $('#lscursos').on('click','.item-curso',function(ev){        
        var id=$(this).attr('idcurso');
        var idcc='&icc='+($(this).attr('idcomplementario')||0);
        idcc+='&tipo='+($(this).attr('tipo')||1);
        idcc+='&idcategoria='+($(this).attr('idcategoria')||0);
        if($(this).hasClass('lock')) return false;
        window.open(_sysUrlBase_+'smart/cursos/ver/?idcurso='+id+idcc, '_blank');
    })

    $("#btn-agregar , #btn-ordenar").click(function(){
        location.href = $(this).data("href");
    })

    $('small.showasignados').on('click',function(ev){
        ev.preventDefault();
        var i=$(this).children('i');
        if(i.hasClass('fa-check-square')){
            i.removeClass('fa-check-square').addClass('fa-square');
            $('.item-curso[idcomplementario="0"][tipo="2"]').parent().parent().hide();
        }else{
            i.removeClass('fa-square').addClass('fa-check-square');
            $('.item-curso[idcomplementario="0"][tipo="2"]').parent().parent().show();
        }
    })
});
</script>