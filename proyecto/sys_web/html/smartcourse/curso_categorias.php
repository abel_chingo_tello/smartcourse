<?php 
$idgui=uniqid(); 
$imgcursodefecto=$this->documento->getUrlBase().'static/media/nofoto.jpg';
?>
<style>

</style>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 1.5ex; margin-top: 1ex;">
            <div class="progress">
                <div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-value="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">% completado</div>
                <div class="progress-bar bg-warning" role="progressbar" aria-value="100" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="wizard">
                <ul class="nav nav-wizard" id="ultabs">
                    <li class="active selected"><a href="#" data-value="10" data-show="paso1"><b class="number">1.</b> Datos del curso </a></li>
                    <li class="" ><a href="#" data-value="20" data-show="paso2"><b class="number">2.</b> Categorias </a></li>
                    <li class="" ><a href="#" data-value="30" data-show="paso3"><b class="number">3.</b> Estructura </a></li>
                    <li class="" ><a href="#" data-value="50" data-show="paso4"><b class="number">4.</b> Sesiones </a></li>
                    <li class="" ><a href="#" data-value="60" data-show="portada"><b class="number">5.</b> Portada </a></li>
                    <li class="" ><a href="#" data-value="70" data-show="indice"><b class="number">6.</b> Indice </a></li>
                    <li class="" ><a href="#" data-value="80" data-show="contenido"><b class="number">7.</b> Contenidos </a></li>
                    <li class="" ><a href="#" data-value="100" data-show="publicar"><b class="number">8.</b> Publicar </a></li>
                </ul>
            </div>
            <div class="tab-content shadow" id="panelesultabs">
                <div class="tab-pane active " role="tabpanel" id="pasoinfo">
                    <form id="frmdatosdelcurso" method="post" target="" enctype="multipart/form-data">
                    <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idcurso;?>">
                    <input type="hidden" name="imagen" id="imagen" value="">
                        <div class="card shadow">
                            <div class="card-body">	
                                <div class="row">
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-12 form-group">
                                                <label style=""><?php echo JrTexto::_('Nombre del curso');?></label> 
                                                <input type="text" autofocus name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Curso 01"))?>" >					           
                                            </div>
                                            <div class="col-6 col-sm-12 form-group">							        	
                                                <label style=""><?php echo JrTexto::_('Autor');?></label> 
                                                <input type="text" name="autor"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Apellidos y Nombres del autor "))?>">					           
                                            </div>
                                            <div class="col-6 col-sm-12 form-group">
                                                <label style=""><?php echo JrTexto::_('Fecha de publicación');?></label> 
                                                <input type="date" name="aniopublicacion" class="form-control" placeholder="<?php echo date('Y-m-d'); ?>" >
                                            </div>
                                            <div class="col-12 form-group">
                                                <label style=""><?php echo JrTexto::_('Reseña');?></label> 
                                                <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso"))?>"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 text-center form-group">
                                        <div class="row">
                                            <div class="col-12">
                                                <label><?php echo JrTexto::_("Imagen de curso") ?> </label>                
                                                <div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
                                                    <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                                                    <div id="sysfilelogo">
                                                    <img src="<?php echo $imgcursodefecto;?>" class="__subirfile img-thumbnail img-fluid center-block" typefile="imagen"  id="imagen" style="max-width: 200px; max-height: 150px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 form-group text-left " style="padding:3em;">		        	
                                                <label style=""><?php echo JrTexto::_('Color del curso');?> </label> <br>
                                                <input type="" name="color" value="rgb(0,0,0,0)" class="vercolor form-control" >			       
                                            </div>
                                        </div>
                                    </div>
                                </div>			            				
                            </div>
                            <div class="card-footer text-center">
                                <button type="button" onclick="history.back();" class="btn btn-warning"><i class=" fa fa-undo"></i> Regresar al listado</button>
                                <button type="submit" class="btn btn-primary" data-pclase="tab-pane" data-showp="#pasocate" ><i class=" fa fa-save"></i> Guardar y continuar <i class=" fa fa-arrow-right"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
