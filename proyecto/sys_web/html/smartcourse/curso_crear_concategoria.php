<?php
$idgui = uniqid();
$imgcursodefecto = $this->documento->getUrlBase() . 'static/media/nofoto.jpg';
$curso = !empty($this->curso) ? $this->curso : '';
$imagen = (!empty($curso["imagen"]) ? $curso["imagen"] : $imgcursodefecto);
$ipos = strripos($imagen, 'static/');
if ($ipos == false && $ipos != 0) {
  $ipos2 = strripos($imagen, '.');
  if ($ipos2 == false && $ipos2 != 0) $imagen = $imgcursodefecto;
  else {
    $ipos1 = strripos($imagen, '/');
    if ($ipos1 == false && $ipos1 != 0) $imagen = $this->documento->getUrlStatic() . '/media/cursos/' . $imagen;
    else $imagen = $this->documento->getUrlStatic() . '/media/cursos/' . substr($imagen, $ipos1);
  }
} else $imagen = $this->documento->getUrlBase() . substr($imagen, $ipos);
$txtjson = !empty($curso["txtjson"]) ? json_encode($curso["txtjson"], true) : '';
$txticon = '#';
?>
<style type="text/css">
  .wizard_horizontal ul.wizard_steps {
    font-weight: bold !important;
  }

  @media (max-width: 600px) {
    .accionesdecurso {
      position: relative !important;
    }

    span.step_descr {
      display: none;
    }
  }

  .li-hover {
    background-color: #eee;
  }

  div.stepContainer {
    height: auto !important;
    min-height: 59px;
  }

  div.stepContainer .content {
    min-height: 300px;
  }

  li.tienehijos>span.accionesdecurso>.btnaddhabilidades,
  li.tienehijos>span.accionesdecurso>.btnmostrarpregunta,
  li.tienehijos>span.accionesdecurso>.btnaddvideosyoutube,
  li.tienehijos>span.accionesdecurso>.btnaddcontent,
  li.tienehijos>span.accionesdecurso>.btnestarea,
  li.tienehijos>span.accionesdecurso>.btnesproyecto,
  li.tienehijos>span.accionesdecurso>.btnpuntaje,
  li.tienehijos>span.accionesdecurso>.btnrecursopagado,
  li.tienehijos>span.accionesdecurso>.btnconteacher,
  li>span>.btnaddhabilidades,
  li>span>.btnpuntaje,
  li>span>.btnaddrubricas {
    display: none;
  }

  /* mine */
  li.concalificacion>span>.btnaddrubricas {
    display: block;
  }

  /* end mine */
  li.concalificacion>span>.btnaddhabilidades,
  li.concalificacion>span>.btnpuntaje,
  li.conrubrica>span>.btnaddrubricas {
    display: block;
  }

  #tablenuevapuntuacion input {
    width: 85px;
    text-transform: uppercase;
    text-align: center;
    height: 25px;
  }

  #tablenuevapuntuacion select {
    height: 24px;
    padding: 0px;
    font-size: 13px;
    width: 100%;
  }

  #tablenuevapuntuacion .select-ctrl-wrapper::after {
    right: 0px;
    font-size: 23px;
  }

  #tablenuevapuntuacion td {
    vertical-align: middle;
    padding: 0.5ex;
    text-align: center;
  }

  .formulagenerada {
    border: 1px solid #ccc;
    padding: 1em;
    border-radius: 4ex;
  }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlStatic(); ?>/libs/jquery-nestable/jquery.nestable.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/green/green.css">
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Edit Course'); ?> <small></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="_crearcurso_">
        <!-- Smart Wizard -->
        <div id="wizard" class="form_wizard wizard_horizontal">
          <ul class="wizard_steps" style="padding: 0px;">
            <li>
              <a href="#step-1">
                <span class="step_no">1</span>
                <span class="step_descr"><?php echo JrTexto::_('Course information'); ?></span>
              </a>
            </li>
            <li>
              <a href="#step-2">
                <span class="step_no">2</span>
                <span class="step_descr"><?php echo JrTexto::_('Cover design'); ?></span>
              </a>
            </li>
            <li>
              <a href="#step-3">
                <span class="step_no">3</span>
                <span class="step_descr"><?php echo JrTexto::_('Course grade'); ?></span>
              </a>
            </li>
            <li>
              <a href="#step-4">
                <span class="step_no">4</span>
                <span class="step_descr"><?php echo JrTexto::_('Course content'); ?></span>
              </a>
            </li>

            <?php if ($this->tipocurso == '1') : ?>
              <li>
                <a href="#step-5">
                  <span class="step_no">5</span>
                  <span class="step_descr"><?php echo JrTexto::_('Post course'); ?></span>
                </a>
              </li>
            <?php endif; ?>
          </ul>
          <div id="step-1">
            <form id="frmdatosdelcurso" method="post" target="" enctype="multipart/form-data">
              <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idcurso; ?>">
              <input type="hidden" name="tipo" id="tipo" value="<?php echo $this->tipocurso; ?>">
              <input type="hidden" name="sks" id="sks" value="1">
              <div class="row">
                <div class="col-md-6 col-sm-12">
                  <div class="col-md-12 form-group">
                    <label><?php echo JrTexto::_('Nombre del curso'); ?></label>
                    <input type="text" autofocus name="nombre" required="required" class="guardarvalorcurso form-control" placeholder="<?php echo ucfirst(JrTexto::_("Curso 01")) ?>" value="<?php echo @$curso["nombre"] ?>">
                    <!-- <input type="text" autofocus name="nombre" required="required" class="guardarvalorcurso form-control" placeholder="<?php echo ucfirst(JrTexto::_("Curso 01")) ?>" value="<?php echo @$curso["nombre"] ?>"<?php echo $this->validar ? ' readonly=""' : ''; ?>> -->
                  </div>
                  <div class="col-md-12 form-group">
                    <label><?php echo JrTexto::_('Autor'); ?></label>
                    <input type="text" name="autor" class="guardarvalorcurso form-control _autor_" placeholder="<?php echo  ucfirst(JrTexto::_("Apellidos y Nombres del autor ")) ?>" value="<?php echo $this->validar ? $this->curusuario["nombre_full"] : @$curso["autor"]; ?>" <?php echo $this->validar ? ' readonly=""' : ''; ?>>
                  </div>
                  <div class="col-md-12 form-group">
                    <label><?php echo JrTexto::_('Reseña'); ?></label>
                    <textarea name="descripcion" class="guardarvalorcurso form-control" rows="5" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso")) ?>" value="<?php echo @$curso["descripcion"] ?>" style="resize: none;"></textarea>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12">
                  <div class="col-md-12 text-center">
                    <label><?php echo JrTexto::_("Imagen de curso") ?> </label>
                    <div style="position: relative;" class="guardarvalorcurso text-center" id="contenedorimagen">
                      <div class="toolbarmouse text-center"><span class="btn borramendia" name='imagen' value=''><i class="fa fa-trash"></i></span></div>
                      <div id="sysfilelogo">
                        <img src="<?php echo $imagen; ?>" class="__subirfile img-thumbnail img-fluid center-block" typefile="imagen" id="imagen" style="max-width: 200px; max-height: 150px;">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 form-group text-center">
                    <br><label><?php echo JrTexto::_('Color del curso'); ?> </label> <br>
                    <input type="" name="color" value="<?php echo !empty($curso["color"]) ? $curso["color"] : 'rgb(0,0,0,0)' ?>" class="guardarvalorcurso vercolor form-control">
                  </div>
                </div>
              </div>
              <!--div class="card-footer text-center">
                    <button type="button" class="buttonPrevious btn btn-warning"><i class=" fa fa-undo"></i> Regresar al listado</button>
                    <button type="submit" class="buttonNext btn btn-primary" data-pclase="tab-pane" data-showp="#pasocate" ><i class=" fa fa-save"></i> Guardar y continuar <i class=" fa fa-arrow-right"></i></button>
                </div-->
            </form>
          </div>
          <div id="step-2">
            <div class="row btnmanagermenu">
              <div class="col-md-6 col-sm-12">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h3>Información de Portada</h3>
                  </div>
                  <div class="col-md-12 form-group">
                    <label style=""><?php echo JrTexto::_('Titulo'); ?></label>
                    <input type="text" autofocus name="titulo" required="required" class="_infoportada form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Titulo de portada")) ?>">
                  </div>
                  <div class="col-md-12 form-group">
                    <label style=""><?php echo JrTexto::_('Resumen'); ?></label>
                    <textarea name="descripcion" class="_infoportada form-control" rows="5" placeholder="<?php echo  ucfirst(JrTexto::_("descripción corta para la portada")) ?>" style="resize: none;"></textarea>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h3>Estilo de portada</h3>
                  </div>
                  <div class="col-md-12 text-center form-group">
                    <label><?php echo JrTexto::_("Imagen de fondo") ?> </label>
                    <div style="position: relative;" class="_infoportada text-center" id="contenedorimagen">
                      <input type="hidden" value="" name="imagenfondo" id="imagenfondo">
                      <div class="toolbarmouse text-center"><span class="btn btnremoveimage borramendia" name="imagenfondo"><i class="fa fa-trash"></i></span></div>
                      <div id="sysfilelogo">
                        <img src="<?php echo $imgcursodefecto; ?>" name="imagenfondo" class=" __subirfile img-fluid center-block" data-type="imagen" nomfile="portadafondo" id="imagenfondo" style="min-height:100px; max-width: 200px; max-height: 150px;">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 text-center">
                    <label><?php echo JrTexto::_('Color de fondo'); ?> </label> <br>
                    <input type="" name="color" id="colorfondo" value="rgb(0,0,0,0)" class="_infoportada vercolor form-control">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="step-3">
            <div class="row">
              <div class="col-md-12" id="_puntuacioncurso_">
                <legend>
                  <h3 class="text-center"><?php echo JrTexto::_('Score'); ?></h3>
                  <hr>
                </legend>
                <div class="row" id="rownumber">
                  <div class="col-md-4 form-group">
                    <label class=""><?php echo ucfirst(JrTexto::_("Type")) ?></label>
                    <div class=" select-ctrl-wrapper select-azul ">
                      <select name="calificacionen" id="calificacionen" class="form-control">
                        <option value="N"><?php echo ucfirst(JrTexto::_("Numeric")) ?></option>
                        <option value="P"><?php echo ucfirst(JrTexto::_("Percentage")) ?></option>
                        <option value="A"><?php echo ucfirst(JrTexto::_("Alfabetic")) ?></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4 form-group pornum">
                    <label class=""><?php echo ucfirst(JrTexto::_("Highest score")) ?><span style="display:none;">%</span></label>
                    <div class="select-ctrl-wrapper select-azul ">
                      <select name="maxpuntaje" class="form-control maxpuntaje">
                        <?php for ($i = 100; $i >= 1; $i--) { ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php }  ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4 form-group pornum">
                    <label class=""><?php echo ucfirst(JrTexto::_("Minimum score to pass")) ?><span style="display:none;">%</span></label>
                    <div class=" select-ctrl-wrapper select-azul ">
                      <select name="minpuntaje" class="form-control minpuntaje">
                        <?php for ($i = 0; $i <= 100; $i++) { ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php }  ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row" style="display: none;">
                  <div class="col-md-12 table-reponsive">
                    <table class="table table-striped table-hover tablepuntuacion" style="border: 1px solid #ccc;">
                      <thead class="headings">
                        <tr>
                          <th><?php echo ucfirst(JrTexto::_("Scale name")); ?></th>
                          <th><?php echo ucfirst(JrTexto::_("Highest score")); ?></th>
                          <th><?php echo ucfirst(JrTexto::_("Minimum score")); ?></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="text" class="form-control gris txtNombreEscala" name="txtNombreEscala" placeholder="e.g.: A, good, ..." value=""></td>
                          <td>
                            <div class="select-ctrl-wrapper select-azul ">
                              <select name="maxpuntaje" id="maxpuntaje" class="form-control maxpuntaje" style="font-size: 15px;">
                                <?php for ($i = 100; $i >= 1; $i--) { ?>
                                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }  ?>
                              </select></div>
                          </td>

                          <td>
                            <div class="select-ctrl-wrapper select-azul ">
                              <select name="minpuntaje" id="minpuntaje" class="form-control minpuntaje" style="font-size: 15px;">
                                <?php for ($i = 0; $i <= 100; $i++) { ?>
                                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }  ?>
                              </select></div>
                          </td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="step-4">
            <div id="_contentcurso_">
              <div class="row">
                <div class="col-md-12">
                  <div class="cf nestable-lists">
                    <div class="dd col-md-12" id="nestable" style="width:100%; max-width: 100% !important;">
                      <ol class="dd-list itemchange" id="">
                        <?php
                        $j = 0;
                        if (!empty($this->temas)) {
                          foreach ($this->temas as $k => $t) {
                            $txtjsontema = json_decode($t["txtjson"], true);
                            $tipo = !empty($txtjsontema["tipo"]) ? $txtjsontema["tipo"] : '#showpaddopciones';
                            $tipofile = !empty($txtjsontema["tipofile"]) ? $txtjsontema["tipofile"] : 'arriba';
                            $link = !empty($txtjsontema["link"]) ? $txtjsontema["link"] : '';
                            $typelink = !empty($txtjsontema["typelink"]) ? $txtjsontema["typelink"] : '';
                            $infoavance = 100;
                            $txticon = !empty($txtjsontema["icon"]) ? '<i class="fa ' . $txtjsontema["icon"] . '"></i>' : '#';
                            if ($txticon == '#' && ($t["tiporecurso"] == 'M' || $tiporecurso == 'E') &&  $typelink == 'smartquiz') {
                              $txticon = '<i class=" fa fa-list"></i>';
                              $txtjsontema['icon'] = 'fa-list';
                            } else if (@$txtjsontema['icon'] == '' || @$txtjsontema['icon'] == '#' || empty($txtjsontema['icon'])) {
                              $txtjsontema['icon'] = '#';
                              $txticon = '#';
                            }
                            $tienehijos = !empty($txtjsontema["options"]) ? 'tienehijos' : '';

                            $txtconcalificacion = '';
                            if ($typelink == 'enlacecolaborativo' || $typelink == 'foro' || $typelink == 'smartquiz' || @$txtjsontema["estarea"] == 'si' || @$txtjsontema["esproyecto"] == 'si') $txtconcalificacion = 'concalificacion';
                            $txtconrubrica = '';
                            if (@$txtjsontema['estarea'] == 'si' || @$txtjsontema["esproyecto"] == 'si') {
                              $txtconrubrica = 'conrubrica';
                            }

                        ?>
                            <!-- my-menu -->
                            <li id="0" idcomplementario="<?= $this->idcomplementario ?>" accimagen="sg" imagen="<?php echo $t["imagen"]; ?>" url="<?php echo $t["url"]; ?>" idlogro="<?php echo $t["idlogro"]; ?>" tiporecurso="<?php echo $t["tiporecurso"]; ?>" idrecurso="<?php echo $t["idrecurso"]; ?>" orden="<?php echo $t["orden"]; ?>" idcursodetalle="<?php echo $t["idcursodetalle"]; ?>" class="dd-item itemchange1 esmenu <?php echo $tienehijos . " " . $txtconcalificacion . " " . $txtconrubrica; ?>" data-id="<?php echo $j ?>" color="<?php echo @$t["color"]; ?>" descripcion="<?php echo @$t["descripcion"]; ?>" txtjson-tipo="<?php echo $tipo; ?>" txtjson-tipofile="<?php echo $tipofile; ?>" txtjson-link="<?php echo $link; ?>" txtjson-typelink="<?php echo $typelink; ?>" style="border-bottom: 1px solid #ddd;" number="<?php echo $k; ?>" data-icon="<?php echo $txtjsontema['icon']; ?>" criterios="<?php echo @$txtjsontema["criterios"]; ?>">
                              <!-- <div class="dd-handle"> -->
                              <i class=" btn btn-xs fa fa-arrows dd-handle" title="<?php echo JrTexto::_('Move'); ?>" style="cursor: move; display: inline-block; margin-top: 0px;"></i> <span class="titulo"><?php echo $t["nombre"]; ?></span>
                              <span class="accionesdecurso btn-group" style="position: absolute; right: 0px;">
                                <!--i class="btn btn-xs fa fa-plus" title="<?php //echo JrTexto::_('Add Menu'); 
                                                                            ?>" ></i-->
                                <i class="btn btn-xs fa fa-plus-circle btnaddoption" title="<?php echo JrTexto::_('Add Pestania'); ?>"></i>
                                <i class="btn btn-xs fa fa-magic btnaddhabilidades" title="<?php echo JrTexto::_('Add habilidades'); ?>"></i>
                                <i class="btn btn-xs fa fa-database btnpuntaje" title="<?php echo JrTexto::_('Puntaje'); ?>"></i>
                                <i class="btn btn-xs fa fa-archive btnaddrubricas <?php echo @$txtjsontema["conrubrica"] == 'si' ? 'bg-success' : '';?>" title="<?php echo JrTexto::_('Add Rubrica');?>"></i>
                                
                                <div class="btn-group btn-group-sm" role="group">
                                  <button id="btnGroupDrop1" type="button" class="btn btn-xs btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo @$txticon; ?>
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="width: 40px !important; min-width: 40px !important;     max-height: 220px; overflow-y: scroll;">
                                    <a class="dropdown-item" data-icon="#" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon">#</i> </a>
                                    <a class="dropdown-item" data-icon="fa-list" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-list <?php echo $txtjsontema['icon'] == 'fa-list' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-folder-o" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-folder-o <?php echo $txtjsontema['icon'] == 'fa-folder-o' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-file-text" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-file-text <?php echo $txtjsontema['icon'] == 'fa-file-text"' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-bar-chart" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-bar-chart <?php echo $txtjsontema['icon'] == 'fa-bar-chart' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-check-square-o" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-check-square-o <?php echo $txtjsontema['icon'] == 'fa-check-square-o' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-envelope" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-envelope <?php echo $txtjsontema['icon'] == 'fa-envelope' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-cloud-download" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-cloud-download <?php echo $txtjsontema['icon'] == 'fa-cloud-download' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-film" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-film <?php echo $txtjsontema['icon'] == 'fa-film' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-keyboard-o" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-keyboard-o <?php echo $txtjsontema['icon'] == 'fa-keyboard-o' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-laptop" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-laptop <?php echo $txtjsontema['icon'] == 'fa-laptop' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-book" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-book <?php echo $txtjsontema['icon'] == 'fa-book' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-th-large" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-th-large <?php echo $txtjsontema['icon'] == 'fa-th-large' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-th" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-th <?php echo $txtjsontema['icon'] == 'fa-th' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-comments-o" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-comments-o <?php echo $txtjsontema['icon'] == 'fa-comments-o' ? 'bg-success' : ''; ?>"></i> </a>
                                    <a class="dropdown-item" data-icon="fa-clipboard" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-clipboard <?php echo $txtjsontema['icon'] == 'fa-clipboard' ? 'bg-success' : ''; ?>"></i> </a>
                                  </div>
                                </div>
                                <i class="btn btn-xs fa fa-question btnmostrarpregunta <?php echo @$txtjsontema["mostrarpregunta"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Show questions'); ?>"></i>

                                <i class="btn btn-xs fa fa-file btnaddcontent" title="<?php echo JrTexto::_('Add content'); ?>" style="<?php echo !empty($txtjsontema["options"]) ? 'display:none;' : ''; ?>"></i>

                                <i class="btn btn-xs fa fa-tasks btnestarea <?php echo @$txtjsontema["estarea"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es una tarea'); ?>"></i>
                                <i class="btn btn-xs fa fa-folder btnesproyecto <?php echo @$txtjsontema["esproyecto"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es un Proyecto'); ?>"></i>
                                <i class="btn btn-xs fa fa-dollar btnrecursopagado <?php echo @$txtjsontema["esrecursopagado"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es un Recurso Pagado'); ?>" <?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>></i>
                                <i class="btn btn-xs fa fa fa-graduation-cap btnconteacher <?php echo @$txtjsontema["necesitadocente"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Necesita docente'); ?>" style="display:none;"></i>

                                <i class="btn btn-xs fa fa-pencil btneditmenu" title="<?php echo JrTexto::_('Edit'); ?>"></i>
                                <i class="btn btn-xs fa fa-trash btnremovemenu" title="<?php echo JrTexto::_('Remove'); ?>"></i>

                              </span>
                              <!-- </div>      -->
                              <?php if (!empty($txtjsontema["options"])) { ?>
                                <!-- <div class="dd col-md-12" id="nestable2" style="width:100%; max-width: 100% !important;"> -->
                                <ol class="dd-list<?php echo $k; ?> dd<?php echo $k; ?> itemchange1" number="<?php echo $k; ?>" id="nestable<?php echo $k; ?>" idcursodetalle="<?php echo $t["idcursodetalle"]; ?>">
                                  <?php foreach ($txtjsontema["options"] as $kjson => $vjson) { //var_dump($vjson);
                                    $txtconcalificacion = '';
                                    $txtconrubrica = '';
                                    if ($vjson["type"] == 'enlacecolaborativo' || $vjson["type"] == 'foro' || $vjson["type"] == 'smartquiz' || @$vjson["estarea"] == 'si' || @$vjson["esproyecto"] == 'si') $txtconcalificacion = 'concalificacion';
                                    if (@$vjson['estarea'] == 'si' || $vjson["esproyecto"] == 'si') {
                                      $txtconrubrica = 'conrubrica';
                                    }

                                  ?>
                                    <li idcomplementario="<?= $this->idcomplementario ?>" idcursodetalle="<?php echo $t["idcursodetalle"]; ?>" class="dd-item<?php echo $k . " " . $txtconcalificacion . " " . $txtconrubrica; ?>" data-id="<?php echo $kjson; ?>" id="<?php echo $vjson["id"]; ?>" type="<?php echo $vjson["type"]; ?>" link="<?php echo $vjson["link"]; ?>" color="<?php echo $vjson["color"]; ?>" imagen="<?php echo $vjson["imagenfondo"]; ?>" style="list-style: none; border-bottom: 1px solid #eee;" criterios="<?php echo !empty($vjson["criterios"]) ? $vjson["criterios"] : ''; ?>">
                                      <!-- <div class=""> -->
                                      <i class=" btn btn-xs fa fa-arrows dd-handle<?php echo $k; ?>" title="<?php echo JrTexto::_('Move'); ?>" style="cursor: move; display: inline-block; margin-top: 0px;"></i> <span class="titulo"><?php echo $vjson["nombre"]; ?></span>
                                      <span class="accionesdecurso btn-group" style="position: absolute; right: 0px;">
                                        <i class="btn btn-xs fa fa-magic btnaddhabilidades" title="<?php echo JrTexto::_('Add habilidades'); ?>"></i>
                                        <i class="btn btn-xs fa fa-database btnpuntaje" title="<?php echo JrTexto::_('Puntaje'); ?>"></i>
                                        <i class="btn btn-xs fa fa-archive btnaddrubricas <?php echo @$vjson["conrubrica"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Add Rubrica'); ?>"></i>
                                        <i class="btn btn-xs fa fa-question btnmostrarpregunta <?php echo @$vjson["mostrarpregunta"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Show questions'); ?>"></i>
                                        <i class="btn btn-xs fa fa-file btnaddcontent" title="<?php echo JrTexto::_('Add content'); ?>"></i>
                                        <!--i class="btn btn-xs fa fa-film btnaddvideos" title="<?php echo JrTexto::_('Add video'); ?>"></i-->
                                        <!--i class="btn btn-xs fa fa-youtube-play btnaddvideosyoutube" title="<?php //echo JrTexto::_('Add Video Youtube'); 
                                                                                                                ?>"></i-->

                                        <i class="btn btn-xs fa fa-tasks btnestarea <?php echo @$vjson["estarea"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es una tarea'); ?>"></i>
                                        <i class="btn btn-xs fa fa-folder btnesproyecto <?php echo @$vjson["esproyecto"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es un Proyecto'); ?>"></i>
                                        <i class="btn btn-xs fa fa-dollar btnrecursopagado <?php echo @$vjson["esrecursopagado"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es un Recurso Pagado'); ?>" <?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>></i>
                                        <i class="btn btn-xs fa fa-graduation-cap btnconteacher <?php echo @$vjson["necesitadocente"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Necesita docente'); ?>" style="display:none;"></i>

                                        <i class="btn btn-xs fa fa-pencil btneditoption" title="<?php echo JrTexto::_('Edit'); ?>"></i>
                                        <i class="btn btn-xs fa fa-trash btnremoveoption" title="<?php echo JrTexto::_('Remove'); ?>"></i>
                                      </span>
                                      <!-- </div> -->
                                    </li>
                                  <?php } ?>
                                </ol>
                                <!-- </div> -->
                              <?php } ?>

                            </li>
                        <?php $j++;
                          }
                        }
                        ?>
                        <li accimagen="sg" imagen="" url="" idpadre="0" idlogro="" 0 tiporecurso="M" idrecurso="0" orden="0" idcursodetalle="0" class="dd-item itemchange1 esmenu liclone" color="rgba(32, 50, 69, 0)" link="" descripcion="" data-id="<?php echo $j + 1; ?>" style="display:none; border-bottom: 1px solid #ddd;" txtjson-tipo="" txtjson-tipofile="arriba" txtjson-link="" txtjson-typelink="">
                          <i class=" btn btn-xs fa fa-arrows dd-handle " title="<?php echo JrTexto::_('Move'); ?>" style="cursor: move; display: inline-block; margin-top: 0px;"></i> <span class="titulo"></span>
                          <span class="accionesdecurso btn-group" style="position: absolute; right: 0px;">
                            <i class="btn btn-xs fa fa-plus-circle btnaddoption" title="<?php echo JrTexto::_('Add Pestania'); ?>"></i>
                            <i class="btn btn-xs fa fa-magic btnaddhabilidades" title="<?php echo JrTexto::_('Add habilidades'); ?>"></i>
                            <i class="btn btn-xs fa fa-database btnpuntaje" title="<?php echo JrTexto::_('Puntaje'); ?>"></i>
                            <i class="btn btn-xs fa fa-archive btnaddrubricas" title="<?php echo JrTexto::_('Add Rubrica'); ?>"></i>

                            <!--i class="btn btn-xs fa fa-youtube-play btnaddvideosyoutube" title="<?php //echo JrTexto::_('Add Video Youtube'); 
                                                                                                    ?>"></i-->
                            <div class="btn-group btn-group-sm" role="group">
                              <button id="btnGroupDrop1" type="button" class="btn btn-xs btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $txticon; ?>
                              </button>
                              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="width: 40px !important; min-width: 40px !important;     max-height: 220px; overflow-y: scroll;">
                                <a class="dropdown-item" data-icon="#" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon">#</i> </a>
                                <a class="dropdown-item" data-icon="fa-list" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-list"></i> </a>
                                <a class="dropdown-item" data-icon="fa-folder-o" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-folder-o"></i> </a>
                                <a class="dropdown-item" data-icon="fa-file-text" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-file-text"></i> </a>
                                <a class="dropdown-item" data-icon="fa-bar-chart" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-bar-chart"></i> </a>
                                <a class="dropdown-item" data-icon="fa-check-square-o" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-check-square-o"></i> </a>
                                <a class="dropdown-item" data-icon="fa-envelope" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-envelope"></i> </a>
                                <a class="dropdown-item" data-icon="fa-cloud-download" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-cloud-download"></i> </a>
                                <a class="dropdown-item" data-icon="fa-film" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-film"></i> </a>
                                <a class="dropdown-item" data-icon="fa-keyboard-o" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-keyboard-o "></i> </a>
                                <a class="dropdown-item" data-icon="fa-laptop" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-laptop "></i> </a>
                                <a class="dropdown-item" data-icon="fa-book" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-book "></i> </a>
                                <a class="dropdown-item" data-icon="fa-th-large" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-th-large "></i> </a>
                                <a class="dropdown-item" data-icon="fa-th" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-th "></i> </a>
                                <a class="dropdown-item" data-icon="fa-comments-o" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-comments-o "></i> </a>
                                <a class="dropdown-item" data-icon="fa-clipboard" style="padding: 0.5ex 1.5ex; text-align: center; "> <i class="mostraricon fa fa-clipboard "></i> </a>
                              </div>
                            </div>
                            <i class="btn btn-xs fa fa-question btnmostrarpregunta" title="<?php echo JrTexto::_('Show questions'); ?>"></i>
                            <i class="btn btn-xs fa fa-file btnaddcontent" title="<?php echo JrTexto::_('Add content'); ?>"></i>
                            <i class="btn btn-xs fa fa-tasks btnestarea" title="<?php echo JrTexto::_('Es una tarea'); ?>"></i>
                            <i class="btn btn-xs fa fa-folder btnesproyecto" title="<?php echo JrTexto::_('Es un Proyecto'); ?>"></i>
                            <!--i class="btn btn-xs fa fa-folder-o iconproyecto" title="<?php //echo JrTexto::_('Con icono de Proyecto'); 
                                                                                        ?>"></i-->
                            <i class="btn btn-xs fa fa-dollar btnrecursopagado" title="<?php echo JrTexto::_('Es un Recurso Pagado'); ?>"></i>

                            <i class="btn btn-xs fa fa-pencil btneditoption" title="<?php echo JrTexto::_('Edit'); ?>"></i>
                            <i class="btn btn-xs fa fa-trash btnremovemenu" title="<?php echo JrTexto::_('Remove'); ?>"></i>
                          </span>
                        </li>
                      </ol>

                    </div>
                    <div class="col-md-12 text-center"> <br><a href="#" idmodal="" class="btnopenmodal btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add Menu'); ?> </a> </div>
                  </div>
                </div>
              </div>
              <div id="_infoTema_" style="display: none;">
                <form id="frmdatosdeltema" method="post" target="" enctype="multipart/form-data">
                  <input type="hidden" name="imagen" id="imagen" value="">
                  <input type="hidden" name="accimagen" id="accimagen" value="sg">
                  <input type="hidden" name="oldimagen" id="oldimagen" value="0">
                  <input type="hidden" name="accmenu" id="accmenu" value="0">
                  <div class="card text-center shadow">
                    <div class="card-body">
                      <!--h5 class="card-title"><?php //echo JrTexto::_('Datos generales del curso'); 
                                                ?></h5-->
                      <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
                          <div class="row">
                            <div class="col-12 form-group">
                              <label style=""><?php echo JrTexto::_('Nombre del Tema/pestaña'); ?></label>
                              <input type="text" autofocus name="nombre" required="required" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Tema 001")) ?>">
                            </div>
                            <div class="col-12 form-group">
                              <label style=""><?php echo JrTexto::_('Descripción'); ?></label>
                              <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del tema")) ?>" rows="5" style="resize: none;"></textarea>
                            </div>
                            <div class="col-6 form-group text-left ">
                              <label style=""><?php echo JrTexto::_('Color'); ?> </label>
                              <input type="" name="color" autocomplete="off" value="transparent" class="form-control">
                            </div>

                          </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6">
                          <label><?php echo JrTexto::_("imagen tema/pestaña") ?> (opcional) </label>
                          <div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
                            <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                            <div id="sysfilelogo">
                              <img src="<?php echo $imgcursodefecto; ?>" class="__subirfile img-fluid center-block" id="imagen" style="max-width: 200px; max-height: 150px;">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-muted">
                      <button type="button" class="cerrarmodal btncancelar btn btn-warning"><i class=" fa fa-close"></i> Cancelar</button>
                      <button type="submit" class="btn btn-primary btnguardartema"><i class=" fa fa-save"></i> Guardar Tema</button>
                    </div>
                  </div>
                </form>
              </div>
              <div id="showEnlazarYoutube" style="display:none">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12 text-center ">
                      <label>Enlazar Video</label>
                      <hr>
                    </div>
                    <div class="col-md-12">
                      <p>Enlace (link) <font color="red">(*)</font>
                        <input type="text" id="txtenlacevideo" name="txtenlacevideo" maxlength="200" class="form-control input-sm" placeholder="Ingrese Enlace (Link)" required="">
                      </p>
                    </div>
                  </div>
                  <div class="row" style="margin-top: 1ex;">
                    <div class="col-md-12 text-center text-muted">
                      <button type="button" class="btnsaveenlace btn btn-success"><i class="fa fa-save"></i> Guardar </button>
                      <button type="button" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> Cerrar </button>
                    </div>
                  </div>
                </div>
              </div>
              <div id="showpaddcontenido" style="display:none">
                <div class="col-md-12">
                  <div class="row" id="addcontentclone">
                    <div class="col-md-12 text-center ">
                      <label>Seleccione plantilla de contenido</label>
                      <hr>
                    </div>
                    <!--div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="texto" title="Texto" data-content="Agregar texto" data-acc="texto">        
                        <i class="fa btn btn-block btn-primary fa-text-width fa-2x "> <br><span>Texto</span> </i>
                      </div-->
                    <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="imagen" title="Imagen" data-content="Agregar una imagen" data-acc="imagen">
                      <i class="fa btn btn-block btn-primary fa-file-image-o fa-2x "> <br><span>Imagen</span> </i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="audio" title="Audio" data-content="Agregar un audio" data-acc="audio">
                      <i class="fa btn btn-block btn-primary fa-file-audio-o fa-2x "> <br><span>Audio</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="video" title="Video" data-content="agregar un video" data-acc="video">
                      <i class="fa btn btn-block btn-primary  fa-file-movie-o fa-2x "> <br><span>Video</span> </i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="pdf" title="Documento" data-content="Agregar un archivo pdf" data-acc="pdf">
                      <i class="fa btn btn-block btn-primary  fa-file-o fa-2x "> <br><span>Documento</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="html" title="HTML" data-content="Agregar contenido HTML - seleccione un zip si incluye mas contenido" data-acc="html">
                      <i class="fa btn btn-block btn-primary fa-file-code-o fa-2x "> <br><span>Html</span></i>
                    </div>
                    <!--div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="flash" title="Flash" data-content="Agregar SWF - archivo Flash" data-acc="flash">
                        <i class="fa btn btn-block btn-primary fa-file-archive-o fa-2x "> <br><span>flash</span></i>
                      </div-->
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticlock" title="SmartTic Look" data-content="Look" data-acc="smarticlock" <?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                      <i class="fa btn btn-block btn-primary fa-eye fa-2x "><br> <span> Mirar </span> </i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticlookPractice" title="SmartTic Practice" data-content="Practice" data-acc="smarticlookPractice" <?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                      <i class="fa btn btn-block btn-primary fa-cogs fa-2x "><br> <span> Practicar</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticDobyyourselft" title="SmartTic Do by yourselft" data-content="Do it by yourself" data-acc="smarticDobyyourselft" <?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                      <i class="fa btn btn-block btn-primary fa-cogs fa-2x "><br> <span> hazlo tu mismo</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="game" title="Juegos" data-content="Agregar juegos interactivos" data-acc="game" <?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                      <i class="fa btn btn-block btn-primary fa-gamepad fa-2x "><br><span>Juegos</span> </i>
                    </div>
                    <div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="smartquiz" title="SmartQuiz" data-content="Agregar Examenes" data-acc="smartquiz">
                      <i class="fa btn btn-block btn-primary fa-tasks fa-2x "> <br><span>Examenes</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="menuvaloracion" title="Mostrar valoracion de curso" data-content="Agregar Examenes" data-acc="smartquiz">
                      <i class="fa btn btn-block btn-primary fa-star fa-2x "> <br><span>Mostrar valoracion</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="enlacesinteres" title="Mostrar Enlaces de Interés" data-content="Agregar Enlaces de Interés" data-acc="enlacesinteres">
                      <i class="fa btn btn-block btn-primary fa-external-link fa-2x "> <br><span>Enlaces de Interés</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smartenglish" title="Smartenglish Sesion" data-content="smart English" data-acc="smartenglish" <?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                      <i class="fa btn btn-block btn-primary fa-globe fa-2x "><br> <span> smartEnglish</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="foro" title="Foros" data-content="foros" data-acc="foros">
                      <i class="fa btn btn-block btn-primary fa-comments fa-2x "><br> <span> Foros</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="zoom" title="Zoom" data-content="zoom" data-acc="zoom">
                      <i class="fa btn btn-block btn-primary fa-video-camera fa-2x "><br> <span> Zoom</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="videoconferencia" title="enlace de video conferencias" data-content="videoconferencia" data-acc="videoconferencia">
                      <i class="fa btn btn-block btn-primary fa-video-camera fa-2x "><br> <span> Enlace de video conferencias</span></i>
                    </div>

                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="enlacecolaborativo" title="Recurso Colaborativo" data-content="Enlace colaborativo" data-acc="enlacecolaborativo">
                      <i class="fa btn btn-block btn-primary fa-american-sign-language-interpreting fa-2x "><br> <span> Recurso colaborativo</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="enlaceyoutube" title="Enlace Youtube" data-content="" data-acc="enlaceyoutube">
                      <i class="fa btn btn-block btn-primary fa-youtube-play fa-2x "><br> <span> Enlace Youtube</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="tabladeaudios" title="Tabla de Audios" data-content="" data-acc="tabladeaudios">
                      <i class="fa btn btn-block btn-primary fa-headphones fa-2x "><br> <span> Tabla de Audios</span></i>
                    </div>
                    <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="multioptions" title="Tabla de multiple Contenido" data-content="" data-acc="multioptions">
                      <i class="fa btn btn-block btn-primary fa-film fa-2x "><br> <span> Multiple Contenido</span></i>
                    </div>
                  </div>
                  <div class="row" style="margin-top: 1ex;">
                    <div class="col-md-12 text-center text-muted">
                      <button type="button" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> Cerrar </button>
                    </div>
                  </div>
                </div>
              </div>
              <div id="showpaddhabilidad" style="display:none">
                <div class="col-md-12">
                  <div class="row" id="addclonehabilidad">
                    <div class="col-md-12 text-center ">
                      <br><label>Seleccione criterios</label>
                      <hr>

                    </div>
                    <!-- <div class="form-group">
                      <label class="control-label">< ?php echo JrTexto::_('Competencias'); ?> <span class="required"> * </span></label>
                      <div class="">
                        <select id="txtidcompetencia" name="idcompetencias" class="form-control">
                          <option value="">< ?php echo JrTexto::_('Seleccione'); ?></option>
                        </select>
                      </div>
                    </div> -->
                    <!-- <div class="form-group">
                      <label class="control-label">< ?php echo JrTexto::_('Capacidades'); ?> <span class="required"> * </span></label>
                      <div class="">
                        <select id="txtidcapacidad" name="idcapacidad" class="form-control">
                          <option value="">< ?php echo JrTexto::_('Seleccione'); ?></option>
                        </select>
                      </div>
                    </div> -->
                    <div class="form-group">
                      <label class="control-label"><?php echo JrTexto::_('Criterios'); ?> <span class="required"> * </span></label>
                      <div class="">
                        <ul class="to_do" id="listado_criterios">
                      </div>
                    </div>
                  </div>
                  <div class="row" style="margin-top: 1ex;">
                    <div class="col-md-12 text-center text-muted">
                      <button type="button" class="cerrarmodal btn btn-warning"><i class="fa fa-arrow-left"></i> Cerrar </button>
                      <a href="#" class="btnsavejson_aux btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                    </div>
                  </div>
                </div>
              </div>
              <div id="showaddpuntaje" style="display:none">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12 text-center ">
                      <label>Puntuación</label>
                      <hr>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 table-responsive" id="aquicargatabla">
                      <table class="table table-striped table-hover" id="tablenuevapuntuacion">
                        <thead>
                          <tr>
                            <th>Item</th>
                            <th>Prefijo</th>
                            <th>Grupo</th>
                            <th class="haygrupo">Valor de grupo</th>
                            <th>Valor de Item</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr id="" class="trclone" style="display: none;">
                            <td class="punitem" style="text-align: left"></td>
                            <td><input name="punprefijo" maxlength="3" class="punprefijo form-control" placeholder="Ej. EF"></td>
                            <td>
                              <div class=" select-ctrl-wrapper select-azul ">
                                <select name="pungrupo" class="pungrupo" class="form-control">
                                  <option value="N">Ninguno</option>
                                  <option value="U1">Unidad 1</option>
                                  <option value="U2">Unidad 2</option>
                                  <option value="U3">Unidad 3</option>
                                  <option value="U4">Unidad 4</option>
                                  <option value="U5">Unidad 5</option>
                                  <option value="S1">Sesion 1</option>
                                  <option value="S2">Sesion 2</option>
                                  <option value="S3">Sesion 3</option>
                                  <option value="S4">Sesion 4</option>
                                  <option value="S5">Sesion 5</option>
                                  <option value="S6">Sesion 6</option>
                                  <option value="S7">Sesion 7</option>
                                  <option value="S8">Sesion 8</option>
                                  <option value="S9">Sesion 9</option>
                                  <option value="S10">Sesion 10</option>
                                  <option value="PRA">Practica</option>
                                  <option value="PD">Practica Dirigida</option>
                                  <option value="PC">Proctica Calificada</option>
                                  <option value="EX">Examen</option>
                                  <option value="EE">Examen de Entrada</option>
                                  <option value="EF">Examen Final</option>
                                  <option value="TA">Tareas</option>
                                  <option value="PR">Proyectos</option>
                                </select>
                              </div>
                            </td>
                            <td class="haygrupo">
                              <div class=" select-ctrl-wrapper select-azul ">
                                <select name="pungruporcentaje" class="pungruporcentaje" class="form-control">
                                  <option value="P">Promedio</option>
                                  <?php for ($i = 99; $i > 0; $i--) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
                                  <?php } ?>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class=" select-ctrl-wrapper select-azul ">
                                <select name="punporcentaje" class="punporcentaje" class="form-control">
                                  <option value="P">Promedio</option>
                                  <?php for ($i = 99; $i > 0; $i--) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
                                  <?php } ?>
                                </select>
                              </div>
                            </td>
                            <td><i class="fa fa-trash btnborrar"></i> </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-12">
                      <hr>
                      <h3>Formula Generada</h3>
                      <div class="formulagenerada">
                      </div>
                      <hr>
                    </div>
                  </div>
                  <div class="row" style="margin-top: 1ex;">
                    <div class="col-md-12 text-center text-muted">
                      <button type="button" class="btnguardarpuntaje btn btn-success"><i class="fa fa-save"></i> Guardar </button>
                      <button type="button" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> Cerrar </button>
                    </div>
                  </div>
                </div>
              </div>
              <div id="showpaddmultivideos" style="display: none;">
                <div class="row ">
                  <div class="col-md-12 table-responsive">
                    <div class="col-md-12">
                      <caption><?php echo JrTexto::_('Mostrar como') ?></caption>
                      <select id="tipomenu" name="tipomenu" class="form-control">
                        <option value="slider"><?php echo JrTexto::_('slider'); ?></option>
                        <option value="menu"><?php echo JrTexto::_('Menu'); ?></option>
                      </select>
                    </div>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo JrTexto::_('Name'); ?></th>
                          <th><?php echo JrTexto::_('Video'); ?></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="trvideoclone" style="display: none;">
                          <td></td>
                          <td><input type="" name="nom" class="form-control" value="Video"></td>
                          <td style="position: relative;">
                            <div><a href="#" class="btn btn-warning btnsubirvideo"><i class="fa fa-file"></i> </a></div>
                          </td>
                          <td> <i class="imove fa fa-arrow-up" adonde="up" style="cursor: pointer"></i> <i class="imove fa fa-arrow-down" adonde="down" style="cursor: pointer"></i> <i class="iborrar fa fa-trash" style="cursor: pointer"></i> </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-12 text-center">
                    <a href="#" class="btnaddvideo btn btn-warning"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></a>
                    <a href="#" class="btnsavejson btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                    <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                  </div>
                </div>
              </div>
              <div id="showaddrubricas" style="display: none;">
                <div id="fix-md" class="row ">
                  <div class="col-md-12 table-responsive my-font-all">
                    <!-- working -->
                    <link rel="stylesheet" href="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/mine/css/soft-rw-theme-paris.css">
                    <link rel="stylesheet" href="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/mine/css/mine.css">
                    <link rel="stylesheet" href="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/rubrica/mine-rubrica.css">
                    <script src="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/mine/js/mine.js"></script>
                    <script src="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/rubrica/mine-rubrica.js"></script>
                    <input id="input-idinstancia" class="my-hide" type="text" disabled>
                    <div class="col-sm-12 my-x-center">
                      <select id="selectRubrica"></select>
                    </div>
                    <div id="main-cont" class="row col-sm-12">
                      <div class="my-loader my-color-green"></div>
                    </div>
                    <div class="my-hide row col-sm-12">
                      <div id="row-cont-rubrica" class="col-sm-12 row-cont-rubrica">
                        <div class="cont-rubrica my-card my-shadow">
                          <h3 onclick="modalRubrica(this)" class="text-center my-full-w">Titulo de rubrica</h3>
                          <div id="out-cont" class="out-cont">
                            <div id="body-rubrica" class="body-cont-rubrica rubrica-black tema-4">
                              <div id="rubrica-code" class="cod-rubrica">
                                <div class="code-text">Código</div>
                                <div class="code-num"></div>
                              </div>
                              <div id="cont-btn-crit" onclick="modalCriterio(this)" class="cont-btn-crit">
                                <div class="criterio-text">Detalle</div>
                                <div class="criterio-num">5%</div>
                              </div>
                              <div id="cont-btn-nivel" onclick="modalNivel(this)" name="cont-btn-nivel" class="cont-btn-nivel">
                                <div class="nivel-text">Excelente</div>
                                <div class="nivel-num">20</div>
                              </div>
                              <div id="cont-btn-indicador" onclick="modalIndicador(this)" class="cont-btn-indicador">
                                <div class="indicador-text">Detalle</div>
                              </div>
                              <button id="btn-new-indicador" onclick="modalIndicador(this)" class="btn-new cont-btn-indic">
                                <span class="btn-icon">+</span>
                                <span class="btn-text">Indicador</span>
                              </button>
                              <button id="btn-new-criterio" onclick="modalCriterio(this)" class="cont-btn-crit btn-new">
                                <span class="btn-icon">+</span>
                                <span class="btn-text">Criterio</span>
                              </button>
                              <button id="btn-new-nivel" onclick="modalNivel(this)" class="cont-btn-nivel btn-new">
                                <span class="btn-icon">+</span>
                                <span class="btn-text">Nivel</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 text-center">
                    <a href="#" class="btnsaverubrica btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                    <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                  </div>
                </div>
              </div>
              <div id="showtabladeaudios" style="display: none;">
                <div class="row ">
                  <div class="col-md-12 table-responsive">
                    <div class="col-md-12 text-center form-group">
                      <label>Titulo de la tabla:</label>
                      <input type="text" name="titulo" placeholder="Titulo de la tabla" class="titulo form-control">
                    </div>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo JrTexto::_('Name'); ?></th>
                          <th><?php echo JrTexto::_('Audio'); ?></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="trclone" style="display: none;">
                          <td></td>
                          <td><input type="" name="nom" class="form-control" value="" placeholder="Nombre de Audio"></td>
                          <td style="position: relative;">
                            <div><a href="#" class="btn btn-warning btnsubir"><i class="fa fa-file-audio-o"></i> </a></div>
                          </td>
                          <td> <i class="imove fa fa-arrow-up" adonde="up" style="cursor: pointer"></i> <i class="imove fa fa-arrow-down" adonde="down" style="cursor: pointer"></i> <i class="iborrar fa fa-trash" style="cursor: pointer"></i> </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-12 text-center">
                    <a href="#" class="btnadd btn btn-warning"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></a>
                    <a href="#" class="btnsavejson btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                    <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                  </div>
                </div>
              </div>
              <div id="showvideoconferencia" class="my-hide">
                <div id="fix-md" class="row soft-rw">
                  <div class="cont-conferencia col-sm-12 my-x-center my-font-all">
                    <form id="form-add" class="col-sm-12 col-md-8 my-mg-center" onsubmit="return saveEvent();">
                      <div class="form-group">
                        <label for="input-conf-titulo">Tema de la conferencia</label>
                        <input required type="text" name="" id="input-conf-titulo" class="form-control" placeholder="Tema de la conferencia...">
                      </div>

                      <div class="form-group">
                        <label for="input-conf-link_mod">Link (moderador)</label>
                        <input required type="text" class="form-control" name="" id="input-conf-link_mod" placeholder="Enlace a la videoconferencia...">
                      </div>
                      <div class="form-group">
                        <label for="input-conf-link_visitor">Link (alumno)</label>
                        <input required type="text" class="form-control" name="" id="input-conf-link_visitor" placeholder="Enlace a la videoconferencia...">
                      </div>
                      <div class="form-group">
                        <label for="fecha">Fecha y hora de inicio</label>
                      </div>
                      <div class="form-group col-sm-12 col-md-6 ">
                        <input required type="date" name="fecha" id="input-conf-fecha" class="form-control" placeholder="">
                      </div>
                      <div class="form-group col-sm-12 col-md-6 ">
                        <input required type="time" name="fecha" id="input-conf-hora_comienzo" class="form-control" placeholder="">
                      </div>

                      <div class="form-group">
                        <label for="input-conf-detalle">Descripción</label>
                        <textarea placeholder="Descripción de la conferencia..." required class="form-control" name="" id="input-conf-detalle" rows="2"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-12 text-center">
                    <a href="#" class="btnsavejson btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                    <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php if ($this->tipocurso == '1') : ?>
            <div id="step-5">
              <!-- <h2 class="StepTitle">Step 4 Content</h2> -->
              <div class="row">
                <div class="col-md-4 form-group">
                  <label style=""><?php echo JrTexto::_('Categorias'); ?></label>
                  <div class="cajaselect">
                    <select name="categorias" id="categoria" class="form-control">
                    </select>
                  </div>
                </div>
                <div class="col-md-4 form-group">
                  <label style=""><?php echo JrTexto::_('Subcategoria'); ?></label>
                  <div class="cajaselect">
                    <select name="subcategorias" id="subcategoria" class="form-control">
                      <option value="0">Todas las subcategorias</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4 form-group">
                  <div><br></div>
                  <span class="btn btn-primary addcategoria"><i class="fa fa-plus"></i> Agregar</span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <table class="table table-striped tablecategorias">
                    <tr>
                      <th colspan="2">Categorias Agregadas</th>
                    </tr>
                    <tr class="trclone" style="display:none">
                      <td class="text-left"></td>
                      <td><i class="fa fa-trash removetcat"></i></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Nueva Rubrica-->
<div class="modal fade my-modal soft-rw my-font-all" id="md-nueva-rubrica" tabindex="-1" role="dialog" aria-labelledby="md-nueva-rubricaLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title">
          Rúbrica
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-no-scroll">
        <form id="form-add-rubrica" onsubmit="return saveRubrica();">
          <input id="input-idrubrica" class="my-hide" type="text" disabled>
          <div class="form-group">
            <label for="nombre-rubrica">Nombre</label>
            <input required type="text" name="nombre-rubrica" id="nombre-rubrica" class="form-control" placeholder="Nombre de la rúbrica...">
          </div>
          <div class="form-group my-hide">
            <select class="form-control" name="" id="selectCompetencia">
              <option disabled value="">Competencia</option>
            </select>
          </div>
          <div class="form-group my-hide">
            <button id="btn-save" onclick="addSelectCompetencia()" type="button" class="btn btn-primary my-float-r">+ Competencia</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-sm-12  col-md-6">
            <div id="autor-btn" class="my-hide">
              <button type="button" onclick="eliminarRubrica()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
              <button id="btn-save" type="submit" form="form-add-rubrica" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div id="default-btn" class="my-hide">
              <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
          <div id="new-btn" class="col-sm-12 my-hide">
            <div class="row my-block">
              <div class="my-float-r">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" form="form-add-rubrica" class="btn btn-primary">Crear</button>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Nivel-->
<div class="modal fade my-modal soft-rw my-font-all" id="md-nivel" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title">
          Nivel
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-no-scroll">
        <form id="form-add-nivel" onsubmit="return saveNivel();">
          <input id="input-idnivel" class="my-hide" disabled type="text">
          <div class="form-group">
            <label for="cuantitativo">Nivel cualitativo</label>
            <input required type="text" name="cualitativo" id="cualitativo" class="form-control" placeholder="Calificación cualitativa...">
          </div>
          <div class="form-group">
            <label for="cuantitativo">Nivel cuantitativo</label>
            <input required type="number" name="cuantitativo" id="cuantitativo" class="form-control" placeholder="Calificación cuantitativa...">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-sm-12  col-md-6">
            <div id="autor-btn" class="my-hide">
              <button type="button" onclick="eliminarNivel()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
              <button id="btn-save" type="submit" form="form-add-nivel" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div id="default-btn" class="my-hide">
              <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
          <div id="new-btn" class="col-sm-12 my-hide">
            <div class="row my-block">
              <div class="my-float-r">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" form="form-add-nivel" class="btn btn-primary">Crear</button>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Criterio-->
<div class="modal fade my-modal soft-rw my-font-all" id="md-criterio" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title">
          Criterio
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-no-scroll">
        <form id="form-add-criterio" onsubmit="return saveCriterio();">
          <input id="input-idcriterio" class="my-hide" disabled type="text">
          <div class="form-group my-hide">
            <select class="form-control" name="" id="selectCapacidad">
              <option disabled value="">Capacidad</option>
            </select>
          </div>
          <div class="form-group">
            <label for="nombre-criterio">Nombre</label>
            <textarea required type="text" name="nombre-criterio" id="nombre-criterio" class="form-control" placeholder="Nombre del criterio..."></textarea>
            <!-- <input required type="text" name="nombre-criterio" id="nombre-criterio" class="form-control" placeholder="Nombre del criterio..."> -->
          </div>
          <div class="form-group">
            <label for="peso-criterio">Peso</label>
            <input type="number" name="peso-criterio" id="peso-criterio" class="form-control" placeholder="Peso del criterio...">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-sm-12  col-md-6">
            <div id="autor-btn" class="my-hide">
              <button type="button" onclick="eliminarCriterio()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
              <button id="btn-save" type="submit" form="form-add-criterio" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div id="default-btn" class="my-hide">
              <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
          <div id="new-btn" class="col-sm-12 my-hide">
            <div class="row my-block">
              <div class="my-float-r">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" form="form-add-criterio" class="btn btn-primary">Crear</button>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Indicador-->
<div class="modal fade my-modal soft-rw my-font-all" id="md-indicador" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title">
          Indicador
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-no-scroll">
        <form id="form-add-indicador" onsubmit="return saveIndicador(this);">
          <input id="input-idindicador" class="my-hide" type="text" disabled>
          <div class="form-group my-hide">
            <select class="form-control" name="" id="selectCriterios">
              <option disabled value="">Buscar Criterio</option>
            </select>
          </div>
          <div class="form-group">
            <label for="detalle-indicador">Detalle</label>
            <textarea required type="text" name="detalle-indicador" id="detalle-indicador" class="form-control" placeholder="Detalle..."></textarea>
            <!-- <input required type="text" name="detalle-indicador" id="detalle-indicador" class="form-control" placeholder="Detalle..."> -->
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-sm-12  col-md-6">
            <div id="autor-btn" class="my-hide">
              <button type="button" onclick="eliminarIndicador()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
              <button id="btn-save" type="submit" form="form-add-indicador" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div id="default-btn" class="my-hide">
              <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
          <div id="new-btn" class="col-sm-12 my-hide">
            <div class="row my-block">
              <div class="my-float-r">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" form="form-add-indicador" class="btn btn-primary">Crear</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo $this->documento->getUrlStatic(); ?>/tema/paris/gentelella/smartWizard/js/jquery.smartWizard.js"></script>
<script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/jquery-nestable/jquery.nestable.min.js"></script>
<script src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/green/icheck.min.js"></script>
<div id="addscript">
  <script type="text/javascript">
    $('ol li').hover(function() {
      $('ol li').removeClass('li-hover');
      $(this).addClass('li-hover');
      $(this).children('ol').css('background-color', 'white');
    }, function() {
      $(this).parent('ol').parent('li').addClass('li-hover');
      $(this).parent('ol').css('background-color', 'white');
      $(this).removeClass('li-hover');
    });
  </script>
</div>
<script type="text/javascript">
  const IDCOMPLEMENTARIO = <?php echo !empty($this->idcomplementario) ? $this->idcomplementario : 0; ?>;
  const IDPERSONA = <?php echo !empty($this->curusuario["idpersona"]) ? $this->curusuario["idpersona"] : 0; ?>;
  const IDROL = <?php echo !empty($this->curusuario["idrol"]) ? $this->curusuario["idrol"] : 0; ?>;
  var configuracion_nota = <?php echo !empty($curso["configuracion_nota"]) ? $curso["configuracion_nota"] : 'false'; ?>;

  var idcurso = <?php echo $this->idcurso ?>;
  var imgdefecto = '<?php echo $imgcursodefecto; ?>';
  var idproyecto = parseInt('<?php echo $this->idproyecto; ?>');
  var url_media = _sysUrlBase_;
  var txtjson = <?php echo !empty($txtjson) ? "JSON.parse(" . $txtjson . ");" : "{
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',imagefondo:'',imagen:'',colorfondo:'rgba(0, 0, 0, 0)'},
    infoindice:'top',infoavance:0}" ?>;

  function onFinishCallback(objs, context) {
    history.back();
  }
  $(document).ready(function() {
    $('#wizard').smartWizard({
      labelNext: '<?php echo JrTexto::_("Siguiente"); ?>',
      labelPrevious: '<?php echo JrTexto::_("Anterior"); ?>',
      labelFinish: '<?php echo JrTexto::_("Finalizar"); ?>',
      onFinish: onFinishCallback
    });
    $('#wizard_verticle').smartWizard({
      transitionEffect: 'slide',
    });
    $('.buttonNext').addClass('btn btn-primary');
    $('.buttonPrevious').addClass('btn btn-warning');
    $('.buttonFinish').addClass('btn btn-default');
    var _infoportada = txtjson.infoportada;
    $('._infoportada[name="titulo"]').val(_infoportada.titulo);
    $('._infoportada[name="descripcion"]').val(_infoportada.descripcion);
    var imagenfondo = _infoportada.imagenfondo == undefined ? imgdefecto : _sysUrlBase_ + _infoportada.imagenfondo;
    $('._infoportada img[name="imagenfondo"]').attr('src', imagenfondo || imgdefecto);
    var colordefondo = _infoportada.colorfondo || 'rgba(0,0,0,0)';
    $('._infoportada[name="color"]').val(colordefondo);
    $('._infoportada[name="color"]').minicolors({
      opacity: true,
      format: 'rgb'
    });
    $('._infoportada[name="color"]').minicolors('settings', {
      value: colordefondo
    });

    __guardavalorcurso__ = function() {
      var formElement = document.getElementById("frmdatosdelcurso");
      var data = new FormData(formElement);
      data.append('txtjson', JSON.stringify(txtjson));
      __sysAyax({
        fromdata: data,
        url: _sysUrlBase_ + 'smart/acad_curso/guardarjson',
        callback: function(rs) {
          if (rs.code == 200) {
            idcurso = rs.newid;
            $('#idcurso').val(idcurso);
            var formData = new FormData();
            formData.append('idcurso', idcurso);
            formData.append('idproyecto', idproyecto);
            __sysAyax({
              showmsjok: false,
              fromdata: formData,
              url: _sysUrlSitio_ + '/proyecto_cursos/guardarProyecto_cursos',
              callback: function(rs) {
                validarcurso();
              }
            })
          }
        }
      });
    }

    __guardavalorcurso = function(el, c, v) {
      var id = parseInt($('#idcurso').val());
      if (id > 0) {
        var data = new FormData();
        data.append('campo', c);
        data.append('valor', v);
        data.append('idcurso', id);
        data.append('icc', <?php echo $this->idcomplementario; ?>);
        __sysAyax({
          fromdata: data,
          url: _sysUrlBase_ + 'json/acad_curso/setcampo',
          callback: function(rs) {
            el.attr('inchange', '');
            el.attr('oldvalor', '');
          }
        })
      } else {
        if ("<?php echo $this->tipocurso; ?>" == "1") {
          if ($('#idcurso').val() == "0") {
            Swal.fire({
              title: '<?php echo JrTexto::_('Confirme'); ?>',
              text: '<?php echo JrTexto::_('¿Es un curso complementario?'); ?>',
              icon: 'warning',
              showCancelButton: true,
              allowOutsideClick: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo JrTexto::_('Si'); ?>',
              cancelButtonText: '<?php echo JrTexto::_('No'); ?>',
            }).then((result) => {
              if (result.value) {
                $("#sks").val("0");
                $("#tipo").val("2");
              } else {
                $("#sks").val("1");
                $("#tipo").val("1");
              }
              __guardavalorcurso__();
            });
          } else {
            __guardavalorcurso__();
          }
        } else {
          __guardavalorcurso__();
        }
      }
    };

    <?php if ($this->validar) { ?>
      var _el_ = $("._autor_")
      var _campo_ = _el_.attr('name') || '';
      var _value_ = _el_.val() || _el_.attr('value');
      __guardavalorcurso(_el_, _campo_, _value_);
    <?php  } ?>

    $('.guardarvalorcurso').blur(function(e) {
      var el = $(this)
      var campo = el.attr('name') || '';
      var value = $(this).val() || el.attr('value');
      if (el.hasClass('vercolor') || campo == 'autor') value = $(this).val();
      var inchange = el.attr('inchange') || 0;
      var oldvalor = el.attr('oldvalor') || '';
      if (inchange == 0 && campo != 'autor') return;
      __guardavalorcurso(el, campo, value);
    }).click(function(e) {
      var el = $(this);
      var oldvalor = el.attr('oldvalor') || '';
      if (oldvalor == '') el.attr('oldvalor', el.val());
    }).change(function() {
      var el = $(this);
      if (el.val() == el.attr('oldvalor')) el.attr('inchange', 0)
      else el.attr('inchange', 1);
    }).on('click', '.borramendia', function(e) {
      e.preventDefault();
      var el = $(this);
      var c = el.attr('name');
      __guardavalorcurso(el, c, '');
      el.find('.__subirfile').attr(src, imgdefecto);
    }).on('click', '.__subirfile', function(e) {
      e.preventDefault();
      var $img = $(this);
      var idcurso = $('#idcurso').val() || '';
      if (idcurso == 0) idcurso = Date.now();
      <?php
      if ($this->idcomplementario != 0) {
      ?>
        var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : 'c_c/');
        var dirmedia_ = 'cursos_c/' + dirmedia;
      <?php
      } else {
      ?>
        var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
        var dirmedia_ = 'cursos/' + dirmedia;
      <?php
      }
      ?>

      // var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
      var nombre = (idcurso != '' ? ('logo_' + idcurso) : '');
      var oldmedia = $img.attr('src');
      __subirfile({
        file: $img,
        typefile: 'imagen',
        uploadtmp: true,
        guardar: true,
        'dirmedia': dirmedia_,
        'oldmedia': oldmedia,
        'nombre': nombre
      }, function(rs) {
        var f = rs.media.replace(url_media, '');
        $img.attr('src', _sysUrlBase_ + f).attr('oldmedia', _sysUrlBase_ + f).attr('data-link', f);
        var c = $img.attr('id');
        __guardavalorcurso($img.closest('.frmchangeimage'), c, f);
      });
    });

    $('._infoportada').blur(function(e) {
      //var idcurso=parseInt($('#idcurso').val()||'0');
      // if(idcurso==0) return alert('Necesita guardar datos basicos del curso primero');
      var el = $(this);
      var name = el.attr('name');
      if (name == 'color') name = 'colorfondo';
      _infoportada[name] = el.val();
      var inchange = el.attr('inchange') || '';
      if (inchange == '' || inchange == undefined) return;
      txtjson.infoportada = _infoportada;
      __txtjson = JSON.stringify(txtjson);
      __guardavalorcurso(el, 'txtjson', __txtjson);
    }).change(function(e) {
      // var idcurso=parseInt($('#idcurso').val()||'0');
      // if(idcurso==0) return alert('Necesita guardar datos basicos del curso primero');
      var el = $(this);
      var v = el.attr('value') || '_blanco_';
      el.attr('inchange', window.btoa(v));
    }).on('click', '.borramendia', function(e) {
      e.preventDefault();
      var el = $(this);
      _infoportada['imagenfondo'] = '';
      txtjson.infoportada = _infoportada;
      __txtjson = JSON.stringify(txtjson);
      __guardavalorcurso(el, 'txtjson', __txtjson);
      el.closest('._infoportada').find('.__subirfile').attr(src, imgdefecto);
    }).on('click', '.__subirfile', function(e) {
      e.preventDefault();
      var $img = $(this);
      var idcurso = parseInt($('#idcurso').val() || '0');
      // if(idcurso==0) return alert('Necesita guardar datos basicos del curso primero');
      // var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : '');
      <?php if ($this->idcomplementario != 0) {  ?>
        var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : '');
        var dirmedia_ = 'cursos_c/' + dirmedia;
      <?php } else {  ?>
        var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : '');
        var dirmedia_ = 'cursos/' + dirmedia;
      <?php } ?>
      var nombre = (idcurso != '' ? ('fondoportada_' + idcurso) : '');
      var oldmedia = $img.attr('src');
      __subirfile({
        file: $img,
        typefile: 'imagen',
        uploadtmp: true,
        guardar: true,
        'dirmedia': dirmedia_,
        'oldmedia': oldmedia,
        'nombre': nombre
      }, function(rs) {
        var f = rs.media.replace(url_media, '');
        //console.lof(f);
        $img.attr('src', _sysUrlBase_ + f).attr('oldmedia', _sysUrlBase_ + f);
        var c = $img.attr('id');
        _infoportada['imagenfondo'] = f;
        txtjson.infoportada = _infoportada;
        __txtjson = JSON.stringify(txtjson);
        __guardavalorcurso($img.closest('._infoportada'), 'txtjson', __txtjson);
      });
    })

    var cambiarcolor = function() {
      $('input[name="color"]').each(function(i, v) {
        var v = $(this).val() || 'rgba(76, 80, 84, 0.75)';
        $(this).val(v);
        $(this).minicolors({
          opacity: true,
          format: 'rgb'
        });
        $(this).minicolors('settings', {
          value: v
        });
      })
    }
    cambiarcolor();

    $('#_puntuacioncurso_').on('change', '#calificacionen', function(ev) {
      ev.preventDefault();
      _this = $(this);
      $row = _this.closest('.row');
      val = _this.val();
      if (val == 'N') {
        $row.find('.pornum').show();
        $row.siblings('.row').hide();
        $row.find('label > span').hide();
      } else if (val == 'P') {
        $row.find('.pornum').show();
        $row.siblings('.row').hide();
        $row.find('label > span').show();
      } else {
        $row.find('.pornum').hide();
        $row.siblings('.row').show();
      }
    }).on('change', 'table .minpuntaje', function() {
      $v = $(this).val();
      $tr = $(this).closest('tr');
      $body = $(this).closest('tbody')
      if ($v == 0) $tr.nextAll().remove();
      else reordenartr($body, $tr);
    }).on('change', 'table .maxpuntaje', function() {
      _this = $(this);
      $v = $(this).val();
      $smin = _this.closest('tr').find('.minpuntaje');
      $vmin = $smin.val();
      $smin.children().remove();
      for (var i = 0; i < $v; i++) {
        $smin.append('<option value="' + i + '">' + i + '</option>');
      }
      $smin.children('option[value="' + $vmin + '"]').attr('selected', true);
      $smin.trigger('change');
    }).on('change', '.pornum .maxpuntaje', function() {
      _this = $(this);
      $v = $(this).val();
      $smin = _this.closest('#rownumber').find('.minpuntaje');
      $vmin = $smin.val();
      $smin.children().remove();
      for (var i = 0; i < $v; i++) {
        $smin.append('<option value="' + i + '">' + i + '</option>');
      }
      $smin.children('option[value="' + $vmin + '"]').attr('selected', true);
    })

    var reordenartr = function(tbody, tr) {
      $v = tr.find('select#minpuntaje').val();
      haynex = tr.next();
      $trclone = tbody.find('tr').first().clone(true);
      if ($v <= 2) return tr.nextAll().remove();
      if (haynex.length == 0) {
        $trclone.find('input.txtNombreEscala').val('');
        $trclone.find('select#maxpuntaje').find('option[value="' + ($v - 1) + '"]').prevAll().remove();
        $trclone.find('select#maxpuntaje').attr('disabled', true).css({
          'background': '#ccc'
        });
        $trclone.find('select#minpuntaje').find('option[value="' + ($v - 2) + '"]').nextAll().remove();
        $body.append($trclone);
      } else {
        $vmax1 = haynex.find('select#maxpuntaje').val();
        $trclone.find('select#maxpuntaje').find('option[value="' + ($v - 1) + '"]').prevAll().remove();
        haynex.find('select#maxpuntaje').children().remove();
        haynex.find('select#maxpuntaje').append($trclone.find('select#maxpuntaje').children());
        haynex.find('select#maxpuntaje').children().first().attr('selected', true);
        $vmin1 = haynex.find('select#minpuntaje option').last().text();
        for ($i = $vmin1; $i < $vmax1; $i++) {
          haynex.find('select#minpuntaje').append('<option value="' + ($i + 1) + '">' + ($i + 1) + '</option>');
        }
        haynex.find('select#maxpuntaje').trigger('change');
        haynex.find('select#minpuntaje').trigger('change');
      }
    }

    if (configuracion_nota != false) {
      var tipocal = configuracion_nota.tipocalificacion || 'P';
      $('#_puntuacioncurso_').find('#calificacionen').val(tipocal);
      if (tipocal == 'P' || tipocal == 'N') {
        $('#_puntuacioncurso_').find('#rownumber select.maxpuntaje').val(configuracion_nota.maxcal || 20);
        $('#_puntuacioncurso_').find('#rownumber select.minpuntaje').val(configuracion_nota.mincal || 14);
      } else {
        $('#_puntuacioncurso_').find('#calificacionen').trigger('change');
        var punt = configuracion_nota.puntuacion;
        var tr = $('#_puntuacioncurso_').find('table.tablepuntuacion tbody').find('tr')
        $.each(punt, function(i, v) {
          tr.find('.txtNombreEscala').val(v.nombre);
          tr.find('select.maxpuntaje').val(v.maxcal).trigger('change');
          tr.find('select.minpuntaje').val(v.mincal);
          if (v.mincal > 0) {
            tr.find('select.minpuntaje').trigger('change');
            tr = tr.next();
          }
        })
      }
      configuracion_nota.notas = configuracion_nota.notas == undefined ? [] : configuracion_nota.notas;
    } else {
      $('#_puntuacioncurso_').find('#calificacionen').val('N');
      $('#_puntuacioncurso_').find('#rownumber select.maxpuntaje').val(20);
      $('#_puntuacioncurso_').find('#rownumber select.minpuntaje').val(14);
      configuracion_nota = {
        tipocalificacion: 'P',
        maxcal: 100,
        mincal: 51,
        notas: []
      };
    }
    $("#_crearcurso_").on("onhide", 'a[href="#step-3"]', function() {
      var json = {  tipocalificacion: 'P',  maxcal: 100,  mincal: 51  };
      var tipocal = $('#_puntuacioncurso_').find('#calificacionen').val();
      configuracion_nota.tipocalificacion = tipocal;
      if (tipocal == 'P' || tipocal == 'N') {
        configuracion_nota.maxcal = $('#_puntuacioncurso_').find('#rownumber select.maxpuntaje').val() || 20;
        configuracion_nota.mincal = $('#_puntuacioncurso_').find('#rownumber select.minpuntaje').val() || 13;
      } else {
        configuracion_nota.puntuacion = [];
        $('#_puntuacioncurso_').find('table.tablepuntuacion tbody tr').each(function(i, v) {
          var tr = $(this);
          var escale = tr.find('.txtNombreEscala').val();
          if (escale == '') {
            tr.find('.txtNombreEscala').css({
              border: '1px solid red'
            });
            __notificar({
              title: '<?php echo JrTexto::_('Attention'); ?>',
              html: '<?php echo JrTexto::_('Error in data') ?>',
              type: 'error'
            });
            hayerror = true;
            return;
          } else tr.find('.txtNombreEscala').css({
            border: '1px solid #4683af'
          });
          configuracion_nota.puntuacion.push({
            nombre: escale,
            maxcal: tr.find('select.maxpuntaje').val(),
            mincal: tr.find('select.minpuntaje').val()
          });
        })
      }
      var data = new FormData()
      data.append('idcurso', idcurso);
      data.append('campo', 'configuracion_nota');
      data.append('valor', JSON.stringify(configuracion_nota));
      __sysAyax({
        fromdata: data,
        url: _sysUrlBase_ + 'json/proyecto_cursos/setCampoxCurso',
        showmsjok: false,
      });
    })

    __ocultarsinoescalificado = function(elli) {
      var activarcalificar = elli.children('.accionesdecurso').children('.btnestarea').hasClass('bg-success') == true ? true : false;
      if (activarcalificar == false) activarcalificar = elli.children('.accionesdecurso').children('.btnesproyecto').hasClass('bg-success') == true ? true : false;
      if (activarcalificar == true) {
        // working
        elli.addClass('conrubrica');
      } else {
        elli.removeClass('conrubrica');
        // actulaizarjson(elli);
        removeInstancia(elli);
        elli.children('.accionesdecurso').children('.btnaddrubricas').removeClass('bg-success');
      }
      if (activarcalificar == false) activarcalificar = elli.attr('txtjson-typelink') == 'smartquiz' ? true : false;
      if (activarcalificar == false) activarcalificar = elli.attr('txtjson-typelink') == 'foro' ? true : false;
      if (activarcalificar == false) activarcalificar = elli.attr('txtjson-typelink') == 'enlacecolaborativo' ? true : false;
      if (activarcalificar == false) activarcalificar = elli.attr('type') == 'smartquiz' ? true : false;
      if (activarcalificar == false) activarcalificar = elli.attr('type') == 'foro' ? true : false;
      if (activarcalificar == false) activarcalificar = elli.attr('type') == 'enlacecolaborativo' ? true : false;
      if (activarcalificar == true) {
        elli.addClass('concalificacion');
      } else {
        elli.removeClass('concalificacion');
      }
    }

    $('#_contentcurso_').on('click', '.btnopenmodal , .btneditmenu', function(e) {
      e.preventDefault();
      var el = $(this);
      var dt = {
        html: $('#_infoTema_').html()
      };
      var paso3 = $('#nestable');
      var liclonado = paso3.find('li.liclone').clone(true);
      var md = __sysmodal(dt);
      if (!el.hasClass('btnopenmodal')) {
        liclonado = el.closest('li');
      } else {
        liclonado.removeClass('liclone').show();
        liclonado.attr('orden', paso3.find('li').length);
      }

      md.find('textarea[name="descripcion"]').val(liclonado.attr('descripcion') || '');
      md.find('input[name="nombre"]').val(liclonado.children('span.titulo').text() || '');
      md.find('input[name="color"]').val(liclonado.attr('color'));
      md.find('input[name="color"]').minicolors({
        opacity: true,
        format: 'rgb'
      });
      md.find('input[name="color"]').minicolors('settings', {
        value: liclonado.attr('color')
      });

      var _liimg = (liclonado.attr('imagen') || imgdefecto);
      var _indeximg = _liimg.lastIndexOf('static/');
      if (_indeximg > -1) _liimg = _liimg.substring(_indeximg);
      _liimg = (_liimg == undefined || _liimg == '') ? imgdefecto : _sysUrlBase_ + _liimg;
      md.find('img.__subirfile').attr('src', _liimg).attr('data-link', liclonado.attr('imagen'));

      md.on('click', '.btnguardartema', function(ev) {
        ev.preventDefault();
        var idcurso = $('#idcurso').val();
        var formData = new FormData();
        formData.append('idcurso', parseInt(idcurso));
        formData.append('idcursodetalle', parseInt(liclonado.attr('idcursodetalle') || 0));
        formData.append('idrecursopadre', parseInt(liclonado.attr('idrecursopadre') || 0));
        formData.append('idrecursoorden', parseInt(liclonado.attr('idrecursoorden') || 0));
        formData.append('orden', parseInt(liclonado.attr('orden') || 0));
        formData.append('idrecurso', liclonado.attr('idrecurso') || 0);
        formData.append('tiporecurso', liclonado.attr('tiporecurso') || 'M');
        formData.append('idlogro', liclonado.attr('idlogro') || 0);
        formData.append('url', liclonado.attr('url') || '');
        formData.append('idpadre', liclonado.attr('idpadre') || 0);
        formData.append('color', md.find('input[name="color"]').val());
        formData.append('esfinal', liclonado.attr('esfinal') || 0);
        formData.append('descripcion', md.find('textarea[name="descripcion"]').val());
        formData.append('nombre', md.find('input[name="nombre"]').val());
        formData.append('imagen', md.find('img.__subirfile').attr('data-link') || '');
        formData.append('accimagen', liclonado.attr('accimagen') || 'sg');
        formData.append('icc', <?php echo $this->idcomplementario; ?>);
        __sysAyax({
          fromdata: formData,
          url: _sysUrlBase_ + 'smart/acad_cursodetalle/guardarMenu',
          callback: function(rs) {
            var dt = rs.newid;
            liclonado.children('span.titulo').text(md.find('input[name="nombre"]').val());
            liclonado.attr('descripcion', md.find('textarea[name="descripcion"]').val());
            liclonado.attr('idcursodetalle', dt.idcursodetalle);
            liclonado.attr('idnivel', dt.idnivel);
            liclonado.attr('idpadre', dt.idpadre);
            liclonado.attr('data-id', dt.orden);
            liclonado.attr('imagen', md.find('img.__subirfile').attr('data-link'));
            liclonado.attr('ordennivel', dt.ordennivel);
            if (el.hasClass('btnopenmodal')) {
              var ultimoli = paso3.children('ol').find('li').last();
              liclonado.insertBefore(ultimoli);
            }
            __cerrarmodal(md, true);

          }
        });
      }).on('click', '.borramendia', function(e) {
        e.preventDefault();
        var el = $(this);
        el.closest('.frmchangeimage').find('.__subirfile').attr(src, imgdefecto).attr('data-link', '');
      }).on('click', '.__subirfile', function(e) {
        e.preventDefault();
        var $img = $(this);
        var idcurso = $('#idcurso').val() || '';
        // var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : '');
        <?php
        if ($this->idcomplementario != 0) {
        ?>
          var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : '');
          var dirmedia_ = 'cursos_c/' + dirmedia;
        <?php
        } else {
        ?>
          var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : '');
          var dirmedia_ = 'cursos/' + dirmedia;
        <?php
        }
        ?>
        var nombre = (idcurso != '' ? ('imgses_' + Date.now() + idcurso) : '');
        var oldmedia = $img.attr('src');
        __subirfile({
          file: $img,
          typefile: 'imagen',
          uploadtmp: true,
          guardar: true,
          'dirmedia': dirmedia_,
          'oldmedia': oldmedia,
          'nombre': nombre
        }, function(rs) {
          var f = rs.media.replace(url_media, '');
          $img.attr('src', _sysUrlBase_ + f).attr('oldmedia', _sysUrlBase_ + f).attr('data-link', f);
        });
      })
    }).on('click', '.btnremovemenu', function(e) {
      Swal.fire({
        title: '<?php echo JrTexto::_('Confirme'); ?>',
        text: '<?php echo JrTexto::_('¿Seguro de eliminar?'); ?>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '<?php echo JrTexto::_('Aceptar'); ?>',
        cancelButtonText: '<?php echo JrTexto::_('Cancelar'); ?>',
      }).then((result) => {
        if (result.value) {
          var $li = $(this).closest('li');
          var idcurso = $('#idcurso').val() || '';
          var formData = new FormData();
          formData.append('idcurso', parseInt(idcurso));
          formData.append('idcursodetalle', parseInt($li.attr('idcursodetalle')));
          formData.append('idrecurso', $li.attr('idrecurso'));
          formData.append('tiporecurso', $li.attr('tipo'));
          formData.append('icc', <?php echo $this->idcomplementario; ?>);
          __sysAyax({
            url: _sysUrlBase_ + 'smart/acad_cursodetalle/eliminarMenu',
            fromdata: formData,
            type: 'json',
            showmsjok: false,
            callback: function(rs) {
              $li.remove();
            }
          });
        }
      });
    }).on('click', '.btnrecursopagado , .btnconteacher , .btnestarea , .btnesproyecto , .iconproyecto, .btnmostrarpregunta , i.mostraricon', function(ev) {
      ev.preventDefault();
      var el = $(this);
      var active = false;
      var li = el.closest('li');
      var ol = li.children('ol');
      var lipadre = li;
      if (el.hasClass('mostraricon')) {
        var icon = el.closest('a').attr('data-icon') || '#';
        var htmlicon = el.closest('a').html();
        el.closest('.btn-group').children('button').html(htmlicon);
        li.attr('data-icon', icon);
      }
      if (el.hasClass('bg-success')) {
        el.removeClass('bg-success')
      } else {
        el.addClass('bg-success')
        active = true;
      }

      if (!li.hasClass('esmenu')) {
        lipadre = li.closest('ol').closest('li');
        ol = lipadre.children('ol');
      }
      //actulaizarjson(lipadre);     
      //preugntar si es tarea o proyecto 
      if (el.hasClass('btnestarea') || el.hasClass('btnesproyecto')) {
        //working
        // return null;
        // cuando da click en icono tarea o proyecto
        __ocultarsinoescalificado(el.closest('li'));

        actulaizarjson(lipadre);

        if (el.hasClass('bg-success')) { //mostrar el alerta          
          if (el.hasClass('btnestarea')) li.find('.btnesproyecto').removeClass('bg-success');
          else li.find('.btnestarea').removeClass('bg-success');
          Swal.fire({
            title: '<?php echo JrTexto::_('¿Seguro de Enviar Correo?'); ?>',
            text: 'Al, aceptar enviara un correo a todos los estudiantes matriculados de la asignación actual',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '<?php echo JrTexto::_('Aceptar'); ?>',
            cancelButtonText: '<?php echo JrTexto::_('Cancelar'); ?>',
          }).then((result) => {
            if (result.value) {
              //si acepta enviar correo
              var _URL = (el.hasClass('btnestarea')) ? String.prototype.concat(_sysUrlBase_, 'json/sendemail/tareasasignada') : String.prototype.concat(_sysUrlBase_, 'json/sendemail/proyectosasignada');
              var _idcursodetalle = null;
              if (typeof lipadre.attr('idcursodetalle') == 'undefined') {
                _idcursodetalle = lipadre.closest('ol').closest('li').attr('idcursodetalle');
              } else {
                _idcursodetalle = lipadre.attr('idcursodetalle');
              }
              formData.append('idcursodetalle', _idcursodetalle);
              formData.append('nombretarea', lipadre.find('.titulo').text());
              __sysAyax({
                url: _URL,
                fromdata: formData,
                type: 'json',
                showmsjok: true,
                callback: function(rs) {
                  console.log("correo enviado");
                }
              });
            } //endif de aceptar el dialogo
          });
        }
      } else {
        //no es tarea y proyectos
        if (el.hasClass('mostraricon')) {
          var icon = el.closest('a').attr('data-icon') || '#';
          var htmlicon = el.closest('a').html();
          el.closest('.btn-group').children('button').html(htmlicon);
          li.attr('data-icon', icon);
        }
        actulaizarjson(lipadre);
      }
    }).on('click', '.btnaddoption, .btneditoption', function(e) {
      var el = $(this);
      var dt = {
        html: $('#_infoTema_').html()
      };
      var paso3 = $('#nestable');
      var md = __sysmodal(dt);
      var liclonado = '';
      var lipadre = el.closest('.esmenu');
      var idcursodetalle = lipadre.attr('idcursodetalle');
      md.find('textarea[name="descripcion"]').closest('.form-group').remove();
      if (el.hasClass('btnaddoption')) {
        md.find('input[name="nombre"]').val('');
        md.find('input[name="color"]').val('rgba(32, 50, 69, 0)');
        md.find('input[name="color"]').minicolors({
          opacity: true,
          format: 'rgb'
        });
        md.find('input[name="color"]').minicolors('settings', {
          value: 'rgba(32, 50, 69, 0)'
        });
        md.find('img.__subirfile').attr('src', imgdefecto).attr('data-link', '');
      } else {
        var liclonado = el.closest('li');
        md.find('input[name="nombre"]').val(liclonado.find('span.titulo').text() || '');
        md.find('input[name="color"]').val(liclonado.attr('color'));
        md.find('input[name="color"]').minicolors({
          opacity: true,
          format: 'rgb'
        });
        md.find('input[name="color"]').minicolors('settings', {
          value: liclonado.attr('color')
        });
        var _liimg = liclonado.attr('imagen') || imgdefecto;
        var _indeximg = _liimg.lastIndexOf('static/');
        if (_indeximg > -1) _liimg = _liimg.substring(_indeximg);
        _liimg = (_liimg == undefined || _liimg == '') ? imgdefecto : _sysUrlBase_ + _liimg;
        md.find('img.__subirfile').attr('src', _liimg).attr('data-link', liclonado.attr('imagen'));
      }
      md.on('click', '.btnguardartema', function(ev) {
        ev.preventDefault();
        var idcurso = $('#idcurso').val();
        var k = lipadre.attr('number') || idcursodetalle;
        lipadre.attr('numbre', k);
        if (lipadre.children('ol').length == 0) {
          lipadre.append('<ol class="dd-list' + k + ' dd' + k + ' itemchange1" number="' + k + '"  id="nestable' + k + '" idcursodetalle="' + idcursodetalle + '" ></ol>')
        }
        ol = lipadre.children('ol');
        var nli = ol.children('li').length;
        if (el.hasClass('btnaddoption')) {
          ol.append('<li class="dd-item' + k + '" data-id="' + idcursodetalle + nli + '" id="' + idcursodetalle + '_' + nli + '" type="" link="" color="' + md.find('input[name="color"]').val() + '" imagen="' + md.find('img.__subirfile').attr('data-link') + '" style="list-style: none; border-bottom: 1px solid #eee;"><i class=" btn btn-xs fa fa-arrows dd-handle' + k + '"  title="<?php echo JrTexto::_('Move'); ?>" style="cursor: move; display: inline-block; margin-top: 0px;"></i>  <span class="titulo">' + md.find('input[name="nombre"]').val() + '</span><span class="btn-group accionesdecurso" style="position: absolute; right: 0px;"> <i class="btn btn-xs fa fa-magic btnaddhabilidades" title="<?php echo JrTexto::_('Add habilidades'); ?>"></i><i class="btn btn-xs fa fa-database btnpuntaje" title="<?php echo JrTexto::_('Puntaje'); ?>"></i> <i class="btn btn-xs fa fa-archive btnaddrubricas" title="<?php echo JrTexto::_('Add Rubrica'); ?>" ></i> <i class="btn btn-xs fa fa-question btnmostrarpregunta" title="<?php echo JrTexto::_('Show questions'); ?>"></i> <i class="btn btn-xs fa fa-file btnaddcontent" title="<?php echo JrTexto::_('Add content'); ?>"></i> <!--i class="btn btn-xs fa fa-film btnaddvideos" title="<?php echo JrTexto::_('Add Video'); ?>"></i--> <!--i class="btn btn-xs fa fa-youtube-play btnaddvideosyoutube" title="<?php echo JrTexto::_('Add Video Youtube'); ?>"></i--> <i class="btn btn-xs fa fa-tasks btnestarea" title="<?php echo JrTexto::_('Es una tarea'); ?>"></i><i class="btn btn-xs fa fa-folder btnesproyecto" title="<?php echo JrTexto::_('Es un Proyecto'); ?>"></i><i class="btn btn-xs fa fa-dollar btnrecursopagado" title="<?php echo JrTexto::_('Es un Recurso Pagado'); ?>"<?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>></i><i class="btn btn-xs fa fa-pencil btneditoption" title="<?php echo JrTexto::_('Edit'); ?>"></i><i class="btn btn-xs fa fa-trash btnremoveoption" title="<?php echo JrTexto::_('Remove'); ?>"></i></span></li>');
          el.siblings('.btnaddcontent').hide();
          lipadre.addClass('tienehijos');
          _nestablehijos();
        } else {
          var liclonado = el.closest('li');
          liclonado.find('span.titulo').text(md.find('input[name="nombre"]').val());
          liclonado.attr('color', md.find('input[name="color"]').val());
          liclonado.attr('imagen', md.find('img.__subirfile').attr('data-link'));
        }
        var addscript = `
            $('ol li').hover(function () {
                $('ol li').removeClass('li-hover');
                $(this).addClass('li-hover');
                $(this).children('ol').css('background-color', 'white');
            }, function () {
                $(this).parent('ol').parent('li').addClass('li-hover');
                $(this).parent('ol').css('background-color', 'white');
                $(this).removeClass('li-hover');
            });
        `;
        $("#addscript").children("script").html(addscript);
        __cerrarmodal(md, true);
        actulaizarjson(lipadre);

      }).on('click', '.borramendia', function(e) {
        e.preventDefault();
        var el = $(this);
        el.closest('.frmchangeimage').find('.__subirfile').attr(src, imgdefecto).attr('data-link', '');
      }).on('click', '.__subirfile', function(e) {
        e.preventDefault();
        var $img = $(this);
        var idcurso = $('#idcurso').val() || '';
        <?php
        if ($this->idcomplementario != 0) {
        ?>
          // var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '/') : '');
          var dirmedia = 'c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/sesion_' + idcursodetalle + '/';
          var dirmedia_ = 'cursos_c/' + dirmedia;
        <?php
        } else {
        ?>
          var dirmedia = 'curso_' + idcurso + '/sesion_' + idcursodetalle + '/';
          var dirmedia_ = 'cursos/' + dirmedia;
        <?php
        }
        ?>
        // var dirmedia = 'curso_' + idcurso + '/sesion_' + idcursodetalle + '/';
        var nombre = 'imgses_' + idcurso + '_' + idcursodetalle;
        var oldmedia = $img.attr('src');
        __subirfile({
          file: $img,
          typefile: 'imagen',
          uploadtmp: true,
          guardar: true,
          'dirmedia': dirmedia_,
          'oldmedia': oldmedia,
          'nombre': nombre
        }, function(rs) {
          var f = rs.media.replace(url_media, '');
          $img.attr('src', _sysUrlBase_ + f).attr('oldmedia', _sysUrlBase_ + f).attr('data-link', f);
        });
      })
    }).on('click', '.btnremoveoption', function(ev) {
      Swal.fire({
        title: '<?php echo JrTexto::_('Confirme'); ?>',
        text: '<?php echo JrTexto::_('¿Seguro de eliminar?'); ?>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '<?php echo JrTexto::_('Aceptar'); ?>',
        cancelButtonText: '<?php echo JrTexto::_('Cancelar'); ?>',
      }).then((result) => {
        if (result.value) {
          var li = $(this).closest('li');
          var lipadre = li.closest('.esmenu');
          var ol = lipadre.children('ol');
          li.remove();
          if (ol.find('li').length == 0) {
            ol.remove();
            lipadre.find('i.btnaddcontent').show();
            lipadre.removeClass('tienehijos');
          }
          actulaizarjson(lipadre);
        }
      });
    }).on('click', '.btnaddcontent', function(ev) {
      var el = $(this);
      var eltmp_ = $(this);
      var elli = $(this).closest('li');
      var dt = {
        html: $('#showpaddcontenido').html()
      };
      var paso3 = $('#nestable');
      var md = __sysmodal(dt);
      var li = el.closest('.esmenu');
      var idcursodetalle = li.attr('idcursodetalle');
      var idcurso = $('#idcurso').val();
      var __addexamen = function(obj, fncall) {
        //let md=__sysmodal({titulo:'Examenes',html:$('#config_plantilla').html()});        
        var lstmp = 'btnaddexamen' + __idgui();
        obj.addClass(lstmp);
        //&xxidccxx='+IDCOMPLEMENTARIO
        if (IDCOMPLEMENTARIO == 0 && IDROL == 1) {
          var url = String.prototype.concat(_sysUrlBase_, 'smart/quiz/buscar/?plt=modal&idproyecto=PY', idproyecto, '&fcall=', lstmp);
        } else if (IDCOMPLEMENTARIO != 0 && IDROL != 1) {
          var url = String.prototype.concat(_sysUrlBase_, 'smart/quiz/buscar/?plt=modal&xxidpersonaxx=', IDPERSONA, '&idproyecto=PY', idproyecto, '&fcall=', lstmp);
        } else {
          var url = String.prototype.concat(_sysUrlBase_, 'smart/quiz/buscar/?plt=modal&idproyecto=PY', idproyecto, '&fcall=', lstmp);
        }

        var mdquiz = __sysmodal({
          titulo: 'Quiz',
          url: url
        });
        var estadosabc2 = {
          '1': '<?php echo JrTexto::_("Active") ?>',
          '0': '<?php echo JrTexto::_("Inactive") ?>'
        };
        var tabledatosabc2 = function() {
          var frm = document.createElement('form');
          var pr = mdquiz.find('#idproyecto').val();
          var formData = new FormData();
          formData.append('t', mdquiz.find('#textoabc2').val() || '');
          formData.append('pr', pr);
          var u = '<?php echo @$this->curusuario["usuario"] ?>';
          formData.append('u', u);
          var idu = '<?php echo @$this->curusuario["idpersona"] ?>';
          formData.append('id', idu);
          var cl = '<?php echo @$this->curusuario["clave"] ?>';
          formData.append('p', cl);
          var url = _sysUrlBase_ + 'smartquiz/service/exam_list/?pr=PY' + idproyecto + '&t=' + mdquiz.find('#textoabc2').val();

          if (IDCOMPLEMENTARIO == 0 && IDROL == 1) {
            url = String.prototype.concat(url, '&u=', u, '&id=', '&p=', cl);
          } else if (IDCOMPLEMENTARIO != 0 && IDROL != 1) {
            url = String.prototype.concat(url, '&xxidpersonaxx=', IDPERSONA, '&u=', u, '&id=', '&p=', cl);
          } else {
            url = String.prototype.concat(url, '&u=', u, '&id=', '&p=', cl);
          }
          $.ajax({
            url: url,
            type: "GET",
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            cache: false,
            beforeSend: function(XMLHttpRequest) {
              $('#datalistadoabc2 #cargando').show('fast').siblings('div').hide('fast');
            },
            success: function(res) {
              if (res.code == 200) {
                var midata = res.data;
                if (midata.length) {
                  var html = '';
                  var controles = mdquiz.find('#controlesabc2').html();
                  $.each(midata, function(i, v) {
                    var urlimg = _sysUrlStatic_ + '/media/imagenes/cursos/';
                    var srcimg = _sysfileExists(v.portada) ? (v.portada) : (urlimg + 'nofoto.jpg');
                    html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="' + v.idexamen + '">' + controles;
                    html += '<div class="item"><img class="img-responsive" src="' + srcimg + '" width="100%"></div>';
                    html += '<div class="nombre"><strong>' + v.titulo + '</strong></div>';
                    html += '</div></div>';
                  });
                  mdquiz.find('#datalistadoabc2 #data').html(html).show('fast').siblings('div').hide('fast');
                } else
                  mdquiz.find('#datalistadoabc2 #sindatos').show('fast').siblings('div').hide('fast');
              } else {
                mdquiz.find('#datalistadoabc2 #error').html(res.message).show('fast').siblings('div').hide('fast');
              }
            },
            error: function(e) {
              $('#datalistadoabc2 #error').show('fast').siblings('div').hide('fast');
            }
          });
        }
        mdquiz.on('change', '#idproyecto', function() {
          tabledatosabc2();
        }).on('click', '.btnbuscar', function(ev) {
          tabledatosabc2();
        }).on('keyup', '#textoabc2', function(ev) {
          if (ev.keyCode == 13) tabledatosabc2();
        }).on("mouseenter", '.panel-user', function() {
          if (!$(this).hasClass('active')) {
            $(this).siblings('.panel-user').removeClass('active');
            $(this).addClass('active');
          }
        }).on("mouseleave", '.panel-user', function() {
          $(this).removeClass('active');
        }).on("click", '.btn-selected', function() {
          $(this).addClass('active');
          var idexamen = $(this).closest('.panel-user').attr('data-id');
          obj.attr('data-idexamen', idexamen);
          var link = _sysUrlBase_ + 'smart/quiz/ver/?idexamen=' + idexamen + '&idproyecto=PY' + idproyecto;
          if (__isFunction(fncall)) fncall(link);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        })
        setTimeout(function() {
          tabledatosabc2()
        }, 500);
      } //end addexamen
      var __addgame = function(obj, fncall) {
        obj.off('addgame');
        var lstmp = 'addgame' + __idgui();
        obj.addClass(lstmp);
        var url = _sysUrlBase_ + 'smart/games/buscar/?plt=modal&fcall=' + lstmp;
        var mdgame = __sysmodal({
          titulo: 'Games',
          url: url
        });
        obj.on('addgame', function(ev) {
          $(this).off(lstmp);
          $(this).removeClass(lstmp);
          var idgame = $(this).attr('data-idgame');
          var link = _sysUrlSitio_ + '/game/ver/?idgame=' + idgame + '&tmp=' + lstmp;
          link = link.replace(_sysUrlBase_, '', link);
          if (__isFunction(fncall)) fncall(link);
          //__cargarplantilla({url:link,donde:'contentpages'});     
        })

        mdgame.on('click', '.btnbuscar', function(ev) {
          var formData = new FormData();
          formData.append('nombre', $(this).siblings('input').val() || '');
          __sysAyax({
            //donde:$('#plnindice'),
            url: _sysUrlSitio_ + '/tools/json_buscarGames/',
            fromdata: formData,
            mostrarcargando: true,
            showmsjok: false,
            callback: function(res) {
              var html = '';
              var midata = res.data;
              if (midata.length) {
                var html = '';
                var controles = mdgame.find('#controlesabc1').html();
                $.each(midata, function(i, v) {
                  let titulo = v.titulo || '';
                  if (titulo !== '') {
                    html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="' + v.idtool + '" style="margin-bottom: 1ex;">' + controles;
                    //html+='<div class="item"><img class="img-responsive" src="'+srcimg+'" width="100%">'; 
                    html += '<div class="nombre"><strong>' + v.titulo + '</strong></div>';
                    html += '</div></div></div>';
                  }
                });
                mdgame.find('#datalistadoabc1 #data').html(html).show('fast').siblings('div').hide('fast');
              } else
                mdgame.find('#datalistadoabc1 #sindatos').show('fast').siblings('div').hide('fast');
            }
          });
        }).on('keyup', '#textoabc3', function(ev) {
          if (ev.keyCode == 13) mdgame.find('.btnbuscar').trigger('click');
        }).on("mouseenter", '.panel-user', function() {
          if (!$(this).hasClass('active')) {
            $(this).siblings('.panel-user').removeClass('active');
            $(this).addClass('active');
          }
        }).on("mouseleave", '.panel-user', function() {
          $(this).removeClass('active');
        }).on("click", '.btn-selected', function() {
          $(this).addClass('active');
          let pnl = $(this).closest('.panel-user');
          let idgame = pnl.attr('data-id');
          let idsesion = pnl.attr('data-idsesion');
          obj.attr('data-idgame', idgame);
          obj.trigger("addgame");
          mdgame.find('.cerrarmodal').trigger('click');
        })
        setTimeout(function() {
          mdgame.find('.btnbuscar').trigger('click');
        }, 500);
      }
      var __addtics = function(obj, met, fncall) {
        obj.off('addtics');
        var lstmp = 'addtics' + __idgui();
        obj.addClass(lstmp);
        var url = _sysUrlBase_ + 'smart/tics/buscar/?plt=modal&fcall=' + lstmp + '&met=' + met;
        var mdtics = __sysmodal({
          titulo: 'Games',
          url: url
        });
        obj.on('addtics', function(ev) {
          $(this).off(lstmp);
          $(this).removeClass(lstmp);
          var idtic = $(this).attr('data-idtic'); // idactividad
          var idsesion = $(this).attr('data-idsesion');
          var link = _sysUrlSitio_ + '/actividad/ver/?idactividad=' + idtic + "&met=" + met + "&idsesion=" + idsesion + '&tmp=' + lstmp;
          link = link.replace(_sysUrlBase_, '', link);
          obj.attr('data-link', link);
          if (__isFunction(fncall)) fncall(link);
          //__cargarplantilla({url:link,donde:'contentpages'});     
        })
        mdtics.on('click', '.btnbuscar', function(ev) {
          var formData = new FormData();
          formData.append('texto', $(this).siblings('input').val() || '');
          formData.append('met', met);
          __sysAyax({
            donde: $('#plnindice'),
            url: _sysUrlBase_ + 'actividad/buscaractividadjson/',
            fromdata: formData,
            mostrarcargando: true,
            showmsjok: false,
            callback: function(rs) {
              midata = rs.data;
              var html = '';
              if (midata != undefined) {
                var controles = mdtics.find('#controlesabc3').html();
                $.each(midata, function(i, v) {
                  var imgtemp = v.imagen || '';
                  var ultimostaticp = imgtemp.lastIndexOf('static');
                  if (ultimostaticp > 0) {
                    imgtemp = _sysUrlBase_ + imgtemp.substr(ultimostaticp);
                  } else {
                    imgtemp = _sysUrlBase_ + 'static/media/nofoto.jpg';
                  }
                  html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="' + v.idactividad + '" data-idsesion="' + v.sesion + '">' + controles;
                  html += '<div class="item"><img class="img-responsive" src="' + imgtemp + '" width="100%"></div>';
                  html += '<div class="nombre"><strong>' + v.nombre + '</strong></div>';
                  html += '</div></div>';
                });
                mdtics.find('#datalistadoabc3 #data').html(html).show('fast').siblings('div').hide('fast');
              } else mdtics.find('#datalistadoabc3 #sindatos').show('fast').siblings('div').hide('fast');
            }
          });
        }).on('keyup', '#textoabc3', function(ev) {
          if (ev.keyCode == 13) mdtics.find('.btnbuscar').trigger('click');
        }).on("mouseenter", '.panel-user', function() {
          if (!$(this).hasClass('active')) {
            $(this).siblings('.panel-user').removeClass('active');
            $(this).addClass('active');
          }
        }).on("mouseleave", '.panel-user', function() {
          $(this).removeClass('active');
        }).on("click", '.btn-selected', function() {
          $(this).addClass('active');
          let pnl = $(this).closest('.panel-user');
          let idtic = pnl.attr('data-id');
          let idsesion = pnl.attr('data-idsesion');
          obj.attr('data-idtic', idtic);
          obj.attr('data-idsesion', idsesion);
          obj.trigger("addtics");
          mdtics.find('.cerrarmodal').trigger('click');
        })
        setTimeout(function() {
          mdtics.find('.btnbuscar').trigger('click');
        }, 500);
      }
      var __addEnglish = function(obj, fncall) {
        //obj.off('addEnglish');
        //var lstmp = 'addEnglish' + __idgui();
        //obj.addClass(lstmp);
        var url = _sysUrlBase_ + 'smart/english/buscar/?plt=blanco';
        var mdenglish = __sysmodal({
          titulo: 'SmartEnglish',
          url: url
        });
        mdenglish.on('keyup', '#textoabc3', function(ev) {
          if (ev.keyCode == 13) mdenglish.find('.btnbuscar').trigger('click');
        }).on("mouseenter", '.panel-user', function() {
          if (!$(this).hasClass('active')) {
            $(this).siblings('.panel-user').removeClass('active');
            $(this).addClass('active');
          }
        }).on("mouseleave", '.panel-user', function() {
          $(this).removeClass('active');
        }).on('click', '.btnbuscar', function(ev) {
          var idfrm = mdenglish.find('form[name="frmbuscar"]').attr('id');
          var formData = new FormData(document.getElementById(idfrm));
          var urlpv = mdenglish.find('select#plataforma option:selected').attr('link');
          mdenglish.find('#cargando').show();
          mdenglish.find('#table').hide();
          formData.append('url', urlpv);
          __sysAyax({
            donde: $('#plnindice'),
            url: _sysUrlBase_ + 'getservice/englishsesiones/',
            fromdata: formData,
            mostrarcargando: true,
            showmsjok: false,
            callback: function(rs) {
              midata = rs.data || [];
              var html = '';
              if (midata.length > 0) {
                mdenglish.find('#cargando').hide();
                mdenglish.find('#table').show();
                var sin = -1;
                mdenglish.find('tbody#aquicarganlassesiones').children('tr').remove();
                $.each(midata, function(is, v) {
                  var imgtemp = v.imagen || '';
                  var ultimostaticp = imgtemp.lastIndexOf('static');
                  if (ultimostaticp > 0) {
                    imgtemp = urlpv + imgtemp.substr(ultimostaticp);
                  } else {
                    imgtemp = urlpv + 'static/media/nofoto.jpg';
                  }
                  if (v.tiporecurso == 'L') {
                    var html = '';
                    sin++;
                    html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" idcursodetalle="' + v.idcursodetalle + '" idrecurso="' + v.idrecurso + '" idcurso="' + v.idcurso + '" idactividad="' + (v.idactividad || 0) + '" ><div class="pnlacciones text-center"><a class="addEnglish btn-selected btn btn-success btn-xs" title="Selected  Assessment"><i class="btnselected fa fa-hand-o-down"></i></a></div>';
                    html += '<div class="item"><img class="img-responsive" src="' + imgtemp + '" width="100%"></div>';
                    html += '<div class="nombre"><strong>' + v.nombre + '</strong></div>';
                    html += '</div></div>';
                    if (sin % 4 == 0) {
                      mdenglish.find('tbody#aquicarganlassesiones').append('<tr><td>' + html + '</td></tr>');
                    } else
                      mdenglish.find('tbody#aquicarganlassesiones').children('tr').last().children('td').append(html);
                  } else {
                    $.each(v.hijo, function(is, vh) {

                      var imgtemp = vh.imagen || '';
                      var ultimostaticp = imgtemp.lastIndexOf('static');
                      if (ultimostaticp > 0) {
                        imgtemp = urlpv + imgtemp.substr(ultimostaticp);
                      } else {
                        imgtemp = urlpv + 'static/media/nofoto.jpg';
                      }
                      if (vh.tiporecurso == 'L') {
                        var html = '';
                        sin++;
                        html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" idcursodetalle="' + vh.idcursodetalle + '" idrecurso="' + vh.idrecurso + '" idcurso="' + vh.idcurso + '" idactividad="' + (vh.idactividad || 0) + '" ><div class="pnlacciones text-center"><a class="addEnglish btn-selected btn btn-success btn-xs" title="Selected  Assessment"><i class="btnselected fa fa-hand-o-down"></i></a></div>';
                        html += '<div class="item"><img class="img-responsive" src="' + imgtemp + '" width="100%"></div>';
                        html += '<div class="nombre"><strong>' + vh.nombre + '</strong></div>';
                        html += '</div></div>';
                        if (sin % 4 == 0) {
                          mdenglish.find('tbody#aquicarganlassesiones').append('<tr><td>' + html + '</td></tr>');
                        } else
                          mdenglish.find('tbody#aquicarganlassesiones').children('tr').last().children('td').append(html);
                      }

                    })
                  }

                })
                //md.find('table').datatable();
              }
            }
          });
        }).on("click", '.addEnglish', function() {
          $(this).addClass('active');
          let pnl = $(this).closest('.panel-user');
          var idcursodetalle = pnl.attr('idcursodetalle') || 0; // idactividad
          var idrecurso = pnl.attr('idrecurso') || 0;
          var idcurso = pnl.attr('idcurso') || 0;
          var idactividad = pnl.attr('idactividad') || 0;
          var urlpv = mdenglish.find('select#plataforma option:selected').attr('link');
          var link = _sysUrlBase_ + 'smart/english/ver/?PV=' + (mdenglish.find('select#plataforma').val() || '') + '&idcursodetalle=' + idcursodetalle + "&idcurso=" + idcurso + "&idrecurso=" + idrecurso + '&idactividad=' + idactividad;
          link = link.replace(_sysUrlBase_, '', link);
          obj.attr('data-link', link);
          if (__isFunction(fncall)) fncall(link);
          __cerrarmodal(mdenglish);
        }).on('change', 'select#idcurso', function(ev) {
          ev.preventDefault();
          mdenglish.find('.btnbuscar').trigger('click');
        }).on('change', 'select#plataforma', function(ev) {
          smartengliscargarcursos();
        })

        var smartengliscargarcursos = function() {
          // var idfrm=md.find('form[name="frmbuscar"]').attr('id'); 'document.getElementById(idfrm)'
          var formData = new FormData();
          var urlpv = mdenglish.find('select#plataforma option:selected').attr('link');
          formData.append('url', urlpv);
          formData.append('plataforma', mdenglish.find('select#plataforma').val() || '');
          //formData.append('idcurso',false);
          formData.append('texto', mdenglish.find('input#texto').val() || '');
          __sysAyax({
            donde: $('#plnindice'),
            url: _sysUrlBase_ + 'getservice/englishcursos/',
            fromdata: formData,
            mostrarcargando: true,
            showmsjok: false,
            callback: function(rs) {
              if (rs.data.length > 0) {
                mdenglish.find('select#idcurso').find('option').remove();
                $.each(rs.data, function(id, d) {
                  mdenglish.find('select#idcurso').append('<option value="' + d.idcurso + '">' + d.nombre + '</option>');
                })
              }
              mdenglish.find('.btnbuscar').trigger('click');
            }
          });
        }

        var timeval = setInterval(function() {
          if (mdenglish.find('form[name="frmbuscar"]').length > 0) {
            clearInterval(timeval);
            smartengliscargarcursos();
          }
        }, 1000);
      }

      var __asiganacionfile = function(it) {
        it.siblings('.cicon').removeClass('active').children('i').removeClass('btn-success btn-primary').addClass('btn-primary');
        it.addClass('active').children('i').addClass('btn-success');
        var _li = elli;
        var link_ = (it.attr('data-link') || '');
        var indexlink_ = link_.lastIndexOf('static/');
        if (indexlink_ > -1) link_ = link_.substring(indexlink_);
        if (_li.hasClass('esmenu')) {
          _li.attr('txtjson-typelink', it.attr('data-type'));
          _li.attr('txtjson-link', link_);
          _li.attr('txtjson-tipo', '#showpaddcontenido');
        } else {
          _li.attr('type', it.attr('data-type'))
          _li.attr('link', link_)
          _li.attr('color', "transparent")
          li.attr('txtjson-tipo', '#showpaddopciones');
        }
        actulaizarjson(li);
        __ocultarsinoescalificado(_li);
        __cerrarmodal(md);
      } //end __asiganacionfile

      var _li_ = el.closest('li');
      var link = '';
      if (_li_.hasClass('esmenu')) {
        link = (_li_.attr('txtjson-link') || '');
        var indexlink = link.lastIndexOf('static/');
        if (indexlink > -1) link = _sysUrlBase_ + link.substring(indexlink);
        md.find('.cicon[data-type="' + _li_.attr('txtjson-typelink') + '"]').children('i').addClass('btn-success').removeClass('btn-primary');
        md.find('.cicon[data-type="' + _li_.attr('txtjson-typelink') + '"]').attr('data-link', link);
      } else {
        link = _li_.attr('link') || '';
        var indexlink = link.lastIndexOf('static/');
        if (indexlink > -1) link = _sysUrlBase_ + link.substring(indexlink);
        md.find('.cicon[data-type="' + _li_.attr('type') + '"]').children('i').addClass('btn-success').removeClass('btn-primary');
        md.find('.cicon[data-type="' + _li_.attr('type') + '"]').attr('data-link', link)
      }
      md.on('click', ' .cicon', function(ev) {
        ev.preventDefault();
        var it = $(this);
        var type = it.attr('data-type') || '';
        var datalink = it.attr('data-link') || '';
        var acc = it.attr('data-acc') || '';
        el = eltmp_;
        console.log('cion', el);
        //if(type===''||type==datatype) return false;
        it.attr('data-oldmedia', datalink)
        it.siblings().removeAttr('data-oldmedia').children('i.btn-success').removeClass('btn-success').addClass('btn-primary');
        var _cont = $('#bookplantilla').children('#contentpages');
        var oldmedia = it.closest('.row').find('.verpopover.btn-success').attr('data-link');
        var nombre = "file_" + idcursodetalle + '_' + Date.now();

        <?php if ($this->idcomplementario != 0) { ?>
          var dirmedia = 'cursos_c/c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/ses_' + idcursodetalle + "/";
        <?php } else {  ?>
          var dirmedia = 'cursos/curso_' + idcurso + '/ses_' + idcursodetalle + "/"; // var dirmedia_ = 'cursos/' + dirmedia;
        <?php } ?> // var dirmedia = 'cursos/curso_' + idcurso + '/ses_' + idcursodetalle + "/";

        switch (type) {
          case 'smartenglish':
            __addEnglish(it, function(link) {
              it.children('i').addClass('btn-success').removeClass('btn-primary');
              it.attr('data-link', link);
              it.attr('data-type', 'smartenglish');
              it.attr('data-oldmedia', link);
              __asiganacionfile(it);
            });
            break;
          case 'smarticlock':
            __addtics(it, 1, function(link) {
              it.children('i').addClass('btn-success').removeClass('btn-primary');
              it.attr('data-link', link);
              it.attr('data-type', 'smarticlock');
              it.attr('data-oldmedia', link);
              __asiganacionfile(it);
            });
            break;
          case 'smarticlookPractice':
            __addtics(it, 2, function(link) {
              it.children('i').addClass('btn-success').removeClass('btn-primary');
              it.attr('data-link', link);
              it.attr('data-type', 'smarticlookPractice');
              it.attr('data-oldmedia', link);
              __asiganacionfile(it);
            });
            break;
          case 'smarticDobyyourselft':
            __addtics(it, 3, function(link) {
              it.children('i').addClass('btn-success').removeClass('btn-primary');
              it.attr('data-link', link);
              it.attr('data-type', 'smarticDobyyourselft');
              it.attr('data-oldmedia', link);
              __asiganacionfile(it);
            });
            break;
          case 'game':
            __addgame(it, function(link) {
              it.children('i').addClass('btn-success').removeClass('btn-primary');
              it.attr('data-link', link);
              it.attr('data-type', 'game');
              it.attr('data-oldmedia', link);
              __asiganacionfile(it);
            });
            break;
          case 'smartquiz':
            __addexamen(it, function(link) {
              it.children('i').addClass('btn-success').removeClass('btn-primary');
              it.attr('data-link', link);
              it.attr('data-type', 'smartquiz');
              it.attr('data-oldmedia', link);
              __asiganacionfile(it);
            });
            break;
          case 'enlacesinteres':
            __subirfile({
                file: it.children('i'),
                typefile: 'enlacesinteres',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              },
              //__subirfile({ file:it.children('i'), typefile:'pdf',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
              function(rs) {
                // console.log(rs)
                it.attr('data-type', 'enlacesinteres');
                it.attr('data-link', rs.media);
                it.attr('data-oldmedia', rs.media);
                // it.attr('color', "transparent");
                __asiganacionfile(it);
                Swal.fire({
                  title: 'Guardando, espere...',
                  showConfirmButton: false,
                  allowOutsideClick: false
                });
                $.post(_sysUrlBase_ + 'json/acad_cursodetalle/procesarEnlaces', {
                  'link': rs.media
                }, function(data) {
                  if (data.code == 200) {
                    Swal.close();
                  }
                }, 'json');
              });
            break;
          case 'pdf':
            __subirfile({
                file: it.children('i'),
                typefile: 'documento',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              },
              //__subirfile({ file:it.children('i'), typefile:'pdf',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
              function(rs) {
                it.attr('data-type', 'pdf');
                it.attr('data-link', rs.media);
                it.attr('data-oldmedia', rs.media);
                __asiganacionfile(it);
              });
            break;
          case 'html':
            __subirfile({
                file: it.children('i'),
                typefile: 'html',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              }, //
              //__subirfile({ file:it.children('i'), typefile:'html',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
              function(rs) {
                it.attr('data-type', 'html');
                it.attr('data-link', rs.media);
                it.attr('data-oldmedia', rs.media);
                __asiganacionfile(it);
              });
            break;
          case 'flash':
            __subirfile({
                file: it.children('i'),
                typefile: 'flash',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              },
              //__subirfile({ file:it.children('i'), typefile:'flash',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
              function(rs) {
                it.attr('data-type', 'flash');
                it.attr('data-link', rs.media);
                it.attr('data-oldmedia', rs.media);
                __asiganacionfile(it);
              });
            break;
          case 'audio':
            __subirfile({
                file: it.children('i'),
                typefile: 'audio',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              },
              //__subirfile({ file:it.children('i'), typefile:'audio',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
              function(rs) {
                it.attr('data-type', 'audio');
                it.attr('data-link', rs.media);
                it.attr('data-oldmedia', rs.media);
                __asiganacionfile(it);
              });
            break;
          case 'video':
            __subirfile({
                file: it.children('i'),
                typefile: 'video',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              },
              //__subirfile({ file:it.children('i'), typefile:'video',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
              function(rs) {
                it.attr('data-type', 'video');
                it.attr('data-link', rs.media);
                it.attr('data-oldmedia', rs.media);
                __asiganacionfile(it);
              });
            break;
          case 'imagen':
            __subirfile({
                file: it.children('i'),
                typefile: 'imagen',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              },
              function(rs) {
                it.attr('data-type', 'imagen');
                it.attr('data-link', rs.media);
                it.attr('data-oldmedia', rs.media);
                __asiganacionfile(it);
              });
            break;
          case 'texto':
            it.children('i').addClass('btn-success').removeClass('btn-primary');
            it.attr('data-idinfotmp', 'txt' + __idgui());
            break;
          case 'menuvaloracion':
            it.children('i').addClass('btn-success').removeClass('btn-primary');
            it.attr('data-type', 'menuvaloracion');
            it.attr('data-link', 'menuvaloracion');
            __asiganacionfile(it);
            break;
          case 'foro':
            it.children('i').addClass('btn-success').removeClass('btn-primary');
            it.attr('data-type', 'foro');
            it.attr('data-link', 'foro');
            // it.attr('color', "transparent");
            __asiganacionfile(it);
            break;
          case 'zoom':
            it.children('i').addClass('btn-success').removeClass('btn-primary');
            it.attr('data-type', 'zoom');
            it.attr('data-link', 'zoom');
            // it.attr('color', "transparent");
            __asiganacionfile(it);
            break;
          case 'enlacecolaborativo':
            it.children('i').addClass('btn-success').removeClass('btn-primary');
            it.attr('data-type', 'enlacecolaborativo');
            it.attr('data-link', 'enlacecolaborativo');
            // it.attr('color', "transparent");
            __asiganacionfile(it);
            break;
          case 'enlaceyoutube':
            var el = _li_;
            var dt = {
              html: $('#showEnlazarYoutube').html()
            };
            var md2 = __sysmodal(dt);
            var tmplink = it.attr('data-link') || '';
            if (tmplink != '') {
              var tmplink = 'https://www.youtube.com/watch?v=' + tmplink;
              md2.find('input#txtenlacevideo').val(tmplink);
            }
            var li_ = _li_;
            md2.on('click', '.btnsaveenlace', function() {
              it.attr('data-type', 'enlaceyoutube');
              var enlace = md2.find('#txtenlacevideo').val().split("v=")[1];
              link_ = enlace.split("&")[0];
              it.attr('data-link', link_);
              __cerrarmodal(md2);
              __asiganacionfile(it);
            })
            break;
          case 'tabladeaudios':
            var dt = {
              html: $('#showtabladeaudios').html()
            };
            var md2 = __sysmodal(dt);
            try {
              if (datalink != '') {
                var link_ = atob(datalink);
                link_ = JSON.parse(link_);
                if (link_.titulo != '' && link_.titulo != undefined)
                  md2.find('input.titulo').val(link_.titulo);
                if (link_.menu != undefined)
                  if (link_.menu.length)
                    $.each(link_.menu, function(i, v) {
                      var trclone = md2.find('.trclone').clone();
                      var index = md2.find('table tbody').find('tr').length;
                      trclone.attr('data-type', v.tipo);
                      trclone.attr('data-link', v.link);
                      trclone.find('td').first().html(index);
                      trclone.find('input[name="nom"]').val(v.nombre);
                      if (v.tipo == 'audio') {
                        trclone.find('.btnsubir').parent().html('<audio controls="true" src="' + _sysUrlBase_ + v.link + '" class="btnsubir" style="max-width:100px;"></audio>');
                      }
                      trclone.removeClass('trclone').removeAttr('style');
                      md2.find('.trclone').before(trclone);
                    })
              }
            } catch (ex) {
              console.log(ex);
            }
            md2.on('click', '.imove', function() {
              var tr = $(this).closest('tr');
              var adonde = $(this).attr('adonde');
              if (adonde == 'up') {
                if (!tr.prev().hasClass('trclone')) tr.prev().before(tr);
              } else {
                if (!tr.next().hasClass('trclone')) tr.next().after(tr);
              }
              md2.find('tbody').find('tr').each(function(i, tr) {
                $(tr).find('td').first().text(i + 1);
              })
            }).on('click', '.iborrar', function() {
              $(this).closest('tr').remove();
              md2.find('tbody').find('tr').each(function(i, tr) {
                $(tr).find('td').first().text(i + 1);
              })
            }).on('click', '.btnadd', function() {
              var trclone = md2.find('.trclone').clone();
              var index = md2.find('table tbody').find('tr').length;
              trclone.find('td').first().html(index);
              trclone.removeClass('trclone').removeAttr('style');
              md2.find('.trclone').before(trclone);
            }).on('click', '.btnsubir', function(e) {
              e.preventDefault();
              var el_ = $(this);
              var $img = el_.children('i');
              var tr = el_.closest('tr');
              <?php if ($this->idcomplementario != 0) {   ?>
                var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : 'c_c/');
                dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
              <?php } else {   ?>
                var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
                dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
              <?php }  ?>
              var nombre = 'ses_' + Date.now();
              var oldmedia = $img.attr('src') || '';
              __subirfile({
                file: $img,
                typefile: 'audio',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              }, function(rs) {
                tr.attr('data-type', 'audio');
                tr.attr('data-link', rs.media);
                tr.attr('data-oldmedia', rs.media);
                el_.closest('div').html('<audio controls="true" src="' + _sysUrlBase_ + rs.media + '" class="btnsubir" style="max-width:100px;"></audio>');
              });
            }).on('click', '.btnsavejson', function() {
              it.attr('data-type', 'tabladeaudios');
              var _jsonmultioptions = [];
              md2.find('tbody').find('tr').each(function(i, tr) {
                var $tr = $(this);
                if (!$tr.hasClass('trclone'))
                  _jsonmultioptions.push({
                    nombre: $tr.find('input[name="nom"]').val() || 'audio',
                    tipo: $tr.attr('data-type') || '',
                    link: $tr.attr('data-link') || ''
                  });
              })

              var __jsonmultioptions = {
                titulo: (md2.find('input.titulo').val() || ''),
                menu: _jsonmultioptions
              };
              __jsonmultioptions = JSON.stringify(__jsonmultioptions);
              __jsonmultioptions = btoa(__jsonmultioptions);
              it.attr('data-link', __jsonmultioptions);
              __cerrarmodal(md2);
              __asiganacionfile(it);
            })
            break;
          case 'multioptions':
            var dt = {
              html: $('#showpaddmultivideos').html()
            };
            var md2 = __sysmodal(dt);
            try {
              if (datalink != '') {
                var link_ = atob(datalink);
                link_ = JSON.parse(link_);
                md2.find('select').val(link_.mostrarcomo || 'slider')
                var __menus = link_.menus || [];
                if (__menus.length > 0)
                  $.each(__menus, function(i, v) {
                    var trvideoclone = md2.find('.trvideoclone').clone();
                    var index = md2.find('table tbody').find('tr').length;
                    trvideoclone.attr('data-type', v.tipo);
                    trvideoclone.attr('data-link', v.link);
                    trvideoclone.find('td').first().html(index);
                    trvideoclone.find('input[name="nom"]').val(v.nombre);
                    if (v.tipo == 'video') {
                      trvideoclone.find('.btnsubirvideo').parent().html('<video controls="true" src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo" style="max-width:200px; max-height:200px;"></video>');
                    } else if (v.tipo == 'imagen') {
                      trvideoclone.find('.btnsubirvideo').parent().html('<img src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo img-responsive" style="max-width:200px; max-height:200px;"/>');
                    } else {
                      trvideoclone.find('.btnsubirvideo').parent().html('<i src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo btn btn-danger fa fa-file-o" > ' + v.tipo + ' </i>');
                    }
                    trvideoclone.removeClass('trvideoclone').removeAttr('style');
                    md2.find('.trvideoclone').before(trvideoclone);
                  })
              }
            } catch (ex) {
              console.log(ex);
            }

            md2.on('click', '.imove', function() {
              var tr = $(this).closest('tr');
              var adonde = $(this).attr('adonde');
              if (adonde == 'up') {
                if (!tr.prev().hasClass('trvideoclone')) tr.prev().before(tr);
              } else {
                if (!tr.next().hasClass('trvideoclone')) tr.next().after(tr);
              }
              md2.find('tbody').find('tr').each(function(i, tr) {
                $(tr).find('td').first().text(i + 1);
              })
            }).on('click', '.iborrar', function() {
              $(this).closest('tr').remove();
              md2.find('tbody').find('tr').each(function(i, tr) {
                $(tr).find('td').first().text(i + 1);
              })
            }).on('click', '.btnaddvideo', function() {
              var trvideoclone = md2.find('.trvideoclone').clone();
              var index = md2.find('table tbody').find('tr').length;
              trvideoclone.find('td').first().html(index);
              trvideoclone.removeClass('trvideoclone').removeAttr('style');
              md2.find('.trvideoclone').before(trvideoclone);
            }).on('click', '.btnsubirvideo', function(e) {
              e.preventDefault();
              var el_ = $(this);
              var $img = el_.children('i');
              var tr = el_.closest('tr');

              <?php if ($this->idcomplementario != 0) {   ?>
                var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : 'c_c/');
                dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
              <?php  } else {    ?>
                var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
                dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
              <?php } ?>

              var nombre = 'ses_' + Date.now();
              var oldmedia = $img.attr('src') || '';
              __subirfile({
                file: $img,
                typefile: 'media',
                uploadtmp: true,
                guardar: true,
                dirmedia: dirmedia,
                'oldmedia': oldmedia,
                'nombre': nombre
              }, function(rs) {
                var tipomedia_ = rs.media.lastIndexOf('.mp4?');
                if (tipomedia_ != -1) tipomedia_ = 'video';
                else if (rs.media.lastIndexOf('.pdf?') != -1)
                  tipomedia_ = 'pdf';
                else if (rs.media.lastIndexOf('.mp3?') != -1)
                  tipomedia_ = 'audio';
                else if (rs.media.lastIndexOf('.html?') != -1)
                  tipomedia_ = 'html';
                else tipomedia_ = 'imagen';
                tr.attr('data-type', tipomedia_);
                tr.attr('data-link', rs.media);
                tr.attr('data-oldmedia', rs.media);
                if (tipomedia_ == 'video') {
                  el_.closest('div').html('<video controls="true" src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo" style="max-width:200px; max-height:200px;"></video>');
                } else if (tipomedia_ == 'imagen') {
                  el_.closest('div').html('<img src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo" style="img-responsive max-width:200px; max-height:200px;"></video>');
                } else {
                  el_.closest('div').html('<i src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo btn btn-danger fa fa-file-o" > ' + tipomedia_ + ' </i>');
                }
              });
            }).on('click', '.btnsavejson', function() {
              it.attr('data-type', 'multioptions');
              var _jsonmultioptions = [];
              md2.find('tbody').find('tr').each(function(i, tr) {
                var $tr = $(this);
                if (!$tr.hasClass('trvideoclone')) {
                  _jsonmultioptions.push({
                    nombre: $tr.find('input[name="nom"]').val() || 'video',
                    tipo: $tr.attr('data-type') || '',
                    link: $tr.attr('data-link') || ''
                  });
                }
              })
              var __jsonmultioptions = {
                mostrarcomo: 'slider',
                menus: _jsonmultioptions
              };
              if (!elli.hasClass('esmenu')) __jsonmultioptions.mostrarcomo = md2.find('select').val();
              __jsonmultioptions = JSON.stringify(__jsonmultioptions);
              __jsonmultioptions = btoa(__jsonmultioptions);
              it.attr('data-link', __jsonmultioptions);
              __cerrarmodal(md2);
              __asiganacionfile(it);
            })
            break;
          case 'videoconferencia':
            var dt = {
              html: $('#showvideoconferencia').html(),
              titulo: 'Enlaces de Video Conferencias'
            };
            var md2 = __sysmodal(dt);
            try {
              if (datalink != '') {
                let conferencia = JSON.parse(atob(datalink));
                md2.find('#input-conf-titulo').val(conferencia.titulo);
                md2.find('#input-conf-link_mod').val(conferencia.link_mod);
                md2.find('#input-conf-link_visitor').val(conferencia.link_visitor);
                md2.find('#input-conf-fecha').val(conferencia.fecha);
                md2.find('#input-conf-hora_comienzo').val(conferencia.hora_comienzo);
                md2.find('#input-conf-detalle').val(conferencia.detalle);
              }
            } catch (ex) {
              console.log(ex);
            }
            md2.on('click', '.btnsavejson', function() {
              it.attr('data-type', 'videoconferencia');

              let conferencia = {
                titulo: md2.find('#input-conf-titulo').val(),
                link_mod: md2.find('#input-conf-link_mod').val(),
                link_visitor: md2.find('#input-conf-link_visitor').val(),
                fecha: md2.find('#input-conf-fecha').val(),
                hora_comienzo: md2.find('#input-conf-hora_comienzo').val(),
                detalle: md2.find('#input-conf-detalle').val(),
              };
              if (conferencia.titulo == '' || conferencia.link == '' || conferencia.fecha == '' ||
                conferencia.hora_comienzo == '' || conferencia.detalle == '') {
                Swal.fire({
                  icon: 'warning',
                  title: 'Complete todos los datos',
                  showConfirmButton: true,
                })
                return null;
              }

              var __jsonmultioptions = conferencia;
              __jsonmultioptions = JSON.stringify(__jsonmultioptions);
              __jsonmultioptions = btoa(__jsonmultioptions);
              it.attr('data-link', __jsonmultioptions);
              __cerrarmodal(md2);
              __asiganacionfile(it);
            })

            break;
        }
        ev.stopPropagation();
      })
    }).on('click', '.btnaddhabilidades', function(ev) {
      var el = $(this);
      var dt = {
        html: $('#showpaddhabilidad').html()
      };
      var paso3 = $('#nestable');
      var md = __sysmodal(dt);
      var li = el.closest('.esmenu');
      var idcursodetalle = li.attr('idcursodetalle');
      var idcurso = $('#idcurso').val();
      var _li_ = el.closest('li');
      var __criterios = function(obj, valor) {
        var formData = new FormData();
        formData.append('idcurso', valor);
        $.ajax({
          url: _sysUrlBase_ + 'json/acad_criterios/listado',
          type: 'POST',
          contentType: false,
          data: formData, // mandamos el objeto formdata que se igualo a la variable data
          processData: false,
          cache: false,
          beforeSend: function(objeto) {
            //$("#loadCategoria").html("<img src='../util/images/load.gif'>");
          },
          success: function(data) {
            var response = JSON.parse(data);
            var html = "";
            crisel = _li_.attr('criterios');
            if (crisel != undefined) {
              var crisel = crisel.split("|");
            } else var crisel = [];

            if (response.data.length > 0) {
              $.each(response.data, function(i, item) {
                var selcri = false;
                $.each(crisel, function(i, c) {
                  if (c == item.idcriterio) selcri = 'checked="checked"';
                })
                html += '<li>';
                html += '<p>';
                html += '<input type="checkbox" ' + selcri + ' class="flat" id="criterio' + i + '" style="position: absolute; opacity: 1;" data-id="' + item.idcriterio + '" data-nombre="' + item.nombre + '"/>';
                html += ' ' + item.nombre;
                html += '</p>';
                html += '</li>';
              });
              obj.find('#listado_criterios').html(html);

              obj.find("input.flat").iCheck({
                checkboxClass: "icheckbox_flat-green"
              })
            } else {
              obj.find('#listado_criterios').html("<h2><center>No tiene criterios designados</center></h2>");
            }
          }
        })
      }

      var link = '';
      md.on('click', '.btnsavejson_aux ', function() {
        var _jsoncriterios = '';
        md.find("#listado_criterios li").each(function(i, v) {
          if ($(v).find('input').is(':checked')) {
            _jsoncriterios += $(this).find('input').attr('data-id') + '|';
          }
        });
        _li_.attr('criterios', _jsoncriterios);
        actulaizarjson(li);
        __cerrarmodal(md);
      })
      __criterios(md, idcurso);
    }).on('click', '.btnaddvideos', function(ev) {
      var el = $(this);
      var dt = {
        html: $('#showpaddmultivideos').html()
      };
      var paso3 = $('#nestable');
      var md = __sysmodal(dt);
      var li = el.closest('.esmenu');
      var idcursodetalle = li.attr('idcursodetalle');
      var idcurso = $('#idcurso').val();
      var li_ = el.closest('li');
      var typetmp = li_.attr('type') || '';
      if (typetmp == 'multioptions') {
        try {
          link = li_.attr('link');
          link = atob(link);
          link = JSON.parse(link);
          md.find('select').val(link.mostrarcomo || 'slider')
          var __menus = link.menus || '';
          if (__menus != '')
            $.each(__menus, function(i, v) {
              var trvideoclone = md.find('.trvideoclone').clone();
              var index = md.find('table tbody').find('tr').length;
              trvideoclone.attr('data-type', v.tipo);
              trvideoclone.attr('data-link', v.link);
              trvideoclone.find('td').first().html(index);
              trvideoclone.find('input[name="nom"]').val(v.nombre);
              if (v.tipo == 'video') {
                trvideoclone.find('.btnsubirvideo').parent().html('<video controls="true" src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo" style="max-width:200px; max-height:200px;"></video>');
              } else if (v.tipo == 'imagen') {
                trvideoclone.find('.btnsubirvideo').parent().html('<img src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo img-responsive" style="max-width:200px; max-height:200px;"/>');
              } else {
                trvideoclone.find('.btnsubirvideo').parent().html('<i src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo btn btn-danger fa fa-file-o" > ' + v.tipo + ' </i>');
              }
              trvideoclone.removeClass('trvideoclone').removeAttr('style');
              md.find('.trvideoclone').before(trvideoclone);
            })
        } catch (ex) {
          console.log(ex);
        }
      }
      md.on('click', '.imove', function() {
        var tr = $(this).closest('tr');
        var adonde = $(this).attr('adonde');
        if (adonde == 'up') {
          if (!tr.prev().hasClass('trvideoclone')) tr.prev().before(tr);
        } else {
          if (!tr.next().hasClass('trvideoclone')) tr.next().after(tr);
        }
        md.find('tbody').find('tr').each(function(i, tr) {
          $(tr).find('td').first().text(i + 1);
        })
      }).on('click', '.iborrar', function() {
        $(this).closest('tr').remove();
        md.find('tbody').find('tr').each(function(i, tr) {
          $(tr).find('td').first().text(i + 1);
        })
      }).on('click', '.btnaddvideo', function() {
        var trvideoclone = md.find('.trvideoclone').clone();
        var index = md.find('table tbody').find('tr').length;
        trvideoclone.find('td').first().html(index);
        trvideoclone.removeClass('trvideoclone').removeAttr('style');
        md.find('.trvideoclone').before(trvideoclone);
      }).on('click', '.btnsubirvideo', function(e) {
        e.preventDefault();
        var el = $(this);
        var $img = el.children('i');
        var tr = el.closest('tr');
        <?php
        if ($this->idcomplementario != 0) {
        ?>
          // var dirmedia = 'cursos_c/c_c_' + idcurso + '/ses_' + idcursodetalle + "/";
          var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : 'c_c/');
          dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
          // var dirmedia_ = 'cursos_c/' + dirmedia;
        <?php
        } else {
        ?>
          var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
          dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
        <?php
        }
        ?>
        // var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
        // dirmedia=dirmedia+(idcursodetalle!=''?('ses_'+idcursodetalle+''):'ses_tmp')+'/';
        var nombre = 'ses_' + Date.now();
        var oldmedia = $img.attr('src') || '';
        __subirfile({
          file: $img,
          typefile: 'media',
          uploadtmp: true,
          guardar: true,
          dirmedia: dirmedia,
          'oldmedia': oldmedia,
          'nombre': nombre
        }, function(rs) {
          var tipomedia_ = rs.media.lastIndexOf('.mp4?');
          if (tipomedia_ != -1) tipomedia_ = 'video';
          else if (rs.media.lastIndexOf('.pdf?') != -1)
            tipomedia_ = 'pdf';
          else if (rs.media.lastIndexOf('.mp3?') != -1)
            tipomedia_ = 'audio';
          else if (rs.media.lastIndexOf('.html?') != -1)
            tipomedia_ = 'html';
          else tipomedia_ = 'imagen';
          tr.attr('data-type', tipomedia_);
          tr.attr('data-link', rs.media);
          tr.attr('data-oldmedia', rs.media);
          if (tipomedia_ == 'video') {
            el.closest('div').html('<video controls="true" src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo" style="max-width:200px; max-height:200px;"></video>');
          } else if (tipomedia_ == 'imagen') {
            el.closest('div').html('<img src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo" style="img-responsive max-width:200px; max-height:200px;"></video>');
          } else {
            el.closest('div').html('<i src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo btn btn-danger fa fa-file-o" > ' + tipomedia_ + ' </i>');
          }
        });
      }).on('click', '.btnsavejson', function() {
        li_.attr('type', 'multioptions');
        var _jsonmultioptions = [];
        md.find('tbody').find('tr').each(function(i, tr) {
          var $tr = $(this);
          if (!$tr.hasClass('trvideoclone')) {
            _jsonmultioptions.push({
              nombre: $tr.find('input[name="nom"]').val() || 'video',
              tipo: $tr.attr('data-type') || '',
              link: $tr.attr('data-link') || ''
            });
          }
        })
        var __jsonmultioptions = {
          mostrarcomo: 'slider',
          menus: _jsonmultioptions
        };
        __jsonmultioptions.mostrarcomo = md.find('select').val();
        __jsonmultioptions = JSON.stringify(__jsonmultioptions);
        __jsonmultioptions = btoa(__jsonmultioptions);
        li_.attr('link', __jsonmultioptions);
        actulaizarjson(li);
        __cerrarmodal(md);
      })
    }).on('click', '.btnpuntaje', function(ev) {
      var el = $(this);
      var li = el.closest('li');
      var lipadre = li.hasClass('esmenu') ? li : li.closest('.esmenu');
      var dt = {
        html: $('#showaddpuntaje').html()
      };
      var md = __sysmodal(dt);
      var idcurso = $('#idcurso').val();
      var hayerror = false;
      var jsonformula = [];
      var __iniciotablapuntaje = function() {
        var notas = configuracion_nota.notas;
        var tb = md.find('#tablenuevapuntuacion');

        if (notas.length) {
          if (notas.length == 1) tb.find('.haygrupo').hide();
          else tb.find('.haygrupo').show();
          $.each(notas, function(ii, n) {
            $.each(n.formula, function(ni, f) {
              var tr = tb.find('tbody tr.trclone').clone();
              tr.attr('id', 'tr' + f.id);
              var litmp = '';
              if (f.type == '' || f.type == undefined) {
                litmp = $('#_contentcurso_').find('li[idcursodetalle="' + f.id + '"]');
                if (litmp.length == 0) litmp = $('#_contentcurso_').find('#' + f.id);
                if (litmp.length > 0) {
                  if (litmp.children('span.accionesdecurso').children('i.btnestarea').hasClass('bg-success')) f.type = 'estarea';
                  else if (litmp.children('span.accionesdecurso').children('i.btnesproyecto').hasClass('bg-success')) f.type = 'esproyecto';
                  else f.type = litmp.hasClass('esmenu') ? litmp.attr('txtjson-typelink') : litmp.attr('type');
                }
              }
              tr.attr('type', f.type || '');
              tr.attr('dataid', f.id);
              tr.find('.punitem').text(f.titulo || 'sintitulo');
              tr.find('select.pungrupo').val(n.prefijo);
              tr.find('select.pungruporcentaje').val((n.tipo == 'P' || n.tipo == 'T') ? 'P' : n.valortipo);
              tr.find('select.punporcentaje').val((f.tipo == 'T' || f.tipo == 'P') ? 'P' : f.valortipo);
              tr.find('input.punprefijo').val(f.prefijo);
              tr.removeClass('trclone').show();
              tb.find('tbody').append(tr);
            })
          })
        }
        var dataid = (li.hasClass('esmenu') ? li.attr('idcursodetalle') : li.attr('id'));
        var idtr = 'tr' + dataid;
        var titulo = li.hasClass('esmenu') ? li.children('.titulo').text() : (lipadre.children('.titulo').text() + ' - ' + li.children('.titulo').text());
        var type = '';
        if (li.children('span.accionesdecurso').children('i.btnestarea').hasClass('bg-success')) type = 'estarea';
        else if (li.children('span.accionesdecurso').children('i.btnesproyecto').hasClass('bg-success')) type = 'esproyecto';
        else type = li.hasClass('esmenu') ? li.attr('txtjson-typelink') : li.attr('type');
        var prefijo = tb.find('#' + idtr).length > 0 ? tb.find('#' + idtr).find('input.punprefijo').val() : (li.children('.titulo').text() || '').substring(0, 3).toUpperCase();
        var tr = tb.find('#' + idtr).length > 0 ? tb.find('#' + idtr) : tb.find('tbody tr.trclone').clone();
        tr.attr('id', idtr);
        tr.attr('dataid', dataid);
        tr.find('.punitem').text(titulo);
        tr.find('input.punprefijo').val(prefijo);
        tr.attr('type', type)
        tr.removeClass('trclone').show();
        if (tb.find('#tr' + idtr).length == 0) tb.find('tbody').append(tr);
        md.find('select.pungrupo').first().trigger('change');
      }
      var __calculogrupopuntaje = function() {
        var formula = [];
        var grupos = [];
        var grupostmp = [];
        md.find('tbody tr').first().nextAll().removeAttr('class');
        md.find('tbody tr').each(function(ii, _trtm) {
          _trtmp = $(_trtm);
          var grupo = _trtmp.attr('grupo') || 'N';
          if (!_trtmp.hasClass('trclone')) {
            if (grupos[grupo] == undefined) {
              grupos[grupo] = [];
              grupostmp.push(grupo);
            }
            grupos[grupo].push(_trtmp);
          }
        })
        if (grupostmp.length <= 1) md.find('.haygrupo').hide();
        else {
          md.find('.haygrupo').show();
        }
        var valgrupos_ = false;
        var sumgrupos_ = 0;
        $.each(grupostmp, function(igt, ggt) {
          var grupo = grupos[ggt];
          //var gruposjsontmp={'id':ggt,'sum':0,tipocal='P'};
          var validar = false;
          var sumgrupo = 0;
          if (igt == 0) {
            formula['PF'] = [];
          }
          $.each(grupo, function(ig, gg) {
            sumgrupo = sumgrupo + parseInt(gg.find('select.punporcentaje').val())
            var punporcentaje = grupo[0].find('select.punporcentaje').val();
            var punporcentajegg = gg.find('select.punporcentaje').val();
            var vg = gg.find('select.pungruporcentaje').val();
            if (punporcentaje == 'P') {
              gg.find('select.punporcentaje').removeClass('bg-danger').val('P');
              gg.find('select.punporcentaje option[selected]').removeAttr('selected');
              gg.find('select.punporcentaje option[value="P"]').attr('selected', 'selected');
              punporcentajegg = 'P';
            }
            if (ig == 0) {
              //gg.find('select.punporcentaje').show();
              var vgtmp = gg.find('select.pungrupo').val();
              gg.find('select.pungruporcentaje').show();

              if (grupo.length == 1) {
                gg.find('select.punporcentaje').hide();
                if (formula[ggt] == undefined)
                  formula[ggt] = [{
                    'tipo': 'T',
                    'id': gg.attr('dataid'),
                    'prefijo': gg.find('input.punprefijo').val(),
                    'valortipo': 1,
                    'titulo': gg.find('td.punitem').text(),
                    'typerecurso': gg.attr('type' || '')
                  }];
              } else {
                if (formula[ggt] == undefined)
                  formula[ggt] = [{
                    'tipo': punporcentajegg == 'P' ? 'P' : '%',
                    'id': gg.attr('dataid'),
                    'prefijo': gg.find('input.punprefijo').val(),
                    'valortipo': punporcentaje,
                    'titulo': gg.find('td.punitem').text(),
                    'typerecurso': gg.attr('type') || ''
                  }];
                gg.find('select.punporcentaje').show();
              }

              if (grupostmp.length == 1)
                formula['PF'] = [{
                  'tipo': 'T',
                  'nombre': gg.find('select.pungrupo option:selected').text(),
                  'prefijo': vgtmp,
                  'formula': formula[ggt],
                  'valortipo': 1
                }];
              else
                formula['PF'].push({
                  'tipo': vg == 'P' ? 'P' : '%',
                  'nombre': gg.find('select.pungrupo option:selected').text(),
                  'prefijo': vgtmp,
                  'formula': formula[ggt],
                  'valortipo': vg
                });
              if (vg != 'P') valgrupos_ = true;
              sumgrupos_ = sumgrupos_ + parseInt(vg);
            } else {
              gg.find('select.pungruporcentaje').hide();
              gg.find('select.punporcentaje').hide();
              formula[ggt].push({
                'tipo': punporcentajegg == 'P' ? 'P' : '%',
                'id': gg.attr('dataid'),
                'prefijo': gg.find('input.punprefijo').val(),
                'valortipo': punporcentajegg,
                'titulo': gg.find('td.punitem').text(),
                'typerecurso': gg.attr('type')
              });
            }
            if (ig > 0 && punporcentaje != 'P' && punporcentaje < 100) {
              validar = true;
              if (punporcentajegg == 'P') {
                gg.find('select.punporcentaje').addClass('bg-danger');
              }
              gg.find('select.punporcentaje').show();
            }
            if (gg.find('input.punprefijo').val() == '') {
              gg.find('input.punprefijo').css({
                'border': '1px solid red'
              });
            } else gg.find('input.punprefijo').css({
              'border': '1px solid #4683af'
            });
          })
          if (validar == true) {
            if (sumgrupo == 100) {
              $.each(grupo, function(ig, gg) {
                gg.find('select.punporcentaje').removeClass('bg-danger').closest('tr').removeClass('Error');
              })
            } else {
              $.each(grupo, function(ig, gg) {
                gg.find('select.punporcentaje').addClass('bg-danger').closest('tr').addClass('Error');
              })
            }
          }
        })
        if ((valgrupos_ == true && sumgrupos_ == 100) || valgrupos_ == false) {
          md.find('tbody tr').first().nextAll().find('select.pungruporcentaje').removeClass('bg-danger').closest('tr').removeClass('Error1');
        } else {
          md.find('tbody tr').first().nextAll().find('select.pungruporcentaje').addClass('bg-danger').closest('tr').addClass('Error1');
        }
        var txtformula = '';
        var formulafinal = formula['PF'];
        jsonformula = formulafinal;
        if (formulafinal.length == 1) {
          txtformula = '';
          var tipotmp = formulafinal[0].tipo;
          var nformula = formulafinal[0].formula.length;
          $.each(formulafinal[0].formula, function(i, v) {
            if (i == 0) tipotmp = v.tipo;
            txtformula += (v.tipo == 'P' || v.tipo == 'T' ? (v.prefijo.toUpperCase()) : (' (' + v.prefijo.toUpperCase() + '*' + (v.valortipo / 100) + ') ')) + '+';
          })
          if (tipotmp == 'P')
            txtformula = '<b>Pormedio Final = PF = (' + txtformula.substring(0, txtformula.length - 1) + ')/' + nformula + '</b>';
          else
            txtformula = '<b>Pormedio Final = PF =' + txtformula.substring(0, txtformula.length - 1) + '</b>';
        } else {
          txtformula = '';
          var tipotmp_ = formulafinal[0].tipo;
          var nformula_ = formulafinal.length;
          var txtformulatmp = '';
          var txtformulatmp_ = '';
          $.each(formulafinal, function(i, f) {
            var tipotmp = formulafinal[i].tipo;
            var nformula = formulafinal[i].formula.length;
            var txtformula = '';
            $.each(f.formula, function(i, v) {
              if (i == 0) tipotmp = v.tipo;
              txtformula += (v.tipo == 'P' || v.tipo == 'T' ? (v.prefijo.toUpperCase()) : (' (' + v.prefijo.toUpperCase() + '*' + (v.valortipo / 100) + ') ')) + '+';
            })
            if (tipotmp == 'P')
              txtformulatmp_ += f.nombre + ' = ' + f.prefijo + ' = (' + txtformula.substring(0, txtformula.length - 1) + ')/' + nformula + '<br>';
            else {
              if (f.prefijo == txtformula.substring(0, txtformula.length - 1))
                txtformulatmp_ += f.nombre + ' = ' + f.prefijo + '<br>';
              else
                txtformulatmp_ += f.nombre + ' = ' + f.prefijo + ' = ' + txtformula.substring(0, txtformula.length - 1) + '<br>';
            }

            txtformulatmp += (tipotmp_ == 'P' ? (formulafinal[i].prefijo.toUpperCase()) : (' (' + formulafinal[i].prefijo.toUpperCase() + '*' + (formulafinal[i].valortipo / 100) + ') ')) + '+';
          })
          if (tipotmp_ == 'P')
            txtformula = txtformulatmp_ + '<b>Promedio Final = PF = (' + txtformulatmp.substring(0, txtformulatmp.length - 1) + ')/' + nformula_ + '<b>';
          else
            txtformula = txtformulatmp_ + '<b>Promedio Final = PF = ' + txtformulatmp.substring(0, txtformulatmp.length - 1) + '</b>';

        }
        md.find('.formulagenerada').html(txtformula);
      }

      var link = '';
      md.on('change', 'select.pungrupo', function(ev) {
        var haygrupo = false;
        md.find('select.pungrupo').each(function(ii, ss) {
          _trtmp = $(ss).closest('tr');
          if (!_trtmp.hasClass('trclone')) {
            _trtmp.attr('grupo', $(ss).val());
          }
        })
        md.find('tbody tr select.pungruporcentaje').first().trigger('change');
      }).on('blur', 'input.punprefijo', function() {
        __calculogrupopuntaje();
      }).on('change', 'input.punprefijo', function() {
        var vv = $(this).val() || '';
        if (vv == '') {
          $(this).css({
            'border': '1px solid red'
          });
        } else $(this).css({
          'border': 'none'
        });
      }).on('change', 'select.pungruporcentaje, select.punporcentaje', function(ev) {
        var v = $(this).val();
        $(this).find('option[selected]').removeAttr('selected');
        $(this).find('option[value="' + v + '"]').attr('selected', 'selected');
        __calculogrupopuntaje();
      }).on('click', '.btnguardarpuntaje', function(ev) {
        var tb = md.find('#tablenuevapuntuacion');
        var hayerror1 = md.find('tbody tr.Error1').length
        var hayerror = md.find('tbody tr.Error').length
        if (hayerror > 0 || hayerror1 > 0) {
          Swal.fire({
            title: '<?php echo JrTexto::_('Errores en Calculos'); ?>',
            text: 'Al Parecer tiene errores en los calculos revise porfavor, las sumatorias deben ser igual al 100 %',
            icon: 'info',
            // showCancelButton: true,
            // confirmButtonColor: '#3085d6',
            //cancelButtonColor: '#d33',
            confirmButtonText: '<?php echo JrTexto::_('Aceptar'); ?>',
            //cancelButtonText: '<?php //echo JrTexto::_('Cancelar');
                                  ?>',
          })
          return false;
        }
        configuracion_nota.notas = jsonformula;
        var data = new FormData()
        data.append('idcurso', idcurso);
        data.append('campo', 'configuracion_nota');
        data.append('valor', JSON.stringify(configuracion_nota));
        __sysAyax({
          fromdata: data,
          url: _sysUrlBase_ + 'json/proyecto_cursos/setCampoxCurso',
          showmsjok: false,
        });
        __cerrarmodal(md, true);
      }).on('click', 'i.btnborrar', function(ev) {
        $(this).closest('tr').remove();
        __calculogrupopuntaje();
      })
      var timeval = setInterval(function() {
        if (md.find('table').length > 0) {
          clearInterval(timeval);
          __iniciotablapuntaje();
        }
      }, 1000);
    }).on('click', '.btnaddrubricas', function(ev) {
      el = $(this);
      $('#input-idinstancia').val('');
      var my_li = $(this).parent().parent();
      // if (condition) {
      let strCriterios = $(my_li).attr('criterios');

      if (strCriterios != '') {
        Swal.fire({
          icon: 'warning',
          title: 'No se puede agregar Rúbricas.',
          text: 'Esta tarea/proyecto cuenta con criterios asignados.',
        })
        return null;
      }
      var el = $(this);
      var li = el.closest('li');
      var lipadre = li.hasClass('esmenu') ? li : li.closest('.esmenu');
      var dt = {
        html: $('#showaddrubricas').find('#fix-md').clone(true)
      };

      var md = __sysmodal(dt, my_li);
      md.on('hidden.bs.modal', function(e) {
        _currentSelect = null;
      })
      getSelectRubrica(md, my_li);
      md.on('click', '.btnsaverubrica', function(e) {
        // working
        if (saveInstancia(md, my_li)) {
          my_li.children('.btn-group').children('i.btnaddrubricas').addClass('conrubrica');
          my_li.children('.accionesdecurso').children('.btnaddrubricas').addClass('bg-success');
          if (my_li.hasClass('esmenu')) {
            actulaizarjson(my_li);
          } else { //es pestaña
            actulaizarjson(my_li.parent().parent());
          }
          // actulaizarjson(my_li);
        };
      })
    })

    var actulaizarjson = function(lipadre) {
      // console.log('actulaizarjson', lipadre);
      if (!lipadre.hasClass('esmenu')) return; //si no es menu, regresa
      var ol = lipadre.children('ol');
      var ddlist = ol.children('li');
      var listorden = {}
      var idcursodetalle = lipadre.attr('idcursodetalle');
      if (ol.length)
        $.each(ddlist, function(i, v) {
          if (!$(v).hasClass('liclone')) {
            var link = ($(v).attr('link') || '');
            var index = link.lastIndexOf('static/');
            link = index > -1 ? link.substring(index) : link;
            var imagen = ($(v).attr('imagen') || '');
            var indeximg = imagen.lastIndexOf('static/');
            imagen = indeximg > -1 ? imagen.substring(indeximg) : imagen;
            var indeximg = imagen.lastIndexOf('nofoto');
            imagen = indeximg > -1 ? '' : imagen;
            var esrecursopagado = $(v).find('i.btnrecursopagado').hasClass('bg-success') ? 'si' : 'no';
            var necesitadocente = $(v).find('i.btnconteacher').hasClass('bg-success') ? 'si' : 'no';
            var estarea = $(v).find('i.btnestarea').hasClass('bg-success') ? 'si' : 'no';
            var esproyecto = $(v).find('i.btnesproyecto').hasClass('bg-success') ? 'si' : 'no';
            var iconproyecto = $(v).find('i.iconproyecto').hasClass('bg-success') ? 'si' : 'no';
            var mostrarpregunta = $(v).find('i.btnmostrarpregunta').hasClass('bg-success') ? 'si' : 'no';
            var conrubrica = $(v).find('i.btnaddrubricas').hasClass('bg-success') ? 'si' : 'no';
            listorden[i] = {
              "nombre": $(v).find('.titulo').text().trim(),
              "id": idcursodetalle + '' + i,
              "link": link,
              "type": $(v).attr('type'),
              "color": "rgba(0, 0, 0, 1)",
              "colorfondo": $(v).attr('color'),
              "colorfondopagina": $(v).attr('color'),
              "imagenfondo": imagen,
              "imagenfondopagina": imagen,
              'esrecursopagado': esrecursopagado,
              'necesitadocente': necesitadocente,
              'estarea': estarea,
              'esproyecto': esproyecto,
              'iconproyecto': iconproyecto,
              'mostrarpregunta': mostrarpregunta,
              'conrubrica': conrubrica,
              'criterios': $(v).attr('criterios') || ''

            };
          }
        })
      var _txtjson_ = {}
      _txtjson_.tipofile = lipadre.attr('txtjson-tipofile');
      _txtjson_.tipo = lipadre.attr('txtjson-tipo');
      _txtjson_.esrecursopagado = lipadre.children('.btn-group').children('i.btnrecursopagado').hasClass('bg-success') ? 'si' : 'no';
      _txtjson_.necesitadocente = lipadre.children('.btn-group').children('i.btnconteacher').hasClass('bg-success') ? 'si' : 'no';
      _txtjson_.estarea = lipadre.children('.btn-group').children('i.btnestarea').hasClass('bg-success') ? 'si' : 'no';
      _txtjson_.esproyecto = lipadre.children('.btn-group').children('i.btnesproyecto').hasClass('bg-success') ? 'si' : 'no';
      //_txtjson_.iconproyecto = lipadre.children('.btn-group').children('i.iconproyecto').hasClass('bg-success') ? 'si' : 'no';
      _txtjson_.mostrarpregunta = lipadre.children('.btn-group').children('i.btnmostrarpregunta').hasClass('bg-success') ? 'si' : 'no';
      _txtjson_.conrubrica = lipadre.children('.btn-group').children('i.conrubrica').hasClass('bg-success') ? 'si' : 'no';
      _txtjson_.icon = lipadre.attr('data-icon') || '#';
      _txtjson_.criterios = lipadre.attr('criterios') || '';
      var link = (lipadre.attr('txtjson-link') || '');
      var index = link.lastIndexOf('static/');
      link = index > -1 ? link.substring(index) : link;
      _txtjson_.link = link;
      _txtjson_.infoavancetema = lipadre.attr('txtjson-tipofile');
      _txtjson_.typelink = lipadre.attr('txtjson-typelink');
      _txtjson_.infoavancetema = 100;
      _txtjson_.options = listorden;
      _txtjson_.criterios = lipadre.attr('criterios') || '';
      var idcurso = $('#idcurso').val();
      var formData = new FormData();
      formData.append('idcursodetalle', parseInt(idcursodetalle));
      formData.append('campo', 'txtjson');
      formData.append('valor', JSON.stringify(_txtjson_));
      formData.append('icc', <?php echo $this->idcomplementario; ?>);
      __sysAyax({
        fromdata: formData,
        url: _sysUrlBase_ + 'json/acad_cursodetalle/setCampo',
        // async:false,
        callback: function(rs) {
          console.log('[__sysAyax from actulaizarjson]', rs)
        }
      })
    };

    var tmpcategorias = {};
    $('select#categoria').on('change', function(ev) {
      var id = parseInt($(this).val());
      $.each(tmpcategorias, function(i, v) {
        var idcat = parseInt(v.idcategoria);
        if (idcat == id) {
          var cat = v.hijos;
          $('select#subcategoria').find('option').remove();
          if (cat.length > 0) {
            $.each(cat, function(i, v) {
              var op = '<option value="' + v.idcategoria + '">' + v.nombre + '</option>';
              $('select#subcategoria').append(op);
            })
          } else {
            var op = '<option value="0">Todas las subcategorias</option>';
            $('select#subcategoria').append(op);
          }
        }
      })
    });

    $('.addcategoria').on('click', function(ev) {
      ev.preventDefault();
      var pnlcat = $('table.tablecategorias');
      var trclone = pnlcat.find('.trclone').clone(true);
      var cat = $('select#categoria');
      var scat = $('select#subcategoria');
      var idcate = parseInt(cat.val() || 0);
      var idscat = parseInt(scat.val() || 0);
      var txt = '';
      if (idscat > 0) {
        txt = cat.find('option:selected').text() + ' -> ' + scat.find('option:selected').text();
        idcate = idscat;
        txtcat = scat.find('option:selected').text();
      } else {
        txt = cat.find('option:selected').text();
        idcate = parseInt(cat.val());
        txtcat = cat.find('option:selected').text();
      }
      idcurso = $('#idcurso').val();
      var formData = new FormData();
      formData.append('idcat', idcate);
      formData.append('idcur', idcurso);
      formData.append('acc', 'I');
      __sysAyax({
        'fromdata': formData,
        showmsjok: false,
        url: _sysUrlBase_ + 'smart/acad_curso/guardarcategorias',
        callback: function(rs) {
          var id = rs.data;
          trclone.removeClass('trclone').show();
          trclone.children('td:first').text(txt);
          trclone.attr('idcursocategoria', id);
          pnlcat.append(trclone);
          //updateavance(parseInt($('ul#ultabs a[data-show="paso2"]').attr('data-value')));     
        }
      });
    })
    $('table.tablecategorias').on('click', '.removetcat', function(ev) {
      var tr = $(this).closest('tr');
      var idcat = tr.attr('idcursocategoria');
      var formData = new FormData();
      formData.append('id', idcat);
      formData.append('acc', 'D');
      __sysAyax({
        fromdata: formData,
        showmsjok: false,
        url: url_media + '/smart/acad_curso/guardarcategorias',
        callback: function(rs) {
          tr.remove();
        }
      });
    });

    var formData = new FormData();
    var idcurso = $('#idcurso').val();
    formData.append('rjson', 'si');
    formData.append('idcurso', parseInt(idcurso));
    __sysAyax({
      fromdata: formData,
      showmsjok: false,
      url: _sysUrlBase_ + 'smart/cursos/paso1categorias',
      callback: function(rs) {
        var dt = rs.categorias;
        var catcurso = rs.categoriascurso;
        tmpcategorias = dt;
        if (dt != undefined) {
          if (dt.length > 0) {
            $('select#categoria').find('option').remove();
            $.each(dt, function(i, v) {
              var op = '<option value="' + v.idcategoria + '">' + v.nombre + '</option>';
              $('select#categoria').append(op);
            })
            $('select#categoria').trigger('change');
          } else {
            var op = '<option value="0">Todas las categorias</option>';
            $('select#categoria').html(op);
          }
        }
        if (catcurso.length > 0) {
          var tbcategorias = $('table.tablecategorias');
          $.each(catcurso, function(i, v) {
            $.each(tmpcategorias, function(ii, vv) {
              if (v.idcategoria == vv.idcategoria) {
                var trclone = tbcategorias.find('.trclone').clone(true);
                trclone.removeClass('trclone').show();
                trclone.children('td').first().text(v.categoria);
                trclone.attr('idcursocategoria', v.idcursocategoria);
                tbcategorias.append(trclone);
              } else if (vv.hijos.length > 0) {
                $.each(vv.hijos, function(hi, hv) {
                  if (hv.idcategoria == v.idcategoria) {
                    var trclone = tbcategorias.find('.trclone').clone(true);
                    trclone.removeClass('trclone').show();
                    trclone.children('td:first').text(vv.nombre + ' -> ' + v.categoria);
                    trclone.attr('idcursocategoria', v.idcursocategoria);
                    tbcategorias.append(trclone);
                  }
                })
              }
            });
          });
        }
      }
    });

    var validarcurso = function() {
      var idcurso = parseInt($('#idcurso').val() || '0');
      if (idcurso == 0) $('.form_wizard').find('.buttonNext').addClass('buttonDisabled');
      else $('.form_wizard').find('.buttonNext').removeClass('buttonDisabled');
    }
    $('.form_wizard').on('click', '.buttonNext', function(ev) {
      if ($(this).hasClass('buttonDisabled')) {
        ev.preventDefault();
        return;
      }
    })
    validarcurso();
    // activate Nestable for list 1
    $('#nestable').nestable({
      maxDepth: 2
    }).on('change', function(ev) {
      ev.preventDefault();
      var ol = $(this).children('ol');
      var ddlist = ol.children('li');
      var listorden = [];
      $.each(ddlist, function(i, v) {
        if (!$(v).hasClass('liclone')) {
          var idcursodetalle = $(v).attr('idcursodetalle');
          listorden[i] = {
            idcursodetalle: idcursodetalle,
            orden: i
          };
        }
        $(v).attr('orden', i);
      })
      var idcurso = $('#idcurso').val();
      var formData = new FormData();
      formData.append('idcurso', parseInt(idcurso));
      formData.append('datos', JSON.stringify(listorden));
      formData.append('idcomplementario', <?php echo $this->idcomplementario; ?>);
      __sysAyax({
        fromdata: formData,
        url: _sysUrlBase_ + 'json/acad_cursodetalle/reordenar',
        callback: function(rs) {
          /* console.log(rs);*/
        }
      })
    });

    var _nestablehijos = function() {
      var listorden_ = $('#nestable').children('ol').children('li');
      $.each(listorden_, function(i, v) {
        var oltmp = $(v).children('ol');
        if (oltmp.length) {
          var num = oltmp.attr('number');
          $('#nestable' + num).nestable({
              rootClass: 'dd' + num,
              maxDepth: 2,
              handleClass: "dd-handle" + num,
              listClass: "dd-list" + num,
              itemClass: "dd-item" + num
            })
            .on('change', function(ev) {
              ev.stopPropagation();
              var ol = $(this).closest('ol');
              var lipadre = ol.closest('li');
              actulaizarjson(lipadre);
            });
        }
      })
    }
    _nestablehijos();
  })

  function onchangecompetencias(e) {
    var formData = new FormData();
    // console.log('click change', e.target.value);
    formData.append('idcompetencia', e.target.value);
    $.ajax({
      url: _sysUrlBase_ + 'json/acad_capacidades/listado',
      type: 'POST',
      contentType: false,
      data: formData, // mandamos el objeto formdata que se igualo a la variable data
      processData: false,
      cache: false,
      beforeSend: function(objeto) {
        //$("#loadCategoria").html("<img src='../util/images/load.gif'>");
      },
      success: function(data) {
        var response = JSON.parse(data);
        var html = "";

        //console.log('data', response.data);
        if (response.data.length > 0) {
          $.each(response.data, function(i, item) {
            //if (item.estado == '1') {
            html += '<option value="' + item.idcapacidad + '">' + item.nombre + '</option>';
            //}
          });
          // console.log('result_capacidad', html);
          document.getElementById('txtidcapacidad').innerHTML = html;
          $("#txtidcapacidad").html(html);
          $("#txtidcapacidad").on('change', function() {
            //$("#txtidcapacidad")
          });
          //if (edit_bol) {
          //$("#txtidcapacidad").val(idcompetencia);
          //cboCursos(0, true, idpadre);
          //}
        } else {
          $("#txtidcapacidad").html(html);
        }
        //console.log('Datos Listado', response);
      }
    })
  }
</script>