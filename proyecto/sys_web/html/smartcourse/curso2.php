<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?>
<style>
.topnom{
    padding:0.5ex 1ex;
    color:#fff;
}
.bloqmenu{
    position:absolute;
    top:1ex;
    right:1ex;
}
.item-curso{
   /* height: 170px;
    /*display: block;*/
    padding:1.5em 1em;
    /*margin: 1em 1.5em;
    /*border-radius: 1ex;
    -webkit-box-shadow: 4px -7px 5px 3px rgba(19, 18, 18, 0.7803921568627451);
    -moz-box-shadow: 4px -7px 5px 3px rgba(19, 18, 18, 0.7803921568627451);
    background-color: 4px -7px 5px 3px rgba(19, 18, 18, 0.7803921568627451);*/
    position:relative;
    cursor:pointer;
}
.item-curso a{
    /*color: #fff;
    position: absolute;
    right:0px;*/
    display:none;

}
.item-curso:hover a{
    display:inline-block;
    position:relative;
    z-index:999;
    /*position: absolute;
    color:#ff9;*/
}
.cursoimage{
    position: absolute;   
    width: 100%;
    height: 90%;
    background-position: center;
    background-size: 80% 70%;
    background-repeat: no-repeat;
    top: -10px;
}
.nomcurso{
    position: absolute;
    bottom: -5px;
    width: 100%;
    background: #6799e62e;
    color: #b70707;
    padding: 0.25ex;
    border-bottom-left-radius: 0.8ex;
    border-bottom-right-radius: 0.8ex;
}


</style>
<div class="col-md-12">
    <div class="row bg-primary">    
        <div class="col-6 text-left topnom"><h3><?php echo JrTexto::_('My Courses');?></h3></div>
        <div class="col-6 text-right topnom">
            <a href="<?php echo $this->documento->getUrlBase() ?>proyecto/cursos/?view="><i class="fa fa-photo"></i></a>
            <a href="<?php echo $this->documento->getUrlBase() ?>proyecto/cursos/?view=1"><i class="fa fa-braille"></i></a>
            <a href="<?php echo $this->documento->getUrlBase() ?>proyecto/cursos/?view=2"><i class="fa fa-navicon"></i></a>            
            <a href="<?php echo $this->documento->getUrlBase() ?>proyecto/cursos/?view=4"><i class="fa fa-server"></i></a>
            <a href="<?php echo $this->documento->getUrlBase() ?>proyecto/cursos/?view=3"><i class="fa fa-folder-open"></i></a>
            <!--a href="<?php echo $this->documento->getUrlSitio();?>/cursos/crear" class="btn btn-success">
                <i class="fa fa-plus "></i> <span class="bolder"><?php echo JrTexto::_('New course');?></span>
            </a-->
            <!--a href="" class="btn btn-warning">
                <i class="fa fa-plus "></i> <span class="bolder">ver categorias</span>
            </a-->        
        </div>
    </div>
</div>
<div class="row" id="lscursos">
    
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var idproyecto=parseInt('<?php echo $this->idproyecto;?>');
var idcategoria=parseInt('<?php echo $this->idcategoria;?>');
$(document).ready(function(){
    var cargarcursos=function(turl){
        var tmpurl=turl||_sysUrlBase_+'json/acad_grupoauladetalle';
        //var tmpurl=turl||_sysUrlSitio_+'/proyecto_cursos/buscarjson';
        var formData = new FormData();        
        formData.append('idproyecto',idproyecto);
        if(idcategoria!='') formData.append('idcategoria',idcategoria);
        formData.append('iddocente',parseInt('<?php echo $this->curusuario["idpersona"];?>')); 

       
        __sysAyax({ // cargar informacion de usuario;
            fromdata:formData,
            showmsjok:false,
            url:tmpurl,
            callback:function(rs){
               dt=rs.data;
               html='';
               $.each(dt,function(i,v){
                var imgtemp=_sysUrlBase_+v.imagen||'';
                var ultimostaticp=imgtemp.lastIndexOf('static');
                if(ultimostaticp>0){
                    imgtemp=_sysUrlBase_+imgtemp.substr(ultimostaticp);
                }else{
                   imgtemp+='static/media/nofoto.jpg';
                }
                html+='<div class="col-md-2 col-sm-4 col-xs-6 text-center">';
                    html+='<div class="item-curso hvr-grow text-center" idcurso="'+v.idcurso+'">'
                    //html+='<div class="cursoimage" style="background-image:url('+img+');"></div>';
                    //html+='<a href="#" class="btn btn-sm btn-danger edit" idcurso="'+v.idcurso+'" alt="Editar curso"><i class="fa fa-pencil"></i></a>';
                    html+='<img src="'+imgtemp+'"  class="img-responsive img-fluid" style="max-width:200px; max-height:210px">'
                    //html+='<h4 class="nomcurso">'+v.nombre+'</h4>';
                    html+='</div></div>';
               });
               $('#lscursos').html(html);
            }
        });
    }

    $('#lscursos').on('click','.item-curso .edit',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var id=$(this).attr('idcurso');
        window.location.href=_sysUrlSitio_+'/cursos/crear?idcurso='+id;
    })
    $('#lscursos').on('click','.item-curso',function(ev){        
        var id=$(this).attr('idcurso');
        window.open(_sysUrlBase_+'smartcourse/cursos/ver/?idcurso='+id, '_blank');
    })
    cargarcursos();
})
</script>