<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;  
?>
<style type="text/css">
    .thumbnail{
        height: auto !important;
        /* padding: 1em; */
        padding: 0 !important;
    }
    .view-first{
        height: 100% !important;
    }
    .img-curso{
        width: 100% !important;
        display: block !important;
        text-align: center;
        cursor: pointer !important; 
    }
    .img-curso{
        text-align: center;
        margin: auto;
       min-height: 115px;
        min-width: 115px;
    }
    .mask{
        height: 100% !important;
    }
    .tools-bottom{
        background-color: #464646 !important;
        margin: 0 !important;
    }
    .categoria_ {
        font-weight: bold;
        margin: 0px;
        background: #a1e5f0;
        padding: 1ex 1ex 1ex 2ex;
        font-size: x-large;
        font-family: initial;
    }
    .subcategoria_{
        transform: rotate(-90deg);
        -webkit-transform: rotate(-90deg);
        text-align: center;
        vertical-align: middle !important;
        font-size: 20px;
        font-weight: bold;
        padding: 0 !important;
        max-width: 120px;
    }
    .subcategoria2_{     
        font-size: 20px;
        font-weight: bold;    
        padding-left: 2.5em !important;
        display: none;
    }

    .curso-lock{
        opacity: 1 !important;
    }
    .curso-lock > i{
        color: white;
        font-size: 45px;
        padding-top: 30%;
    }   
    .titulo-p{
        float: none !important;
        font-size: 24px !important;
        font-weight: bold !important;
        text-align: center !important;
        width: 100% !important;
    }
    .tools a { cursor: pointer; } 
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="titulo-p-2"><?php echo JrTexto::_('My Courses');?> <?php if(@$this->solocursosprincipales!=2){?> <small style="cursor: pointer" class="showasignados">
                    <i class="fa fa-check-square"></i> <?=JrTexto::_("Only show assigned courses")?>
                </small><?php } ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" id="btn-agregar" data-toggle="popover" title="Crea un nuevo curso" class="btn btn-success" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/crear" >
                            <i class="fa fa-plus"></i> <?php echo JrTexto::_('New course');?>
                          </button>
                          <?php if($this->curusuario["tuser"]=='s'){?>
                          <button type="button" id="btn-asignar" data-toggle="popover" title="Asigna un curso a este proyecto" class="btn btn-warning"data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/asignar"><i class="fa fa-location-arrow"></i> <?php echo ucfirst(JrTexto::_('Add'))." ".ucfirst(JrTexto::_('Course'));?> </button>
                            <?php } ?>
                          <button type="button" id="btn-ordenar" data-toggle="popover" title="Ordena el listado de cursos" class="btn btn-secondary" alt="<?php echo ucfirst(JrTexto::_('Order'));?>" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/ordenar"><i class="fa fa-sort-amount-desc"></i> <?php echo ucfirst(JrTexto::_('Order'));?> </button>
                          <button type="button" id="btncambiarvista" class="btn btn-success" data-toggle="popover" title="Cambia de vista " data-content="Cambia los módulos o subcategorías de vertical a horizontal "> <i class="fa fa-toggle-on" title="" ></i> </button>
                        </div>                    
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
          
            <div class="x_content" style=" padding: 1.5ex;">            
                <div class="row" id="lscursos">

               <div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                <?php 
                $bloquear=false;
                $ncursos=0;
                $aprovo=false;
                $open=true;
                $haycursosamostrar=false;
                if(!empty($this->categorias)){
                foreach ($this->categorias as $k=> $v){
                     if(empty($v["estado"])) continue;
                     if(!empty($v["tienecursos"])){ 
                        $haycursosamostrar=true;
                        ?>
                        <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="headingThree<?php echo $k; ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree<?php echo $k; ?>" aria-expanded="<?php echo $open==true?'true':'false'; ?>" aria-controls="collapseThree" style="font-family: Arial; padding: 0px; color:#111;">
                                <div class="panel-title categoria_"><i class="fa fa-angle-right" ></i> <?php echo $v["nombre"]; ?>                               
                                </div>                              
                            </a>                            
                        <div id="collapseThree<?php echo $k; ?>" class="panel-collapse collapse <?php echo $open==true?'show':''; ?>" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body" style="border: 0px;">
                        <table class="table table-striped">                           
                            <tbody>
                        <?php 
                        $open=false;
                            if(!empty($v["hijos"]))
                            foreach ($v["hijos"] as $ks=> $vs){ 
                                if(empty($vs["estado"])) continue;
                                if(!empty($vs["cursos"]) ){ ?>                                
                                    <tr><th class="subcategoria2_" colspan="2"> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $vs["nombre"]; ?></th></tr> 
                                <tr>
                                    <td class="subcategoria_"><?php echo $vs["nombre"]; ?></td>
                                    <td>
                                        <div class="row cursosdecategoria">                            
                                        <?php 
                                        foreach($vs["cursos"] as $kcur =>$cur){
                                            $img=$cur["imagen"];
                                            $posimg=strripos($img, 'static/');
                                            if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                                            else $img=URL_BASE.substr($img, $posimg);
                                            $haycursosamostrar=true;
                                            ?>
                                            <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                                                <div class="thumbnail">
                                                    <div class="image view view-first item-curso" idcurso="<?php echo $cur['idcurso'];?>" idcomplementario="<?php echo @$cur['idcomplementario'];?>" tipo="<?php echo @$cur['tipo'];?>" idcategoria="<?php echo @$cur['idcategoria'];?>">
                                                        <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $cur['nombre'];?>" />
                                                            <div class="mask">
                                                                <div class="tools tools-bottom" >
                                                                <?php if(@$cur['tipo']==2||$this->curusuario["tuser"]=='s'){ ?>
                                                                    <a class="a-mask edit" data-toggle="popover" title="<?=JrTexto::_("Edit Course")?>"><i class="fa fa-pencil"></i></a>
                                                                <?php if(empty($cur['idcomplementario'])){ ?>
                                                                        <a class="a-mask mirarcategoria" data-toggle="popover" title="<?=JrTexto::_("Edit category")?>"><i class="fa fa-cc"></i></a>
                                                                    <?php } ?>
                                                                    <a class="a-mask formular" data-toggle="popover" title="<?=JrTexto::_("Change course assessment formula")?>"><i class="fa fa-calculator"></i></a>
                                                                    <a class="a-mask remove" data-toggle="popover" title="<?=JrTexto::_("Delete course")?>"><i class="fa fa-trash"></i></a>
                                                                <?php } 
                                                                    if($this->curusuario["tuser"]=='s'){?>
                                                                    <a class="a-mask copy" data-toggle="popover" title="<?=JrTexto::_("Copy course")?>"><i class="fa fa-copy"></i></a>
                                                                    <a class="a-mask download-curso" data-toggle="popover" title="<?=JrTexto::_("Download course content")?>"><i class="fa fa-cloud-download"></i></a>
                                                                <?php } ?>
                                                                </div>
                                                            </div>
                                                        <div style="font-size: 11px; margin-top: 0px;"  id="nombrecurso"><?php echo $cur["nombre"]; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        </div>
                                    </td>
                                </tr>
                        <?php }
                    }
                    if(!empty($v["cursos"]) ){ ?>
                                <tr>                                    
                                    <td colspan="2">
                                        <div class="row cursosdecategoria">                            
                                        <?php 
                                        foreach($v["cursos"] as $kcur =>$cur){
                                            $img=$cur["imagen"];
                                            $posimg=strripos($img, 'static/');
                                            if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                                            else $img=URL_BASE.substr($img, $posimg);
                                            $haycursosamostrar=true;
                                            ?>
                                            <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                                                <div class="thumbnail">
                                                    <div class="image view view-first item-curso" idcurso="<?php echo $cur['idcurso'];?>" idcomplementario="<?php echo @$cur['idcomplementario'];?>" tipo="<?php echo @$cur['tipo'];?>" idcategoria="<?php echo @$cur['idcategoria'];?>">
                                                        <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $cur['nombre'];?>" />
                                                        <div class="mask">
                                                            <div class="tools tools-bottom" >
                                                                <?php if(@$cur['tipo']==2||$this->curusuario["tuser"]=='s'){ ?>
                                                                    <a class="a-mask edit" data-toggle="popover" title="<?=JrTexto::_("Edit course")?>"><i class="fa fa-pencil"></i></a>
                                                                <?php if(empty($cur['idcomplementario'])){ ?>
                                                                        <a class="a-mask mirarcategoria" data-toggle="popover" title="<?=JrTexto::_("Edit Category")?>"><i class="fa fa-cc"></i></a>
                                                                    <?php } ?>
                                                                    <a class="a-mask formular" data-toggle="popover" title="<?=JrTexto::_("Change course assessment formula")?>"><i class="fa fa-calculator"></i></a>
                                                                    <a class="a-mask remove" data-toggle="popover" title="<?=JrTexto::_("Delete course")?>"><i class="fa fa-trash"></i></a>
                                                                <?php } 
                                                                    if($this->curusuario["tuser"]=='s'){?>
                                                                    <a class="a-mask copy" data-toggle="popover" title="Copiar curso"><i class="fa fa-copy"></i></a>
                                                                    <a class="a-mask download-curso" data-toggle="popover" title="<?=JrTexto::_("Download course content")?>"><i class="fa fa-cloud-download"></i></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div style="font-size: 11px; margin-top: 0px; " id="nombrecurso"><?php echo $cur["nombre"]; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php  } ?>
                                        </div>
                                    </td>
                                </tr>
                        <?php }

                     ?>
                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                    <?php } ?>
                <?php }
                }
                if(!empty($this->haycursoslibres)){ ?>
                    <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="headingThreea" data-toggle="collapse" data-parent="#accordion1" href="#collapseThreea" aria-expanded="<?php echo $open==true?'true':'false'; ?>" aria-controls="collapseThree" style="font-family: Arial; padding: 0px; color:#111;">
                              <div class="panel-title categoria_"><i class="fa fa-angle-right" ></i> <?=JrTexto::_("Free course")?></div>
                            </a>
                        <div id="collapseThreea" class="panel-collapse collapse <?php echo $open==true?'show':''; ?>" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body" style="border: 0px;">
                            <div class="row">
                    <?php  
                    $open=false;
                    foreach ($this->haycursoslibres as $kl => $cur){ $haycursosamostrar=true; 
                         $img=$cur["imagen"];
                         $posimg=strripos($img, 'static/');
                         if($posimg===false)  $img=URL_BASE.$imgdefecto; 
                         else $img=URL_BASE.substr($img, $posimg);

                        ?>                            
                        <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                            <div class="thumbnail">
                                <div class="image view view-first item-curso"  idcurso="<?php echo $cur['idcurso'];?>" idcomplementario="<?php echo @$cur['idcomplementario'];?>" tipo="<?php echo @$cur['tipo'];?>" style="padding: 1ex"  idcategoria="<?php echo @$cur['idcategoria'];?>">
                                    <img class="img-curso" src="<?php echo $img;?>" alt="<?php echo $cur['nombre'];?>" />
                                    <div class="mask">
                                        <div class="tools tools-bottom" >
                                            <?php if(@$cur['tipo']==2||$this->curusuario["tuser"]=='s'){ ?>
                                                <a class="a-mask edit" data-toggle="popover" title="<?=JrTexto::_("Edit Course")?>"><i class="fa fa-pencil"></i></a>
                                            <?php if(empty($cur['idcomplementario'])){ ?>
                                                    <a class="a-mask mirarcategoria" data-toggle="popover" title="<?=JrTexto::_("Edit Category")?>"><i class="fa fa-cc"></i></a>
                                                <?php } ?>
                                                <a class="a-mask formular" data-toggle="popover" title="<?=JrTexto::_("Change course assessment formula")?>"><i class="fa fa-calculator"></i></a>
                                                <a class="a-mask remove" data-toggle="popover" title="<?=JrTexto::_("Delete course")?>"><i class="fa fa-trash"></i></a>
                                            <?php } 
                                                if($this->curusuario["tuser"]=='s'){?>
                                                <a class="a-mask copy" data-toggle="popover" title="Copiar curso"><i class="fa fa-copy"></i></a>
                                                <a class="a-mask download-curso" data-toggle="popover" title="<?=JrTexto::_("Download course content")?>"><i class="fa fa-cloud-download"></i></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div style="font-size: 11px; margin-top: 0px;   "  id="nombrecurso" ><?php echo $cur["nombre"]; ?></div>
                                </div>
                            </div>
                        </div>    
                        <?php }  ?>                           
                        </div>
                        </div>
                        </div>
                    </div>
                <?php }
                if($haycursosamostrar==false){ ?>
                    <div class="row">
                        <div style="text-align: center; font-size: 2em;
                            /*position: absolute;*/
                            width: 100%;
                            background-color: white;
                            height: 100%;">
                                <br><br>
                                <i class="fa fa-info" style="font-size: 3em"></i><br><br>
                                <?=JrTexto::_("You have no courses currently assigned")?>.
                            </div>
                    </div>
                  <?php } ?>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="display: none">
    <div id="shopnlcategorias"> 
        <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoaula" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
        <div class="col-md-6 col-sm-6 col-xs-12 pnlcat">   
          <label><?=JrTexto::_("Level")?> / <?=JrTexto::_("Career")?> / <?=JrTexto::_("Category")?></label>         
          <div class="select-ctrl-wrapper select-azul">
          <select id="idcategoria" name="idcategoria"  class=" form-control selchangecat pnlidcategoria0">
              <option value="0"><?=JrTexto::_("Unique")?> - <?=JrTexto::_("Free Course")?></option>
              <?php if(!empty($this->categoriasProyecto))
              foreach ($this->categoriasProyecto as $k=>$v){ if($v["idpadre"]==0){?>
                <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
              <?php } } ?>
          </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 pnlcat" style="display:none">   
          <label><?=JrTexto::_("Sub Level")?> / <?=JrTexto::_("Module")?> / <?=JrTexto::_("Sub Category")?></label>         
          <div class="select-ctrl-wrapper select-azul">
          <select id="nomsel" name="nomsel" class=" form-control selchangecat pnlidcategoria1">
              <option value="0"><?=JrTexto::_("Select")?></option>
              <?php if(!empty($this->categoriasProyecto))
              foreach ($this->categoriasProyecto as $k=>$v){
                if($v["idpadre"]!=0){?>
                <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
              <?php } } ?>                
          </select>
          </div>
        </div>
        <div class="clearfix"></div>           
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" tb="acad_grupoaula" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
</div>
<div style="display: none">
    <div id="pnlcopiarcursos">
        <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoaula" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
            <div class="row">               
                <div class="col-md-6 col-xs-12 pnlaccion">   
                  <label><?=JrTexto::_("Select")?></label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="accion" name="accion"  class=" form-control selchangecat pnlidcategoria0">
                        <option value="copiar">Copiar</option> 
                        <option value="asignar">Asignar</option>
                  </select>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12 pnlempresa">   
                  <label><?=JrTexto::_("Select Company")?> / <?=JrTexto::_("Project")?></label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="idproyecto" name="idproyecto"  class=" form-control selchangecat pnlidcategoria0">
                    <?php if(!empty($this->proyectos))
                         foreach ($this->proyectos as $k => $v){?>
                            <option value="<?php echo $v['idproyecto']; ?>"><?php echo $v['idproyecto'].": ".$v['empresa']; ?></option>
                         <?php } ?>
                  </select>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12 ">   
                  <label><?=JrTexto::_("Copy as")?> : </label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="como" name="como"  class=" form-control selchangecat pnlidcategoria0">
                        <option value="1">Autoestudio - Tipo 1</option> 
                        <option value="2">Curso oficial de la empresa - Tipo 2 </option>                     
                  </select>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12 ">   
                  <label><?=JrTexto::_("Name of the course to be copied")?> : </label>
                  <input type="text" id="nombre" name="nombre" class="form-control" placeholder="<?=JrTexto::_("Name of the course to be copied")?>">
                </div>

                <div class="clearfix"></div>           
                  <hr>
                  <div class="col-md-12 form-group text-center">
                    <button id="btn-save" tb="acad_grupoaula" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
                    <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    const oUSER = <?php echo !empty($this->curusuario)? json_encode($this->curusuario,true):'[]';?>;
$(document).ready(function(){
    $("#btn-agregar , #btn-ordenar, #btn-asignar").click(function(){
        location.href = $(this).data("href");
    })

    $('#btncambiarvista').click(function(ev){
        if($(this).children('i').hasClass('fa-toggle-on')){
            $('#btncambiarvista').children('i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
            $('.subcategoria2_').show();
            $('.subcategoria_').hide(); 
            localStorage.setItem('mostrarmodulosverical',true);   
        }else{
            $('#btncambiarvista').children('i').removeClass('fa-toggle-off').addClass('fa-toggle-on');
            $('.subcategoria_').show();
            $('.subcategoria2_').hide();
            localStorage.setItem('mostrarmodulosverical',false);
        }
    })
    var mostrarmodulosverical=localStorage.getItem('mostrarmodulosverical')||false;
    if(mostrarmodulosverical=='true'){
        $('#btncambiarvista').children('i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
        $('.subcategoria2_').show();
        $('.subcategoria_').hide();
    }

    $('[data-toggle="popover"]').popover({
        container: 'body',
        trigger:'hover',
        placement:'bottom'
    })
    $('#lscursos').on('click','.item-curso .edit',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso');
        var idcc='&icc='+(dt.attr('idcomplementario')||0);
        idcc+='&tipo='+(dt.attr('tipo')||1);
        idcc+='&cat='+(dt.attr('idcategoria')||0);
        window.location.href=_sysUrlSitio_+'/cursos/crear?idcurso='+id+idcc;
    }).on('click','.item-curso .mirarcategoria',function(ev){
        ev.preventDefault();
        ev.stopPropagation();        
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso')||0;
        var idcc=dt.attr('idcomplementario')||0; 
        var tipo=dt.attr('tipo')||1; 
        var idcat=dt.attr('idcategoria')||1;    
        var titulo = '<?php echo JrTexto::_("Course Categories"); ?>';
        var frm=$('#shopnlcategorias').clone();
        var _md = __sysmodal({'html': frm,'titulo': titulo});
        _md.on('change','select',function(ev){
            var el=$(this);            
            if(el.hasClass('pnlidcategoria0')){
                if(el.val()>0){
                    _md.find('select.pnlidcategoria0').attr('id','selcat').attr('name','selcat');
                    _md.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria');
                    _md.find('select.pnlidcategoria1').children('option').first().nextAll().hide().removeClass('porsel');
                    _md.find('select.pnlidcategoria1').children('option[padre="'+el.val()+'"]').show().addClass('porsel');
                    _md.find('select.pnlidcategoria1').parent().parent().show();
                    if(_md.find('select.pnlidcategoria1').children('option.porsel[value="'+idcat+'"]').length>0)
                        _md.find('select.pnlidcategoria1').val(idcat);
                    else  _md.find('select.pnlidcategoria1').val(0);

                }else{
                    _md.find('select.pnlidcategoria1').attr('id','selcat').attr('name','selcat');
                    _md.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria');
                    _md.find('select.pnlidcategoria1').parent().parent().hide();
                }
            }
        }).on('click','#btn-save',function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var data=new FormData()
            data.append('idcurso',id);
            data.append('idcomplementario',idcc);
            if(_md.find('select#idcategoria').hasClass('pnlidcategoria0'))
                data.append('idcategoria',_md.find('select#idcategoria').val());
            else{
                let idcattmp=_md.find('select#idcategoria').val()||0;
                if(idcattmp==0){
                    data.append('idcategoria',_md.find('select#selcat').val());
                }else 
                    data.append('idcategoria',_md.find('select#idcategoria').val());
            }
            data.append('tipo',tipo);
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/acad_curso/guardarcategoria',
                callback:function(rs){
                  if(rs.code==200){
                    window.location.reload();
                  }
                }
            });
        })

        var pnl1=_md.find('select.pnlidcategoria0').find('option[value="'+idcat+'"]');
        if(pnl1.length>0){
            _md.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria').val(idcat);
            _md.find('select.pnlidcategoria1').attr('id','selcat').attr('name','selcat');
            _md.find('select.pnlidcategoria1').parent().parent().hide();
        }else{
            var pnl2=_md.find('select.pnlidcategoria1').find('option[value="'+idcat+'"]');
            if(pnl2.length>0){
                _md.find('select.pnlidcategoria0').attr('id','selcat').attr('name','selcat').val(pnl2.attr('padre')||0);
                _md.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria').val(idcat);
                _md.find('select.pnlidcategoria1').children('option').first().nextAll().hide();
                _md.find('select.pnlidcategoria1').children('option[padre="'+(pnl2.attr('padre')||0)+'"]').show();
                _md.find('select.pnlidcategoria1').parent().parent().show();
            }
        }
    }).on('click','.item-curso .copy',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso')||0;
        var idcc=dt.attr('idcomplementario')||0;
        var tipo=dt.attr('tipo')||1;  
        var nombrecurso = dt.find("#nombrecurso").text();
        var titulo = '<?php echo JrTexto::_("Copy or Assign course"); ?>: '  +nombrecurso;
        var frm=$('#pnlcopiarcursos').clone();
        if(tipo==2){
            titulo = '<?php echo JrTexto::_("Copy course"); ?>: ' +nombrecurso;
            pnlaccion =frm.find('.pnlaccion').hide();
            frm.find('select#accion option[value="copiar"]').attr('selected','selected').siblings('option').removeAttr('selected');
            frm.find('.pnlempresa').removeClass('col-md-6 col-md-12').addClass('col-md-12');
        }
        frm.find('select#idproyecto').val(<?php echo $this->idproyecto; ?>);
        frm.find('input#nombre').val(nombrecurso);
        var _md = __sysmodal({'html': frm,'titulo': titulo,zindex:true}); 
        _md.on('click','#btn-save',function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if($(this).hasClass('disabled')) return;
            $(this).addClass('disabled');
            var el=$(this);
            Swal.showLoading()
            var data=new FormData()
            data.append('idcurso',id);
            data.append('idcomplementario',idcc);
            data.append('accion',_md.find('select#accion').val());
            data.append('idproyecto',_md.find('select#idproyecto').val());
            data.append('tipo',tipo);
            data.append('como',_md.find('select#como').val());
            data.append('nombre',_md.find('input#nombre').val());
              __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/acad_curso/copiar',
                showmsjok:true,
                callback:function(rs){ 
                    el.removeClass('disabled');
                    //window.location.reload();
                }               
            });
              setTimeout(function(){_md.find('#btn-save').removeClass('disabled');},3000);
        }).on('change','#accion',function(ev){
            var acc=$(this).val();
            if(acc=='asignar'){
                _md.find('select#como option[value="1"]').attr('selected','selected').siblings('option').removeAttr('selected');
                _md.find('select#como').parent().parent().hide();
            }else{
                _md.find('select#como').parent().parent().show();
            }
            _md.find('#btn-save').removeClass('disabled');
        }).on('change','#como',function(ev){
            _md.find('#btn-save').removeClass('disabled');
        })
        return false;     
    }).on('click','.item-curso .formular',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso');
        id+='&icc='+(dt.attr('idcomplementario')||0);
        id+='&tipo='+(dt.attr('tipo')||1);
        id+='&idcategoria='+(dt.attr('idcategoria')||0);        
        window.location.href=_sysUrlBase_+'/cursos/formular/?idcurso='+id;
    }).on('click','.item-curso .remove',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso');
        var tipo=dt.attr('tipo');
        var idcc=dt.attr('idcomplementario')||0; 
        Swal.fire({
            title: '<?php echo JrTexto::_('Are you sure to delete this course?');?>',
            text: '<?php echo JrTexto::_('Everything related to it will also be eliminated'); ?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '<?php echo JrTexto::_('Accept');?>',
            cancelButtonText: '<?php echo JrTexto::_('Cancel');?>',
        }).then((result) => {
            if (result.value){
                Swal.showLoading();
                // console.log($(this).closest(".visorCursos").parent())
                $.post(_sysUrlBase_ + 'json/acad_curso/eliminar',{
                    'idcurso': id,
                    'tipo': tipo,
                    'eliminar': 'no',
                    'idcc':idcc
                }, function(data){
                    Swal.close();
                    if(data.code == 200){
                        location.reload();
                    }else{
                        Swal.fire({                             
                            text: data.msj||'<?=JrTexto::_("Unexpected error, try later")?>',
                            type: 'warning',                                
                            confirmButtonText: '<?php echo JrTexto::_('Accept');?>',
                        })
                    }
                }, 'json');
            }
        });
    }).on('click','.item-curso',function(ev){        
        var id=$(this).attr('idcurso');
        var idcc='&icc='+($(this).attr('idcomplementario')||0);
        idcc+='&tipo='+($(this).attr('tipo')||1);
        idcc+='&idcategoria='+($(this).attr('idcategoria')||0);
        window.open(_sysUrlBase_+'smart/cursos/ver/?idcurso='+id+idcc, '_blank');
    }).on('click','.item-curso .download-curso',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var dt =$(this).closest('.item-curso');
        var id=dt.attr('idcurso')||0;
        var idcc=dt.attr('idcomplementario')||0;
        var tipo=dt.attr('tipo')||1;  
        var nombrecurso = dt.find("#nombrecurso").text().replace(' ','_');
        var data=new FormData()
        data.append('idcurso',id);
        data.append('idcomplementario',idcc);
        //data.append('tipo',tipo);
        swal.showLoading();
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/descargar/procesar',
            callback:function(rs){
              console.log(rs); 
              swal.close(); 
              if(rs.code==200){
                let a=document.createElement('a');
                //a.target='_blank';
                a.download=nombrecurso;
                a.href=rs.zip;
                a.click();
              }else{
                
                //swal
              }
            }
        });
    })

    $('small.showasignados').on('click',function(ev){
        ev.preventDefault();
        var i=$(this).children('i');
        if(i.hasClass('fa-check-square')){
            i.removeClass('fa-check-square').addClass('fa-square');
            $('.item-curso[idcomplementario="0"][tipo="2"]').parent().parent().hide();
        }else{
            i.removeClass('fa-square').addClass('fa-check-square');
            $('.item-curso[idcomplementario="0"][tipo="2"]').parent().parent().show();
        }
    })

    $('.accordion > .panel > .panel-heading').click(function(ev){
        if($(this).hasClass('collapsed')){ // se abre 
            let panel=$(this).parent().siblings('.panel');
            panel.children('.panel-heading').addClass('collapsed');
            panel.children('.show').removeClass('show').addClass('collapsed');
        }
    })
});
</script>