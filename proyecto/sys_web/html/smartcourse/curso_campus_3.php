<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;
?>
<style>
	.form-control, .input-group-addon{ border: 1px solid #cbcdcf; border-radius: 0.29em;}
	.card-secondary:not(.card-outline)>.card-header{ background-color:#ececec; }
	.card-secondary:not(.card-outline)>.card-header, .card-secondary:not(.card-outline)>.card-header a { color:#747c84; }
	.item-curso{ cursor: pointer !important; }
	.imgicon { margin: 0 auto; width: 100%; max-width: 250px; }
	.visorCursos .card-title {white-space: nowrap;
    text-overflow: ellipsis;
    width: 90%;
    overflow: hidden;}
</style>
<div class="card card-secondary">
	<div class="card-header">
		<div class="row justify-content-around">
			<div class="col-sm-4">
				<div class="form-group">
					<label><?=JrTexto::_("Study Groups")?></label>
					<select id="cmbgrupos" class="form-control">
						<?php if(!empty($this->grupos)): ?>
							<?php foreach($this->grupos as $grupo): ?>
								<option value="<?php echo $grupo['idgrupoaula'] ?>"	><?php echo $grupo['strgrupoaula'] ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</div>
					
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label><?=JrTexto::_("Category")?></label>
					<select id="cmbcategorias" class="form-control">
						<option value="0"><?=JrTexto::_("Unclassified")?></option>
						<?php if(!empty($this->categorias)): ?>
							<?php foreach($this->categorias as $v): ?>
								<option value="<?php echo $v['idcategoria'] ?>"><?php echo $v['nombre'] ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</div>
			</div>		
		</div>
    </div>
    <!-- END CARD -->
    <div class="card-body">
    	<div id="lscursos" class="row">
    		<?php if(!empty($this->categorias)): ?>
    			<?php $allCursos = array(); ?>
    			<?php foreach ($this->categorias as $k=> $v): ?>
    				<?php
    					//obtener los cursos
    					$tmpc = array_values(array_column($v['hijos'], 'cursos'));
    					if(!empty($tmpc[0])){
    						$allCursos =  array_merge($allCursos,$tmpc[0]) ;
    					}
    				?>
    			<?php endforeach; ?>
    			<?php if(!empty($allCursos)): ?>
    				<?php foreach($allCursos as $curso): ?>
    					<?php if(!empty($curso['estamatriculado'])): ?>
							<div class="col-lg-3 col-md-4 col-sm-4">
						    	<div class="card card-outline card-primary visorCursos">
						          <div class="card-header">
						            <h4 class="card-title"><?php echo $curso['nombre'] ?></h4>

						            <div class="card-tools">
						              <button type="button" class="btn btn-tool text-secondary" data-card-widget="collapse"><i class="fa fa-minus"></i>
						              </button>
						            </div>
						            <!-- /.card-tools -->
						          </div>
						          <!-- /.card-header -->
						          <div class="card-body" style="display: block;">
						          	<?php if(!empty($curso['imagen'])): ?>
						          		<?php
							          		$img=$curso["imagen"];
							                $posimg=strripos($img, 'static/');
							                if($posimg===false)  $img=URL_BASE.$imgdefecto; 
							                else $img=URL_BASE.substr($img, $posimg);
						          		?>
						          		<div class="image view view-first item-curso" idcurso="<?php echo $curso['idcurso'];?>" idcomplementario="<?php echo @$curso['idcomplementario'];?>" tipo="<?php echo @$curso['tipo'];?>">
						          			<img class="img-curso imgicon" src="<?php echo $img ?>" />
						          		</div>
						          	<?php else: ?>
						            	<h1 class="text-center"><i class="fa fa-picture-o" aria-hidden="true"></i></h1>
						          	<?php endif; ?>
						          </div>
						          <!-- /.card-body -->
						        </div>
				    		</div>
    					<?php endif; ?>
    				<?php endforeach; ?>
    			<?php endif; ?>
    		<?php endif; ?>
    		
    	</div>
    </div> 
</div>

<script type="text/javascript">
	const CATEGORIAS = <?php echo !empty($this->categorias) ? json_encode($this->categorias) : '[]'; ?>;
	const CURSOS = <?php echo !empty($allCursos) ? json_encode($allCursos) : '[]' ?>;
	const GRUPODETALLES = <?php echo !empty($this->gruposdetalles) ? json_encode($this->gruposdetalles) : '[]' ?>;

	console.log(CATEGORIAS,CURSOS,GRUPODETALLES);
	
	$(document).ready(function(){
	    $('#lscursos').on('click','.item-curso .edit',function(ev){
	        ev.preventDefault();
	        ev.stopPropagation();
	        var id=$(this).attr('idcurso');
	        var idcc=$(this).attr('idcomplementario');
	        if(idcc != ""){
	            idcc = '&icc=' + idcc;
	        }
	        // console.log(_sysUrlSitio_+'/cursos/crear?idcurso='+id+idcc);
	        window.location.href=_sysUrlSitio_+'/cursos/crear?idcurso='+id+idcc;
	    })
	    $('#lscursos').on('click','.item-curso',function(ev){
	        var id=$(this).attr('idcurso');
	        var idcc=$(this).attr('idcomplementario');
	        if(idcc != ""){
	            idcc = '&icc=' + idcc;
	        }
	        window.open(_sysUrlBase_+'smart/cursos/ver/?idcurso='+id+idcc, '_blank');
	    });
	    $('body').on('change','#cmbgrupos',function(){
	    	var idgrupoaula = $(this).val();
	    	var filtro = [];
	    	var drawCursos = [];
	    	//buscar cursos con el idgrupoaula
	    	filtro = GRUPODETALLES.filter((el) => el.idgrupoaula == idgrupoaula);
			console.log(filtro,CURSOS);
	    	filtro.forEach((el)=>{
	    		var index = CURSOS.findIndex((e)=>e.idcurso == el.idcurso);
				console.log(index);
	    		if(index != -1 && drawCursos.findIndex((elm)=>elm.idcurso == CURSOS[index].idcurso) == -1){
	    			drawCursos.push(CURSOS[index]);
	    		}
	    	});
	    	drawListCourse(drawCursos);
	    });
	    $('body').on('change','#cmbcategorias',function(){
	    	var idcategoria = $(this).val();
	    	// var idgrupoaula = $('#cmbgrupos').val();
	    	// var filtro = [];
	    	// var drawCursos = [];

	    	findgrupos(idcategoria);
	    	// if(idgrupoaula == 0){
	    	// }else{
		    // 	//buscar categoria
		    // 	filtro = CATEGORIAS.filter((el) => el.idcategoria == idcategoria);

		    // 	filtro.forEach((el)=>{
		    // 		if(el.hijos != null && typeof el.hijos != 'undefined' && Object.keys(el.hijos).length > 0){
		    // 			el.hijos.forEach((h)=>{
		    // 				if(Object.keys(h.cursos).length > 0){
		    // 					drawCursos.push(h.cursos);
		    // 				}
		    // 			});
		    // 		}
		    // 	});
		    // 	drawListCourse(drawCursos);
	    	// }
	    });

	    $('#cmbcategorias').trigger('change');
	});
	//simular sttripos de php
	function strripos (haystack, needle, offset) {
		//  discuss at: https://locutus.io/php/strripos/
		// original by: Kevin van Zonneveld (https://kvz.io)
		// bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
		// bugfixed by: Brett Zamir (https://brett-zamir.me)
		//    input by: saulius
		//   example 1: strripos('Kevin van Zonneveld', 'E')
		//   returns 1: 16

		haystack = (haystack + '')
		.toLowerCase()
		needle = (needle + '')
		.toLowerCase()

		var i = -1
		if (offset) {
			i = (haystack + '')
		  		.slice(offset)
	  			.lastIndexOf(needle) // strrpos' offset indicates starting point of range till end,
			// while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
			if (i !== -1) {
			  i += offset
			}
		} else {
			i = (haystack + '')
				.lastIndexOf(needle)
		}
		return i >= 0 ? i : false
	}
	function findgrupos(idcategoria){
	    var group = [];
	    filtro = GRUPODETALLES.filter((el) =>{
	    	return (idcategoria == 0) ? (el.categoriapadre_grupoaula == null || el.categoriapadre_grupoaula == 0) : el.categoriapadre_grupoaula == idcategoria
	    });
	    filtro.forEach((el)=>{
	    	var indice = group.findIndex((i)=>i.idgrupoaula == el.idgrupoaula);
	    	if(indice == -1){
	    		group.push(el);
	    	}
	    });
	    $('#cmbgrupos').html('');
	    if(Object.keys(group).length > 0){
			group.forEach((el)=>{
				$('#cmbgrupos').append('<option value="'+el.idgrupoaula+'">'+el.strgrupoaula+'</option>');
		    });
	    }
	    $('#cmbgrupos').trigger('change');
	}
	function findcourses(filtros = {}){
		if(Object.keys(filtros).length == 0 || filtros.idgrupoaula == null || typeof filtros.idgrupoaula == 'undefined'){
			return [];
		}
		var idgrupoaula = filtros.idgrupoaula;
		var idcategoria = null;
    	var filtro = [];
    	var drawCursos = [];
    	var findcategoriaPadre = [];

		if(filtro.idcategoriaPadre != null && typeof filtro.idcategoriaPadre != 'undefined'){
    		findcategoriaPadre = CATEGORIAS.filter((el) => el.idcategoria == idcategoria);	
		}

    	//buscar cursos con el idgrupoaula
    	filtro = GRUPODETALLES.filter((el) => el.idgrupoaula == idgrupoaula);
    	filtro.forEach((el)=>{
    		var index = CURSOS.findIndex((e)=>e.idcurso == el.idcurso);
    		if(index != -1 && drawCursos.findIndex((elm)=>elm.idcurso == CURSOS[index].idcurso) == -1){
	    		if(Object.keys(findcategoriaPadre).length > 0){
	    			filtro.forEach((el)=>{
			    		if(el.hijos != null && typeof el.hijos != 'undefined' && Object.keys(el.hijos).length > 0){
			    			el.hijos.forEach((h)=>{
			    				if(Object.keys(h.cursos).length > 0){
			    					drawCursos.push(h.cursos);
			    				}
			    			});
			    		}
			    	});
	    		}else{
    				drawCursos.push(CURSOS[index]);
	    		}
    		}
    	});
	}
	function drawListCourse(courses){
		var contenedor = $('#lscursos');
		contenedor.html('');

    	if(Object.keys(courses).length > 0){
    		courses.forEach((el)=>{
    			var posimg = false;
    			_img = el.imagen;
				posimg = strripos(_img,'static/');
				if(posimg === false) {
					_img = String.prototype.concat(_sysUrlBase_,'static/media/cursos/nofoto.jpg');
				}else {
					_img = String.prototype.concat(_sysUrlBase_,_img.substring(posimg));
				}
    			contenedor.append(
    				`
					<div class="col-lg-3 col-md-4 col-sm-4">
				    	<div class="card card-outline card-primary visorCursos">
				          <div class="card-header">
				            <h4 class="card-title">${el.nombre}</h4>

				            <div class="card-tools">
				              <button type="button" class="btn btn-tool text-secondary" data-card-widget="collapse"><i class="fa fa-minus"></i>
				              </button>
				            </div>
				          </div>
				          <div class="card-body" style="display: block;">
							<div class="image view view-first item-curso" idcurso="${el.idcurso}" idcomplementario="${el.idcomplementario}" tipo="${el.tipo}">
				          			<img class="img-curso imgicon" src="${_img}" />
			          		</div>
						</div>
			         </div>
    				`
    				);
    		});
    	}
	}
</script>