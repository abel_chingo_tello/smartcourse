<style type="text/css">
    #nestable .dropdown-item {
        padding: 0.5ex 1.5ex;
        text-align: center;
    }
</style>
<?php
// ob_start(); 
function drawHtml($arrLi, $idcomplementario, $tipocurso, $eshijo = false)
{
    global $txticon; ?>
    <ol class="dd-list itemchange" id="">
        <?php
        $j = 0;
        if (!empty($arrLi)) {
            //DIBUJA MENUS?
            foreach ($arrLi as $k => $t) {
                $txtjsontema = json_decode($t["txtjson"], true);
                $tipo = !empty($txtjsontema["tipo"]) ? $txtjsontema["tipo"] : '#showpaddopciones';
                $tipofile = !empty($txtjsontema["tipofile"]) ? $txtjsontema["tipofile"] : 'arriba';
                $link = !empty($txtjsontema["link"]) ? $txtjsontema["link"] : '';
                $typelink = !empty($txtjsontema["typelink"]) ? $txtjsontema["typelink"] : '';
                $infoavance = 100;
                $txticon = !empty($txtjsontema["icon"]) ? '<i class="fa ' . $txtjsontema["icon"] . '"></i>' : '#';
                if ($txticon == '#' && ($t["tiporecurso"] == 'M' || $tiporecurso == 'E') &&  $typelink == 'smartquiz') {
                    $txticon = '<i class=" fa fa-list"></i>';
                    $txtjsontema['icon'] = 'fa-list';
                } else if (@$txtjsontema['icon'] == '' || @$txtjsontema['icon'] == '#' || empty($txtjsontema['icon'])) {
                    $txtjsontema['icon'] = '#';
                    $txticon = '#';
                }
                $fechaLimite = "nomostrar";
                $txtLmite = null;
                if (@$txtjsontema['estarea'] == 'si' || @$txtjsontema["esproyecto"] == 'si') {
                    if ((isset($txtjsontema["fecha_limite"]) && !empty($txtjsontema["fecha_limite"]))) {
                        $fechaLimite =  $txtjsontema["fecha_limite"];
                        if ($txtjsontema["fecha_limite"] == "Sin fecha limite") {
                            $txtLmite =  $txtjsontema["fecha_limite"];
                        } else {
                            $txtLmite =  "Vence: " . $txtjsontema["fecha_limite"];
                        }
                    }
                }

                $tienehijos = !empty($txtjsontema["options"]) ? 'tienehijos' : '';
                $txtconcalificacion = '';
                if ($typelink == 'enlacecolaborativo' || $typelink == 'foro' || $typelink == 'smartquiz' || @$txtjsontema["estarea"] == 'si' || @$txtjsontema["esproyecto"] == 'si') $txtconcalificacion = 'concalificacion';
                $txtconrubrica = (@$txtjsontema['estarea'] == 'si' || @$txtjsontema["esproyecto"] == 'si') ? 'conrubrica' : '';
                $vermenu = (@$txtjsontema['_vermenu'] == 'no') ? 'no' : 'si';

        ?>
                <!-- DIBUJA MENU SIMPLE O CONTENEDOR-->
                <!-- my-menu -->
                <li id="<?php echo $t["idcursodetalle"]; ?>" <?php if ($fechaLimite != "nomostrar") {
                                                                    echo 'title="' . $txtLmite . '" fecha_limite="' . $fechaLimite . '"';
                                                                } ?> espadre="<?= $t['espadre'] ?>" tipo-li="<?= ($t['espadre'] == 0) ? 'menu' : 'contenedor' ?>" idpadre="<?= $t['idpadre'] ?>" idcomplementario="<?= $idcomplementario ?>" accimagen="sg" imagen="<?php echo $t["imagen"]; ?>" url="<?php echo $t["url"]; ?>" idlogro="<?php echo $t["idlogro"]; ?>" tiporecurso="<?php echo $t["tiporecurso"]; ?>" idrecurso="<?php echo $t["idrecurso"]; ?>" orden="<?php echo $t["orden"]; ?>" data-idcursodetalle="<?php echo $t["idcursodetalle"]; ?>" idcursodetalle="<?php echo $t["idcursodetalle"]; ?>" class="dd-item <?= $t['espadre'] == 1 ? ' borde-padre ' : ' ' ?> itemchange1 esmenu <?php echo $tienehijos . " " . $txtconcalificacion . " " . $txtconrubrica; ?>" data-id="<?php echo $j ?>" color="<?php echo @$t["color"]; ?>" descripcion="<?php echo @$t["descripcion"]; ?>" txtjson-tipo="<?php echo $tipo; ?>" txtjson-tipofile="<?php echo $tipofile; ?>" txtjson-link="<?php echo $link; ?>" txtjson-typelink="<?php echo $typelink; ?>" style="border-bottom: 1px solid #ddd;" number="<?php echo $k; ?>" data-icon="<?php echo $txtjsontema['icon']; ?>" criterios="<?php echo @$txtjsontema["criterios"]; ?>">
                    <!-- <div class="dd-handle"> -->
                    <i class=" btn btn-xs fa fa-arrows dd-handle" title="<?php echo JrTexto::_('Move'); ?>" style="cursor: move; display: inline-block; margin-top: 0px;"></i> <span class="titulo"><?php echo $t["nombre"]; ?></span>
                    <span class="accionesdecurso btn-group" style="position: absolute; right: 0px;">
                        <!-- working -->
                        <?php if ($t['espadre'] == 0) { ?>
                            <i class="btn btn-xs fa fa-plus-circle btnaddoption" title="<?php echo JrTexto::_('Add Pestania'); ?>"></i>
                            <i class="btn btn-xs fa fa-magic btnaddhabilidades" title="<?php echo JrTexto::_('Add habilidades'); ?>"></i>
                            <!-- (mine rm) -->
                            <i class="btn btn-xs fa fa-archive btnaddrubricas <?php echo @$txtjsontema["conrubrica"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Add Rubrica'); ?>"></i>
                        <?php } ?>
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-xs btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo @$txticon; ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="width: 40px !important; min-width: 40px !important;     max-height: 220px; overflow-y: scroll;">
                                <a class="dropdown-item" data-icon="#"> <i class="mostraricon">#</i> </a>
                                <a class="dropdown-item" data-icon="fa-list"> <i class="mostraricon fa fa-list <?php echo $txtjsontema['icon'] == 'fa-list' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-folder-o"> <i class="mostraricon fa fa-folder-o <?php echo $txtjsontema['icon'] == 'fa-folder-o' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-file-text"> <i class="mostraricon fa fa-file-text <?php echo $txtjsontema['icon'] == 'fa-file-text"' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-bar-chart"> <i class="mostraricon fa fa-bar-chart <?php echo $txtjsontema['icon'] == 'fa-bar-chart' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-check-square-o"> <i class="mostraricon fa fa-check-square-o <?php echo $txtjsontema['icon'] == 'fa-check-square-o' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-envelope"> <i class="mostraricon fa fa-envelope <?php echo $txtjsontema['icon'] == 'fa-envelope' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-cloud-download"> <i class="mostraricon fa fa-cloud-download <?php echo $txtjsontema['icon'] == 'fa-cloud-download' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-film"> <i class="mostraricon fa fa-film <?php echo $txtjsontema['icon'] == 'fa-film' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-keyboard-o"> <i class="mostraricon fa fa-keyboard-o <?php echo $txtjsontema['icon'] == 'fa-keyboard-o' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-laptop"> <i class="mostraricon fa fa-laptop <?php echo $txtjsontema['icon'] == 'fa-laptop' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-book"> <i class="mostraricon fa fa-book <?php echo $txtjsontema['icon'] == 'fa-book' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-th-large"> <i class="mostraricon fa fa-th-large <?php echo $txtjsontema['icon'] == 'fa-th-large' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-th"> <i class="mostraricon fa fa-th <?php echo $txtjsontema['icon'] == 'fa-th' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-comments-o"> <i class="mostraricon fa fa-comments-o <?php echo $txtjsontema['icon'] == 'fa-comments-o' ? 'bg-success' : ''; ?>"></i> </a>
                                <a class="dropdown-item" data-icon="fa-clipboard"> <i class="mostraricon fa fa-clipboard <?php echo $txtjsontema['icon'] == 'fa-clipboard' ? 'bg-success' : ''; ?>"></i> </a>
                            </div>
                        </div>
                        <?php if ($t['espadre'] == 0) { ?>
                            <i class="btn btn-xs fa fa-question btnmostrarpregunta <?php echo @$txtjsontema["mostrarpregunta"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Show questions'); ?>"></i>
                            <i class="btn btn-xs fa fa-file btnaddcontent" title="<?php echo JrTexto::_('Add content'); ?>" style="<?php echo !empty($txtjsontema["options"]) ? 'display:none;' : ''; ?>"></i>
                            <i class="btn btn-xs fa fa-tasks btnestarea <?php echo @$txtjsontema["estarea"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es una tarea'); ?>"></i>
                            <i class="btn btn-xs fa fa-folder btnesproyecto <?php echo @$txtjsontema["esproyecto"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es un Proyecto'); ?>"></i>
                            <i class="btn btn-xs fa fa-dollar btnrecursopagado <?php echo @$txtjsontema["esrecursopagado"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es un Recurso Pagado'); ?>" <?php echo ($tipocurso == '1') ? '' : '"'; ?>></i>
                            <i class="btn btn-xs fa fa fa-graduation-cap btnconteacher <?php echo @$txtjsontema["necesitadocente"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Necesita docente'); ?>" style="display:none;"></i>
                        <?php } ?>
                        <!-- working -->
                        <i class="btn btn-xs fa btnverenmenu <?php echo @$vermenu == 'no' ? 'fa-eye-slash' : 'fa-eye'; ?>" title="<?php echo JrTexto::_('ver/ocultar menu'); ?>"></i>
                        <i class="btn btn-xs fa fa-pencil btneditmenu" title="<?php echo JrTexto::_('Edit'); ?>"></i>
                        <i class="btn btn-xs fa fa-trash btnremovemenu" title="<?php echo JrTexto::_('Remove'); ?>"></i>

                    </span>
                    <!-- </div>      -->
                    <!-- DIBUJA PESTAÑAS? -->
                    <?php if ($t['espadre'] != 1 && !empty($txtjsontema["options"])) { ?>
                        <!-- <div class="dd col-md-12" id="nestable2" style="width:100%; max-width: 100% !important;"> -->
                        <ol class="dd-list esjson" idcursodetalle="<?php echo $t["idcursodetalle"]; ?>">
                            <?php foreach ($txtjsontema["options"] as $kjson => $vjson) { //var_dump($vjson);
                                $txtconcalificacion = '';
                                $fechaLimiteP = "nomostrar";
                                $txtLimiteP = null;
                                if (@$vjson['estarea'] == 'si' || @$vjson["esproyecto"] == 'si') {
                                    if ((isset($vjson["fecha_limite"]) && !empty($vjson["fecha_limite"]))) {
                                        $fechaLimiteP =  $vjson["fecha_limite"];
                                        if ($vjson["fecha_limite"] == "Sin fecha limite") {
                                            $txtLimiteP =  $vjson["fecha_limite"];
                                        } else {
                                            $txtLimiteP =  "Vence: " . $vjson["fecha_limite"];
                                        }
                                    }
                                }
                                if (isset($vjson["type"]) && ($vjson["type"] == 'enlacecolaborativo' || $vjson["type"] == 'foro' || $vjson["type"] == 'smartquiz' || @$vjson["estarea"] == 'si' || @$vjson["esproyecto"] == 'si')) $txtconcalificacion = 'concalificacion';
                                $txtconrubrica = (@$vjson['estarea'] == 'si' || @$vjson["esproyecto"] == 'si') ? 'conrubrica' : '';
                                $vermenu = (@$vjson['_vermenu'] == 'no') ? 'no' : 'si';
                            ?>
                                <li idcomplementario="<?= $idcomplementario ?>" <?php if ($fechaLimiteP != "nomostrar") {
                                                                                    echo 'title="' . $txtLimiteP . '" fecha_limite="' . $fechaLimiteP . '"';
                                                                                } ?> tipo-li="pestania" idcursodetalle="<?php echo $t["idcursodetalle"]; ?>" idpestania="<?php echo $vjson["id"]; ?>" class="dd-item <?php echo $txtconcalificacion . " " . $txtconrubrica; ?>" data-id="<?php echo $kjson; ?>" id="<?php echo $vjson["id"]; ?>" type="<?php echo $vjson["type"]; ?>" link="<?php echo $vjson["link"]; ?>" color="<?php echo $vjson["color"]; ?>" imagen="<?php echo $vjson["imagenfondo"]; ?>" style="list-style: none; border-bottom: 1px solid #eee;" criterios="<?php echo !empty($vjson["criterios"]) ? $vjson["criterios"] : ''; ?>">
                                    <!-- <div class=""> -->
                                    <i class=" btn btn-xs fa fa-arrows dd-handle " title="<?php echo JrTexto::_('Move'); ?>" style="cursor: move; display: inline-block; margin-top: 0px;"></i> <span class="titulo"><?php echo $vjson["nombre"]; ?></span>
                                    <span class="accionesdecurso btn-group" style="position: absolute; right: 0px;">
                                        <i class="btn btn-xs fa fa-magic btnaddhabilidades" title="<?php echo JrTexto::_('Add habilidades'); ?>"></i>
                                        <i class="btn btn-xs fa fa-archive btnaddrubricas <?php echo @$vjson["conrubrica"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Add Rubrica'); ?>"></i>
                                        <i class="btn btn-xs fa fa-question btnmostrarpregunta <?php echo @$vjson["mostrarpregunta"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Show questions'); ?>"></i>
                                        <i class="btn btn-xs fa fa-file btnaddcontent" title="<?php echo JrTexto::_('Add content'); ?>"></i>
                                        <i class="btn btn-xs fa fa-tasks btnestarea <?php echo @$vjson["estarea"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es una tarea'); ?>"></i>
                                        <i class="btn btn-xs fa fa-folder btnesproyecto <?php echo @$vjson["esproyecto"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es un Proyecto'); ?>"></i>
                                        <i class="btn btn-xs fa fa-dollar btnrecursopagado <?php echo @$vjson["esrecursopagado"] == 'si' ? 'bg-success' : ''; ?>" title="<?php echo JrTexto::_('Es un Recurso Pagado'); ?>" <?php echo ($tipocurso == '1') ? '' : ' '; ?>></i>
                                        <i class="btn btn-xs fa btnverenmenu <?php echo @$vermenu == 'no' ? 'fa-eye-slash' : 'fa-eye'; ?>" title="<?php echo JrTexto::_('ver/ocultar menu'); ?>"></i>
                                        <i class="btn btn-xs fa fa-pencil btneditoption" title="<?php echo JrTexto::_('Edit'); ?>"></i>
                                        <i class="btn btn-xs fa fa-trash btnremoveoption" title="<?php echo JrTexto::_('Remove'); ?>"></i>
                                    </span>
                                    <!-- </div> -->
                                </li>
                            <?php } ?>
                        </ol>
                        <!-- </div> -->
                    <?php } ?>
                    <?php
                    //DIBUJA MENUS SIMPLES DENTRO DEL CONTENEDOR
                    if (isset($t['hijos']) && is_array($t['hijos']) && count($t['hijos']) > 0) {
                        drawHtml($t['hijos'], $idcomplementario, $tipocurso, true);
                    }
                    ?>

                </li>
            <?php $j++;
            }
        }
        if (!$eshijo) { ?>
            <li accimagen="sg" imagen="" ubicacion="static" url="" idpadre="0" idlogro="" 0 tiporecurso="M" idrecurso="0" orden="0" idcursodetalle="0" class="dd-item itemchange1 esmenu liclone" color="rgba(32, 50, 69, 0)" link="" descripcion="" data-id="<?php echo $j + 1; ?>" style="display:none; border-bottom: 1px solid #ddd;" txtjson-tipo="" txtjson-tipofile="arriba" txtjson-link="" txtjson-typelink="">
                <i class=" btn btn-xs fa fa-arrows dd-handle " title="<?php echo JrTexto::_('Move'); ?>" style="cursor: move; display: inline-block; margin-top: 0px;"></i> <span class="titulo"></span>
                <span class="accionesdecurso btn-group" style="position: absolute; right: 0px;">
                    <i class="btn btn-xs fa fa-plus-circle btnaddoption" title="<?php echo JrTexto::_('Add Pestania'); ?>"></i>
                    <i class="btn btn-xs fa fa-magic btnaddhabilidades" title="<?php echo JrTexto::_('Add habilidades'); ?>"></i>

                    <i class="btn btn-xs fa fa-archive btnaddrubricas" title="<?php echo JrTexto::_('Add Rubrica'); ?>"></i>
                    <div class="btn-group btn-group-sm" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-xs btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $txticon; ?>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="width: 40px !important; min-width: 40px !important;     max-height: 220px; overflow-y: scroll;">
                            <a class="dropdown-item" data-icon="#"> <i class="mostraricon">#</i> </a>
                            <a class="dropdown-item" data-icon="fa-list"> <i class="mostraricon fa fa-list"></i> </a>
                            <a class="dropdown-item" data-icon="fa-folder-o"> <i class="mostraricon fa fa-folder-o"></i> </a>
                            <a class="dropdown-item" data-icon="fa-file-text"> <i class="mostraricon fa fa-file-text"></i> </a>
                            <a class="dropdown-item" data-icon="fa-bar-chart"> <i class="mostraricon fa fa-bar-chart"></i> </a>
                            <a class="dropdown-item" data-icon="fa-check-square-o"> <i class="mostraricon fa fa-check-square-o"></i> </a>
                            <a class="dropdown-item" data-icon="fa-envelope"> <i class="mostraricon fa fa-envelope"></i> </a>
                            <a class="dropdown-item" data-icon="fa-cloud-download"> <i class="mostraricon fa fa-cloud-download"></i> </a>
                            <a class="dropdown-item" data-icon="fa-film"> <i class="mostraricon fa fa-film"></i> </a>
                            <a class="dropdown-item" data-icon="fa-keyboard-o"> <i class="mostraricon fa fa-keyboard-o "></i> </a>
                            <a class="dropdown-item" data-icon="fa-laptop"> <i class="mostraricon fa fa-laptop "></i> </a>
                            <a class="dropdown-item" data-icon="fa-book"> <i class="mostraricon fa fa-book "></i> </a>
                            <a class="dropdown-item" data-icon="fa-th-large"> <i class="mostraricon fa fa-th-large "></i> </a>
                            <a class="dropdown-item" data-icon="fa-th"> <i class="mostraricon fa fa-th "></i> </a>
                            <a class="dropdown-item" data-icon="fa-comments-o"> <i class="mostraricon fa fa-comments-o "></i> </a>
                            <a class="dropdown-item" data-icon="fa-clipboard"> <i class="mostraricon fa fa-clipboard "></i> </a>
                        </div>
                    </div>
                    <i class="btn btn-xs fa fa-question btnmostrarpregunta" title="<?php echo JrTexto::_('Show questions'); ?>"></i>
                    <i class="btn btn-xs fa fa-file btnaddcontent" title="<?php echo JrTexto::_('Add content'); ?>"></i>
                    <i class="btn btn-xs fa fa-tasks btnestarea" title="<?php echo JrTexto::_('Es una tarea'); ?>"></i>
                    <i class="btn btn-xs fa fa-folder btnesproyecto" title="<?php echo JrTexto::_('Es un Proyecto'); ?>"></i>
                    <i class="btn btn-xs fa fa-dollar btnrecursopagado" title="<?php echo JrTexto::_('Es un Recurso Pagado'); ?>" <?php echo ($tipocurso == '1') ? '' : ' style="display:none;"'; ?>></i>
                    <i class="btn btn-xs fa btnverenmenu fa-eye" title="<?php echo JrTexto::_('ver/ocultar menu'); ?>"></i>
                    <i class="btn btn-xs fa fa-pencil btneditmenu" title="<?php echo JrTexto::_('Edit'); ?>"></i>
                    <i class="btn btn-xs fa fa-trash btnremovemenu" title="<?php echo JrTexto::_('Remove'); ?>"></i>
                </span>
            </li>
        <?php } ?>
    </ol>
<?php
}
// $html = ob_get_clean();
// echo $html;
?>