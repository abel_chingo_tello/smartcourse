<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;
?>
<div class="card card-secondary">
	<div class="card-header">
		<div class="row justify-content-around">
			<div class="col-md-4">
				<div class="form-group">
					<label>Carrera</label>
					<select id="cmbcategorias" class="form-control">
						<?php $hay=false;
							if(!empty($this->categorias)): 
								foreach($this->categorias as $v):
									if(empty($v["estado"])) continue;
									if(!empty($v["tienecursos"])){
							 ?><option value="<?php echo $v['idcategoria'] ?>" <?php echo $hay==false?'selected="selected"':'';?> ><?php echo strtoupper($v['nombre']) ?></option>
							<?php $hay=true; 
								} 
								endforeach; 
							endif; 
						if(!empty($this->haycursoslibres)||empty($hay)){ ?><option value="0" class="cursoslibres"><?php echo JrTexto::_('Free Courses'); ?></option><?php }?>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><?=JrTexto::_("Study Groups")?></label>
					<select id="cmbgrupos" class="form-control">
					</select>
				</div>
			</div>
		</div>
	</div>
    <!-- END CARD -->
    <div class="card-body">
    	<div id="lscursos" class="row">
    	</div>
	</div>
</div>
<script type="text/javascript">
	const CATEGORIAS = <?php echo !empty($this->categorias) ? json_encode($this->categorias) : '[]'; ?>;
	const CURSOS = <?php echo !empty($this->allcursos) ? json_encode($this->allcursos) : '[]'; ?>;
	const oUSER = <?php echo !empty($this->curusuario)? json_encode($this->curusuario,true):'[]';?>;	
	
	var catpadres=[];
	$.each(CATEGORIAS,function(i,v){
		if(catpadres[i]==undefined) catpadres[i]=[];
		catpadres[i].push(parseInt(i));
		if(v.hijos!=undefined)
		$.each(v.hijos,function(ii,vv){
			catpadres[i].push(parseInt(ii));
		})
	})
	const cursostutoriales=[207,678];

	$(document).ready(function(){
	    $('#lscursos').on('click','.item-curso',function(ev){
	        var id=$(this).attr('idcurso');
	        var idcc=id+'&icc='+$(this).attr('idcomplementario')||0;
	        idcc=idcc+'&tipo='+$(this).attr('tipo')||1;
	        let idgrupoauladetalle=$(this).attr('idgrupoauladetalle')||'';
	        if(idgrupoauladetalle!=undefined && idgrupoauladetalle!='') idcc+='&idgrupoauladetalle='+idgrupoauladetalle;
	        let idmatricula=$(this).attr('idmatricula')||'';
	        if(idmatricula!=undefined && idmatricula!='') idcc+='&idmatricula='+idmatricula;
	        window.open(_sysUrlBase_+'smart/cursos/ver/?idcurso='+idcc, '_blank');
	    });

	    $('body').on('change','#cmbgrupos',function(){
	    	var idgrupoaula = $(this).val();
	    	var filtro = [];
	    	var drawCursos = [];
	    	let cursosSeleccionados=[];
	    	if(CURSOS!=undefined)
			$.each(CURSOS,function(i,v){ // grupos a la categoria principal	
				if(parseInt(idgrupoaula)==parseInt(v.idgrupoaula)  || cursostutoriales.includes(parseInt(v.idcurso)) ){					
					cursosSeleccionados.push(v);
				}
			})
	    	drawListCourse(cursosSeleccionados,false);
	    });
	    $('body').on('change','#cmbcategorias',function(){
	    	var idcategoria = $(this).val();
	    	let categorias=catpadres[idcategoria];

    		let gruposaula=[];
    		let grupostmp=[];		
			if(CURSOS!=undefined)
			$.each(CURSOS,function(i,v){ // grupos a la categoria principal
				if(idcategoria!=0 && idcategoria!=''){
					if(v.idcategoria!=undefined && grupostmp.includes(v.idgrupoaula)==false)
						if(categorias.includes(parseInt(v.idcategoria)) && v.idgrupoaula!=-1){
							let idgrupoaula=v.idgrupoaula;
							grupostmp.push(v.idgrupoaula);
							gruposaula.push({idgrupoaula:v.idgrupoaula , strgrupoaula : v.strgrupoaula});
						}
				}else{
					if(grupostmp.includes(v.idgrupoaula)==false){
						let idgrupoaula=v.idgrupoaula;
						grupostmp.push(v.idgrupoaula);
						gruposaula.push({idgrupoaula:v.idgrupoaula , strgrupoaula : v.strgrupoaula});
					}
				}
			})
			let hay=false;
			$('#cmbgrupos').html('');
			$.each(gruposaula,function(i,v){			
				$('#cmbgrupos').append('<option value="'+v.idgrupoaula+'">'+v.strgrupoaula+'</option>');
				hay=true;
			})		
			if(hay==false){ $('#cmbgrupos').append('<option value="0"><?=JrTexto::_("No groups")?></option>');}
			$('#cmbgrupos').trigger('change');	    	
	    });
	    $('#cmbcategorias').trigger('change');
	});
	//simular sttripos de php
	function strripos (haystack, needle, offset) {
		//  discuss at: https://locutus.io/php/strripos/
		// original by: Kevin van Zonneveld (https://kvz.io)
		// bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
		// bugfixed by: Brett Zamir (https://brett-zamir.me)
		//    input by: saulius
		//   example 1: strripos('Kevin van Zonneveld', 'E')
		//   returns 1: 16

		haystack = (haystack + '').toLowerCase()
		needle = (needle + '').toLowerCase()

		var i = -1
		if (offset) {
			i = (haystack + '').slice(offset).lastIndexOf(needle) // strrpos' offset indicates starting point of range till end,
			// while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
			if (i !== -1) {
			  i += offset
			}
		} else {
			i = (haystack + '').lastIndexOf(needle)
		}
		return i >= 0 ? i : false
	}

	function drawListCourse(courses, showpie){
		var contenedor = $('#lscursos');
		contenedor.html('');
    	if(Object.keys(courses).length > 0){
    		courses.forEach((el)=>{
    			var posimg = false;
    			_img = el.imagen;
				posimg = strripos(_img,'static/');
				if(posimg === false) {
					_img = String.prototype.concat(_sysUrlBase_,'static/media/cursos/nofoto.jpg');
				}else {
					_img = String.prototype.concat(_sysUrlBase_,_img.substring(posimg));
				}
				var idcc = "";
				if(el.idcomplementario != undefined){
					idcc = el.idcomplementario;
				}
				var idmat = "";
				if(el.idmatricula != undefined){
					idmat = el.idmatricula;
				}
				var edit = '';

				let html=`<div class="card card-outline card-primary visorCursos" id="card${el.idcurso}">
				          <div class="card-header">
				            <h4 class="card-title">${el.strcurso||el.nombre}</h4>				           
				          </div>
				          <div class="card-body" style="display: block;">
							<div class="image view view-first item-curso" idcurso="${el.idcurso}" idmatricula="${idmat}" idcomplementario="${idcc}" tipo="${el.tipo}" idcategoria="${el.idcategoria}" idgrupoauladetalle="${el.idgrupoauladetalle||''}" idgrupoaula="${el.idgrupoaula}">
								<img class="img-curso imgicon" src="${_img}" />
								${edit}
			          		</div>
						</div>`;
					if(showpie==true){
						html+=`<div class="card-footer">
							<label for="">${el.strgrupoaula}</label>
						</div>`
					}				
    			if(cursostutoriales.includes(parseInt(el.idcurso))){
    				if($('#card'+el.idcurso).length==0)
    			 	contenedor.prepend(`<div class="col-lg-3 col-md-4 col-sm-4">${html}</div>`);
    			}
    			else contenedor.append(	`<div class="col-lg-3 col-md-4 col-sm-4">${html}</div>`);    			
    		});
    		$('[data-toggle="popover"]').popover({
			    container: 'body',
			    trigger:'hover',
			    placement:'bottom'
			})
    	} else {
			let notienecursos='<?php echo ucfirst(JrTexto::_('You have no courses currently assigned')); ?>.';
			var sincursos = `
					<div class="row">
						<div class="col-md-12">
							<h3 class="no-mensajes">${notienecursos}...</h3>
						</div>
                    </div>
					`;
			contenedor.html(sincursos);
		}
	}
</script>