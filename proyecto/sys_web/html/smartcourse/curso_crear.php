<?php
$idgui = uniqid();
global $txticon;
$imgcursodefecto = $this->documento->getUrlBase() . 'static/media/nofoto.jpg';
$curso = !empty($this->curso) ? $this->curso : '';
$imagen = (!empty($curso["imagen"]) ? $curso["imagen"] : $imgcursodefecto);
$ipos = strripos($imagen, 'static/');
if ($ipos == false && $ipos != 0) {
    $ipos2 = strripos($imagen, '.');
    if ($ipos2 == false && $ipos2 != 0) $imagen = $imgcursodefecto;
    else {
        $ipos1 = strripos($imagen, '/');
        if ($ipos1 == false && $ipos1 != 0) $imagen = $this->documento->getUrlStatic() . '/media/cursos/' . $imagen;
        else $imagen = $this->documento->getUrlStatic() . '/media/cursos/' . substr($imagen, $ipos1);
    }
} else $imagen = $this->documento->getUrlBase() . substr($imagen, $ipos);
$txtjson = !empty($curso["txtjson"]) ? json_encode($curso["txtjson"], true) : '';
$txticon = '#';
?>
<style type="text/css">
    #hora-limite-tapr:disabled,
    #fecha-limite-tapr:disabled {
        background-color: #dfdfdf !important;
    }

    #fecha-limite-frm {
        font-size: 14px;
        font-weight: 600;
    }

    .wizard_horizontal ul.wizard_steps {
        font-weight: bold !important;
    }

    @media (max-width: 600px) {
        .accionesdecurso {
            position: relative !important;
        }

        span.step_descr {
            display: none;
        }
    }

    .li-hover {
        background-color: #eee;
    }

    div.stepContainer {
        height: auto !important;
        min-height: 59px;
    }

    div.stepContainer .content {
        min-height: 300px;
    }

    li.tienehijos>span.accionesdecurso>.btnaddhabilidades,
    li.tienehijos>span.accionesdecurso>.btnmostrarpregunta,
    li.tienehijos>span.accionesdecurso>.btnaddvideosyoutube,
    li.tienehijos>span.accionesdecurso>.btnaddcontent,
    li.tienehijos>span.accionesdecurso>.btnestarea,
    li.tienehijos>span.accionesdecurso>.btnesproyecto,
    li.tienehijos>span.accionesdecurso>.btnrecursopagado,
    li.tienehijos>span.accionesdecurso>.btnconteacher,
    li>span>.btnaddhabilidades,
    li>span>.btnaddrubricas {
        display: none;
    }

    /* mine */
    li.concalificacion>span>.btnaddrubricas {
        display: block;
    }

    /* end mine */
    li.concalificacion>span>.btnaddhabilidades,
    li.conrubrica>span>.btnaddrubricas {
        display: block;
    }

    #tablenuevapuntuacion input {
        width: 85px;
        text-transform: uppercase;
        text-align: center;
        height: 25px;
    }

    #tablenuevapuntuacion select {
        height: 24px;
        padding: 0px;
        font-size: 13px;
        width: 100%;
    }

    #idForo {
        height: 40px;
        padding: 5px;
        font-size: 16px;
        width: 100%;
        margin: 10px 0px 50px 0px;
    }

    #tablenuevapuntuacion .select-ctrl-wrapper::after {
        right: 0px;
        font-size: 23px;
    }

    #tablenuevapuntuacion td {
        vertical-align: middle;
        padding: 0.5ex;
        text-align: center;
    }

    .formulagenerada {
        border: 1px solid #ccc;
        padding: 1em;
        border-radius: 4ex;
    }

    .dd-handlemove{
        
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlStatic(); ?>/libs/jquery-nestable/jquery.nestable.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/green/green.css">
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo JrTexto::_('Edit Course'); ?> <small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="_crearcurso_">
                <!-- Smart Wizard -->
                <div id="wizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps" style="padding: 0px;">
                        <li>
                            <a href="#step-1">
                                <span class="step_no">1</span>
                                <span class="step_descr"><?php echo JrTexto::_('Course information'); ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">
                                <span class="step_no">2</span>
                                <span class="step_descr"><?php echo JrTexto::_('Cover design'); ?></span>
                            </a>
                        </li>
                        <!--li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr"><?php echo JrTexto::_('Course grade'); ?></span>
                          </a>
                        </li-->
                        <li>
                            <a href="#step-3">
                                <span class="step_no">3</span>
                                <span class="step_descr"><?php echo JrTexto::_('Course content'); ?></span>
                            </a>
                        </li>
                    </ul>
                    <div id="step-1">
                        <form id="frmdatosdelcurso" method="post" target="" enctype="multipart/form-data">
                            <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idcurso; ?>">
                            <input type="hidden" name="tipo" id="tipo" value="<?php echo $this->tipocurso; ?>">
                            <input type="hidden" name="sks" id="sks" value="1">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12 form-group">
                                        <label><?php echo JrTexto::_('Nombre del curso'); ?></label>
                                        <input type="text" autofocus name="nombre" required="required" class="guardarvalorcurso form-control" placeholder="<?php echo ucfirst(JrTexto::_("Curso 01")) ?>" value="<?php echo @$curso["nombre"] ?>">
                                        <!-- <input type="text" autofocus name="nombre" required="required" class="guardarvalorcurso form-control" placeholder="<?php echo ucfirst(JrTexto::_("Curso 01")) ?>" value="<?php echo @$curso["nombre"] ?>"<?php echo $this->validar ? ' readonly=""' : ''; ?>> -->
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label><?php echo JrTexto::_('Autor'); ?></label>
                                        <input type="text" name="autor" class="guardarvalorcurso form-control _autor_" placeholder="<?php echo  ucfirst(JrTexto::_("Apellidos y Nombres del autor ")) ?>" value="<?php echo $this->validar ? $this->curusuario["nombre_full"] : @$curso["autor"]; ?>" <?php echo $this->validar ? ' readonly=""' : ''; ?>>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label><?php echo JrTexto::_('Reseña'); ?></label>
                                        <textarea name="descripcion" class="guardarvalorcurso form-control" rows="5" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso")) ?>" value="<?php echo @$curso["descripcion"] ?>" style="resize: none;"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12 text-center">
                                        <label><?php echo JrTexto::_("Imagen de curso") ?> </label>
                                        <div style="position: relative;" class="guardarvalorcurso text-center" id="contenedorimagen">
                                            <div class="toolbarmouse text-center"><span class="btn borramendia" name='imagen' value=''><i class="fa fa-trash"></i></span></div>
                                            <div id="sysfilelogo">
                                                <img src="<?php echo $imagen; ?>" class="__subirfile img-thumbnail img-fluid center-block" typefile="imagen" id="imagen" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-group text-center">
                                        <br><label><?php echo JrTexto::_('Color del curso'); ?> </label> <br>
                                        <input type="" name="color" value="<?php echo !empty($curso["color"]) ? $curso["color"] : 'rgb(0,0,0,0)' ?>" class="guardarvalorcurso vercolor form-control">
                                        <div class="form-group select-cont soft-rw">
                                            <label for="">Idioma</label>
                                            <select class="form-control" name="" id="selectIdioma">
                                                <option value="ES">Español</option>
                                                <option value="EN">Inglés</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 my-font-all soft-rw">
                                    <div class="form-check bloqueo-contenido">
                                        <label class="form-check-label">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"> </i> Avance secuencial
                                        </label>
                                        <input type="checkbox" name="" id="check-bloqueo-contenido" value="checked">
                                    </div>
                                </div>
                            </div>
                            <!--div class="card-footer text-center">
                                <button type="button" class="buttonPrevious btn btn-warning"><i class=" fa fa-undo"></i> Regresar al listado</button>
                                <button type="submit" class="buttonNext btn btn-primary" data-pclase="tab-pane" data-showp="#pasocate" ><i class=" fa fa-save"></i> Guardar y continuar <i class=" fa fa-arrow-right"></i></button>
                            </div-->
                        </form>
                    </div>
                    <div id="step-2">
                        <div class="row btnmanagermenu">
                            <div class="col-md-6 col-sm-12">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h3>Información de Portada</h3>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label style=""><?php echo JrTexto::_('Titulo'); ?></label>
                                        <input type="text" autofocus name="titulo" required="required" class="_infoportada form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Titulo de portada")) ?>">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label style=""><?php echo JrTexto::_('Resumen'); ?></label>
                                        <textarea name="descripcion" class="_infoportada form-control" rows="5" placeholder="<?php echo  ucfirst(JrTexto::_("descripción corta para la portada")) ?>" style="resize: none;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h3>Estilo de portada</h3>
                                    </div>
                                    <div class="col-md-12 text-center form-group">
                                        <label><?php echo JrTexto::_("Imagen de fondo") ?> </label>
                                        <div style="position: relative;" class="_infoportada text-center" id="contenedorimagen">
                                            <input type="hidden" value="" name="imagenfondo" id="imagenfondo">
                                            <div class="toolbarmouse text-center"><span class="btn btnremoveimage borramendia" name="imagenfondo"><i class="fa fa-trash"></i></span></div>
                                            <div id="sysfilelogo">
                                                <img src="<?php echo $imgcursodefecto; ?>" name="imagenfondo" class=" __subirfile img-fluid center-block" data-type="imagen" nomfile="portadafondo" id="imagenfondo" style="min-height:100px; max-width: 200px; max-height: 150px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <label><?php echo JrTexto::_('Color de fondo'); ?> </label> <br>
                                        <input type="" name="color" id="colorfondo" value="rgb(0,0,0,0)" class="_infoportada vercolor form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-3">
                        <div id="_contentcurso_">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="cf nestable-lists">
                                        <div class="dd col-md-12" id="nestable" style="width:100%; max-width: 100% !important;">
                                            <?php
                                            include('liDrawer.php');
                                            drawHtml($this->temas, $this->idcomplementario, $this->tipocurso);
                                            ?>
                                        </div>
                                        <div class="col-md-12 text-center soft-rw">
                                            <br>
                                            <button id="open-md-submenu" style="color:white;" class="btn btn-info btn-sm">
                                                <i class="fa fa-plus"></i>
                                                Añadir contenedor
                                            </button>
                                            <a href="#" idmodal="" class="btnopenmodal btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Añadir menu'); ?> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="showpEnlacesInteres" style="display: none;">
                                <div class="row ">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Imagen</th>
                                                    <th>Titulo</th>
                                                    <th>Link</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="trvideoclone" style="display: none;">
                                                    <td></td>
                                                    <td>
                                                        <div class="imagen-enlace">
                                                            <a href="#" class="btn btn-warning btnsubirvideo">
                                                                <i class="fa fa-image"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" name="titulo" class="form-control" value="Sin Título"></td>
                                                    <td><input type="text" name="link" class="form-control" value="Link"></td>
                                                    <td><i class="iborrar fa fa-trash" style="cursor: pointer"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button class="btnaddvideo btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                        <button class="btnsavejson btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                                        <button class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</button>
                                    </div>
                                </div>
                            </div>
                            <div id="showpBiblioteca" style="display: none;">
                                <div class="row ">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Portada</th>
                                                    <th>Titulo</th>
                                                    <th>Contenido</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="trvideoclone" style="display: none;">
                                                    <td></td>
                                                    <td>
                                                        <div class="imagen-enlace">
                                                            <a href="#" class="btn btn-warning btnsubirvideo">
                                                                <i class="fa fa-image"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" name="titulo" class="form-control" value="Sin Título"></td>
                                                    <td style="position: relative;">
                                                        <div><a href="#" class="btn btn-warning btnsubirvideo2"><i class="fa fa-file"></i> </a></div>
                                                    </td>
                                                    <td><i class="iborrar fa fa-trash" style="cursor: pointer"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button class="btnaddvideo btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                        <button class="btnsavejson btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                                        <button class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</button>
                                    </div>
                                </div>
                            </div>
                            <div id="_infoTema_" style="display: none;">
                                <form id="frmdatosdeltema" method="post" target="" enctype="multipart/form-data">
                                    <input type="hidden" name="imagen" id="imagen" value="">
                                    <input type="hidden" name="accimagen" id="accimagen" value="sg">
                                    <input type="hidden" name="oldimagen" id="oldimagen" value="0">
                                    <input type="hidden" name="accmenu" id="accmenu" value="0">
                                    <div class="card text-center shadow">
                                        <div class="card-body">
                                            <!--h5 class="card-title"><?php //echo JrTexto::_('Datos generales del curso'); 
                                                                        ?></h5-->
                                            <div class="row">
                                                <div name="to-center" class="col-12 col-sm-6 col-md-6">
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <label name="to-rename" style=""><?php echo JrTexto::_('Nombre del Tema/pestaña'); ?></label>
                                                            <input type="text" autofocus name="nombre" required="required" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Tema 001")) ?>">
                                                        </div>
                                                        <div name="to-hide" class="col-12 form-group">
                                                            <label style=""><?php echo JrTexto::_('Descripción'); ?></label>
                                                            <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del tema")) ?>" rows="5" style="resize: none;"></textarea>
                                                        </div>
                                                        <div name="to-hide" class="col-6 form-group text-left ">
                                                            <label style=""><?php echo JrTexto::_('Color'); ?> </label>
                                                            <input type="" name="color" autocomplete="off" value="transparent" class="form-control">
                                                        </div>

                                                    </div>
                                                </div>
                                                <div name="to-hide" class="col-12 col-sm-6 col-md-6">
                                                    <label><?php echo JrTexto::_("imagen tema/pestaña") ?> (opcional) </label>
                                                    <div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
                                                        <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                                                        <div id="sysfilelogo">
                                                            <img src="<?php echo $imgcursodefecto; ?>" class="__subirfile img-fluid center-block" id="imagen" style="max-width: 200px; max-height: 150px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-muted">
                                            <button type="button" class="cerrarmodal btncancelar btn btn-warning"><i class=" fa fa-close"></i> Cancelar</button>
                                            <button type="submit" class="btn btn-primary btnguardartema"><i class=" fa fa-save"></i> Guardar Tema</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="showEnlazarYoutube" style="display:none">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12 text-center ">
                                            <label>Enlazar Video</label>
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Enlace (link) <font color="red">(*)</font>
                                                <input type="text" id="txtenlacevideo" name="txtenlacevideo" maxlength="200" class="form-control input-sm" placeholder="Ingrese Enlace (Link)" required="">
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 1ex;">
                                        <div class="col-md-12 text-center text-muted">
                                            <button type="button" class="btnsaveenlace btn btn-success"><i class="fa fa-save"></i> Guardar </button>
                                            <button type="button" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> Cerrar </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--START MODAL VIDEO VIMEO-->
                            <div id="showEnlazarVimeo" style="display:none">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12 text-center ">
                                            <label>Enlazar Video Vimeo</label>
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <p class="alert alert-info">El enlace requerido debe contener el siguiente formato: https://vimeo.com/286898202</p>
                                            <p>Enlace (link) <font color="red">(*)</font>
                                                <input type="text" id="txtenlacevideovimeo" name="txtenlacevideovimeo" maxlength="200" class="form-control input-sm" placeholder="Ingrese Enlace (Link)" required="">
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 1ex;">
                                        <div class="col-md-12 text-center text-muted">
                                            <button type="button" class="btnsaveenlace-vimeo btn btn-success"><i class="fa fa-save"></i> Guardar </button>
                                            <button type="button" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> Cerrar </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END MODAL VIDEO VIMEO-->
                            <div id="showpaddcontenido" style="display:none">
                                <div class="col-md-12">
                                    <div class="row" id="addcontentclone">
                                        <div class="col-md-12 text-center ">
                                            <label>Seleccione plantilla de contenido</label>
                                            <hr>
                                        </div>
                                        <!--div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="texto" title="Texto" data-content="Agregar texto" data-acc="texto">        
                                            <i class="fa btn btn-block btn-primary fa-text-width fa-2x "> <br><span>Texto</span> </i>
                                        </div-->
                                        <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="imagen" title="Imagen" data-content="Agregar una imagen" data-acc="imagen">
                                            <i class="fa btn btn-block btn-primary fa-file-image-o fa-2x "> <br><span>Imagen</span> </i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="audio" title="Audio" data-content="Agregar un audio" data-acc="audio">
                                            <i class="fa btn btn-block btn-primary fa-file-audio-o fa-2x "> <br><span>Audio</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="video" title="Video" data-content="agregar un video" data-acc="video">
                                            <i class="fa btn btn-block btn-primary  fa-file-movie-o fa-2x "> <br><span>Video</span> </i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="pdf" title="Documento" data-content="Agregar un archivo pdf" data-acc="pdf">
                                            <i class="fa btn btn-block btn-primary  fa-file-o fa-2x "> <br><span>Documento</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="html" title="HTML" data-content="Agregar contenido HTML - seleccione un zip si incluye mas contenido" data-acc="html">
                                            <i class="fa btn btn-block btn-primary fa-file-code-o fa-2x "> <br><span>Html</span></i>
                                        </div>
                                        <!--div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="flash" title="Flash" data-content="Agregar SWF - archivo Flash" data-acc="flash">
                                        <i class="fa btn btn-block btn-primary fa-file-archive-o fa-2x "> <br><span>flash</span></i>
                                        </div-->
                                        <!--div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticlock" title="SmartTic Look" data-content="Look" data-acc="smarticlock" <?php //echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                                            <i class="fa btn btn-block btn-primary fa-eye fa-2x "><br> <span> Mirar </span> </i>
                                        </div-->
                                        <!--div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticlookPractice" title="SmartTic Practice" data-content="Practice" data-acc="smarticlookPractice" <?php //echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                                            <i class="fa btn btn-block btn-primary fa-cogs fa-2x "><br> <span> Practicar</span></i>
                                        </div-->
                                        <!--div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticDobyyourselft" title="SmartTic Do by yourselft" data-content="Do it by yourself" data-acc="smarticDobyyourselft" <?php //echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                                            <i class="fa btn btn-block btn-primary fa-cogs fa-2x "><br> <span> hazlo tu mismo</span></i>
                                        </div-->
                                        <!--div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="game" title="Juegos" data-content="Agregar juegos interactivos" data-acc="game" <?php //echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                                            <i class="fa btn btn-block btn-primary fa-gamepad fa-2x "><br><span>Juegos</span> </i>
                                        </div-->
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="enlaceyoutube" title="Enlace Youtube" data-content="" data-acc="enlaceyoutube">
                                            <i class="fa btn btn-block btn-primary fa-youtube-play fa-2x "><br> <span> Enlace Youtube</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="enlacevimeo" title="Enlace Vimeo" data-content="" data-acc="enlaceyoutube">
                                            <i class="fa btn btn-block btn-primary fa-vimeo fa-2x "><br> <span> Enlace Vimeo</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="smartquiz" title="SmartQuiz" data-content="Agregar Examenes" data-acc="smartquiz">
                                            <i class="fa btn btn-block btn-primary fa-tasks fa-2x "> <br><span>Examenes</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="menuvaloracion" title="Mostrar valoracion de curso" data-content="Agregar Examenes" data-acc="smartquiz">
                                            <i class="fa btn btn-block btn-primary fa-star fa-2x "> <br><span>Mostrar valoracion</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="enlacesinteres" title="Mostrar Enlaces de Interés" data-content="Agregar Enlaces de Interés" data-acc="enlacesinteres">
                                            <i class="fa btn btn-block btn-primary fa-external-link fa-2x "> <br><span>Enlaces de Interés</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="smartlibrery" title="Mostrar Biblioteca" data-content="Agregar Enlaces de Interés" data-acc="smartlibrery">
                                            <i class="fa btn btn-block btn-primary fa-external-link fa-2x "> <br><span>Biblioteca</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="tabladeaudios" title="Tabla de Audios" data-content="" data-acc="tabladeaudios">
                                            <i class="fa btn btn-block btn-primary fa-headphones fa-2x "><br> <span> Tabla de Audios</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="multioptions" title="Tabla de multiple Contenido" data-content="" data-acc="multioptions">
                                            <i class="fa btn btn-block btn-primary fa-film fa-2x "><br> <span> Multiple Contenido</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smartenglish" title="Smartenglish Sesion" data-content="smart English" data-acc="smartenglish" <?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>>
                                            <i class="fa btn btn-block btn-primary fa-globe fa-2x "><br> <span> SmartEnglish</span></i>
                                        </div>
                                        
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="zoom" title="Zoom" data-content="zoom" data-acc="zoom">
                                            <i class="fa btn btn-block btn-primary fa-video-camera fa-2x "><br> <span> Zoom</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="videoconferencia" title="Enlace de video conferencias" data-content="videoconferencia" data-acc="videoconferencia">
                                            <i class="fa btn btn-block btn-primary fa-video-camera fa-2x "><br> <span> Video conferencias</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="descargacontrolada" title="Enlace de video conferencias" data-content="descargacontrolada" data-acc="descargacontrolada">
                                            <i class="fa btn btn-block btn-primary fa-download fa-2x "><br> <span> Descargas</span></i>
                                        </div>

                                        <div class="col-md-12"><br><h4 style="font-size: 11px;">Recursos colaborativos<hr></h4> </div>   
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="foro"  title="Solicita a tus estudiantes crear o comentar un foro en grupo  o individualemnete" data-content="Foro" data-acc="foro">
                                            <i class="fa btn btn-block btn-primary fa-comments fa-2x "><br> <span> Foros</span></i>
                                        </div>
                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="wiki" title="Solicita a tus estudiantes crear una Wiki en grupo o individualmente" data-content="Wiki" data-acc="Wiki">
                                            <i class="fa btn btn-block btn-primary fa-american-sign-language-interpreting fa-2x "><br> <span> Wiki</span></i>
                                        </div>

                                        <!--div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="webquest" title="Solicita a tus estudiantes responder un cuestionario en grupo o individualmente" data-content="WebQuest" data-acc="WebQuest">
                                            <i class="fa btn btn-block btn-primary fa-american-sign-language-interpreting fa-2x "><br> <span> WebQuest</span></i>
                                        </div>                                        

                                        <div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="enlacecolaborativo" title="Solicita a tus estudiantes un archivo creado en grupo o individualmente" data-content="Enlace colaborativo" data-acc="enlacecolaborativo">
                                            <i class="fa btn btn-block btn-primary fa-american-sign-language-interpreting fa-2x "><br> <span> Archivo colaborativo</span></i>
                                        </div-->                                        
                                    </div>
                                    <div class="row" style="margin-top: 1ex;">                                       
                                        <div class="col-md-12 text-center text-muted"><br>
                                            <hr>
                                            <button type="button" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> Cerrar </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="showpaddhabilidad" style="display:none">
                                <div class="col-md-12">
                                    <div class="row" id="addclonehabilidad">
                                        <div class="col-md-12 text-center ">
                                            <br><label>Seleccione criterios</label>
                                            <hr>

                                        </div>

                                        <div class="form-group">
                                            <label class="control-label"><?php echo JrTexto::_('Criterios'); ?> <span class="required"> * </span></label>
                                            <div class="">
                                                <ul class="to_do" id="listado_criterios">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 1ex;">
                                        <div class="col-md-12 text-center text-muted">
                                            <button type="button" class="cerrarmodal btn btn-warning"><i class="fa fa-arrow-left"></i> Cerrar </button>
                                            <a href="#" class="btnsavejson_aux btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="showpaddmultivideos" style="display: none;">
                                <div class="row ">
                                    <div class="col-md-12 table-responsive">
                                        <div class="col-md-12">
                                            <caption><?php echo JrTexto::_('Mostrar como') ?></caption>
                                            <select id="tipomenu" name="tipomenu" class="form-control">
                                                <option value="slider"><?php echo JrTexto::_('slider'); ?></option>
                                                <option value="menu"><?php echo JrTexto::_('Menu'); ?></option>
                                            </select>
                                        </div>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th><?php echo JrTexto::_('Name'); ?></th>
                                                    <th><?php echo JrTexto::_('Video')."/".JrTexto::_('File'); ?></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="trvideoclone" style="display: none;">
                                                    <td></td>
                                                    <td><input type="" name="nom" class="form-control" value="Video"></td>
                                                    <td style="position: relative;">
                                                        <div>
                                                            <a href="#" class="btn btn-warning btnsubirvideo" title="Subir archivo"><i class="fa fa-file"></i> </a>
                                                            <a href="#" class="btn btn-info btnenlacevideo-vimeo" title="Enlace Vimeo"><i class="fa fa-vimeo"></i> </a>
                                                        </div>
                                                    </td>
                                                    <td> <i class="imove fa fa-arrow-up" adonde="up" style="cursor: pointer"></i> <i class="imove fa fa-arrow-down" adonde="down" style="cursor: pointer"></i> <i class="iborrar fa fa-trash" style="cursor: pointer"></i> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <a href="#" class="btnaddvideo btn btn-warning"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></a>
                                        <a href="#" class="btnsavejson btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                                        <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                                    </div>
                                </div>
                            </div>
                            <div id="showaddrubricas" style="display: none;">
                                <div id="fix-md" class="row ">
                                    <div class="col-md-12 table-responsive my-font-all">
                                        <!-- working -->
                                        <link rel="stylesheet" href="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/mine/css/soft-rw-theme-paris.css?as">
                                        <link rel="stylesheet" href="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/mine/css/mine.css">
                                        <link rel="stylesheet" href="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/rubrica/mine-rubrica.css">
                                        <script src="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/mine/js/mine.js?"></script>
                                        <script src="<?= $this->documento->getUrlStatic() ?>/libs/othersLibs/rubrica/mine-rubrica.js?"></script>
                                        <input id="input-idinstancia" class="my-hide" type="text" disabled>
                                        <div class="col-sm-12 my-x-center">
                                            <select id="selectRubrica"></select>
                                        </div>
                                        <div id="main-cont" class="row col-sm-12">
                                            <div class="my-loader my-color-green"></div>
                                        </div>
                                        <div class="my-hide row col-sm-12">
                                            <div id="row-cont-rubrica" class="col-sm-12 row-cont-rubrica">
                                                <div class="cont-rubrica my-card my-shadow">
                                                    <h3 onclick="modalRubrica(this)" class="text-center my-full-w">Titulo de rubrica</h3>
                                                    <div id="out-cont" class="out-cont">
                                                        <div id="body-rubrica" class="body-cont-rubrica rubrica-black tema-4">
                                                            <div id="rubrica-code" class="cod-rubrica">
                                                                <div class="code-text">Código</div>
                                                                <div class="code-num"></div>
                                                            </div>
                                                            <div id="cont-btn-crit" onclick="modalCriterio(this)" class="cont-btn-crit">
                                                                <div class="criterio-text">Detalle</div>
                                                                <div class="criterio-num">5%</div>
                                                            </div>
                                                            <div id="cont-btn-nivel" onclick="modalNivel(this)" name="cont-btn-nivel" class="cont-btn-nivel">
                                                                <div class="nivel-text">Excelente</div>
                                                                <div class="nivel-num">20</div>
                                                            </div>
                                                            <div id="cont-btn-indicador" onclick="modalIndicador(this)" class="cont-btn-indicador">
                                                                <div class="indicador-text">Detalle</div>
                                                            </div>
                                                            <button id="btn-new-indicador" onclick="modalIndicador(this)" class="btn-new cont-btn-indic">
                                                                <span class="btn-icon">+</span>
                                                                <span class="btn-text">Descriptor</span>
                                                            </button>
                                                            <button id="btn-new-criterio" onclick="modalCriterio(this)" class="cont-btn-crit btn-new">
                                                                <span class="btn-icon">+</span>
                                                                <span class="btn-text">Criterio</span>
                                                            </button>
                                                            <button id="btn-new-nivel" onclick="modalNivel(this)" class="cont-btn-nivel btn-new">
                                                                <span class="btn-icon">+</span>
                                                                <span class="btn-text">Nivel</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <a href="#" class="btnsaverubrica btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                                        <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                                    </div>
                                </div>
                            </div>
                            <div id="showtabladeaudios" style="display: none;">
                                <div class="row ">
                                    <div class="col-md-12 table-responsive">
                                        <div class="col-md-12 text-center form-group">
                                            <label>Titulo de la tabla:</label>
                                            <input type="text" name="titulo" placeholder="Titulo de la tabla" class="titulo form-control">
                                        </div>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th><?php echo JrTexto::_('Name'); ?></th>
                                                    <th><?php echo JrTexto::_('Audio'); ?></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="trclone" style="display: none;">
                                                    <td></td>
                                                    <td><input type="" name="nom" class="form-control" value="" placeholder="Nombre de Audio"></td>
                                                    <td style="position: relative;">
                                                        <div><a href="#" class="btn btn-warning btnsubir"><i class="fa fa-file-audio-o"></i> </a></div>
                                                    </td>
                                                    <td> <i class="imove fa fa-arrow-up" adonde="up" style="cursor: pointer"></i> <i class="imove fa fa-arrow-down" adonde="down" style="cursor: pointer"></i> <i class="iborrar fa fa-trash" style="cursor: pointer"></i> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <a href="#" class="btnadd btn btn-warning"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></a>
                                        <a href="#" class="btnsavejson btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                                        <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                                    </div>
                                </div>
                            </div>
                            <div id="showvideoconferencia" class="my-hide">
                                <div id="fix-md" class="row soft-rw">
                                    <div class="cont-conferencia col-sm-12 my-x-center my-font-all">
                                        <form id="form-add" class="col-sm-12 col-md-8 my-mg-center" onsubmit="return saveEvent();">
                                            <div class="form-group">
                                                <label for="input-conf-titulo">Tema de la conferencia</label>
                                                <input required type="text" name="" id="input-conf-titulo" class="form-control" placeholder="Tema de la conferencia...">
                                            </div>

                                            <div class="my-hide form-group">
                                                <label for="input-conf-link_mod">Link (moderador)</label>
                                                <input type="text" class="form-control" name="" id="input-conf-link_mod" placeholder="Enlace a la videoconferencia...">
                                            </div>
                                            <div class="form-group">
                                                <label for="input-conf-link_visitor">Link <span class="my-hide">(alumno)</span> </label>
                                                <input required type="text" class="form-control" name="" id="input-conf-link_visitor" placeholder="Enlace a la videoconferencia...">
                                            </div>
                                            <div class="form-group">
                                                <label for="fecha">Fecha y hora de inicio</label>
                                            </div>
                                            <div class="form-group col-sm-12 col-md-6 ">
                                                <input required type="date" name="fecha" id="input-conf-fecha" class="form-control" placeholder="">
                                            </div>
                                            <div class="form-group col-sm-12 col-md-6 ">
                                                <input required type="time" name="fecha" id="input-conf-hora_comienzo" class="form-control" placeholder="">
                                            </div>

                                            <div class="form-group">
                                                <label for="input-conf-detalle">Descripción</label>
                                                <textarea placeholder="Descripción de la conferencia..." required class="form-control" name="input-conf-detalle" id="" rows="5"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <a href="#" class="btnsavejson btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                                        <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                                    </div>
                                </div>
                            </div>
                            <div id="showrecursocolaborativo" class="my-hide">                                
                                <div class="row" id="pnlcrearrecurso1" >
                                    <form id="frmcrearrecurso" method="post" target="" enctype="multipart/form-data">
                                        <input type="hidden" name="idrecurso" id="idrecurso" value="">
                                        <input type="hidden" name="tipo" id="tipo" value="">
                                        <input type="hidden" name="estado" id="estado" value="1">                                    
                                        <div class="col-md-12 col-12 form-group">
                                            <label name="titulo"><?php echo JrTexto::_('Titulo'); ?></label>
                                            <input type="text" autofocus name="titulo" required="required" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Escriba el titulo aquí")) ?>">
                                        </div>
                                        <div class="col-md-12 col-12 form-group">
                                            <label name="titulo"><?php echo JrTexto::_('Descripción e Indicaciones'); ?></label>
                                            <div>
                                            <textarea name="descripcion" class="descripcion form-control" rows="5" style="resize: none;"></textarea>
                                            </div>
                                        </div> 
                                        <div class="clearfix"></div>    
                                        <hr>
                                        <div class="col-md-12 text-center pnlaccion">                                            
                                            <button class="btn btn-primary btnguarrecurso" type="submit"><i class=" fa fa-save"></i> Guardar</button>
                                            <button class="btn btn-warning btncancelar" type="buttom" ><i class=" fa fa-refresh"></i> Cancelar</button>                                            
                                        </div>                               
                                    </form> 
                                </div>
                                <div class="row" id="tablalistadorecurso" style="display: none;"></div>
                                <div class="row" id="asignarestudiantes" style="display: none;">
                                    <div class="col-md-12 form-group">
                                        <label>Seleccione Grupo de Estudio</label>
                                        <div>
                                            <select name="idgrupoauladetalle" id="idgrupoauladetalle" class="form-control"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-12 form-group">
                                        <label>Definir Tema</label>
                                        <select name="definirTema" id="definirTema" class="form-control">
                                            <option value="">El Tema se definió en la descripción o indicaciones</option>                                            
                                            <option value="si">Definir Tema Grupal </option>
                                            <!--option>Definir Tema Individual</option-->
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-xs-12 form-group">
                                        <label>Número de grupos</label>
                                        <input type="number" name="nporgrupo" id="nporgrupo" class="form-control" value="1">
                                    </div>
                                    <hr>
                                    <div class="col-md-12">
                                        <br>
                                        <h4>Grupos y Participantes</h4>                                        
                                        <hr>
                                    </div>
                                    <div class="col-md-12 " id="listadodegrupos">                                        
                                    </div>                                    
                                </div>
                            </div>

                             <div id="showdescargascontroladas" class="my-hide">
                                <div id="fix-md" class="row soft-rw">                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="input-conf-titulo">Titulo</label>
                                            <input required type="text" name="" class="titulo form-control" placeholder="define un titulo">
                                        </div>
                                        <div class="form-group">
                                            <label for="input-conf-detalle">Descripción</label>
                                            <textarea placeholder="Describe tu archivo" class="descripcion form-control"  rows="5"></textarea>
                                        </div>
                                        <div class="form-group archivossubidos row" id="archivossubidos" numerodearchivo="1">
                                        </div>
                                        <div class="btnsubirarchivo text-center" style="border: 1px dashed; padding: 1em; margin: 1em auto; font-size: 1.5em; cursor: pointer; position: relative;">
                                             <h2>Click para subir tu archivo</h2>
                                                <i class="fa fa-upload"></i>
                                        </div>                                                                                    
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <a href="#" class="btnsavejson btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
                                        <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Nueva Rubrica-->
<div class="modal fade my-modal soft-rw my-font-all" id="md-nueva-rubrica" tabindex="-1" role="dialog" aria-labelledby="md-nueva-rubricaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">
                    Rúbrica
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-no-scroll">
                <form id="form-add-rubrica" onsubmit="return saveRubrica();">
                    <input id="input-idrubrica" class="my-hide" type="text" disabled>
                    <div class="form-group">
                        <label for="nombre-rubrica">Nombre</label>
                        <input required type="text" name="nombre-rubrica" id="nombre-rubrica" class="form-control" placeholder="Nombre de la rúbrica...">
                    </div>
                    <div class="form-group my-hide">
                        <select class="form-control" name="" id="selectCompetencia">
                            <option disabled value="">Competencia</option>
                        </select>
                    </div>
                    <div class="form-group my-hide">
                        <button id="btn-save" onclick="addSelectCompetencia()" type="button" class="btn btn-primary my-float-r">+ Competencia</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12  col-md-6">
                        <div id="autor-btn" class="my-hide">
                            <button type="button" onclick="eliminarRubrica()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                            <button id="btn-save" type="submit" form="form-add-rubrica" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div id="default-btn" class="my-hide">
                            <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <div id="new-btn" class="col-sm-12 my-hide">
                        <div class="row my-block">
                            <div class="my-float-r">
                                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="submit" form="form-add-rubrica" class="btn btn-primary">Crear</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Modal Nivel-->
    <div class="modal fade my-modal soft-rw my-font-all" id="md-nivel" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Nivel
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-nivel" onsubmit="return saveNivel();">
                        <input id="input-idnivel" class="my-hide" disabled type="text">
                        <div class="form-group">
                            <label for="cuantitativo">Nivel cualitativo</label>
                            <input required type="text" name="cualitativo" id="cualitativo" class="form-control" placeholder="Calificación cualitativa...">
                        </div>
                        <div class="form-group">
                            <label for="cuantitativo">Nivel cuantitativo</label>
                            <input required type="number" name="cuantitativo" id="cuantitativo" class="form-control" placeholder="Calificación cuantitativa...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarNivel()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button id="btn-save" type="submit" form="form-add-nivel" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" form="form-add-nivel" class="btn btn-primary">Crear</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Criterio-->
    <div class="modal fade my-modal soft-rw my-font-all" id="md-criterio" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Criterio
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-criterio" onsubmit="return saveCriterio();">
                        <input id="input-idcriterio" class="my-hide" disabled type="text">
                        <div class="form-group my-hide">
                            <select class="form-control" name="" id="selectCapacidad">
                                <option disabled value="">Capacidad</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nombre-criterio">Nombre</label>
                            <textarea required type="text" name="nombre-criterio" id="nombre-criterio" class="form-control" placeholder="Nombre del criterio..."></textarea>
                            <!-- <input required type="text" name="nombre-criterio" id="nombre-criterio" class="form-control" placeholder="Nombre del criterio..."> -->
                        </div>
                        <div class="form-group">
                            <label for="peso-criterio">Peso</label>
                            <input type="number" name="peso-criterio" id="peso-criterio" class="form-control" placeholder="Peso del criterio...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarCriterio()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button id="btn-save" type="submit" form="form-add-criterio" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" form="form-add-criterio" class="btn btn-primary">Crear</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Indicador-->
    <div class="modal fade my-modal soft-rw my-font-all" id="md-indicador" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Indicador
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-indicador" onsubmit="return saveIndicador(this);">
                        <input id="input-idindicador" class="my-hide" type="text" disabled>
                        <div class="form-group my-hide">
                            <select class="form-control" name="" id="selectCriterios">
                                <option disabled value="">Buscar Criterio</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="detalle-indicador">Detalle</label>
                            <textarea required type="text" name="detalle-indicador" id="detalle-indicador" class="form-control" placeholder="Detalle..."></textarea>
                            <!-- <input required type="text" name="detalle-indicador" id="detalle-indicador" class="form-control" placeholder="Detalle..."> -->
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarIndicador()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button id="btn-save" type="submit" form="form-add-indicador" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" form="form-add-indicador" class="btn btn-primary">Crear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Fecha Limite-->
    <div class="modal fade my-modal soft-rw my-font-all" id="md-fecha-limite-tarea" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Fecha límite
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="fecha-limite-frm" onsubmit="return false">
                        <input id="input-fecha-limite" class="my-hide" type="text" disabled>
                        <div class="form-group">
                            <div class="col-md-6 my-del-pd pd-fix">
                                <label for="">Fecha</label>
                                <input required type="date" class="form-control" name="" id="fecha-limite-tapr" placeholder="">
                            </div>
                            <div class="col-md-6 my-del-pd">
                                <label for="">Hora</label>
                                <input required type="time" class="form-control" name="" id="hora-limite-tapr" placeholder="">
                            </div>

                        </div>
                        <div class="col-sm-12 my-mg-top">
                            <div class="form-check my-y-center">
                                <input class="col-sm-1" type="checkbox" name="" id="check-sin-fecha-limite" value="checked">
                                <label for="check-sin-fecha-limite" class="col-sm-11">Sin fecha límite</label>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarIndicador()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button id="btn-save" type="submit" form="form-add-indicador" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div name="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button name="cancelar" type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button name="crear" type="submit" class="btn btn-primary">Crear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Submenu-->
    <div class="modal fade my-modal soft-rw my-font-all" id="md-submenu" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Crear menu contenedor
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-submenu">
                        <input id="input-idsubmenu" class="my-hide" type="text" disabled>
                        <div class="form-group">
                            <label for="detalle-submenu">Nombre</label>
                            <input required type="text" name="nombre-submenu" id="input-nombre-submenu" class="form-control" placeholder="Nombre del menu contenedor..."></input>
                            <!-- <input required type="text" name="detalle-submenu" id="detalle-submenu" class="form-control" placeholder="Detalle..."> -->
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarsubmenu()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button name="btn-save" type="submit" form="form-add-submenu" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div name="modo-crear" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button id="btn-md-guardar" name="btn-save" type="submit" form="form-add-submenu" class="btn btn-primary">Crear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo $this->documento->getUrlStatic(); ?>/tema/paris/gentelella/smartWizard/js/jquery.smartWizard.js"></script>
    <script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/jquery-nestable/jquery.nestable.min.js"></script>
    <script src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/green/icheck.min.js"></script>
    <div id="addscript">
    <script type="text/javascript">
        $('ol li').hover(function() {
            $('ol li').removeClass('li-hover');
            $(this).addClass('li-hover');
            $(this).children('ol').css('background-color', 'white');
        }, function() {
            $(this).parent('ol').parent('li').addClass('li-hover');
            $(this).parent('ol').css('background-color', 'white');
            $(this).removeClass('li-hover');
        });
    </script>
</div>
<script type="text/javascript">
    const __oCurso = <?= json_encode($this->curso) ?>;
    __oCurso.idgrupoauladetalle = <?= $this->idgrupoauladetalle ?>;


    var oCrudTarea;
    var oMine = null;
    // var oCC_Vimeo;
    $(document).ready(() => {
        const oSubmenu = new Submenu;
        // oCC_Vimeo = new CC_Vimeo({
        //     idproyecto,
        //     oCurso:__oCurso
        // });
        oMine = new Mine();
        oSubmenu.launchCursoCrear();
        // oSubmenu.debug = true;
         //console.log(JSON.parse(atob(`<?php #echo base64_encode(json_encode($this->temas)) 
                                            ?>`)));
        const oCrudCurso = new CrudCurso;
        oCrudCurso.launchCursoCrear();
        oCrudTarea = new CrudTarea;
        oCrudTarea.launchCursoCrear();
    })
</script>
<script type="text/javascript">
    const IDCOMPLEMENTARIO = <?php echo !empty($this->idcomplementario) ? $this->idcomplementario : 0; ?>;
    const IDPERSONA = <?php echo !empty($this->curusuario["idpersona"]) ? $this->curusuario["idpersona"] : 0; ?>;
    const IDROL = <?php echo !empty($this->curusuario["idrol"]) ? $this->curusuario["idrol"] : 0; ?>;
    var configuracion_nota = <?php echo !empty($curso["configuracion_nota"]) ? $curso["configuracion_nota"] : 'false'; ?>;
    //const IDGRUPOAULADETALLE = <?php //echo !empty($this->idgrupoauladetalle) ? $this->idgrupoauladetalle : 0; ?>;

    var idcurso = <?php echo $this->idcurso ?>;
    var imgdefecto = '<?php echo $imgcursodefecto; ?>';
    var idproyecto = parseInt('<?php echo $this->idproyecto; ?>');
    var url_media = _sysUrlBase_;
    var txtjson = <?php echo !empty($txtjson) ? "JSON.parse(" . $txtjson . ");" : "{
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',imagefondo:'',imagen:'',colorfondo:'rgba(0, 0, 0, 0)'}, infoindice:'top',infoavance:0}" ?>;

    function onFinishCallback(objs, context) {
        history.back();
    }
    $(document).ready(function() {
        const fechaTarea = new FechaTarea({
            idsection: `md-fecha-limite-tarea`,
        });
        $('#wizard').smartWizard({
            labelNext: '<?php echo JrTexto::_("Siguiente"); ?>',
            labelPrevious: '<?php echo JrTexto::_("Anterior"); ?>',
            labelFinish: '<?php echo JrTexto::_("Finalizar"); ?>',
            onFinish: onFinishCallback
        });
        $('#wizard_verticle').smartWizard({
            transitionEffect: 'slide',
            });
            $('.buttonNext').addClass('btn btn-primary');
            $('.buttonPrevious').addClass('btn btn-warning');
            $('.buttonFinish').addClass('btn btn-default');
            var _infoportada = txtjson.infoportada;
            $('._infoportada[name="titulo"]').val(_infoportada.titulo);
            $('._infoportada[name="descripcion"]').val(_infoportada.descripcion);
            var imagenfondo = _infoportada.imagenfondo == undefined ? imgdefecto : _sysUrlBase_ + _infoportada.imagenfondo;
            $('._infoportada img[name="imagenfondo"]').attr('src', imagenfondo || imgdefecto);
            var colordefondo = _infoportada.colorfondo || 'rgba(0,0,0,0)';
            $('._infoportada[name="color"]').val(colordefondo);
            $('._infoportada[name="color"]').minicolors({
                opacity: true,
                format: 'rgb'
            });
            $('._infoportada[name="color"]').minicolors('settings', {
                value: colordefondo
            });

        __guardavalorcurso__ = function() {
            var formElement = document.getElementById("frmdatosdelcurso");
            var data = new FormData(formElement);
            data.append('txtjson', JSON.stringify(txtjson));
            __sysAyax({
                fromdata: data,
                url: _sysUrlBase_ + 'smart/acad_curso/guardarjson',
                callback: function(rs) {
                    if (rs.code == 200) {
                        idcurso = rs.newid;
                        $('#idcurso').val(idcurso);
                        var formData = new FormData();
                        formData.append('idcurso', idcurso);
                        formData.append('idproyecto', idproyecto);
                        __sysAyax({
                            showmsjok: false,
                            fromdata: formData,
                            url: _sysUrlSitio_ + '/proyecto_cursos/guardarProyecto_cursos',
                            callback: function(rs) {
                                validarcurso();
                            }
                        })
                    }
                }
            });
        }

        __guardavalorcurso = function(el, c, v) {
            var id = parseInt($('#idcurso').val());
            if (id > 0) {
                var data = new FormData();
                data.append('campo', c);
                data.append('valor', v);
                data.append('idcurso', id);
                data.append('icc', <?php echo $this->idcomplementario; ?>);
                __sysAyax({
                    fromdata: data,
                    url: _sysUrlBase_ + 'json/acad_curso/setcampo',
                    callback: function(rs) {
                        el.attr('inchange', '');
                        el.attr('oldvalor', '');
                    }
                })
            } else {
                if ("<?php echo $this->tipocurso; ?>" == "1") {
                    if ($('#idcurso').val() == "0") {
                        Swal.fire({
                            title: '<?php echo JrTexto::_('Confirme'); ?>',
                            text: '<?php echo JrTexto::_('¿Es un curso complementario?'); ?>',
                            icon: 'warning',
                            showCancelButton: true,
                            allowOutsideClick: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '<?php echo JrTexto::_('Si'); ?>',
                            cancelButtonText: '<?php echo JrTexto::_('No'); ?>',
                        }).then((result) => {
                            if (result.value) {
                                $("#sks").val("0");
                                $("#tipo").val("2");
                            } else {
                                $("#sks").val("1");
                                $("#tipo").val("1");
                            }
                            __guardavalorcurso__();
                        });
                    } else {
                        __guardavalorcurso__();
                    }
                } else {
                    __guardavalorcurso__();
                }
            }
        };

        <?php if ($this->validar) { ?>
            var _el_ = $("._autor_")
            var _campo_ = _el_.attr('name') || '';
            var _value_ = _el_.val() || _el_.attr('value');
            __guardavalorcurso(_el_, _campo_, _value_);
        <?php  } ?>

        $('.guardarvalorcurso').blur(function(e) {
            var el = $(this)
            var campo = el.attr('name') || '';
            var value = $(this).val() || el.attr('value');
            if (el.hasClass('vercolor') || campo == 'autor') value = $(this).val();
            var inchange = el.attr('inchange') || 0;
            var oldvalor = el.attr('oldvalor') || '';
            if (inchange == 0 && campo != 'autor') return;
            __guardavalorcurso(el, campo, value);
        }).click(function(e) {
            var el = $(this);
            var oldvalor = el.attr('oldvalor') || '';
            if (oldvalor == '') el.attr('oldvalor', el.val());
        }).change(function() {
            var el = $(this);
            if (el.val() == el.attr('oldvalor')) el.attr('inchange', 0)
            else el.attr('inchange', 1);
        }).on('click', '.borramendia', function(e) {
            e.preventDefault();
            var el = $(this);
            var c = el.attr('name');
            __guardavalorcurso(el, c, '');
            el.find('.__subirfile').attr(src, imgdefecto);
        }).on('click', '.__subirfile', function(e) {
            e.preventDefault();
            var $img = $(this);
            var idcurso = $('#idcurso').val() || '';
            if (idcurso == 0) idcurso = Date.now();
            <?php
            if ($this->idcomplementario != 0) {
            ?>
                var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : 'c_c/');
                var dirmedia_ = 'cursos_c/' + dirmedia;
            <?php
            } else {
            ?>
                var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
                var dirmedia_ = 'cursos/' + dirmedia;
            <?php
            }
            ?>

            // var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
            var nombre = (idcurso != '' ? ('logo_' + idcurso) : '');
            var oldmedia = $img.attr('src');
            __subirfile({
                file: $img,
                typefile: 'imagen',
                uploadtmp: true,
                guardar: true,
                'dirmedia': dirmedia_,
                'oldmedia': oldmedia,
                'nombre': nombre
            }, function(rs) {
                var f = rs.media.replace(url_media, '');
                $img.attr('src', _sysUrlBase_ + f).attr('oldmedia', _sysUrlBase_ + f).attr('data-link', f);
                var c = $img.attr('id');
                __guardavalorcurso($img.closest('.frmchangeimage'), c, f);
            });
        });

        $('._infoportada').blur(function(e) {
            //var idcurso=parseInt($('#idcurso').val()||'0');
            // if(idcurso==0) return alert('Necesita guardar datos basicos del curso primero');
            var el = $(this);
            var name = el.attr('name');
            if (name == 'color') name = 'colorfondo';
            _infoportada[name] = el.val();
            var inchange = el.attr('inchange') || '';
            if (inchange == '' || inchange == undefined) return;
            txtjson.infoportada = _infoportada;
            __txtjson = JSON.stringify(txtjson);
            __guardavalorcurso(el, 'txtjson', __txtjson);
        }).change(function(e) {
            // var idcurso=parseInt($('#idcurso').val()||'0');
            // if(idcurso==0) return alert('Necesita guardar datos basicos del curso primero');
            var el = $(this);
            var v = el.attr('value') || '_blanco_';
            el.attr('inchange', window.btoa(v));
        }).on('click', '.borramendia', function(e) {
            e.preventDefault();
            var el = $(this);
            _infoportada['imagenfondo'] = '';
            txtjson.infoportada = _infoportada;
            __txtjson = JSON.stringify(txtjson);
            __guardavalorcurso(el, 'txtjson', __txtjson);
            el.closest('._infoportada').find('.__subirfile').attr(src, imgdefecto);
        }).on('click', '.__subirfile', function(e) {
            e.preventDefault();
            var $img = $(this);
            var idcurso = parseInt($('#idcurso').val() || '0');
            // if(idcurso==0) return alert('Necesita guardar datos basicos del curso primero');
            // var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : '');
            <?php if ($this->idcomplementario != 0) {  ?>
                var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : '');
                var dirmedia_ = 'cursos_c/' + dirmedia;
            <?php } else {  ?>
                var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : '');
                var dirmedia_ = 'cursos/' + dirmedia;
            <?php } ?>
            var nombre = (idcurso != '' ? ('fondoportada_' + idcurso) : '');
            var oldmedia = $img.attr('src');
            __subirfile({
                file: $img,
                typefile: 'imagen',
                uploadtmp: true,
                guardar: true,
                'dirmedia': dirmedia_,
                'oldmedia': oldmedia,
                'nombre': nombre
            }, function(rs) {
                var f = rs.media.replace(url_media, '');
                //console.lof(f);
                $img.attr('src', _sysUrlBase_ + f).attr('oldmedia', _sysUrlBase_ + f);
                var c = $img.attr('id');
                _infoportada['imagenfondo'] = f;
                txtjson.infoportada = _infoportada;
                __txtjson = JSON.stringify(txtjson);
                __guardavalorcurso($img.closest('._infoportada'), 'txtjson', __txtjson);
            });
        })

        var cambiarcolor = function() {
            $('input[name="color"]').each(function(i, v) {
                var v = $(this).val() || 'rgba(76, 80, 84, 0.75)';
                $(this).val(v);
                $(this).minicolors({
                    opacity: true,
                    format: 'rgb'
                });
                $(this).minicolors('settings', {
                    value: v
                });
            })
        }
        cambiarcolor();

        __ocultarsinoescalificado = function(elli) {
            var activarcalificar = elli.children('.accionesdecurso').children('.btnestarea').hasClass('bg-success') == true ? true : false;
            if (activarcalificar == false) activarcalificar = elli.children('.accionesdecurso').children('.btnesproyecto').hasClass('bg-success') == true ? true : false;
            if (activarcalificar == true) {
                // working
                elli.addClass('conrubrica');
            } else {
                elli.removeClass('conrubrica');
                // actulaizarjson(elli);
                removeInstancia(elli);
                elli.children('.accionesdecurso').children('.btnaddrubricas').removeClass('bg-success');
            }
            if (activarcalificar == false) activarcalificar = elli.attr('txtjson-typelink') == 'smartquiz' ? true : false;
            if (activarcalificar == false) activarcalificar = elli.attr('txtjson-typelink') == 'foro' ? true : false;
            if (activarcalificar == false) activarcalificar = elli.attr('txtjson-typelink') == 'enlacecolaborativo' ? true : false;
            if (activarcalificar == false) activarcalificar = elli.attr('type') == 'smartquiz' ? true : false;
            if (activarcalificar == false) activarcalificar = elli.attr('type') == 'foro' ? true : false;
            if (activarcalificar == false) activarcalificar = elli.attr('type') == 'enlacecolaborativo' ? true : false;
            if (elli.attr('txtjson-typelink') == 'smartquiz' || elli.attr('type') == 'smartquiz') {
                elli.children('.accionesdecurso').children('.btnestarea').removeClass('bg-success');
                elli.children('.accionesdecurso').children('.btnesproyecto').removeClass('bg-success');
            }
            if (activarcalificar == true) {
                elli.addClass('concalificacion');
            } else {
                elli.removeClass('concalificacion');
            }
        }

        //Obtine todos los grupos de estudio de este curso
        var _listargruposdeestecurso=function(loading=false){
            if(loading) Swal.showLoading();                    
            return new Promise((resolve, reject) => {
                 $.post(_sysUrlBase_ + 'json/acad_grupoauladetalle/listadogruposdeestudioxcurso',{
                    'idgrupoauladetalle': __oCurso.idgrupoauladetalle,
                    'idcurso': __oCurso.idcurso,
                    'idcomplementario': __oCurso.idcomplementario,                            
                }, function(data){
                    Swal.close();                   
                    if (data.code == 200) {
                        resolve(data.data);
                    }else{
                        console.log('Error',data);
                        reject(false);
                    }
                }, 'json');
                return;
            });
        }

        //obtiene todo el listado de alumnos de un grupoauladetalle
        var __listadoestudiantesdelgrupo=function(loading=false,idgrupoauladetalle){            
            return new Promise((resolve, reject) => {
                if(idgrupoauladetalle=='' || idgrupoauladetalle==undefined || idgrupoauladetalle==null){
                    //let data=;
                    resolve([]);                    
                }
                if(loading) Swal.showLoading();
                $.post(_sysUrlBase_ + 'json/acad_matricula/',{
                    'idgrupoauladetalle': idgrupoauladetalle,
                    'sql':'sql-alumnos',
                },function(data){
                    if(loading) Swal.close();
                    if (data.code == 200){          
                        resolve(data.data);
                    }else{
                        console.log('Error',data);
                        reject(false);
                    }
                },'json');
                return;
            });
        }

        var __listarrecursocolaborativo=function(loading=false,idrecurso){
            return new Promise((resolve, reject) => {
                if(idrecurso=='' || idrecurso==undefined || idrecurso==null){                   
                    resolve([]);                    
                }
                if(loading) Swal.showLoading();
                $.post(_sysUrlBase_ + 'json/recursos_colaborativos/',{
                    'idrecurso': idrecurso,  
                    'sqlget':true,                  
                },function(data){
                    if(loading) Swal.close();
                    if (data.code == 200){          
                        resolve(data.data);
                    }else{
                        console.log('Error',data);
                        reject(false);
                    }
                },'json');
                return;
            });
        }

        var __listarinfoAjax=function(loading=false,params){
            return new Promise((resolve, reject) => {                
                if(loading) Swal.showLoading();
                $.post(_sysUrlBase_ + params.url,params.data,function(data){
                    if(loading) Swal.close();
                    if (data.code == 200){
                        resolve(data.data);
                    }else{
                        console.log('Error',data);
                        reject(false);
                    }
                },'json');
                return;
            });
        }

        var __listarrecursocolaborativoasignados=function(loading=false,idrecurso){
            return new Promise((resolve, reject) => {
                if(idrecurso=='' || idrecurso==undefined || idrecurso==null){                   
                    resolve([]);                    
                }
                if(loading) Swal.showLoading();
                $.post(_sysUrlBase_ + 'json/recursos_colaborativosasignacion/',{
                    'idrecurso': idrecurso,  
                    'idgrupoauladetalle':__oCurso.idgrupoauladetalle,                  
                },function(data){
                    if(loading) Swal.close();
                    if (data.code == 200){          
                        resolve(data.data);
                    }else{
                        console.log('Error',data);
                        reject(false);
                    }
                },'json');
                return;
            });
        }

        $('#_contentcurso_').on('click', '.btnopenmodal , .btneditmenu', function(e) {
            e.preventDefault();
            var el = $(this);
            var dt = {
                html: $('#_infoTema_').html()
            };
            var paso3 = $('#nestable');
            var liclonado = paso3.find('li.liclone').clone(true);
            var md = __sysmodal(dt);
            if (!el.hasClass('btnopenmodal')) {
                liclonado = el.closest('li');
                if (liclonado.attr('espadre') == 1) {
                    md.find('[name="to-rename"]').html('Nombre del Menu padre');
                    md.find('[name="to-rename"]').addClass('separar');
                    md.find('#frmdatosdeltema').addClass('soft-rw my-font-all');
                    md.find('[name="to-center"]').addClass('my-center');
                    md.find('[name="to-hide"]').hide();
                }
            } else {
                liclonado.removeClass('liclone').show();
                liclonado.attr('orden', paso3.find('li').length);
                liclonado.attr('tipo-li', 'idk');
            }

            md.find('textarea[name="descripcion"]').val(liclonado.attr('descripcion') || '');
            md.find('input[name="nombre"]').val(liclonado.children('span.titulo').text() || '');
            md.find('input[name="color"]').val(liclonado.attr('color'));
            md.find('input[name="color"]').minicolors({
                opacity: true,
                format: 'rgb'
            });
            md.find('input[name="color"]').minicolors('settings', {
                value: liclonado.attr('color')
            });

            var _liimg = (liclonado.attr('imagen') || imgdefecto);
            var _indeximg = _liimg.lastIndexOf('static/');
            if (_indeximg > -1) _liimg = _liimg.substring(_indeximg);
            _liimg = (_liimg == undefined || _liimg == '') ? imgdefecto : _sysUrlBase_ + _liimg;
            md.find('img.__subirfile').attr('src', _liimg).attr('data-link', liclonado.attr('imagen'));

            md.on('click', '.btnguardartema', function(ev) {
                var btnGuardar = $(this);
                if (btnGuardar.attr('disabled') == 'disabled') {
                    return false;
                }
                btnGuardar.attr('disabled', 'disabled');
                ev.preventDefault();
                var idcurso = $('#idcurso').val();
                var formData = new FormData();
                formData.append('idcurso', parseInt(idcurso));
                formData.append('idcursodetalle', parseInt(liclonado.attr('idcursodetalle') || 0));
                formData.append('idrecursopadre', parseInt(liclonado.attr('idrecursopadre') || 0));
                formData.append('idrecursoorden', parseInt(liclonado.attr('idrecursoorden') || 0));
                formData.append('orden', parseInt(liclonado.attr('orden') || 0));
                formData.append('idrecurso', liclonado.attr('idrecurso') || 0);
                formData.append('tiporecurso', liclonado.attr('tiporecurso') || 'M');
                formData.append('idlogro', liclonado.attr('idlogro') || 0);
                formData.append('url', liclonado.attr('url') || '');
                formData.append('idpadre', liclonado.attr('idpadre') || 0);
                formData.append('espadre', liclonado.attr('espadre') || 0);
                formData.append('color', md.find('input[name="color"]').val());
                formData.append('esfinal', liclonado.attr('esfinal') || 0);
                formData.append('descripcion', md.find('textarea[name="descripcion"]').val());
                formData.append('nombre', md.find('input[name="nombre"]').val());
                //Actualiza menus
                if (oCrudTarea.esTareaProyecto(liclonado)) {
                    fechaTarea.li = liclonado;
                    fechaTarea.updateSesionPestania({
                        nombre: md.find('input[name="nombre"]').val(),
                    });
                    oCrudTarea.li = liclonado;
                    oCrudTarea.updateSesionPestania({
                        nombre: md.find('input[name="nombre"]').val(),
                    });
                }

                formData.append('imagen', md.find('img.__subirfile').attr('data-link') || '');
                formData.append('accimagen', liclonado.attr('accimagen') || 'sg');
                formData.append('icc', IDCOMPLEMENTARIO);
                __sysAyax({
                    fromdata: formData,
                    url: _sysUrlBase_ + 'smart/acad_cursodetalle/guardarMenu',
                    callback: function(rs) {
                        var dt = rs.newid;
                        liclonado.children('span.titulo').text(md.find('input[name="nombre"]').val());
                        liclonado.attr('descripcion', md.find('textarea[name="descripcion"]').val());
                        liclonado.attr('idcursodetalle', dt.idcursodetalle);
                        liclonado.attr('tipo-li', 'menu');
                        //console.log(dt.orden);
                        // liclonado.attr('data-idcursodetalle', dt.idcursodetalle);
                        liclonado.data('idcursodetalle', dt.idcursodetalle);
                        liclonado.attr('idnivel', dt.idnivel);
                        liclonado.attr('idpadre', dt.idpadre);
                        liclonado.attr('espadre', dt.espadre);
                        liclonado.data('id', dt.orden);
                        liclonado.attr('imagen', md.find('img.__subirfile').attr('data-link'));
                        liclonado.attr('ordennivel', dt.ordennivel);
                        if (el.hasClass('btnopenmodal')) {
                            var ultimoli = paso3.children('ol').find('li').last();
                            liclonado.insertBefore(ultimoli);
                        }
                        btnGuardar.removeAttr('disabled');
                        __cerrarmodal(md, true);

                    }
                });
            }).on('click', '.borramendia', function(e) {
                e.preventDefault();
                var el = $(this);
                el.closest('.frmchangeimage').find('.__subirfile').attr(src, imgdefecto).attr('data-link', '');
            }).on('click', '.__subirfile', function(e) {
                e.preventDefault();
                var $img = $(this);
                var idcurso = $('#idcurso').val() || '';
                // var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : '');
                <?php
                if ($this->idcomplementario != 0) {
                ?>
                    var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : '');
                    var dirmedia_ = 'cursos_c/' + dirmedia;
                <?php
                } else {
                ?>
                    var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : '');
                    var dirmedia_ = 'cursos/' + dirmedia;
                <?php
                }
                ?>
                var nombre = (idcurso != '' ? ('imgses_' + Date.now() + idcurso) : '');
                var oldmedia = $img.attr('src');
                __subirfile({
                    file: $img,
                    typefile: 'imagen',
                    uploadtmp: true,
                    guardar: true,
                    'dirmedia': dirmedia_,
                    'oldmedia': oldmedia,
                    'nombre': nombre
                }, function(rs) {
                    var f = rs.media.replace(url_media, '');
                    $img.attr('src', _sysUrlBase_ + f).attr('oldmedia', _sysUrlBase_ + f).attr('data-link', f);
                });
            })
        }).on('click', '.btnremovemenu', function(e) {
            Swal.fire({
                title: '<?php echo JrTexto::_('Confirme'); ?>',
                text: '<?php echo JrTexto::_('¿Seguro de eliminar?'); ?>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<?php echo JrTexto::_('Aceptar'); ?>',
                cancelButtonText: '<?php echo JrTexto::_('Cancelar'); ?>',
            }).then((result) => {
                if (result.value) {
                    var $li = $(this).closest('li');
                    var idcurso = $('#idcurso').val() || '';
                    var formData = new FormData();

                    let arrIdChilds = [];
                    if ($li.attr('espadre') == 1) {
                        let arrLiChilds = $li.find('ol').children('li');
                        for (let index = 0; index < arrLiChilds.length; index++) {
                            const liHijo = arrLiChilds[index];
                            arrIdChilds.push($(liHijo).attr('idcursodetalle'));
                        }
                    }

                    formData.append('idcurso', parseInt(idcurso));
                    formData.append('idcursodetalle', parseInt($li.attr('idcursodetalle')));
                    formData.append('idrecurso', $li.attr('idrecurso'));
                    formData.append('tiporecurso', $li.attr('tipo'));
                    formData.append('arrIdChilds', JSON.stringify(arrIdChilds));
                    formData.append('icc', <?php echo $this->idcomplementario; ?>);
                    __sysAyax({
                        url: _sysUrlBase_ + 'smart/acad_cursodetalle/eliminarMenu',
                        fromdata: formData,
                        type: 'json',
                        showmsjok: false,
                        callback: function(rs) {
                            $li.remove();
                        }
                    });
                }
            });
        }).on('click', '.btnrecursopagado , .btnconteacher , .iconproyecto, .btnmostrarpregunta , i.mostraricon, .btnverenmenu', function(ev) {
            ev.preventDefault();
            var el = $(this);
            var active = false;
            var li = el.closest('li');
            var ol = li.children('ol');
            var lipadre = li;
            if (el.hasClass('mostraricon')) {
                var icon = el.closest('a').attr('data-icon') || '#';
                var htmlicon = el.closest('a').html();
                el.closest('.btn-group').children('button').html(htmlicon);
                li.attr('data-icon', icon);
            } else if (el.hasClass('btnverenmenu')) {
                if ($(this).hasClass('fa-eye')) {
                    $(this).removeClass('fa-eye').addClass('fa-eye-slash');
                } else $(this).addClass('fa-eye').removeClass('fa-eye-slash');
            }
            if (!el.hasClass('btnverenmenu')) {
                if (el.hasClass('bg-success')) {
                    el.removeClass('bg-success')
                } else {
                    el.addClass('bg-success')
                    active = true;
                }
            }

            if (!li.hasClass('esmenu')) {
                lipadre = li.closest('ol').closest('li');
                ol = lipadre.children('ol');
            }
            //actulaizarjson(lipadre);     
            //preugntar si es tarea o proyecto 
            // if (el.hasClass('btnestarea') || el.hasClass('btnesproyecto')) {


            // } else {
            //no es tarea y proyectos
            if (el.hasClass('mostraricon')) {
                var icon = el.closest('a').attr('data-icon') || '#';
                var htmlicon = el.closest('a').html();
                el.closest('.btn-group').children('button').html(htmlicon);
                li.attr('data-icon', icon);
            }
            actulaizarjson(lipadre);
            // }
        }).on('click', '.btnestarea, .btnesproyecto', function(ev) {
            // cuando da click en icono tarea o proyecto
            ev.preventDefault();
            const flujoTareas = (fecha_limite = 'Sin fecha limite') => {
                let $this = this;

                var el = $(this);
                var active = false;
                var li = el.closest('li');
                var ol = li.children('ol');
                var lipadre = li;
                if (el.hasClass('mostraricon')) {
                    var icon = el.closest('a').attr('data-icon') || '#';
                    var htmlicon = el.closest('a').html();
                    el.closest('.btn-group').children('button').html(htmlicon);
                    li.attr('data-icon', icon);
                }

                if (el.hasClass('bg-success')) {
                    el.removeClass('bg-success')
                } else {
                    el.addClass('bg-success')
                    if (el.hasClass('btnestarea')) {
                        li.find('.btnesproyecto').removeClass('bg-success');
                    } else {
                        li.find('.btnestarea').removeClass('bg-success');
                    }
                    active = true;
                }

                if (!li.hasClass('esmenu')) {
                    lipadre = li.closest('ol').closest('li');
                    ol = lipadre.children('ol');
                }
                ////
                __ocultarsinoescalificado(el.closest('li'));
                actulaizarjson(lipadre);
                if (el.hasClass('bg-success')) { //mostrar el alerta          
                    /*
                        Swal.fire({
                            title: '<?php echo JrTexto::_('¿Seguro de Enviar Correo?'); ?>',
                            text: 'Al, aceptar enviara un correo a todos los estudiantes matriculados de la asignación actual',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '<?php echo JrTexto::_('Aceptar'); ?>',
                            cancelButtonText: '<?php echo JrTexto::_('Cancelar'); ?>',
                        }).then((result) => {
                            if (result.value) {
                                //si acepta enviar correo
                                var _URL = (el.hasClass('btnestarea')) ? String.prototype.concat(_sysUrlBase_, 'json/sendemail/tareasasignada') : String.prototype.concat(_sysUrlBase_, 'json/sendemail/proyectosasignada');
                                var _idcursodetalle = null;
                                if (typeof lipadre.attr('idcursodetalle') == 'undefined') {
                                    _idcursodetalle = lipadre.closest('ol').closest('li').attr('idcursodetalle');
                                } else {
                                    _idcursodetalle = lipadre.attr('idcursodetalle');
                                }
                                let idcurso = $('#idcurso').val() || '';
                                let formData = new FormData();
                                formData.append('idcurso', parseInt(idcurso));
                                formData.append('fecha_limite', fecha_limite);
                                formData.append('idcursodetalle', _idcursodetalle);
                                formData.append('nombretarea', lipadre.find('.titulo').text());
                                __sysAyax({
                                    url: _URL,
                                    fromdata: formData,
                                    type: 'json',
                                    showmsjok: true,
                                    callback: function(rs) {
                                        console.log("correo enviado");
                                    }
                                });
                            } //endif de aceptar el dialogo
                        });
                    */
                }
            }
            let el = $(this);
            let li = el.parent().parent();
            if (li.attr('txtjson-typelink') != 'smartquiz' &&
                li.attr('type') != 'smartquiz') {
                if (el.hasClass('btnestarea')) {
                    li.attr('tipo-cron', 'T');
                } else {
                    li.attr('tipo-cron', 'P');
                }

                fechaTarea.li = li;
                fechaTarea.flujo = flujoTareas;
                if (!el.hasClass('bg-success')) {
                    fechaTarea.showModal();
                } else {
                    fechaTarea.deleteCron();
                }
            }
        }).on('click', '.btnaddoption, .btneditoption', function(e) {
            var el = $(this);
            var dt = {
                html: $('#_infoTema_').html()
            };
            var paso3 = $('#nestable');
            var md = __sysmodal(dt);
            var liclonado = '';
            var lipadre = el.closest('.esmenu');
            var idcursodetalle = lipadre.attr('idcursodetalle');
            md.find('textarea[name="descripcion"]').closest('.form-group').remove();
            if (el.hasClass('btnaddoption')) {
                md.find('input[name="nombre"]').val('');
                md.find('input[name="color"]').val('rgba(32, 50, 69, 0)');
                md.find('input[name="color"]').minicolors({
                    opacity: true,
                    format: 'rgb'
                });
                md.find('input[name="color"]').minicolors('settings', {
                    value: 'rgba(32, 50, 69, 0)'
                });
                md.find('img.__subirfile').attr('src', imgdefecto).attr('data-link', '');
            } else {
                var liclonado = el.closest('li');
                md.find('input[name="nombre"]').val(liclonado.find('span.titulo').text() || '');
                md.find('input[name="color"]').val(liclonado.attr('color'));
                md.find('input[name="color"]').minicolors({
                    opacity: true,
                    format: 'rgb'
                });
                md.find('input[name="color"]').minicolors('settings', {
                    value: liclonado.attr('color')
                });
                var _liimg = liclonado.attr('imagen') || imgdefecto;
                var _indeximg = _liimg.lastIndexOf('static/');
                if (_indeximg > -1) _liimg = _liimg.substring(_indeximg);
                _liimg = (_liimg == undefined || _liimg == '') ? imgdefecto : _sysUrlBase_ + _liimg;
                md.find('img.__subirfile').attr('src', _liimg).attr('data-link', liclonado.attr('imagen'));
            }
            md.on('click', '.btnguardartema', function(ev) { //(not sure)cuando se está editando y guarda...
                ev.preventDefault();
                var idcurso = $('#idcurso').val();
                var k = lipadre.attr('number') || idcursodetalle;
                lipadre.attr('numbre', k);
                if (lipadre.children('ol').length == 0) {
                    lipadre.append('<ol class="dd-list esjson itemchange1" number="' + k + '"  dcursodetalle="' + idcursodetalle + '" ></ol>')
                }
                ol = lipadre.children('ol');
                var nli = ol.children('li').length;
                if (el.hasClass('btnaddoption')) {
                    ol.append('<li tipo-li="pestania" idcursodetalle="' + idcursodetalle + '" idpestania="' + idcursodetalle + '' + nli + '" class="dd-item" data-id="' + idcursodetalle + nli + '" id="' + idcursodetalle + '_' + nli + '" type="" link="" color="' + md.find('input[name="color"]').val() + '" imagen="' + md.find('img.__subirfile').attr('data-link') + '" style="list-style: none; border-bottom: 1px solid #eee;"><i class=" btn btn-xs fa fa-arrows dd-handle "  title="<?php echo JrTexto::_('Move'); ?>" style="cursor: move; display: inline-block; margin-top: 0px;"></i>  <span class="titulo">' + md.find('input[name="nombre"]').val() + '</span><span class="btn-group accionesdecurso" style="position: absolute; right: 0px;"> <i class="btn btn-xs fa fa-magic btnaddhabilidades" title="<?php echo JrTexto::_('Add habilidades'); ?>"></i> <i class="btn btn-xs fa fa-archive btnaddrubricas" title="<?php echo JrTexto::_('Add Rubrica'); ?>" ></i> <i class="btn btn-xs fa fa-question btnmostrarpregunta" title="<?php echo JrTexto::_('Show questions'); ?>"></i> <i class="btn btn-xs fa fa-file btnaddcontent" title="<?php echo JrTexto::_('Add content'); ?>"></i> <!--i class="btn btn-xs fa fa-film btnaddvideos" title="<?php echo JrTexto::_('Add Video'); ?>"></i--> <!--i class="btn btn-xs fa fa-youtube-play btnaddvideosyoutube" title="<?php echo JrTexto::_('Add Video Youtube'); ?>"></i--> <i class="btn btn-xs fa fa-tasks btnestarea" title="<?php echo JrTexto::_('Es una tarea'); ?>"></i><i class="btn btn-xs fa fa-folder btnesproyecto" title="<?php echo JrTexto::_('Es un Proyecto'); ?>"></i><i class="btn btn-xs fa fa-dollar btnrecursopagado" title="<?php echo JrTexto::_('Es un Recurso Pagado'); ?>"<?php echo ($this->tipocurso == '1') ? '' : ' style="display:none;"'; ?>></i>  <i class="btn btn-xs fa btnverenmenu fa-eye" title="<?php echo JrTexto::_('ver/ocultar menu'); ?>"></i><i class="btn btn-xs fa fa-pencil btneditoption" title="<?php echo JrTexto::_('Edit'); ?>"></i><i class="btn btn-xs fa fa-trash btnremoveoption" title="<?php echo JrTexto::_('Remove'); ?>"></i></span></li>');
                    el.siblings('.btnaddcontent').hide();
                    lipadre.addClass('tienehijos');
                    _nestablehijos();
                } else {
                    var liclonado = el.closest('li');
                    liclonado.find('span.titulo').text(md.find('input[name="nombre"]').val());
                    liclonado.attr('color', md.find('input[name="color"]').val());
                    liclonado.attr('imagen', md.find('img.__subirfile').attr('data-link'));
                }
                var addscript = `
                    $('ol li').hover(function () {
                        $('ol li').removeClass('li-hover');
                        $(this).addClass('li-hover');
                        $(this).children('ol').css('background-color', 'white');
                    }, function () {
                        $(this).parent('ol').parent('li').addClass('li-hover');
                        $(this).parent('ol').css('background-color', 'white');
                        $(this).removeClass('li-hover');
                    });
                `;
                $("#addscript").children("script").html(addscript);
                __cerrarmodal(md, true);
                actulaizarjson(lipadre);
            }).on('click', '.borramendia', function(e) {
                e.preventDefault();
                var el = $(this);
                el.closest('.frmchangeimage').find('.__subirfile').attr(src, imgdefecto).attr('data-link', '');
            }).on('click', '.__subirfile', function(e) {
                e.preventDefault();
                var $img = $(this);
                var idcurso = $('#idcurso').val() || '';
                <?php
                if ($this->idcomplementario != 0) {
                ?>
                    // var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '/') : '');
                    var dirmedia = 'c_c_' + idcurso + '_' + IDCOMPLEMENTARIO + '/sesion_' + idcursodetalle + '/';
                    var dirmedia_ = 'cursos_c/' + dirmedia;
                <?php
                } else {
                ?>
                    var dirmedia = 'curso_' + idcurso + '/sesion_' + idcursodetalle + '/';
                    var dirmedia_ = 'cursos/' + dirmedia;
                <?php
                }
                ?>
                // var dirmedia = 'curso_' + idcurso + '/sesion_' + idcursodetalle + '/';
                var nombre = 'imgses_' + idcurso + '_' + idcursodetalle;
                var oldmedia = $img.attr('src');
                __subirfile({
                    file: $img,
                    typefile: 'imagen',
                    uploadtmp: true,
                    guardar: true,
                    'dirmedia': dirmedia_,
                    'oldmedia': oldmedia,
                    'nombre': nombre
                }, function(rs) {
                    var f = rs.media.replace(url_media, '');
                    $img.attr('src', _sysUrlBase_ + f).attr('oldmedia', _sysUrlBase_ + f).attr('data-link', f);
                });
            })
        }).on('click', '.btnremoveoption', function(ev) {
            Swal.fire({
                title: '<?php echo JrTexto::_('Confirme'); ?>',
                html: '<h4>¿Seguro de eliminar este Menu ?</h4> <br> Considere que si tiene asignado examenes y tareas  tambien podrian ser eliminados',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<?php echo JrTexto::_('Aceptar'); ?>',
                cancelButtonText: '<?php echo JrTexto::_('Cancelar'); ?>',
            }).then((result) => {
                if (result.value) {
                    var li = $(this).closest('li');
                    var lipadre = li.closest('.esmenu');
                    var ol = lipadre.children('ol');
                    li.remove();
                    if (ol.find('li').length == 0) {
                        ol.remove();
                        lipadre.find('i.btnaddcontent').show();
                        lipadre.removeClass('tienehijos');
                    }
                    actulaizarjson(lipadre);
                }
            });
        }).on('click', '.btnaddcontent', function(ev) {
            var el = $(this);
            var eltmp_ = $(this);
            var elli = $(this).closest('li');
            var dt = {
                html: $('#showpaddcontenido').html()
            };
            var paso3 = $('#nestable');
            var md = __sysmodal(dt);
            var li = el.closest('.esmenu');
            var idcursodetalle = li.attr('idcursodetalle');
            var idcurso = $('#idcurso').val();
            var __addexamen = function(obj, fncall) {
                //let md=__sysmodal({titulo:'Examenes',html:$('#config_plantilla').html()});        
                var lstmp = 'btnaddexamen' + __idgui();
                obj.addClass(lstmp);
                //&xxidccxx='+IDCOMPLEMENTARIO
                if (IDCOMPLEMENTARIO == 0 && IDROL == 1) {
                    var url = String.prototype.concat(_sysUrlBase_, 'smart/quiz/buscar/?plt=modal&idproyecto=PY', idproyecto, '&fcall=', lstmp);
                } else if (IDCOMPLEMENTARIO != 0 && IDROL != 1) {
                    var url = String.prototype.concat(_sysUrlBase_, 'smart/quiz/buscar/?plt=modal&xxidpersonaxx=', IDPERSONA, '&idproyecto=PY', idproyecto, '&fcall=', lstmp);
                } else {
                    var url = String.prototype.concat(_sysUrlBase_, 'smart/quiz/buscar/?plt=modal&idproyecto=PY', idproyecto, '&fcall=', lstmp);
                }

                var mdquiz = __sysmodal({
                    titulo: 'Quiz',
                    url: url
                });
                var estadosabc2 = {
                    '1': '<?php echo JrTexto::_("Active") ?>',
                    '0': '<?php echo JrTexto::_("Inactive") ?>'
                };
                var tabledatosabc2 = function() {
                    var frm = document.createElement('form');
                    //var formData = new FormData();
                    //formData.append('titulo', mdquiz.find('#textoabc2').val() || '');
                    var url = _sysUrlBase_ + 'json/quizexamenes/?titulo=' + (mdquiz.find('#textoabc2').val() || '');

                    /*if (IDCOMPLEMENTARIO == 0 && IDROL == 1) {
                        url = String.prototype.concat(url, '&u=', u, '&id=', '&p=', cl);
                    } else if (IDCOMPLEMENTARIO != 0 && IDROL != 1) {
                        url = String.prototype.concat(url, '&xxidpersonaxx=', IDPERSONA, '&u=', u, '&id=', '&p=', cl);
                    } else {
                        url = String.prototype.concat(url, '&u=', u, '&id=', '&p=', cl);
                    }*/
                    $.ajax({
                        url: url,
                        type: "GET",
                        //data: formData,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        cache: false,
                        beforeSend: function(XMLHttpRequest) {
                            Swal.showLoading();
                        },
                        success: function(res) {

                            if (res.code == 200) {
                                var midata = res.datos;
                                if (midata.length) {
                                    var html = '';
                                    var controles = mdquiz.find('#controlesabc2').html();
                                    $.each(midata, function(i, v) {
                                        var urlimg = _sysUrlStatic_ + '/media/imagenes/cursos/';
                                        let portada = (v.portada || '');
                                        portada = portada.replace(/__xRUTABASEx__\/static/g, '<?php echo URL_SMARTQUIZ; ?>static');

                                        var srcimg = _sysfileExists(portada) ? (portada) : (urlimg + 'nofoto.jpg');
                                        html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="' + v.idexamen + '">' + controles;
                                        html += '<div class="item"><img class="img-responsive" src="' + srcimg + '" width="100%"></div>';
                                        html += '<div class="nombre"><strong>' + v.titulo + '</strong></div>';
                                        html += '</div></div>';
                                    });
                                    mdquiz.find('#datalistadoabc2 #data').html(html).show('fast').siblings('div').hide('fast');
                                } else
                                    mdquiz.find('#datalistadoabc2 #sindatos').show('fast').siblings('div').hide('fast');
                            } else {
                                mdquiz.find('#datalistadoabc2 #error').html(res.message).show('fast').siblings('div').hide('fast');
                            }
                            Swal.close();
                        },
                        error: function(e) {
                            Swal.close();
                            //$('#datalistadoabc2 #error').show('fast').siblings('div').hide('fast');
                        }
                    });
                }
                mdquiz.on('change', '#idproyecto', function() {
                    tabledatosabc2();
                }).on('click', '.btnbuscar', function(ev) {
                    tabledatosabc2();
                }).on('keyup', '#textoabc2', function(ev) {
                    if (ev.keyCode == 13) tabledatosabc2();
                }).on("mouseenter", '.panel-user', function() {
                    if (!$(this).hasClass('active')) {
                        $(this).siblings('.panel-user').removeClass('active');
                        $(this).addClass('active');
                    }
                }).on("mouseleave", '.panel-user', function() {
                    $(this).removeClass('active');
                }).on("click", '.btn-selected', function() {
                    $(this).addClass('active');
                    var idexamen = $(this).closest('.panel-user').attr('data-id');
                    obj.attr('data-idexamen', idexamen);
                    var link = 'smart/quiz/ver/?idexamen=' + idexamen + '&idproyecto';
                    if (__isFunction(fncall)) fncall(link);
                    $(this).closest('.modal').find('.cerrarmodal').trigger('click');
                })
                setTimeout(function() {
                    tabledatosabc2()
                }, 500);
            } //end addexamen
            var __addgame = function(obj, fncall) {
                obj.off('addgame');
                var lstmp = 'addgame' + __idgui();
                obj.addClass(lstmp);
                var url = _sysUrlBase_ + 'smart/games/buscar/?plt=modal&fcall=' + lstmp;
                var mdgame = __sysmodal({
                    titulo: 'Games',
                    url: url
                });
                obj.on('addgame', function(ev) {
                    $(this).off(lstmp);
                    $(this).removeClass(lstmp);
                    var idgame = $(this).attr('data-idgame');
                    var link = _sysUrlSitio_ + '/game/ver/?idgame=' + idgame + '&tmp=' + lstmp;
                    link = link.replace(_sysUrlBase_, '', link);
                    if (__isFunction(fncall)) fncall(link);
                    //__cargarplantilla({url:link,donde:'contentpages'});     
                })

                mdgame.on('click', '.btnbuscar', function(ev) {
                    var formData = new FormData();
                    formData.append('nombre', $(this).siblings('input').val() || '');
                    __sysAyax({
                        //donde:$('#plnindice'),
                        url: _sysUrlSitio_ + '/tools/json_buscarGames/',
                        fromdata: formData,
                        mostrarcargando: true,
                        showmsjok: false,
                        callback: function(res) {
                            var html = '';
                            var midata = res.data;
                            if (midata.length) {
                                var html = '';
                                var controles = mdgame.find('#controlesabc1').html();
                                $.each(midata, function(i, v) {
                                    let titulo = v.titulo || '';
                                    if (titulo !== '') {
                                        html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="' + v.idtool + '" style="margin-bottom: 1ex;">' + controles;
                                        //html+='<div class="item"><img class="img-responsive" src="'+srcimg+'" width="100%">'; 
                                        html += '<div class="nombre"><strong>' + v.titulo + '</strong></div>';
                                        html += '</div></div></div>';
                                    }
                                });
                                mdgame.find('#datalistadoabc1 #data').html(html).show('fast').siblings('div').hide('fast');
                            } else
                                mdgame.find('#datalistadoabc1 #sindatos').show('fast').siblings('div').hide('fast');
                        }
                    });
                }).on('keyup', '#textoabc3', function(ev) {
                    if (ev.keyCode == 13) mdgame.find('.btnbuscar').trigger('click');
                }).on("mouseenter", '.panel-user', function() {
                    if (!$(this).hasClass('active')) {
                        $(this).siblings('.panel-user').removeClass('active');
                        $(this).addClass('active');
                    }
                }).on("mouseleave", '.panel-user', function() {
                    $(this).removeClass('active');
                }).on("click", '.btn-selected', function() {
                    $(this).addClass('active');
                    let pnl = $(this).closest('.panel-user');
                    let idgame = pnl.attr('data-id');
                    let idsesion = pnl.attr('data-idsesion');
                    obj.attr('data-idgame', idgame);
                    obj.trigger("addgame");
                    mdgame.find('.cerrarmodal').trigger('click');
                })
                setTimeout(function() {
                    mdgame.find('.btnbuscar').trigger('click');
                }, 500);
            }// end game
            var __addtics = function(obj, met, fncall) {
                obj.off('addtics');
                var lstmp = 'addtics' + __idgui();
                obj.addClass(lstmp);
                var url = _sysUrlBase_ + 'smart/tics/buscar/?plt=modal&fcall=' + lstmp + '&met=' + met;
                var mdtics = __sysmodal({
                    titulo: 'Games',
                    url: url
                });
                obj.on('addtics', function(ev) {
                    $(this).off(lstmp);
                    $(this).removeClass(lstmp);
                    var idtic = $(this).attr('data-idtic'); // idactividad
                    var idsesion = $(this).attr('data-idsesion');
                    var link = _sysUrlSitio_ + '/actividad/ver/?idactividad=' + idtic + "&met=" + met + "&idsesion=" + idsesion + '&tmp=' + lstmp;
                    link = link.replace(_sysUrlBase_, '', link);
                    obj.attr('data-link', link);
                    if (__isFunction(fncall)) fncall(link);
                    //__cargarplantilla({url:link,donde:'contentpages'});     
                })
                mdtics.on('click', '.btnbuscar', function(ev) {
                    var formData = new FormData();
                    formData.append('texto', $(this).siblings('input').val() || '');
                    formData.append('met', met);
                    __sysAyax({
                        donde: $('#plnindice'),
                        url: _sysUrlBase_ + 'actividad/buscaractividadjson/',
                        fromdata: formData,
                        mostrarcargando: true,
                        showmsjok: false,
                        callback: function(rs) {
                            midata = rs.data;
                            var html = '';
                            if (midata != undefined) {
                                var controles = mdtics.find('#controlesabc3').html();
                                $.each(midata, function(i, v) {
                                    var imgtemp = v.imagen || '';
                                    var ultimostaticp = imgtemp.lastIndexOf('static');
                                    if (ultimostaticp > 0) {
                                        imgtemp = _sysUrlBase_ + imgtemp.substr(ultimostaticp);
                                    } else {
                                        imgtemp = _sysUrlBase_ + 'static/media/nofoto.jpg';
                                    }
                                    html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="' + v.idactividad + '" data-idsesion="' + v.sesion + '">' + controles;
                                    html += '<div class="item"><img class="img-responsive" src="' + imgtemp + '" width="100%"></div>';
                                    html += '<div class="nombre"><strong>' + v.nombre + '</strong></div>';
                                    html += '</div></div>';
                                });
                                mdtics.find('#datalistadoabc3 #data').html(html).show('fast').siblings('div').hide('fast');
                            } else mdtics.find('#datalistadoabc3 #sindatos').show('fast').siblings('div').hide('fast');
                        }
                    });
                }).on('keyup', '#textoabc3', function(ev) {
                    if (ev.keyCode == 13) mdtics.find('.btnbuscar').trigger('click');
                }).on("mouseenter", '.panel-user', function() {
                    if (!$(this).hasClass('active')) {
                        $(this).siblings('.panel-user').removeClass('active');
                        $(this).addClass('active');
                    }
                }).on("mouseleave", '.panel-user', function() {
                    $(this).removeClass('active');
                }).on("click", '.btn-selected', function() {
                    $(this).addClass('active');
                    let pnl = $(this).closest('.panel-user');
                    let idtic = pnl.attr('data-id');
                    let idsesion = pnl.attr('data-idsesion');
                    obj.attr('data-idtic', idtic);
                    obj.attr('data-idsesion', idsesion);
                    obj.trigger("addtics");
                    mdtics.find('.cerrarmodal').trigger('click');
                })
                setTimeout(function() {
                    mdtics.find('.btnbuscar').trigger('click');
                }, 500);
            }//end  tics
            var __addEnglish = function(obj, fncall) {
                //obj.off('addEnglish');
                //var lstmp = 'addEnglish' + __idgui();
                //obj.addClass(lstmp);
                var url = _sysUrlBase_ + 'smart/english/buscar/?plt=blanco';
                var mdenglish = __sysmodal({
                    titulo: 'SmartEnglish',
                    url: url
                });
                mdenglish.on('keyup', '#textoabc3', function(ev) {
                    if (ev.keyCode == 13) mdenglish.find('.btnbuscar').trigger('click');
                }).on("mouseenter", '.panel-user', function() {
                    if (!$(this).hasClass('active')) {
                        $(this).siblings('.panel-user').removeClass('active');
                        $(this).addClass('active');
                    }
                }).on("mouseleave", '.panel-user', function() {
                    $(this).removeClass('active');
                }).on('click', '.btnbuscar', function(ev) {
                    var idfrm = mdenglish.find('form[name="frmbuscar"]').attr('id');
                    var formData = new FormData(document.getElementById(idfrm));
                    var urlpv = mdenglish.find('select#plataforma option:selected').attr('link');
                    mdenglish.find('#cargando').show();
                    mdenglish.find('#table').hide();
                    formData.append('url', urlpv);
                    __sysAyax({
                        donde: $('#plnindice'),
                        url: _sysUrlBase_ + 'getservice/englishsesiones/',
                        fromdata: formData,
                        mostrarcargando: true,
                        showmsjok: false,
                        callback: function(rs) {
                            midata = rs.data || [];
                            var html = '';
                            if (midata.length > 0) {
                                mdenglish.find('#cargando').hide();
                                mdenglish.find('#table').show();
                                var sin = -1;
                                mdenglish.find('tbody#aquicarganlassesiones').children('tr').remove();
                                $.each(midata, function(is, v) {
                                    var imgtemp = v.imagen || '';
                                    var ultimostaticp = imgtemp.lastIndexOf('static');
                                    if (ultimostaticp > 0) {
                                        imgtemp = urlpv + imgtemp.substr(ultimostaticp);
                                    } else {
                                        imgtemp = urlpv + 'static/media/nofoto.jpg';
                                    }
                                    if (v.tiporecurso == 'L') {
                                        var html = '';
                                        sin++;
                                        html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" idcursodetalle="' + v.idcursodetalle + '" idrecurso="' + v.idrecurso + '" idcurso="' + v.idcurso + '" idactividad="' + (v.idactividad || 0) + '" ><div class="pnlacciones text-center"><a class="addEnglish btn-selected btn btn-success btn-xs" title="Selected  Assessment"><i class="btnselected fa fa-hand-o-down"></i></a></div>';
                                        html += '<div class="item"><img class="img-responsive" src="' + imgtemp + '" width="100%"></div>';
                                        html += '<div class="nombre"><strong>' + v.nombre + '</strong></div>';
                                        html += '</div></div>';
                                        if (sin % 4 == 0) {
                                            mdenglish.find('tbody#aquicarganlassesiones').append('<tr><td>' + html + '</td></tr>');
                                        } else
                                            mdenglish.find('tbody#aquicarganlassesiones').children('tr').last().children('td').append(html);
                                    } else {
                                        $.each(v.hijo, function(is, vh) {

                                            var imgtemp = vh.imagen || '';
                                            var ultimostaticp = imgtemp.lastIndexOf('static');
                                            if (ultimostaticp > 0) {
                                                imgtemp = urlpv + imgtemp.substr(ultimostaticp);
                                            } else {
                                                imgtemp = urlpv + 'static/media/nofoto.jpg';
                                            }
                                            if (vh.tiporecurso == 'L') {
                                                var html = '';
                                                sin++;
                                                html += '<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" idcursodetalle="' + vh.idcursodetalle + '" idrecurso="' + vh.idrecurso + '" idcurso="' + vh.idcurso + '" idactividad="' + (vh.idactividad || 0) + '" ><div class="pnlacciones text-center"><a class="addEnglish btn-selected btn btn-success btn-xs" title="Selected  Assessment"><i class="btnselected fa fa-hand-o-down"></i></a></div>';
                                                html += '<div class="item"><img class="img-responsive" src="' + imgtemp + '" width="100%"></div>';
                                                html += '<div class="nombre"><strong>' + vh.nombre + '</strong></div>';
                                                html += '</div></div>';
                                                if (sin % 4 == 0) {
                                                    mdenglish.find('tbody#aquicarganlassesiones').append('<tr><td>' + html + '</td></tr>');
                                                } else
                                                    mdenglish.find('tbody#aquicarganlassesiones').children('tr').last().children('td').append(html);
                                            }

                                        })
                                    }

                                })
                                //md.find('table').datatable();
                            }
                        }
                    });
                }).on("click", '.addEnglish', function() {
                    $(this).addClass('active');
                    let pnl = $(this).closest('.panel-user');
                    var idcursodetalle = pnl.attr('idcursodetalle') || 0; // idactividad
                    var idrecurso = pnl.attr('idrecurso') || 0;
                    var idcurso = pnl.attr('idcurso') || 0;
                    var idactividad = pnl.attr('idactividad') || 0;
                    var urlpv = mdenglish.find('select#plataforma option:selected').attr('link');
                    var link = _sysUrlBase_ + 'smartbook/ver/?PV=' + (mdenglish.find('select#plataforma').val() || '') + '&idcursodetalle=' + idcursodetalle + "&idcurso=" + idcurso + "&idrecurso=" + idrecurso + '&idactividad=' + idactividad;
                    link = link.replace(_sysUrlBase_, '', link);
                    obj.attr('data-link', link);
                    if (__isFunction(fncall)) fncall(link);
                    __cerrarmodal(mdenglish);
                }).on('change', 'select#idcurso', function(ev) {
                    ev.preventDefault();
                    mdenglish.find('.btnbuscar').trigger('click');
                }).on('change', 'select#plataforma', function(ev) {
                    smartengliscargarcursos();
                })

                var smartengliscargarcursos = function() {
                    // var idfrm=md.find('form[name="frmbuscar"]').attr('id'); 'document.getElementById(idfrm)'
                    var formData = new FormData();
                    var urlpv = mdenglish.find('select#plataforma option:selected').attr('link');
                    formData.append('url', urlpv);
                    formData.append('plataforma', mdenglish.find('select#plataforma').val() || '');
                    //formData.append('idcurso',false);
                    formData.append('texto', mdenglish.find('input#texto').val() || '');
                    __sysAyax({
                        donde: $('#plnindice'),
                        url: _sysUrlBase_ + 'getservice/englishcursos/',
                        fromdata: formData,
                        mostrarcargando: true,
                        showmsjok: false,
                        callback: function(rs) {
                            mdenglish.find('select#idcurso').children('option').remove();
                            if (rs.data.length > 0) {
                                mdenglish.find('select#idcurso').find('option').remove();
                                $.each(rs.data, function(id, d) {
                                    mdenglish.find('select#idcurso').append('<option value="' + d.idcurso + '">' + d.nombre + '</option>');
                                })
                            }
                            mdenglish.find('.btnbuscar').trigger('click');
                        }
                    });
                }

                var timeval = setInterval(function() {
                    if (mdenglish.find('form[name="frmbuscar"]').length > 0) {
                        clearInterval(timeval);
                        smartengliscargarcursos();
                    }
                }, 1000);
            }//end addEnglish
            var __enlacesInteres = function(el, nombre, dirmedia, fncall) {
                // var _sysUrlBase_ = "../../";
                var dt = {
                    header: true,
                    titulo: "Enlaces de Interés",
                    html: $('#showpEnlacesInteres').html()
                };
                var md = __sysmodal(dt);
                var typetmp = el.data("link");

                var addOptionsMenu = function() {
                    var trvideoclone = md.find('.trvideoclone').clone();
                    var index = md.find('table tbody').find('tr').length;
                    trvideoclone.find('td').first().html(index);
                    trvideoclone.removeClass('trvideoclone').removeAttr('style');
                    md.find('.trvideoclone').before(trvideoclone);
                }

                // console.log(typetmp)

                if (typetmp != undefined) {
                    try {
                        $.post(_sysUrlBase_ + 'json/acad_cursodetalle/_leerEnlaces', {
                            'link': typetmp
                        }, function(data) {
                            if (data.code == 200) {
                                $.each(data.data, function(i, item) {
                                    var trvideoclone = md.find('.trvideoclone').clone();
                                    var index = md.find('table tbody').find('tr').length;
                                    trvideoclone.attr("data-type", "imagen").attr("data-link", "").attr("data-base64", item.img);
                                    trvideoclone.find('td').first().html(index);
                                    trvideoclone.find('.imagen-enlace').html('<img src="' + _sysUrlBase_ + item.img + '" class="btnsubirvideo img-responsive" style="max-width:200px; max-height:200px;"></video>');
                                    trvideoclone.find('input[name="titulo"]').val(item.titulo);
                                    trvideoclone.find('input[name="link"]').val(item.link);
                                    trvideoclone.removeClass('trvideoclone').removeAttr('style');
                                    md.find('.trvideoclone').before(trvideoclone);
                                });
                            }
                        }, 'json');
                    } catch (ex) {
                        console.log(ex);
                    }
                } else {
                    addOptionsMenu();
                }
                // addOptionsMenu();
                md.on('click', '.iborrar', function() {
                    $(this).closest('tr').remove();
                    md.find('tbody').find('tr').each(function(i, tr) {
                        $(tr).find('td').first().text(i + 1);
                    })
                }).on('click', '.btnaddvideo', function() {
                    addOptionsMenu();
                }).on('click', '.btnsubirvideo', function(e) {
                    e.preventDefault();
                    var el_ = $(this);
                    var $img = el_.children('i');
                    var tr = el_.closest('tr');
                    var nombre_ = 'img_enlace_interes_' + Date.now();
                    var oldmedia = $img.attr('src') || '';
                    __subirfile({
                        file: $img,
                        typefile: 'imagen',
                        uploadtmp: true,
                        guardar: true,
                        dirmedia: dirmedia,
                        'oldmedia': oldmedia,
                        'nombre': nombre_
                    }, function(rs) {
                        var tipomedia_ = 'imagen';
                        tr.attr('data-type', tipomedia_);
                        tr.attr('data-link', rs.media);
                        tr.attr('data-oldmedia', rs.media);
                        el_.closest('div').html('<img src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo img-responsive" style="max-width:200px; max-height:200px;"></video>');
                    });
                }).on('click', '.btnsavejson', function() {
                    var _jsonmultioptions = [];
                    md.find('tbody').find('tr').each(function(i, tr){
                        var $tr = $(this);
                        if (!$tr.hasClass('trvideoclone')) {
                            _jsonmultioptions.push({
                                base64: $tr.attr('data-base64') || '',
                                img: $tr.attr('data-link') || '',
                                titulo: $tr.find('input[name="titulo"]').val() || 'Sin Título',
                                link: $tr.find('input[name="link"]').val() || 'Link'
                            });
                        }
                    })
                    $.post(_sysUrlBase_ + 'json/acad_cursodetalle/_procesarEnlaces', {
                        // 'link': rs.media
                        'enlaces': JSON.stringify(_jsonmultioptions),
                        'nombre': nombre,
                        'dirmedia': dirmedia
                    }, function(data) {
                        if (data.code == 200) {
                            if (__isFunction(fncall)) fncall(data.data);
                            __cerrarmodal(md);
                        }
                    }, 'json');
                })
            }

            //smartlibrary
            var __smartlibrery = function(el, nombre, dirmedia, fncall) {
                // var _sysUrlBase_ = "../../";
                var dt = {
                    header: true,
                    titulo: "Biblioteca",
                    html: $('#showpBiblioteca').html()
                };
                var md = __sysmodal(dt);
                var typetmp = el.data("link");

                var addOptionsMenu = function() {
                    var trvideoclone = md.find('.trvideoclone').clone();
                    var index = md.find('table tbody').find('tr').length;
                    trvideoclone.find('td').first().html(index);
                    trvideoclone.removeClass('trvideoclone').removeAttr('style');
                    md.find('.trvideoclone').before(trvideoclone);
                }

                // console.log(typetmp)

                if (typetmp != undefined) {
                    try {
                        $.post(_sysUrlBase_ + 'json/acad_cursodetalle/_leerEnlaces', {
                            'link': typetmp
                        }, function(data) {
                            if (data.code == 200) {
                                $.each(data.data, function(i, item) {
                                    var trvideoclone = md.find('.trvideoclone').clone();
                                    var index = md.find('table tbody').find('tr').length;

                                    trvideoclone.attr('data-type2', item.tipo);
                                    trvideoclone.attr('data-link2', item.link);
                                    trvideoclone.find('td').first().html(index);
                                    if (item.tipo == 'video') {
                                        trvideoclone.find('.btnsubirvideo2').parent().html('<video controls="true" src="' + _sysUrlBase_ + item.link + '" class="btnsubirvideo2" style="max-width:200px; max-height:200px;"></video>');
                                    } else if (item.tipo == 'imagen') {
                                        trvideoclone.find('.btnsubirvideo2').parent().html('<img src="' + _sysUrlBase_ + item.link + '" class="btnsubirvideo2 img-responsive" style="max-width:200px; max-height:200px;"/>');
                                    } else {
                                        trvideoclone.find('.btnsubirvideo2').parent().html('<i src="' + _sysUrlBase_ + item.link + '" class="btnsubirvideo2 btn btn-danger fa fa-file-o" > ' + item.tipo + ' </i>');
                                    }

                                    trvideoclone.attr("data-type", "imagen").attr("data-link", "").attr("data-base64", item.img);
                                    // trvideoclone.find('td').first().html(index);
                                    trvideoclone.find('.imagen-enlace').html('<img src="' + _sysUrlBase_ + item.img + '" class="btnsubirvideo img-responsive" style="max-width:200px; max-height:200px;"></video>');
                                    trvideoclone.find('input[name="titulo"]').val(item.titulo);
                                    trvideoclone.find('input[name="link"]').val(item.link);
                                    trvideoclone.removeClass('trvideoclone').removeAttr('style');
                                    md.find('.trvideoclone').before(trvideoclone);
                                });
                            }
                        }, 'json');
                    } catch (ex) {
                        console.log(ex);
                    }
                } else {
                    addOptionsMenu();
                }
                // addOptionsMenu();
                md.on('click', '.iborrar', function() {
                    $(this).closest('tr').remove();
                    md.find('tbody').find('tr').each(function(i, tr) {
                        $(tr).find('td').first().text(i + 1);
                    })
                }).on('click', '.btnaddvideo', function() {
                    addOptionsMenu();
                }).on('click', '.btnsubirvideo', function(e) {
                    e.preventDefault();
                    var el_ = $(this);
                    var $img = el_.children('i');
                    var tr = el_.closest('tr');
                    var nombre_ = 'img_enlace_interes_' + Date.now();
                    var oldmedia = $img.attr('src') || '';
                    __subirfile({
                        file: $img,
                        typefile: 'imagen',
                        uploadtmp: true,
                        guardar: true,
                        dirmedia: dirmedia,
                        'oldmedia': oldmedia,
                        'nombre': nombre_
                    }, function(rs) {
                        var tipomedia_ = 'imagen';
                        tr.attr('data-type', tipomedia_);
                        tr.attr('data-link', rs.media);
                        tr.attr('data-oldmedia', rs.media);
                        el_.closest('div').html('<img src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo img-responsive" style="max-width:200px; max-height:200px;"></video>');
                    });
                }).on('click', '.btnsubirvideo2', function(e) {
                    e.preventDefault();
                    var el_ = $(this);
                    var $img = el_.children('i');
                    var tr = el_.closest('tr');

                    var nombre = 'ses_' + Date.now();
                    var oldmedia = $img.attr('src') || '';
                    __subirfile({
                        file: $img,
                        typefile: 'media',
                        uploadtmp: true,
                        guardar: true,
                        dirmedia: dirmedia,
                        'oldmedia': oldmedia,
                        'nombre': nombre
                    }, function(rs) {
                        var tipomedia_ = rs.media.lastIndexOf('.mp4?');
                        if (tipomedia_ != -1) tipomedia_ = 'video';
                        else if (rs.media.lastIndexOf('.pdf?') != -1)
                            tipomedia_ = 'pdf';
                        else if (rs.media.lastIndexOf('.mp3?') != -1)
                            tipomedia_ = 'audio';
                        else if (rs.media.lastIndexOf('.html?') != -1)
                            tipomedia_ = 'html';
                        else tipomedia_ = 'imagen';
                        tr.attr('data-type2', tipomedia_);
                        tr.attr('data-link2', rs.media);
                        tr.attr('data-oldmedia2', rs.media);
                        if (tipomedia_ == 'video') {
                            el_.closest('div').html('<video controls="true" src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo2" style="max-width:200px; max-height:200px;"></video>');
                        } else if (tipomedia_ == 'imagen') {
                            el_.closest('div').html('<img src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo2 img-responsive" style="max-width:200px; max-height:200px;"></video>');
                        } else {
                            el_.closest('div').html('<i src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo2 btn btn-danger fa fa-file-o" > ' + tipomedia_ + ' </i>');
                        }
                    });
                }).on('click', '.btnsavejson', function() {
                    var _jsonmultioptions = [];
                    md.find('tbody').find('tr').each(function(i, tr) {
                        var $tr = $(this);
                        if (!$tr.hasClass('trvideoclone')) {
                            _jsonmultioptions.push({
                                base64: $tr.attr('data-base64') || '',
                                img: $tr.attr('data-link') || '',
                                titulo: $tr.find('input[name="titulo"]').val() || 'Sin Título',
                                tipo: $tr.attr('data-type2') || '',
                                link: $tr.attr('data-link2') || ''
                            });
                        }
                    })
                    $.post(_sysUrlBase_ + 'json/acad_cursodetalle/_procesarLibrary', {
                        // 'link': rs.media
                        'enlaces': JSON.stringify(_jsonmultioptions),
                        'nombre': nombre,
                        'dirmedia': dirmedia
                    }, function(data) {
                        if (data.code == 200) {
                            if (__isFunction(fncall)) fncall(data.data);
                            __cerrarmodal(md);
                        }
                    }, 'json');
                })
            }//end smartlibrary

            var __asiganacionfile = function(it) {
                it.siblings('.cicon').removeClass('active').children('i').removeClass('btn-success btn-primary').addClass('btn-primary');
                it.addClass('active').children('i').addClass('btn-success');
                var _li = elli;
                var link_ = (it.attr('data-link') || '');
                var indexlink_ = link_.lastIndexOf('static/');
                if (indexlink_ > -1) link_ = link_.substring(indexlink_);
                if (_li.hasClass('esmenu')) {
                    _li.attr('txtjson-typelink', it.attr('data-type'));
                    _li.attr('txtjson-link', link_);
                    _li.attr('txtjson-tipo', '#showpaddcontenido');
                } else {
                    _li.attr('type', it.attr('data-type'))
                    _li.attr('link', link_)
                    _li.attr('color', "transparent")
                    li.attr('txtjson-tipo', '#showpaddopciones');
                }
                actulaizarjson(li);
                __ocultarsinoescalificado(_li);
                __cerrarmodal(md);
            } //end __asiganacionfile

            var __addRecursocolaborativo=function(obj,fncall){                
                var type=obj.attr('data-acc');
                let idrecurso=obj.attr('data-link')||'';
                let html=$('#showrecursocolaborativo').clone();
                var mdColaborativo = __sysmodal({
                    titulo: obj.find('span').text(),
                    html: html.html(),
                    footer:true,
                    tam:'xl'
                });
                //mdColaborativo.find('#tablalistadorecurso').hide(); // Buscar recurso creado
                mdColaborativo.find('#pnlcrearrecurso1').fadeIn(100); // Formulario crear curso
                mdColaborativo.find('#asignarestudiantes').hide(); //Asingar estudiantes
                let idtxt='txt'+__idgui();
                if(idrecurso!=''){
                    __listarrecursocolaborativo(true,idrecurso).then(rs=>{
                        if(rs.idrecurso!=undefined&&rs.idrecurso!=null){
                            mdColaborativo.find('#inicio').hide();  
                            let frm=mdColaborativo.find('#pnlcrearrecurso1');                          
                            frm.find('textarea[name="descripcion"]').val(rs.descripcion).attr('id',idtxt);
                            frm.find('input[name="tipo"]').val(type.substring(0,1).toUpperCase()); 
                            frm.find('input[name="idrecurso"]').val(rs.idrecurso); 
                            frm.find('input[name="titulo"]').val(rs.titulo);  
                            mdColaborativo.find('#asignarestudiantes input#nporgrupo').val(rs.ngrupos||1);
                            mdColaborativo.find('#asignarestudiantes select#definirTema').val(rs.temasxgrupo=='S'?'si':'');
                            __listarrecursocolaborativoasignados(true,rs.idrecurso).then(rsalu=>{
                                alumnosxgrupo=rsalu; 
                                _recreargrupos();                             
                            })                           
                        }
                    })
                }else{
                    let frm=mdColaborativo.find('#pnlcrearrecurso1');
                    frm.find('input[name="tipo"]').val(type.substring(0,1).toUpperCase()); 
                    frm.find('input[name="idrecurso"]').val('');
                    frm.find('input[name="titulo"]').val('');
                    frm.find('textarea[name="descripcion"]').val('').attr('id',idtxt);
                }
                setTimeout(function(){__initNewEditor(idtxt,'');},500);

                mdColaborativo.find('#pnlcrearrecurso1')
                .on('submit','#frmcrearrecurso',function(ev){
                    ev.preventDefault();
                    Swal.showLoading();
                    let frm=$(this);
                    $.post(_sysUrlBase_ + 'json/recursos_colaborativos/guardar',{
                            'titulo': frm.find('input[name="titulo"]').val()||'',
                            'descripcion': frm.find('textarea[name="descripcion"]').val()||'',
                            'file': frm.find('input[name="file"]').val()||'',
                            'tipo': frm.find('input[name="tipo"]').val()||'F',
                            'estado': frm.find('input[name="estado"]').val()||'1',
                            'idrecurso': frm.find('input[name="idrecurso"]').val()||'',
                        }, function(data){
                             Swal.close();
                            if (data.code == 200){
                                frm.find('input[name="idrecurso"]').val(data.newid);
                                mdColaborativo.find('#inicio').hide();
                                mdColaborativo.find('#tablalistadorecurso').hide();
                                mdColaborativo.find('#pnlcrearrecurso1').fadeIn(100);
                                mdColaborativo.find('#pnlcrearrecurso1 .pnlaccion').fadeIn(100);
                                frm.find('div#recursoguardado').fadeIn(100);
                                mdColaborativo.find('#asignarestudiantes').fadeIn(100);
                                obj.attr('data-link', data.newid);
                                if (__isFunction(fncall)) fncall(data.newid);
                            }else{
                                //mensaje de Error
                            }
                        }, 'json');
                    return;
                }).on('click','.btncancelar',function(ev){
                    ev.preventDefault();
                    __cerrarmodal(mdColaborativo);
                }).on('click','.btnAsignarestudiantes',function(ev){
                    mdColaborativo.find('#inicio').hide();
                    mdColaborativo.find('#tablalistadorecurso').hide();
                    mdColaborativo.find('#pnlcrearrecurso1').fadeIn(100);
                    mdColaborativo.find('#pnlcrearrecurso1 .pnlaccion').hide();
                    mdColaborativo.find('#asignarestudiantes').fadeIn(100);
                })

                _listargruposdeestecurso(false).then(rs_gruposdeestudio=>{
                    let select=mdColaborativo.find('#asignarestudiantes select#idgrupoauladetalle');
                    $.each(rs_gruposdeestudio,function(i,v){
                        let htmloption=`<option value="${v.idgrupoauladetalle}" fecha_final="${v.fecha_final}" idcurso="${v.idcurso}" idcomplementario="${v.idcomplementario}">${v.strgrupoaula}</option>`;
                        select.append(htmloption);
                    })
                    select.trigger('change');
                })


                var allalumnodelgrupoauladetalle=[]; //todo el listado de alumnos
                var alumnosxgrupo=[]; //alumnos x grupo
                var alumnosantiguos=[]; //alumnos  para eliminar
                var _recreargrupos=function(){
                    let ngrupos=mdColaborativo.find('#asignarestudiantes input#nporgrupo').val();
                    var definirTema=mdColaborativo.find('#asignarestudiantes select#definirTema').val();
                    let idguittmp=__idgui();
                    let idnestable='nestable'+idguittmp;
                    let htmlgrupos=``;
                    var grupos=[];
                    let allalumno=allalumnodelgrupoauladetalle;
                    var excesodealumno=[];
                    alumnosantiguos=[];
                    if(allalumnodelgrupoauladetalle.length>0){                       
                        var nalumnoxgrupo=Math.ceil(allalumnodelgrupoauladetalle.length/ngrupos);
                        var residuonalumnoxgrupo=allalumnodelgrupoauladetalle.length%ngrupos;
                        let allalumno2=allalumno;                        
                        //crear todos los grupos.
                        //ver cuantos gruposexsisten
                        let ngruposactuales=0;
                        if(alumnosxgrupo.length>0)
                        $.each(alumnosxgrupo,function(i,gg){
                            let estaagregado=grupos.find(gr=>gr.idgrupo=='g'+gg.grupo);
                            if(estaagregado==undefined){                                                           
                                grupos.push({
                                    idgrupo:'g'+gg.grupo,
                                    nombre:gg.grupo,
                                    idrecurso:gg.idrecurso,
                                    alumnos:[],
                                    tema:gg.tema,
                                    titulo:gg.titulo,

                                });
                            }
                        })                        
                        for(var i=grupos.length+1;i<=ngrupos;i++){
                            grupos.push({
                                idgrupo:'sg'+i,
                                nombre:i,
                                alumnos:[],
                                tema:'',
                                titulo:'',
                            })
                        }//total de grupos preparados

                        //merge entre alumnosasignados y alumnosmatriculados
                        if(allalumno2.length)
                            $.each(allalumno2,function(i,al){
                                let alumnoasignado=alumnosxgrupo.find(alu=>alu.idalumno==al.idalumno);
                                if(alumnoasignado!=undefined){
                                    al.idcursodetalle=alumnoasignado.idcursodetalle;
                                    al.idgrupoauladetalle=alumnoasignado.idgrupoauladetalle;
                                    al.idpestania=alumnoasignado.idpestania;
                                    al.idrecursoasignacion=alumnoasignado.idrecursoasignacion;
                                    al.grupo=alumnoasignado.grupo;
                                    al.idrecurso=alumnoasignado.idrecurso;
                                    al.respuestas=alumnoasignado.respuestas;
                                    allalumno2[i]=al;
                                    alumnosxgrupo= alumnosxgrupo.filter(function(car){
                                        return car.idalumno !== al.idalumno; 
                                    });
                                }
                            })
                        alumnosantiguos=alumnosxgrupo; // alumnos que  ya no pertenecen a este grupoauladetalle
                        $.each(grupos,function(i,g){
                            $.each(allalumno2,function(j,a){
                                if('g'+a.grupo==g.idgrupo && g.alumnos.length<nalumnoxgrupo){
                                    g.alumnos.push(a);
                                    grupos[i]=g;
                                    allalumno2= allalumno2.filter(function(aa) {
                                        return aa.idalumno !== a.idalumno; 
                                    });
                                }
                            })
                        })
                       
                        $.each(allalumno2,function(ia,a){                            
                            $.each(grupos,function(i,g){
                                if(g.alumnos.length < nalumnoxgrupo){
                                    g.alumnos.push(a);
                                    grupos[i]=g;                                    
                                }
                            })
                        })                        
                        $.each(grupos,function(i,g){
                            htmlgrupos+=`
                            <li class="dd-item iscontainer" data-id="${i+1}" style="background-color:#${i%2==0?'e8e7e7':'dedcdc'};">
                                <i class=" btn btn-xs fa fa-arrows dd-handle" title="Move" style="cursor: move; display: inline-block; margin-top: 0px; padding-top: 1.5ex;"></i>
                                <span class="dd-content"> GRUPO : <span class="nombregrupo">${g.nombre}</span></span>`;
                                if(definirTema=='si'){
                                htmlgrupos+=`
                                    <div class="dd-tema">
                                        <textarea class="" id="textarea_${i+1}_${idguittmp}" placeholder="Tema a tratar">${g.tema||''}</textarea>
                                    </div>`;
                                }
                            htmlgrupos+=`
                                <ol class="dd-list">`;
                                $.each(g.alumnos,function(ia,alu){
                                    htmlgrupos+=`
                                    <li class="dd-item" idalumno="${alu.idalumno}" idasignacion="${alu.idrecursoasignacion||''}" >
                                        <i class="fa fa-user dd-handle" title="Move" style="background-color: transparent; border: 0px; cursor: move; display: inline-block; margin-top: 0px; padding-top: 1.5ex; color: #b54444;"></i>
                                        <span class="dd-content"> ${alu.stralumno}</span>
                                    </li>`;

                                })                                         
                            htmlgrupos+=`
                                </ol>
                            </li>`
                        })

                   let html=`
                    <div class="dd" id="${idnestable}" style="max-width:100%">
                        <ol class="dd-list">${htmlgrupos}</ol>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <hr>
                            <button class="btn btn-primary btnguardargrupos" type="submit"><i class=" fa fa-save"></i> Guardar</button>
                            <button class="btn btn-warning btncancelar" ><i class=" fa fa-refresh"></i> Cancelar</button>
                        </div>
                    </div>`;
                        
                        mdColaborativo.find('#pnlcrearrecurso1 .pnlaccion').hide();
                        mdColaborativo.find('#asignarestudiantes').fadeIn(100);
                        mdColaborativo.find('#listadodegrupos').html(html)
                        .on('click','.btnguardargrupos',function(ev){
                            let tmp_gruposhtml=mdColaborativo.find('#listadodegrupos').children('.dd').children('ol');
                            let grupos=[];
                            tmp_gruposhtml.children('li').each(function(i,gr){
                                let $gr=$(gr);
                                let alumnos=[];
                                $gr.children('ol').children('li').each(function(ii,alu){
                                   alumnos.push({idalumno:parseInt($(alu).attr('idalumno')),idrecursoasignacion:parseInt($(alu).attr('idasignacion')||'')});
                                })                                 
                                grupos[i]={
                                    id:i,
                                    nombre:$gr.children('span.dd-content').children('.nombregrupo').text(),
                                    tema:$gr.children('div.dd-tema').children('textarea').val()||'',
                                    alumnos:alumnos
                                }
                            })

                            ev.preventDefault();
                            Swal.showLoading();
                            let frm=mdColaborativo.find('#pnlcrearrecurso1');
                            let idgrupoauladetalle=__oCurso.idgrupoauladetalle||''
                            if(idgrupoauladetalle==''||idgrupoauladetalle==-1||idgrupoauladetalle=='0')
                                idgrupoauladetalle=mdColaborativo.find('select#idgrupoauladetalle').val()||'';
                            $.post(_sysUrlBase_ + 'json/recursos_colaborativosasignacion/guardarasignaciones',{
                                    'titulo': frm.find('input[name="titulo"]').val()||'',
                                    'descripcion': frm.find('textarea[name="descripcion"]').val()||'',
                                    'file': frm.find('input[name="file"]').val()||'',
                                    'tipo': frm.find('input[name="tipo"]').val()||'F',
                                    'estado': frm.find('input[name="estado"]').val()||'1',
                                    'idrecurso': frm.find('input[name="idrecurso"]').val(),
                                    'grupos': JSON.stringify(grupos),
                                    'idgrupoauladetalle': idgrupoauladetalle||'',
                                    'idcursodetalle': idcursodetalle,
                                    'idpestania':elli.attr('idpestania')||'0',
                                    'definirTema':mdColaborativo.find('#asignarestudiantes select[name="definirTema"]').val(),
                                    'ngrupos':grupos.length,
                                    'borraruser':JSON.stringify(alumnosantiguos),
                                    'idcurso':__oCurso.idcurso,
                                    'idcc': __oCurso.idcomplementario ,
                                    'strcurso':__oCurso.nombre                                 
                                }, function(data){
                                     Swal.close();
                                    if(data.code == 200){
                                        frm.find('input[name="idrecurso"]').val(data.newid.idrecurso);
                                        mdColaborativo.find('#inicio').hide();
                                        mdColaborativo.find('#tablalistadorecurso').hide();
                                        mdColaborativo.find('#pnlcrearrecurso1').fadeIn(100);
                                        mdColaborativo.find('#pnlcrearrecurso1 .pnlaccion').hide();
                                        frm.find('div#recursoguardado').fadeIn(100);
                                        if(data.newid.asignaciones.length){
                                            mdColaborativo.find('#listadodegrupos').find('li[idalumno]').each(function(ia,al){
                                                let idalumno=$(this).attr('idalumno');
                                                let alu=data.newid.asignaciones.find(aa=>aa.idalumno==idalumno);
                                                $(this).attr('idasignacion',alu.idrecursoasignacion)
                                            })
                                        }else{
                                            Swal.fire({
                                                type:'warning',
                                                icon: 'warning',
                                                title: 'Información',
                                                text: 'Se registró correctamente, pero no se realizaron asignaciones de estudiantes',                               
                                            })
                                        }
                                        obj.attr('data-link', data.newid.idrecurso);
                                        if (__isFunction(fncall)) fncall(data.newid.idrecurso);
                                        __cerrarmodal(mdColaborativo);
                                    }else{
                                        Swal.fire({
                                            type:'warning',
                                            icon: 'warning',
                                            title: 'Oops...',
                                            text: 'Error  al asignar estudiantes ',                               
                                        })
                                        //mensaje de Error
                                    }
                                }, 'json');
                            return;
                        }).on('click','.btncancelar',function(ev){
                            ev.preventDefault();
                            __cerrarmodal(mdColaborativo);                           
                        })
                        if(definirTema=='si'){
                            for(var i=1;i<=ngrupos;i++){
                                __initNewEditor('textarea_'+i+'_'+idguittmp,'');
                            }
                        }
                        mdColaborativo.find('#listadodegrupos').find('#'+idnestable).nestable({
                            onDragStart: function (l, e){},
                            beforeDragStop: function(l,e, p){
                                let li=p.parent('li');
                                if(li.hasClass('iscontainer')&&e.hasClass('iscontainer')) {
                                    Swal.fire({
                                        type:'warning',
                                        icon: 'warning',
                                        title: 'Oops...',
                                        text: 'Los grupos no pueden tener sub grupos',                               
                                        })
                                    return false;
                                };
                                if((li.length>0 && li.hasClass('iscontainer'))||(li.length==0 && e.hasClass('iscontainer'))){
                                    return true;
                                }else{
                                    Swal.fire({
                                        type:'warning',
                                        icon: 'warning',
                                        title: 'Oops...',
                                        text: 'Los Estudiantes deben pertencer a un grupo',                               
                                        }) 
                                    return false;
                                }                       
                            }
                        })
                    }else{
                        html=`<div class="alert alert-danger"><h3 class="text-center">Lo sentimos Este grupo de estudios no tiene Estudiantes Matriculados. </h3></div>`;
                        mdColaborativo.find('#listadodegrupos').html(html);
                    }
                }

                mdColaborativo.find('#asignarestudiantes')
                .on('change','select#idgrupoauladetalle',function(ev){
                    let idgrupoauladetalle=$(this).val();
                    __listadoestudiantesdelgrupo(false,idgrupoauladetalle).then(rs_estudiantes=>{
                        allalumnodelgrupoauladetalle=rs_estudiantes;
                        mdColaborativo.find('select#definirTema').trigger('change');
                    })
                }).on('change','input#nporgrupo',function(ev){
                    mdColaborativo.find('select#definirTema').trigger('change');
                }).on('change','select#definirTema',function(ev){
                    let definirtema=$(this).val();
                    _recreargrupos();
                })
            }
            var _li_ = el.closest('li');
            var link = '';
            if (_li_.hasClass('esmenu')) {
                link = (_li_.attr('txtjson-link') || '');
                var indexlink = link.lastIndexOf('static/');
                if (indexlink > -1) link = _sysUrlBase_ + link.substring(indexlink);
                md.find('.cicon[data-type="' + _li_.attr('txtjson-typelink') + '"]').children('i').addClass('btn-success').removeClass('btn-primary');
                md.find('.cicon[data-type="' + _li_.attr('txtjson-typelink') + '"]').attr('data-link', link);
            } else {
                link = _li_.attr('link') || '';
                var indexlink = link.lastIndexOf('static/');
                if (indexlink > -1) link = _sysUrlBase_ + link.substring(indexlink);
                md.find('.cicon[data-type="' + _li_.attr('type') + '"]').children('i').addClass('btn-success').removeClass('btn-primary');
                md.find('.cicon[data-type="' + _li_.attr('type') + '"]').attr('data-link', link);
            }
            md.on('click', ' .cicon', function(ev) {
                ev.preventDefault();
                var it = $(this);
                var type = it.attr('data-type') || '';
                var datalink = it.attr('data-link') || '';
                var acc = it.attr('data-acc') || '';
                el = eltmp_;
                // console.log('cion', el);
                //if(type===''||type==datatype) return false;
                it.attr('data-oldmedia', datalink)
                it.siblings().removeAttr('data-oldmedia').children('i.btn-success').removeClass('btn-success').addClass('btn-primary');
                var _cont = $('#bookplantilla').children('#contentpages');
                var oldmedia = it.closest('.row').find('.verpopover.btn-success').attr('data-link');
                var nombre = "file_" + idcursodetalle + '_' + Date.now();

                <?php if ($this->idcomplementario != 0) { ?>
                    var dirmedia = 'cursos_c/c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/ses_' + idcursodetalle + "/";
                <?php } else {  ?>
                    var dirmedia = 'cursos/curso_' + idcurso + '/ses_' + idcursodetalle + "/"; // var dirmedia_ = 'cursos/' + dirmedia;
                <?php } ?> // var dirmedia = 'cursos/curso_' + idcurso + '/ses_' + idcursodetalle + "/";

                switch (type) {
                    case 'smartenglish':
                        __addEnglish(it, function(link) {
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', link);
                            it.attr('data-type', 'smartenglish');
                            it.attr('data-oldmedia', link);
                            __asiganacionfile(it);
                        });
                        break;
                    case 'smarticlock':
                        __addtics(it, 1, function(link) {
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', link);
                            it.attr('data-type', 'smarticlock');
                            it.attr('data-oldmedia', link);
                            __asiganacionfile(it);
                        });
                        break;
                    case 'smarticlookPractice':
                        __addtics(it, 2, function(link) {
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', link);
                            it.attr('data-type', 'smarticlookPractice');
                            it.attr('data-oldmedia', link);
                            __asiganacionfile(it);
                        });
                        break;
                    case 'smarticDobyyourselft':
                        __addtics(it, 3, function(link) {
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', link);
                            it.attr('data-type', 'smarticDobyyourselft');
                            it.attr('data-oldmedia', link);
                            __asiganacionfile(it);
                        });
                        break;
                    case 'game':
                        __addgame(it, function(link) {
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', link);
                            it.attr('data-type', 'game');
                            it.attr('data-oldmedia', link);
                            __asiganacionfile(it);
                        });
                        break;
                    case 'smartquiz':
                        __addexamen(it, function(link) {
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', link);
                            it.attr('data-type', 'smartquiz');
                            it.attr('data-oldmedia', link);
                            __asiganacionfile(it);
                        });
                        break;
                    case 'enlacesinteres':
                        __enlacesInteres(it, nombre, dirmedia, function(link) {
                            it.attr('data-link', link);
                            it.attr('data-type', 'enlacesinteres');
                            it.attr('data-oldmedia', link);
                            __asiganacionfile(it);
                        });
                        break;
                    case 'pdf':
                        __subirfile({
                                file: it.children('i'),
                                typefile: 'documento',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            },
                            //__subirfile({ file:it.children('i'), typefile:'pdf',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
                            function(rs) {
                                it.attr('data-type', 'pdf');
                                it.attr('data-link', rs.media);
                                it.attr('data-oldmedia', rs.media);
                                __asiganacionfile(it);
                            });
                        break;
                    case 'html':
                        __subirfile({
                                file: it.children('i'),
                                typefile: 'html',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            }, //
                            //__subirfile({ file:it.children('i'), typefile:'html',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
                            function(rs) {
                                it.attr('data-type', 'html');
                                it.attr('data-link', rs.media);
                                it.attr('data-oldmedia', rs.media);
                                __asiganacionfile(it);
                            });
                        break;
                    case 'flash':
                        __subirfile({
                                file: it.children('i'),
                                typefile: 'flash',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            },
                            //__subirfile({ file:it.children('i'), typefile:'flash',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
                            function(rs) {
                                it.attr('data-type', 'flash');
                                it.attr('data-link', rs.media);
                                it.attr('data-oldmedia', rs.media);
                                __asiganacionfile(it);
                            });
                        break;
                    case 'audio':
                        __subirfile({
                                file: it.children('i'),
                                typefile: 'audio',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            },
                            //__subirfile({ file:it.children('i'), typefile:'audio',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
                            function(rs) {
                                it.attr('data-type', 'audio');
                                it.attr('data-link', rs.media);
                                it.attr('data-oldmedia', rs.media);
                                __asiganacionfile(it);
                            });
                        break;
                    case 'video':
                        // oCC_Vimeo.getEstructuraCurso().then(rs => {
                        //     console.log('rs', rs);
                        // })
                        // console.log('video menu, pestaña');
                        // console.log('it', it);
                        __subirfile({
                                file: it.children('i'),
                                typefile: 'video',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            },
                            //__subirfile({ file:it.children('i'), typefile:'video',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
                            function(rs) {
                                it.attr('data-type', 'video');
                                it.attr('data-link', rs.media);
                                it.attr('data-oldmedia', rs.media);
                                __asiganacionfile(it);
                            });
                        break;
                    case 'imagen':
                        __subirfile({
                                file: it.children('i'),
                                typefile: 'imagen',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            },
                            function(rs) {
                                it.attr('data-type', 'imagen');
                                it.attr('data-link', rs.media);
                                it.attr('data-oldmedia', rs.media);
                                __asiganacionfile(it);
                            });
                        break;
                    case 'texto':
                        it.children('i').addClass('btn-success').removeClass('btn-primary');
                        it.attr('data-idinfotmp', 'txt' + __idgui());
                        break;
                    case 'menuvaloracion':
                        it.children('i').addClass('btn-success').removeClass('btn-primary');
                        it.attr('data-type', 'menuvaloracion');
                        it.attr('data-link', 'menuvaloracion');
                        __asiganacionfile(it);
                        break;
                    case 'foro':
                        __addRecursocolaborativo(it,function(idrecurso){
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', idrecurso);
                            it.attr('data-type', 'foro');                           
                            __asiganacionfile(it);
                        });

                        /*let idForo = $("#idForo");
                        function getForosAula() {

                            return new Promise((resolve, reject) => {
                                // console.log("id curso: ", idcurso)
                                const datos = new FormData();
                                datos.append('idPersona', IDPERSONA);
                                datos.append('idComplentario', IDCOMPLEMENTARIO);
                                datos.append('idCurso', idcurso);
                                $url = `${_sysUrlBase_}json/foros/getForoAula`;
                                fetch($url, {
                                        method: 'POST',
                                        body: datos
                                    })
                                    .then(response => response.json())
                                    .then(respuestaJson => {
                                        resolve({
                                            data: respuestaJson.data
                                        })
                                    })
                                    .catch(e => {
                                        //console.warn(e)
                                        reject(e);
                                    });
                            });
                        }

                        getForosAula().then(response => {
                            let aforos = response.data;

                            $('#idForo option[value!="0"]').remove();
                            for (var i = 0; i < aforos.length; i++) {
                                idForo.append("<option value=" + aforos[i]["idpublicacion"] + ">" + aforos[i]["titulo"] + "</option>");
                            }
                            var dt = {
                                html: $('#showforos').html(),
                                titulo: 'Agregar foro'
                            };
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            var md2 = __sysmodal(dt);
                            try {
                                if (datalink != '') {
                                    let idPubliForo = JSON.parse(atob(datalink));
                                    md2.find('#idForo').val(idPubliForo.idPublicacionForo);
                                }
                            } catch (ex) {
                                console.log(ex);
                            }
                            md2.on('click', '.btnsavejson', function() {

                                it.attr('data-type', 'foro');

                                const idPublicacionForo = md2.find('#idForo').val();

                                if (idPublicacionForo == 0) {
                                    Swal.fire("", "debe seleccionar un foro", "warning");
                                } else {
                                    let notaMax = 20;
                                    let notaMin = 0;
                                    let tipoNota = 'N';
                                    if (configuracion_nota.hasOwnProperty('maxcal')) {
                                        notaMax = configuracion_nota.maxcal == false || configuracion_nota.maxcal == null || configuracion_nota.maxcal == undefined ? 20 : configuracion_nota.maxcal;
                                    }
                                    if (configuracion_nota.hasOwnProperty('mincal')) {
                                        notaMin = configuracion_nota.mincal == false || configuracion_nota.mincal == null || configuracion_nota.mincal == undefined ? 0 : configuracion_nota.mincal;
                                    }
                                    if (configuracion_nota.hasOwnProperty('tipocalificacion')) {
                                        tipoNota = configuracion_nota.tipocalificacion == false || configuracion_nota.tipocalificacion == null || configuracion_nota.tipocalificacion == undefined ? 'N' : configuracion_nota.tipocalificacion;
                                    }

                                    let envioDatos = {
                                        tipoCalif: tipoNota,
                                        confNotaMin: notaMin,
                                        confNotaMax: notaMax,
                                        idPublicacionForo: idPublicacionForo
                                    }

                                    // console.log("envioDatos: ", envioDatos);
                                    __jsonmultioptions = envioDatos;
                                    __jsonmultioptions = JSON.stringify(__jsonmultioptions);
                                    __jsonmultioptions = btoa(__jsonmultioptions);
                                    it.attr('data-link', __jsonmultioptions);
                                    __cerrarmodal(md2);
                                    __asiganacionfile(it);
                                }

                            })
                        });*/
                        break;
                    case 'wiki':
                        __addRecursocolaborativo(it,function(idrecurso){
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', idrecurso);
                            it.attr('data-type', 'wiki');                           
                            __asiganacionfile(it);
                        });
                        break;
                    case 'webquest':
                        __addRecursocolaborativo(it,function(idrecurso){
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', idrecurso);
                            it.attr('data-type', 'webquest');                           
                            __asiganacionfile(it);
                        });
                        break;
                    case 'enlacecolaborativo':
                        __addRecursocolaborativo(it,function(idrecurso){
                            it.children('i').addClass('btn-success').removeClass('btn-primary');
                            it.attr('data-link', idrecurso);
                            it.attr('data-type', 'enlacecolaborativo');                           
                            __asiganacionfile(it);
                        });
                        break;
                    case 'zoom':
                        it.children('i').addClass('btn-success').removeClass('btn-primary');
                        it.attr('data-type', 'zoom');
                        it.attr('data-link', 'zoom');
                        // it.attr('color', "transparent");
                        __asiganacionfile(it);
                        break;
                    /*case 'enlacecolaborativo':
                        it.children('i').addClass('btn-success').removeClass('btn-primary');
                        it.attr('data-type', 'enlacecolaborativo');
                        it.attr('data-link', 'enlacecolaborativo');
                        // it.attr('color', "transparent");
                        __asiganacionfile(it);
                        break;*/
                    case 'enlaceyoutube':
                        var el = _li_;
                        var dt = {
                            html: $('#showEnlazarYoutube').html()
                        };
                        var md2 = __sysmodal(dt);
                        var tmplink = it.attr('data-link') || '';
                        if (tmplink != '') {
                            var tmplink = 'https://www.youtube.com/watch?v=' + tmplink;
                            md2.find('input#txtenlacevideo').val(tmplink);
                        }
                        var li_ = _li_;
                        md2.on('click', '.btnsaveenlace', function() {
                            it.attr('data-type', 'enlaceyoutube');
                            var enlace = md2.find('#txtenlacevideo').val().split("v=")[1];
                            link_ = enlace.split("&")[0];
                            it.attr('data-link', link_);
                            __cerrarmodal(md2);

                            __asiganacionfile(it);
                        })
                        break;
                    case 'enlacevimeo':
                    {
                        var el = _li_;
                        var dt = {
                            html: $('#showEnlazarVimeo').html()
                        };
                        var md2 = __sysmodal(dt);
                        var tmplink = it.attr('data-link') || '';
                        if (tmplink != '') {
                            // var tmplink = 'https://www.youtube.com/watch?v=' + tmplink;
                            console.log("tmplink",tmplink)
                            md2.find('input#txtenlacevideovimeo').val(tmplink);
                        }
                        var li_ = _li_;
                        md2.on('click', '.btnsaveenlace-vimeo', function() {
                            it.attr('data-type', 'enlacevimeo');
                            var enlace = md2.find('#txtenlacevideovimeo').val();
                            it.attr('data-link', enlace);
                            __cerrarmodal(md2);

                            __asiganacionfile(it);
                        })
                        
                    }break;
                    case 'tabladeaudios':
                        var dt = {
                            html: $('#showtabladeaudios').html()
                        };
                        var md2 = __sysmodal(dt);
                        try {
                            if (datalink != '') {
                                var link_ = atob(datalink);
                                link_ = JSON.parse(link_);
                                if (link_.titulo != '' && link_.titulo != undefined)
                                    md2.find('input.titulo').val(link_.titulo);
                                if (link_.menu != undefined)
                                    if (link_.menu.length)
                                        $.each(link_.menu, function(i, v) {
                                            var trclone = md2.find('.trclone').clone();
                                            var index = md2.find('table tbody').find('tr').length;
                                            trclone.attr('data-type', v.tipo);
                                            trclone.attr('data-link', v.link);
                                            trclone.find('td').first().html(index);
                                            trclone.find('input[name="nom"]').val(v.nombre);
                                            if (v.tipo == 'audio') {
                                                trclone.find('.btnsubir').parent().html('<audio controls="true" src="' + _sysUrlBase_ + v.link + '" class="btnsubir" style="max-width:100px;"></audio>');
                                            }
                                            trclone.removeClass('trclone').removeAttr('style');
                                            md2.find('.trclone').before(trclone);
                                        })
                            }
                        } catch (ex) {
                            console.log(ex);
                        }
                        md2.on('click', '.imove', function() {
                            var tr = $(this).closest('tr');
                            var adonde = $(this).attr('adonde');
                            if (adonde == 'up') {
                                if (!tr.prev().hasClass('trclone')) tr.prev().before(tr);
                            } else {
                                if (!tr.next().hasClass('trclone')) tr.next().after(tr);
                            }
                            md2.find('tbody').find('tr').each(function(i, tr) {
                                $(tr).find('td').first().text(i + 1);
                            })
                        }).on('click', '.iborrar', function() {
                            $(this).closest('tr').remove();
                            md2.find('tbody').find('tr').each(function(i, tr) {
                                $(tr).find('td').first().text(i + 1);
                            })
                        }).on('click', '.btnadd', function() {
                            var trclone = md2.find('.trclone').clone();
                            var index = md2.find('table tbody').find('tr').length;
                            trclone.find('td').first().html(index);
                            trclone.removeClass('trclone').removeAttr('style');
                            md2.find('.trclone').before(trclone);
                        }).on('click', '.btnsubir', function(e) {
                            e.preventDefault();
                            var el_ = $(this);
                            var $img = el_.children('i');
                            var tr = el_.closest('tr');
                            <?php if ($this->idcomplementario != 0) {   ?>
                                var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : 'c_c/');
                                dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
                            <?php } else {   ?>
                                var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
                                dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
                            <?php }  ?>
                            var nombre = 'ses_' + Date.now();
                            var oldmedia = $img.attr('src') || '';
                            __subirfile({
                                file: $img,
                                typefile: 'audio',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            }, function(rs) {
                                tr.attr('data-type', 'audio');
                                tr.attr('data-link', rs.media);
                                tr.attr('data-oldmedia', rs.media);
                                el_.closest('div').html('<audio controls="true" src="' + _sysUrlBase_ + rs.media + '" class="btnsubir" style="max-width:100px;"></audio>');
                            });
                        }).on('click', '.btnsavejson', function() {
                            it.attr('data-type', 'tabladeaudios');
                            var _jsonmultioptions = [];
                            md2.find('tbody').find('tr').each(function(i, tr) {
                                var $tr = $(this);
                                if (!$tr.hasClass('trclone'))
                                    _jsonmultioptions.push({
                                        nombre: $tr.find('input[name="nom"]').val() || 'audio',
                                        tipo: $tr.attr('data-type') || '',
                                        link: $tr.attr('data-link') || ''
                                    });
                            })

                            var __jsonmultioptions = {
                                titulo: (md2.find('input.titulo').val() || ''),
                                menu: _jsonmultioptions
                            };
                            __jsonmultioptions = JSON.stringify(__jsonmultioptions);
                            __jsonmultioptions = btoa(__jsonmultioptions);
                            it.attr('data-link', __jsonmultioptions);
                            __cerrarmodal(md2);
                            __asiganacionfile(it);
                        })
                        break;
                    case 'multioptions':
                        var dt = {
                            html: $('#showpaddmultivideos').html()
                        };
                        var md2 = __sysmodal(dt);
                        try{
                            if (datalink != '') {
                                var link_ = atob(datalink);
                                link_ = JSON.parse(link_);
                                md2.find('select').val(link_.mostrarcomo || 'slider')
                                var __menus = link_.menus || [];
                                if (__menus.length > 0)
                                    $.each(__menus, function(i, v) {
                                        var trvideoclone = md2.find('.trvideoclone').clone();
                                        var index = md2.find('table tbody').find('tr').length;
                                        trvideoclone.attr('data-type', v.tipo);
                                        trvideoclone.attr('data-link', v.link);
                                        if (v.hasOwnProperty('ubicacion')) {
                                            trvideoclone.attr('ubicacion', v.ubicacion);
                                        } else {
                                            trvideoclone.attr('ubicacion', "static");
                                        }

                                        trvideoclone.find('td').first().html(index);
                                        if (v.tipo == "enlaceyoutube") {
                                            v.link = 'https://www.youtube.com/watch?v=' + v.link;
                                            trvideoclone.find('input[name="nom"]').val(v.link);
                                        } else {
                                            trvideoclone.find('input[name="nom"]').val(v.nombre);
                                        }
                                        if (v.tipo == 'video') {
                                            trvideoclone.find('.btnsubirvideo').parent().html('<video controls="true" src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo" style="max-width:200px; max-height:200px;"></video>');
                                        } else if (v.tipo == 'imagen') {
                                            trvideoclone.find('.btnsubirvideo').parent().html('<img src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo img-responsive" style="max-width:200px; max-height:200px;"/>');
                                        } else if(v.tipo == "enlacevimeo"){
                                            trvideoclone.find('.btnsubirvideo').parent().html(`<input type="text" class="btnenlacevideo-vimeo" value="${v.link}" readonly />`);
                                        } else {
                                            trvideoclone.find('.btnsubirvideo').parent().html('<i src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo btn btn-danger fa fa-file-o" > ' + v.tipo + ' </i>');
                                        }
                                        trvideoclone.removeClass('trvideoclone').removeAttr('style');
                                        md2.find('.trvideoclone').before(trvideoclone);
                                    })
                            }
                        } catch (ex) {
                            console.log(ex);
                        }

                        md2.on('click', '.imove', function() {
                            var tr = $(this).closest('tr');
                            var adonde = $(this).attr('adonde');
                            if (adonde == 'up') {
                                if (!tr.prev().hasClass('trvideoclone')) tr.prev().before(tr);
                            } else {
                                if (!tr.next().hasClass('trvideoclone')) tr.next().after(tr);
                            }
                            md2.find('tbody').find('tr').each(function(i, tr) {
                                $(tr).find('td').first().text(i + 1);
                            })
                        }).on('click', '.iborrar', function() {
                            $(this).closest('tr').remove();
                            md2.find('tbody').find('tr').each(function(i, tr) {
                                $(tr).find('td').first().text(i + 1);
                            })
                        }).on('click', '.btnaddvideo', function() {
                            var trvideoclone = md2.find('.trvideoclone').clone();
                            var index = md2.find('table tbody').find('tr').length;
                            trvideoclone.find('td').first().html(index);
                            trvideoclone.removeClass('trvideoclone').removeAttr('style');
                            md2.find('.trvideoclone').before(trvideoclone);
                        }).on('click', '.btnsubirvideo', function(e) {
                            e.preventDefault();
                            var el_ = $(this);
                            var $img = el_.children('i');
                            var tr = el_.closest('tr');

                            <?php if ($this->idcomplementario != 0) {   ?>
                                var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : 'c_c/');
                                dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
                            <?php  } else {    ?>
                                var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
                                dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
                            <?php } ?>

                            var nombre = 'ses_' + Date.now();
                            var oldmedia = $img.attr('src') || '';
                            // console.log('multiopt menu, pestaña');
                            // console.log('tr', tr);
                            __subirfile({
                                file: $img,
                                typefile: 'anymedia',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            }, function(rs) {
                                var tipomedia_ = rs.media.lastIndexOf('.mp4?');
                                if (tipomedia_ != -1) tipomedia_ = 'video';
                                else if(rs.media.lastIndexOf('.pdf?') != -1)
                                    tipomedia_ = 'pdf';
                                else if(rs.media.lastIndexOf('.mp3?') != -1)
                                    tipomedia_ = 'audio';
                                else if(rs.media.lastIndexOf('.doc') != -1 || rs.media.lastIndexOf('.docx') != -1 )
                                    tipomedia_ = 'word';
                                else if(rs.media.lastIndexOf('.xls') != -1 || rs.media.lastIndexOf('.xlsx') != -1 )
                                    tipomedia_ = 'excel';
                                 else if(rs.media.lastIndexOf('.ppt') != -1 || rs.media.lastIndexOf('.pptx') != -1 )
                                    tipomedia_ = 'power point';
                                else if(rs.media.lastIndexOf('.html?') != -1)
                                    tipomedia_ = 'html';
                                else tipomedia_ = 'imagen';
                                tr.attr('data-type', tipomedia_);
                                tr.attr('data-link', rs.media);
                                tr.attr('data-oldmedia', rs.media);
                                if (tipomedia_ == 'video') {
                                    el_.closest('div').html('<video controls="true" src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo" style="max-width:200px; max-height:200px;"></video>');
                                } else if (tipomedia_ == 'imagen') {
                                    el_.closest('div').html('<img src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo" style="img-responsive max-width:200px; max-height:200px;"></video>');
                                } else {
                                    el_.closest('div').html('<i src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo btn btn-danger fa fa-file-o" > ' + tipomedia_ + ' </i>');
                                }
                            });
                        }).on("click",".btnenlacevideo-vimeo",function(e){
                            e.preventDefault();
                            var el_ = $(this);
                            var tr = el_.closest('tr');
                            var dt = {
                                html: `
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="alert alert-info">El enlace requerido debe contener el siguiente formato: https://vimeo.com/286898202</p>
                                                <p>Enlace (link) <font color="red">(*)</font>
                                                    <input type="text" maxlength="200" class="form-control input-sm multioption-enlacevimeo" placeholder="Ingrese Enlace (Link)" required="">
                                                </p>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <a href="#" class="btnadd-enlacevimeo btn btn-warning"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></a>
                                                <a href="#" class="cerrarmodal btn btn-primary"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></a>
                                            </div>
                                        </div>
                                    </div>
                                `
                            };
                            var md3 = __sysmodal(dt);
                            md3.on('click', '.btnadd-enlacevimeo', function() {
                                let enlaceVimeoMultioptions =md3.find(".multioption-enlacevimeo").val()
                                tr.attr('data-type', "enlacevimeo");
                                tr.attr('data-link', enlaceVimeoMultioptions);
                                if(tr.find('input[name="nom"]').length == 0){
                                    tr.append(`<input type="hidden" name="nom" value="Enlace de Vimeo" />`);
                                }
                                el_.closest('div').html(`<input type="text" class="btnenlacevideo-vimeo" value="${enlaceVimeoMultioptions}" readonly />`);
                                __cerrarmodal(md3);
                            })
                            console.log("Mostrar el modal de los enlaces");
                        }).on('click', '.btnsavejson', function() {
                            it.attr('data-type', 'multioptions');
                            var _jsonmultioptions = [];
                            var nEnlace = 0;
                            md2.find('tbody').find('tr').each(function(i, tr) {
                                var $tr = $(this);
                                if (!$tr.hasClass('trvideoclone')) {

                                    var nombre_ = $tr.find('input[name="nom"]').val() || 'video';

                                    var enlace = nombre_.split("v=")[1];
                                    if (enlace != undefined) {
                                        enlace = enlace.split("&")[0];
                                        nEnlace++;
                                    }

                                    _jsonmultioptions.push({
                                        nombre: (enlace != undefined) ? "Video de Youtube " + nEnlace : nombre_,
                                        tipo: (enlace != undefined) ? "enlaceyoutube" : $tr.attr('data-type') || '',
                                        link: (enlace != undefined) ? enlace : $tr.attr('data-link') || '',
                                        ubicacion: (enlace != undefined) ? "youtube" : $tr.attr('ubicacion') || ''
                                    });
                                }
                            })
                            var __jsonmultioptions = {
                                mostrarcomo: 'slider',
                                menus: _jsonmultioptions
                            };

                            console.log(elli,md2.find('select').val());

                            if(!elli.hasClass('esmenu')) __jsonmultioptions.mostrarcomo = md2.find('select').val();
                            __jsonmultioptions = JSON.stringify(__jsonmultioptions);
                            __jsonmultioptions = btoa(__jsonmultioptions);
                            it.attr('data-link', __jsonmultioptions);
                            __cerrarmodal(md2);
                            __asiganacionfile(it);
                        })
                        break;
                    case 'videoconferencia':
                        var dt = {
                            html: $('#showvideoconferencia').html(),
                            titulo: 'Enlaces de Video Conferencias'
                        };
                        var md2 = __sysmodal(dt);                     
                        setTimeout(function(){
                            let idtmpgui='txt'+__idgui();
                            console.log(idtmpgui);
                            md2.find('textarea[name="input-conf-detalle"]').attr('id',idtmpgui)
                            __initNewEditor(idtmpgui,'');
                        },500);
                        try {
                            if (datalink != '') {
                                let conferencia = JSON.parse(atob(datalink));
                                md2.find('#input-conf-titulo').val(conferencia.titulo);
                                md2.find('#input-conf-link_mod').val(conferencia.link_mod);
                                md2.find('#input-conf-link_visitor').val(conferencia.link_visitor);
                                md2.find('#input-conf-fecha').val(conferencia.fecha);
                                md2.find('#input-conf-hora_comienzo').val(conferencia.hora_comienzo);
                                md2.find('textarea[name="input-conf-detalle"]').val(conferencia.detalle);
                            }
                        } catch (ex) {
                            console.log(ex);
                        }
                        md2.on('click', '.btnsavejson', function() {
                            it.attr('data-type', 'videoconferencia');

                            let conferencia = {
                                titulo: md2.find('#input-conf-titulo').val(),
                                link_mod: md2.find('#input-conf-link_mod').val(),
                                link_visitor: md2.find('#input-conf-link_visitor').val(),
                                fecha: md2.find('#input-conf-fecha').val(),
                                hora_comienzo: md2.find('#input-conf-hora_comienzo').val(),
                                detalle: md2.find('textarea[name="input-conf-detalle"]').val(),
                            };
                            if (conferencia.titulo == '' || conferencia.link_visitor == '' || conferencia.fecha == '' ||
                                conferencia.hora_comienzo == '' || conferencia.detalle == '') {
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Complete todos los datos',
                                    showConfirmButton: true,
                                });

                                return null;
                            } else {
                                if (!validUrl(conferencia.link_visitor)) {
                                    Swal.fire({
                                        icon: 'warning',
                                        title: 'Ingrese una Url válida!',
                                        showConfirmButton: true,
                                    });
                                    return null;
                                }
                            }
                            if (conferencia.link_mod != '') {
                                if (!validUrl(conferencia.link_mod)) {
                                    Swal.fire({
                                        icon: 'warning',
                                        title: 'Ingrese una Url válida!',
                                        showConfirmButton: true,
                                    });
                                    return null;
                                } else {
                                    if (!conferencia.link_mod.includes('http')) {
                                        conferencia.link_mod = 'http://' + conferencia.link_mod;
                                    }
                                }
                            }
                            if (!conferencia.link_visitor.includes('http')) {
                                conferencia.link_visitor = 'http://' + conferencia.link_visitor;
                            }
                            var __jsonmultioptions = conferencia;
                            __jsonmultioptions = JSON.stringify(__jsonmultioptions);
                            __jsonmultioptions = btoa(__jsonmultioptions);
                            it.attr('data-link', __jsonmultioptions);
                            __cerrarmodal(md2);
                            __asiganacionfile(it);
                        })
                        break;
                    case 'smartlibrery':
                        __smartlibrery(it, nombre, dirmedia, function(link) {
                            it.attr('data-link', link);
                            it.attr('data-type', 'smartlibrery');
                            it.attr('data-oldmedia', link);
                            __asiganacionfile(it);
                        });
                        break;
                    case 'descargacontrolada':
                        var el=_li_;
                        var dt={html:$('#showdescargascontroladas').html()};
                        var vermodal=__sysmodal(dt);
                        if(datalink!=''){
                            try{
                                __listarinfoAjax(true,{
                                    url:'json/descargascontroladas/link',
                                    data:{
                                        datalink:datalink,
                                    }
                                }).then(rs=>{
                                    console.log(rs);
                                    vermodal.find('input.titulo').val(rs.titulo);
                                    vermodal.find('textarea.descripcion').val(rs.descripcion);
                                    $.each(rs.archivos,function(i,t){
                                        let newfile=`<div class="file-item-subido col-md-2" style="text-align: center; border: 1px solid #ccc;  border-radius: 1ex; position:relative;">
                                         <a href="${t}" class="btndescargar" target="_blank" style="font-size: 4em; cursor:pointer; "><i class="fa fa-file "></i></a>
                                         <i class="fa fa-trash btnborrarfile" style=" position: absolute; right: 1ex;  top: 1ex; cursor:pointer" ></i>
                                        </div>`;
                                        vermodal.find('.archivossubidos').append(newfile);
                                        if((vermodal.find('.archivossubidos').attr('numerodearchivo')||1)==1){
                                            vermodal.find('.btnsubirarchivo').hide();
                                        }
                                    })
                                })
                            }catch(e){console.log(e)}
                        }
                        
                        vermodal.on('click','.btnsubirarchivo',function(ev){
                            ev.preventDefault();
                            let it2=$(this);
                             __subirfile({
                                file: it2.children('i'),
                                typefile: 'file',
                                uploadtmp: true,
                                guardar: true,
                                dirmedia: dirmedia,
                                'oldmedia': oldmedia,
                                'nombre': nombre
                            },
                            //__subirfile({ file:it.children('i'), typefile:'video',guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},
                            function(rs){
                                let newfile=`<div class="file-item-subido col-md-2" style="text-align: center; border: 1px solid #ccc;  border-radius: 1ex; position:relative;">
                                 <a href="${rs.media}" class="btndescargar" target="_blank" download style="font-size: 4em; cursor:pointer;"><i class="fa fa-file "></i></a>
                                 <i class="fa fa-trash btnborrarfile" style=" position: absolute; right: 1ex;  top: 1ex; cursor:pointer" ></i>
                                </div>`;
                                vermodal.find('.archivossubidos').append(newfile);
                                if((vermodal.find('.archivossubidos').attr('numerodearchivo')||1)==1){
                                    it2.hide();
                                }                               
                            });
                        }).on('click','.btnborrarfile',function(ev){
                            $(this).closest('.file-item-subido').remove();
                             vermodal.find('.btnsubirarchivo').show();
                        }).on('click','.btnsavejson',function(ev){
                            ev.preventDefault();
                            let titulo=vermodal.find('input.titulo').val()||'';
                            let descripcion=vermodal.find('textarea.descripcion').val()||'';
                            let files=[];
                            vermodal.find('.file-item-subido').each(function(i,fi){
                                files.push($(fi).children('a').attr('href'));
                            })
                            if(datalink=='') datalink=dirmedia+nombre+'.php';

                            ev.preventDefault();
                            Swal.showLoading();                           
                            $.post(_sysUrlBase_ + 'json/descargascontroladas/guardar',{
                                    'titulo': titulo,
                                    'descripcion': descripcion,
                                    'archivos': files,
                                    'link': datalink,
                                    //'estado': 1,
                                    //'dirmedia':dirmedia
                                    //'idrecurso': frm.find('input[name="idrecurso"]').val()||'',
                                }, function(data){
                                    Swal.close();
                                    if (data.code == 200){
                                        it.attr('data-type', 'descargacontrolada');
                                        it.attr('data-link', data.link);                                  
                                        __asiganacionfile(it);
                                        __cerrarmodal(vermodal);
                                    }else{
                                        //mensaje de Error
                                    }
                                }, 'json');
                            return;                           
                        }).on('click','a.btndescargar',function(ev){
                            ev.preventDefault();
                            var element = document.createElement('a');
                            let urlfile=$(this).attr('href').toString();
                              element.setAttribute('href', _sysUrlBase_+urlfile); 
                              var filenameWithExtension = urlfile.replace(/^.*[\\\/]/, '').split('?')[0];
                              console.log(filenameWithExtension);

                              element.setAttribute('download',filenameWithExtension);                             
                              element.style.display = 'none';
                              document.body.appendChild(element);
                              element.click();
                              document.body.removeChild(element);
                        })

                        break;
                }
                ev.stopPropagation();
            })
        }).on('click', '.btnaddhabilidades', function(ev) {
            var el = $(this);
            var dt = {
                html: $('#showpaddhabilidad').html()
            };
            var paso3 = $('#nestable');
            var md = __sysmodal(dt);
            var li = el.closest('.esmenu');
            var idcursodetalle = li.attr('idcursodetalle');
            var idcurso = $('#idcurso').val();
            var _li_ = el.closest('li');
            var __criterios = function(obj, valor) {
                var formData = new FormData();
                formData.append('idcurso', valor);
                $.ajax({
                    url: _sysUrlBase_ + 'json/acad_criterios/listado',
                    type: 'POST',
                    contentType: false,
                    data: formData, // mandamos el objeto formdata que se igualo a la variable data
                    processData: false,
                    cache: false,
                    beforeSend: function(objeto) {
                        //$("#loadCategoria").html("<img src='../util/images/load.gif'>");
                    },
                    success: function(data) {
                        var response = JSON.parse(data);
                        var html = "";
                        crisel = _li_.attr('criterios');
                        if (crisel != undefined) {
                            var crisel = crisel.split("|");
                        } else var crisel = [];

                        if (response.data.length > 0) {
                            $.each(response.data, function(i, item) {
                                var selcri = false;
                                $.each(crisel, function(i, c) {
                                    if (c == item.idcriterio) selcri = 'checked="checked"';
                                })
                                html += '<li>';
                                html += '<p>';
                                html += '<input type="checkbox" ' + selcri + ' class="flat" id="criterio' + i + '" style="position: absolute; opacity: 1;" data-id="' + item.idcriterio + '" data-nombre="' + item.nombre + '"/>';
                                html += ' ' + item.nombre;
                                html += '</p>';
                                html += '</li>';
                            });
                            obj.find('#listado_criterios').html(html);

                            obj.find("input.flat").iCheck({
                                checkboxClass: "icheckbox_flat-green"
                            })
                        } else {
                            obj.find('#listado_criterios').html("<h2><center>No tiene criterios designados</center></h2>");
                        }
                    }
                })
            }

            var link = '';
            md.on('click', '.btnsavejson_aux ', function() {
                var _jsoncriterios = '';
                md.find("#listado_criterios li").each(function(i, v) {
                    if ($(v).find('input').is(':checked')) {
                        _jsoncriterios += $(this).find('input').attr('data-id') + '|';
                    }
                });
                _li_.attr('criterios', _jsoncriterios);
                actulaizarjson(li);
                __cerrarmodal(md);
            })
            __criterios(md, idcurso);
        }).on('click', '.btnaddvideos', function(ev) {
            var el = $(this);
            var dt = {
                html: $('#showpaddmultivideos').html()
            };
            var paso3 = $('#nestable');
            var md = __sysmodal(dt);
            var li = el.closest('.esmenu');
            var idcursodetalle = li.attr('idcursodetalle');
            var idcurso = $('#idcurso').val();
            var li_ = el.closest('li');
            var typetmp = li_.attr('type') || '';
            if (typetmp == 'multioptions') {
                try {
                    link = li_.attr('link');
                    link = atob(link);
                    link = JSON.parse(link);
                    md.find('select').val(link.mostrarcomo || 'slider')
                    var __menus = link.menus || '';
                    if (__menus != '')
                        $.each(__menus, function(i, v) {
                            var trvideoclone = md.find('.trvideoclone').clone();
                            var index = md.find('table tbody').find('tr').length;
                            trvideoclone.attr('data-type', v.tipo);
                            trvideoclone.attr('data-link', v.link);
                            trvideoclone.find('td').first().html(index);
                            trvideoclone.find('input[name="nom"]').val(v.nombre);
                            if (v.tipo == 'video') {
                                trvideoclone.find('.btnsubirvideo').parent().html('<video controls="true" src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo" style="max-width:200px; max-height:200px;"></video>');
                            } else if (v.tipo == 'imagen') {
                                trvideoclone.find('.btnsubirvideo').parent().html('<img src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo img-responsive" style="max-width:200px; max-height:200px;"/>');
                            } else {
                                trvideoclone.find('.btnsubirvideo').parent().html('<i src="' + _sysUrlBase_ + v.link + '" class="btnsubirvideo btn btn-danger fa fa-file-o" > ' + v.tipo + ' </i>');
                            }
                            trvideoclone.removeClass('trvideoclone').removeAttr('style');
                            md.find('.trvideoclone').before(trvideoclone);
                        })
                } catch (ex) {
                    console.log(ex);
                }
            }
            md.on('click', '.imove', function() {
                var tr = $(this).closest('tr');
                var adonde = $(this).attr('adonde');
                if (adonde == 'up') {
                    if (!tr.prev().hasClass('trvideoclone')) tr.prev().before(tr);
                } else {
                    if (!tr.next().hasClass('trvideoclone')) tr.next().after(tr);
                }
                md.find('tbody').find('tr').each(function(i, tr) {
                    $(tr).find('td').first().text(i + 1);
                })
            }).on('click', '.iborrar', function() {
                $(this).closest('tr').remove();
                md.find('tbody').find('tr').each(function(i, tr) {
                    $(tr).find('td').first().text(i + 1);
                })
            }).on('click', '.btnaddvideo', function() {
                var trvideoclone = md.find('.trvideoclone').clone();
                var index = md.find('table tbody').find('tr').length;
                trvideoclone.find('td').first().html(index);
                trvideoclone.removeClass('trvideoclone').removeAttr('style');
                md.find('.trvideoclone').before(trvideoclone);
            }).on('click', '.btnsubirvideo', function(e) {
                e.preventDefault();
                var el = $(this);
                var $img = el.children('i');
                var tr = el.closest('tr');
                <?php
                if ($this->idcomplementario != 0) {
                ?>
                    // var dirmedia = 'cursos_c/c_c_' + idcurso + '/ses_' + idcursodetalle + "/";
                    var dirmedia = (idcurso != '' ? ('c_c_' + idcurso + '_<?php echo $this->idcomplementario; ?>/') : 'c_c/');
                    dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
                    // var dirmedia_ = 'cursos_c/' + dirmedia;
                <?php
                } else {
                ?>
                    var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
                    dirmedia = dirmedia + (idcursodetalle != '' ? ('ses_' + idcursodetalle + '') : 'ses_tmp') + '/';
                <?php
                }
                ?>
                // var dirmedia = (idcurso != '' ? ('curso_' + idcurso + '/') : 'curso/');
                // dirmedia=dirmedia+(idcursodetalle!=''?('ses_'+idcursodetalle+''):'ses_tmp')+'/';
                var nombre = 'ses_' + Date.now();
                var oldmedia = $img.attr('src') || '';
                __subirfile({
                    file: $img,
                    typefile: 'media',
                    uploadtmp: true,
                    guardar: true,
                    dirmedia: dirmedia,
                    'oldmedia': oldmedia,
                    'nombre': nombre
                }, function(rs) {
                    var tipomedia_ = rs.media.lastIndexOf('.mp4?');
                    if (tipomedia_ != -1) tipomedia_ = 'video';
                    else if (rs.media.lastIndexOf('.pdf?') != -1)
                        tipomedia_ = 'pdf';
                    else if (rs.media.lastIndexOf('.mp3?') != -1)
                        tipomedia_ = 'audio';
                    else if (rs.media.lastIndexOf('.html?') != -1)
                        tipomedia_ = 'html';
                    else tipomedia_ = 'imagen';
                    tr.attr('data-type', tipomedia_);
                    tr.attr('data-link', rs.media);
                    tr.attr('data-oldmedia', rs.media);
                    if (tipomedia_ == 'video') {
                        el.closest('div').html('<video controls="true" src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo" style="max-width:200px; max-height:200px;"></video>');
                    } else if (tipomedia_ == 'imagen') {
                        el.closest('div').html('<img src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo" style="img-responsive max-width:200px; max-height:200px;"></video>');
                    } else {
                        el.closest('div').html('<i src="' + _sysUrlBase_ + rs.media + '" class="btnsubirvideo btn btn-danger fa fa-file-o" > ' + tipomedia_ + ' </i>');
                    }
                });
            }).on('click', '.btnsavejson', function() {
                li_.attr('type', 'multioptions');
                var _jsonmultioptions = [];
                md.find('tbody').find('tr').each(function(i, tr) {
                    var $tr = $(this);
                    if (!$tr.hasClass('trvideoclone')) {
                        _jsonmultioptions.push({
                            nombre: $tr.find('input[name="nom"]').val() || 'video',
                            tipo: $tr.attr('data-type') || '',
                            link: $tr.attr('data-link') || '',
                            ubicacion: $tr.attr('ubicacion') || ''
                        });
                    }
                })
                var __jsonmultioptions = {
                    mostrarcomo: 'slider',
                    menus: _jsonmultioptions

                };
                __jsonmultioptions.mostrarcomo = md.find('select').val();
                __jsonmultioptions = JSON.stringify(__jsonmultioptions);
                __jsonmultioptions = btoa(__jsonmultioptions);
                li_.attr('link', __jsonmultioptions);
                actulaizarjson(li);
                __cerrarmodal(md);
            })
        }).on('click', '.btnaddrubricas', function(ev) {
            el = $(this);
            $('#input-idinstancia').val('');
            var my_li = $(this).parent().parent();
            // if (condition) {
            let strCriterios = $(my_li).attr('criterios') || '';
            // console.log('strCriterios', strCriterios);
            if (strCriterios != '') {
                Swal.fire({
                    icon: 'warning',
                    title: 'No se puede agregar Rúbricas.',
                    text: 'Esta tarea/proyecto cuenta con criterios asignados.',
                })
                return null;
            }
            var el = $(this);
            var li = el.closest('li');
            var lipadre = li.hasClass('esmenu') ? li : li.closest('.esmenu');
            var dt = {
                html: $('#showaddrubricas').find('#fix-md').clone(true)
            };

            var md = __sysmodal(dt, my_li);
            md.on('hidden.bs.modal', function(e) {
                _currentSelect = null;
            })
            getSelectRubrica(md, my_li);
            md.on('click', '.btnsaverubrica', function(e) {
                // working
                if (saveInstancia(md, my_li)) {
                    my_li.children('.btn-group').children('i.btnaddrubricas').addClass('conrubrica');
                    my_li.children('.accionesdecurso').children('.btnaddrubricas').addClass('bg-success');
                    if (my_li.hasClass('esmenu')) {
                        actulaizarjson(my_li);
                    } else { //es pestaña
                        actulaizarjson(my_li.parent().parent());
                    }
                    // actulaizarjson(my_li);
                };
            })
        })

        var actulaizarjson = function(lipadre) {
            // console.log('actulaizarjson', lipadre);
            if (!lipadre.hasClass('esmenu')) return; //si no es menu, regresa
            var ol = lipadre.children('ol');
            var ddlist = ol.children('li');
            var listorden = {}
            var idcursodetalle = lipadre.attr('idcursodetalle');
            if (ol.length) //sus pestañas
                $.each(ddlist, function(i, v) {
                    if (!$(v).hasClass('liclone')) {
                        var estarea = $(v).find('i.btnestarea').hasClass('bg-success') ? 'si' : 'no';
                        var esproyecto = $(v).find('i.btnesproyecto').hasClass('bg-success') ? 'si' : 'no';
                        var link = ($(v).attr('link') || '');
                        var index = link.lastIndexOf('static/');
                        link = index > -1 ? link.substring(index) : link;
                        var imagen = ($(v).attr('imagen') || '');
                        var indeximg = imagen.lastIndexOf('static/');
                        imagen = indeximg > -1 ? imagen.substring(indeximg) : imagen;
                        var indeximg = imagen.lastIndexOf('nofoto');
                        imagen = indeximg > -1 ? '' : imagen;
                        var esrecursopagado = $(v).find('i.btnrecursopagado').hasClass('bg-success') ? 'si' : 'no';
                        var necesitadocente = $(v).find('i.btnconteacher').hasClass('bg-success') ? 'si' : 'no';
                        var _vermenu = $(v).find('i.btnverenmenu').hasClass('fa-eye') ? 'si' : 'no';
                        var iconproyecto = $(v).find('i.iconproyecto').hasClass('bg-success') ? 'si' : 'no';
                        var mostrarpregunta = $(v).find('i.btnmostrarpregunta').hasClass('bg-success') ? 'si' : 'no';
                        var conrubrica = $(v).find('i.btnaddrubricas').hasClass('bg-success') ? 'si' : 'no';
                        let fecha_limite = $(v).attr('fecha_limite') || '';
                        let ubicacion = $(v).attr('ubicacion') || 'static';
                        if ($(v).attr('type') == 'smartquiz') {
                            estarea = 'no';
                            esproyecto = 'no';
                        }
                        listorden[i] = {
                            "nombre": $(v).find('.titulo').text().trim(),
                            "id": idcursodetalle + '' + i,
                            "link": link,
                            "type": $(v).attr('type'),
                            "color": "rgba(0, 0, 0, 1)",
                            "colorfondo": $(v).attr('color'),
                            "colorfondopagina": $(v).attr('color'),
                            "imagenfondo": imagen,
                            "imagenfondopagina": imagen,
                            'esrecursopagado': esrecursopagado,
                            'necesitadocente': necesitadocente,
                            'estarea': estarea,
                            'esproyecto': esproyecto,
                            'iconproyecto': iconproyecto,
                            'mostrarpregunta': mostrarpregunta,
                            'conrubrica': conrubrica,
                            '_vermenu': _vermenu,
                            'criterios': $(v).attr('criterios') || '',
                            fecha_limite,
                            ubicacion
                        };
                        //mine
                        if (listorden[i].estarea == 'si' || listorden[i].esproyecto == 'si') {
                            fechaTarea.li = $(v);
                            fechaTarea.updateSesionPestania({
                                nombre: `${$($(lipadre).find('.titulo')[0]).text().trim()} - ${listorden[i].nombre}`,
                                recurso: listorden[i].link,
                                tiporecurso: listorden[i].type,
                                tipo: listorden[i].estarea == 'si' ? 'T' : 'P'
                            });
                            oCrudTarea.li = $(v);
                            oCrudTarea.updateSesionPestania({
                                nombre: `${$($(lipadre).find('.titulo')[0]).text().trim()} - ${listorden[i].nombre}`,
                                recurso: listorden[i].link,
                                tiporecurso: listorden[i].type,
                                tipo: listorden[i].estarea == 'si' ? 'T' : 'P'
                            });
                        }
                    }
                })
            var _txtjson_ = {}
            _txtjson_.estarea = lipadre.children('.btn-group').children('i.btnestarea').hasClass('bg-success') ? 'si' : 'no';
            _txtjson_._vermenu = lipadre.children('.btn-group').children('i.btnverenmenu').hasClass('fa-eye') ? 'si' : 'no';
            _txtjson_.esproyecto = lipadre.children('.btn-group').children('i.btnesproyecto').hasClass('bg-success') ? 'si' : 'no';
            _txtjson_.tipofile = lipadre.attr('txtjson-tipofile');
            _txtjson_.tipo = lipadre.attr('txtjson-tipo');
            _txtjson_.ubicacion = lipadre.attr('ubicacion');
            _txtjson_.esrecursopagado = lipadre.children('.btn-group').children('i.btnrecursopagado').hasClass('bg-success') ? 'si' : 'no';
            _txtjson_.necesitadocente = lipadre.children('.btn-group').children('i.btnconteacher').hasClass('bg-success') ? 'si' : 'no';
            //_txtjson_.iconproyecto = lipadre.children('.btn-group').children('i.iconproyecto').hasClass('bg-success') ? 'si' : 'no';
            _txtjson_.mostrarpregunta = lipadre.children('.btn-group').children('i.btnmostrarpregunta').hasClass('bg-success') ? 'si' : 'no';
            _txtjson_.conrubrica = lipadre.children('.btn-group').children('i.conrubrica').hasClass('bg-success') ? 'si' : 'no';
            _txtjson_.icon = lipadre.attr('data-icon') || '#';
            _txtjson_.criterios = lipadre.attr('criterios') || '';
            _txtjson_.fecha_limite = lipadre.attr('fecha_limite') || '';
            // console.log('lipadre', $(lipadre).find('.titulo').text().trim());
            var link = (lipadre.attr('txtjson-link') || '');
            var index = link.lastIndexOf('static/');
            link = index > -1 ? link.substring(index) : link;
            _txtjson_.link = link;
            _txtjson_.typelink = lipadre.attr('txtjson-typelink');
            if (_txtjson_.typelink == 'smartquiz') {
                _txtjson_.estarea = 'no';
                _txtjson_.esproyecto = 'no';
            }
            _txtjson_.infoavancetema = lipadre.attr('txtjson-tipofile');
            _txtjson_.infoavancetema = 100;
            _txtjson_.options = listorden;
            _txtjson_.criterios = lipadre.attr('criterios') || '';
            if (_txtjson_.estarea == 'si' || _txtjson_.esproyecto == 'si') {
                fechaTarea.li = $(lipadre);
                fechaTarea.updateSesionPestania({
                    recurso: _txtjson_.link,
                    tiporecurso: _txtjson_.typelink,
                    tipo: _txtjson_.estarea == 'si' ? 'T' : 'P'
                });
            }
            var idcurso = $('#idcurso').val();
            var formData = new FormData();
            formData.append('idcursodetalle', parseInt(idcursodetalle));
            formData.append('campo', 'txtjson');
            formData.append('valor', JSON.stringify(_txtjson_));
            formData.append('icc', IDCOMPLEMENTARIO);
            __sysAyax({
                fromdata: formData,
                url: _sysUrlBase_ + 'json/acad_cursodetalle/setCampo',
                // async:false,
                callback: function(rs) {
                    console.log('[__sysAyax from actulaizarjson]', rs)
                }
            })
        };

        var validarcurso = function() {
            var idcurso = parseInt($('#idcurso').val() || '0');
            if (idcurso == 0) $('.form_wizard').find('.buttonNext').addClass('buttonDisabled');
            else $('.form_wizard').find('.buttonNext').removeClass('buttonDisabled');
        }
        $('.form_wizard').on('click', '.buttonNext', function(ev) {
            if ($(this).hasClass('buttonDisabled')) {
                ev.preventDefault();
                return;
            }
        })
        validarcurso();
        // activate Nestable for list 1

        var __nestable = function() {
            var oljson = false;
            $('#nestable').nestable({
                onDragStart: function(divnestable, itemlimove) {
                    $ol = itemlimove.closest('ol');
                    divnestable.find('ol.esjson').addClass('nomoveaqui dd-nochildren')
                    divnestable.find('ol.esjson').children('li').addClass('dd-nochildren');
                    if ($ol.hasClass('esjson')) {
                        divnestable.find('ol').addClass('nomoveaqui');
                        oljson = Date.now();
                        $ol.attr('moveenjson', oljson);
                        $ol.removeClass('nomoveaqui dd-nochildren');
                        $ol.parent('li').removeClass('dd-nochildren');
                    } else {
                        oljson = false;
                        divnestable.find('ol').removeClass('nomoveaqui');
                        divnestable.find("li[espadre=0]").addClass('dd-nochildren');
                        divnestable.find("li[espadre=1]").removeClass('dd-nochildren');
                    }
                },
                beforeDragStop: function(divnestable, itemlimove, $oldonde) {
                    if ($oldonde.hasClass('nomoveaqui')) {
                        Swal.fire({
                            type: 'error',
                            icon: 'error',
                            title: 'Oops...',
                            text: 'El Menu seleccionado no se puede mover en este lugar'
                        })
                        return false;
                    }

                    if ($oldonde.hasClass('esjson')) {
                        if (oljson == false) return false;
                        else if ($oldonde.attr('moveenjson') != oljson) return false;
                    }
                    var lipadre = $oldonde.parent();
                    if (lipadre.attr('espadre') == 1 && itemlimove.attr('espadre') == 1) {
                        Swal.fire({
                            type: 'error',
                            icon: 'error',
                            title: 'Oops...',
                            text: 'El Menu contenedor no se puede estar dentro de un menu contenedor'
                        })
                        return false;
                    }
                    divnestable.find('li').removeClass('_cambioesteli_');
                    itemlimove.addClass('_cambioesteli_');
                    return true;
                }

            }).on('change', function(ev) { //(al parecer)Cuando se reordenan los menus
                __reordenar();
                return;
            });
        }
        __nestable();
        var __reordenar = function() {
            var liquecambio = $('#nestable').find('li._cambioesteli_');
            var ol = liquecambio.closest('ol');
            if (ol.hasClass('esjson')) {
                var lipadre = ol.closest('li');
                actulaizarjson(lipadre);
            } else {
                var listorden = [];
                var lipadres = $('#nestable').children('ol').children('li'); //.nestable('serialize');
                $.each(lipadres, function(i, v) {
                    if (!$(v).hasClass('liclone')) {
                        var _espadre = $(v).attr('espadre') || 0;
                        var _idpadre = $(v).attr('idcursodetalle');
                        $(v).attr('orden', i);
                        listorden[i] = {
                            idcursodetalle: _idpadre,
                            orden: i,
                            idpadre: 0
                        };
                        if (_espadre == 1) {
                            var children = [];
                            $hijos = $(v).children('ol').children('li');
                            $.each($hijos, function(j, vv) {
                                children[j] = {
                                    idcursodetalle: $(vv).attr('idcursodetalle'),
                                    orden: j,
                                    idpadre: _idpadre
                                };
                                $(vv).attr('orden', j);
                            })
                            listorden[i].children = children;
                        }
                    }
                });
                var idcurso = $('#idcurso').val();
                var formData = new FormData();
                formData.append('idcurso', parseInt(idcurso));
                formData.append('datos', JSON.stringify(listorden));
                formData.append('idcomplementario', IDCOMPLEMENTARIO);
                __sysAyax({
                    fromdata: formData,
                    url: _sysUrlBase_ + 'json/acad_cursodetalle/reordenar',
                    callback: function(rs) {}
                })
            }
        }

        var _nestablehijos = function() {
            $('#nestable').nestable('destroy');
            __nestable();
        }
    })

    function onchangecompetencias(e) {
        var formData = new FormData();
        // console.log('click change', e.target.value);
        formData.append('idcompetencia', e.target.value);
        $.ajax({
            url: _sysUrlBase_ + 'json/acad_capacidades/listado',
            type: 'POST',
            contentType: false,
            data: formData, // mandamos el objeto formdata que se igualo a la variable data
            processData: false,
            cache: false,
            beforeSend: function(objeto) {
                //$("#loadCategoria").html("<img src='../util/images/load.gif'>");
            },
            success: function(data) {
                var response = JSON.parse(data);
                var html = "";

                //console.log('data', response.data);
                if (response.data.length > 0) {
                    $.each(response.data, function(i, item) {
                        //if (item.estado == '1') {
                        html += '<option value="' + item.idcapacidad + '">' + item.nombre + '</option>';
                        //}
                    });
                    // console.log('result_capacidad', html);
                    document.getElementById('txtidcapacidad').innerHTML = html;
                    $("#txtidcapacidad").html(html);
                    $("#txtidcapacidad").on('change', function() {
                        //$("#txtidcapacidad")
                    });
                    //if (edit_bol) {
                    //$("#txtidcapacidad").val(idcompetencia);
                    //cboCursos(0, true, idpadre);
                    //}
                } else {
                    $("#txtidcapacidad").html(html);
                }
                //console.log('Datos Listado', response);
            }
        })
    }
</script>