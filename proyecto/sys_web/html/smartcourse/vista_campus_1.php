<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;
?>
<div class="row justify-content-end">
	<div class="col-sm-12 text-right">
		<div class="btn-group" role="group" aria-label="Basic example">
		  <button type="button" id="btn-agregar" class="btn btn-success" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/crear" >
		  	<i class="fa fa-plus"></i> <?php echo JrTexto::_('New course');?>
		  </button>
		  <?php if($this->curusuario["tuser"]=='s'){?>
		  <button type="button" id="btn-asignar" class="btn btn-warning" alt="<?php echo JrTexto::_('Order');?>" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/asignar"><i class="fa fa-location-arrow"></i> <?php echo ucfirst(JrTexto::_('Add'))." ".ucfirst(JrTexto::_('Course'));?> </button>
			<?php } ?>
		  <button type="button" id="btn-ordenar" class="btn btn-secondary" alt="<?php echo ucfirst(JrTexto::_('Order'));?>" data-href="<?php echo $this->documento->getUrlSitio();?>/cursos/ordenar"><i class="fa fa-sort-amount-desc"></i> <?php echo ucfirst(JrTexto::_('Order'));?> </button>
		</div>
	</div>
</div>
<div class="card card-secondary">
	<div class="card-header">
		<div class="row justify-content-around">
			<div class="col-md-4">
				<div class="form-group">
					<label><?=JrTexto::_("Career")?></label>
					<select id="cmbcategorias" class="form-control">
						<?php $hay=false;
							if(!empty($this->categorias)): 
								foreach($this->categorias as $v):
									if(empty($v["estado"])) continue;
									if(!empty($v["tienecursos"])){
							 ?><option value="<?php echo $v['idcategoria'] ?>" <?php echo $hay==false?'selected="selected"':'';?> ><?php echo strtoupper($v['nombre']) ?></option>
							<?php $hay=true; 
								} 
								endforeach; 
							endif; 
						if(!empty($this->haycursoslibres)||empty($hay)){ ?><option value="0" class="cursoslibres"><?=JrTexto::_("Free Courses")?></option><?php }?>
					</select>
				</div>
			</div>
			<?php if($this->rol != 1): ?>
			<div class="col-md-4">
				<div class="form-group">
					<label><?=JrTexto::_("Study Groups")?></label>
					<select id="cmbgrupos" class="form-control">
					</select>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
    <!-- END CARD -->
    <div class="card-body">
    	<div id="lscursos" class="row">
    	</div>
	</div>
</div>
<div style="display: none">
    <div id="shopnlcategorias"> 
        <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoaula" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
        <div class="col-md-6 col-sm-6 col-xs-12 pnlcat">   
          <label><?=JrTexto::_("Level")?> / <?=JrTexto::_("Career")?> / <?=JrTexto::_("Category")?></label>         
          <div class="select-ctrl-wrapper select-azul">
          <select id="idcategoria" name="idcategoria"  class="form-control selchangecat pnlidcategoria0">
              <option value="0"><?=JrTexto::_("Unique")?> - <?=JrTexto::_("Free Course")?></option>
              <?php if(!empty($this->categoriasProyecto))
              foreach ($this->categoriasProyecto as $k=>$v){ if($v["idpadre"]==0){?>
                <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
              <?php } } ?>
          </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 pnlcat" style="display:none">   
          <label><?=JrTexto::_("Sub Level")?> / <?=JrTexto::_("Module")?> / <?=JrTexto::_("Sub Category")?></label>         
          <div class="select-ctrl-wrapper select-azul">
          <select id="nomsel" name="nomsel" class=" form-control selchangecat pnlidcategoria1">
              <option value="0"><?=JrTexto::_("Select")?></option>
              <?php if(!empty($this->categoriasProyecto))
              foreach ($this->categoriasProyecto as $k=>$v){
                if($v["idpadre"]!=0){?>
                <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
              <?php } } ?>                
          </select>
          </div>
        </div>
        <div class="clearfix"></div>           
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" tb="acad_grupoaula" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
</div>
<div style="display: none">
    <div id="pnlcopiarcursos">
        <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoaula" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
            <div class="row">               
                <div class="col-md-6 col-xs-12 pnlaccion">   
                  <label><?=JrTexto::_("Select Action")?></label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="accion" name="accion"  class=" form-control selchangecat pnlidcategoria0">
                        <option value="copiar"><?=JrTexto::_("Copy")?></option> 
                        <option value="asignar"><?=JrTexto::_("Assign")?></option>
                  </select>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12 pnlempresa">   
                  <label><?=JrTexto::_("Select Company")?> / <?=JrTexto::_("Project")?></label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="idproyecto" name="idproyecto"  class=" form-control selchangecat pnlidcategoria0">
                    <?php if(!empty($this->proyectos))
                         foreach ($this->proyectos as $k => $v){?>
                            <option value="<?php echo $v['idproyecto']; ?>"><?php echo $v['idproyecto'].": ".$v['empresa']; ?></option>
                         <?php } ?>
                  </select>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12 ">   
                  <label><?=JrTexto::_("Copy as")?> : </label>         
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="como" name="como"  class=" form-control selchangecat pnlidcategoria0">
                        <option value="1">Autoestudio - Tipo 1</option> 
                        <option value="2">Curso oficial de la empresa - Tipo 2 </option>                     
                  </select>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12 ">   
                  <label><?=JrTexto::_("Name of the course to be copied")?> : </label>
                  <input type="text" id="nombre" name="nombre" class="form-control" placeholder="<?=JrTexto::_("Name of the course to be copied")?>">
                </div>

                <div class="clearfix"></div>           
                  <hr>
                  <div class="col-md-12 form-group text-center">
                    <button id="btn-save" tb="acad_grupoaula" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
                    <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
	const CATEGORIAS = <?php echo !empty($this->categorias) ? json_encode($this->categorias) : '[]'; ?>;
	const CURSOS = <?php echo !empty($this->allcursos) ? json_encode($this->allcursos) : '[]'; ?>;
	const oUSER = <?php echo !empty($this->curusuario)? json_encode($this->curusuario,true):'[]';?>;	
	
	var catpadres=[];
	$.each(CATEGORIAS,function(i,v){
		if(catpadres[i]==undefined) catpadres[i]=[];
		catpadres[i].push(parseInt(i));
		if(v.hijos!=undefined)
		$.each(v.hijos,function(ii,vv){
			catpadres[i].push(parseInt(ii));
		})
	})

	const cursostutoriales=[207,678];
	$(document).ready(function(){
		$("#btn-agregar , #btn-ordenar, #btn-asignar").click(function(){
        	location.href = $(this).data("href");
    	})

	    $('#lscursos').on('click','.item-curso .edit',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			var dt =$(this).closest('.item-curso');
	        var id=dt.attr('idcurso');
	        var idcc='&icc='+(dt.attr('idcomplementario')||0);
	        idcc+='&tipo='+(dt.attr('tipo')||1);
	        idcc+='&cat='+(dt.attr('idcategoria')||0);
	        window.location.href=_sysUrlSitio_+'/cursos/crear?idcurso='+id+idcc;
		}).on('click','.item-curso .mirarcategoria',function(ev){
	        ev.preventDefault();
	        ev.stopPropagation();        
	        var dt =$(this).closest('.item-curso');
	        var id=dt.attr('idcurso')||0;
	        var idcc=dt.attr('idcomplementario')||0; 
	        var tipo=dt.attr('tipo')||1; 
	        var idcat=dt.attr('idcategoria')||1;    
	        var titulo = '<?php echo JrTexto::_(" Categorias de Curso"); ?>';
	        var frm=$('#shopnlcategorias').clone();
	        var _md = __sysmodal({'html': frm,'titulo': titulo});
	        _md.on('change','select',function(ev){
	            var el=$(this);            
	            if(el.hasClass('pnlidcategoria0')){
	                if(el.val()>0){
	                    _md.find('select.pnlidcategoria0').attr('id','selcat').attr('name','selcat');
	                    _md.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria');
	                    _md.find('select.pnlidcategoria1').children('option').first().nextAll().hide().removeClass('porsel');
	                    _md.find('select.pnlidcategoria1').children('option[padre="'+el.val()+'"]').show().addClass('porsel');
	                    _md.find('select.pnlidcategoria1').parent().parent().show();
	                    if(_md.find('select.pnlidcategoria1').children('option.porsel[value="'+idcat+'"]').length>0)
	                        _md.find('select.pnlidcategoria1').val(idcat);
	                    else  _md.find('select.pnlidcategoria1').val(0);

	                }else{
	                    _md.find('select.pnlidcategoria1').attr('id','selcat').attr('name','selcat');
	                    _md.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria');
	                    _md.find('select.pnlidcategoria1').parent().parent().hide();
	                }
	            }
	        }).on('click','#btn-save',function(ev){
	            ev.preventDefault();
	            ev.stopPropagation();
	            var data=new FormData()
	            data.append('idcurso',id);
	            data.append('idcomplementario',idcc);
	            if(_md.find('select#idcategoria').hasClass('pnlidcategoria0'))
	                data.append('idcategoria',_md.find('select#idcategoria').val());
	            else{
	                let idcattmp=_md.find('select#idcategoria').val()||0;
	                if(idcattmp==0){
	                    data.append('idcategoria',_md.find('select#selcat').val());
	                }else 
	                    data.append('idcategoria',_md.find('select#idcategoria').val());
	            }
	            data.append('tipo',tipo);
	            __sysAyax({ 
		              fromdata:data,
		                url:_sysUrlBase_+'json/acad_curso/guardarcategoria',
		                callback:function(rs){
		                  if(rs.code==200){
		                    window.location.reload();
		                  }
		                }
		            });
		        })

		        var pnl1=_md.find('select.pnlidcategoria0').find('option[value="'+idcat+'"]');
		        if(pnl1.length>0){
		            _md.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria').val(idcat);
		            _md.find('select.pnlidcategoria1').attr('id','selcat').attr('name','selcat');
		            _md.find('select.pnlidcategoria1').parent().parent().hide();
		        }else{
		            var pnl2=_md.find('select.pnlidcategoria1').find('option[value="'+idcat+'"]');
		            if(pnl2.length>0){
		                _md.find('select.pnlidcategoria0').attr('id','selcat').attr('name','selcat').val(pnl2.attr('padre')||0);
		                _md.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria').val(idcat);
		                _md.find('select.pnlidcategoria1').children('option').first().nextAll().hide();
		                _md.find('select.pnlidcategoria1').children('option[padre="'+(pnl2.attr('padre')||0)+'"]').show();
		                _md.find('select.pnlidcategoria1').parent().parent().show();
		            }
		        }
		}).on('click','.item-curso .formular',function(ev){
	        ev.preventDefault();
	        ev.stopPropagation();
	        var dt =$(this).closest('.item-curso');
	        var id=dt.attr('idcurso');
	        id+='&icc='+(dt.attr('idcomplementario')||0);
	        id+='&tipo='+(dt.attr('tipo')||1);
	        id+='&idcategoria='+(dt.attr('idcategoria')||0);        
	        window.location.href=_sysUrlBase_+'/cursos/formular/?idcurso='+id;
	    }).on('click','.item-curso .copy',function(ev){
	        ev.preventDefault();
	        ev.stopPropagation();
	        var dt =$(this).closest('.item-curso');
	        var id=dt.attr('idcurso')||0;
	        var idcc=dt.attr('idcomplementario')||0;
	        var tipo=dt.attr('tipo')||1;  
	        var nombrecurso = dt.find("#nombrecurso").text();
	        var titulo = '<?php echo JrTexto::_(" Copiar  o Asignar curso"); ?>: '  +nombrecurso;
	        var frm=$('#pnlcopiarcursos').clone();
	        if(tipo==2){
	            titulo = '<?php echo JrTexto::_(" Copiar curso"); ?>: ' +nombrecurso;
	            pnlaccion =frm.find('.pnlaccion').hide();
	            frm.find('select#accion option[value="copiar"]').attr('selected','selected').siblings('option').removeAttr('selected');
	            frm.find('.pnlempresa').removeClass('col-md-6 col-md-12').addClass('col-md-12');
	        }
	        frm.find('select#idproyecto').val(<?php echo $this->idproyecto; ?>);
	        frm.find('input#nombre').val(nombrecurso);
	        var _md = __sysmodal({'html': frm,'titulo': titulo,zindex:true}); 
	        _md.on('click','#btn-save',function(ev){
	            ev.preventDefault();
	            ev.stopPropagation();
	            if($(this).hasClass('disabled')) return;
	            $(this).addClass('disabled');
	            var el=$(this);
	            Swal.showLoading()
	            var data=new FormData()
	            data.append('idcurso',id);
	            data.append('idcomplementario',idcc);
	            data.append('accion',_md.find('select#accion').val());
	            data.append('idproyecto',_md.find('select#idproyecto').val());
	            data.append('tipo',tipo);
	            data.append('como',_md.find('select#como').val());
	            data.append('nombre',_md.find('input#nombre').val());
	              __sysAyax({ 
	              fromdata:data,
	                url:_sysUrlBase_+'json/acad_curso/copiar',
	                showmsjok:true,
	                callback:function(rs){ 
	                    el.removeClass('disabled');
	                    //window.location.reload();
	                }               
	            });
	              setTimeout(function(){_md.find('#btn-save').removeClass('disabled');},3000);
	        }).on('change','#accion',function(ev){
	            var acc=$(this).val();
	            if(acc=='asignar'){
	                _md.find('select#como option[value="1"]').attr('selected','selected').siblings('option').removeAttr('selected');
	                _md.find('select#como').parent().parent().hide();
	            }else{
	                _md.find('select#como').parent().parent().show();
	            }
	            _md.find('#btn-save').removeClass('disabled');
	        }).on('change','#como',function(ev){
	            _md.find('#btn-save').removeClass('disabled');
	        })
	        return false;     
	    }).on('click','.item-curso .remove',function(ev){
	        ev.preventDefault();
	        ev.stopPropagation();
	        var dt =$(this).closest('.item-curso');
            var id=dt.attr('idcurso');
            var tipo=dt.attr('tipo');
            var idcc=dt.attr('idcomplementario')||0; 
            Swal.fire({
                title: '<?php echo JrTexto::_('Are you sure to delete this course?');?>',
                text: '<?php echo JrTexto::_('Everything related to it will also be eliminated'); ?>',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<?php echo JrTexto::_('Accept');?>',
                cancelButtonText: '<?php echo JrTexto::_('Cancel');?>',
            }).then((result) => {            	
                if (result.value){
                	Swal.showLoading();
                    // console.log($(this).closest(".visorCursos").parent())
                    $.post(_sysUrlBase_ + 'json/acad_curso/eliminar',{
                        'idcurso': id,
                        'tipo': tipo,
                        'eliminar': 'no',
                        'idcc':idcc
                    }, function(data){
                        Swal.close();
						if(data.code == 200){
							location.reload();
						}else{
							Swal.fire({								
								text: data.msj||'<?=JrTexto::_("Unexpected error, try later")?>',
								type: 'warning',								
								confirmButtonText: '<?php echo JrTexto::_('Accept');?>',
							})
						}
                    }, 'json');
                }
            });
	    }).on('click','.item-curso',function(ev){
	        var id=$(this).attr('idcurso');
	        var idcc=id+'&icc='+$(this).attr('idcomplementario')||0;
	        idcc=idcc+'&tipo='+$(this).attr('tipo')||1;
	        let idgrupoauladetalle=$(this).attr('idgrupoauladetalle')||'';
	        if(idgrupoauladetalle!=undefined && idgrupoauladetalle!='') idcc+='&idgrupoauladetalle='+idgrupoauladetalle;
	        window.open(_sysUrlBase_+'smart/cursos/ver/?idcurso='+idcc, '_blank');
	    });

	    $('body').on('change','#cmbgrupos',function(){
	    	var idgrupoaula = $(this).val();
	    	var filtro = [];
	    	var drawCursos = [];
	    	let cursosSeleccionados=[];
	    	console.log(CURSOS);
	    	if(CURSOS!=undefined)
			$.each(CURSOS,function(i,v){ // grupos a la categoria principal	
				if(parseInt(idgrupoaula)==parseInt(v.idgrupoaula)  || cursostutoriales.includes(parseInt(v.idcurso)) ){					
					cursosSeleccionados.push(v);
				}
			})
			console.log(cursosSeleccionados)
	    	drawListCourse(cursosSeleccionados,false);
	    });
	    $('body').on('change','#cmbcategorias',function(){
	    	var idcategoria = $(this).val();
	    	let categorias=catpadres[idcategoria];
	    	if($('select#cmbgrupos').length){
	    		let gruposaula=[];
	    		let grupostmp=[];		
				if(CURSOS!=undefined)
				$.each(CURSOS,function(i,v){ // grupos a la categoria principal
					if(idcategoria!=0 && idcategoria!=''){
						if(v.idcategoria!=undefined && grupostmp.includes(v.idgrupoaula)==false)
							if(categorias.includes(parseInt(v.idcategoria)) && v.idgrupoaula!=-1){
								let idgrupoaula=v.idgrupoaula;
								grupostmp.push(v.idgrupoaula);
								gruposaula.push({idgrupoaula:v.idgrupoaula , strgrupoaula : v.strgrupoaula});
							}
					}else{
						if(grupostmp.includes(v.idgrupoaula)==false){
							let idgrupoaula=v.idgrupoaula;
							grupostmp.push(v.idgrupoaula);
							gruposaula.push({idgrupoaula:v.idgrupoaula , strgrupoaula : v.strgrupoaula});
						}
					}
				})
				let hay=false;
				$.each(gruposaula,function(i,v){			
					$('#cmbgrupos').append('<option value="'+v.idgrupoaula+'">'+v.strgrupoaula+'</option>');
					hay=true;
				})		
				if(hay==false){ $('#cmbgrupos').append('<option value="0">Sin grupos</option>');}
				$('#cmbgrupos').trigger('change');
	    	}else{ //no hay grupos
	    		let cursosSeleccionados=[];
		    	if(CURSOS!=undefined)
				$.each(CURSOS,function(i,v){ // grupos a la categoria principal	
					if(idcategoria!=0 && idcategoria!=''){
						if(categorias.includes(parseInt(v.idcategoria))){					
							cursosSeleccionados.push(v);
						}
					}else{
						if(v.idcategoria==undefined||v.idcategoria=='0'||v.idcategoria=='')
							cursosSeleccionados.push(v);
					}
				})				
		    	drawListCourse(cursosSeleccionados);
	    	}
	    });
	    $('#cmbcategorias').trigger('change');
	});
	//simular sttripos de php
	function strripos (haystack, needle, offset) {
		//  discuss at: https://locutus.io/php/strripos/
		// original by: Kevin van Zonneveld (https://kvz.io)
		// bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
		// bugfixed by: Brett Zamir (https://brett-zamir.me)
		//    input by: saulius
		//   example 1: strripos('Kevin van Zonneveld', 'E')
		//   returns 1: 16

		haystack = (haystack + '').toLowerCase()
		needle = (needle + '').toLowerCase()

		var i = -1
		if (offset) {
			i = (haystack + '').slice(offset).lastIndexOf(needle) // strrpos' offset indicates starting point of range till end,
			// while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
			if (i !== -1) {
			  i += offset
			}
		} else {
			i = (haystack + '').lastIndexOf(needle)
		}
		return i >= 0 ? i : false
	}

	function drawListCourse(courses, showpie){
		var contenedor = $('#lscursos');
		contenedor.html('');
    	if(Object.keys(courses).length > 0){
    		courses.forEach((el)=>{
    			var posimg = false;
    			_img = el.imagen;
				posimg = strripos(_img,'static/');
				if(posimg === false) {
					_img = String.prototype.concat(_sysUrlBase_,'static/media/cursos/nofoto.jpg');
				}else {
					_img = String.prototype.concat(_sysUrlBase_,_img.substring(posimg));
				}
				var idcc = "";
				if(el.idcomplementario != undefined){
					idcc = el.idcomplementario;
				}
				var edit = '';

				if(oUSER.tuser=='s' || el.tipo == '2'){
					edit += '<div class="mask">';
					edit += '	<div class="tools tools-bottom">';
					edit += '		<a class="a-mask edit" data-toggle="popover" title="<?=JrTexto::_("Edit Course")?>"><i class="fa fa-pencil"></i></a>';
					if(el.idcomplementario==0||el.idcomplementario=='')
					edit += '		<a class="a-mask mirarcategoria" data-toggle="popover" title="<?=JrTexto::_("Edit category")?>"><i class="fa fa-cc"></i></a>';	
					edit += '		<a class="a-mask formular" data-toggle="popover" title="<?=JrTexto::_("Change course assessment formula")?>"><i class="fa fa-calculator"></i></a>';
					edit += '		<a class="a-mask remove" data-toggle="popover" title="<?=JrTexto::_("Delete course")?>"><i class="fa fa-trash"></i></a>';
					if(oUSER.tuser=='s')
					edit += '		<a class="a-mask copy" data-toggle="popover" title="Copiar curso"><i class="fa fa-copy"></i></a>';
					edit += '	</div>';
					edit += '</div>';
				}

				let html=`<div class="card card-outline card-primary visorCursos" id="card${el.idcurso}">
				          <div class="card-header">
				            <h4 class="card-title">${el.strcurso||el.nombre}</h4>				           
				          </div>
				          <div class="card-body" style="display: block;">
							<div class="image view view-first item-curso" idcurso="${el.idcurso}" idcomplementario="${idcc}" tipo="${el.tipo}" idcategoria="${el.idcategoria}" idgrupoauladetalle="${el.idgrupoauladetalle||''}" idgrupoaula="${el.idgrupoaula}">
								<img class="img-curso imgicon" src="${_img}" />
								${edit}
			          		</div>
						</div>`;
					if(showpie==true){
						html+=`<div class="card-footer">
							<label for="">${el.strgrupoaula}</label>
						</div>`
					}				
    			if(cursostutoriales.includes(parseInt(el.idcurso))){
    				if($('#card'+el.idcurso).length==0)
    			 	contenedor.prepend(`<div class="col-lg-3 col-md-4 col-sm-4">${html}</div>`);
    			}
    			else contenedor.append(	`<div class="col-lg-3 col-md-4 col-sm-4">${html}</div>`);    			
    		});
    		$('[data-toggle="popover"]').popover({
			    container: 'body',
			    trigger:'hover',
			    placement:'bottom'
			})


    	} else {
			var sincursos = `
					<div class="row">
						<div class="col-md-12">
							<h3 class="no-mensajes"><?=JrTexto::_("No assigned courses")?>...</h3>
						</div>
                    </div>
					`;
			contenedor.html(sincursos);
		}
	}
</script>