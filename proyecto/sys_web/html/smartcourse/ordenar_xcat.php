<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$imgdefecto='static/media/cursos/nofoto.jpg';
$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:2;
?>
<style type="text/css">
    .thumbnail{
        height: auto !important;
        /* padding: 1em; */
        padding: 0 !important;
    }
    .dd{
        max-width:100%;
    }
    .categoria > .dd-handle{
            border: 1px solid #4594cb;
    }
    .modulo > .dd-handle{
            border: 1px solid #f0aa89;
    }
    .curso > .dd-handle{
            border: 1px solid #ccc;
    }
    .dd-handle{ cursor: pointer; }
     .nomove, .aquino , .nomove > .dd-handle, .aquino > .dd-handle{
        cursor: no-drop;
    }
    .nomove > .dd-handle , .aquino  > .dd-handle{
        background: #ccc;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlStatic(); ?>/libs/jquery-nestable/jquery.nestable.min.css">
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="titulo-p-2"><?php echo JrTexto::_('Ordenar mis cursos ');?></h2>

                <div class="clearfix"></div>
            </div>
            <div class="col-md-12" style=" padding: 2ex;">            
                <div class="row" id="lscursos">
                    <div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                        <div class="dd nestable" en="" style="max-width: 100%;">
                            <ol class="dd-list">
                                <?php if(!empty($this->categorias)){ $ordencat=0; ?>
                                <?php foreach ($this->categorias as $k=> $v){ $ordencat++;  $idpk1=@$v["idcategoria"];  ?>   
                                    <li class="dd-item categoria" data-type="categoria" orden="<?php echo $ordencat; ?>" data-id="<?php echo @$v["idcategoria"]; ?>">
                                    <div class="dd-handle"><?php echo !empty($v["categoria"])?$v["categoria"]:'Sin Nombre'; ?></div>
                                    <?php if(!empty($v["hijos"]) || !empty($v["cursos"])){ $ordenmodulos=0; ?>
                                        <ol class="dd-list" en="<?php echo $idpk1; ?>">
                                        <?php 
                                        if(!empty($v["hijos"]))
                                        foreach ($v["hijos"] as $ks=> $mod){ 
                                            $idpk2=@$v["idcategoria"]."_".@$mod["idcategoria"]; $ordenmodulos++; ?>
                                            <li class="dd-item modulo " data-type="modulo" orden="<?php echo $ordenmodulos; ?>" data-id="<?php echo $mod["idcategoria"] ?>">
                                                <div class="dd-handle"><?php echo $mod["categoria"] ?></div>                               
                                                <?php if(!empty($mod["cursos"])){ $ordencurso=0; ?>
                                                    <ol class="dd-list" en="<?php echo $idpk2; ?>">
                                                    <?php foreach ($mod["cursos"] as $kc=> $cur){ $ordencurso++; ?>
                                                        <li class="dd-item curso " data-type="curso" orden="<?php echo $ordencurso; ?>" data-id="<?php echo $cur["idcurso"] ?>">
                                                            <div class="dd-handle"><?php echo $cur["curso"] ?></div>
                                                        </li>
                                                    <?php } ?>
                                                    </ol> 
                                                <?php }   ?>
                                            </li>
                                        <?php }
                                        if(!empty($v["cursos"])){ $ordenmodulos++; ?>                                            
                                            <?php foreach ($v["cursos"] as $kc=> $cur){ $ordenmodulos++; ?>
                                                <li class="dd-item curso " data-type="curso" orden="<?php echo $ordenmodulos; ?>" data-id="<?php echo $cur["idcurso"] ?>">
                                                    <div class="dd-handle"><?php echo $cur["curso"] ?></div>
                                                </li>
                                            <?php } ?>                                          
                                        <?php  } ?>
                                        </ol>
                                    <?php } ?> 
                                </li>
                                <?php } } ?>
                                <?php if(!empty($this->haycursoslibres)){ ?>
                                <li class="dd-item nomove cursolibre" data-type="nosave"  data-id="0">
                                    <div class="dd-handle">Cursos Libres</div>   
                                    <ol class="dd-list" >
                                    <?php foreach ($this->haycursoslibres as $kl => $kv){   //var_dump($kv);?>
                                        <li class="dd-item curso " data-type="curso"  data-id="<?php echo $kv['idcurso'];?>">
                                            <div class="dd-handle"><?php echo $kv["curso"]; ?></div>
                                        </li>
                                    <?php } ?>
                                    </ol>
                                </li>
                                <?php } ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <button class="btn btn-warning btn-cancelar"><i class="fa fa-undo"></i> Cancelar</button>
                <button class="btn btn-primary btn-guardar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/jquery-nestable/jquery.nestable.min.js"></script>
<script type="text/javascript">
    
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.dd').nestable({
        onDragStart: function (l, e){            
            l.find('ol').addClass('aquino dd-nochildren');
            e.closest('ol').removeClass('aquino dd-nochildren');
            e.find('.curso').addClass('dd-nochildren');
            if(e.hasClass('nomove')) return false;
        },
        beforeDragStop: function(l,e, p){        
            if(e.hasClass('categoria')){ 
                let index=p.children('div.dd-placeholder').index()+1;
                let hijos=p.children().length;              
                if(index>hijos-1){  
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No se puede mover En este lugar',
                      //footer: '<a href>Why do I have this issue?</a>'
                    })      
                    return false;
                }
            }
            if(p.closest('li').hasClass('curso')) {
               Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No se puede mover En este lugar',
                      //footer: '<a href>Why do I have this issue?</a>'
                    }) 
                return false;
            }
            if(!p.hasClass('aquino') && !p.hasClass('dd-nochildren') ){                    
                return true;
            }
            else {
                Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'No se puede mover En este lugar',
                      //footer: '<a href>Why do I have this issue?</a>'
                    }) 
                return false;
            }
        }
    });
    $('.btn-guardar').click(function(){
        var datos=[];
        $('.dd').children('ol').children('li').each(function(i,li){
            let _li=$(li);
            datos.push({tipo:_li.attr('data-type'),id:_li.attr('data-id'),'orden':i})
            _li.children('ol').children('li').each(function(i1,li1){
                let _li1=$(li1);
                 datos.push({tipo:_li1.attr('data-type'),id:_li1.attr('data-id'),'orden':i1})
                 _li1.children('ol').children('li').each(function(i2,li2){
                    let _li2=$(li2);
                     datos.push({tipo:_li2.attr('data-type'),id:_li2.attr('data-id'),'orden':i2})                    
                })                
            })
        })
        var formData = new FormData();        
          formData.append('datos', JSON.stringify(datos));         
          __sysAyax({
            fromdata: formData,
            url: _sysUrlBase_ + 'json/acad_curso/reordenar',
            showmsjok:true,
            //async:false,
            callback: function(rs) {
              window.location.href=_sysUrlSitio_+'/cursos'
            }
        })
        console.log(datos);
    })
    $('.btn-cancelar').click(function(){
        window.location.href=_sysUrlSitio_+'/cursos'
    })
  
});
</script>