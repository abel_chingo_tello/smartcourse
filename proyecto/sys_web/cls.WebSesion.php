<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
class WebSesion extends JrWeb
{
	private $oNegSesion;
	protected $oNegConfig;
	//public $oNegHistorialSesion;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSesion = new NegSesion;
		$this->oNegConfig = NegConfiguracion::getInstancia();
		//$this->oNegHistorialSesion = new NegHistorial_sesion;
	}
	public function defecto()
	{
		return $this->login();
	}
	public function login()
	{
		try {		
			global $aplicacion;	
			if(true === NegSesion::existeSesion()&&(empty($_REQUEST['usuario']) && empty($_REQUEST['clave']) && empty($_REQUEST['token']) )) {
				return $aplicacion->redir();
			}elseif(!empty($_REQUEST['token'])){
				$filtro=array();
				if(!empty($_REQUEST['usuario'])) $filtro['usuario']=$_REQUEST['usuario'];
				$filtro['token']=$_REQUEST['token'];
				$res=$this->oNegSesion->ingresar($filtro);
				if($res["code"]==200 && $res["islogin"]==true) return  $aplicacion->redir();
			   	echo json_encode($res);	
			}elseif(!empty($_REQUEST['usuario'])&&!empty($_REQUEST['clave'])){				
				$res=$this->oNegSesion->ingresar(array('usuario'=>$_REQUEST['usuario'],'clave'=>$_REQUEST['clave']));
				if($res["code"]==200 && $res["islogin"]==true) return  $aplicacion->redir();				
			   	echo json_encode($res);					
			   	exit();
			}else{				
				return $this->form(@$usuario);
			}
			return $this->iniciar(true);
		} catch(Exception $e){			
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			
		}
	}
	public function iniciar($entro=false){
		try {		
			global $aplicacion;
			if(false === NegSesion::existeSesion() && $entro==false){ 
				return $this->login();
			}
			return $aplicacion->redir();
			//$aplicacion->typereturn();						
			//
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>'Error al cambiar de rol<br>'.JrTexto::_($e->getMessage())));
            exit(0);			
		}
	}

	public function noroles(){
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Change empty'), true);
			$this->documento->plantilla = 'excepcion';
			$this->msj=JrTexto::_('Rol empty').'!!';
			$this->esquema = 'error/general';
			if(true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}
			return parent::getEsquema();
		} catch(Exception $e){	
			echo json_encode(array('code'=>'Error','msj'=>'Error noroles<br>'.JrTexto::_($e->getMessage())));
            exit(0);
		}		
	}
	protected function form($usuario = null)
	{
		try {		
		
			global $aplicacion;			
			if(true == NegSesion::existeSesion()){
				$aplicacion->redir();
			}	
			
			$this->usuario = $usuario;			
			$this->documento->plantilla = 'login';
			$this->esquema = 'login';
			return parent::getEsquema();			
		} catch(Exception $e) {
			
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}
	}
	
	public function salir()
	{		
		global $aplicacion;
		try {			
			if(true === NegSesion::existeSesion()){
				//$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Sesion cerrada,correctamente')));
			return $aplicacion->redir();
			exit(0);
		} catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>'Error al cerrar sesion<br>'.JrTexto::_($e->getMessage())));
			return $aplicacion->redir();
            exit(0);
		}
	}	
	public function cambiar_ambito()
	{
		try{
			global $aplicacion;			
			$usuarioAct = NegSesion::getUsuario();	
			//exit($usuarioAct);
			if(!empty($_GET['rol'])&&!empty($_GET['idrol'])) {
				$oNegSesion = new NegSesion;
				$cambio=$oNegSesion->cambiar_rol($_GET['idrol'],$_GET['rol'],$usuarioAct["idpersona"],$usuarioAct["idproyecto"],$usuarioAct["idempresa"]);
				$usuarioAct = NegSesion::getUsuario();
			}
			$aplicacion->redir();
		} catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>'Error al cambiar de rol<br>'.JrTexto::_($e->getMessage())));
            exit(0);			
		}
	}

	public function cambiarrol()
	{
		try{			
			global $aplicacion;			
			$usuarioAct = NegSesion::getUsuario();
			$cambio=false;
			if(!empty($_REQUEST['rol'])&&!empty($_REQUEST['idrol'])) {
				$oNegSesion = new NegSesion;
				$idproyecto=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];
				$idempresa=!empty($_REQUEST["idempresa"])?$_REQUEST["idempresa"]:$usuarioAct["idempresa"];
				$cambio=$oNegSesion->cambiar_rol($_REQUEST['idrol'],$_REQUEST['rol'],$usuarioAct["idpersona"],$idproyecto,$idempresa);
				$usuarioAct = NegSesion::getUsuario();
			}
			echo json_encode(array( 'code'=>200,'msj'=>'ok','data'=>$cambio));			
			exit(0);
		} catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}


	protected function iniciarHistorialSesion($lugar)
	{		
		/*$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->tipousuario = ($usuarioAct['rol']=='Alumno')?'A':'P';
		$this->oNegHistorialSesion->idusuario = $usuarioAct['idpersona'];
		$this->oNegHistorialSesion->lugar = $lugar;
		$this->oNegHistorialSesion->fechaentrada = date('Y-m-d H:i:s');
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$idHistSesion = $this->oNegHistorialSesion->agregar();
		$sesion = JrSession::getInstancia();
		$sesion->set('idHistorialSesion', $idHistSesion, '__admin_m3c');
		$sesion->set('fechaentrada',date('Y-m-d H:i:s') , '__admin_m3c');*/
	}
	protected function terminarHistorialSesion($lugar)
	{
		/*$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->idhistorialsesion = $usuarioAct['idHistorialSesion'];
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$resp = $this->oNegHistorialSesion->editar();*/
	}	
}