<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class WebDefecto extends JrWeb
{	
	public function __construct()
	{
		
		parent::__construct();
		global $aplicacion;
		$this->usuario = NegSesion::getUsuario();
		if(empty($this->usuario)) $aplicacion->redir('sesion/login');		
	}

	public function defecto()
	{
		try {
			global $aplicacion;
			//if(empty($this->usuario)) echo json_encode(array('code'=>200,'msj'=>'Usuario no logueado'));
			$this->documento->setTitulo(JrTexto::_('Administrable'), true);
			$this->documento->plantilla = 'general';
			$this->esquema = 'inicio';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	} 
}