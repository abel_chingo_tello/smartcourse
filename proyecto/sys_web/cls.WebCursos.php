<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursocategoria', RUTA_BASE);
class WebCursos extends JrWeb
{
	private $oNegAcad_categorias;
	private $oNegAcad_curso;
	private $oNegAcad_grupoauladetalle;
	private $oNegAcad_matricula;
	private $oNegProyecto;
	private $oNegAcad_grupoaula;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_categorias = new NegAcad_categorias;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegCursodetalle = new NegAcad_cursodetalle;
		$this->oNegProyecto = new NegProyecto;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_cursocategoria = new NegAcad_cursocategoria;
	}

	public function defecto()
	{
		try {
			global $aplicacion;
			$this->UrlBase = URL_RAIZ;
			$usuarioAct = NegSesion::getUsuario();
			$this->curusuario = $usuarioAct;
			$this->documento->stylesheet('card_cursos', '/tema/css/');
			//var_dump($this->curusuario);exit();
			$filtros = array();
			$this->idempresa = $this->curusuario["idempresa"];
			$this->idproyecto = $this->curusuario["idproyecto"];
			$proyecto = $this->oNegProyecto->buscar(array("idproyecto" => $this->idproyecto));
			$this->tipo_portal = $proyecto[0]["tipo_portal"];

			$this->rol = $this->curusuario["idrol"];
			$this->documento->setTitulo(JrTexto::_('cursos'), true);
			$this->documento->plantilla = 'mantenimientos';
			$this->idcategoria = !empty($_REQUEST["idcategoria"]) ? $_REQUEST["idcategoria"] : 0;

			$filtros["idproyecto"] = $usuarioAct["idproyecto"];
			//categorias del proyecto
			$this->categoriasProyecto = $this->oNegAcad_categorias->buscar($filtros);
			$this->categorias = $this->categoriasProyecto;
			$this->haycursoslibres = array();
			$haycursoslibres = array();
			$this->haycursosmatriculadosxcarrera = false;
			//if ($this->tipo_portal == "1"){
			$this->proyectos = $this->oNegProyecto->buscar();
			//var_dump($this->proyectos);
			$filtros = array();
			$filtros["allcategorias"] = true;
			$cursotutorialingles = array(
				'idcurso' =>  '678', 'nombre' =>  'Manejo y uso de la plataforma - Ingles', 'strcurso' => 'Manejo y uso de la plataforma  - Ingles',
				'imagen' =>  'static/media/cursos/curso_678/logo_678.png?id=20200511151249', 'descripcion' =>  '', 'estado' =>  '0', 'fecharegistro' =>  '2020-05-11 01:00:00',
				'idusuario' =>  '5', 'color' =>  'rgba(0, 0, 0, 0)', 'abreviado' =>  '', 'autor' =>  '', 'aniopublicacion' =>  '2020-05-11', 'tipo' =>  '1', 'sks' =>  '1',
				'idproycurso' =>  '824', 'idproyecto' =>  '7', 'idcomplementario' =>  0, 'idcategoria' =>  '32', 'idgrupoauladetalle' => '-1', 'idgrupoaula' => '-1',
				'iddocente' => '1', 'strgrupoaula' =>  '', 'strdocente' =>  'Sistema'
			);
			$cursotutorialcomputacion = array(
				'idcurso' => '207', 'strcurso' => 'Manejo y uso de la plataforma', 'nombre' => 'Manejo y uso de la plataforma',
				'imagen' => 'static/media/cursos/curso_207/logo_207.png?id=20200413163556', 'descripcion' => '', 'estado' => '0', 'fecharegistro' => '2020-04-13 01:00:00',
				'idusuario' => '1', 'color' => 'rgba(0, 0, 0, 0)', 'abreviado' => '', 'autor' => '', 'aniopublicacion' => '2020-04-13',  'tipo' => '1', 'sks' => '1',
				'idproycurso' => '451', 'idproyecto' => '7', 'idcomplementario' => 0, 'idcategoria' => '31', 'idgrupoauladetalle' => '-1', 'idgrupoaula' => '-1',
				'iddocente' => '1', 'strgrupoaula' =>  '', 'strdocente' =>  'Sistema'
			);
			
			$cursotutorialmasterclass=array();

			if ($this->rol == 1) { //administrdor
				$filtros["todos"] = false;
				$filtros["estado"] = 0;
				$this->solocursosprincipales = $proyecto[0]["solocursosprincipales"];
				$cursos = $this->oNegAcad_curso->cursos($filtros);

				$cursos2 = array();
				if (!empty($cursos["miscursos"]))
					foreach ($cursos["miscursos"] as $key => $value) {
						if ($this->solocursosprincipales == '2') {
							if (empty($value["idcomplementario"])) $cursos2[] = $value;
						} else $cursos2[] = $value;
					}
				$cursos["miscursos"] = $cursos2;
			} else if ($this->rol == 2) { // docente
				$filtros["iddocente"] = $usuarioAct["idpersona"];
				$cursos = $this->oNegAcad_grupoauladetalle->cursos($filtros);

				
				/** Bloquear cursos con grupos de estudio vencidos  _iniciobloqueo_ ***/
				$cursostmp = array();
				foreach ($cursos["miscursos"] as $key => $cur){					
					$fecha = date("Y-m-d", strtotime($cur["fecha_final"]));
					$fechaActual = date('Y-m-d');
					$ndiasextras=!empty($cur["dias_extradocente"])?$cur["dias_extradocente"]:30;
					$fecha_condias_extra=date("Y-m-d", strtotime($fecha."+ ".$ndiasextras." days"));
					//echo "fecha actual : ".$fechaActual."<br>Fecha Vencimiento : ".$fecha."<br>Fecha vencimiento con dias extra : ".$fecha_condias_extra."<br><hr><br>";
					if($fecha_condias_extra >= $fechaActual){						
					//var_dump($fecha_condias_extra);
						$cur["fecha_grupovencida"]=false;
						if($fecha<$fechaActual){							
							$cur["fecha_grupovencida"]=true;							
						}
						$cursostmp[] = $cur;
					}
				}
				$cursos["miscursos"] = $cursostmp;
				//var_dump($cursos["miscursos"]);
			} else { //alumnos u otros
				$filtros["idalumno"] = $usuarioAct["idpersona"];
				$filtros["idproyecto"] = $usuarioAct["idproyecto"];
				$filtros["estado"]=1; // solo matriculas activas;
				$cursos = $this->oNegAcad_matricula->cursos($filtros);
				//var_dump($cursos);
				#if($usuarioAct["idproyecto"]==7){ // aplicar fecha vencimiento para eiger
				$cursostmp = array();
				foreach ($cursos["miscursos"] as $key => $cur){
					$fecha = date("Y-m-d", strtotime($cur["fecha_vencimiento"]));
					$fechaActual = date('Y-m-d');
					if ($fecha >= $fechaActual)
						$cursostmp[] = $cur;
				}
				$cursos["miscursos"] = $cursostmp;
				$cursotutorialmasterclass = array(
					'idcurso' => '954', 'strcurso' => 'INDUCCIÓN SMARTENGLISH', 'nombre' => 'INDUCCIÓN SMARTENGLISH',
					'imagen' => 'static/media/cursos/curso_954/logo_954.jpeg', 'descripcion' => '', 'estado' => '1', 'fecharegistro' => '2020-04-13 01:00:00',
					'idusuario' => '1', 'color' => 'rgba(0, 0, 0, 0)', 'abreviado' => '', 'autor' => '', 'aniopublicacion' => '2020-04-13',  'tipo' => '1', 'sks' => '1',
					'idproycurso' => '0', 'idproyecto' => '46', 'idcomplementario' => 0, 'idcategoria' => '356', 'idgrupoauladetalle' => '-1', 'idgrupoaula' => '-1',
					'iddocente' => '1', 'strgrupoaula' =>  '', 'strdocente' =>  'Sistema'
				);
				$cursotutorialmasterclass2 = array(
					'idcurso' => '954', 'strcurso' => 'INDUCCIÓN SMARTENGLISH', 'nombre' => 'INDUCCIÓN SMARTENGLISH',
					'imagen' => 'static/media/cursos/curso_954/logo_954.jpeg', 'descripcion' => '', 'estado' => '1', 'fecharegistro' => '2020-04-13 01:00:00',
					'idusuario' => '1', 'color' => 'rgba(0, 0, 0, 0)', 'abreviado' => '', 'autor' => '', 'aniopublicacion' => '2020-04-13',  'tipo' => '1', 'sks' => '1',
					'idproycurso' => '0', 'idproyecto' => '46', 'idcomplementario' => 0, 'idcategoria' => '390', 'idgrupoauladetalle' => '-1', 'idgrupoaula' => '-1',
					'iddocente' => '1', 'strgrupoaula' =>  '', 'strdocente' =>  'Sistema'
				);

				#}					
			}
			$haycomputacion = false; //15
			$hayingles = false;  //24
			$haytutorialcomputacion = false;
			$haytutorialingles = false;			
			if (!empty($this->categorias)) {
				$cate_ = array();
				foreach ($this->categorias as $k => $v) {
					if ($v["idpadre"] == 0) {
						$cate_[$v["idcategoria"]] = $v;
						if (empty($cate_[$v["idcategoria"]]["tienecursos"])) {
							$cate_[$v["idcategoria"]]["tienecursos"] = false;
						}
						$cate_[$v["idcategoria"]]["cursos"] = array();
						if (!empty($cursos["miscursos"]))
							foreach ($cursos["miscursos"] as $kc => $vc) {
								if ($vc["idcategoria"] == $v["idcategoria"]) {
									$cate_[$v["idcategoria"]]["cursos"][] = $vc;
									$cate_[$v["idcategoria"]]["tienecursos"] = true;
								}
							}
					}
				}
				foreach ($this->categorias as $k => $v) {
					if ($v["idpadre"] != 0) {
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]] = $v;
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["tienecursos"] = false;
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["cursos"] = array();
						//$grupos[$v["idpadre"]]['hijos'][$v["idcategoria"]]["grupos"] = array();						
						if (!empty($cursos["miscursos"]))
							foreach ($cursos["miscursos"] as $kc => $vc) {
								if ($vc["idcategoria"] == $v["idcategoria"]) {
									if ($v['idcategoria'] == 31) {
										$haytutorialcomputacion = true;
									} else if ($v['idcategoria'] == 32) {
										$haytutorialingles = true;
									} else if ($v['idpadre'] == 15) $haycomputacion = true;
									else if ($v['idpadre'] == 24) $hayingles = true;
									$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["cursos"][] = $vc;
									$cate_[$v["idpadre"]]["tienecursos"] = true;
									$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["tienecursos"] = true;
								}
							}
					}
				}
				if ($haycomputacion && $haytutorialcomputacion == false && $this->idproyecto == 7) { // computacion 								 
					$cate_[15]["tienecursos"] = true;
					$cate_[15]['hijos'][31]["tienecursos"] = true;
					$cate_[15]['hijos'][31]["cursos"][] = $cursotutorialcomputacion;
					$haytutorialcomputacion = true;
				}
				if ($hayingles && $haytutorialingles == false  && $this->idproyecto == 7) { // ingles 								 
					$cate_[24]["tienecursos"] = true;
					$cate_[24]['hijos'][32]["tienecursos"] = true;
					$cate_[24]['hijos'][32]["cursos"][] = $cursotutorialingles;
					$haytutorialcomputacion = true;
				}
				

				//var_dump($cate_[355]['hijos'][356]);
				if (!empty($cursotutorialmasterclass)  && $this->idproyecto == 46 && !empty($cate_[355]['hijos'][356]["cursos"])){ // ingles 	
					$hijos=$cate_[355]['hijos'][356]["cursos"];
					array_unshift($hijos,$cursotutorialmasterclass);
					$cate_[355]["tienecursos"] = true;
					$cate_[355]['hijos'][356]["tienecursos"] = true;
					$cate_[355]['hijos'][356]["cursos"]=$hijos;
				}

				if (!empty($cursotutorialmasterclass2)  && $this->idproyecto == 46 && !empty($cate_[181]['hijos'][390]["cursos"])){ // ingles 	
					$hijos=$cate_[181]['hijos'][390]["cursos"];
					array_unshift($hijos,$cursotutorialmasterclass);
					$cate_[181]["tienecursos"] = true;
					$cate_[181]['hijos'][390]["tienecursos"] = true;
					$cate_[181]['hijos'][390]["cursos"]=$hijos;
				}

				$this->categorias = $cate_;
			}

			$this->haycursoslibres = array();
			if (!empty($cursos["miscursos"]))
				foreach ($cursos["miscursos"] as $kc => $vc) {
					if (empty($vc["idcategoria"])) {
						$this->haycursoslibres[] = $vc;
					}
				}

			if ($haytutorialcomputacion == false && !empty($this->haycursoslibres)  && $this->idproyecto == 7) $this->haycursoslibres[] = $cursotutorialcomputacion;

			$v = isset($_REQUEST["view"]) ? $_REQUEST["view"] : (!empty($_COOKIE["viewcursos"]) ? $_COOKIE["viewcursos"] : '');
			$_COOKIE["viewcursos"] = $v;
			setcookie("viewcursos", $v, time() + 60 * 60 * 24 * 180, $this->documento->getUrlBase());

			if ($this->tipo_portal == "1") {
				$this->esquema = 'smartcourse/vista_candado_' . (abs($this->rol)) . $v;
			} else if ($this->tipo_portal == 2) {
				$allcursos = !empty($cursos["miscursos"]) ? $cursos["miscursos"] : array();
				if ($haycomputacion  && $this->idproyecto == 7) {
					$allcursos[] = $cursotutorialcomputacion;
				}
				if ($hayingles  && $this->idproyecto == 7) {
					$allcursos[] = $cursotutorialingles;
				}
				$this->allcursos = !empty($allcursos) ? $allcursos : array();
				$this->esquema = 'smartcourse/vista_campus_' . (abs($this->rol));
			} else {
				$this->esquema = 'smartcourse/curso' . (abs($this->rol));
			}
			//SI ES MOBILE FORZAR VISTA
			if (JrTools::ismobile()) {
				$this->documento->plantilla = !empty($_REQUEST["tlp"]) ? $_REQUEST["tlp"] : "mobile/principal";
				$this->esquema = "mobile/cursos";
			}
			//var_dump($this->esquema);
			//$this->cursodesbloqueados = array('6403' => array(71)); //codigo de persona y array de cursos desbloqueados.
			//if (!empty($this->cursodesbloqueados[$usuarioAct["idpersona"]]) && $this->rol == 1) {
			//	$this->esquema = 'smartcourse/curso1_xcat_user';
			//}
			$this->documento->setTitulo(JrTexto::_('Cursos'), true);
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ordenar()
	{
		try {
			$usuarioAct = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('cursos'), true);
			$filtros["idproyecto"] = $usuarioAct["idproyecto"];
			$this->idproyecto = $usuarioAct["idproyecto"];
			$proyecto = $this->oNegProyecto->buscar(array("idproyecto" => $this->idproyecto));
			$this->categoriasProyecto = $this->oNegAcad_categorias->buscar($filtros);
			$this->categorias = $this->categoriasProyecto;
			//$this->proyectos=$this->oNegProyecto->buscar();
			$filtros = array();
			$filtros["allcategorias"] = true;
			$filtros["todos"] = false;
			$filtros["estado"] = 0;
			$this->solocursosprincipales = $proyecto[0]["solocursosprincipales"];
			$cursos = $this->oNegAcad_curso->cursos($filtros);
			$cursos2 = array();
			if (!empty($cursos["miscursos"]))
				foreach ($cursos["miscursos"] as $key => $value) {
					if ($this->solocursosprincipales == '2') {
						if (empty($value["idcomplementario"])) {
							$cur = array('curso' => $value["nombre"], 'idcurso' => $value["idcurso"], 'idcategoria' => $value["idcategoria"]);
							$cursos2[] = $cur;
						}
					} else {
						$cur = array('curso' => $value["nombre"], 'idcurso' => $value["idcurso"], 'idcategoria' => $value["idcategoria"]);
						$cursos2[] = $cur;
					}
				}
			$cursos["miscursos"] = $cursos2;
			if (!empty($this->categorias)) {
				$cate_ = array();
				foreach ($this->categorias as $k => $v) {
					if ($v["idpadre"] == 0) {
						$cat = array('categoria' => $v["nombre"], 'idcategoria' => $v["idcategoria"], 'orden' => $v["orden"]);
						$cate_[$v["idcategoria"]] = $cat;
						if (empty($cate_[$v["idcategoria"]]["tienecursos"])) {
							$cate_[$v["idcategoria"]]["tienecursos"] = false;
						}
						$cate_[$v["idcategoria"]]["cursos"] = array();
						if (!empty($cursos["miscursos"]))
							foreach ($cursos["miscursos"] as $kc => $vc) {
								if ($vc["idcategoria"] == $v["idcategoria"]) {
									$cate_[$v["idcategoria"]]["cursos"][] = $vc;
									$cate_[$v["idcategoria"]]["tienecursos"] = true;
								}
							}
					}
				}
				foreach ($this->categorias as $k => $v) {
					if ($v["idpadre"] != 0) {
						$cat = array('categoria' => $v["nombre"], 'idcategoria' => $v["idcategoria"], 'orden' => $v["orden"]);
						//$cate_[$v["idcategoria"]] = $cat;
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]] = $cat;
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["tienecursos"] = false;
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["cursos"] = array();
						if (!empty($cursos["miscursos"]))
							foreach ($cursos["miscursos"] as $kc => $vc) {
								if ($vc["idcategoria"] == $v["idcategoria"]) {
									$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["cursos"][] = $vc;
									$cate_[$v["idpadre"]]["tienecursos"] = true;
									$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["tienecursos"] = true;
								}
							}
					}
				}
				$this->categorias = $cate_;
			}
			$this->haycursoslibres = array();
			if (!empty($cursos["miscursos"]))
				foreach ($cursos["miscursos"] as $kc => $vc) {
					if (@$vc["idcategoria"] == 0) $this->haycursoslibres[] = $vc;
				}
			$this->esquema = 'smartcourse/ordenar_xcat';
			$this->documento->plantilla = 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	//pasos para crear cursos
	public function crear()
	{
		try {
			global $aplicacion;
			$this->curusuario = NegSesion::getUsuario();
			$this->documento->script('jquery.dataTables.min', '/libs/othersLibs/dataTables/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/othersLibs/dataTables/');
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('moment', '/libs/moment/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('conferencias', '/libs/othersLibs/conferencias/');
			$this->documento->script('conferencias', '/libs/othersLibs/conferencias/');
			$this->documento->stylesheet('fecha-limite-tapr', '/libs/othersLibs/fecha-limite-tapr/');
			$this->documento->script('fecha-limite-tapr', '/libs/othersLibs/fecha-limite-tapr/');
			$this->documento->script('mine-crud-tarea', '/libs/othersLibs/crud-tarea/');
			$this->documento->script('mine-submenu', '/libs/othersLibs/submenu/');
			$this->documento->stylesheet('mine-submenu', '/libs/othersLibs/submenu/');
			$this->documento->script('mine-crud-curso', '/libs/othersLibs/crud-curso/');
			$this->documento->script('mine2', '/libs/othersLibs/mine/js/');
			$this->documento->script('mine-vimeo', '/libs/othersLibs/vimeo/');
			$this->documento->script('curso-crear-vimeo', '/libs/othersLibs/vimeo/');
			$this->documento->stylesheet('mine-crud-curso', '/libs/othersLibs/crud-curso/');
			$this->documento->script('tinymce.min', '/libs/tinymce54/');



			$this->idproyecto = $this->curusuario["idproyecto"];
			$this->idcurso = (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') ? $_REQUEST["idcurso"] : 0;
			$this->idcomplementario = (isset($_REQUEST["icc"]) && @$_REQUEST["icc"] != '') ? $_REQUEST["icc"] : 0;
			$this->idgrupoauladetalle = (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') ? $_REQUEST["idgrupoauladetalle"] : 0;
			$this->curso = array();
			$this->categoriasdelcurso = array();
			$this->temas = array();
			$this->validar = false;
			if (!empty($this->idcurso)) {
				$this->tipo = (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') ? $_REQUEST["tipo"] : 0;
				$this->idcategoria = (isset($_REQUEST["idcategoria"]) && @$_REQUEST["idcategoria"] != '') ? $_REQUEST["idcategoria"] : 0;
				$this->curso = $this->oNegAcad_curso->buscar(array('idcurso' => $this->idcurso, 'idproyecto' => $this->idproyecto));

				if (empty($this->curso[0])) $this->curso = $this->oNegAcad_curso->buscar(array('idcurso' => $this->idcurso));
				if (!empty($this->curso)) {
					$this->curso = $this->curso[0];
					if (!isset($this->curso["idcomplementario"])) {
						$this->curso["idcomplementario"] = 0;
					}
					if ($this->idcomplementario != 0) {
						// $this->curso = $this->curso[0];
						$idcurso = $this->curso["idcurso"];
						$filtros_ = array();
						$filtros_["complementario"] = true;
						$filtros_["idcursoprincipal"] = $this->curso["idcurso"];
						$filtros_["idcategoria"] = @$_REQUEST["cat"];
						if (!empty($this->idcomplementario) && empty($_REQUEST["grupo"])) {
							$filtros_["complementarios"] = true;
							$filtros_["idcurso"] = $this->idcomplementario;
							$filtros_["estado"] = 0;
							$vc_ = $this->oNegAcad_curso->_cursos($filtros_);
						} else {
							$filtros_["idgrupoaula"] = @$_REQUEST["grupo"];
							$vc_ = $this->oNegAcad_curso->buscarcursos($filtros_);
						}
						if (count($vc_) > 0) {
							$vc_ = $vc_[0];
							$this->curso = array_merge($this->curso, $vc_);
							$this->curso["idcomplementario"] = $vc_["idcurso"];
							$this->curso["idcurso"] = $idcurso;
						} else {
							$this->curso["tipo"] = 1;
						}
						$this->validar = true;
					}
					$this->categoriasdelcurso = $this->oNegAcad_curso->buscarxcategoria(array('idcurso' => $this->curso["idcurso"], 'solocategoria' => true));
					if ($this->idcomplementario != 0) {
						$this->temas = $this->oNegCursodetalle->buscarconnivel(array('idcurso' => $this->idcomplementario, 'complementario' => true));
					} else {
						$this->temas = $this->oNegCursodetalle->buscarconnivel(array('idcurso' => $this->curso["idcurso"]));
					}
					// echo json_encode($this->temas);exit();
				}
			}
			// echo json_encode($this->curso);exit();
			$this->rol = $this->curusuario["idrol"];
			$this->tuser = $this->curusuario["tuser"];
			$this->tipocurso = "2";
			if ($this->rol == "1" && $this->tuser == "s") {
				$this->tipocurso = "1";
			}
			#$this->idgrupoauladetalle = (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') ? $_REQUEST["idgrupoauladetalle"] : -1;
			$this->documento->setTitulo(JrTexto::_('cursos'), true);
			$this->documento->plantilla = 'mantenimientos';
			$this->esquema = 'smartcourse/curso_crear';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function asignar()
	{
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			$this->esquema = 'smartcourse/asignar';
			$this->documento->plantilla = 'mantenimientos';
			if ($usuarioAct["tuser"] != 's') {
				$this->esquema = 'error/general';
				$this->msj = 'Solo los súper Administradores tienen acceso a esta opción.<br>Comuníquese con su Representante.';
				return parent::getEsquema();
			}
			$this->documento->stylesheet('card_cursos', '/tema/css/');
			$filtros = array();
			$usuarioAct = NegSesion::getUsuario();
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			//if(!empty($_REQUEST["sqlasignados"])){ 
			//	$filtros["sqlasignados"]=true;
			//	if(!empty($_REQUEST["noasignados"])) $filtros["noasignados"]=true;
			//}
			$filtros["sqlasignados"] = true;
			$filtros["noasignados"] = true;
			$filtros["idproyecto"] = !empty($_REQUEST["idproyecto"]) ? $_REQUEST["idproyecto"] : $usuarioAct["idproyecto"];
			$this->datos = $this->oNegAcad_curso->buscar($filtros);
			$this->documento->setTitulo(JrTexto::_('Asignar curso'), true);
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function categorias()
	{
		try {
			global $aplicacion;
			$user = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('cursos'), true);
			$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : 0;
			$idcc = !empty($_REQUEST["idcc"]) ? $_REQUEST["idcc"] : 0;
			$this->categorias = $this->oNegAcad_categorias->buscar(array('idproyecto' => $user['idproyecto']));
			$idcats = array();
			if (!empty($categorias)) {
				foreach ($categorias as $k => $v) {
					$idcats[] = $v["idcategoria"];
				}
			}
			if ($idcc == 0) { // buscar como curso principal				
				$this->curso = $this->oNegAcad_curso->_cursos(array('idcurso' => $idcurso, 'estado' => 0));
				$cursoscat = $this->oNegAcad_cursocategoria->buscar(array('idcategoria' => $idcats, 'idcurso' => $idcurso));
				if (!empty($this->curso[0])) {
					$this->curso[0]["idcategoria"] = !empty($cursoscat[0]["idcategoria"]) ? $cursoscat[0]["idcategoria"] : 0;
				}
			} else { // buscar como curso extension
				$this->curso = $this->oNegAcad_curso->_cursos(array('idcurso' => $idcc, 'idcursoprincipal' => $idcurso, 'complementarios' => true, 'estado' => 0));
			}
			$this->documento->plantilla = 'mantenimientos';
			$this->esquema = 'smartcourse/curso_categorias';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
