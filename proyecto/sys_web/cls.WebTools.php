<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_SITIO') or die();
//JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegRecord', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHerramientas', RUTA_BASE);
//JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
class WebTools extends JrWeb
{

	protected $oNegActividad;
    protected $oNegRecord;
    protected $oNegHerramientas;
    protected $oNegResources;  
    protected $oNegNiveles;
    protected $oNegCursoDetalle;
    protected $oNegHistorial_sesion;

    public function __construct()
    {
    	//$this->usuarioAct = NegSesion::getUsuario();
        $this->oNegHerramientas = new NegHerramientas;
        //$this->oNegRecord = new NegRecord;
        //$this->oNegResources = new NegResources;
        //$this->oNegActividad=new NegActividad;
        //$this->oNegNiveles = new NegNiveles;
        //$this->oNegCursoDetalle = new NegAcad_cursodetalle;
        //$this->oNegHistorial_sesion = new NegHistorial_sesion;
        parent::__construct(); 
    }

    public function defecto(){
        return false;
    }

    public function json_buscarGames(){
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            $filtros=array();
            $filtros["titulo"]=!empty($_REQUEST["txtBuscar"])?$_REQUEST["txtBuscar"]:'';
            $filtros["idnivel"]=!empty($_REQUEST["nivel"])?$_REQUEST["nivel"]:'';
            $filtros["idunidad"]=!empty($_REQUEST["unidad"])?$_REQUEST["unidad"]:'';
            $filtros["idactividad"]=!empty($_REQUEST["actividad"])?$_REQUEST["actividad"]:'';
            $juegos=$this->oNegHerramientas->buscarJuegos($filtros);
            $data=array('code'=>'ok','data'=>$juegos);
            echo json_encode($data);
            exit();
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }            

}