<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-08-2018 
 * @copyright	Copyright (C) 16-08-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegProyecto_cursos', RUTA_BASE, 'sys_negocio');
class WebProyecto_cursos extends JrWeb
{
	private $oNegProyecto_cursos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegProyecto_cursos = new NegProyecto_cursos;				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idproycurso"])&&@$_REQUEST["idproycurso"]!='')$filtros["idproycurso"]=$_REQUEST["idproycurso"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			//var_dump($filtros);
			$this->datos=$this->oNegProyecto_cursos->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Proyecto_cursos'), true);
			$this->esquema = 'proyecto_cursos-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Proyecto_cursos').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegProyecto_cursos->idproycurso = @$_GET['id'];
			$this->datos = $this->oNegProyecto_cursos->dataProyecto_cursos;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Proyecto_cursos').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'proyecto_cursos-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idproycurso"])&&@$_REQUEST["idproycurso"]!='')$filtros["idproycurso"]=$_REQUEST["idproycurso"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];			
			$this->datos=$this->oNegProyecto_cursos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarProyecto_cursos(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idproycurso)) {
				$this->oNegProyecto_cursos->idproycurso = $idproycurso;
				$accion='_edit';
			}else{
				if(!empty($idproyecto)&&!empty($idcurso)){
					$datos=$this->oNegProyecto_cursos->buscar(array('idproyecto'=>$idproyecto,'idcurso'=>$idcurso));
					if(empty($datos[0])){
						$this->oNegProyecto_cursos->idproyecto=@$idproyecto;
						$this->oNegProyecto_cursos->idcurso=@$idcurso;
						$res=$this->oNegProyecto_cursos->agregar();
            	 		echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
					}else{
						$res=$this->oNegProyecto_cursos->editar();
            			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
					}
				}else{
					echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));					
				}
			}
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$this->oNegProyecto_cursos->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	} 
}