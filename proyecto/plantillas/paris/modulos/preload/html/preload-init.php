<?php $idrol=!empty($this->usuarioAct["idrol"])?$this->usuarioAct["idrol"]:2;
 ?>
<link href="<?php echo $this->documento->getUrlTema()?>/css/preload.css" rel="stylesheet">
<style>
#loader-wrapper .loader-section{
    background: #000000dd !important;
}
</style>
<div id="loader-wrapper">
    <div id="loader">
      <img src="<?php echo $this->documento->getUrlStatic()?>/media/cargando.gif" class="img-fluid">
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<div id="pnltiempoplataforma" class="text-center" style=" position: fixed; background: #f1f4f6c7;  border-radius: 1ex;  bottom: 1em;  font-weight: 700;  padding: 1ex;  color: black;  left: 50px;">
   <span id="tiempoenplataforma" data-tipo="r" data-start="<?php  echo $idrol==3?'00:05:00':($idrol==2?'00:20:00':'00:30:00')?>"></span>
   <small class="form-text text-muted">Tiempo para cerrar sesion</small>
</div>
<script type="text/javascript" src="<?php echo URL_BASE ?>/static/libs/chingo/achtcronometrostorage.js"></script>
<script type="text/javascript">
var _systiempoplataforma='';
$(document).ready(function() {    
    setTimeout(function(){
        $('body').addClass('loaded');        
       // $('h1').css('color','#222222');
    }, 1000);
    _systiempoplataforma=$('#tiempoenplataforma').achtcronometro2({
            callbacktermino:function(){ localStorage.setItem('statusventanaid','close'); $('a[data-href="cerrarsesion"]').trigger('click')},
           // callbackreset:function(){console.log('reset')}
    });
    $(window).mousemove(function(){
        if(_systiempoplataforma.length)_systiempoplataforma.trigger('reiniciar');
    })
});
</script>