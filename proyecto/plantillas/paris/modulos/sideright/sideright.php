<?php
/**
 * @autor		Alvaro Alonso Cajusol Vallejos
 * @fecha		10/01/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS);
JrCargador::clase('modulos::sideright::Sideright', RUTA_TEMA);
$oMod = new Sideright;
echo $oMod->mostrar(@$atributos['posicion']);