<?php 
defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';
$rutastatic=$this->documento->getUrlStatic();
$idrol = $this->usuario["idrol"];
?>
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
<!-- Left navbar links -->
<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
  </li>
</ul>

<!-- SEARCH FORM -->
<!-- <form class="form-inline ml-3">
  <div class="input-group input-group-sm">
    <input class="form-control form-control-navbar" type="search" placeholder="Buscar" aria-label="Search">
    <div class="input-group-append">
      <button class="btn btn-navbar" type="submit">
        <i class="fas fa-search"></i>
      </button>
    </div>
  </div>
</form> -->

<!-- Right navbar links -->
<ul class="navbar-nav ml-auto">

  <!-- Notifications Dropdown Menu -->
  <li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="#">
      <i class="fa fa-cogs"></i>
      <!-- <span class="badge badge-warning navbar-badge">15</span> -->
    </a>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
    <?php 
      if(count($this->usuario['roles']) > 1|| $this->usuario["tuser"]=='s'){ 
    ?>
        <a class="dropdown-item" style="cursor: pointer;" onclick="__url__('<?php echo URL_BASE . 'sesion/cambiarrol'; ?>')">
          <i class="mr-2 fa fa-cubes fa-fw"></i> <?php echo ucfirst(JrTexto::_("Change rol"));?>
        </a>
    <?php 
      } 
    ?>
      <a class="dropdown-item" style="cursor: pointer;" onclick="__url__('<?php echo URL_BASE . 'personal/perfil'; ?>')">
        <i class="mr-2 fa fa-address-book fa-fw"></i>  <?php echo ucfirst(JrTexto::_("My profile"));?>
      </a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" style="cursor: pointer;">
        <i class="mr-2 fa fa-unlock-alt fa-fw"></i> <?php echo ucfirst(JrTexto::_('Change password')); ?>
      </a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" style="cursor: pointer;" onclick="cerrarSesion();">
        <i class="mr-2 fa fa-power-off fa-fw"></i> <?php echo ucfirst(JrTexto::_("Sign off"));?>
      </a>
    </div>
  </li>
  <?php if($this->usuario['tuser'] == "s"){ ?>
  <li class="nav-item">
    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
      <i class="fas fa-charging-station"></i>
    </a>
  </li>
  <?php } ?>
</ul>
</nav>
<script>
function cerrarSesion(){
  $.post(_sysUrlBase_+'json/sesion/salir', function(data, status){
    if(data.code==200){
      window.location.href=_sysUrlSitio_;
    }
  },'json');
}
</script>