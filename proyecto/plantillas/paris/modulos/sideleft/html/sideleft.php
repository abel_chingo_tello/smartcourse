<?php 
defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';
$rutastatic=$this->documento->getUrlStatic();

$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$imgdefecto=$rutastatic."/media/web/nofoto.jpg";
$imgfoto=$rutastatic."/media/usuarios/user_avatar.jpg";
$jsonlogin='{logo1:""}';
$usu='';
if(!empty($this->usuario)) $usu=$this->usuario;
if(!empty($this->proyecto)){
    $py=$this->proyecto;
    $jsonlogin=$py["jsonlogin"];
}
$frm=!empty($this->datos)?$this->datos:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'static/media/usuarios/user_avatar.jpg';
$ifoto=strripos($fotouser, "static/");
if($ifoto!='') $fotouser=$this->documento->getUrlBase().substr($fotouser,$ifoto);
if($ifoto==''){
  $ifoto=strripos($fotouser, "/");
  $ifoto2=strripos($fotouser, ".");
  if($ifoto!='' && $ifoto2!='') $fotouser=$this->documento->getUrlBase().'static/media/usuarios'.substr($fotouser,$ifoto);
  elseif($ifoto2!='') $fotouser=$this->documento->getUrlBase().'static/media/usuarios/'.$fotouser;
  else $fotouser=$this->documento->getUrlBase().'static/media/usuarios/user_avatar.jpg';
}
$strrole=array(1=>'Administrator',2=>'Teacher',3=>'Student');
if(!empty($strrole[@$usu["idrol"]])){
  $strrole=ucfirst(JrTexto::_($strrole[@$usu["idrol"]]));
}else
  $strrole=ucfirst(@$usu["rol"]);
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
<!-- Brand Logo -->
<a href="./" class="brand-link">
  <img src="<?php echo URL_BASE . $this->logo_emp; ?>" class="brand-image img-circle elevation-2" tyle="height: 2.1rem; width: 2.1rem;">
  <span class="brand-text font-weight-light"><strong style="font-weight: bold;"><?php echo $this->nomempresa; ?></strong></span>
</a>

<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="<?php echo $fotouser;?>" class="img-circle elevation-2" style="height: 2.1rem; width: 2.1rem;">
    </div>
    <div class="info">
      <a class="d-block"><?php echo $usu["nombre_full"]; ?></a>
    </div>
  </div>
  <div class="user-panel mt-3 pb-3 mb-3 text-center" style="margin-top: 0 !important;">
    <a class="nav-header" style="padding-bottom: 0; margin-bottom: 0;"><b><?php echo ucfirst(JrTexto::_('Role')); ?>:</b> <?php echo @$strrole; ?></a>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" id="mostrarmodulos"></ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>

<script>
  var py=JSON.parse(`<?php echo !empty($jsonlogin)?str_replace('\n','<br>',$jsonlogin):'{}';?>`);
  var url_media='<?php echo URL_MEDIA;?>';
  var imgdefecto='<?php echo $imgdefecto; ?>';
  var fotodefecto='<?php echo $imgfoto ?>';
  var jsonproyecto=JSON.parse('<?php echo $jsonlogin;?>');
  var idproyecto=parseInt('<?php echo $py["idproyecto"]; ?>');
  var idempresa=parseInt('<?php echo $py["idempresa"]; ?>');
  var dtuserlogin=<?php echo json_encode($this->usuario); ?>;
  var idrol=parseInt(dtuserlogin.idrol);
  _menus_ = py.menus;

  function buscarArray(array, key, value, retorno = "boolean"){
      for (i in array) {
        if(array[i][key] == value){
          if(retorno === "boolean"){
            return true;
          } else if(retorno === "int"){
            return i;
          }
        }
      }
      if(retorno === "boolean"){
        return false;
      } else if(retorno === "int"){
        return -1;
      }
  }

    $.post(_sysUrlBase_+'json/modulos/listado', function(data, status){
        var url_ = window.location.href.split("#");
        $.each(data.data,function(i,v){
          var idmodulo=v.idmodulo||0;
          var add = buscarArray(_menus_, "id", idmodulo);
          if(add){
            if(idmodulo==7 || idmodulo==1){
                if(idrol!=10 && idrol!=1){
                  add=false;
                }
            }
          }
          if (add){
            var html = '';
            html += '<li class="nav-item" data-id="'+v.idmodulo+'" data-link="'+v.link+'" data-nombre="'+v.nombre+'" data-icon="'+v.icono+'" data-nombretraducido="'+v.nombretraducido+'">';
            html += '<a id="mnu'+v.idmodulo+'" class="mnus nav-link" style="cursor: pointer;" onclick="__url__(' + "'" + _sysUrlBase_ + v.link + "'" +',' + v.idmodulo + ')">';
            html += '<i class="nav-icon fas fa ' + v.icono +' fa-fw"></i>';
            html += '<p>' + v.nombretraducido + '</p>';
            html += '</a>';
            html += '</li>';
            $('#mostrarmodulos').append(html);
            if(url_.length > 1){
                var id = window.atob(url_[1]);
                if(parseInt(v.idmodulo) == parseInt(id)){
                  __url__(v.link, id);
                }
            } else {
              __url__("<?php echo URL_BASE; ?>proyecto/cursos/", 8);
            }
          }
        });
    },'json');
</script>