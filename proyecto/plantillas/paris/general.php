<?php 
defined('_BOOT_') or die(''); $version=!empty(_version_)?_version_:'1.0';
$rutastatic=$documento->getUrlStatic();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EDUKT</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $rutastatic;?>/tema/paris/adminLTE3/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $rutastatic;?>/tema/paris/adminLTE3/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/sweetalert/sweetalert2.all.min.js"></script>
    <!-- jQuery -->
    <script src="<?php echo $rutastatic;?>/tema/paris/adminLTE3/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $rutastatic;?>/tema/paris/adminLTE3/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $rutastatic;?>/tema/paris/adminLTE3/dist/js/adminlte.min.js"></script>
    <script type="text/javascript" src="<?php echo $rutastatic;?>/tema/js/funciones.js?vs=<?php echo $version; ?>"></script>
    <script>
        var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>';
        var sitio_url_base=_sysUrlSitio_ = '<?php echo $documento->getUrlSitio()?>'; 
        var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
        var _sysUrlStatic_ = '<?php echo $rutastatic;?>';
        var _menus_ = new Array();
    </script>
    <style>
        .iframecontent {
            border: 0px;
            width: 100%;
            height: calc(100vh - 4px);
            margin: 0px;
            padding: 0px;
            border-top-left-radius: 0.8ex;
        }
    </style>
  </head>
  <body class="hold-transition sidebar-mini">
    <div class="wrapper">
      <!-- Navbar -->
      <jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <jrdoc:incluir tipo="modulo" nombre="sideleft" /></jrdoc:incluir>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content">
            <iframe id="iFrmContent" src="<?php echo URL_BASE; ?>proyecto/cursos/" frameborder="0" class="iframecontent"></iframe>
        </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <jrdoc:incluir tipo="modulo" nombre="pie" /></jrdoc:incluir>

      <!-- Control Sidebar -->
      <jrdoc:incluir tipo="modulo" nombre="sideright" /></jrdoc:incluir>
      <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $rutastatic;?>/tema/paris/adminLTE3/dist/js/demo.js"></script>
    <?php
        $configs = JrConfiguracion::get_();
        require_once(RUTA_SITIO . 'ServerxAjax.php');
        echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
    ?>
    <script>
    function __url__(enlace, mnu = null){
        $("#iFrmContent").attr("src", enlace);
        $(".mnus").removeClass("active");
        if(mnu !== null){
            $("#mnu"+mnu).addClass("active");
            window.location.href = "#" + window.btoa(mnu);
        }
    }
    </script>
  </body>
</html>