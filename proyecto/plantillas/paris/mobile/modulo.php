<?php 
defined('_BOOT_') or die(''); $version=!empty(_version_)?_version_:'1.0';
$rutastatic=$documento->getUrlStatic();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- template mantenimiento--> 
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google" content="notranslate" />
    <link href="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL_BASE ?>frontend/plantillas/paris/mobile/assets/css/index.css" />
    <!-- Font Awesome -->
    <link href="<?php echo $rutastatic;?>/tema/paris/gentelella/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo $rutastatic;?>/tema/paris/gentelella/nprogress/nprogress.css" rel="stylesheet">

    <link href="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo $rutastatic;?>/tema/paris/gentelella/build/css/custom.min.css" rel="stylesheet">

    <link href="<?php echo $rutastatic;?>/tema/css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $rutastatic;?>/tema/css/hover.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $rutastatic;?>/tema/css/pnotify.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $rutastatic;?>/libs/datepicker/datetimepicker.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $rutastatic;?>/libs/sliders/slick/slick.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $rutastatic;?>/libs/sliders/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $rutastatic;?>/libs/alert/jquery-confirm.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $rutastatic;?>/libs/datatable1.10/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $rutastatic;?>/libs/datatable1.10/extensions/Buttons/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $rutastatic;?>/libs/minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css">
    <!--link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" type="text/css"-->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css?vs=<?php echo $version; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_inicio.css?vs=<?php echo $version; ?>" rel="stylesheet" type="text/css">    
    <!-- <script type="text/javascript" src="<?php echo $rutastatic;?>/tema/js/jquery.min.js?vs=<?php echo $version; ?>"></script> -->
    <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/poppers/dist/umd/popper.min.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery-ui.min.js"></script>
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/sweetalert/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="<?php echo $rutastatic;?>/tema/js/pnotify.min.js"></script>
    <script type="text/javascript" src="<?php echo $rutastatic;?>/tema/js/funciones.js?vs=<?php echo $version; ?>"></script>
    <script type="text/javascript" src="<?php echo $rutastatic;?>/js/inicio.js?vs=<?php echo $version; ?>"></script>
    <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/minicolors/jquery.minicolors.min.js?vs=<?php echo $version; ?>"></script>
    <script>
            var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>';
            var sitio_url_base=_sysUrlSitio_ = '<?php echo $documento->getUrlSitio()?>'; 
            var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
            var _sysUrlStatic_ = '<?php echo $rutastatic;?>';</script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/pronunciacion.js?vs=<?php echo $version; ?>"></script>
    <jrdoc:incluir tipo="cabecera" />
    <!-- <script src="<?php echo $rutastatic;?>/tema/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/sliders/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $documento->getUrlTema(); ?>/css/frm.css">
    <style type="text/css">
        form{ width: 100%; }
        .btn-activar i{color:red;}
        .btn-activar.active i{ color:blue; } 
        i.fa-edit{color:orange;}
        i.fa-trash{color:red;}
    </style>
    <link href="<?php echo $rutastatic;?>/tema/paris/propio/template.css?vs=<?php echo $version; ?>" rel="stylesheet">
    <script>var navBarParisReturnText = "<?php echo JrTexto::_('Back'); ?>";</script>
    <script src="<?php echo $rutastatic;?>/tema/paris/propio/template.js?vs=<?php echo $version; ?>"></script>
</head>
<body style="padding-top: 10px;">
<jrdoc:incluir tipo="modulo" nombre="preload" /></jrdoc:incluir>
<div class="container-fluid">
    <jrdoc:incluir tipo="mensaje" />
    <jrdoc:incluir tipo="recurso" />
</div>
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>

    <!-- <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
   <!-- <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap/dist/js/bootstrap.bundle.min.js"></script> -->
    <!-- FastClick -->
    <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/raphael/raphael.min.js"></script>
    <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/moment/min/moment.min.js"></script>
    <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?php echo $documento->getUrlStatic()?>/libs/moment/moment.js?vs=<?php echo $version; ?>"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.js?vs=<?php echo $version; ?>"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/datepicker/locales/moment-with-locales.js?vs=<?php echo $version; ?>"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/fullcalendar/fullcalendar.js?vs=<?php echo $version; ?>"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/fullcalendar/locale-all.js?vs=<?php echo $version; ?>"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/alert/jquery-confirm.min.js?vs=<?php echo $version; ?>"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/datatable1.10/media/js/jquery.dataTables.min.js?vs=<?php echo $version; ?>"></script>
<script type="text/javascript" src="<?php echo $rutastatic; ?>/js/asistentetooltip.js?vs=<?php echo $version; ?>"></script>  
<jrdoc:incluir tipo="docsCss" />
<jrdoc:incluir tipo="docsJs" />
<!-- <script type="text/javascript" src="<?php //echo $documento->getUrlStatic()?>/tema/js/jquery.ui.touch.min.js?vs=<?php //echo $version; ?>"></script> -->
<script src="<?php echo URL_BASE ?>frontend/plantillas/paris/mobile/assets/js/sidebar.js"></script>
<script>
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    });
  }
</script>
</body>
</html>