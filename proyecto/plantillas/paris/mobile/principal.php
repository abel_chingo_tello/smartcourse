<?php 
  defined('_BOOT_') or die(''); $version=!empty(_version_)?_version_:'1.0';
  $user=NegSesion::getUsuario();
  $empresasconchat=array(27,46);
  $rutastatic=$documento->getUrlStatic();
?>
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="<?php echo URL_BASE ?>frontend/plantillas/paris/mobile/assets/css/index.css" />
  <link href="<?php echo $documento->getUrlStatic()?>/tema/paris/gentelella/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="<?php echo $rutastatic;?>/tema/paris/gentelella/nprogress/nprogress.css" rel="stylesheet">
  <link href="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  
  <link href="<?php echo $rutastatic;?>/tema/css/animate.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $rutastatic;?>/tema/css/hover.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $rutastatic;?>/tema/css/pnotify.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $rutastatic;?>/libs/datepicker/datetimepicker.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $rutastatic;?>/libs/sliders/slick/slick.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo $rutastatic;?>/libs/sliders/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo $rutastatic;?>/libs/alert/jquery-confirm.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $rutastatic;?>/libs/datatable1.10/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $rutastatic;?>/libs/datatable1.10/extensions/Buttons/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $rutastatic;?>/libs/minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css?vs=<?php echo $version; ?>" rel="stylesheet" type="text/css">
  <link href="<?php echo $documento->getUrlTema()?>/css/colores_inicio.css?vs=<?php echo $version; ?>" rel="stylesheet" type="text/css">    

  <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/poppers/dist/umd/popper.min.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery-ui.min.js"></script>
  
  <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/sweetalert/v2.10/sweetalert2.all.min.js"></script> 
  <script type="text/javascript" src="<?php echo $rutastatic;?>/tema/js/pnotify.min.js"></script>
  

  <script>
    var _sysUrlBase_ = '<?php echo $documento->getUrlBase() ?>';
    var sitio_url_base = _sysUrlSitio_ = '<?php echo $documento->getUrlSitio() ?>';
    var _sysIdioma_ = '<?php echo $documento->getIdioma() ?>';
    var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';
    var _menus_ = new Array();
  </script>
  
  <script type="text/javascript"src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js?vs=<?php echo $version; ?>"></script>
  <script type="text/javascript" src="<?php echo $rutastatic;?>/js/inicio.js?vs=<?php echo $version; ?>"></script>
  <script type="text/javascript" src="<?php echo $rutastatic;?>/libs/minicolors/jquery.minicolors.min.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/js/pronunciacion.js?vs=<?php echo $version; ?>"></script>

  <jrdoc:incluir tipo="cabecera" />
  <style>
    .chatwith{ bottom: calc(10px + 55px)!important; }
    .navbar_mobile{ background: white!important; }
  </style>
</head>

<body>
  <!-- ===============  TOP  =================== -->
  <nav class="navbar">
    <div class="container_icon_menu_nav">
      <span class="icon icon-menu" onclick="opensidebar()"></span>
    </div>
    <div class="container_txt_nav">
      <label class="txt_label_nav"><?php echo ucfirst(JrTexto::_('Active Time')); ?></label>
      <p class="txt_counter_nav">01:29:59</p>
    </div>

  </nav>
  <!-- ===============  MODAL CONFIGURATION    =================== -->
  <div class="modal-container" id="modal_config">
    <div class="modal"  >
      <span class="icon-close-modal" onclick="closeModal('modal_config')">X</span>
      <div class="config-container">
        <ul class="config-list">
          <li><a href="<?=URL_BASE?>personal/perfil"  ><?php echo ucfirst(JrTexto::_("My profile"));?></a></li>
          <li><a href="#"><?php echo ucfirst(JrTexto::_("Change Password"));?></a></li>
          <li><a href="javascript:void(0)" class="onCerrarSesion"><?php echo ucfirst(JrTexto::_("Sign off"));?></a></li>
        </ul>
      </div>
    </div>


  </div>
  <jrdoc:incluir tipo="recurso" />
  <script>
    sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
  </script>


  <!-- ===============  BOTTOM  =================== -->
  <div class="container_icon_bottom">
    <li class="circle"><a href=""><span class="icon icon-donwload"></span></a></li>
    <li class="circle"><a href=""><span class="icon icon-traslate"></span></a></li>
    <li class="circle"><a href="#" onclick="openModal('modal_config')"><span class="icon icon-config"></span></a></li>
  </div>


  <!-- ===============  SIDEBAR    =================== -->
  <jrdoc:incluir tipo="modulo" nombre="sideleft" posicion="sideleft_mobile" />
  </jrdoc:incluir>

  <!-- FastClick -->
  <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/nprogress/nprogress.js"></script>
  <!-- morris.js -->
  <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/raphael/raphael.min.js"></script>
  <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/morris.js/morris.min.js"></script>
  <!-- bootstrap-progressbar -->
  <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
  <!-- bootstrap-daterangepicker -->
  <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/moment/min/moment.min.js"></script>
  <script src="<?php echo $rutastatic;?>/tema/paris/gentelella/bootstrap-daterangepicker/daterangepicker.js"></script>

  <script src="<?php echo $documento->getUrlStatic()?>/libs/moment/moment.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/libs/datepicker/locales/moment-with-locales.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/libs/fullcalendar/fullcalendar.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/libs/fullcalendar/locale-all.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/libs/alert/jquery-confirm.min.js?vs=<?php echo $version; ?>"></script>
  <script src="<?php echo $documento->getUrlStatic()?>/libs/datatable1.10/media/js/jquery.dataTables.min.js?vs=<?php echo $version; ?>"></script>

  <jrdoc:incluir tipo="docsJs" />
  <jrdoc:incluir tipo="docsCss" />

  
  <script type="module" src="<?php echo URL_BASE ?>frontend/plantillas/paris/mobile/assets/js/main.js"></script>
  <script>
    /**
   * Funcion Copia de __url__ : Redirecciona a un nuevo modulo de la plataforma
   * @param {*} enlace URL del modulo
   * @param {*} mnu Indice del menu, por ejemplo (-1), siempre en negativo por logica actual
   * @param {*} isSubMenu 
   */
    function __url__(enlace, mnu = null, isSubMenu = false) {
      $("#iFrmContent").attr("src", enlace);
      $(".mnus").removeClass("active");
      if (isSubMenu) {
        let li = $("#mnu" + mnu).parent().parent().parent();

        li.addClass("menu-open").children("ul").show();
      }
      if (mnu !== null) {
        $("#mnu" + mnu).addClass("active");
        window.location.href = "#" + window.btoa(mnu);
      }  
      if(isMobile.any()!=null ||($('body').width()<=580 && $('body').hasClass('sidebar-open'))){
        $('body').removeClass('sidebar-open').addClass('sidebar-collapse');
      }
    }
  </script>
  <!--MOSTRAR CHAT DE SOPORTE-->
  <?php if(in_array($user["idempresa"],$empresasconchat)): ?>
    <script defer  src="https://widget.tochat.be/bundle.js?key=742ebb89-adcc-4f34-a930-0cd897ef6328"></script>
  <?php endif; ?>
</body>
<script src="<?php echo URL_BASE ?>frontend/plantillas/paris/mobile/assets/js/sidebar.js"></script>
<script>
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    });
  }
</script>