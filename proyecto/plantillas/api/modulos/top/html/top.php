<?php 
defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';
$rutastatic=$this->documento->getUrlStatic();
?>
<style type="text/css">
  header{
      background-color: #a73924;
      height: 60px;
  }
  a, a:link { color:#fff; }
  header #logo img{
    max-height: 50px;
    max-width: 150px;
  }
  header .custommenu{
    position: relative;
    z-index: 98;
  }
  header #account{
      white-space: nowrap;
  }
  .menutop1>li{
      background-color: rgb(68, 102, 187);
      float: left;
      z-index: 1040 !important;
  }
  .menutop1 li a{
    color: #FFF;
    font-size: 2em;
    text-align: center;
}
.user-menu{
    background: white !important;
    width: 320px !important;
    left: auto; right: -10px
}
header .navbar-toggler{ color:#ece0e0; border: 1px solid #ece0e0; }
header .navbar-toggler i {font-size: 3ex;}
.user-menu .datos-user { color: #a73924;  font-size: small;  font-weight: 700;}
.user-menu .datos-user > .user-correo{font-weight: normal;}
img{width: 100%}
</style>
<header>
  <nav class="navbar navbar-expand-lg " style="padding-bottom: 0px; padding-top: 0px; ">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
  </button>
    <a href="<?php echo $this->documento->getUrlSitio();?>" class="navbar-brand" id="logo">
      <img src="<?php echo $this->documento->getUrlStatic();?>/tema/logo.png" class="hvr-wobble-skew img-fluid" alt="logo">
    </a>
  
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav justify-content-end" style="flex: auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Inicio</a>
      </li>
      <!--li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown link
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li-->
       <li class="dropdown" id="account"> 
          <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-user-circle" style="font-size: 2.5rem;"></i>
          </a>
          <div class="user-menu dropdown-menu" style="">
            <div class="row" style="padding: 1ex;">
              <div class="col">
                <a class="user-photo">
                  <img src="https://abacoeducacion.org/web/static/media/imagenes/user_avatar.jpg" alt="user" class="img-fluid" >
                </a>
              </div>
              <div class="col datos-user">
                <div class="user-nombre">Mamud Venegas, Alex</div>
                <div class="user-correo">alex.mamud@hotmail.com</div>
                <div class="user-rol text-center">Administrador</div>
                <!--div class="user-miperfil text-center"><br><a href="<?php echo $this->documento->getUrlSitio(); ?>/personal" class="btn btn-success btn-sm "> <i class="fa fa-user"></i> Mi Perfil</a></div-->
              </div> 
            </div>
            <div class="row" style="padding: 1ex;">
              <div class="col-md-12"><hr style="    margin-top: 0px; margin-bottom: 1ex"></div>
              <div class="col text-left" style="margin-left: 1ex;">
                  <a href="<?php echo $this->documento->getUrlSitio(); ?>/personal/cambiarclave/?id=16481537" class="btn btn-sm btn-warning"> <i class=" fa fa-key"></i> Change password</a>
              </div>
              <div class="col text-right" style="margin-right: 1ex;">
                  <a href="<?php echo $this->documento->getUrlSitio(); ?>/sesion/salir" class="btn btn-primary btn-sm ">Salir <i class="fa fa-sign-out"></i> </a>
              </div>
            </div>
          </div> 
      </li>
    </ul>
  </div>
</nav>
</header>
<script type="text/javascript">
  var user=JSON.parse(localStorage.getItem("usersesion"));
  if(user){
    $('#account .user-nombre').text(user.nombre_full);
    $('#account .user-correo').text(user.email);
    $('#account .user-rol').text(user.rol);
  }
</script>
