<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018 
 * @copyright	Copyright (C) 17-04-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegLibre_tema', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLibre_tipo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLibre_palabras', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebTema_libre extends JrWeb
{
	private $oNegTipo;
	private $oNegTema;
	private $oNegPalabras;
	private $oNegAcad_curso;		
	public function __construct()
	{
		parent::__construct();		
		$this->usuarioAct = NegSesion::getUsuario();
        $this->oNegMatricula = new NegAcad_matricula;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegTipo = new NegLibre_tipo;
		$this->oNegTema = new NegLibre_tema;
		$this->oNegPalabras = new NegLibre_palabras;
	}

	public function defecto(){
		return $this->listarLibre_tipo();
	}

	public function listarLibre_tipo()
	{
		try{
			global $aplicacion;

			$filtros=array();
			if(isset($_REQUEST["idtipo"])&&@$_REQUEST["idtipo"]!='')$filtros["idtipo"]=$_REQUEST["idtipo"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo_contenido"])&&@$_REQUEST["tipo_contenido"]!='')$filtros["tipo_contenido"]=$_REQUEST["tipo_contenido"];
			
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Free Lesson')) ],
            ];

			$this->ruta='tema/?idtipo=';
			$this->campo_id='idtipo';
			$this->oNegTipo->setLimite(0,99999);
			$this->datos=$this->oNegTipo->buscar($filtros);
			$this->documento->plantilla = 'alumno/general';
			$this->documento->setTitulo( ucfirst(JrTexto::_('Free Lesson')) , true);
			$this->esquema = 'libre_tema/alum-libre_tipo';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function tema()
	{
		try{
			global $aplicacion;
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');

            $filtros=array();
			if(isset($_REQUEST["idtema"])&&@$_REQUEST["idtema"]!='')$filtros["idtema"]=$_REQUEST["idtema"];
			if(isset($_REQUEST["idtipo"])&&@$_REQUEST["idtipo"]!='')$filtros["idtipo"]=$_REQUEST["idtipo"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			$this->oNegTema->setLimite(0,99999);
			$this->datos=$this->oNegTema->buscar($filtros);

			$this->breadcrumb = [
                /*[ 'texto'=> ucfirst(JrTexto::_('Free Lesson')), 'link'=>'/tema_libre/' ],*/
                [ 'texto'=> @$this->datos[0]['tipo_nombre'] ],
            ];

			$this->ruta='contenido/?idtema=';
			$this->campo_id='idtema';
			$this->documento->plantilla = 'alumno/general';
			$this->documento->setTitulo( ucfirst(JrTexto::_('Free Lesson')).' - '.@$this->datos[0]['tipo_nombre'] , true);
			$this->esquema = 'libre_tema/alum-libre_tipo';
			$rol=$this->usuarioAct["idrol"];
			$this->idrol=$rol;
            //var_dump($this->usuarioAct);
			if(@$filtros["idtipo"]==22){
				if ($rol==3) {
					$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
					$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
					$this->cursosMatric=array();
					foreach($cursosProyecto as $cur){
						$cur["activo"]=false;			
						foreach($cursosMat as $curM){ 
							if($cur["idcurso"]==$curM["idcurso"]){
								$cur['activo']=true;
							}
						}
						// if($cur["idcurso"]!=31)	
						$this->cursosMatric[]=$cur;
						//var_dump($this->cursosMatric);
					}
					$this->esquema = 'libre_tema/woorkbooks_alu';
				}else{
					if($rol==2){
						$this->esquema = 'libre_tema/woorkbooks_doc';
		            }else{
		            	if ($rol==1) {
		            		$this->esquema = 'libre_tema/woorkbooks';
		            	}
		            }
				}
				
				
			}
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function prueba(){
		var_dump('hola');
		$this->documento->script('slick.min', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
		$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
        $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
        $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

		$this->documento->plantilla = 'alumno/general';
		$this->esquema = 'libre_tema/prueba';
		return parent::getEsquema();
	}
	public function diccionario(){
		$this->documento->script('slick.min', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
		$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
        $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
        $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

		$this->documento->plantilla = 'alumno/general';
		$this->esquema = 'libre_tema/diccionario';
		return parent::getEsquema();
	}
	public function contenidomp3()
	{
		global $aplicacion;
		if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros["audio"]=$_REQUEST["audio"];

		$this->documento->plantilla = 'alumno/general';
		$this->esquema = 'libre_tema/recursos_audios';
		return parent::getEsquema();
	}
	public function contenidomp3alumno()
	{
		global $aplicacion;
		if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros["audio"]=$_REQUEST["audio"];

		$this->documento->plantilla = 'alumno/general';
		$this->esquema = 'libre_tema/recursos_audio_alu';
		return parent::getEsquema();
	}
	public function contenidomp3doc()
	{
		global $aplicacion;
		if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros["audio"]=$_REQUEST["audio"];

		$this->documento->plantilla = 'alumno/general';
		$this->esquema = 'libre_tema/recursos_audio_alu';
		return parent::getEsquema();
	}
	public function listarcontenidovocal()
	{
		
		$this->documento->plantilla = 'returnjson'; // como docente
        try {
            if (isset($_REQUEST["idcurso"])) {
                $this->idcurso=$_REQUEST["idcurso"];
                // $this->nombre=$_REQUEST["nombre"];
                
            }else{
                $this->idcurso=0;
            }
            // var_dump($_REQUEST["nombre"]);	
            global $aplicacion;
            $this->todoVocabulary=array();
            $usuarioAct = NegSesion::getUsuario();
            if($usuarioAct['idrol']==1){
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoVocabulary=$this->oNegTema->buscarvocabulario(array('idcurso'=>$this->idcurso,'nombre'=>$this->nombre,'idtipo'=>1));
                }else{
                    $this->todoVocabulary=$this->oNegTema->buscarvocabulario(array('idcurso'=>$this->idcurso,'idtipo'=>1));
                }
            // $this->todoTareas=$this->oNegTema->buscar(array('id_detalle'=>$this->idcurso,'id_tipo'=>1));
            
            }else if($usuarioAct['idrol']==3){
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoVocabulary=$this->oNegTema->buscarvocabulario(array('idcurso'=>$this->idcurso,'nombre'=>$this->nombre,'idtipo'=>1));
                }else{
                    $this->todoVocabulary=$this->oNegTema->buscarvocabulario(array('idcurso'=>$this->idcurso,'idtipo'=>1));
                }
            }elseif ($usuarioAct['idrol']==2) {
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoVocabulary=$this->oNegTema->buscarvocabulario(array('idcurso'=>$this->idcurso,'nombre'=>$this->nombre,'idtipo'=>1));
                }else{
                    $this->todoVocabulary=$this->oNegTema->buscarvocabulario(array('idcurso'=>$this->idcurso,'idtipo'=>1));
                }
            }
            $datos=array();
			$i=0;
			if(!empty($this->todoVocabulary)){
				foreach($this->todoVocabulary as $dt){
					$datos[$i]=$dt;
					$datos[$i]["imagen"]=RUTA_BASE."static/libreria/image/".$dt["idtema"].".jpg";
					$rutafile=RUTA_BASE."static/libreria/image/".$dt["idtema"].".jpg";
					if(!is_dir($rutafile) && is_file($rutafile)){
						$datos[$i]["imagen"]=URL_BASE."/static/libreria/image/".$dt["idtema"].".jpg";
						
					}else{
						$datos[$i]["imagen"]=URL_BASE."/static/media/web/noaudio.png";
					}
					$i++;
				}
			}
			
            $respuesta = array("todo"=>$datos);
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}
	public function contenido()
	{
		try{
			global $aplicacion;
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->script('pronunciacion', '/js/');

            $filtros=array();
			if(isset($_REQUEST["idpalabra"])&&@$_REQUEST["idpalabra"]!='')$filtros["idpalabra"]=$_REQUEST["idpalabra"];
			if(isset($_REQUEST["idtema"])&&@$_REQUEST["idtema"]!='')$filtros["idtema"]=$_REQUEST["idtema"];
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros["audio"]=$_REQUEST["audio"];
			if(isset($_REQUEST["idagrupacion"])&&@$_REQUEST["idagrupacion"]!='')$filtros["idagrupacion"]=$_REQUEST["idagrupacion"];
			if(isset($_REQUEST["url_iframe"])&&@$_REQUEST["url_iframe"]!='')$filtros["url_iframe"]=$_REQUEST["url_iframe"];

			$tema = $this->oNegTema->buscar(array("idtema"=>@$_REQUEST["idtema"] ));
			if(empty($tema)) { throw new Exception(JrTexto::_("Topic does not exists")); }
			else { $tema = $tema[0]; }

			$this->breadcrumb = [
                /*[ 'texto'=> ucfirst(JrTexto::_('Free Lesson')), 'link'=>'/tema_libre/' ],*/
                [ 'texto'=> @$tema['tipo_nombre'], 'link'=>'/tema_libre/tema/?idtipo='.@$tema['idtipo'] ],
                [ 'texto'=> @$tema['nombre'] ],
            ];
			switch (@$tema['tipo_contenido']) {
				case 'TABLE':
					$this->oNegPalabras->setLimite(0,99999);
					$this->datos = $this->oNegPalabras->buscarTable($filtros);
					$this->esquema = 'libre_tema/alum-palabras_table';
					break;
				case 'IFRAME':
					$this->origen = @$tema['origen'];
					$this->datos = $this->oNegPalabras->buscar($filtros);
					$this->esquema = 'libre_tema/alum-palabras_iframe';
					break;
				case 'HTML':
					$this->origen = @$tema['origen'];
					$this->datos = $this->oNegPalabras->buscar($filtros);
					$this->esquema = 'libre_tema/alum-palabras_html';
					break;
			}
			$this->documento->plantilla = 'alumno/general';
			$this->documento->setTitulo( ucfirst(JrTexto::_('Free Lesson')).' - '.@$this->datos[0]['tema_nombre'] , true);
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //
	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Libre_tema', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idtema"])&&@$_REQUEST["idtema"]!='')$filtros["idtema"]=$_REQUEST["idtema"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["idtipo"])&&@$_REQUEST["idtipo"]!='')$filtros["idtipo"]=$_REQUEST["idtipo"];
						
			$this->datos=$this->oNegTema->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
}