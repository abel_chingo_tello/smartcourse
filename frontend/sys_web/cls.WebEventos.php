<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-04-2020 
 * @copyright	Copyright (C) 07-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEvento_programacion', RUTA_BASE);
class WebEventos extends JrWeb
{
	private $oNegEvento_programacion;

	public function __construct()
	{
		parent::__construct();
		$this->oNegEvento_programacion = new NegEvento_programacion;
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Evento_programacion', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			$this->documento->stylesheet('mine-eventos', '/libs/othersLibs/eventos/');
			$this->documento->script('mine-eventos', '/libs/othersLibs/eventos/');

			$filtros = array();
			if (isset($_REQUEST["id_programacion"]) && @$_REQUEST["id_programacion"] != '') $filtros["id_programacion"] = $_REQUEST["id_programacion"];
			if (isset($_REQUEST["fecha"]) && @$_REQUEST["fecha"] != '') $filtros["fecha"] = $_REQUEST["fecha"];
			if (isset($_REQUEST["titulo"]) && @$_REQUEST["titulo"] != '') $filtros["titulo"] = $_REQUEST["titulo"];
			if (isset($_REQUEST["detalle"]) && @$_REQUEST["detalle"] != '') $filtros["detalle"] = $_REQUEST["detalle"];
			if (isset($_REQUEST["hora_comienzo"]) && @$_REQUEST["hora_comienzo"] != '') $filtros["hora_comienzo"] = $_REQUEST["hora_comienzo"];
			if (isset($_REQUEST["hora_fin"]) && @$_REQUEST["hora_fin"] != '') $filtros["hora_fin"] = $_REQUEST["hora_fin"];
			if (isset($_REQUEST["idpersona"]) && @$_REQUEST["idpersona"] != '') $filtros["idpersona"] = $_REQUEST["idpersona"];
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];

			$oUsuario = NegSesion::getUsuario();

			$this->usuario = $oUsuario;
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Evento_programacion'), true);
			$this->esquema = 'evento/eventos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Evento_programacion', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Nuevo';
			$this->documento->setTitulo(JrTexto::_('Evento_programacion') . ' /' . JrTexto::_('New'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Evento_programacion', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Editar';
			$this->oNegEvento_programacion->id_programacion = @$_GET['id'];
			$this->datos = $this->oNegEvento_programacion->dataEvento_programacion;
			$this->pk = @$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Evento_programacion') . ' /' . JrTexto::_('Edit'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'evento_programacion-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
