<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-01-2018 
 * @copyright	Copyright (C) 15-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
//JrCargador::clase('sys_negocio::NegManuales', RUTA_BASE, 'sys_negocio');
class WebCorregir extends JrWeb
{
	//private $oNegManuales;
		
	public function __construct()
	{
		parent::__construct();		
		//$this->oNegManuales = new NegManuales;
				
	}

	public function defecto(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Matando Errores'), true);
			$this->esquema = 'corregir/listado';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function historial(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='historial_sesion';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function valoracion(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='valoracion';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function examenes(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='notas_quiz';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function actividad_alumno(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='actividad_alumno';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function tareas(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='tareas';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function bitacora_alumno_smartbook(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='bitacora_alumno_smartbook';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function bitacora_alumno_smartbook_se(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='bitacora_alumno_smartbook_se';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function transferencias(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='transferencias';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function eliminarinfouser(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='eliminarinfouser';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'corregir/secuencia';
			$this->enviaridalumno=true;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}