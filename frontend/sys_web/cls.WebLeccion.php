<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		14-11-2016 
 * @copyright	Copyright (C) 14-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebLeccion extends JrWeb
{
	private $oNegNiveles;
	public function __construct()
	{
		parent::__construct();
		$this->oNegNiveles = new NegNiveles;		
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{

			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idnivel"])&&@$_REQUEST["idnivel"]!='')$filtros["idnivel"]=$_REQUEST["idnivel"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"]; else $filtros["tipo"]='L';
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];

			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $this->idnivel=!empty($filtros["idnivel"])?$filtros["idnivel"]:(!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:1);
			
            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["txtUnidad"])?$_GET["txtUnidad"]:($_idunidad);

			$this->datos=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Activities'), true);
			$this->esquema = 'niveles-actividad';			
			return parent::getEsquema();			
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;

            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:($_idunidad);

			$tipo=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:'L';
			$idpadre=!empty($_REQUEST["idpadre"])?$_REQUEST["idpadre"]:$this->idunidad;
			$this->orden=$this->oNegNiveles->maxorden($tipo,$idpadre);

			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Activity').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->oNegNiveles->idnivel = @$_GET['id'];
			$this->datos = $this->oNegNiveles->dataNiveles;
			
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:1;
			$this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;

			$this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));		    
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:($_idunidad);
			$this->frmaccion='Editar';			
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Lesson').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	private function form()
	{
		try {
			global $aplicacion;						
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'niveles-actividad-frm';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
 
}