<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegLinks', RUTA_BASE, 'sys_negocio');
class WebCalificaciones extends JrWeb
{
	private $oNegGrupoAulaDet;
	private $oNegNotas;
	//private $oNegLinks;

	public function __construct()
	{
		parent::__construct();
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegGrupoAulaDet = new NegAcad_grupoauladetalle;
		$this->oNegNotas = new NegNotas;
		$this->oNegMatricula = new NegAcad_matricula;
		//$this->oNegLinks = new NegLinks;
	}

	public function defecto(){
		return $this->listar();
	}

	public function listar()
	{
		try {
			global $aplicacion;
			if($this->usuarioAct["rol"]=="Alumno"){
				$this->vistaAlumno();
			} else {
				$this->vistaDocente();
			}

			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('scores')) ],
            ];
            $this->documento->setTitulo(ucfirst (JrTexto::_('List').' - '.JrTexto::_('Scores')), true);
			$this->documento->plantilla = 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	/*========================== FUNCIONES AJAX ==========================*/
	public function jxGetArchivos()
	{
		try {
			$this->documento->plantilla = 'returnjson';

			if(isset($_REQUEST["iddocente"]) && @$_REQUEST["iddocente"]!='') $filtros["iddocente"] = $_REQUEST["iddocente"];
			if(isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"]!='') $filtros["identificador"] = $_REQUEST["idgrupoauladetalle"];

			$data = $this->oNegNotas->getArchivos($filtros);

			echo json_encode(array('code'=>'ok', 'msj'=>JrTexto::_('Success'), 'data'=>$data ));
			exit(0);
		} catch (Exception $e) {
			echo json_encode( array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())) );
			exit(0);
		}
	}

	/*======================== FUNCIONES PRIVADAS ========================*/
	private function vistaDocente()
	{
		try {
			$this->documento->script('calificaciones', '/js/docente/');

            $grupoAulaDet = $this->oNegGrupoAulaDet->buscar(array("iddocente"=>$this->usuarioAct["dni"]));

            $this->locales = array();
            foreach ($grupoAulaDet as $gad) {
            	if( !array_key_exists($gad["idlocal"], $this->locales) && $gad["idlocal"]!=0) {
            		$this->locales[$gad["idlocal"]] = array(
            			"idlocal" => $gad["idlocal"],
            			"nombre" => $gad["strlocal"],
            		); 
            	}
            }
			$this->esquema = 'docente/calificaciones';
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function vistaAlumno()
	{
		try {
			$this->cursosMatric = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$this->usuarioAct['dni'], 'estado'=>1));
			
			$this->esquema = 'alumno/calificaciones';
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

}