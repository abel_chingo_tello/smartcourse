<?php

/**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_SITIO') or die();
//JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegRecord', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHerramientas', RUTA_BASE);
//JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
class WebTools extends JrWeb
{
    protected $oNegActividad;
    protected $oNegRecord;
    protected $oNegHerramientas;
    protected $oNegResources;
    protected $oNegNiveles;
    protected $oNegCursoDetalle;
    protected $oNegHistorial_sesion;

    public function __construct()
    {
        //$this->usuarioAct = NegSesion::getUsuario();
        $this->oNegHerramientas = new NegHerramientas;
        //$this->oNegRecord = new NegRecord;
        //$this->oNegResources = new NegResources;
        //$this->oNegActividad=new NegActividad;
        //$this->oNegNiveles = new NegNiveles;
        //$this->oNegCursoDetalle = new NegAcad_cursodetalle;
        //$this->oNegHistorial_sesion = new NegHistorial_sesion;        
        parent::__construct();
    }
    public function defecto()
    {

        return false;
    }
    public function json_buscarGames()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            $filtros = array();
            $filtros["titulo"] = !empty($_REQUEST["txtBuscar"]) ? $_REQUEST["txtBuscar"] : '';
            $filtros["idnivel"] = !empty($_REQUEST["nivel"]) ? $_REQUEST["nivel"] : '';
            $filtros["idunidad"] = !empty($_REQUEST["unidad"]) ? $_REQUEST["unidad"] : '';
            $filtros["idactividad"] = !empty($_REQUEST["actividad"]) ? $_REQUEST["actividad"] : '';
            $juegos = $this->oNegHerramientas->buscarJuegos($filtros);
            $data = array('code' => 'ok', 'data' => $juegos);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $ex) {
            $data = array('code' => 'Error', 'mensaje' => JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }
    public function notas_quiz()
    {
        try {
            set_time_limit(0);
            JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
            global $aplicacion;
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $filtros = array();
            $this->oNegNotas_quiz = new NegNotas_quiz;
            if (isset($_REQUEST["idexamenproyecto"]) && !empty(@$_REQUEST["idexamenproyecto"])) {
                $filtros["idexamenproyecto"] = $_REQUEST["idexamenproyecto"];
            }
            if (isset($_REQUEST["idnota"]) && !empty(@$_REQUEST["idnota"])) {
                $filtros["idnota"] = $_REQUEST["idnota"];
            }
            if (isset($_REQUEST["l"]) && !empty(@$_REQUEST["l"])) {
                // $filtros["l"] = $_REQUEST["l"];
                $cantidad = 100;
                if (isset($_REQUEST["c"]) && !empty(@$_REQUEST["c"])) {
                    $cantidad = (int) $_REQUEST["c"];
                }
                $hasta = $cantidad - 1;
                // $hasta = ($cantidad * (int) $_REQUEST["l"]) - 1;
                $desde = ((int) $_REQUEST["l"] - 1) * $cantidad;
                // var_dump($desde, $hasta);exit();
                $this->oNegNotas_quiz->setLimite($desde, $hasta);
            }
            $this->datos = $this->oNegNotas_quiz->buscar($filtros);
            // var_dump($this->datos);exit();
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->documento->setTitulo(JrTexto::_('Curse'), true);
            $this->esquema = 'tools/index';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function save_nota_quiz()
    {
        try {
            set_time_limit(0);
            JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
            $this->oNegNotas_quiz = new NegNotas_quiz;
            if (empty($_REQUEST)) {
                echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
                exit(0);
            }
            $resultado = array();
            $notas_quiz = json_decode($_REQUEST["notas"], true);
            if (!empty($_REQUEST["notasProc"])) {
                $resultado = json_decode($_REQUEST["notasProc"], true);
            }
            foreach ($notas_quiz as $key => $nq) {
                // var_dump("nota_quiz", $nq);
                if (!in_array($nq['id'], $resultado)) {
                    $this->oNegNotas_quiz->setCampo($nq['id'], "preguntas", $nq['preguntas']);
                    array_push($resultado, $nq['id']);
                }
            }
            echo json_encode(array('code' => 200, 'data' => $resultado));
            exit(0);
        } catch (Exception $e) {
            echo json_encode(array('code' => 'error', 'msj' => 'Datos imcompletos'));
            exit(0);
        }
    }
}
