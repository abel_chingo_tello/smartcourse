<?php
set_time_limit(0);
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');

JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMinedu', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_competencias', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_unidad_capacidad', RUTA_BASE, 'sys_negocio');

class WebReportealumno extends JrWeb
{	
	private $oNegMatricula;
	private $oNegAcad_cursodetalle;
	private $oNegPersonal;
	private $oNegAcad_grupoauladetalle;
	private $oNegExamenes;
	private $oNegNotas_quiz;
	private $oNegPersona_setting;
	private $oNegReportealumno;
	private $oNegAcad_curso;
	private $oNegHistorial_sesion;
	private $oNegActividad_alumno;
	private $oNegMinedu;
	private $oNegCompetencias;
	private $oNegMin_unidad_capacidad;

	public function __construct()
	{
		parent::__construct();
		$this->oNegPersonal = new NegPersonal;
		$this->oNegReportealumno = new NegReportealumno;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegExamenes = new NegExamenes;
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegPersona_setting = new NegPersona_setting;
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegMinedu = new NegMinedu;
		$this->oNegCompetencias = new NegMin_competencias;
		$this->oNegMin_unidad_capacidad = new NegMin_unidad_capacidad;
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('Reports'), true);			
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			/*if($this->user['idproyecto'] === '1'){
				if($this->user['idrol'] === '3'){					
					$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
					//$this->lista_tiempo=$this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','idusuario'=>$this->user['dni']));
					// $this->esquema = 'menu_reportealumnoPV_ejercito';
				}else{
					//lista de estudiantes
					// var_dump($this->user);


					// $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
					// $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
					// $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
					
					// $this->cursos = $this->oNegAcad_grupoauladetalle->buscar(array('iddocente' => $this->user['idpersona'], 'idproyecto' => $this->user['idproyecto']));
					// $this->alumnos = array();
					// if(!empty($this->cursos[0])){
					// 	$this->alumnos_tmp = $this->oNegMatricula->buscar(array('idcurso' => $this->cursos[0]['idcurso'], 'idproyecto' =>$this->user['idproyecto'] ));
						
					// 	foreach($this->alumnos_tmp as $value){
					// 		$x = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','idusuario'=>$this->user['dni']));
					// 		$tiempo = !empty($x) ? NegTools::conversorSegundosHoras($x[0]['tiempo']) : 0;

					// 		$data= array('idalumno'=>$value['idalumno'], 'nombre' => $value['stralumno'] ,'tiempo' => $tiempo );
					// 		$this->alumnos[] = $data;
					// 		// $this->alumnos[][$value['idalumno']] = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','idusuario'=>$this->user['dni']));							
					// 	}
					// 	// var_dump($this->alumnos);
					// }
					// $this->esquema = 'menu_reportedocentePV_ejercito';					
				}
			}*/

			$this->esquema = ($this->user['idproyecto'] == 3) ? 'menu_reportealumno' : 'smartreportes' ;
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	/**
	 * ////////////////////////////////////////
	 * REPORTES DEL SMARTCOURSE
	 * ///////////////////////////////////////
	 * 
	 */
	public function exameniniciales(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
      $this->cursos = $this->oNegMatricula->buscar(array('idalumno' => @$this->user["idpersona"], 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));
			$this->examen = null;
			// var_dump($this->cursos);
			if(!empty($this->cursos)){
				$idcurso = $this->cursos[0]['idcurso'];
				$infoCursoDetalle = $this->oNegAcad_cursodetalle->buscar(array('idcurso'=> $idcurso));
				$key = array_search('1',array_column($infoCursoDetalle,'orden'));
				//get id examen
				$jsonParse = json_decode($infoCursoDetalle[$key]['txtjson'],true);
				if(isset($jsonParse['link'])){
					$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
					$idexamen = explode('&',$parte)[0];
					$examen = $this->oNegNotas_quiz->buscar(array('idalumno'=>@$this->user["idpersona"],'idrecurso'=>$idexamen));
					if(!empty($examen)){
						$this->examen = $examen[0];
						$this->examen['preguntas'] = str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$this->examen['preguntas']);
					}
				}
			}
			$this->documento->setTitulo('Exámenes iniciales', true);
			$this->documento->plantilla = "sintop";
			$this->esquema = "reportealumno/exameninicial";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function examenfinales(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
      $this->cursos = $this->oNegMatricula->buscar(array('idalumno' => @$this->user["idpersona"], 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));
			$this->examen = null;
			// var_dump($this->cursos);
			if(!empty($this->cursos)){
				$idcurso = $this->cursos[0]['idcurso'];
				$infoCursoDetalle = $this->oNegAcad_cursodetalle->buscar(array('idcurso'=> $idcurso));
				$key = array_search(count($infoCursoDetalle),array_column($infoCursoDetalle,'orden'));
				//get id examen
				$jsonParse = json_decode($infoCursoDetalle[$key]['txtjson'],true);
				if(isset($jsonParse['link'])){
					$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
					$idexamen = explode('&',$parte)[0];
					$examen = $this->oNegNotas_quiz->buscar(array('idalumno'=>@$this->user["idpersona"],'idrecurso'=>$idexamen));
					if(!empty($examen)){
						$this->examen = $examen[0];
						$this->examen['preguntas'] = str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$this->examen['preguntas']);
					}
				}
			}
			$this->documento->setTitulo('Exámenes finales', true);
			$this->documento->plantilla = "sintop";
			$this->esquema = "reportealumno/examenfinal";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function tiempoestudio(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = $this->user['dni'];
			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
							
			$this->plataforma = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
				'idusuario' => $idalumno,
				'idproyecto' => $this->user['idproyecto'],
				'lugar' => 'P')
			));
			$this->sesiones = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
				'idusuario' => $idalumno,
				'idproyecto' => $this->user['idproyecto'],
				'lugar' => 'S')
			));
			$this->examenes = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
				'idusuario' => $idalumno,
				'idproyecto' => $this->user['idproyecto'],
				'lugar' => 'E')
			));
			$this->documento->setTitulo('Tiempo de estudio', true);
			$this->documento->plantilla = "sintop";
			$this->esquema = "reportealumno/tiempoestudio";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function comparativo(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->cursos = $this->oNegMatricula->buscar(array('idalumno' => @$this->user["idpersona"], 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));
			
			
			
			$this->documento->script('Chart.min', '/libs/chartjs/');

			$this->documento->setTitulo('Comparativo de exámenes',true);
			$this->documento->plantilla = "sintop";
			$this->esquema = "reportealumno/comparativodeexamen";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function progresocursos(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->cursos = $this->oNegMatricula->buscar(array('idalumno' => @$this->user["idpersona"], 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));
			
			$this->documento->script('Chart.min', '/libs/chartjs/');

			$this->documento->setTitulo('Comparativo de exámenes',true);
			$this->documento->plantilla = "sintop";
			$this->esquema = "reportealumno/progresocurso";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function autoevaluacion(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->cursos = $this->oNegMatricula->buscar(array('idalumno' => @$this->user["idpersona"], 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));

			$this->documento->setTitulo('Autoevaluación',true);
			$this->documento->plantilla = "sintop";
			
			$this->esquema = "reportealumno/autoevaluacion";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error($e->getMessage());
		}
	}
	public function progresoxcompetencia(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->cursos = $this->oNegMatricula->buscar(array('idalumno' => @$this->user["idpersona"], 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));

			$this->documento->setTitulo("Progreso por competencias", true);
			$this->documento->plantilla = "sintop";
			$this->esquema = "reportealumno/competencia";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error($e->getMessage());
		}
	}
	public function progresoxpermanencia(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			
			$this->cursos = $this->oNegMatricula->buscar(array('idalumno' => @$this->user["idpersona"], 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));		

			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			
			$this->documento->setTitulo("Progreso por permanencia", true);
			$this->documento->plantilla = "sintop";
			$this->esquema = "reportealumno/permanencia";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error($e->getMessage());
		}
	}
	
	public function autoevaluacionjson(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			if(is_null($idcurso)){
				throw new Exception("idcurso is not defined");
			}
			$result = array();
			$lista = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso'=>$idcurso));

			if(!empty($lista)){
				foreach($lista as $value){
					if(isset($value['txtjson']) && !empty($value['txtjson'])){
						$jsonParse = json_decode($value['txtjson'],true);
						if(isset($jsonParse['options'])){
							foreach($jsonParse['options'] as $optionsValue){
								if(strcasecmp($optionsValue['nombre'], "Autoevaluación") == 0 || strcasecmp($optionsValue['nombre'], "Autoevaluacion") == 0){
									if(isset($optionsValue['link'])){
										$parte = substr($optionsValue['link'],(strpos($optionsValue['link'],'idexamen=')+9));
										$idexamen = explode('&',$parte)[0];
										$examen = $this->oNegNotas_quiz->buscar(array('idalumno'=>@$this->user["idpersona"],'idrecurso'=>$idexamen));
										$_examen = null;
										if(!empty($examen)){
											$_examen = $examen[0];
										}
										$result[] = array('sesion' => $value['nombre'], 'examen'=> $_examen);
									}
								}
							}// end foreach
						} // end if isset options
					}// end if isset txtjson
				}//end foreach
			}//end if empty lista
			echo json_encode(array('code'=>'ok','message'=>'request completed','data'=>$result));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
			exit(0);
		}
	}

	public function permanenciabarrajson(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			if(is_null($idcurso)){
				throw new Exception("idcurso is not defined");
			}
			$result = array();
			$points = array();
			$progreso = 0;
			$lista = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso'=>$idcurso));
			if(!empty($lista)){
				foreach($lista as $value){
					$_name = 'S';
					if(isset($value['txtjson']) && !empty($value['txtjson'])){
						$jsonParse = json_decode($value['txtjson'],true); 
						if(isset($jsonParse['link']) && strpos($jsonParse['link'],'quiz/ver') !== false){
							$_name = 'E';
						}
					}//end if
					$_name .= $value['orden'];
					$points[] = $_name;
				}
				$progreso =	$this->oNegReportealumno->getTotalCurso(array('idcurso'=>$idcurso,'idalumno'=>$this->user['idpersona']));
				$result = array('progreso'=> floor($progreso),'puntos' => $points);
			}
			echo json_encode(array('code'=>'ok','message'=>'request completed','data'=>$result));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
			exit(0);
		}
	}
	public function competenciacursojson(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			if(is_null($idcurso)){
				throw new Exception("idcurso is not defined");
			}
			$result = array();
			$lista = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso'=>$idcurso));
			if(!empty($lista)){
				foreach($lista as $value){
					if(isset($value['txtjson']) && !empty($value['txtjson'])){
						$jsonParse = json_decode($value['txtjson'],true); 
						if(!isset($jsonParse['link']) || strpos($jsonParse['link'],'quiz/ver') === false){
							$result[] = $value['nombre'];
						}
					}
				}
			}
			echo json_encode(array('code'=>'ok','message'=>'request completed','data'=>$result));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
			exit(0);
		}
	}
	public function progresocompetenciacursojson(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			if(is_null($idcurso)){
				throw new Exception("idcurso is not defined");
			}
			$result = 0;

			$infoCursoDetalle = $this->oNegAcad_cursodetalle->buscar(array('idcurso'=> $idcurso));
			$key = array_search(count($infoCursoDetalle),array_column($infoCursoDetalle,'orden'));
			//get id examen
			$jsonParse = json_decode($infoCursoDetalle[$key]['txtjson'],true);
			if(isset($jsonParse['link'])){
				$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
				$idexamen = explode('&',$parte)[0];
				$examen = $this->oNegNotas_quiz->buscar(array('idalumno'=>@$this->user["idpersona"],'idrecurso'=>$idexamen));
				if(!empty($examen)){
					$result = ($examen[0]['calificacion_en'] == 'N') ? ($examen[0]['nota'] * 100) / 20 : $examen[0]['nota'];
				}
			}

			echo json_encode(array('code'=>'ok','message'=>'request completed','data'=>$result));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
			exit(0);
		}
	}
	public function totalprogresscursojson(){
		try{
			global $aplicacion;

			$this->user = NegSesion::getUsuario();			
			
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;

			if(is_null($idcurso)){
				throw new Exception("Id is not defined");
			}

			$result = $this->oNegReportealumno->getTotalCurso(array('idcurso'=>$idcurso,'idalumno'=>$this->user['idpersona']));

			echo json_encode(array('code'=>'ok','message' => 'Request successfull', 'data'=> $result));
			exit(0);
		}catch(Exception $e){ 
			echo json_encode(array('code'=>'error','message'=>$e->getMessage())); 
			exit(0);
		}
	}
	public function progresoxsesionesjson(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();			

			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			if(is_null($idcurso)){
				throw new Exception("idcurso is not defined");
			}
			$result = array();
			$lista = $this->oNegAcad_cursodetalle->buscarconnivel(array('idcurso'=>$idcurso));
			if(!empty($lista)){
				foreach($lista as $value){
					$jsonParse = json_decode($value['txtjson'],true);
					$idsesion = (isset($value['idrecurso']))? $value['idrecurso'] : 0;
					if(isset($jsonParse['link']) && !isset($jsonParse['options'])){
						$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
						$idexamen = explode('&',$parte)[0];
						$objExamen = $this->oNegNotas_quiz->buscar(array('idrecurso'=>$idexamen,'idalumno'=>$this->user['idpersona'],'idproyecto'=>$this->user['idproyecto']));
						$p = (!empty($objExamen)) ? 100 : 0;
					}else{
						$p = $this->oNegReportealumno->getprogreso(array('idcurso'=>$idcurso,'idalumno'=>$this->user['idpersona'],'idsesion'=>$idsesion));
					}
					$result[] = array('sesion'=>$value['nombre'],'progreso'=> $p);
				}
			}
			echo json_encode(array('code'=>'ok','message'=>'request completed','data'=>$result));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error','message'=> $e->getMessage()));
			exit(0);
		}
	}

	public function obtenerexameninicial_json(){
		try{
			$this->user = NegSesion::getUsuario();
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			if(is_null($idcurso)){
				throw new Exception("no se definio el idcurso");
			}
			$result = array();
			$infoCursoDetalle = $this->oNegAcad_cursodetalle->buscar(array('idcurso'=> $idcurso));
			$key = array_search('1',array_column($infoCursoDetalle,'orden'));
			//get id examen
			$jsonParse = json_decode($infoCursoDetalle[$key]['txtjson'],true);
			//logic
			if(isset($jsonParse['link'])){
				$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
				$idexamen = explode('&',$parte)[0];
				$examen = $this->oNegNotas_quiz->buscar(array('idalumno'=>@$this->user["idpersona"],'idrecurso'=>$idexamen));
				if(!empty($examen)){
					$result = $examen[0];
					$result['preguntas'] = str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$result['preguntas']);
				}
			}
			echo json_encode(array('code'=>'ok','message' => 'Request successfull','data'=>$result));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error', 'message'=>$e->getMessage()));
			exit(0);
		}
	}
	public function obtenerexamenfinal_json(){
		try{
			$this->user = NegSesion::getUsuario();
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			if(is_null($idcurso)){
				throw new Exception("no se definio el idcurso");
			}
			$result = array();
			$infoCursoDetalle = $this->oNegAcad_cursodetalle->buscar(array('idcurso'=> $idcurso));
			$key = array_search(count($infoCursoDetalle),array_column($infoCursoDetalle,'orden'));
			//get id examen
			$jsonParse = json_decode($infoCursoDetalle[$key]['txtjson'],true);
			//logic
			if(isset($jsonParse['link'])){
				$parte = substr($jsonParse['link'],(strpos($jsonParse['link'],'idexamen=')+9));
				$idexamen = explode('&',$parte)[0];
				$examen = $this->oNegNotas_quiz->buscar(array('idalumno'=>@$this->user["idpersona"],'idrecurso'=>$idexamen));
				if(!empty($examen)){
					$result = $examen[0];
					$result['preguntas'] = str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$result['preguntas']);
				}
			}
			echo json_encode(array('code'=>'ok','message' => 'Request successfull','data'=>$result));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'error', 'message'=>$e->getMessage()));
			exit(0);
		}
	}
	/**
	 * ////////////////////////////////////////
	 * REPORTES DEL SMARTCOURSE///////////////////////////////////////////
	 * ///////////////////////////////////////
	 * 
	 */
	/**
	 * ////////////////////////////////////////
	 * REPORTES DEL SMARTENGLISH
	 * ///////////////////////////////////////
	 * 
	 */
	public function repor_tiempoe()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			$this->user = NegSesion::getUsuario();

			$this->isIframe = (isset($_REQUEST['isIframe'])) ? true : false ;

			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];

			//echo $usuarioAct['dni'];
			$this->lista_tiempo = null;
			$this->lista_tiempo=$this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'P')
			));
			$this->lista_tiempoc=$this->oNegReportealumno->buscar(array('tipo'=>'tiempo_curso','idusuario'=>$usuarioAct['dni']));			
			//$this->lista_tiempoc=$this->oNegReportealumno->buscar(array('tipo'=>'examen_s','idusuario'=>$usuarioAct['dni']));
			
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Tiempo de Estudio'), true);
			
			if($this->user['idproyecto'] == 1){
				if($this->user['idrol'] === '3'){
					$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
					$this->lista_tiempo=$this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','idusuario'=>$this->user['dni'],'idproyecto'=>$this->user['idproyecto']));
					$this->esquema = 'menu_reportealumnoPV_ejercito';
				}else{
					
					$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
					$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
					$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
					
					$this->cursos = $this->oNegAcad_grupoauladetalle->buscar(array('iddocente' => $this->user['idpersona'], 'idproyecto' => $this->user['idproyecto']));
					$this->alumnos = array();
					if(!empty($this->cursos[0])){
						$this->alumnos_tmp = $this->oNegMatricula->buscar(array('idcurso' => $this->cursos[0]['idcurso'], 'idproyecto' =>$this->user['idproyecto']));
						
						foreach($this->alumnos_tmp as $value){
							$x = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','idusuario'=>$value['idalumno'], 'idproyecto' =>$this->user['idproyecto']));
							$tiempo = !empty($x) ? NegTools::conversorSegundosHoras($x[0]['tiempo']) : 0;
							
							if(!empty($tiempo)){
							    $time = explode(':',$tiempo);
							    for($j=0; $j < count($time); $j++){
							        switch($j){
							            case 0: $letra = "h"; 
							                break;
							            case 1: $letra = "m"; 
							                break;
							            case 2: $letra = "s"; 
							                break;
							        }
							        $time[$j] = "{$time[$j]}{$letra}";
								}
								
							    $tiempo = implode(':',$time);
							}
							$data= array('idalumno'=>$value['idalumno'], 'nombre' => $value['stralumno'] ,'tiempo' => $tiempo );
							
							$this->alumnos[] = $data;
							// $this->alumnos[][$value['idalumno']] = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','idusuario'=>$this->user['dni']));							
						}
						// var_dump($this->alumnos);
					}

					$this->esquema = 'menu_reportedocentePV_ejercito';					
				}
			}else{
				//Importar libreria para los charts 
				$this->documento->script('Chart.min', '/libs/chartjs/');
				
				$this->tareas = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'T')
				));
				$this->actividades = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'A')
				));
				$this->smartbook = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'TR')
				));
				$this->games = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'G')
				));
				$this->examenes = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'E')
				));
				$this->esquema = 'reportealumno/repor_tiempoe';
			}
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function rptexamenes()
	{
		try {
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('Notas'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			if($this->user['idproyecto'] !== 4){
				$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
				$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
				$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
				if($this->user['idrol'] == "3"){
					$miscursos=$this->oNegMatricula->buscar(array('idproyecto'=>$this->user["idproyecto"],'idalumno'=>$this->user["dni"]));
				
					$this->alumnos = array();
					$examenes=array();
					foreach($miscursos as $cur){
						$sesiones=$this->oNegAcad_cursodetalle->sesiones($cur['idcurso'],0);
						$examenes=array();
						$data["nombre"]=$cur["nombre"];
						foreach($sesiones as $ex){
							$idexamen=0;
							if($ex["tiporecurso"]=='E') $examenes[]=(int)$ex["idrecurso"];
							else if($ex["tiporecurso"]=='M'){
								$dt=json_decode($ex["txtjson"]);
								$tipo=$dt->tipo;
								$tipelink=$dt->typelink;
								if($tipo=='#showpaddcontenido'&& $tipelink=='smartquiz'){
									$linkquiz=$dt->link;									
									$pattern = '/\?idexamen\=(.*?)\&idproyecto/si';
									preg_match($pattern, $linkquiz, $result);
									$examenes[]=$result[1];
								}elseif('#showpaddopciones'){
									$options=$dt->options;
									foreach($options as $op){
										$tipelink=$op->type;
										if($tipelink=='smartquiz'){
											$linkquiz=$op->link;									
											$pattern = '/\?idexamen\=(.*?)\&idproyecto/si';
											preg_match($pattern, $linkquiz, $result);
											$examenes[]=$result[1];
										}
									}
								}
							}
						}

						$data['notas']=array();
						foreach($examenes as $ex){
							$x = $this->oNegNotas_quiz->buscar(array('idrecurso'=>$ex,'idproyecto'=>$this->user['idproyecto'],'idalumno'=>$this->user['idpersona']));							
							$xn=0;
							if(!empty($x))
							foreach($x as $nt){
								$xn=$nt["nota"]>$xn?$nt["nota"]:$xn;
							}
							$nota=$xn>0?($xn*20/100):0;						
							$data['notas'][$ex]=$nota;
						}
						$this->alumnos[]=$data;
					}
					$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
					$this->esquema = 'reportealumno/examenes';
				}else{
					$miscursos = $this->oNegAcad_grupoauladetalle->buscar(array('iddocente' => $this->user['idpersona'], 'idproyecto' => $this->user['idproyecto']));
				
					//$this->oNegNotas_quiz
					$this->alumnos = array();					
					if(!empty($miscursos[0])){
						//var_dump($miscursos);
						$sesiones=$this->oNegAcad_cursodetalle->sesiones($miscursos[0]['idcurso'],0);
						$examenes=array();
						foreach($sesiones as $ex){
							$idexamen=0;
							if($ex["tiporecurso"]=='E') $examenes[]=(int)$ex["idrecurso"];
							else if($ex["tiporecurso"]=='M'){
								$dt=json_decode($ex["txtjson"]);
								$tipo=$dt->tipo;
								$tipelink=$dt->typelink;
								if($tipo=='#showpaddcontenido'&& $tipelink=='smartquiz'){
									$linkquiz=$dt->link;									
									$pattern = '/\?idexamen\=(.*?)\&idproyecto/si';
									preg_match($pattern, $linkquiz, $result);
									$examenes[]=$result[1];
								}elseif('#showpaddopciones'){
									$options=$dt->options;
									foreach($options as $op){
										$tipelink=$op->type;
										if($tipelink=='smartquiz'){
											$linkquiz=$op->link;									
											$pattern = '/\?idexamen\=(.*?)\&idproyecto/si';
											preg_match($pattern, $linkquiz, $result);
											$examenes[]=$result[1];
										}
									}
								}								
							}							
						}
						
						$misalumnos= $this->oNegMatricula->buscar(array('idcurso' => $miscursos[0]['idcurso'], 'idproyecto' =>$this->user['idproyecto'] ));
						foreach($misalumnos as $value){
							$data= array('idalumno'=>$value['idalumno'], 'nombre' => $value['stralumno'],'notas'=>array());
							foreach($examenes as $ex){
								$x = $this->oNegNotas_quiz->buscar(array('idrecurso'=>$ex,'idproyecto'=>$this->user['idproyecto'],'idalumno'=>$value['idalumno']));							
								$xn=0;
								if(!empty($x))
								foreach($x as $nt){
									$xn=$nt["nota"]>$xn?$nt["nota"]:$xn;
								}
								$nota=$xn>0?($xn*20/100):0;						
								$data['notas'][$ex]=$nota;
							}														
							$this->alumnos[] = $data;
							//var_dump($data);
						}
					}
					$this->esquema = 'reportealumno/examenes';	
									
				}
			}else{
				//$this->esquema = 'reportealumno/repor_tiempoe';
				exit("falta otros proyectos");
			}
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ubicacion(){
		try{
			//test
			global $aplicacion;
			$this->user = NegSesion::getUsuario(); 

			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			
			$this->examen = 0;

			$this->isIframe = (isset($_REQUEST['isIframe'])) ? true : false ;
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			$idrol = (isset($_REQUEST['idrol'])) ? $_REQUEST['idrol'] : 3; //defecto 3
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
			
			$this->datos_perfil = $this->oNegPersonal->dataPersonal;
      		$name = $this->user['nombre_full']; //defecto
		  	$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;
			// var_dump($this->foto);


			$filtro = array(
				'idpersona'=>$idalumno,
				'idrol' => $idrol,
				'tipo' => 'EU',
				'idproyecto' => $idproyecto
			);
			$this->setting = $this->oNegPersona_setting->buscar($filtro);
			if(!empty($this->setting)){
				
				$datos = json_decode($this->setting[0]['datos'],true);

				if($datos['accion'] == 'si'){
					$examen = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $datos['idexamen'], 'tipo' => 'U'));
					if(isset($examen[0])){
						$nombreNivel = 'A1';
						//calcular la calificacion final acuerdo del total del nivel obtenido
						$_json = json_decode($examen[0]['calificacion_total']);
						$notaInt = intval($examen[0]['nota']);
						$max = 20;
						foreach($_json as $value){
							$max = intval($value->max);
							$min = intval($value->min);
							if($notaInt >= $min && $notaInt <= $max){
								$nombreNivel = $value->nombre;
								break;
							}
						}
						$totalxNivel = $max;
						$calculo = ( ($examen[0]['nota'] * 100) / $totalxNivel ) * 0.20;
						//Ubicar nuevos valores para enviar a la vista
						$examen[0]['nivelUbicado'] = $nombreNivel;
						$examen[0]['notaFinal'] = intval(floor($calculo));//round($calculo,2);
						$this->examen = $examen[0];
					}//end if isset examen
				}
			}
			// var_dump($this->examen);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_("Reports"), true);
			$this->esquema = 'reportealumno/ubicacion';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function comparativa(){
		$this->documento->plantilla = 'returnjson';
  }
  
  public function reporte_entrada(){
    try{
      
      global $aplicacion;
      $this->entrada = true;
      $this->isIframe = (isset($_REQUEST['isIframe']));
      $this->user = NegSesion::getUsuario();
      $usuario = (isset($_REQUEST['usuario'])) ? $_REQUEST['usuario'] : $this->user['usuario'] ;
      $clave = (isset($_REQUEST['clave'])) ? $_REQUEST['clave'] : $this->user['clave'] ;
      $idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'] ;
      $this->datos_perfil = $this->oNegPersonal->dataPersonal;
      $this->fullName = $this->oNegPersonal->buscar(array('idpersona'=> $idalumno, 'rol' => 3));
      $this->fullName = (!empty($this->fullName)) ? "{$this->fullName[0]['nombre']} {$this->fullName[0]['ape_paterno']} {$this->fullName[0]['ape_materno']}" : $this->user['nombre_full'];
      $this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno' => $idalumno, 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));
      $datos = null;
      $resultado = array();
      if(!empty($this->cursos)){
        foreach($this->cursos as $key => $val){
          $resultado[$val['idcurso']] = null;
          $datos = $this->oNegAcad_cursodetalle->sesiones($val['idcurso'],0);
          foreach($datos as $keyU => $unity){
            if(isset($unity['idrecurso'])){
              if($unity['tiporecurso'] == 'E'){
                $jsonSesion = json_decode($unity['txtjson'],true);
                if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'E'){
                  $keyexamen_tmp=$unity['idrecurso'];
                  if($valor = $this->oNegNotas_quiz->buscar(array('idalumno' =>$idalumno,'idrecurso' => $unity['idrecurso'],'tipo' => 'E'))){
                    foreach($valor as $v){
					  $resultado[$val['idcurso']] = $v;
					  
					  
					}//endforeach
					// var_dump($resultado);
					$resultado[$val['idcurso']]['preguntas'] = str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$resultado[$val['idcurso']]['preguntas']);
					// var_dump($resultado);
                  }//end if valor
                }//endif !empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'E'
              }
            }//end if isset(unity['idrecurso'])
          }//end foreach unity
        }//endforeach cursos
      }
      $this->url = URL_SMARTQUIZ.'examenes/resolver/?idexamen='.$keyexamen_tmp.'&id='.@$idalumno.'&pr='.IDPROYECTO.'&u='.@$usuario.'&p='.@$clave;
      $this->examen = $resultado;
      // $this->documento->setTitulo(JrTexto::_("Reporte de examen de Entrada"));
      $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
      $this->esquema = "reportealumno/reporte_entradasalida";
      return parent::getEsquema();        
    }catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}  
  }

  public function reporte_salida(){
    try{
      global $aplicacion;
      $this->entrada = false;
      $this->isIframe = (isset($_REQUEST['isIframe']));
      $this->user = NegSesion::getUsuario();
      $idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'] ;
      $this->datos_perfil = $this->oNegPersonal->dataPersonal;
      $this->fullName = $this->oNegPersonal->buscar(array('idpersona'=> $idalumno, 'rol' => 3));
      $this->fullName = (!empty($this->fullName)) ? "{$this->fullName[0]['nombre']} {$this->fullName[0]['ape_paterno']} {$this->fullName[0]['ape_materno']}" : $this->user['nombre_full'];
      $this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno' => $idalumno, 'estado' => 1, "idproyecto" => @$this->user["idproyecto"]));
      $datos = null;
      $resultado = array();
      if(!empty($this->cursos)){
        foreach($this->cursos as $key => $val){
          $resultado[$val['idcurso']] = null;
          $datos = $this->oNegAcad_cursodetalle->sesiones($val['idcurso'],0);
          foreach($datos as $keyU => $unity){
            if(isset($unity['idrecurso'])){
              if($unity['tiporecurso'] == 'E'){
				$jsonSesion = json_decode($unity['txtjson'],true);
                if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'S'){
                  if($valor = $this->oNegNotas_quiz->buscar(array('idalumno' =>$idalumno,'idrecurso' => $unity['idrecurso'],'tipo' => 'E'))){
					foreach($valor as $v){
                      $resultado[$val['idcurso']] = $v;
                    }//endforeach
                  }//end if valor
                }//endif !empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'S'
              }
            }//end if isset(unity['idrecurso'])
          }//end foreach unity
        }//endforeach cursos
      }

	  $this->examen = $resultado;
      // $this->documento->setTitulo(JrTexto::_("Reporte de examen de Salida"));
      $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
      $this->esquema = "reportealumno/reporte_entradasalida";
      return parent::getEsquema();
    }catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
  }

  public function progreso(){
  	try{
  		global $aplicacion;
      $this->isIframe = (isset($_REQUEST['isIframe']));
      $this->user = NegSesion::getUsuario();
      $idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
      $idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
      $this->datos_perfil = $this->oNegPersonal->dataPersonal;
      $name = $this->user['nombre_full']; //defecto
		  $foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

      $this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$idalumno, 'estado'=>1, "idproyecto"=>$idproyecto));

      $progresos = array();

      if(!empty($this->cursos)){
      	foreach ($this->cursos as $key => $value) {
      		$progresos[$value['idcurso']] = null;
      		$temas = $this->oNegAcad_cursodetalle->sesiones($value['idcurso'],0);
      		foreach($temas as $tem){
				$tem["progreso"]=0;
				$tem["nactividad"]=0;
				
				if( $tem['tiporecurso'] != 'E'){
					$tem=$this->oNegAcad_curso->getProgresoUnidad($tem,$value['idcurso'],$idalumno);
					$progresos[$value['idcurso']][] = $tem;
				}//endif
			}//endforeach

      	}//endforeach cursos
      }

			//$this->oNegAcad_curso->getProgresoUnidad($det,$idCurso,$idpersona)
	//   var_dump($progresos);
    //   var_dump($progresos[31][0]['hijo']);
      $this->progresos = $progresos;
      // $this->documento->setTitulo(JrTexto::_("Reporte de examen de Salida"));
      //Importar libreria para los charts 
	  $this->documento->script('Chart.min', '/libs/chartjs/');
      $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
      $this->esquema = "reportealumno/progreso";
      return parent::getEsquema();
  	}catch(Exception $e){
  		return $aplicacion->error(JrTexto::_($e->getMessage()));
  	}
  }
	private function obtenerequivalente($v,$max,$isInteger = true){
		$r = 0;
		if($v != 0){
			$r = ($isInteger == true) ? intval(($v * 100) / $max) : ($v * 100) / $max;
		}
		return ($r > 100) ? 100 : $r;
	}
  public function progreso_habilidad(){
	  try{
		//code here
		global $aplicacion;
		$this->isIframe = (isset($_REQUEST['isIframe']));
		$this->user = NegSesion::getUsuario();
		$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
		$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
		$this->datos_perfil = $this->oNegPersonal->dataPersonal;
		$name = $this->user['nombre_full']; //defecto
		$foto = 'user_avatar.jpg'; //defecto
		if(isset($_REQUEST['idalumno'])){
			if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
				$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
				$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
			}
		}
		$this->fullname = $name;
		$this->foto = $foto;
		
		$this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$idalumno, 'estado'=>1, "idproyecto"=>$idproyecto));
		$datos = array();
		$habilidad_total = array();
		$habilidad_bimestre_all = array();
		$habilidad_trimestre_all = array();

		if(!empty($this->cursos)){
			//buscar total de las habilidades en tabla resumen
			$resultadoTotal = $this->oNegMinedu->progresosresumen(array('idalumno' => $idalumno));
			//constantes
			$porciento_bimestre = 100/4; $porciento_trimestre = 100/3;
			foreach($this->cursos as $key => $value){
				$habilidad_total[$value['idcurso']] = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
				$habilidad_bimestre_all[$value['idcurso']] = array('1' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'2' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'3' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'4' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0));
				$habilidad_trimestre_all[$value['idcurso']] = array('1' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'2' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'3' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0));
				
				if(!empty($resultadoTotal) && isset($resultadoTotal[0])){
					$habilidad_total[$value['idcurso']] = array('4'=>$resultadoTotal[0]['prog_hab_L_'.$value['nombre']],'5'=>$resultadoTotal[0]['prog_hab_R_'.$value['nombre']],'6'=>$resultadoTotal[0]['prog_hab_W_'.$value['nombre']],'7'=>$resultadoTotal[0]['prog_hab_S_'.$value['nombre']]);
					$habilidad_bimestre_all[$value['idcurso']] = array(
						'1' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_bimestre),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_bimestre),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_bimestre),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_bimestre)),
						'2' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_bimestre * 2),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_bimestre * 2),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_bimestre * 2),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_bimestre * 2)),
						'3' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_bimestre * 3),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_bimestre * 3),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_bimestre * 3),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_bimestre * 3)),
						'4' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_bimestre * 4),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_bimestre * 4),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_bimestre * 4),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_bimestre * 4))
					);
					$habilidad_trimestre_all[$value['idcurso']] = array(
						'1' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_trimestre),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_trimestre),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_trimestre),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_trimestre)),
						'2' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_trimestre * 2),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_trimestre * 2),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_trimestre * 2),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_trimestre * 2)),
						'3' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_trimestre * 3),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_trimestre * 3),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_trimestre * 3),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_trimestre * 3))
					);
				}

			}//endforeach
		}
		// var_dump($habilidad_bimestre_all);
		// var_dump($habilidad_trimestre_all);
		// var_dump($habilidad_total);
		
		$this->habilidad_bimestre_all = $habilidad_bimestre_all;
		$this->habilidad_trimestre_all = $habilidad_trimestre_all;
		$this->habilidad_total = $habilidad_total;
		
		$this->documento->script('Chart.min', '/libs/chartjs/');
		$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
		$this->esquema = "reportealumno/progresoxhabilidad";
		return parent::getEsquema();
	  }catch(Exception $e){
		  return $aplicacion->error(JrTexto::_($e->getMessage()));
	  }
  }

  public function progreso_competencia(){
	try{
		global $aplicacion;
		$this->user = NegSesion::getUsuario();
		$this->isIframe = (isset($_REQUEST['isIframe']));
		$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
		$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
		$this->datos_perfil = $this->oNegPersonal->dataPersonal;
		$name = $this->user['nombre_full']; //defecto
		$foto = 'user_avatar.jpg'; //defecto
		if(isset($_REQUEST['idalumno'])){
			if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
				$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
				$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
			}
		}
		$this->fullname = $name;
		$this->foto = $foto;
		
		$this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$idalumno, 'estado'=>1, "idproyecto"=>$idproyecto));

		$habilidadxcurso = array();
		if(!empty($this->cursos)){
			$resultadoTotal = $this->oNegMinedu->progresosresumen(array('idalumno' => $idalumno));
			foreach($this->cursos as $key => $value){
				$habilidadxcurso[$value['idcurso']] = array('4'=>$resultadoTotal[0]['prog_hab_L_'.$value['nombre']],'5'=>$resultadoTotal[0]['prog_hab_R_'.$value['nombre']],'6'=>$resultadoTotal[0]['prog_hab_W_'.$value['nombre']],'7'=>$resultadoTotal[0]['prog_hab_S_'.$value['nombre']]);
			}
		}
		
		$this->habilidadxcurso = $habilidadxcurso;
		$this->competencia = $this->oNegCompetencias->competencias();
		$this->capacidadesxunidad = $this->oNegMin_unidad_capacidad->capacidades();
		
		$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
		$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
		$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

		$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
		$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"));
		$this->esquema = 'reportealumno/progresoxcompetencia';

		return parent::getEsquema();
	}catch(Exception $e){
		return $aplicacion->error(JrTexto::_($e->getMessage()));
	}
	}

  private function setUltimavez(&$contenedor,$row,$idalumno,$letter){
  	if(isset($row)){
	  	$contenedor[$letter]['fecha'] = 0;
	  	$contenedor[$letter]['tiempo'] = 0;
		  if($valor = $this->oNegHistorial_sesion->tiempo_fecha_ingreso(array('idusuario' => $idalumno, 'lugar' => $letter,'fechaentrada' => $row)) ){

			$contenedor[$letter]['fecha'] = $row;
			$contenedor[$letter]['tiempo'] = round($valor[0]['tiempo'] / 3600, 2);
		  }//valor
	  }
  }

  public function usodominio(){
    //SELECT Distinct DATE(hs.fechaentrada) AS fecha , (select ABS(SUM(TIME_TO_SEC(TIMEDIFF(TIME(h.fechaentrada), TIME(h.fechasalida)))))  FROM historial_sesion h WHERE h.idusuario = 97 AND h.lugar = 'P' AND DATE(h.fechaentrada) = fecha) AS tiempo FROM historial_sesion hs WHERE hs.idusuario = 97 AND hs.lugar = 'P' ;
    //select distinct date(hs.fechaentrada) from historial_sesion hs where hs.idusuario = 97 and hs.lugar = 'P';
    //select ABS(SUM(TIME_TO_SEC(TIMEDIFF(TIME(h.fechaentrada), TIME(h.fechasalida))))) from historial_sesion h where h.idusuario = 97 and h.lugar = 'P' and date(h.fechaentrada) like '2018-09-28%' ;
    try{
		  global $aplicacion;
		  $this->isIframe = $this->isIframe = (isset($_REQUEST['isIframe'])) ? true : false ;
		  $this->user = NegSesion::getUsuario();
		  $idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'] ;
		  $idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
		  //Importar libreria para los charts 
		  $this->documento->script('Chart.min', '/libs/chartjs/');
		  $this->datos_Perfil = $this->oNegPersonal->dataPersonal;
		  //$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
		  $name = $this->user['nombre_full']; //defecto
		  $foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;
			/*
				Plataforma Virtual 20 horas

				SmartBook 10 horas
				HomeWork 5 horas
				Activity 10 horas
				SmartQuiz 10 horas
			*/
		  $fechas = $this->oNegHistorial_sesion->fechas_de_ingreso(array('idusuario' => $idalumno, 'lugar' => 'P'));
			$fechasplataforma = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'P',
					'idproyecto' => $idproyecto)
				));;
			$fechassmartbook = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'TR',
					'idproyecto' => $idproyecto)
				));
			$fechaspractice = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'A',
					'idproyecto' => $idproyecto)
				));
			$fechassmarquiz = $this->examenes = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'E',
					'idproyecto' => $idproyecto)
				));
			$fechashomework = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'T',
					'idproyecto' => $idproyecto)
				));
			
			$hrplataforma = (!empty($fechasplataforma)) ? $fechasplataforma[0]['tiempo'] : 0;
			$hrsmartbook = (!empty($fechassmartbook)) ? $fechassmartbook[0]['tiempo'] : 0;
			$hrpractice = (!empty($fechaspractice)) ? $fechaspractice[0]['tiempo'] : 0;
			$hrsmarquiz = (!empty($fechassmarquiz)) ? $fechassmarquiz[0]['tiempo'] : 0;
			$hrhomework = (!empty($fechashomework)) ? $fechashomework[0]['tiempo'] : 0;

			$hrplataforma = ($hrplataforma != 0) ? (100 * ($hrplataforma/3600)) / 20 : 0;
			$hrsmartbook = ($hrsmartbook != 0) ? (100 * ($hrsmartbook/3600)) / 10 : 0;
			$hrpractice = ($hrpractice != 0) ? (100 * ($hrpractice/3600)) / 10 : 0;
			$hrsmarquiz = ($hrsmarquiz != 0) ? (100 * ($hrsmarquiz/3600)) / 10 : 0;
			$hrhomework = ($hrhomework != 0) ? (100 * ($hrhomework/3600)) / 5 : 0;

			$hrplataforma = ($hrplataforma > 100) ? 100 : $hrplataforma;
			$hrsmartbook = ($hrsmartbook > 100) ? 100 : $hrsmartbook;
			$hrpractice = ($hrpractice > 100) ? 100 : $hrpractice;
			$hrsmarquiz = ($hrsmarquiz > 100) ? 100 : $hrsmarquiz;
			$hrhomework = ($hrhomework > 100) ? 100 : $hrhomework;

			$sumaHr = $hrplataforma + $hrsmartbook + $hrpractice + $hrsmarquiz + $hrhomework;

			$dominio = (100 * $sumaHr) / 500;

			$Uso = array(
				'dominio' => $dominio,
				'smartbook' =>$hrsmartbook,
				'activity' =>$hrpractice,
				'smarquiz' =>$hrsmarquiz,
				'homework' =>$hrhomework,
			);

			$fechas_smartbook = $this->oNegHistorial_sesion->fechas_de_ingreso(array('idusuario' => $idalumno, 'lugar' => 'TR'));
			$fechas_smartask = $this->oNegHistorial_sesion->fechas_de_ingreso(array('idusuario' => $idalumno, 'lugar' => 'T'));
			$fechas_smarquiz = $this->oNegHistorial_sesion->fechas_de_ingreso(array('idusuario' => $idalumno, 'lugar' => 'E'));

			$tiempos_utlimavez = array();

			if(!empty($fechas_smartbook)){
				$this->setUltimavez($tiempos_utlimavez,$fechas_smartbook[0]['fecha'],$idalumno,'TR');
			}
			if(!empty($fechas_smartask)){
				$this->setUltimavez($tiempos_utlimavez,$fechas_smartask[0]['fecha'],$idalumno,'T');
			}
			if(!empty($fechas_smarquiz)){
				$this->setUltimavez($tiempos_utlimavez,$fechas_smarquiz[0]['fecha'],$idalumno,'E');
			}

			/*
		  if(isset($fechas_smartbook[0]['fecha'])){
		  	$tiempos_utlimavez[$fechas_smartbook[0]['fecha']] = 0;
			  if($valor = $this->oNegHistorial_sesion->tiempo_fecha_ingreso(array('idusuario' => $idalumno, 'lugar' => 'TR','fechaentrada' => $fechas_smartbook[0]['fecha'])) ){

				$tiempos_utlimavez[$fechas_smartbook[0]['fecha']] = round($valor[0]['tiempo'] / 3600, 2);
			  }//valor
		  }
		  */


		  $tiempos = array();
		  foreach($fechas as $value){
			  if(isset($value['fecha'])){
				  if($valor = $this->oNegHistorial_sesion->tiempo_fecha_ingreso(array('idusuario' => $idalumno, 'lugar' => 'P','fechaentrada' => $value['fecha'])) ){
					$tiempos[$value['fecha']] = round($valor[0]['tiempo'] / 3600, 2);
				  }//valor
			  }
		  }//endforeach

		  if(!empty($tiempos) && !empty($fechas)){
		  	$tiempos_utlimavez['P']['fecha'] = $fechas[0]['fecha'];
		  	$tiempos_utlimavez['P']['tiempo'] = $tiempos[$fechas[0]['fecha']];
		  }

		  //var_dump($fechas);
		  //var_dump($tiempos);
		  //var_dump($Uso);
		  //var_dump($tiempos_utlimavez);
		  $this->fechas = $fechas;
		  $this->tiempos = $tiempos;
		  $this->dominio = $Uso;
		  $this->ultimavez = $tiempos_utlimavez;
      $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
      $this->esquema = "reportealumno/usodominio";
      return parent::getEsquema();
    }catch(Exception $e){
      return $aplicacion->error(JrTexto::_($e->getMessage()));
    }
  }
	public function entrada(){
		try{
			global $aplicacion;
			$this->entrada = true;
			$this->isIframe = (isset($_REQUEST['isIframe'])) ? true : false ;
			$this->user = NegSesion::getUsuario();
			$idalumnoDNI = (isset($_REQUEST['idalumnoDNI'])) ? $_REQUEST['idalumnoDNI'] : $this->user['dni'];
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			
			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			$this->fullName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3));
			$this->fullName = (!empty($this->fullName)) ? "{$this->fullName[0]['nombre']} {$this->fullName[0]['ape_paterno']} {$this->fullName[0]['ape_materno']}" : $this->user['nombre_full'];
			// Mostrar/Seleccionar los cursos asignado en matricula
			$this->cursos = array();
			$this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$idalumno, 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));;
			
			$datos = null;
			$datos_examen_entrada = null;

			$TotalEntrada = null;
			//Obtener todas las habilidades de la bd
			$habilidades = $this->oNegReportealumno->buscar(array('tipo'=>'lista_habilidades'));

			$habilidad_sum = array();
			$habilidad_sum_contrario = array();
			// 4 Listening 5 reading 6 writing 7 speaking
			$infoentrada = array();
			$infosalida = array();

			foreach($this->cursos as $key => $value){
				// if($key == 1){
					//inicializar sumatoria de habilidades por curso
					$habilidad_sum[$value['idcurso']]['4'] = 0;
					$habilidad_sum[$value['idcurso']]['5'] = 0;
					$habilidad_sum[$value['idcurso']]['6'] = 0;
					$habilidad_sum[$value['idcurso']]['7'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['4'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['5'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['6'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['7'] = 0;
					//Buscar las unidades del curso...

					$datos= $this->oNegAcad_cursodetalle->sesiones($value['idcurso'],0);
					//  var_dump($datos);
					//obtener examen de entradas con el curso especificado por el ciclo
					// $datos_examen_entrada = $this->oNegReportealumno->buscar(array('tipo'=>'examen_e','cond'=>array(
					// 	'idusuario' => $this->user['idpersona'],
					// 	'tiporecurso' => 'E',
					// 	'idcurso' => $value['idcurso'] ))
					// );
					// var_dump($datos[$key]);
					// var_dump($datos[$key]['hijo']);
					foreach($datos as $keyU => $unity){
						//$datos[$keyU]['tiporecurso'] == 'E' 
						$sum = 0;
						$sum2 = 0;
						//recorrer las unidades para encontrar los examenes
						if(isset($unity['idrecurso'])){
							// foreach($datos[$keyU]['hijo'] as $hijo){
								if($unity['tiporecurso'] == 'E'){
									$jsonSesion = json_decode($unity['txtjson'],true);
									if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'E'){
										if( $valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
											//verificar si solo hay una fila en el resutlado de la consulta
											//recorrer varias notas obtenidas
											foreach($valor as $value2){
												$sum += $value2['nota'];
												$infoentrada[$value['idcurso']] = $value2['regfecha'];
												if(isset($value2['habilidad_puntaje'])){
													$json = json_decode($value2['habilidad_puntaje'],true);
													foreach($json as $k => $v){
														if(intval($k) <= 7){
															$habilidad_sum[$value['idcurso']][$k] += $v;											
														}
													}
												}
											}//endforeach recorrer notas
	
                    					}//End if valor
                  					}//end condition !empty($jsonSesion) && strtoupper($jsonSesion['tipo'])
                  					if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'S'){
										//Obtener las habilidades del contrario
										if( $valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
											//verificar si solo hay una fila en el resutlado de la consulta
											//recorrer varias notas obtenidas
											foreach($valor as $value2){
												$sum2 = $value2['nota'];
												$infosalida[$value['idcurso']] = $value2['regfecha'];
												if(isset($value2['habilidad_puntaje'])){
													$json = json_decode($value2['habilidad_puntaje'],true);
													foreach($json as $k => $v){
														if(intval($k) <= 7){
															$habilidad_sum_contrario[$value['idcurso']][$k] += $v;											
														}
													}
												}
											}//endforeach recorrer notas
	
										}//End if valor
                  					}//end condition !empty($jsonSesion) && strtoupper($jsonSesion['tipo'])
								} //end if condicion del hijo == E
								
							// }//end foreach recorrida hijos
						}//end if check si esta definido $datos[$keyU]['hijo']
						$TotalEntrada[$value['idcurso']]['E'][] = intval($sum);
						$TotalEntrada[$value['idcurso']]['S'][] = intval($sum2);

					}

				// }//end key filter
				
			}//End foreach cursos

	//   var_dump($TotalEntrada);
	//   var_dump($habilidad_sum);
	//   var_dump($habilidad_sum_contrario);

      $this->TotalJSON = json_encode($TotalEntrada);
	  $this->Total = $TotalEntrada;
	  $this->infoEntrada = $infoentrada;
	  $this->infoSalida = $infosalida;
	  $this->TotalHab = $habilidad_sum;
	  $this->TotalHabContrario = $habilidad_sum_contrario;

    //   $_title = $this->entrada == true ? 'Reporte de Examen de Entrada' : 'Reporte de Examen de Salida';
	  $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
	  $this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"), true);
      $this->esquema = 'reportealumno/entradasalida';
      return parent::getEsquema();
		}catch(Exception $e){
      return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function salida(){
		try{
			global $aplicacion;
			$this->entrada = false;
			$this->isIframe = (isset($_REQUEST['isIframe'])) ? true : false ;

			$this->user = NegSesion::getUsuario();

			$idalumnoDNI = (isset($_REQUEST['idalumnoDNI'])) ? $_REQUEST['idalumnoDNI'] : $this->user['idpersona'];
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			// Mostrar/Seleccionar los cursos asignado en matricula
			$this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$idalumno, 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));;
			
			$datos = null;
			$datos_examen_entrada = null;

			$TotalEntrada = null;
			//Obtener todas las habilidades de la bd

			$habilidad_sum = array();
			$habilidad_sum_contrario = array();
			// 4 Listening 5 reading 6 writing 7 speaking
						
			foreach($this->cursos as $key => $value){
				// if($key == 1){
					//inicializar sumatoria de habilidades por curso
					$habilidad_sum[$value['idcurso']]['4'] = 0;
					$habilidad_sum[$value['idcurso']]['5'] = 0;
					$habilidad_sum[$value['idcurso']]['6'] = 0;
					$habilidad_sum[$value['idcurso']]['7'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['4'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['5'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['6'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['7'] = 0;
					//Buscar las unidades del curso...
					$datos= $this->oNegAcad_cursodetalle->sesiones($value['idcurso'],0);
					// var_dump($datos);
					//obtener examen de entradas con el curso especificado por el ciclo
					// $datos_examen_entrada = $this->oNegReportealumno->buscar(array('tipo'=>'examen_e','cond'=>array(
					// 	'idusuario' => $this->user['idpersona'],
					// 	'tiporecurso' => 'E',
					// 	'idcurso' => $value['idcurso'] ))
					// );
					// var_dump($datos[$key]);
					// var_dump($datos[$key]['hijo']);
					foreach($datos as $keyU => $unity){
						//$datos[$keyU]['tiporecurso'] == 'E' 
						$sum = 0;
						//recorrer las unidades para encontrar los examenes
						if(isset($unity['idrecurso'])){
							// foreach($datos[$keyU]['hijo'] as $hijo){
								if($unity['tiporecurso'] == 'E'){
                  $jsonSesion = json_decode($unity['txtjson'],true);
                  if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'S'){
                    if( $valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
                      //verificar si solo hay una fila en el resutlado de la consulta
                      //recorrer varias notas obtenidas
                      foreach($valor as $value2){
                        $sum += $value2['nota'];
                        if(isset($value2['habilidad_puntaje'])){
                          $json = json_decode($value2['habilidad_puntaje'],true);
                          foreach($json as $k => $v){
                            if(intval($k) <= 7){
                              $habilidad_sum[$value['idcurso']][$k] += $v;											
                            }
                          }
                        }
                      }//endforeach recorrer notas
  
                    }//End if valor
                  }//end if !empty($jsonSesion) && strtoupper($jsonSesion['tipo'])
                  if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'E'){
                    //Obtener las habilidades del contrario
                    if( $valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
                      //verificar si solo hay una fila en el resutlado de la consulta
                      //recorrer varias notas obtenidas
                      foreach($valor as $value2){
                        if(isset($value2['habilidad_puntaje'])){
                          $json = json_decode($value2['habilidad_puntaje'],true);
                          foreach($json as $k => $v){
                            if(intval($k) <= 7){
                              $habilidad_sum_contrario[$value['idcurso']][$k] += $v;											
                            }
                          }
                        }
                      }//endforeach recorrer notas
  
                    }//End if valor

                  }//end if !empty($jsonSesion) && strtoupper($jsonSesion['tipo'])
								} //end if condicion del hijo == E
								
							// }//end foreach recorrida hijos
						}//end if check si esta definido $datos[$keyU]['hijo']
						$TotalEntrada[$value['idcurso']]['S'][] = $sum;

					}

				// }//end key filter
				
			}//End foreach cursos

	//   var_dump($TotalEntrada);
	//   var_dump($habilidad_sum);
	//   var_dump($habilidad_sum_contrario);
      $this->TotalJSON = json_encode($TotalEntrada);
	  $this->Total = $TotalEntrada;
	  $this->TotalHab = $habilidad_sum;
	  $this->TotalHabContrario = $habilidad_sum_contrario;

      $_title = $this->entrada == true ? 'Reporte de Examen de Entrada' : 'Reporte de Examen de Salida';
	  $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
	  $this->documento->setTitulo(JrTexto::_($_title), true);
      $this->esquema = 'reportealumno/entradasalida';
      return parent::getEsquema();
		}catch(Exception $e){
      return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function getBimestreTrimestre($idcurso,$idalumno,$datos,&$sumBimestre ,&$sumBimestre_habilidad,&$sumBimestre_habilidad_total,$esBimestre = true){
		$resultado = false;
		try{
			$letter = ($esBimestre == true) ? 'B' : 'T';
			foreach($datos as $keyU => $unity){
				//verificar si es examen en la unidad
				if($unity['tiporecurso'] == 'E'){
					$jsonSesion = json_decode($unity['txtjson'],true);
					if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == $letter){
						if($valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
							$jsonDatos = json_decode($valor[0]['datos'],true);
							if(!empty($jsonDatos)){
								
								$index = strval($jsonDatos['num_examen']);
								
								if(!empty($index)){
									$sumBimestre[$idcurso][$index] += $valor[0]['nota'];
									//resultado por habilidad de cada examen bimestral
									if(isset($valor[0]['habilidad_puntaje'])){
										$jsonHabilidades = json_decode($valor[0]['habilidad_puntaje'],true);
										foreach($jsonHabilidades as $k => $v){
											if(intval($k) <= 7){
												if(isset($sumBimestre_habilidad[$idcurso][$index][$k]) && !empty($sumBimestre_habilidad[$idcurso][$index][$k])){
													
													$sumBimestre_habilidad[$idcurso][$index][$k] += $v; //total por bimestre y habilidad
												}else{
													$sumBimestre_habilidad[$idcurso][$index][$k] = $v; //total por bimestre y habilidad
												}
												$sumBimestre_habilidad_total[$idcurso][$k] += $v; //total por habilidad
											}//end if check if habilidad is true
										}//endforeach
									}//end if isset

								}//endif empty index
							}//end empty
						}//busqueda de recurso
					}
				}//end condition
				$resultado = true;
			}//end foreach
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		return $resultado;
	}

	public function repor_examene()
	{
		try {
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->isIframe = (isset($_REQUEST['isIframe'])) ? true : false ;
			$idalumnoDNI = (isset($_REQUEST['idalumnoDNI'])) ? $_REQUEST['idalumnoDNI'] : $this->user['dni'];
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$idalumno, 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));
			
			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			// Mostrar/Seleccionar los cursos asignado en matricula
			//buscar los examenes del alumno segun su id, filtrar de tipo B y T para mostrar
			$examenes_bimestre = $this->oNegPersonal->buscar(array(
				"idalumno" => $idalumno
				,"tipoJson" => '"tipo":"B"'
				,"idproyecto" => 3
			));
			$examenes_trimestre = $this->oNegPersonal->buscar(array(
				"idalumno" => $idalumno
				,"tipoJson" => '"tipo":"T"'
				,"idproyecto" => 3
			));
			//calcular el total de los examanes bimestrar y trimestral
			// 4 Listening 5 reading 6 writing 7 speaking
			$sumBimestre = array();
			$sumTrimestre = array();
			$sumBimestre_habilidad = array();
			$sumTrimestre_habilidad = array();
			$sumBimestre_habilidad_total = array();
			$_sumBimestre_habilidad_total = array('4' => 0, '5' => 0, '6' => 0, '7' => 0);
			$sumTrimestre_habilidad_total = array();
			$_sumTrimestre_habilidad_total = array('4' => 0, '5' => 0, '6' => 0, '7' => 0);
			if(!empty($this->cursos)){
				foreach ($this->cursos as $key => $val) {
					for($c = 1; $c <= 4; $c++){
						$sumBimestre[$val['idcurso']][strval($c)] = 0;
					}
					for($c = 1; $c <= 3; $c++){
						$sumTrimestre[$val['idcurso']][strval($c)] = 0;
					}
					$sumBimestre_habilidad_total[$val['idcurso']] = $_sumBimestre_habilidad_total;
					$sumTrimestre_habilidad_total[$val['idcurso']] = $_sumTrimestre_habilidad_total;
					//obtiene datos de la sesion por el curso..
					$datos= $this->oNegAcad_cursodetalle->sesiones($val['idcurso'],0);
					$result_b = $this->getBimestreTrimestre($val['idcurso'],$idalumno,$datos,$sumBimestre,$sumBimestre_habilidad,$sumBimestre_habilidad_total); //Bimestre
					$result_t = $this->getBimestreTrimestre($val['idcurso'],$idalumno,$datos,$sumTrimestre,$sumTrimestre_habilidad,$sumTrimestre_habilidad_total,false); //Trimestre

				}
			}//check if not is empty cursos
			
			if(empty($sumBimestre_habilidad)){
				if(!empty($this->cursos)){
					foreach ($this->cursos as $key => $val) {
						for($i = 1; $i <= 4; $i++){
							$sumBimestre_habilidad[$val['idcurso']][strval($i)][4] = 0;
							$sumBimestre_habilidad[$val['idcurso']][strval($i)][5] = 0;
							$sumBimestre_habilidad[$val['idcurso']][strval($i)][6] = 0;
							$sumBimestre_habilidad[$val['idcurso']][strval($i)][7] = 0;
						}
					}
				}
			}
			if(empty($sumTrimestre_habilidad)){
				if(!empty($this->cursos)){
					foreach ($this->cursos as $key => $val) {
						for($i = 1; $i <= 3; $i++){
							$sumTrimestre_habilidad[$val['idcurso']][strval($i)][4] = 0;
							$sumTrimestre_habilidad[$val['idcurso']][strval($i)][5] = 0;
							$sumTrimestre_habilidad[$val['idcurso']][strval($i)][6] = 0;
							$sumTrimestre_habilidad[$val['idcurso']][strval($i)][7] = 0;
						}
					}
				}
			}
			//limitar solamente hasta 100%
			foreach($sumBimestre_habilidad_total as $k => $habilidad){
				foreach($habilidad as $key => $value){
					if($value > 100){
						$sumBimestre_habilidad_total[$k][$key] = ($value / ((ceil($value / 100)) * 100)) * 100;
					}
				}//endforeach
			}
			foreach($sumTrimestre_habilidad_total as $k => $habilidad){
				foreach($habilidad as $key => $value){
					if($value > 100){
						$sumTrimestre_habilidad_total[$k][$key] = ($value / ((ceil($value / 100)) * 100)) * 100;
					}
				}//endforeach
			}

			//depositar el resultado por habilidad de cada examen bimestral y trimestral.
			//enviar datos a la vista...
			//bimestre
			$this->sumBimestre = $sumBimestre;
			$this->sumBimestre_habilidad = $sumBimestre_habilidad;
			$this->sumBimestre_habilidad_total = $sumBimestre_habilidad_total;
			//trimestre
			$this->sumTrimestre = $sumTrimestre;
			$this->sumTrimestre_habilidad = $sumTrimestre_habilidad;
			$this->sumTrimestre_habilidad_total = $sumTrimestre_habilidad_total;
			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reports -- SmartEnglish'), true);
			$this->esquema = 'reportealumno/repor_examene';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function getUnidadesMineduFormat($id){
		try{
			$unidades = array('1' => array(), '2' => array(),'3' => array(),'4' => array(),'5' => array(),'6' => array(),'7' => array(), '8' => array());
		
			$this->lista_unidades=$this->oNegReportealumno->buscar(array('tipo'=>'lista_unidades','idpadre'=> $id));
			for($i = 0; $i < count($this->lista_unidades); $i++ ){
				$index = strval($i+1);
				switch($i){
					case 0: $unidades['1'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					case 1 :
					case 2 : $unidades['2'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					case 3 :
					case 4 : $unidades['3'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					case 5 : $unidades['4'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					case 6 : $unidades['5'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					case 7 :
					case 8 : $unidades['6'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					case 9 :
					case 10 : $unidades['7'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					case 11 : $unidades['8'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					case 12 :
					case 14 : $unidades['9'][$index] = $this->lista_unidades[$i]['idnivel']; 
						break;
					}
			}
			return $unidades;
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function getBimestresMineduFormat($unidades){
		$bimestres = array();
		$j=1;
		for($i=0; $i < count($unidades); $i++){
			if(($i+1) % 2 == 0){
				$bimestres[$j][$i] = $unidades[$i];
				$bimestres[$j][$i+1] = $unidades[$i+1];
				$j++;
			}
			
		}
		return $bimestres;
	}

	public function repor_examens()
	{
		try {
			global $aplicacion;
			$this->entrada = false;

			//$this->documento->script('loader', '/libs/demochart/');
			
			$usuarioAct = NegSesion::getUsuario();
			$this->user = $usuarioAct;
			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;

			// Mostrar/Seleccionar los cursos asignado en matricula
			$this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$usuarioAct['dni'], 'estado'=>1, "idproyecto"=>@$usuarioAct["idproyecto"]));;

			$datos = null;
			$datos_examen_entrada = null;
			
			$id_nivel = ($this->cbobimestre == null) ? 1 : $this->cbobimestre;

			$notasTotalBimestre = null;
			$notasTotalUnidadxBimestre = null;

			//Obtener todas las habilidades de la bd
			$habilidades = $this->oNegReportealumno->buscar(array('tipo'=>'lista_habilidades'));
			// 4 Listening 5 reading 6 writing 7 speaking
			$countHabilidades = array('E' => array('4' => 0, '5' => 0, '6' => 0, '7' => 0), 'S' => array('4' => 0, '5' => 0, '6' => 0, '7' => 0) );
			$sumHabilidades = array('E' => array('4' => 0, '5' => 0, '6' => 0, '7' => 0), 'S' => array('4' => 0, '5' => 0, '6' => 0, '7' => 0) );

			foreach($this->cursos as $key => $value){

				// if($key == 1){

					$datos= $this->oNegAcad_cursodetalle->sesiones($value['idcurso'],0);

					//obtener examen de entradas con el curso especificado por el ciclo
					$datos_examen_entrada = $this->oNegReportealumno->buscar(array('tipo'=>'examen_e','cond'=>array(
						'idusuario' => $usuarioAct['idpersona'],
						'tiporecurso' => 'E',
						'idcurso' => $value['idcurso'] ))
					);
					$datos_examen_salida = $this->oNegReportealumno->buscar(array('tipo'=>'examen_e','cond'=>array(
						'idusuario' => $usuarioAct['idpersona'],
						'tiporecurso' => 'S',
						'idcurso' => $value['idcurso'] ))
					);

					//recorrer datos para obtener las unidades e hijos
					$bimestre_count = 1; //obligado que comience en 1
					foreach($datos as $keyU => $unity){
						$index = $keyU + 2;
						//verificar si esta en una posicion para comenzar un bimestre
						if($index % 2 == 0){
							//recorrer los hijos de las unidades para ver los examenes
							//Sumar primera unidad del bimestre
							$sum1 = 0;
							$sum1_1 = 0;
							$sum1_u =0;
							$sum1_u_s =0;

							foreach($datos[$keyU]['hijo'] as $value2){
								$k= false;
								$k2= false;
								if($value2['tiporecurso'] === 'E'){
									$k = array_search($value2['idrecurso'], array_column($datos_examen_entrada, 'idrecurso'));
									$k2 = array_search($value2['idrecurso'], array_column($datos_examen_salida, 'idrecurso'));
								}
								if( is_bool($k) === false || is_bool($k2) === false ){
									
									//sumar nota de la unidad
									if(is_bool($k) === false && is_bool($k2) === true){

										$sum1 = $sum1 + floatval($datos_examen_entrada[$k]['nota']);

									}elseif (is_bool($k) === true && is_bool($k2) === false){
										$sum1_1 = $sum1_1 + floatval($datos_examen_salida[$k2]['nota']);

									}// }else{
										// $sum1 = $sum1 + floatval($datos_examen_entrada[$k]['nota']);
										// $sum1_1 = $sum1_1 + floatval($datos_examen_salida[$k2]['nota']);
										// var_dump("paso3");
										// var_dump($k);
										// var_dump($k2);
										// var_dump($datos_examen_salida[$k]['nota']);
										// var_dump($datos_examen_salida[$k2]['nota']);
									// }
									$sum1_u = $sum1;
									$sum1_u_s = $sum1_1;

									//sumar por habilidad
									//Obtener las habilidades del examen.
									if($value2['tiporecurso'] === 'E'){
										$habExam = $this->oNegExamenes->buscar($value2['idrecurso']);
										$habExam = json_decode($habExam['habilidades_todas'], true);
										//buscar si existe una habilidad valida para contar y sumar.
										foreach($habExam as $object){
											$k_hab = array_search($object['skill_id'], $countHabilidades);
											if (array_key_exists($object['skill_id'], $countHabilidades)) {
												if($k != false && $k2 == false ){
													$countHabilidades['E'][$object['skill_id']] += 1; 
													$sumHabilidades['E'][$object['skill_id']] += floatval($datos_examen_entrada[$k]['nota']);

												}else if($k == false && $k2 != false){
													$countHabilidades['S'][$object['skill_id']] += 1; 
													$sumHabilidades['S'][$object['skill_id']] += floatval($datos_examen_salida[$k2]['nota']);
												}
											}
										}
									}
									
								}//end if verifica si existe $k
							}
							//Sumar segunda unidad del bimestre
							$sum2 = 0;
							$sum2_u = 0;
							$sum2_2 = 0;
							$sum2_u_s = 0;

							foreach($datos[$index-1]['hijo'] as $value2){
								$k= false;
								$k2= false;
								if($value2['tiporecurso'] === 'E'){
									$k = array_search($value2['idrecurso'], array_column($datos_examen_entrada, 'idrecurso'));
									$k2 = array_search($value2['idrecurso'], array_column($datos_examen_salida, 'idrecurso'));
								}
								if( is_bool($k) === false || is_bool($k2) === false ){
									if(is_bool($k) === false && is_bool($k2) === true){
										$sum2 = $sum2 + floatval($datos_examen_entrada[$k]['nota']);

									}elseif (is_bool($k) === true && is_bool($k2) === false){
										$sum2_2 = $sum2_2 + floatval($datos_examen_salida[$k2]['nota']);

									}   // }else{
									// 	$sum2 = $sum2 + floatval($datos_examen_entrada[$k]['nota']);
									// 	$sum2_2 = $sum2_2 + floatval($datos_examen_salida[$k2]['nota']);
									// 	var_dump("paso3");
									// 	var_dump($k);
									// 	var_dump($k2);
									// 	var_dump($datos_examen_salida[$k]['nota']);
									// 	var_dump($datos_examen_salida[$k2]['nota']);
									// }
									$sum2_u = $sum2;
									$sum2_u_s = $sum2_2;
									//sumar por habilidad
									//Obtener las habilidades del examen.
									if($value2['tiporecurso'] === 'E'){
										$habExam = $this->oNegExamenes->buscar($value2['idrecurso']);
										$habExam = json_decode($habExam['habilidades_todas'], true);
										//buscar si existe una habilidad valida para contar y sumar.
										foreach($habExam as $object){
											// $k_hab = array_search($object->skill_id, $countHabilidades);
											if (array_key_exists($object['skill_id'], $countHabilidades)) {
												if($k != false && $k2 == false ){
													$countHabilidades['E'][$object['skill_id']] += 1; 
													$sumHabilidades['E'][$object['skill_id']] += floatval($datos_examen_entrada[$k]['nota']);
	
												}else if($k == false && $k2 != false){
													$countHabilidades['S'][$object['skill_id']] += 1; 
													$sumHabilidades['S'][$object['skill_id']] += floatval($datos_examen_salida[$k2]['nota']);
												}
											}
										}//endforeach
									}
								}//end if verifica si existe $k
							}//endforeach de datos

							//sumar las dos unidades del bimestre
							$notasTotalBimestre[$value['idcurso']]['E'][] = $sum1 + $sum2;
							$notasTotalBimestre[$value['idcurso']]['S'][] = $sum1_1 + $sum2_2;
							$notasTotalUnidadxBimestre[$value['idcurso']]['E'][$bimestre_count][]= $sum1_u;
							$notasTotalUnidadxBimestre[$value['idcurso']]['E'][$bimestre_count][]= $sum2_u;
							$notasTotalUnidadxBimestre[$value['idcurso']]['S'][$bimestre_count][]= $sum1_u_s;
							$notasTotalUnidadxBimestre[$value['idcurso']]['S'][$bimestre_count][]= $sum2_u_s;

							$bimestre_count++; //contar bimestre
						} //endif verificar si es un bimestre
					} //endfor recorrida de los datos
					
				// } //end if filtro $key
				
			} //foreach cursos

			// $_bimestres = $this->getBimestresMineduFormat($this->getUnidadesMineduFormat($id_nivel));
			

			foreach($sumHabilidades['E'] as $key => $value){
				if(!empty($sumHabilidades['E'][$key])){
					$sumHabilidades['E'][$key] = ($value / (100 * $countHabilidades['E'][$key] )) * 100;
				}
			}
			foreach($sumHabilidades['S'] as $key => $value){
				if(!empty($sumHabilidades['S'][$key])){
					$sumHabilidades['S'][$key] = ($value / (100 * $countHabilidades['S'][$key] )) * 100;
				}
			}


			$this->ToltalBimestre = $notasTotalBimestre; //Enviar a la vista el total de los bimestres
			$this->notasTotalUnidad = $notasTotalUnidadxBimestre; // Enviar el total de las unidades por bimestre
			$this->TotalHabilidades = $sumHabilidades; //total de las habilidades con las notas de entradas.
			// $this->lista_examene=$this->oNegReportealumno->buscar(array('tipo'=>'examen_s','idusuario'=>$usuarioAct['dni']));

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Examen de Salida'), true);
			$this->esquema = 'reportealumno/repor_examene';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function repor_productividad()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			
			$this->lista_curso=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_curso','idusuario'=>$usuarioAct['dni']));
			
			$this->notase=[];
			$this->notass=[];

			if(!empty($this->lista_curso))
        	foreach ($this->lista_curso as $lista1){
        		$idcurso=$lista1['idcurso'];
        		$notas=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_nota','idusuario'=>$usuarioAct['dni'],'idcurso'=>$lista1['idcurso'],'tipo2'=>"E"));
        		$this->notase[$idcurso]= $notas[0]["nota"];

        		$notas=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_nota','idusuario'=>$usuarioAct['dni'],'idcurso'=>$lista1['idcurso'],'tipo2'=>"S"));
        		$this->notass[$idcurso]= $notas[0]["nota"];

        		//echo $notas[0]["nota"];
        	}

			/**/
			

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Productividad'), true);
			$this->esquema = 'reportealumno/repor_productividad';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function repor_progreso()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			$this->user = NegSesion::getUsuario();
			

			$this->cbonivel=@$_POST['cbo_nivel'];
			if (!$this->cbonivel) $this->cbonivel=0;

			$this->cbounidad=@$_POST['cbo_unidad'];
			if (!$this->cbounidad) $this->cbounidad=0;
			
			$this->lista_niveles = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$usuarioAct['dni'], 'estado'=>1, "idproyecto"=>@$usuarioAct["idproyecto"]));;

			//Verificar si es el proyecto del ejercito
			if($this->user['idproyecto'] === '1'){
				$this->cursoCompleto = array();
				if(!empty($this->lista_niveles[0])){
					$this->cursoCompleto = $this->oNegAcad_cursodetalle->sesiones($this->lista_niveles[0]['idcurso'],0);
					if (!empty($this->cbonivel)){
						//recibir notas del curso seleccionado
						$this->notas = $this->oNegReportealumno->buscar(array('tipo'=>'examen_e','cond'=>array(
							'idusuario' => $usuarioAct['dni'],
							'tiporecurso' => 'E',
							'idcurso' => $this->cbonivel ))
						);
						var_dump($this->notas);
						echo "asdsa1111";
					}else{
						//recibir notas del primer curso
						$this->notas = $this->oNegReportealumno->buscar(array('tipo'=>'examen_e','cond'=>array(
							'idusuario' => $usuarioAct['dni'],
							'tiporecurso' => 'E',
							'idcurso' => $this->lista_niveles[0]['idcurso'] ))
						);
						var_dump($this->notas);
						echo "asdsa2222";
					}
					
				}
				// var_dump($this->notas);
				// var_dump($this->cursoCompleto);
			}else{

				// var_dump($this->lista_niveles);
	
				$this->lista_unidades=$this->oNegReportealumno->buscar(array('tipo'=>'lista_unidades','idpadre'=>$this->cbonivel));
	
				//echo @$_POST['cbo_nivel']."as";
	
				// if (!@$_POST['cbo_nivel']){
				// 	$this->lista_progreso=$this->oNegReportealumno->buscar(array('tipo'=>'progreso_niveles','idusuario'=>$usuarioAct['dni']));	
				// }
	
				// if (@$_POST['cbo_nivel'] && !@$_POST['cbo_unidad']){
				// 	$this->lista_progreso=$this->oNegReportealumno->buscar(array('tipo'=>'progreso_unidades','idusuario'=>$usuarioAct['dni'],'idnivel'=>@$_POST['cbo_nivel']));	
				// }
	
				// if (@$_POST['cbo_nivel'] && @$_POST['cbo_unidad']){
				// 	$this->lista_progreso=$this->oNegReportealumno->buscar(array('tipo'=>'progreso_actividades','idusuario'=>$usuarioAct['dni'],'idunidad'=>@$_POST['cbo_unidad']));	
				// }
	
				$this->lista_progreso=$this->oNegReportealumno->buscar(array('tipo'=>'progreso_niveles','idusuario'=>$usuarioAct['dni']));
			}

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Progreso del Estudiante'), true);
			$this->esquema = 'reportealumno/repor_progreso';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function repor_progresohab()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			
			// var_dump($usuarioAct);

			$this->cbonivel=@$_POST['cbo_nivel'];
			if (!$this->cbonivel) $this->cbonivel=0;

			$this->cbounidad=@$_POST['cbo_unidad'];
			if (!$this->cbounidad) $this->cbounidad=0;

			$this->lista_niveles=$this->oNegReportealumno->buscar(array('tipo'=>'lista_niveles','idpadre'=>'0'));

			$this->lista_unidades=$this->oNegReportealumno->buscar(array('tipo'=>'lista_unidades','idpadre'=>$this->cbonivel));

			//echo @$_POST['cbo_nivel']."as";

			/*if (!@$_POST['cbo_nivel']){
				$this->lista_habilidades=$this->oNegReportealumno->buscar(array('tipo'=>'lista_habilidades'));
			}

			if (@$_POST['cbo_nivel'] && !@$_POST['cbo_unidad']){

				$this->lista_habilidades=$this->oNegReportealumno->buscar(array('tipo'=>'lista_habilidades','idnivel'=>@$_POST['cbo_nivel']));

				$this->lista_progreso=$this->oNegReportealumno->buscar(array('tipo'=>'progreso_unidades','idusuario'=>$usuarioAct['dni'],'idnivel'=>@$_POST['cbo_nivel']));	
			}
			/*
			if (@$_POST['cbo_nivel'] && @$_POST['cbo_unidad']){
				$this->lista_habilidades=$this->oNegReportealumno->buscar(array('tipo'=>'lista_habilidades'));
			}*/
			
			$this->habil=[];
			$this->habiltotal=[];
			$this->lista_habilidades=$this->oNegReportealumno->buscar(array('tipo'=>'lista_habilidades'));

			if(!empty($this->lista_habilidades))
        	foreach ($this->lista_habilidades as $lista1){
        		$idhab=$lista1['idmetodologia'];
        		
        		
				if($idhab >= 4 && $idhab <= 7 ){
					if (!@$_POST['cbo_nivel']){
						$total=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'progreso_actividadeshab','idusuario'=>$usuarioAct['dni'],'hab'=>$idhab));
					}
	
					if (@$_POST['cbo_nivel'] && !@$_POST['cbo_unidad']){
	
						$total=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'progreso_actividadeshab_nivel','idusuario'=>$usuarioAct['dni'],'hab'=>$idhab,'idnivel'=>@$_POST['cbo_nivel']));
						
					}
					
					if (@$_POST['cbo_nivel'] && @$_POST['cbo_unidad']){
	
						$total=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'progreso_actividadeshab_unidad','idusuario'=>$usuarioAct['dni'],'hab'=>$idhab,'idunidad'=>@$_POST['cbo_unidad']));
	
						
					}
	
					$totalpor=0;
					$totalcount=0;
					foreach ($this->lista_curso2 as $lista2){
						$totalpor=$totalpor+$lista2['porcentajeprogreso'];
						$totalcount++;
					}
	
					$this->habil[$idhab]= $totalpor;
					$this->habiltotal[$idhab]= $totalcount;
				}

        	}
			
			

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Progreso del Estudiante'), true);
			$this->esquema = 'reportealumno/repor_progresohab';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];				

				$datos=$this->oNegReportealumno->buscar($filtro);

				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
}