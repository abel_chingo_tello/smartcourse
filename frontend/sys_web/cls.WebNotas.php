<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotasenquiz', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
class WebNotas extends JrWeb
{
	private $oNegCursos;
	public function __construct()
	{
		parent::__construct();
		$this->oNegCursos = new NegAcad_curso;
		$this->oNegCursoDetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegNotasenquiz = new NegNotasenquiz;
		$this->oNegNotas_quiz = new NegNotas_quiz;
	}

	public function defecto()
	{
		return $this->obtener();
	}

	public function obtener()
	{
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			$idproyecto=$usuarioAct["idproyecto"];//26; //
			$filtros=array('idproyecto'=>$idproyecto);

			// pasos 00 -- Crea la siguiente tabla;
			/********************************************************
			CREATE TABLE `aa_sesion` (`idalumno` int(11) NOT NULL, `idexamen` int(11) NOT NULL, `idcurso` int(11) NOT NULL, `tipocurso` int(11) NOT NULL,`idsesion` int(11) NOT NULL, `idpestania` int(11) NOT NULL,`usuario` varchar(50) NOT NULL,`idcomplementario` int(11) NOT NULL, `idproyecto` int(11) NOT NULL) ENGINE=InnoDB;
			********************************************************/

			// pasos 01 -- buscar  items a evaluar de alumnos y cursos;
			/***********************************************************/
			$cursos = $this->oNegAcad_grupoauladetalle->cursos($filtros);		
		  	foreach ($cursos["miscursos"] as $key => $cur){
				$tipo=!empty($cur["tipo"])?$cur["tipo"]:0;
				if($tipo==2 && $cur["idcomplementario"]>0){
					$cursotemas = $this->oNegCursoDetalle->temasacalificar($cur["idcurso"], $cur["idcomplementario"], $tipo, $cur["idcategoria"]);
					$cursostmp=array();					
					$alumnos = $this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$cur["idgrupoauladetalle"]));
					foreach ($alumnos as $akey => $alu) {
						foreach ($cursotemas["temasaevaluar"] as $kt => $tem){							
							if($tem["tipo"]=='smartquiz'){
								echo "INSERT INTO aa_sesion values ('".$alu["idalumno"]."','".$tem["idexamen"]."','".$cur["idcurso"]."','".$tipo."','".$tem["idsesion"]."','".$tem["idpestania"]."','".$alu["usuario"]."','".$cur["idcomplementario"]."','".$alu["idproyecto"]."');<br>";
							}
						}						
					}
				}
			}
exit();
			//copia y ejecuta esos escript
			/****************************************/

			// pasos 03 consulta los datos que vas a pasar y crea una tabla de ellos;
			/***********************************************************************
			CREATE TABLE aceglob4_smart_course.aaexalumnos AS SELECT p.identificador,p.usuario, ea.*,ee.calificacion_por,calificacion_en,calificacion_total,calificacion_min,ee.idproyecto as idproyectoexamen,ee.calificacion,ee.habilidades_todas as habilidades,ee.tiempo_total  FROM aceglob4_smart_course_quiz.personal p INNER JOIN aceglob4_smart_course_quiz.examen_alumno ea ON p.dni=ea.idalumno INNER JOIN aceglob4_smart_course_quiz.examenes ee on ea.idexamen=ee.idexamen WHERE date(ea.fecha)=date('2020-05-26')


			++++++++++++++++++++++++++
			CREATE TABLE aceglob4_smart_course.aaexalumnos938
			 AS 
			 SELECT p.identificador,p.usuario , ea.* ,
			ee.calificacion_por,calificacion_en,calificacion_total,calificacion_min,ee.idproyecto as idproyectoexamen,ee.calificacion,ee.habilidades_todas as habilidades,ee.tiempo_total
			 FROM aceglob4_smart_course_quiz.examen_alumno ea INNER JOIN aceglob4_smart_course_quiz.examenes ee ON ea.idexamen=ee.idexamen INNER JOIN aceglob4_smart_course_quiz.personal p ON  dni=idalumno WHERE ea.idexamen=938

			// pasos 04 consulta los datos que vas a pasar y crea una tabla de ellos;
			/***********************************************************************

			create table aaexalumnos1 as SELECT * FROM `aaexalumnos` a1 WHERE puntaje=( select max(puntaje) from aaexalumnos a2 WHERE a2.identificador=a1.identificador AND a2.usuario=a1.usuario AND a2.idexamen=a1.idexamen)


			create table aanotaquiztmp 
			as 
			SELECT ss.idalumno as alumno, ss.idcurso,ss.tipocurso,ss.idsesion,ss.idpestania,ss.idcomplementario,ss.idproyecto, aa.* FROM `aaexalumnos1` aa INNER JOIN aa_sesion ss on identificador=ss.idalumno WHERE aa.idexamen=ss.idexamen
	

			/*$sesiones = $this->oNegNotasenquiz->notas();
			$examenes = $this->oNegNotasenquiz->notas(array('sql'=>2));
			$alu=array();
			if(!empty($examenes))
				foreach($examenes as $ke => $exa){
					foreach ($sesiones as $ks => $vs) {
						if($exa["identificador"]==$vs["idalumno"] && $exa["idexamen"]==$vs["idexamen"]){
							//'idalumno' => 469 
					      	//'idexamen' => 625
					      	//'idcurso' => 410 
					      	//'usuario' => DU62763742
							$cur["idcurso"]=$vs['idcurso'];
							$cur["idproyecto"]=$vs['idproyecto'];
							$cur["idcomplementario"]=$vs['idcomplementario'];
							$cur["idpestania"]=$vs['idpestania'];
							$cur["idsesion"]=$vs['idsesion'];
							$cur["tipo"]=$vs['tipo'];							
							$alu[]=$cur;
						}
					}
				}	
				echo count($examenes)." - ".count($alu);		
			var_dump($alu);*/
			//
			/**/
			//print_r($alum);
			
/*			$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : 0;
			$icc = !empty($_REQUEST["icc"]) ? $_REQUEST["icc"] : 0;
			$tipo = !empty($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : 1;
			$idcategoria = !empty($_REQUEST["idcategoria"]) ? $_REQUEST["idcategoria"] : '';
			
			var_dump($this->curso);*/
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	/*public function guardarNotas(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$sesiones = $this->oNegNotasenquiz->notas(array("sql"=>"SELECT * FROM aanotaquiztmp"));
			echo count($sesiones)."--<br>";
			set_time_limit (0) ;
			foreach ($sesiones as $ses => $v){
				$notaquiz=$this->oNegNotas_quiz->buscar(array('idexamenidalumno'=>$v["idalumno"],'idalumno'=>$v["alumno"],'idrecurso'=>@$v["idexamen"],'idproyecto'=>$v["idproyecto"],'idcursodetalle'=>$v["idsesion"],'idcurso'=>$v["idcurso"],'tipo'=>'E','idcomplementario'=>$v["idcomplementario"],'idexamenproyecto'=>$v["idproyectoexamen"]));
				$accion="_add";
				if(!empty($notaquiz[0])){
					$this->oNegNotas_quiz->idnota=$notaquiz[0]["idnota"];
					if($this->oNegNotas_quiz->nota>$v["puntaje"]){
						$accion="ninguno";
					}else
					$accion='_edit';
				}

				$this->oNegNotas_quiz->idcursodetalle=$v["idsesion"];
				$this->oNegNotas_quiz->idcurso=$v["idcurso"];
				$this->oNegNotas_quiz->idrecurso=$v["idexamen"];
				$this->oNegNotas_quiz->idalumno=$v["alumno"];
				$this->oNegNotas_quiz->tipo='E';
				$this->oNegNotas_quiz->nota=$v["puntaje"];
				$this->oNegNotas_quiz->notatexto=$v["resultado"];				
				$this->oNegNotas_quiz->idproyecto=$v["idproyecto"];
				$this->oNegNotas_quiz->calificacion_en=$v["calificacion_en"];
				$this->oNegNotas_quiz->calificacion_total=$v["calificacion_total"];
				$this->oNegNotas_quiz->calificacion_min=$v["calificacion_min"];
				$this->oNegNotas_quiz->tiempo_total=$v["tiempo_total"];
				$this->oNegNotas_quiz->tiempo_realizado=$v["tiempoduracion"];
				$this->oNegNotas_quiz->calificacion=$v["calificacion"];
				$this->oNegNotas_quiz->habilidades=$v["habilidades"];
				$this->oNegNotas_quiz->habilidad_puntaje=$v["puntajehabilidad"];
				$this->oNegNotas_quiz->intento=$v["intento"];
				$this->oNegNotas_quiz->idexamenalumnoquiz=$v["idexaalumno"];
				$this->oNegNotas_quiz->idexamenproyecto=$v["idproyectoexamen"];
				$this->oNegNotas_quiz->idexamenidalumno=$v["idalumno"];
				
				if($accion=='_add'){
					$this->oNegNotas_quiz->regfecha=!empty($v["fecha"])?$v["fecha"]:date('Y-m-d');
					$this->oNegNotas_quiz->idcomplementario= $v["idcomplementario"];
					$this->oNegNotas_quiz->datos = '{"tipo":"A","num_examen":0,"R":"recuperado"}';
					$this->oNegNotas_quiz->preguntas= 'nopasado';
					$res=$this->oNegNotas_quiz->agregar();
					echo "agregado: ".$v["idcurso"]."--".$ses."  -- ".$res."<br>";
				} elseif($accion=='_edit'){
					$res=$this->oNegNotas_quiz->editar();
					echo "editado: ".$v["idcurso"]."--".$ses."  -- ".$res."<br>";
				}
			}

			//AA70717089
			exit();
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getLine())));
			exit();
		}
	}*/

}