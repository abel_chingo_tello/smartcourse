<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-05-2017 
 * @copyright	Copyright (C) 08-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegConfiguracion_docente', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
class WebConfiguracion_docente extends JrWeb
{
	private $oNegConfiguracion_docente;
	protected $oNegNiveles;
    private $oNegResources;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegConfiguracion_docente = new NegConfiguracion_docente;
		$this->oNegNiveles = new NegNiveles;
        $this->oNegResources = new NegResources;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Configuracion_docente', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegConfiguracion_docente->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Configuracion_docente'), true);
			$this->esquema = 'configuracion_docente-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Configuracion_docente', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Configuracion_docente').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Configuracion_docente', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegConfiguracion_docente->idconfigdocente = @$_GET['id'];
			$this->datos = $this->oNegConfiguracion_docente->dataConfiguracion_docente;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Configuracion_docente').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegConfiguracion_docente->idconfigdocente = @$_GET['id'];
			$this->datos = $this->oNegConfiguracion_docente->dataConfiguracion_docente;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Configuracion_docente').' /'.JrTexto::_('see'), true);
			$this->esquema = 'configuracion_docente-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'configuracion_docente-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function modal_config_tiempos()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Configuracion_docente', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$usuarioAct = NegSesion::getUsuario();
			$filtros = array(
				'iddocente' => $usuarioAct["dni"],
				'idrol' => $usuarioAct["rol"],
			);
			$this->config_doc=$this->oNegConfiguracion_docente->buscar($filtros);
			$this->frmaccion='Nuevo';
			if(!empty($this->config_doc)){ $this->frmaccion='Editar'; };
			$this->mostrarCampo = array(
				'tiempototal' => true,
				'tiempoactividades' => true,
				'tiempoteacherresrc' => true,
				'tiempogames' => true,
				/*'tiempoexamenes' => true,*/
			);
			$this->esquema = 'docente/modal_configuracion_docente';
			$this->documento->plantilla = 'modal';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function tiemposXactividad()
	{
		$this->documento->plantilla = 'returnjson';
		try {               
			$filtro=array(
				'idpadre'=> $_POST['idpadre'],
				'tipo'=> $_POST['tipo']
			);
			$usuarioAct = NegSesion::getUsuario();
			$filtroT=array(
				/*'iddocente' => $usuarioAct["dni"],
				'idrol' => $usuarioAct["rol"],*/
				'idactividad'=> null,
			);
			$actividades = array();
			if($filtro['tipo']=='L') { $actividades=$this->oNegNiveles->buscar($filtro); }
			if($filtro['tipo']=='U') { $actividades=$this->oNegNiveles->buscarActxIdNivel($filtro); }
			$i=0;
			if(!empty($actividades)){
				foreach ($actividades as $act) {
					$filtroT['idactividad']=$act['idnivel'];
					$tiempo = $this->oNegConfiguracion_docente->buscar($filtroT);
					$actividades[$i]['tiempoactividades']=(empty($tiempo))?'00:00:00':$tiempo[0]['tiempoactividades'];
					$actividades[$i]['tiempoteacherresrc']=(empty($tiempo))?'00:00:00':$tiempo[0]['tiempoteacherresrc'];
					$actividades[$i]['tiempogames']=(empty($tiempo))?'00:00:00':$tiempo[0]['tiempogames'];
					$actividades[$i]['tiempototal']=(empty($tiempo))?'00:00:00':$tiempo[0]['tiempototal'];
					$actividades[$i]['idconfigdocente']=(empty($tiempo))?null:$tiempo[0]['idconfigdocente'];
					$actividades[$i]['escalas']=(empty($tiempo))?null:$tiempo[0]['escalas'];
					$i++;
				}
			}
            echo json_encode(array('code'=>'ok','data'=>$actividades));
            return parent::getEsquema();
		} catch(Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		} 
	}

	public function xGuardarConfigDocente()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			
			if(empty($_POST['arrConfigs'])) {
				throw new Exception(JrTexto::_('There are no values to register'));
			}
			$arrConfigs = json_decode($_POST['arrConfigs'], true);
			$usuarioAct = NegSesion::getUsuario();
			foreach ($arrConfigs as $conf) {
				if($conf['idconfigdocente']!==null){
					$this->oNegConfiguracion_docente->idconfigdocente = @$conf['idconfigdocente'];
				}
				$this->oNegConfiguracion_docente->__set('tiempototal',@$conf['tiempototal']);
				$this->oNegConfiguracion_docente->__set('tiempoactividades',@$conf['tiempoactividades']);
				$this->oNegConfiguracion_docente->__set('tiempoteacherresrc',@$conf['tiempoteacherresrc']);
				$this->oNegConfiguracion_docente->__set('tiempogames',@$conf['tiempogames']);
				$this->oNegConfiguracion_docente->__set('escalas',@$conf['escalas']);
				//$this->oNegConfiguracion_docente->__set('tiempoexamenes',@$conf['tiempoexamenes']);
				if($conf['idconfigdocente']==null){
					$this->oNegConfiguracion_docente->__set('iddocente',@$usuarioAct["dni"]);
					$this->oNegConfiguracion_docente->__set('idrol',@$usuarioAct["rol"]);
					$this->oNegConfiguracion_docente->__set('idnivel',@$conf['idnivel']);
					$this->oNegConfiguracion_docente->__set('idunidad',@$conf['idunidad']);
					$this->oNegConfiguracion_docente->__set('idactividad',@$conf['idactividad']);
					$res=$this->oNegConfiguracion_docente->agregar();
				}else{
					$res=$this->oNegConfiguracion_docente->editar();
			    }
			    if(empty($res)){ throw new Exception(JrTexto::_('An error has occurred trying yo insert values')); }
			}
            echo json_encode(array('code'=>'ok','data'=>array('mensaje'=>JrTexto::_('Updated data') )));
            return parent::getEsquema();
		} catch (Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
		}
	}
	
	// ========================== Funciones xajax ========================== //
	public function xSaveConfiguracion_docente(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdconfigdocente'])) {
					$this->oNegConfiguracion_docente->idconfigdocente = $frm['pkIdconfigdocente'];
				}
				$usuarioAct = NegSesion::getUsuario();
				$iddocente = $usuarioAct["dni"];
				$idrol = $usuarioAct["rol"];
				
				$this->oNegConfiguracion_docente->__set('iddocente',@$iddocente);
				$this->oNegConfiguracion_docente->__set('idrol',@$idrol);
				$this->oNegConfiguracion_docente->__set('idnivel',@$frm['opcIdnivel']);
				$this->oNegConfiguracion_docente->__set('idunidad',@$frm['opcIdunidad']);
				$this->oNegConfiguracion_docente->__set('idactividad',@$frm['opcIdactividad']);
				$this->oNegConfiguracion_docente->__set('tiempototal',@$frm["txtTiempototal"]);
				$this->oNegConfiguracion_docente->__set('tiempoactividades',@$frm["txtTiempoactividades"]);
				$this->oNegConfiguracion_docente->__set('tiempoteacherresrc',@$frm["txtTiempoteacherresrc"]);
				$this->oNegConfiguracion_docente->__set('tiempogames',@$frm["txtTiempogames"]);
				$this->oNegConfiguracion_docente->__set('tiempoexamenes',@$frm["txtTiempoexamenes"]);
				
			    if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegConfiguracion_docente->agregar();
				}else{
					$res=$this->oNegConfiguracion_docente->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegConfiguracion_docente->idconfigdocente);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDConfiguracion_docente(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegConfiguracion_docente->__set('idconfigdocente', $pk);
				$this->datos = $this->oNegConfiguracion_docente->dataConfiguracion_docente;
				$res=$this->oNegConfiguracion_docente->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegConfiguracion_docente->__set('idconfigdocente', $pk);
				$res=$this->oNegConfiguracion_docente->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}