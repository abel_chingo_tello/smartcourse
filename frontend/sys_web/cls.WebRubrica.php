<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020 
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRubrica', RUTA_BASE);
class WebRubrica extends JrWeb
{
	private $oNegRubrica;

	public function __construct()
	{
		parent::__construct();
		$this->oNegRubrica = new NegRubrica;
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('es', '/libs/select2/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');

			$this->documento->stylesheet('mine-rubrica', '/libs/othersLibs/rubrica/');
			$this->documento->script('mine-rubrica', '/libs/othersLibs/rubrica/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros = array();
			if (isset($_REQUEST["idrubrica"]) && @$_REQUEST["idrubrica"] != '') $filtros["idrubrica"] = $_REQUEST["idrubrica"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["fecha_creacion"]) && @$_REQUEST["fecha_creacion"] != '') $filtros["fecha_creacion"] = $_REQUEST["fecha_creacion"];
			if (isset($_REQUEST["idproyecto"]) && @$_REQUEST["idproyecto"] != '') $filtros["idproyecto"] = $_REQUEST["idproyecto"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];

			$this->datos = $this->oNegRubrica->buscar($filtros);
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Rubrica'), true);
			$this->esquema = 'rubrica/rubrica';
			$usuarioAct = NegSesion::getUsuario();
			if ($usuarioAct['idrol'] == "3") {
				$this->msj="NO TIENES PERMISOS PARA ACCEDER A ESTE MÓDULO";
				$this->esquema = 'error/general';
			}
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Rubrica', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Nuevo';
			$this->documento->setTitulo(JrTexto::_('Rubrica') . ' /' . JrTexto::_('New'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Rubrica', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Editar';
			$this->oNegRubrica->idrubrica = @$_GET['id'];
			$this->datos = $this->oNegRubrica->dataRubrica;
			$this->pk = @$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Rubrica') . ' /' . JrTexto::_('Edit'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'rubrica-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
