<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-05-2017 
 * @copyright	Copyright (C) 22-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAulasvirtuales', RUTA_BASE, 'sys_negocio');
class WebAulasvirtuales extends JrWeb
{
	private $oNegAulasvirtuales;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAulasvirtuales = new NegAulasvirtuales;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Aulasvirtuales', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegAulasvirtuales->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Aulasvirtuales'), true);
			$this->esquema = 'aulasvirtuales-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Aulasvirtuales', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Aulasvirtuales').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Aulasvirtuales', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegAulasvirtuales->aulaid = @$_GET['id'];
			$this->datos = $this->oNegAulasvirtuales->dataAulasvirtuales;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Aulasvirtuales').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegAulasvirtuales->aulaid = @$_GET['id'];
			$this->datos = $this->oNegAulasvirtuales->dataAulasvirtuales;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Aulasvirtuales').' /'.JrTexto::_('see'), true);
			$this->esquema = 'aulasvirtuales-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'aulasvirtuales-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveAulasvirtuales(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkAulaid'])) {
					$this->oNegAulasvirtuales->aulaid = $frm['pkAulaid'];
				}
				
				$this->oNegAulasvirtuales->__set('idnivel',@$frm["txtIdnivel"]);
					$this->oNegAulasvirtuales->__set('idunidad',@$frm["txtIdunidad"]);
					$this->oNegAulasvirtuales->__set('idactividad',@$frm["txtIdactividad"]);
					$this->oNegAulasvirtuales->__set('fecha_inicio',@$frm["txtFecha_inicio"]);
					$this->oNegAulasvirtuales->__set('fecha_final',@$frm["txtFecha_final"]);
					$this->oNegAulasvirtuales->__set('titulo',@$frm["txtTitulo"]);
					$this->oNegAulasvirtuales->__set('descripcion',@$frm["txtDescripcion"]);
					$this->oNegAulasvirtuales->__set('moderadores',@$frm["txtModeradores"]);
					$this->oNegAulasvirtuales->__set('estado',@$frm["txtEstado"]);
					$this->oNegAulasvirtuales->__set('video',@$frm["txtVideo"]);
					$this->oNegAulasvirtuales->__set('chat',@$frm["txtChat"]);
					$this->oNegAulasvirtuales->__set('notas',@$frm["txtNotas"]);
					$this->oNegAulasvirtuales->__set('dirigidoa',@$frm["txtDirigidoa"]);
					$this->oNegAulasvirtuales->__set('estudiantes',@$frm["txtEstudiantes"]);
					$this->oNegAulasvirtuales->__set('dni',@$frm["txtDni"]);
					$this->oNegAulasvirtuales->__set('fecha_creado',@$frm["txtFecha_creado"]);
					$this->oNegAulasvirtuales->__set('portada',@$frm["txtPortada"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAulasvirtuales->agregar();
					}else{
									    $res=$this->oNegAulasvirtuales->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAulasvirtuales->aulaid);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAulasvirtuales(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAulasvirtuales->__set('aulaid', $pk);
				$this->datos = $this->oNegAulasvirtuales->dataAulasvirtuales;
				$res=$this->oNegAulasvirtuales->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAulasvirtuales->__set('aulaid', $pk);
				$res=$this->oNegAulasvirtuales->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}