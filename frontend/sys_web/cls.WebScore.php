<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-05-2017 
 * @copyright	Copyright (C) 11-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMinedu', RUTA_BASE, 'sys_negocio');

class WebScore extends JrWeb{

    private $oNegMatricula;
    private $oNegPersonal;
    private $oNegAcad_curso;
    private $oNegActividad_alumno;
    private $oNegAcad_cursodetalle;
    private $oNegMinedu;

    public function __construct()
	{
        parent::__construct();
        $this->oNegPersonal = new NegPersonal;
        $this->oNegAcad_curso = new NegAcad_curso;
        $this->oNegMatricula = new NegAcad_matricula;	
        $this->oNegActividad_alumno = new NegActividad_alumno;
        $this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
        $this->oNegMinedu = new NegMinedu;
    }
    
    public function defecto(){
		return $this->ver();
    }
    public function progresohabilidadunidad(){
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            $this->user = NegSesion::getUsuario();
            $idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
            $idrecurso = (isset($_REQUEST['idrecurso'])) ? $_REQUEST['idrecurso'] : null;
            //$idactividad = (isset($_REQUEST['idactividad'])) ? $_REQUEST['idactividad'] : null;
            $idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $data  = array();
            
            

            if(!is_null($idcurso) && !is_null($idrecurso)){
                $data = array('4' => 0, '5' =>  0, '6' => 0, '7' => 0);
                $habilidades_count = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
                
                $sesiones = $this->oNegAcad_cursodetalle->sesiones2($idcurso,0);
                //throw new Exception("Error Processing Request", 1);
                foreach($sesiones as $value){
                    if($value['idcurso'] == $idcurso && $value['tiporecurso'] != 'E' && $value['idrecurso'] == $idrecurso){
                        
                        //var_dump($value);
                        foreach($value['hijo'] as $activity){
                        //var_dump($activity);

                                //foreach($unity as $activity){
                                    if($valor = $this->oNegActividad_alumno->buscar(array('idalumno' => $idalumno,'sesion' => $activity['idrecurso']))){
                                        
                                        $habilidades = null;
                                        foreach($valor as $result){
                                            $habilidades = (!empty($result['habilidades'])) ? explode('|',$result['habilidades']) : null;
                                            
                                            for($i=0; $i < count($habilidades); $i++){
                                                if(isset($data[$habilidades[$i]])){
                                                    $data[$habilidades[$i]] += $result['porcentajeprogreso'];
                                                    $habilidades_count[$habilidades[$i]] += 1;
                                                }
                                            }//end for habilidades de resultado
                                        }
                                    }//end if valor
                                //}
                            
                        }//end foreach value
                    }
                }
                
                foreach($data as $ckey => $cvalue){
                    $data[$ckey] = ($cvalue > 0) ? round(($cvalue*100)/($habilidades_count[$ckey]*100),2)  : 0;
                }

            }//endif check main

            return json_encode(array('code'=>'ok','data'=>$data));
        }catch(Exception $e){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function progresohabilidad(){
        $this->documento->plantilla = 'returnjson';
        try{
            //$tiempo_inicial = microtime(true); //true es para que sea calculado en segundos
            global $aplicacion;
            $this->user = NegSesion::getUsuario();
            $idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
            // $idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
            $idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;

            $datos = array();
            $_data = array();
            $habilidad_total = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
            if(!is_null($idalumno)){
                $tmp_cursoname = $this->oNegAcad_curso->buscar(array('idcurso' => $idcurso,'estado'=>1, "idproyecto"=>'3'));
                $cursoname = (!empty($tmp_cursoname)) ? strtoupper($tmp_cursoname[0]['nombre']) : 'A1';
                $obtenerprogresos = $this->oNegMinedu->progresosresumen(array('idalumno' => $idalumno));
                if(!empty($obtenerprogresos)){
                    $habilidad_total['4'] = $obtenerprogresos[0]['prog_hab_L_'.$cursoname];
                    $habilidad_total['5'] = $obtenerprogresos[0]['prog_hab_R_'.$cursoname];
                    $habilidad_total['6'] = $obtenerprogresos[0]['prog_hab_W_'.$cursoname];
                    $habilidad_total['7'] = $obtenerprogresos[0]['prog_hab_S_'.$cursoname];
                }
            }
            // $habilidad_total = array();
            // $habilidades_count = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
            // if(!empty($idcurso)){
            //     $habilidad_total[$idcurso] = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
            //     $datos = $this->oNegAcad_cursodetalle->sesiones2($idcurso,0);

            //     foreach($datos as $unity){
            //         if(isset($unity['tiporecurso']) && $unity['tiporecurso'] != 'E'){
            //             if(isset($unity['hijo'])){
            //                 foreach($unity['hijo'] as $keyU => $activity){
            //                     if($valor = $this->oNegActividad_alumno->buscar(array('idalumno' => $idalumno,'sesion' => $activity['idrecurso']))){

            //                         $habilidades = null;
            //                         foreach($valor as $result){
            //                             $habilidades = (!empty($result['habilidades'])) ? explode('|', $result['habilidades']) : null;
            //                             for ($i=0; $i < count($habilidades); $i++) { 
            //                                 if(isset($habilidad_total[$idcurso][$habilidades[$i]]) ){
            //                                     $habilidad_total[$idcurso][$habilidades[$i]] += $result['porcentajeprogreso'];
            //                                     $habilidades_count[$habilidades[$i]] += 1;
            //                                     //var_dump($habilidad_total[$value['idcurso']][$habilidades[$i]]);
            //                                 }
            //                             }//end for
            //                         }//end foreach result
            //                     }
            //                 }//end foreach
            //             }//end if isset
            //         }
            //     }//end foreach
            //     foreach($habilidad_total[$idcurso] as $ckey => $cvalue){
            //         $habilidad_total[$idcurso][$ckey] = ($cvalue > 0) ? ($cvalue*100)/($habilidades_count[$ckey]*100) : 0;
            //     }
            // }
            //$tiempo_final = microtime(true);
            //echo $tiempo = $tiempo_final - $tiempo_inicial; //este resultado estará en segundos
            return json_encode(array('code'=>'ok','data'=>$habilidad_total));
        }catch(Exception $e){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
        
    }
    public function ver(){
        try{
            global $aplicacion;
            $this->user = NegSesion::getUsuario();
            $idalumno = $this->user['idpersona'];
            $idproyecto = $this->user['idproyecto'];

            $this->cursos = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$idalumno, 'estado'=>1, "idproyecto"=>$idproyecto));
            $progresoCurso = array();
            $cursoScore = array();
            
            
            // var_dump($this->cursos);
            // exit();
            if(!empty($this->cursos)){
                foreach($this->cursos as $key => $value){
                    if($value['idcurso'] == 31 || $value['idcurso'] == 35 || $value['idcurso'] == 3){
                        $progresoCurso[$value['idcurso']] = null;
                        $habilidad_total[$value['idcurso']] = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
                        $value['progreso'] = 0;
                        $temas = $this->oNegAcad_cursodetalle->sesiones2($value['idcurso'],0);
                        foreach($temas as $tem){
                            $tem["progreso"]=0;
                            
                            if( $tem['tiporecurso'] != 'E'){

                                $tem=$this->oNegAcad_curso->getProgresoUnidad($tem,$value['idcurso'],$idalumno);
                                $value['progreso'] += $tem["progreso"];
                                // $value["nactividad"]= $tem['nhijo'];
                                $progresoCurso[$value['idcurso']][] = $tem;
                            }//endif
                        }//endforeach
                        $cursoScore[$value['idcurso']] = $value;
                    }
                }//endforeach
            }//end if cursos
            $this->progresos = $progresoCurso;
            $this->cursoScore = $cursoScore;

            //Importar libreria para los charts 
	        $this->documento->script('Chart.min', '/libs/chartjs/');
            $this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');

            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->documento->setTitulo(JrTexto::_('Scores'), true);
            $this->esquema = 'score/index';
            return parent::getEsquema();
        }catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
        }

    }
}

?>