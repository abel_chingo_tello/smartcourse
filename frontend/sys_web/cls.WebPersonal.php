<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-01-2018 
 * @copyright	Copyright (C) 31-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRoles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_apoderado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_educacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_experiencialaboral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_referencia', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegPersona_record', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegAcad_horariogrupodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
class WebPersonal extends JrWeb
{
	private $oNegPersonal;
	private $oNegGeneral;
	private $oNegUbigeo;
	private $oNegUgel;
	private $oNegRoles;
	private $oNegAcad_matricula;
	private $oNegAcad_grupoaula;
	private $oNegHorariogrupoaula;
	private $oNegAcad_grupoauladetalle;	
	private $oNegAcad_curso;
	private $oNegPersona_rol;
	private $oNegPerRecord;
	private $oNegMin_dre;
	private $oNegProyecto;

	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
		$this->oNegPerApoderado = new NegPersona_apoderado;
		$this->oNegPerMetas = new NegPersona_metas;
		$this->oNegPerEducacion = new NegPersona_educacion;
		$this->oNegPerExplaboral = new NegPersona_experiencialaboral;
		$this->oNegPerReferencia = new NegPersona_referencia;
		$this->oNegPerMetas = new NegPersona_metas;
		// $this->oNegPerRecord = new NegPersona_record;
		$this->oNegGeneral = new NegGeneral;
		$this->oNegUbigeo = new NegUbigeo;
		$this->oNegUgel = new NegUgel;
		$this->oNegRoles = new NegRoles;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegGrupoaula = new NegAcad_grupoaula;
		$this->oNegGrupoauladetalle = new NegAcad_grupoauladetalle;
		// $this->oNegHorariogrupoaula = new NegAcad_horariogrupodetalle;
		$this->oNegCurso = new NegAcad_curso;
		$this->oNegPersona_rol = new NegPersona_rol;
		$this->oNegDre =new NegMin_dre;
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegProyecto = new NegProyecto;
	}

	public function defecto(){
       // $this->documento->script('events', '/libs/fullcalendar/');    
		return $this->filtros();
	}

	public function filtros()
	{
		try{
			// echo $_GET["xyz"];
			// exit();
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');			
            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->script('datetimepicker.min', '/libs/datetimepicker/js/');
			$this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
			$usuarioAct = NegSesion::getUsuario();
			$this->idRolUser=$usuarioAct["idrol"];
			$this->idproyecto=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			if(!empty($_REQUEST["idproyecto"]))$this->idproyecto=$_REQUEST["idproyecto"];
			//$this->documento->script('croppie.min', '/libs/croppie/');
			//$this->documento->stylesheet('croppie', '/libs/croppie/');
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			//$this->fkubigeo=$this->oNegUbigeo->buscar();
			//$this->fkidugel=$this->oNegUgel->buscar();
			$this->idrol=!empty($_REQUEST["rol"])?$_REQUEST["rol"]:1;
			$this->idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->fkrol=$this->oNegRoles->buscar();
			//$this->fkcursos=$this->oNegCurso->buscarxproyecto(array('idproyecto'=>$this->idproyecto,'orderby'=>'nombre'));
			$this->fkcursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>$this->idproyecto));
			///var_dump($this->fkcursos);
			$vista = !empty($_REQUEST['verlis']) ? $_REQUEST['verlis'] : '';
			$this->documento->setTitulo(JrTexto::_('Personal'), true);
			$this->dress=$this->oNegDre->buscar(array('idproyecto' => $this->idproyecto));
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';

			if($usuarioAct["tuser"]=='s')
				$this->esquema = 'academico/personal-list';
			else 
				$this->esquema = 'academico/personal';
			//var_dump($this->esquema);
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function listado(){
		try{
			global $aplicacion;
			$filtros=array();
			$usuarioAct = NegSesion::getUsuario();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='') $filtros["rol"]=$_REQUEST["rol"];
			else{$filtros["rol"]=1;}
			$this->idrol=$filtros["rol"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];

			if(isset($_REQUEST["dre"])&&@$_REQUEST["dre"]!=''&&@$_REQUEST["dre"]!='undefined')$filtros["iddre"]=$_REQUEST["dre"];
			if(isset($_REQUEST["ugel"])&&@$_REQUEST["ugel"]!=''&&@$_REQUEST["ugel"]!='undefined')$filtros["idugel"]=$_REQUEST["ugel"];
			if(isset($_REQUEST["iiee"])&&@$_REQUEST["iiee"]!=''&&@$_REQUEST["iiee"]!='undefined')$filtros["idiiee"]=$_REQUEST["iiee"];
			if(isset($_REQUEST["curso"])&&@$_REQUEST["curso"]!=''&&@$_REQUEST["curso"]!='undefined')$filtros["idcurso"]=$_REQUEST["curso"];
			$filtros["idproyecto"]=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			if(!empty($_REQUEST["idproyecto"]))$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			//$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			//$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->datos=$this->oNegPersonal->buscar($filtros);
			$this->documento->setTitulo(JrTexto::_('Personal').' /', true);
			$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:'1';
			$this->esquema = 'usuario/personal-vista'.$vista;
			//var_dump($this->esquema);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	
	public function buscar()
	{
		try{
			global $aplicacion;				
			$usuarioAct = NegSesion::getUsuario();			
			$this->idproyecto=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			$this->idrol=!empty($_REQUEST["idrol"])?$_REQUEST["idrol"]:1;
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personas'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->esquema = 'personal-buscar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function adicionar()
	{
		try{
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->idproyecto=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			$this->idrol=!empty($_REQUEST["idrol"])?$_REQUEST["idrol"]:1;				
			$this->documento->setTitulo(JrTexto::_('Personas'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->esquema = 'personal-agregaryretornar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Curse').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->frmaccion='Editar';
			$this->oNegPersonal->idpersona = !empty($_REQUEST['idpersona'])?$_REQUEST['idpersona']:(!empty($_REQUEST['id'])?$_REQUEST['id']:0);
			$this->datos = $this->oNegPersonal->dataPersonal;
			$this->pk=$this->oNegPersonal->idpersona;
			$this->documento->setTitulo(JrTexto::_('Curse').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
            $this->documento->script('datetimepicker.min', '/libs/datetimepicker/js/');
			$this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');		
			$usuarioAct = NegSesion::getUsuario();
			$this->idproyecto=$usuarioAct["idproyecto"];
			$this->esquema = 'personal-frm';
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->idrol=!empty($_REQUEST["idrol"])?$_REQUEST["idrol"]:3;
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function formulario(){
		try {
			
			global $aplicacion;			
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->esquema = 'usuario/formulario';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			$this->isdocente=false;
			$this->isalumno=false;
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
				if(!empty($this->datos)) $this->isdocente=$this->datos["rol"]==2?true:false;
				if(!empty($this->datos)) $this->isalumno=$this->datos["rol"]==3?true:false;
				if($this->isdocente==false){	
					$roli=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idrol'=>2));
					$this->isdocente=!empty($roli[0])?true:false;
				}
				if($this->isalumno==false){	
					$roli=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idrol'=>3));
					$this->isalumno=!empty($roli[0])?true:false;
				}
			}
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function formularioreturn(){
		try {
			global $aplicacion;	
			$this->esquema = 'usuario/formularioreturn';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function roles(){
		try {
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->esquema = 'personal-roles';
			$idpersona=!empty($_REQUEST["idpersona"])?$_REQUEST["idpersona"]:$this->usuarioAct["idpersona"];
			$this->roles=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idproyecto'=>$this->usuarioAct["idproyecto"]));
			$this->fkrol=$this->oNegRoles->buscar();
			//var_dump($this->roles);
			//var_dump($this->fkrol);
			
			$this->oNegPersonal->idpersona=$idpersona;
			$this->datospersona=$this->oNegPersonal->getXid();
			
            $this->dress=$this->oNegDre->buscar();			
			$iddep=$this->oNegUgel->buscar(array('idugel'=>$this->datospersona["idugel"]));
			if(!empty($iddep[0])){
				$dre=$this->oNegDre->buscar(array('ubigeo'=>$iddep[0]["iddepartamento"]));
				if(!empty($dre[0])){$this->strdre=$dre[0]["descripcion"];
					$this->iddre=$dre[0]["ubigeo"];
				}
			}
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importar(){
		try {
			global $aplicacion;
			$this->esquema = 'importar/personal-importar';
			$this->idrol=!empty($_REQUEST["idrol"])?$_REQUEST["idrol"]:3;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importardatos(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(empty($_POST)){
				echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 		exit(0);
			}else{
				if(!empty($_POST["datosimportados"])){
					$dt=json_decode($_POST["datosimportados"]);
					if(!empty($dt)){
						$this->datos=array();
						foreach($dt as $v){
							$this->datos[]=$this->oNegPersonal->importar(@$v->dni,@$v->tipodoc,@$v->dni,@$v->ape_paterno,@$v->ape_materno,@$v->nombre,@$v->fechanac,@$v->sexo,@$v->estado_civil,$v->ubigeo,$v->urbanizacion,@$v->direccion,@$v->telefono,@$v->celular,@$v->email,@$v->idugel,$this->usuarioAct["idpersona"],date('Y-m-d'),@$v->usuario,@$v->clave,@$v->token,@$v->idrol,@$v->foto,@$v->estado,@$v->situacion,@$v->idioma,'n',@$v->idiiee,@$v->rol,@$v->ugel,@$v->iiee,$this->usuarioAct["idproyecto"],@$this->usuarioAct["idempresa"]);
						}
					}
				}
			}
			echo json_encode(array('code'=>'ok','data'=>$this->datos,'msj'=>JrTexto::_("Datos Importados")));
		 	exit(0);
		}catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

    /******** menus primeros */
	public function informacion(){
		try {
			global $aplicacion;			
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');			
			$this->esquema = 'usuario/formulario-informacion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			$this->isdocente=false;
			$this->isalumno=false;
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
				if(!empty($this->datos)) $this->isdocente=$this->datos["rol"]==2?true:false;
				if(!empty($this->datos)) $this->isalumno=$this->datos["rol"]==3?true:false;
				if($this->isdocente==false){	
					$roli=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idrol'=>2));
					$this->isdocente=!empty($roli[0])?true:false;
				}
				if($this->isalumno==false){	
					$roli=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idrol'=>3));
					$this->isalumno=!empty($roli[0])?true:false;
				}
			}
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_datospersonales(){
		try {
			global $aplicacion;	
			$this->esquema = 'usuario/formulario-datospersonales';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_direccion(){
		try {
			global $aplicacion;	
			$this->esquema = 'usuario/formulario-direccion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));			
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));
			if(!empty($this->fkpais[0])){
				//$pais='PE';
				$pais='PE';
				$this->idpais=$pais;
				$idubigeo=str_pad($this->datos["ubigeo"],6,'0');
				$this->fkdepartamento=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>'all'));				
				if(!empty($this->fkdepartamento[0])){
					$depa=substr($idubigeo, 0,2);
					$this->iddepa=!empty($depa)&&@$depa!='00'?$depa:@$this->fkdepartamento[0]["departamento"];
					$this->fkprovincia=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$this->iddepa,'provincia'=>'all'));
					if(!empty($this->fkprovincia[0])){
						$pro=substr($idubigeo, 2,2);
						$this->idpro=!empty($pro)&&@$pro!='00'?$pro:@$this->fkprovincia[0]["provincia"];
						$this->fkdistrito=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$this->iddepa,'provincia'=>$this->idpro,'distrito'=>'all'));
						$this->iddis=substr($idubigeo, -2);
					}
				}
			}
			//$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			//$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_apoderado(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-apoderados';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->apoderados=$this->oNegPerApoderado->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->fkparentesco=$this->oNegGeneral->buscar(array('tipo_tabla'=>'parentesco','mostrar'=>1));
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_educacion(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-educacion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->educacion=$this->oNegPerEducacion->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}			
			$this->fktipoestudio=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipoestudio','mostrar'=>1));
			$this->fkareaestudio=$this->oNegGeneral->buscar(array('tipo_tabla'=>'areaestudio','mostrar'=>1));
			$this->fksituacionestudio=$this->oNegGeneral->buscar(array('tipo_tabla'=>'situacionestudio','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_record(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-record';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';			
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->records=$this->oNegPerRecord->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}			
			$this->fktiporecord=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tiporecord','mostrar'=>1));			
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_experiencialaboral(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-experiencialaboral';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->experiencias=$this->oNegPerExplaboral->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}	
			$this->fkarea=$this->oNegGeneral->buscar(array('tipo_tabla'=>'areaempresa','mostrar'=>1));			
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_metas(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-metas';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->metas=$this->oNegPerMetas->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_referencias(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-referencias';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->referencias=$this->oNegPerReferencia->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}			
			
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	
    public function getgrupoauladetalle($idgrupoauladetalle){
		try {
			global $aplicacion;	
			if($idgrupoauladetalle){
				$grupoauladetalle=$this->oNegGrupoauladetalle->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
				if(!empty($grupoauladetalle[0])){
					return $grupoauladetalle[0];
				}else return false;
			}else return false;
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function grupoestudio(){
		try {
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->esquema = 'usuario/grupoestudio';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=@$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = $idpersona;
				$this->persona = $this->oNegPersonal->dataPersonal;
				$this->gruposdocente=$this->oNegGrupoauladetalle->buscar(array('iddocente'=>$idpersona));
				$this->comoalumno=$this->oNegMatricula->buscar(array('idalumno'=>$idpersona));				
			}
			$this->documento->setTitulo(JrTexto::_('Groups').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cursos(){
		try {
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->esquema = 'usuario/cursos';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=@$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = $idpersona;
				$this->persona = $this->oNegPersonal->dataPersonal;
				$this->gruposdocente=$this->oNegGrupoauladetalle->buscar(array('iddocente'=>$idpersona));
				$this->comoalumno=$this->oNegMatricula->buscar(array('idalumno'=>$idpersona));				
			}
			$this->documento->setTitulo(JrTexto::_('Courses').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function notas(){
		try {
			global $aplicacion;	
			$this->esquema = 'usuario/notas';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';	
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function horarios(){
		try {
			global $aplicacion;	
			$this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
            $this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
            $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
			$this->esquema = 'usuario/horarios';
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=@$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = $idpersona;
				$this->persona = $this->oNegPersonal->dataPersonal;
				$this->gruposdocente=$this->oNegGrupoauladetalle->buscar(array('iddocente'=>$idpersona));
				$this->horarios=array();
				if(!empty($this->gruposdocente))
					foreach ($this->gruposdocente as $rw){
        			$tmphorarios=$this->oNegHorariogrupoaula->buscar(array('idgrupoauladetalle'=>$rw["idgrupoauladetalle"]));
        			if(!empty($tmphorarios))
        			foreach ($tmphorarios as $hr){
        				$this->horarios[$hr["idhorario"]]='{id:'.$hr["idhorario"].
        									',title:\''.$rw["strcurso"].
        									'\',start:\''.@$hr["fecha_finicio"].
        									'\',end:\''.$hr["fecha_final"].
        									'\',backgroundColor:\''.$hr["color"].
        									'\',color:\''.$hr["color"].'\'},';
        			}
        		}
				$this->comoalumno=$this->oNegMatricula->buscar(array('idalumno'=>$idpersona));
				if(!empty($this->comoalumno))
					foreach ($this->comoalumno as $rw){
        			$tmphorarios=$this->oNegHorariogrupoaula->buscar(array('idgrupoauladetalle'=>$rw["idgrupoauladetalle"]));
        			if(!empty($tmphorarios))
        			foreach ($tmphorarios as $hr){
        				$this->horarios[$hr["idhorario"]]='{id:'.$hr["idhorario"].
        									',title:\''.$rw["strcurso"].
        									'\',start:\''.@$hr["fecha_finicio"].
        									'\',end:\''.$hr["fecha_final"].
        									'\',backgroundColor:\''.$hr["color"].
        									'\',color:\''.$hr["color"].'\'},';
        			}
        		}
			}			
			$this->documento->setTitulo(JrTexto::_('Schedule').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function historial(){
		try {
			global $aplicacion;	
			/*$this->esquema = 'usuario/formulario-informacion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';	
			if(!empty($_REQUEST['dni'])){
				$this->oNegPersonal->dni = @$_REQUEST['dni'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}			*/
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function asistencias(){
		try {
			global $aplicacion;	
			/*$this->esquema = 'usuario/formulario-informacion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';	
			if(!empty($_REQUEST['dni'])){
				$this->oNegPersonal->dni = @$_REQUEST['dni'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}*/
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function perfil(){
		try {
			global $aplicacion;			
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');	
			$this->documento->script('datetimepicker.min', '/libs/datetimepicker/js/');
			$this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');		
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			$usuarioAct = NegSesion::getUsuario();	
			$this->usuarioAct=$usuarioAct;
			// var_dump($this->usuarioAct);exit();
			if(!empty($_REQUEST['idpersona']))$idpersona = @$_REQUEST['idpersona'];	
			elseif(!empty($_REQUEST['id']))$idpersona = @$_REQUEST['id'];
			else $idpersona = $usuarioAct["idpersona"];	
			$this->oNegPersonal->idpersona = $idpersona;
			$this->datos = $this->oNegPersonal->dataPersonal;
			$this->apoderados=$this->oNegPerApoderado->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			$this->educacion=$this->oNegPerEducacion->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			$this->experiencias=$this->oNegPerExplaboral->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			$this->metas=$this->oNegPerMetas->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			$this->referencias=$this->oNegPerReferencia->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');		
			//$this->esquema = 'personal-frm';
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			
			if (JrTools::ismobile() && $usuarioAct["idrol"] == 3) {
				$this->documento->plantilla = 'mobile/principal';
			}else{
				$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			}
			$this->esquema = 'usuario/personal-perfil';
			// var_dump($this->documento->plantilla);
			// var_dump($this->documento->esquema);
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	} 

	public function json_datos(){
		$this->documento->plantilla = 'verblanco';
		if(!empty($_REQUEST['idpersona'])){
			$idpersona = @$_REQUEST['idpersona'];								
		}else{
			$usuarioAct = NegSesion::getUsuario();				
			$idpersona = $usuarioAct["idpersona"];				
		}
		$this->oNegPersonal->idpersona = $idpersona;
		$this->datos = $this->oNegPersonal->dataPersonal;
		echo json_encode(array('code'=>200,'datos'=>$this->datos));
		exit(0);
	}

	public function verficha(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.JrTexto::_('Ficha'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!=''){
				$this->oNegPersonal->idpersona=$_REQUEST["id"];
				$this->datos=$this->oNegPersonal->dataPersonal;				
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}
			$this->esquema = 'docente/ficha';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cambiarclave(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.ucfirst(JrTexto::_('Change Password')), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->user='personal';
			$this->esquema = 'personal-cambiarclave';
			$idpersona=!empty($_REQUEST["idpersona"])?$_REQUEST["idpersona"]:@$_REQUEST["id"];
			$idpersona=!empty($idpersona)?$idpersona:$this->usuarioAct["idpersona"];
			$this->oNegPersonal->idpersona=$idpersona;
			/*if((isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')||(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')){
				$this->oNegPersonal->idpersona=!empty($_REQUEST["idpersona"])?$_REQUEST["idpersona"]:$_REQUEST["id"];				
				if(empty($this->oNegPersonal->idpersona)) $this->oNegPersonal->idpersona=$this->usuarioAct["idpersonal"];*/
				$this->datos=$this->oNegPersonal->dataPersonal;

				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			/*}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}*/
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}	
	}

	public function guardarDatos(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			@extract($_POST);
			$accion='add';			
			if(!empty($idpersona)){
				$datos=$this->oNegPersonal->buscar(array('idpersona'=>$idpersona));
				if(!empty($datos)){
					$accion='edit';
					$this->oNegPersonal->set('idpersona',$idpersona);
				}
			}

			$this->oNegPersonal->set('regusuario',$usuarioAct["idpersona"]);
			$idrol=0;
			foreach ($_POST as $key => $value){
				if($key!='idpersona')
					$this->oNegPersonal->set($key,$value);
				if($key=='rol')$idrol=$value;
			}
	
			/*if(!empty($_FILES['fotouser'])){
				$file=$_FILES["fotouser"];
				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$newfoto=$idpersona.'-'.date("Ymdhis").'.'.$ext;
				$dir_media=RUTA_BASE . 'static' . SD . 'media' . SD . 'usuarios' ;
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newfoto)) 
		  		{
		  			$this->oNegPersonal->set('foto',$newfoto);
		  		}
			}*/
			
			$this->oNegPersona_rol->idrol=$idrol;
			$this->oNegPersona_rol->idempresa=!empty($idempresa)?$idempresa:$usuarioAct["idempresa"];
			if(empty($idproyecto)&&!empty($idempresa)){
				$hayproyecto=$this->oNegProyecto->buscar(array('idempresa'=>$idempresa));
				if(empty($hayproyecto[0])){
					$this->oNegProyecto->idempresa=@$res;
					$this->oNegProyecto->jsonlogin='{}';
					$this->oNegProyecto->fecha=date('Y-m-d');
					$this->oNegProyecto->idioma=!empty($idioma)?$idioma:'EN';
					$idproyecto=$this->oNegProyecto->agregar();
				}else{
					$idproyecto=$hayproyecto[0]["idproyecto"];
				}
			}
			//$this->oNegPersona_rol->idproyecto=!empty($idproyecto)?$idproyecto:$usuarioAct["idproyecto"];
			if($accion=='add'){
				$res=$this->oNegPersonal->agregar();
				$this->oNegPersona_rol->idpersonal=$res;				
        	 	$re=array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('saved successfully'),'newid'=>$res);
			}else{
				$res=$this->oNegPersonal->editar();				
				$this->oNegPersona_rol->idpersonal=$idpersona;				
        		$re=array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('update successfully'),'newid'=>$idpersona); 
			}
			if(!empty($_FILES["fotofile"])){
            	JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
            	$resi = NegTools::subirFile($_FILES["fotofile"],'usuarios/','','imagen',"usu_".$res);
            	if($resi['code']==200){
            		$this->oNegPersonal->setCampo($res,'foto',$resi["rutaStatic"]."?tmp=".date('Ymdhis'));
            		$re["foto"]=$resi["rutaStatic"]."?tmp=".date('Ymdhis');
            	}
            }            
            echo json_encode($re);
			if($idrol!=0)$this->oNegPersona_rol->agregar2();
			exit(0);   	
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
        }
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Personal', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='')$filtros["rol"]=$_REQUEST["rol"];
			if(isset($_REQUEST["foto"])&&@$_REQUEST["foto"]!='')$filtros["foto"]=$_REQUEST["foto"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"];
			
			$this->datos=$this->oNegPersonal->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarPersonal(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdpersona)) {
				$this->oNegPersonal->idpersona = $frm['pkIdpersona'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	           	
			$this->oNegPersonal->tipodoc=@$txtTipodoc;
			$this->oNegPersonal->dni=@$txtDni;
			$this->oNegPersonal->ape_paterno=@$txtApe_paterno;
			$this->oNegPersonal->ape_materno=@$txtApe_materno;
			$this->oNegPersonal->nombre=@$txtNombre;
			$this->oNegPersonal->fechanac=@$txtFechanac;
			$this->oNegPersonal->sexo=@$txtSexo;
			$this->oNegPersonal->estado_civil=@$txtEstado_civil;
			$this->oNegPersonal->ubigeo=@$txtUbigeo;
			$this->oNegPersonal->urbanizacion=@$txtUrbanizacion;
			$this->oNegPersonal->direccion=@$txtDireccion;
			$this->oNegPersonal->telefono=@$txtTelefono;
			$this->oNegPersonal->celular=@$txtCelular;
			$this->oNegPersonal->email=@$txtEmail;
			$this->oNegPersonal->idugel=@$txtIdugel;
			$this->oNegPersonal->regusuario=@$txtRegusuario;
			$this->oNegPersonal->regfecha=@$txtRegfecha;
			$this->oNegPersonal->usuario=@$txtUsuario;
			$this->oNegPersonal->clave=@$txtClave;
			$this->oNegPersonal->token=@$txtToken;
			$this->oNegPersonal->rol=@$txtRol;
			$this->oNegPersonal->foto=@$txtFoto;
			$this->oNegPersonal->estado=@$txtEstado;
			$this->oNegPersonal->situacion=@$txtSituacion;
			$this->oNegPersonal->idioma=@$txtIdioma;
					
            if($accion=='_add') {
            	$res=$this->oNegPersonal->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersonal->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarclave(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);
            }
      
			global $aplicacion;
            @extract($_POST);
            if(!empty(@$idpersona)) {
				$this->oNegPersonal->idpersona = $idpersona;
				$this->oNegPersonal->email=@$email;
				$this->oNegPersonal->usuario=@$usuario;
				$this->oNegPersonal->clave=md5($clave);
				$this->oNegPersonal->token=md5($clave);
				$res=$this->oNegPersonal->editar();				
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Password')).' '.JrTexto::_('update successfully'),'newid'=>$res));
			}
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePersonal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdpersona'])) {
					$this->oNegPersonal->idpersona = $frm['pkIdpersona'];
				}
				
					$this->oNegPersonal->tipodoc=@$frm["txtTipodoc"];
					$this->oNegPersonal->dni=@$frm["txtDni"];
					$this->oNegPersonal->ape_paterno=@$frm["txtApe_paterno"];
					$this->oNegPersonal->ape_materno=@$frm["txtApe_materno"];
					$this->oNegPersonal->nombre=@$frm["txtNombre"];
					$this->oNegPersonal->fechanac=@$frm["txtFechanac"];
					$this->oNegPersonal->sexo=@$frm["txtSexo"];
					$this->oNegPersonal->estado_civil=@$frm["txtEstado_civil"];
					$this->oNegPersonal->ubigeo=@$frm["txtUbigeo"];
					$this->oNegPersonal->urbanizacion=@$frm["txtUrbanizacion"];
					$this->oNegPersonal->direccion=@$frm["txtDireccion"];
					$this->oNegPersonal->telefono=@$frm["txtTelefono"];
					$this->oNegPersonal->celular=@$frm["txtCelular"];
					$this->oNegPersonal->email=@$frm["txtEmail"];
					$this->oNegPersonal->idugel=@$frm["txtIdugel"];
					$this->oNegPersonal->regusuario=@$frm["txtRegusuario"];
					$this->oNegPersonal->regfecha=@$frm["txtRegfecha"];
					$this->oNegPersonal->usuario=@$frm["txtUsuario"];
					$this->oNegPersonal->clave=@$frm["txtClave"];
					$this->oNegPersonal->token=@$frm["txtToken"];
					$this->oNegPersonal->rol=@$frm["txtRol"];
					$this->oNegPersonal->foto=@$frm["txtFoto"];
					$this->oNegPersonal->estado=@$frm["txtEstado"];
					$this->oNegPersonal->situacion=@$frm["txtSituacion"];
					$this->oNegPersonal->idioma=@$frm["txtIdioma"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegPersonal->agregar();
					}else{
									    $res=$this->oNegPersonal->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPersonal->idpersona);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPersonal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersonal->__set('idpersona', $pk);
				$this->datos = $this->oNegPersonal->dataPersonal;
				$res=$this->oNegPersonal->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersonal->__set('idpersona', $pk);
				$res=$this->oNegPersonal->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersonal->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function actualizartabla(){
		
		//$this->oNegPersonal->actualizartabla("acad_curso","idusuario");
		//$this->oNegPersonal->actualizartabla("acad_grupoauladetalle","iddocente");
		//$this->oNegPersonal->actualizartabla("acad_matricula","idalumno");
		//$this->oNegPersonal->actualizartabla("acad_matricula","idusuario");
		//$this->oNegPersonal->actualizartabla("actividad_alumno","idalumno");
		//$this->oNegPersonal->actualizartabla("historial_sesion","idusuario");
		//$this->oNegPersonal->actualizartabla("bitacora_alumno_smartbook","idusuario");
		//$this->oNegPersonal->actualizartabla("alumno_logro","id_alumno");
		//$this->oNegPersonal->actualizartabla("notas_alumno","identificador");
		//$this->oNegPersonal->actualizartabla("notas_archivo","iddocente");
		//$this->oNegPersonal->actualizartabla("recursos","idpersonal");
		//$this->oNegPersonal->actualizartabla("notas_quiz","idalumno");
		//UPDATE `abacoedu_smartlearn`.`bitacora_alumno_smartbook` SET `idcurso` = '35' WHERE idcurso=2 //Pasar el historial del curso
	}
	
}