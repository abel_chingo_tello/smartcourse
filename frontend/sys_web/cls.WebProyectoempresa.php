<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-12-2018 
 * @copyright	Copyright (C) 19-12-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
class WebProyectoempresa extends JrWeb
{
	private $oNegProyecto;
	private $oNegBolsa_empresas;		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegProyecto = new NegProyecto;
		$this->oNegBolsa_empresas = new NegBolsa_empresas;
		$this->curusuario = NegSesion::getUsuario();
				
	}

	public function defecto(){
		return $this->configuracion();
	}


	public function configuracion(){
		try{
			global $aplicacion;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			$tipouser=$this->curusuario["tuser"];
			$this->infoempresa=array();
			if($tipouser=='s'){
				$this->infoempresa=$this->oNegProyecto->buscar(array('idproyecto'=>$idproyecto));
				if(!empty($this->infoempresa[0])){
					$this->infoempresa=$this->infoempresa[0];
					$idempresa=$this->infoempresa["idempresa"];
				}else{
					$idempresa=$this->curusuario["idempresa"];
					$idproyecto=$this->curusuario["idproyecto"];	
				}
			}else{
				$idempresa=$this->curusuario["idempresa"];
				$idproyecto=$this->curusuario["idproyecto"];
			}

			if(empty($this->infoempresa)){
				$this->infoempresa=$this->oNegProyecto->buscar(array('idproyecto'=>$idproyecto));
				if(!empty($this->infoempresa[0])){
					$this->infoempresa=$this->infoempresa[0];
					$idempresa=$this->infoempresa["idempresa"];
				}
			}
			$this->paris = false;
			if(isset($_REQUEST["paris"])){
				$this->paris = true;
			}
			$this->academico = false;
			if(isset($_REQUEST["academico"])){
				$this->academico = true;
			}
			$this->datos=$this->infoempresa;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'empresas/bienvenida';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function configurarlogin(){
		try{
			global $aplicacion;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			$tipouser=$this->curusuario["tuser"];
			$this->infoempresa=array();
			if($tipouser=='s'){
				$this->infoempresa=$this->oNegProyecto->buscar(array('idproyecto'=>$idproyecto));
				if(!empty($this->infoempresa[0])){
					$this->infoempresa=$this->infoempresa[0];
					$idempresa=$this->infoempresa["idempresa"];
				}else{
					$idempresa=$this->curusuario["idempresa"];
					$idproyecto=$this->curusuario["idproyecto"];
				}
			}else{
				$idempresa=$this->curusuario["idempresa"];
				$idproyecto=$this->curusuario["idproyecto"];
			}

			if(empty($this->infoempresa)){
				$this->infoempresa=$this->oNegProyecto->buscar(array('idproyecto'=>$idproyecto));			
				if(!empty($this->infoempresa[0])){
					$this->infoempresa=$this->infoempresa[0];
					$idempresa=$this->infoempresa["idempresa"];
				}
			}
			$this->paris = false;
			if(isset($_REQUEST["paris"])){
				$this->paris = true;
			}
			$this->datos=$this->infoempresa;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'modal';
			//var_dump($this->documento->plantilla);
			$this->esquema = 'empresas/crearlogin';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function configurarmodulos(){
		try{
			global $aplicacion;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			$tipouser=$this->curusuario["tuser"];
			$this->infoempresa=array();
			if($tipouser=='s'){
				$this->infoempresa=$this->oNegProyecto->buscar(array('idproyecto'=>$idproyecto));
				if(!empty($this->infoempresa[0])){
					$this->infoempresa=$this->infoempresa[0];
					$idempresa=$this->infoempresa["idempresa"];
				}else{
					$idempresa=$this->curusuario["idempresa"];
					$idproyecto=$this->curusuario["idproyecto"];
				}
			}else{
				$idempresa=$this->curusuario["idempresa"];
				$idproyecto=$this->curusuario["idproyecto"];
			}

			if(empty($this->infoempresa)){
				$this->infoempresa=$this->oNegProyecto->buscar(array('idproyecto'=>$idproyecto));			
				if(!empty($this->infoempresa[0])){
					$this->infoempresa=$this->infoempresa[0];
					$idempresa=$this->infoempresa["idempresa"];
				}
			}
			$this->datos=$this->infoempresa;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'empresas/selmodulos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}