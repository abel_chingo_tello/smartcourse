<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebAcademico extends JrWeb
{
    private $oNegBolsa_empresas;
    private $oNegProyecto;
	public function __construct()
	{
		parent::__construct();       
	}

	public function defecto(){
		return $this->panelcontrol();		
	}

	public function panelcontrol(){
		try{
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			global $aplicacion;
			$filtros=array();
			$this->curusuario = NegSesion::getUsuario();
			$this->idempresa=$this->curusuario["idempresa"];
			$this->idproyecto=$this->curusuario["idproyecto"];		
			$this->documento->setTitulo(JrTexto::_('Proyectos'),true);
            // $this->documento->plantilla ='proyecto/administrable';
            $this->documento->plantilla ='mantenimientos';
            $idrol=$this->curusuario['idrol'];
            $page=$idrol==3?'alumno':($idrol==2?'docente':($idrol==1?'admin':'error'));
            if($this->curusuario["tuser"]=='s'&&$idrol==1)$page='admin_s';
            $this->esquema = 'academico/panel_'.$page;
            // var_dump($this->esquema);
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function ambientes(){
		try{			
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			$plt=(isset($_REQUEST["plt"])&&@$_REQUEST["plt"]!='')?$_REQUEST["plt"]:'proyecto/administrable';
			@session_start();
			if(!empty($idempresa))$_SESSION["idempresa"]=$idempresa;
			else $idempresa=@$_SESSION["idempresa"];
			if(!empty($idproyecto))$_SESSION["idproyecto"]=$idproyecto;
			else $idproyecto=@$_SESSION["idproyecto"];
			$this->curusuario = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('Proyectos'),true);
            $this->documento->plantilla =$plt;
            $page=(isset($_REQUEST["page"])&&@$_REQUEST["page"]!='')?'_'.$_REQUEST["page"]:'';
			$this->esquema = 'academico/ambientes'.$page;
			$this->idproyecto = $idproyecto;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function personal(){
		try{			
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			$plt=(isset($_REQUEST["plt"])&&@$_REQUEST["plt"]!='')?$_REQUEST["plt"]:'proyecto/administrable';
			@session_start();
			if(!empty($idempresa))$_SESSION["idempresa"]=$idempresa;
			else $idempresa=@$_SESSION["idempresa"];
			if(!empty($idproyecto))$_SESSION["idproyecto"]=$idproyecto;
			else $idproyecto=@$_SESSION["idproyecto"];
			$this->curusuario = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('Personal'),true);
            $this->documento->plantilla =$plt;
            $page=(isset($_REQUEST["page"])&&@$_REQUEST["page"]!='')?'_'.$_REQUEST["page"]:'';
			$this->esquema = 'academico/personal'.$page;
			$this->idproyecto = $idproyecto;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function gruposestudio(){
		try{			
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			$plt=(isset($_REQUEST["plt"])&&@$_REQUEST["plt"]!='')?$_REQUEST["plt"]:'man-listadoall';
			@session_start();
			if(!empty($idempresa))$_SESSION["idempresa"]=$idempresa;
			else $idempresa=@$_SESSION["idempresa"];
			if(!empty($idproyecto))$_SESSION["idproyecto"]=$idproyecto;
			else $idproyecto=@$_SESSION["idproyecto"];
			$this->curusuario = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('Personal'),true);
            $this->documento->plantilla =$plt;
            $page=(isset($_REQUEST["page"])&&@$_REQUEST["page"]!='')?'_'.$_REQUEST["page"]:'';
			$this->esquema = 'academico/gruposestudio';
			$this->idproyecto = $idproyecto;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function matriculas(){
		try{			
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			$plt=(isset($_REQUEST["plt"])&&@$_REQUEST["plt"]!='')?$_REQUEST["plt"]:'man-listadoall';
			@session_start();
			if(!empty($idempresa))$_SESSION["idempresa"]=$idempresa;
			else $idempresa=@$_SESSION["idempresa"];
			if(!empty($idproyecto))$_SESSION["idproyecto"]=$idproyecto;
			else $idproyecto=@$_SESSION["idproyecto"];
			$this->curusuario = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('Personal'),true);
            $this->documento->plantilla =$plt;
            $page=(isset($_REQUEST["page"])&&@$_REQUEST["page"]!='')?'_'.$_REQUEST["page"]:'';
			$this->esquema = 'academico/matriculas';
			$this->idproyecto = $idproyecto;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}