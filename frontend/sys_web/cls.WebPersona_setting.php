<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019 
 * @copyright	Copyright (C) 19-09-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE);
class WebPersona_setting extends JrWeb
{
	private $oNegPersona_setting;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_setting = new NegPersona_setting;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_setting', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["datos"])&&@$_REQUEST["datos"]!='')$filtros["datos"]=$_REQUEST["datos"];
			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"];
			if(isset($_REQUEST["fechamodificacion"])&&@$_REQUEST["fechamodificacion"]!='')$filtros["fechamodificacion"]=$_REQUEST["fechamodificacion"];
			if(isset($_REQUEST["oldid"])&&@$_REQUEST["oldid"]!='')$filtros["oldid"]=$_REQUEST["oldid"];
			if(isset($_REQUEST["oldido"])&&@$_REQUEST["oldido"]!='')$filtros["oldido"]=$_REQUEST["oldido"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			$this->datos=$this->oNegPersona_setting->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Persona_setting'), true);
			$this->esquema = 'persona_setting-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_setting', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Persona_setting').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_setting', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegPersona_setting->idpersonasetting = @$_GET['id'];
			$this->datos = $this->oNegPersona_setting->dataPersona_setting;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Persona_setting').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'persona_setting-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}