<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017 
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno_logro', RUTA_BASE, 'sys_negocio');
class WebAlumno_logro extends JrWeb
{
	private $oNegAlumno_logro;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAlumno_logro = new NegAlumno_logro;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Alumno_logro', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["id_alumno_logro"])&&@$_REQUEST["id_alumno_logro"]!='')$filtros["id_alumno_logro"]=$_REQUEST["id_alumno_logro"];
			if(isset($_REQUEST["id_alumno"])&&@$_REQUEST["id_alumno"]!='')$filtros["id_alumno"]=$_REQUEST["id_alumno"];
			if(isset($_REQUEST["id_logro"])&&@$_REQUEST["id_logro"]!='')$filtros["id_logro"]=$_REQUEST["id_logro"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			
			$this->datos=$this->oNegAlumno_logro->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Alumno_logro'), true);
			$this->esquema = 'alumno_logro-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Alumno_logro', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Alumno_logro').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Alumno_logro', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegAlumno_logro->id_alumno_logro = @$_GET['id'];
			$this->datos = $this->oNegAlumno_logro->dataAlumno_logro;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Alumno_logro').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'alumno_logro-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Alumno_logro', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}*/
			$filtros=array();
			if(isset($_REQUEST["id_alumno_logro"])&&@$_REQUEST["id_alumno_logro"]!='')$filtros["id_alumno_logro"]=$_REQUEST["id_alumno_logro"];
			if(isset($_REQUEST["id_alumno"])&&@$_REQUEST["id_alumno"]!='')$filtros["id_alumno"]=$_REQUEST["id_alumno"];
			if(isset($_REQUEST["id_logro"])&&@$_REQUEST["id_logro"]!='')$filtros["id_logro"]=$_REQUEST["id_logro"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["bandera"])&&@$_REQUEST["bandera"]!='')$filtros["bandera"]=$_REQUEST["bandera"];
						
			$this->datos=$this->oNegAlumno_logro->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function actualizarBandera(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			if( empty($_REQUEST['id_logro']) && empty($_REQUEST['idrecurso']) ){
				throw new Exception(JrTexto::_('No filtros'));
			}
			$usuarioAct = NegSesion::getUsuario();	
			$filtros=array();
			$filtros["id_logro"]=$_REQUEST["id_logro"];
			$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			$filtros['id_alumno']=$usuarioAct['dni'];
			$alum_logro=$this->oNegAlumno_logro->buscar($filtros);
			$res=null;
			if(!empty($alum_logro)){
				$this->oNegAlumno_logro->id_alumno_logro=$alum_logro[0]['id_alumno_logro'];
				$this->oNegAlumno_logro->bandera=1;
				$res=$this->oNegAlumno_logro->editar();
			}
			echo json_encode(array('code'=>'ok','data'=>$alum_logro));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}
	public function guardarAlumno_logro(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkId_alumno_logro)) {
				$this->oNegAlumno_logro->id_alumno_logro = $frm['pkId_alumno_logro'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	
				$this->oNegAlumno_logro->id_alumno=@$txtId_alumno;
				$this->oNegAlumno_logro->id_logro=@$txtId_logro;
				$this->oNegAlumno_logro->fecha=@$txtFecha;
					
            if($accion=='_add') {
            	$res=$this->oNegAlumno_logro->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Alumno_logro')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAlumno_logro->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Alumno_logro')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAlumno_logro(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_alumno_logro'])) {
					$this->oNegAlumno_logro->id_alumno_logro = $frm['pkId_alumno_logro'];
				}
				
				$this->oNegAlumno_logro->id_alumno=@$frm["txtId_alumno"];
				$this->oNegAlumno_logro->id_logro=@$frm["txtId_logro"];
				$this->oNegAlumno_logro->fecha=@$frm["txtFecha"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAlumno_logro->agregar();
					}else{
									    $res=$this->oNegAlumno_logro->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAlumno_logro->id_alumno_logro);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAlumno_logro(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAlumno_logro->__set('id_alumno_logro', $pk);
				$this->datos = $this->oNegAlumno_logro->dataAlumno_logro;
				$res=$this->oNegAlumno_logro->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAlumno_logro->__set('id_alumno_logro', $pk);
				$res=$this->oNegAlumno_logro->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAlumno_logro->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}