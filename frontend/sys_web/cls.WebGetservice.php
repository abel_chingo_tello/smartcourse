<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebGetservice extends JrWeb
{
	public $PV=array('PVADU_1'=>array('link'=>URL_SEAdultos,'nombre'=>'Plataforma Adultos'),'PVADO_1'=>array('link'=>URL_SEAdolecentes,'nombre'=>'Plataforma Adolescentes'));
	public function __construct()
	{
		parent::__construct();
	}

	private function printJSON($state,$message,$data=array()){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
        echo json_encode(array('code'=>$state,'message'=>$message,'data'=>$data));
        exit(0);
    }

	public function defecto(){
		$this->printJSON(403,"You don't have permission to access this server.",array());
	}


	private function result($url,$data){
		$postdata = http_build_query($data);
		$opts = array('http' => array(
		        'method'  => 'POST',
		        'header'  => 'Content-Type: application/x-www-form-urlencoded',
		        'content' => $postdata		        
	    ),"ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false,));
		$context  = stream_context_create($opts);
    	$result = file_get_contents($url, false, $context);		   
    	if($result===false) return $this->printJSON(500,"Error al obtener data");
		$dt=json_decode($result,true);
		if(!empty($dt["code"])) return $this->printJSON($dt["code"],$dt["message"],$dt["data"]);
		else return $this->printJSON(500,$result,array());
    }

	public function englishsesiones(){
		if(empty($_REQUEST)) return $this->printJSON(500,"data incomplete");
		$postdata=array();
		$postdata["idcurso"]=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
		$postdata["texto"]=!empty($_REQUEST["texto"])?$_REQUEST["texto"]:'';
		if(empty($postdata["idcurso"])) return $this->printJSON(500,"data incomplete");
		$url=(!empty($_REQUEST["plataforma"])?$_REQUEST["plataforma"]:"PVADU_1");
		if(!empty($this->PV[$url]))$url=$this->PV[$url]["link"];
		$url.='servicenologin/temasxcurso/';
		return $this->result($url,$postdata);
	}

	public function englishcursos(){
		if(empty($_REQUEST)) return $this->printJSON(500,"data incomplete");
		$postdata=!empty($_REQUEST)?$_REQUEST:array();
		$url=(!empty($_REQUEST["plataforma"])?$_REQUEST["plataforma"]:"PVADU_1");
		if(!empty($this->PV[$url]))$url=$this->PV[$url]["link"];
		$url.='servicenologin/cursos/';
		return $this->result($url,$postdata);
	}
}