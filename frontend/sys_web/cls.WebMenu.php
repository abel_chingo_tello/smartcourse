<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016 
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegMenu', RUTA_BASE, 'sys_negocio');
class WebMenu extends JrWeb
{
	private $oNegMenu;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegMenu = new NegMenu;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'reportes';
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');		
			/*if(!NegSesion::tiene_acceso('Menu', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->documento->stylesheet('jquery.dataTables.min','/libs/othersLibs/dataTables/');
            $this->documento->stylesheet('tablas', '/libs/abelchingo/');
			//$this->documento->script('jquery.dataTables.min','/libs/othersLibs/dataTables/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            /*$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');*/

            $this->documento->stylesheet('select2.min', '/libs/select2/');
            $this->documento->script('select2.min', '/libs/select2/');

            $this->documento->script('abelchingo', '/libs/abelchingo/');
            $this->documento->script('bdmenus', '/libs/abelchingo/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');		
			
			//$this->datos=$this->oNegMenu->buscar();
			/*$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';*/
			$this->documento->setTitulo(JrTexto::_('Menu'), true);
			$this->esquema = 'menu/menu-listado';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}
}