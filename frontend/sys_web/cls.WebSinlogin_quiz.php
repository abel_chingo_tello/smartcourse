<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-01-2021 
 * @copyright	Copyright (C) 15-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSinlogin_quiz', RUTA_BASE);
class WebSinlogin_quiz extends JrWeb
{
	private $oNegSinlogin_quiz;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSinlogin_quiz = new NegSinlogin_quiz;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Sinlogin_quiz', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["idexamen"])&&@$_REQUEST["idexamen"]!='')$filtros["idexamen"]=$_REQUEST["idexamen"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["notatexto"])&&@$_REQUEST["notatexto"]!='')$filtros["notatexto"]=$_REQUEST["notatexto"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["calificacion_en"])&&@$_REQUEST["calificacion_en"]!='')$filtros["calificacion_en"]=$_REQUEST["calificacion_en"];
			if(isset($_REQUEST["calificacion_total"])&&@$_REQUEST["calificacion_total"]!='')$filtros["calificacion_total"]=$_REQUEST["calificacion_total"];
			if(isset($_REQUEST["calificacion_min"])&&@$_REQUEST["calificacion_min"]!='')$filtros["calificacion_min"]=$_REQUEST["calificacion_min"];
			if(isset($_REQUEST["tiempo_total"])&&@$_REQUEST["tiempo_total"]!='')$filtros["tiempo_total"]=$_REQUEST["tiempo_total"];
			if(isset($_REQUEST["tiempo_realizado"])&&@$_REQUEST["tiempo_realizado"]!='')$filtros["tiempo_realizado"]=$_REQUEST["tiempo_realizado"];
			if(isset($_REQUEST["calificacion"])&&@$_REQUEST["calificacion"]!='')$filtros["calificacion"]=$_REQUEST["calificacion"];
			if(isset($_REQUEST["habilidades"])&&@$_REQUEST["habilidades"]!='')$filtros["habilidades"]=$_REQUEST["habilidades"];
			if(isset($_REQUEST["habilidad_puntaje"])&&@$_REQUEST["habilidad_puntaje"]!='')$filtros["habilidad_puntaje"]=$_REQUEST["habilidad_puntaje"];
			if(isset($_REQUEST["intento"])&&@$_REQUEST["intento"]!='')$filtros["intento"]=$_REQUEST["intento"];
			if(isset($_REQUEST["datos"])&&@$_REQUEST["datos"]!='')$filtros["datos"]=$_REQUEST["datos"];
			if(isset($_REQUEST["fechamodificacion"])&&@$_REQUEST["fechamodificacion"]!='')$filtros["fechamodificacion"]=$_REQUEST["fechamodificacion"];
			if(isset($_REQUEST["preguntas"])&&@$_REQUEST["preguntas"]!='')$filtros["preguntas"]=$_REQUEST["preguntas"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			$this->datos=$this->oNegSinlogin_quiz->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Sinlogin_quiz'), true);
			$this->esquema = 'sinlogin_quiz-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Sinlogin_quiz', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Sinlogin_quiz').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Sinlogin_quiz', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegSinlogin_quiz->idpersona = @$_GET['id'];
			$this->datos = $this->oNegSinlogin_quiz->dataSinlogin_quiz;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Sinlogin_quiz').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'sinlogin_quiz-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}