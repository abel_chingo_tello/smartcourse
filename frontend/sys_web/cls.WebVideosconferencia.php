<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020 
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
// JrCargador::clase('sys_negocio::NegVideosconferencia', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegZoom', RUTA_BASE);
JrCargador::clase('sys_negocio::NegZoomalumnos', RUTA_BASE);

class WebVideosconferencia extends JrWeb
{
	private $oZoom;
	private $userID;
	private $tocken;
	private $usuarioAct;
	private $oNegZoom;
	private $oNegZoomalumnos;

	protected $oNegGrupoaulaDetalle;
		
	public function __construct()
	{
		$this->oNegZoom = new NegZoom;
		$this->oNegZoomalumnos= new NegZoomalumnos();
		$this->oNegGrupoaulaDetalle = new NegAcad_grupoauladetalle;

		$this->usuario = NegSesion::getUsuario();

		$this->userID='juanpablo.venegas@eduktvirtual.com';//'50947009';
		$this->curuserID=$this->usuario["email"];
		$this->tocken='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IjktclB0bkNQUk9pVklWNGctWHZWYVEiLCJleHAiOjE2NzI1NDkxNDAsImlhdCI6MTU3MDE5OTMxOX0.2UIgzmnS8DIcNhbe21IZ9eLJFT_pgMTiBXV_MBPm-dw';

		parent::__construct();
	}

	private function crearusuario($dt=array()){		
		$xy=array( //crear usuario
			'metodo'=>'POST',
			'datos'=>array(
				'action'=>'create',
				"user_info"=>array(
					"email"=>!empty($dt["email"])?$dt["email"]:$user["email"],
					'type'=>!empty($dt["tipo"])?$dt["tipo"]:1,// 1 basico 2 pro 3 corp
					'first_name'=>!empty($dt["nombre"])?$dt["nombre"]:'anonimo1',
					'last_name'=>!empty($dt["apellido"])?$dt["apellido"]:'ape anonimo2',
					'password'=>!empty($dt["clave"])?$dt["clave"]:'demo123'/**/
				)
			)
		);
		return NegZoom::requestZoom($this->tocken,$xy);
	}

	public function defecto(){
		try{
			global $aplicacion;	

			$idrol = $this->usuario['idrol'];
			$rol = $this->usuario["rol"];

			//especificar esquema segun rol
			$esquema = $idrol == 1 ? "videoconferencia/docente" : (($idrol == 2) ? "videoconferencia/docente" : "videoconferencia/alumno");

			//solo docentes
			if($idrol != 3){
				$rst=NegZoom::requestZoom($this->tocken,array('url'=>'users/'.$this->curuserID));
				$nom =explode(",",$this->usuario["nombre_full"]);
				$nomuser=@$nom[1];
				$apeuser=@$nom[0];
				$userclave='user'.$this->usuario["idpersona"]."@".date('Y');
				if(!empty($rst["id"])){ // ususario ya esta registrado
					//var_dump($rst["status"]);
					if($rst["verified"]=='1'||$rst["status"]=='active'){
						$this->esquema = 'videoconferencia/docente';	
						$esquema = "videoconferencia/docente";
					}else{
						$this->datos=array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
						$rst=$this->crearusuario($this->datos);				
						$esquema = "videoconferencia/verificarcuenta";
					}
					$this->datos= array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
				}else{
					if($rst["code"]==1010||$rst["code"]==1001||$rst["code"]==1120){ // usuario no existe
						$this->datos=array('email'=>$this->curuserID,'nombre'=>$nomuser,'apellido'=>$apeuser,'clave'=>$userclave);
						$rst=$this->crearusuario($this->datos);				
						$esquema = "videoconferencia/verificarcuenta";
					}
				}// endif !empty $rst
			}//endif


			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        	$this->documento->script('jquery-confirm.min', '/libs/alert/');
        	$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
        	$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
        	$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Video Conferencias'), true);
			$this->esquema = $esquema;
			// var_dump($this->esquema);exit();
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function invitar()
	{
		try {
			global $aplicacion;	
			$this->oNegZoom->idzoom = @$_GET['id'];
			$this->datos = $this->oNegZoom->dataZoom;
			
			$this->invitados=$this->oNegZoomalumnos->buscar(array('idzoom'=>$this->oNegZoom->idzoom));
			$this->miscursos=$this->oNegGrupoaulaDetalle->buscar(array('iddocente'=>$this->usuario["idpersona"],'idproyecto'=>$this->usuario["idproyecto"]));	

			$this->esquema = 'videoconferencia/invitar';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Zoom', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Virtual classes').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Zoom', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegZoom->idzoom = @$_GET['id'];
			$this->datos = $this->oNegZoom->dataZoom;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Zoom').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'videoconferencia/frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

}