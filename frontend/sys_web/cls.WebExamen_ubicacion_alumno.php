<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		01-03-2018 
 * @copyright	Copyright (C) 01-03-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamen_ubicacion_alumno', RUTA_BASE, 'sys_negocio');
class WebExamen_ubicacion_alumno extends JrWeb
{
	private $oNegExamen_ubicacion_alumno;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamen_ubicacion_alumno = new NegExamen_ubicacion_alumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion_alumno', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idexam_alumno"])&&@$_REQUEST["idexam_alumno"]!='')$filtros["idexam_alumno"]=$_REQUEST["idexam_alumno"];
			if(isset($_REQUEST["idexamen"])&&@$_REQUEST["idexamen"]!='')$filtros["idexamen"]=$_REQUEST["idexamen"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["resultado"])&&@$_REQUEST["resultado"]!='')$filtros["resultado"]=$_REQUEST["resultado"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			
			$this->datos=$this->oNegExamen_ubicacion_alumno->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Examen_ubicacion_alumno'), true);
			$this->esquema = 'examen_ubicacion_alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Examen_ubicacion_alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion_alumno', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegExamen_ubicacion_alumno->idexam_alumno = @$_GET['id'];
			$this->datos = $this->oNegExamen_ubicacion_alumno->dataExamen_ubicacion_alumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Examen_ubicacion_alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'examen_ubicacion_alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idexam_alumno"])&&@$_REQUEST["idexam_alumno"]!='')$filtros["idexam_alumno"]=$_REQUEST["idexam_alumno"];
			if(isset($_REQUEST["idexamen"])&&@$_REQUEST["idexamen"]!='')$filtros["idexamen"]=$_REQUEST["idexamen"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["resultado"])&&@$_REQUEST["resultado"]!='')$filtros["resultado"]=$_REQUEST["resultado"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
						
			$this->datos=$this->oNegExamen_ubicacion_alumno->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xGuardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdexam_alumno)) {
				$this->oNegExamen_ubicacion_alumno->idexam_alumno = $frm['pkIdexam_alumno'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();

           	$this->oNegExamen_ubicacion_alumno->idexamen=@$txtIdexamen;
           	$this->oNegExamen_ubicacion_alumno->idalumno=@$usuarioAct['dni'];
           	$this->oNegExamen_ubicacion_alumno->estado=@$txtEstado;
           	$this->oNegExamen_ubicacion_alumno->resultado=@$txtResultado;
           	$this->oNegExamen_ubicacion_alumno->fecha=@date('Y-m-d H:i:s');

           	if($accion=='_add') {
            	$res=$this->oNegExamen_ubicacion_alumno->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Examen_ubicacion_alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegExamen_ubicacion_alumno->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Examen_ubicacion_alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveExamen_ubicacion_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdexam_alumno'])) {
					$this->oNegExamen_ubicacion_alumno->idexam_alumno = $frm['pkIdexam_alumno'];
				}
				
				$this->oNegExamen_ubicacion_alumno->idexamen=@$frm["txtIdexamen"];
				$this->oNegExamen_ubicacion_alumno->idalumno=@$frm["txtIdalumno"];
				$this->oNegExamen_ubicacion_alumno->estado=@$frm["txtEstado"];
				$this->oNegExamen_ubicacion_alumno->resultado=@$frm["txtResultado"];
				$this->oNegExamen_ubicacion_alumno->fecha=@$frm["txtFecha"];

				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegExamen_ubicacion_alumno->agregar();
				}else{
					$res=$this->oNegExamen_ubicacion_alumno->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegExamen_ubicacion_alumno->idexam_alumno);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDExamen_ubicacion_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegExamen_ubicacion_alumno->__set('idexam_alumno', $pk);
				$this->datos = $this->oNegExamen_ubicacion_alumno->dataExamen_ubicacion_alumno;
				$res=$this->oNegExamen_ubicacion_alumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegExamen_ubicacion_alumno->__set('idexam_alumno', $pk);
				$res=$this->oNegExamen_ubicacion_alumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegExamen_ubicacion_alumno->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}