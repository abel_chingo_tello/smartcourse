<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-01-2021 
 * @copyright	Copyright (C) 19-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSinlogin_quizproyecto', RUTA_BASE);
JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
class WebSinlogin_quizproyecto extends JrWeb
{
	private $oNegSinlogin_quizproyecto;
		
	public function __construct()
	{
		parent::__construct();	
		$this->oNegExamenes = new NegQuizexamenes;	
		$this->oNegSinlogin_quizproyecto = new NegSinlogin_quizproyecto;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Sinlogin_quizproyecto', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idquiz"])&&@$_REQUEST["idquiz"]!='')$filtros["idquiz"]=$_REQUEST["idquiz"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["user_registro"])&&@$_REQUEST["user_registro"]!='')$filtros["user_registro"]=$_REQUEST["user_registro"];
			if(isset($_REQUEST["fecha_hasta"])&&@$_REQUEST["fecha_hasta"]!='')$filtros["fecha_hasta"]=$_REQUEST["fecha_hasta"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$usuarioAct = NegSesion::getUsuario();
			$persona=$this->oNegExamenes->persona();
			$filtros=array();
			if($usuarioAct=='n')
				$filtros["idproyecto"]=$persona["quiz_idproyecto"];
			$this->examen=$this->oNegExamenes->buscar($filtros);	
			
			//$this->datos=$this->oNegSinlogin_quizproyecto->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Asignar examen proyecto'), true);
			$this->esquema = 'sinlogin_quizproyecto-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Sinlogin_quizproyecto', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Asignar examen proyecto').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Sinlogin_quizproyecto', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegSinlogin_quizproyecto->idasignacion = @$_GET['id'];
			$this->datos = $this->oNegSinlogin_quizproyecto->dataSinlogin_quizproyecto;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Asignar examen proyecto').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'sinlogin_quizproyecto-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}