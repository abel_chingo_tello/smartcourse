<?php
set_time_limit(0);

defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoaula',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegReportes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto_cursos',RUTA_BASE,'sys_negocio');

class WebCronjobs extends JrWeb
{
  protected $oNegAcad_grupoauladetalle;
  protected $oNegAcad_grupoaula;
	protected $oNegGeneral;
  protected $oNegReportes;
  protected $oNegProyecto_cursos;

  public function __construct(){
    parent::__construct();
		$this->oNegReportes = new NegReportes;
    $this->oNegGeneral = new NegGeneral;
    $this->oNegAcad_grupoaula = new NegAcad_grupoaula;
    $this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
    $this->oNegProyecto_cursos = new NegProyecto_cursos;
  }
  public function defecto(){
    try{
      $this->documento->plantilla = "blanco";
      var_dump($this->documento->getUrlBase());
		  return 'cron job work';
    }catch(Exception $e){
      return $e->getMessage();
    }
  }
  private function tableComparativoHTML($obj){
    try{
      $table = '';
			$tableHead = '<tr><th>#</th><th>Nombre</th><th>Nota de entrada</th><th>Nota de salida</th></tr>';
      $tablebody = '';
      foreach($obj as $key => $value){
				$nota1 = ($value['entrada']['calificacion_en'] != 'N') ? (floatval($value['entrada']['nota']) * 0.20) : $value['entrada']['nota'];
				$nota2 = ($value['salida']['calificacion_en'] != 'N') ? (floatval($value['salida']['nota']) * 0.20) : $value['salida']['nota'];

				$tablebody .= '<tr>';
				$tablebody .= '<td>'.(intval($key) + 1).'</td><td>'.$value['nombre'].'</td><td>'.$nota1.' pts / 20 pts</td><td>'.$nota2.' pts / 20 pts</td>';
				$tablebody .= '</tr>';
      }
      if(empty($tablebody)){
        $tablebody = '<tr><td>0</td><td>Vacio</td><td>0</td><td>0</td></tr>';
      }
      $table = '<table border="1" style="width:100%;"><thead>'.$tableHead.'</thead><tbody>'.$tablebody.'</tbody></table>';
      return $table;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  public function tableAutoevaluacionHTML($obj){
    $html = '';
    $tableHead = '<tr><th>#</th><th>Nombre</th><th>Nota</th></tr>';
    $tablebody = '';
    $sesionesCategoria = array();
    foreach($obj['sesiones'] as $value){
      $sesionesCategoria[$value['orden']] = null;
    }
    //ubicar los alumnos
    if(isset($obj['sesiones']['alumnos'])){
      foreach($obj['sesiones']['alumnos'] as $key => $value){
        $prepare = array();
        $prepare['id'] = (intval($key) + 1);
        $prepare['nombre'] = $value['nombre'];
        foreach($obj['sesiones'] as $currentsesion){
          $arraysearch = array_search($currentsesion['nombre'],array_column($value['sesion'],'nombre'));
          if($arraysearch !== false ){
            $prepare['nota'] = ($value['sesion'][$arraysearch]['examen']['calificacion_en'] != 'N') ? (floatval($value['sesion'][$arraysearch]['examen']['nota']) * 0.20) : $value['sesion'][$arraysearch]['examen']['nota'];
            $sesionesCategoria[$currentsesion['orden']][] = $prepare;
          }
        }
      }
    }
    //hacer las tablas
    foreach($sesionesCategoria as $key => $value){
      
      $content = '';
      $filas = '';
      $llave = array_search($key,array_column($obj['sesiones'],'orden'));
      if($llave !== false && $llave != null){
        $content .= '<h1>'.$obj['sesiones'][$llave]['orden'].') '.$obj['sesiones'][$llave]['nombre'].'</h1>';
      }
      if($value != null){
        foreach($value as $rows){
          $filas .= "<tr>";
          $filas .= "<td>{$rows['id']}</td><td>{$rows['nombre']}</td><td>{$rows['nota']} pts / 20 pts</td>";
          $filas .= "</tr>";
        }
      }
      $html .= $content."<table border=\"1\" style=\"width:100%\"><thead>{$tableHead}</thead><tbody>{$filas}</tbody></table><div style=\"page-break-after:always;\"></div>";
    }
    return $html;
  }
  public function tableProgresosHTML($obj){
      $html = '';
			$headsesiones = '';
			
			foreach($obj['sesiones'] as $value){
				$headsesiones .= '<td>'.$value['orden'].' Sesión</td>';
			}
			$tableHead = '<tr><th>#</th><th>Nombre</th>'.$headsesiones.'<th>Total</th></tr>';
      $tablebody = '';
      if(isset($obj['alumnos'])){
        foreach($obj['alumnos'] as $key => $value){
          $str_progresos = '';
          if($value['progresos'] != null){
            foreach($value['progresos'] as $vprogress){
              $str_progresos .= '<td>'.$vprogress['progreso'].'</td>';
            }
          }
          $tablebody .= '<tr>';
          $tablebody .= '<td>'.(intval($key) + 1).'</td><td>'.$value['alumno'].'</td>'.$str_progresos.'<td>'.$value['total'].'</td>';
          $tablebody .= '</tr>';
        }
      }
			$html = '<table border="1" style="width:100%;"><thead>'.$tableHead.'</thead><tbody>'.$tablebody.'</tbody></table>';
			return $html;
  }
  public function informesv1(){
    try{
      $this->documento->plantilla = "blanco";
      //obtener parametros
      $idempresa = isset($_REQUEST['empresa']) ? $_REQUEST['empresa'] : null;
      $proyecto = isset($_REQUEST['proyecto']) ? $_REQUEST['proyecto'] : null;
      $idgrupoaula = isset($_REQUEST['grupoaula']) ? $_REQUEST['grupoaula'] : null;
      $curso = isset($_REQUEST['curso']) ? $_REQUEST['curso'] : null;
      
      if( empty($idempresa) || empty($proyecto) ){
        throw new Exception("campos obligatorios");
      }

      //variables extras
      $filepath = array();
      $fecha1 = date('Y-m-d');
      $table ='';
      $_datosAenviar = array();

      //procesar valores
      //- obtener todos los grupos y tipos
      // $fktipos = $this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
      $table = array('','','');
      if(empty($idgrupoaula) && empty($curso)){
        $gruposaula = $this->oNegAcad_grupoaula->buscar(array('idproyecto'=>$proyecto));
        $filtros["idproyecto"] = $proyecto;
        foreach($gruposaula as $value){
          $filtros["idgrupoaula"] = $value['idgrupoaula'];
          $arrCursos = $this->oNegAcad_grupoauladetalle->buscar($filtros);
          if(!empty($arrCursos)){
            foreach($arrCursos as $cursos){
              //plasmar un titulo
              $table[0] .= '<h1 style="text-align:center;">Reporte de exámenes del curso: '.$cursos['strcurso'].'</h1><h2 style="text-align:center;">Grupo: '.$value['nombre'].' </h2>';
              $table[1] .= '<h1 style="text-align:center;">Reporte de autoevaluacion del curso: '.$cursos['strcurso'].'</h1><h2 style="text-align:center;">Grupo: '.$value['nombre'].' </h2>';
              $table[2] .= '<h1 style="text-align:center;">Reporte de progreso del curso: '.$cursos['strcurso'].'</h1><h2 style="text-align:center;">Grupo: '.$value['nombre'].' </h2>';
              /**////////////////////////////// */
              $_idCurso=$cursos['idcurso'];
              //procesar notas comparativo de entrada y salida
              $result = $this->oNegReportes->alumnosNotas(array('urlBase'=>$this->documento->getUrlBase(),'idgrupoaula' => $value['idgrupoaula'],'idcurso'=>$_idCurso, 'idproyecto' => $proyecto));
              $table[0] .=  $this->tableComparativoHTML($result);
              //procesar notas autoevaluacion
              $result = $this->oNegReportes->alumnosNotasAutoevaluacion(array('urlBase'=>$this->documento->getUrlBase(),'idgrupoaula' => $value['idgrupoaula'],'idcurso'=>$_idCurso, 'idproyecto' => $proyecto));
              $table[1] .=  $this->tableAutoevaluacionHTML($result);
              //procesar notas progreso
              $result = $this->oNegReportes->alumnosNotasAutoevaluacion(array('urlBase'=>$this->documento->getUrlBase(),'idgrupoaula' => $value['idgrupoaula'],'idcurso'=>$_idCurso, 'idproyecto' => $proyecto));
              $table[2] .=  $this->tableProgresosHTML($result);
              /**////////////////////////////// */
              //plasmar un salto de pagina
              $table[0] .= '<div style="page-break-after:always;"></div>';
              $table[1] .= '<div style="page-break-after:always;"></div>';
              $table[2] .= '<div style="page-break-after:always;"></div>';
            }
          }
        }
      }else{
        $filtros["idgrupoaula"] = $idgrupoaula;
        $filtros["idcurso"] = $curso;
        $arrCursos = $this->oNegAcad_grupoauladetalle->buscar($filtros);
      }
      if(!empty($table[0])){
        $html = '<html><body>'.$table[0].'</body></html>';
        $filepath[] = $this->oNegReportes->makePDF('reporte_correo1',$html);
      }
      if(!empty($table[1])){
        $html = '<html><body>'.$table[1].'</body></html>';
        $filepath[] = $this->oNegReportes->makePDF('reporte_correo2',$html);
      }
      if(!empty($table[2])){
        $html = '<html><body>'.$table[2].'</body></html>';
        $filepath[] = $this->oNegReportes->makePDF('reporte_correo3',$html);
      }
      //prepara datos para enviar
      $_datosAenviar['empresa'] = $idempresa;
      $_datosAenviar['filespath'] = $filepath;
      $_datosAenviar['nameReporte'] = 'Informe mensual automatico';
      $_datosAenviar['asunto'] = 'Informe mensual automatico';
      //enviar datos
      include_once(RUTA_BASE."frontend".SD."sys_web".SD."cls.WebSendemail.php");
      $obj_websendemail = new WebSendemail();
      $obj_websendemail->func_reporteasocios($_datosAenviar);
      
		  return 'request successfull';
    }catch(Exception $e){
      return $e->getMessage();
    }
  }
  public function publicidad(){
    try{
      $this->documento->plantilla = "blanco";
      //obtener parametros
      $idempresa = isset($_REQUEST['empresa']) ? $_REQUEST['empresa'] : null;
      $proyecto = isset($_REQUEST['proyecto']) ? $_REQUEST['proyecto'] : null;
      $grupoaula = isset($_REQUEST['grupoaula']) ? $_REQUEST['grupoaula'] : null;
      
      if(empty($idempresa) || empty($proyecto)){
        throw new Exception("campos obligatorios");
      }
      //variables extras
      $_datosAenviar = array();
      //buscar cursos amarrados con el proyecto.
      $cursos = $this->oNegProyecto_cursos->buscar(array('idproyecto' => $proyecto,'sql2'=>true));
      
      //procesar datos
      $_datosAenviar['empresa']=$idempresa;
      $_datosAenviar['proyecto']=$proyecto;
      $_datosAenviar['cursos'] = $cursos;
      $_datosAenviar['asunto'] = 'Participa en nuestros cursos';
      //enviar datos
      include_once(RUTA_BASE."frontend".SD."sys_web".SD."cls.WebSendemail.php");
      $obj_websendemail = new WebSendemail();
      $obj_websendemail->func_publicidadalumnos($_datosAenviar);
		  return 'request successfull';
    }catch(Exception $e){
      return $e->getMessage();
    }
  }
}

?>