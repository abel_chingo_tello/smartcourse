<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-01-2018 
 * @copyright	Copyright (C) 15-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
//JrCargador::clase('sys_negocio::NegManuales', RUTA_BASE, 'sys_negocio');
class WebCertificados extends JrWeb
{
	//private $oNegManuales;
		
	public function __construct()
	{
		parent::__construct();		
		//$this->oNegManuales = new NegManuales;
				
	}

	public function defecto(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Matando Errores'), true);
			$this->esquema = 'certificados/listado';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'mantenimientos';
			$this->tabla='historial_sesion';
			$this->documento->setTitulo(JrTexto::_('Corrigiendo'), true);
			$this->esquema = 'certificados/ver';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}	
}