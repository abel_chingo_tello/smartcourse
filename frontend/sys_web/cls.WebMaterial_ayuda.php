<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-01-2018 
 * @copyright	Copyright (C) 17-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegMaterial_ayuda', RUTA_BASE, 'sys_negocio');

class WebMaterial_ayuda extends JrWeb
{
	private $oNegMaterial_ayuda;		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegMaterial_ayuda = new NegMaterial_ayuda;				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Material_ayuda', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idmaterial"])&&@$_REQUEST["idmaterial"]!='')$filtros["idmaterial"]=$_REQUEST["idmaterial"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			
			$this->datos=$this->oNegMaterial_ayuda->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Material_ayuda'), true);
			$this->esquema = 'academico/material_ayuda-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Material_ayuda', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Material_ayuda').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Material_ayuda', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegMaterial_ayuda->idmaterial = @$_GET['id'];
			$this->datos = $this->oNegMaterial_ayuda->dataMaterial_ayuda;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Material_ayuda').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'academico/material_ayuda-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Material_ayuda', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idmaterial"])&&@$_REQUEST["idmaterial"]!='')$filtros["idmaterial"]=$_REQUEST["idmaterial"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			$this->datos=$this->oNegMaterial_ayuda->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarMaterial_ayuda(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='Nuevo';            
            if(!empty(@$idmaterial)) {
				$this->oNegMaterial_ayuda->idmaterial = $idmaterial;
				$accion='Editar';
			}else{
				$this->oNegMaterial_ayuda->archivo='';
			}
           	$usuarioAct = NegSesion::getUsuario();          	
	        if(!empty($_FILES['archivo'])){
				$file=$_FILES["archivo"];
				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$newfoto=$tipo.'-'.date("Ymdhis").'.'.$ext;
				$dir_media=RUTA_BASE . 'static' . SD . 'media' . SD . 'material_ayuda' ;
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newfoto)) 
		  		{
		  			$this->oNegMaterial_ayuda->archivo='/static/media/material_ayuda/'.$newfoto;
		  			//$this->oNegPersonal->set('foto',$newfoto);
		  		}					
			}   	
			$this->oNegMaterial_ayuda->nombre=@$nombre;
			$this->oNegMaterial_ayuda->tipo=@$tipo;
			$this->oNegMaterial_ayuda->descripcion=@$descripcion;
			$this->oNegMaterial_ayuda->mostrar=@$mostrar;
					
            if($accion=='Nuevo') {
            	$res=$this->oNegMaterial_ayuda->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Material_ayuda')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegMaterial_ayuda->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Material_ayuda')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveMaterial_ayuda(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['idmaterial'])) {
					$this->oNegMaterial_ayuda->idmaterial = $frm['idmaterial'];
				}
				
					$this->oNegMaterial_ayuda->nombre=@$frm["nombre"];
					$this->oNegMaterial_ayuda->archivo=@$frm["archivo"];
					$this->oNegMaterial_ayuda->tipo=@$frm["tipo"];
					$this->oNegMaterial_ayuda->descripcion=@$frm["descripcion"];
					$this->oNegMaterial_ayuda->mostrar=@$frm["mostrar"];
					
				   if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegMaterial_ayuda->agregar();
					}else{
						$res=$this->oNegMaterial_ayuda->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegMaterial_ayuda->idmaterial);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDMaterial_ayuda(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegMaterial_ayuda->__set('idmaterial', $pk);
				$this->datos = $this->oNegMaterial_ayuda->dataMaterial_ayuda;
				$res=$this->oNegMaterial_ayuda->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegMaterial_ayuda->__set('idmaterial', $pk);
				$res=$this->oNegMaterial_ayuda->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegMaterial_ayuda->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}