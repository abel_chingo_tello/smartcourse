<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_RAIZ') or die();

JrCargador::clase('sys_negocio::NegGenerador', RUTA_RAIZ);
class WebGenerador extends JrWeb
{
	private $oNegSesion;
	private $oNegConfiguracion;
	private $oNegGenerador;
	
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSesion = new NegSesion;
		$this->oNegGenerador = new NegGenerador;
		$this->usuario = '';//NegSesion::getUsuarioActivo();
	}
	
	public function defecto()
	{		
		return $this->vergenerador();
	}

	public function vergenerador()
	{
		try{

			global $aplicacion;	
			$this->tablas=$this->oNegGenerador->getTablas();
			$this->documento->setTitulo(JrTexto::_('Generador'), true);
			$this->esquema = 'generador/plantilla';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}
	}

	public function getCampos(){
		try {
			if(empty($_REQUEST["tb"])) echo json_encode(array('code'=>'error','msj'=>'tabla no selecionada'));
			$campos=$this->oNegGenerador->getCampos($_REQUEST["tb"]);
			if(!empty($campos))
				echo json_encode(array('code'=>'200','datos'=>$campos));
			else{
				echo json_encode(array('code'=>'error','msj'=>'No exiten campos'));	
			}					
		} catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Error al obtener campos'));
		}
		exit(0);
	}

	public function generarPagina(){
		try {
			if(empty($_REQUEST)) echo json_encode(array('code'=>'error','msj'=>'datos nullos'));
			$frm=$_REQUEST;
			$this->tablaActiva=@$frm["tablaActiva"];
			$this->tabla=$frm["tablaActiva"];
			$this->pk=@$frm["pk"];
			$this->campos=@$frm["campo"];
			$this->frm=$frm;
                   
        if(!empty($frm["createdatos"])){	                	
			$archivo=RUTA_BASE.'sys_datos'.SD.'cls.Dat'.ucfirst($this->tabla).'.php';
			if(file_exists($archivo)) @rename($archivo,RUTA_BASE.'sys_datos'.SD.'cls.Dat'.ucfirst($this->tabla).'_'.date("Ymd_Hms").'_old.php'); 						
			$fp = fopen($archivo, "a");
			$this->esquema = 'generador/datos';
			$tpl_esquema=parent::getEsquema();
			$write = fputs($fp,$tpl_esquema);
			fclose($fp);
			@chmod($archivo,0777);
		}
		if(!empty($frm["createnegocio"])){
			$archivo=RUTA_BASE.'sys_negocio'.SD.'cls.Neg'.ucfirst($this->tabla).'.php';
			if(file_exists($archivo)) @rename($archivo,RUTA_BASE.'sys_negocio'.SD.'cls.Neg'.ucfirst($this->tabla).'_'.date("Ymd_Hms").'_old.php');
			$fp = @fopen($archivo, "a");
			$this->esquema = 'generador/negocio';
			$tpl_esquema=parent::getEsquema();
			$write = fputs($fp,$tpl_esquema);
			fclose($fp);
			@chmod($archivo,0777);
		}
		$tpl_web=RUTA_SITIO.'sys_web'.SD;
		if(!empty($frm["createclaseweb"])){
			$archivo=$tpl_web.'cls.Web'.ucfirst($this->tabla).'.php';
			if(file_exists($archivo)) @rename($archivo,$tpl_web.'cls.Web'.ucfirst($this->tabla).'_'.date("Ymd_Hms").'_old.php');
			$fp = @fopen($archivo, "a");
			$this->esquema = 'generador/web';
			$tpl_esquema=parent::getEsquema();
			$write = fputs($fp,$tpl_esquema);
			fclose($fp);
			@chmod($archivo,0777);
		}
		$tpl_html=$tpl_web.'html'.SD;
		

		if(!empty($frm["createclasewebjson"])){
		$tpl_web=RUTA_BASE.'json'.SD.'sys_web'.SD;
			$archivo=$tpl_web.'cls.Web'.ucfirst($this->tabla).'.php';
			if(file_exists($archivo)) @rename($archivo,$tpl_web.'cls.Web'.ucfirst($this->tabla).'_'.date("Ymd_Hms").'_old.php');
			$fp = @fopen($archivo, "a");
			$this->esquema = 'generador/json';
			$tpl_esquema=parent::getEsquema();
			$write = fputs($fp,$tpl_esquema);
			fclose($fp);
			@chmod($archivo,0777);
		}
		if(!empty($frm["createlistado"])){
			$archivo=$tpl_html.$this->tabla.'-list.php';
			if(file_exists($archivo)) @rename($archivo,$tpl_html.$this->tabla.'-list_'.date("Ymd_Hms").'_old.php');
			$fp = @fopen($archivo, "a");
			$this->esquema = 'generador/listado';
			$tpl_esquema=parent::getEsquema();
			$write = fputs($fp,$tpl_esquema);
			fclose($fp);
			@chmod($archivo,0777);
		}
		if(!empty($frm["createver"])){
			$archivo=$tpl_html.$this->tabla.'-ver.php';
			if(file_exists($archivo)) @rename($archivo,$tpl_html.$this->tabla.'-ver_'.date("Ymd_Hms").'_old.php');
			$fp = @fopen($archivo, "a");
			$this->esquema = 'generador/ver';
			$tpl_esquema=parent::getEsquema();
			$write = fputs($fp,$tpl_esquema);
			fclose($fp);
			@chmod($archivo,0777);
		}
		if(!empty($frm["createfrm"])){
			$archivo=$tpl_html.$this->tabla.'-frm.php';
			if(file_exists($archivo)) @rename($archivo,$tpl_html.$this->tabla.'-frm_'.date("Ymd_Hms").'_old.php');
			$fp = @fopen($archivo, "a");
			$this->esquema = 'generador/frm';
			$tpl_esquema=parent::getEsquema();
			$write = fputs($fp,$tpl_esquema);
			fclose($fp);
			@chmod($archivo,0777);
		}
			echo json_encode(array('code'=>200,'msj'=>'Archivos Generados'));

		} catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Error al obtener campos'));
		}
		exit(0);
	}



	//setGenerarPagina
	public function xsetGenerarPagina(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return ;}
				$frm = $args[0];
				try {
					$this->tablaActiva=@$frm["tablaActiva"];
					$this->tabla=$frm["tablaActiva"];
					$this->pk=@$frm["pk"];
					$this->campos=@$frm["campo"];
					$this->frm=@$frm;
                   
	                if(!empty($frm["createdatos"])){	                	
						$archivo=RUTA_BASE.'sys_datos'.SD.'cls.Dat'.ucfirst($this->tabla).'.php';
						if(file_exists($archivo)) @rename($archivo,RUTA_BASE.'sys_datos'.SD.'cls.Dat'.ucfirst($this->tabla).'_'.date("Ymd_Hms").'_old.php'); 						
						$fp = fopen($archivo, "a");
						$this->esquema = 'generador/datos';
						$tpl_esquema=parent::getEsquema();
						$write = fputs($fp,$tpl_esquema);
						fclose($fp);
						@chmod($archivo,0777);
					}
					if(!empty($frm["createnegocio"])){
						$archivo=RUTA_BASE.'sys_negocio'.SD.'cls.Neg'.ucfirst($this->tabla).'.php';
						if(file_exists($archivo)) @rename($archivo,RUTA_BASE.'sys_negocio'.SD.'cls.Neg'.ucfirst($this->tabla).'_'.date("Ymd_Hms").'_old.php');
						$fp = @fopen($archivo, "a");
						$this->esquema = 'generador/negocio';
						$tpl_esquema=parent::getEsquema();
						$write = fputs($fp,$tpl_esquema);
						fclose($fp);
						@chmod($archivo,0777);
					}
					$tpl_web=RUTA_BASE.'frontend'.SD.'sys_web'.SD;
					if(!empty($frm["createclaseweb"])){
						$archivo=$tpl_web.'cls.Web'.ucfirst($this->tabla).'.php';
						if(file_exists($archivo)) @rename($archivo,$tpl_web.'cls.Web'.ucfirst($this->tabla).'_'.date("Ymd_Hms").'_old.php');
						$fp = @fopen($archivo, "a");
						$this->esquema = 'generador/web';
						$tpl_esquema=parent::getEsquema();
						$write = fputs($fp,$tpl_esquema);
						fclose($fp);
						@chmod($archivo,0777);
					}
					$tpl_html=$tpl_web.'html'.SD;

					if(!empty($frm["createlistado"])){
						$archivo=$tpl_html.$this->tabla.'-list.php';
						if(file_exists($archivo)) @rename($archivo,$tpl_html.$this->tabla.'-list_'.date("Ymd_Hms").'_old.php');
						$fp = @fopen($archivo, "a");
						$this->esquema = 'generador/listado';
						$tpl_esquema=parent::getEsquema();
						$write = fputs($fp,$tpl_esquema);
						fclose($fp);
						@chmod($archivo,0777);
					}
					if(!empty($frm["createver"])){
						$archivo=$tpl_html.$this->tabla.'-ver.php';
						if(file_exists($archivo)) @rename($archivo,$tpl_html.$this->tabla.'-ver_'.date("Ymd_Hms").'_old.php');
						$fp = @fopen($archivo, "a");
						$this->esquema = 'generador/ver';
						$tpl_esquema=parent::getEsquema();
						$write = fputs($fp,$tpl_esquema);
						fclose($fp);
						@chmod($archivo,0777);
					}
					if(!empty($frm["createfrm"])){
						$archivo=$tpl_html.$this->tabla.'-frm.php';
						if(file_exists($archivo)) @rename($archivo,$tpl_html.$this->tabla.'-frm_'.date("Ymd_Hms").'_old.php');
						$fp = @fopen($archivo, "a");
						$this->esquema = 'generador/frm';
						$tpl_esquema=parent::getEsquema();
						$write = fputs($fp,$tpl_esquema);
						fclose($fp);
						@chmod($archivo,0777);
					}

					$oRespAjax->setReturnValue(true);
				} catch(Exception $e) {
					$oRespAjax->call('agregar_msj_interno', 'warning', JrTexto::_('Error al generar Paginas'));
					$oRespAjax->setReturnValue(false);
				}				
			} catch(Exception $e) {
				$oRespAjax->call('agregar_msj_interno', 'warning', JrTexto::_($e->getMessage()));
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
		
}