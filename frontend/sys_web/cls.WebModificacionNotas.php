<?php

defined('RUTA_BASE') or die();
class WebModificacionNotas extends JrWeb
{

	public function __construct()
	{
		parent::__construct();
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Evento_programacion', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/');
			$this->documento->stylesheet('dataTables.material.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/');
			$this->documento->stylesheet('responsive.dataTables.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/');
			$this->documento->script('jquery.dataTables.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/');
			$this->documento->script('dataTables.material.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/');
			$this->documento->script('dataTables.responsive.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/');
			$this->documento->script('dataTables.buttons.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/Buttons-1.6.2/js/');
			$this->documento->script('buttons.flash.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/Buttons-1.6.2/js/');
			$this->documento->script('buttons.html5.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/Buttons-1.6.2/js/');
			$this->documento->script('buttons.print.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/Buttons-1.6.2/js/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/Buttons-1.6.2/css/');
			$this->documento->script('pdfmake.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/pdfmake-0.1.36/');
			$this->documento->script('vfs_fonts', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/pdfmake-0.1.36/');
			$this->documento->script('jszip.min', '/libs/othersLibs/reporte_notas/DataTables_v1.10.20/JSZip-2.5.0/');



			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			$this->documento->script('mine2', '/libs/othersLibs/mine/js/');

			$this->documento->stylesheet('mine-modificacionNotas', '/libs/othersLibs/modificacionNotas/');
			$this->documento->script('mine-crudNotas', '/libs/othersLibs/modificacionNotas/');
			// $this->documento->stylesheet('mine-reporte_notas', '/libs/othersLibs/reporte_notas/');
			$oUsuario = NegSesion::getUsuario();
			$this->usuario = $oUsuario;
			$this->esquema = 'modificacionNotas/modificacion_notas';
			
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas'), true);
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


}
