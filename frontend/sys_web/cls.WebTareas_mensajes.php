<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-02-2020 
 * @copyright	Copyright (C) 11-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTareas_mensajes', RUTA_BASE);
class WebTareas_mensajes extends JrWeb
{
	private $oNegTareas_mensajes;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTareas_mensajes = new NegTareas_mensajes;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tareas_mensajes', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idtareamensaje"])&&@$_REQUEST["idtareamensaje"]!='')$filtros["idtareamensaje"]=$_REQUEST["idtareamensaje"];
			if(isset($_REQUEST["idtarea"])&&@$_REQUEST["idtarea"]!='')$filtros["idtarea"]=$_REQUEST["idtarea"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='')$filtros["rol"]=$_REQUEST["rol"];
			if(isset($_REQUEST["mensaje"])&&@$_REQUEST["mensaje"]!='')$filtros["mensaje"]=$_REQUEST["mensaje"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			$this->datos=$this->oNegTareas_mensajes->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tareas_mensajes'), true);
			$this->esquema = 'tareas_mensajes-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tareas_mensajes', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Tareas_mensajes').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tareas_mensajes', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegTareas_mensajes->idtareamensaje = @$_GET['id'];
			$this->datos = $this->oNegTareas_mensajes->dataTareas_mensajes;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Tareas_mensajes').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'tareas_mensajes-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}