<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-04-2020 
 * @copyright	Copyright (C) 07-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
class WebParticipantes extends JrWeb
{
	

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Evento_programacion', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			// $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			// $this->documento->script('jquery-confirm.min', '/libs/alert/');
			
			// $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			// $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			// $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
	
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/othersLibs/participantes/DataTables_v1.10.20/');
			$this->documento->stylesheet('dataTables.material.min', '/libs/othersLibs/participantes/DataTables_v1.10.20/');
			$this->documento->stylesheet('responsive.dataTables.min', '/libs/othersLibs/participantes/DataTables_v1.10.20/');

			$this->documento->script('jquery.dataTables.min', '/libs/othersLibs/participantes/DataTables_v1.10.20/');
			$this->documento->script('dataTables.material.min', '/libs/othersLibs/participantes/DataTables_v1.10.20/');
			$this->documento->script('dataTables.responsive.min', '/libs/othersLibs/participantes/DataTables_v1.10.20/');
			

			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			$this->documento->script('mine2', '/libs/othersLibs/mine/js/');

			$this->documento->stylesheet('mine-participantes', '/libs/othersLibs/participantes/');
			$this->documento->script('mine-participantes', '/libs/othersLibs/participantes/');


			$filtros = array();
			if (isset($_REQUEST["id_programacion"]) && @$_REQUEST["id_programacion"] != '') $filtros["id_programacion"] = $_REQUEST["id_programacion"];
			if (isset($_REQUEST["fecha"]) && @$_REQUEST["fecha"] != '') $filtros["fecha"] = $_REQUEST["fecha"];
			if (isset($_REQUEST["titulo"]) && @$_REQUEST["titulo"] != '') $filtros["titulo"] = $_REQUEST["titulo"];
			if (isset($_REQUEST["detalle"]) && @$_REQUEST["detalle"] != '') $filtros["detalle"] = $_REQUEST["detalle"];
			if (isset($_REQUEST["hora_comienzo"]) && @$_REQUEST["hora_comienzo"] != '') $filtros["hora_comienzo"] = $_REQUEST["hora_comienzo"];
			if (isset($_REQUEST["hora_fin"]) && @$_REQUEST["hora_fin"] != '') $filtros["hora_fin"] = $_REQUEST["hora_fin"];
			if (isset($_REQUEST["idpersona"]) && @$_REQUEST["idpersona"] != '') $filtros["idpersona"] = $_REQUEST["idpersona"];
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];

			$oUsuario = NegSesion::getUsuario();
			
			$this->usuario = $oUsuario;
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Participantes'), true);
			$this->esquema = 'participantes/participantes';
			if ($oUsuario['idrol'] != "2") {
				$this->msj="NO TIENES PERMISOS PARA ACCEDER A ESTE MÓDULO";
				$this->esquema = 'error/general';
			}
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

}
