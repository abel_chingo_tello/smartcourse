<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
//JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebSendemail extends JrWeb
{	
	//private $oNegNiveles;	

	public function __construct()
	{
		parent::__construct();
	//	$this->oNegNiveles = new NegNiveles;		
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;			
					
			$this->esquema = 'correogeneral';			
			$this->documento->plantilla = 'mail';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function enviarcorreoall(){ //funcion reutilizable  para enviar correo 
		$this->documento->plantilla = 'returnjson';
		try{
			if(empty($_REQUEST)){
				echo json_encode(array('code'=>'Error','msj'=>"Email ".JrTexto::_("data incorrect")));				
				exit();
			}
			@extract($_REQUEST);			
			if(empty($mensaje)){
				echo json_encode(array('code'=>'Error','msj'=>"Email ".JrTexto::_("Message empty")));				
				exit();
			}

			$usuarioActivo = NegSesion::getUsuario();
			$deemail=$usuarioActivo["email"];
			$denombre=ucfirst($usuarioActivo["nombre_full"]);

			JrCargador::clase('jrAdwen::JrCorreo');
			$oCorreo = new JrCorreo;			
			$oCorreo->setRemitente($deemail,$denombre);
			$oCorreo->setAsunto($asunto);

			$iadjunto=0;
			if(!empty($files)){ $_ajuntos = json_decode($files,true);
				foreach($_ajuntos as $filesrc){$iadjunto++;
					$oCorreo->addadjuntos($filesrc, 'file_'.$iadjunto);				
				}
			}

			if(!empty($images)){$_imagenes = json_decode($images,true);
				$mensaje1=$mensaje;
				foreach($_imagenes as $imgsrc){	$iadjunto++;					
					$imgsrc1=@str_replace($this->documento->getUrlBase()."/",RUTA_BASE,$imgsrc);
					$imgsrc2=@str_replace("/",SD,$imgsrc1);					
					$oCorreo->addadjuntos($imgsrc2, 'simg_'.$iadjunto);
					$mensaje=@str_replace($imgsrc,'cid:simg_'.$iadjunto,$mensaje1);					
				}
			}

			
			$emails=json_decode($paraemail,true);  //destinatarios en json;
			$paratodos=!empty($atodos)?$atodos:false; //a todos en un solo correo de 1 en 1;

			$this->esquema = 'correos/general';
			if($paratodos===true){
				$this->Asunto=$asunto;
				if(!empty($emails)){					
					foreach ($emails as $destino){
						$nombre=!empty($destino["nombre"])?$destino["nombre"]:@$destino['name'];
						$oCorreo->addDestinarioPhpmailer($destino['email'], ucfirst($nombre));
					}
				}else{
					echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Mail message, without recipient')));
	        		exit();
				}
				$this->mensaje='<p>'.@str_replace('!!usuario!!','',$mensaje).'</p>';
				$oCorreo->setMensaje(parent::getEsquema());
				$envio=$oCorreo->sendPhpmailer();
				if($envio) echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('Email message successfully sent')));
				else echo json_encode(array('code'=>'Error','msj'=>JrTexto::_("Error sending mail, please try again later")));
	        	exit();
			}else{
				if(!empty($emails)){
					$x=array();
					$tmpmensaje=$mensaje;
					foreach ($emails as $destino){
					    $this->Asunto=$asunto;					
						$this->esquema = 'correos/general';
						$oCorreo->limpiarDestinatarios();
						$nombre=!empty($destino["nombre"])?$destino["nombre"]:@$destino['name'];
						$oCorreo->addDestinarioPhpmailer($destino['email'], ucfirst($nombre));
						//$this->asunto=$nombre." -  ".$destino['email'];
						$this->mensaje='<p>'.@str_replace('!!usuario!!',ucfirst($nombre),$tmpmensaje).'</p>';
						$ruta = RUTA_SITIO . 'sys_web'.SD.'html'.SD.'correos'.SD.'general.php';
						$datos='';
						if(is_file($ruta)){
							ob_start();
							require($ruta);
							$datos = ob_get_contents();
							ob_end_clean();
						}
						$oCorreo->setMensaje($datos);
						$envio=$oCorreo->sendPhpmailer();										
					}					
					echo json_encode(array('code'=>'ok','msj'=>"Mensaje de correo, enviado Satisfactoriamente"));					
			        exit();
				}else{
					echo json_encode(array('code'=>'Error','msj'=>"Mensaje de correo, sin destinatario"));
	        		exit();
				}
			}
			}catch(Exception $e){
				echo json_encode(array('code'=>'Error','msj'=>"Error al enviar correo".$e));
	        	exit();
			}
	}

	public function enviar(){
		try{
			JrCargador::clase('jrAdwen::JrCorreo');
			$oCorreo = new JrCorreo;
			$this->documento->plantilla = 'returnjson';

			$usuarioActivo = NegSesion::getUsuario();
			$this->deemail=$usuarioActivo["email"];
			$this->denombre=$usuarioActivo["nombre_full"];
			$oCorreo->setRemitente($this->deemail,$this->denombre);

			$this->mensaje=$_POST["msje"];
			$this->esquema = 'syscorreo-general';
			$oCorreo->setAsunto($_POST["asunto"]);
			$oCorreo->setMensaje(parent::getEsquema());
			
			//esto se puede  hacer por varios correos...
			$this->para=$_POST["email"];
			$oCorreo->addemails($this->para, $_POST["nombre"]);
			$envio=$oCorreo->sendMail();
			$data=array('code'=>'ok','envio'=>$envio);
        	echo json_encode($data);			
		}catch(Exception $e){echo json_encode(array('code'=>'Error','msj'=>"Error al enviar correo".$e->message()));
	        	exit();}
	}

	public function enviarphpmailer(){
		$this->documento->plantilla = 'returnjson';
		try{
			JrCargador::clase('jrAdwen::JrCorreo');
			$oCorreo = new JrCorreo;
			extract($_POST);

			$usuarioActivo = NegSesion::getUsuario();
			$this->deemail=$deemail;
			$this->denombre=$usuarioActivo["nombre_full"];
			$oCorreo->setRemitente($this->deemail,$this->denombre);
			$mensaje=$msje;
			$oCorreo->setAsunto($asunto);			
			$iadjunto=0;
			if(!empty($imgs)){
				$imgs=json_decode($images);
				foreach ($imgs as $imgsrc){ $iadjunto++;
					$oCorreo->addadjuntos($imgsrc, 'img_'.$iadjunto);
					$mensaje=@str_replace($imgsrc,'cid:img_'.$iadjunto,$mensaje);
				}
			}
			
			if(!empty($files)){
				$files=json_decode($files);
				foreach ($files as $imgsrc){ $iadjunto++;
					$oCorreo->addadjuntos($imgsrc, 'file_'.$iadjunto);
				}
			}

			if(!empty($paraemail)){
				$paraemail=json_decode($paraemail);				
				foreach ($paraemail as $email){ 
					$oCorreo->addDestinarioPhpmailer($email);
				}
			}else{
				$data=array('code'=>'Error','msj'=>"correo sin destinatario");
        		echo json_encode($data);
        		exit();
			}

			$this->esquema = 'syscorreo-general';
			$oCorreo->setMensaje(parent::getEsquema());
			$envio=$oCorreo->sendPhpmailer();
			$data=array('code'=>'ok','msj'=>"correo enviado satisfactoriamente");
        	echo json_encode($data);
        	exit();
		}catch(Exception $e){ 
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
        	echo json_encode($data);
		}
	}

	public function enviar_phpmailer(){
		$this->documento->plantilla = 'returnjson';
		try{						
			JrCargador::clase('jrAdwen::JrCorreo');
			$oCorreo = new JrCorreo;
			$usuarioActivo = NegSesion::getUsuario();
			$this->deemail=$usuarioActivo["email"];
			$this->denombre=$usuarioActivo["nombre_full"];
			$oCorreo->setRemitente($this->deemail,$this->denombre);
			$this->mensaje='<p>'.$_POST["msje"].'</p>';
			$oCorreo->setAsunto($_POST["asunto"]);
			
			//agregando archivos adjuntos:
			$arrAdjuntos = json_decode($_POST['adjuntos'],true);
			$imgs = $this->subirAdjuntos($arrAdjuntos);
			if($imgs && !empty($imgs)){
				$x=1;
				$this->mensaje.='<h1 style="text-align: center; background: #4466bb; color: #fff; padding: 15px 0;"><strong>Seguimiento del Estudiante</strong></h1>';
				foreach ($imgs as $ruta_adj) {
					$oCorreo->addadjuntos($ruta_adj, 'report_'.$x);
					$this->mensaje.='<div><img src="cid:report_'.$x.'" alt="report_'.$x.'" style="width:100%"></div>';
					$x++;
				}
			}
			//esto se puede  hacer por varios correos...
			$paraemail=json_decode($_POST['paraemail'], true);
			if(!empty($paraemail)){
				foreach ($paraemail as $destino){ 
					$oCorreo->addDestinarioPhpmailer($destino['email'], $destino['nombre']);
				}
			}else{
				$data=array('code'=>'Error','msj'=>"correo sin destinatario");
        		echo json_encode($data);
        		exit();
			}
			//$this->para=$_POST["email"];
			//$oCorreo->addDestinarioPhpmailer($this->para, $_POST["nombre"]);

			$this->esquema = 'syscorreo-general';
			$oCorreo->setMensaje(parent::getEsquema());
			$envio=$oCorreo->sendPhpmailer();

			//borrando imgs temporales...
			if(!$imgs && !empty($imgs)){
				foreach ($imgs as $ruta_adj) {
					unlink($ruta_adj);
				}
			}
			$data=array('code'=>'ok','envio'=>$envio);
        	echo json_encode($data);
			//$this->msj_ok =$envio;			
		}catch(Exception $e){}
	}

	public function enviar_phpmailer_angular(){
		$this->documento->plantilla = 'returnjson';
		try{						
			JrCargador::clase('jrAdwen::JrCorreo');
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);

			$oCorreo = new JrCorreo;
			$usuarioActivo = NegSesion::getUsuario();
			$this->deemail=$usuarioActivo["email"];
			$this->denombre=$usuarioActivo["nombre_full"];
			$oCorreo->setRemitente($this->deemail,$this->denombre);
			$this->mensaje='<p>'.$request['msje'].'</p>';
			$oCorreo->setAsunto($request['asunto']);
			
			//agregando archivos adjuntos:
			$arrAdjuntos = $request['adjuntos'];
			foreach($arrAdjuntos as $adjuntos){
				$this->mensaje.='<a href="http://ingles.aceglobal-training.com/"> <img src="'.$adjuntos['src'].'"></a>';
			}
			//esto se puede  hacer por varios correos...
				
			$paraemail=$request['paraemail'];
			if(!empty($paraemail)){
				foreach ($paraemail as $destino){ 
					$oCorreo->addDestinarioPhpmailer($destino['email'], $destino['nombre']);
				}
			}else{
				$data=array('code'=>'Error','msj'=>"correo sin destinatario");
				echo json_encode($data);
				exit();
			}

			$this->esquema = 'syscorreo-general';
			$oCorreo->setMensaje(parent::getEsquema());
			$envio=$oCorreo->sendPhpmailer();
			
			$data=array('code'=>'ok','envio'=>$envio);
        	echo json_encode($data);
			//$this->msj_ok =$envio;			
		}catch(Exception $e){}
	}

	private function subirAdjuntos($arrAdjuntos=array())
		{
			if(empty($arrAdjuntos)) return false;
			$i=1;
			$arrArchivos = array();
			foreach ($arrAdjuntos as $archivo) {
				if($archivo['tipo']=='base64'){
					$source = $archivo['src'];
					list($type, $source) = explode(';', $source);
					list(, $source) = explode(',', $source);
					$source = base64_decode($source);

					$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . 'temp_email' ;
					@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . 'temp_email','0777');
					$name_file = date("Y_m_d_h_i_s").'_report'.$i.'.png';

					file_put_contents($dir_media.SD.$name_file, $source);
					array_push($arrArchivos, $dir_media.SD.$name_file);
				}
				$i++;
			}
			return $arrArchivos;
		}
}