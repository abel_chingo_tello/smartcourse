<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividades', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
class WebTodasUnidades extends JrWeb
{
    private $oNegActividad;
    private $oNegNiveles;
    private $oNegMetodologia;
        
    public function __construct()
    {
        parent::__construct();      
        $this->oNegActividad = new NegActividades;
        $this->oNegNiveles = new NegNiveles;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
    }

    public function defecto(){
        return $this->listado();
    }

    public function listado()
    {
        try{
            global $aplicacion;         
            if(!NegSesion::tiene_acceso('Actividades', 'listar')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }            
            $this->documento->script('lightslider', '/libs/lightslider/js/');
            $this->documento->stylesheet('lightslider', '/libs/lightslider/css/');

            $this->documento->setTitulo(JrTexto::_('Todas las Lecciones'), true);
            
            $this->esquema = 'todas-unidades';
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }
}