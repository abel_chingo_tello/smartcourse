<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-09-2018 
 * @copyright   Copyright (C) 16-09-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula',RUTA_BASE);
JrCargador::clase('sys_negocio::NegProyecto',RUTA_BASE);
class WebNotas_quiz extends JrWeb
{
	private $oNegNotas_quiz;
	protected $oNegPersonasetting;
	protected $oNegPersonal;
	protected $oNegAcad_curso;
	protected $oNegAcad_cursodetalle;
	private $oNegAcad_matricula;
	private $oNegProyecto;
	private  $notificar=false;
	public function __construct()
	{
		parent::__construct();      
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegPersonasetting = new NegPersona_setting;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegProyecto = new NegProyecto;

	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;         
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'list')) {
			//  throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["notatexto"])&&@$_REQUEST["notatexto"]!='')$filtros["notatexto"]=$_REQUEST["notatexto"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["calificacion_en"])&&@$_REQUEST["calificacion_en"]!='')$filtros["calificacion_en"]=$_REQUEST["calificacion_en"];
			if(isset($_REQUEST["calificacion_total"])&&@$_REQUEST["calificacion_total"]!='')$filtros["calificacion_total"]=$_REQUEST["calificacion_total"];
			if(isset($_REQUEST["calificacion_min"])&&@$_REQUEST["calificacion_min"]!='')$filtros["calificacion_min"]=$_REQUEST["calificacion_min"];
			if(isset($_REQUEST["tiempo_total"])&&@$_REQUEST["tiempo_total"]!='')$filtros["tiempo_total"]=$_REQUEST["tiempo_total"];
			if(isset($_REQUEST["tiempo_realizado"])&&@$_REQUEST["tiempo_realizado"]!='')$filtros["tiempo_realizado"]=$_REQUEST["tiempo_realizado"];
			if(isset($_REQUEST["calificacion"])&&@$_REQUEST["calificacion"]!='')$filtros["calificacion"]=$_REQUEST["calificacion"];
			if(isset($_REQUEST["habilidades"])&&@$_REQUEST["habilidades"]!='')$filtros["habilidades"]=$_REQUEST["habilidades"];
			if(isset($_REQUEST["habilidad_puntaje"])&&@$_REQUEST["habilidad_puntaje"]!='')$filtros["habilidad_puntaje"]=$_REQUEST["habilidad_puntaje"];
			if(isset($_REQUEST["intento"])&&@$_REQUEST["intento"]!='')$filtros["intento"]=$_REQUEST["intento"];
			if(isset($_REQUEST["idexamenalumnoquiz"])&&@$_REQUEST["idexamenalumnoquiz"]!='')$filtros["idexamenalumnoquiz"]=$_REQUEST["idexamenalumnoquiz"];
			if(isset($_REQUEST["idexamenproyecto"])&&@$_REQUEST["idexamenproyecto"]!='')$filtros["idexamenproyecto"]=$_REQUEST["idexamenproyecto"];
			if(isset($_REQUEST["idexamenidalumno"])&&@$_REQUEST["idexamenidalumno"]!='')$filtros["idexamenidalumno"]=$_REQUEST["idexamenidalumno"];
			
			$this->datos=$this->oNegNotas_quiz->buscar($filtros);
			$this->datos=$this->oNegNotas_quiz->buscar(array('idrecurso' => 115, 'idexamenalumnoquiz' => 2517));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas_quiz'), true);
			$this->esquema = 'notas_quiz-list';         
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;         
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'add')) {
			//  throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notas_quiz').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function resultados(){
		try {
			global $aplicacion; 
			if(!empty($_REQUEST["examencurso"])){
				if($_REQUEST["examencurso"]==1){//viene desde cursos smartcourso
					//var_dump($_REQUEST);
					$idcurso=$_REQUEST["idcurso"];
					$idexamen=$_REQUEST["idexamen"];
					$iddetalle=$_REQUEST["iddetalle"];
					$tipo=$_REQUEST["tipo"];
					$idnota=$_REQUEST["idnota_quiz"];
					$idpestania=!empty($_REQUEST["idpestania"])?$_REQUEST["idpestania"]:0;
					JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE);
					$oNegBitacora_alumno_smartbook = new NegBitacora_alumno_smartbook;
					$usuarioAct = NegSesion::getUsuario();
					$oNegBitacora_alumno_smartbook->idcurso=@$idcurso;
					$oNegBitacora_alumno_smartbook->idsesion=@$iddetalle;
					$oNegBitacora_alumno_smartbook->idusuario=@$usuarioAct["idpersona"];
					$oNegBitacora_alumno_smartbook->estado='T';
					$oNegBitacora_alumno_smartbook->regfecha=date('Y-m-d H:i:s');
					$hay=$this->oNegNotas_quiz->buscar(array('idnota'=>$idnota,'idalumno'=>@$usuarioAct["idpersona"]));
				
					$calcularprogreso=false;
					$notabloqueoexamenes=65;
					$this->terminoprogreso=false;
					if($tipo=='E'){
						$oNegBitacora_alumno_smartbook->estado='T';
						$this->terminoprogreso=true;
						//$oNegBitacora_alumno_smartbook->progreso=100; // es bitacora samrtbook;
					}else{
						$this->proyecto=$this->oNegProyecto->buscar(array('idproyecto'=>$usuarioAct["idproyecto"]));
						if(!empty($this->proyecto[0])) $this->proyecto=$this->proyecto[0];
						if(!empty($this->proyecto)){ // solo rol alumno
							if(!empty($this->proyecto["jsonlogin"])) $jsontmp=json_decode($this->proyecto["jsonlogin"],true);
							if(!empty($jsontmp['bloqueodeexamenes'])){
								if($jsontmp['bloqueodeexamenes']=='no'){
									$oNegBitacora_alumno_smartbook->estado='T';
									$this->terminoprogreso=true;
									//$oNegBitacora_alumno_smartbook->progreso=100;
								}else {
									$bloqueodeexamenes=$jsontmp['bloqueodeexamenes']; // si == todos menos el examen de entrada; sf==solo examen final
									$notabloqueoexamenes=intval($jsontmp['notabloqueoexamenes']);
									if($bloqueodeexamenes=='sf' && $tipo=='A'){//soloexamen final
										$oNegBitacora_alumno_smartbook->estado='T';
										$this->terminoprogreso=true;
									}else{ //validar nota
										$calcularprogreso=true;
									}
								}
							}
						}
					}


					if($calcularprogreso==false){
						$this->terminoprogreso=true;
						$bitsmartbook=$oNegBitacora_alumno_smartbook->editar();
					}

					
					$this->examen=array();
					if(!empty($hay[0])){
						$this->examen=$hay[0];
						//var_dump($this->examen);
						if($calcularprogreso==true){
							$nt=floatval($this->examen["nota"]);
							$cen=$this->examen["calificacion_en"];
							$cto=intval($this->examen["calificacion_total"]);
							$cmi=$this->examen["calificacion_min"];
							if($cen=='P'||$cen=='N'){if($cto!=100) $nt=($nt*100)/$cto;}
							if($nt>=$notabloqueoexamenes){
								$oNegBitacora_alumno_smartbook->estado='T';
								$bitsmartbook=$oNegBitacora_alumno_smartbook->editar();
								JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE);
								$oNegBitacora_smartbook = new NegBitacora_smartbook;								
								$haybitbook=$oNegBitacora_smartbook->buscar(array('idbitacora_alum_smartbook'=>$bitsmartbook,'idusuario'=>@$usuarioAct["idpersona"],'idsesionB'=>$idpestania));
								if(!empty($haybitbook[0])){
									$this->terminoprogreso=true;
									$oNegBitacora_smartbook->setCampo($haybitbook[0]["idbitacora"],'progreso',100);
								}
							}			
						}
					}
					
				 	//Aqui hya que corregir si se quiere actualizar nota entrada y nota final				
					/*if($tipo=='E'||$tipo=='F' && !empty($idcurso)){
						$rescurso=$this->oNegAcad_curso->setCampo($idcurso,$tipo=='F'?'e_fin':'e_ini',$idexamen);
					}*/

					/////////////////////////////////
					if(!empty($idcurso)&&!empty($usuarioAct["idpersona"])&&!empty($usuarioAct["idproyecto"])){
						$filtromatriculas=array('idcurso'=>$_REQUEST["idcurso"],'idalumno'=>$usuarioAct["idpersona"],'idproyecto'=>$usuarioAct["idproyecto"],'sql'=>'sql1');
						$notas_config=$this->oNegAcad_matricula->notas_config($filtromatriculas);
						//print_r($notas_config);
						//var_dump($notas_config);
						if(!empty($notas_config))
							foreach ($notas_config as $k => $v){
								$this->oNegAcad_matricula->setCampo($v["idmatricula"], 'notafinal', $v["promedionotas"]);
								$this->oNegAcad_matricula->setCampo($v["idmatricula"], 'formulajson', json_encode($v["formula"]));
								if($tipo=='F'&&empty($v["fecha_termino"]))
								$this->oNegAcad_matricula->setCampo($v["idmatricula"], 'fecha_termino', date('Y-m-d'));								
							}
					}				
					/////////////////////////////////
				}
			}           
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');         
			$this->esquema = 'nota_quiz_resultados';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function editar()
	{
		try {
			global $aplicacion;         
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'edit')) {
			//  throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegNotas_quiz->idnota = @$_GET['id'];
			$this->datos = $this->oNegNotas_quiz->dataNotas_quiz;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notas_quiz').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion; 
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');         
			$this->esquema = 'notas_quiz-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;         
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'list')) {
			//  echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//  exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["notatexto"])&&@$_REQUEST["notatexto"]!='')$filtros["notatexto"]=$_REQUEST["notatexto"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["calificacion_en"])&&@$_REQUEST["calificacion_en"]!='')$filtros["calificacion_en"]=$_REQUEST["calificacion_en"];
			if(isset($_REQUEST["calificacion_total"])&&@$_REQUEST["calificacion_total"]!='')$filtros["calificacion_total"]=$_REQUEST["calificacion_total"];
			if(isset($_REQUEST["calificacion_min"])&&@$_REQUEST["calificacion_min"]!='')$filtros["calificacion_min"]=$_REQUEST["calificacion_min"];
			if(isset($_REQUEST["tiempo_total"])&&@$_REQUEST["tiempo_total"]!='')$filtros["tiempo_total"]=$_REQUEST["tiempo_total"];
			if(isset($_REQUEST["tiempo_realizado"])&&@$_REQUEST["tiempo_realizado"]!='')$filtros["tiempo_realizado"]=$_REQUEST["tiempo_realizado"];
			if(isset($_REQUEST["calificacion"])&&@$_REQUEST["calificacion"]!='')$filtros["calificacion"]=$_REQUEST["calificacion"];
			if(isset($_REQUEST["habilidades"])&&@$_REQUEST["habilidades"]!='')$filtros["habilidades"]=$_REQUEST["habilidades"];
			if(isset($_REQUEST["habilidad_puntaje"])&&@$_REQUEST["habilidad_puntaje"]!='')$filtros["habilidad_puntaje"]=$_REQUEST["habilidad_puntaje"];
			if(isset($_REQUEST["intento"])&&@$_REQUEST["intento"]!='')$filtros["intento"]=$_REQUEST["intento"];
			if(isset($_REQUEST["idexamenalumnoquiz"])&&@$_REQUEST["idexamenalumnoquiz"]!='')$filtros["idexamenalumnoquiz"]=$_REQUEST["idexamenalumnoquiz"];
			if(isset($_REQUEST["idexamenproyecto"])&&@$_REQUEST["idexamenproyecto"]!='')$filtros["idexamenproyecto"]=$_REQUEST["idexamenproyecto"];
			if(isset($_REQUEST["idexamenidalumno"])&&@$_REQUEST["idexamenidalumno"]!='')$filtros["idexamenidalumno"]=$_REQUEST["idexamenidalumno"];
						
			$this->datos=$this->oNegNotas_quiz->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function guardarNotas_quiz(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if(empty($_REQUEST)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit(0);            
			}

			$dtoff=json_decode(base64_decode($_REQUEST["datosoffline"]),true);		
			if(false === NegSesion::existeSesion() ){
				JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE);
				$sesion=New NegSesion;
				$dt=$sesion->ingresar(array('_user_'=>$dtoff['_user_'],'idempresa'=>$dtoff['idempresa'],'idproyecto'=>$dtoff['idproyecto']));
				if($dt["code"]!=200){				
					echo json_encode(array('code'=>201,'msj'=>ucfirst(JrTexto::_('tiempo de espera superado, Sesion terminada,Examen no guardado, lo sentimos vuelva hacer el examen'))));
					exit();
				}				
			}

			$res=0;
			@extract($_REQUEST);
			$accion='_add';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0; 
			if(empty($idcurso)||@$idcurso==-1){
				echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('Curso no existe'))));
				exit();
			}
          
		   $usuarioAct = NegSesion::getUsuario();
		   $idalumno=!empty($idalumno)?$idalumno:$usuarioAct["idpersona"];
		   $idcc=!empty($_REQUEST["idcc"])?$_REQUEST["idcc"]:0;
		   try{
			   JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE);
	           $oNegHistorial_sesion = new NegHistorial_sesion;
	           $oNegHistorial_sesion->setCampo($dtoff['idHistSesion'],'fechasalida',date('Y-m-d H:i:s'));
       	   }catch(Exception $ex){}

			$strJson = array();
			$strJson['tipo'] = (isset($_REQUEST['tipexamen'])) ? $_REQUEST['tipexamen'] : @$tipo; 
			$strJson['num_examen'] = (isset($_REQUEST['numexamen'])) ? $_REQUEST['numexamen'] : 0; 
			$preguntas = str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',$preguntas);
			$idproyecto=$usuarioAct["idproyecto"];
			$idcursodetalle=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:0;
			$this->oNegNotas_quiz->idcursodetalle=$idcursodetalle;
			$this->oNegNotas_quiz->idgrupoauladetalle=$idgrupoauladetalle;
			$this->oNegNotas_quiz->idcurso=$idcurso;
			$this->oNegNotas_quiz->idrecurso=@$idrecurso;
			$this->oNegNotas_quiz->idalumno=$idalumno;
			$this->oNegNotas_quiz->tipo=@$tipo;
			$this->oNegNotas_quiz->nota=@$nota;
			$this->oNegNotas_quiz->notatexto=@$notatexto;
			$this->oNegNotas_quiz->regfecha=!empty($regfecha)?$regfecha:date('Y-m-d');
			$this->oNegNotas_quiz->idproyecto=$idproyecto;
			$this->oNegNotas_quiz->calificacion_en=@$calificacion_en;
			$this->oNegNotas_quiz->calificacion_total=@$calificacion_total;
			$this->oNegNotas_quiz->calificacion_min=@$calificacion_min;
			$this->oNegNotas_quiz->tiempo_total=@$tiempo_total;
			$this->oNegNotas_quiz->tiempo_realizado=@$tiempo_realizado;
			$this->oNegNotas_quiz->calificacion=@$calificacion;
			$this->oNegNotas_quiz->habilidades=@$habilidades;
			$this->oNegNotas_quiz->habilidad_puntaje=@$habilidad_puntaje;
			$this->oNegNotas_quiz->intento=!empty($intento)?$intento:1;
			$this->oNegNotas_quiz->idexamenalumnoquiz=!empty($dtoff["idpestania"])?$dtoff["idpestania"]:-1;
			$this->oNegNotas_quiz->idexamenproyecto=-100;
			$this->oNegNotas_quiz->idexamenidalumno=!empty($idexamenidalumno)?$idexamenidalumno:-1;
			$this->oNegNotas_quiz->idcomplementario= $idcc;
			//$this->oNegNotas_quiz->preguntas = json_encode($dtoff);
			$this->oNegNotas_quiz->datos=json_encode($strJson);;
			$this->oNegNotas_quiz->preguntasoffline=@$preguntasoffline;
			$_COOKIE['idexamen']='terminado';
			//Falta Actualizar el tiempo del examen. 

			$archivo=RUTA_BASE.'static'.SD.'media'.SD.'examenes';
			@mkdir($archivo,0777,true);
			@chmod($archivo, 0777);
			$archivo=$archivo.SD.'E'.$usuarioAct["idempresa"]."_P".$usuarioAct["idproyecto"];
			@mkdir($archivo,0777,true);
			@chmod($archivo, 0777);
			$archivo=$archivo.SD.'curso_'.$idcurso."_".$idcc;
			@mkdir($archivo,0777,true);		
			$archivo=$archivo.SD.'ICD_'.$idcursodetalle.'EX_'.$idrecurso."_A".$idalumno."_PE".$dtoff["idpestania"]."_I".(!empty($intento)?$intento:1).".php";
			if(file_exists($archivo)) {
				@rename($archivo,$archivo."_".date('YMDHis'));
				@chmod($archivo, 0777);
			}
			$fp = fopen($archivo, "a");	

			$txtpreguntas=base64_encode(preg_replace('/\s+/', ' ', $preguntas));
			$textexamen = <<<STREXAMEN
<?php 
\$preguntas=<<<STRCHINGO
	$txtpreguntas	
STRCHINGO;
\$preguntas=base64_decode(trim(\$preguntas));
?>
STREXAMEN;
$write = fputs($fp,$textexamen);
			fclose($fp);
			@chmod($archivo,0777);
			$mensajefinal='';
			////////////////////
			//si se quiere  guardar solo el static
			$pos1=stripos($archivo,'static'.SD.'media'.SD.'examenes');
			if ($pos1 !== false) {
				$archivo=substr($archivo,$pos1);
			}

			$dtoff['archivo']=$archivo;

			$this->oNegNotas_quiz->preguntas = json_encode($dtoff);
         	$mensajefinal='';	
			if($accion=='_add'){
				$res=$this->oNegNotas_quiz->agregar();				
				$mensajefinal=array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Assessment')).' '.JrTexto::_('saved successfully'),'newid'=>$res);
			}		
			$infoCurso=$this->oNegAcad_curso->buscar(array('idcurso'=>$idcurso,'idproyecto'=>$usuarioAct["idproyecto"]));
			$infoCurso=$this->oNegAcad_cursodetalle->geformula($idcurso, $idcc,true);

			$infoUsuario = $this->oNegPersonal->buscar(array('idpersona'=>$idalumno));
			$infoEmpresa = $this->oNegProyecto->buscar(array('idproyecto'=>$usuarioAct["idproyecto"]));				
			$this->empresa=$infoEmpresa[0];
			$this->empresa["nombre"]=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:@$this->empresa["empresa"];
			$this->oNegProyecto->idproyecto=$usuarioAct["idproyecto"];
			$proyecto=$this->oNegProyecto->dataProyecto;
			JrCargador::clase('jrAdwen::JrCorreo');
			$oCorreo = new JrCorreo;
			if(!empty($proyecto['correo_servidor']) && !empty($proyecto['correo_puerto']) && !empty($proyecto['correo_email']) && !empty($proyecto['correo_clave'])){
				$oCorreo->Host=$proyecto['correo_servidor'];
				$oCorreo->Port=intval($proyecto['correo_puerto']);
				$oCorreo->Username=$proyecto['correo_email'];
				$oCorreo->Password=$proyecto['correo_clave'];				
			}

			JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
			$examenes=new NegQuizexamenes();
			$infoexamen=$examenes->buscar(array('idexamen'=>$idrecurso));
			if(!empty($infoexamen[0])) $infoexamen=$infoexamen[0];

				
			$asunto=JrTexto::_('Exam result');
			$infonombrecurso_=str_replace('.', "", $infoCurso["nombre"]);
			$titulo=@$infoexamen["titulo"]." - ".$infonombrecurso_;
			$deemail=!empty($this->empresa["correoempresa"])?$this->empresa["correoempresa"]:$usuarioAct["email"];
			$denombre=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:$usuarioAct["nombre_full"];
			$oCorreo->setRemitente($deemail,$denombre);
			$this->titulo=$titulo;
			$paraelasunto=$asunto." - ".$titulo;
			$this->Asunto=$this->pasarHtml($asunto." - ".$titulo);
			$this->mensaje='';
			$this->mensaje.='<div style="text-align:center">'.$this->pasarHtml(JrTexto::_('Score'));//$nota;

			$notatexto0=str_ireplace('the percentage reflected in this email is not your final grade. some writing questions and all the speaking ones still have to be reviewed by your teacher. after having qualified them, your teacher will inform you that you can now verify your final grade in the platform’s grade report', $this->pasarHtml(JrTexto::_('The percentage reflected in this email is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having qualified them, your teacher will inform you that you can now verify your final grade in the platform’s grade report')), $notatexto);

			$notatexto1=str_ireplace('Your result is located on the scale', $this->pasarHtml(JrTexto::_('Your result is located on the scale')), $notatexto0);
			$notatexto2=str_ireplace('Remember', $this->pasarHtml(JrTexto::_('Remember')), $notatexto1);
			$notatexto3=str_ireplace('out of', $this->pasarHtml(JrTexto::_('de')), $notatexto2);
			$notatexto4=str_ireplace('Desaprobaste el examen', '<span style="background-color: #FFB200; padding: 0.5ex; color: #000;">'.$this->pasarHtml('No has conseguido aprobar el examen').'</span>', $notatexto3);
			$notatexto5=str_ireplace('you failed the assessment', '<span style="background-color: #FFB200; padding: 0.5ex; color: #000;">'.$this->pasarHtml(JrTexto::_('You have not managed to pass the exam')).'</span>', $notatexto4);

	
			$this->mensaje.=$notatexto5;
			//exit();
			$this->mensaje.="<div>";
			$this->email_estudiante=$usuarioAct["nombre_full"];
			$this->email_curso=$infonombrecurso_;
			$this->infoexamen=@$infoexamen;
			$oCorreo->setAsunto($paraelasunto);
			//$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com','Abel');
			if(!empty($usuarioAct["email"]))
				$oCorreo->addDestinarioPhpmailer($usuarioAct["email"],$usuarioAct["nombre_full"]);
			if(!empty($this->empresa["correoempresa"])){
				$oCorreo->setRemitente($this->empresa["correoempresa"],$this->empresa["nombre"]);
				$oCorreo->addDestinarioPhpmailer($this->empresa["correoempresa"],$this->empresa["nombre"]);
			}elseif(!empty($usuarioAct["email"])) {
				$oCorreo->setRemitente($usuarioAct["email"],$usuarioAct["nombre_full"]);
			}

			$filelogo=!empty($this->empresa["logo"])?$this->empresa["logo"]:$this->empresa["logoempresa"];

			if(!empty($filelogo)){
				$ipost=stripos($filelogo,'?');
				if($ipost===false) $filelogo=$filelogo;
				else $filelogo=substr($filelogo,0,$ipost);
			}

			if(!empty($filelogo) && is_file(RUTA_BASE.$filelogo)){
				$ipost=stripos($filelogo,'?');
				if($ipost===false) $this->logoempresa=$filelogo;
				else $this->logoempresa=substr($filelogo,0,$ipost);
				$img=str_replace('\\', '', $this->logoempresa);
				$this->logoempresa=URL_BASE.$img;
				//$oCorreo->addadjuntos(RUTA_BASE.$this->logoempresa,'logoempresa');
			}
			if(!empty($infoUsuario[0]["emailpadre"]))
				$oCorreo->addDestinarioPhpmailer($infoUsuario[0]["emailpadre"],'Padre de '.$usuarioAct["nombre_full"]);	
			$this->para='';
			$this->esquema = 'correos/general_examen_2';
			$oCorreo->setMensaje(parent::getEsquema());
			try{
				if($this->notificar==true){
					JrCargador::clase('sys_negocio::NegNotificacion_de', RUTA_BASE);
					$oNotificar= new NegNotificacion_de;
					$datosanotificar=array('tipo'=>'E','idrecurso'=>$idrecurso,'idgrupoauladetalle'=>$idgrupoauladetalle,'nota'=>$nota,'curso'=>$infoCurso);
					$oNotificar->notificar($datosanotificar);
				}				
				$envio=$oCorreo->sendPhpmailer();	
				//var_dump($envio);			
			}catch(Exception $ex){
				//var_dump($ex);
				//if(!empty($mensajefinal)) $mensajefinal['msj'] .=", ". JrTexto::_('No hemos podido enviar el correo');	
				//else $mensajefinal=array('code'=>'Error','msj'=>ucfirst(JrTexto::_('No hemos podido enviar el correo')));							
			}			
			if(!empty($mensajefinal)) echo json_encode($mensajefinal);
			else echo json_encode(array('code'=>'ok','msj'=>'Examen guardado correctamente'));
			exit();
		}catch(Exception $e){
			@extract($_REQUEST);
			$dtoff=json_decode(base64_decode($_REQUEST["datosoffline"]),true);
			 $idcc=!empty($_REQUEST["idcc"])?$_REQUEST["idcc"]:0;
			$archivo=RUTA_BASE.'static'.SD.'media'.SD.'examenes';
			@mkdir($archivo,0777,true);
			@chmod($archivo, 0777);
			$archivo=$archivo.SD.'E'.$usuarioAct["idempresa"]."_P".$usuarioAct["idproyecto"];
			@mkdir($archivo,0777,true);
			@chmod($archivo, 0777);
			$archivo=$archivo.SD.'curso_'.$idcurso."_".$idcc;
			@mkdir($archivo,0777,true);	
			$archivo=$archivo.SD.'ICD_'.$idcursodetalle.'EX_'.$idrecurso."_A".$idalumno."_PE".$dtoff["idpestania"]."_I".(!empty($_REQUEST["intento"])?$_REQUEST["intento"]:1)."_Error".date('Y-m-d').".php";
			if(file_exists($archivo)) @chmod($archivo, 0777);
			$fp = fopen($archivo, "a");	
			$idcursodetalle=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:0;
			$textexamen = '<?php 				
				$idcursodetalle='.$idcursodetalle.';
				$idcurso='.$idcurso.';
				$tipo='.@$tipo.';
				$nota='.@$nota.';
				$notatexto='.@$notatexto.';
				$regfecha='.date('Y-m-d').';				
				$calificacion_en='.@$calificacion_en.';
				$calificacion_total='.@$calificacion_total.';
				$calificacion_min='.@$calificacion_min.';
				$tiempo_total='.@$tiempo_total.';
				$tiempo_realizado='.@$tiempo_realizado.';
				$calificacion='.@$calificacion.';
				$habilidades='.@$habilidades.';
				$habilidad_puntaje='.@$habilidad_puntaje.';
				$intento='.(!empty($intento)?$intento:1).';
				$idexamenproyecto=-100;
				$idexamenidalumno=-1;
				$dtoff=json_encode('."'".base64_decode($_REQUEST["datosoffline"])."'".',true);
				$preguntas=json_decode('."'".preg_replace('/\s+/', ' ', $preguntas)."'".',true); 
				?>';
			$write = fputs($fp,$textexamen);
			fclose($fp);
			@chmod($archivo,0777);

			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()."--".$e->getLine())));
			exit();
		}
	}

	public function noguardarNotas_quiz(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if(empty($_REQUEST)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit(0);            
			}
			$res=0;
			@extract($_REQUEST);
			$accion='_add';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0; 
			if(empty($idcurso)||@$idcurso==-1){
				echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('Curso no existe'))));
				exit();
			}
          
			/*if(!empty(@$idnota)) {
				$this->oNegNotas_quiz->idnota = $idnota;
				$accion='_edit';
			}*/
		   $usuarioAct = NegSesion::getUsuario();
		   $idalumno=!empty($idalumno)?$idalumno:$usuarioAct["idpersona"];
		   $idcc=!empty($_REQUEST["idcc"])?$_REQUEST["idcc"]:0;
			$dtoff=json_decode(base64_decode($_REQUEST["datosoffline"]),true);   
			

			$strJson = array();
			$strJson['tipo'] = (isset($_REQUEST['tipexamen'])) ? $_REQUEST['tipexamen'] : @$tipo; 
			$strJson['num_examen'] = (isset($_REQUEST['numexamen'])) ? $_REQUEST['numexamen'] : 0; 
			$preguntas = str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',$preguntas);
			$infoaguardar=array();
			$infoaguardar2='';
			$idproyecto=$usuarioAct["idproyecto"];
			$idcursodetalle=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:0;
			$infoaguardar['idcursodetalle']=($idcursodetalle);
			$infoaguardar2.='idcursodetalle='.($idcursodetalle).'<br>';
			$infoaguardar['idcurso']=($idcurso);
			$infoaguardar2.='idcurso='.($idcurso).'<br>';
			$infoaguardar['idrecurso']=($idrecurso);
			$infoaguardar2.='idexamen='.($idrecurso).'<br>';
			$infoaguardar['idalumno']=$idalumno;
			$infoaguardar2.='idalumno='.($idalumno).'<br>';
			$infoaguardar['tipo']=($tipo);
			$infoaguardar['nota']=($nota);			
			$infoaguardar['notatexto']=($notatexto);
			$infoaguardar['regfecha']=(!empty($regfecha)?$regfecha:date('Y-m-d'));			
			$infoaguardar['idproyecto']=$idproyecto;
			$infoaguardar['calificacion_en']=($calificacion_en);
			$infoaguardar['calificacion_total']=($calificacion_total);
			$infoaguardar['calificacion_min']=(@$calificacion_min);
			$infoaguardar['tiempo_total']=(@$tiempo_total);
			$infoaguardar['tiempo_realizado']=(@$tiempo_realizado);
			$infoaguardar['calificacion']=(@$calificacion);
			$infoaguardar['habilidades']=(@$habilidades);
			$infoaguardar['habilidad_puntaje']=(@$habilidad_puntaje);
			$infoaguardar['intento']=($intento);
			$infoaguardar['idexamenalumnoquiz']=(!empty($idexamenalumnoquiz)?$idexamenalumnoquiz:-1);
			$infoaguardar['idexamenproyecto']=(!empty($idexamenproyecto)?$idexamenproyecto:-1);
			$infoaguardar['idexamenidalumno']=(!empty($idexamenidalumno)?$idexamenidalumno:-1);
			$infoaguardar['idcomplementario']=$idcc;
			$infoaguardar2.='idcomplementario='.($idcc).'<br>';
			$infoaguardar['datos']=json_encode($strJson);			
			$_COOKIE['idexamen']='terminado';
			$infoCurso = $this->oNegAcad_curso->buscar(array('idcurso'=>$idcurso,'idproyecto'=>$usuarioAct["idproyecto"]));
			$infoUsuario = $this->oNegPersonal->buscar(array('idpersona'=>$idalumno));
			$infoEmpresa = $this->oNegProyecto->buscar(array('idproyecto'=>$usuarioAct["idproyecto"]));	

			if(empty($infoCurso[0])) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Curso no existe')));
				exit();
			}

			$idcc=!empty($_REQUEST["idcc"])?$_REQUEST["idcc"]:0;
			$archivo=RUTA_BASE.'static'.SD.'media'.SD.'examenes';
			@mkdir($archivo,0777,true);
			@chmod($archivo, 0777);
			$archivo=$archivo.SD.'E'.$usuarioAct["idempresa"]."_P".$usuarioAct["idproyecto"];
			@mkdir($archivo,0777,true);
			@chmod($archivo, 0777);
			$archivo=$archivo.SD.'curso_'.$idcurso."_".$idcc;
			@mkdir($archivo,0777,true);
			$archivo=$archivo.SD.'ICD_'.$idcursodetalle.'EX_'.$idrecurso."_A".$idalumno."_PE".$dtoff["idpestania"]."_I".(!empty($_REQUEST["intento"])?$_REQUEST["intento"]:1)."_Error".date('YmdHis').".txt";
			if(file_exists($archivo)) @chmod($archivo, 0777);
			$fp = fopen($archivo, "a");	
			$idcursodetalle=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:0;
			$textexamen = '<?php 				
				$idcursodetalle='.$idcursodetalle.';
				$idcurso='.$idcurso.';
				$tipo='.@$tipo.';
				$nota='.@$nota.';
				$notatexto='.@$notatexto.';
				$regfecha='.date('Y-m-d').';				
				$calificacion_en='.@$calificacion_en.';
				$calificacion_total='.@$calificacion_total.';
				$calificacion_min='.@$calificacion_min.';
				$tiempo_total='.@$tiempo_total.';
				$tiempo_realizado='.@$tiempo_realizado.';
				$calificacion='.@$calificacion.';
				$habilidades='.@$habilidades.';
				$habilidad_puntaje='.@$habilidad_puntaje.';
				$intento='.(!empty($intento)?$intento:1).';
				$idexamenproyecto=-100;
				$idexamenidalumno=-1;
				$dtoff=json_encode('."'".base64_decode($_REQUEST["datosoffline"])."'".',true);
				$preguntas=json_decode('."'".preg_replace('/\s+/', ' ', $preguntas)."'".',true); 
				?>';
			$write = fputs($fp,$textexamen);
			fclose($fp);
			@chmod($archivo,0777);
			$infoCurso=$infoCurso[0];
			$this->empresa=$infoEmpresa[0];
			$this->empresa["nombre"]=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:@$this->empresa["empresa"];
			JrCargador::clase('jrAdwen::JrCorreo');
			$oCorreo = new JrCorreo;
			$asunto=JrTexto::_('Examen No Guardado');
			$titulo='Examen de '.$infoCurso["nombre"];
			$deemail=!empty($this->empresa["correoempresa"])?$this->empresa["correoempresa"]:$usuarioAct["email"];
			$denombre=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:$usuarioAct["nombre_full"];
			$oCorreo->setRemitente($deemail,$denombre);
			$this->titulo=$titulo;
			$this->Asunto=$asunto." - ".$titulo;
			$this->mensaje='<hr><br>'.$usuarioAct["nombre_full"].'<hr>';
			$this->mensaje.='<div style="text-align:center">Nota Obtenida '.$nota."<br>";
			$this->mensaje.=@$notatexto;
			$this->mensaje.='<hr><br>'.$infoaguardar2.'<hr>';
			$this->mensaje.='<hr><br>Datos: a verificar en: '.$archivo.'<hr>';
			$this->mensaje.="<div>";
			
			$oCorreo->setAsunto($this->Asunto);
			$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com','Abel');
			$oCorreo->setRemitente($usuarioAct["email"],$usuarioAct["nombre_full"]);
			$oCorreo->addDestinarioPhpmailer($usuarioAct["email"],$usuarioAct["nombre_full"]);
			$oCorreo->addDestinarioPhpmailer('coordinacion@eduktvirtual.com','Coordinación de Eduktvirtual');			
			if(!empty($this->empresa["correoempresa"]))
				$oCorreo->addDestinarioPhpmailer($this->empresa["correoempresa"],$this->empresa["nombre"]);
			//else $oCorreo->setRemitente($usuarioAct["email"],$usuarioAct["nombre_full"]);
			
			$this->para='';
			$this->esquema = 'correos/general_examen';
			$oCorreo->setMensaje(parent::getEsquema());
			try{
				if(file_exists($archivo))
				$oCorreo->addadjuntos($archivo,'examennoguardado');
				$archivo=RUTA_BASE.'static'.SD.'media'.SD.'Curso_'.$idcurso."__EX".$idrecurso."_Alu".$idalumno.'.txt';
				if(file_exists($archivo)) @unlink($archivo); 
				$fp = fopen($archivo, "a");
				$infoaguardar['preguntas']=@$preguntas;
				$infoaguardar['preguntasoffline']=@$preguntasoffline;
				$tpl_esquema=json_encode($infoaguardar);				
				$write = fputs($fp,$tpl_esquema);
				fclose($fp);
				@chmod($archivo,0777);
				if(!empty($archivo) && is_file($archivo)){
					$oCorreo->addadjuntos($archivo,'logoempresa');
				}				
				$envio=$oCorreo->sendPhpmailer();
			}catch(Exception $ex){
				if(!empty($mensajefinal)) $mensajefinal['msj'] .=", ". JrTexto::_('No hemos podido enviar el correo');	
				else $mensajefinal=array('code'=>'ok','msj'=>ucfirst(JrTexto::_('No hemos podido enviar el correo')));								
			}
			if(!empty($mensajefinal)) echo json_encode($mensajefinal);
			else echo json_encode(array('code'=>'ok','msj'=>'Mensaje Enviado correctamente'));
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()."--".$e->getLine())));
			exit();
		}
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveNotas_quiz(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdnota'])) {
					$this->oNegNotas_quiz->idnota = $frm['pkIdnota'];
				}
				
				$this->oNegNotas_quiz->idcursodetalle=@$frm["txtIdcursodetalle"];
				$this->oNegNotas_quiz->idrecurso=@$frm["txtIdrecurso"];
				$this->oNegNotas_quiz->idalumno=@$frm["txtIdalumno"];
				$this->oNegNotas_quiz->tipo=@$frm["txtTipo"];
				$this->oNegNotas_quiz->nota=@$frm["txtNota"];
				$this->oNegNotas_quiz->notatexto=@$frm["txtNotatexto"];
				$this->oNegNotas_quiz->regfecha=@$frm["txtRegfecha"];
				$this->oNegNotas_quiz->idproyecto=@$frm["txtIdproyecto"];
				$this->oNegNotas_quiz->calificacion_en=@$frm["txtCalificacion_en"];
				$this->oNegNotas_quiz->calificacion_total=@$frm["txtCalificacion_total"];
				$this->oNegNotas_quiz->calificacion_min=@$frm["txtCalificacion_min"];
				$this->oNegNotas_quiz->tiempo_total=@$frm["txtTiempo_total"];
				$this->oNegNotas_quiz->tiempo_realizado=@$frm["txtTiempo_realizado"];
				$this->oNegNotas_quiz->calificacion=@$frm["txtCalificacion"];
				$this->oNegNotas_quiz->habilidades=@$frm["txtHabilidades"];
				$this->oNegNotas_quiz->habilidad_puntaje=@$frm["txtHabilidad_puntaje"];
				$this->oNegNotas_quiz->intento=@$frm["txtIntento"];
				$this->oNegNotas_quiz->idexamenalumnoquiz=@$frm["txtIdexamenalumnoquiz"];
				$this->oNegNotas_quiz->idexamenproyecto=@$frm["txtIdexamenproyecto"];
				$this->oNegNotas_quiz->idexamenidalumno=@$frm["txtIdexamenidalumno"];
					
					if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegNotas_quiz->agregar();
					}else{
						$res=$this->oNegNotas_quiz->editar();
					}
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNotas_quiz->idnota);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas_quiz(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_quiz->__set('idnota', $pk);
				$this->datos = $this->oNegNotas_quiz->dataNotas_quiz;
				$res=$this->oNegNotas_quiz->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_quiz->__set('idnota', $pk);
				$res=$this->oNegNotas_quiz->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
			   
				if(empty($args[0])) { return;}
				$this->oNegNotas_quiz->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}        
}