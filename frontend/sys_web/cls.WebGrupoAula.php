<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
class WebGrupoAula extends JrWeb
{
    private $oNegCursos;
    public function __construct()
    {
        parent::__construct();
        $this->oNegCursos = new NegAcad_curso;
        $this->oNegCursoDetalle = new NegAcad_cursodetalle;
    }

    public function defecto()
    {
        return $this->listado();
    }

    public function listado() //esto es lo que se muestra
    {
        try {
            global $aplicacion;
            //if(!NegSesion::tiene_acceso('Ugel', 'list')) {
            //	throw new Exception(JrTexto::_('Restricted access').'!!');
            //}
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            //$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
            $filtros = array();
            $usuarioAct = NegSesion::getUsuario();
            $this->idrol = $usuarioAct["idrol"];
            //$this->datos=$this->oNegUgel->buscar($filtros);
            //carga plantilla mantenimientos: .\frontend\plantillas\paris\mantenimientos.php
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'grupoaula';
            $this->documento->setTitulo(JrTexto::_('Courses'), true); //title tab
            $this->esquema = 'cursos'; //posible plantilla
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function formular()
    {
        try {
            global $aplicacion;

            $usuarioAct = NegSesion::getUsuario();
            $this->idrol = $usuarioAct["idrol"];
            $idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : '';
            //var_dump($usuarioAct["idproyecto"]);
            if (empty($idcurso)) return $aplicacion->error(JrTexto::_('course Empty'));
            define('IDPROYECTO', $usuarioAct["idproyecto"]);
            $this->curso = $this->oNegCursos->buscar(array('idcurso' => $idcurso, 'idproyecto' => $usuarioAct["idproyecto"]));
            if (empty($this->curso)) return $aplicacion->error(JrTexto::_('Course Empty, unassigned course'));
            $this->curso = $this->curso[0];
            $this->examenes = $this->oNegCursoDetalle->examenes($this->curso["idcurso"], 0);
            $this->documento->setTitulo(JrTexto::_('Courses'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'curso-formula';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    /*public function cursos(){
		try{
			global $aplicacion;
			$filtros=array();
			$this->documento->setTitulo(JrTexto::_('Course'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';			
			$this->esquema = 'academico/curso';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		/*global $aplicacion;
		$this->documento->setTitulo(JrTexto::_('Setting'), true);
        $this->esquema = 'doc_cursos';            
        return parent::getEsquema();*/
    //} 
}
