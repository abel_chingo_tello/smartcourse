<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020 
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_capacidades', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_competencias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_criterios', RUTA_BASE);
class WebAcad_criterios extends JrWeb
{
	private $oNegAcad_criterios;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_criterios = new NegAcad_criterios;
		$this->oNegAcad_competencias = new NegAcad_competencias;
		$this->oNegAcad_capacidades = new NegAcad_capacidades;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegAcad_categorias = new NegAcad_categorias;
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_criterios', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros = array();
			$usuarioAct = NegSesion::getUsuario();
			if (isset($_REQUEST["idcapacidad"]) && @$_REQUEST["idcapacidad"] != '') $filtros["idcapacidad"] = $_REQUEST["idcapacidad"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			$this->idproyecto = $filtros["idproyecto"];
			$this->categorias = $this->oNegAcad_categorias->buscar(array('idproyecto'=>$filtros["idproyecto"]));
			$this->competencias=$this->oNegAcad_competencias->buscar(array('idproyecto'=>$filtros["idproyecto"]));
			$this->cursos=$this->oNegAcad_curso->buscarxcategoria(array('idproyecto'=>$filtros["idproyecto"],'cursosall'=>true));

			$this->datos = $this->oNegAcad_criterios->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->idproyecto = $usuarioAct["idproyecto"];
			$this->documento->setTitulo(JrTexto::_('Criterios'), true);
			$this->esquema = 'curriculo/acad_criterios-list';

			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_criterios', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Nuevo';
			$this->documento->setTitulo(JrTexto::_('criterios') . ' /' . JrTexto::_('New'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_criterios', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Editar';
			$this->oNegAcad_criterios->idcriterio = @$_GET['id'];
			$this->datos = $this->oNegAcad_criterios->dataAcad_criterios;
			$this->pk = @$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('criterios') . ' /' . JrTexto::_('Edit'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_criterios-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
