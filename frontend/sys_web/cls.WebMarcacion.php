<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017 
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_marcacion', RUTA_BASE, 'sys_negocio');
class WebMarcacion extends JrWeb
{
	private $oNegGauladetalle;
	private $oNegMarcacion;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegMarcacion = new NegAcad_marcacion;

	}

	public function defecto(){		
		return $this->filtro();
	}

	public function filtro(){
		try{			
			global $aplicacion;			
			$this->documento->script('moment', '/libs/moment/');
			$this->documento->script('achtcronometro', '/libs/chingo/');
			$this->documento->script('datetimepicker', '/libs/datepicker/');
			 $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
			 $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			$this->esquema = 'academico/marcacion';	
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'sintop';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function vermarcacion(){
		try{
			global $aplicacion;
			$this->documento->script('moment', '/libs/moment/');
			$this->documento->script('achtcronometro', '/libs/chingo/');
			$this->documento->script('datetimepicker', '/libs/datepicker/');
			$this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
			$dni=!empty($_REQUEST["dni"])?$_REQUEST["dni"]:'';
			if($dni==''){
				$this->msj=JrTexto::_('Sorry you do not have scheduled hours');
				$this->esquema = 'academico/marcacion-error';		
			}else{
				$fecha=date('Y-m-d H:i:s');
				$this->horario=$this->yamarco=array();				
				$this->tipomarcacion='';
				$this->entrada=false;
				$this->marcar=true;

				$horarioentrada=$this->oNegGauladetalle->vermarcacion(array('iddocente'=>$dni,'fechainicio'=>$fecha));				
				if(!empty($horarioentrada[0])){
					$this->horario=$horarioentrada[0];
					$this->tipomarcacion='I'; //entrada	
					$yamarco=$this->oNegMarcacion->buscar(array('idhorario'=>$this->horario["idhorario"],'iddocente'=>$dni));
					if(!empty($yamarco[0])){ // ya marco entrada
						$mentrada=$yamarco[0];
						$this->entrada=$mentrada;
						$this->tipomarcacion='O';
						if($mentrada["horasalida"]=='00:00:00'){
							$this->marcar=true;							
						}else{							
							$this->msj=JrTexto::_('I have already set your entry and exit times'); //Ya marco su horario de entrada,y de salida
							$this->marcar=false;
						}
					}
				}

				if($this->marcar==true){
					$horarioentrada=$this->oNegGauladetalle->vermarcacion(array('iddocente'=>$dni,'fechafinal'=>$fecha));
					if(!empty($horarioentrada[0])){
						$this->horario=$horarioentrada[0];
						$this->tipomarcacion='O'; //salida	
						$yamarco=$this->oNegMarcacion->buscar(array('idhorario'=>$this->horario["idhorario"],'iddocente'=>$dni));
						if(!empty($yamarco[0])){ // ya marco entrada
							$mentrada=$yamarco[0];
							$this->entrada=$mentrada;
							if($mentrada["horasalida"]=='00:00:00'){
								$this->entrada["horasalida"]=date('H:i:s');
								$this->marcar=true;
							}else{
								$this->tipomarcacion='O';
								$this->msj=JrTexto::_('I have already set your entry and exit times');//Ya marco su horario de entrada,y de salida
								$this->marcar=false;
							}
						}				
					}elseif(!empty($this->horario)){
						$this->msj=JrTexto::_('Sorry but you can not mark your exit yet');
						$this->marcar=false;
					}
				}

				if(empty($this->horario)){
					$horarioentrada=$this->oNegGauladetalle->vermarcacion(array('iddocente'=>$dni,'fechanext'=>$fecha));
					$this->marcar=false;					
					if(!empty($horarioentrada[0])){
						$this->horario=$horarioentrada[0];
						$this->tipomarcacion='N'; //siguiente Marcacion
						$this->msj=JrTexto::_('This is your next dial'); //Esta es su siguiente marcación
					}else{						
						$this->msj=JrTexto::_('Sorry you do not have scheduled hours'); //disculpe pero usted no tiene horas programadas
					}
				}
				if(empty($this->horario)){
					$this->esquema = 'academico/marcacion-error';
					$this->msj=JrTexto::_("Sorry you do not have scheduled hours");					
				}else{
					$this->esquema = 'academico/marcacion-validar';	
				}
			}			
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}	
	}

	public function mismarcaciones(){
		try{
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			global $aplicacion;
			$dni=!empty($_REQUEST["dni"])?$_REQUEST["dni"]:'';
			$fini=!empty($_REQUEST["fecha_finicio"])?$_REQUEST["fecha_finicio"]:date('Y-m-d');
			$ffin=!empty($_REQUEST["fecha_final"])?$_REQUEST["fecha_final"]:'';
			if($dni==''){
				$this->esquema = 'academico/marcacion-error-sindatos';		
			}else{
				$filtros=array();
				$filtros['iddocente']=$dni;
				if($ffin=='') $filtros['fecha']=$fini;
				else{
					$filtros['fecha_inicio']=$fini;
					$filtros['fecha_final']=$ffin;
				}				
				$this->mismarcaciones=$this->oNegMarcacion->mismarcaciones($filtros);

				$this->esquema = 'academico/marcacion-ver';	
			}			
			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}	
	}

	public function listado()
	{
		try{
			global $aplicacion;
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];

			$this->datos=$this->oNegAcad_curso->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Curse'), true);
			$this->esquema = 'acad_curso-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}



	public function importarcontenido(){ // guardar lo importado
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			if(empty($_POST["idNivel"])){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $filtro=array();
            if(!empty($tipo)) $filtro['tipo']=$tipo;
            $filtro['idnivel']=($idNivel>0)?$idNivel:0;
            $this->datos=$this->oNegActividad->importarcontenido($filtro);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;	
			$filtros=array();				
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];		
			$this->datos=$this->oNegAcad_curso->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	public function guardarcursodetalle(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            @extract($_POST);
            if(!empty($pkIdcurso)){
				$this->oNegAcad_curso->idcurso = $pkIdcurso;
				if(empty($this->oNegAcad_curso->dataAcad_curso)){
					echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Course')." ".JrTexto::_('not register')));
					exit(0);
				}
			}
			$json=json_decode($jsondetalle);
			if(!empty($json)){
					$this->oNegCursodetalle->agregarvarios($pkIdcurso,$json,$idpadre);
			}
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Course')).' '.JrTexto::_('saved successfully'))); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}    
}