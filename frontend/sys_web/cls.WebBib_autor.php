<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017 
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_autor', RUTA_BASE, 'sys_negocio');
class WebBib_autor extends JrWeb
{
	private $oNegBib_autor;
	private $oNegApps_countries;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_autor = new NegBib_autor;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_autor', 'list')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegBib_autor->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_autor'), true);
			$this->esquema = 'bib_autor-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_autor', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Bib_autor').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Bib_autor', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegBib_autor->id_autor = @$_GET['id'];
			$this->datos = $this->oNegBib_autor->dataBib_autor;
			$this->categoria=$this->oNegBib_categoria->buscar();
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_autor').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_autor->id_autor = @$_GET['id'];
			$this->datos = $this->oNegBib_autor->dataBib_autor;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_autor').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_autor-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_autor-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveBib_autor(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_autor'])) {
					$this->oNegBib_autor->id_autor = $frm['pkId_autor'];
				}
				
				$this->oNegBib_autor->__set('nombre',@$frm["txtNombre"]);
					$this->oNegBib_autor->__set('ap_paterno',@$frm["txtAp_paterno"]);
					$this->oNegBib_autor->__set('ap_materno',@$frm["txtAp_materno"]);
					$this->oNegBib_autor->__set('id_pais',@$frm["txtId_pais"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegBib_autor->agregar();
					}else{
									    $res=$this->oNegBib_autor->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBib_autor->id_autor);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBib_autor(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_autor->__set('id_autor', $pk);
				$this->datos = $this->oNegBib_autor->dataBib_autor;
				$res=$this->oNegBib_autor->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_autor->__set('id_autor', $pk);
				$res=$this->oNegBib_autor->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xGetAutor(){

		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;						
			$this->oNegBib_autor->id_autor = @$_GET['id'];
			$this->datos = $this->oNegBib_autor->oNegBib_autor;
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		}catch(Exception $e){
			$data=array('code'=>'Error xGetAutor','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function xAutor(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$this->oNegBib_autor->setLimite(0,99999);
			$this->datos =$this->oNegBib_autor->buscar();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		}catch(Exception $e){
			$data=array('code'=>'Error xAutor','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function xRegistrarAutor(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);
			//$this->datos = '';
				//JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
            
            $this->oNegBib_autor->__set('nombre',$request['nombre']);
		    $this->oNegBib_autor->__set('ap_paterno',$request['ap_paterno']);
		    $this->oNegBib_autor->__set('ap_materno',$request['ap_materno']);
		    $this->oNegBib_autor->__set('id_pais',$request['id_pais']);
			$res=$this->oNegBib_autor->agregar();
			#$id_autor = $this->oNegBib_autor->idautor;
			echo json_encode(array('code'=>'ok','data'=>$res));
		}catch(Exception $e){
			$data=array('code'=>'Error xRegistrarAutor','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	     
}