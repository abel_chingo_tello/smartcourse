<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017 
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_historial', RUTA_BASE, 'sys_negocio');
class WebBib_historial extends JrWeb
{
	private $oNegBib_historial;

	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_historial = new NegBib_historial;
	}

	public function defecto(){
		return $this->listado();
	}

	public function xListar()
	{
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			#if(!NegSesion::tiene_acceso('Bib_historial', 'list')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$usuarioAct = NegSesion::getUsuario();
			$tipo_usuario = ($usuarioAct['rol']=="Alumno")?'A':'P';
			$filtros=[];
			if(!empty($_GET['id_historial']))	{
				$filtros['id_historial']=$_GET['id_historial'];
			}
			if(!empty($_GET['id_estudio']))	{
				$filtros['id_estudio']=$_GET['id_estudio'];
			}
			$filtros['id_personal']=$usuarioAct['dni'];
			$filtros['tipo_usuario']=$tipo_usuario;
			if(!empty($_GET['descripcion']))	{
				$filtros['descripcion']=$_GET['descripcion'];
			}	
			$this->datos=$this->oNegBib_historial->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos,'msj'=>'success'));
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>($e->getMessage())));
		}
	}


	public function xAgregar()
	{
		$this->documento->plantilla = 'returnjson';
		try {

			global $aplicacion;			
			#if(!NegSesion::tiene_acceso('Bib_historial', 'add')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$res=null;
			$usuarioAct = NegSesion::getUsuario();
			$tipo_usuario = ($usuarioAct['rol']=="Alumno")?'A':'P';
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);
			$filtros=$datosHist=array();
			$accion=null;
			if(!empty($request['id_historial'])) {
				$this->oNegBib_historial->id_historial = $request['id_historial'];
				$accion = 'Editar';
			}else{
				if(!empty($request['id_estudio']))	{
					$filtros['id_estudio']=$request['id_estudio'];
				}	
				$filtros['id_personal']=$usuarioAct['dni'];
				$filtros['tipo_usuario']=$tipo_usuario;
				if(!empty($request['descripcion']))	{
					$filtros['descripcion']=$request['descripcion'];
				}
				if(!empty($filtros)){
					$datosHist = $this->oNegBib_historial->buscar($filtros);
					if(!empty($datosHist)){
						$this->oNegBib_historial->__set('id_historial',$datosHist[0]['id_historial']);
						$accion = 'Editar';
					}
				}
			}

			$this->oNegBib_historial->__set('id_estudio',$request['id_estudio']);
			$this->oNegBib_historial->__set('descripcion',$request['descripcion']);
			$this->oNegBib_historial->__set('tipo_usuario',$tipo_usuario);
			$this->oNegBib_historial->__set('id_personal',$usuarioAct['dni']);
			$pk= $this->oNegBib_historial->id_historial;

			if(!empty($pk)){
				$cantidad=(int)$this->oNegBib_historial->dataBib_historial['cantidad']+1;
			}else{
				$cantidad=1;
			}
			$this->oNegBib_historial->__set('cantidad',$cantidad);
			if($accion=='Editar') {
				$res=$this->oNegBib_historial->editar();
			}else{
				$res=$this->oNegBib_historial->agregar();
			}

			echo json_encode(array('code'=>'ok','data'=>$res,'msj'=>'success'));
		} catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>($e->getMessage())));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			#if(!NegSesion::tiene_acceso('Bib_historial', 'edit')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$this->frmaccion='Editar';
			$this->oNegBib_historial->id_historial = @$_GET['id'];
			$this->datos = $this->oNegBib_historial->dataBib_historial;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_historial').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_historial->id_historial = @$_GET['id'];
			$this->datos = $this->oNegBib_historial->dataBib_historial;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_historial').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_historial-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_historial-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
  
}