<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020 
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
class WebMensajes extends JrWeb
{
	private $oNegAcad_grupoaula;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Foros', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			//$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            //$this->documento->script('jquery-confirm.min', '/libs/alert/');
            //$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            //$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            //$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->documento->script('bootstrap-wysiwyg.min', '/libs/gentelella/js/');
			$this->documento->script('prettify', '/libs/gentelella/js/');
			$this->documento->script('custom', '/libs/gentelella/js/');
			$this->documento->script('jquery.hotkeys', '/libs/gentelella/js/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('soft-rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			// $this->documento->stylesheet('clean-blog.min', '/libs/othersLibs/foro/');
			$this->documento->script('tinymce.min', '/libs/othersLibs/foro/tinymce/');
			$this->documento->script('es_MX', '/libs/othersLibs/foro/tinymce/langs/');
			$usuarioAct = NegSesion::getUsuario();
			$filtros = array(
				"idproyecto"=>$usuarioAct["idproyecto"],
				"fechaactiva"=>true
			);
			$this->grupos=$this->oNegAcad_grupoaula->buscar($filtros);
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Foros'), true);
			$this->esquema = 'mensajes/listado';
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

}