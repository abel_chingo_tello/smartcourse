<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016 
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegVocabulario', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebVocabulario extends JrWeb
{
	private $oNegVocabulario;
	private $oNegMatricula;
	private $oNegCursoDetalle;

	public function __construct()
	{
		parent::__construct();		
		$this->oNegVocabulario = new NegVocabulario;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegCursoDetalle = new NegAcad_cursodetalle;
		$this->oNegNiveles = new NegNiveles;
	}

	public function defecto(){
		return $this->visor();
	}
/*
	public function listado()
	{
		try{
			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
						
			$this->datos=$this->oNegVocabulario->buscar();
			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Vocabulario'), true);
			$this->esquema = 'vocabulario-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Vocabulario', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Vocabulario').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Vocabulario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegVocabulario->idvocabulario = @$_GET['id'];
			$this->datos = $this->oNegVocabulario->dataVocabulario;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Vocabulario').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegVocabulario->idvocabulario = @$_GET['id'];
			$this->datos = $this->oNegVocabulario->dataVocabulario;
				
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Vocabulario').' /'.JrTexto::_('see'), true);
			$this->esquema = 'vocabulario-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;				

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'vocabulario-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/

	public function visor()
	{
		try {
			global $aplicacion;

			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Vocabulary')) , 'link'=> '/vocabulario/visor'],
                [ 'texto'=> ucfirst(JrTexto::_('Visor')), ]
            ];

            $this->cursos_nivel = array();
			$this->cursos_nivel = $this->oNegMatricula->cursosAlumno(array("idalumno"=>$this->usuarioAct["dni"]));

			$this->esquema = 'tools/vocabulary_visor';
			$this->documento->plantilla = 'alumno/general';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function jxBuscarXCursoDet()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$this->idCurso = !empty(@$_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$this->idCursoDetalle = !empty(@$_REQUEST["iddetalle"])?$_REQUEST["iddetalle"]:0;
			$this->datos = $this->obtenerVocabulario_CursoDet();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
		} catch (Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
		}
	}
	// ========================== Funciones xajax ========================== //
	public function xSaveVocabulario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				$rutabase=$this->documento->getUrlBase();				 
				$texto=@str_replace($rutabase,'__xRUTABASEx__',trim(@$frm["texto"]));
				if(empty($texto)) return;
				$usuarioAct = NegSesion::getUsuario();
				$this->oNegVocabulario->__set('idnivel',@$frm['idNivel']);
				$this->oNegVocabulario->__set('idunidad',@$frm['idUnidad']);
				$this->oNegVocabulario->__set('idactividad',@$frm['idActividad']);
				$this->oNegVocabulario->__set('idpersonal',@$usuarioAct["dni"]);
				$this->oNegVocabulario->__set('texto',$texto);
				$this->oNegVocabulario->__set('orden',($usuarioAct["rol"]==1?0:1));			
				$res=$this->oNegVocabulario->agregar();
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegVocabulario->idvocabulario);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	/*public function xGetxIDVocabulario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegVocabulario->__set('idvocabulario', $pk);
				$this->datos = $this->oNegVocabulario->dataVocabulario;
				$res=$this->oNegVocabulario->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}*/
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegVocabulario->__set('idvocabulario', $pk);
				$res=$this->oNegVocabulario->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	// ========================== Funciones Privadas ========================== //
	private function obtenerVocabulario_CursoDet()
	{
		try {

			$this->oNegCursoDetalle->idcursodetalle = $this->idCursoDetalle;
            $curso_det = $this->oNegCursoDetalle->getXid();
            $sesion = $this->oNegNiveles->buscar(array('idnivel'=>$curso_det['idrecurso']));
            $s = !empty($sesion)?$sesion[0]['idnivel']:0;
            $unidad = $this->oNegNiveles->buscar(array('idnivel'=>$sesion[0]['idpadre']));
            $u = !empty($unidad)?$unidad[0]['idnivel']:0;
            $nivel = $this->oNegNiveles->buscar(array('idnivel'=>$unidad[0]['idpadre']));
            $n = !empty($nivel)?$nivel[0]['idnivel']:0;

            $vocab = null;
            $vocabulario=$this->oNegVocabulario->buscar(array("idnivel"=>$n,"idunidad"=>$u,"idactividad"=>$s,"tool"=>'V' ));

            if( !empty($vocabulario) ){ $vocab = $vocabulario[0]; }

			return $vocab;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
			
		}
	}   
}