<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017 
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_tipo', RUTA_BASE, 'sys_negocio');
class WebBib_tipo extends JrWeb
{
	private $oNegBib_tipo;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_tipo = new NegBib_tipo;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_tipo', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegBib_tipo->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_tipo'), true);
			$this->esquema = 'bib_tipo-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_tipo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Bib_tipo').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Bib_tipo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegBib_tipo->id_tipo = @$_GET['id'];
			$this->datos = $this->oNegBib_tipo->dataBib_tipo;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_tipo').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_tipo->id_tipo = @$_GET['id'];
			$this->datos = $this->oNegBib_tipo->dataBib_tipo;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_tipo').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_tipo-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_tipo-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveBib_tipo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_tipo'])) {
					$this->oNegBib_tipo->id_tipo = $frm['pkId_tipo'];
				}
				
				$this->oNegBib_tipo->__set('nombre',@$frm["txtNombre"]);
					$this->oNegBib_tipo->__set('filtro',@$frm["txtFiltro"]);
					$this->oNegBib_tipo->__set('extension',@$frm["txtExtension"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegBib_tipo->agregar();
					}else{
									    $res=$this->oNegBib_tipo->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBib_tipo->id_tipo);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBib_tipo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_tipo->__set('id_tipo', $pk);
				$this->datos = $this->oNegBib_tipo->dataBib_tipo;
				$res=$this->oNegBib_tipo->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_tipo->__set('id_tipo', $pk);
				$res=$this->oNegBib_tipo->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function Tipojson(){
		try{
			global $aplicacion;
		$this->documento->plantilla = 'returnjson';
			 $this->datos =$this->oNegBib_tipo->buscar();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit();
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
            exit();
		}
	}
	public function TipoMul(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';
			$filtros['id_registro']='2';
			$this->datos =$this->oNegBib_tipo->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit();
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
            exit();
		}
	}
	public function TipoLib(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';
			$filtros['id_registro']='1';
			$this->datos =$this->oNegBib_tipo->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit();
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
            exit();
		}
	}
	public function TipoSer(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';
			$filtros['id_registro']='3';
			$this->datos =$this->oNegBib_tipo->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit();
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
            exit();
		}
	}
	public function TipoMusica(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';
			$filtros['id_registro']='5';
			$this->datos =$this->oNegBib_tipo->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit();
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
            exit();
		}
	}    
}