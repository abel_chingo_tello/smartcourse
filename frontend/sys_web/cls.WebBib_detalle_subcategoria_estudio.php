<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017 
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_detalle_subcategoria_estudio', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_estudio', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_subcategoria', RUTA_BASE, 'sys_negocio');
class WebBib_detalle_subcategoria_estudio extends JrWeb
{
	private $oNegBib_detalle_subcategoria_estudio;
	private $oNegBib_subcategoria;
	private $oNegBib_estudio;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_detalle_subcategoria_estudio = new NegBib_detalle_subcategoria_estudio;
		$this->oNegBib_subcategoria = new NegBib_subcategoria;
		$this->oNegBib_estudio = new NegBib_estudio;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_detalle_subcategoria_estudio', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegBib_detalle_subcategoria_estudio->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_detalle_subcategoria_estudio'), true);
			$this->esquema = 'bib_detalle_subcategoria_estudio-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_detalle_subcategoria_estudio', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->estudio = $this->oNegBib_estudio->buscar();
			$this->subcategoria = $this->oNegBib_subcategoria->buscar();
			$this->documento->setTitulo(JrTexto::_('Bib_detalle_subcategoria_estudio').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Bib_detalle_subcategoria_estudio', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegBib_detalle_subcategoria_estudio->id_detalle = @$_GET['id'];
			$this->datos = $this->oNegBib_detalle_subcategoria_estudio->dataBib_detalle_subcategoria_estudio;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_detalle_subcategoria_estudio').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_detalle_subcategoria_estudio->id_detalle = @$_GET['id'];
			$this->datos = $this->oNegBib_detalle_subcategoria_estudio->dataBib_detalle_subcategoria_estudio;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_detalle_subcategoria_estudio').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_detalle_subcategoria_estudio-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_detalle_subcategoria_estudio-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveBib_detalle_subcategoria_estudio(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_detalle'])) {
					$this->oNegBib_detalle_subcategoria_estudio->id_detalle = $frm['pkId_detalle'];
				}
				
				$this->oNegBib_detalle_subcategoria_estudio->__set('id_subcategoria',@$frm["txtId_subcategoria"]);
					$this->oNegBib_detalle_subcategoria_estudio->__set('id_estudio',@$frm["txtId_estudio"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegBib_detalle_subcategoria_estudio->agregar();
					}else{
									    $res=$this->oNegBib_detalle_subcategoria_estudio->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBib_detalle_subcategoria_estudio->id_detalle);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBib_detalle_subcategoria_estudio(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_detalle_subcategoria_estudio->__set('id_detalle', $pk);
				$this->datos = $this->oNegBib_detalle_subcategoria_estudio->dataBib_detalle_subcategoria_estudio;
				$res=$this->oNegBib_detalle_subcategoria_estudio->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_detalle_subcategoria_estudio->__set('id_detalle', $pk);
				$res=$this->oNegBib_detalle_subcategoria_estudio->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}