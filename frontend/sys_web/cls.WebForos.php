<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020 
 * @copyright	Copyright (C) 09-03-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegForos', RUTA_BASE);
class WebForos extends JrWeb
{
	private $oNegForos;

	public function __construct()
	{
		parent::__construct();
		$this->oNegForos = new NegForos;
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado() //devuelve solo vista, con el idpersona
	{
		try {
			global $aplicacion;
			$this->documento->stylesheet('clean-blog.min', '/libs/othersLibs/foro/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/othersLibs/dataTables/');
			$this->documento->script('jquery.dataTables.min', '/libs/othersLibs/dataTables/');
			// $this->documento->script('template', '/tema/paris/propio/');
			$filtros = array();
			if (isset($_REQUEST["idforo"]) && @$_REQUEST["idforo"] != '') $filtros["idforo"] = $_REQUEST["idforo"];
			if (isset($_REQUEST["titulo"]) && @$_REQUEST["titulo"] != '') $filtros["titulo"] = $_REQUEST["titulo"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["idusuario"]) && @$_REQUEST["idusuario"] != '') $filtros["idusuario"] = $_REQUEST["idusuario"];
			if (isset($_REQUEST["rol"]) && @$_REQUEST["rol"] != '') $filtros["rol"] = $_REQUEST["rol"];
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["idgrupoaula"]) && @$_REQUEST["idgrupoaula"] != '') $filtros["idgrupoaula"] = $_REQUEST["idgrupoaula"];
			if (isset($_REQUEST["idgrupoauladetalle"]) && @$_REQUEST["idgrupoauladetalle"] != '') $filtros["idgrupoauladetalle"] = $_REQUEST["idgrupoauladetalle"];
			if (isset($_REQUEST["idforopadre"]) && @$_REQUEST["idforopadre"] != '') $filtros["idforopadre"] = $_REQUEST["idforopadre"];
			if (isset($_REQUEST["tipo"]) && @$_REQUEST["tipo"] != '') $filtros["tipo"] = $_REQUEST["tipo"];
			if (isset($_REQUEST["regfecha"]) && @$_REQUEST["regfecha"] != '') $filtros["regfecha"] = $_REQUEST["regfecha"];
			if (isset($_REQUEST["otrosdatos"]) && @$_REQUEST["otrosdatos"] != '') $filtros["otrosdatos"] = $_REQUEST["otrosdatos"];
			if (isset($_REQUEST["texto"]) && @$_REQUEST["texto"] != '') $filtros["texto"] = $_REQUEST["texto"];
			// $this->datos = $this->oNegForos->listar($filtros);
			$usuarioAct = NegSesion::getUsuario();
			$this->idpersona = $usuarioAct['idpersona'];
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Foros'), true);
			// $this->mydata="some data";
			$this->esquema = 'foros/listado';

			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;
			$this->documento->script('tinymce.min', '/libs/othersLibs/foro/tinymce/');
			$this->documento->script('es_MX', '/libs/othersLibs/foro/tinymce/langs/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->stylesheet('clean-blog.min', '/libs/othersLibs/foro/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			
			//dibuja dentro o sin sidelefft
			if (isset($_REQUEST["idpublicacion"]) && @$_REQUEST["idpublicacion"] != '') $filtros["idpublicacion"] = $_REQUEST["idpublicacion"];
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Foros') . ' /' . JrTexto::_('New'), true);
			$this->publicacion = null;
			$usuarioAct = NegSesion::getUsuario();
			$this->oNegForos->idpersona = $usuarioAct['idpersona'];
			if (isset($filtros["idpublicacion"])) {
				$this->oNegForos->idpublicacion = $filtros["idpublicacion"];
				$this->publicacion = $this->oNegForos->getXid();
			}
			$this->esquema = 'foros/nuevo';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function visualizar() //usa el metodo buscar
	{
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			$this->documento->stylesheet('clean-blog.min', '/libs/othersLibs/foro/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Foros') . ' /' . JrTexto::_('New'), true);
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/othersLibs/dataTables/');
			$this->documento->script('jquery.dataTables.min', '/libs/othersLibs/dataTables/');

			if (isset($_REQUEST["idpublicacion"]) && @$_REQUEST["idpublicacion"] != '') $filtros["idpublicacion"] = $_REQUEST["idpublicacion"];
			$filtros["idpersona"] = $usuarioAct['idpersona'];
			$this->data = $this->oNegForos->buscar($filtros)[0];
			//echo "<hr>".print_r($this->data)."<hr>";
			$this->esquema = 'foros/visualizar';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function editar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Foros', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion = 'Editar';
			$this->oNegForos->idforo = @$_GET['id'];
			$this->datos = $this->oNegForos->dataForos;
			$this->pk = @$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Foros') . ' /' . JrTexto::_('Edit'), true);
			return $this->form();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'foros-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
