<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-02-2021 
 * @copyright	Copyright (C) 17-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotificacion_para', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotificacion_de', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
class WebNotificacion_para extends JrWeb
{
	private $oNegNotificacion_para;
	private $oNegNotificacion_de;
	private $oNegPersonal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotificacion_para = new NegNotificacion_para;
		$this->oNegNotificacion_de = new NegNotificacion_de;
	$this->oNegPersonal = new NegPersonal;
			
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notificacion_para', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idnotificacion"])&&@$_REQUEST["idnotificacion"]!='')$filtros["idnotificacion"]=$_REQUEST["idnotificacion"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			$this->datos=$this->oNegNotificacion_para->buscar($filtros);
			$this->fkidnotificacion=$this->oNegNotificacion_de->buscar();
			$this->fkidpersona=$this->oNegPersonal->buscar();
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notificacion_para'), true);
			$this->esquema = 'notificacion_para-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notificacion_para', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notificacion_para').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notificacion_para', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegNotificacion_para->idnotipersona = @$_GET['id'];
			$this->datos = $this->oNegNotificacion_para->dataNotificacion_para;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notificacion_para').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fkidnotificacion=$this->oNegNotificacion_de->buscar();
			$this->fkidpersona=$this->oNegPersonal->buscar();
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'notificacion_para-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}