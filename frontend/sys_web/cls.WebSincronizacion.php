<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
//JrCargador::clase('sys_negocio::NegLinks', RUTA_BASE, 'sys_negocio');
class WebSincronizacion extends JrWeb
{
	//private $oNegLinks;
	public function __construct()
	{
		parent::__construct();		
	//	$this->oNegLinks = new NegLinks;
	}

	public function defecto(){
		return $this->versetting();
	}

	public function versetting(){
		global $aplicacion;
		$this->documento->setTitulo(JrTexto::_('Setting'), true);
		global $aplicacion;
		$usuarioAct = NegSesion::getUsuario();
		$idrol=$usuarioAct["idrol"];
		if($idrol==3)//alumno
		$this->esquema = 'sincronizar/vista1'; 
		elseif($idrol==2)//docente
		$this->esquema = 'sincronizar/vista2'; 
		elseif($idrol==1) //administrador
		$this->esquema = 'sincronizar/vista2'; 
		else
          $this->esquema = 'sincronizar/vistageneral';            
        return parent::getEsquema();
	} 
}