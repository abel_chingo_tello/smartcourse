<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016 
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRoles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPermisos', RUTA_BASE, 'sys_negocio');
class WebPermisos extends JrWeb
{
	private $oNegPermisos;
	private $oNegRoles;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPermisos = new NegPermisos;
		$this->oNegRoles = new NegRoles;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Permisos', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
            $this->idRol=!empty($_GET["idRol"])?$_GET["idRol"]:1;
			$this->roles=$this->oNegRoles->buscar();
			$this->datos=$this->oNegPermisos->buscar();
			$this->datosxRol=$this->oNegPermisos->buscar(Array('rol'=>$this->idRol));

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Permisos'), true);
			$this->esquema = 'permisos-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Permisos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->roles=$this->oNegRoles->buscar();
			$this->datos=$this->oNegMenu->buscar();
			$this->documento->setTitulo(JrTexto::_('Permisos').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Permisos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegPermisos->idpermiso = @$_GET['id'];
			$this->datos = $this->oNegPermisos->dataPermisos;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Permisos').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegPermisos->idpermiso = @$_GET['id'];
			$this->datos = $this->oNegPermisos->dataPermisos;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Permisos').' /'.JrTexto::_('see'), true);
			$this->esquema = 'permisos-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'permisos-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSavePermisos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdpermiso'])) {
					$this->oNegPermisos->idpermiso = $frm['pkIdpermiso'];
				}
				
					$this->oNegPermisos->__set('rol',@$frm["txtRol"]);
					$this->oNegPermisos->__set('menu',@$frm["txtMenu"]);
					$this->oNegPermisos->__set('_list',@$frm["txt_list"]);
					$this->oNegPermisos->__set('_add',@$frm["txt_add"]);
					$this->oNegPermisos->__set('_edit',@$frm["txt_edit"]);
					$this->oNegPermisos->__set('_delete',@$frm["txt_delete"]);
				    if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegPermisos->agregar();
					}else{
						$res=$this->oNegPermisos->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPermisos->idpermiso);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try{               
				if(empty($args[0])) { return;}
				$frm=$args[0];
				$res=$this->oNegPermisos->setCampo($frm["rol"],$frm["menu"],$frm["campo"],$frm["estado"]);
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Success')), JrTexto::_('Register Update'), 'success');
				$oRespAjax->setReturnValue($res);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
	     
}