<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-01-2018 
 * @copyright	Copyright (C) 03-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_educacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebPersona_educacion extends JrWeb
{
	private $oNegPersona_educacion;
	private $oNegPersonal;
	private $oNegGeneral;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_educacion = new NegPersona_educacion;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegGeneral = new NegGeneral;
			
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Persona_educacion', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["ideducacion"])&&@$_REQUEST["ideducacion"]!='')$filtros["ideducacion"]=$_REQUEST["ideducacion"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["institucion"])&&@$_REQUEST["institucion"]!='')$filtros["institucion"]=$_REQUEST["institucion"];
			if(isset($_REQUEST["tipodeestudio"])&&@$_REQUEST["tipodeestudio"]!='')$filtros["tipodeestudio"]=$_REQUEST["tipodeestudio"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["areaestudio"])&&@$_REQUEST["areaestudio"]!='')$filtros["areaestudio"]=$_REQUEST["areaestudio"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["fechade"])&&@$_REQUEST["fechade"]!='')$filtros["fechade"]=$_REQUEST["fechade"];
			if(isset($_REQUEST["fechahasta"])&&@$_REQUEST["fechahasta"]!='')$filtros["fechahasta"]=$_REQUEST["fechahasta"];
			if(isset($_REQUEST["actualmente"])&&@$_REQUEST["actualmente"]!='')$filtros["actualmente"]=$_REQUEST["actualmente"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			
			$this->datos=$this->oNegPersona_educacion->buscar($filtros);
			$this->fktipodeestudio=$this->oNegGeneral->buscar();
			$this->fkareaestudio=$this->oNegGeneral->buscar();
			$this->fksituacion=$this->oNegGeneral->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Persona_educacion'), true);
			$this->esquema = 'persona_educacion-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Persona_educacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Persona_educacion').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Persona_educacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegPersona_educacion->ideducacion = @$_GET['id'];
			$this->datos = $this->oNegPersona_educacion->dataPersona_educacion;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Persona_educacion').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fktipodeestudio=$this->oNegGeneral->buscar();
			$this->fkareaestudio=$this->oNegGeneral->buscar();
			$this->fksituacion=$this->oNegGeneral->buscar();
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'persona_educacion-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Persona_educacion', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}*/
			$filtros=array();
			if(isset($_REQUEST["ideducacion"])&&@$_REQUEST["ideducacion"]!='')$filtros["ideducacion"]=$_REQUEST["ideducacion"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["institucion"])&&@$_REQUEST["institucion"]!='')$filtros["institucion"]=$_REQUEST["institucion"];
			if(isset($_REQUEST["tipodeestudio"])&&@$_REQUEST["tipodeestudio"]!='')$filtros["tipodeestudio"]=$_REQUEST["tipodeestudio"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["areaestudio"])&&@$_REQUEST["areaestudio"]!='')$filtros["areaestudio"]=$_REQUEST["areaestudio"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["fechade"])&&@$_REQUEST["fechade"]!='')$filtros["fechade"]=$_REQUEST["fechade"];
			if(isset($_REQUEST["fechahasta"])&&@$_REQUEST["fechahasta"]!='')$filtros["fechahasta"]=$_REQUEST["fechahasta"];
			if(isset($_REQUEST["actualmente"])&&@$_REQUEST["actualmente"]!='')$filtros["actualmente"]=$_REQUEST["actualmente"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			$this->datos=$this->oNegPersona_educacion->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarPersona_educacion(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            if(!empty(@$ideducacion)) {
				$this->oNegPersona_educacion->ideducacion = $ideducacion;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegPersona_educacion->idpersona=@$idpersona;
			$this->oNegPersona_educacion->institucion=@$institucion;
			$this->oNegPersona_educacion->tipodeestudio=@$tipodeestudio;
			$this->oNegPersona_educacion->titulo=@$titulo;
			$this->oNegPersona_educacion->areaestudio=!empty($areaestudio)?$areaestudio:0;
			$this->oNegPersona_educacion->situacion=@$situacion;
			$this->oNegPersona_educacion->fechade=@$fechade;
			$this->oNegPersona_educacion->fechahasta=!empty($fechahasta)?$fechahasta:date('Y/m/d');
			$this->oNegPersona_educacion->actualmente=@$actualmente;
			$this->oNegPersona_educacion->mostrar=!empty($mostrar)?$mostrar:1;
					
            if($accion=='_add') {
            	$res=$this->oNegPersona_educacion->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_educacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersona_educacion->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_educacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePersona_educacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdeducacion'])) {
					$this->oNegPersona_educacion->ideducacion = $frm['pkIdeducacion'];
				}
				
				$this->oNegPersona_educacion->idpersona=@$frm["txtIdpersona"];
					$this->oNegPersona_educacion->institucion=@$frm["txtInstitucion"];
					$this->oNegPersona_educacion->tipodeestudio=@$frm["txtTipodeestudio"];
					$this->oNegPersona_educacion->titulo=@$frm["txtTitulo"];
					$this->oNegPersona_educacion->areaestudio=@$frm["txtAreaestudio"];
					$this->oNegPersona_educacion->situacion=@$frm["txtSituacion"];
					$this->oNegPersona_educacion->fechade=@$frm["txtFechade"];
					$this->oNegPersona_educacion->fechahasta=@$frm["txtFechahasta"];
					$this->oNegPersona_educacion->actualmente=@$frm["txtActualmente"];
					$this->oNegPersona_educacion->mostrar=@$frm["txtMostrar"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegPersona_educacion->agregar();
					}else{
									    $res=$this->oNegPersona_educacion->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPersona_educacion->ideducacion);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPersona_educacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_educacion->__set('ideducacion', $pk);
				$this->datos = $this->oNegPersona_educacion->dataPersona_educacion;
				$res=$this->oNegPersona_educacion->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_educacion->__set('ideducacion', $pk);
				$res=$this->oNegPersona_educacion->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersona_educacion->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}