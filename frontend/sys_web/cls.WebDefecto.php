<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
class WebDefecto extends JrWeb
{	
	public function __construct()
	{
		
		parent::__construct();
		global $aplicacion;
		$this->curusuario = NegSesion::getUsuario();
		if(empty($this->curusuario)) $aplicacion->redir('sesion/login');
		$this->oNegBolsa_empresas = new NegBolsa_empresas;
		$this->oNegProyecto = new NegProyecto;	
	}

	public function defecto()
	{
		try {
			global $aplicacion;
			// $this->oNegProyecto->idproyecto = $this->curusuario["idproyecto"];
			// $this->proyecto = $this->oNegProyecto->dataProyecto;
			// $nomempresa=@$_COOKIE["sesionnomempresa"];
			// $this->documento->setTitulo($nomempresa, true);
			// $this->documento->plantilla ='proyecto/administrable';
			$this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->curusuario["idempresa"]));
			if(!empty($this->empresa[0])) $this->empresa=$this->empresa[0];
			else{
				NegSesion::salir();
				$aplicacion->redir('sesion/login');
    			//exit('Empresa no registrada');
			}
			$this->nomempresa = $this->empresa["nombre"];
			$this->logo_emp = $this->empresa["logo"];
			$this->documento->setTitulo($this->nomempresa, false);
			$this->documento->plantilla =!empty($_REQUEST["tpl"])?$_REQUEST["tpl"]:'general';
			// $tuser=$this->curusuario["tuser"];
			// $tipouser='';
			// $this->tipo_ = $tuser;
			// if($tuser=='s') $this->esquema = 'empresas/addmodulos_s';
			// else 
			$this->esquema = 'paris/iframe';
			// $this->documento->plantilla = 'login';
			// $this->esquema = 'paris/login';
			if (JrTools::ismobile()) {
				$this->documento->plantilla = 'mobile/principal';
				$this->esquema = 'mobile/principal';
				// echo "is mobile success";
			}
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	private function mostrarweb(){

	} 
	public function bienvenido(){
		try {
			global $aplicacion;
			$this->documento->plantilla='modal';
			$this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->curusuario["idempresa"]))[0];
			$this->esquema = 'paris/bienvenido';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}

	}
}