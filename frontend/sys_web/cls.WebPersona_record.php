<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		01-02-2018 
 * @copyright	Copyright (C) 01-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_record', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebPersona_record extends JrWeb
{
	private $oNegPersona_record;
	private $oNegPersonal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_record = new NegPersona_record;
		$this->oNegPersonal = new NegPersonal;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_record', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idrecord"])&&@$_REQUEST["idrecord"]!='')$filtros["idrecord"]=$_REQUEST["idrecord"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tiporecord"])&&@$_REQUEST["tiporecord"]!='')$filtros["tiporecord"]=$_REQUEST["tiporecord"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			$this->datos=$this->oNegPersona_record->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Persona_record'), true);
			$this->esquema = 'persona_record-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_record', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Persona_record').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_record', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegPersona_record->idrecord = @$_GET['id'];
			$this->datos = $this->oNegPersona_record->dataPersona_record;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Persona_record').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'persona_record-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_record', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idrecord"])&&@$_REQUEST["idrecord"]!='')$filtros["idrecord"]=$_REQUEST["idrecord"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tiporecord"])&&@$_REQUEST["tiporecord"]!='')$filtros["tiporecord"]=$_REQUEST["tiporecord"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
						
			$this->datos=$this->oNegPersona_record->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarPersona_record(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';  
            $url='';          
            if(!empty(@$idrecord)) {
				$this->oNegPersona_record->idrecord = $idrecord;
				$accion='_edit';
				$url=@$this->oNegPersona_record->archivo;
			}

			if(!empty($_FILES['archivo'])){
				$file=$_FILES["archivo"];
				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$newfoto=$idpersona.'-'.date("Ymdhis").'.'.$ext;
				$dir_media=RUTA_BASE . 'static' . SD . 'media' . SD . 'usuarios';
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newfoto)) 
		  		{
		  			$url='static/media/usuarios/'.$newfoto;
		  			$this->oNegPersona_record->archivo=$url;		  			
		  		}					
			}      	
	        $usuarioAct = NegSesion::getUsuario(); 	
			$this->oNegPersona_record->idpersona=@$idpersona;
			$this->oNegPersona_record->tiporecord=@$tiporecord;
			$this->oNegPersona_record->titulo=@$titulo;
			$this->oNegPersona_record->descripcion=@$descripcion;			
			$this->oNegPersona_record->fecharegistro=!empty($fecharegistro)?$fecharegistro:date('Y-m-d H:i:s');
			$this->oNegPersona_record->mostrar=!empty($mostrar)?$mostrar:1;
			$this->oNegPersona_record->idusuario=$usuarioAct["idpersona"];

            if($accion=='_add'){
            	$res=$this->oNegPersona_record->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_record')).' '.JrTexto::_('saved successfully'),'newid'=>$res,'archivo'=>$url)); 
            }else{
            	$res=$this->oNegPersona_record->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_record')).' '.JrTexto::_('update successfully'),'newid'=>$res,'archivo'=>$url)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_record->__set('idrecord', $pk);
				$res=$this->oNegPersona_record->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersona_record->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}     
}