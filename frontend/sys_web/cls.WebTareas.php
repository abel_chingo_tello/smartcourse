<?php
defined('RUTA_BASE') or die();
/* JrCargador::clase('sys_negocio::NegTareas', RUTA_BASE); */
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
class WebTareas extends JrWeb
{
	//private $oNegTareas;
	private $oNegAcad_curso;

	public function __construct()
	{
		parent::__construct();
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		//$this->oNegTareas = new NegTareas;
	}

	public function defecto()
	{
		return $this->listado();
	}



	public function listado()
	{
		try {
			global $aplicacion;
			$this->documento->stylesheet('list-theme', '/tema/css/list/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('funciones', '/js/');
			// mine
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			
			$this->documento->stylesheet('soft-rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('mine-rubrica', '/libs/othersLibs/rubrica/');
			
			$this->documento->script('mine-rubrica', '/libs/othersLibs/rubrica/');
			$this->documento->script('mine-crud-tarea', '/libs/othersLibs/crud-tarea/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			// mine
			$filtros = array();
			if (isset($_REQUEST["idcurso"]) && @$_REQUEST["idcurso"] != '') $filtros["idcurso"] = $_REQUEST["idcurso"];
			if (isset($_REQUEST["nombre"]) && @$_REQUEST["nombre"] != '') $filtros["nombre"] = $_REQUEST["nombre"];
			if (isset($_REQUEST["descripcion"]) && @$_REQUEST["descripcion"] != '') $filtros["descripcion"] = $_REQUEST["descripcion"];
			if (isset($_REQUEST["estado"]) && @$_REQUEST["estado"] != '') $filtros["estado"] = $_REQUEST["estado"];
			if (isset($_REQUEST["fecharegistro"]) && @$_REQUEST["fecharegistro"] != '') $filtros["fecharegistro"] = $_REQUEST["fecharegistro"];
			if (isset($_REQUEST["idusuario"]) && @$_REQUEST["idusuario"] != '') $filtros["idusuario"] = $_REQUEST["idusuario"];

			$this->usuarioAct = NegSesion::getUsuario();
			if ($this->usuarioAct['idrol'] == 2) {
				$this->idalumno = $this->usuarioAct['idpersona'];
				$this->documento->stylesheet('tareas-docente', '/libs/othersLibs/fixing-styles/');
				$this->esquema = 'tareas/docente';
				$this->gruposauladetalle = $this->oNegAcad_grupoauladetalle->buscar(array('idproyecto' => $this->usuarioAct["idproyecto"], 'iddocente' => $this->usuarioAct['idpersona']));
				$this->misgrupos = array();
				$this->alumnosxgrupoaula = array();
				if (!empty($this->gruposauladetalle)) {
					foreach ($this->gruposauladetalle as $k => $v) {
						$v["idcomplementario"] = '0';
						if ($v["tipocurso"] == '2') {
							$filtros_ = array();
							$filtros_["complementario"] = true;
							$filtros_["idcursoprincipal"] = $v["idcurso"];
							$filtros_["idgrupoaula"] = $v["idgrupoaula"];
							// $filtros_["idcategoria"] = $v["idcategoria"];
							$vc_ = $this->oNegAcad_curso->buscarcursos($filtros_);							
							if (count($vc_) > 0) {
								$vc_ = $vc_[0];
								$v["idcomplementario"] = $vc_["idcurso"];
								$v["nombre"] = $vc_["nombre"];
							}
						}
						$this->gruposauladetalle[$k] = $v;						
						if (empty($this->alumnosxgrupoaula[$v["idgrupoauladetalle"]])) {
							$this->alumnosxgrupoaula[$v["idgrupoauladetalle"]] = $this->oNegAcad_matricula->buscar(array('idproyecto' => $this->usuarioAct["idproyecto"], 'idgrupoauladetalle' => $v["idgrupoauladetalle"]));
						}
						if (empty($this->misgrupos[$v["idgrupoaula"]])) {
							$this->misgrupos[$v["idgrupoaula"]] = array('nombre' => $v["strgrupoaula"], "datos" => array());
							$this->misgrupos[$v["idgrupoaula"]]["datos"][] = $v;
						} else $this->misgrupos[$v["idgrupoaula"]]["datos"][] = $v;
					}
				}
				// var_dump($this->misgrupos[26]["datos"]);
			} else if ($this->usuarioAct['idrol'] == 3) {
				$this->gruposauladetalle = $this->oNegAcad_matricula->buscar(array('idproyecto' => $this->usuarioAct["idproyecto"], 'idalumno' => $this->usuarioAct['idpersona']));				
				if (!empty($this->gruposauladetalle)) {
					$this->documento->stylesheet('mine-v2-tareas', '/libs/othersLibs/v2_tareas/');
					$this->esquema = 'tareas/alumno';
				} else {					
					$this->esquema = 'tareas/404';
				}				
			} else {
				$this->esquema = 'tareas/404solodocentes';
			}			
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tareas'), true);
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
