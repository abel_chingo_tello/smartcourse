<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-11-2016 
 * @copyright	Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulasvirtuales', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');
class WebAulavirtual extends JrWeb
{
	private $oNegAlumno;
	private $oNegPersonal;
	private $oNegAulasvirtuales;
	protected $oNegAulavirtualinvitados;

	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
		$this->oNegAlumno=new NegAlumno;
		$this->oNegAulasvirtuales = new NegAulasvirtuales;
		$this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;
	}
	public function defecto(){
		return $this->ver();
	}

	public function ver()
	{
		try{
			global $aplicacion;	
            $filtros=array();
            @extract($_REQUEST);
            if(empty($_REQUEST)){
            	$aplicacion->redir();
            }
            $filtros["aulaid"]=@$id;                        
            $this->aula=$this->oNegAulasvirtuales->buscar($filtros); 
            $this->userssmart=@$user;
            $this->emailsmart=@$email;
            $this->tiposmart=@$tipo;
            if(empty($user)||empty($email)||empty($tipo)){
            	throw new Exception('Smartclass '.JrTexto::_('data incorrect').'!!');
            	exit();
            }
            if(empty($this->aula)){
            	throw new Exception('Smartclass '.JrTexto::_('empty').'!!');
            	exit();
            }
            $aula=$this->aula=$this->aula[0];
            if(NegSesion::existeSesion()){
	            $this->usuario = NegSesion::getUsuario();
            }

            $fini=new DateTime($aula["fecha_inicio"]);
			$fini->modify('-20 minutes');
			$ffin=new DateTime($aula["fecha_final"]);
			$fnow=new DateTime("now");
			$msj='';
			$errorlogin=false;
			if($aula["estado"]=='CL'){
				throw new Exception('Smartclass '.JrTexto::_('closed').'!!');
				exit();
			}
			if($aula["estado"]=='BL'){
				throw new Exception('Smartclass '.JrTexto::_('blocked, no more participants').'!!');
				exit();
			}
			if($aula["estado"]=='0'&&$this->tiposmart=='M'){
				$this->oNegAulasvirtuales->setCampo($aula["aulaid"], 'estado', 'AB');
			}
			if($fini>=$fnow&&$fnow<$ffin){
				throw new Exception('Smartclass '.JrTexto::_('not yet active for user connection').'!!');
				exit();
			}else if($fnow>$ffin){
				throw new Exception('Smartclass '.JrTexto::_('expired').'!!');
				exit();
			}
            $rutanode='https://'.$_SERVER["HTTP_HOST"];
			$this->documento->stylesheet('emoji', '/libs/emoji/');
			//$this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
            //$this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('tinymce.min', '/libs/tinymce/');
            $this->documento->script('screenfull.min', '/libs/screenfull/');
            $this->documento->script('chartist.min', '/libs/chartist/');
            $this->documento->stylesheet('chartist.min', '/libs/chartist/');
            //$this->documento->script('fabric-brush.min', '/libs/pizarra/');
            $this->documento->script('html2canvas.min', '/libs/html2canvas/');
            /*$this->documento->stylesheet('select2.min', '/libs/select2/css/');
            $this->documento->script('select2.min', '/libs/select2/js/'); */
            //$this->documento->script('jquery-ui.min', '/tema/js/');
        	//$this->documento->stylesheet('jquery-ui.min', '/tema/css/');
        	$this->documento->script('pdf', '/libs/pizarra/');
        	$this->documento->script('pdf.worker', '/libs/pizarra/');
        	
        	//$this->documento->script('RecordRTC', '/libs/pizarra/');
            $this->documento->script('getScreenId', '/libs/pizarra/'); 
            $this->documento->script('socket.io', '/libs/pizarra/'); 	
        	$this->documento->script('RTCMultiConnection', '/libs/pizarra/');
            
        	//$this->documento->script('SSEConnection', '/libs/pizarra/');
        	$this->documento->script('screenshot', '/libs/pizarra/');
        	$this->documento->script('RecordRTC', '/libs/pizarra/');        	
            //$this->documento->script('WebSocketConnection', '/libs/pizarra/');
            $this->documento->script('movepdf', '/libs/pizarra/');
            $this->documento->script('emit', '/libs/pizarra/');
            $this->documento->script('pizarra', '/libs/pizarra/');
			$this->documento->plantilla ='aulavirtual/trasmitir';
			$this->documento->setTitulo('Smartclass', true);
			$this->esquema = 'aulavirtual/ver';
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			//$aplicacion->redir();
		}
	}

	public function requisitos(){
		try {
			global $aplicacion;
			$this->usuarioAct=null;
			//$this->documento->script('DetectRTC', '/libs/pizarra/');
			$filtros=array();
			if(NegSesion::existeSesion()){
				$this->usuarioAct = NegSesion::getUsuario();
				//$filtros["dni"]=@$this->usuarioAct["dni"];
			}
            $filtros["aulaid"]=@$_REQUEST["id"];
            $this->aula=$this->oNegAulasvirtuales->buscar($filtros);
            if(empty($this->aula)) throw new Exception('Smartclass '. JrTexto::_('does not exist').'!!');
			$this->documento->setTitulo(JrTexto::_('Smartclass'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'aulavirtual/general';
			$this->esquema = 'aulavirtual/requisitos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function json_verificarusuario(){
		try {
			$this->documento->plantilla = 'returnjson';
			global $aplicacion;
			if(empty($_REQUEST)){
				echo json_encode(array('code'=>'Error','msj'=>'Smartclass '.JrTexto::_('No data to send')));
				exit();
			}
			@extract($_REQUEST);
			if(!empty($usuario))
			$usuario=json_decode($usuario)[0];	
			$email=$usuario->email;
			$user=$usuario->usuario;
			$idaula=intval($idaula);
			if(empty($idaula)){
				echo json_encode(array('code'=>'Error','msj'=>'Smartclass '. JrTexto::_("empty")));				
				exit();
			}
			$usuarioAct=null;
			$filtros["aulaid"]=$idaula;
            $aula=$this->oNegAulasvirtuales->buscar($filtros);
            if(empty($aula[0])){
            	echo json_encode(array('code'=>'Error','msj'=>'Smartclass '.JrTexto::_('data incorrect')));
				exit();
            }
            $aula=$aula[0];
            $dni=$aula["dni"];
            $estado=$aula["estado"];
            $dirigidoa=$aula["dirigidoa"];
            if(NegSesion::existeSesion()){
				$usuarioAct = NegSesion::getUsuario();
				$dniuser=@$usuarioAct["dni"];
				$emailuser=@$usuarioAct["email"];
				if($dniuser==$dni){
					echo json_encode(array('code'=>'ok','msj'=>array('user'=>'miaula','estado'=>$estado,'como'=>'M')));
					exit();
				}
				$useraula=$this->oNegAulavirtualinvitados->buscar(array("idaula"=>$idaula,'email'=>$emailuser));
				if(!empty($useraula[0])){
					echo json_encode(array('code'=>'ok','msj'=>array('user'=>'miaula','estado'=>$estado,$useraula["como"])));
					exit();	
				}
			}			
			$useraula=$this->oNegAulavirtualinvitados->buscar(array("idaula"=>$idaula,'email'=>$email));
			if(!empty($useraula[0])){
				$useraula=$useraula[0];
				echo json_encode(array('code'=>'ok','msj'=>array('user'=>$user,'estado'=>$estado,'como'=>$useraula["como"])));
				exit();	
			}
			if($dirigidoa=='P'){
				echo json_encode(array('code'=>'ok','msj'=>array('user'=>$user,'estado'=>$estado,'como'=>'U')));
				exit();	
			}
			echo json_encode(array('code'=>'error','msj'=>'Smartclass '.JrTexto::_('user email has not been invited')));
			exit();	
            return parent::getEsquema();
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
			exit();			
		}
	}

	public function cargarmedia(){
		try {			
			$this->documento->plantilla = 'returnjson';
			if(empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incorrect')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$tipo="aulasvirtuales";			
			try{
				$newname=@$_POST["aula"].date("Ymdhis")."_".$file["name"];
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo,'0777');
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)) 
			  	{
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File upload success'),'namefile'=>$newname,'extension'=>$ext));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error loading file')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}
	}


	public function subirmediamedia(){
		try {			
			$this->documento->plantilla = 'returnjson';
			if(empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incorrect'),'aa'=>$_FILES["filearchivo"]));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$tipo="aulasvirtuales";			
			try{
				$newname=@$_POST["aula"]."_".date('Ymdhis').$ext;
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo,'0777');
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)) 
			  	{
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File upload success'),'namefile'=>$newname,'extension'=>$ext));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error loading file')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}
	}
}