<?php
defined('RUTA_BASE') or die();

class WebParche extends JrWeb
{
    public function __construct()
	{
		parent::__construct();		
    }
    public function defecto(){
        try{
			global $aplicacion;	

            $this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
            $this->esquema = 'parche/inicio';			
            return parent::getEsquema();
        }catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function extraerzip(){
        $this->documento->plantilla = "returnjson";
        $zip = null;
        try{
            $ejecutable = null;
            $file = (isset($_REQUEST['file'])) ? $ruta =$_REQUEST['file'] : null;
            if(is_null($file)){
                throw new Exception("Se requiere una ruta del archivo valida para la extraccion");
            }
            $zip = new ZipArchive;
            $ruta = substr($file,0,-4);
            //date('Y-m-d');
            $ruta_tmp = explode(DIRECTORY_SEPARATOR, $ruta);
            $ruta_tmp[count($ruta_tmp)-1] = date('Y-m-d')."_".end($ruta_tmp);
            $ruta = implode(DIRECTORY_SEPARATOR,$ruta_tmp);
            if ($zip->open($file) === TRUE) {
                $zip->extractTo($ruta);
                $zip->close();
                $name=substr($file,0,-4);      
                $ejecutable = $ruta.DIRECTORY_SEPARATOR."ejecutar.php";
            } else {
                throw new Exception("No se logro crear u / o abrir el directorio especificado...");
            }
            echo json_encode(array('code'=>'ok','data'=>$ejecutable));
            return parent::getEsquema();
        }catch(Exception $e){
            if(!is_null($zip)){
                $zip->close();
            }
            $data = array('code'=>'Error','mensaje' => $e->getMessage());
            echo json_encode($data);
        }
    }
    public function ejecutar(){
        $this->documento->plantilla = "returnjson";
        try{
            $file = (isset($_REQUEST['file'])) ? $_REQUEST['file'] : null;
            if(!is_null($file) && is_file($file)){
                $destiny = mb_strrchr($file,DIRECTORY_SEPARATOR);
                $ruta_externa = substr($file, 0 , -strlen($destiny));
                require_once($file);
            }
            echo json_encode(array('code'=>'ok','data'=>'ok'));
            return parent::getEsquema();
        }catch(Exception $e){
            $data = array('code'=>'Error','mensaje' => $e->getMessage());
            echo json_encode($data);
        }
    }
    public function save(){
		$this->documento->plantilla = "returnjson";
        try{
			global $aplicacion;
            $target_path = null;
            $dir = null;
            // var_dump($_FILES["archivo"]['tmp_name']);
            // foreach($_FILES["archivo"]['tmp_name'] as $key => $tmp_name)
            // {
                if($_FILES["archivo"]["name"]) {
                    $filename = $_FILES["archivo"]["name"]; //Obtenemos el nombre original del archivo
                    $source = $_FILES["archivo"]["tmp_name"]; //Obtenemos un nombre temporal del archivo
                    
                    $directorio = realpath(getcwd()."/sys_temp"); //Declaramos un  variable con la ruta donde guardaremos los archivos
                    
                    if(!file_exists($directorio) && $directorio !== false){
                        // mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");	
                        throw new Exception("Error No existe el directorio RUTA_BASE/system ");
                    }
                    
                    $dir=opendir($directorio); //Abrimos el directorio de destino
                    $target_path = $directorio.DIRECTORY_SEPARATOR.$filename; //Indicamos la ruta de destino, así como el nombre del archivo
                    
                    //Movemos y validamos que el archivo se haya cargado correctamente
                    if(!move_uploaded_file($source, $target_path)) {	
                        throw new Exception("Ha ocurrido un error, al momento de mover (subir) el archivo por favor, inténtelo de nuevo.");
                    }
                    closedir($dir); //Cerramos el directorio de destino
                }
            // }
            echo json_encode(array('code'=>'ok','data'=>$target_path));
            return parent::getEsquema();
        }catch(Exception $e){
            if(!is_null($dir)){
                closedir($dir);
            }
            $data = array('code'=>'Error','mensaje' => $e->getMessage());
            echo json_encode($data);
        }
    }
}
?>