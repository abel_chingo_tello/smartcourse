<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016 
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTeacherresources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');

class WebTeacherresources extends JrWeb
{
	private $oNegTeacherresource;
	private $oNegActividad;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTeacherresources = new NegTeacherresources;
		$this->oNegActividad = new NegActividad;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
						
			//$this->datos=$this->oNegVocabulario->buscar();
			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Vocabulario'), true);
			$this->esquema = 'vocabulario-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}


	/*public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Vocabulario', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Vocabulario').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Vocabulario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegVocabulario->idvocabulario = @$_GET['id'];
			$this->datos = $this->oNegVocabulario->dataVocabulario;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Vocabulario').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegVocabulario->idvocabulario = @$_GET['id'];
			$this->datos = $this->oNegVocabulario->dataVocabulario;
				
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Vocabulario').' /'.JrTexto::_('see'), true);
			$this->esquema = 'vocabulario-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;				

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'vocabulario-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}*/


	// ========================== Funciones xajax ========================== //
	public function xSaveTeacherresources(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				$rutabase=$this->documento->getUrlBase();				 
				$texto=@str_replace($rutabase,'__xRUTABASEx__',trim(@$frm["texto"]));
				if(empty($texto)) return;
				$usuarioAct = NegSesion::getUsuario();
				$this->oNegTeacherresources->__set('idnivel',@$frm['idNivel']);
				$this->oNegTeacherresources->__set('idunidad',@$frm['idUnidad']);
				$this->oNegTeacherresources->__set('idactividad',@$frm['idActividad']);
				$this->oNegTeacherresources->__set('idpersonal',@$usuarioAct["dni"]);
				$this->oNegTeacherresources->__set('texto',$texto);
				$this->oNegTeacherresources->__set('orden',($usuarioAct["rol"]==1?0:1));				
				$res=$this->oNegTeacherresources->agregar();
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTeacherresources->idtool);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	/*public function xGetxIDVocabulario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegVocabulario->__set('idvocabulario', $pk);
				$this->datos = $this->oNegVocabulario->dataVocabulario;
				$res=$this->oNegVocabulario->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}*/
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTeacherresource->__set('idtool', $pk);
				$res=$this->oNegTeacherresource->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}    
}