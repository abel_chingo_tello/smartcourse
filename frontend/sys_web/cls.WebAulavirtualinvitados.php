<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-05-2017 
 * @copyright	Copyright (C) 24-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulasvirtuales', RUTA_BASE, 'sys_negocio');

class WebAulavirtualinvitados extends JrWeb
{
	private $oNegAulavirtualinvitados;
	private $oNegAulasvirtuales;
	public function __construct()
	{
		parent::__construct();
		$this->oNegAulasvirtuales = new NegAulasvirtuales;	
		$this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Aulavirtualinvitados', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$this->datos=$this->oNegAulavirtualinvitados->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Aulavirtualinvitados'), true);
			$this->esquema = 'aulavirtualinvitados-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function mostrar(){ //listado de participantes
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('SmartClass Participants'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'aulavirtual/participants';
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!=''){
				$filtros=array();
            	$filtros["aulaid"]=@$_REQUEST["id"];            
            	$aula=$this->oNegAulasvirtuales->buscar($filtros);            	
				/*$this->oNegAlumno->dni=$_REQUEST["id"];
				$this->datos=$this->oNegAlumno->dataAlumno;*/
				if(empty($aula)){
					throw new Exception(JrTexto::_('SmartClass does not exist or is not selected').'!!');	
					return;
				}
				if(!empty($aula[0])){
					$this->aula=$aula[0];					
					$this->fini=new DateTime($this->aula["fecha_inicio"]);
					$this->ffin=new DateTime($this->aula["fecha_final"]);
					$this->fnow=new DateTime("now");
					$this->participantes=$this->oNegAulavirtualinvitados->buscar(array('idaula'=>$this->aula["aulaid"]));

				}
			}else{
				throw new Exception(JrTexto::_('SmartClass does not exist').'!!');	
				return;
			}			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Aulavirtualinvitados', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Aulavirtualinvitados').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Aulavirtualinvitados', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegAulavirtualinvitados->idinvitado = @$_GET['id'];
			$this->datos = $this->oNegAulavirtualinvitados->dataAulavirtualinvitados;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Aulavirtualinvitados').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegAulavirtualinvitados->idinvitado = @$_GET['id'];
			$this->datos = $this->oNegAulavirtualinvitados->dataAulavirtualinvitados;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Aulavirtualinvitados').' /'.JrTexto::_('see'), true);
			$this->esquema = 'aulavirtualinvitados-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'aulavirtualinvitados-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function json_guardar()
	{
		try {
			$this->documento->plantilla = 'returnjson';
			global $aplicacion;
			if(empty($_REQUEST)){
				echo json_encode(array('code'=>'Error','msj'=>"Smartclass".JrTexto::_("data incorrect")));				
				exit();
			}
			@extract($_REQUEST);
			if(empty($idaula)){
				echo json_encode(array('code'=>'Error','msj'=>"Smartclass ".JrTexto::_("empty")));				
				exit();
			}
			$aula=$this->oNegAulasvirtuales->buscar(array('aulaid'=>$idaula));
			if(empty($aula)){
				echo json_encode(array('code'=>'Error','msj'=>"Smartclass ".JrTexto::_("does not exist")));				
				exit();				
			}else $aula=$aula[0];
			if(!empty($aula)){
				$aula=$aula;					
				$this->fini=new DateTime($aula["fecha_inicio"]);
				$this->ffin=new DateTime($aula["fecha_final"]);
				$this->fnow=new DateTime("now");
				$this->participantes=$this->oNegAulavirtualinvitados->buscar(array('idaula'=>$idaula));
			}
			$emails=json_decode($paraemail,true);
			if(!empty($emails)){					
				foreach ($emails as $user){					
					$this->oNegAulavirtualinvitados->__set('idaula',$idaula);
					$this->oNegAulavirtualinvitados->__set('dni','0');
					$this->oNegAulavirtualinvitados->__set('email',@$user["email"]);
					$this->oNegAulavirtualinvitados->__set('asistio','0');
					$this->oNegAulavirtualinvitados->__set('como','U');
					$nombre=!empty($user["nombre"])?$user["nombre"]:@$user["name"];
					$this->oNegAulavirtualinvitados->__set('usuario',@$nombre);
					$this->oNegAulavirtualinvitados->agregar();					 
				}
				echo json_encode(array('code'=>'ok','msj'=>"Invitados Guardados"));
        		exit();
			}else{
				echo json_encode(array('code'=>'Error','msj'=>"Mensaje de correo, sin destinatario"));
        		exit();
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
        	exit();
			//return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function json_eliminar(){
		try {
			$this->documento->plantilla = 'returnjson';
			global $aplicacion;
			if(empty($_REQUEST)){
				echo json_encode(array('code'=>'Error','msj'=>"No esisten datos a enviar"));				
				exit();
			}
			@extract($_REQUEST);
			if(empty($idaula)){
				echo json_encode(array('code'=>'Error','msj'=>"SmartClass empty"));				
				exit();
			}
			$aula=$this->oNegAulasvirtuales->buscar(array('aulaid'=>intval($idaula)));
			if(empty($aula)){
				echo json_encode(array('code'=>'Error','msj'=>"SmartClass does not exist or is not selected"));				
				exit();				
			}else $aula=$aula[0];
			if(!empty($aula)){
				$aula=$aula;
				$filtros=array();
				$filtros['idaula']=intval($aula["aulaid"]);
				if(!empty($email)) $filtros["email"]=strval($email);
				$res=$this->oNegAulavirtualinvitados->eliminarxfiltro($filtros);
				echo json_encode(array('code'=>'ok','msj'=>"SmartClass remove invited ok"));				
				exit();
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
        	exit();			
		}
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveAulavirtualinvitados(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdinvitado'])) {
					$this->oNegAulavirtualinvitados->idinvitado = $frm['pkIdinvitado'];
				}
				
					$this->oNegAulavirtualinvitados->__set('idaula',@$frm["txtIdaula"]);
					$this->oNegAulavirtualinvitados->__set('dni',@$frm["txtDni"]);
					$this->oNegAulavirtualinvitados->__set('email',@$frm["txtEmail"]);
					$this->oNegAulavirtualinvitados->__set('asistio',@$frm["txtAsistio"]);
					$this->oNegAulavirtualinvitados->__set('como',@$frm["txtComo"]);
					$this->oNegAulavirtualinvitados->__set('usuario',@$frm["txtUsuario"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAulavirtualinvitados->agregar();
					}else{
									    $res=$this->oNegAulavirtualinvitados->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAulavirtualinvitados->idinvitado);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAulavirtualinvitados(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAulavirtualinvitados->__set('idinvitado', $pk);
				$this->datos = $this->oNegAulavirtualinvitados->dataAulavirtualinvitados;
				$res=$this->oNegAulavirtualinvitados->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAulavirtualinvitados->__set('idinvitado', $pk);
				$res=$this->oNegAulavirtualinvitados->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}