<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-01-2018 
 * @copyright	Copyright (C) 19-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_marcacion', RUTA_BASE, 'sys_negocio');
class WebAcad_marcacion extends JrWeb
{
	private $oNegAcad_marcacion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_marcacion = new NegAcad_marcacion;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_marcacion', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idmarcacion"])&&@$_REQUEST["idmarcacion"]!='')$filtros["idmarcacion"]=$_REQUEST["idmarcacion"];
			if(isset($_REQUEST["idgrupodetalle"])&&@$_REQUEST["idgrupodetalle"]!='')$filtros["idgrupodetalle"]=$_REQUEST["idgrupodetalle"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idhorario"])&&@$_REQUEST["idhorario"]!='')$filtros["idhorario"]=$_REQUEST["idhorario"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["horaingreso"])&&@$_REQUEST["horaingreso"]!='')$filtros["horaingreso"]=$_REQUEST["horaingreso"];
			if(isset($_REQUEST["horasalida"])&&@$_REQUEST["horasalida"]!='')$filtros["horasalida"]=$_REQUEST["horasalida"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["nalumnos"])&&@$_REQUEST["nalumnos"]!='')$filtros["nalumnos"]=$_REQUEST["nalumnos"];
			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"];
			
			$this->datos=$this->oNegAcad_marcacion->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Marking'), true);
			$this->esquema = 'acad_marcacion-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_marcacion', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Marking').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_marcacion', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_marcacion->idmarcacion = @$_GET['id'];
			$this->datos = $this->oNegAcad_marcacion->dataAcad_marcacion;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Marking').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_marcacion-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_marcacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idmarcacion"])&&@$_REQUEST["idmarcacion"]!='')$filtros["idmarcacion"]=$_REQUEST["idmarcacion"];
			if(isset($_REQUEST["idgrupodetalle"])&&@$_REQUEST["idgrupodetalle"]!='')$filtros["idgrupodetalle"]=$_REQUEST["idgrupodetalle"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idhorario"])&&@$_REQUEST["idhorario"]!='')$filtros["idhorario"]=$_REQUEST["idhorario"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["horaingreso"])&&@$_REQUEST["horaingreso"]!='')$filtros["horaingreso"]=$_REQUEST["horaingreso"];
			if(isset($_REQUEST["horasalida"])&&@$_REQUEST["horasalida"]!='')$filtros["horasalida"]=$_REQUEST["horasalida"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["nalumnos"])&&@$_REQUEST["nalumnos"]!='')$filtros["nalumnos"]=$_REQUEST["nalumnos"];
			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"];
						
			$this->datos=$this->oNegAcad_marcacion->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarmarcacion(){
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idmarcacion)) {
				$this->oNegAcad_marcacion->idmarcacion = $idmarcacion;
				$accion='_edit';
			}           	
			$this->oNegAcad_marcacion->idgrupodetalle=@$idgrupodetalle;
			$this->oNegAcad_marcacion->idcurso=@$idcurso;
			$this->oNegAcad_marcacion->idhorario=@$idhorario;
			$this->oNegAcad_marcacion->fecha=!empty($fecha)?$fecha:date('Y-m-d');
			$this->oNegAcad_marcacion->horasalida=!empty($horasalida)?$horasalida:'00:00:00';
			$this->oNegAcad_marcacion->iddocente=@$iddocente;
			$this->oNegAcad_marcacion->nalumnos=!empty($nalumnos)?$nalumnos:0;
			$this->oNegAcad_marcacion->observacion=!empty($observacion)?$observacion:'';					
            if($accion=='_add') {
            	$this->oNegAcad_marcacion->horaingreso=@$horaingreso;
            	$res=$this->oNegAcad_marcacion->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Marking')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_marcacion->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Marking')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAcad_marcacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdmarcacion'])) {
					$this->oNegAcad_marcacion->idmarcacion = $frm['pkIdmarcacion'];
				}
				
				$this->oNegAcad_marcacion->idgrupodetalle=@$frm["txtIdgrupodetalle"];
					$this->oNegAcad_marcacion->idcurso=@$frm["txtIdcurso"];
					$this->oNegAcad_marcacion->idhorario=@$frm["txtIdhorario"];
					$this->oNegAcad_marcacion->fecha=@$frm["txtFecha"];
					$this->oNegAcad_marcacion->horaingreso=@$frm["txtHoraingreso"];
					$this->oNegAcad_marcacion->horasalida=@$frm["txtHorasalida"];
					$this->oNegAcad_marcacion->iddocente=@$frm["txtIddocente"];
					$this->oNegAcad_marcacion->nalumnos=@$frm["txtNalumnos"];
					$this->oNegAcad_marcacion->observacion=@$frm["txtObservacion"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAcad_marcacion->agregar();
					}else{
									    $res=$this->oNegAcad_marcacion->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAcad_marcacion->idmarcacion);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAcad_marcacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_marcacion->__set('idmarcacion', $pk);
				$this->datos = $this->oNegAcad_marcacion->dataAcad_marcacion;
				$res=$this->oNegAcad_marcacion->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_marcacion->__set('idmarcacion', $pk);
				$res=$this->oNegAcad_marcacion->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAcad_marcacion->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}