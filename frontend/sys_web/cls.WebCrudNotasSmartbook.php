<?php

defined('RUTA_BASE') or die();
class WebCrudNotasSmartbook extends JrWeb
{

	public function __construct()
	{
		parent::__construct();
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Evento_programacion', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->script('tinymce.min', '/libs/tinymce54/');
			$this->documento->script('encode', '/libs/chingo/');
			$this->documento->script('jquery-ui.min', '/tema/js/');
			$this->documento->script('jquery.ui.touch-punch.min', '/libs/jquery-ui-touch/');
			$this->documento->stylesheet('jquery-ui.min', '/tema/css/');

			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->script('jquery.maskedinput.min', '/tema/js/');
			$this->documento->script('cronometro', '/libs/chingo/');
			/*  $this->documento->script('editactividad', '/libs/smartenglish/');
			$this->documento->script('actividad_completar', '/libs/smartenglish/');
			$this->documento->script('actividad_ordenar', '/libs/smartenglish/');
			$this->documento->script('actividad_imgpuntos', '/libs/smartenglish/');             
			$this->documento->script('actividad_fichas', '/libs/smartenglish/');
			$this->documento->script('actividad_dialogo', '/libs/smartenglish/');
			$this->documento->script('manejadores_dby', '/libs/smartenglish/');                        
			$this->documento->script('manejadores_practice', '/libs/smartenglish/');*/
			$this->documento->script('audioRecorder', '/libs/speach/');
			$this->documento->script('wavesurfer.min', '/libs/audiorecord/');
			$this->documento->script('callbackManager', '/libs/speach/');
			$this->documento->script('jquery.md5', '/tema/js/');
			$this->documento->script('actividad_verdad_falso', '/libs/smartenglish/');
			$this->documento->script('actividad_dialogo', '/libs/smartenglish/');
			$this->documento->script('plantillas', '/libs/smartenglish/');
			//$this->documento->script('', .'js/chingo/cronometro.js');

			$this->documento->stylesheet('speach', '/libs/smartenglish/');
			$this->documento->script('speach', '/libs/smartenglish/');
			$this->documento->stylesheet('actividad_nlsw', '/libs/smartenglish/');
			$this->documento->script('actividad_nlsw', '/libs/smartenglish/');
			//$this->documento->script('completar','/libs/smartenglish/'); 
			$this->documento->script('tools_games', '/libs/smartenglish/');

			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->stylesheet('estilo', '/libs/crusigrama/');
			$this->documento->script('crossword', '/libs/crusigrama/');
			$this->documento->script('micrusigrama', '/libs/crusigrama/');
			$this->documento->script('snap-puzzle', '/libs/rompecabezas/');
			$this->documento->script('wordfind', '/libs/sopaletras/js/');
			$this->documento->script('wordfind.game', '/libs/sopaletras/js/');
			$this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
			$this->documento->script('pronunciacion', '/js/');
			$this->documento->script('scorm1', '/js/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');

			//mine
			$this->documento->script('audioRecorder', '/libs/speach/');
			$this->documento->stylesheet('speach', '/libs/smartenglish/');
			$this->documento->script('speach', '/libs/smartenglish/');
			$this->documento->script('actividad_nlsw', '/libs/smartenglish/');
			$this->documento->script('callbackManager', '/libs/speach/');
			$this->documento->stylesheet('actividad_nlsw', '/libs/smartenglish/');
			$this->documento->script('actividad_nlsw', '/libs/smartenglish/');
			$this->documento->script('', '/static/libs/smartenglish/plantillas.js?id=' . rand(0, 1000));
			$this->documento->script('wavesurfer.min', '/libs/audiorecord/');
			$this->documento->script('tinymce.min', '/libs/tinymce54/');
			//mine

			$this->url_ADULTOS = URL_SEAdultos;
			$this->url_ADOLESCENTES = URL_SEAdolecentes;
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine2', '/libs/othersLibs/mine/js/');

			$this->documento->stylesheet('mine-crudNotasSmartbook', '/libs/othersLibs/crudNotasSmartbook/');
			$this->documento->script('mine-crudNotasSmartbook', '/libs/othersLibs/crudNotasSmartbook/');
			// $this->documento->stylesheet('mine-reporte_notas', '/libs/othersLibs/reporte_notas/');
			$oUsuario = NegSesion::getUsuario();
			$this->usuario = $oUsuario;
			$this->esquema = 'crudNotasSmartbook/crudNotasSmartbook';

			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas'), true);
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
