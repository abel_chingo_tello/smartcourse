<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Ugel'">&nbsp;<?php echo JrTexto::_('Ugel'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<style type="text/css">
  .btn_removetr{cursor: pointer;}
</style>
<div class="row"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>">
  <div class="col-md-12 col-sm-12 col-xs-12 text-center" id="pnlbotonesde<?php echo $idgui; ?>">
    <div class="panel">
      <div class="panel-body">
        <div id="msj-interno"></div><div class="alert alert-info" role="alert"><strong>Paso 01</strong> Descargue plantilla excel para subir datos.</div>
        <a class="btn btn-primary" href="<?php echo $this->documento->getUrlStatic();?>/importar_xls/ugel.xlsx" download ><i class="fa fa-cloud-download"></i> <?php echo JrTexto::_('Download')?></a>
        <hr>
        <div class="alert alert-info" role="alert"><strong>Paso 02</strong> Suba plantilla excel llenada para importar.</div>
        <a class="btn btn-primary btnimport<?php echo $idgui; ?>" href="javascript:void(0)" ><i class="fa fa-cloud-upload"></i> <?php echo JrTexto::_('Upload')?></a>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 hide" id="paneldatosaimportar<?php echo $idgui; ?>" style="display: none;" ></div>
  <div id="datosimport<?php echo $idgui; ?>" class="col-md-12" style="display: none;">
    <div class="col table-responsive table-responsive-sm">
    <table class="formventana table table-striped table-hover tableimport<?php echo $idgui; ?>" idgui="<?php echo $idgui;?>">
      <thead>
      <tr class="tr01 headings">
          <th><?php echo JrTexto::_("Id");?></th>
          <th><?php echo JrTexto::_("descripcion");?></th>
          <th><?php echo JrTexto::_("abrev");?></th>
          <th><?php echo ucfirst(JrTexto::_("ubigeo - dre")); ?></th>
          <th><?php echo ucfirst(JrTexto::_("nombre - dre")); ?></th>
          <th><?php echo ucfirst(JrTexto::_("idprovincia")); ?></th>
          <th><?php echo ucfirst(JrTexto::_('Actions')); ?></th>        
        </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <div class="col-md-12 text-center"><hr>
      <a href="javascript:void(0)" class="btncancel btn btn-warning"><i class="fa fa-save"></i> <?php echo JrTexto::_('Cancel') ?></a>
      <a href="javascript:void(0)" class="btnsaveimport<?php echo $idgui; ?> btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Import') ?></a>
    </div>
  </div>
  <span id="contenfile" style="display: none;"><input type="file" name="importjson" id="importjson" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"></span>
</div>
<script id="cargaimportar" src="<?php echo $this->documento->getUrlStatic();?>/libs/importexcel/xlsx.full.min.js"></script>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm_ugel.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var ven=$('#vent-<?php echo $idgui; ?>');
    if(!ven.hasClass('ismodaljs')) frm_ugelimport($('#vent-<?php echo $idgui; ?>'));
  })
</script>