<?php
defined('RUTA_BASE') or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla != 'modal' ? false : true;
if (!empty($this->datos)) $frm = $this->datos;
$ventanapadre = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : 'eeeexzx-1';
$_imgdefecto = "static/media/nofoto.jpg";
if (!$ismodal) { ?>
  <div class="row" id="breadcrumb">
    <div class="col">
      <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio(); ?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio(); ?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio(); ?>/acad_competencias">&nbsp;<?php echo JrTexto::_('Competencias'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion); ?></li>
      </ol>
    </div>
  </div>
<?php } ?>
<div class="row ventana" id="vent-<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
  <div class="col">
    <div id="msj-interno"></div>
    <form method="post" id="frm-<?php echo $idgui; ?>" tb="acad_competencias" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui; ?>">
      <input type="hidden" name="idcompetencia" id="idcompetencia" value="<?php echo $this->pk; ?>">
      <div class="form-group">
        <label class="control-label"><?php echo JrTexto::_('Carrera'); ?> <span class="required"> * </span></label>
        <div class="">
          <select id="txtidCurso" name="idcurso" class="form-control" required>
            <option value=""><?php JrTexto::_("Seleccione"); ?></option>
            <option value="1"><?php echo JrTexto::_('Activo'); ?></option>
            <option value="0"><?php echo JrTexto::_('Inactivo'); ?></option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label"><?php echo JrTexto::_('Categoria'); ?> <span class="required"> * </span></label>
        <div class="">
          <select id="txtidcategoria" name="idcategoria" class="form-control" required>
            <option value=""><?php JrTexto::_("Seleccione"); ?></option>
            <option value="1"><?php echo JrTexto::_('Activo'); ?></option>
            <option value="0"><?php echo JrTexto::_('Inactivo'); ?></option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label"><?php echo JrTexto::_('Nombre'); ?> <span class="required"> * </span></label>
        <div class="">
          <textarea id="txtNombre" name="nombre" class="form-control"><?php echo @trim($frm["nombre"]); ?></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label"><?php echo JrTexto::_('Estado'); ?> <span class="required"> * </span></label>
        <div class="">
          <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"] == 1 ? "fa-check-circle" : "fa-circle"; ?>" data-value="<?php echo @$frm["estado"]; ?>" data-valueno="0" data-value2="<?php echo @$frm["estado"] == 1 ? 1 : 0; ?>">
            <span> <?php echo JrTexto::_(@$frm["estado"] == 1 ? "Activo" : "Inactivo"); ?></span>
            <input type="hidden" name="estado" value="<?php echo !empty($frm["estado"]) ? $frm["estado"] : 0; ?>">
          </a>

        </div>
      </div>
      <hr>
      <div class="col-md-12 form-group text-center">
        <button id="btn-saveAcad_competencias" type="submit" tb="acad_competencias" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save'); ?> </button>
        <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('acad_competencias')) ?>" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel'); ?></a>
      </div>
    </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic() . '/js/web/frm.js' ?>"></script>