<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/bitacora_alumno_smartbook">&nbsp;<?php echo JrTexto::_('Bitacora_alumno_smartbook'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="bitacora_alumno_smartbook" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idbitacora_smartbook" id="idbitacora_smartbook" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idcurso');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdcurso" name="idcurso" required="required" class="form-control" value="<?php echo @$frm["idcurso"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idsesion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdsesion" name="idsesion" required="required" class="form-control" value="<?php echo @$frm["idsesion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idusuario');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdusuario" name="idusuario" required="required" class="form-control" value="<?php echo @$frm["idusuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtEstado" name="estado" required="required" class="form-control" value="<?php echo @$frm["estado"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Regfecha');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtRegfecha" name="regfecha" required="required" class="form-control" value="<?php echo @$frm["regfecha"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Oldid');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtOldid" name="oldid" required="required" class="form-control" value="<?php echo @$frm["oldid"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Oldido');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtOldido" name="oldido" required="required" class="form-control" value="<?php echo @$frm["oldido"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveBitacora_alumno_smartbook" type="submit" tb="bitacora_alumno_smartbook" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('bitacora_alumno_smartbook'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/bitacora_alumno_smartbook/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/bitacora_alumno_smartbook');
                }
              }
          });
       }
  })
})
</script>

