<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$metas=!empty($this->metas[0])?$this->metas[0]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<div class="col-md-12">
<div class="panel panel-body" id="pnlb_<?php echo $idgui; ?>">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" class="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"] ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
    <input type="hidden" name="accionmet" id="accionmet<?php echo $idgui; ?>" value="add">
    <input type="hidden" name="idmeta" id="idmeta<?php echo $idgui; ?>" value="<?php echo @$metas["idmeta"] ?>">
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-12">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('My goals')); ?></label>
            <textarea name="meta" class="form-control" id="meta<?php echo $idgui; ?>"><?php echo @$metas["meta"] ?></textarea> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('My objectives')); ?></label>
            <textarea name="objetivo" class="form-control" id="objetivo<?php echo $idgui; ?>"><?php echo @$metas["objetivo"] ?></textarea> 
          </div>  
        </div>
      </div>
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a-->
        <button type="submit" class="btn btn-primary btnsave<?php echo $idgui ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
  $("#pnla_<?php echo $idgui; ?>").on('click','.btneliminarmeta',function(ev){
    var id=$(this).attr('data-id');
    var res = xajax__('', 'persona_metas', 'eliminar', id);
    if(usermet[id]!=undefined) delete usermet[id];
    $(this).closest('.item-user').remove();    
  });
  $('#frm<?php echo $idgui;?>').bind({ 
    keyup:function(ev){
      if(ev.which == 13) return false;
    },submit: function(event){
      var idguitmp='<?php echo $idgui; ?>';
      var myForm = document.getElementById('frm<?php echo $idgui; ?>'); 
      var formData = new FormData(myForm);
      var accion=$('#accionmet'+idguitmp).val();
      formData.append('accion',accion);
      formData.append('idpersona', $('#idpersona'+idguitmp).val()); 
      formData.append('idmeta', $('#idmeta'+idguitmp).val()); 
      formData.append('plt','blanco');
      var url=_sysUrlBase_+'/persona_metas/guardarPersona_metas';     
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        cache: false,
        dataType:'json',
        beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
        success: function(data){  
          if(data.code=='Error'){
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
            $('#idmeta'+idguitmp).val(data.newid);
          }
        },
        error: function(e){ console.log(e); },
        complete: function(xhr){ }
      });
      return false;
    }
  });
});
</script>