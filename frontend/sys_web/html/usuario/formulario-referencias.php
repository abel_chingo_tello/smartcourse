<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$referencias=$this->referencias;
//var_dump($frm);
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
  .item-user{
    padding: 1ex;
    position: relative;
  }
  .pnlacciones{
    position: absolute;
    bottom: 0px;
    text-align: center;
    width: 100%;
    background: #469de8;
    /*margin: -11px;*/
    padding: 5px;
    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
  }
  @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
  }
  .pnlacciones a{
    margin: 0.25ex;
    width: 30px;
  }
  .panel-user .pnlacciones{
    display: none;    
  }
  .panel-user.active .pnlacciones{
    display: block;
    top:auto !important;
  }
</style>
<div class="col-md-12">
  <div class="panel panel-body" style="display: block" id="pnla_<?php echo $idgui; ?>">
    <div id="addaqui<?php echo $idgui; ?>">
      <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-userclone<?php echo $idgui; ?>" style="display: none;" >
        <div class="panel-user">
        <div class="row">       
          <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo "><i class="nombreclone<?php echo $idgui; ?>"></i><br>
            <small class="cargoclone<?php echo $idgui; ?>"></small>
          </h4>
          <div class="col-xs-12 texto_info "><strong><?php echo JrTexto::_('Relationship'); ?> :</strong>
            <ul class="col-xs-12 list-unstyled informacion">
              <li class="col-xs-12">
                <div class="col-xs-2 icono "><i class="fa fa-handshake-o"></i></div>
                <div class="col-xs-10 relacionclone<?php echo$idgui; ?> "> </div>
              </li>           
            </ul>
          </div> 
          <div class="col-xs-12 texto_info"><strong><?php echo JrTexto::_('Contact'); ?> :</strong>
            <ul class="col-xs-12 list-unstyled informacion">
              <li class="col-xs-12">
                <div class="col-xs-2 icono "><i class="fa fa-at"></i></div>
                <div class="col-xs-10 texto_info correoclone<?php echo $idgui; ?>"></div>
              </li>
              <li class="col-xs-12">
                <div class="col-xs-2 icono "><i class="fa fa-phone"></i></div>
                <div class="col-xs-10 texto_info telefonoclone<?php echo $idgui; ?>"></div>
              </li>
            </ul>
          </div>        
        </div>
        </div>
        <div class="pnlacciones text-center">
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/verficha/?id=<?php echo $ref["ndoc"];?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"> <i class="fa fa-user"></i><i class="fa fa-eye"></i></a-->
          <a class="btn btn-warning btn-xs btneditarreferencia" data-modal="si" href="javascript:void(0);"  data-id="0" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horario/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a-->
          <a class="btneliminarreferencia btn btn-danger btn-xs" href="javascript:void(0);" data-id="0" data-titulo="<?php echo JrTexto::_('Delete'); ?>"><i class="fa fa-trash-o"></i></a>
        </div>
      </div>
 <?php 
$xuserref=array();
if(!empty($referencias))
  foreach ($referencias as $ref){
    $url=$this->documento->getUrlBase();
    $xuserref[$ref["idreferencia"]]=$ref;
    $fullnombre=ucfirst($ref["nombre"]); ?>
  <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-user<?php echo $ref["idreferencia"]; ?>" >
    <div class="panel-user">
      <div class="row">       
        <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo "><i class="nombre<?php echo $ref["idreferencia"]; ?>"><?php echo $fullnombre; ?></i><br>
            <small class="cargo<?php echo $ref["idreferencia"]; ?>"><?php echo $ref["cargo"]; ?></small></h4>
        <div class="col-xs-12 texto_info "><strong><?php echo JrTexto::_('Relationship'); ?> :</strong>
          <ul class="col-xs-12 list-unstyled informacion">
            <li class="col-xs-12">
              <div class="col-xs-2 icono "><i class="fa fa-handshake-o"></i></div>
              <div class="col-xs-10 relacion<?php echo $ref["idreferencia"]; ?> "> <?php echo $ref["relacion"]; ?></div>
            </li>           
          </ul>
        </div> 
        <div class="col-xs-12 texto_info"><strong><?php echo JrTexto::_('Contact'); ?> :</strong>
          <ul class="col-xs-12 list-unstyled informacion">
            <li class="col-xs-12">
              <div class="col-xs-2 icono "><i class="fa fa-at"></i></div>
              <div class="col-xs-10 texto_info correo<?php echo $ref["idreferencia"]; ?>"><?php echo $ref["correo"] ?></div>
            </li>
            <li class="col-xs-12">
              <div class="col-xs-2 icono "><i class="fa fa-phone"></i></div>
              <div class="col-xs-10 texto_info telefono<?php echo $ref["idreferencia"]; ?>"><?php echo $ref["telefono"] ?></div>
            </li>
          </ul>
        </div>        
      </div>
      <div class="pnlacciones text-center">
        <a class="btn btn-warning btn-xs btneditarreferencia" data-modal="si" href="javascript:void(0);"  data-id="<?php echo $ref["idreferencia"];?>" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>        
        <a class="btneliminarreferencia btn btn-danger btn-xs" href="javascript:void(0);" data-id="<?php echo $ref["idreferencia"];?>" data-titulo="<?php echo JrTexto::_('delete'); ?>"><i class="fa fa-trash-o"></i></a>
      </div>
  </div>
</div>
<?php } ?>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center">
  <!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a-->
  <button class="btn btn-warning btnaddref<?php echo $idgui ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('Add'));?></button></div>  
</div>
</div>
<div class="col-md-12">
<div class="panel panel-body" style="display: none;" id="pnlb_<?php echo $idgui; ?>">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" class="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"] ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
    <input type="hidden" name="accionref" id="accionref<?php echo $idgui; ?>" value="add">
    <input type="hidden" name="idreferencia" id="idreferencia<?php echo $idgui; ?>" value="">
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-12">        
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
              <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Name')); ?></label>                    
                <input type="text" class="form-control" name="nombre" id="nombre<?php echo $idgui; ?>" value="" placeholder="" required="required" > 
            </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Position')); ?></label>                    
              <input type="text" class="form-control" name="cargo" id="cargo<?php echo $idgui; ?>" value="" placeholder="" required="required" > 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Relationship')); ?></label>                    
              <input type="text" class="form-control" name="relacion" id="relacion<?php echo $idgui; ?>" value="" placeholder="" required="required" > 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('E-mail')); ?></label>                    
              <input type="text" class="form-control" name="correo" id="correo<?php echo $idgui; ?>" value="" placeholder="" required="required" > 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Telephone')); ?></label>                    
              <input type="text" class="form-control" name="telefono" id="telefono<?php echo $idgui; ?>" value="" placeholder="" required="required" > 
          </div>  
        </div>
      </div>      
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a href="javascript:void(0);" class="btn btn-danger btncancel<?php echo $idgui ?>"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Cancel');?></a>
        <button type="submit" class="btn btn-primary btnsave<?php echo $idgui ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
  var userref=<?php echo !empty($xuserref)?json_encode($xuserref):'[]' ?>;
  $('.cls<?php echo $idgui ?>').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  }).on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  });

 
  $(".btnaddref<?php echo $idgui ?>").click(function(ev){
    $('#accionref<?php echo $idgui; ?>').val('add');
    $('#idreferencia<?php echo $idgui; ?>').val('');    
    $('#nombre<?php echo $idgui; ?>').val('');
    $('#cargo<?php echo $idgui; ?>').val('');
    $('#relacion<?php echo $idgui; ?>').val(''); 
    $('#telefono<?php echo $idgui; ?>').val('');
    $('#correo<?php echo $idgui; ?>').val('');    
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
  });
  $('.btncancel<?php echo $idgui ?>').click(function(ev){
    $('#accionref<?php echo $idgui; ?>').val('');
    $('#idreferencia<?php echo $idgui; ?>').val('');
    $('#nombre<?php echo $idgui; ?>').val('');
    $('#cargo<?php echo $idgui; ?>').val('');
    $('#relacion<?php echo $idgui; ?>').val(''); 
    $('#telefono<?php echo $idgui; ?>').val('');
    $('#correo<?php echo $idgui; ?>').val('');     
    $("#pnla_<?php echo $idgui; ?>").show();
    $("#pnlb_<?php echo $idgui; ?>").hide();
  })

  $("#pnla_<?php echo $idgui; ?>").on('click','.btneditarreferencia',function(ev){
    var id=$(this).attr('data-id');
    var user=userref!=[]?(userref[id]!=undefined?userref[id]:[]):[];
    $('#idpersona<?php echo $idgui; ?>').val(user.idpersona||'');
    $('#idreferencia<?php echo $idgui; ?>').val(user.idreferencia||'');
    $('#accionref<?php echo $idgui; ?>').val('edit');
    $('#nombre<?php echo $idgui; ?>').val(user.nombre||'');
    $('#cargo<?php echo $idgui; ?>').val(user.cargo||'');
    $('#relacion<?php echo $idgui; ?>').val(user.relacion||''); 
    $('#telefono<?php echo $idgui; ?>').val(user.telefono||'');   
    $('#correo<?php echo $idgui; ?>').val(user.correo||'');
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
  }).on('click','.btneliminarreferencia',function(ev){
    var id=$(this).attr('data-id');
    var res = xajax__('', 'persona_referencia', 'eliminar', id);
    if(userref[id]!=undefined) delete userref[id];
    $(this).closest('.item-user').remove();    
  });
  var changefoto=false;
  $('#frm<?php echo $idgui;?>').bind({ 
    keyup:function(ev){
      if(ev.which == 13) return false;
    },submit: function(event){
      var idguitmp='<?php echo $idgui; ?>';
      var myForm = document.getElementById('frm<?php echo $idgui; ?>'); 
      var formData = new FormData(myForm);
      var accion=$('#accionref'+idguitmp).val();
      formData.append('accion',accion);
      formData.append('idpersona', $('#idpersona'+idguitmp).val()); 
      formData.append('idreferencia', $('#idreferencia'+idguitmp).val()); 
      formData.append('plt','blanco');
      var url=_sysUrlBase_+'/persona_referencia/guardarPersona_referencia';     
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        cache: false,
        dataType:'json',
        beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
        success: function(data){  
          if(data.code=='Error'){
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
            var _id=data.newid; 
            var usertmp={
              idreferencia:_id,
              idpersona:$('#idpersona'+idguitmp).val(),
              nombre:$('#nombre'+idguitmp).val(),
              cargo:$('#cargo'+idguitmp).val(),
              relacion:$('#relacion'+idguitmp).val(),
              correo:$('#correo'+idguitmp).val(),
              telefono:$('#telefono'+idguitmp).val()
            }       
            if(accion=='add'){
              $newuser=$('#pnla_'+idguitmp+' .item-user:first-child').clone(true,true);
              $newuser.removeAttr('style');
              $newuser.attr('id','item-user'+_id);
              $newuser.find('.nombreclone'+idguitmp).addClass('nombre'+_id).removeClass('nombreclone'+idguitmp).text(usertmp.nombre);
              $newuser.find('.cargoclone'+idguitmp).addClass('cargo'+_id).removeClass('cargoclone'+idguitmp).text(usertmp.cargo);
              $newuser.find('.relacionclone'+idguitmp).addClass('relacion'+_id).removeClass('relacionclone'+idguitmp).text(usertmp.relacion);              
              $newuser.find('.correoclone'+idguitmp).addClass('correo'+_id).removeClass('correoclone'+idguitmp).text(' : '+usertmp.correo);
              $newuser.find('.telefonoclone'+idguitmp).addClass('telefono'+_id).removeClass('telefonoclone'+idguitmp).text(' : '+usertmp.telefono);
              $newuser.find('.btneditarreferencia').attr('data-id',_id);
              $newuser.find('.btneliminarreferencia').attr('data-id',_id);
              userref[_id]=usertmp;          
              $("#pnla_"+idguitmp+' #addaqui'+idguitmp).append($newuser);
            }else{
              $edituser=$('#item-user'+data.newid);
              $edituser.find('.nombre'+_id).text(usertmp.nombre);
              $edituser.find('.cargo'+_id).text(usertmp.cargo);
              $edituser.find('.relacion'+_id).text(usertmp.relacion);             
              $edituser.find('.correo'+_id).text(' : '+usertmp.correo);
              $edituser.find('.telefono'+_id).text(' : '+usertmp.telefono);
              $edituser.find('.btneditarreferencia').attr('data-id',_id);
              $edituser.find('.btneliminarreferencia').attr('data-id',_id);
              userref[_id]=usertmp;              
            }
            $("#pnla_"+idguitmp).show();
            $("#pnlb_"+idguitmp).hide();
          }
        },
        error: function(e){ console.log(e); },
        complete: function(xhr){ }
      });
      return false;
    }
  });

  <?php 
  if(empty($referencias)) echo '$(".btnaddref'.$idgui.'").trigger("click");';
  ?>
});
</script>