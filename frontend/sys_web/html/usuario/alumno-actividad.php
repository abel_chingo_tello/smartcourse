<?php
// echo "alvaro";
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$ventanapadre = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!empty($this->datos)) $personal = $this->datos;

//echo $this->idalumno;//json_encode($personal);
?>

<link href="<?php echo URL_BASE; ?>static/libs/x-editable/dist/bootstrap4-editable/css/bootstrap-editable.css" rel="stylesheet" />
<link href="<?php echo URL_BASE; ?>static/libs/img-resize/img-resize.css" rel="stylesheet" />
<script src="<?php echo URL_BASE; ?>static/libs/x-editable/dist/bootstrap4-editable/js/bootstrap-editable.min.js"></script>

<style>
    .timeline .tags-file {
        position: absolute;
        right: 0;
        width: 84px;
        bottom: 20px;
    }

    .byline {
        line-height: 1.8;
    }

    .separation {
        padding: 10px;
    }

    .contenido {
        background-color: #87ceeb;
        width: 50%;
        position: relative;
        height: 100%;
        margin-top: 10px;
        text-align: center;
        padding: 10px;

    }

    .contenido_amigo {
        background-color: #20f1f14d;
        border-radius: 5px;
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);
        display: inline-block;
        font-size: 13px;
        padding: 15px;
        vertical-align: top;
    }

    .fecha_izquierda {
        color: #777;
        font-style: italic;
        font-size: 13px;
        text-align: left;
        margin-right: 35px;
        margin-top: 10px;
    }

    .fecha_derecha {
        color: #777;
        font-style: italic;
        font-size: 13px;
        text-align: right;
        margin-top: 10px;
    }

    .contenido_autor {
        background-color: #87ceeb;
        border-radius: 5px;
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);
        display: inline-block;
        font-size: 13px;
        padding: 15px;
        vertical-align: top;
    }

    #chat {
        height: 50vh;
        overflow: auto;
    }

    .mensaje-autor {
        float: right;
        text-align: right;
        margin-right: 15px;
    }

    .accordion .panel-heading {
        text-decoration: none;
    }

    .editable-click {
        border: 2px solid red;
        /* padding: 1px; */
        border-radius: 10px 10px;
        padding-left: 5px;
        padding-right: 6px;
        color: black;
        text-decoration: none;
        border-bottom: none;
    }

    .editable-click,
    a.editable-click,
    a.editable-click:hover {
        border-bottom: 2px solid red;
    }
</style>
<div class="panel">
    <div class="form-group">
        <label for="selectCurso"><?php echo JrTexto::_('Select a course'); ?></label>
        <select class="form-control" id="selectCurso" name="selectCurso">
            <?php
            foreach ($personal as $key => $value) {
                echo '<option value="' . $value['idcurso'] . '"  data-id="' . $value['idcurso'] . '" data-confignota="' . json_decode($value['configuracion_nota'], true)['maxcal'] . '">' . $value['nombre'] . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="panel-body">
        <div class="col-md-12 text-center">
            <h4 id="title_table"><?php echo JrTexto::_('Actividad por Curso'); ?></h4>
        </div>
        <!-- <ol id="lista2">
        </ol> -->
        <!-- <table class="table table-striped" id="lista2"></table> -->
        <div class="accordion" id="lista2" role="tablist" aria-multiselectable="true">

        </div>
    </div>
    <div class="modal fade" id="modalclone" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title" id="modaltitle" style="width: 100%"></h5>
                    <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" id="modalcontent">
                    <div style="text-align: center; margin-top: 2em;"><img src="< ?php echo $documento->getUrlStatic() ?>/media/cargando.gif"><br><span style="color: #006E84;">
                            <!-- < ?php echo JrTexto::_("Loading"); ?> --></span></div>
                </div>
                <div class="modal-footer" id="modalfooter">
                    <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal">
                        <!-- < ?php echo JrTexto::_('Close'); ?> --></button>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none;">
        <div id="subirtarea">
            <div class="x_panel">
                <div class="x_title">
                    <h2 style="font-size: 22px;"><i class="fa fa-tasks estarea"></i> <small><?php echo JrTexto::_('Task'); ?></small> <span class="estareaproyecto" style="display: none;">/</span>
                        <i class="fa fa-folder esproyecto"></i> <small><?php echo JrTexto::_('Project'); ?></small></h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <ul class="nav nav-tabs bar_tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active idtab isa" id="frmsubirarchivo_tab" data-toggle="tab" role="tab" href="#frmsubirarchivo">Subir Archivo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link idtab isa" id="listadomensajes-tab" data-toggle="tab" role="tab" href="#listadomensajes">Mensajes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link idtab isa" id="listadoarchivos-tab" data-toggle="tab" role="tab" href="#listadoarchivos">Archivos subidos</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active idtab" id="frmsubirarchivo" role="tabpanel" aria-labelledby="frmsubirarchivo_tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <form id="frmarchivosmultimedia" method="post" target="" enctype="multipart/form-data">
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label><br><?php echo JrTexto::_("Tipo de Archivo") ?></label><br>
                                            <div class="btn-group" role="group" aria-label="" style="z-index: 150000">
                                                <button type="button" class="btn btn-secondary btntipofile" title="Texto"><i class="fa fa-font"></i></button>
                                                <button type="button" class="btn btn-secondary btntipofile" title="Imagen"><i class="fa fa-photo"></i></button>
                                                <button type="button" class="btn btn-secondary btntipofile" title="Audio"><i class="fa fa-file-audio-o"></i></button>
                                                <button type="button" class="btn btn-secondary btntipofile" title="Video"><i class="fa fa-video-camera"></i></button>
                                                <button type="button" class="btn btn-secondary btntipofile" title="Html5"><i class="fa fa-html5"></i></button>
                                                <button type="button" class="btn btn-secondary btntipofile" title="Link"><i class="fa fa-link"></i></button>
                                            </div>
                                            <div id="aquitipofile">

                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label style=""><?php echo JrTexto::_('Mensaje'); ?> <i class="fa fa-comment"></i> </label>
                                            <textarea id="_mensaje_" class="form-control" rows="4" placeholder="<?php echo  ucfirst(JrTexto::_("Mensaje")) ?>" value="" style="resize: none;"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-12 text-center">
                                    <button class="btnguardartarea btn btn-primary "><i class="fa fa-save"></i> Responder Tarea </button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane idtab mensajeenviados" id="listadomensajes" role="tabpanel" aria-labelledby="listadomensajes-tab">
                            <div id="chat">
                                <div id="mensajes">
                                    <h2 class="text-center">No tiene Mensajes enviados</h2>
                                </div>
                            </div>
                            <div id="caja-mensaje">
                                <textarea id="_mensaje2_" class="form-control" rows="2" placeholder="<?php echo  ucfirst(JrTexto::_("Mensaje")) ?>" value="" style="resize: none;"></textarea>
                                <div class="text-center">
                                    <button style="margin: 1ex" class="btn btn-primary btnsendmensaje"> Enviar Mensaje → </button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane table-responsive idtab archivosenviados" id="listadoarchivos" role="tabpanel" aria-labelledby="listadoarchivos-tab">
                            <table class="table  table-striped">
                                <thead>
                                    <tr>
                                        <th>Archivo</th>
                                        <th>Fecha</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3">Aun no a enviado archivos de este trabajo</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 text-center" style="padding: 1ex;">
                        <button class="salirmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--div para el fondo de pantalla-->
    <div id='background'></div>
    <!--div para visualizar la imagen grande con el boton cerrar-->
    <div id='preview'>
        <div id='close'></div>
        <div id='content-img'></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var url_media = _sysUrlBase_;
        //$("#table_< ?php echo $idgui; ?>").DataTable();
        //var idalumno = < ?php echo $this->idalumno?>;
        contab = 0;
        $("#selectCurso").on('change', function() {
            console.log($('select[name="selectCurso"] option:selected').text())
            $("#title_table").html("Actividad del Curso " + $('select[name="selectCurso"] option:selected').text())
            //$('#selectCurso').attr('data-confignota');
            listar_cursos($('#selectCurso').val(), $('#selectCurso option:selected').attr('data-confignota'));
            console.log('idalumno', <?php echo $this->idalumno ?>)
            console.log('data-config', $('#selectCurso option:selected').attr('data-confignota'))
        });

        function mostrarmodal() {
            var htmltarea = $('#subirtarea').clone(true);
            htmltarea.find('.idtab').each(function(i, v) {
                if ($(v).hasClass('isa')) $(v).attr('href', $(v).attr('href') + contab);
                $(v).attr('id', $(v).attr('id') + contab);
            })

            var el = $(this);

            /* var tipotarea = '';
            if (el.hasClass('estarea') && el.hasClass('esproyecto')) {
                htmltarea.find('.estareaproyecto').show();
                tipotarea = 'TP';
            } else if (el.hasClass('estarea')) {
                htmltarea.find('.esproyecto').remove();
                tipotarea = 'T';
            } else if (el.hasClass('esproyecto')) {
                htmltarea.find('.estarea').remove();
                tipotarea = 'P';
            } */
            var dt = {
                html: htmltarea
            };
            console.log('el', el, 'dt', dt);
            var md = __sysmodal(dt);
            console.log('md', md);
            var _cargarmensajesyarchivos = function() {
                var formData = new FormData();
                formData.append('idcurso', parseInt(el.attr('idcurso')));
                formData.append('idtarea', parseInt(el.attr('data-idtarea')));
                formData.append('idsesion', parseInt(el.attr('idcursodetalle')));
                formData.append('idpestania', parseInt(el.attr('idcursopestania') || '0'));
                formData.append('tipo', el.attr('tipo'));

                __sysajax({
                    fromdata: formData,
                    url: _sysUrlBase_ + 'json/tareas/archivosalumnoymensajes',
                    showmsjok: false,
                    callback: function(rs) {
                        console.log('data archivos', rs)
                        $pnlmensajes = md.find('.mensajeenviados #mensajes');
                        if (rs.mensajes) {
                            $pnlmensajes.html('');
                            var yo = '<?php echo $this->idalumno ?>';
                            $.each(rs.mensajes, function(i, v) {
                                var fotouser = (v.foto == '' || v.foto == null) ? 'static/media/usuarios/user_avatar.jpg' : v.foto;
                                var msj = "";
                                var ifoto = fotouser.lastIndexOf('static/');
                                if (ifoto == -1) {
                                    fotouser = _sysUrlBase_ + 'static/media/usuarios/user_avatar.jpg';
                                } else fotouser = _sysUrlBase_ + fotouser.substring(ifoto);
                                console.log('idusuarioMensajes', yo);
                                if (v.idusuario == yo) {
                                    //msj = '<div class="mensaje-autor"><!--img src="' + fotouser + '" alt="" class="foto img-responsive"--><div class="flecha-izquierda"></div><div class="contenido_autor"><b>' + v.usuario + '</b><br>' + v.mensaje + '</div><div class="">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                                    msj = '<div class="mensaje-autor"><div class="flecha-izquierda"></div><div class="contenido_autor"><b>' + v.usuario + '</b><br>' + v.mensaje + '</div><div class="fecha_derecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                                } else {
                                    //msj = '<div class="mensaje-amigo"><div class="contenido"><b>' + v.usuario + '</b><br>' + v.mensaje + '</div><div class="flecha-derecha"></div><img src="' + fotouser + '" alt="" class="foto img_responsive"><div class="">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                                    msj = '<div class="mensaje-amigo"><div class="contenido_amigo"><b>' + v.usuario + '</b><br>' + v.mensaje + '</div><div class="flecha-derecha"></div><div class="fecha_izquierda">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                                }
                                $pnlmensajes.append(msj);
                            })
                        }
                        $pnlarchivos = md.find('.archivosenviados table tbody');
                        if (rs.archivos) {
                            $pnlarchivos.html('');
                            $.each(rs.archivos, function(i, v) {
                                var html = '<tr><td><a href="' + v.media + '" target="_blank" download class="btn btn-sm"> <i class="fa fa-download"></i> des</a></td><td>' + v.fecha_registro + '</td><td class="text-center"> <i class="fa fa-trash borrararchivosubido" style="cursor:pointer" id="' + v.idtarearecurso + '"></i></tr>';
                                $pnlarchivos.append(html);
                            })
                        }
                    }
                })
            }
            var _guardartarea = function(solomsj = false) {
                var link = el.attr('data-link');
                var tipolink = el.attr('data-tipolink');
                if (tipolink == 'smartquiz') link = _sysUrlSitio_ + link.substring(link.lastIndexOf("/quiz/"));
                else link = _sysUrlBase_ + link.substring(link.lastIndexOf("static/"));
                var tipomedia = md.find('#infolink').attr('tipolink') || '';
                if (tipomedia == 'texto' || tipomedia == 'link') mediainfo = md.find('#infolink').attr('value') || '';
                else mediainfo = md.find('#infolink').val() || '';
                if (mediainfo == undefined) mediainfo = '';
                $mensaje = md.find('#_mensaje_').val() || 'Envio este archivo.';
                if (tipomedia == 'imagen') mediainfo = md.find('#infolink').attr('value') || '';
                if (mediainfo != '' && $mensaje != '') {
                    $mensaje = '<a href="' + mediainfo + '" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> File</a><br>' + $mensaje;
                }
                var formData = new FormData();
                formData.append('tipo', el.attr('tipo'));
                formData.append('idcurso', parseInt(el.attr('idcurso')));
                formData.append('idsesion', parseInt(el.attr('idcursodetalle')));
                formData.append('idpestania', parseInt(el.attr('idcursopestania') || '0'));
                formData.append('recurso', el.attr('recurso'));
                formData.append('tiporecurso', el.attr('tiporecurso'));
                formData.append('iddocente', parseInt(el.attr('iddocente')));
                formData.append('nombre', el.attr('data-nombre') || '');
                formData.append('descripcion', el.attr('data-descripcion') || '');
                formData.append('media', mediainfo);
                formData.append('tipomedia', tipomedia);
                if (solomsj) {
                    formData.append('solomensaje', true);
                    formData.append('mensaje', md.find('#_mensaje2_').val() || '');
                    md.find('#_mensaje2_').val('');
                } else {
                    formData.append('mensaje', $mensaje);
                    md.find('#_mensaje_').val('');
                }
                __sysajax({
                    fromdata: formData,
                    url: _sysUrlBase_ + 'json/tareas/guardar',
                    callback: function(rs) {
                        _cargarmensajesyarchivos();
                    }
                })
            }
            md.on('click', '.salirmodal', function(ev) {
                __cerrarmodal(md, true);
            }).on('click', '.btnguardartarea', function(ev) {
                _guardartarea(false);
            }).on('click', '.btntipofile', function(ev) {
                var el_ = $(this);
                el_.siblings().removeClass('btn-primary').addClass('btn-secondary');
                el_.addClass('btn-primary').removeClass('btn-secondary');
                var title = el_.attr('title');
                var aquitipofile = md.find('#aquitipofile');
                if (title == 'Texto') {
                    aquitipofile.html('<br><textarea id="infolink" tipolink="Texto" class="form-control" rows="5" placeholder="Copie o escriba su texto de repuesta"  style="resize: none;"></textarea>');
                } else if (title == 'Imagen') {
                    var idimagen = __idgui();
                    aquitipofile.html('<input type="hidden" id="infolink" tipolink="imagen" ><div><img src="" class="img-responsive" id="' + idimagen + '" style="max-width:130px; max-height:130px;"></div>');
                    $img = aquitipofile.find('#' + idimagen);
                    __subirfile_({
                        file: $img,
                        typefile: 'imagen',
                        uploadtmp: true,
                        guardar: true,
                        dirmedia: 'trabajos/'
                    }, function(rs) {
                        console.log(rs);
                        let f = rs.media.replace(url_media, '');
                        //let f = rs.media;
                        console.log('imagen', rs);
                        aquitipofile.find('#infolink').attr('value', f);
                        $img.attr('src', url_media + f);
                    });

                } else if (title == 'Video') {
                    var idimagen = __idgui();
                    aquitipofile.html('<input type="hidden" id="infolink"  tipolink="video"><div><video controls src="" class="img-responsive" id="' + idimagen + '" style="max-width:130px; max-height:130px;"></div>');
                    $img = aquitipofile.find('#' + idimagen);
                    __subirfile_({
                        file: $img,
                        typefile: 'video',
                        uploadtmp: true,
                        guardar: true,
                        dirmedia: 'trabajos/'
                    }, function(rs) {
                        let f = rs.media.replace(url_media, '');
                        aquitipofile.find('#infolink').attr('value', f);
                        $img.attr('src', url_media + f);
                    });

                } else if (title == 'Audio') {
                    var idimagen = __idgui();
                    aquitipofile.html('<input type="hidden" id="infolink"  tipolink="audio"><div><audio controls src="" class="img-responsive" id="' + idimagen + '"></div>');
                    $img = aquitipofile.find('#' + idimagen);
                    __subirfile_({
                        file: $img,
                        typefile: 'audio',
                        uploadtmp: true,
                        guardar: true,
                        dirmedia: 'trabajos/'
                    }, function(rs) {
                        let f = rs.media.replace(url_media, '');
                        aquitipofile.find('#infolink').attr('value', f);
                        $img.attr('src', url_media + f);
                    });

                } else if (title == 'Html5') {
                    var idimagen = __idgui();
                    aquitipofile.html('<input type="hidden" id="infolink"  tipolink="html"><div><a src="img" class="img-responsive" id="' + idimagen + '">Link subido</a></div>');
                    $img = aquitipofile.find('#' + idimagen);
                    __subirfile_({
                        file: $img,
                        typefile: 'html',
                        uploadtmp: true,
                        guardar: true,
                        dirmedia: 'trabajos/'
                    }, function(rs) {
                        let f = rs.media.replace(url_media, '');
                        aquitipofile.find('#infolink').attr('value', f);
                        $img.attr('href', url_media + f);
                    });

                } else if (title == 'Link') {
                    aquitipofile.html('<br><input id="infolink"  tipolink="link" value="" placeholder="Copie o escriba el enlace " class="form-control"> ');
                }
            }).on('click', '.borrararchivosubido', function(ev) {
                var el_ = $(this);
                var formData = new FormData();
                formData.append('idtarearecurso', parseInt(el_.attr('id')));
                __sysajax({
                    fromdata: formData,
                    url: _sysUrlBase_ + 'json/tareas_archivosalumno/eliminar',
                    showmsjok: false,
                    callback: function(rs) {
                        el_.closest('tr').remove();
                    }
                })
            }).on('click', '.btnsendmensaje', function(ev) {
                _guardartarea(true);
            })
            _cargarmensajesyarchivos();
            contab++;
        }

        function mostrarImagen() {
            $('#background').css('height', '100%');

            console.log('height', $(".content").find('#iFrmContent').height());
            //$('#preview').css('top', (($(".panel").height() / 2) - ($('#preview').height() / 2) + $(".panel").scrollTop()));
            $('#preview').css('top', '20vh');
            $('#preview').css('height', 'auto');
            $('#preview').css('left', ($(".panel").width() / 2) - ($('#preview').width() / 2));
            // Cargamos la imagen en la capa grande
            $('#content-img').html("<img src='" + $(this).attr("src") + "' style='width:100%;'>");
            // Mostramos las capas
            $('#preview').fadeIn();
            $('#background').fadeIn();

            // Cerramos las capas al pulsar sobre el fondo
            $("#background").click(function() {
                $('#background').fadeOut();
                $('#preview').fadeOut();
            });
            // Cerramos las capas al pulsar sobre la cruz
            $("#close").click(function() {
                $('#background').fadeOut();
                $('#preview').fadeOut();
            });
        }

        function listar_cursos(idcurso, maxcal) {
            console.log(<?php echo json_encode($personal) ?>);
            var formData = new FormData();
            formData.append('idcurso', parseInt(idcurso));
            <?php if ($this->tipousuario == 3) {
            ?>
                formData.append('idalumno', parseInt(<?php echo $this->idalumno ?>));
            <?php
            } else {
            ?>
                formData.append('iddocente', parseInt(<?php echo $this->idalumno ?>));
            <?php
            }
            ?>


            $.ajax({
                url: _sysUrlBase_ + 'json/tareas/listado',
                type: 'POST',
                contentType: false,
                data: formData, // mandamos el objeto formdata que se igualo a la variable data
                processData: false,
                cache: false,
                beforeSend: function(objeto) {
                    //$("#loadCategoria").html("<img src='../util/images/load.gif'>");
                },
                success: function(data) {
                    var response = JSON.parse(data);
                    var html = "";
                    <?php if ($this->tipousuario == 3) { ?>
                        console.log('data', response.data.length);
                        if (response.data.length > 0) {
                            $.each(response.data, function(i, item) {
                                if (item.tipo == 'TP' || item.tipo == 'T') {
                                    html += '<div class="panel">';
                                    html += '<a class="panel-heading collapsed" role="tab" id="heading' + i + '" data-toggle="collapse" data-parent="#lista2" href="#collapse' + i + '" aria-expanded="false" aria-controls="collapse' + i + '">';
                                    html += '<h4 class="panel-title">' + ((item.idsesion != 0) ? 'SESION' : 'PESTAÑA') + ' - ' + item.nombre + '</h4>';
                                    html += '</a>';
                                    html += '<div id="collapse' + i + '" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading' + i + '" style="">';
                                    html += '<div class="panel-body">';
                                    html += '<ul class="list-unstyled timeline">';
                                    html += '<li style="background:#fcffff;">';
                                    html += '<div class="block">';
                                    html += '<div class="tags">';
                                    html += '<a class="tag" style="text-decoration:none;">';
                                    html += '<span>' + item.nombre_alumno + '</span>';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="block_content">';
                                    html += '<h2 class="title">';
                                    html += '<a>' + item.descripcion + '</a>';
                                    html += '</h2>';
                                    html += '<div class="byline">';
                                    html += '<span>- Recurso</span>: <a href="' + item.recurso + '">Link para ver el recurso</a><br>';
                                    html += '<span>- Tipo recurso</span>: <a>' + item.tiporecurso + '</a><br>';
                                    html += '<span>- Nota</span>: ' + item.nota + ' en Base a ' + maxcal;
                                    html += '<div class="col-md-2 contenido btnsubirfilealumno estarea" style="cursor: pointer;    border-radius: 15px 2px;background-color: #1abb9c;" idcurso="' + item.idcurso + '" idcursopestania="' + item.idpestania + '" idcursodetalle="' + item.idsesion + '" iddocente="' + item.iddocente + '" tiporecurso="' + item.tiporecurso + '" recurso="' + item.recurso + '" tipo="' + item.tipo + '" data-link="static/media/cursos/curso_171/ses_1591/file_1591_5_1580940389570.pdf?id=20200205170643" data-tipolink="pdf" data-nombre="' + item.nombre + '" data-descripcion="' + item.descripcion + '" id="contenido' + i + '"><i class="fa fa-upload"  style="cursor: pointer;font-size: 30px;color: white;"></i></div>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</li>';
                                    /*html += '<li style="background:#fcffff;">';
                                    html += '<div class="block">';
                                    html += '<div class="tags">';
                                    html += '<a href="" class="tag">';
                                    html += '<span>Notificación</span>';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="block_content">';
                                    html += '<h2 class="title">';
                                    html += '<a>' + item.descripcion + '</a>';
                                    html += '</h2>';
                                    html += '<div class="byline">';
                                    html += '<span>13 hours ago</span> by <a>Jane Smith</a>';
                                    html += '</div>';
                                    html += '<p class="excerpt">Tiene un mensaje </a>';
                                    html += '</p>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</li>';*/
                                    html += '</ul>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                }

                            });
                            $("#lista2").html(html);
                            $('.btnsubirfilealumno').each(function() {
                                document.getElementById($(this).attr('id')).addEventListener("click", mostrarmodal, false);
                            });
                        } else {
                            html += "<center><h3>No hay ninguna actividad registrada para este curso.</h3></center>";
                            $("#lista2").html(html);
                        }
                    <?php } else { ?>
                        if (response.data.length > 0) {
                            //html += '<tbody>';
                            console.log('view', response.data);
                            $.each(response.data, function(i, item) {
                                if (item.tipo == 'TP' || item.tipo == 'T') {
                                    html += '<div class="panel">';
                                    html += '<a class="panel-heading collapsed" role="tab" id="heading' + i + '" data-toggle="collapse" data-parent="#lista2" href="#collapse' + i + '" aria-expanded="false" aria-controls="collapse' + i + '">';
                                    html += '<h4 class="panel-title">' + ((item.idsesion != 0) ? 'SESION' : 'PESTAÑA') + ' - ' + item.nombre + '</h4>';
                                    html += '</a>';
                                    html += '<div id="collapse' + i + '" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading' + i + '" style="">';
                                    html += '<div class="panel-body">';
                                    html += '<ul class="list-unstyled timeline">';
                                    html += '<li style="background:#fcffff;">';
                                    html += '<div class="block">';
                                    html += '<div class="tags">';
                                    html += '<a class="tag" style="text-decoration:none;">';
                                    html += '<span>' + item.nombre_alumno + '</span>';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="block_content">';
                                    html += '<h2 class="title">';
                                    html += '<a>' + item.descripcion + '</a>';
                                    html += '</h2>';
                                    html += '<div class="byline">';
                                    html += '<span class="separation"><b>Recurso</b></span>: <a href="' + item.recurso + '" style="color: #1abb9c;"><i class="fa fa-globe"></i> Link del recurso contestado</a><br>';
                                    html += '<span class="separation"><b>Tipo recurso</b></span>: <a>' + item.tipo_media.toUpperCase() + '</a><br>';
                                    if (item.tipo_media == 'imagen')
                                        html += '<span class="separation"><b>Recurso enviado</b></span>:<br><img id="img' + i + '" class="separation img-sep" src="https://localhost/smartcourse/' + item.media.split('?')[0] + '" width="20%"/><br>';

                                    /*$.each(item.criterios, function(i, item) {
                                        
                                    });*/
                                    if (item.criterios != '') {
                                        $.each(JSON.parse(item.criterios), function(i, item) {
                                            html += '<span class="separation"><b>Nota</b></span>: <a href="#" class="nota" id="nota' + i + '" data-name="nota" data-base="' + maxcal + '" data-pk="' + item.idtarea + '">' + item.nota + '</a> en Base a ' + maxcal;
                                        });
                                    }
                                    console.log('criterios', JSON.parse(item.criterios));

                                    html += '<div class="tags-file"><div class="col-md-2 contenido btnsubirfilealumno estarea" style="cursor: pointer;border-radius: 15px 2px;background-color: #1abb9c;max-width: 100%;width: 80%;" idcurso="' + item.idcurso + '" idcursopestania="' + item.idpestania + '" idcursodetalle="' + item.idsesion + '" iddocente="' + item.iddocente + '" tiporecurso="' + item.tiporecurso + '" recurso="' + item.recurso + '" tipo="' + item.tipo + '" data-link="static/media/cursos/curso_171/ses_1591/file_1591_5_1580940389570.pdf?id=20200205170643" data-tipolink="pdf" data-nombre="' + item.nombre + '" data-idtarea ="' + item.idtarea + '" data-descripcion="' + item.descripcion + '" id="contenido' + i + '"><i class="fa fa-upload"  style="cursor: pointer;font-size: 30px;color: white;"></i></div></div>';
                                    //html += '<div class="col-md-2 contenido btnsubirfilealumno estarea" style="cursor: pointer;border-radius: 15px 2px;background-color: #1abb9c;" idcurso="' + item.idcurso + '" idcursopestania="' + item.idpestania + '" idcursodetalle="' + item.idsesion + '" iddocente="' + item.iddocente + '" tiporecurso="' + item.tiporecurso + '" recurso="' + item.recurso + '" tipo="' + item.tipo + '" data-link="static/media/cursos/curso_171/ses_1591/file_1591_5_1580940389570.pdf?id=20200205170643" data-tipolink="pdf" data-nombre="' + item.nombre + '" data-idtarea ="' + item.idtarea + '" data-descripcion="' + item.descripcion + '" id="contenido' + i + '"><i class="fa fa-upload"  style="cursor: pointer;font-size: 30px;color: white;"></i></div>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</li>';
                                    /*html += '<li style="background:#fcffff;">';
                                    html += '<div class="block">';
                                    html += '<div class="tags">';
                                    html += '<a href="" class="tag">';
                                    html += '<span>Notificación</span>';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="block_content">';
                                    html += '<h2 class="title">';
                                    html += '<a>' + item.descripcion + '</a>';
                                    html += '</h2>';
                                    html += '<div class="byline">';
                                    html += '<span>13 hours ago</span> by <a>Jane Smith</a>';
                                    html += '</div>';
                                    html += '<p class="excerpt">Tiene un mensaje </a>';
                                    html += '</p>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</li>';*/
                                    html += '</ul>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                }

                            });
                            //html += '</tbody>';
                            $("#lista2").html(html);
                            $('.nota').each(function() {
                                console.log('id', $(this).attr('id'));
                                $('#' + $(this).attr('id')).editable({
                                    url: 'json/tareas/updateNota',
                                    type: 'text',
                                    title: 'Ingrese Nota',
                                    success: function(response, newValue) {
                                        console.log('response', response);
                                        console.log('newvalue', newValue);
                                    }
                                });
                            });
                            $('.btnsubirfilealumno').each(function() {
                                document.getElementById($(this).attr('id')).addEventListener("click", mostrarmodal, false);
                            });

                            $(".img-sep").each(function() {
                                document.getElementById($(this).attr('id')).addEventListener("click", mostrarImagen, false);
                            });

                            /* $('.notabaseen').each(function() {
                                $('#' + $(this).attr('id')).editable({
                                    url: 'json/tareas/updateNota',
                                    type: 'text',
                                    title: 'Ingrese Nota',
                                    success: function(response, newValue) {
                                        console.log('response', response);
                                        console.log('newvalue', newValue);
                                    }
                                });
                            }); */
                            /*$('.btnsubirfilealumno').each(function() {
                                document.getElementById($(this).attr('id')).addEventListener("click", mostrarmodal, false);
                            });*/
                        } else {
                            html += "<center><h3>No hay ninguna actividad registrada para este curso.</h3></center>";
                            $("#lista2").html(html);
                        }
                    <?php
                    }
                    ?>
                    console.log('Datos Listado', response);
                }
            })
        }
        listar_cursos();

    });
</script>