<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"eeeexzx-1";
if(!empty($this->datos)) $frm=$this->datos;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  .select-ctrl-wrapper:after{
      right: 0px;
    }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
  hr{
    margin-bottom: 1ex;
    margin-top: 1ex;
    border-top: 1px solid #dde6ec;
  }
</style>
<?php if($this->documento->plantilla!='modal'){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="#" onclick="window.history.back();"><i class="fa fa-history"></i>&nbsp;<?php echo JrTexto::_('Return'); ?></a></li>       
        <li class="active">&nbsp;<?php echo JrTexto::_('Change Password'); ?></li>        
    </ol>
</div> </div>
<?php } ?>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm<?php echo $idgui;?>"  class="form-horizontal form-label-left" >
          <input type="hidden" name="idpersona" id="idpersona<?php echo $idgui;?>" value="<?php echo $frm["idpersona"];?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtemail">
              <?php echo JrTexto::_('Email');?> :
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="email"  id="txtemail<?php echo $idgui;?>" name="txtemail" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["email"];?>" placeholder="email@empresa.com" autofocus>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtusuario">
              <?php echo JrTexto::_('User');?> :
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="txtusuario<?php echo $idgui;?>" name="txtusuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["usuario"];?>" placeholder="<?php echo JrTexto::_('user');?>">
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtclave">
               <?php echo JrTexto::_('Password');?> <span class="required"> (*) :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="password"  autocomplete="false" id="txtclave<?php echo $idgui;?>" name="txtclave" required="required" class="form-control col-md-7 col-xs-12" placeholder="<?php echo JrTexto::_('Password');?>" >
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtclave2">
              <?php echo JrTexto::_('Repit password');?> <span class="required"> (*) :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="password" autocomplete="false"  id="txtclave2<?php echo $idgui;?>" name="txtclave2" required="required" class="form-control col-md-7 col-xs-12" placeholder="<?php echo JrTexto::_('repit password');?>" >
              </div>
          </div>
          <div class="text-center">
            <hr><br>
            <button type="submit" class="btn btn-primary btnsave<?php echo $idgui; ?>"> <i class="fa fa-save"></i> <?php echo JrTexto::_('Save') ?> </button>
            <button  type="button" class="btn btn-warning btncancel<?php echo $idgui; ?>"><i class="fa fa-history"></i> <?php echo JrTexto::_('Cancel') ?></button>
          </div>
        </form>
        <script type="text/javascript">
          $(document).ready(function(){
            $('.btncancel<?php echo $idgui; ?>').click(function(ev){
              ev.preventDefault();
              <?php if($this->documento->plantilla!='modal'){?>
              window.history.back();
              <?php }else{ ?>
                $(this).closest('.modal').modal('hide');
              <?php }?>
            });

            $('#frm<?php echo $idgui;?>').bind({    
              submit: function(ev){
                  ev.preventDefault();
                  var frm=$(this);
                  var btn=$('#btnsave<?php echo $idgui; ?>');
                  var usuario=$('#txtusuario<?php echo $idgui; ?>').val()||'';
                  var clave=$('#txtclave<?php echo $idgui; ?>').val()||'';
                  var reclave=$('#txtclave2<?php echo $idgui; ?>').val()||'';
                  var email=$('#txtemail<?php echo $idgui; ?>').val()||'';
                  if(clave!=reclave){
                     mostrar_notificacion('<?php echo JrTexto::_('Attention');?>','<?php echo JrTexto::_('Password incorrect') ?>','warning');
                     $('#txtclave<?php echo $idgui; ?>').focus();
                     return;
                  }
                  var formData = new FormData(); 
                  formData.append('idpersona', $('#idpersona<?php echo $idgui;?>').val()||'');
                  formData.append('usuario', usuario);
                  formData.append('email', email);
                  formData.append('clave', clave);
                  formData.append('reclave',reclave);                 
                  var url=_sysUrlBase_+'/personal/guardarclave';
                  $.ajax({
                    url: url,
                    type: "POST",
                    data:  formData,
                    contentType: false,
                    processData: false,
                    dataType:'json',
                    cache: false,
                    beforeSend: function(XMLHttpRequest){ btn.attr('disabled', true); },      
                    success: function(data){                        
                      if(data.code=='Error'){
                         mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
                      }else{
                         mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
                        let htmlcorreo='Datos de usuario<br>';
                        htmlcorreo+='<b>Usuario<b>:'+usuario+'<br>';
                        htmlcorreo+='<b>Clave</b>:'+clave+'<br>';
                        htmlcorreo+='<b>Link de Acceso a la Plataforma<b><br>';
                        htmlcorreo+='<a href="'+_sysUrlBase_+'">'+_sysUrlBase_+'</a>';

                        var fd2 = new FormData();                       
                        fd2.append('mensaje', htmlcorreo);
                        fd2.append('paratodos', false);
                        fd2.append('asunto', 'Cuenta de Acceso a plataforma');
                        fd2.append('paraemail', JSON.stringify([{nombre:'',email:email}]));

                        $.ajax({
                          url: _sysUrlBase_+'/sendemail/enviarcorreoall',
                          type: "POST",
                          data:  fd2,
                          contentType: false,
                          processData: false,
                          dataType:'json',
                          cache: false, 
                          success: function(data){ 
                            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
                          },
                           error: function(e){ console.log(e); }
                        });                       
                        <?php if($this->documento->plantilla!='modal'){?>
                        window.history.back();
                        <?php }else{ ?>
                        frm.closest('.modal').modal('hide');
                        <?php }?>
                      }
                      btn.attr('disabled', false);
                    },
                    error: function(e){ console.log(e); btn.attr('disabled', false); },
                    complete: function(xhr){ btn.attr('disabled', false); }
                  });
                }
            });
          });
        </script>
      </div>
    </div>
  </div>
</div>