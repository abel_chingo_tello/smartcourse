<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->gruposdocente)) $comodocente=$this->gruposdocente;
if(!empty($this->comoalumno)) $comoalumno=$this->comoalumno;
?>
<div class="panel">
	<div class="panel-body" >
    <?php if(!empty($comodocente)){?>
    <div class="col-md-12 text-center"><h4><?php echo JrTexto::_('As a teacher'); ?></h4></div>
    <table class="table table-striped table-responsive" id="table_<?php echo $idgui; ?>">
  	<thead>
    	<tr class="headings">
      		<th>#</th>
        	<!--th><?php echo JrTexto::_("Study Group") ;?></th-->
          <th><?php echo JrTexto::_("Course"); ?></th>
          <th><?php echo JrTexto::_("Type") ;?></th>        	
        	<th><?php echo JrTexto::_("Teacher") ;?></th>        	
        	<th><?php echo JrTexto::_("Ubication") ;?></th>
          <!--th><?php echo JrTexto::_("Status") ;?></th--> 
        	<!--th class="sorting_disabled"><span class="nobr"><?php //echo JrTexto::_('Actions');?></span></th-->
    	</tr>
  	</thead>
  	<tbody>
  		<?php 
  		$i=0;
  		$url=$this->documento->getUrlBase();
  		if(!empty($comodocente))
  			foreach ($comodocente as $per){ 
            //$dt=$this->datosAlumno($per["idalumno"]);
  					$i++;
  					?>
  				  <tr data-id="<?php echo @$per["iddocente"]; ?>">
  					<td><?php echo $i; ?></td>
  					<!--td class="fullnombre"><b><?php //echo @$per["strgrupoaula"];?></td-->
            <td><?php echo @$per["strcurso"];?></td> 
            <td><?php echo @$per["strtipogrupo"];?></td>  					
            <td><?php echo @$per["strdocente"];?></td>  				  					
  					<td><?php echo @$per["strlocal"]."<br>".@$per["strambiente"];?></td>   					
  					<!--td><?php echo @$per["strestadogrupo"];?></td-->
  				</tr>
  		<?php }	?>
    </tbody>
    </table>
    <?php } ?>
    <hr>
    <?php if(!empty($comoalumno)){?>
    <div class="col-md-12 text-center"><h4><?php echo JrTexto::_('As a Student'); ?></h4></div>
    <table class="table table-striped table-responsive" id="table_2<?php echo $idgui; ?>">
    <thead>
      <tr class="headings">
          <th>#</th>
          <!--th>DNI: <?php //echo JrTexto::_("Study Group") ;?></th-->
          <th><?php echo JrTexto::_("Course"); ?></th>
          <th><?php echo JrTexto::_("Type") ;?></th>          
          <th><?php echo JrTexto::_("Teacher") ;?></th>         
          <th><?php echo JrTexto::_("Ubication") ;?></th>
          <!--th><?php echo JrTexto::_("Status") ;?></th--> 
          <!--th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th-->
      </tr>
    </thead>
    <tbody>
      <?php 
      $i=0;
      $url=$this->documento->getUrlBase();
      if(!empty($comoalumno))
        foreach ($comoalumno as $per){ 
            $dt=$this->getgrupoauladetalle($per["idgrupoauladetalle"]);
            $i++;
            ?>
            <tr data-id="<?php echo @$per["iddocente"]; ?>">
            <td><?php echo $i; ?></td>
            <!--td class="fullnombre"><b><?php echo @$dt["strgrupoaula"];?></td-->
             <td><?php echo @$per["strcurso"];?></td>
            <td><?php echo @$dt["strtipogrupo"];?></td>            
            <td><?php echo @$per["strdocente"];?></td>                      
            <td><?php echo @$dt["strlocal"]."<br>".@$dt["strambiente"];?></td>            
            <!--td><?php //echo @$dt["strestadogrupo"];?></td-->
          </tr>
      <?php } ?>
    </tbody>
    </table>
    <?php } ?>
	</div>       
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#table_<?php echo $idgui; ?>').DataTable({
			"searching": false,
      		"processing": false
			<?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
		});
    $('#table_2<?php echo $idgui; ?>').DataTable({
      "searching": false,
          "processing": false
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    })
	})
</script>