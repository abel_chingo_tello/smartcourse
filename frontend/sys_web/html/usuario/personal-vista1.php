<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
if(!empty($this->datos)) $personal=$this->datos;
$estados[1]=JrTexto::_('Active');
$estados[0]=JrTexto::_('Inactive');
$personaltmp=array();
?>
	<div class="table-striped table-responsive" >
<table class="table  " id="table_<?php echo $idgui; ?>">
  	<thead>
    	<tr class="headings">
      		<th>#</th>
         
        	<th><?php echo ucfirst(JrTexto::_("Name"));?></th>
        	<th><?php echo ucfirst(JrTexto::_("Gender")); ?></th>
        	<th><?php echo ucfirst(JrTexto::_("Telephone"));?></th>
        	<?php if(empty($datareturn)){?>
          <th><?php echo ucfirst(JrTexto::_("Email"));?></th>
        	<th><?php echo ucfirst(JrTexto::_("User")); ?></th> 
          <?php } ?>                 
        	<!--th><?php //echo JrTexto::_("Rol"); ?></th-->
        	<th><?php echo ucfirst(JrTexto::_("Photo")); ?></th>
        	<th><?php echo ucfirst(JrTexto::_("State")); ?></th>                  
        	<th class="sorting_disabled"><span class="nobr"><?php echo ucfirst(JrTexto::_('Actions'));?></span></th>
    	</tr>
  	</thead>
  	<tbody>
  		<?php 
  		$i=0;
  		$url=$this->documento->getUrlBase();
  		if(!empty($personal))
  			foreach ($personal as $per){ 
            if(!in_array($per["idpersona"],$personaltmp)){
              array_push($personaltmp,$per["idpersona"]);
  					$i++;
  					$fullnombre=$per["ape_paterno"]." ".$per["ape_materno"].", ".$per["nombre"];
  					?>
  				<tr data-id="<?php echo $per["idpersona"]; ?>" data-dni="<?php echo $per["dni"]; ?>" data-nombre="<?php echo $fullnombre;?>">
  					<td><?php echo $i; ?></td>
  					<td class="fullnombre" data-nombre="<?php echo $fullnombre;?>"><?php echo $per["strtipodoc"].": ".$per["dni"]."<br>".$fullnombre;?></td>
  					<td><?php echo $per["strsexo"];?></td>
  					<td><?php echo $per["telefono"]."<br>".$per["celular"];?></td>
  					<?php if(empty($datareturn)){?><td><?php echo $per["email"]; ?></td>
  					<td><?php echo $per["usuario"]; ?></td> <?php } ?> 
  					<td><img class="img-circle img-responsive" src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo !empty($per["foto"])?$per["foto"]:'user_avatar.jpg' ?>" style="max-height:40px; max-width:40px;"></td>
  					<td><a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="<?php echo $per["idpersona"];?>"> <i class="fa fa<?php echo $per["estado"]==1?'-check':''; ?>-circle-o fa-lg"></i> <?php echo $estados[$per["estado"]]; ?></a>
  					</td>
  					<td class="text-center">
              <?php if(!empty($datareturn)){?>
              <a class="btn-selected btn btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected')); ?>"><i class="fa fa-hand-o-down"></i></a>
              <?php }else{ ?>
  						<a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>personal/perfil/?idpersona=<?php echo $per["idpersona"]; ?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"><i class="fa fa-eye"></i></a>
              <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>personal/formulario/?idpersona=<?php echo $per["idpersona"]; ?>&rol=<?php echo @$this->idrol; ?>&return=personal" data-titulo="<?php echo ucfirst(JrTexto::_('Personal'))." ".JrTexto::_('Edit'); ?>"><i class="fa fa-pencil"></i></a>
              <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>personal/cambiarclave/?idpersona=<?php echo $per["idpersona"]; ?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-key"></i></a>
							<a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>personal/roles/?idpersona=<?php echo $per["idpersona"]; ?>" data-titulo="<?php echo JrTexto::_('Roles'); ?>"><i class="fa fa-user-secret"></i></a>							
							<?php /*if($this->idrol=='2'){?>
              <!--a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?idpersona=<?php echo ($per["idpersona"]); ?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a-->
              <!--a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horarios/?idpersona=<?php echo $per["idpersona"]; ?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a-->
              <?php } */ ?>
              <a class="btn-eliminar btn btn-xs" href="javascript:;" data-titulo="<?php echo JrTexto::_('delete'); ?>" data-id="<?php echo $per["idpersona"]; ?>&rol=<?php echo @$this->idrol; ?>" ><i class="fa fa-trash-o"></i></a>
              <?php } ?>
  					</td>
  				</tr>
  		<?php }}	?>
    </tbody>
</table>
	</div>       
<script type="text/javascript">
	$(document).ready(function(){
		$('#table_<?php echo $idgui; ?>').DataTable({
      'pageLength': 50,
			"searching": false,
      "processing": false
			<?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
		}).on('click','.btn-selected',function(){
			var pnltr=$(this).closest('tr')
      var id=pnltr.attr('data-id');
			var dni=pnltr.attr('data-dni');
      var nombre=pnltr.attr('data-nombre').trim();   
      <?php if(!empty($ventanapadre)){ ?>
      var datareturn={id:id,nombre:nombre,dni:dni}; 
      var tmpobj=$('.tmp<?php echo $ventanapadre ?>')
      if(tmpobj.length){
        tmpobj.attr('data-return',JSON.stringify(datareturn));
				tmpobj.attr('data-idpersona',id).attr('data-nombre',nombre).attr('data-dni',dni);
        tmpobj.on('returndata').trigger('returndata');
      } 
      $(this).closest('.modal').modal('hide'); 
      <?php } ?> 
    });
	})
</script>