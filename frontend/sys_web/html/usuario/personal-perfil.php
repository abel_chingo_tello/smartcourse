<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'static/media/usuarios/user_avatar.jpg';
$ifoto=strripos($fotouser, "static/");
if($ifoto!='') $fotouser=$this->documento->getUrlBase().substr($fotouser,$ifoto);
if($ifoto==''){
  $ifoto=strripos($fotouser, "/");
  $ifoto2=strripos($fotouser, ".");
  if($ifoto!='' && $ifoto2!='') $fotouser=$this->documento->getUrlBase().'static/media/usuarios'.substr($fotouser,$ifoto);
  elseif($ifoto2!='') $fotouser=$this->documento->getUrlBase().'static/media/usuarios/'.$fotouser;
  else $fotouser=$this->documento->getUrlBase().'static/media/usuarios/user_avatar.jpg';
}

$experiencias=$this->experiencias;
$metas=$this->metas;
$referencias=$this->referencias;
$educacion=$this->educacion;
?>
<style>
  .titulo-p{
    float: none !important;
    font-size: 24px !important;
    font-weight: bold !important;
    text-align: center !important;
    width: 100% !important;
  }
  .x_title{
      border-bottom: 2px solid #E6E9ED;
      padding: 1px 5px 6px;
      margin-bottom: 10px;
  }

</style>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2 class="titulo-p text-center"><?php echo JrTexto::_('My user profile');?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content view">
        <div class="col-md-1 col-sm-1"></div>
        <div class="col-md-4 col-sm-4">
          <div class="profile_img">
            <div id="crop-avatar">
              <!-- Current avatar -->
              <img class="img-responsive avatar-view" src="<?php echo $fotouser; ?>" alt="Foto de Usuario" title="Change the avatar" style="width: 100%; border: solid;">
            </div>
          </div>
          <h3 class="text-center">
            <?php echo ucfirst(@$frm["ape_paterno"]).' '.ucfirst(@$frm["ape_materno"]).', '.ucfirst(@$frm["nombre"]); ?>
          </h3>
        </div>
        <div class="col-md-6 col-sm-6 text-center">
          <table class="table table-striped text-left">
            <tbody>
              <tr>
                <td>
                  <i class="fa fa-file user-profile-icon"></i>
                </td>
                <td><?php echo JrTexto::_('Id number') ?></td>
                <td>
                  <?php echo @$frm["dni"]; ?>
                </td>
              </tr>
              <tr>
                <td>
                  <i class="fa fa-tag user-profile-icon"></i>
                </td>
                <td><?php echo JrTexto::_('Gender') ?></td>
                <td>
                  <?php echo @$frm["strsexo"]; ?>
                </td>
              </tr>
              <tr>
                <td>
                  <i class="fa fa-info user-profile-icon"></i>
                </td>
                <td><?php echo JrTexto::_('Age');?></td>
                <td>
                  <?php
                    if(!empty($frm["fechanac"])){
                      list($Y,$m,$d) = explode("-",$frm["fechanac"]);
                      echo ( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y )." ".JrTexto::_('Years');
                    } 
                  ?>
                </td>
              </tr>
              <tr>
                <td>
                  <i class="fa fa-envelope user-profile-icon"></i>
                </td>
                <td><?php echo JrTexto::_('Email');?></td>
                <td>
                  <?php echo @$frm["email"]; ?>
                </td>
              </tr>
              <tr>
                <td>
                  <i class="fa fa-phone user-profile-icon"></i>
                </td>
                <td><?php echo JrTexto::_('Mobile'); ?></td>
                <td>
                  <?php echo @$frm["celular"]; ?>
                </td>
              </tr>
            </tbody>
          </table>

          <button type="button" class="btn btn-info" onclick="imprimir_()"><i class="fa fa-print"></i> <?php echo JrTexto::_('Print') ?></button>
          <button type="button" class="btn btn-warning" onclick="editar_()"><i class="fa fa-pencil"></i> Editar</button>

        </div>
        <div class="col-md-1 col-sm-1"></div>
      </div> 
      <form id="frmSavePerfil">
          <input type="hidden" id="idpersona" name="idpersona" value="<?php echo @$frm["idpersona"]; ?>">
        <div class="x_content edit" style="display: none;">
          <div class="col-md-1 col-sm-1"></div>
          <div class="col-md-6 col-sm-6 text-center">
            <table class="table table-striped text-left">
              <tbody>
                <tr>
                  <td>
                    <i class="fa fa-file user-profile-icon"></i>
                  </td>
                  <td><?php echo JrTexto::_('Id number') ?></td>
                  <td>
                    <input type="text" id="dni" name="dni" value="<?php echo @$frm["dni"]; ?>" style="width: 100%;" readonly="">
                  </td>
                </tr>
                <tr>
                  <td>
                    <i class="fa fa-circle-o user-profile-icon"></i>
                  </td>
                  <td><?php echo JrTexto::_('Name') ?></td>
                  <td>
                    <input type="text" id="nombre" name="nombre" value="<?php echo @$frm["nombre"]; ?>" style="width: 100%;">
                  </td>
                </tr>
                <tr>
                  <td>
                    <i class="fa fa-circle-o user-profile-icon"></i>
                  </td>
                  <td><?php echo JrTexto::_("Father's last name") ?></td>
                  <td>
                    <input type="text" id="ape_paterno" name="ape_paterno" value="<?php echo @$frm["ape_paterno"]; ?>" style="width: 100%;">
                  </td>
                </tr>
                <tr>
                  <td>
                    <i class="fa fa-circle-o user-profile-icon"></i>
                  </td>
                  <td><?php echo JrTexto::_("Mother's last name") ?></td>
                  <td>
                    <input type="text" id="ape_materno" name="ape_materno" value="<?php echo @$frm["ape_materno"]; ?>" style="width: 100%;">
                  </td>
                </tr>
                <tr>
                  <td>
                    <i class="fa fa-tag user-profile-icon"></i>
                  </td>
                  <td><?php echo JrTexto::_('Gender') ?></td>
                  <td>
                    <select name="sexo" id="sexo" style="width: 100%;">
                      <option value="M" <?php echo (@$frm["sexo"] == "M") ? 'selected=""' : ''; ?>>Masculino</option>
                      <option value="F" <?php echo (@$frm["sexo"] == "F") ? 'selected=""' : ''; ?>>Femenino</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    <i class="fa fa-info user-profile-icon"></i>
                  </td>
                  <td><?php echo JrTexto::_('Date of birthday');?></td>
                  <td>
                    <input type="date" id="fechanac" name="fechanac" value="<?php echo @$frm["fechanac"]; ?>" style="width: 100%;">
                  </td>
                </tr>
                <tr>
                  <td>
                    <i class="fa fa-envelope user-profile-icon"></i>
                  </td>
                  <td><?php echo JrTexto::_('Email');?></td>
                  <td>
                    <input type="email" id="email" name="email" value="<?php echo @$frm["email"]; ?>" style="width: 100%;">
                  </td>
                </tr>
                <tr>
                  <td>
                    <i class="fa fa-phone user-profile-icon"></i>
                  </td>
                  <td><?php echo JrTexto::_('Mobile'); ?></td>
                  <td>
                    <input type="text" id="celular" name="celular" value="<?php echo @$frm["celular"]; ?>" style="width: 100%;">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="profile_img">
              <div id="crop-avatar">
                <!-- Current avatar -->
                <img id="upfile1" class="img-responsive avatar-view" src="<?php echo $fotouser; ?>" alt="Foto de Usuario" title="Change the avatar" style="width: 100%; border: solid;">
                <input type="hidden" id="fotouser" value="<?php echo $fotouser; ?>">
                <input type="file" id="file1" name="file1" accept="image/*" capture style="display:none"/>
                <input type="hidden" id="txtvalidarfile" name="txtvalidarfile" value="0">
              </div>
            </div>
            <br>
            <div class="text-center">
              <button type="button" class="btn btn-warning" onclick="cancelar_()"><i class="fa fa-arrow-left"></i> Cancelar</button>
              <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save') ?></button>
            </div>
          </div>
          <div class="col-md-1 col-sm-1"></div>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  function imprimir_(){
    window.print();
  }
  function editar_(){
    $.post(_sysUrlBase_ + 'json/persona/listado',
    {idpersona: $("#idpersona").val()},
    function(data, status) {
      if (data.code === 200) {
        var p = data.data[0];
        $("#dni").val(p.dni);
        $("#nombre").val(p.nombre);
        $("#ape_paterno").val(p.ape_paterno);
        $("#ape_materno").val(p.ape_materno);
        $("#sexo").val(p.sexo);
        $("#fechanac").val(p.fechanac);
        $("#email").val(p.email);
        $("#celular").val(p.celular);
      }
    }, 'json');
    $(".edit").show();
    $(".view").hide();
  }
  function cancelar_(){
    $(".view").show();
    $(".edit").hide();
  }

  $("#upfile1").click(function () {
    $("#file1").trigger('click');
});

var input = document.getElementById('file1'); // see Example 4
input.onchange = function () {
    var file = input.files[0];
    drawOnCanvas(file);
};

function drawOnCanvas(file) {
    try {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#upfile1").attr("src", e.target.result);
            $("#txtvalidarfile").val("1");
        };
        reader.readAsDataURL(file);
    } catch (error) {
        $("#upfile1").attr("src", $("#fotouser").val());
        $("#txtvalidarfile").val("0");
    }
}
  $("#frmSavePerfil").submit(function(evt){
    evt.preventDefault();
    var inputFileImage = document.getElementById("file1");
    var file = inputFileImage.files[0];
    var data = new FormData();
    data.append('idpersona', $("#idpersona").val());
    data.append('dni', $("#dni").val());
    data.append('nombre', $("#nombre").val());
    data.append('ape_paterno', $("#ape_paterno").val());
    data.append('ape_materno', $("#ape_materno").val());
    data.append('sexo', $("#sexo").val());
    data.append('fechanac', $("#fechanac").val());
    data.append('email', $("#email").val());
    data.append('celular', $("#celular").val());
    data.append('archivo', file);
    data.append('validarFile', $("#txtvalidarfile").val());
    $.ajax({
        url: _sysUrlBase_+'json/persona/editarPerfil',
        type: 'POST',
        contentType: false,
        data: data,
        processData: false,
        cache: false,
        success: function(data) {
            var data = JSON.parse(data);
            if(data.code === 200){
              if($("#txtvalidarfile").val() == "1"){
                window.parent.actualizarFotoUsuActual(data.foto);
                location.reload();
              } else {
                location.reload();
              }
            }
        }
    });
  });
</script>