<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlBase();?>/roles'">&nbsp;<?php echo JrTexto::_('DRE'); ?></a></li-->
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>">
  <div class="col-md-12 col-sm-12 col-xs-12" id="pnlbotonesde<?php echo $idgui; ?>">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <table class="table table-striped table-responsive tableimport<?php echo $idgui; ?>">
          <thead>
            <tr class="tr01">              
              <th><?php echo ucfirst(JrTexto::_("Rol")); ?></th>
              <th><?php echo ucfirst(JrTexto::_('Actions')); ?></th>
            </tr>
          </thead>
          <tbody>
          <?php 
          $hayroles=array();
          if(!empty($this->roles)){
            foreach($this->roles as $rol){
              $hayroles[]=$rol["idrol"];
              ?>
              <tr class="tr01" data-iddetalle="<?php echo $rol["iddetalle"]; ?>" data-idrol="<?php echo $rol["idrol"]; ?>" data-rolname="<?php echo $rol["rol"]; ?>" data-idpersona="<?php echo $rol["idpersonal"] ?>">
              <?php if($rol["idrol"]==1||$rol["idrol"]==5||$rol["idrol"]==9){?>
                <td><?php echo $rol["rol"];?></td>
                <td><a class="btn btn-xs btneliminarrol"  href="#"><i class="fa fa-trash"></i></a>
              <?php }elseif($rol["idrol"]==2){ ?>
                <td><?php echo $rol["rol"];?></td>
                <td><a class="btn btn-xs btnverroldocente" href="#"  ><i class="fa fa-eye"></i></a>
               
              <?php }elseif($rol["idrol"]==3){ ?>
                <td><?php echo $rol["rol"];?></td>
                <td><a class="btn btn-xs btnverrolalumno" href="#" ><i class="fa fa-eye"></i></a>
              <?php }elseif($rol["idrol"]==4||$rol["idrol"]==6){ ?>
                <td><?php echo $rol["rol"]." - ".@$this->datospersona["strlocal"];?></td>
                <td><!--a class="btn btn-xs btnveriiee" href="#" ><i class="fa fa-eye"></i></a-->
                <a class="btn btn-xs btneditiee" href="#" ><i class="fa fa-pencil"></i></a>
                <a class="btn btn-xs btneliminarrol" href="#" ><i class="fa fa-trash"></i></a>
              <?php }elseif($rol["idrol"]==7){ ?>
                <td><?php echo $rol["rol"]." - ".@$this->datospersona["strugel"];?></td>
                <td><!--a class="btn btn-xs btnverugel" href="#" ><i class="fa fa-eye"></i></a-->
                <a class="btn btn-xs btneditugel" href="#" ><i class="fa fa-pencil"></i></a>
                <a class="btn btn-xs btneliminarrol" href="#" ><i class="fa fa-trash"></i></a>
              <?php }elseif($rol["idrol"]==8){ ?>
                <td><?php echo $rol["rol"]." - ".@$this->strdre;?></td>
                <td><!--a class="btn btn-xs btnverdre" href="#" ><i class="fa fa-eye"></i></a-->
                <a class="btn btn-xs btneditdre" href="#" ><i class="fa fa-pencil"></i></a>
                <a class="btn btn-xs btneliminarrol" href="#" ><i class="fa fa-trash"></i></a>
              <?php } ?>
              </td>
            </tr>
          <?php }} ?>
          </tbody>
        </table>
        <a class="btn btn-warning btnagregarrol<?php echo $idgui; ?>" href="javascript:void(0)" ><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add')?></a>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 hide" id="paneldatosaimportar<?php echo $idgui; ?>" >
  <div class="panel">
  <div class="panel-body">
          <input type="hidden" id="iddetallerol_" value="0">
          <input type="hidden" id="idpersonarol_" value="<?php echo $this->datospersona["idpersona"]; ?>">
          <div class="row">
              <div class="col-xs-6 col-sm-4 col-md-3 form-group">
                  <label><?php echo JrTexto::_('Rol'); ?></label>
                  <div class="cajaselect">            
                  <select id="_fkcbrol" name="rol" class="form-control" >                  
                    <?php if(!empty($this->fkrol))
                      foreach ($this->fkrol as $fkrol){
                        // $hayroles[]=3;
                        // $hayroles[]=2;
                        if($fkrol['idrol'] == '1' && $this->user['idrol'] != '1') continue;
                        if(in_array($fkrol["idrol"],$hayroles)) continue; 
                        
                        ?>
                        <option value="<?php echo $fkrol["idrol"]?>" <?php echo $fkrol["idrol"]==@$this->idrol?"selected":""; ?> ><?php echo ucfirst($fkrol["rol"]); ?></option><?php } ?>                        
                  </select>
              </div>
            </div>
            <?php if($this->user['idproyecto'] == 3): ?>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group _pnldocente<?php echo $idgui;?>" >
              <label><?php echo ucfirst(JrTexto::_('Dre')); ?></label>
              <div class="cajaselect">
                <select id="_fkcbdre" name="dre" class="form-control" ><?php 
                    if(!empty($this->dress))
                    foreach ($this->dress as $fk) { ?><option <?php echo @$this->iddre==$fk["ubigeo"]?'selected="selected"':'';?> value="<?php echo $fk["ubigeo"]?>" ><?php echo $fk["descripcion"] ?></option><?php } ?>
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group _pnldocente<?php echo $idgui;?>" >
              <label><?php echo ucfirst(JrTexto::_('UGEL')); ?></label>
              <div class="cajaselect">
                <select id="_fkcbugel" name="ugel" class="form-control" ></select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group _pnldocente<?php echo $idgui;?>" >
              <label><?php echo ucfirst(JrTexto::_('IIEE')); ?></label>
              <div class="cajaselect">
                <select id="_fkcbiiee" name="iiee" class="form-control" ></select>
              </div>
            </div>
            <?php endif; ?>
            <div class="col-xs-12 col-sm-6 col-md-3 text-center _pnlnodocente<?php echo $idgui;?>"><br>
                <a class="btn btn-primary  btnsaverolpersona" ><i class="fa fa-save"></i> <?php echo JrTexto::_('Save')?></a>
              </div>
            </div>
            </div>
          </form>
</div>
</div>


<script type="text/javascript">
var txtatention='<?php echo JrTexto::_('Attention');?> ';
var txtiiee='<?php echo JrTexto::_('IIEE');?> ';
var txtcurso='<?php echo JrTexto::_('Course');?> ';
var txtgrado='<?php echo JrTexto::_('Grade');?> ';
var txtseccion='<?php echo JrTexto::_('Section');?> ';
var idlocaltmp='<?php echo !empty($this->datospersona["idlocal"])?$this->datospersona["idlocal"]:'0';?>';
var idugeltmp='<?php echo !empty($this->datospersona["idugel"])?$this->datospersona["idugel"]:'0';?>';
var iddretmp='<?php echo !empty($this->iddre)?$this->iddre:'0';?>';
console.log(iddretmp,idugeltmp,idlocaltmp);
var secciones=[];
secciones[null]='';secciones[0]='';secciones[1]='A';secciones[2]='B';secciones[3]='C';secciones[4]='D';secciones[5]='E';secciones[6]='F';secciones[7]='G';secciones[8]='H';
$(document).ready(function(){
  $('#vent-<?php echo $idgui;?>').on('click','.btneliminarrol',function(ev){
    ev.preventDefault();
    var btn=$(this);
    var iddetalle=$(this).closest('tr').attr('data-iddetalle')||-1;
    if(iddetalle==-1) return;
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'persona_rol', 'eliminar', iddetalle);
        if(res) btn.closest('tr').remove();
      }
    }); 
  }).on('click','.btnverrolalumno',function(ev){
    var btn=$(this);
    var tr=$(this).closest('tr');
    var idrol=tr.attr('data-idrol')||-1;
    var idpersona=tr.attr('data-idpersona')||-1;
    if(!$(this).children('i').hasClass('fa-eye')){
      tr.siblings('tr.tralumno'+idpersona).remove();
      $(this).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
    }else{
      $(this).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
      tr.attr('id','tralumno'+idpersona);
      if(idrol==-1||idpersona==-1) return;
      var formData = new FormData();
        formData.append('idalumno', idpersona);
        sysajax({
            fromdata:formData,
            url:_sysUrlBase_+'/acad_matricula/buscarjson',
            msjatencion:txtatention,
            type:'json',
            showmsjok : false,
            callback:function(data){
              var dt=data.data;
              if(dt.length>0){
                html='';
                tr.siblings('tr.tralumno'+idpersona).remove();
                $.each(dt,function(i,v){
                  html+='<tr class="tralumno'+idpersona+'" ><td> -- '+v.nombre+' </td><td> <a class="eliminarmatricula" data-padre="tralumno'+idpersona+'" data-idmatricula="'+v.idmatricula+'"><i class="fa fa-trash"></i></a></td></tr>';
                })
                $(html).insertAfter(tr);
              }else{
                if(btn.siblings('.btneliminarrol').length==0)
                $(' <a href="#" class="btn btn-xs btneliminarrol"><i class=" fa fa-trash"></i></a>').insertAfter(btn);
              }
            }
        });
    }
  }).on('click','.btnverroldocente',function(ev){
    var btn=$(this);
    var tr=$(this).closest('tr');
    var idrol=tr.attr('data-idrol')||-1;
    var idpersona=tr.attr('data-idpersona')||-1;
    if(!$(this).children('i').hasClass('fa-eye')){
      tr.siblings('tr.trdocente'+idpersona).remove();
      $(this).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
    }else{
      $(this).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
      tr.attr('id','trdocente'+idpersona);
      if(idrol==-1||idpersona==-1) return;
      var formData = new FormData();
        formData.append('iddocente', idpersona);
        sysajax({
            fromdata:formData,
            url:_sysUrlBase_+'/acad_grupoauladetalle/buscarjson',
            msjatencion:txtatention,
            type:'json',
            showmsjok : false,
            callback:function(data){
              var dt=data.data;
              if(dt.length>0){
                html='';
                tr.siblings('tr.trdocente'+idpersona).remove();
                $.each(dt,function(i,v){
                  html+='<tr class="trdocente'+idpersona+'" ><td> -- <b>'+txtiiee+':</b>'+v.strlocal+', <b>'+txtcurso+':</b>'+v.strcurso+', <b>'+txtgrado+':</b>'+v.idgrado+', <b>'+txtseccion+':</b>'+secciones[v.idsesion]+' </td><td> <a class="eliminarasignaciondocente" data-padre="trdocente'+idpersona+'" data-idgrupoauladetalle="'+v.idgrupoauladetalle+'"><i class="fa fa-trash"></i></a></td></tr>';
                })
                $(html).insertAfter(tr);
              }else{
                if(btn.siblings('.btneliminarrol').length==0)
                $(' <a href="#" class="btn btn-xs btneliminarrol"><i class=" fa fa-trash"></i></a>').insertAfter(btn);
              }
            }
        });
    }
  }).on('click','.eliminarmatricula',function(ev){
      var tr=$(this).closest('tr');
      var idmatricula=$(this).attr('data-idmatricula')||-1;
      var datapadre=$(this).attr('data-padre');
      if(idmatricula==-1) return;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_matricula', 'eliminar', idmatricula);
          if(res){
            if(tr.siblings('tr.'+datapadre).length==0){
              var iddetalle=$('tr#'+datapadre).attr('data-iddetalle');
              var res = xajax__('', 'persona_rol', 'eliminar', iddetalle);
              if(res)$('tr#'+datapadre).remove();
            }
            tr.remove()
          }
        }
    });
  }).on('click','.eliminarasignaciondocente',function(ev){
      var tr=$(this).closest('tr');
      var idgrupoauladetalle=$(this).attr('data-idgrupoauladetalle')||-1;
      var datapadre=$(this).attr('data-padre');
      if(idgrupoauladetalle==-1) return;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_grupoauladetalle', 'setcampo', idgrupoauladetalle,'iddocente','0');
          if(res){           
            if(tr.siblings('tr.'+datapadre).length==0){
              var iddetalle=$('tr#'+datapadre).attr('data-iddetalle');
              var res = xajax__('', 'persona_rol', 'eliminar', iddetalle);
              if(res)$('tr#'+datapadre).remove();
            }
            tr.remove()
          }
        }
    });
    
 
  }).on('click','.btneditiee',function(ev){
    var idrol=$(this).closest('tr').attr('data-idrol');
    var idrolname=$(this).closest('tr').attr('data-rolname');
    $('#_fkcbrol').append('<option value="'+idrol+'" selected="selected">'+idrolname+'</option>');
    $('#_fkcbrol').trigger('change');
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide'); 
    $('.btnagregarrol<?php echo $idgui; ?>').hide();
  }).on('click','.btneditugel',function(ev){
    var idrol=$(this).closest('tr').attr('data-idrol');
    $('#_fkcbrol').val(idrol);
    $('#_fkcbrol').trigger('change');
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide'); 
    $('.btnagregarrol<?php echo $idgui; ?>').hide();
  }).on('click','.btneditdre',function(ev){
    var idrol=$(this).closest('tr').attr('data-idrol');
    $('#_fkcbrol').val(idrol);
    $('#_fkcbrol').trigger('change');
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide'); 
    $('.btnagregarrol<?php echo $idgui; ?>').hide();
  }).on('click','.btnagregarrol<?php echo $idgui; ?>',function(ev){
    $(this).hide();
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide');    
  })
  $('#_fkcbrol').change(function(ev){
    var rol=$(this).val()||'';
    if(rol!='1' && rol!='9' && rol!='5'){
      $('._pnldocente<?php echo $idgui;?>').show();
      $('._pnlnodocente<?php echo $idgui;?>').removeClass('col-md-6').addClass('col-md-3');
      if(rol=='7'){       
        $('#_fkcbiiee').closest('._pnldocente<?php echo $idgui;?>').hide();
      }else if(rol=='8'){        
        $('#_fkcbiiee').closest('._pnldocente<?php echo $idgui;?>').hide();
        $('#_fkcbugel').closest('._pnldocente<?php echo $idgui;?>').hide();        
      }
      if(iddretmp!=0){
        $('#_fkcbdre').val(iddretmp);
        $('select#_fkcbdre').trigger('change');
      }
    }else{
      $('._pnldocente<?php echo $idgui;?>').hide();
    }
  });


  $('select#_fkcbdre').change(function(ev){
      ev.preventDefault();
      var iddre=$(this).val();
      var fd2= new FormData();
      fd2.append("iddepartamento", iddre);
      sysajax({
        fromdata:fd2,
        url:_sysUrlBase_+'/ugel/buscarjson',
        callback:function(rs){
            $sel=$('#_fkcbugel');
            $sel.children('option').remove();
            dt=rs.data;
            $.each(dt,function(i,v){
              var selop=v.idugel==idugeltmp?'selected="selected"':'';
              $sel.append('<option value="'+v.idugel+'" '+selop+' >'+v.descripcion+'</option>');
            })
            $sel.trigger('change');
        }
      })
  })
  
  $('select#_fkcbugel').change(function(ev){
      ev.preventDefault();
      var idugel=$(this).val();
      var fd2= new FormData();
      fd2.append("idugel", idugel);
      sysajax({
      fromdata:fd2,
      url:_sysUrlBase_+'/local/buscarjson',
      callback:function(rs){
          $sel=$('#_fkcbiiee');
          $sel.children('option').remove();
          dt=rs.data;
          $.each(dt,function(i,v){
            var selop=v.idlocal==idlocaltmp?'selected="selected"':'';
            $sel.append('<option value="'+v.idlocal+'" '+selop+'>'+v.nombre+'</option>');
          })
      }})
  }) 
  
  $('#_fkcbrol').trigger('change');

  $('.btnsaverolpersona').on('click',function(ev){
    btn=$(this);
    var idrol=$('#_fkcbrol').val()||-1;
    var idpersona=$('#idpersonarol_').val()||-1;
    var iddetallerol=$('#iddetallerol_').val()||-1;
    if(idrol==-1||idpersona ==-1) return;
    var formData = new FormData();
        formData.append('txtIdrol', idrol);
        formData.append('txtIdpersonal', idpersona);
        formData.append('idempresa', '');
        formData.append('idproyecto', '');
        formData.append('idlocal', $('#_fkcbiiee').val());
        formData.append('idugel', $('select#_fkcbugel').val());
        sysajax({
            fromdata:formData,
            url:_sysUrlBase_+'/persona_rol/guardarPersona_rol',
            msjatencion:txtatention,
            type:'json',
            showmsjok : true,
            callback:function(data){
              btn.closest('.modal').modal('hide');
              setTimeout(function(){btn.closest('.modal').remove(); },2500);
            }
        });
  })
});


</script>

