<?php
// echo "alvaro";
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$ventanapadre = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!empty($this->datos)) $personal = $this->datos;
// var_dump($personal);
?>
<div class="panel">
	<div class="panel-body">
		<table class="table table-striped table-responsive" id="table_<?php echo $idgui; ?>">
			<thead>
				<tr class="headings">
					<th>#</th>
					<th>DNI: <?php echo ucfirst(JrTexto::_("Student")); ?></th>
					<th><?php echo JrTexto::_("Telephone"); ?></th>
					<th><?php echo JrTexto::_("Course"); ?></th>
					<th><?php echo JrTexto::_("Teacher"); ?></th>
					<?php if (empty($ventanapadre)) { ?>
						<!--th><?php //echo JrTexto::_("Email") ;
								?></th--> <?php } ?>
					<th><?php echo JrTexto::_("Ubication"); ?></th>

					<!--th><?php //echo JrTexto::_("Rol"); 
							?></th-->
					<th><?php echo JrTexto::_("Foto"); ?></th>
					<th><?php echo JrTexto::_("Estado"); ?></th>
					<th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 0;
				$url = $this->documento->getUrlBase();
				if (!empty($personal))
					foreach ($personal as $per) {
						$dt = $this->datosAlumno($per["idalumno"]);
						$i++;
				?>
					<tr data-id="<?php echo @$per["idalumno"]; ?>">
						<td><?php echo $i; ?></td>
						<td class="fullnombre"><b><?php echo @$per["idalumno"] . "</b><br>" . @$per["stralumno"]; ?></td>
						<td><?php echo @$dt["telefono"] . "<br>" . @$dt["celular"]; ?></td>
						<td><?php echo @$per["strcurso"]; ?></td>
						<td><?php echo @$per["strdocente"]; ?></td>
						<?php if (empty($ventanapadre)) { ?>
							<!--td><?php //echo @$dt["email"]; 
									?></td--><?php } ?>
						<td><?php echo @$dt["strlocal"] . "<br>" . @$dt["strambiente"]; ?></td>
						<td><img class="img-circle img-responsive" src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo !empty($dt["foto"]) ? $dt["foto"] : 'user_avatar.jpg' ?>" style="max-height:40px; max-width:40px;"></td>
						<td><a href="javascript:;" class="btn-chkoption" campo="estado" data-id="<?php echo @$per["idalumno"]; ?>"> <i class="fa fa<?php echo @$dt["estado"] == 1 ? '-check' : ''; ?>-circle-o fa-lg"></i> <?php echo @$estados[$dt["estado"]]; ?></a>
						</td>
						<td class="text-center">
							<?php if ($ismodal && !empty($ventanapadre)) { ?>
								<a class="btn-selected btn btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected')); ?>"><i class="fa fa-hand-o-down"></i></a>
							<?php } else { ?>
								<a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/perfil/?idpersona=<?php echo $per["idalumno"]; ?>" data-titulo="<?php echo JrTexto::_('Ficha') . " " . @$per["stralumno"]; ?>"><i class="fa fa-user"></i><i class="fa fa-eye"></i></a>
								<a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/formulario/?idpersona=<?php echo $per["idalumno"]; ?>&rol=alumno&datareturn=false;" data-titulo="<?php echo JrTexto::_('Personal') . " " . JrTexto::_('Edit'); ?>"><i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>
								<a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?idpersona=<?php echo $per["idalumno"]; ?>" data-titulo="<?php echo JrTexto::_('Change') . " " . JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
								<a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?idpersona=<?php echo $per["idalumno"]; ?>" data-titulo="<?php echo JrTexto::_('View') . " " . JrTexto::_('notes'); ?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>
								<a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horarios/?idpersona=<?php echo $per["idalumno"]; ?>" data-titulo="<?php echo JrTexto::_('View') . " " . JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a>
								<!--a class="btn-eliminar btn btn-xs" href="javascript:;" data-titulo="<?php echo JrTexto::_('delete'); ?>" data-id="<?php //echo @$per["idalumno"]; 
																																						?>" ><i class="fa fa-trash-o"></i></a-->
							<?php } ?>
						</td>
					</tr>
				<?php }	?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table_<?php echo $idgui; ?>').DataTable({
			"pageLength": 50,
			"searching": false,
			"processing": false
			<?php echo $this->documento->getIdioma() != 'EN' ? (',"language": { "url": "' . $this->documento->getUrlStatic() . '/libs/datatable1.10/idiomas/' . $this->documento->getIdioma() . '.json"}') : '' ?>
		}).on('click', '.btn-selected', function() {
			var id = $(this).closest('tr').attr('data-id');
			var nombre = $(this).closest('tr').find('.fullnombre').text().trim();
			<?php if (!empty($ventanapadre)) { ?>

				var datareturn = {
					id: id,
					nombre: nombre
				};
				var tmpobj = $('.tmp<?php echo $ventanapadre ?>')
				if (tmpobj.length) {
					tmpobj.attr('data-return', JSON.stringify(datareturn));
					tmpobj.on('returndata').trigger('returndata');
				}
				$(this).closest('.modal').modal('hide');
			<?php } ?>
		});
	})
</script>