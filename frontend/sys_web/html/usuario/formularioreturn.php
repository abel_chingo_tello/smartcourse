<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$return=!empty($_REQUEST["return"])?$_REQUEST["return"]:'false';
$frm=!empty($this->datos)?$this->datos:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';
$rol=!empty($_REQUEST["rol"])?$_REQUEST["rol"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
</style>
<div class="row">
<div class="col-md-12">
<div class="panel panel-body">
<input type="hidden" id="datareturn<?php echo $idgui ?>" value="<?php echo $return;?>">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" name="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"]; ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion; ?>">
    <input type="hidden" name="rol" id="rol<?php echo $idgui; ?>" value="<?php echo @$rol; ?>">
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-9">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Document type")); ?></label>
            <div class="cajaselect">
              <select name="tipodoc" id="tipodoc<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fktipodoc)) foreach ($this->fktipodoc as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>"  >
                    <?php echo $fk["nombre"] ?>
                    </option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left">N° <?php echo ucfirst(JrTexto::_('Id card')); ?></label>                    
              <input type="text" class="form-control" name="dni" id="tmpdni<?php echo $idgui; ?>" required="required" value="<?php echo @$frm["dni"]; ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4"><br>
          <a class="btn btn-info btnsearch<?php echo $idgui ?>" href="javascript:void(0)"><i class="fa fa-search"></i> <?php echo JrTexto::_('Search'); ?></a>          
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Names')); ?></label>                    
              <input type="text" class="form-control"  required="required" name="nombre" id="nombre<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["nombre"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Father's last name")); ?></label>                    
              <input type="text" class="form-control"  required="required" name="ape_paterno" id="ape_paterno<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["ape_paterno"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Mother's last name")); ?></label>                    
              <input type="text" class="form-control"  required="required" name="ape_materno" id="ape_materno<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["ape_materno"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Gender")); ?></label>
            <div class="cajaselect">
              <select name="sexo" id="sexo<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                  <option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> >
                    <?php echo $fksexo["nombre"] ?>
                    </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Marital Status")); ?></label>
            <div class="cajaselect">
              <select name="estado_civil" id="estado_civil<?php echo $idgui;?>" class="form-control">
                <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                  <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                    <?php echo $fkestado_civil["nombre"] ?>
                    </option>
                <?php } ?>                        
              </select>
            </div>
          </div>
        </div> 

        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Telephone')); ?></label>                    
              <input type="text" class="form-control"  required="required" name="telefono" id="telefono<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["telefono"]); ?>" > 
          </div> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Mobile number')); ?></label>                    
              <input type="text" class="form-control" name="celular" id="celular<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["celular"]); ?>" > 
          </div> 
        </div> 
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 text-center">        
          <div class="col-xs-12 col-sm-12 col-md-12 text-center contool">
            <div><label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Photo')); ?></label></div>
            <div style="position: relative;">
              <div class="toolbarmouse"><span class="btn"><i class="fa fa-pencil"></i></span></div>
              <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="foto_alumno img-responsive center-block thumbnail" id="foto<?php echo $idgui; ?>">
              <input type="file" class="input-file-invisible" name="foto" id="fileFoto<?php echo $idgui; ?>" accept="image/*">
            </div>      
          </div>                   
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Birthday")); ?></label>
            <div class="form-group">
              <div class="input-group date datetimepicker">
                <input type="text" class="form-control" required="required" name="fechanac" id="fechanac<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["fechanac"]); ?>"> 
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
              </div>
            </div>
            </div>
          </div>
      </div>
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-default btnretornar<?php echo $idgui; ?>" href="javascript:void(0)" ><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a>
        <button type="submit" class="btn btn-primary cerrarmodal"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
       
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
</div>
<script type="text/javascript">
 $(document).ready(function(){
    var changefoto=false;
    var msjatencion='<?php echo JrTexto::_('Attention');?>';
    var idgui='<?php echo !empty($idgui)?$idgui:now();?>';
    var buscaralumno<?php echo $idgui; ?>=function(){
     var dniobj=$('#tmpdni'+idgui);
      var dni=dniobj.val();
      if(dni=='') {
         mostrar_notificacion(msjatencion,'<?php echo JrTexto::_('empty information');?>','warning');
        dniobj.focus();
        return false;
      }
      $('#idpersona'+idgui).val(0);
       var formData = new FormData();       
      formData.append('dni', dni); 
     var data={
        fromdata:formData,
        url:_sysUrlBase_+'/personal/buscarjson',
        msjatencion:msjatencion,
        type:'json',
        //showmsjok : true,
        callback:function(rs){
          var dt=rs.data;
          if(dt[0]!=undefined){
            var rw=dt[0];
            $('#idpersona'+idgui).val(rw.idpersona);
            $('#nombre'+idgui).val(rw.nombre);
            $('#ape_paterno'+idgui).val(rw.ape_paterno);
            $('#ape_materno'+idgui).val(rw.ape_materno);
            $('#sexo'+idgui).val(rw.sexo);
            $('#telefono'+idgui).val(rw.telefono);
            $('#celular'+idgui).val(rw.celular);
            $('#fechanac'+idgui).val(rw.fechanac);
            $('#estado_civil'+idgui).val(rw.estado_civil||'S');
            if(rw.foto!=''){
              $('#foto'+idgui).attr('src',_sysUrlStatic_+'/media/usuarios/'+rw.foto);              
            }
           if($('#datareturn').val()!='false'){
             $('.btnseleccionar<?php echo $idgui ?>').removeClass('hide');
           }else{

           }
          }else{          
            $('.btnseleccionar<?php echo $idgui ?>').addClass('hide');
          }        
        }
      }
      sysajax(data);
    }
    $('.btnsearch'+idgui).click(function(){
      buscaralumno<?php echo $idgui; ?>();
    });
    $('#frm<?php echo $idgui;?>').bind({
     submit: function(event){
      event.preventDefault();
        btn=$(this);
        var myForm = document.getElementById('frm'+idgui);
        var formData = new FormData(myForm);
        var dreturn=$('#datareturn<?php echo $idgui ?>').val()||-1;
            if('actualuarmatricula123'==dreturn){
            formData.append('idlocal', $('.fkcbiiee').val()||'1');
            formData.append('idugel', $('.fkcbugel').val()||'1');
            }
        if(changefoto==true) formData.append('fotouser', $('.input-file-invisible')[0].files[0]);
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/personal/guardarDatos',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : true,
          callback:function(data){
            $('input.idpersona').val(data.newid);
            var dreturn=$('#datareturn<?php echo $idgui ?>').val()||-1;
            if('actualuarmatricula123'==dreturn){ // matricular alumno
                var fd = new FormData();
                fd.append('iddocente', $('#idteacher').val()||'');
                fd.append('idgrupoauladetalle', $('#idgrupoauladetalle').val()||'');
                fd.append('idiiee', $('.fkcbiiee').val()||'');
                fd.append('idcurso', $('.fkcbcurso').val()||'');
                fd.append('seccion', $('.fkcbseccion').val()||'');
                fd.append('grado', $('.fkcbgrado').val()||'');
                fd.append('ugel', $('.fkcbugel').val()||'');
                fd.append('idpersona', data.newid);
                var data={
                    fromdata:fd,
                    url:_sysUrlBase_+'/matricula/matricular',
                    msjatencion:'<?php echo JrTexto::_('Attention');?>',
                    type:'json',
                    showmsjok : true,
                    callback:function(data){
                        if($(this).closest('.modal').length)
                            $(this).closest('.modal').modal('hide');
                        else{
                            var dreturn=$('#datareturn<?php echo $idgui ?>').val()||-1;
                            if(dreturn ==-1)window.history.back();
                            else $('#'+dreturn).trigger('click');
                        }
                       console.log(data);
                    }
                }
                sysajax(data);
            }
          }
        }
        sysajax(data);
        return false;
      }
    });


    $('#fechanac<?php echo $idgui; ?>').datetimepicker({ //lang:'es',  //timepicker:false,
      format:'YYYY/MM/DD'
    }); 

    var inputFile = document.getElementById('fileFoto'+idgui);
    inputFile.addEventListener('change', mostrarImagen, false);
    function mostrarImagen(event) {
      var file=event.target.files[0];
      changefoto=true;
      var reader=new FileReader();      
      reader.onload=function(event){
        var img=document.getElementById('foto'+idgui);
        img.src=event.target.result;
      }
      reader.readAsDataURL(file);
    };

    <?php if(!empty($fcall)){ ?>
    $('.btnseleccionar<?php echo $idgui ?>').click(function(){      
        var tmpobj=$('.tmp<?php echo $fcall ?>');
        if(tmpobj.length){
            var dniobj=$('#tmpdni'+idgui);
            tmpobj.attr('data-returndni',$('#tmpdni'+idgui).val());
            tmpobj.attr('data-returnidpersona',$('#idpersona'+idgui).val());
            tmpobj.attr('data-returnnombre',$('#ape_paterno'+idgui).val()+' '+$('#ape_materno'+idgui).val()+', '+$('#nombre'+idgui).val());
            tmpobj.attr('data-return',dniobj.val());
            tmpobj.on('returndata').trigger('returndata');
        } 
        <?php if($this->documento->plantilla=='modal'){ ?>
        $(this).closest('.modal').modal('hide');
        <?php } ?>
    });
    <?php } ?>
    $('.btnretornar<?php echo $idgui; ?>').click(function(){
      if($(this).closest('.modal').length)
        $(this).closest('.modal').modal('hide');
      else{
          var dreturn=$('#datareturn<?php echo $idgui ?>').val()||-1;
          if(dreturn ==-1)window.history.back();
          else $('#'+dreturn).trigger('click');
      }
    })
  });
</script>