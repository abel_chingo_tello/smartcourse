<style>
    #cmp-unidades {
        display: grid;
        place-items: center;
    }

    #cmp-unidades .cont-unidades {
        width: 100%;
        display: grid;
        place-items: center;
        justify-content: center;
        row-gap: 1rem;
        column-gap: 2rem;
        grid-template-columns: repeat(auto-fit, 250px);
    }

    #cmp-unidades .card-unidad {
        height: 280px;
        display: grid;
        /* grid-template-rows: repeat(4, auto); */
        grid-template-rows: auto 1fr auto auto;
        grid-auto-flow: row;
        place-items: center;
        width: 100%;
        padding: 4px;
        row-gap: 7px;
        column-gap: 1rem;
        border-radius: 20px;
        transition: transform 0.4s;
    }

    #cmp-unidades .card-unidad:hover {
        box-shadow: 0 10px 10px 0 rgba(0, 0, 0, .1);
        transform: scale(0.9, 0.9);
    }

    #cmp-unidades .card-unidad .titulo {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        font-size: 1.5rem;
        color: black;
    }

    #cmp-unidades .card-unidad .roman-icon {
        border-radius: 50%;
        background-color: #ffffffd4;
        height: 3rem;
        width: 3rem;
        font-weight: bolder;
        text-align: center;
        transition: background-color 0.2s;
    }

    #cmp-unidades .card-unidad:hover .roman-icon {
        box-shadow: 0 10px 10px 0 rgba(0, 0, 0, .1);
        background-color: white;
    }


    #cmp-unidades .card-unidad>* {
        width: 100%;
        height: 100%;
        text-align: center;
    }

    #cmp-unidades .card-unidad .cont-img {
        display: grid;
        padding-left: 7px;
        padding-right: 7px;
    }

    #cmp-unidades .card-unidad .cont-img .img {
        height: 100%;
        width: 100%;
        border-radius: 20px;
        color: white;
        font-size: 2rem;
        display: grid;
        place-content: center;
    }

    .avance {
        color: #6b6b6b;
        font-size: 2rem;
        font-weight: 800;
    }

    .avance small,
    .avance .s-separator {
        font-weight: 800 !important;
    }

    .avance .s-separator {
        font-size: 1.9rem;
    }

    .avance>.fail {
        color: #ff5555;
    }

    .avance>.sucess {
        color: #00e600;
    }
</style>
<section id="cmp-unidades" class="col-sm-12 my-card my-shadow">
    <header>
        <h3><i><?php echo JrTexto::_('Content'); ?></i></h3>
        <div class="my-line"></div>
    </header>
    <div m-render m-for="$.arrUnidades, unidad,i" class="cont-unidades">
        <div class="card-unidad my-cursor-pointer" data-index="{{i}}">
            <div class="titulo">
                {{(
                    unidad.nombre=='Sin Unidad' && $.arrUnidades.length==1?
                        (
                        `
                        <div>Curso estructurado</div>
                        <div><?php echo JrTexto::_('By Sessions'); ?></div>
                        <i><small>(<?php echo JrTexto::_('View Content'); ?>)</small></i>
                        `
                        )
                    :
                        unidad.nombre
                )}}
                

            </div>
            <div class="cont-img">
                <div class="img" style="background-color:{{$.arrColors[i]}}">
                    <span class="roman-icon" style="color:{{$.arrColors[i]}}">
                    {{(
                        unidad.nombre=='Sin Unidad' ?
                            ( $.arrUnidades.length==1?
                                ('<i class="fa fa-eye" aria-hidden="true"></i>')
                            :
                                ('?')
                            )
                        :
                            (Mine.romanize(i+1))
                    )}}
                    </span>
                </div>

            </div>
            <div class="barra-progreso">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: {{unidad.progreso}}%;" aria-valuenow="{{unidad.progreso}}" aria-valuemin="0" aria-valuemax="100">{{(unidad.progreso).toFixed(2)}}%</div>
                </div>
            </div>
            <div class="avance"><span class="{{(Math.round(unidad.progreso*unidad.cantSesiones)==0?'fail':'sucess')}}">{{unidad.sesionesCompletadas}}</span><span class="s-separator">/</span><small>{{unidad.cantSesiones}}</small></div>
        </div>
    </div>
    <div m-render m-show="$.arrUnidades.length==0" class="my-msg">
        <?php echo JrTexto::_('This course has no units'); ?>...
    </div>
</section>

<?php include("unidadViewer.php") ?>
<script>
    class Unidades {
        defaults = {
            model: {
                arrUnidades: [],
            }
        };
        constructor(data) {
            Object.assign(this.defaults, data)
            this.mine = new Mine;
            this.render = new Render({
                instance: this,
                selector: '#cmp-unidades',
                model: this.defaults.model,
                render: false
            });
            this.selectedIndex = null;
            this.section = $('#cmp-unidades');
            this.cmpUnidadViewer = new UnidadViewer;

        }
        launch() {
            this.model = this.render.model;
            this.arrColors = Mine.getColors(this.render.model.arrUnidades.length);
            this.render.model.arrColors = this.arrColors;
            this.render.render();
            // console.log('working! cmp-unidades model:', this.model);
        }
        onRendered(huboCambios) { //cuando termina de renderizarse
            this.model = this.render.model;
            let htmlUnidad = this.section.find('.card-unidad');
            htmlUnidad.click(e => {
                this.onSelect(e.currentTarget);
            })
            // this.onSelect(htmlUnidad[0]);
            // console.log(' cmp-unidades renderizado con modelo:', this.model);
        }
        onSelect(htmlCurso) {
            let index = $(htmlCurso).attr('data-index');
            // if (index != this.selectedIndex) {
            this.selectedIndex = index;
            this.selected = this.model.arrUnidades[this.selectedIndex];
            this.cmpUnidadViewer.render.model.oUnidad = this.selected;
            this.cmpUnidadViewer.render.model.oCurso = this.model.oCurso;
            this.cmpUnidadViewer.render.model.oAlumno = this.model.oAlumno;
            this.cmpUnidadViewer.launch();
            // }
        }
    }
</script>