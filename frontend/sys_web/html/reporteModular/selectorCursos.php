<style>
    #cmp-selectorCursos .alumno {
        height: 100%;
    }

    #cmp-selectorCursos .cont-cursos {
        position: relative;
        display: flex;
        max-height: 202px;
        /* overflow-x: auto;
        overflow-y: hidden; */
    }

    #cmp-selectorCursos .cont-img>.nombre-curso {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        text-align: center;
        display: grid;
        place-content: center;

    }

    #cmp-selectorCursos .cont-img>.nombre-curso>.nombre-text {
        font-size: 2.5em;
        padding: 15px;
        font-weight: bold;
        color: rgb(255, 255, 255);
        text-shadow: 2px 2px 8px #040404;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    #cmp-selectorCursos .cont-cursos .card-curso {
        position: relative;
        /* border: 1px solid black; */
        width: 230px;
        height: 200px;
        display: inline-block;
        margin-right: 5px;
    }

    #cmp-selectorCursos .card-curso>.bottom-card {
        margin-top: 5px;
        padding-left: 3px;
        padding-right: 3px;
        position: absolute;
        bottom: 0;
        width: 100%;

    }

    #cmp-selectorCursos .progress-bar {
        /* padding-left: 2px; */
    }

    #cmp-selectorCursos .card-curso .selected-curso {

        margin-top: 3px;
        height: 3px;
        width: 100%;
    }

    #cmp-selectorCursos .selected-curso.active {
        background-color: greenyellow;
    }

    #cmp-selectorCursos .card-curso .progreso-curso {
        /* height: 20px; */
        width: 100%;
        /* background-color: dodgerblue; */
    }

    #cmp-selectorCursos .card-curso:hover .selected-curso {
        background-color: #fff70b40;
    }

    #cmp-selectorCursos .card-curso .cont-img {
        position: relative;
        padding: 7px;
        transition: transform 0.4s;
    }

    #cmp-selectorCursos .card-curso .cont-img:hover {
        transform: scale(0.9, 0.9);
    }

    #cmp-selectorCursos .card-curso .cont-img::before {
        box-shadow: 0 10px 10px 0 rgba(0, 0, 0, .1);
        content: "";
        position: absolute;
        background: #2098D1;
        border-radius: 50%;
        width: 1px;
        height: 1px;
        background: transparent;
        transition: width .2s, height .2s;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto;

    }

    #cmp-selectorCursos .card-curso .cont-img:hover::before {
        background-color: #15b4ff;
        width: 100%;
        height: 100%;
    }

    #cmp-selectorCursos .card-curso .cont-img:active::before {
        background-color: #47c4ff;

    }

    #cmp-selectorCursos .alumno img {
        max-height: 120px;
        border-radius: 50%;
        height: 120px;
        width: 120px;
        max-width: 120px;
    }

    #cmp-selectorCursos .card-curso .cont-img img {
        height: 150px;
        width: 100%;
    }

    /* .card-curso .cont-img:hover img{
            opacity: 0;
        } */
    @keyframes pulse {
        0% {
            -moz-box-shadow: 0 0 0 0 rgba(204, 44, 44, 0.5);
            box-shadow: 0 0 0 0 rgba(204, 44, 44, 0.5);
        }

        70% {
            -moz-box-shadow: 0 0 0 10px rgba(204, 44, 44, 0);
            box-shadow: 0 0 0 10px rgba(204, 44, 44, 0);
        }

        100% {
            -moz-box-shadow: 0 0 0 0 rgba(204, 44, 44, 0);
            box-shadow: 0 0 0 0 rgba(204, 44, 44, 0);
        }
    }
</style>
<section id="cmp-selectorCursos" class="col-sm-12 my-card my-shadow">
    <div class=" col-md-12">
        <div m-render class="col-md-3 col-sm-12 alumno ">

            <img class="my-mg-center my-block" src="{{($.oAlumno.foto != '' ? _sysUrlBase_ +'static/media/usuarios/'+ $.oAlumno.foto: _sysUrlBase_ + 'static/libs/othersLibs/participantes/img/foto_usuario.png')}}">

            <p class="my-text-center my-font-1_1">{{$.oAlumno.nombre_full}}</p>
        </div>
        <div class="col-md-9 cont-progreso my-float-r">
            <div class="cont-labels">
                <span class="inicio"><?php echo JrTexto::_('Start'); ?></span>
                <span class="fin my-float-r"><?php echo JrTexto::_('End'); ?></span>
            </div>
            <div id="barra-progreso" class="barra">
                <i class="fa fa-flag flag l-flag lift"></i>
                <i class="fa fa-flag-checkered flag r-flag lift"></i>
                <i class="fa fa-child lift"></i>
                <span class="circle l-circle"></span>
                <span class="circle r-circle my-float-r"></span>
            </div>
        </div>
        <div m-render m-for="$.arrCursos, curso,i" class="slick-curso col-md-9 my-float-r cont-cursos">
            <div class="card-curso  my-cursor-pointer " data-index="{{i}}">
                <div class="cont-img">
                    <img class="my-circle my-fix-size my-shadow" src="{{(curso.imagen != '' ? _sysUrlBase_ + curso.imagen: _sysUrlBase_ + 'static/media/cursos/no-image.jpg')}}">
                    <!-- <img class="my-circle my-fix-size my-shadow" src="{{_sysUrlBase_ + 'static/media/cursos/no-image.jpg'}}"> -->
                    <div class="nombre-curso" title="{{curso.nombrecurso}}"><span class="nombre-text">{{curso.nombrecurso}}</span></div>
                </div>
                <div class="bottom-card">
                    <div class="progreso-curso">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: {{(curso.progreso).toFixed(2)}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">{{(curso.progreso).toFixed(2)}}%</div>
                        </div>
                    </div>
                    <div class="selected-curso"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    class SelectorCursos {
        defaults = {
            model: {
                oAlumno: {},
                arrCursos: []
            }
        };
        constructor(params) {
            this.defaults = Object.assign(this.defaults, params)
            this.mine = new Mine;
            this.render = new Render({
                instance: this,
                selector: '#cmp-selectorCursos',
                model: this.defaults.model,
                render: false
            });
            this.cmpUnidades = this.defaults.cmpUnidades;
            this.cmpProgresoUnidad = this.defaults.cmpProgresoUnidad;
            this.cmpHabilidades = this.defaults.cmpHabilidades;
            this.section = $('#cmp-selectorCursos');
            this.selected = null;
            this.selectedIndex = null;
        }
        launch() {
            this.selectedIndex = null;
            this.model = this.render.model;
            this.render.render();
            // console.log('working! cmp-selectorCursos model:', this.render.model);
        }
        onRendered(huboCambios) {
            this.model = this.render.model;
            let htmlCursos = this.section.find('.card-curso');
            htmlCursos.click(e => {
                this.onSelect(e.currentTarget);
            })
            this.loadSlick();
            this.onSelect(htmlCursos[0]);
        }
        onSelect(target) {
            let htmlCurso = $(target);
            let index = htmlCurso.attr('data-index');
            if (index != this.selectedIndex) {
                this.selectedIndex = index;
                this.unselect();
                this.selected = this.model.arrCursos[index];
                // console.log('this.selected Curso', this.selected);
                this.posicionarPj(this.selected.progreso);
                this.cmpUnidades.render.model.arrUnidades = this.selected.arrUnidades;
                this.cmpUnidades.render.model.oAlumno = this.model.oAlumno;
                this.cmpUnidades.render.model.oCurso = this.selected;
                this.cmpUnidades.launch();
                if (this.selected.arrUnidades.length > 0) {
                    this.cmpProgresoUnidad.render.model.arrUnidades = this.selected.arrUnidades;
                    this.cmpProgresoUnidad.show();
                    this.cmpProgresoUnidad.launch();
                    this.cmpHabilidades.show();
                    this.cmpHabilidades.launch({
                        idalumno: this.model.oAlumno.idpersona,
                        idcurso: this.selected.idcurso,
                        idcomplementario: this.selected.idcomplementario,
                        idgrupoauladetalle: this.selected.idgrupoauladetalle,
                        merge: true,
                        arrSmartbook: this.selected.arrSmartbook,
                        arrExamenes: this.selected.arrExamenes
                    });
                } else {
                    this.cmpProgresoUnidad.hide();
                    this.cmpHabilidades.hide();
                }


                let htmlSelectedBar = htmlCurso.find('.selected-curso');
                htmlSelectedBar.addClass('active');
            }
        }
        unselect() {
            let arrtHtmlSelectedBar = this.section.find('.selected-curso.active');
            for (let index = 0; index < arrtHtmlSelectedBar.length; index++) {
                const htmlSelectedBar = arrtHtmlSelectedBar[index];
                $(htmlSelectedBar).removeClass('active');
            }
        }

        posicionarPj(progreso) {
            let barraProgreso = this.section.find('#barra-progreso');
            let ancho = barraProgreso.width(); //100%
            let pj = barraProgreso.find('i.fa.fa-child');
            let px = (ancho * parseFloat(progreso) / 100);
            pj.attr('style', `left: ${px}px`)
        }
        loadSlick() {
            let slick = this.section.find('.slick-curso');
            if (slick.hasClass('slick-initialized')) {
                slick.slick('unslick');
            }
            slick.slick({
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        }
    }
</script>