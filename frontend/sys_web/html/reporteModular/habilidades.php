<style>
    .cont-habilidades {
        position: relative;
        min-height: 100px;
    }

    .chart-hab-cont {
        width: 100%;
        display: grid;
        place-items: center;
        justify-content: center;
        row-gap: 1rem;
        column-gap: 1rem;
        grid-template-columns: repeat(auto-fit, 150px);
        /* border: 1px solid red; */
    }

    .small {
        background-color: white;
        color: black;
    }

    .progress.progress-circle {
        width: 150px;
        height: 150px;
        background: none;
        position: relative;
    }

    .progress.progress-circle::after {
        content: "";
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border: 6px solid #eee;
        position: absolute;
        top: 0;
        left: 0;
    }

    .progress.progress-circle>span {
        width: 50%;
        height: 100%;
        overflow: hidden;
        position: absolute;
        top: 0;
        z-index: 1;
    }

    .progress.progress-circle .progress-left {
        left: 0;
    }

    .progress.progress-circle .progress-bar {
        width: 100%;
        height: 100%;
        background: none;
        border-width: 6px;
        border-style: solid;
        position: absolute;
        top: 0;
    }

    .progress.progress-circle .progress-left .progress-bar {
        left: 100%;
        border-top-right-radius: 80px;
        border-bottom-right-radius: 80px;
        border-left: 0;
        -webkit-transform-origin: center left;
        transform-origin: center left;
    }

    .progress.progress-circle .progress-right {
        right: 0;
    }

    .progress.progress-circle .progress-right .progress-bar {
        left: -100%;
        border-top-left-radius: 80px;
        border-bottom-left-radius: 80px;
        border-right: 0;
        -webkit-transform-origin: center right;
        transform-origin: center right;
    }

    .progress.progress-circle .progress-value {
        position: absolute;
        top: 0;
        left: 0;
    }
</style>
<section id="cmp-habilidades" class="col-sm-12 my-card my-shadow">
    <header>
        <h3><i><?php echo JrTexto::_('Skills obtained'); ?></i></h3>
        <div class="my-line"></div>
    </header>
    <div class="cont-habilidades col-sm-12 text-center">
        <div m-render m-show="$.arrHabilidades.length!=0" class="grafica-hab-curso col-sm-12 col-md-6">
            <canvas id="chart-hab-curso" width="400" height="400"></canvas>
        </div>
        <div class="grafica-hab col-sm-12 col-md-6">
            <div m-render m-for="$.arrHabilidades, habilidad,i" class="chart-hab-cont">
                <div class="char-item ">
                    <p class="my-font-1">{{habilidad.skill_name}}</p>
                    <div class="progress progress-circle mx-auto" data-value='{{Math.round(habilidad.progreso)}}'>
                        <span class="progress-left">
                            <span class="progress-bar border-primary"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar border-primary"></span>
                        </span>
                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                            <div class="h2 font-weight-bold">{{(habilidad.progreso!=null?(parseFloat(habilidad.progreso)).toFixed(2)+'%':'<?php echo JrTexto::_('Not evaluated'); ?>')}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div m-render m-show="$.arrHabilidades.length==0&&!$.loading" class="my-msg">
            <?php echo JrTexto::_('This course has no skills'); ?>...
        </div>
        <div m-render m-show="$.loading" class="my-spinner"></div>
    </div>
</section>

<script>
    class Habilidades {
        defaults = {
            model: {
                arrHabilidades: [],
                loading: true
            }
        };
        constructor(data) {
            Object.assign(this.defaults, data)
            this.mine = new Mine;
            this.render = new Render({
                instance: this,
                selector: '#cmp-habilidades',
                model: this.defaults.model,
                render: false
            });
            this.section = $('#cmp-habilidades');
            this.chartHabCurso = null;
        }
        launch(params) {
            this.render.model.arrHabilidades = [];
            this.render.model.loading = true;
            this.render.render();
            this.getProgresoHabilidad(params).then(arrHabilidades => {
                this.render.model.arrHabilidades = arrHabilidades;
                this.render.model.loading = false;
                this.render.render();
            });
            this.model = this.render.model;
            // console.log('working! cmp-habilidades model:', this.render.model);
        }
        getProgresoHabilidad(params) { //{idcurso,idalumno,arrSmartbook,arrExamenes}
            return new Promise((resolve, reject) => {
                this.mine.postData({
                    url: _sysUrlBase_ + 'json/ReporteModular/getProgresoHabilidad',
                    data: {
                        params
                    },
                    showloading: false
                }).then(rs => {
                    resolve(rs);
                })
            })
        }
        onRendered(huboCambios) { //cuando termina de renderizarse
            this.model = this.render.model;
            // console.log(' cmp-habilidades renderizado con modelo:', this.model);
            this.loadChartHabCurso();
            this.loadChartItem();
        }
        loadChartHabCurso() {
            if (this.chartHabCurso != null) {
                this.chartHabCurso.destroy();
            }
            let arrLabels = [];
            let arrData = [];
            let arrColors = Mine.getColors(this.model.arrHabilidades.length, {
                tipo: 2
            });

            this.model.arrHabilidades.forEach(habilidad => {
                arrLabels.push(habilidad.skill_name);
                arrData.push(habilidad.progreso == null ? 0 : habilidad.progreso);
            });
            this.chartHabCurso = new Chart('chart-hab-curso', {
                type: 'pie',
                data: {
                    labels: arrLabels,
                    datasets: [{
                        label: '<?php echo ucfirst(JrTexto::_('Acquired skills')); ?>',
                        data: arrData,
                        backgroundColor: arrColors,
                        borderColor: arrColors,
                        borderWidth: 1
                    }]
                },
            });
        }
        loadChartItem() {
            $(function() {

                $(".progress").each(function() {

                    var value = $(this).attr('data-value');
                    var left = $(this).find('.progress-left .progress-bar');
                    var right = $(this).find('.progress-right .progress-bar');

                    if (value > 0) {
                        if (value <= 50) {
                            right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
                        } else {
                            right.css('transform', 'rotate(180deg)')
                            left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
                        }
                    }

                })

                function percentageToDegrees(percentage) {

                    return percentage / 100 * 360

                }

            });

        }
        hide() {
            if (!this.section.hasClass('my-hide')) {
                this.section.addClass('my-hide');
            }
        }
        show() {
            if (this.section.hasClass('my-hide')) {
                this.section.removeClass('my-hide');
            }
        }
    }
</script>