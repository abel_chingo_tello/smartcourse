<style>
    #cmp-unidadViewer .slick-initialized {
        margin-bottom: 0 !important;
    }

    #cmp-unidadViewer .cont-hab {
        width: 100%;
        display: grid;
        place-items: center;
        justify-content: center;
        row-gap: 1rem;
        column-gap: 1rem;
        grid-template-columns: repeat(auto-fit, 200px);
        /* border: 1px solid red; */
    }

    #cmp-unidadViewer .hab-wraper {
        min-height: 100px;

    }

    #cmp-unidadViewer .cont-hab>* {
        width: 100%;
    }

    #cmp-unidadViewer .modal .my-card.my-shadow {
        margin-bottom: 1rem;
    }

    #cmp-unidadViewer .modal-content {
        /* background-color: #f4f4f4; */
    }

    #cmp-unidadViewer .card-prog-sesion {
        display: grid;
        grid-template-rows: 1fr;
        height: 200px;
        width: 200px;
    }

    #cmp-unidadViewer .card-prog-sesion .card-g {
        height: 100%;
        width: 100%;
        position: relative;
    }

    #cmp-unidadViewer .card-prog-sesion .sesion-cover {
        height: 100%;
        width: 100%;
        position: relative;

    }

    #cmp-unidadViewer .cont-nombre-sesion {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: transparent;
        display: grid;
        place-content: center;
        padding-left: 4px;
        padding-right: 4px;
    }

    #cmp-unidadViewer .nombre-sesion {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        font-size: 2.3rem;
        color: white;
    }

    #cmp-unidadViewer .card-prog-sesion:hover .progreso-sesion {
        visibility: visible;
        height: 100%;
        width: 100%;
    }

    #cmp-unidadViewer .progreso-sesion {
        transition: all 0.3s;
        transition-property: height, width;
        /* visibility: hidden; */
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 0px;
        height: 0px;
        margin: auto;
        font-size: 2.3rem;
        color: white;
        display: grid;
        place-content: center;
        background-color: #6363636b;
        font-weight: bolder;
        text-shadow: 2px 2px 8px #040404;
    }

    #cmp-unidadViewer .card-prog-sesion:hover .prog-text {
        visibility: visible;
    }

    #cmp-unidadViewer .prog-text {
        visibility: hidden;
    }
</style>
<section id="cmp-unidadViewer">
    <div class="modal fade my-modal soft-rw my-font-all" id="md-unidadViewer" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog my-full-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 m-render class="modal-title" id="modal-title">
                        {{$.oCurso.nombrecurso}} - {{$.oUnidad.nombre}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <main class="col-sm-12">
                        <div class="my-w-100 hab-wraper my-relative my-card my-shadow">
                            <div m-render m-for="$.arrHabilidades, habilidad,i" class="cont-hab">
                                <div class="cont-progress">
                                    <p class="my-font-1 text-center">{{habilidad.skill_name}}</p>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: {{habilidad.progreso}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">{{habilidad.progreso}}%</div>
                                    </div>
                                </div>
                            </div>
                            <div m-render m-show="$.arrHabilidades.length==0&&!$.loading" class="my-msg">
                                <?php echo JrTexto::_('This unit has no skills'); ?>...
                            </div>
                            <div m-render m-show="$.loading" class="my-spinner"></div>
                        </div>
                        <div class="pj col-sm-12 my-card my-shadow">
                            <div class="col-sm-12 cont-progreso my-float-r">
                                <div class="cont-labels">
                                    <span class="inicio"><?php echo JrTexto::_('Start'); ?></span>
                                    <span class="fin my-float-r"><?php echo JrTexto::_('End'); ?></span>
                                </div>
                                <div m-render id="barra-progreso" class="barra">
                                    <i class="fa fa-flag flag l-flag lift"></i>
                                    <i class="fa fa-flag-checkered flag r-flag lift"></i>
                                    <i class="fa fa-child lift" style="left:{{parseFloat($.oUnidad.progreso)}}%"></i>
                                    <span class="circle l-circle"></span>
                                    <span class="circle r-circle my-float-r"></span>
                                </div>
                            </div>
                        </div>
                        <div class="carrusel col-sm-12 my-card my-shadow">
                            <div id="cont-carrusel" m-render m-for="$.oUnidad.arrSesiones, sesion,i" class="slick-sesiones">
                                <div class="card-prog-sesion">
                                    <div class="card-g my-cursor-pointer">
                                        <div class="sesion-cover" style="background-color:{{$.arrColors[i]}}">
                                            <span class="cont-nombre-sesion">
                                                <span class="nombre-sesion">
                                                    {{sesion.nombre}}
                                                </span>

                                            </span>
                                            <span class="progreso-sesion">
                                                <div class="prog-text">
                                                    {{(parseFloat(sesion.progreso)).toFixed(2)}}%
                                                </div>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div m-render class="col-sm-12 my-center gotoUnit">
                            <a class="btn btn-primary" target="_blank" href="{{$.sysurl}}smart/cursos/ver/?idcurso={{$.oCurso.idcurso}}&icc={{$.oCurso.idcomplementario}}&idgrupoauladetalle={{$.oCurso.idgrupoauladetalle}}">Continuar Unidad</a>
                        </div>
                    </main>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    class UnidadViewer {
        defaults = {
            model: {
                oUnidad: {},
                oAlumno: {},
                arrHabilidades: [],
                oCurso: {},
                sysurl: _sysUrlBase_,
                loading: true
            }
        };
        constructor(data) {
            Object.assign(this.defaults, data)
            this.mine = new Mine;
            this.render = new Render({
                instance: this,
                selector: '#cmp-unidadViewer',
                model: this.defaults.model,
                render: false
            });
            this.section = $('#cmp-unidadViewer');
        }
        launch() {
            this.render.model.loading = true;
            this.model = this.render.model;

            this.arrColors = Mine.getColors(this.render.model.oUnidad.arrSesiones.length);
            this.render.model.arrColors = this.arrColors;
            // console.log('----this.render.model.oUnidad.arrSesiones', this.render.model.oUnidad.arrSesiones);
            // console.log('----', this.arrColors);
            // this.render.render();
            $('#md-unidadViewer').modal('show');
            // console.log('modalshowwwwwww', this.md);
            this.render.model.arrHabilidades = [];
            this.render.render();
            setTimeout(() => {
                this.loadSlick();
            }, 150);

            this.getProgresoHabilidad({
                idalumno: this.render.model.oAlumno.idpersona,
                arrSmartbook: this.render.model.oUnidad.arrSmartbook,
                idcurso: this.render.model.oCurso.idcurso,
                idcomplementario: this.render.model.oCurso.idcomplementario,
                idgrupoauladetalle: this.render.model.oCurso.idgrupoauladetalle,
                arrExamenes: this.render.model.oUnidad.arrExamenes
            }).then(arrHabilidades => {
                this.render.model.arrHabilidades = arrHabilidades;
                this.render.model.loading = false;
                this.render.render({
                    norender: 'cont-carrusel'
                });
            });
        }
        onRendered(huboCambios) { //cuando termina de renderizarse
            this.model = this.render.model;
            // console.log(' cmp-unidadViewer renderizado con modelo', this.model);
        }
        loadSlick() {
            let slick = this.section.find('.slick-sesiones');
            if (slick.hasClass('slick-initialized')) {
                slick.slick('unslick');
            }
            slick.slick({
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 3
            });
        }
        getProgresoHabilidad(params) { //{idcurso,idalumno,arrSmartbook,arrExamenes}
            return new Promise((resolve, reject) => {
                this.mine.postData({
                    url: _sysUrlBase_ + 'json/ReporteModular/getProgresoHabilidad',
                    data: {
                        params
                    },
                    showloading: false
                }).then(rs => {
                    resolve(rs);
                })
            })
        }
    }
</script>