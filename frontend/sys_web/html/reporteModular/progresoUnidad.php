<style>
    /* #cmp-progresoUnidad {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
        width: 100%;
    } */

    .hoja {
        background-color: white;
        border-radius: 20px;
        /* min-height: 400px; */
        border: 1px solid #cfcfcf6e;

    }

    .cont-prog-unidad {
        display: grid;
        place-items: center;
        text-align: center;
    }

    .chart-prog {
        width: 100%;
    }

    .hoja canvas {
        height: 100%;
        width: 100%;
        max-height: 100%;
        max-width: 100%;
    }

    .cont-select {
        height: 100px;
        width: 100%;
        display: grid;
        place-content: center;
        grid-template-columns: 60px 1fr;
    }

    .cont-select label {
        display: grid;
        place-content: center;
        color: black;
        font-size: 1.1rem;
        padding-right: 5px;
    }

    .cont-select>* {
        width: 100%;
        height: 100%;
    }
</style>
<section id="cmp-progresoUnidad" class="col-sm-12">
    <div class="hoja col-md-12 col-lg-6 my-shadow">
        <div class="cont-prog-unidad">
            <div class="col-sm-12 col-md-6">
                <div class="cont-select">
                    <!-- <label for="">Unidad: </label>
                    <select class="form-control" name="" id="selectUnidad">
                    </select> -->
                </div>
            </div>
            <h3><?php echo JrTexto::_('Unit'); ?></h3>
            <div class="chart-prog" style="padding:20px">
                <canvas id="chart-prog-unidades" width="200" height="200"></canvas>
            </div>
        </div>
    </div>
    <div class="hoja col-md-12  col-lg-6 my-shadow">
        <div class="cont-prog-unidad">
            <div class="col-sm-12 col-md-6">
                <div class="cont-select">
                    <label for=""><?php echo JrTexto::_('Unit'); ?>: </label>
                    <select class="form-control" name="" id="selectUnidad">
                    </select>
                </div>
            </div>
            <h3 class="col-sm-12"><?php echo JrTexto::_('Sessions');?> ?></h3>
            <div class="chart-prog">
                <canvas id="chart-prog-sesiones" width="200" height="200"></canvas>
            </div>
        </div>
    </div>
</section>

<script>
    class ProgresoUnidad {
        defaults = {
            model: {
                arrUnidades: [],
            }
        };
        constructor(data) {
            Object.assign(this.defaults, data)
            this.mine = new Mine;
            this.render = new Render({
                instance: this,
                selector: '#cmp-progresoUnidad',
                model: this.defaults.model,
                render: false
            });
            this.section = $('#cmp-progresoUnidad');
            this.chartProgUnidades = null;
            this.chartProgSesiones = null;

        }
        launch() {
            this.render.render();
            this.model = this.render.model;
            // console.log('working! cmp-progresoUnidad model:', this.model);
        }
        onRendered(huboCambios) { //cuando termina de renderizarse
            this.model = this.render.model;
            this.launchChartUnidades();
            this.loadSelectUnidad();
            // console.log('cmp-progresoUnidad renderizado con modelo:', this.model);
        }
        loadSelectUnidad() {
            let arrUnidades = this.model.arrUnidades.map((unidad) => {
                unidad.id = unidad.nombre;
                unidad.text = unidad.nombre;
                return unidad;
            });

            this.selectUnidades = this.section.find('#selectUnidad');
            this.clearSelects();
            // console.log('---------------------------##############this.selectUnidades',this.selectUnidades);
            this.selectUnidades.select2({
                data: arrUnidades
            })
            this.selectUnidades.on("change.select2", () => { //Añadido evento al cambiar
                let unidad = this.selectUnidades.select2('data')[0]; //obteniendo elemento seleccionado

                this.launchChartSesiones(unidad.arrSesiones);
            })
            this.selectUnidades.trigger('change.select2');

        }
        launchChartSesiones(arrSesiones) {
            if (this.chartProgSesiones != null) {
                this.chartProgSesiones.destroy();
            }
            let arrData = [];
            let arrLabels = [];
            if (arrSesiones.length > 0) {
                let arrColors = Mine.getColors(arrSesiones.length, {
                    tipo: 2
                });
                arrSesiones.forEach(sesion => {
                    arrData.push(parseFloat(sesion.progreso).toFixed(2));
                    arrLabels.push(sesion.nombre);
                });
                this.chartProgSesiones = new Chart('chart-prog-sesiones', {
                    type: 'horizontalBar',
                    // labels: arrLabels,
                    data: {
                        labels: arrLabels,
                        datasets: [{
                            // label: "Test",
                            data: arrData,
                            backgroundColor: arrColors,
                            borderColor: arrColors,
                            //borderWidth: 1
                            // hoverBackgroundColor: ["#66A2EB", "#FCCE56"]
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                    min: 0, // Edit the value according to what you need,
                                    max: 100
                                }
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }
                });
            }
        }
        launchChartUnidades() {
            if (this.chartProgUnidades != null) {
                this.chartProgUnidades.destroy();
            }
            let arrData = [];
            let arrLabels = [];
            if (this.model.arrUnidades.length > 0) {
                let arrColors = Mine.getColors(this.model.arrUnidades.length, {
                    tipo: 2
                });
                this.model.arrUnidades.forEach(unidad => {
                    arrData.push(parseFloat(unidad.progreso).toFixed(2));
                    arrLabels.push(unidad.nombre);
                });
                this.chartProgUnidades = new Chart('chart-prog-unidades', {
                    type: 'horizontalBar',
                    // labels: arrLabels,
                    data: {
                        labels: arrLabels,
                        datasets: [{
                            // label: "Test",
                            data: arrData,
                            backgroundColor: arrColors,
                            borderColor: arrColors,
                            //borderWidth: 1
                            // hoverBackgroundColor: ["#66A2EB", "#FCCE56"]
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                    min: 0, // Edit the value according to what you need,
                                    max: 100
                                }
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }
                });
            }
        }
        clearSelects() {
            this.selectUnidades.empty();
        }
        hide() {
            if (!this.section.hasClass('my-hide')) {
                this.section.addClass('my-hide');
            }
        }
        show() {
            if (this.section.hasClass('my-hide')) {
                this.section.removeClass('my-hide');
            }
        }
    }
</script>