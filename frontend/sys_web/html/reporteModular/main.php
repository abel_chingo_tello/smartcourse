<?php
defined("RUTA_BASE") or die();
$usuarioAct = NegSesion::getUsuario();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Report"); ?>'
        });
    </script>
<?php } ?>
<style>
    .progress {
        border-radius: 5px;
    }

    section {
        visibility: hidden;
    }

    #main-rpt #cont-modulos section,
    #cont-selects {
        margin-bottom: 1rem;
    }

    .progress-bar {
        font-size: 1rem;
        line-height: 0;
        text-shadow: 2px 2px 4px #000000;
    }

    .cont-progreso .barra .fa-child {
        font-size: 2rem;
        color: red;
        z-index: 10;
        margin-left: -9px;
        position: absolute;
        animation: pulse 2s infinite;
        border-radius: 50%;
        left: 0px;
        color: rgb(253, 69, 74);
    }

    .cont-progreso .barra .flag {
        color: #777777;
        font-size: 2rem;
        position: absolute;

    }

    .cont-progreso .flag.l-flag {
        left: 0px;
    }

    .cont-progreso .flag.r-flag {
        right: -26px;
    }

    .cont-progreso {
        position: relative;
        padding-right: 20px;
        margin-bottom: 20px;
    }

    .cont-progreso .barra {
        height: 3px;
        background: lightgrey;
        border-radius: 15px;
        width: 100%;
        position: relative;
    }

    .cont-progreso .circle {
        width: 30px;
        height: 15px;
        background-color: #555;
        border-radius: 50%;
        position: absolute;
        bottom: -6px;
        color: #555;
    }

    .cont-progreso .cont-labels {
        margin-bottom: 50px;
    }

    .cont-progreso .barra .lift {
        bottom: 10px;
    }

    .cont-progreso .r-circle {
        right: -14px;
    }

    .cont-progreso .l-circle {
        left: -14px;
    }

    .slick-curso .slick-list.draggable {
        margin: auto;
        width: 100%;
    }
    header {
        background-color: white;
        height: 60px;
    }
    header>h3>i{
        color: black;
    }
</style>

<main id="rptMD" class="container my-font-all soft-rw">
    <div class="col-sm-12" id="main-rpt">
        <div id="cont-selects" class="row my-x-center  my-hide">

            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="selectGrupo"><?php echo JrTexto::_('Group'); ?></label>
                    <select class="form-control" name="" id="selectGrupo">
                    </select>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="selectAlumnos"><?php echo JrTexto::_('Student'); ?></label>
                    <select class="form-control" name="" id="selectAlumnos">
                    </select>
                </div>
            </div>
        </div>
        <!-- <?php //if ($usuarioAct['idrol'] == "2") {include("filtroAlumno.php");} 
                ?> -->
        <div id="msg-alert" class="col-sm-12 my-card my-shadow my-hide">
            <h3 class="my-italic text-center"></h3>
        </div>
        <div id="cont-modulos" class="my-hide">
            <?php include("selectorCursos.php") ?>
            <?php include("unidades.php") ?>
            <?php include("progresoUnidad.php") ?>
            <?php include("habilidades.php") ?>
        </div>
        <!-- <?php //include("reporte-1.php") 
                ?> -->
    </div>
</main>

<script>
    class MainReporte {
        mine = new Mine;
        constructor() {
            this.section = $('#rptMD');
            this.oSelectorCursos = null;
        }
        launch() {
            this.getInitialData().then((initialData) => {
                switch (initialData.idrol) {
                    case "3":
                        let oAlumno = initialData.data;
                        this.run(oAlumno);
                        break;
                    default:
                        this.loadSelectGrupo();
                        break;
                }
            })
        }
        showMsg(text) {
            $('#msg-alert').find('h3').text(text);
            this.hideModulos();
            $('#msg-alert').removeClass('my-hide');
        }
        hideMsg() {
            this.showModulos();
            $('#msg-alert').addClass('my-hide');
        }
        showModulos() {
            $('#cont-modulos').removeClass('my-hide');
        }
        hideModulos() {
            $('#cont-modulos').addClass('my-hide');
        }
        getProgresoCurso(params = {
            idcurso,
            idcomplementario,
            idalumno
        }) {
            return new Promise((resolve, reject) => {
                this.mine.postData({
                    url: _sysUrlBase_ + 'json/ReporteModular/getCursoProgreso',
                    data: {
                        params
                    }
                }).then(arrCursos => {
                    resolve(arrCursos);
                })
            })
        }
        getInitialData() {
            return new Promise((resolve, reject) => {
                this.mine.postData({
                    url: _sysUrlBase_ + 'json/ReporteModular/getInitialData',
                }).then((initialData) => {
                    resolve(initialData);
                })
            });
        }
        loadTestSelect(rs) {
            const oMySelects = new mySelects;
            oMySelects.insertSelectCursos({
                target: '#selects-cont',
                rol: 3,
                data: rs,
                onFire: (selected) => {
                    oMine.postData({
                        url: 'json/ReporteModular/getCursoProgreso',
                        data: {
                            params: {
                                idcurso: selected.idcurso,
                                idcomplementario: selected.idcomplementario,
                                idalumno: selected.idalumno
                            }
                        }
                    }).then(rs => {
                        // console.log('rs', rs);
                    })
                }
            });
        }
        loadSelectGrupo() {
            this.section.find('#cont-selects').removeClass("my-hide");
            let $this = this;
            this.mine.postData({
                url: _sysUrlBase_ + 'json/Mine/getGrupoAulaAlumnos',
            }).then(rs => {
                if (rs.length == 0) {
                    this.showMsg('<?php echo JrTexto::_('You have no assigned group'); ?>...');
                } else {
                    let arrGrupos = rs.map((grupo) => {
                        grupo.id = grupo.idgrupoaula;
                        grupo.text = grupo.nombre_grupo;
                        return grupo;
                    });
                    $this.selectGrupo = $this.section.find('#selectGrupo');
                    // console.log('arrGrupos', arrGrupos);
                    $this.selectGrupo.select2({
                        data: arrGrupos
                    })
                    $this.selectGrupo.on("change.select2", () => { //Añadido evento al cambiar
                        let grupo = $this.selectGrupo.select2('data')[0]; //obteniendo elemento seleccionado
                        if (grupo.arrAlumnos.length == 0) {
                            this.selectAlumnos = this.section.find('#selectAlumnos');
                            this.selectAlumnos.empty();
                            this.showMsg('<?php echo JrTexto::_('there are no students enrolled in the course yet'); ?>...');
                        } else {
                            $this.loadSelectAlumnos(grupo.arrAlumnos);
                        }
                    })
                    $this.selectGrupo.trigger('change.select2');
                }
            });
        }
        loadSelectAlumnos(arr) {
            let arrAlumnos = arr.map((oAlumno) => {
                oAlumno.id = oAlumno.idpersona;
                oAlumno.text = oAlumno.nombre_full;
                return oAlumno;
            });
            this.selectAlumnos = this.section.find('#selectAlumnos');
            this.selectAlumnos.empty(); //CHECKKKK
            this.selectAlumnos.select2({
                data: arrAlumnos
            })
            this.selectAlumnos.on("change.select2", () => { //Añadido evento al cambiar
                let oAlumno = this.selectAlumnos.select2('data')[0]; //obteniendo elemento seleccionado
                // console.log('---ALUMNO SELECCIONADO', oAlumno);
                this.run(oAlumno)
            })
            this.selectAlumnos.trigger('change.select2');
        }
        run(oAlumno) {
            this.getProgresoCurso({
                idalumno: oAlumno.idpersona
            }).then(arrCursos => {
                if (arrCursos.length > 0) {
                    this.hideMsg();
                    if (this.oSelectorCursos == null) {
                        const oUnidades = new Unidades();
                        const oProgresoUnidad = new ProgresoUnidad();
                        const oHabilidades = new Habilidades();
                        this.oSelectorCursos = new SelectorCursos({
                            model: {
                                oAlumno,
                                arrCursos
                            },
                            cmpUnidades: oUnidades,
                            cmpHabilidades: oHabilidades,
                            cmpProgresoUnidad: oProgresoUnidad
                        });
                    } else {
                        this.oSelectorCursos.render.model.oAlumno = oAlumno;
                        this.oSelectorCursos.render.model.arrCursos = arrCursos;
                    }
                    this.oSelectorCursos.launch();
                } else {
                    this.showMsg('<?php echo JrTexto::_("You are not enrolled in any course"); ?>...');
                }
            })
        }
    }
    $(document).ready(() => {
        const oMainReporte = new MainReporte;
        oMainReporte.launch();
    })
</script>