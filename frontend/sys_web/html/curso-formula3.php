<?php
defined('RUTA_BASE') or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla != 'modal' ? false : true;
if (!empty($this->datos)) $frm = $this->datos;
$ventanapadre = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : 'eeeexzx-1';
$_imgdefecto = "static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Evacuation Formula") . " : " . $this->curso["nombre"]; ?>'
    });
  </script>
<?php }
$formulaActual = !empty($this->curso["formula_evaluacion"]) ? json_decode($this->curso["formula_evaluacion"], true) : array();
?>
<style type="text/css">
  tr[borrar] {
    background-color: #ccc;
  }

  tr[borrar] select,
  tr[borrar] input.punprefijo,
  tr[borrar] .select-ctrl-wrapper.select-azul::after {
    display: none !important;
    text-decoration: line-through;
  }

  tr[borrar] input.punitem {
    text-decoration: line-through;
  }
</style>
<div class="row my-font-all">
  <div class="col-md-12 col-sm-12 ">
    <div class="my-card my-shadow col-sm-12 my-mg-bottom my-center my-column my-hide">
      <h1><i>Zona experimental</i></h1>
      <div style="width:50%;margin:auto;">
        <select id="select-try" style="width:100%"></select>
      </div>
    </div>

    <div class="x_panel  my-shadow">
      <div class="x_content x_content_2">
        <div class="row ventana" id="vent-<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12 table-responsive" id="aquicargatabla">
                <table class="table table-striped table-hover" id="tablenuevapuntuacion">
                  <thead>
                    <tr class="my-text-center my-color-black my-uppercase">
                      <th><span class="fm-title">Sesión</span>
                        <br><small><i>(menú/pestaña)</i></small></th>
                      <th><span class="fm-title">Prefijo de la sesión</span>
                        <br><small><i>(para la fórmula)</i></small>
                      </th>
                      <th><span class="fm-title">Peso de la sesión</span>
                        <br><small><i>(ponderación dentro del grupo)</i></small>
                      </th>
                      <th><span class="fm-title">Grupo</span></th>
                      <th class="haygrupo"><span class="fm-title">Peso del grupo</span>
                        <br><small><i>(ponderación del grupo en la fórmula)</i></small>
                      </th>

                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <!-- TR CLONE -->
                    <tr id="" class="trclone" style="display: none;">
                      <!-- nombre del item -->
                      <td class="punitem" style="text-align: left"></td>
                      <!-- prefijo -->
                      <td><input name="punprefijo" maxlength="3" class="punprefijo form-control" placeholder="Ej. EF"></td>
                      <!-- peso del grupo -->
                      <td class="haygrupo">
                        <div class=" select-ctrl-wrapper select-azul ">
                          <select name="pungruporcentaje" class="pungruporcentaje" class="form-control">
                            <option value="P">Promedio</option>
                            <?php for ($i = 99; $i > 0; $i--) { ?>
                              <option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
                            <?php } ?>
                          </select>
                        </div>
                      </td>
                      <!-- grupo -->
                      <td>
                        <div class=" select-ctrl-wrapper select-azul ">
                          <select name="pungrupo" class="pungrupo" class="form-control">
                            <option value="N">Ninguno</option>
                            <option value="B1">Bimestre 1</option>
                            <option value="B2">Bimestre 2</option>
                            <option value="B3">Bimestre 3</option>
                            <option value="B4">Bimestre 4</option>
                            <option value="B5">Bimestre 5</option>
                            <option value="T1">Trimestre 1</option>
                            <option value="T2">Trimestre 2</option>
                            <option value="T3">Trimestre 3</option>
                            <option value="U1">Unidad 1</option>
                            <option value="U2">Unidad 2</option>
                            <option value="U3">Unidad 3</option>
                            <option value="U4">Unidad 4</option>
                            <option value="U5">Unidad 5</option>
                            <option value="S1">Sesion 1</option>
                            <option value="S2">Sesion 2</option>
                            <option value="S3">Sesion 3</option>
                            <option value="S4">Sesion 4</option>
                            <option value="S5">Sesion 5</option>
                            <option value="S6">Sesion 6</option>
                            <option value="S7">Sesion 7</option>
                            <option value="S8">Sesion 8</option>
                            <option value="S9">Sesion 9</option>
                            <option value="S10">Sesion 10</option>
                            <option value="PRA">Practica</option>
                            <option value="PD">Practica Dirigida</option>
                            <option value="PC">Practica Calificada</option>
                            <option value="EX">Examen</option>
                            <option value="EE">Examen de Entrada</option>
                            <option value="EF">Examen Final</option>
                            <option value="TA">Tareas</option>
                            <option value="PR">Proyectos</option>
                          </select>
                        </div>
                      </td>

                      <!-- peso del item -->
                      <td>
                        <div class=" select-ctrl-wrapper select-azul ">
                          <select name="punporcentaje" class="punporcentaje select-peso-item" class="form-control">
                            <option value="P">Promedio</option>
                            <?php for ($i = 99; $i >= 0; $i--) { ?>
                              <option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
                            <?php } ?>
                          </select>
                        </div>
                      </td>
                      <td><i class="fa fa-trash btnborrar"></i> </td>
                    </tr>
                    <?php
                    $nt /*número de tareas*/ = $np /*número de proyectos*/ = $ne /* número de exámenes */ = 0;   
                    //var_dump($this->curso["temasaevaluar"]);                
                    if (!empty($this->curso["temasaevaluar"]))
                      foreach ($this->curso["temasaevaluar"] as $k => $t) {
                        $ns = ''/*Prefijo*/;
                        $tipo = '';
                        if ($t["tipo"] == 'smartquiz') {
                          $ne++;
                          $ns = ($ne == 1) ? 'EE' : 'E' . $ne;
                          $tipo = ($ne == 1) ? "EE" : "EX";
                          if ($ne == $this->curso["nexamenes"]) {
                            $ns = 'EF';
                            $tipo = "EF";
                          }
                        } else if ($t["tipo"] == 'estarea') {
                          $nt++;
                          $ns = 'T' . $nt;
                          $tipo = "TA";
                        } else if ($t["tipo"] == 'esproyecto') {
                          $np++;
                          $ns = 'P' . $np;
                          $tipo = "PR";
                        }
                       
                        $ns = !empty($t["prefijo"]) ? $t["prefijo"] : $ns;
                        $grupo = !empty($t["grupo"]) ? $t["grupo"] : $tipo;
                        $item = (!empty($t["item"])||@$t["item"]=='0')?@$t["item"]:'P';
                        $grupoitem = (!empty($t["grupoitem"])||@$t["grupoitem"]=='0')?@$t["grupoitem"]:'P';
                        $borrado = !empty($t["borrado"]) ? $t["borrado"] : false;


                    ?>
                      <tr id="<?php echo $t["id"] ?>" <?php echo $borrado == true ? 'borrar="true"' : '';  ?> grupo="<?php echo !empty($t["grupo"]) ? $t["grupo"] : $tipo; ?>" type="<?php echo $t["tipo"] ?>" idexamen="<?php echo @$t["idexamen"] ?>">
                        <!-- nombre del item -->
                        <td class="punitem" style="text-align: left"><input name="punitem" class="punitem form-control" value="<?php echo $t["nombreunico"] ?>"></td>
                        <!-- prefijo -->
                        <td><input name="punprefijo" maxlength="3" class="punprefijo form-control" value="<?php echo $ns; ?>"></td>
                        <!-- peso del grupo -->
                        <td class="col-md-12 form-group">
                          <div class=" select-ctrl-wrapper select-azul ">
                            <select name="punporcentaje" class="punporcentaje form-control" vtem="<?php echo $item ?>">
                              <option value="P" <?php echo $item == "P" ? 'selected="selected"' : ""; ?>>Promedio</option>
                              <?php for ($i = 99; $i >= 0; $i--) { ?>
                                <option value="<?php echo $i; ?>" <?php echo ($item == $i && $item!='P') ? 'selected="selected"' : ""; ?>><?php echo $i; ?>%</option>
                              <?php } ?>
                            </select>
                          </div>
                        </td>
                        <!-- grupo -->
                        <td class=" form-group">
                          <div class=" select-ctrl-wrapper select-azul ">
                            <select name="pungrupo" class="pungrupo form-control">
                              <option value="N" <?php echo $grupo == "N" ? 'selected="selected"' : ""; ?>>Ninguno</option>
                              <option value="B1" <?php echo $grupo == "B1" ? 'selected="selected"' : ""; ?>>Bimestre 1</option>
                              <option value="B2" <?php echo $grupo == "B2" ? 'selected="selected"' : ""; ?>>Bimestre 2</option>
                              <option value="B3" <?php echo $grupo == "B3" ? 'selected="selected"' : ""; ?>>Bimestre 3</option>
                              <option value="B4" <?php echo $grupo == "B4" ? 'selected="selected"' : ""; ?>>Bimestre 4</option>
                              <option value="B5" <?php echo $grupo == "B5" ? 'selected="selected"' : ""; ?>>Bimestre 5</option>
                              <option value="T1" <?php echo $grupo == "T1" ? 'selected="selected"' : ""; ?>>Trimestre 1</option>
                              <option value="T2" <?php echo $grupo == "T2" ? 'selected="selected"' : ""; ?>>Trimestre 2</option>
                              <option value="T3" <?php echo $grupo == "T3" ? 'selected="selected"' : ""; ?>>Trimestre 3</option>
                              <option value="U1" <?php echo $grupo == "U1" ? 'selected="selected"' : ""; ?>>Unidad 1</option>
                              <option value="U2" <?php echo $grupo == "U2" ? 'selected="selected"' : ""; ?>>Unidad 2</option>
                              <option value="U3" <?php echo $grupo == "U3" ? 'selected="selected"' : ""; ?>>Unidad 3</option>
                              <option value="U4" <?php echo $grupo == "U4" ? 'selected="selected"' : ""; ?>>Unidad 4</option>
                              <option value="U5" <?php echo $grupo == "U5" ? 'selected="selected"' : ""; ?>>Unidad 5</option>
                              <option value="S1" <?php echo $grupo == "S1" ? 'selected="selected"' : ""; ?>>Sesion 1</option>
                              <option value="S2" <?php echo $grupo == "S2" ? 'selected="selected"' : ""; ?>>Sesion 2</option>
                              <option value="S3" <?php echo $grupo == "S3" ? 'selected="selected"' : ""; ?>>Sesion 3</option>
                              <option value="S4" <?php echo $grupo == "S4" ? 'selected="selected"' : ""; ?>>Sesion 4</option>
                              <option value="S5" <?php echo $grupo == "S5" ? 'selected="selected"' : ""; ?>>Sesion 5</option>
                              <option value="S6" <?php echo $grupo == "S6" ? 'selected="selected"' : ""; ?>>Sesion 6</option>
                              <option value="S7" <?php echo $grupo == "S7" ? 'selected="selected"' : ""; ?>>Sesion 7</option>
                              <option value="S8" <?php echo $grupo == "S8" ? 'selected="selected"' : ""; ?>>Sesion 8</option>
                              <option value="S9" <?php echo $grupo == "S9" ? 'selected="selected"' : ""; ?>>Sesion 9</option>
                              <option value="S10" <?php echo $grupo == "S10" ? 'selected="selected"' : ""; ?>>Sesion 10</option>
                              <option value="PRA" <?php echo $grupo == "PRA" ? 'selected="selected"' : ""; ?>>Practica</option>
                              <option value="PD" <?php echo $grupo == "PD" ? 'selected="selected"' : ""; ?>>Practica Dirigida</option>
                              <option value="PC" <?php echo $grupo == "PC" ? 'selected="selected"' : ""; ?>>Practica Calificada</option>
                              <option value="EX" <?php echo $grupo == "EX" ? 'selected="selected"' : ""; ?>>Examen</option>
                              <option value="EE" <?php echo $grupo == "EE" ? 'selected="selected"' : ""; ?>>Examen de Entrada</option>
                              <option value="EF" <?php echo $grupo == "EF" ? 'selected="selected"' : ""; ?>>Examen Final</option>
                              <option value="TA" <?php echo $grupo == "TA" ? 'selected="selected"' : ""; ?>>Tareas</option>
                              <option value="PR" <?php echo $grupo == "PR" ? 'selected="selected"' : ""; ?>>Proyectos</option>
                            </select>
                          </div>
                        </td>


                        <!-- peso del item -->
                        <td class="haygrupo form-group"> 
                          <div class=" select-ctrl-wrapper select-azul ">
                            <select name="pungruporcentaje" class="select-peso-item pungruporcentaje form-control" vtem1="<?php echo $grupoitem; ?>">
                              <option value="P" <?php echo $grupoitem == "P" ? 'selected="selected"' : ""; ?>>Promedio</option>
                              <?php for ($i = 99; $i >= 0; $i--) {
                                ?>
                                <option value="<?php echo $i; ?>" <?php echo ($grupoitem == $i && $grupoitem!='P') ? 'selected="selected"' : ""; ?> ><?php echo $i; ?>%</option>
                              <?php } ?>
                            </select>
                          </div>
                        </td>
                        <td><i class="fa <?php echo $borrado == "true" ? 'fa-eye' : 'fa-trash';  ?> btnborrar"></i> </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <div class="col-md-12">
                <hr>
                <div class="row">
                  <div class="col-md-6 col-sm-12">
                    <h3>Fórmula Generada</h3>
                    <div class="formulagenerada"></div>
                  </div>



                  <div class="col-md-6 col-sm-12" id="_puntuacioncurso_">
                    <legend>
                      <h3><?php echo JrTexto::_('Score'); ?></h3>
                    </legend>
                    <div class="row" id="rownumber">
                      <div class="col-md-12 col-sm-12 form-group">
                        <label><?php echo ucfirst(JrTexto::_("Type")) ?></label><br>
                        <div class="select-ctrl-wrapper select-azul ">
                          <select name="calificacionen" id="calificacionen" class="form-control">
                            <option value="N"><?php echo ucfirst(JrTexto::_("Numeric")) ?></option>
                            <option value="P"><?php echo ucfirst(JrTexto::_("Percentage")) ?></option>
                            <option value="A"><?php echo ucfirst(JrTexto::_("Alfabetic")) ?></option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-12 form-group pornum">
                        <label class=""><?php echo ucfirst(JrTexto::_("Highest score")) ?><span style="display:none;">%</span></label>
                        <div class=" select-ctrl-wrapper select-azul ">
                          <select name="maxpuntaje" class="form-control maxpuntaje">
                            <?php for ($i = 100; $i >= 1; $i--) { ?>
                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php }  ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-12 form-group pornum">
                        <label><?php echo ucfirst(JrTexto::_("Minimum score to pass")) ?><span style="display:none;">%</span></label>
                        <div class="select-ctrl-wrapper select-azul ">
                          <select name="minpuntaje" class="form-control minpuntaje">
                            <?php for ($i = 0; $i <= 100; $i++) { ?>
                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php }  ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row" style="display: none;">
                      <div class="col-md-12 table-reponsive">
                        <table class="table table-striped table-hover tablepuntuacion">
                          <thead class="headings">
                            <tr>
                              <th><?php echo ucfirst(JrTexto::_("Scale name")); ?></th>
                              <th><?php echo ucfirst(JrTexto::_("Highest score")); ?></th>
                              <th><?php echo ucfirst(JrTexto::_("Minimum score")); ?></th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input type="text" class="form-control gris txtNombreEscala" name="txtNombreEscala" placeholder="e.g.: A, good, ..." value=""></td>
                              <td>
                                <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                                  <select name="maxpuntaje" id="maxpuntaje" class="form-control maxpuntaje" style="font-size: 15px;">
                                    <?php for ($i = 100; $i >= 1; $i--) { ?>
                                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php }  ?>
                                  </select></div>
                              </td>

                              <td>
                                <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                                  <select name="minpuntaje" id="minpuntaje" class="form-control minpuntaje" style="font-size: 15px;">
                                    <?php for ($i = 0; $i <= 100; $i++) { ?>
                                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php }  ?>
                                  </select> </div>
                              </td>
                              <td></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>


                </div>
              </div>
            </div>
            <div class="row" style="margin-top: 1ex;">
              <div class="col-md-12 text-center text-muted">
                <button type="button" class="btnguardarpuntaje btn btn-success"><i class="fa fa-save"></i> Guardar </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(e) {
    var configuracion_nota = <?php echo !empty($this->curso["formula_evaluacion"]) ? $this->curso["formula_evaluacion"] : 'false'; ?>;
    // console.log('configuracion_nota', configuracion_nota);
    var ventana = $('#vent-<?php echo $idgui; ?>');
    var tbpuntuacion = ventana.find('#_puntuacioncurso_');
    var tablepuntuacion = ventana.find('table#tablenuevapuntuacion');

    tbpuntuacion.on('change', '#calificacionen', function(ev) {
      ev.preventDefault();
      _this = $(this);
      $row = _this.closest('.row');
      val = _this.val();
      if (val == 'N') {
        $row.find('.pornum').show();
        $row.siblings('.row').hide();
        $row.find('label > span').hide();
      } else if (val == 'P') {
        $row.find('.pornum').show();
        $row.siblings('.row').hide();
        $row.find('label > span').show();
      } else {
        $row.find('.pornum').hide();
        $row.siblings('.row').show();
      }
    }).on('change', 'table .minpuntaje', function() {
      $v = $(this).val();
      $tr = $(this).closest('tr');
      $body = $(this).closest('tbody')
      if ($v == 0) $tr.nextAll().remove();
      else reordenartr($body, $tr);
    }).on('change', 'table .maxpuntaje', function() {
      _this = $(this);
      $v = $(this).val();
      $smin = _this.closest('tr').find('.minpuntaje');
      $vmin = $smin.val();
      $smin.children().remove();
      for (var i = 0; i < $v; i++) {
        $smin.append('<option value="' + i + '">' + i + '</option>');
      }
      $smin.children('option[value="' + $vmin + '"]').attr('selected', true);
      refreshSelects();
      $smin.trigger('change');
    }).on('change', '.pornum .maxpuntaje', function() {
      _this = $(this);
      $v = $(this).val();
      $smin = _this.closest('#rownumber').find('.minpuntaje');
      $vmin = $smin.val();
      $smin.children().remove();
      for (var i = 0; i < $v; i++) {
        $smin.append('<option value="' + i + '">' + i + '</option>');
      }
      $smin.children('option[value="' + $vmin + '"]').attr('selected', true);
      refreshSelects();
    })

    var reordenartr = function(tbody, tr) {
      $v = tr.find('select#minpuntaje').val();
      haynex = tr.next();
      $trclone = tbody.find('tr').first().clone(true);
      if ($v <= 2) return tr.nextAll().remove();
      if (haynex.length == 0) {
        $trclone.find('input.txtNombreEscala').val('');
        $trclone.find('select#maxpuntaje').find('option[value="' + ($v - 1) + '"]').prevAll().remove();
        $trclone.find('select#maxpuntaje').attr('disabled', true).css({
          'background': '#ccc'
        });
        $trclone.find('select#minpuntaje').find('option[value="' + ($v - 2) + '"]').nextAll().remove();
        $body.append($trclone);
      } else {
        $vmax1 = haynex.find('select#maxpuntaje').val();
        $trclone.find('select#maxpuntaje').find('option[value="' + ($v - 1) + '"]').prevAll().remove();
        haynex.find('select#maxpuntaje').children().remove();
        haynex.find('select#maxpuntaje').append($trclone.find('select#maxpuntaje').children());
        haynex.find('select#maxpuntaje').children().first().attr('selected', true);
        refreshSelects();
        $vmin1 = haynex.find('select#minpuntaje option').last().text();
        for ($i = $vmin1; $i < $vmax1; $i++) {
          haynex.find('select#minpuntaje').append('<option value="' + ($i + 1) + '">' + ($i + 1) + '</option>');
        }
        haynex.find('select#maxpuntaje').trigger('change');
        haynex.find('select#minpuntaje').trigger('change');
      }
    }
    var jsonformula = '';
    var __calculogrupopuntaje = function() {
      var formula = [];
      var grupos = [];
      var grupostmp = [];
      tablepuntuacion.find('tbody tr').first().nextAll().removeAttr('class');
      tablepuntuacion.find('tbody tr').each(function(ii, _trtm) {
        _trtmp = $(_trtm);
        var grupo = _trtmp.attr('grupo') || 'N';
        if (!_trtmp.hasClass('trclone')) {
          if (grupos[grupo] == undefined) {
            grupos[grupo] = [];
            grupostmp.push(grupo);
          }
          grupos[grupo].push(_trtmp);
        }
      })
      //Si hay un solo item que está en el grupo, no tiene sentido que indiquen un peso  
      if (grupostmp.length <= 1) tablepuntuacion.find('.haygrupo').hide();
      else {
        tablepuntuacion.find('.haygrupo').show();
      }
      var valgrupos_ = false;
      var sumgrupos_ = 0;
      var itemgrupo0 = -1;
      $.each(grupostmp, function(igt, ggt) {
        var grupo = grupos[ggt];
        var validar = false;
        var sumgrupo = 0;
        if (igt == 0) {
          formula['PF'] = [];
        }
        var igborrar = -1;
        var tipogrupo = 'P';
        $.each(grupo, function(ig, gg) {
          var borrar = gg.attr('borrar') ? true : false;
          var vg = gg.find('select.pungruporcentaje').val();
          if (borrar == false) {
            if (igborrar == -1) {
              igborrar = ig;
            }
            if (itemgrupo0 == -1) {
              itemgrupo0 = gg.find('select.pungruporcentaje').val();
              if (itemgrupo0 == 'P') {
                gg.closest('tbody').find('select.pungruporcentaje').hide().val('P');
                gg.find('select.pungruporcentaje').show();
                valgrupos_ = false;
              } else {
                valgrupos_ = true;
              }
            }
          }

          var punporcentaje = gg.find('select.punporcentaje').val();
          if (ig == igborrar) { // el primero activo de cada grupo
            punporcentaje = gg.find('select.punporcentaje').val();
            if (itemgrupo0 != 'P') {
              gg.find('select.pungruporcentaje').show();
              sumgrupos_ = sumgrupos_ + parseInt(vg);
            }
            //if(punporcentaje!='P' && grupo.length>0){
            tablepuntuacion.find('tr[grupo="' + ggt + '"] select.punporcentaje').show();
            $.each(tablepuntuacion.find('tr[grupo="' + ggt + '"]'), function(ee, uu) {
              if ($(uu).attr('borrar') == true) {} else $(uu).find('.select.punporcentaje').val('P').hide();
            })
            //}

            if (tablepuntuacion.find('tr[grupo="' + ggt + '"] select.punporcentaje:visible').length == 1 || grupo.length == 1) {
              tablepuntuacion.find('tr[grupo="' + ggt + '"] select.punporcentaje').find('option').removeAttr('selected');
              refreshSelects();
              tablepuntuacion.find('tr[grupo="' + ggt + '"] select.punporcentaje').val('P').hide().removeClass('bg-danger').css({
                'color': '#000'
              });
              //tablepuntuacion.find('tr[grupo="'+ggt+'"] select.punporcentaje').find('option').first().attr('selected','selected');
              gg.find('select.punporcentaje').val('P');
              validar = false;
              tipogrupo = 'P';
            } else if (punporcentaje == 'P') {
              tablepuntuacion.find('tr[grupo="' + ggt + '"] select.punporcentaje').val('P').hide();
              gg.find('select.punporcentaje').show().removeClass('bg-danger').css({
                'color': '#000'
              }).val('P');
              validar = false;
              tipogrupo = 'P';
            } else {
              tablepuntuacion.find('tr[grupo="' + ggt + '"] select.punporcentaje').show();
              tipogrupo = gg.find('select.punporcentaje').val();
              validar = true;
              sumgrupo = sumgrupo + parseInt(gg.find('select.punporcentaje').val());
              tipogrupo = '%';
            }
            punporcentaje = gg.find('select.punporcentaje').val();
          } else {
            gg.find('select.pungruporcentaje').hide();
            if (validar == true && borrar == false)
              sumgrupo = sumgrupo + parseInt(gg.find('select.punporcentaje').val());
          }
          if (tablepuntuacion.find('select.pungruporcentaje:visible').length == 1) {
            tablepuntuacion.find('select.pungruporcentaje').val('P');
            tablepuntuacion.find('th.haygrupo').hide();
            tablepuntuacion.find('td.haygrupo').hide();
          }


          if (ig == 0) {
            formula[ggt] = [{
              'tipo': punporcentaje == 'P' ? 'P' : '%',
              'id': gg.attr('id'),
              'prefijo': gg.find('input.punprefijo').val(),
              'valortipo': punporcentaje,
              'titulo': gg.find('td.punitem').children('input').val() || '',
              'typerecurso': (gg.attr('type') || ''),
              'borrado': borrar,
            }];
            formula['PF'][igt] = {
              'tipo': vg == 'P' ? 'P' : '%',
              'nombre': gg.find('select.pungrupo option:selected').text(),
              'prefijo': ggt,
              'formula': formula[ggt],
              'valortipo': vg,
              'borrado': borrar
            };
          } else {
            formula[ggt].push({
              'tipo': punporcentaje == 'P' ? 'P' : '%',
              'id': gg.attr('id'),
              'prefijo': gg.find('input.punprefijo').val(),
              'valortipo': punporcentaje,
              'titulo': gg.find('td.punitem').children('input').val() || '',
              'typerecurso': (gg.attr('type') || ''),
              'borrado': borrar
            });
            if (ig == igborrar) {
              formula['PF'][igt]['tipo'] = vg == 'P' ? 'P' : '%';
              formula['PF'][igt]['prefijo'] = ggt;
              formula['PF'][igt]['valortipo'] = vg;
              formula['PF'][igt]['borrado'] = borrar;
            }
          }
          if (gg.find('input.punprefijo').val() == '') {
            gg.find('input.punprefijo').css({
              'border': '1px solid red'
            });
          } else gg.find('input.punprefijo').css({
            'border': '1px solid #4683af'
          });
        })

        if ((validar == true && sumgrupo == 100) || (validar == false)) {
          tablepuntuacion.find('tr[grupo="' + ggt + '"] select.punporcentaje').removeClass('bg-danger').css({
            'color': '#000'
          }).closest('tr').removeClass('Error')
        } else {
          tablepuntuacion.find('tr[grupo="' + ggt + '"] select.punporcentaje').addClass('bg-danger').css({
            'color': '#fff'
          }).closest('tr').addClass('Error')
        }
        /*if(tablepuntuacion.find('tr[grupo="'+ggt+'"] select.punporcentaje:visible').length==1){
          tablepuntuacion.find('tr[grupo="'+ggt+'"] select.punporcentaje').hide();
        }*/


      })
      if ((valgrupos_ == true && sumgrupos_ == 100) || valgrupos_ == false) {
        tablepuntuacion.find('tbody tr').find('select.pungruporcentaje').removeClass('bg-danger').css({
          'color': '#000'
        }).closest('tr').removeClass('Error1');
      } else {
        tablepuntuacion.find('tbody tr').find('select.pungruporcentaje').addClass('bg-danger').css({
          'color': '#fff'
        }).closest('tr').addClass('Error1');
      }
      var txtformula = '';
      jsonformula = formulafinal = formula['PF'] || '';
      if (jsonformula != '')
        if (formulafinal.length == 1) {
          txtformula = '';
          var tipotmp = formulafinal[0].tipo;
          var nformula = formulafinal[0].formula.length;
          $.each(formulafinal[0].formula, function(i, v) {
            if (v.borrado == false) {
              if (i == 0) tipotmp = v.tipo;
              txtformula += (v.tipo == 'P' || v.tipo == 'T' ? (v.prefijo.toUpperCase()) : (' (' + v.prefijo.toUpperCase() + '*' + (v.valortipo / 100) + ') ')) + '+';
            }
          })
          if (tipotmp == 'P')
            txtformula = '<b>Promedio Final = PF = (' + txtformula.substring(0, txtformula.length - 1) + ')/' + nformula + '</b>';
          else
            txtformula = '<b>Promedio Final = PF =' + txtformula.substring(0, txtformula.length - 1) + '</b>';
        } else {
          txtformula = '';
          var nformula_ = 0;
          var txtformulatmp = '';
          var txtformulatmp_ = '';
          $.each(formulafinal, function(i, f) {
            var tipotmp = formulafinal[i].tipo;
            var nformula = 0;
            var txtformula = '';
            $.each(f.formula, function(i, v) {
              if (v.borrado == false) {
                nformula++;
                if (i == 0) tipotmp = v.tipo;
                txtformula += (v.tipo == 'P' || v.tipo == 'T' ? (v.prefijo.toUpperCase()) : (' (' + v.prefijo.toUpperCase() + '*' + (v.valortipo / 100) + ') ')) + '+';
              }
            })
            if (nformula == 1)
              txtformulatmp_ += f.nombre + ' = ' + f.prefijo + ' = ' + txtformula.substring(0, txtformula.length - 1) + '<br>'
            else if (tipotmp == 'P' && nformula > 1)
              txtformulatmp_ += f.nombre + ' = ' + f.prefijo + ' = (' + txtformula.substring(0, txtformula.length - 1) + ')/' + nformula + '<br>';
            else {
              if (nformula > 0) {
                if (f.prefijo == txtformula.substring(0, nformula - 1))
                  txtformulatmp_ += f.nombre + ' = ' + f.prefijo + '<br>';
                else
                  txtformulatmp_ += f.nombre + ' = ' + f.prefijo + ' = ' + txtformula.substring(0, txtformula.length - 1) + '<br>';
              }
            }
            if (nformula > 0) {
              nformula_++;
              txtformulatmp += (itemgrupo0 == 'P' ? (formulafinal[i].prefijo.toUpperCase()) : (' (' + formulafinal[i].prefijo.toUpperCase() + '*' + (formulafinal[i].valortipo / 100) + ') ')) + '+';
            }
          })
          if (nformula_ > 1) {
            tablepuntuacion.find('th.haygrupo').show();
            tablepuntuacion.find('td.haygrupo').show();
            //tablepuntuacion.find('select.pungruporcentaje option[selected]').removeAttr('selected');
          }

          if (itemgrupo0 == 'P' && nformula_ > 1)
            txtformula = txtformulatmp_ + '<b>Promedio Final = PF = (' + txtformulatmp.substring(0, txtformulatmp.length - 1) + ')/' + nformula_ + '<b>';
          else
            txtformula = txtformulatmp_ + '<b>Promedio Final = PF = ' + txtformulatmp.substring(0, txtformulatmp.length - 1) + '</b>';

        }
      ventana.find('.formulagenerada').html(txtformula);
    }

    tablepuntuacion.on('change', 'select.pungrupo', function(ev) {
      var haygrupo = false;
      tablepuntuacion.find('select.pungrupo').each(function(ii, ss) {
        _trtmp = $(ss).closest('tr');
        if (!_trtmp.hasClass('trclone')) {
          _trtmp.attr('grupo', $(ss).val());
        }
      })
      tablepuntuacion.find('tbody tr select.pungruporcentaje').first().trigger('change');
    }).on('blur', 'input.punprefijo', function() {
      __calculogrupopuntaje();
    }).on('change', 'input.punprefijo', function() {
      var vv = $(this).val() || '';
      if (vv == '') {
        $(this).css({
          'border': '1px solid red'
        });
      } else $(this).css({
        'border': 'none'
      });
    }).on('change', 'select.pungruporcentaje, select.punporcentaje', function(ev) {
      var v = $(this).val();
      $(this).find('option[selected]').removeAttr('selected');
      refreshSelects();
      $(this).val(v);     
      __calculogrupopuntaje();
    }).on('click', 'i.btnborrar', function(ev) {
      var tr = $(this).closest('tr');
      if ($(this).hasClass('fa-trash')) {
        tr.attr('borrar', true);
        $(this).removeClass('fa-trash').addClass('fa-eye');
      } else {
        tr.removeAttr('borrar');
        $(this).addClass('fa-trash').removeClass('fa-eye');
      }
      __calculogrupopuntaje();
    })

    ventana.on('click', '.btnguardarpuntaje', function(ev) {
      var json = {
        tipocalificacion: 'P',
        maxcal: 100,
        mincal: 51
      };
      var tipocal = $('#_puntuacioncurso_').find('#calificacionen').val();
      configuracion_nota.tipocalificacion = tipocal;
      if (tipocal == 'P' || tipocal == 'N') {
        configuracion_nota.maxcal = $('#_puntuacioncurso_').find('#rownumber select.maxpuntaje').val() || 20;
        configuracion_nota.mincal = $('#_puntuacioncurso_').find('#rownumber select.minpuntaje').val() || 13;
      } else {
        configuracion_nota.puntuacion = [];
        $('#_puntuacioncurso_').find('table.tablepuntuacion tbody tr').each(function(i, v) {
          var tr = $(this);
          var escale = tr.find('.txtNombreEscala').val();
          if (escale == '') {
            tr.find('.txtNombreEscala').css({
              border: '1px solid red'
            });
            __notificar({
              title: '<?php echo JrTexto::_('Attention'); ?>',
              html: '<?php echo JrTexto::_('Error in data') ?>',
              type: 'error'
            });
            hayerror = true;
            return;
          } else tr.find('.txtNombreEscala').css({
            border: '1px solid #4683af'
          });
          configuracion_nota.puntuacion.push({
            nombre: escale,
            maxcal: tr.find('select.maxpuntaje').val(),
            mincal: tr.find('select.minpuntaje').val()
          });
        })
      }

      var hayerror1 = tablepuntuacion.find('tbody tr.Error1').length
      var hayerror = tablepuntuacion.find('tbody tr.Error').length
      if (hayerror > 0 || hayerror1 > 0) {
        Swal.fire({
          title: '<?php echo JrTexto::_('Errores en Calculos'); ?>',
          text: 'Al Parecer tiene errores en los calculos revise porfavor, las sumatorias deben ser igual al 100 %',
          icon: 'info',
          confirmButtonText: '<?php echo JrTexto::_('Aceptar'); ?>'
        })
        return false;
      }
      configuracion_nota.formulatxt = $('div.formulagenerada').html();
      configuracion_nota.notas = jsonformula = undefined ? '' : jsonformula;
      var data = new FormData()
      data.append('idcurso', <?php echo $this->curso["idcursoprincipal"]; ?>);
      data.append('tipo', <?php echo $this->curso["tipo"]; ?>);
      data.append('idcc', <?php echo !empty($this->curso["idcomplementario"]) ? $this->curso["idcomplementario"] : 0; ?>);
      data.append('configuracion_nota', JSON.stringify(configuracion_nota));
      __sysAyax({
        fromdata: data,
        url: _sysUrlBase_ + 'cursos/guardarformula/',
        showmsjok: true,
      });
    })

    if (configuracion_nota != false) {
      var tipocal = configuracion_nota.tipocalificacion || 'P';
      $('#_puntuacioncurso_').find('#calificacionen').val(tipocal);
      if (tipocal == 'P' || tipocal == 'N') {
        $('#_puntuacioncurso_').find('#rownumber select.maxpuntaje').val(configuracion_nota.maxcal || 20);
        $('#_puntuacioncurso_').find('#rownumber select.minpuntaje').val(configuracion_nota.mincal || 14);
      } else {
        $('#_puntuacioncurso_').find('#calificacionen').trigger('change');
        var punt = configuracion_nota.puntuacion;
        var tr = $('#_puntuacioncurso_').find('table.tablepuntuacion tbody').find('tr')
        $.each(punt, function(i, v) {
          tr.find('.txtNombreEscala').val(v.nombre);
          tr.find('select.maxpuntaje').val(v.maxcal).trigger('change');
          tr.find('select.minpuntaje').val(v.mincal);
          if (v.mincal > 0) {
            tr.find('select.minpuntaje').trigger('change');
            tr = tr.next();
          }
        })
      }
      configuracion_nota.notas = configuracion_nota.notas == undefined ? [] : configuracion_nota.notas;
    } else {
      $('#_puntuacioncurso_').find('#calificacionen').val('N');
      $('#_puntuacioncurso_').find('#rownumber select.maxpuntaje').val(20);
      $('#_puntuacioncurso_').find('#rownumber select.minpuntaje').val(14);
      configuracion_nota = {
        tipocalificacion: 'P',
        maxcal: 100,
        mincal: 51,
        notas: []
      };
    }
    __calculogrupopuntaje();
  })
</script>