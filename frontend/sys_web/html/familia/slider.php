<?php
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Family Slider"); ?>'
  });
</script>

<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2 class="titulo-p text-center"><?=JrTexto::_("Family Slider")?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="modal fade" id="modalImg" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><?=JrTexto::_("Preview")?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <img id="img_modal" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <center>
            <div id="listado" class="col-sm-12"></div>
        </center>
        <br>
        <div class="row">
            <label for="input-file-max-fs"><?=JrTexto::_("New Image")?>:</label>
            <input type="file" id="input-file-max-fs-4" name="input-file-max-fs-4" class="dropify" data-max-file-size="10M" accept="image/*"/>
        </div>
      </div>
      <div class="x_footer">
        <center>
            <button type="button" class="btn btn-primary" onclick="subirFoto('familia-slider', 4)"> <?=JrTexto::_("Save")?> </button>
        </center>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function () {
    $('.dropify').dropify();

    var drEvent1 = $('#input-file-events-1').dropify();
    drEvent1.on('dropify.beforeClear', function (event, element) {
        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
    });
    drEvent1.on('dropify.afterClear', function (event, element) {
        alert('File deleted');
    });
    drEvent1.on('dropify.errors', function (event, element) {
        console.log('Has Errors');
    });

    var drEvent2 = $('#input-file-events-2').dropify();
    drEvent2.on('dropify.beforeClear', function (event, element) {
        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
    });
    drEvent2.on('dropify.afterClear', function (event, element) {
        alert('File deleted');
    });
    drEvent2.on('dropify.errors', function (event, element) {
        console.log('Has Errors');
    });

    var drEvent3 = $('#input-file-events-3').dropify();
    drEvent2.on('dropify.beforeClear', function (event, element) {
        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
    });
    drEvent3.on('dropify.afterClear', function (event, element) {
        alert('File deleted');
    });
    drEvent3.on('dropify.errors', function (event, element) {
        console.log('Has Errors');
    });

    var drEvent4 = $('#input-file-events-4').dropify();
    drEvent4.on('dropify.beforeClear', function (event, element) {
        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
    });
    drEvent4.on('dropify.afterClear', function (event, element) {
        alert('File deleted');
    });
    drEvent4.on('dropify.errors', function (event, element) {
        console.log('Has Errors');
    });
    listarSlider();
});

function listarSlider() {
    $.post(_sysUrlBase_+"json/familia_slider", 
    {
        idproyecto: "<?php echo $this->idproyecto; ?>",
        idempresa: "<?php echo $this->idempresa; ?>"
    }, function(resultado){
        if (resultado.code === 200) {
            var c = "'", html = "";
            html += '<table id="tblSlider" class="table table-striped table-bordered table-hover dt-responsive" width="100%">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height: 40px;">';
            html += '<th style="text-align: center"><?=JrTexto::_("Image")?></th>';
            html += '<th style="text-align: center"><?=JrTexto::_("Options")?></th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            $.each(resultado.data, function (i, item) {
                html += '<tr>';
                var src = _sysUrlBase_ + "static/media/familia-slider/" + item.link + "?x=" + (Math.random()*(999999 - 0));
                html += '<td align="center"><img src="' + src + '" width="100px" height="50px" data-toggle="modal" data-target="#modalImg" onclick="modalImg(' + c + src + c + ')"></td>';
                html += '<td align="center" style="vertical-align:middle;">';
                html += '<button type="button" class="btn btn-xs" onclick="eliminarSlider(' + item.codigo + ')" title="Eliminar Imagen"><i class="fa fa-trash"></i></button>';
                html += '</td>';
                html += '</tr>';
            });
            html += '</tbody>';
            html += '</table>';
            $("#listado").html(html);
            $('#tblSlider').dataTable({
                "language": {
                    "url": _sysUrlBase_+"static/tema/paris/datatable_json/personalizar.json"
                },
                "searching": false,
                "lengthChange": false
            });
        }
    }, 'json');
}

function modalImg(src) {
    $("#img_modal").attr("src", src);
}

function subirFoto(directorio, numero) {
    Swal.fire({
    title: '<?php echo JrTexto::_('Confirm');?>',
    text: '<?php echo JrTexto::_('Do you want to upload image?'); ?>',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: '<?php echo JrTexto::_('I agree');?>',
    cancelButtonText: '<?php echo JrTexto::_('Cancel');?>',
    }).then((result) => {
        if (result.value) {
            Swal.fire({
                title: '<?=JrTexto::_("Processing")?>...',
                showConfirmButton: false,
                allowOutsideClick: false
            });
            var id = "input-file-max-fs-" + numero;
            var inputFileImage = document.getElementById(id);
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('archivo', file);
            data.append('directorio', directorio);
            $.ajax({
                url: _sysUrlBase_+'json/familia_slider/guardar',
                type: 'POST',
                contentType: false,
                data: data,
                processData: false,
                cache: false,
                success: function(data) {
                    var data = JSON.parse(data);
                    if(data.code === 200){
                        Swal.fire('Exito!',data.msj,'success');
                        listarSlider();
                    } else {
                        Swal.fire('Advertencia!', data.msj,'warning');
                    }
                }
            });
        }
    });
}

function eliminarSlider(codigo) {
    Swal.fire({
    title: '<?php echo JrTexto::_('Confirm');?>',
    text: '<?php echo JrTexto::_('Do you want to delete the image?'); ?>',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: '<?php echo JrTexto::_('I agree');?>',
    cancelButtonText: '<?php echo JrTexto::_('Cancel');?>',
    }).then((result) => {
        if (result.value) {
            Swal.fire({
                title: '<?=JrTexto::_("Deleting, please wait")?>...',
                showConfirmButton: false,
                allowOutsideClick: false
            });
            var data = new FormData();
            data.append('codigo', codigo);
            $.ajax({
                url: _sysUrlBase_+'json/familia_slider/eliminar',
                type: 'POST',
                contentType: false,
                data: data,
                processData: false,
                cache: false,
                success: function(data) {
                    var data = JSON.parse(data);
                    if(data.code === 200){
                        Swal.fire('<?=JrTexto::_("Success")?>!',data.msj,'success');
                        listarSlider();
                    }
                }
            });
        }
    });
}
</script>