<?php
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Foro Familia"); ?>'
  });
</script>
<form id="frmAddConsulta">
    <div class="modal fade" id="mdAddConsulta" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mdAddConsultaTitle"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <input type="hidden" id="respuesta">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p><?=JrTexto::_("Consultation content")?> <font color="red">(*)</font>
                                <textarea type="text" id="txtcontenidoc" name="txtcontenidoc" class="form-control input-sm" required="" rows="3" style="resize: none;"></textarea>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" aria-hidden="true"><i class="fa fa-save"> </i> <?=JrTexto::_("Save")?></button>
                    <button id="btncerrarc" type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-close"> </i> <?=JrTexto::_("Cerrar")?></button>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2 class="titulo-p text-center"><?=JrTexto::_("Family Forum")?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="foro" class="card collapsed-card" style="margin-bottom: 0;">
            <!-- <div class="card-header text-center">
                <button type="button" id="btnAddConsulta" class="btn btn-outline-primary btn-xs" data-toggle="modal" data-target="#mdAddConsulta">
                    <i class="fas fa-plus-circle"></i> Hacer una Pregunta
                </button>
            </div> -->
            <div class="card-body">
                <div class="col-md-12">
                    <div style="overflow-y: scroll; max-height: calc(100vh - 140px);" id="listarConsulta"></div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
    listarConsultas();
});
function listarConsultas() {
    $.post(_sysUrlBase_+"json/familia_foro", 
    {
        sql: "1",
        idproyecto: "<?php echo $this->idproyecto; ?>",
        idempresa: "<?php echo $this->idempresa; ?>"
    }
    ,function (datosJSON) {
        if (datosJSON.code === 200) {
            var html = "",
                contador = 0,
                ultimo,
                style_;
            if (Object.keys(datosJSON.data).length > 0) {
                $.each(datosJSON.data, function(i, item) {
                    if (item.padre === "p" && i > 0) {
                        html += '</div></div>';

                        html += '<div class="card-footer" data-toggle="modal" data-target="#mdAddConsulta" onclick="abrirModalAddConsulta(' + ultimo + ')" style="cursor: pointer;"><i class="fa fa-pencil"></i>&nbsp;<?=JrTexto::_("Write a reply")?></div>';

                        html += '</div><br>';
                    }
                    if (item.padre === "p") {
                        ultimo = item.codigo;
                        html += '<div class="post card">';

                        //preguntas
                        html += '<div class="card-header"><span data-card-widget="collapse" style="cursor:pointer; font-size: 15px;"><b>' + item.persona + '</b> [' + item.fecha + ']<span class="badge badge-light ml-2 commentbox"><span class="ncomment">' + item.hijos + '</span> <i class="fa fa-commenting-o" aria-hidden="true"></i></span>';
                        html += '<p style="word-wrap: break-word;">' + item.contenido + '</p></div>';
                        style_ = '';
                        if(parseInt(item.hijos) == 0){
                            style_ = ' style="display: none;"';
                        }
                        html += '<div class="card-body"' + style_ + '>';
                        html += '<div style="margin-left: 20px;">';
                    } else {
                        //respuestas
                        html += '<span style="font-size: 15px;"><b>' + item.persona + '</b> [' + item.fecha + ']</span>';
                        html += '<p style="word-wrap: break-word;">' + item.contenido + '</p>';
                    }
                    contador++;
                });

                if (contador > 0) {
                    html += '</div></div>';

                    html += '<div class="card-footer" data-toggle="modal" data-target="#mdAddConsulta" onclick="abrirModalAddConsulta(' + ultimo + ')" style="cursor: pointer;"><i class="fa fa-pencil"></i>&nbsp;<?=JrTexto::_("Write a reply")?></div>';

                    html += '</div><br>';
                }
                $("#listarConsulta").html(html);
            } else {
                $("#listarConsulta").html(`
                    <hr>
                    <div class="row justify-content-center">
                        <div class="col-sm-12 text-center">
                            <h4 style="color:#50555a;"><?=JrTexto::_("No questions have been registered yet")?>.</h4>
                        </div>
                    </div>
                `);
            }
        }
    }, "json");
}
function abrirModalAddConsulta(respuesta){
    $("#mdAddConsultaTitle").html("<?=JrTexto::_("Add reply")?>");
    $("#txtcontenidoc").val("");
    $("#respuesta").val(respuesta);
}
$("#btnAddConsulta").click(function(){
    abrirModalAddConsulta("");
});
$("#frmAddConsulta").submit(function(evento) {
    evento.preventDefault();
    $.post(_sysUrlBase_ + "json/familia_foro/guardar2", {
        idproyecto: "<?php echo $this->idproyecto; ?>",
        idempresa: "<?php echo $this->idempresa; ?>",
        contenido: $("#txtcontenidoc").val(),
        respuesta: $("#respuesta").val()
    }, function(resultado) {
        if (resultado.code === 200) {
            $("#btncerrarc").click();
            listarConsultas();
        } else if(resultado.code === 300){
            alert(resultado.msj);
        }
    }, 'json');
});
</script>