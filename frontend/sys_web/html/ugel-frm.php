<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php //echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/ugel">&nbsp;<?php echo JrTexto::_('Ugel'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="formventana form-horizontal form-label-left"  idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idugel" id="idugel" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Name');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtDescripcion" name="descripcion" required="required" class="form-control" value="<?php echo @$frm["descripcion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Abrev');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtAbrev" name="abrev" required="required" class="form-control" value="<?php echo @$frm["abrev"];?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Dre');?> <span class="required"> * </span></label>
              <div class="">
                <div class="select-ctrl-wrapper select-azul">
                <select id="txtIddre" class="form-control select-ctrl " name="iddre" datavalue="<?php echo @$frm["iddre"];?>" ></select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Provincia');?> <span class="required"> * </span></label>
              <div class="">                
                <div class="select-ctrl-wrapper select-azul">
                <select id="txtIdprovincia" class="form-control select-ctrl " name="idprovincia" datavalue="<?php echo @$frm["idprovincia"];?>" ></select>
                </div>                                  
              </div>
            </div>          
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveUgel" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('ugel'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm_ugel.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui; ?>');
    if(!frm.hasClass('ismodaljs')) frm_ugel($('#vent-<?php echo $idgui; ?>'));
  })
</script>