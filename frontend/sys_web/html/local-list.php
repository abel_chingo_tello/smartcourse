<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Locales"); ?>'
    });
    /*addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });*/
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;} 
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="row">
    <div class="col">    
       <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <div class="cajaselect">
                <select name="iddre" id="iddre" class="form-control">
                  <option value="0"><?php echo JrTexto::_('Only'); ?></option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <div class="cajaselect">
                <select name="idugel" id="idugel" class="form-control">
                  <option value="0"><?php echo JrTexto::_('Only'); ?></option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <!--label><?php //echo  ucfirst(JrTexto::_("text to search"))?></label-->
              <div class="input-group">
                <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 text-center">             
             <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/local/agregar" data-titulo="<?php echo ucfirst(JrTexto::_('Add'))." ".JrTexto::_('Local') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
             <a class="btn btn-warning btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/local/importar" data-titulo="<?php echo ucfirst(JrTexto::_('Import'))." ".JrTexto::_('Local') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Import')?></a>
          </div>
        </div>
      </div>
    </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th>
                  <th><?php echo JrTexto::_("Direccion") ;?></th>
                  <th><?php echo JrTexto::_("Ubigeo") ;?></th>
                  <!--th><?php //echo JrTexto::_("Tipo") ;?></th>
                  <th><?php //echo JrTexto::_("Vacantes") ;?></th>
                  <th><?php //echo JrTexto::_("Idugel") ;?></th>
                  <th><?php //echo JrTexto::_("Idproyecto") ;?></th-->
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5d9e5b5fa455f='';
function refreshdatos5d9e5b5fa455f(){
    tabledatos5d9e5b5fa455f.ajax.reload();
}
$(document).ready(function(){  
  var estados5d9e5b5fa455f={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5d9e5b5fa455f='<?php echo ucfirst(JrTexto::_("local"))." - ".JrTexto::_("edit"); ?>';
  var draw5d9e5b5fa455f=0;
  var _imgdefecto='';
  var ventana_5d9e5b5fa455f=$('#ventana_<?php echo $idgui; ?>');
  var _cargarcombos5d9e5b5fa455f=function(){
    $sel=ventana_5d9e5b5fa455f.find('select#iddre');
    $sel.children('option:eq(0)').siblings().remove();
    var fd = new FormData();   
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/min_dre',
      callback:function(rs){       
        iddre=$sel.attr('datavalue');
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.iddre+'" '+(v.iddre==iddre?'selected="selected"':'')+ '>'+v.iddre +" : "+ v.descripcion+'</option>');
          })
        $sel.trigger('change');
      }})
    }
    _cargarcombos5d9e5b5fa455f();


  tabledatos5d9e5b5fa455f=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
        {'data': '<?php echo JrTexto::_("Direccion") ;?>'},
        {'data': '<?php echo JrTexto::_("Ubigeo") ;?>'},
            /*{'data': '<?php // echo JrTexto::_("Tipo") ;?>'},
            {'data': '<?php //echo JrTexto::_("Vacantes") ;?>'},
            {'data': '<?php //echo JrTexto::_("Idugel") ;?>'},
            {'data': '<?php //echo JrTexto::_("Idproyecto") ;?>'},*/
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'json/local',
        type: "post",                
        data:function(d){
          d.json=true                   
          d.iddre=ventana_5d9e5b5fa455f.find('select#iddre').val();
          d.idugel=ventana_5d9e5b5fa455f.find('select#idugel').val();
          d.texto=ventana_5d9e5b5fa455f.find('#texto').val();
          draw5d9e5b5fa455f=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5d9e5b5fa455f;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push({             
              '#':(i+1),
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,
                '<?php echo JrTexto::_("Direccion") ;?>': data[i].direccion,
                '<?php echo JrTexto::_("Ubigeo") ;?>': data[i].id_ubigeo,
              '<?php echo JrTexto::_("Tipo") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].tipo==0?'':'active')+' " data-campo="tipo"  data-id="'+data[i].idlocal+'"> <i class="fa fa'+(data[i].tipo=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5d9e5b5fa455f[data[i].tipo]+'</a>',
              '<?php echo JrTexto::_("Vacantes") ;?>': data[i].vacantes,
              '<?php echo JrTexto::_("Idugel") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].idugel==0?'':'active')+' " data-campo="idugel"  data-id="'+data[i].idlocal+'"> <i class="fa fa'+(data[i].idugel=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5d9e5b5fa455f[data[i].idugel]+'</a>',
              '<?php echo JrTexto::_("Idproyecto") ;?>': data[i].idproyecto,
                            //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/local/editar/?id='+data[i].idlocal+'" data-titulo="'+tituloedit5d9e5b5fa455f+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idlocal+'" ><i class="fa fa-trash"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5d9e5b5fa455f.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5d9e5b5fa455f();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5d9e5b5fa455f();
  }).on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idlocal',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/local/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5d9e5b5fa455f.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/local/setcampo','masvalores':{idlocal:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idlocal',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/local/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5d9e5b5fa455f.ajax.reload();
          }
        }
    });
  }).on('change','select#iddre',function(ev){
    $sel=ventana_5d9e5b5fa455f.find('select#idugel');
    $sel.children('option:eq(0)').siblings().remove();
    if($(this).val()==''||$(this).val()=='0'){return $sel.trigger('change'); }
    var fd = new FormData();
    fd.append('iddre',$(this).val()); 
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/ugel',
      callback:function(rs){        
        var idsel=$sel.attr('datavalue');
        dt=rs.data;
        $.each(dt,function(i,v){
          $sel.append('<option value="'+v.idugel+'" '+(v.idugel==idsel?'selected="selected"':'')+ '>'+v.idugel+" : "+ v.descripcion+'</option>');
        })
        $sel.trigger('change');
    }})
  }).on('change','select#idugel',function(ev){ refreshdatos5d9e5b5fa455f(); 
  }).on('click','.btnbuscar',function(ev){ refreshdatos5d9e5b5fa455f(); 
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var titulo=$(this).attr('data-titulo')||'';
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      refreshdatos5d9e5b5fa455f();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frm_local(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_localimport(_md);
            })
          });
       }           
    })   
  })

});
</script>