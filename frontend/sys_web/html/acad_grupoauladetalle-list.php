<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_('Back')?></a></li> 
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li-->        
        <li class="active">&nbsp;<?php echo JrTexto::_("Grupo - courses"); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="row">
    <div class="col">    
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-4" >
          <!--label>Idgrupoaula</label-->
          <div class="select-ctrl-wrapper select-azul">
            <select name="idgrupoaula" id="idgrupoaula" class="form-control select-ctrl">
                <?php 
                if(!empty($this->fkidgrupoaula))
                foreach ($this->fkidgrupoaula as $r) { ?><option value="<?php echo $r["idgrupoaula"]?>" <?php echo $this->idgrupoaula==$r["idgrupoaula"]?'selected="selectted"':'';?> ><?php echo $r["nombre"] ?></option>
                 <?php } ?>
            </select>
          </div>
        </div>  

        <!--div class="col-xs-6 col-sm-4 col-md-4">
          <div class="form-group">
            <label><?php //echo  ucfirst(JrTexto::_("text to search"))?></label>
            <div class="input-group">
              <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php //echo  ucfirst(JrTexto::_("text to search"))?>">
              <span class="input-group-addon btn btnbuscar"><?php //echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
            </div>
          </div>
        </div!-->
        <div class="col-md-6 col-sm-6 col-xs-12 text-center">             
           <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/acad_grupoauladetalle/agregar" data-titulo="<?php echo JrTexto::_('Assign course'); ?>" ><i class="fa fa-plus"></i> <?php echo JrTexto::_('Assign course')?></a>
           <a class="btn btn-success btnmatricular" href="<?php echo $this->documento->getUrlSitio();?>/alumnos/" data-titulo="<?php echo JrTexto::_('Matricular'); ?>" ><i class="fa fa-users"></i> <?php echo JrTexto::_('Assign Students')?></a>
          </div>
        </div>
      </div>
  </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>                 
                    <th><?php echo JrTexto::_("Course") ;?></th>
                    <th><?php echo JrTexto::_("Teacher") ;?></th>
                    <th class=""><?php echo JrTexto::_("Local") ;?></th>
                    <th><?php echo JrTexto::_("Ambient") ;?></th>                    
                    <th><?php echo JrTexto::_("Date") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5dc3603a44f8c='';
function refreshdatos5dc3603a44f8c(){
    tabledatos5dc3603a44f8c.ajax.reload();
}
$(document).ready(function(){ 
   var infoempresa=__infoempresa();
  console.log(infoempresa); 
  var estados5dc3603a44f8c={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5dc3603a44f8c='<?php echo ucfirst(JrTexto::_("Assign course"))." - ".JrTexto::_("edit"); ?>';
  var draw5dc3603a44f8c=0;
  var _imgdefecto='';
  var ventana_5dc3603a44f8c=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5dc3603a44f8c=ventana_5dc3603a44f8c.find('.table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
     
      "ajax":{
        url:_sysUrlBase_+'json/acad_grupoauladetalle',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.idgrupoaula=$('#idgrupoaula').val(),            
            draw5dc3603a44f8c=d.draw;           
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5dc3603a44f8c;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            finicio=data[i].fecha_inicio||'';
            ffinal=data[i].fecha_final||'';
            finicio=finicio.substring(0,10);
            ffinal=ffinal.substring(0,10);                    
            datainfo.push([            
              (i+1), data[i].idcurso+': '+data[i].strcurso,
              (data[i].strdocente||'-'),
               data[i].strlocal||'<?php echo JrTexto::_('Only') ?>',
              data[i].strambiente||'<?php echo JrTexto::_('Only') ?>',
              finicio+' - '+ffinal,

         /* '<?php //echo JrTexto::_("Idcurso") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].idcurso==0?'':'active')+' " data-campo="idcurso"  data-id="'+data[i].idgrupoauladetalle+'"> <i class="fa fa'+(data[i].idcurso=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5dc3603a44f8c[data[i].idcurso]+'</a>',
          '<?php //echo JrTexto::_("Iddocente") ;?>': data[i].iddocente,
          '<?php //echo JrTexto::_("Idlocal") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].idlocal==0?'':'active')+' " data-campo="idlocal"  data-id="'+data[i].idgrupoauladetalle+'"> <i class="fa fa'+(data[i].idlocal=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5dc3603a44f8c[data[i].idlocal]+'</a>',
          '<?php //echo JrTexto::_("Idambiente") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].idambiente==0?'':'active')+' " data-campo="idambiente"  data-id="'+data[i].idgrupoauladetalle+'"> <i class="fa fa'+(data[i].idambiente=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5dc3603a44f8c[data[i].idambiente]+'</a>',
          '<?php //echo JrTexto::_("Nombre") ;?>': data[i].nombre,
            '<?php //echo JrTexto::_("Fecha_inicio") ;?>': data[i].fecha_inicio,
          '<?php //echo JrTexto::_("Fecha_final") ;?>': data[i].fecha_final,
          '<?php //echo JrTexto::_("Idgrado") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].idgrado==0?'':'active')+' " data-campo="idgrado"  data-id="'+data[i].idgrupoauladetalle+'"> <i class="fa fa'+(data[i].idgrado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5dc3603a44f8c[data[i].idgrado]+'</a>',
          '<?php //echo JrTexto::_("Idsesion") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].idsesion==0?'':'active')+' " data-campo="idsesion"  data-id="'+data[i].idgrupoauladetalle+'"> <i class="fa fa'+(data[i].idsesion=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5dc3603a44f8c[data[i].idsesion]+'</a>',*/
          //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
         '<a class="btn btn-xs btnmatricular"  href="'+_sysUrlSitio_+'/alumnos/?idgrupoauladetalle='+data[i].idgrupoauladetalle+'&idcurso='+data[i]["idcurso"]+'" data-titulo="<?php echo JrTexto::_('Assign Students')?>"><i class="fa fa-users"></i></a><a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/acad_grupoauladetalle/editar/?id='+data[i].idgrupoauladetalle+'" data-titulo="'+tituloedit5dc3603a44f8c+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idgrupoauladetalle+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='ES'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5dc3603a44f8c.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5dc3603a44f8c();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5dc3603a44f8c();
  })

  .on('change','#idgrupoaula',function(ev){refreshdatos5dc3603a44f8c();
  }).on('change','#idcurso',function(ev){refreshdatos5dc3603a44f8c();
  }).on('change','#idlocal',function(ev){refreshdatos5dc3603a44f8c();
  }).on('change','#idambiente',function(ev){refreshdatos5dc3603a44f8c();
  }).on('change','#datefecha_inicio',function(ev){ refreshdatos5dc3603a44f8c();
  }).on('change','#datefecha_final',function(ev){ refreshdatos5dc3603a44f8c();
  }).on('change','#idgrado',function(ev){refreshdatos5dc3603a44f8c();
  }).on('change','#idsesion',function(ev){refreshdatos5dc3603a44f8c();
  }).on('change','#datefechamodificacion',function(ev){ refreshdatos5dc3603a44f8c();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5dc3603a44f8c();
  })
 

  ventana_5dc3603a44f8c.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idgrupoauladetalle',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/acad_grupoauladetalle/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5dc3603a44f8c.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/acad_grupoauladetalle/setcampo','masvalores':{idgrupoauladetalle:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idgrupoauladetalle',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_grupoauladetalle/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5dc3603a44f8c.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    url+='&idgrupoaula='+$('#idgrupoaula').val()||'';
    var titulo=$(this).attr('data-titulo')||'';
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      tabledatos5dc3603a44f8c.ajax.reload();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frm(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_import(_md);
            })
          });
       }           
    })   
  }).on('click','.btnmatricular',function(e){
    e.preventDefault();
    var _this=$(this);
    var url=_this.attr('href');
      if(url.indexOf('?')!=-1) url+='&plt=mantenimientos';
      else url+='?plt=mantenimientos';
      url=url+'&idgrupoaula='+ventana_5dc3603a44f8c.find('select#idgrupoaula').val();
      redir(url);
  })
});
</script>