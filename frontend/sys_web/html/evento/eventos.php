<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        var idpersona = <?= $this->usuario['idpersona'] ?>;

        addItemNavBar({
            text: '<?php echo JrTexto::_("Events"); ?>'
        });

        addButonNavBar({
            id: 'compose',
            class: 'btn btn-success btn-sm Agreggarforo',
            text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('New') ?>',
            attr: {
                "data-toggle": "modal"
            },
            attr: {
                "data-target": "#exampleModal"
            },
            click: `modalNuevo()`
        });
    </script>
<?php } ?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlStatic(); ?>/tema/paris/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlStatic(); ?>/tema/paris/fullcalendar/dist/fullcalendar.print.min.css" media="print">
<div class="container my-font-all">
    <div class="col-sm-12">
        <div id="cont-leyenda" class="cont-leyenda my-mg-auto">
            <span class="cont-ley"><span class="icon-ley color-ley-autor color-ley-pasado"></span><span class="text-ley"><?=JrTexto::_("Past")?></span></span>
            <span class="cont-ley"><span class="icon-ley color-ley-profe"></span><span class="text-ley"><?=JrTexto::_("Teacher")?></span></span>
            <span class="cont-ley"><span class="icon-ley color-ley-autor"></span><span class="text-ley"><?=JrTexto::_("My authorship")?></span></span>
        </div>
    </div>

    <div class="row col-sm-12">
        <div class="my-card my-shadow">
            <div id="calendar"></div>
        </div>

    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        <?=JrTexto::_("Event")?>
                        <a id="zoom-url" class="my-hide" href="#" target="_blank"><span id="tipo-conf">Zoom </span><sup><i class="fa fa-external-link" aria-hidden="true"></i></sup></a>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add" onsubmit="return saveEvent();">
                        <div id="cont-autor" class="form-group my-hide">
                            <label for=""><?=JrTexto::_("Author")?></label>
                            <input type="text" class="form-control" disabled name="" id="autor" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                            <input type="number" name="id_programacion" id="id_programacion" disabled class="my-hide">
                            <label for="fecha"><?=JrTexto::_("Event date")?></label>
                            <input required type="date" name="fecha" id="fecha" class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for=""><?=JrTexto::_("Title")?></label>
                            <input required type="text" name="" id="titulo" class="form-control" placeholder="<?=JrTexto::_("Title of the event")?>...">
                        </div>
                        <div class="form-group">
                            <label for=""><?=JrTexto::_("Description")?></label>
                            <textarea placeholder="<?=JrTexto::_("Event description")?>..." required class="form-control" name="" id="detalle" rows="2"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 my-del-pd pd-fix">
                                <label for=""><?=JrTexto::_("Start time")?></label>
                                <input required type="time" class="form-control" name="" id="hora_comienzo" placeholder="">
                            </div>
                            <div class="col-md-6 my-del-pd">
                                <label for=""><?=JrTexto::_("End time")?></label>
                                <input required type="time" class="form-control" name="" id="hora_fin" placeholder="">
                            </div>
                        </div>
                        <br>
                        <?php if ($this->usuario['idrol'] === '2') { ?>
                            <div id="cont-select" class="my-hide">
                                <div class="cont-radio col-sm-12 text-center">
                                    <label for="" class="my-float-l"><?=JrTexto::_("Publish")?>: </label>
                                    <div class="form-check form-check-inline">
                                        <input required class="form-check-input" type="radio" name="radio" id="r1" value="idgrupoaula">
                                        <label class="form-check-label" for="r1"><?=ucfirst(JrTexto::_("Group"))?></label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input required class="form-check-input" type="radio" name="radio" id="r2" value="idgrupoauladetalle">
                                        <label class="form-check-label" for="r2"><?=JrTexto::_("Course")?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="" id="selectList">
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminar()" class="btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> <?=JrTexto::_("Delete")?></button>
                                <button id="btn-save" type="submit" form="form-add" class="btn btn-primary"><?=JrTexto::_("Save")?></button>
                            </div>

                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal"><?=JrTexto::_("Close")?></button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal"><?=JrTexto::_("Cancel")?></button>
                                    <button type="submit" form="form-add" class="btn btn-primary"><?=JrTexto::_("Create")?></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo $this->documento->getUrlStatic(); ?>/tema/paris/moment/moment.js"></script>
<script src="<?php echo $this->documento->getUrlStatic(); ?>/tema/paris/fullcalendar/dist/locale/es.js"></script>
<script src="<?php echo $this->documento->getUrlStatic(); ?>/tema/paris/fullcalendar/dist/fullcalendar.min.js"></script>