<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Recursos_colabasig_comentario"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">            
                                                                                                              
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>           
          </div>
        </div>
    </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idasignacion") ;?></th>
                    <th><?php echo JrTexto::_("Idpersona") ;?></th>
                    <th><?php echo JrTexto::_("Comentario") ;?></th>
                    <th><?php echo JrTexto::_("Fecha") ;?></th>
                    <th><?php echo JrTexto::_("File") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
            </tbody>
            </table>
        </div>
    </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="recursos_colabasig_comentario" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idrecursocomentario" id="idrecursocomentario" value="">
          <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idasignacion');?> <span class="required"> * </span></label>
                <input type="text"  id="idasignacion" name="idasignacion" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idpersona');?> <span class="required"> * </span></label>
                <input type="text"  id="idpersona" name="idpersona" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Comentario');?> <span class="required"> * </span></label>
                <input type="text"  id="comentario" name="comentario" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Fecha');?> <span class="required"> * </span></label>
                <input type="text"  id="fecha" name="fecha" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('File');?> <span class="required"> * </span></label>
                <input type="text"  id="file" name="file" required="required" class="form-control" value="">
                          </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="recursos_colabasig_comentario" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos6009d790b33e2='';
function refreshdatos6009d790b33e2(){
    tabledatos6009d790b33e2.ajax.reload();
}
$(document).ready(function(){  
  var estados6009d790b33e2={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit6009d790b33e2='<?php echo ucfirst(JrTexto::_("recursos_colabasig_comentario"))." - ".JrTexto::_("edit"); ?>';
  var draw6009d790b33e2=0;
  var _imgdefecto='';
  var ventana_6009d790b33e2=$('#ventana_<?php echo $idgui; ?>');
   tabledatos6009d790b33e2=ventana_6009d790b33e2.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/recursos_colabasig_comentario',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw6009d790b33e2=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw6009d790b33e2;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push([            
              (i+1),
              data[i].idasignacion,data[i].idpersona,data[i].comentario,data[i].fecha,data[i].file,              //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idrecursocomentario+'" data-titulo="'+tituloedit6009d790b33e2+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idrecursocomentario+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_6009d790b33e2.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos6009d790b33e2();
  }).on('blur','.textosearchlist',function(ev){refreshdatos6009d790b33e2();
  })

  .on('click','.btnbuscar',function(ev){ refreshdatos6009d790b33e2();
  })
 

  ventana_6009d790b33e2.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idrecursocomentario',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/recursos_colabasig_comentario/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos6009d790b33e2.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/recursos_colabasig_comentario/setcampo','masvalores':{idrecursocomentario:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idrecursocomentario',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/recursos_colabasig_comentario/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos6009d790b33e2.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modalrecursos_colabasig_comentario(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modalrecursos_colabasig_comentario(el);
  })

  /*Formulario*/
  var __modalrecursos_colabasig_comentario=function(el){
    var titulo = '<?php echo JrTexto::_("Recursos_colabasig_comentario"); ?>';
    var filtros=$('#ventana_<?php echo $idgui; ?>');
    var frm=$('#frm<?php echo $idgui; ?>').clone();     
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/recursos_colabasig_comentario/guardar',
          showmsjok:true,
          callback:function(rs){   tabledatos6009d790b33e2.ajax.reload(); __cerrarmodal(_md);  }
        });
    })

    var pk=el.attr('pk')||'';
    if(pk!=''){      
      var data=new FormData();
      data.append('idrecursocomentario',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/recursos_colabasig_comentario',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];  
            //_md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());          
          }
         }
      });
    }else{
      /*if(filtros.find('select#tipo_portal').val()!='')
        _md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());*/
    }
  }
});
</script>