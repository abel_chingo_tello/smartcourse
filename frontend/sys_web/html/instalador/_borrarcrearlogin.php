<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->infoempresa)) $frm=$this->infoempresa;
$ruta=URL_MEDIA."/static/media/nofoto.jpg";
$imgdefecto=$ruta;
if(!empty($frm["logo"])) $ruta=URL_MEDIA.$frm["logo"];
?>
<?php /*if(!$ismodal){?>
<div class="col-12">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("crear pantalla inicio sesion"); ?></li>
  </ol>
</nav>
</div>
<?php } */?>
<div class="col-12 text-center">
<h1>Paso 02 : <small>Configuracion de Proyecto</small></h1>
</div>
<div class="col-12">
    <form method="post" id="frmproyecto"  target="" enctype="multipart/form-data" class="form-horizontal form-label-left" >
        <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
        <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
        <input type="hidden" name="fecha" id="fecha" value="<?php echo !empty($frm["fecha"])?$frm["fecha"]:date('Y-m-d');?>">
        <input type="hidden" name="logo" id="logo" value="<?php echo @$frm["logo"];?>">
        <!--input type="hidden" name="logo2" id="logo2i" value="<?php //echo @$frm["logo"];?>"-->
        <input type="hidden" name="imagenfondo" id="imagenfondo" value="">
        <input type="hidden" name="videofondo" id="videofondo" value="">
        <div class="row">
            <div class="form-group col-md-4 col-sm-12">
                <label><?php echo JrTexto::_('Name') ?></label>
                <input type="" name="nombre" class="form-control" placeholder="<?php echo JrTexto::_('Name of the project, headquarters, branch') ?>">
            </div>
            <div class="form-group col-md-4 col-sm-12">
                <label><?php echo JrTexto::_('Link') ?>:</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><?php echo $this->documento->getUrlBase(); ?></div>
                    </div>
                    <input type="text" name="nombreurl" id="nombreurl" value="<?php echo @$frm["nombreurl"]; ?>" class="form-control" placeholder="<?php echo JrTexto::_('EMP001') ?>">
                </div>               
            </div>
            <div class="form-group col-md-4 col-sm-12">
                <label><?php echo JrTexto::_('Type Bussiness') ?></label>
                <div class="cajaselect">
                    <select name="tipoempresa" id="tipoempresa" class="form-control">                     
                        <option value="1" <?php echo @$frm["tipo_empresa"]==1?'selected="selcted"':'';?>>Coorporativo</option>
                        <option value="0" <?php echo @$frm["tipo_empresa"]==0?'selected="selcted"':'';?>>IIEE</option>                        
                    </select>
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-12"></div>
        </div>
        <!--div class="row text-center selimagen">
            <div class="col-6">
                <img src="<?php //echo $this->documento->getUrlStatic(); ?>/media/login1.jpg" class="nosel img-fluid imgseleccionaruna" data-value="1">
            </div>
            <div class="col-6">
                <img src="<?php //echo $this->documento->getUrlStatic(); ?>/media/login2.jpg" class="nosel img-fluid imgseleccionaruna" data-value="2">
            </div>
        </div>
        <div><hr></div-->
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 form-group">
                <label><?php echo JrTexto::_("Typo de fondo") ?></label>
                <div class="cajaselect">
                    <select name="tipofondo" id="tipofondo" class="form-control">	                  
                        <option value="imagen">Imagen</option>
                        <option value="video">Video</option>
                        <option value="color">Color de fondo</option> 	                 
                    </select>
                </div>
                <br>
                <div style="position: relative;" class="frmchangeimage text-center" id="archivofondo">
                    <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                    <div id="sysfileimg" class="aquiseveimagen" data-old='imagen' data-file='<?php echo $imgdefecto; ?>'>
                    <img src="<?php echo $imgdefecto;?>" nombreguardar="<?php echo !empty($frm["idempresa"])?('Emp'.$frm["idempresa"].(!empty($frm["idproyecto"])?('_Pr'.$frm["idproyecto"]):'')):'';?>_fondo" class="__subirfile img-fluid center-block" data-type="imagen" id="imagenfondo" style="max-width: 200px; max-height: 150px;">
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 form-group text-center"><br><br><br>
                <label><?php echo JrTexto::_("logo") ?>(*)</label>                
                <div style="position: relative;" class="frmchangeimage text-center" id="archivologo1">
                    <div class="toolbarmouse text-center"><span class="btn btnremoveimagelogo"><i class="fa fa-trash"></i></span></div>
                    <div id="sysfilelogo">
                    <img src="<?php echo $ruta;?>" nombreguardar="<?php echo !empty($frm["idempresa"])?('Emp'.$frm["idempresa"].(!empty($frm["idproyecto"])?('_Pr'.$frm["idproyecto"]):'')):'';?>_logo"  class="__subirfilelogo img-fluid center-block" data-type="imagen" id='logo' style="max-width: 200px; max-height: 150px;">
                    </div>
                </div>
            </div>
            <!--div class="col-12 col-sm-6 col-md-4 form-group text-center logoopcional"><br><br><br>
                <label><?php //echo JrTexto::_("logo 02") ?> (opcional) </label>                
                <div style="position: relative;" class="frmchangeimage text-center" id="archivologo2">
                    <div class="toolbarmouse text-center"><span class="btn btnremoveimagelogo"><i class="fa fa-trash"></i></span></div>
                    <div id="sysfilelogo">
                    <img src="<?php //echo $ruta;?>" class="__subirfilelogo img-fluid center-block" data-type="imagen"  id="logo2" style="max-width: 200px; max-height: 150px;">
                    </div>
                </div>
            </div-->
        </div>
        <div class="row">		             
            <div class="col-12 form-group text-center">
                <hr>
                <button type="" class="btn btn-default" onclick="history.back();" > <i class=" fa fa-arrow-left"></i> <?php echo ucfirst(JrTexto::_('Regresar'));?> </button>
                <button type="submit" class="btn btn-success" ><?php echo ucfirst(JrTexto::_('Continuar'));?> <i class=" fa fa-arrow-right"></i> </button>              
            <br><br>
            </div>
        </div>
	</form>
</div>
<script type="text/javascript">
 var url_media='<?php echo $this->documento->getUrlBase();?>';
 var imgdefecto='<?php echo $imgdefecto; ?>';
	$(document).ready(function(){
        var jsonlogin={tipologin:1,tipofondo:'imagen',colorfondo:'rgba(20, 92, 186, 0.93)',imagenfondo:imgdefecto,videofondo:'',logo1:''};
        var idempresa=parseInt('<?php echo $frm["idempresa"]; ?>');
        var idproyecto=parseInt('<?php echo $frm["idproyecto"];?>');
   
        $('.imgseleccionaruna').on('click',function(ev){
            let $img=$(this);
            let v=$img.attr('data-value');          
            if($img.hasClass('nosel')){
                $img.closest('.selimagen').find('img').removeClass('Selected').addClass('nosel');
                $img.removeClass('nosel').addClass('Selected');
                if(v=='2') $('.logoopcional').show();
                else $('.logoopcional').hide();
                jsonlogin.tipologin=parseInt(v);
            }else{
                $img.addClass('nosel').removeClass('Selected');
                jsonlogin.tipologin=0;
            }
        });
        var htmltmp={'imagen':'','video':'','color':''};

        $('#tipofondo').on('change',function(ev){
            let tipo=$(this).val();
            let sysfile=$('#sysfileimg');
            let oldtipo=sysfile.attr('data-old');
            let oldfile=sysfile.attr('data-file');
            htmltmp[oldtipo]= sysfile.html(); 
            sysfile.attr('data-old',tipo);
            jsonlogin.tipofondo=tipo; 
            if(htmltmp[tipo]!=""){               
                sysfile.html(htmltmp[tipo]);
                if(tipo=='color') sysfile.siblings('.toolbarmouse').hide(); 
                else sysfile.siblings('.toolbarmouse').show(); 
            }else if(tipo=='imagen'){               
                sysfile.siblings('.toolbarmouse').show();
                let imgf=$('input#imagenfondo').val();
                let imgfondo=imgf!=''?(url_media+imgf):imgdefecto;               
                sysfile.html('<img src="'+imgfondo+'" id="imagenfondo" data-type="imagen" class="__subirfile img-fluid center-block" style="max-width: 200px; max-height: 150px;">')
            }else if(tipo=='video'){               
                sysfile.siblings('.toolbarmouse').show();
                let vif=$('input#videofondo').val();
                if(vif!=''){
                    sysfile.html('<video src="'+url_media+vif+'" id="videofondo" data-type="video" controls="true" style="max-width: 250px; max-height: 200px;"></video><img src="'+_sysUrlBase_+'/static/media/novideo.png" data-type="video" class="invisible img-fluid center-block" style="display:none; max-width: 200px; max-height: 150px;">')
                }else
                sysfile.html('<video src="" id="videofondo" controls="true" data-type="video" class="invisible" style="display:none; max-width: 250px; max-height: 200px;"></video><img src="'+_sysUrlBase_+'/static/media/novideo.png" data-type="video" class=" img-fluid center-block" style="max-width: 200px; max-height: 150px;">');
            }else if(tipo=='color'){
                sysfile.siblings('.toolbarmouse').hide();          
                let colortmp=jsonlogin.colorfondo!=''?jsonlogin.colorfondo:'rgba(20, 92, 186, 0.93)';
                sysfile.html('<input type="" name="colorfondo" value="'+colortmp+'" class="vercolor colorfondo form-control" >');
                sysfile.find('input[name="colorfondo"]').val(colortmp);
                sysfile.find('input[name="colorfondo"]').minicolors({opacity: true,format:'rgb'});
			    sysfile.find('input[name="colorfondo"]').minicolors('settings',{value:colortmp});
                $('.fondologin').css('background',colortmp);
            }
        });

        $('.__subirfilelogo').on('click',function(){
            let $img=$(this);            
            __subirfile({file:$img,typefile:'imagen',uploadtmp:true,guardar:true},function(rs){                    
                let f=rs.media.replace(url_media, '');
				$('input#'+$img.attr('id')).val(f);
                $img.attr('src',url_media+f);
            });
        });

        $('#sysfileimg').on('click','img,video',function(){
            let $img=$(this);           
            let ct=$img.closest('#sysfileimg');
            let tf=$img.attr('data-type');
            if(tf=='video')$img=ct.children('video');               
            __subirfile({file:$img,typefile:tf,uploadtmp:true,guardar:true,dirmedia:'empresa/'},function(rs){
                let f=rs.media.replace(url_media, '');               
                if(tf=='video'){
                    $('input#videofondo').val(f);
                    $img.siblings().hide()
                }else{
                    $('input#imagenfondo').val(f); 

                } 
                $img.attr('src',url_media+f);  
                $img.removeClass('invisible').show();          
            });
        });

        $('.btnremoveimagelogo').on('click',function(){
            let $img= $(this).closest('.frmchangeimage').find('img');
            $img.attr('src',_sysUrlBase_+'/static/media/nofoto.jpg');
            $('input#'+$img.attr('id')).val('');
        })

        $('.btnremoveimage').on('click',function(){
            let $img= $(this).closest('#archivofondo').find('img');
            if($img.parent().attr('data-old')=='video'){
                $img.attr('src',_sysUrlBase_+'/static/media/novideo.png');
                $img.siblings('video').hide();
                $('input#videofondo').val('');
            }else{
                $img.attr('src',_sysUrlBase_+'/static/media/nofoto.jpg');
                $('input#imagenfondo').val('');
            }                
            $img.removeClass('invisible').show();                     
        })

		$('#frmproyecto').bind({    
     		submit: function(ev){
                ev.preventDefault();
                let jsontmp1=jsonlogin;
                if(jsonlogin.tipofondo=='video'){ 
                    jsontmp1.videofondo=$('input#videofondo').val();
                    jsontmp1.imagenfondo='';
                  
                }else if(jsonlogin.tipofondo=='imagen'){  
                    jsontmp1.imagenfondo=$('input#imagenfondo').val();
                    jsontmp1.videofondo='';
                }               
                jsontmp1.logo1=$('input#logo').val();
                //jsontmp1.logo2=$('#logo2i').val();
                var jsontmp= JSON.stringify(jsontmp1);
                var tmpfrm=document.getElementById('frmproyecto');
                 var formData = new FormData(tmpfrm);                    
                    formData.append('jsonlogin',jsontmp); 
                    //formData.append('logo',jsontmp1.logo1);  
                __sysAyax({
                    fromdata:formData,
                    showmsjok:true,
                    url:_sysUrlBase_+'json/proyecto/guardar',
                    callback:function(rs){
                       let idproyecto=rs.newid;
                       $('#idproyecto').val(idproyecto);
                       window.location=_sysUrlBase_+'/proyectoempresa/configurarmodulos/?idempresa='+$('#idempresa').val()+'&idproyecto='+idproyecto;
                    }
                });
                return false;
     		}
        });

                
        if(idempresa!=''){
            var formData = new FormData();
            formData.append('idempresa',idempresa);
            formData.append('idproyecto',idproyecto);
            __sysAyax({
                fromdata:formData,
                showmsjok:false,
                url:_sysUrlBase_+'json/proyecto',					
                callback:function(rs){
                    if(rs.data.length>0){
                        let info=rs.data[0];
                        $('#idempresa').val(info.idempresa);
                        $('#idproyecto').val(info.idproyecto);
                        $('#fecha').val(info.fecha);
                        let jsontmp=JSON.parse(info.jsonlogin);
                        $.extend(jsonlogin,jsontmp);
                    } 
                    if(jsonlogin.imagenfondo!=''){
                        $('input#imagenfondo').val(jsonlogin.imagenfondo);
                        $('img#imagenfondo').attr('src',url_media+jsonlogin.imagenfondo);
                    }
                    if(jsonlogin.videofondo!=''){
                        $('input#videofondo').val(jsonlogin.videofondo);
                        $('video#videofondo').attr('src',url_media+jsonlogin.videofondo);
                    }

                    $('.imgseleccionaruna[data-value="' + parseInt(jsonlogin.tipologin) + '"]').trigger('click');
                    $('#tipofondo').val(jsonlogin.tipofondo).trigger('change');
                                    
                    if(jsonlogin.logo1.trim()!=""){$('img#logo1').attr('src',url_media+jsonlogin.logo1);}
                    else{$('img#logo1').attr('src',url_media+($('input#logo1i').val()||imgdefecto));}
                    //if(jsonlogin.logo2.trim()!="") $('img#logo2').attr('src',url_media+jsonlogin.logo2);
                   // else $('img#logo2').attr('src',url_media+($('input#logo2i').val()||imgdefecto));
                }
            });
        }
    });
</script>