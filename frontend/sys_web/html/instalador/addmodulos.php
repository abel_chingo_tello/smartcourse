<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$imgdefecto=$this->documento->getUrlStatic()."/media/web/nofoto.jpg";
$imgfoto=URL_MEDIA."/static/media/usuarios/user_avatar.jpg";
$jsonlogin='{logo1:""}';
$usu='';
if(!empty($this->curusuario)) $usu=$this->curusuario;
if(!empty($this->proyecto)){
    $py=$this->proyecto;
    //var_dump($this->proyecto);
    $jsonlogin=$py["jsonlogin"];
}
?>

<style type="text/css">
  .fondocolor{
      background-color:#00A9AC;
  }
  .barleft{
      min-height:calc( 100vh);
      padding:0px;
  }
  .content1{
    min-height:calc( 100vh - 145px);
    /*border-bottom-left-radius: 2ex;*/
    border-top-left-radius: 0.8ex;
    background-color:#fff;
    padding:0px;
  }
  .iframecontent{
    border:0px;
    width:100%;
    height:calc(100vh - 4px);
    margin:0px;
    padding:0px;
    /*border-bottom-left-radius: 2ex;*/
    border-top-left-radius: 0.8ex;
}
  .side-menu{
      margin-top:1.5em;

  }
  .side-menu li{
      width:100%;     
      border-top: 0.20ex solid #0404044f;
  }
  .side-menu li:last-child{      
      border-bottom: 0.20ex solid #0404044f;
  }
  .side-menu li a{
      color:#fff;
      font-family: fuente_pri;
      font-size: 16px;
      padding:1ex;
      display:block;
      text-decoration:none;
  }
  .side-menu li.active a{
       background-color:#fdf9f950;
  }
  .side-menu li.active:after {
     position: absolute;
    z-index: -1;
    content: '';
    top: calc(50% - 10px);
    right: 0;
    border-style: solid;
    border-width: 10px 10px 10px 0;
    border-color: transparent #fff transparent transparent;
    -webkit-transition-duration: 0.3s;
    transition-duration: 0.3s;
    -webkit-transition-property: transform;
    transition-property: transform;
}
.editbtn-admin{
    #position:absolute;
    width:100%;
    bottom:0px;
    display:block;
    background-color:#00000020;
    
}
.editbtn-admin a{
    text-align:center;
    text-decoration:none;
    padding:0.5ex;
    width:49%;
    color:#fff;
}
#mostrarmodulos{
    display:block;
}
#mostrarmodulos li{
    display:inline-block;
    padding:1ex;
}
#mostrarmodulos li span{
    border: 1px solid #ccc;
    display: block;
    padding: 1.5em 1ex;
    text-align: center;
    background: #cccccc4d;
}
.ui-state-highlight{
    display:block;
}
body{
    overflow-y: auto;
}
.iframecontent{
    overflow-y:auto;
}
::-webkit-scrollbar {
    width: .5em;
}

::-webkit-scrollbar:horizontal {
    height: .5em;
}
 
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}
 
::-webkit-scrollbar-thumb {
  background-color: darkgrey;
  outline: 1px solid slategrey;
}

</style>
<div class="row fondocolor" >
<div class="col-md-2 col-sm-4 col-xs-12  barleft">
    <div class="infouser text-center" style=" min-height:130px; color:#fff">        
        <img class="fotouserlogin user img-thumbnail img-fluid rounded-circle" src="<?php echo $imgfoto;?>" style=" max-height:120px; max-width:130px;"><br>
        <b><?php echo @$usu["nombre_full"]; ?></b><br>
        <small><b>Rol:</b> <?php echo @$usu["rol"]; ?></small>
    </div>
    <ul class="nav side-menu">
<?php if(count($usu['roles'])>1){ ?><li class="hvr-bounce-to-right"><a href="javascript:void(0);" data-href="<DOMINIO>.COM/sesion/cambiarrol/?tpl=out"><i class="fa fa-cubes fa-fw" style="font-size: 20px"></i> Cambiar Rol</a></li><?php } ?>
        <li class="hvr-bounce-to-right active"><a href="javascript:void(0);" data-href="<DOMINIO>.COM/personal/perfil"><i class="fa fa-user fa-fw" style="font-size: 20px"></i> Mi Perfil</a></li>                    
        <li class="hvr-bounce-to-right cerrarsesion"><a href="javascript:void(0);" data-href="cerrarsesion"><i class="fa fa-power-off fa-fw" style="font-size: 20px"></i> Cerrar Sesión</a></li>
    </ul>
    <?php if($usu["idrol"]==1||$usu["idrol"]==10){?>
    <div class="editbtn-admin">
        <a href="#panelmodulos" class="col-md-6 col-sm-12 enmodal btn d-inline-block hvr-overline-from-center text-center"><i class="fa fa-institution"></i></a>
        <a href="#settingpanel" class="col-md-6 col-sm-12 enmodal btn d-inline-block hvr-overline-from-center text-center"><i class="fa fa-cogs"></i></a>
    </div>
    <?php } ?>
</div>
<div class="col-md-10 col-sm-8 col-xs-12">
    <div class="row" style="height:145px">
        <div class="col-6 text-left">
            <br>
            <img class="user " id="logoempresa1" src="<?php echo $imgdefecto;?>" style=" max-height:120px; max-width:300px;">            
        </div>
        <div class="col-6 text-right" style="padding-top:1em;">
            <a href="#" class="btn btn-primary"><i class="fa fa-video-camara"></i><i class="fa fa-question-circle"></i></a>
            <a href="#" class="btn btn-primary"><i class=" fa fa-home"></i></a>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12 content1">
        <iframe id="iframecontent" src="https://abacoeducacion.org" class="iframecontent"></iframe>
        </div>
    </div>       
</div>

<div class="modal" tabindex="-1" id="panelmodulos">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title">Seleccione Modulos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" >
                <ul id="mostrarmodulos">
                </ul>        
            </div>
            <!--div class="modal-footer text-center">
                <button type="button" class="btnaplicarestilo btn btn-primary">Aplicar</button>       
            </div-->
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" id="settingpanel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title">Estilo de la pagina</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-6 col-md-6 form-group">
                    <label><?php echo JrTexto::_("Color de fondo") ?></label>
                    <input type="" name="colorfondo" value="" class="vercolor colorfondo form-control" >
                </div>        
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btnaplicarestilo btn btn-primary">Aplicar</button>       
            </div>
        </div>
    </div>
</div>

<script>
var py=JSON.parse('<?php echo $jsonlogin ?>');
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgdefecto; ?>';
var fotodefecto='<?php echo $imgfoto ?>';
var jsonproyecto=JSON.parse('<?php echo $jsonlogin;?>');
var idproyecto=parseInt('<?php echo $py["idproyecto"]; ?>');
var idempresa=parseInt('<?php echo $py["idempresa"]; ?>');
var dtuserlogin=<?php echo json_encode($this->curusuario); ?>;
$(document).ready(function(){

    var cambiarfondo=function(colortmp){
        $('#settingpanel').find('input[name="colorfondo"]').val(colortmp);
        $('#settingpanel').find('input[name="colorfondo"]').minicolors({opacity: true,format:'rgb'});
        $('#settingpanel').find('input[name="colorfondo"]').minicolors('settings',{value:colortmp});
        $('.fondocolor').css('background-color',colortmp);
    }

    __sysajax({ // cargar informacion de usuario;
        showmsjok:false,
        url:url_media+'/personal/json_datos',
        callback:function(rs){
            if(rs.code==200){
                dt=rs.datos;
                var foto=(dt.foto==''||dt.foto==undefined)?fotodefecto:dt.foto;
                 $('img.fotouserlogin').attr('src',foto);
                foto=url_media+'/static/media/usuarios/'+(foto.replace(url_media+'/static/media/usuarios/','')); 
                __isFileExist(foto,function(rs2){ $('img.fotouserlogin').attr('src',foto);})
            }
        }
    })

    __sysajax({ // carga la edicion de los modulos
        showmsjok:false,
        url:_sysUrlBase_+'/modulos/buscarjson',
        callback:function(rs){
            var md=rs.data;
            $.each(md,function(i,v){
               $('#mostrarmodulos').append('<li class="col-md-4 col-sm-6 col-xs-12 btn" data-id="'+v.idmodulo+'" data-link="'+v.link+'" data-nombre="'+v.nombre+'" data-icon="'+v.icono+'" ><span><i class="fa '+v.icono+'"></i><br>'+v.nombre+'</span></li>');
            })
            $( "#mostrarmodulos" ).sortable({
                placeholder: "ui-state-highlight",
                stop:function(ev,iu){                   
                   editarmenus();
                }
                });
            $( "#mostrarmodulos" ).disableSelection();
            updatemenus();
        }
    });

    var editarmenus=function(){
        var lis=$('#mostrarmodulos').find('li');
        var smenu=$('.side-menu');
        smenu.find('.smenu').remove();
        var ultimoli=smenu.children('li').last();
        var menus={};
        var idrol=parseInt(dtuserlogin.idrol);
        $.each(lis,function(i,k){
            var v=$(k);
            if(v.hasClass('active')){
                var idmodulo=v.attr('data-id')||0;
                var add=true
                if(idmodulo==7 || idmodulo==1)
                 if(idrol!=10&&idrol!=1)add=false;
               // if((idrol!=10&&idrol!=1)&&idmodulo==7)continue;
               if (add==true){
                menus[i]={id:v.attr('data-id'),nombre:v.attr('data-nombre'),link:v.attr('data-link'),icono:v.attr('data-icon'),texto:v.attr('data-texto')};
                var html='<li  class="ini_mod'+v.attr('data-nombre').toLowerCase()+'  hvr-bounce-to-right smenu smenu'+v.attr('data-id')+'" data-id="'+v.attr('data-id')+'">';
                    html+='<a  href="javascript:void(0);" data-href="'+v.attr('data-link')+'" ><i class="fa '+v.attr('data-icon')+' fa-fw" style="font-size: 20px"></i> ';
                    html+=v.attr('data-nombre')+'</a></li>';
                $(html).insertBefore(ultimoli);
               } 
            }
        });
        jsonproyecto.menus=menus;        
    }

    var updatemenus=function(){       
        if(jsonproyecto.menus!=''&&jsonproyecto.menus!=undefined){ 
            var ultimoliadd=$('#mostrarmodulos').find('li').first();
            $.each(jsonproyecto.menus,function(i,v){
                $($('#mostrarmodulos li[data-id="'+v.id+'"]')).insertBefore(ultimoliadd);              
                $('#mostrarmodulos li[data-id="'+v.id+'"]').addClass('active btn-success');                
            })
            editarmenus();
        }
    }

    $('#mostrarmodulos').on('click','li',function(){
        var li=$(this);
        li.toggleClass('active');
        var id=li.attr('data-id');
        if(li.hasClass('active')){
            li.addClass('btn-success');            
        }else{
            $('.side-menu li.smenu'+id).remove();
            li.removeClass('btn-success');
        }
        editarmenus();
    });

    var redimencionarchat=function(){
        var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;      
        $('.barleft').css('height',height+'px');
    }
    $(window).resize(function(){
        redimencionarchat(); 
    });

    $('.side-menu').on('click','a',function(ev){
        ev.preventDefault();
        var a=$(this);
        var li=a.closest('li');
        li.addClass('active').siblings('li').removeClass('active');       
        var url=a.attr('data-href');
        if(url=='cerrarsesion'){
            __sysajax({ showmsjok:true,  url:_sysUrlBase_+'/login/salir', callback:function(rs){
               if(rs.code==200)
               window.location.href=rs.urlredir;
             }});
             return;            
        }

        //console.log(dtuserlogin["idrol"]);
        var roltmp=dtuserlogin["idrol"]==1?'admin':((dtuserlogin["idrol"]==2)?'docente':'alumno');        
        url=url.replace(/<DOMINIO>.COM/gi,url_media);
        url=url.replace(/<TYPE_USER>/gi,roltmp);
        url=url.replace(/<TYPE>/gi,roltmp);
        url=url.replace(/<ID_ALUMNO>/gi,dtuserlogin["idpersona"]);
        url=url.replace(/<SLUG_PROYECTO>/gi,'PY'+idproyecto);
        url=url.replace(/<USERNAME>/gi,dtuserlogin["usuario"]);
        url=url.replace(/<PASSWORD>/gi,dtuserlogin["clave"]);
        url=url.replace(/<IDIOMA>/gi,_sysIdioma_);
        url=url.indexOf('?')==-1?(url+'?plt=sintop'):(url+'&plt=sintop');
        $('#iframecontent').attr('src',url+'&idproyecto='+idproyecto+'&idioma='+_sysIdioma_);
    })

    $('.editbtn-admin').on('click','a',function(ev){
        ev.preventDefault();
        var a=$(this);        
        if(a.hasClass('enmodal')){
            var hr=a.attr('href');           
            $(hr).modal('show');
        }else{
            $('#iframecontent').attr('src',a.attr('href'));
        }       
    })
    $('.btnaplicarestilo').click(function(){
        $('.fondocolor').css('background-color',$('#settingpanel').find('input[name="colorfondo"]').val());
    });

    var guardarjsoproyecto=function(){
         var colortmp=$('.fondocolor').css('background-color');
        jsonproyecto.colorfondoadmin=colortmp;
        var jsontmp= JSON.stringify(jsonproyecto);
        var dt=	{idempresa:idempresa,idproyecto:idproyecto,jsonlogin:jsontmp};
        __sysajax({
            fromdata:dt,
            showmsjok:false,
            url:_sysUrlBase_+'/proyecto/guardarProyecto',           
        });
    }
    $('#settingpanel').on('hidden.bs.modal', function (e) {
        guardarjsoproyecto();                                
    })
    $('#panelmodulos').on('hidden.bs.modal', function (e) {
        guardarjsoproyecto();                                
    })
    var iniciarver=function(){
        //alert(py.colorfondoadmin);
        //idempresa
        var logo_subido = url_media + '/static/media/empresa/logo-'+idempresa+'.jpg';
        var logo1=(py.logo1=='' || py.logo1==undefined)?imgdefecto:url_media+py.logo1;
        //var logo1=(py.logo1=='')?imgdefecto:logo_subido;
        $('#logoempresa1').attr('src',logo1);
        $('#iframecontent').attr('src',url_media+'/personal/perfil/?plt=sintop');
        var colortmp=$('.fondocolor').css('background-color');        
        if(jsonproyecto.colorfondoadmin==''||jsonproyecto.colorfondoadmin==undefined) jsonproyecto.colorfondoadmin=colortmp;
        else colortmp=jsonproyecto.colorfondoadmin;         
        cambiarfondo(colortmp);
       // if($('[data-nombre="'+v.attr('data-nombre')+'"]').length)
        setTimeout(function(){
            $('.ini_modsmartcourse').children('a').trigger('click');
        },400)        
    }
    iniciarver();
})
</script>