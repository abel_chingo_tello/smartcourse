<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->empresa)) $frm=$this->empresa;
$ruta="/static/media/web/nofoto.jpg";
$imgdefecto=$ruta;
$logodefecto=URL_MEDIA.'/static/media/sinfoto.jpg';
if(!empty($frm["logo"])) $logodefecto=URL_MEDIA.$frm["logo"];
$py=!empty($this->proyecto)?$this->proyecto:'';
$jsonlogin=!empty($py["jsonlogin"])?$py["jsonlogin"]:"";
?>
<?php /*if(!$ismodal){?>
<div class="col-12">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("crear pantalla inicio sesion"); ?></li>
  </ol>
</nav>
</div>
<?php } */?>
<style>
.fondologin{   
    height:99.9vh;
    width:100%;
    position:relative;
    overflow:hidden;
}
.contentmedia{
    position:relative;   
    width:100%;   
    height:100%;
    overflow:hidden;
    opacity:0.7;  
}

.contentmedia img, .contentmedia video{
    margin: auto;
    position: relative;
    z-index: 1;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    min-width: 100%;
    min-height: 100%;
    opacity:0.85;
}

.pnllogin{
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    z-index:3;
}
.frmpnllogin{
    border: 1px solid #ce8989;
    background: #4a4845e0;
    color: #fff;    
}
.imglogo1{
    max-width:300px;
    max-height:250px;
}

</style>
<div class="fondologin">
    <div class="contentmedia"></div>
    <div class="pnllogin ">
      <div class="text-center"><img src="<?php echo $logodefecto; ?>" class="imglogo2" style="max-width:300px; max-height:250px;"></div>
      <div  class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 form-group enlogo1 text-center"><img src="<?php echo $logodefecto; ?>" class="imglogo1" style="max-width:300px; max-height:250px;"></div>
            <div class="col-12 col-sm-12 col-md-6 form-group enlogo1">
                <div class="alert alert-danger alert-dismissible fade" style="display:none" role="alert" id="alertmsj">
                    <h4>Error</h4>
                    <span class="mensaje">
                    <?php if(!empty($this->error)){
                        foreach($this->error as $e){  echo $e."<br>"; }
                    } ?>                    
                    </span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <form method="post" id="frmlogin"  target="" enctype="multipart/form-data" class="form-horizontal form-label-left" >
                <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
                <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$py["idproyecto"];?>">
                <div class="row frmpnllogin">
                    <div class="col-12 col-sm-12 col-md-12 form-group">
                        <label><strong>Usuario</strong><span class="required">*</span></label>              
                        <input type="text"  name="usuario"  class="form-control"  placeholder="Ingresa tu usuario">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 form-group">
                        <label><b>Clave</b><span class="required">*</span></label>              
                        <input type="password"  name="clave"  class="form-control"  placeholder="Ingresa tu clave">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 form-group">
                        <button class="btn btn-primary form-control">Ingresar <i class="fa fa-arrow-right"></i></button>
                    </div>
                </div>
            </form>
            </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
 var url_media='<?php echo URL_MEDIA;?>';
 var imgdefecto='<?php echo $imgdefecto; ?>';
 var jsonlogin={tipologin:1,tipofondo:'imagen',colorfondo:'rgba(46, 47, 47, 0.84)',imagenfondo:imgdefecto,videofondo:'',logo1:'',logo2:''};
 var idempresa='<?php echo !empty($frm["idempresa"])?$frm["idempresa"]:0; ?>';
 var jsonproyecto=JSON.parse('<?php echo !empty($jsonlogin)?$jsonlogin:'{}';?>');
 var _sysIdioma_='ES';
	$(document).ready(function(){
        $.extend(jsonlogin,jsonproyecto);      
        $('.fondologin').css('background',jsonlogin.colorfondo);
        htmlfondo='';
        if(jsonlogin.tipofondo=='imagen'){
            htmlfondo='<img src="'+url_media+jsonlogin.imagenfondo+'">'
        }else if(jsonlogin.tipofondo=='video'){
            htmlfondo='<video autoplay="true" loop="" muted="" src="'+url_media+jsonlogin.videofondo+'"></video>'
        }
        
        $('.contentmedia').html(htmlfondo);
        if(jsonlogin.tipologin==1){
            $('img.imglogo2').hide(0);
            $('.enlogo1').removeClass('col-md-6').addClass('col-12');
            $('.pnllogin').css({'max-width':'500px'});
        }

        $('#frmlogin').bind({    
     		submit: function(ev){
                ev.preventDefault();
     			var dt=__formdata('frmlogin');
                __sysajax({
                    fromdata:dt,
                    showmsjok:true,
                    url:_sysUrlBase_+'/sesion/loginuserempresa',
                    callback:function(rs){
                       if(rs.code=='ok'||rs.code==200){
                           $('#alertmsj').children('h4').html("");
                           $('#alertmsj').children('.mensaje').html(rs.msj);
                           $('#alertmsj').removeClass('alert-danger').addClass('alert-success').show();
                           setTimeout(function(){
                            window.location=_sysUrlBase_+'/instalar/paso3/';
                           },1000)
                       }else{
                            $('#alertmsj').children('h4').html("Error");
                            $('#alertmsj').children('.mensaje').html(rs.msj);
                            $('#alertmsj').addClass('alert-danger').removeClass('alert-success').show();
                       }
                    }
                });
                return false;
     		}
        });

    });
</script>