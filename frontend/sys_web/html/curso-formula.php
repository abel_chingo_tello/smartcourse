<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">

<?php if(!$ismodal){?>
  <div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Evacuation Formula"); ?>'
  });
</script>
<?php } 
$detcurso=json_decode($this->curso["txtjson"],true);
?>

<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2 class="titulo-p"><?php echo $this->curso["nombre"];?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content x_content_2">
        <div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>">
          <div class="col-md-12 table-responsive">
            <table class="table table-striped table-hover porcentajeexamenes">
              <thead class="headings">
                <tr>
                  <th>#</th>
                  <th><?php echo ucfirst(JrTexto::_('Exams')); ?></th>
                  <th><span id="totalporcentaje"></span><?php echo JrTexto::_('%')?></th>
                </tr>
              </thead>
              <tbody>
                <?php $ie=0;
                $ne=count($this->examenes);
                if(!empty($this->examenes))
                foreach($this->examenes as $ex){ $ie++;           
                  $porc=0;
                  if($ie==1)$tipo='E';
                  if($ne==$ie){
                    $tipo='F';
                    $porc=100;
                  }elseif($ie>1){
                    $tipo=$ie;
                  }
                  $img=substr($ex["imagen"],strrpos($ex["imagen"],'smartquiz/'));
                  ?>
                  <tr>
                    <td><?php echo $ie; ?></td>
                    <td><?php echo $ex["nombre"]; ?></td>
                    <td><input type="number" tipo="<?php echo $tipo;?>" idcursodetalle="<?php echo $ex["idcursodetalle"]; ?>" idrecurso="<?php echo $ex["idrecurso"]; ?>" idexamen="<?php echo $ex["idexamen"]; ?>" nombre="<?php echo $ex["nombre"]; ?>" imagen="<?php echo $img; ?>" class="porcentajeexamenesvalor" minval='0' maxval='100' name="" value="<?php echo $porc; ?>"></td>
                  </tr>
                <?php } ?>
                   <tr>
                    <td><?php echo ($ie+1); ?></td><td><?=JrTexto::_("Directed practice")?> (<?=JrTexto::_("Average projects")?>)</td>
                    <td><input type="number" tipo="PD" nombre="promedioTareas"  class="esproyecto porcentajeexamenesvalor" minval='0' maxval='100' name="" value="0"></td>
                  </tr>
                  <tr>
                    <td><?php echo ($ie+2); ?></td><td><?=JrTexto::_("Qualified practice")?> (<?=JrTexto::_("Average tasks")?>)</td>
                    <td><input type="number" tipo="PC" nombre="promedioProyectos" class="estarea porcentajeexamenesvalor" minval='0' maxval='100' name="" value="0"></td>
                  </tr>
              </tbody>
            </table>
          </div> 
          <div class="col-md-12">
            <legend><?php echo JrTexto::_('Score'); ?></legend>
            <div class="row" id="rownumber">
              <div class="col-md-4 form-group">
                <label class="col-xs-12"><?php echo ucfirst(JrTexto::_("Type"))?></label>
                <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                  <select name="calificacionen" id="calificacionen" class="form-control">
                    <option value="N"><?php echo ucfirst(JrTexto::_("Numeric"))?></option>
                    <option value="P"><?php echo ucfirst(JrTexto::_("Percentage"))?></option>
                    <option value="A"><?php echo ucfirst(JrTexto::_("Alfabetic"))?></option>
                  </select>          
                </div>
              </div>
              <div class="col-md-4 form-group pornum">
                <label class="col-xs-12"><?php echo ucfirst(JrTexto::_("Highest score"))?><span style="display:none;">%</span></label>
                <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                  <select name="maxpuntaje" class="form-control maxpuntaje">            
                    <?php for ($i=100; $i >=1 ; $i--) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php }  ?>
                  </select>          
                </div>
              </div>
              <div class="col-md-4 form-group pornum">
                <label class="col-xs-12"><?php echo ucfirst(JrTexto::_("Minimum score to pass"))?><span style="display:none;">%</span></label>
                <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                  <select name="minpuntaje" class="form-control minpuntaje">            
                    <?php for ($i=0; $i <=100 ; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php }  ?>
                  </select>          
                </div>
              </div>
            </div>
            <div class="row" style="display: none;">
              <div class="col-md-12 table-reponsive">
                <table class="table table-striped table-hover tablepuntuacion">
                  <thead class="headings">
                    <tr>
                      <th><?php echo ucfirst(JrTexto::_("Scale name"));?></th>
                      <th><?php echo ucfirst(JrTexto::_("Highest score"));?></th>
                        <th><?php echo ucfirst(JrTexto::_("Minimum score"));?></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><input type="text" class="form-control gris txtNombreEscala" name="txtNombreEscala" placeholder="e.g.: A, good, ..." value=""></td>
                      <td><div class="col-xs-12 select-ctrl-wrapper select-azul ">
                        <select name="maxpuntaje" id="maxpuntaje"  class="form-control maxpuntaje" style="font-size: 15px;">            
                          <?php for ($i=100; $i >=1 ; $i--) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                          <?php }  ?>
                        </select></div>
                      </td>

                      <td><div class="col-xs-12 select-ctrl-wrapper select-azul ">
                        <select name="minpuntaje" id="minpuntaje"  class="form-control minpuntaje" style="font-size: 15px;">            
                          <?php for ($i=0; $i <=100 ; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                          <?php }  ?>
                        </select>    </div>      
                      </td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>  
            
          </div>
          <div class="col-md-12 text-center">
            <hr>
            <button class="savecalificacion_nota btn btn-success"><i class="fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save')); ?></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(e){
    var configuracion_nota=<?php echo !empty($this->curso["configuracion_nota"])?$this->curso["configuracion_nota"]:'false'; ?>;
    $('table.porcentajeexamenes').on('change','input.porcentajeexamenesvalor',function(){
      var input=$('input.porcentajeexamenesvalor');
      var sum=0;
      $.each(input,function(k){
        sum=sum+parseFloat($(this).val());
      })
      if(sum!=100){  $('#totalporcentaje').addClass('Error');       
        input.css({'border':'1px solid red'});
      }else {
        input.css({'border':'1px  solid #4683af'});
        $('#totalporcentaje').removeClass('Error'); 
      }
      $('#totalporcentaje').text(sum);
    })
    $('input.porcentajeexamenesvalor').trigger('change');

    $('#calificacionen').change(function(ev){
      _this=$(this);
      $row=_this.closest('.row');
      val=_this.val();
      if(val=='N'){
        $row.find('.pornum').show();
        $row.siblings('.row').hide();
        $row.find('label > span').hide();
      }else if(val=='P'){
        $row.find('.pornum').show();
        $row.siblings('.row').hide();
        $row.find('label > span').show();
      }else{
        $row.find('.pornum').hide();
        $row.siblings('.row').show();
      }
    })
    var reordenartr=function(tbody,tr){
      $v=tr.find('select#minpuntaje').val();
      haynex=tr.next();
      $trclone=tbody.find('tr').first().clone(true);
      if($v<=2) return tr.nextAll().remove();      
      if(haynex.length==0){
        $trclone.find('input.txtNombreEscala').val('');
          $trclone.find('select#maxpuntaje').find('option[value="'+($v-1)+'"]').prevAll().remove();
          $trclone.find('select#maxpuntaje').attr('disabled',true).css({'background':'#ccc'});
          $trclone.find('select#minpuntaje').find('option[value="'+($v-2)+'"]').nextAll().remove();
          $body.append($trclone);
        }else{
          $vmax1=haynex.find('select#maxpuntaje').val();
          //if($v!=$vmax1){

          $trclone.find('select#maxpuntaje').find('option[value="'+($v-1)+'"]').prevAll().remove();
          haynex.find('select#maxpuntaje').children().remove();
          haynex.find('select#maxpuntaje').append($trclone.find('select#maxpuntaje').children());
          haynex.find('select#maxpuntaje').children().first().attr('selected',true);
          $vmin1=haynex.find('select#minpuntaje option').last().text();          
          for($i=$vmin1;$i<$vmax1;$i++){
            haynex.find('select#minpuntaje').append('<option value="'+($i+1)+'">'+($i+1)+'</option>');
          }
          haynex.find('select#maxpuntaje').trigger('change');
          haynex.find('select#minpuntaje').trigger('change');       
        }
    }


    $('.table').on('change','.minpuntaje',function(){
      $v=$(this).val();
      $tr=$(this).closest('tr');
      $body=$(this).closest('tbody')
      if($v==0) $tr.nextAll().remove();
      else reordenartr($body,$tr);
    }).on('change','.maxpuntaje',function(){
      _this=$(this);
      $v=$(this).val();
      $smin=_this.closest('tr').find('.minpuntaje');
      $vmin=$smin.val();
      $smin.children().remove();
      for (var i = 0; i <$v; i++){
        $smin.append('<option value="'+i+'">'+i+'</option>');
      }
      $smin.children('option[value="'+$vmin+'"]').attr('selected',true);  
      $smin.trigger('change');    
    })
    $('.pornum').on('change','.maxpuntaje',function(){
      _this=$(this);
      $v=$(this).val();
      $smin=_this.closest('#rownumber').find('.minpuntaje');
      $vmin=$smin.val();
      $smin.children().remove();
      for (var i = 0; i <$v; i++){
        $smin.append('<option value="'+i+'">'+i+'</option>');
      }
      $smin.children('option[value="'+$vmin+'"]').attr('selected',true);      
    })

    $('.savecalificacion_nota').on('click',function(ev){
      var totalporcentaje=$('#totalporcentaje').text();
      var hayerror=false;
      if($('#totalporcentaje').hasClass('Error')){
        __notificar({title:'<?php echo JrTexto::_('Attention');?>',html:'<?php echo JrTexto::_('Error in data')?>',type:'error'});
        return;
      }
      var input=$('input.porcentajeexamenesvalor');
      var json={examenes:{},tipocalificacion:'P',maxcal:100,mincal:51};
      $.each(input,function(i,v){        
        var k=$(this);
        if(k.hasClass('esproyecto')){
          json.proyecto={puntaje:k.val(),tipo:k.attr('tipo'),descripcion:k.attr('nombre')}
        }else if(k.hasClass('estarea')){
          json.tarea={puntaje:k.val(),tipo:k.attr('tipo'),descripcion:k.attr('nombre')}
        }else{
          json.examenes[i]={
            idexamen:k.attr('idexamen'),
            idcursodetalle:k.attr('idcursodetalle'),
            tipo:k.attr('tipo'),
            idrecurso:k.attr('idrecurso'),
            nombre:k.attr('nombre'),
            imagen:k.attr('imagen'),
            porcentaje:k.val()
          }
        }
      })
      var tipocal=$('#calificacionen').val();
      json.tipocalificacion=tipocal;
      if(tipocal=='P'||tipocal=='N'){
        json.maxcal=$('#rownumber').find('select.maxpuntaje').val()||20;
        json.mincal=$('#rownumber').find('select.minpuntaje').val()||14;
      }else{
        json.puntuacion=[];
        $('table.tablepuntuacion tbody').find('tr').each(function(i,v){
          var tr=$(this);
          var escale=tr.find('.txtNombreEscala').val();
          if(escale==''){
            tr.find('.txtNombreEscala').css({border:'1px solid red'});
             __notificar({title:'<?php echo JrTexto::_('Attention');?>',html:'<?php echo JrTexto::_('Error in data')?>',type:'error'});
             hayerror=true;
            return ;
          }else tr.find('.txtNombreEscala').css({border:'1px solid #4683af'});
          json.puntuacion.push({nombre:escale,maxcal:tr.find('select.maxpuntaje').val(),mincal:tr.find('select.minpuntaje').val()});  
        })
      }
      if(hayerror==true) return;
      var data=new FormData()
      data.append('idcurso','<?php echo $this->curso["idcurso"];?>');
      data.append('campo','configuracion_nota');
      data.append('valor',JSON.stringify(json));
      __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/proyecto_cursos/setCampoxCurso',
            showmsjok:true,
            callback:function(rs){
              if(rs.code==200){
                window.history.back();
              }
            }
        });
    })

    if(configuracion_nota!=false){
      var cargarexamenes=configuracion_nota.examenes;
      $.each(cargarexamenes,function(i,v){
        var tipo=v.tipo||'';
        if(tipo=='') ele=$('input.porcentajeexamenesvalor[idexamen="'+v.idexamen+'"]');
        else ele=$('input.porcentajeexamenesvalor[idexamen="'+v.idexamen+'"][tipo="'+tipo+'"]');
        ele.val(v.porcentaje);
      });
      if(configuracion_nota.tarea!=undefined)
      $('input[nombre="promedioTareas"]').val(parseFloat(configuracion_nota.tarea.puntaje))
      if(configuracion_nota.proyecto!=undefined)
      $('input[nombre="promedioProyectos"]').val(parseFloat(configuracion_nota.proyecto.puntaje))


      $('input.porcentajeexamenesvalor').last().trigger('change');
      var tipocal=configuracion_nota.tipocalificacion||'P';
      $('#calificacionen').val(tipocal);

      if(tipocal=='P'||tipocal=='N'){
       // console.log($('#rownumber').find('select#maxpuntaje').children('option[value="'+(configuracion_nota.maxcal||20)+'"]'));
       $('#rownumber').find('select.maxpuntaje').val(configuracion_nota.maxcal||20);
       $('#rownumber').find('select.minpuntaje').val(configuracion_nota.mincal||14);
      }else{
        $('#calificacionen').trigger('change');
        var punt=configuracion_nota.puntuacion;
        var tr=$('table.tablepuntuacion tbody').find('tr')
        $.each(punt,function(i,v){
          tr.find('.txtNombreEscala').val(v.nombre);
          tr.find('select.maxpuntaje').val(v.maxcal).trigger('change');
          tr.find('select.minpuntaje').val(v.mincal);
          if(v.mincal>0){
            tr.find('select.minpuntaje').trigger('change');
            tr=tr.next();
          }
        })
      }
    }
  })
</script>