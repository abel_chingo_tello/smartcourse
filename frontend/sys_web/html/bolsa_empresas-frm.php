<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/bolsa_empresas">&nbsp;<?php echo JrTexto::_('Bolsa_empresas'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="bolsa_empresas" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idempresa" id="idempresa" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Rason social');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtRason_social" name="rason_social" required="required" class="form-control" value="<?php echo @$frm["rason_social"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Ruc');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtRuc" name="ruc" required="required" class="form-control" value="<?php echo @$frm["ruc"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Logo');?> <span class="required"> * </span></label>
              <div class="">
               
                            <img src="<?php echo URL_BASE.(!empty($frm["logo"])?$frm["logo"]:$_imgdefecto);?>" alt="imagenfile" file="logo" class="subirfile img-thumbnail" style="max-height:200px; max-width:200px;">
            <input type="hidden" name="logo" value="<?php echo !empty($frm["logo"])?URL_BASE.$frm["logo"]:"";?>">

                <!--div class="row">
                  <input type="hidden" name="" id="txt">
                  <input type="hidden" name="_old" value="';?>">
                  <div class="col-md-12">
                    <div class="img-container">
                      <img src="';?>" alt="img" data-ancho="100"  data-alto="100">
                    </div>
                  </div>
                </div-->
                                 
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Direccion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtDireccion" name="direccion" required="required" class="form-control" value="<?php echo @$frm["direccion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Telefono');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTelefono" name="telefono" required="required" class="form-control" value="<?php echo @$frm["telefono"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Representante');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtRepresentante" name="representante" required="required" class="form-control" value="<?php echo @$frm["representante"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Usuario');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtUsuario" name="usuario" required="required" class="form-control" value="<?php echo @$frm["usuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Clave');?> <span class="required"> * </span></label>
              <div class="">
               <input type="password" id="txtClave" name="clave" class="form-control" required /> 
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Correo');?> <span class="required"> * </span></label>
              <div class="">
                <input type="email" id="txtCorreo" name="correo" required="required" class="form-control" value="<?php echo @$frm["correo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
              <div class="">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle";?>" 
                data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="estado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tipo matricula');?> <span class="required"> * </span></label>
              <div class="">
               <select id="txttipo_matricula" name="tipo_matricula" class="form-control" required>
              <option value=""><?php JrTexto::_("Seleccione");?></option>
                              <option value="1"><?php echo JrTexto::_('Activo');?></option>
              <option value="0"><?php echo JrTexto::_('Inactivo');?></option>
                                   
              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idpersona');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdpersona" name="idpersona" required="required" class="form-control" value="<?php echo @$frm["idpersona"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveBolsa_empresas" type="submit" tb="bolsa_empresas" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('bolsa_empresas'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/bolsa_empresas/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/bolsa_empresas');
                }
              }
          });
       }
  })
})
</script>

