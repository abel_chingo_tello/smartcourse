<?php
$JsonVariable = null;
?>
<style type="text/css">
    .cajaselect {  
        overflow: hidden;
        width: 230px;
        position:relative;
        font-size: 1.8em;
    }
    select:focus{ outline: none;}
    .cajaselect::after{ font-family: FontAwesome; content: "\f0dd"; display: inline-block; text-align: center; width: 30px; height: 100%; background-color: #4683af; position: absolute; top: 0; right: 0px; pointer-events: none; color: antiquewhite; bottom: 0px; }
    .select-ctrl-wrapper:after{ right:0!important; }
    .space-20{ padding:20px; }
    .space-10{ padding:10px; }
</style>
<div class="space-10"></div>
<!--START PANEL-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <h5><?php echo JrTexto::_('report') ?> - <?php echo JrTexto::_('final exams') ?></h5>
    </div>
    <div class="panel-body row">
        <!--START SELECT COURSES-->
        <div class="col-lg-12 col-md-12">
            <div style="display:block;width:100%; margin:10px 0; border-bottom: 1px solid gray; padding-bottom: 4px; text-align:center;">
                <p style="font-size:large; display:inline-block; margin:0; padding-right:15px;"><?php echo JrTexto::_('courses') ?></p>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="select_viewGeneral" id="select_viewGeneral" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
                        <?php 
                            if(empty($this->cursos)){
                                echo '<option value="0">No hay cursos matriculados</option>';
                            }else{
                                foreach($this->cursos as $key => $value){
                                    echo "<option value='{$value['idcurso']}'>{$value['nombre']}</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <!--END SELECT COURSES-->
        <!--START SELECT COURSES-->
        <div class="col-lg-12 col-md-12">
            <div id="content-result-nota"></div>
            <?php 
            if(!is_null($this->examen)){
                $display = 'initial';
                $display2 = 'none';
            }else{
                $display = 'none';
                $display2 = 'initial';
            }
            ?>
            <div id="displayInfo-container" style="display:<?php echo $display ?>;">
                <div id="resultInfo" style="text-align:center;"><?php echo $this->examen['notatexto'] ?></div>
                <div class="col-sm-4" id="result-Fecha">
                    <h3 style="border: 2px solid gray; border-radius: 2em; text-align:center;"><?php echo JrTexto::_('date') ?></h3>
                    <p style="font-weight:bold; text-align:center; font-size:large;border: 2px solid black; border-top: none; border-radius: 0.2em;"><?php echo $this->examen['regfecha'] ?></p>
                </div>
                <div class="col-sm-4" id="result-Intentos">
                    <h3 style="border: 2px solid gray; border-radius: 2em; text-align:center;"><?php echo JrTexto::_('number of attempts') ?></h3>
                    <p style="font-weight:bold; text-align:center; font-size:large;border: 2px solid black; border-top: none; border-radius: 0.2em;"><?php echo $this->examen['intento'] ?></p>
                </div>
                <div class="col-sm-4" id="result-Tiempo">
                    <h3 style="border: 2px solid gray; border-radius: 2em; text-align:center;"><?php echo JrTexto::_('time') ?></h3>
                    <p style="font-weight:bold; text-align:center; font-size:large;border: 2px solid black; border-top: none; border-radius: 0.2em;"><?php echo $this->examen['tiempo_realizado'] ?></p>
                </div>
            </div>
            <div id="displayInfo-alert" style="display:<?php echo $display2 ?>;">
                <div class="alert alert-danger" style="width: 90%;"><h1><?php echo JrTexto::_('you do not have a test performed') ?></h1></div>
            </div>
        </div>
        <!--END SELECT -->
    </div>
</div>
<!--END PANEL-->
<div id="testpreview" class="panel panel-primary">
    <div class="panel-heading"><h5><?php echo JrTexto::_('final exam') ?></h5></div>
    <div class="panel-body row">
        <div class="col-lg-12 col-md-12">
            <?php if(!is_null($this->examen)): ?>
                <div id="previewFrame" style="display:block; max-height:600px; overflow-y:auto;">
                    <?php
                    $jsonParse = json_decode($this->examen['preguntas'],true);
                    foreach($jsonParse as $value){
                        echo $value;
                    }
                    ?>
                </div>
                <div class="" id="previewFrame2" style="display:none; width:100%; max-height: 600px; overflow: auto; border: 1px solid gray;">
                    <div class="alert alert-danger"><?php echo JrTexto::_('there is no final exam') ?></div>
                </div>
            <?php else:?>
                <div id="previewFrame" style="display:none; max-height:600px; overflow-y:auto;"></div>
                <div class="" id="previewFrame2" style="display:block; width:100%; max-height: 600px; overflow: auto; border: 1px solid gray;">
                    <div class="alert alert-danger"><?php echo JrTexto::_('there is no final exam') ?></div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
function showNotInfo(){
    $('#displayInfo-container').hide();
    $('#displayInfo-alert').show();
    $('#previewFrame2').show();
    $('#previewFrame').hide();
}
function showYesInfo(){
    $('#displayInfo-container').show();
    $('#displayInfo-alert').hide();
}

function DisplayPreviewTest(obj){
    $('#previewFrame2').hide();
    $('#previewFrame').show();
    $('#previewFrame').html(' ');
    for(var i in obj){
        $('#previewFrame').append(obj[i]);
    }
}

function DisplayInfo(){
    var idcurso = $('#select_viewGeneral').val();
    if(idcurso != 0){
        $.ajax({
            url: _sysUrlBase_ + "/reportealumno/obtenerexamenfinal_json",
            type: 'POST',
            dataType:'json',
            data: {'idcurso':idcurso}
        }).done(function(response){
            if(response.code == 'ok'){
                if(response.data != ''){
                    showYesInfo();
                    $('#resultInfo').html(response.data.notatexto);
                    $('#result-Fecha').find('p').html(response.data.regfecha);
                    $('#result-Intentos').find('p').html(response.data.intento);
                    $('#result-Tiempo').find('p').html(response.data.tiempo_realizado);
                    DisplayPreviewTest(JSON.parse(response.data.preguntas));
                }else{
                    showNotInfo();
                }
            }else{
                showNotInfo();
                alert(response.message);
            }
        }).fail(function(xhr){
            alert(xhr.responseText);
        });
    }
}
$('document').ready(function(){
    $('#select_viewGeneral').change(function(){
        DisplayInfo();
    });
});
</script>
