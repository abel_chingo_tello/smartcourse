<style type="text/css">
    .cajaselect {  
        overflow: hidden;
        width: 230px;
        position:relative;
        font-size: 1.8em;
    }
    select:focus{ outline: none;}
    .cajaselect::after{ font-family: FontAwesome; content: "\f0dd"; display: inline-block; text-align: center; width: 30px; height: 100%; background-color: #4683af; position: absolute; top: 0; right: 0px; pointer-events: none; color: antiquewhite; bottom: 0px; }
    .select-ctrl-wrapper:after{ right:0!important; }
    .space-20{ padding:20px; }
    .space-10{ padding:10px; }
</style>
<div style="padding:20px;"></div>
<!--START PANEL-->
<div class="panel panel-primary">
    <div class="panel-heading"><h5><?php echo JrTexto::_('compare between start and end exams') ?></h5></div>
    <!--START PANEL BODY-->
    <div class="panel-body row">
        <div class="col-lg-12 col-md-12">
            <div class="text-center">
                <p style="font-size:large; display:inline-block; margin:0; padding-right:15px;"><?php echo JrTexto::_('courses') ?></p>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="select_viewGeneral" id="select_viewGeneral" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
                        <?php 
                            if(empty($this->cursos)){
                                echo '<option value="0">No hay cursos matriculados</option>';
                            }else{
                                echo '<option value="0">Seleccionar</option>';
                                foreach($this->cursos as $key => $value){
                                    echo "<option value='{$value['idcurso']}'>{$value['nombre']}</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div style="padding:20px;"></div>
            <div id="noteTable-container">
                <table id="noteTable" class="table" style="text-align:center;">
                    <thead >
                        <th style="text-align:center;"><?php echo JrTexto::_('courses') ?></th>
                        <th style="text-align:center;"><?php echo JrTexto::_('initial exam grade') ?></th>
                        <th style="text-align:center;"><?php echo JrTexto::_('final exam grade') ?></th>
                        <th style="text-align:center;"><?php echo JrTexto::_('average') ?></th>
                    </thead>
                    <tbody>
                        <tr style="font-size:x-large;">
                            <td>N/A</td>
                            <td>N/A</td>
                            <td>N/A</td>
                            <td>N/A</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="chart-container" id="comparativo-container" style="position: relative; margin:0 auto; height:100%; width:100%">
              <canvas id="comparativo" style="min-height:250px; margin: 0 auto"></canvas>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="chart-container" id="comparativo2-container" style="position: relative; margin:0 auto; height:100%; width:100%">
              <canvas id="comparativo2" style="margin: 0 auto"></canvas>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="chart-container" id="comparativo3-container" style="position: relative; margin:0 auto; height:100%; width:100%">
              <canvas id="comparativo3" style="margin: 0 auto"></canvas>
            </div>
        </div>
    </div>
    <!--END PANEL BODY-->
</div>
<!--END PANEL-->

<script type="text/javascript">

function drawChart2(obj,dat,texto  = 'Comparativo entre ex menes inicial y final' ){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: false,
      title: {
        display: true,
        text: texto
      },
      tooltips: {
        custom: function(tooltipModel) {
          if(tooltipModel.body){
            // console.log(tooltipModel.body[0].lines[0]);
            var strline = tooltipModel.body[0].lines[0];
            var search = strline.search('hide');
            if(search != -1){
              tooltipModel.width = 125;
              tooltipModel.body[0].lines[0] = strline.replace('hide','Diferencia');
            }
          }
        }
      },
      scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        },
      legend: {
        labels: {
          filter: function(item, chart) {
            // Logic to remove a particular legend item goes here
            return !item.text.includes('hide');
          }
        },
        onHover: function(event, legendItem) {
          document.getElementById(obj).style.cursor = 'pointer';
        },
        onClick: function(e,legendItem){
          var index = legendItem.datasetIndex;
          var ci = this.chart;
          // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
          var meta = ci.getDatasetMeta(index);
          // alert(index);
          if(index == 2){
            var meta2 = ci.getDatasetMeta(3);
            meta2.hidden = meta2.hidden === null? !ci.data.datasets[3].hidden : null;
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;

          }else{
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
          }

          ci.update();
        }
      }//end legend
    }
  });
}

function drawcomparativa(val = 0){
    var datosRadar = new Array(1);
    var datosRadar2 = new Array(1);
    var datosRadar3 = new Array(1);
    var datosRadar4 = new Array(1);
    
    datosRadar.fill(0);
    datosRadar2.fill(0);
    datosRadar3.fill(0);
    datosRadar4.fill(0);
    
    if(val != 0){
        $.ajax({
            url: _sysUrlBase_ + "/reportealumno/obtenerexameninicial_json",
            type: 'POST',
            dataType:'json',
            data: {'idcurso':val},
            async: false
        }).done(function(response){
            if(response.code == 'ok'){
                var valoor = 0;
                if(response.data != ''){
                    valoor = (response.data.calificacion_en != 'N') ? Math.floor(response.data.nota * 0.20) : Math.floor(response.data.nota);
                }
                datosRadar[0]= valoor;
                $('#noteTable').find('tbody tr').eq(0).find('td').eq(1).text(valoor.toString()  + " pts / 20 pts" );
            }else{
                alert(response.message);
            }
        }).fail(function(xhr){
            alert(xhr.responseText);
        });
        $.ajax({
            url: _sysUrlBase_ + "/reportealumno/obtenerexamenfinal_json",
            type: 'POST',
            dataType:'json',
            data: {'idcurso':val},
            async: false
        }).done(function(response){
            if(response.code == 'ok'){
                var valoor = 0;
                if(response.data != ''){
                    valoor = (response.data.calificacion_en != 'N') ? Math.floor(response.data.nota * 0.20) : Math.floor(response.data.nota);
                }
                datosRadar2[0]= valoor;
                $('#noteTable').find('tbody tr').eq(0).find('td').eq(2).text(valoor.toString()  + " pts / 20 pts");

            }else{
                alert(response.message);
            }
        }).fail(function(xhr){
            alert(xhr.responseText);
        });

        $('#noteTable').find('tbody tr').eq(0).find('td').eq(3).text((datosRadar[0] + datosRadar2[0]) / 2 + " pts / 20 pts");


        for(var i = 0; i < datosRadar.length; i++){
            if(datosRadar[i] > datosRadar2[i]){
            datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
            }else if(datosRadar[i] < datosRadar2[i]){
            datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
            }
        }
    }

    var barChartData = {
    labels: ["Entrada"],
    datasets: [{
        label: 'Beginning Test',
        backgroundColor: '#36a2eb',
        stack:'Stack 0',
        data: [
          datosRadar[0]
        ]
    }, {
        label: 'Final Test',
        backgroundColor: '#ff6384',
        stack:'Stack 1',
        data: [
          datosRadar2[0]
        ]
    },{
        label: 'Difference',
        backgroundColor: '#d4b02f',
        stack: 'Stack 0',
        data: [
          Math.abs(datosRadar3[0])
        ]
    },{
        label: 'hide',
        backgroundColor: '#d4b02f',
        stack: 'Stack 1',
        
        data: [
            Math.abs(datosRadar4[0])
        ]
    }]

  };
  $('#comparativo-container').html("").html('<canvas id="comparativo" style="min-height:250px; margin: 0 auto"></canvas>'); //No es la mejor opcion investigar update de la
  drawChart2("comparativo",barChartData);
}

function captureName(obj){
    var name = '';
    obj.find('option').each(function(e,i){
        if($(this).attr('value') == obj.val()){
            name = $(this).text();
            return;
        }
    });
    return name;
}

$(document).ready(function(){
    drawcomparativa();
    $('#select_viewGeneral').change(function(){
        if($('#select_viewGeneral').val() != 0){
            $('#noteTable').find('tbody tr').eq(0).find('td').eq(0).text(captureName($('#select_viewGeneral')));
        }
        drawcomparativa($('#select_viewGeneral').val());
    });

    
});

</script>