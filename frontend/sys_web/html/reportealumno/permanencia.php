<style>
.foto{ cursor:pointer; }
.cursoContent{
    text-align:center;
}
.progress{
    margin-top:5px;
}
#child{
    box-shadow: 0 0 0 rgba(204,44,44, 0.5);
    animation: pulse 2s infinite;
    border-radius:50%;
}
.imgr{
    -webkit-animation: 3s rotate linear infinite;
    animation: 3s rotate linear infinite;
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
.selected{
    background-color:red!important;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

@-webkit-keyframes pulse {
  0% {
    -webkit-box-shadow: 0 0 0 0 rgba(204,44,44, 0.5);
  }
  70% {
      -webkit-box-shadow: 0 0 0 10px rgba(204,44,44, 0);
  }
  100% {
      -webkit-box-shadow: 0 0 0 0 rgba(204,44,44, 0);
  }
}
@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 rgba(204,44,44, 0.5);
    box-shadow: 0 0 0 0 rgba(204,44,44, 0.5);
  }
  70% {
      -moz-box-shadow: 0 0 0 10px rgba(204,44,44, 0);
      box-shadow: 0 0 0 10px rgba(204,44,44, 0);
  }
  100% {
      -moz-box-shadow: 0 0 0 0 rgba(204,44,44, 0);
      box-shadow: 0 0 0 0 rgba(204,44,44, 0);
  }
}
</style>
<div style="padding:15px;"></div>
<!--START PANEL-->
<div class="panel panel-primary">
 <div class="panel-heading"><h5>Permanencia</h5></div>
 <div class="panel-body row">
    <!--START BARRA PROGRESO-->
    <div class="col-lg-12 col-md-12">
        <!--start design-->
        <div class='progresbarra' style="width:100%;height: 97px;border:1px solid gainsboro;border-radius: 1em; margin:2px auto;">
            <p style=" display: inline-block; font-size: 11px; position: relative; left: 2%; font-weight: bold;">Iniciar</p>
            <p class='progrsfinish' style=" display: inline-block; font-size: 11px; position: absolute; right: 2%; font-weight: bold;">Completado</p>

            <div style=" font-size: 2em; position: relative; width: 100%; height: 30px;" class="progresimg">
                <i class="fa fa-flag" aria-hidden="true" style="position: absolute;left: 3%;opacity: 0.8;"></i>
                <i class="fa fa-flag-checkered" aria-hidden="true" style="position: absolute;left: 96%;opacity: 0.8;"></i>
                <div id="barraprogresototal" style=" height: 25px; width: 59%; margin-left: 29px; max-width: 93%; background-color: #daf6da; border-radius: 1em;">
                    <i class="fa fa-child hvr-pulse" aria-hidden="true" style="position: absolute;left: 62%;color: #fd454a;" id="child"></i>
                </div>
            </div>
            <div class="base" style="position:relative;width: 100%;top: 0px;height: 35px;">
                <p style=" display: inline-block; position: absolute; bottom: 0; left: 3%; margin: 0; font-weight: bold;">Inicio</p>
                <div class="base-barra" style="width: 87%; position: absolute; height: 100%; margin: 0 6.3%;">
                    <p style=" display: inline-block; position: absolute; bottom: 0; left: 20%; margin: 0; font-weight: bold;">E1</p>
                    <p style=" display: inline-block; position: absolute; bottom: 0; left: 50%; margin: 0; font-weight: bold;">S1</p>
                    <p style=" display: inline-block; position: absolute; bottom: 0; left: 60%; margin: 0; font-weight: bold;">S2</p>
                    <p style=" display: inline-block; position: absolute; bottom: 0; left: 75%; margin: 0; font-weight: bold;">E2</p>
                    <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 19%;position: absolute;z-index: 1;"></div>
                    <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 49%;position: absolute;z-index: 1;"></div>
                    <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 59%;position: absolute;z-index: 1;"></div>
                    <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 74%;position: absolute;z-index: 1;"></div>
                    
                </div>
                <p style=" display: inline-block; position: absolute; bottom: 0; left: 96%; margin: 0; font-weight: bold;">Final</p>
                <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 2%;/* position: relative; */position: absolute;z-index: 1;"></div>
                <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 95%;position: absolute;z-index: 1;"></div>
                <div class="line" style="position: absolute;width: 100%;height:3px;background:lightgrey;top:5px;z-index: 0;"></div>
            </div>
        </div>
        <!--end design-->
        <div style="padding:20px;"></div>
    </div>
    <!--END BARRA PROGRESO-->
    <!--START SLIDER -->
    <div class="col-lg-12 col-md-12">
        <div class="card col-sm-12" id="cursos-container">
            <div class="card-body">
                <!--<div class="nivelCircle hvr-radial-out courseactive">-->
                <!--START CONTENT-->
                <?php if(!empty($this->cursos)): ?>
                    <?php foreach($this->cursos as $key => $value): ?>
                        <?php if($key == 0): ?>
                            <div class="cursoContent col-sm-3 courseactive" data-idcurso="<?php echo $value['idcurso']?>">                    
                        <?php else:?>
                            <div class="cursoContent col-sm-3" data-idcurso="<?php echo $value['idcurso']?>">
                        <?php endif; ?>
                            <div class="nivelCircle hvr-reveal hvr-glow">
                                <img src="<?php echo $this->documento->getUrlBase().$value['imagen']; ?>" style="position: absolute; left: 0; height: 100%;">
                                <div class="foto" style=" width: 167px; height: 128px; position: relative;"></div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="32.46" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div>
                            </div>
                            <?php if($key == 0): ?>
                                <div class="select selected" style=" height: 7px; background-color: #e1e1e1; border: 1px solid #a4a4a4; border-bottom-color: gray;"></div>
                            <?php else:?>
                                <div class="select" style=" height: 7px; background-color: #e1e1e1; border: 1px solid #a4a4a4; border-bottom-color: gray;"></div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                <!--END CONTENT-->
            </div>
        </div>
    </div>
    <!--END SLIDER-->
 </div>

</div>
<!--END PANEL-->
<script type="text/javascript">
function drawBarra(){
    var idcurso = $('.courseactive').data('idcurso');
    $.ajax({
        url: _sysUrlBase_ + "/reportealumno/permanenciabarrajson",
        type: 'POST',
        dataType: 'json',
        data: {'idcurso': idcurso}
    }).done(function(resp){
        console.log(resp);
        if(resp.code == 'ok'){
            if(resp.data != ''){
                var puntos = resp.data.puntos;
                var pforma = '';
                var ptext = '';
                var sumcoordenadas = (puntos.length == 1) ? Math.floor(100/(puntos.length + 1)) : Math.floor(87/(puntos.length));
                var _sumcoordenadas = sumcoordenadas;
                for(var i = 0; i < puntos.length; i++){
                    ptext = ptext.concat('<p style=" display: inline-block; position: absolute; bottom: 0; left: '+sumcoordenadas+'%; margin: 0; font-weight: bold;">'+puntos[i]+'</p>');
                    pforma = pforma.concat('<div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: '+(sumcoordenadas - 1)+'%;position: absolute;z-index: 1;"></div>');
                    sumcoordenadas += _sumcoordenadas;
                }
                $('#barraprogresototal').css('width', resp.data.progreso +'%');
                /**Mejorar la ubicacion del progreso */
                $('#child').css('left', (resp.data.progreso + 2) +'%');
                $('.progresbarra .base .base-barra').html(ptext);
                $('.progresbarra .base .base-barra').append(pforma);
            }
        }else{
            alert(resp.message);
        }
    }).fail(function(xhr,textStatus,errorThrow){
        alert(xhr.responseText);
    });
}
$(document).ready(function(){
    drawBarra();
    $('#cursos-container .card-body').slick({
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 2
    });
    $('.cursoContent').on('click',function(){
        $('.cursoContent').each(function(e,i){
            $(this).removeClass('courseactive');
            $(this).find('.select').removeClass('selected');
        });
        $(this).addClass('courseactive');
        $(this).find('.select').addClass('selected');
        drawBarra();        
    });
});
</script>