<?php defined('RUTA_BASE') or die(); 
$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$idgui = uniqid();


$RUTA_BASE = $this->documento->getUrlBase();
//if(empty($this->datos)) $frm=$this->datos;
$frm=$this->datos;

//echo $this->pk1."eve";

//echo $this->frmaccion."as";
?>

<style type="text/css">
  
.border{
  border: solid 0px #f00;
  text-align: justify;

}
</style>
<div class="col-md-12 container">
<div class="row border" id="contenido" style="padding-top: 1ex; ">

<form class="form-horizontal" id="frm_unidad" name="frm_unidad" >
<input type="text" name="accion" id="accion" value="<?=$this->frmaccion?>">
<input type="text" name="pkIduni" id="pkIduni" value="<?=$frm['idnivel']?>">
<input type="text" name="pkIdpadre" id="pkIdpadre" value="<?=$this->pk1?>">
<input type="text" name="rutas" id="rutas" value="<?=$this->rutas?>">
<input type="text" name="nivel" id="nivel" value="<?=$this->nivel?>">

<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    	<?php echo JrTexto::_("Add")?>
    </div>

<div class = 'panel-body' >

		<div class = 'col-xs-12 col-sm-12 col-md-10 col-lg-10 border'  >
		<div class="form-group">


		<div class="col-md-12">
		<label for="titulo"><?php echo JrTexto::_("Title")?></label>
			<input type="text" name="txttit" id="txttit" class="form-control gris" placeholder="<?php echo JrTexto::_("Add")?> <?php echo JrTexto::_("Title")?>" required value="<?php echo @$frm['nombre']; ?>" style="height: 35px">
		</div>


		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			
			<label for="titulo"><?php echo JrTexto::_("Order")?></label>
			<div class="cajaselect " >
				<select name="txtorden" id="txtorden" style="width: 100%; height: 35px" required>
					<option value="" ><?php echo JrTexto::_("Select")?></option>
					<?
					for ($i=1; $i<=20; $i++){
					?>
					<option value="<?=$i?>" <?php echo $frm["orden"]==$i?'Selected':'';?>><?=$i?></option>
					<?
					}//for
					?>
									
				</select>
			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

			<label for="titulo"><?php echo JrTexto::_("Status")?></label>
			<div class="cajaselect " >
				<select name="txtest" class="" id="txtest" style="width: 100%; height: 35px" required >						
					<option value="1" <?php echo $frm["estado"]=='1'?'Selected':'';?>>Activo</option>
					<option value="0" <?php echo $frm["estado"]=='2'?'Selected':'';?>>Inactico</option>
									
				</select>
			</div>

		</div>

		
		


		</div>
		</div>

		<div class = 'col-xs-12 col-sm-12 col-md-2 col-lg-2 border'  >
		<div class="form-group">


			<a href="#" class="thumbnail btnportada istooltip" data-tipo="image" data-url=".img-portada" title="<?php echo ucfirst(JrTexto::_('Select an image')); ?>">
                <?php
                 $imgSrc=($this->frmaccion=='Nuevo')?$this->documento->getUrlStatic().'/media/web/nofoto.jpg':str_replace('__xRUTABASEx__', $RUTA_BASE, @$frm['imagen']);
                ?>
                <img src="<?php echo $imgSrc; ?>" alt="cover" class="img-responsive img-portada img-thumbnails istooltip">
                <input type="hidden" id="txtFoto" name="txtFoto" value="<?php echo $imgSrc; ?>">
            </a>


		</div>
		</div>

		

		<div class = 'col-xs-12 col-sm-12 col-md-12 col-lg-12 border' style="text-align: center;"  >
		<div class="form-group">

		<a id="btn-saveHoja"  class="btn btn-success" onclick="guardarUnid()" ><i class=" fa fa-save"></i> <?php echo JrTexto::_("Save")?></a>
		  <a type="button" class="btn btn-warning btn-close" data-dismiss="modal"   ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_("Cancel")?></a>

		</div>
		</div>


</div><!--panel body-->
</div>
           
</form>

</div>
</div>

<script type="text/javascript">

$('.istooltip').tooltip();

$(document).ready(function() {

	$('.btnportada').click(function(e){ 
        var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
        selectedfile(e,this,txt);
    });

    $('.btnportada img').load(function() {
        var src=$(this).attr('src');
        $('#txtFoto').val(src);
    });

});
	//var guardarComp = function() {
	function guardarUnid() {
		//console.log("as");
        var val_count=0; 
        $('#frm_unidad *[required]').each(function(i, elem) {
            var value = $(elem).val();
            if(value=='' || value.trim()==''){
                $(elem).css("border","solid 1px #f00");
                setTimeout (function(){
	            	$(elem).css("border","");	            
	        	}, 3000);
	        	val_count++;
            }
        });

        if (val_count>0){
        	return false;
        }
        
        //alert(_sysUrlBase_+"/frontend/componentes/guardar");

        $.ajax({
            url: _sysUrlBase_+'/componentes/xSaveNivel2',
            type: 'POST',
            dataType: 'json',
            data: $('#frm_unidad').serialize(),
        }).done(function(resp) {
        	
        	console.log("asas");
            if(resp.code=='ok'){
                var id = resp.data;
                //alert(id);

                mostrar_notificacion('<?php echo JrTexto::_('Done') ?>', '<?php echo JrTexto::_('Data updated') ?>','success');

                var nivel =parseInt(resp.nivel) - parseInt(1);                 
                var rutas = $("#rutas").val();
                var cant_part = rutas.split(",");
                var idcod= cant_part[0];

                alert(idcod);
                setTimeout (function(){

	                //if($('#frm1 #accion').val()=="Nuevo"){
	                    return redir(_sysUrlBase_+'/componentes/unidades/?id1=<?=$this->pk1?>&id='+idcod+'&rutas='+rutas+'&nivel='+nivel);
	                //}
		            
		        }, 1000);

                
                
                
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error');
            }
        });
        
    }




</script>