<style type="text/css">
    .cajaselect {  
        overflow: hidden;
        width: 230px;
        position:relative;
        font-size: 1.8em;
    }
    select:focus{ outline: none;}
    .cajaselect::after{ font-family: FontAwesome; content: "\f0dd"; display: inline-block; text-align: center; width: 30px; height: 100%; background-color: #4683af; position: absolute; top: 0; right: 0px; pointer-events: none; color: antiquewhite; bottom: 0px; }
    .select-ctrl-wrapper:after{ right:0!important; }
    .space-20{ padding:20px; }
    .space-10{ padding:10px; }
</style>
<div class="space-10"></div>
<!--START PANEL-->
<div class="panel panel-primary">
    <div class="panel-heading"><h5><?php echo JrTexto::_('self-assessment report') ?></h5></div>
    <div class="panel-body row">
        <!--START SELECT COURSES-->
        <div class="col-lg-12 col-md-12">
            <div style="display:block;width:100%; margin:10px 0; border-bottom: 1px solid gray; padding-bottom: 4px; text-align:center;">
                <p style="font-size:large; display:inline-block; margin:0; padding-right:15px;"><?php echo JrTexto::_('scourses') ?></p>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="select_viewGeneral" id="select_viewGeneral" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
                        <?php 
                            if(empty($this->cursos)){
                                echo '<option value="0">No hay cursos matriculados</option>';
                            }else{
                                foreach($this->cursos as $key => $value){
                                    echo "<option value='{$value['idcurso']}'>{$value['nombre']}</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <!--END SELECT COURSES-->
        <!--START RESULTADOS-->
        <div class="col-lg-12 col-md-12">
            <div id="displayInfo-container" style="display:block">
                
            </div>
            <div id="displayInfo-alert" style="display:block">
                <div class="alert alert-danger" style="width: 90%;"><h1><?php echo JrTexto::_('the course does not have self-evaluation') ?></h1></div>
            </div>
        </div>
        <!--END RESULTADOS-->
    </div>
</div>
<!-- END PANEL-->
<script type="text/javascript">
function drawresult(){
     var idcurso = $('#select_viewGeneral').val();
     $.ajax({
        url: _sysUrlBase_ + "/reportealumno/autoevaluacionjson",
        type: 'POST',
        dataType: 'json',
        data: {'idcurso': idcurso}
     }).done(function(resp){
        if(resp.code == 'ok'){
            if(resp.data != ''){
                $('#displayInfo-alert').hide();
                $('#displayInfo-container').show();
                var navTab = '';
                var contentTab = '';
                for(value of resp.data){
                    navTab = navTab.concat('<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#'+value.sesion+'">'+value.sesion+'</a></li>');
                    var habilidadesContent = '';
                    var resultadoExamen = '<p class="alert alert-danger">no ha realizado la autoevaluación</p>';
                    //habilidades:
                    if( value.examen != null){
                        value.examen.habilidades = JSON.parse(value.examen.habilidades);
                        for(element of value.examen.habilidades ){
                            var nameSkill = element.skill_name;
                            var puntaje = value.examen.habilidad_puntaje[element.skill_id];
                            habilidadesContent = habilidadesContent.concat('<div><i class="fa fa-circle" aria-hidden="true"></i> '+nameSkill+' <div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="32.46" aria-valuemin="0" aria-valuemax="100" style="width: '+puntaje+'%;">'+puntaje+'%</div></div></div>');
                        }
                        resultadoExamen = '<div id="resultInfo" style="text-align:center;">'+value.examen.notatexto+'</div><div id="habilidad">'+habilidadesContent+'</div>';
                    }
                    contentTab = contentTab.concat('<div class="tab-pane container" id="'+value.sesion+'">'+resultadoExamen+'</div>');
                    $('#displayInfo-container').html('<ul class="nav nav-tabs">'+navTab+'</ul><div class="tab-content">'+contentTab+'</div>');
                }
            }else{
                $('#displayInfo-alert').show();
                $('#displayInfo-container').hide();
            }
        }else{
            $('#displayInfo-container').hide();
            $('#displayInfo-alert').show();
            alert(resp.message);
        }
     }).fail(function(xhr,textStatus,errorThrow){
        $('#displayInfo-container').hide();
        $('#displayInfo-alert').show();
        alert(xhr.responseText);
     });
}
$(document).ready(function(){
    drawresult();
    $('#select_viewGeneral').change(function(){
        drawresult();
    });
});
</script>
