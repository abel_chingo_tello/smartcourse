<?php defined('RUTA_BASE') or die();
$idgui = uniqid();

$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();



?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css"
>




<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
}

</style>
<div class="container">
  <div class="row " id="levels" style="padding-top: 1ex; ">
            <div class="col-md-12">
                <ol class="breadcrumb">
                  <li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Home")?></a></li>                  
                  <li class="active"><?php echo JrTexto::_("Reporte de Productividad")?></li>
                </ol>
            </div>
             
               

    </div>

  

<div class="row " id="levels" style="padding-top: 1ex; ">

<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("Reporte")?>
    </div>

    <div class = 'panel-body' style="text-align: center;  " >
        


       
        <?php
        $reporte2='';
        $display='';
        $reporte="";
        $fondocolor="0099FF,99CC33,FF6600,FFCC00,FF6699,";
        $colores=explode(",",$fondocolor);
        $x=0;

        if(!empty($this->lista_curso)){
        foreach ($this->lista_curso as $lista1){
          
          $curso = $lista1['nombre'];
          $idcurso = $lista1['idcurso'];
          $notae = ($this->notase[$idcurso]/100)*20;
          $notas = ($this->notass[$idcurso]/100)*20;

          $reporte.="['".$curso."', ".$notae.", ".$notas."],";

         
        }//for*/

        

        /*for ($i=1;$i<=10;$i++){
          $ale = rand (1,20);
          $ale2 = rand (1,20);
          $reporte.="['".$curso.$ale."', ".$ale.", ".$ale2."],";          
        }*/

          
        }else{
          $reporte2='No Hay datos!!!';
          $display="none";
        }


        ?>

        <div id="columnchart_material" style="width: auto; height: 600px; overflow: auto; display: <?php echo $display?>"></div>
        <?php echo $reporte2;?>


    </div>
</div>



</div>
</div>


<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic();?>/libs/demochart/loader.js"></script>



    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Cursos', 'Entrada', 'Salida'],
          <?=$reporte?>
        ]);

        var options = {
          chart: {
            title: 'Reporte de Productividad',
            subtitle: '',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>