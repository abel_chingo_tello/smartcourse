
<?php

defined('RUTA_BASE') or die();
$idgui = uniqid();

$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$url = $this->documento->getUrlBase();
?>

<style type="text/css">
.select-ctrl-wrapper:after{
  right:0!important;
}
</style>

<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <div class="col-md-12">
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
      <li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Reports")?></a></li>                  
      <li class="active">
        <?php echo JrTexto::_("Progress by Competition"); ?>
      </li>
    </ol>
  </div>
</div>
<?php endif; ?>

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <?php echo JrTexto::_("Report");?> 
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4" style="text-align:center;">
                    <div class="" style="/*height:90px;*/">
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                        <h2><?php echo $this->fullname ?></h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <div style="text-align:center;">
                        <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Courses"); ?></p>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="select_viewTable" id="select_course" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                        <?php
                            if(!empty($this->cursos)){               
                            foreach ($this->cursos as $key => $lista_nivel){
                                $nombre=$lista_nivel['nombre'];
                                $idnivel=$lista_nivel['idcurso'];
                                echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                            }
                            }else{
                            echo '<option value="0">Sin cursos registrado</option>';
                            }
                        ?>
                        </select>
                    </div>
                    <div style="padding:10px 5px; text-align:left;" id="listarcompetenciaxcurso">
                        <div class="item_competencia" data-idcompetencia="1">
                            <p style="font-weight:bold;"><i class="fa fa-circle" aria-hidden="true"></i> Se comunica oralmente en inglés como lengua extranjera</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                            </div>
                        </div>
                        <div class="item_competencia" data-idcompetencia="2">
                            <p style="font-weight:bold;"><i class="fa fa-circle" aria-hidden="true"></i> Lee diversos tipos de textos escritos en inglés como lengua extranjera</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-warning progress-bar-animated active" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                            </div>
                        </div>
                        <div class="item_competencia" data-idcompetencia="3">
                            <p style="font-weight:bold;"><i class="fa fa-circle" aria-hidden="true"></i> Escribe diversos tipos de textos en inglés como lengua extranjera.</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-danger progress-bar-animated active" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12"><hr /></div>
            <div class="col-md-12">
                <h2><?php echo JrTexto::_("See progress per unit"); ?></h2>
                <div>
                    <h4 style="display:inline-block;"><?php echo JrTexto::_("Units"); ?></h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select class="select-docente form-control select-ctrl " style="min-width:220px; width:325px; height:35px;" id="item1">
                            <option value="0"><?php echo JrTexto::_("Select Unit"); ?></option>
                        </select>
                    </div>
                </div>
                <hr/>
            </div>
            <div class="col-md-12" id="listarcompetenciaxunit">
                <div class="col-sm-4 comp_item" data-idcompetencia="1">
                    <div style="border:1px solid gray; border-radius:0.5em; padding:2px 5px; background-color:#f2f5ff; font-weight:bold;">
                        <h2>Competencia</h2>
                        <h4>Se comunica oralmente en inglés como lengua extranjera</h4>
                        <p>Capacidades:</p>
                        <ul>
                        </ul>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-success progress-bar-animated active" style="width:0%;">0%</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 comp_item" data-idcompetencia="2">
                    <div style="border:1px solid gray; border-radius:0.5em; padding:2px 5px; background-color:#fffaf2; font-weight:bold;">
                        <h2>Competencia</h2>
                        <h4>Lee diversos tipos de textos escritos en inglés como lengua extranjera</h3>
                        <p>Capacidades:</p>
                        <ul>
                        </ul>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-success progress-bar-animated active" style="width:0%;">0%</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 comp_item" data-idcompetencia="3">
                    <div style="border:1px solid gray; border-radius:0.5em; padding:2px 5px; background-color:#fff3f2; font-weight:bold;">
                        <h2>Competencia</h2>
                        <h4>Escribe diversos tipos de textos en inglés como lengua extranjera.</h3>
                        <p>Capacidades:</p>
                        <ul>
                        </ul>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-success progress-bar-animated active" style="width:0%;">0%</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--END PANEL PRIMARY-->
    </div>
</div>

<script type="text/javascript">
var habilidadesGeneral = <?php echo (!empty($this->habilidadxcurso)) ? json_encode($this->habilidadxcurso) : 'null' ?>;
var capacidadesUnidad = <?php echo (!empty($this->capacidadesxunidad)) ? json_encode($this->capacidadesxunidad) : 'null' ?>;
var competencias = <?php echo (!empty($this->competencia)) ? json_encode($this->competencia) : 'null' ?>;

function displayHabilidadGeneral(val){
    if(val != 0){
        if(habilidadesGeneral != null){
            $('#listarcompetenciaxcurso').find('.item_competencia').each(function(k,v){
                if($(this).data('idcompetencia') == 1){
                    var valor = 0;
                    var v1 = parseFloat(habilidadesGeneral[val]['4']);
                    var v2 = parseFloat(habilidadesGeneral[val]['7']);
                    if(v1 != 0 || v2 != 0){
                        valor = v1;
                    }
                    $(this).find('.progress-bar').css('width',valor.toString()+'%');
                    $(this).find('.progress-bar').text(valor.toString()+'%');
                }else if($(this).data('idcompetencia') == 2){
                    var valor = 0;
                    var v1 = parseFloat(habilidadesGeneral[val]['5']);
                    if(v1 != 0 ){
                        valor = v1;
                    }
                    $(this).find('.progress-bar').css('width',valor.toString()+'%');
                    $(this).find('.progress-bar').text(valor.toString()+'%');
                }else if($(this).data('idcompetencia') == 3){
                    var valor = 0;
                    var v1 = parseFloat(habilidadesGeneral[val]['6']);
                    if(v1 != 0 ){
                        valor = v1;
                    }
                    $(this).find('.progress-bar').css('width',valor.toString()+'%');
                    $(this).find('.progress-bar').text(valor.toString()+'%');
                }
            });
        }//end if validar habilidades generales
    }//end if validar valor
}

function drawSelectUnit(val){
    if(val != 0){
        $('#item1').html('<option value="0"><?php echo JrTexto::_("Select Unit"); ?></option>');
        if(capacidadesUnidad != null){
            for(var i = 0; i < Object.keys(capacidadesUnidad).length;i++){
                if(capacidadesUnidad[i].idcurso == val){
                    $('#item1').append('<option value="'+capacidadesUnidad[i].orden+'" >'+capacidadesUnidad[i].unidad+'</option>');
                }
            }
        }// end if validar capacidad
    }//end if validar valor
}

$('document').ready(function(){
    displayHabilidadGeneral($('#select_course').val());
    drawSelectUnit($('#select_course').val());
    $('#select_course').change(function(){
        displayHabilidadGeneral($(this).val());
        drawSelectUnit($(this).val());
        $('#listarcompetenciaxunit .comp_item').find('.progress-bar').css('width','0%');
        $('#listarcompetenciaxunit .comp_item').find('.progress-bar').text('0%');
        $('#listarcompetenciaxunit .comp_item').find('ul').html(' ');
    });
    $('#item1').change(function(){

        var val = $('#select_course').val();
        var val2 = $(this).val();
        
        if(capacidadesUnidad != null && competencias!=null && val2 != 0){
            /**BUSCAR CAPACIDADES */
            $('#listarcompetenciaxunit').find('.comp_item').each(function(k,v){$(this).find('ul').html(' ');});
            $('#listarcompetenciaxunit').find('.comp_item').each(function(k,v){
                for(var i = 0; i < Object.keys(capacidadesUnidad).length;i++){
                    if(capacidadesUnidad[i].idcurso == val && capacidadesUnidad[i].orden == val2){
                        var idcompetencia = $(this).data("idcompetencia");
                        // console.log(Object.keys(capacidadesUnidad[i].capacidades).length);
                        // console.log(capacidadesUnidad[i].capacidades);
                        for(var j = 0; j < Object.keys(capacidadesUnidad[i].capacidades).length; j++){
                            // console.log(capacidadesUnidad[i].capacidades[j]);
                            // console.log(capacidadesUnidad[i].capacidades[j]);
                            if(capacidadesUnidad[i].capacidades[j].idcompetencia == idcompetencia){
                                $(this).find('ul').append('<li>'+capacidadesUnidad[i].capacidades[j].nombre+'</li>');
                            }
                        }
                        break;
                    }
                }
            });
            /**BUSCAR HABILIDAD */
            for(var i = 0; i < Object.keys(capacidadesUnidad).length; i++){
                if(capacidadesUnidad[i].idcurso == val && capacidadesUnidad[i].orden == val2){
                    var objeto = new Object;
                    objeto.idcurso = val;
                    objeto.idrecurso = capacidadesUnidad[i].idrecurso;
                    console.log(objeto);
                    $.ajax({
                        url: _sysUrlBase_+'/reportes/getHabilidadxUnidad',
                        type: 'POST',
                        dataType: 'json',
                        data: objeto,
                    }).done(function(resp) {
                        if(resp.code=='ok'){
                            if(resp.data){
                                $('#listarcompetenciaxunit').find('.comp_item').each(function(k,v){
                                    var valor = 0;
                                    if($(this).data('idcompetencia') == 1){
                                        if(resp.data['4'] != 0 && resp.data['7'] != 0){
                                            valor = (parseFloat(resp.data['4']) + parseFloat(resp.data['7']) ) * 100 / 200 ;
                                        }
                                    }else if($(this).data('idcompetencia') == 2){
                                        valor = (resp.data['5'] != 0) ? resp.data['5'] : 0;                                        
                                    }else if($(this).data('idcompetencia') == 3){
                                        valor = (resp.data['6'] != 0) ? resp.data['6'] : 0;                                                                                
                                    }
                                    $(this).find('.progress-bar').css('width',valor.toString()+'%');
                                    $(this).find('.progress-bar').text(valor.toString()+'%');
                                });
                            }
                        }else{
                            return false;
                        }
                    }).fail(function(xhr, textStatus, errorThrown) {
                        return false;
                    });
                    break;
                }//end if
            }

        }//end if verificar

    });
});
</script>