<?php defined('RUTA_BASE') or die();
$idgui = uniqid();

$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$tiemposArray = array('tiempoPV' => 0, 'tiempoG' => 0, 'tiempoA' => 0, 'tiempoE' => 0, 'tiempoSB' => 0, 'tiempoT' => 0);

function conversorSegundosHoras($tiempo_en_segundos)
{
    $horas = floor($tiempo_en_segundos / 3600);
    $minutos = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
    $segundos = $tiempo_en_segundos - ($horas * 3600) - ($minutos * 60);

    return $horas . ':' . $minutos . ":" . $segundos;
}

if (!empty($this->plataforma)) {
    foreach ($this->plataforma as $lista1) {
        $tiemposArray['plataforma'] = abs($lista1['tiempo']);
        $totalpv = conversorSegundosHoras(abs($lista1['tiempo']));
        //echo $totalpv."<br>";
        $horas = explode(":", $totalpv);

        $hora = $horas[0];
        if (!$hora) $hora = 0;
        $min = $horas[1];
        if (!$min) $min = 0;
        $totalpv = number_format($hora . '.' . $min, 2, '.', ' ');

        $horasCount_plataforma = $horas;
    } //for
}
if (!empty($this->sesiones)) {
    foreach ($this->sesiones as $lista1) {
        $tiemposArray['sesiones'] = abs($lista1['tiempo']);
        $totalpv = conversorSegundosHoras(abs($lista1['tiempo']));
        //echo $totalpv."<br>";
        $horas = explode(":", $totalpv);

        $hora = $horas[0];
        if (!$hora) $hora = 0;
        $min = $horas[1];
        if (!$min) $min = 0;
        $totalsesiones = number_format($hora . '.' . $min, 2, '.', ' ');

        $horasCount_sesiones = $horas;
    } //for
}
if (!empty($this->examenes)) {
    foreach ($this->examenes as $lista1) {
        $tiemposArray['examenes'] = abs($lista1['tiempo']);
        $totalpv = conversorSegundosHoras(abs($lista1['tiempo']));
        //echo $totalpv."<br>";
        $horas = explode(":", $totalpv);

        $hora = $horas[0];
        if (!$hora) $hora = 0;
        $min = $horas[1];
        if (!$min) $min = 0;
        $totalexamenes = number_format($hora . '.' . $min, 2, '.', ' ');

        $horasCount_examenes = $horas;
    } //for
}

//echo conversorSegundosHoras(16064);
?>
<style type="text/css">
    .row>.item-recurso {
        text-align: center;
        padding: 1ex;
    }

    .row>.item-recurso .panel {
        text-align: center;
        padding: 0.2ex;
        margin: 0.05ex;
        0.1ex;
    }

    .row>.item-recurso .panel-body {
        padding: 0.1ex;
    }

    .slick-slide {
        position: relative;
    }

    .cajaselect {
        overflow: hidden;
        width: 230px;
        position: relative;
        font-size: 1.8em;
    }

    select#level-item,
    select#unidad-item,
    select#actividad-item {
        background: transparent;
        border: 2px solid #4683af;
        padding: 5px;
        width: 250px;
        padding: 0.3ex 2ex;
    }

    select:focus {
        outline: none;
    }

    .cajaselect::after {
        font-family: FontAwesome;
        content: "\f0dd";
        display: inline-block;
        text-align: center;
        width: 30px;
        height: 100%;
        background-color: #4683af;
        position: absolute;
        top: 0;
        right: 0px;
        pointer-events: none;
        color: antiquewhite;
        bottom: 0px;
    }

    .titulo {
        border: solid 0px #f00;
        position: relative;
        top: 30px;
        left: 15px;
        z-index: 1000;
        width: 85%;
        background-color: rgba(255, 255, 255, 0.53);
        color: #000;
    }

    .autor {
        border: solid 0px #f00;
        position: relative;
        bottom: 28px;
        left: 12px;
        z-index: 1000;
        width: 85%;
        text-align: right;
        background-color: rgba(255, 255, 255, 0.53);
        color: #000;
    }

    .caratula {
        border: solid 0px #f00;
        position: absolute;
        top: 38px;
        left: 25px;
        width: 75%;
        height: 68%
    }

    .border {
        border: solid 0px #f00;
    }

    .border2 {
        border: solid 0px #337AB7;
        border-radius: 5px;
    }

    .div_mante {
        opacity: 0;
        -webkit-transition: opacity 500ms;
        -moz-transition: opacity 500ms;
        -o-transition: opacity 500ms;
        -ms-transition: opacity 500ms;
        transition: opacity 500ms;
    }

    .card {
        display: block;
        width: 100%;
        border: 1px solid #ccc;
        padding: 0;
    }

    .card .card-title {
        font-size: 15px;
        font-weight: bold;
        border-bottom: 1px solid #ccc;
        padding-left: 15px;
        line-height: 2.2;
        height: 80px;
    }

    .card .card-info {
        font-size: 2.5em;
        text-shadow: 2px 1px 3px #aaa;
        font-weight: bolder;
        text-align: center;
        border-bottom: 1px solid #efefef;
    }

    .card .card-info:last-child {
        border-bottom: none;
    }

    .card .card-info .info-tiempo.tiempo-optimo {
        font-size: .7em;
    }

    .card.card-time .card-info .anio:after,
    .card.card-time .card-info .mes:after,
    .card.card-time .card-info .dia:after,
    .card.card-time .card-info .hora:after,
    .card.card-time .card-info .min:after,
    .card.card-time .card-info .seg:after {
        font-size: 0.7em;
    }

    .card.card-time .card-info .anio:after {
        content: "y";
    }

    .card.card-time .card-info .mes:after {
        content: "m";
    }

    .card.card-time .card-info .dia:after {
        content: "d";
    }

    .card.card-time .card-info .hora:after {
        content: "h";
    }

    .card.card-time .card-info .min:after {
        content: "m";
    }

    .card.card-time .card-info .seg:after {
        content: "s";
    }

    .card .card-info .promedio {
        line-height: 75px;
    }

    .card .card-info .comentario span {
        display: block;
    }

    .card .card-info .comentario span.letras {
        font-size: .5em;
    }

    /*
* CUSTOM
*/
    .time-container {
        padding: 10px 5px;
        float: none;
        display: inline-block;
        vertical-align: top;
    }

    @media only screen and (max-width: 990px) {
        .card .card-info {
            font-size: 1.5em;
        }

        .card .card-title {
            font-size: 13px;
        }
    }
</style>
<div style="padding:20px;"></div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h5>Tiempo de estudio</h5>
    </div>
    <div class="panel-body row">
        <div class="col-lg-4 col-md-4 col-xs-12">
            <!--START CARD-->
            <div class="card card-time">
                <div class="card-title">Tiempo en la plataforma virtual</div>
                <div class="card-info">
                    <div class="col-xs-12  resultado_tiempos">
                        <!--col-sm-6-->
                        <div class="info-tiempo tiempo-obtenido">
                            <span class="anio" style="display: none;">00</span>
                            <span class="mes" style="display: none;">00</span>
                            <span class="dia" style="display: none;">00</span>
                            <span class="hora"><?php echo (!empty($horasCount_plataforma[0])) ? $horasCount_plataforma[0] : '0'; ?></span>
                            <span class="min"><?php echo (!empty($horasCount_plataforma[1])) ? $horasCount_plataforma[1] : '0'; ?></span>
                            <span class="seg"><?php echo (!empty($horasCount_plataforma[2])) ? $horasCount_plataforma[2] : '0'; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <!--END CARD-->
        </div>
        <div class="col-lg-4 col-md-4 col-xs-12">
            <!--START CARD-->
            <div class="card card-time">
                <div class="card-title">Tiempo en los exámenes</div>
                <div class="card-info">
                    <div class="col-xs-12  resultado_tiempos">
                        <!--col-sm-6-->
                        <div class="info-tiempo tiempo-obtenido">
                            <span class="anio" style="display: none;">00</span>
                            <span class="mes" style="display: none;">00</span>
                            <span class="dia" style="display: none;">00</span>
                            <span class="hora"><?php echo (!empty($horasCount_examenes[0])) ? $horasCount_examenes[0] : '0'; ?></span>
                            <span class="min"><?php echo (!empty($horasCount_examenes[1])) ? $horasCount_examenes[1] : '0'; ?></span>
                            <span class="seg"><?php echo (!empty($horasCount_examenes[2])) ? $horasCount_examenes[2] : '0'; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <!--END CARD-->
        </div>
        <div class="col-lg-4 col-md-4 col-xs-12">
            <!--START CARD-->
            <div class="card card-time">
                <div class="card-title">Tiempo en sesiones</div>
                <div class="card-info">
                    <div class="col-xs-12  resultado_tiempos">
                        <!--col-sm-6-->
                        <div class="info-tiempo tiempo-obtenido">
                            <span class="anio" style="display: none;">00</span>
                            <span class="mes" style="display: none;">00</span>
                            <span class="dia" style="display: none;">00</span>
                            <span class="hora"><?php echo (!empty($horasCount_sesiones[0])) ? $horasCount_sesiones[0] : '0'; ?></span>
                            <span class="min"><?php echo (!empty($horasCount_sesiones[1])) ? $horasCount_sesiones[1] : '0'; ?></span>
                            <span class="seg"><?php echo (!empty($horasCount_sesiones[2])) ? $horasCount_sesiones[2] : '0'; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <!--END CARD-->
        </div>
    </div>
</div>