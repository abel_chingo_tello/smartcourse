<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: black; font-size: large; }

.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){ margin:0 auto; }
.text-nota1{ font-size: x-large; font-weight: bold; color: #b73333; }
.text-custom1 { font-size: large; border-bottom: 1px solid gray; border-radius: 10%; box-shadow: 0px 1px #e20b0b; }
#tabla_alumnos td{ cursor:pointer; }
#tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
#tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
#tabla_alumnos_length { display: none; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>
<div style="padding:10px;"></div>
<!--START PANEL-->
<div class="panel panel-primary">
    <div class="panel-heading"><h5>Progreso del alumno</h5></div>
    <div class="panel-body row">
        <div class="col-lg-12 col-md-12 text-center">
            <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Courses"); ?></p>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select name="select_viewTable" id="select_viewTable" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                    <?php 
                        if(empty($this->cursos)){
                            echo '<option value="0">No hay cursos matriculados</option>';
                        }else{
                            foreach($this->cursos as $key => $value){
                                echo "<option value='{$value['idcurso']}'>{$value['nombre']}</option>";
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="col-lg-12 col-md-12">
                <div id="progress_total" style="text-align:center;">
                    <h4 style="padding-top:15px;">Total progreso en el curso</h4>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                        aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                            <!-- 40% Complete (success) -->
                        </div>
                    </div>
                    <div class="countPercentage" data-value="35"><span>0</span>%</div>
                    <div style="padding:20px;"></div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div style="border:1px solid gray; border-radius:1em; padding:5px 0; box-shadow: -3px 3px 14px;">
                    <h3>Contenido</h3>
                    <div id="chart1-container">
                        <canvas id="chart1" class="chartjs-render-monitor" style="display: block;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END PANEL-->

<script type="text/javascript">
function countText(obj,valueText, ToValue){
  var currentValue = parseInt(valueText);
  var nextVal = ToValue;
  var diff = nextVal - currentValue;
  var step = ( 0 < diff ? 1 : -1 );
  for (var i = 0; i < Math.abs(diff); ++i) {
      setTimeout(function() {
          currentValue += step
          obj.text(currentValue);
      }, 50 * i)   
  }
}

function drawBarHorizontal(ctx,data){
    var myPieChart = new Chart(ctx,{
        type: 'horizontalBar',
        data: data,
        options: {
        legend: false,
            scales: {
                xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Percentage'
                },
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}
function drawBarMain(){
    var progressbar = $('#progress_total').find('.progress-bar');
    var progressText = $('#progress_total').find('.countPercentage');
    var idcurso = $('#select_viewTable').val();
    if(idcurso != 0){
        $.ajax({
            url: _sysUrlBase_+'/reportealumno/totalprogresscursojson',
            type: 'POST',
            dataType: 'json',
            data: {'idcurso':idcurso},
        }).done(function(resp) {
            if(resp.code == 'ok'){
                progressbar.css('width',Math.floor(resp.data)+'%');
                progressText.find('span').text(Math.floor(resp.data));
            }else{
                alert(resp.message);
            }
         }).fail(function(xhr,textStatus,errorThrown){
            alert(xhr.responseText); 
            return false;
         });
    }
}
function drawChartProgress(){
    var idcurso = $('#select_viewTable').val();
    if(idcurso != 0){
        $.ajax({
            url: _sysUrlBase_+'/reportealumno/progresoxsesionesjson',
            type: 'POST',
            dataType: 'json',
            data: {'idcurso':idcurso},
        }).done(function(resp) {
            if(resp.code == 'ok'){
                var labels = new Array();
                var data_progreso = new Array();
                for(value of resp.data){
                  labels.push(value.sesion);
                  data_progreso.push(value.progreso);
                }            
                chartData = {
                  labels: labels,
                  datasets: [{
                    backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)'],
                    data: data_progreso,
                    borderColor: 'white',
                    borderWidth: 2
                  }]
                };
                $('#chart1-container').html("").html('<canvas id="chart1" class="chartjs-render-monitor" style="display: block;"></canvas>'); //No es la mejor opcion investigar update de la libreria
                drawBarHorizontal(document.getElementById("chart1").getContext('2d'),chartData);
            }else{
                alert(resp.message);
            }
         }).fail(function(xhr,textStatus,errorThrown){
            alert(xhr.responseText); 
            return false;
         });
    }
}
$('document').ready(function(){
    drawBarMain();
    drawChartProgress()
    $('#select_viewTable').change(function(){
        drawBarMain(); 
        drawChartProgress()       
    });
});
</script>