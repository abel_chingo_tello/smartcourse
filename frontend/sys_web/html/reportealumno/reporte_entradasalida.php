<?php
defined('RUTA_BASE') or die(); 
$url = $this->documento->getUrlBase();
$idgui = uniqid();
$frm=!empty($this->datos_Perfil)?$this->datos_Perfil:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';
$url_smartquiz = $this->url;
//variables for javascript
$JsonVariable = null;
if(!empty($this->examen)){
    $JsonVariable = 0;
    if(count($this->examen) == 1){
        // var_dump($this->examen);
        if(!is_null($this->examen[array_keys($this->examen)[0]])){
            // var_dump("extract");
            extract($this->examen[array_keys($this->examen)[0]]);
        }else{
            // var_dump("nullear");
            $JsonVariable = null;
        }
    }else{
        $JsonVariable = json_encode($this->examen);
    }
}

?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: black; font-size: large; text-align: center; }
</style>

<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_fichas.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_imagen_puntos.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_completar.css">
<style>
    .contenedor-preguntas{
        overflow: hidden;
        border-bottom: 2px solid #bbb;
    }
    .contenedor-preguntas:last-child{border: none;}
    .tmpl-media img,
    .tmpl-media audio{
        max-height: 250px;
    }
    .tmpl-media .embed-responsive{
        height: 250px;
        padding: 0;
        margin: 1ex;
    }
    
    *[data-corregido='good']:before, .good:before{
        background-color: #2ed353;
        border-radius: 50%;
        content: "\f118";
        display: inline-block;
        color: white;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: 1.7em;
        height: 23px;
        position: absolute;
        right: 0;
        text-align: center;
        width: 23px;
    }
    *[data-corregido='bad']:before, .bad:before{
        background: #fc5c5c;
        border-radius: 50%;
        color: white;
        content: "\f119";
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        height: 23px;
        font-size: 1.7em;
        position: absolute;
        right: 0;
        text-align: center;
        width: 23px;
    }
    .tpl_plantilla:before{
        content: '';
        background: transparent;
    }

    ul#list-intentos li {
      display: inline-block;
    }

    ul#list-intentos li a {
      background: #fff;
      border-radius: 50%;
      height: 22px;
      padding: 1px 5px;
      text-align: center;
      width: 22px;
    }
    ul#list-intentos li.active a {
      background: #6fe3ff;
      box-shadow: 0px 0px 3px 1px #444;
    }

    .panel .panel-heading {
      overflow: hidden !important;
    }
    
    .panel .panel-heading .list-unstyled {
      margin: 0;
    }

    @media (min-width: 768px){
        .table-key-value .key{
            width: 50% !important;
        }
    }

    /*estilos de impresion*/
    @media print{
        .contenedor-preguntas{
            page-break-after: always;
            break-after: always;
        }
        /* **** Join (Fichas) **** */
        .plantilla.plantilla-fichas .ejerc-fichas .ficha {
            width: 23%;
        }
    
        /* **** Verdadero o Falso **** */
        .plantilla.plantilla-verdad_falso .premise>div:first-child{
            width: 60%;
        }
        .plantilla.plantilla-verdad_falso .premise>.options{
            width: 40%;
            float: right;
        }

        /* **** Ordenar Parrafo **** */
        .plantilla.plantilla-ordenar.ord_parrafo .drag-parr,
        .plantilla.plantilla-ordenar.ord_parrafo .drop-parr{
            width: 50%;
        }

        /* **** Ordenar Parrafo **** */
        .plantilla.plantilla-img_puntos .contenedor-img{
            width: 75%;
        }
        .plantilla.plantilla-img_puntos .tag-alts{
            width: 25%;
        }
        .plantilla.plantilla-img_puntos .dot-container>.dot{
            background: #fff;
            border: 5px solid #4c5e9b;
        }
        .plantilla.plantilla-img_puntos .dot-container>.dot:after{
            border:  3px solid #efefef;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }
    }
</style>

<?php if(!$this->isIframe): ?>
<!-- <div style="position:relative;  margin:10px 0;">
    <a href="<?php echo $this->documento->getUrlBase();?>/reportealumno" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
</div> -->
<?php endif; ?>

<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <div class="col-md-12">
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
      <li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Reports")?></a></li>
      <li class="active">
        <?php echo ($this->entrada == true) ? JrTexto::_("Beginning test") : JrTexto::_("Final test"); ?>
      </li>
    </ol>
  </div>
</div>
<?php endif; ?>

<!--START CONTAINER-->
<div class="">
    <!--START PANEL-->
    <div class = 'panel panel-primary' >
        <div class = 'panel-heading' style="text-align: left;">
        <?php echo JrTexto::_("Report");?>
        </div>
        <div class = 'panel-body' style="text-align: center;  " >
            <h3 style="font-weight:bold;"><?php echo ($this->entrada == true) ? JrTexto::_("Beginning Test Score") : JrTexto::_("Final Test Score"); ?></h3>

            <div style=" display:block;width:100%; margin:10px 0; border-bottom: 1px solid gray; padding-bottom: 4px;">
                <p style="font-size:large; display:inline-block; margin:0; padding-right:15px;"><?php echo JrTexto::_('Courses'); ?></p>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="select_viewGeneral" id="select_viewGeneral" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
                    
                    <?php
                        if(!empty($this->cursos)){               
                        foreach ($this->cursos as $key => $lista_nivel){
                            $nombre=$lista_nivel['nombre'];
                            $idnivel=$lista_nivel['idcurso'];
                            echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                        }
                        }else{
                            echo "<option value='0'>".JrTexto::_('No data')."</option>";
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="container-info">
                <div class="row" style="margin:15px auto;">
                    <div class="col-md-4" style="/*height:90px;*/">
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                        <h2><?php echo $this->fullName ?></h2>
                    </div>
                    <div class="col-md-8" id="content-result">
                        <div id="content-result-nota"></div>
                        <?php if(!is_null($JsonVariable)):?>
                            <div class="col-sm-4" id="result-Fecha">
                                <h3 style="border: 2px solid gray; border-radius: 2em;"><?php echo JrTexto::_('Date'); ?></h3>
                                <p style="font-weight:bold; font-size:large;border: 2px solid black; border-top: none; border-radius: 0.2em;"></p>
                            </div>
                            <div class="col-sm-4" id="result-Intentos">
                                <h3 style="border: 2px solid gray; border-radius: 2em;"><?php echo JrTexto::_('Attempt'); ?></h3>
                                <p style="font-weight:bold; font-size:large;border: 2px solid black; border-top: none; border-radius: 0.2em;"></p>
                            </div>
                            <div class="col-sm-4" id="result-Tiempo">
                                <h3 style="border: 2px solid gray; border-radius: 2em;"><?php echo JrTexto::_('Time'); ?></h3>
                                <p style="font-weight:bold; font-size:large;border: 2px solid black; border-top: none; border-radius: 0.2em;"></p>
                            </div>
                        <?php else:?>
                            <div class="alert alert-danger" style="width: 90%;"><h1><?php echo JrTexto::_('Does not contain a test performed'); ?></h1></div>
                        <?php endif;?>
                    </div>
                </div>
                <!--end row-->
                
            </div>
        </div>
    </div>
    <!--END PANEL-->
    
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align: left;" >
            <?php echo JrTexto::_("Test");?>
        </div>
        <div class="panel-body" style="padding:0;">
            <div class="row" style="margin:15px auto;">
                <div class="col-md-12">
                    <div style="width:100%; padding: 5px; text-align: center; background-color: #337ab7;">
                      <?php if($this->entrada == true):?>
                          <h4 id="previewTitle" style="color:white;"><?php echo JrTexto::_('Beginning Test'); ?></h4>
                        <?php else: ?>
                          <h4 id="previewTitle" style="color:white;"><?php echo JrTexto::_('Final Test'); ?></h4>
                      <?php endif; ?>
                    </div>
                    <div class="" id="previewFrame2" style="width:100%; max-height: 600px; overflow: auto; border: 1px solid gray;">
                      <?php if($this->entrada == true):?>
                          <div class="alert alert-danger"><?php echo JrTexto::_('No there is beginning test'); ?></div>
                        <?php else: ?>
                          <div class="alert alert-danger"><?php echo JrTexto::_('No there is final test'); ?></div>
                      <?php endif; ?>
                    </div>

                    <!--<iframe id="previewFrame" style="width: 100%;" src=""></iframe>-->
                </div>
                <div class="col-sm-12"><h4><?php echo JrTexto::_('Skills progress report'); ?></h4></div>
                <div class="col-sm-3">
                  <div class="">
                    <p class="text-custom1">Listening</p>
                    <div id="barListening" class="circleProgressBar1" style="font-size:0.8em;"></div>
                  </div>
                </div>
                <div class="col-sm-3 ">
                  <div class="">
                    <p class="text-custom1">Reading</p>
                    <div id="barReading" class="circleProgressBar1" style="font-size:0.8em;"></div>
                  </div>
                </div>
                <div class="col-sm-3 ">
                  <div class="">
                    <p class="text-custom1">Writing</p>
                    <div id="barWriting" class="circleProgressBar1" style="font-size:0.8em;"></div>
                  </div>
                </div>
                <div class="col-sm-3 ">
                  <div class="">
                    <p class="text-custom1">Speaking</p>
                    <div id="barSpeaking" class="circleProgressBar1" style="font-size:0.8em;"></div>
                  </div>
                </div>
            </div>        
        </div>
    </div>
    <!--END PANEL-->
</div>
<!--END CONTAINER-->
<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">
var JsonVariable = <?php echo (!is_null($JsonVariable)) ? $JsonVariable : 'null' ?>;

var textofijo = $('#previewTitle').text();

function drawHabilidad(obj , option = null, value = 0, speed = null){
  var _anim = speed === null ? 1 : speed;
  var option = option === null ? {colorFg:"#FFFFFF",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
  option.animationSpeed = _anim;
  var bar=new RadialProgress(obj,option);
  bar.setValue(value);
  bar.draw(true);
}
var calcular = function(valor){
  if(typeof valor == 'undefined' || !valor){
    return 0;
  }
  return (valor == 0) ? 0 : (valor / 100);
};
if(JsonVariable != null){
    if(JsonVariable == 0){
        JsonVariable = new Object();
        JsonVariable.idalumno = <?php echo (isset($idalumno)) ? $idalumno : 'null' ?>;
        JsonVariable.idrecurso = <?php echo (isset($idrecurso)) ? $idrecurso : 'null' ?>;
        JsonVariable.nota = <?php echo (isset($nota)) ? $nota : 'null' ?>;
        JsonVariable.notatexto = '<?php echo (isset($notatexto)) ? $notatexto : 'null' ?>';
        JsonVariable.regfecha = '<?php echo (isset($regfecha)) ? $regfecha : 'null' ?>';
        JsonVariable.intento = <?php echo (isset($intento)) ? $intento : 'null' ?>;
        JsonVariable.tiempo_realizado = '<?php echo (isset($tiempo_realizado)) ? $tiempo_realizado : 'null' ?>';
        JsonVariable.habilidades = <?php echo (isset($habilidades)) ? $habilidades : 'null' ?>;
        JsonVariable.habilidad_puntaje = <?php echo (isset($habilidad_puntaje)) ? $habilidad_puntaje : 'null' ?>;
        JsonVariable.preguntas = <?php echo (isset($preguntas)) ? $preguntas : 'null'; ?>;
        
    }

}

// console.log(JsonVariable);
// console.log(Object.keys(JsonVariable).length);

var opti = {colorFg:"#337ab7",thick:10,fixedTextSize:0.3,colorText:"#000000" };

function main(){
    //draw preview
    drawHabilidad(document.getElementById("barListening"),opti, 0,0.15);
    drawHabilidad(document.getElementById("barReading"),opti, 0,0.15);
    drawHabilidad(document.getElementById("barWriting"),opti, 0,0.15);
    drawHabilidad(document.getElementById("barSpeaking"),opti, 0,0.15);
    
    viewPreview();


    if($('#select_viewGeneral').val() != 0){
        var idcurso = $('#select_viewGeneral').val();
        if(JsonVariable[idcurso]){
            //coder here
            $('#content-result-nota').html(JsonVariable[idcurso].notatexto);
            $('#content-result-nota').append('<h1>\<?php echo JrTexto::_("Final Score"); ?>: '+(Math.floor(JsonVariable[idcurso].nota * 0.20))+'pts</h1>');
            $('#result-Fecha').find('p').html(JsonVariable[idcurso].regfecha);
            $('#result-Intentos').find('p').html(JsonVariable[idcurso].intento);
            $('#result-Tiempo').find('p').html(JsonVariable[idcurso].tiempo_realizado);
        }else{
            //codehere
            $('#content-result-nota').html(JsonVariable.notatexto);
            $('#content-result-nota').append('<h1>\<?php echo JrTexto::_("Final Score"); ?>: '+(Math.floor(JsonVariable.nota * 0.20))+'pts</h1>');
            $('#result-Fecha').find('p').html(JsonVariable.regfecha);
            $('#result-Intentos').find('p').html(JsonVariable.intento);
            $('#result-Tiempo').find('p').html(JsonVariable.tiempo_realizado);
        }
    }
}

function viewPreview(){
    if($('#select_viewGeneral').val() != 0){
        //idusuario
        //https://localhost/pvingles.local/smartquiz/reporte/resultado_alumno/?slug=smartlearn2017&identificador=97&idexamen=342
        var url_preview = '';
        var idcurso = $('#select_viewGeneral').val();
        /*
        if(JsonVariable[idcurso]){
            url_preview = _sysUrlBase_+'/smartquiz/reporte/resultado_alumno/?slug=smartlearn2017&identificador='+JsonVariable[idcurso].idalumno+'&idexamen='+JsonVariable[idcurso].idrecurso;            
        }else{
            url_preview = _sysUrlBase_+'/smartquiz/reporte/resultado_alumno/?slug=smartlearn2017&identificador='+JsonVariable.idalumno+'&idexamen='+JsonVariable.idrecurso;
        }
        $('#previewFrame').attr('src', url_preview);
        */
        var title = function(){ 
          var t = '';
          $('#select_viewGeneral').find('option').each(function(k,v){
            if($(this).attr('value') == $('#select_viewGeneral').val()){
              t= $(this).text();
            }
          }); 
          return t;
        }
        
        $('#previewTitle').text(textofijo+' '+title());
        console.log("hola");
        if(JsonVariable != null){



          $('#previewFrame2').html(' ');

          if(typeof JsonVariable[idcurso] == 'undefined'){
            JsonVariable.habilidad_puntaje = (typeof JsonVariable.habilidad_puntaje != 'object') ? JSON.parse(JsonVariable.habilidad_puntaje) : JsonVariable.habilidad_puntaje;
            drawHabilidad(document.getElementById("barListening"),opti, calcular(parseFloat(JsonVariable.habilidad_puntaje[4])),0.15);
            drawHabilidad(document.getElementById("barReading"),opti, calcular(parseFloat(JsonVariable.habilidad_puntaje[5])),0.15);
            drawHabilidad(document.getElementById("barWriting"),opti, calcular(parseFloat(JsonVariable.habilidad_puntaje[6])),0.15);
            drawHabilidad(document.getElementById("barSpeaking"),opti, calcular(parseFloat(JsonVariable.habilidad_puntaje[7])),0.15);
            // cadena = JSON.parse(JsonVariable.preguntas);
            if(JsonVariable.preguntas != null && typeof JsonVariable.preguntas != 'object'){
                JsonVariable.preguntas = JSON.parse(JsonVariable.preguntas);
            }
            for(var i in JsonVariable.preguntas){
                // var cadena = (JsonVariable.preguntas[i]).replace('__xRUTABASEx__',_sysUrlBase_);
                // console.log(JsonVariable.preguntas[i]);
              $('#previewFrame2').append(JsonVariable.preguntas[i]);
            }
          }else{
            JsonVariable[idcurso].habilidad_puntaje = (typeof JsonVariable[idcurso].habilidad_puntaje != 'object') ? JSON.parse(JsonVariable[idcurso].habilidad_puntaje) : JsonVariable[idcurso].habilidad_puntaje;
            drawHabilidad(document.getElementById("barListening"),opti, calcular(parseInt(JsonVariable[idcurso].habilidad_puntaje['4'])),0.15);
            drawHabilidad(document.getElementById("barReading"),opti, calcular(parseInt(JsonVariable[idcurso].habilidad_puntaje['5'])),0.15);
            drawHabilidad(document.getElementById("barWriting"),opti, calcular(parseInt(JsonVariable[idcurso].habilidad_puntaje['6'])),0.15);
            drawHabilidad(document.getElementById("barSpeaking"),opti, calcular(parseInt(JsonVariable[idcurso].habilidad_puntaje['7'])),0.15);
            if(JsonVariable[idcurso].preguntas != null && typeof JsonVariable[idcurso].preguntas != 'object'){
                JsonVariable[idcurso].preguntas = JSON.parse(JsonVariable[idcurso].preguntas);
            }
            for(var i in JsonVariable[idcurso].preguntas){
              $('#previewFrame2').append(JsonVariable[idcurso].preguntas[i]);
            }
          }

        }
    }
}

$('document').ready(function(){
    var url = '<?php echo $url_smartquiz ?>';
    $.ajax({
        url: url,
        type: 'POST',
        data: {}
    }).done(function(resp) {
        console.log("listo");
    }).fail(function(xhr, textStatus, errorThrown) { 
        return false; 
    });
    if(JsonVariable != null){
        main();
    }
    

    $('#select_viewGeneral').on('change',function(){
        if($('#select_viewGeneral').val() != 0){
            var idcurso = $('#select_viewGeneral').val();
            if(JsonVariable[idcurso]){
                //coder here
                $('#content-result-nota').html(JsonVariable[idcurso].notatexto);
                $('#result-Fecha').find('p').html(JsonVariable[idcurso].regfecha);
                $('#result-Intentos').find('p').html(JsonVariable[idcurso].intento);
                $('#result-Tiempo').find('p').html(JsonVariable[idcurso].tiempo_realizado);
            }else{
                //codehere
                $('#content-result-nota').html(JsonVariable.notatexto);
                $('#result-Fecha').find('p').html(JsonVariable.regfecha);
                $('#result-Intentos').find('p').html(JsonVariable.intento);
                $('#result-Tiempo').find('p').html(JsonVariable.tiempo_realizado);
            }
        }
        viewPreview();
    });
});

</script>