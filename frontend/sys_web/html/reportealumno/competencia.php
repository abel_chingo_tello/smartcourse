<style>
.cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

.cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }
select {
    padding-right:30px !important;
}
.select-ctrl-wrapper:after{
  right:0!important;
}

</style>
<div style="padding:15px;"></div>
<div class="panel panel-primary">
    <div class="panel-heading"><h5><?php echo JrTexto::_('progress by competition') ?></h5></div>
    <div class="panel-body row">
        <div class="col-lg-12 col-md-12" style="text-align:center;">
            <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Courses"); ?></p>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select name="select_viewTable" id="select_viewTable" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                    <?php 
                        if(empty($this->cursos)){
                            echo '<option value="0">No hay cursos matriculados</option>';
                        }else{
                            foreach($this->cursos as $key => $value){
                                echo "<option value='{$value['idcurso']}'>{$value['nombre']}</option>";
                            }
                        }
                    ?>
                </select>
            </div>
            <div style="padding:20px;"></div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div id="progress_total" style="text-align:center; width:70%; margin:0 auto;">
                <h4 style="padding-top:15px;"><?php echo JrTexto::_('progress') ?></h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                    aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        <!-- 40% Complete (success) -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div id="contenedor01" style="font-weight:bold; font-size:large;  width: 70%; background: #e7fbe8; border-radius: 1em; margin: 0 auto; padding: 15px;;">
                <div>
                    <i class="fa fa-circle" aria-hidden="true"></i> <?php echo JrTexto::_('target number') ?> 1
                </div>
                <div>
                    <i class="fa fa-circle" aria-hidden="true"></i> <?php echo JrTexto::_('target number') ?> 2
                </div>
                <div>
                    <i class="fa fa-circle" aria-hidden="true"></i> <?php echo JrTexto::_('target number') ?> 3
                </div>
            </div>
            
        </div>
    </div>
</div>

<script type="text/javascript">
function drawlist(){
    idcurso = $('#select_viewTable').val();
    if(idcurso != 0){
        $.ajax({
            url: _sysUrlBase_ + "/reportealumno/competenciacursojson",
            type: 'POST',
            dataType: 'json',
            data: {'idcurso': idcurso}
        }).done(function(resp){
            if(resp.code == 'ok'){
                if(resp.data != ''){
                    var lista = '';
                    for(var i = 0; i < resp.data.length;i++){
                        lista = lista.concat("<div><i class='fa fa-circle' aria-hidden='true'> "+resp.data[i]+"</i></div>");
                    }
                    $('#contenedor01').html(lista);
                }
            }else{
                alert(resp.message);
            }
        }).fail(function(xhr,textStatus,errorThrow){
            alert(xhr.responseText);
        });
    }
}
function drawbar(){
    idcurso = $('#select_viewTable').val();
    if(idcurso != 0){
        $.ajax({
            url: _sysUrlBase_ + "/reportealumno/progresocompetenciacursojson",
            type: 'POST',
            dataType: 'json',
            data: {'idcurso': idcurso}
        }).done(function(resp){
            if(resp.code == 'ok'){
                if(resp.data != ''){
                    $('#progress_total').find('.progress-bar').css('width',resp.data+'%').text(resp.data+'%');
                }
            }else{
                alert(resp.message);
            }
        }).fail(function(xhr,textStatus,errorThrow){
            alert(xhr.responseText);
        });
    }
}
$(document).ready(function(){
    drawlist(); drawbar();
    $('#select_viewTable').change(function(){
        drawlist(); drawbar();
    });
});
</script>