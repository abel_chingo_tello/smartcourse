<?php 

defined('RUTA_BASE') or die(); 
$idgui = uniqid();

$url = $this->documento->getUrlBase();

$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$habilidades = !empty($this->examen) ? $this->examen['habilidades'] : 0;
$habilidadesPuntaje = !empty($this->examen) ? $this->examen['habilidad_puntaje'] : 0;

?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: white; font-size: large; }
.text-custom2{font-size: 4em; text-decoration: underline; text-shadow: 1px 4px 5px #969696; }
.text-nota1{font-size: 4em; text-shadow: 1px 4px 1px #272626; color: #43a943;}
.text-nota2{font-size: 7em; font-weight: bold; color: #cc5e5a;}
.col-center{padding:10px 5px; float:none; display:inline-block;  vertical-align:top;}
</style>
<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
    <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
        <li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Reports")?></a></li>
        <li class="active">
          <?php 
              echo JrTexto::_("Placement Test");
          ?>
        </li>
	  </ol>	
	</div>
</div>
<?php endif; ?>

<div class="panel panel-primary">
	<div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("Placement Test")?>
    </div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="height:90px;">
			    <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; "> 
		    </div>
		    <div class="col-md-12">
            	<div class="" style="text-align:center;">
              		<h4><?php echo $this->fullname; ?></h4>
            	</div>
        	</div>
		</div>
		<!--End row panel-->
	</div>
	<!--End panel body-->
</div>
<?php if(empty($this->examen)): ?>
<div class="panel panel-danger">
	<div class="panel-heading">
		<?php echo JrTexto::_("Result")?>
	</div>
	<div class="panel-body" style="text-align: center;">
		<h1><?php echo JrTexto::_('There is not report'); ?></h1>
		<h4><?php echo JrTexto::_('You have not taken the placement test'); ?></h4>
	</div>
</div>

<?php else: ?>

<div class="panel panel-success">
	<div class="panel-heading">
		<?php echo JrTexto::_("Result")?>
	</div>
	<div class="panel-body" style="text-align: center;">
		<div class="row" style="text-align: center;">
			<div class="col-md-6 ">
				<h1><?php echo JrTexto::_('Percentage Rating'); ?></h1>
				<div class="text-nota1">
					<?php echo $this->examen['nota']."%"; ?>
				</div>
			</div>
			<div class="col-md-6 ">
				<h1><?php echo JrTexto::_('Final Score'); ?></h1>
				<div class="text-nota1">
        <?php echo $this->examen['notaFinal']." pts"; ?>
				</div>
			</div>
			<div class="col-md-12">
				<div class="text-custom2"><?php echo JrTexto::_('Level Located'); ?></div>
				<div class="text-nota2"><?php echo $this->examen['nivelUbicado']; ?></div>
			</div>
			<div class="col-md-12">
				<h1><?php echo JrTexto::_('Skills Building'); ?></h1>
				
				<div class="col-sm-3">
	              <div class="Listening-color">
	                <p class="text-custom1">Listening</p>
	                <div id="barListening" class="circleProgressBar1" style="font-size:0.5em;"></div>
	              </div>
	            </div>
	            <div class="col-sm-3 ">
	              <div class="Reading-color">
	                <p class="text-custom1">Reading</p>
	                <div id="barReading" class="circleProgressBar1" style="font-size:0.5em;"></div>
	              </div>
	            </div>
	            <div class="col-sm-3 ">
	              <div class="Writing-color">
	                <p class="text-custom1">Writing</p>
	                <div id="barWriting" class="circleProgressBar1" style="font-size:0.5em;"></div>
	              </div>
	            </div>
	            <div class="col-sm-3 ">
	              <div class="Speaking-color">
	                <p class="text-custom1">Speaking</p>
	                <div id="barSpeaking" class="circleProgressBar1" style="font-size:0.5em;"></div>
	              </div>
	            </div>
			</div>
      <div class="col-md-12" style="margin-top: 10px;">
        <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:80vw">
          <canvas id="piechart"></canvas>
        </div>
      </div>
		</div>

	</div>
</div>

<?php endif; ?>

<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">

var HabilidadesJSON = <?php echo $habilidades ?>;
var HabilidadesPuntajeJSON = <?php echo $habilidadesPuntaje ?>;

/*/funciones */

function drawHabilidad(obj , option = null, value = 0, speed = null){
  var _anim = speed === null ? 1 : speed;
  var option = option === null ? {colorFg:"#FFFFFF",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
  option.animationSpeed = _anim;
  var bar=new RadialProgress(obj,option);
  bar.setValue(value);
  bar.draw(true);
}
var textTranslate = '<?php echo JrTexto::_('Statistics of the skills obtained by the test') ?>';
function drawPie(obj, _data){
  new Chart(obj, {
      type: 'pie',
      data: {
        labels: ["Listening", "Reading", "Writing", "Speaking"],
        datasets: [{
          label: "Habilidades obtenidas por el Examen",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
          data: _data
        }]
      },
      options: {
        title: {
          display: true,
          text: textTranslate
        }
      }
  });
}

console.log(HabilidadesJSON);
console.log(HabilidadesPuntajeJSON);
$('document').ready(function(){
  //Comenzar sintaxis
  if(HabilidadesJSON != 0){

    var datos = new Array();
    datos[0] = 0;
    datos[1] = 0;
    datos[2] = 0;
    datos[3] = 0;
    for(var i in HabilidadesJSON){
      var index = HabilidadesJSON[i].skill_id;
      if(index == 4){
        datos[0] = HabilidadesPuntajeJSON[index];
        drawHabilidad(document.getElementById("barListening"),null, HabilidadesPuntajeJSON[index] / 100 ,0.1);
      }else if(index == 5){
        datos[1] = HabilidadesPuntajeJSON[index];
        drawHabilidad(document.getElementById("barReading"),null, HabilidadesPuntajeJSON[index] / 100,0.1);
      }else if(index == 6){
        datos[2] = HabilidadesPuntajeJSON[index];
        drawHabilidad(document.getElementById("barWriting"),null, HabilidadesPuntajeJSON[index] / 100,0.1);
      }else if(index == 7){
        datos[3] = HabilidadesPuntajeJSON[index];
        drawHabilidad(document.getElementById("barSpeaking"),null, HabilidadesPuntajeJSON[index] / 100,0.1);
      }
    }
    //Chequear que no se ha renderizado un drawHabilidad
    $('.circleProgressBar1').each(function( key, value ){
      if($(this).html().length < 1){
        drawHabilidad(document.getElementById($(this).attr('id')) );
      }
    });
    
    //Dibujar charts piechart
    drawPie(document.getElementById("piechart"),datos);
  }

});

</script>