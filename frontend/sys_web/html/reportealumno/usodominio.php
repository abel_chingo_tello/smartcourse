<?php
defined('RUTA_BASE') or die(); 
$idgui = uniqid();
$url = $this->documento->getUrlBase();
//array to json

$jsonFechas = json_encode($this->fechas);
$jsonTiempos = json_encode($this->tiempos);
$jsonDominio = json_encode($this->dominio);
//$jsonUltimavez = json_encode($this->ultimavez);

?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#85add0;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: white; font-size: large; }
.col-sm-6{ margin:5px auto; }
</style>

<?php if(!$this->isIframe): ?>
<!-- <div style="position:relative;  margin:10px 0;">
    <a href="<?php echo $this->documento->getUrlBase();?>/reportealumno" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
</div> -->
<?php endif; ?>

<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <div class="col-md-12">
    <ol class="breadcrumb">
			<li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
			<li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Reports")?></a></li>
      <li class="active">
				<?php echo JrTexto::_("Use and Management of the Virtual Platform"); ?>
      </li>
    </ol>
  </div>
</div>
<?php endif; ?>

<!--START CONTAINER-->
<div class="">
    <!--START PANEL-->
    <div class = 'panel panel-primary' >
    	<div class="panel-heading" style="text-align:left;">
    		<?php echo JrTexto::_("Report");?>
    	</div>
    	<div class = 'panel-body' style="text-align: center;  " >
            <h3 style="font-weight:bold;"><?php echo JrTexto::_("Use and Management of the Virtual Platform"); ?></h3>
            <div class="row" style="margin:15px auto;">
                <div class="col-md-4" style="/*height:90px;*/">
                    <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                    <h2><?php echo $this->fullname ?></h2>
                    <?php if(isset($this->ultimavez['P'])): ?>
	                    <h4>
											<?php echo JrTexto::_('Last Time on the Platform'); ?>
							<span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['P']['fecha']; ?></span>
	                    </h4>
	                    <h4>
												<?php echo JrTexto::_('Realized Hours'); ?>
	                    	<span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['P']['tiempo']; ?></span>
	                    </h4>
                	<?php endif; ?>
                	<?php if(isset($this->ultimavez['TR'])): ?>
	                    <h4>
											<?php echo JrTexto::_('Last Time in the SmartBook'); ?>
							<span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['TR']['fecha']; ?></span>
	                    </h4>
	                    <h4>
												<?php echo JrTexto::_('Realized Hours'); ?>
	                    	<span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['TR']['tiempo']; ?></span>
	                    </h4>
                	<?php endif; ?>
                	<?php if(isset($this->ultimavez['A'])): ?>
	                    <h4>
												<?php echo JrTexto::_('Last Time in the Exercises'); ?>
							<span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['A']['fecha']; ?></span>
	                    </h4>
	                    <h4>
												<?php echo JrTexto::_('Realized Hours'); ?>
	                    	<span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['A']['tiempo']; ?></span>
	                    </h4>
                	<?php endif; ?>
                	<?php if(isset($this->ultimavez['E'])): ?>
	                    <h4>
												<?php echo JrTexto::_('Last Time on Tests'); ?>
							<span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['E']['fecha']; ?></span>
	                    </h4>
	                    <h4>
												<?php echo JrTexto::_('Realized Hours'); ?>
	                    	<span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['E']['tiempo']; ?></span>
	                    </h4>
                	<?php endif; ?>
                	<?php if(isset($this->ultimavez['T'])): ?>
	                    <h4>
												<?php echo JrTexto::_('Last Time in Activities'); ?>
							<span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['T']['fecha']; ?></span>
	                    </h4>
	                    <h4>
												<?php echo JrTexto::_('Realized Hours'); ?>
	                    	<span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['T']['tiempo']; ?></span>
	                    </h4>
                	<?php endif; ?>

                </div>
                <div class="col-md-8">
                	<h3><?php echo JrTexto::_("Management of The Virtual Platform"); ?></h3>
                	<div class="col-sm-12">
                		<div class="Writing-color">
			                <p class="text-custom1"><?php echo JrTexto::_('Command'); ?></p>
			                <div id="barPlataforma" class="circleProgressBar1" style="font-size:0.8em;"></div>
			             </div>
                	</div>
                	<div class="col-sm-6">
		              <div class="Listening-color">
		                <p class="text-custom1">SmartBook</p>
		                <div id="barSmartBook" class="circleProgressBar1" style="font-size:0.6em;"></div>
		              </div>
		            </div>
		            <div class="col-sm-6">
		              <div class="Reading-color">
		                <p class="text-custom1"><?php echo JrTexto::_('Activity'); ?></p>
		                <div id="barHomeWork" class="circleProgressBar1" style="font-size:0.6em;"></div>
		              </div>
		            </div>
		            <div class="col-sm-6">
		              <div class="Writing-color" style="background-color:#7cd1d1;">
		                <p class="text-custom1"><?php echo JrTexto::_('Sessions'); ?></p>
		                <div id="barActivity" class="circleProgressBar1" style="font-size:0.6em;"></div>
		              </div>
		            </div>
		            <div class="col-sm-6">
		              <div class="Speaking-color">
		                <p class="text-custom1"><?php echo JrTexto::_('Test'); ?></p>
		                <div id="barSmartQuiz" class="circleProgressBar1" style="font-size:0.6em;"></div>
		              </div>
		            </div>

                </div>
                <!--end top-->
                <div class="col-md-12">
                	<h3><?php echo JrTexto::_("Platform Usage"); ?></h3>
					<canvas id="chart1" width="1000" height="300" class="chartjs-render-monitor" style="display: block;"></canvas>
                </div>
            </div>
            <!--end row-->
        </div>
        <!--END PANEL BODY-->
    </div>
    <!--END PANEL-->
</div>
<!--END CONTAINER-->

<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">
	var jsonFechas = <?php echo $jsonFechas?>;
	var jsonTiempos = <?php echo $jsonTiempos?>;
	var jsonDominio = <?php echo $jsonDominio ?>;

	function drawHabilidad(obj , option = null, value = 0, speed = null){
    var _anim = speed === null ? 1 : speed;
    var option = option === null ? {colorFg:"#FFFFFF",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
    option.animationSpeed = _anim;
    var bar=new RadialProgress(obj,option);
    bar.setValue(value);
    bar.draw(true);
  }

	function drawTimeline(ctx,data){
	    var myPieChart = new Chart(ctx,{
	        type: 'bar',
	        data: data,
	        options: {
				scales: {
					xAxes: [{
						type: 'time',
						distribution: 'series',
						ticks: {
							source: 'labels'
						}
					}],
					yAxes: [{
						scaleLabel: {
							display: true,
							labelString: 'Hours on the virtual platform (h.min)'
						}
					}]
				}
			}
	    });
	  }

	$('document').ready(function(){
		var obj = document.getElementById('chart1').getContext('2d');
		var labels = new Array();
		var _data = new Array();

		for(var i in jsonFechas){
			if(jsonFechas[i].fecha){
				labels.push(jsonFechas[i].fecha);
				if(jsonTiempos[jsonFechas[i].fecha]){
					_data.push(jsonTiempos[jsonFechas[i].fecha]);
				}
			}
		}//end for in
		//console.log(labels);
		//console.log(_data);

		var data = {
				labels: labels,
				datasets: [{
					label: 'Use of the platform',
					data: _data,
					type: 'line',
					borderColor:"rgb(75, 192, 192)",
					pointRadius: 0.1,
					fill: false,
					lineTension: 0.1,
					borderWidth: 2
				}]
			};

		drawTimeline(obj,data);

		//draw el dominio en la plataforma virtual
		var calcular = function(valor){
			return (valor == 0) ? 0 : (valor / 100);
		};

		drawHabilidad(document.getElementById("barPlataforma"),null, calcular(jsonDominio.dominio),0.15);
		drawHabilidad(document.getElementById("barSmartBook"),null, calcular(jsonDominio.smartbook),0.15);
		drawHabilidad(document.getElementById("barHomeWork"),null, calcular(jsonDominio.homework),0.15);
		drawHabilidad(document.getElementById("barActivity"),null, calcular(jsonDominio.activity),0.15);
		drawHabilidad(document.getElementById("barSmartQuiz"),null, calcular(jsonDominio.smarquiz),0.15);
	});
</script>