<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Sinlogin_quiz"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">            
                                                                                                                                                                                                                                                                                                                  
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>           
          </div>
        </div>
    </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idnota") ;?></th>
                    <th><?php echo JrTexto::_("Idexamen") ;?></th>
                    <th><?php echo JrTexto::_("Tipo") ;?></th>
                    <th><?php echo JrTexto::_("Nota") ;?></th>
                    <th><?php echo JrTexto::_("Notatexto") ;?></th>
                    <th><?php echo JrTexto::_("Regfecha") ;?></th>
                    <th><?php echo JrTexto::_("Idproyecto") ;?></th>
                    <th><?php echo JrTexto::_("Calificacion en") ;?></th>
                    <th><?php echo JrTexto::_("Calificacion total") ;?></th>
                    <th><?php echo JrTexto::_("Calificacion min") ;?></th>
                    <th><?php echo JrTexto::_("Tiempo total") ;?></th>
                    <th><?php echo JrTexto::_("Tiempo realizado") ;?></th>
                    <th><?php echo JrTexto::_("Calificacion") ;?></th>
                    <th><?php echo JrTexto::_("Habilidades") ;?></th>
                    <th><?php echo JrTexto::_("Habilidad puntaje") ;?></th>
                    <th><?php echo JrTexto::_("Intento") ;?></th>
                    <th><?php echo JrTexto::_("Datos") ;?></th>
                    <th><?php echo JrTexto::_("Fechamodificacion") ;?></th>
                    <th><?php echo JrTexto::_("Preguntas") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
            </tbody>
            </table>
        </div>
    </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="sinlogin_quiz" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idpersona" id="idpersona" value="">
          <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idnota');?> <span class="required"> * </span></label>
                <input type="text"  id="idnota" name="idnota" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idexamen');?> <span class="required"> * </span></label>
                <input type="text"  id="idexamen" name="idexamen" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Tipo');?> <span class="required"> * </span></label>
                <input type="text"  id="tipo" name="tipo" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Nota');?> <span class="required"> * </span></label>
                <input type="text"  id="nota" name="nota" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Notatexto');?> <span class="required"> * </span></label>
                <input type="text"  id="notatexto" name="notatexto" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Regfecha');?> <span class="required"> * </span></label>
                <input type="text"  id="regfecha" name="regfecha" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idproyecto');?> <span class="required"> * </span></label>
                <input type="text"  id="idproyecto" name="idproyecto" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Calificacion en');?> <span class="required"> * </span></label>
                <input type="text"  id="calificacion_en" name="calificacion_en" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Calificacion total');?> <span class="required"> * </span></label>
                <input type="text"  id="calificacion_total" name="calificacion_total" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Calificacion min');?> <span class="required"> * </span></label>
                <input type="text"  id="calificacion_min" name="calificacion_min" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Tiempo total');?> <span class="required"> * </span></label>
                <input type="text"  id="tiempo_total" name="tiempo_total" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Tiempo realizado');?> <span class="required"> * </span></label>
                <input type="text"  id="tiempo_realizado" name="tiempo_realizado" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Calificacion');?> <span class="required"> * </span></label>
                <input type="text"  id="calificacion" name="calificacion" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Habilidades');?> <span class="required"> * </span></label>
                <input type="text"  id="habilidades" name="habilidades" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Habilidad puntaje');?> <span class="required"> * </span></label>
                <input type="text"  id="habilidad_puntaje" name="habilidad_puntaje" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Intento');?> <span class="required"> * </span></label>
                <input type="text"  id="intento" name="intento" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Datos');?> <span class="required"> * </span></label>
                <input type="text"  id="datos" name="datos" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Fechamodificacion');?> <span class="required"> * </span></label>
                <input type="text"  id="fechamodificacion" name="fechamodificacion" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Preguntas');?> <span class="required"> * </span></label>
                <input type="text"  id="preguntas" name="preguntas" required="required" class="form-control" value="">
                          </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="sinlogin_quiz" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos60021b8b6ed48='';
function refreshdatos60021b8b6ed48(){
    tabledatos60021b8b6ed48.ajax.reload();
}
$(document).ready(function(){  
  var estados60021b8b6ed48={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit60021b8b6ed48='<?php echo ucfirst(JrTexto::_("sinlogin_quiz"))." - ".JrTexto::_("edit"); ?>';
  var draw60021b8b6ed48=0;
  var _imgdefecto='';
  var ventana_60021b8b6ed48=$('#ventana_<?php echo $idgui; ?>');
   tabledatos60021b8b6ed48=ventana_60021b8b6ed48.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/sinlogin_quiz',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw60021b8b6ed48=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw60021b8b6ed48;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push([            
              (i+1),
              data[i].idnota,data[i].idexamen,data[i].tipo,data[i].nota,data[i].notatexto,data[i].regfecha,data[i].idproyecto,data[i].calificacion_en,data[i].calificacion_total,data[i].calificacion_min,data[i].tiempo_total,data[i].tiempo_realizado,data[i].calificacion,data[i].habilidades,data[i].habilidad_puntaje,data[i].intento,data[i].datos,data[i].fechamodificacion,data[i].preguntas,              //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idpersona+'" data-titulo="'+tituloedit60021b8b6ed48+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idpersona+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_60021b8b6ed48.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos60021b8b6ed48();
  }).on('blur','.textosearchlist',function(ev){refreshdatos60021b8b6ed48();
  })

  .on('click','.btnbuscar',function(ev){ refreshdatos60021b8b6ed48();
  })
 

  ventana_60021b8b6ed48.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idpersona',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/sinlogin_quiz/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos60021b8b6ed48.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/sinlogin_quiz/setcampo','masvalores':{idpersona:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idpersona',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/sinlogin_quiz/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos60021b8b6ed48.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modalsinlogin_quiz(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modalsinlogin_quiz(el);
  })

  /*Formulario*/
  var __modalsinlogin_quiz=function(el){
    var titulo = '<?php echo JrTexto::_("Sinlogin_quiz"); ?>';
    var filtros=$('#ventana_<?php echo $idgui; ?>');
    var frm=$('#frm<?php echo $idgui; ?>').clone();     
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/sinlogin_quiz/guardar',
          showmsjok:true,
          callback:function(rs){   tabledatos60021b8b6ed48.ajax.reload(); __cerrarmodal(_md);  }
        });
    })

    var pk=el.attr('pk')||'';
    if(pk!=''){      
      var data=new FormData();
      data.append('idpersona',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/sinlogin_quiz',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];  
            //_md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());          
          }
         }
      });
    }else{
      /*if(filtros.find('select#tipo_portal').val()!='')
        _md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());*/
    }
  }
});
</script>