<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Matriculas"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> <?=JrTexto::_("Add")?>',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
  label{ margin-top: .5rem;  margin-bottom: 1px; }
</style>

<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
 <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <div class="row" id="addselect"> 
           <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?=JrTexto::_("Level")?>/<?=JrTexto::_("Career")?>/<?=JrTexto::_("Category")?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idcategoria" name="idcategoria"  class=" form-control selchangecat pnlidcategoria0">
                  <option value="0"><?=JrTexto::_("Unique")?></option>
                  <?php if(!empty($this->categorias))
                  foreach ($this->categorias as $k=>$v){ if($v["idpadre"]==0){?>
                    <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                  <?php } } ?>                
              </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat" style="display:none">   
              <label><?=JrTexto::_("Sub Level")?> / <?=JrTexto::_("Module")?> /<?=JrTexto::_("Sub Category")?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="nomsel" name="nomsel" class=" form-control selchangecat pnlidcategoria1">
                  <option value="0"><?=JrTexto::_("Unique")?></option>
                  <?php if(!empty($this->categorias))
                  foreach ($this->categorias as $k=>$v){ if($v["idpadre"]!=0){?>
                    <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                  <?php } } ?>                
              </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?php echo JrTexto::_('Classroom groups') ?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idgrupoaula" name="idgrupoaula"  class=" form-control">
                <option value=""><?php echo JrTexto::_('All'); ?></option>
              </select>
              </div>
            </div>
           
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?php echo JrTexto::_('Grade') ?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idgrado" name="idgrado"  class=" form-control">
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                <option value="0"><?php echo JrTexto::_('Only Grade'); ?></option>
                  <?php 
                  if(!empty($this->grados))
                  foreach ($this->grados as $r) { ?><option value="<?php echo $r["idgrado"]?>" ><?php echo JrTexto::_('Grade')." ".JrTexto::_($r["abrev"])." : ".JrTexto::_($r["descripcion"]) ?></option>
                   <?php } ?>              
                </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?php echo JrTexto::_('Section') ?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idsesion" name="idsesion"  class=" form-control">
                <option value=""><?php echo JrTexto::_('All'); ?></option>
              <option value="0"><?php echo JrTexto::_('Only Section'); ?></option>
                <?php 
                if(!empty($this->seccion))
                foreach ($this->seccion as $s) { ?><option value="<?php echo $s["idsesion"]?>" ><?php echo JrTexto::_('Section')." ".$s["descripcion"] ?></option>
                 <?php } ?>              
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <label><?php echo  ucfirst(JrTexto::_("text to search"))?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>  
            <div class="clearfix"></div> 
            </div>         
        </div>
        <div class="x_content">
           <div class="row">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr class="headings">
                    <th>#</th>                             
                    <th><?php echo JrTexto::_("Student"); ?></th>
                    <th><?php echo JrTexto::_("Course"); ?></th>
                    <th><?php echo JrTexto::_("User Data"); ?></th>
                    <th><?php echo JrTexto::_("Photo"); ?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>  
    </div>
  </div>
</div>




<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_matricula" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idmatricula" id="idmatricula" value="">
          <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idgrupoauladetalle');?> <span class="required"> * </span></label>
               <select id="idgrupoauladetalle" name="idgrupoauladetalle" class="form-control" required>
              <option value=""><?php JrTexto::_("Select");?></option>
                              <option value="1"><?php echo JrTexto::_('Active');?></option>
              <option value="0"><?php echo JrTexto::_('Inactive');?></option>
                                   
              </select>
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idalumno');?> <span class="required"> * </span></label>
               <select id="idalumno" name="idalumno" class="form-control" required>
              <option value=""><?php JrTexto::_("Select");?></option>
                              <option value="1"><?php echo JrTexto::_('Active');?></option>
              <option value="0"><?php echo JrTexto::_('Inactive');?></option>
                                   
              </select>
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Registration date');?> <span class="required"> * </span></label>
                <input name="fecha_registro" id="fecha_registro" class="verdatetime form-control" required="required" value="">   
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Status');?> <span class="required"> * </span></label>
                                <a style="cursor:pointer;" class="chkformulario fa fa-check-circle-o" data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Active":"Inactive");?></span>
                 <input type="hidden" name="estado" id="estado"  value="" > 
                 </a>
                                              </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idusuario');?> <span class="required"> * </span></label>
                <input type="text"  id="idusuario" name="idusuario" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Fecha matricula');?> <span class="required"> * </span></label>
                <input name="fecha_matricula" id="fecha_matricula" class="verdatetime form-control" required="required" value="">   
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Expiration date');?> <span class="required"> * </span></label>
                <input name="fecha_vencimiento" id="fecha_vencimiento" class="verdatetime form-control" required="required" value="">   
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Notafinal');?> <span class="required"> * </span></label>
                <input type="text"  id="notafinal" name="notafinal" required="required" class="form-control" value="">
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Formulajson');?> <span class="required"> * </span></label>
               <textarea id="formulajson" name="formulajson" class="form-control" ></textarea>
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Fecha termino');?> <span class="required"> * </span></label>
                <input name="fecha_termino" id="fecha_termino" class="verdate form-control" required="required" type="date" value="<?php echo date('Y-m-d') ?>">   
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Pagorecurso');?> <span class="required"> * </span></label>
                                <a style="cursor:pointer;" class="chkformulario fa fa-check-circle-o" data-value="<?php echo @$frm["pagorecurso"];?>"  data-valueno="0" data-value2="<?php echo @$frm["pagorecurso"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["pagorecurso"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="pagorecurso" id="pagorecurso"  value="" > 
                 </a>
                                              </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Tipomatricula');?> <span class="required"> * </span></label>
               <select id="tipomatricula" name="tipomatricula" class="form-control" required>
              <option value=""><?php JrTexto::_("Select");?></option>
                              <option value="1"><?php echo JrTexto::_('Active');?></option>
              <option value="0"><?php echo JrTexto::_('Inactive');?></option>
                                   
              </select>
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idcomplementario');?> <span class="required"> * </span></label>
               <select id="idcomplementario" name="idcomplementario" class="form-control" required>
              <option value=""><?php JrTexto::_("Select");?></option>
                              <option value="1"><?php echo JrTexto::_('Active');?></option>
              <option value="0"><?php echo JrTexto::_('Inactive');?></option>
                                   
              </select>
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idcategoria');?> <span class="required"> * </span></label>
               <select id="idcategoria" name="idcategoria" class="form-control" required>
              <option value=""><?php JrTexto::_("Select");?></option>
                              <option value="1"><?php echo JrTexto::_('Active');?></option>
              <option value="0"><?php echo JrTexto::_('Inactive');?></option>
                                   
              </select>
                          </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="acad_matricula" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5eb4f651e1bc1='';
function refreshdatos5eb4f651e1bc1(){
    tabledatos5eb4f651e1bc1.ajax.reload();
}
$(document).ready(function(){  
  var estados5eb4f651e1bc1={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5eb4f651e1bc1='<?php echo ucfirst(JrTexto::_("acad_matricula"))." - ".JrTexto::_("edit"); ?>';
  var categorias=<?php echo !empty($this->categorias)?json_encode($this->categorias):'[]'; ?>;
  var draw5eb4f651e1bc1=0;
  var _imgdefecto='';

  var addcategoria=function(sel,enpln){
    var idpadre=sel.val();
    if(sel.hasClass('pnlidcategoria0')){ 
      if(idpadre==0){
        enpln.find('select.pnlidcategoria1').closest('.pnlcat').hide();
        enpln.find('select.pnlidcategoria1').attr('id','nomsel').attr('name','nomsel');
        enpln.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria');
      }else{   
        enpln.find('select.pnlidcategoria1').closest('.pnlcat').show();
        enpln.find('select.pnlidcategoria1').children('option').remove();
        $.each(categorias,function(i,v){
          if(v.idpadre==idpadre){
            enpln.find('select.pnlidcategoria1').append('<option value="'+v.idcategoria+'">'+v.nombre+'</option>')
          }
        })
        enpln.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria');
        enpln.find('select.pnlidcategoria0').attr('id','nomsel').attr('name','nomsel');
      }
    }
    console.log(enpln);
    if(enpln.attr('id')=='addselect') _addgrupos(enpln);
  }

  var _addgrupos=function(enpln){
    var data=new FormData()
    data.append('idcategoria',enpln.find('idcategoria').val());        
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_grupoaula',
        callback:function(rs){
          enpln.find('select#idgrupoaula').children('option').first().nexAll().remove();
          if(rs.code==200){
            console.log(rs);
            $.each(rs.data,function(i,e){
              enpln.find('select#idgrupoaula').append('<option value="'+e.idgrupoaula+'">'+e.nombre+'</option>');
            })
          }
        }
    });

  }


 
  var ventana_5eb4f651e1bc1=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5eb4f651e1bc1=ventana_5eb4f651e1bc1.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/acad_matricula',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.idgrupoauladetalle=$('#cbidgrupoauladetalle').val(),
             d.idalumno=$('#cbidalumno').val(),
             d.fecha_registro=$('#datefecha_registro').val(),
             d.estado=$('#cbestado').val(),
             d.fecha_matricula=$('#datefecha_matricula').val(),
             d.fecha_vencimiento=$('#datefecha_vencimiento').val(),
             d.fecha_termino=$('#datefecha_termino').val(),
             d.pagorecurso=$('#cbpagorecurso').val(),
             d.tipomatricula=$('#cbtipomatricula').val(),
             d.idcomplementario=$('#cbidcomplementario').val(),
             d.idcategoria=$('#cbidcategoria').val(),
             //d.texto=$('#texto').val(),
                        
            draw5eb4f651e1bc1=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5eb4f651e1bc1;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push([            
              (i+1),
              
                '<a href="javascript:;"  class="btn-activar  '+(data[i].idgrupoauladetalle==0?'':'active')+' " data-campo="idgrupoauladetalle"  data-id="'+data[i].idmatricula+'"> <i class="fa fa'+(data[i].idgrupoauladetalle=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eb4f651e1bc1[data[i].idgrupoauladetalle]+'</a>',
          
                '<a href="javascript:;"  class="btn-activar  '+(data[i].idalumno==0?'':'active')+' " data-campo="idalumno"  data-id="'+data[i].idmatricula+'"> <i class="fa fa'+(data[i].idalumno=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eb4f651e1bc1[data[i].idalumno]+'</a>',
          data[i].fecha_registro,
                '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idmatricula+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eb4f651e1bc1[data[i].estado]+'</a>',
          data[i].idusuario,data[i].fecha_matricula,data[i].fecha_vencimiento,data[i].notafinal,data[i].formulajson,data[i].fecha_termino,
                '<a href="javascript:;"  class="btn-activar  '+(data[i].pagorecurso==0?'':'active')+' " data-campo="pagorecurso"  data-id="'+data[i].idmatricula+'"> <i class="fa fa'+(data[i].pagorecurso=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eb4f651e1bc1[data[i].pagorecurso]+'</a>',
          
                '<a href="javascript:;"  class="btn-activar  '+(data[i].tipomatricula==0?'':'active')+' " data-campo="tipomatricula"  data-id="'+data[i].idmatricula+'"> <i class="fa fa'+(data[i].tipomatricula=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eb4f651e1bc1[data[i].tipomatricula]+'</a>',
          
                '<a href="javascript:;"  class="btn-activar  '+(data[i].idcomplementario==0?'':'active')+' " data-campo="idcomplementario"  data-id="'+data[i].idmatricula+'"> <i class="fa fa'+(data[i].idcomplementario=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eb4f651e1bc1[data[i].idcomplementario]+'</a>',
          
                '<a href="javascript:;"  class="btn-activar  '+(data[i].idcategoria==0?'':'active')+' " data-campo="idcategoria"  data-id="'+data[i].idmatricula+'"> <i class="fa fa'+(data[i].idcategoria=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eb4f651e1bc1[data[i].idcategoria]+'</a>',
                        //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idmatricula+'" data-titulo="'+tituloedit5eb4f651e1bc1+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idmatricula+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });
   addcategoria($('#addselect'))
  ventana_5eb4f651e1bc1.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5eb4f651e1bc1();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5eb4f651e1bc1();
  }).on('change','#idcategoria , .selchangecat',function(ev){addcategoria($(this),$('#addselect'));

  }).on('change','#idgrupoauladetalle',function(ev){refreshdatos5eb4f651e1bc1();
  }).on('change','#idalumno',function(ev){refreshdatos5eb4f651e1bc1();
  }).on('change','#datefecha_registro',function(ev){ refreshdatos5eb4f651e1bc1();
  }).on('change','#estado',function(ev){refreshdatos5eb4f651e1bc1();
  }).on('change','#datefecha_matricula',function(ev){ refreshdatos5eb4f651e1bc1();
  }).on('change','#datefecha_vencimiento',function(ev){ refreshdatos5eb4f651e1bc1();
  }).on('change','#datefecha_termino',function(ev){ refreshdatos5eb4f651e1bc1();
  }).on('change','#pagorecurso',function(ev){refreshdatos5eb4f651e1bc1();
  }).on('change','#tipomatricula',function(ev){refreshdatos5eb4f651e1bc1();
  }).on('change','#idcomplementario',function(ev){refreshdatos5eb4f651e1bc1();
  }).on('change','#idcategoria',function(ev){refreshdatos5eb4f651e1bc1();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5eb4f651e1bc1();
  })
 

  ventana_5eb4f651e1bc1.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idmatricula',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/acad_matricula/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5eb4f651e1bc1.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/acad_matricula/setcampo','masvalores':{idmatricula:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idmatricula',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_matricula/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5eb4f651e1bc1.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modalacad_matricula(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modalacad_matricula(el);
  })

  /*Formulario*/
  var __modalacad_matricula=function(el){
    var titulo = '<?php echo JrTexto::_("Acad_matricula"); ?>';
    var filtros=$('#ventana_<?php echo $idgui; ?>');
    var frm=$('#frm<?php echo $idgui; ?>').clone();     
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/acad_matricula/guardar',
          showmsjok:true,
          callback:function(rs){   tabledatos5eb4f651e1bc1.ajax.reload(); __cerrarmodal(_md);  }
        });
    })

    var pk=el.attr('pk')||'';
    if(pk!=''){      
      var data=new FormData();
      data.append('idmatricula',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/acad_matricula',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];  
            //_md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());          
          }
         }
      });
    }else{
      /*if(filtros.find('select#tipo_portal').val()!='')
        _md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());*/
    }
  }
});
</script>