<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Cursos de grupos aula"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> Agregar curso',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
  label{ margin-top: .5rem;  margin-bottom: 1px; }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>">
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <div class="row" id="addselect">            
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?=JrTexto::_("Level")?>/<?=JrTexto::_("Career")?>/<?=JrTexto::_("Category")?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idcategoria" name="idcategoria"  class=" form-control selchangecat pnlidcategoria0">
                  <option value="0"><?=JrTexto::_("Unique")?></option>
                  <?php if(!empty($this->categorias))
                  foreach ($this->categorias as $k=>$v){ if($v["idpadre"]==0){?>
                    <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                  <?php } } ?>                
              </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat" style="display:none">   
              <label><?=JrTexto::_("Sub level")?> / <?=JrTexto::_("Module")?> /<?=JrTexto::_("Sub Category")?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="nomsel" name="nomsel" class=" form-control selchangecat pnlidcategoria1">
                  <option value="0"><?=JrTexto::_("Unique")?></option>
                  <?php if(!empty($this->categorias))
                  foreach ($this->categorias as $k=>$v){ if($v["idpadre"]==0){?>
                    <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                  <?php } } ?>                
              </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?php echo JrTexto::_('Classroom groups') ?></label>         
              <div class="select-ctrl-wrapper select-azul">
                <select id="idgrupoaula" name="idgrupoaula"  class=" form-control">                
                  <option value="-1"><?php echo JrTexto::_('None'); ?></option>
                </select>
              </div>
            </div>
           
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <label><?php echo  ucfirst(JrTexto::_("text to search"))?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr class="headings">
                    <th>#</th>                             
                    <th><?php echo JrTexto::_("Course"); ?></th>
                    <th><?php echo JrTexto::_("Teacher"); ?></th>
                    <th><?php echo JrTexto::_("Local"); ?></th>
                    <th><?php echo JrTexto::_("Ambient"); ?></th>
                    <th><?php echo JrTexto::_("Date"); ?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>     
    </div>
  </div>
</div>

<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoauladetalle" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idgrupoauladetalle" id="idgrupoauladetalle" value="">
          <input type="hidden" name="idgrupoaula" id="idgrupoaula" value="">
          <input type="hidden" name="idcategoria" id="idcategoria" value="">
          <div class="form-group col-md-12 ">
            <label><?php echo  ucfirst(JrTexto::_("Course"))?></label>
            <div class="input-group" style="margin-bottom: -2px;">
              <input type="hidden" name="idcomplementario" id="idcomplementario" value="0" >           
              <input type="text" name="idcurso" id="idcurso" class="disabled form-control border0" disabled placeholder="ID curso" style="max-width: 150px">
              <input type="text" name="nomcurso" id="nomcurso" disabled class="disabled form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Teacher Name"))?>">
              <span class="input-group-addon btn btnbuscarcurso"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
            </div>        
          </div>

          <div class="form-group col-md-12">
            <label><?php echo  ucfirst(JrTexto::_("Teacher"))?></label>
            <div class="input-group" style="margin-bottom: -2px;">
              <input type="hidden" name="iddocente" id="iddocente" value="" >
              <input type="text" name="dni" id="dni" class="form-control border0" value="" placeholder="<?php echo  ucfirst(JrTexto::_("N° Doc."))?>" style="max-width: 150px">
              <input type="text" name="personanombre" id="personanombre" disabled class="disabled form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Teacher Name"))?>">
              <span class="input-group-addon btn btnbuscarteacher" ><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
            </div>
            <small id="emailHelp" class="form-text text-muted" style="color: #9a9fa3!important;"><?php echo JrTexto::_('Enter N° Doc and enter to search, or press the search button to see the list of teachers'); ?></small>
          </div>
          <div class="form-group col-md-6 col-sm-12">
            <label class="control-label"><?php echo JrTexto::_('Local');?> <span class="required"> * </span></label>
             <select id="idlocal" name="idlocal" class="form-control" required>
              <option value="0"><?php echo JrTexto::_('Only') ?></option>
              <?php if(!empty($this->locales))
              foreach ($this->locales as $k=>$v){ ?>
                <option value="<?php echo $v["idlocal"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
              <?php } ?> 
            </select>
          </div>

          <div class="form-group col-md-6 col-sm-12">
            <label class="control-label"><?php echo JrTexto::_('Ambient');?> <span class="required"> * </span></label>
             <select id="idambiente" name="idambiente" class="form-control" required>
              <option value="0"><?php echo JrTexto::_('Only') ?></option>
              <?php if(!empty($this->ambientes))
              foreach ($this->ambientes as $k=>$v){ ?>
                <option value="<?php echo $v["idambiente"]; ?>" idlocal="<?php echo $v["idlocal"]; ?>" ><?php echo ucfirst($v["numero"]); ?></option>
              <?php } ?>                                 
            </select>
          </div>

          <div class="form-group col-md-6 col-sm-12">
            <label class="control-label"><?php echo JrTexto::_('Date first');?> <span class="required"> * </span></label>
            <input name="fecha_inicio" id="fecha_inicio" class="verdate form-control" required="required" type="date" value="<?php echo date('Y-m-d') ?>">
          </div>



          <div class="form-group col-md-6 col-sm-12">
            <label class="control-label"><?php echo JrTexto::_('Date finish');?> <span class="required"> * </span></label>
            <input name="fecha_final" id="fecha_final" class="verdate form-control" required="required" type="date" value="<?php echo date('Y-m-d',strtotime('+1 month',strtotime(date('Y-m-d')))); ?>">   
          </div>

          <div class="clearfix"></div>            
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="acad_grupoauladetalle" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5eacec4c98551='';
function refreshdatos5eacec4c98551(){
    tabledatos5eacec4c98551.ajax.reload();
}
$(document).ready(function(){  
  var estados5eacec4c98551={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5eacec4c98551='<?php echo ucfirst(JrTexto::_("Courses by group"))." - ".JrTexto::_("edit"); ?>';
  var categorias=<?php echo !empty($this->categorias)?json_encode($this->categorias):'[]'; ?>;
  var draw5eacec4c98551=0;
  var _imgdefecto='';

  var addcategoria=function(sel,enpln){
        var idpadre=sel.val();
        if(sel.hasClass('pnlidcategoria0')){ 
          if(idpadre==0){
            enpln.find('select.pnlidcategoria1').closest('.pnlcat').hide();
            enpln.find('select.pnlidcategoria1').attr('id','nomsel').attr('name','nomsel');
            enpln.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria');
          }else{   
            enpln.find('select.pnlidcategoria1').closest('.pnlcat').show();
            enpln.find('select.pnlidcategoria1').children('option').remove();
            $.each(categorias,function(i,v){
              if(v.idpadre==idpadre){
                enpln.find('select.pnlidcategoria1').append('<option value="'+v.idcategoria+'">'+v.nombre+'</option>')
              }
            })
            enpln.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria');
            enpln.find('select.pnlidcategoria0').attr('id','nomsel').attr('name','nomsel');
          }
        }
        if(enpln.attr('id')=='addselect') gruposaula(enpln);
  }
  var gruposaula=function(enpln){
    var idcat=enpln.find('select#idcategoria').val();
    enpln.find('select#idgrupoaula').children('option').remove();
    var data=new FormData();
    data.append('idcategoria',idcat);
    __sysAyax({ 
      fromdata:data,
      url:_sysUrlBase_+'json/acad_grupoaula',
      callback:function(rs){ 
        if(rs.data.length>0){
          $.each(rs.data,function(i,v){
            var vtmp=btoa(JSON.stringify(v));
            enpln.find('select#idgrupoaula').append('<option value="'+v.idgrupoaula+'" json="'+vtmp+'">'+v.nombre+'</option>');
          })          
        }else{
          enpln.find('select#idgrupoaula').append('<option value="-1" json="">Ninguno</option>');
        }
        if(enpln.attr('id')=='addselect')  enpln.find('select#idgrupoaula').trigger('change');
       }
    });
  }

  var ventana_5eacec4c98551=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5eacec4c98551=ventana_5eacec4c98551.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/acad_grupoauladetalle',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.idgrupoaula=$('#idgrupoaula').val();
             // d.iddocente=$('#iddocente').val();
             // d.idlocal=$('#idlocal').val();
             // d.idambiente=$('#idambiente').val();
             // d.fecha_inicio=$('#datefecha_inicio').val();
             // d.fecha_final=$('#datefecha_final').val();
           
             //d.idcomplementario=$('#cbidcomplementario').val();
             d.texto=$('#texto').val();                        
             draw5eacec4c98551=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5eacec4c98551;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push([            
              (i+1),  data[i].strcurso , data[i].strdocente,             
                data[i].strlocal,  data[i].strambiente, data[i].fecha_inicio+' -- '+data[i].fecha_final,

              ' <a class="btn-matriculas btn btn-xs" href="javascript:;" data-id="'+data[i].idgrupoaula+'" ><i class="fa fa-users"></i></a> <a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idgrupoauladetalle+'" data-titulo="'+tituloedit5eacec4c98551+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idgrupoauladetalle+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5eacec4c98551.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5eacec4c98551();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5eacec4c98551();
  }).on('change','#idcurso',function(ev){refreshdatos5eacec4c98551(); 
  }).on('change','#idambiente',function(ev){refreshdatos5eacec4c98551();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5eacec4c98551();
  }).on('change','#idcategoria , .selchangecat ',function(ev){addcategoria($(this),ventana_5eacec4c98551.find('#addselect'));
  }).on('change','#idgrupoaula',function(ev){
    try{
      var op=$(this).find('option[value="'+$(this).val()+'"]');
      if(op.length >0 ){
        var json=op.attr('json')||''; 
        if(json!='') {
          json=JSON.parse(atob(json));
        }
        console.log(json);
      }
      refreshdatos5eacec4c98551();
    }catch(ex){console.log(ex)}

  })
  addcategoria($(this),ventana_5eacec4c98551.find('#addselect'));

  ventana_5eacec4c98551.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idgrupoauladetalle',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/acad_grupoauladetalle/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5eacec4c98551.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/acad_grupoauladetalle/setcampo','masvalores':{idgrupoauladetalle:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idgrupoauladetalle',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_grupoauladetalle/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5eacec4c98551.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modalacad_grupoauladetalle(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modalacad_grupoauladetalle(el);
  })

  /*Formulario*/
  var __modalacad_grupoauladetalle=function(el){
    var titulo = '<?php echo JrTexto::_("Asignacion de curso"); ?>';
    var filtros=$('#ventana_<?php echo $idgui;?>');
    var pk=el.attr('pk')||'';
    if(el.pk==''){
      var selidgrupoaula=filtros.find('select#idgrupoaula')
      if(selidgrupoaula.val()=='') {
        alert('Nesecita seleccionar un grupo aula'); return -1;
      }
    }
    var frm=$('#frm<?php echo $idgui; ?>').clone();     
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('change','select#idlocal',function(ev){
      _md.find('select#idambiente option').first().nextAll().hide();
      _md.find('select#idambiente option[idlocal="'+$(this).val()+'"]').show();
    }).on('blur','input#dni',function(ev){
      var data=new FormData();
      var dni=$(this).val()||'';
      if(dni!='')data.append('dni', dni); 
      else return -1;
      data.append('sqlsolopersona',true);
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/persona',             
          callback:function(rs){
            if(rs.code==200){
              if(rs.data.length==0) return;
              rw=rs.data[0];
              _md.find('input#iddocente').val(rw.idpersona);
              _md.find('input#dni').val(rw.dni);
              _md.find('input#personanombre').val(rw.ape_paterno+' '+rw.ape_materno+', '+rw.nombre);           
          } 
        }
       });
    }).on('click','span.btnbuscarcurso',function(ev){
      ev.preventDefault();
      var _this=$(this); 
      var url=_sysUrlBase_+'json/acad_curso/buscar/?plt=modal';
      var titulo='Buscar curso';
      var _md2=__sysmodal({'url':url,'titulo':titulo});
      var refresh='refresh'+__idgui();
      //_this.addClass(refresh);
      _md2.on(refresh, function(ev){
        var _dr=_this.parent();      
        var id=_md2.attr('idpersona');
        _dr.find('#idpersona').val(id);
        var dni=_md2.attr('dni');
        _dr.find('#dni').val(dni);
        var nombre=_md2.attr('nombre');
        _dr.find('#personanombre').val(nombre);
        __cerrarmodal(_md2);
      })
      _md2.on('shown.bs.modal',function(){
        var formventana=_md2.find('.formventana');
        formventana.attr('datareturn',refresh).addClass('ismodaljs');
          if(_md2.find('#cargarscript').length)
          $.getScript(_md2.find('#cargarscript').attr('src'),function(){frmpersonabuscar(_md2);});
      })
    }).on('click','span.btnbuscarteacher',function(ev){
      ev.preventDefault();
      var _this=$(this); 
      var url=_sysUrlBase_+'personal/buscar/?plt=modal&idrol=2';   
      var titulo='Buscar Docente';
      var _md2=__sysmodal({'url':url,'titulo':titulo});
      var refresh='refresh'+__idgui();
      //_this.addClass(refresh);
      _md2.on(refresh, function(ev){
        var _dr=_this.parent();      
        var id=_md2.attr('idpersona');
        _dr.find('#idpersona').val(id);
        var dni=_md2.attr('dni');
        _dr.find('#dni').val(dni);
        var nombre=_md2.attr('nombre');
        _dr.find('#personanombre').val(nombre);
        __cerrarmodal(_md2);  /**/      
      })
      _md2.on('shown.bs.modal',function(){
        var formventana=_md2.find('.formventana');
        formventana.attr('datareturn',refresh).addClass('ismodaljs');
          if(_md2.find('#cargarscript').length)
          $.getScript(_md2.find('#cargarscript').attr('src'),function(){frmpersonabuscar(_md2);});
      })
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/acad_grupoauladetalle/guardar',
          showmsjok:true,
          callback:function(rs){   tabledatos5eacec4c98551.ajax.reload(); __cerrarmodal(_md);  }
        });
    })

    _md.find('input#idgrupoaula').val(filtros.find('select#idgrupoaula').val());
    _md.find('input#idcategoria').val(filtros.find('select#idcategoria').val());
    var infogrupo=filtros.find('select#idgrupoaula option[value="'+filtros.find('select#idgrupoaula').val()+'"]').attr('json')||'';
    if(infogrupo!=''){
      try{
        infogrupo=JSON.parse(atob(infogrupo));
        _md.find('select#idlocal').val(infogrupo.idlocal);
        _md.find('select#idambiente option').first().nextAll().hide();
        _md.find('select#idambiente option[idlocal="'+infogrupo.idlocal+'"]').show();
      }catch(ex){console.log(ex)}
    }
    if(pk!=''){      
      var data=new FormData();
      data.append('idgrupoauladetalle',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/acad_grupoauladetalle',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];
            _md.find('select#idlocal').val(dt.idlocal);
            _md.find('select#idambiente option').first().nextAll().hide();
            _md.find('select#idambiente option[idlocal="'+dt.idlocal+'"]').show();
            _md.find('select#idambiente').val(dt.idambiente);
            if(dt.fecha_inicio!='' && dt.fecha_inicio!=null)
              _md.find('input#fecha_inicio').val(dt.fecha_inicio);
            if(dt.fecha_final!='' && dt.fecha_final!=null)
              _md.find('input#fecha_final').val(dt.fecha_final);
            _md.find('input#idcurso').val(dt.idcurso);
            _md.find('input#idcomplementario').val(dt.idcomplementario);            
            _md.find('input#nomcurso').val(dt.strcurso);
            _md.find('input#iddocente').val(dt.iddocente);
            _md.find('input#dni').val(dt.dnidocente);
            _md.find('input#personanombre').val(dt.strdocente);        
          }
         }
      });
    }
  }
});
</script>