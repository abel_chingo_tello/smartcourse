<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$idproyecto=$this->idproyecto;
$url=$this->documento->getUrlBase();
?>
<style type="text/css">
  .widget_tally_box{
    max-width: none !important;
  }
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_content x_content_2">
        <div class="col-md-12 col-sm-12 text-center listFirst slick-items"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_content x_content_2">
        <div class="col-md-12 col-sm-12 text-center listSecond slick-items"></div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(e){
  $(".listFirst").html("");
  $(".listSecond").html("");
  
 /* addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php echo $url;?>acad_grupoaula",
    icono: "fa-sitemap",
    titulo: "<?php echo JrTexto::_("Study Group"); ?>",
    slickItem: true
  });*/

  addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php echo $url;?>acad_matricula",
    icono: "fa-book",
    titulo: "<?php echo JrTexto::_("Courses"); ?>",
    slickItem: true
  });

   addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php echo $url;?>acad_matricula",
    icono: "fa-graduation-cap",
    titulo: "<?php echo JrTexto::_("Student"); ?>",
    slickItem: true
  });

   addWidgetPanel({
    contentClass: 'listSecond',
    link: "<?php echo $url;?>Examenes",
    icono: "fa-file-text",
    titulo: "<?php echo JrTexto::_("Examenes"); ?>",
    slickItem: true
  });

	__infoempresa();	
 	var slikitems=$('.slick-items').slick(optionslike); 	
    $('header').removeClass('static');
});
</script>