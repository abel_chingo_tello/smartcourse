<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Grupos Aula"); ?>'
    });
   /* addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });*/
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
  label{ margin-top: .5rem;  margin-bottom: 1px; }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>">
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <div class="row" id="addselect">            
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?=JrTexto::_("Level")?>/<?=JrTexto::_("Career")?>/<?=JrTexto::_("Category")?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idcategoria" name="idcategoria"  class=" form-control selchangecat pnlidcategoria0">
                  <option value="0"><?=JrTexto::_("Unique")?></option>
                  <?php if(!empty($this->categorias))
                  foreach ($this->categorias as $k=>$v){ if($v["idpadre"]==0){?>
                    <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                  <?php } } ?>                
              </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat" style="display:none">   
              <label><?=JrTexto::_("Sub Level")?> / <?=JrTexto::_("Module")?> /<?=JrTexto::_("Sub Category")?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="nomsel" name="nomsel" class=" form-control selchangecat pnlidcategoria1">
                  <option value="0"><?=JrTexto::_("Unique")?></option>
                  <?php if(!empty($this->categorias))
                  foreach ($this->categorias as $k=>$v){ if($v["idpadre"]==0){?>
                    <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                  <?php } } ?>                
              </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?php echo JrTexto::_('Local') ?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idlocal" name="idlocal"  class=" form-control">
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                <option value="0"><?php echo JrTexto::_('Unique'); ?></option>
                  <?php 
                  if(!empty($this->local))
                  foreach ($this->local as $r) { ?><option value="<?php echo $r["idlocal"]?>" ><?php echo ucfirst($r["nombre"]); ?></option>
                   <?php } ?>              
                </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?php echo JrTexto::_('State') ?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="estado" name="estado"  class=" form-control">
                <option value=""><?php echo ucfirst(JrTexto::_("All Status"))?></option>
                <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>             
              </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?php echo JrTexto::_('Grade') ?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idgrado" name="idgrado"  class=" form-control">
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                <option value="0"><?php echo JrTexto::_('Only Grade'); ?></option>
                  <?php 
                  if(!empty($this->grados))
                  foreach ($this->grados as $r) { ?><option value="<?php echo $r["idgrado"]?>" ><?php echo JrTexto::_('Grade')." ".JrTexto::_($r["abrev"])." : ".JrTexto::_($r["descripcion"]) ?></option>
                   <?php } ?>              
                </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 pnlcat">   
              <label><?php echo JrTexto::_('Section') ?></label>         
              <div class="select-ctrl-wrapper select-azul">
              <select id="idsesion" name="idsesion"  class=" form-control">
                <option value=""><?php echo JrTexto::_('All'); ?></option>
              <option value="0"><?php echo JrTexto::_('Only Section'); ?></option>
                <?php 
                if(!empty($this->seccion))
                foreach ($this->seccion as $s) { ?><option value="<?php echo $s["idsesion"]?>" ><?php echo JrTexto::_('Section')." ".$s["descripcion"] ?></option>
                 <?php } ?>              
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <label><?php echo  ucfirst(JrTexto::_("text to search"))?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr class="headings">
                    <th>#</th>                             
                    <th><?php echo JrTexto::_("Name"); ?></th>
                    <th><?php echo JrTexto::_("Date"); ?></th>
                    <th><?php echo JrTexto::_("Status"); ?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>     
    </div>
  </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoaula" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idgrupoaula" id="idgrupoaula" value="">
          <input type="hidden" name="tipo" id="tipo" value="v">
          <input type="hidden" name="nvacantes" id="nvacantes" value="10000">
          
          <div class="form-group col-md-12">
              <label class="control-label"><?php echo JrTexto::_('Name');?> <span class="required"> * </span></label>              
              <input type="text"  id="nombre" name="nombre" required="required" class="form-control" value="">
          </div>

          <div class="form-group col-md-12">
            <label class="control-label"><?php echo JrTexto::_('Comment');?> <span class="required"> * </span></label>
            <textarea id="comentario" name="comentario" class="form-control"></textarea>
          </div>

          <div class="form-group col-md-6 col-sm-12 ">
            <label class="control-label"><?php echo JrTexto::_('Date First');?> <span class="required"> * </span></label>
            <input name="fecha_inicio" id="fecha_inicio" class="verdate form-control" required="required" type="date" value="<?php echo date('Y-m-d') ?>"> 
          </div>

          <div class="form-group  col-md-6 col-sm-12 ">
            <label class="control-label"><?php echo JrTexto::_('Date Finish');?> <span class="required"> * </span></label>
            <input name="fecha_fin" id="fecha_fin" class="verdate form-control" required="required" type="date" value="<?php echo date('Y-m-d',strtotime('+3 month',strtotime(date('Y-m-d')))); ?>">
          </div>
          <div class="col-m-12" id="pnleditselected">            
          </div>
          <div class="clearfix"></div>           
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="acad_grupoaula" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5eac3a9176246='';
function refreshdatos5eac3a9176246(){
    tabledatos5eac3a9176246.ajax.reload();
}
$(document).ready(function(){  
  var estados5eac3a9176246={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5eac3a9176246='<?php echo ucfirst(JrTexto::_("Classroom groups"))." - ".JrTexto::_("edit"); ?>';
  var categorias=<?php echo !empty($this->categorias)?json_encode($this->categorias):'[]'; ?>;
  var draw5eac3a9176246=0;
  var _imgdefecto='';

  var addcategoria=function(sel,enpln){
        var idpadre=sel.val();
        console.log(sel,enpln)
        if(sel.hasClass('pnlidcategoria0')){ 
          if(idpadre==0){
            enpln.find('select.pnlidcategoria1').closest('.pnlcat').hide();
            enpln.find('select.pnlidcategoria1').attr('id','nomsel').attr('name','nomsel');
            enpln.find('select.pnlidcategoria0').attr('id','idcategoria').attr('name','idcategoria');
          }else{   
            enpln.find('select.pnlidcategoria1').closest('.pnlcat').show();
            enpln.find('select.pnlidcategoria1').children('option').remove();
            $.each(categorias,function(i,v){
              if(v.idpadre==idpadre){
                enpln.find('select.pnlidcategoria1').append('<option value="'+v.idcategoria+'">'+v.nombre+'</option>')
              }
            })
            enpln.find('select.pnlidcategoria1').attr('id','idcategoria').attr('name','idcategoria');
            enpln.find('select.pnlidcategoria0').attr('id','nomsel').attr('name','nomsel');
          }
        }
        if(enpln.attr('id')=='addselect') refreshdatos5eac3a9176246();
  }
     
  var ventana_5eac3a9176246=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5eac3a9176246=ventana_5eac3a9176246.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/acad_grupoaula',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.estado=$('#estado').val();
            d.idgrado=$('#idgrado').val();
            d.idsesion=$('#idsesion').val();
            d.idcategoria=$('#idcategoria').val();
            d.idlocal=$('#idlocal').val();
            d.texto=$('#texto').val();                        
            draw5eac3a9176246=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5eac3a9176246;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            var fini=data[i].fecha_inicio==null?'':data[i].fecha_inicio;
            var ffin=data[i].fecha_fin==null?'':data[i].fecha_fin;
            datainfo.push([            
              (i+1),
              data[i].nombre,
              fini+' -- '+ ffin,
              '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idgrupoaula+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eac3a9176246[data[i].estado]+'</a>',               
              '<a class="btn-cursos btn btn-xs" href="javascript:;" data-id="'+data[i].idgrupoaula+'" ><i class="fa fa fa-book"></i></a> <!--a class="btn-matriculas btn btn-xs" href="javascript:;" data-id="'+data[i].idgrupoaula+'" ><i class="fa fa-users"></i></a>  <a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idgrupoaula+'" data-titulo="'+tituloedit5eac3a9176246+'"><i class="fa fa-edit"></i></a>  <a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idgrupoaula+'" ><i class="fa fa-trash"></i></a-->'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5eac3a9176246.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5eac3a9176246();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5eac3a9176246();
  }).on('change','select#estado, select#idlocal, select#idgrado, select#idsesion ',function(ev){refreshdatos5eac3a9176246();  
  }).on('change','#idcategoria , .selchangecat',function(ev){addcategoria($(this),$('#addselect'));
  }).on('click','.btnbuscar',function(ev){ refreshdatos5eac3a9176246();
  }).on('click','.btn-matriculas',function(ev){
    window.location=_sysUrlBase_+'/acad_matriculas/'
  }).on('click','.btn-cursos',function(ev){
    window.location=_sysUrlBase_+'/acad_grupoauladetalle/'    
  })

 

  ventana_5eac3a9176246.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idgrupoaula',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/acad_grupoaula/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5eac3a9176246.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/acad_grupoaula/setcampo','masvalores':{idgrupoaula:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idgrupoaula',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_grupoaula/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5eac3a9176246.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modalacad_grupoaula(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modalacad_grupoaula(el);
  })

  /*Formulario*/
  var __modalacad_grupoaula=function(el){
    var titulo = '<?php echo JrTexto::_("Grupos aula"); ?>';
    var filtros=$('#ventana_<?php echo $idgui; ?>');
    var frm=$('#frm<?php echo $idgui; ?>').clone();     
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/acad_grupoaula/guardar',
          showmsjok:true,
          callback:function(rs){   
            filtros.find('select.pnlidcategoria0').val(_md.find('select.pnlidcategoria0').val());
            filtros.find('select.pnlidcategoria0').trigger('change');
            filtros.find('select.pnlidcategoria1').val(_md.find('select.pnlidcategoria1').val());
            filtros.find('select.estado').val(_md.find('select.estado').val());
            filtros.find('select.idsesion').val(_md.find('select.idsesion').val());
            filtros.find('select.idlocal').val(_md.find('select.idlocal').val());
            filtros.find('select.idgrado').val(_md.find('select.idgrado').val());
            tabledatos5eac3a9176246.ajax.reload(); __cerrarmodal(_md);  }
        });
    }).on('change','#idcategoria , .selchangecat',function(ev){
      console.log($(this));
      addcategoria($(this),_md.find('#pnleditselected'));
    })
    var pk=el.attr('pk')||'';
    var pnlcat=$('#addselect').find('div.pnlcat');
    _md.find('#pnleditselected').html('');    
    $.each(pnlcat,function(i,v){
      var seltmp=$(v).clone();
      seltmp.addClass('form-group col-md-6').removeClass('col-md-3');
      _md.find('#pnleditselected').append(seltmp);  
    })
    _md.find('select#idsesion').children('option').first().remove();
    _md.find('select#idgrado').children('option').first().remove();
    _md.find('select#estado').children('option').first().remove();
    _md.find('select#idlocal').children('option').first().remove();
    if(pk!=''){      
      var data=new FormData();
      data.append('idgrupoaula',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/acad_grupoaula',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0]; 
            _md.find('select#estado').val(dt.estado);
            _md.find('select#idsesion').val(dt.idsesion);
            _md.find('select#idgrado').val(dt.idgrado);
            _md.find('select#idlocal').val(dt.idlocal);
            if(dt.fecha_inicio!='' && dt.fecha_inicio!=null)
              _md.find('input#fecha_inicio').val(dt.fecha_inicio);
            if(dt.fecha_fin!='' && dt.fecha_fin!=null)
              _md.find('input#fecha_fin').val(dt.fecha_fin);
            _md.find('input#nombre').val(dt.nombre);
            _md.find('textarea#comentario').val(dt.comentario);
            _idcat=dt.idcategoria;
            _idcatpadre=0;
            if(categorias.length>0 && _idcat>0)
              $.each(categorias,function(i,v){
                if(v.idcategoria==_idcat) _idcatpadre=v.idpadre;
              })
            if(_idcatpadre!=0){      
              _md.find('select.pnlidcategoria0').val(_idcatpadre);
              _md.find('select.pnlidcategoria1').val(_idcat);
            }else{
              _md.find('select.pnlidcategoria0').val(_idcat);
            }           
          }
         }
      });
    }else{
      _md.find('select.pnlidcategoria0').val(filtros.find('select.pnlidcategoria0').val());
      _md.find('select.pnlidcategoria1').val(filtros.find('select.pnlidcategoria1').val());
    }
  }
});
</script>