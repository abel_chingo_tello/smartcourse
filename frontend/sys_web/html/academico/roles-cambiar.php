<style type="text/css">
	.pnlrol{
		display: inline-block;
	    width: 250px;	   
	    border: 1px solid #ddd;
	    cursor: pointer;
		background-color: white;
		padding-top: 10px;
	}
	.img-responsive{
		display: inline-block;
	}
	.titulo00{
		color:rgb(165, 42, 42);
		margin-top: 10%;
		padding: 1.5ex;
	}
	.titulo{
		color:rgb(165, 42, 42);
		padding: 1ex;
	}

	.select-ctrl-wrapper:after {
    background: #555;
    border-radius: 0 4px 4px 0;
    color: #fff;
    content: "\f0d7";
    font: normal normal normal 34px/1 FontAwesome;
    padding: 0 5px;
    pointer-events: none;
    position: absolute;
    right: 0px;
    top: 0;
}

  .titulo-p{
    float: none !important;
    font-size: 24px !important;
    font-weight: bold !important;
    text-align: center !important;
    width: 100% !important;
  }
</style>
<div class="row">
	<div class="col-md-12 col-sm-12 ">
	<div class="x_panel">
		<div class="x_title">
		<h2 class="titulo-p"><?php echo JrTexto::_('Select a role'); ?></h2>
		<div class="clearfix"></div>
		</div>
		<div class="x_content">
		<div class="col-md-12 text-center">
			
		<?php
		 if(!empty($this->roles)){
		 	$nrol=count($this->roles);		 	
		 foreach ($this->roles as $rolk => $rol){?>
		<div class="pnlrol hvr-float-shadow" data-idrol="<?php echo $rol["idrol"] ?>" data-rol="<?php echo $rol["rol"]; ?>" data-empresa="<?php echo $rol["idempresa"]; ?>" data-proyecto="<?php echo $rol["idproyecto"]; ?>" >
			<div class="imagen">
				<?php 
				$srcimg_=$this->documento->getUrlStatic().'/media/imagenes/rol_'.strtolower($rol["rol"]).".jpg";
				//$srcimg=is_file($srcimg_)?$srcimg_:($this->documento->getUrlStatic().'/media/imagenes/rol_default.jpg');
				$srcimg=$srcimg_;
				?>
				<img src="<?php echo $srcimg; ?>" class="img-responsive">
			</div>
			<div class="titulo"><?php echo $rol["rol"];?></div>
		</div>		 	
		 <?php } }?>
		</div>
		</div>
	</div>
	</div>
</div>
<?php 
if(!empty($this->noroles)){?>
<div class="col-md-12 col-sm-12 ">
	<div class="x_panel">
		<div class="x_title">
		<h2 class="titulo-p"><?php echo JrTexto::_('Other Roles'); ?></h2>
		<div class="clearfix"></div>
		</div>
		<div class="x_content">

			<div class="col-xs-12 col-sm-12 col-md-4">
				<div class="select-ctrl-wrapper select-azul">
					<select id="idrol2empresa" class="form-control select-ctrl " name="idrol2empresa">
						<option value=""><?php echo JrTexto::_('Selected Role') ?></option>
						<?php  foreach ($this->noroles as $rolk => $rol){?>
						<option  value="<?php echo $rol["idrol"] ?>" data-rol="<?php echo $rol["rol"]; ?>" data-empresa="<?php echo $rol["idempresa"]; ?>" data-proyecto="<?php echo $rol["idproyecto"]; ?>"><?php echo $rol["empresa"]." - ".$rol["rol"]; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			</div>
	</div>
</div>
<?php } ?>


<script type="text/javascript">
	$('.pnlrol').click(function(ev){
		__sysAyax({ // carga la edicion de los modulos
			showmsjok:false,
			url:_sysUrlBase_+'json/sesion/cambiarrol/?idrol='+$(this).attr('data-idrol')+'&rol='+$(this).attr('data-rol'),
			callback:function(rs){
				if(rs.data==true){
					localStorage.setItem("usersesion", JSON.stringify(rs.usersesion));
                    localStorage.setItem("userempresa", JSON.stringify(rs.userempresa));
                    localStorage.setItem("userproyecto", JSON.stringify(rs.userproyecto));
                    __cambiarinfouser();
                    if(this.location!=top.location){
						if(this.location.href.indexOf('#')==-1&&top.location.href.indexOf('#')==-1)
							window.top.location.href=top.location.href;
						else 
							window.top.location.href=_sysUrlBase_;
					}else{
						top.location.href=_sysUrlBase_;
					}
				}            
			}
		});
	})

	$('#idrol2empresa').change(function(ev){
		var idrol=$(this).val()||'';
		var idempresa=$(this).children('option:selected').attr('data-empresa');
		var idproyecto=$(this).children('option:selected').attr('data-proyecto');
		var rol=$(this).children('option:selected').attr('data-rol');
		/*if(idrol!=''){
			console.log(idrol,idproyecto,idempresa);
		}*/
		__sysAyax({ // carga la edicion de los modulos
			showmsjok:false,
			url:_sysUrlBase_+'json/sesion/cambiarrol/?idrol='+idrol+'&rol='+rol+'&idempresa='+idempresa+'&idproyecto='+idproyecto,
			callback:function(rs){
				if(rs.data==true){					
                    localStorage.setItem("userempresa", JSON.stringify(rs.userempresa));
                    localStorage.setItem("userproyecto", JSON.stringify(rs.userproyecto));
					localStorage.setItem("usersesion", JSON.stringify(rs.usersesion));
                    __cambiarinfouser();
					if(this.location!=top.location){
						window.top.location.href=top.location.href;
					}else{
						top.location.href=_sysUrlBase_;
					}
				}
			}
		});
	})
</script>