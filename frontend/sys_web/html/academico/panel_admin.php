<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$idproyecto=$this->idproyecto;
$url=$this->documento->getUrlBase();
?>
<style type="text/css">
  .widget_tally_box{
    max-width: none !important;
  }
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_content x_content_2">
        <div class="col-md-12 col-sm-12 text-center listFirst slick-items"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_content x_content_2">
        <div class="col-md-12 col-sm-12 text-center listSecond slick-items"></div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(e){
  $(".listFirst").html("");
  $(".listSecond").html("");
  addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php echo $url;?>proyectoempresa?paris=1&academico=1",
    icono: "fa-university",
    titulo: "<?php echo JrTexto::_("Empresas"); ?>",
   // classTipo: "tipoempresa1",
    slickItem: true
  });
  addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php echo $url;?>miproyecto",
    icono: "fa-university",
    titulo: "<?php echo JrTexto::_("Proyecto"); ?>",
   // classTipo: "tipoempresa1",
    slickItem: true
  });

  /*addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php //echo $url;?>min_dre",
    icono: "fa-university",
    titulo: "<?php //echo JrTexto::_("Dre"); ?>",
    classTipo: "tipoempresa1",
    slickItem: true
  });*/

  /*addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php //echo $url;?>min_dre",
    icono: "fa-university",
    titulo: "<?php //echo JrTexto::_("Dre"); ?>",
    classTipo: "tipoempresa1",
    slickItem: true
  });*/
  /*addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php //echo $url;?>ugel",
    icono: "fa-briefcase",
    titulo: "<?php //echo JrTexto::_("Ugel"); ?>",
    classTipo: "tipoempresa1",
    slickItem: true
  });*/
  /*addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php //echo $url;?>local",
    icono: "fa-building",
    titulo: "<?php //echo JrTexto::_("Local/IIEE"); ?>",
    classTipo: "tipoempresa1",
    slickItem: true
  });*/
 /* addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php //echo $url;?>ambiente",
    icono: "fa-flag",
    titulo: "<?php //echo JrTexto::_("Classroom"); ?>",
    slickItem: true
  });*/
  /*addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php //echo $url;?>cursos",
    icono: "fa-book",
    titulo: "<?php //echo JrTexto::_("Course"); ?>",
    slickItem: true
  }); */
  addWidgetPanel({
    contentClass: 'listFirst',
    link: "<?php echo $url;?>acad_categorias",
    icono: "fa-tasks",
    titulo: "<?php echo JrTexto::_("Categories"); ?>",
    slickItem: true
  });

  addWidgetPanel({
    contentClass: 'listSecond',
    link: "<?php echo $url;?>acad_grupoaula",
    icono: "fa-sitemap",
    titulo: "<?php echo JrTexto::_("Study Group"); ?>",
    slickItem: true
  });
  addWidgetPanel({
    contentClass: 'listSecond',
    link: "<?php echo $url;?>personal",
    icono: "fa-user",
    titulo: "<?php echo JrTexto::_("Personal"); ?>",
    slickItem: true
  });
  addWidgetPanel({
    contentClass: 'listSecond',
    link: "<?php echo $url;?>acad_matricula",
    icono: "fa-graduation-cap",
    titulo: "<?php echo JrTexto::_("Student"); ?>",
    slickItem: true
  });
	__infoempresa();	
 	var slikitems=$('.slick-items').slick(optionslike); 	
    $('header').removeClass('static');
});
</script>