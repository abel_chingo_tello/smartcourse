<?php
defined('RUTA_BASE') or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla != 'modal' ? false : true;
if (!empty($this->datos)) $frm = $this->datos;
$ventanapadre = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : 'eeeexzx-1';
$_imgdefecto = "static/media/nofoto.jpg";
if (!$ismodal) { ?>
  <div class="row" id="breadcrumb">
    <div class="col">
      <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio(); ?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio(); ?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio(); ?>/acad_capacidades">&nbsp;<?php echo JrTexto::_('Acad_capacidades'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion); ?></li>
      </ol>
    </div>
  </div>
<?php } ?>
<div class="row ventana" id="vent-<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
  <div class="col">
    <div id="msj-interno"></div>
    <form method="post" id="frm-<?php echo $idgui; ?>" tb="acad_capacidades" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui; ?>">
      <input type="hidden" name="idcapacidad" id="idcapacidad" value="<?php echo $this->pk; ?>">
      <div class="form-group">
        <label class="control-label"><?php echo JrTexto::_('Competencia'); ?> <span class="required"> * </span></label>
        <div class="">
          <!-- <input type="text" id="txtIdcompetencia" name="idcompetencia" required="required" class="form-control" value="< ?php echo @$frm["idcompetencia"]; ?>"> -->
          <select id="txtIdcompetencia" name="idcompetencia" class="form-control">
            <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label"><?php echo JrTexto::_('Idcurso'); ?> <span class="required"> * </span></label>
        <div class="">
          <select id="txtidcurso" name="idcurso" class="form-control">
            <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
          </select>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label"><?php echo JrTexto::_('Nombre'); ?> <span class="required"> * </span></label>
        <div class="">
          <textarea id="txtNombre" name="nombre" class="form-control"><?php echo @trim($frm["nombre"]); ?></textarea>
        </div>
      </div>


      <hr>
      <div class="col-md-12 form-group text-center">
        <button id="btn-saveAcad_capacidades" type="submit" tb="acad_capacidades" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save'); ?> </button>
        <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('acad_capacidades')) ?>" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel'); ?></a>
      </div>
    </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic() . '/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev) {
    var frm = $('#frm-<?php echo $idgui; ?>');
    if (!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui; ?>'));
  })
</script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#frm-<?php echo $idgui; ?>').bind({
      submit: function(event) {
        event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui; ?>");
        var data = new FormData(fele)
        __sysAyax({
          fromdata: data,
          url: _sysUrlBase_ + 'json/acad_capacidades/guardar',
          //showmsjok:true,
          callback: function(rs) {
            if (rs.code == 200) {
              redir(_sysUrlSitio_ + '/acad_capacidades');
            }
          }
        });
      }
    })
  })
</script>