<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";

if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Expertise"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> <?php echo JrTexto::_('add') ?>',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>">
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <div class="row" id="addselect">            
            <div class="col-md-4 col-sm-6 col-xs-12">   
                <label><?=JrTexto::_("Level")?>/<?=JrTexto::_("Career")?>/<?=JrTexto::_("Category")?></label>         
                <select id="selectchange0"  class=" form-control selchange">
                  <option value="0"><?=JrTexto::_("Unique")?></option>
                <?php
                if(!empty($this->categorias))
                foreach ($this->categorias as $k=>$v){ if($v["idpadre"]==0){?>
                  <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                <?php }
              } ?>                
              </select>
            </div>
          </div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr class="headings">
                    <th>#</th>                             
                    <th><?php echo JrTexto::_("Name"); ?></th>
                    <th><?php echo JrTexto::_("State"); ?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>     
    </div>
  </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui; ?>" tb="acad_competencias" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui; ?>">
        <input type="hidden" name="idcompetencia" id="idcompetencia" value="">
        <div class="row" id="addfrmselect">          
        </div>       
        <div class="form-group">
          <label class="control-label"><?php echo JrTexto::_('Name'); ?> <span class="required"> * </span></label>
          <div class="">
            <textarea id="txtNombre" name="nombre" class="form-control"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label"><?php echo JrTexto::_('State'); ?> <span class="required"> * </span></label>
          <div class="">
            <a style="cursor:pointer;" class="chkformulario fa fa-check-circle-o" data-value="1" data-valueno="0" data-value2="0">
              <span> <?php echo  JrTexto::_("Active"); ?></span>
              <input type="hidden" name="estado" value="1">
            </a> 
          </div>
        </div>
        <hr>
        <div class="col-md-12 form-group text-center">
          <button id="btn-saveAcad_competencias" type="submit" tb="acad_competencias" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save'); ?> </button>
          <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel'); ?></a>
        </div>
      </form>
    </div>
  </div>
</div>


  <script type="text/javascript">
    var tabledatos5e57ef3e2a0c6 = '';
    var categorias=<?php echo !empty($this->categorias)?json_encode($this->categorias):'[]'; ?>;
    var cargoini=false;
    function refreshdatos5e57ef3e2a0c6(){
      console.log(cargoini);
      if(cargoini==true) tabledatos5e57ef3e2a0c6.ajax.reload();
      cargoini=true;
    }
    $(document).ready(function() {
      var estados5e57ef3e2a0c6={'1':'<?php echo JrTexto::_("Active") ?>','0': '<?php echo JrTexto::_("Inactive") ?>','C': '<?php echo JrTexto::_("Cancelled") ?>'}
      var tituloedit5e57ef3e2a0c6 = '<?php echo ucfirst(JrTexto::_("Expertise")) . " - " . JrTexto::_("edit"); ?>';
      var draw5e57ef3e2a0c6 = 0;
      var _imgdefecto = '';
      var ventana_5e57ef3e2a0c6 = $('#ventana_<?php echo $idgui; ?>');

      var addcategoria=function(sel,enpln){
        var idpadre=sel.val();
        var addselect=enpln.find('select#selectchange0').parent().clone();
        addselect.children('select').attr('id','selectchange'+idpadre);
        addselect.children('select').find('option').remove();
        var hay=false;
        if(idpadre!=0){
          $.each(categorias,function(i,v){
            if(v.idpadre==idpadre){
              hay=true;
              addselect.children('select').append('<option value="'+v.idcategoria+'" padre="'+v.idpadre+'">'+v.nombre+'</option>')
            }
          })
        }
        if(hay==true){
          if(idpadre==0) addselect.children('label').text('<?=JrTexto::_("Level")?> / <?=JrTexto::_("Career")?> / <?=JrTexto::_("Category")?>');
          else addselect.children('label').text('<?=JrTexto::_("Sub Level")?> / <?=JrTexto::_("Module")?> / <?=JrTexto::_("Sub Category")?>');
          enpln.append(addselect);
          addcategoria(enpln.find('select#selectchange'+idpadre),enpln);
        }else{
          //if(idpadre==0) addselect.children('label').text('Nivel / Carrera / Categoria');
          //else addselect.children('label').text('Sub Nivel / Modulo / Sub Categoria');
          sel.attr('id','idcategoria').removeClass('selchange');
          sel.attr('name','idcategoria');
          if(enpln.attr('id')=='addselect') {
            refreshdatos5e57ef3e2a0c6(); 
          }
        }
      }
     
      tabledatos5e57ef3e2a0c6 = ventana_5e57ef3e2a0c6.find('.table').DataTable({
        "searching": true,
        "processing": false,
        //"serverSide": true,       
        "ajax": {
          url: _sysUrlBase_ + 'json/acad_competencias',
          type: "post",
          data: function(d) {
            d.json = true
            d.idcategoria = $('select#idcategoria').val();
            //d.estado = $('#cbestado').val(),
            //d.texto=$('#texto').val(),
            draw5e57ef3e2a0c6 = d.draw;
            // draw5eaba8b672583=d.draw;
            // console.log(d);
          },
          dataSrc: function(json) {
            var data = json.data;
            json.draw = draw5e57ef3e2a0c6;
            json.recordsTotal = json.data.length;
            json.recordsFiltered = json.data.length;
            var datainfo = new Array();
            for (var i = 0; i < data.length; i++) {
              var n=i+1;
              datainfo.push([
                n,data[i].nombre,'<a href="javascript:;"  class="btn-activar  ' + (data[i].estado == 0 ? '' : 'active') + ' " data-campo="estado"  data-id="' + data[i].idcompetencia + '"> <i class="fa fa' + (data[i].estado == '1' ? '-check' : '') + '-circle-o fa-lg"></i> ' + estados5e57ef3e2a0c6[data[i].estado] + '</a>','<a class="btn btn-xs btnvermodal" nombre="'+data[i].nombre+'" estado="'+data[i].estado+'" data-idcat="' + data[i].idcategoria + '" pk="'+data[i].idcompetencia+'"  href="#" ><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="' + data[i].idcompetencia + '" ><i class="fa fa-trash"></i></a>'
              ]);
            }
            return datainfo;
          },
          error: function(d) {
            console.log(d)
          }
        },
        "language": {
          "url": _sysIdioma_ == 'es' ? _sysUrlStatic_ + "/libs/datatable1.10/idiomas/es.json" : ''
        }
      });

      if($('select#selectchange0').length>0){
        addcategoria($('select#selectchange0'),$('#addselect'));
      }

      ventana_5e57ef3e2a0c6.on('keydown', '.textosearchlist', function(ev){
        if (ev.keyCode === 13) refreshdatos5e57ef3e2a0c6();
      }).on('blur', '.textosearchlist', function(ev) {
        refreshdatos5e57ef3e2a0c6();
      }).on('change', '#idcategoria', function(ev){
        $(this).parent().nextAll().remove();
        $(this).removeAttr('name').addClass('selchange');
        $(this).attr('id','selectchange0');
        addcategoria($(this),$('#addselect'));

      }).on('change', '#estado', function(ev) {
        refreshdatos5e57ef3e2a0c6();
      }).on('click', '.btnbuscar', function(ev) {
        refreshdatos5e57ef3e2a0c6();
      }).on('change','select.selchange',function(ev){
        ev.preventDefault();       
        $(this).parent().nextAll().remove();
        addcategoria($(this),$('#addselect'));
      })


      ventana_5e57ef3e2a0c6.on('click', '.btn-eliminar', function() {
        var id = $(this).attr('data-id');
        $.confirm({
          title: '<?php echo JrTexto::_('Confirm action'); ?>',
          content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
          confirmButton: '<?php echo JrTexto::_('Accept'); ?>',
          cancelButton: '<?php echo JrTexto::_('Cancel'); ?>',
          confirmButtonClass: 'btn-success',
          cancelButtonClass: 'btn-danger',
          closeIcon: true,
          confirm: function() {
            var data = new FormData()
            data.append('idcompetencia', id);
            __sysAyax({
              fromdata: data,
              url: _sysUrlBase_ + 'json/acad_competencias/eliminar',
              callback: function(rs) {
                if (rs.code == 200) {
                  tabledatos5e57ef3e2a0c6.ajax.reload();
                }
              }
            });
          }
        })
      }).on('click', '.subirfile', function(ev) {
        __subirfile({
          file: $(this),
          dataurlsave: 'json/acad_competencias/setcampo',
          'masvalores': {
            idcompetencia: $(this).attr('idpk'),
            campo: 'imagen'
          }
        });
      }).on('click', '.btn-activar', function() {
        _this = $(this);
        var activo = 1;
        if (_this.hasClass('active')) {
          activo = 0;
        }
        var data = new FormData();
        data.append('idcompetencia', _this.attr('data-id'));
        data.append('campo', _this.attr('data-campo'));
        data.append('valor', activo);
        __sysAyax({
          fromdata: data,
          url: _sysUrlBase_ + 'json/acad_competencias/setcampo',
          showmsjok: true,
          callback: function(rs) {
            if (rs.code == 200) {
              tabledatos5e57ef3e2a0c6.ajax.reload();
            }
          }
        });
      }).on('click', '.btnvermodal', function(e) {
        e.preventDefault();        
        var el=$(this);
        _modalcompetencia(el); 
      }); 

      var _modalcompetencia=function(el){
        var titulo = '<?php echo JrTexto::_('Expertise') ?>';
        var frm=$('#frm<?php echo $idgui; ?>').clone();
       // frm.find('#addfrmselect').html(cbo.html());
        var _md = __sysmodal({'html': frm,'titulo': titulo});        
        var cbo=$('#addselect').clone();
        cbo.children('div.col-md-4').removeClass('col-md-4').addClass('col-md-6');
        _md.find('#addfrmselect').html(cbo.html());
        _md.find('input#idcompetencia').val(el.attr('pk')||'');
        var estado=el.attr('estado')||'0';
        var nombre=el.attr('nombre')||'';
        if(estado=='0'){
          _md.find('a.chkformulario').removeClass('fa-check-circle-o').addClass('fa-circle-o');
          _md.find('a.chkformulario').children('span').text('<?php echo JrTexto::_('Inactive') ?>');
          _md.find('a.chkformulario').children('input').val(0);
        }
        if(nombre!=''){
          _md.find('textarea[name="nombre"]').val(nombre);
        }

        $('#addselect').find('select').each(function(i,v){ var _v=$(v); _md.find('#'+_v.attr('id')||'uiu').val(_v.val()); })
        _md.on('change','select.selchange',function(ev){
          ev.preventDefault();
          var el=$(this);
          var id=el.attr('id');
          var eltmp=$('#addselect select#'+id);
          $(this).parent().nextAll().remove();
          eltmp.parent().nextAll().remove();
          addcategoria($(this),_md.find('#addfrmselect'));
          eltmp.val(el.val());
          addcategoria(eltmp,$('#addselect'));
        }).on('change','select#idcategoria',function(ev){
          ev.preventDefault();
          $(this).parent().nextAll().remove();
          var eltmp=$('#addselect select#idcategoria');
          eltmp.parent().nextAll().remove();
          $(this).removeAttr('name').addClass('selchange');
          eltmp.removeAttr('name').addClass('selchange');
          $(this).attr('id','selectchange0');
          eltmp.attr('id','selectchange0');
          addcategoria($(this),_md.find('#addfrmselect'));
          eltmp.val($(this).val());
          addcategoria(eltmp,$('#addselect'));

        }).on('click','.chkformulario',function(ev){
          _this = $(this);           
          if(_this.hasClass('fa-check-circle-o')) {            
            _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
            _this.children('span').text('<?php echo JrTexto::_('Inactive'); ?>')
            _this.children('input').val(0);
          }else{            
            _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
            _this.children('span').text('<?php echo JrTexto::_('Active'); ?>');
            _this.children('input').val(1);
          }
        }).on('submit','form.formventana',function(ev){
            ev.preventDefault();
            var id=__idgui();
            $(this).attr('id',id);
            var fele = document.getElementById(id);
            var data=new FormData(fele);
            __sysAyax({ 
              fromdata:data,
              url:_sysUrlBase_+'json/acad_competencias/guardar',
              showmsjok:true,
              callback:function(rs){tabledatos5e57ef3e2a0c6.ajax.reload(); __cerrarmodal(_md);  }
            });
        })
      }

      $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
        e.preventDefault();        
        var el=$(this);
        _modalcompetencia(el);
      })
    });  
  </script>