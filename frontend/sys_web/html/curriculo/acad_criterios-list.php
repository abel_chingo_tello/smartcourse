<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Criteria"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('add') ?>',
      href: '<?php echo $this->documento->getUrlSitio(); ?>/acad_criterios/agregar'
    });
  </script>
  <!-- <div class="row" id="breadcrumb">
    <div class="col">
      <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;< ?php echo JrTexto::_('Back') ?></a></li>
        <li><a href="< ?php echo $this->documento->getUrlSitio(); ?>"><i class="fa fa-home"></i>&nbsp;< ?php echo JrTexto::_("Home"); ?></a>
        <li class="active">&nbsp;< ?php echo JrTexto::_("Acad_criterios"); ?></li>
      </ol>
    </div>
  </div> -->
<?php } ?>
<style type="text/css">
 input::placeholder {color: #c0bebd !important;  font-size: 12px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>">
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <div class="row" id="addselect">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <label><?=JrTexto::_("Level")?>/<?=JrTexto::_("Career")?>/<?=JrTexto::_("Category")?></label>
                <select id="selectchange0"  class=" form-control selchange">
                  <option value="0"><?=JrTexto::_("Unique")?></option>
                <?php if(!empty($this->categorias)) foreach ($this->categorias as $k=>$v){ if($v["idpadre"]==0){?>
                  <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                <?php }} ?>                
              </select>
            </div>       
          </div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr class="headings">
                    <th>#</th>
                    <th><?php echo JrTexto::_("Course"); ?></th>
                    <th><?php echo JrTexto::_("Capacity"); ?></th>
                    <th><?php echo JrTexto::_("Criterion"); ?></th>
                    <th><?php echo JrTexto::_("State"); ?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col">
      <div id="msj-interno"></div>
       <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_criterios" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
        <input type="hidden" name="idcriterio" id="idcriterio" value="">      
        <div class="row" id="addfrmselect">          
        </div>
        <div class="form-group">
          <label class="control-label"><?php echo JrTexto::_('Name'); ?> <span class="required"> * </span></label>
          <div class="">
            <textarea id="nombre" name="nombre" required="required" class="form-control"></textarea>
          </div>
        </div>        
        <div class="form-group">
          <label class="control-label"><?php echo JrTexto::_('State'); ?> <span class="required"> * </span></label>
          <div class="">
            <a style="cursor:pointer;" class="chkformulario fa fa-check-circle-o" data-value="1" data-valueno="0" data-value2="0">
              <span> <?php echo  JrTexto::_("Active"); ?></span>
              <input type="hidden" name="estado" value="1">
            </a> 
          </div>
        </div>
        <hr>
        <div class="col-md-12 form-group text-center">
          <button id="btn-saveAcad_criterios" type="submit" tb="acad_criterios" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save'); ?> </button>
          <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel'); ?></a>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  var tabledatos5e57ef520aa7c = '';
  var categorias=<?php echo !empty($this->categorias)?json_encode($this->categorias):'[]'; ?>;
  var competencias=<?php echo !empty($this->competencias)?json_encode($this->competencias):'[]'; ?>;
  var cursos=<?php echo !empty($this->cursos)?json_encode($this->cursos):'[]'; ?>;
  function refreshdatos5e57ef520aa7c() {
    tabledatos5e57ef520aa7c.ajax.reload();
  }
  $(document).ready(function() {
    var estados5e57ef520aa7c={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C': '<?php echo JrTexto::_("Cancelled") ?>'}
    var tituloedit5e57ef520aa7c = '<?php echo ucfirst(JrTexto::_("Criteria")) . " - " . JrTexto::_("edit"); ?>';
    var draw5e57ef520aa7c = 0;
    var _imgdefecto = '';
    var ventana_5e57ef520aa7c = $('#ventana_<?php echo $idgui; ?>');
    var addcategoria=function(sel,enpnl){
      var idpadre=sel.val();
      var addselect=enpnl.find('select#selectchange0').parent().clone();
      addselect.children('select').attr('id','selectchange'+idpadre);
      addselect.children('select').find('option').remove();
      var hay=false;
      if(idpadre!=0){
        $.each(categorias,function(i,v){
          if(v.idpadre==idpadre){
            hay=true;
            addselect.children('select').append('<option value="'+v.idcategoria+'" padre="'+v.idpadre+'">'+v.nombre+'</option>')
          }
        })
      }
      if(hay==true){
        if(idpadre==0) addselect.children('label').text('<?=JrTexto::_("Level")?> / <?=JrTexto::_("Career")?> / <?=JrTexto::_("Category")?>');
        else addselect.children('label').text('<?=JrTexto::_("Sub Level")?> / <?=JrTexto::_("Module")?> / <?=JrTexto::_("Sub Category")?>');
        enpnl.append(addselect);
        addcategoria(enpnl.find('select#selectchange'+idpadre),enpnl);
      }else{
        sel.attr('id','idcategoria').removeClass('selchange');
        sel.attr('name','idcategoria');
        setTimeout(function(){agregarcompetencias(sel,enpnl)},200);
      }
    }

    var agregarcompetencias=function(el,enpnl){
      var idcat=el.val();
      var html='';      
      el.parent().nextAll().remove();    
      var nt=enpnl.attr('id')=='addselect'?4:6;    
        html='<div class="col-md-12"></div><div class="col-md-'+nt+' col-sm-6 col-xs-12"><label style="padding-top:1.5ex; margin:0px;"><?=JrTexto::_("Competence")?></label><select name="idcompetencia" id="idcompetencia" class=" form-control changedatos">';            
        if(competencias.length>0){
          var haycom=false;
          $.each(competencias,function(i,v){
            if(idcat==v.idcategoria){
              haycom=true;
              html+='<option value="'+v.idcompetencia+'">'+v.nombre+'</option>';
            }
          })
          if(haycom==false){
            html+='<option value="0"><?=JrTexto::_("No competition")?></option>';  
          }
        }else{
          html+='<option value="-1"><?=JrTexto::_("Select")?></option>';
        }
        html+='</select></div>';
        html+='<div class="col-md-'+nt+' col-sm-6 col-xs-12"><label style="padding-top:1.5ex; margin:0px;"><?=JrTexto::_("Courses")?></label><select id="idcurso" name="idcurso" class=" form-control "><option value=""><?=JrTexto::_("All")?></option></select></div>';
        html+='<div class="col-md-'+nt+' col-sm-6 col-xs-12"><label style="padding-top:1.5ex; margin:0px;"><?=JrTexto::_("Capabilities")?></label><select id="idcapacidad" name="idcapacidad" class=" form-control "><option value=""><?=JrTexto::_("All")?></option></select></div>';
        enpnl.append(html);
      addcursos(enpnl);
    }

    var addcursos=function(enpnl){
        var data=new FormData();
        var idcat=enpnl.find('select#idcategoria').val()||0;
        if(enpnl.attr('id')=='addselect') enpnl.find('select#idcurso').children('option').first().nextAll().remove();
        else {
          enpnl.find('select#idcurso').children('option').remove();
          enpnl.find('select#idcurso').attr('required','required');
        }
        if(idcat!=0)data.append('idcategoria',idcat)
        data.append('idcompetencia',  enpnl.find('select#idcompetencia').val()||0)
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/acad_curso/',
          showmsjok:false,
          async:false,
          callback:function(rs){  
            if(rs.data.length>0){
              var dt=rs.data;              
              $.each(dt,function(i,v){
                enpnl.find('select#idcurso').append('<option value="'+v.idcurso+'">'+v.nombre+'</option>');
              })
            }
            agregarcapacidad(enpnl);
          }
        });
    }

    var agregarcapacidad=function(enpnl){
      var data=new FormData();
      if(enpnl.attr('id')=='addselect') enpnl.find('select#idcapacidad').children('option').first().nextAll().remove();
      else{
        enpnl.find('select#idcapacidad').children('option').remove();
        enpnl.find('select#idcapacidad').attr('required','required');
      }
      data.append('idcurso', enpnl.find('select#idcurso').val())
      data.append('idcompetencia',  enpnl.find('select#idcompetencia').val())
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/acad_capacidades',
        showmsjok:false,
        async:false,
        callback:function(rs){  
          if(rs.data.length>0){
            var dt=rs.data;          
            $.each(dt,function(i,v){
              enpnl.find('select#idcapacidad').append('<option value="'+v.idcapacidad+'">'+v.nombre+'</option>');
            })
          }
         if(enpnl.attr('id')=='addselect') refreshdatos5e57ef520aa7c();
        }
      });
    }


    tabledatos5e57ef520aa7c = ventana_5e57ef520aa7c.find('.table').DataTable({
      "searching": true,      
      "ajax": {
        url: _sysUrlBase_ + 'json/acad_criterios',
        type: "post",
        data: function(d) {
          d.json = true;
          d.idcurso = ventana_5e57ef520aa7c.find('select#idcurso').val()||'',
          d.idcapacidad=ventana_5e57ef520aa7c.find('select#idcapacidad').val()||'',
          //d.idcapacidad = $('#cbidcapacidad').val(),
          //d.idcurso = $('#cbidcurso').val(),
          //d.estado = $('#cbestado').val(),
          //d.texto=$('#texto').val(),
          draw5e57ef520aa7c = d.draw;
          // console.log(d);
        },
        dataSrc: function(json) {
          var data = json.data;
          json.draw = draw5e57ef520aa7c;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for (var i = 0; i < data.length; i++){
            datainfo.push([
              (i + 1), data[i].curso, data[i].capacidad, data[i].nombre,
              '<a href="javascript:;"  class="btn-activar  ' + (data[i].estado == 0 ? '' : 'active') + ' " data-campo="estado"  data-id="' + data[i].idcriterio + '"> <i class="fa fa' + (data[i].estado == '1' ? '-check' : '') + '-circle-o fa-lg"></i> ' + estados5e57ef520aa7c[data[i].estado] + '</a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si"  pk="'+data[i].idcriterio+'"  nombre="'+data[i].nombre+'"  estado="'+data[i].estado+'"  idcurso="'+data[i].idcurso+'" idcompetencia="'+data[i].idcompetencia+'"  idcapacidad="'+data[i].idcapacidad+'" href="' + _sysUrlSitio_ + '/acad_criterios/editar/?id=' + data[i].idcriterio + '" data-titulo="' + tituloedit5e57ef520aa7c + '"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="' + data[i].idcriterio + '" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo
        },
        error: function(d) {
          console.log(d)
        }
      },
      "language": {
        "url": _sysIdioma_ == 'es' ? _sysUrlStatic_ + "/libs/datatable1.10/idiomas/es.json" : ''
      }
    });

    if($('select#selectchange0').length>0){
      addcategoria($('select#selectchange0'),$('#addselect'));
    }

    ventana_5e57ef520aa7c.on('keydown', '.textosearchlist', function(ev) {
        if (ev.keyCode === 13) refreshdatos5e57ef520aa7c();
    }).on('blur', '.textosearchlist', function(ev) {
        refreshdatos5e57ef520aa7c();
    }).on('change', '#idcategoria', function(ev){
      $(this).parent().nextAll().remove();
      $(this).removeAttr('name').addClass('selchange');
      $(this).attr('id','selectchange0')
      addcategoria($(this),$('#addselect'));
    }).on('change', 'select#idcompetencia', function(ev) {
      agregarcapacidad($('#addselect'));
    }).on('change','select.selchange',function(ev){
      ev.preventDefault(); 
      $(this).parent().nextAll().remove();       
      addcategoria($(this),$('#addselect'));
    }).on('change','select#idcapacidad',function(ev){
       refreshdatos5e57ef520aa7c();
    }).on('change','select#idcurso',function(ev){
      agregarcapacidad($('#addselect'))
    })

    ventana_5e57ef520aa7c.on('click', '.btn-eliminar', function() {
      var id = $(this).attr('data-id');
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action'); ?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept'); ?>',
        cancelButton: '<?php echo JrTexto::_('Cancel'); ?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function() {
          var data = new FormData()
          data.append('idcriterio', id);
          __sysAyax({
            fromdata: data,
            url: _sysUrlBase_ + 'json/acad_criterios/eliminar',
            callback: function(rs) {
              if (rs.code == 200) {
                tabledatos5e57ef520aa7c.ajax.reload();
              }
            }
          });
        }
      })
    }).on('click', '.subirfile', function(ev) {
      __subirfile({
        file: $(this),
        dataurlsave: 'json/acad_criterios/setcampo',
        'masvalores': {
          idcriterio: $(this).attr('idpk'),
          campo: 'imagen'
        }
      });
    }).on('click', '.btn-activar', function() {
      _this = $(this);
      var activo = 1;
      if (_this.hasClass('active')) {
        activo = 0;
      }
      var data = new FormData();
      data.append('idcriterio', _this.attr('data-id'));
      data.append('campo', _this.attr('data-campo'));
      data.append('valor', activo);
      __sysAyax({
        fromdata: data,
        url: _sysUrlBase_ + 'json/acad_criterios/setcampo',
        showmsjok: true,
        callback: function(rs) {
          if (rs.code == 200) {
            tabledatos5e57ef520aa7c.ajax.reload();
          }
        }
      });
    }).on('click', '.btnvermodal', function(e){
      e.preventDefault();        
      var el=$(this);
      _modalcriterios(el); 
    })


    var _modalcriterios=function(el){
        var titulo = '<?php echo JrTexto::_('Criteria') ?> / <?=JrTexto::_("Performance")?>';
        var frm=$('#frm<?php echo $idgui; ?>').clone();
        
        var idcurso=el.attr('idcurso')||$('#addselect select#idcurso').val()||'';
        var idcompetencia=el.attr('idcompetencia')||$('#addselect select#idcompetencia').val()||'-1';
        var idcapacidad=el.attr('idcapacidad')||$('#addselect select#idcapacidad').val()||'-1';
        // frm.find('#addfrmselect').html(cbo.html());
        var _md = __sysmodal({'html': frm,'titulo': titulo});        
        var cbo=$('#addselect').clone();
        cbo.children('div.col-md-4').removeClass('col-md-4').addClass('col-md-6');      
        _md.find('#addfrmselect').html(cbo.html());
        _md.find('input#idcriterio').val(el.attr('pk')||'');
        _md.find('select#idcurso').children('option').first().remove();
        _md.find('select#idcurso').val(idcurso);
        _md.find('select#idcapacidad').children('option').first().remove();
        _md.find('select#idcapacidad').attr('required','required');
        _md.find('select#idcurso').attr('required','required');
        _md.find('select#idcompetencia').attr('required','required');
        _md.find('select#idcapacidad').val(idcapacidad);
        //_md.find('select#idcompetencia').children('option').first().remove();  
        var estado=el.attr('estado')||'0';
        var nombre=el.attr('nombre')||'';
        if(estado=='0'){
          _md.find('a.chkformulario').removeClass('fa-check-circle-o').addClass('fa-circle-o');
          _md.find('a.chkformulario').children('span').text('<?php echo JrTexto::_('Inactive') ?>');
          _md.find('a.chkformulario').children('input').val(0);
        }
        if(nombre!=''){
          _md.find('textarea[name="nombre"]').val(nombre);
        }

        $('#addselect').find('select').each(function(i,v){ var _v=$(v); _md.find('#'+_v.attr('id')||'uiu').val(_v.val()); })
        //_md.find('select#idcurso').val(idcurso);
        //_md.find('select#idcompetencia').val(idcompetencia);
        _md.on('change','select.selchange',function(ev){
          ev.preventDefault();
          var el=$(this);
          var id=el.attr('id');
          var eltmp=$('#addselect select#'+id);
          el.parent().nextAll().remove();
          eltmp.parent().nextAll().remove();
          addcategoria(el,_md.find('#addfrmselect'));
          eltmp.val(el.val());
          addcategoria(eltmp,$('#addselect'));
        }).on('change','select#idcompetencia',function(ev){
          ev.preventDefault();
          var el=$(this);
          var id=el.attr('id');
          agregarcapacidad(_md.find('#addfrmselect'));
          $('#addselect').find('select#idcompetencia').val(el.val());
          agregarcapacidad($('#addselect'));
        }).on('change','select#idcategoria',function(ev){
          ev.preventDefault();
          var el=$(this);
          var eltmp=$('#addselect select#idcategoria');
          el.parent().nextAll().remove();
          eltmp.parent().nextAll().remove();
          el.removeAttr('name').addClass('selchange');
          eltmp.removeAttr('name').addClass('selchange');
          el.attr('id','selectchange0');
          eltmp.attr('id','selectchange0');
          addcategoria(el,_md.find('#addfrmselect'));
          eltmp.val(el.val());
          addcategoria(eltmp,$('#addselect'));
        }).on('change','select#idcurso',function(ev){
          agregarcapacidad(_md.find('#addfrmselect'));   
          $('#addselect').find('select#idcurso').val($(this).val());  
          agregarcapacidad($('#addselect'));
        }).on('click','.chkformulario',function(ev){
          _this = $(this);           
          if(_this.hasClass('fa-check-circle-o')) {            
            _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
            _this.children('span').text('<?php echo JrTexto::_('Inactive'); ?>')
            _this.children('input').val(0);
          }else{            
            _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
            _this.children('span').text('<?php echo JrTexto::_('Active'); ?>');
            _this.children('input').val(1);
          }
        }).on('change','select#idcapacidad',function(ev){
          $('#addselect select#idcapacidad').val($(this).val());
          $('#addselect select#idcapacidad').trigger('change');
        }).on('submit','form.formventana',function(ev){
            ev.preventDefault();
            var id='_frmtmp_'+__idgui();
            $(this).attr('id',id);
            var fele = document.getElementById(id);
            var data=new FormData(fele);
            __sysAyax({ 
              fromdata:data,
              url:_sysUrlBase_+'json/acad_criterios/guardar',
              showmsjok:true,
              callback:function(rs){ tabledatos5e57ef520aa7c.ajax.reload(); __cerrarmodal(_md);  }
            });
        })
    }

    $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
        var el=$(this);
        _modalcriterios(el);        
    })
  });
</script>