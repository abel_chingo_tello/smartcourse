<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/recursos_colabasig_comentario">&nbsp;<?php echo JrTexto::_('Recursos_colabasig_comentario'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col-md-12">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="recursos_colabasig_comentario" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idrecursocomentario" id="idrecursocomentario" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idasignacion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdasignacion" name="idasignacion" required="required" class="form-control" value="<?php echo @$frm["idasignacion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idpersona');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdpersona" name="idpersona" required="required" class="form-control" value="<?php echo @$frm["idpersona"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Comentario');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtComentario" name="comentario" required="required" class="form-control" value="<?php echo @$frm["comentario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Fecha');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtFecha" name="fecha" required="required" class="form-control" value="<?php echo @$frm["fecha"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('File');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtFile" name="file" required="required" class="form-control" value="<?php echo @$frm["file"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveRecursos_colabasig_comentario" type="submit" tb="recursos_colabasig_comentario" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('recursos_colabasig_comentario'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/recursos_colabasig_comentario/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/recursos_colabasig_comentario');
                }
              }
          });
       }
  })
})
</script>

