<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/tareas">&nbsp;<?php echo JrTexto::_('Tareas'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="tareas" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idtarea" id="idtarea" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tipo');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTipo" name="tipo" required="required" class="form-control" value="<?php echo @$frm["tipo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idcurso');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdcurso" name="idcurso" required="required" class="form-control" value="<?php echo @$frm["idcurso"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idsesion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdsesion" name="idsesion" required="required" class="form-control" value="<?php echo @$frm["idsesion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idpestania');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdpestania" name="idpestania" required="required" class="form-control" value="<?php echo @$frm["idpestania"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idalumno');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdalumno" name="idalumno" required="required" class="form-control" value="<?php echo @$frm["idalumno"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Recurso');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtRecurso" name="recurso" required="required" class="form-control" value="<?php echo @$frm["recurso"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tiporecurso');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTiporecurso" name="tiporecurso" required="required" class="form-control" value="<?php echo @$frm["tiporecurso"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nota');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNota" name="nota" required="required" class="form-control" value="<?php echo @$frm["nota"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Notabaseen');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNotabaseen" name="notabaseen" required="required" class="form-control" value="<?php echo @$frm["notabaseen"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Iddocente');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIddocente" name="iddocente" required="required" class="form-control" value="<?php echo @$frm["iddocente"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idproyecto');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdproyecto" name="idproyecto" required="required" class="form-control" value="<?php echo @$frm["idproyecto"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Descripcion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtDescripcion" name="descripcion" required="required" class="form-control" value="<?php echo @$frm["descripcion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Fecha calificacion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtFecha_calificacion" name="fecha_calificacion" required="required" class="form-control" value="<?php echo @$frm["fecha_calificacion"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveTareas" type="submit" tb="tareas" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('tareas'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/tareas/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/tareas');
                }
              }
          });
       }
  })
})
</script>

