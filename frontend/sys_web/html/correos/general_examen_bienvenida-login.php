<?php
// $this->Asunto, $this->mensaje, $this->destinatario,$entidadEmpresa
$titulo = $this->Asunto;
$mensaje = $this->mensaje;
// $destinatario = $this->destinatario;
$entidadEmp = $this->entidadEmpresa;
$_img = '';
$_colorprincipal = 'background: #fff;';
$_colorsecundario = 'background: #bb5b21;';
$_colortexto = 'color:#f9f3f3;';
if (!empty($entidadEmp)) {
    if (!empty($entidadEmp['dirlogo'])) {
        // $oCorreo->addembedded_image($entidadEmp['dirlogo'],'logoimage');
        $_img = '<div><img style="max-width:175px; max-height:170px;" src="cid:logoimage" /></div>';
    }
    if (!empty($entidadEmp['colorprincipal'])) {
        $_colorprincipal = 'background: ' . $entidadEmp['colorprincipal'] . ';';
    }
    if (!empty($entidadEmp['colorsecundario'])) {
        $_colorsecundario = 'background: ' . $entidadEmp['colorsecundario'] . ';';
    }
    if (!empty($entidadEmp['colortexto'])) {
        $_colortexto = 'color: ' . $entidadEmp['colortexto'] . ';';
    }
}
$urlemail='https://smartknowledgeusa.skscourses.com/static/media/email/';
//$_img = '<div><img src="https://abaco.desarrollador.ml/static/media/email/logo.png" style="max-width:175px; max-height:170px;" src="cid:logoimage" /></div>';

?>

<html>

<head>
    <title><?php echo $this->Asunto; ?></title>
</head>
<body  class="ecxdeviceWidth" style="text-align: center; padding: 1ex;" >
<style>
    table {
    border-collapse:collapse;
    }

    table[class="ecxdeviceWidth"] {
    width:600px;
    max-width:600px;
    }

    table[class="ecxbtn"] {
    font-size:21px;
    }

    table[class="ecxWidthTableInt_I"] {
    max-width:425px !important;
    width:100% !important;
    text-align:left !important;
    }

    table[class="ecxWidthTableInt_D"] {
    width:100% !important;
    max-width:90px !important;
    text-align:right !important;
    }

    @media only screen and (max-width: 600px), screen and (max-device-width: 600px) {
    table[class="ecxdeviceWidth"] {
    width:440px !important;
    padding:0 !important;
    }
    table[class="ecxbtn"] {
    font-size:18px !important;
    }
    table[class="ecxWidthTableInt_I"] {
    width:440px !important;
    }
    table[class="ecxWidthTableInt_D"] {
    width:440px !important;
    text-align:left !important;
    }
    }

    @media only screen and (max-width: 479px), screen and (max-device-width: 479px) {
    table[class="ecxdeviceWidth"] {
    width:280px !important;
    padding:0 !important;
    margin: 0px !important; 
    }
    table[class="ecxWidthTableInt_I"] {
    width:280px !important;
    text-align:left !important;
    }
    table[class="ecxWidthTableInt_D"] {
    width:280px !important;
    text-align:left !important;
    }
    table[class="ecxdeviceWidth"] td[calss="centrar"]{
        width: 100% !important;
        text-align: center;
    }
    }
</style>

    <table class="ecxdeviceWidth" border="0" width="800" cellspacing="0" cellpadding="0" style="border-radius:15px;   width: 100% !important;  table-layout: fixed; background: #fff;" align="center">
        <tbody>
        <tr>
            <td>                
        <div>
        <div>
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400&display=swap" rel="stylesheet">            
            <div style=" background-color: #fff; padding-top: 30px; padding-bottom: 15px;  height:90% border-radius: 15px;">
                <table class="ecxdeviceWidth" width="100%"  border="0" cellspacing="0" cellpadding="0" >                            
                    <tr >
                        <td style="background-color: #35B7E8;  border-top-left-radius:15px;border-top-right-radius:15px;margin:0 auto;width: 100%;height: 37%;text-align: center;padding-top: 15px; text-align: center; padding: 1ex; " bgcolor="#35B7E8" >
                           <?= $_img; ?>
                        </td>
                    </tr>
                    <tr style="background-color: white;text-align: center; align-items: center;height: 63%;" bgcolor="#ffffff">
                        <td>
                            <div style="background-color: #35B7E8; margin:0 auto;width: 100%;height: 20%; text-align: center;padding-top: 15px;">                        
                                <h1 style="font-family: 'Montserrat', sans-serif;color: white;"><?php echo $this->pasarHtml('Correo Informativo');?></h1>
                            </div>

                            <div  style="background-color: white;text-align: center; align-items: center;height: 63%;">
                                <h1 style="font-family: 'Montserrat', sans-serif;color: #838383;">
                                    <?php if(!empty($mensaje["msjTitulo"])): ?>
                                        <?php echo $this->pasarHtml(!empty($mensaje["msjTitulo"])?$mensaje["msjTitulo"]:JrTexto::_("Untitled"))?>
                                    <?php else: ?>
                                        <?php echo $this->pasarHtml(!empty($titulo)?$titulo:JrTexto::_("Untitled")); ?>
                                    <?php endif; ?>
                                </h1>
                                <p style="font-size:15px;font-family: 'Montserrat', sans-serif;color: #838383;">
                                    <?php if(!empty($mensaje["msjAcceso"])): ?>
                                        <?php echo $this->pasarHtml(!empty($mensaje["msjAcceso"])?$mensaje["msjAcceso"]:JrTexto::_("No Access Message"));?>
                                    <?php else: ?>
                                        <?=JrTexto::_("Access to")?> <?php echo $this->pasarHtml(!empty($mensaje["producto"])?$mensaje["producto"]:JrTexto::_("Product not defined")); ?>
                                    <?php endif; ?>
                                </p>

                                <table border="0" cellspacing="0" cellpadding="0" style="max-width: 400px; margin: auto;">
                                    <tr>
                                        <td style="text-align: right;" class="centrar">
                                            <h3 style="font-family: 'Montserrat', sans-serif;color: #35B7E8; margin: 0; padding: 0.5ex; "><?php echo $this->pasarHtml('Usuario');?>: </h3>
                                        </td>
                                        <td style="text-align: left;" class="centrar">
                                            <h3 style="font-family: 'Montserrat', sans-serif;color: #838383; margin: 0; padding: 1ex; "><?php echo $this->pasarHtml(!empty($mensaje["usuario"])?$mensaje["usuario"]:JrTexto::_("no user")); ?></h3>                                            
                                        </td>    
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;" class="centrar">
                                            <h3 style="font-family: 'Montserrat', sans-serif;color: #35B7E8; margin: 0; padding: 0.5ex;"><?php echo $this->pasarHtml('Clave');?>: </h3>
                                        </td>
                                        <td style="text-align: left;" class="centrar">
                                            <h3 style="font-family: 'Montserrat', sans-serif;color: #838383; margin: 0; padding: 1ex;" ><?php echo $this->pasarHtml(!empty($mensaje["clave"])?$mensaje["clave"]:JrTexto::_("no key")); ?></h3>                                           
                                        </td>
                                    <tr>
                                        <td colspan="2" style="text-align: center;">
                                            <h3 style="font-family: 'Montserrat', sans-serif;color: #35B7E8;"><?php echo $this->pasarHtml(!empty($mensaje["producto"])?$mensaje["producto"]:'Sin producto'); ?>: </h3>
                                            <a href="<?php echo !empty($mensaje["url"])?$mensaje["url"]:URL_BASE; ?>" style="font-family:  'Montserrat', sans-serif;color: #838383;"><?php echo !empty($mensaje["url"])?$mensaje["url"]:URL_BASE; ?></a>
                                        </td>
                                    </tr>
                                </table>                               

                            </div>
                            
                        </td>
                    </tr>                    
                    <tr >
                        <td style="justify-content: center;align-items: center;text-align: center; font-size:0.6rem!important; font-family: 'Montserrat', sans-serif;color: #838383;">
                            <br>
                            <h3><?php echo $this->pasarHtml(JrTexto::_("need help?").' '.JrTexto::_("Contact us")); ?>:</h3>
                            <h3><?php echo $this->pasarHtml(!empty($this->empresa["correoempresa"]) ? $this->empresa["correoempresa"] : 'info@smartknowledge-solutions.com'); ?></h3>
                            <h3><?php echo $this->pasarHtml(!empty($this->empresa["telefono"]) ? $this->empresa["telefono"] : '(+51) 960 177 900'); ?></h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <table class="ecxdeviceWidth" border="0" width="400" cellspacing="0" cellpadding="0" style="max-width:600px;" align="center">
                            <tr>
                                <td style="text-align: center;">
                                <a href="https://www.facebook.com/Smart-Knowledge-Solutions-SKS-101471175078643" style="text-decoration-line: none;">
                                    <img width="35px" height="35px;" border="0" src="<?php echo $urlemail; ?>facebook.png" style="width: 30px; max-height: 30px; margin: 0 1.5ex;">
                                </a>
                                </td>
                                <td style="text-align: center;">
                                <a href="https://www.instagram.com/smartknowledgesolutions/" style="text-decoration-line: none;">
                                    <img  width="35px" height="35px;" border="0" src="<?php echo $urlemail; ?>instagram.png" style="width: 30px; max-height: 30px; margin: 0 1.5ex;">
                                </a>
                                </td>
                                <td style="text-align: center;">
                                <a href="https://www.linkedin.com/company/smart-knowledge-solution-llc" style="text-decoration-line: none;">
                                    <img  width="35px" height="35px;" border="0" src="<?php echo $urlemail; ?>linkedin.png" style="width: 40px; max-height: 30px; margin: 0 1.5ex;">
                                </a>
                                </td><td style="text-align: center;">
                                <a href="https://www.youtube.com/channel/UCiH_mTxmpuKBqgXZAd0euhA" style="text-decoration-line: none;">
                                    <img  width="35px" height="35px;" border="0" src="<?php echo $urlemail; ?>youtube.png" style="width: 30px; max-height: 30px; margin: 0 1.5ex;">
                                </a>
                                </td>
                            </tr>                            
                            </table><br><br>
                        </td>
                    </tr>
                     <tr>
                        <td style="text-align: center; padding: 1ex;"><br>
                            <h5 style="font-family: 'Montserrat', sans-serif;color: #838383;"> <?php echo $this->pasarHtml(JrTexto::_('This email was generated automatically. please do not reply to this email'));?>.</h5>
                            <h5 style="font-family: 'Montserrat', sans-serif;color: #838383;"><?php echo $this->pasarHtml(JrTexto::_('When you log in it is recommended to read the requirements so that your interaction does not present complications'));?>.</h5>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #35B7E8;  border-bottom-left-radius:15px;border-bottom-right-radius:15px;margin:0 auto;width: 100%;height: 37%;text-align: center;padding-bottom: 15px; text-align: center; padding: 1ex; " bgcolor="#35B7E8">
                        </td>
                    </tr>
                </table>         
        </div>
    </div>    
</td></tr></tbody></table>
</body>
</html>