 <?php
defined('RUTA_BASE') or die();
?>
<html>

<head>
    <title><?php echo $this->Asunto; ?></title>
</head>
<body  class="ecxdeviceWidth" style="text-align: center; padding: 1ex;" >
<style>
table {
border-collapse:collapse;
}

table[class="ecxdeviceWidth"] {
width:600px;
max-width:600px;
}

table[class="ecxbtn"] {
font-size:21px;
}

table[class="ecxWidthTableInt_I"] {
max-width:425px !important;
width:100% !important;
text-align:left !important;
}

table[class="ecxWidthTableInt_D"] {
width:100% !important;
max-width:90px !important;
text-align:right !important;
}

@media only screen and (max-width: 600px), screen and (max-device-width: 600px) {
table[class="ecxdeviceWidth"] {
width:440px !important;
padding:0 !important;
}
table[class="ecxbtn"] {
font-size:18px !important;
}
table[class="ecxWidthTableInt_I"] {
width:440px !important;
}
table[class="ecxWidthTableInt_D"] {
width:440px !important;
text-align:left !important;
}
}

@media only screen and (max-width: 479px), screen and (max-device-width: 479px) {
table[class="ecxdeviceWidth"] {
width:280px !important;
padding:0 !important;
margin: 0px !important; 
}
table[class="ecxWidthTableInt_I"] {
width:280px !important;
text-align:left !important;
}
table[class="ecxWidthTableInt_D"] {
width:280px !important;
text-align:left !important;
}
}
</style>

    <table class="ecxdeviceWidth" border="0" width="800" cellspacing="0" cellpadding="0" style="border-radius:15px;   width: 100% !important;  table-layout: fixed; background: #fff;" align="center">
        <tbody>
        <tr>
            <td>                
        <div>
        <div>
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400&display=swap" rel="stylesheet">            
            <div style=" background-color: #fff; padding-top: 30px; padding-bottom: 15px;  height:90% border-radius: 15px;">
                <table class="ecxdeviceWidth" width="100%"  border="0" cellspacing="0" cellpadding="0" >                            
                    <tr >
                        <td style="background-color: #35B7E8;  border-top-left-radius:15px;border-top-right-radius:15px;margin:0 auto;width: 100%;height: 37%;text-align: center;padding-top: 15px; text-align: center; padding: 1ex; " bgcolor="#35B7E8" >
                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td></td>
                                    <td width="150" style="text-align: center;">
                                         <div style="width: 100px; max-width: 150px">
                                         <?php echo !empty($this->logoempresa) ? ('<img src="' . $this->logoempresa . '" width="100"  style="text-align: center;" style="max-width:150px; max-height:100px;" alt="logo de empresa">') : ''; ?>
                                         </div> 
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: white;text-align: center;justify-content: center;    border-bottom-left-radius: 15px;border-bottom-right-radius: 15px;align-items: center;height: 63%;" bgcolor="#ffffff">
                        <td>
                            <h1 style="font-size:1.7rem !important;font-family: 'Montserrat', sans-serif;color: #838383;"><?php echo $this->pasarHtml(!empty($this->infoexamen["titulo"]) ? $this->infoexamen["titulo"] : $this->Asunto); ?></h1>
                            <h1 style="font-size:1.7rem !important;font-family: 'Montserrat', sans-serif;color: #838383;"><?php echo $this->pasarHtml(JrTexto::_('Course')); ?>: <?php echo $this->pasarHtml(@$this->email_curso); ?></h1>
                            <h3 style="font-size:1rem !important; font-family: 'Montserrat', sans-serif;color: #838383;"><?php 
                                $Student_=JrTexto::_('Student_');
                                $Student_=$Student_=='Student_'?'Student':$Student_;
                                echo $this->pasarHtml($Student_); ?>: <?php echo  $this->pasarHtml(@$this->email_estudiante); ?></h3>

                            <div style="font-size:1rem !important; justify-content: center;align-items: center;background-color:#F3F3F3; color: #838383;text-align: center;width: 100%; max-width:700px; font-family: 'Montserrat', sans-serif;padding: 8px;margin:0 auto">
                                    <?php echo $this->mensaje; ?>
                            </div>

                        </td>
                    </tr>                    
                    <tr >
                        <td style="justify-content: center;align-items: center;text-align: center; font-size:0.6rem!important; font-family: 'Montserrat', sans-serif;color: #838383;">
                            <br>
                            <h3><?php echo $this->pasarHtml(JrTexto::_('need help?') . " " . JrTexto::_('Contact Us')); ?>:</h3>
                            <h3><?php echo $this->pasarHtml(!empty($this->empresa["correoempresa"]) ? $this->empresa["correoempresa"] : 'info@smartknowledge-solutions.com'); ?></h3>
                            <h3><?php echo $this->pasarHtml(!empty($this->empresa["telefono"]) ? $this->empresa["telefono"] : '(+51) 960 177 900'); ?></h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <table class="ecxdeviceWidth" border="0" width="400" cellspacing="0" cellpadding="0" style="max-width:600px;" align="center">
                            <tr>
                                <td style="text-align: center;">
                                <a href="https://www.facebook.com/Smart-Knowledge-Solutions-SKS-101471175078643" style="text-decoration-line: none;">
                                    <img width="35px" height="35px;" border="0" src="<?php echo URL_BASE; ?>static/media/email/facebook.png" style="width: 30px; max-height: 30px; margin: 0 1.5ex;">
                                </a>
                                </td>
                                <td style="text-align: center;">
                                <a href="https://www.instagram.com/smartknowledgesolutions/" style="text-decoration-line: none;">
                                    <img  width="35px" height="35px;" border="0" src="<?php echo URL_BASE; ?>static/media/email/instagram.png" style="width: 30px; max-height: 30px; margin: 0 1.5ex;">
                                </a>
                                </td>
                                <td style="text-align: center;">
                                <a href="https://www.linkedin.com/company/smart-knowledge-solution-llc" style="text-decoration-line: none;">
                                    <img  width="35px" height="35px;" border="0" src="<?php echo URL_BASE; ?>static/media/email/linkedin.png" style="width: 40px; max-height: 30px; margin: 0 1.5ex;">
                                </a>
                                </td><td style="text-align: center;">
                                <a href="https://www.youtube.com/channel/UCiH_mTxmpuKBqgXZAd0euhA" style="text-decoration-line: none;">
                                    <img  width="35px" height="35px;" border="0" src="<?php echo URL_BASE; ?>static/media/email/youtube.png" style="width: 30px; max-height: 30px; margin: 0 1.5ex;">
                                </a>
                                </td>
                            </tr>                            
                            </table><br><br>
                        </td>
                    </tr>
                     <tr>
                        <td style="text-align: center; padding: 1ex;">
                            <h5 style="font-family: 'Montserrat', sans-serif;color: #838383;">
                                <?php echo $this->pasarHtml(JrTexto::_('This email was generated automatically. please do not reply to this email')); ?>.
                            </h5>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #35B7E8;  border-bottom-left-radius:15px;border-bottom-right-radius:15px;margin:0 auto;width: 100%;height: 37%;text-align: center;padding-bottom: 15px; text-align: center; padding: 1ex; " bgcolor="#35B7E8">
                        </td>
                    </tr>
                </table>         
        </div>
    </div>    
</td></tr></tbody></table>
</body>
</html>