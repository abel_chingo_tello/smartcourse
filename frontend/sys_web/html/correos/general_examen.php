<?php
defined('RUTA_BASE') or die();
?>
<style>
table {
border-collapse:collapse;
}

table[class="ecxdeviceWidth"] {
width:600px;
max-width:600px;
}

table[class="ecxbtn"] {
font-size:21px;
}

table[class="ecxWidthTableInt_I"] {
max-width:425px !important;
width:100% !important;
text-align:left !important;
}

table[class="ecxWidthTableInt_D"] {
width:100% !important;
max-width:90px !important;
text-align:right !important;
}

@media only screen and (max-width: 600px), screen and (max-device-width: 600px) {
table[class="ecxdeviceWidth"] {
width:440px !important;
padding:0 !important;
}
table[class="ecxbtn"] {
font-size:18px !important;
}
table[class="ecxWidthTableInt_I"] {
width:440px !important;
}
table[class="ecxWidthTableInt_D"] {
width:440px !important;
text-align:left !important;
}
}

@media only screen and (max-width: 479px), screen and (max-device-width: 479px) {
table[class="ecxdeviceWidth"] {
width:280px !important;
padding:0 !important;
}
table[class="ecxWidthTableInt_I"] {
width:280px !important;
text-align:left !important;
}
table[class="ecxWidthTableInt_D"] {
width:280px !important;
text-align:left !important;
}
}
</style>

<table style="  width: 100% !important;  table-layout: fixed; background: #fff;" align="center">
    <caption >
        <?php echo !empty($this->logoempresa)?('<img src="cid:logoempresa" style="max-width: 150px; max-height: 150px; text-align: center;" alt="logo de empresa">'):'';?>
        <br><h2 style="text-align: center;"><?php echo !empty($this->titulo)?$this->titulo:'';?></h2></caption>
    <tbody>
        <tr>
            <td>
                <table class="ecxdeviceWidth" style="max-width:800px;" align="center">
                <tbody>
                <?php if(!empty($this->Asunto)){?>
                    <tr><td colspan="3" style="background: #3c8dbc; color: #f9f3f3; text-align: center; padding: 2ex;  /* padding: 6ex; */">
                        <?php echo @$this->Asunto; ?><br></td></tr>
                <?php } ?>
                <tr>                   
                    <td style="padding-bottom:8px; color:#353e4a; background-color: #ffffff;">
                     <?php echo $this->mensaje; ?>                                  	
                    </td>
                </tr>
                <tr style=" background: #17a2b8;color: #000;"><td colspan="3">
                    <h2><?=JrTexto::_("Need help?")?></h2>                    
                    <?=JrTexto::_("Write to our email")?> <b><?php echo !empty($this->empresa["correoempresa"])?$this->empresa["correoempresa"]:'info@eduktvirtual.com';?></b><br>
                    <?=JrTexto::_("Call us at")?> <b><?php echo !empty($this->empresa["telefono"])?$this->empresa["telefono"]:'+51 1-435-3738';?></b><br>
                    <?=JrTexto::_("Contact us on whatsapp at")?> <b><?php echo !empty($this->empresa["telefono"])?$this->empresa["telefono"]:'+51 1-435-3738';?></b><br>
                    <?=JrTexto::_("Visit us here")?> : <a href="<?php echo $this->documento->getUrlBase(); ?>" style="color: #ffe520;"><b><?php echo $this->documento->getUrlBase(); ?></b></a>
                </td>
                </tr>
                <tr>                    
                    <td colspan="3" style="color: #717175;font-size: 12px;padding: 1ex;text-align: center;background: #ecece9;">
                    <?php echo $this->pasarHtml(JrTexto::_('This email has been generated automatically. Please do not reply to this email'))?>.</td>                               
                </tr>
            </tbody>
          	</table>
            </td>
        </tr>
    </tbody>
</table>