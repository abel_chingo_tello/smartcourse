<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/sinlogin_persona">&nbsp;<?php echo JrTexto::_('Sinlogin_persona'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col-md-12">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="sinlogin_persona" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idpersona" id="idpersona" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nombres');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNombres" name="nombres" required="required" class="form-control" value="<?php echo @$frm["nombres"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Telefono');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTelefono" name="telefono" required="required" class="form-control" value="<?php echo @$frm["telefono"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Correo');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtCorreo" name="correo" required="required" class="form-control" value="<?php echo @$frm["correo"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveSinlogin_persona" type="submit" tb="sinlogin_persona" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('sinlogin_persona'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/sinlogin_persona/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/sinlogin_persona');
                }
              }
          });
       }
  })
})
</script>

