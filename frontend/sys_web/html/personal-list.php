<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_('Back')?></a></li> 
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li-->        
        <li class="active">&nbsp;<?php echo JrTexto::_("Personal"); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
  .pd2{margin-bottom: 1ex;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
    <div class="row">                                             
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2">
        <label class="control-label"><?php echo JrTexto::_('Role'); ?></label>
        <div class="cajaselect">
            <input type="hidden" id="hiddenfkcbrol" value="<?php echo (isset($_REQUEST['idrol']) && !empty($_REQUEST['idrol'])) ? $_REQUEST['idrol'] : 2 ?>" />          
            <select id="idrol" name="rol" class="form-control" >                  
              <?php if(!empty($this->fkrol))
                foreach ($this->fkrol as $fkrol) { 
                  if($this->idRolUser==10){
                    if($fkrol["idrol"]==1||$fkrol["idrol"]==10||$fkrol["idrol"]==5||$fkrol["idrol"]==4||$fkrol["idrol"]==3) continue;
                  }else if($fkrol["idrol"]==3) continue ?>
                  <option value="<?php echo $fkrol["idrol"]?>" <?php echo $fkrol["idrol"]==@$this->idrol?"selected":""; ?> ><?php echo ucfirst($fkrol["rol"]); ?></option><?php } ?>
            </select>
        </div>
      </div>
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2">
        <label class="control-label"><?php echo JrTexto::_('DRE'); ?></label>
        <div class="cajaselect">
          <select id="iddre" name="iddre" class="form-control" >
            <option value="0"><?php echo JrTexto::_('Only'); ?></option>
              <?php 
              if(!empty($this->dress))
              foreach ($this->dress as $fk) { ?><option value="<?php echo $fk["ubigeo"]?>" ><?php echo $fk["descripcion"] ?></option><?php } ?>
          </select>
        </div>
      </div>
                                                                                                                  
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2" >
        <label class="control-label"><?php echo JrTexto::_('Ugel'); ?></label>
        <div class="cajaselect">
          <select id="idugel" name="idugel" class="form-control" >
            <option value="0"><?php echo JrTexto::_('Only'); ?></option>
          </select>
        </div>
      </div>

      <div class="form-group  col-xs-6 col-sm-4 col-md-3 pd2" >
        <label class="control-label"><?php echo JrTexto::_('Local/IIEE'); ?></label>
        <div class="cajaselect">
          <select id="idlocal" name="idlocal" class="form-control" >
            <option value="0"><?php echo JrTexto::_('Only'); ?></option>
          </select>
        </div>
      </div>
    
      <div class="form-group  col-xs-6 col-sm-4 col-md-3 pd2" >
        <label class="control-label"><?php echo JrTexto::_('Course'); ?></label>
        <div class="cajaselect">
          <select id="idcurso" name="idcurso" class="form-control" >
          <option value=""><?php echo JrTexto::_('All Courses'); ?></option>
              <?php if(!empty($this->fkcursos))
              foreach ($this->fkcursos as $fk) { ?><option value="<?php echo $fk["idcurso"]?>" ><?php echo $fk["nombre"] ?></option><?php } ?>
          </select>
        </div>
      </div>

      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2">
      <label><?php echo  ucfirst(JrTexto::_("Gender"))?></label>                   
        <div class="cajaselect">
          <select id="sexo" name="sexo" class="form-control" >
            <option value=""><?php echo JrTexto::_('All Gender'); ?></option>
              <?php 
              if(!empty($this->fksexo))
              foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
               <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group  col-xs-6 col-sm-4 col-md-3 pd2" >
        <label><?php echo  ucfirst(JrTexto::_("Marital status"))?></label>
         <div class="cajaselect">
          <select id="estado_civil" name="estado_civil" class="form-control" >
            <option value=""><?php echo JrTexto::_('All marital status'); ?></option>
              <?php 
              if(!empty($this->fkestado_civil))
              foreach ($this->fkestado_civil as $fkestado_civil) { ?><option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> ><?php echo $fkestado_civil["nombre"] ?></option><?php } ?>
            </select>
        </div>
      </div>                                                          
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2" >
        <label><?php echo  ucfirst(JrTexto::_("Status"))?></label>
        <div class="cajaselect">
          <select name="estado" id="estado" class="form-control">
              <option value=""><?php echo JrTexto::_('all status'); ?></option>
              <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
              <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
          </select>
        </div>
      </div>


              <div class="form-group col-xs-6 col-sm-6 col-md-6">
                 <label><?php echo  ucfirst(JrTexto::_("Search"))." : N° Doc/".JrTexto::_("Name");?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>

            <div class="form-group col-md-6 col-sm-6 col-sm-12 text-center"> 
            <div style="width: 100%"><br></div>            
               <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/personal/agregar" data-titulo="<?php echo ucfirst(JrTexto::_('Add')) ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
               <a class="btn btn-warning btnvermodal" nuser="varios" href="<?php echo $this->documento->getUrlSitio();?>/personal/importar" data-titulo="<?php echo ucfirst(JrTexto::_('Import'))." ".JrTexto::_('Personal') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Import')?></a>
              </div>
            </div>
          </div>
      </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>                  
                    <th><?php echo JrTexto::_("Name") ;?></th>                    
                    <!--th><?php echo JrTexto::_("Gender") ;?></th-->
                   <th style="min-width: 200px;"><?php echo JrTexto::_("Contact") ;?></th> 
                  <th><?php echo JrTexto::_("Photo") ;?></th>
                  <th><?php echo JrTexto::_("Status") ;?></th>
                  <th><?php echo JrTexto::_("Is Demo") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5dae03704f6f0='';
function refreshdatos5dae03704f6f0(){
    tabledatos5dae03704f6f0.ajax.reload();
}
$(document).ready(function(){  

var _imgdefecto='static/media/usuarios/user_avatar.jpg';
var ini=true;

  var estados5dae03704f6f0={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5dae03704f6f0='<?php echo ucfirst(JrTexto::_("personal"))." - ".JrTexto::_("edit"); ?>';
  var draw5dae03704f6f0=0;
  var ventana_5dae03704f6f0=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5dae03704f6f0=ventana_5dae03704f6f0.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/persona',
        type: "post",                
        data:function(d){
            var idrol=ventana_5dae03704f6f0.find('#idrol').val();
            d.json=true;                
            d.tipodoc=ventana_5dae03704f6f0.find('#tipodoc').val();
            d.fechanac=ventana_5dae03704f6f0.find('#datefechanac').val();
            d.sexo=ventana_5dae03704f6f0.find('#sexo').val();
            d.estado_civil=ventana_5dae03704f6f0.find('#estado_civil').val();
            d.estado=ventana_5dae03704f6f0.find('#estado').val();
            //d.ubigeo=ventana_5dae03704f6f0.find('#ubigeo').val();           
            d.rol=idrol;
            d.texto=ventana_5dae03704f6f0.find('input.textosearchlist').val();
            if(!ventana_5dae03704f6f0.find('select#iddre').hasClass('hide')) d.iddre=ventana_5dae03704f6f0.find('#iddre').val();
            if(!ventana_5dae03704f6f0.find('select#idugel').hasClass('hide')) d.idugel=ventana_5dae03704f6f0.find('#idugel').val();
            if(!ventana_5dae03704f6f0.find('select#idlocal').hasClass('hide')) d.idlocal=ventana_5dae03704f6f0.find('#idlocal').val();
            if(!ventana_5dae03704f6f0.find('select#idcurso').hasClass('hide')) d.idcurso=ventana_5dae03704f6f0.find('#idcurso').val();
            draw5dae03704f6f0=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5dae03704f6f0;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){             
             var varfoto=data[i].foto||'static/media/usuarios/user_avatar.jpg';
              var ifoto=varfoto.lastIndexOf("static/");
              if(ifoto>=0) varfoto=_sysUrlBase_+varfoto.substring(ifoto);
              if(ifoto==-1){
                var ifoto=varfoto.lastIndexOf("/");
                var ifoto2=varfoto.lastIndexOf(".");
                if(ifoto>=0 && ifoto2>=0) varfoto=_sysUrlBase_+'static/media/usuarios/'+varfoto.substring(ifoto);
                else if(ifoto2>=0) varfoto=_sysUrlBase_+'static/media/usuarios/'+varfoto;
                else varfoto=_sysUrlBase_+'static/media/usuarios/user_avatar.jpg';
              }
           


            varfoto='<img src="'+varfoto+'" class="img-thumbnail" style="max-height:70px; max-width:50px;">';            
            var tel=data[i].telefono!=''?('<i class="fa fa-phone"></i> '+data[i].telefono):'';
            var cel=(data[i].celular!=''&&tel!='')?(tel+'/ <i class="fa fa-mobile"></i> '+data[i].celular+'<br>'):'';
            cel=(data[i].celular!=''&&tel=='')?('<i class="fa fa-mobile"></i> '+data[i].celular+'<br>'):cel;
            var ema=data[i].email!=''?('<i class="fa fa-envelope"></i> '+data[i].email)+'<br>':'';
               ini=false;                              
            datainfo.push([             
              (i+1), (data[i].dni+'<br>'+data[i].ape_paterno+' '+data[i].ape_materno+','+data[i].nombre),
              cel+ema+'<i class="fa fa-user"></i> '+data[i].usuario,  varfoto,
              '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idpersona+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5dae03704f6f0[data[i].estado]+'</a>',
               '<a href="javascript:;"  class="btn-activar  '+(data[i].esdemo==0?'':'active')+' " data-campo="esdemo"  data-id="'+data[i].idpersona+'"> <i class="fa fa'+(data[i].esdemo=='1'?'-check':'')+'-circle-o fa-lg"></i></a>',

              '<a class="btn btn-sm btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/personal/perfil/?idpersona='+data[i].idpersona+'&rol='+(ventana_5dae03704f6f0.find('#idrol').val())+'" data-titulo="'+tituloedit5dae03704f6f0+'"><i class="fa fa-eye"></i></a>  <a class="btn btn-sm btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/personal/editar/?idpersona='+data[i].idpersona+'&rol='+(ventana_5dae03704f6f0.find('#idrol').val())+'" data-titulo="'+tituloedit5dae03704f6f0+'"><i class="fa fa-edit"></i></a>  <a class="btn btn-sm btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/personal/cambiarclave/?idpersona='+data[i].idpersona+'&amp;rol='+(ventana_5dae03704f6f0.find('#idrol').val())+'" data-titulo="<?php echo JrTexto::_('Change Password');?>"><i class="fa fa-key"></i></a> <a class="btn btn-sm btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/personal/roles/?idpersona='+data[i].idpersona+'" data-titulo="Roles"><i class="fa fa-user-secret"></i></a> <a class="btn-eliminarhistorial btn btn-sm" href="javascript:;" data-titulo="<?php echo JrTexto::_('Delete history');?>" data-id="'+data[i].idpersona+'"><i class="fa fa-history"></i></a> <a class="btn-eliminar btn btn-sm" href="javascript:;" data-id="'+data[i].idpersona+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5dae03704f6f0.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5dae03704f6f0();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5dae03704f6f0(); 
  }).on('change','#idrol',function(ev){
    var rol=$(this).val()||'';
    if(rol!='1' && rol!='9' && rol!='10'){ //2,8
     ventana_5dae03704f6f0.find('select#iddre').removeClass('hide').closest('.pd2').show();
     ventana_5dae03704f6f0.find('select#idugel').removeClass('hide').closest('.pd2').show();
     ventana_5dae03704f6f0.find('select#idlocal').removeClass('hide').closest('.pd2').show();
     ventana_5dae03704f6f0.find('select#idcurso').removeClass('hide').closest('.pd2').show();
     if(idrol==7){ 
       ventana_5dae03704f6f0.find('select#iddre').addClass('hide').closest('.pd2').hide();
     }else if(idrol==6||idrol==5||idrol==4){
        ventana_5dae03704f6f0.find('select#iddre').addClass('hide').closest('.pd2').hide();
        ventana_5dae03704f6f0.find('select#idugel').addClass('hide').closest('.pd2').hide();
     }
    }else{//docente  2
     ventana_5dae03704f6f0.find('select#iddre').addClass('hide').closest('.pd2').hide();
     ventana_5dae03704f6f0.find('select#idugel').addClass('hide').closest('.pd2').hide();
     ventana_5dae03704f6f0.find('select#idlocal').addClass('hide').closest('.pd2').hide();
     ventana_5dae03704f6f0.find('select#idcurso').addClass('hide').closest('.pd2').hide(); 
    }
    if(ini==false) refreshdatos5dae03704f6f0();
  }).on('change','#estado',function(ev){refreshdatos5dae03704f6f0();
  }).on('change','#sexo',function(ev){refreshdatos5dae03704f6f0();
  }).on('change','#estado_civil',function(ev){refreshdatos5dae03704f6f0();
  }).on('change','select#iddre',function(ev){    
    $sel=ventana_5dae03704f6f0.find('select#idugel');
    $sel.children('option:eq(0)').siblings().remove();
    if($(this).val()==''||$(this).val()=='') {
      return $sel.trigger('change');
    }
    var fd = new FormData();
    fd.append('iddre',$(this).val()); 
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/ugel',
      callback:function(rs){        
        var idsel=$sel.attr('idsel');
        dt=rs.data;
        $.each(dt,function(i,v){
          $sel.append('<option value="'+v.idugel+'" '+(v.idugel==idsel?'selected="selected"':'')+ '>'+v.idugel+" : "+ v.descripcion+'</option>');
        })
        $sel.trigger('change');
    }})
  }).on('change','select#idugel',function(ev){
    var $sel=ventana_5dae03704f6f0.find('select#idlocal');
    $sel.children('option:eq(0)').siblings().remove();
    if($(this).val()==''||$(this).val()=='0') {
      return $sel.trigger('change');
    }     
    var fd = new FormData();
    fd.append('idugel',$(this).val()); 
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/local',
      callback:function(rs){
        var idsel=$sel.attr('idsel');
        dt=rs.data;
        $.each(dt,function(i,v){
          $sel.append('<option value="'+v.idlocal+'" '+(v.idlocal==idsel?'selected="selected"':'')+ '>'+v.idlocal+" : "+ v.nombre+'</option>');
        })
        $sel.trigger('change');
    }})
  }).on('change','select#idlocal',function(ev){
    refreshdatos5dae03704f6f0();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5dae03704f6f0();

  })
  ventana_5dae03704f6f0.find('select#idrol').trigger('change'); 
  ventana_5dae03704f6f0.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idpersona',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/persona/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5dae03704f6f0.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){  __subirfile({file:$(this),dataurlsave:'json/persona/setcampo','masvalores':{idpersona:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idpersona',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/persona/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5dae03704f6f0.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var idrol=ventana_5dae03704f6f0.find('#idrol').val()||3;
    var url=url+'&idrol='+idrol;
    var titulo=$(this).attr('data-titulo')||'';    
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      tabledatos5dae03704f6f0.ajax.reload();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frmpersona(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              _md.attr('idrol',ventana_5dae03704f6f0.find('select#idrol').val());
              _md.attr('idugel',ventana_5dae03704f6f0.find('select#idugel').val());         
              _md.attr('idlocal',ventana_5dae03704f6f0.find('select#idlocal').val()); 
              frm_personaimport(_md);
            })
          });
       }           
    })   
  }).on("click",'.btn-eliminarhistorial',function(){
      var formData = new FormData();
      idpersona=$(this).attr('data-id');
      formData.append('idpersona',idpersona);
    
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete history ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           __sysAyax({ 
            fromdata:formData,
              url:_sysUrlBase_+'json/persona/borrarhistorial',
              showmsjok:true   
          });
        }
      }); 
    })
});
</script>