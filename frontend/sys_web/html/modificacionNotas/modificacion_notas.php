<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("notes"); ?>'
        });
        var __usuario = <?= json_encode($this->usuario) ?>
    </script>
<?php } ?>

<div class="container my-font-all">
    <div class="row my-x-center">
        <?php if ($this->usuario["idrol"] == 1) { ?>
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label for=""><?php echo JrTexto::_("groups"); ?></label>
                    <select class="form-control" name="" id="selectGrupos">
                    </select>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectCursos"><?php echo JrTexto::_("smartcourse"); ?></label>
                <select class="form-control" name="" id="selectCursos">
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectAlumnos"><?php echo JrTexto::_("students"); ?></label>
                <select class="form-control" name="" id="selectAlumnos">
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectExamenes"><?php echo JrTexto::_("exams"); ?></label>
                <select class="form-control" name="" id="selectExamenes">
                </select>
            </div>
        </div>
    </div>
    <div class="row col-sm-12 my-x-center">
        <div class="box-cont">
            <p class="my-font-1_5">
                <small><i  id="conf_examen"></i></small>
            </p>
            <div id="drawQuiz">
                <div id="div-msg" class="my-card my-shadow msg my-mg-bottom">
                    <?php echo JrTexto::_("loading"); ?>...
                </div>

            </div>
            <div class="col-sm-12">
                <button id="btn-save" class="btn btn-success my-center">
                        <?php echo JrTexto::_("save"); ?>
                </button>
            </div>
        </div>

    </div>

</div>
<!-- Modal crudNotas-->
<div class="modal fade my-modal soft-rw my-font-all" id="md-crudNotas" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">
                    <?php echo JrTexto::_("assign note"); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-no-scroll">
                <form id="form-add-crudNotas">
                    <input id="input-idcrudNotas" class="my-hide" type="text" disabled>
                    <div class="form-group">
                        <label for="detalle-crudNotas"><?php echo JrTexto::_("note"); ?></label>
                        <input required  pattern="^([0-9]{1,})((,|\.){0,1}[0-9]{1,}){0,1}" title="Solo n�meros enteros o decimales" type="text" name="nombre-crudNotas" id="input-nota" class="form-control" placeholder="<?php echo JrTexto::_('note')?>..."></input>
                        <!-- <input required type="text" name="detalle-crudNotas" id="detalle-crudNotas" class="form-control" placeholder="Detalle..."> -->
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12  col-md-6">
                        <div id="autor-btn" class="my-hide">
                            <button type="button" onclick="eliminarcrudNotas()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> <?php echo JrTexto::_("delete"); ?></button>
                            <button name="btn-save" type="submit" form="form-add-crudNotas" class="btn btn-primary"><?php echo JrTexto::_("save"); ?></button>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div id="default-btn" class="my-hide">
                            <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal"><?php echo JrTexto::_("close"); ?></button>
                        </div>
                    </div>
                    <div name="modo-crear" class="col-sm-12 my-hide">
                        <div class="row my-block">
                            <div class="my-float-r">
                                <button type="button" class="btn  btn-secondary" data-dismiss="modal"><?php echo JrTexto::_("cancel"); ?></button>
                                <button name="btn-save" type="submit" form="form-add-crudNotas" class="btn btn-primary"><?php echo JrTexto::_("save"); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(() => {
        const oCrudNotas = new CrudNotas();
        oCrudNotas.launch();
    });
</script>