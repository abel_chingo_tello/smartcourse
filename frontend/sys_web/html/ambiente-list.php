<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<div id="ventana_<?php echo $idgui; ?>">
<?php
if(!$ismodal){?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Classroom"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal', 
      text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?>', 
      href: '<?php echo $this->documento->getUrlSitio();?>/ambiente/agregar'
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>

<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_content">
        <div class="row">
          <div class="col-md-12">
            <div class="form-view">
              <div class="row">
                <div class="col">
                <div class="row">         
                    <div class="col table-responsive">
                        <table class="table table-striped table-hover">
                          <thead>
                            <tr class="headings">
                              <th>#</th>
                              <!--th><?php //echo JrTexto::_("Idlocal") ;?></th-->
                              <th><?php echo JrTexto::_("Number") ;?></th>
                              <th><?php echo JrTexto::_("Capacity") ;?></th>
                              <!--th><?php //echo JrTexto::_("Tipo") ;?></th-->
                              <th><?php echo JrTexto::_("State") ;?></th>
                              <!--th><?php // echo JrTexto::_("Turno") ;?></th-->
                              <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>

<script type="text/javascript">
var tabledatos5da47d301f032='';
function refreshdatos5da47d301f032(){
    tabledatos5da47d301f032.ajax.reload();
}
$(document).ready(function(){  
  __infoempresa();
  var estados5da47d301f032={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5da47d301f032='<?php echo ucfirst(JrTexto::_("ambiente"))." - ".JrTexto::_("edit"); ?>';
  var draw5da47d301f032=0;
  var _imgdefecto='';
  var ventana_5da47d301f032=$('#ventana_<?php echo $idgui; ?>');
  tabledatos5da47d301f032=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    {
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        //{'data': '<?php //echo JrTexto::_("Idlocal") ;?>'},
        {'data': '<?php echo JrTexto::_("Number") ;?>'},
        {'data': '<?php echo JrTexto::_("Capacity") ;?>'},
        //{'data': '<?php //echo JrTexto::_("Tipo") ;?>'},
        {'data': '<?php echo JrTexto::_("State") ;?>'},
        //{'data': '<?php //echo JrTexto::_("Turno") ;?>'},            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'json/ambiente',
        type: "post",                
        data:function(d){
          d.json=true                   
          // d.tipo=$('#tipo').val();
          // d.estado=$('#estado').val();
          // d.turno=$('#turno').val();
          // d.idlocal=$('#idlocal').val();
          // d.texto=$('#texto').val();                       
          draw5da47d301f032=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          //console.log(json);
          var data=json.data;             
          json.draw = draw5da47d301f032;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            datainfo.push({            
              '#':(i+1),
              //'<?php // echo JrTexto::_("Idlocal") ;?>': data[i].idlocal,
             '<?php echo JrTexto::_("Number") ;?>': data[i].tipo_ambiente+' - '+data[i].numero,
              '<?php echo JrTexto::_("Capacity") ;?>': data[i].capacidad,
              //'<?php // echo JrTexto::_("Tipo") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].tipo==0?'':'active')+' " data-campo="tipo"  data-id="'+data[i].idambiente+'"> <i class="fa fa'+(data[i].tipo=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5da47d301f032[data[i].tipo]+'</a>',
              '<?php echo JrTexto::_("State") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idambiente+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5da47d301f032[data[i].estado]+'</a>',
              //'<?php //echo JrTexto::_("Turno") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].turno==0?'':'active')+' " data-campo="turno"  data-id="'+data[i].idambiente+'"> <i class="fa fa'+(data[i].turno=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5da47d301f032[data[i].turno]+'</a>',
                            //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/ambiente/editar/?id='+data[i].idambiente+'" data-titulo="'+tituloedit5da47d301f032+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idambiente+'" ><i class="fa fa-trash"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
  });

  ventana_5da47d301f032.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5da47d301f032();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5da47d301f032();
  }).on('change','#idlocal',function(ev){ refreshdatos5da47d301f032();
  }).on('change','#estado',function(ev){ refreshdatos5da47d301f032();
  }).on('change','#turno',function(ev){ refreshdatos5da47d301f032();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5da47d301f032();
  }).on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idambiente',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/ambiente/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5da47d301f032.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/ambiente/setcampo','masvalores':{idambiente:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idambiente',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/ambiente/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5da47d301f032.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var titulo=$(this).attr('data-titulo')||'';
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      refreshdatos5da47d301f032();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frm_ambiente(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_ambienteimport(_md);
            })
          });
       }           
    })   
  })  
});
</script>