<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/sinlogin_quiz">&nbsp;<?php echo JrTexto::_('Sinlogin_quiz'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col-md-12">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="sinlogin_quiz" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idpersona" id="idpersona" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idnota');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdnota" name="idnota" required="required" class="form-control" value="<?php echo @$frm["idnota"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idexamen');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdexamen" name="idexamen" required="required" class="form-control" value="<?php echo @$frm["idexamen"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tipo');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTipo" name="tipo" required="required" class="form-control" value="<?php echo @$frm["tipo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nota');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNota" name="nota" required="required" class="form-control" value="<?php echo @$frm["nota"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Notatexto');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNotatexto" name="notatexto" required="required" class="form-control" value="<?php echo @$frm["notatexto"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Regfecha');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtRegfecha" name="regfecha" required="required" class="form-control" value="<?php echo @$frm["regfecha"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idproyecto');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdproyecto" name="idproyecto" required="required" class="form-control" value="<?php echo @$frm["idproyecto"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Calificacion en');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtCalificacion_en" name="calificacion_en" required="required" class="form-control" value="<?php echo @$frm["calificacion_en"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Calificacion total');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtCalificacion_total" name="calificacion_total" required="required" class="form-control" value="<?php echo @$frm["calificacion_total"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Calificacion min');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtCalificacion_min" name="calificacion_min" required="required" class="form-control" value="<?php echo @$frm["calificacion_min"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tiempo total');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTiempo_total" name="tiempo_total" required="required" class="form-control" value="<?php echo @$frm["tiempo_total"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tiempo realizado');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTiempo_realizado" name="tiempo_realizado" required="required" class="form-control" value="<?php echo @$frm["tiempo_realizado"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Calificacion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtCalificacion" name="calificacion" required="required" class="form-control" value="<?php echo @$frm["calificacion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Habilidades');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtHabilidades" name="habilidades" required="required" class="form-control" value="<?php echo @$frm["habilidades"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Habilidad puntaje');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtHabilidad_puntaje" name="habilidad_puntaje" required="required" class="form-control" value="<?php echo @$frm["habilidad_puntaje"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Intento');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIntento" name="intento" required="required" class="form-control" value="<?php echo @$frm["intento"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Datos');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtDatos" name="datos" required="required" class="form-control" value="<?php echo @$frm["datos"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Fechamodificacion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtFechamodificacion" name="fechamodificacion" required="required" class="form-control" value="<?php echo @$frm["fechamodificacion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Preguntas');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtPreguntas" name="preguntas" required="required" class="form-control" value="<?php echo @$frm["preguntas"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveSinlogin_quiz" type="submit" tb="sinlogin_quiz" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('sinlogin_quiz'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/sinlogin_quiz/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/sinlogin_quiz');
                }
              }
          });
       }
  })
})
</script>

