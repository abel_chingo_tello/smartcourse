<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Notificacion_para"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">            
                                        
            <div class="col-xs-6 col-sm-4 col-md-3">             
            <div class="select-ctrl-wrapper select-azul">
              <select id="fkcbidnotificacion" name="fkcbidnotificacion" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione Idnotificacion'); ?></option>
                  <?php 
                          if(!empty($this->fkidnotificacion))
                            foreach ($this->fkidnotificacion as $fkidnotificacion) { ?><option value="<?php echo $fkidnotificacion["idnotificacion"]?>" <?php echo $fkidnotificacion["idnotificacion"]==@$frm["idnotificacion"]?"selected":""; ?> ><?php echo $fkidnotificacion["texto"] ?></option><?php } ?>                        
              </select>
            </div>
            </div>
                          
            <div class="col-xs-6 col-sm-4 col-md-3">             
            <div class="select-ctrl-wrapper select-azul">
              <select id="fkcbidpersona" name="fkcbidpersona" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione Idpersona'); ?></option>
                  <?php 
                          if(!empty($this->fkidpersona))
                            foreach ($this->fkidpersona as $fkidpersona) { ?><option value="<?php echo $fkidpersona["idpersona"]?>" <?php echo $fkidpersona["idpersona"]==@$frm["idpersona"]?"selected":""; ?> ><?php echo $fkidpersona["nombre"] ?></option><?php } ?>                        
              </select>
            </div>
            </div>
                                                    
                    <div class="col-xs-6 col-sm-4 col-md-3 " >
                      <!--label>Estado</label-->
                      <div class="select-ctrl-wrapper select-azul">
                      <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                          <option value=""><?php echo ucfirst(JrTexto::_("All Estado"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select>
                    </div>
                    </div>
                                
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>           
          </div>
        </div>
    </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Ficacion de")."(".JrTexto::_("Texto").")"; ?></th>
                    <th><?php echo JrTexto::_("Onal")."(".JrTexto::_("Nombre").")"; ?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
            </tbody>
            </table>
        </div>
    </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="notificacion_para" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idnotipersona" id="idnotipersona" value="">
          <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idnotificacion');?> <span class="required"> * </span></label>
              <select id="idnotificacion" name="idnotificacion" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkidnotificacion))
                foreach ($this->fkidnotificacion as $fkidnotificacion) { ?><option value="<?php echo $fkidnotificacion["idnotificacion"]?>" <?php echo $fkidnotificacion["idnotificacion"]==@$frm["idnotificacion"]?"selected":""; ?> ><?php echo $fkidnotificacion["texto"] ?></option><?php } ?>              </select>
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Idpersona');?> <span class="required"> * </span></label>
              <select id="idpersona" name="idpersona" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkidpersona))
                foreach ($this->fkidpersona as $fkidpersona) { ?><option value="<?php echo $fkidpersona["idpersona"]?>" <?php echo $fkidpersona["idpersona"]==@$frm["idpersona"]?"selected":""; ?> ><?php echo $fkidpersona["nombre"] ?></option><?php } ?>              </select>
                          </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
                                <a style="cursor:pointer;" class="chkformulario fa fa-check-circle-o" data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="estado" id="estado"  value="" > 
                 </a>
                                              </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="notificacion_para" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos602d7f9d9721b='';
function refreshdatos602d7f9d9721b(){
    tabledatos602d7f9d9721b.ajax.reload();
}
$(document).ready(function(){  
  var estados602d7f9d9721b={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit602d7f9d9721b='<?php echo ucfirst(JrTexto::_("notificacion_para"))." - ".JrTexto::_("edit"); ?>';
  var draw602d7f9d9721b=0;
  var _imgdefecto='';
  var ventana_602d7f9d9721b=$('#ventana_<?php echo $idgui; ?>');
   tabledatos602d7f9d9721b=ventana_602d7f9d9721b.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/notificacion_para',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.idnotificacion=$('#fkcbidnotificacion').val(),
             d.idpersona=$('#fkcbidpersona').val(),
             d.estado=$('#cbestado').val(),
             //d.texto=$('#texto').val(),
                        
            draw602d7f9d9721b=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw602d7f9d9721b;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push([            
              (i+1),
              data[i].texto,data[i].nombre,
                '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idnotipersona+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados602d7f9d9721b[data[i].estado]+'</a>',
                        //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idnotipersona+'" data-titulo="'+tituloedit602d7f9d9721b+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idnotipersona+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_602d7f9d9721b.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos602d7f9d9721b();
  }).on('blur','.textosearchlist',function(ev){refreshdatos602d7f9d9721b();
  })

  .on('change','#fkcbidnotificacion',function(ev){refreshdatos602d7f9d9721b();
  }).on('change','#fkcbidpersona',function(ev){refreshdatos602d7f9d9721b();
  }).on('change','#estado',function(ev){refreshdatos602d7f9d9721b();
  }).on('click','.btnbuscar',function(ev){ refreshdatos602d7f9d9721b();
  })
 

  ventana_602d7f9d9721b.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idnotipersona',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/notificacion_para/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos602d7f9d9721b.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/notificacion_para/setcampo','masvalores':{idnotipersona:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idnotipersona',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/notificacion_para/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos602d7f9d9721b.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modalnotificacion_para(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modalnotificacion_para(el);
  })

  /*Formulario*/
  var __modalnotificacion_para=function(el){
    var titulo = '<?php echo JrTexto::_("Notificacion_para"); ?>';
    var filtros=$('#ventana_<?php echo $idgui; ?>');
    var frm=$('#frm<?php echo $idgui; ?>').clone();     
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/notificacion_para/guardar',
          showmsjok:true,
          callback:function(rs){   tabledatos602d7f9d9721b.ajax.reload(); __cerrarmodal(_md);  }
        });
    })

    var pk=el.attr('pk')||'';
    if(pk!=''){      
      var data=new FormData();
      data.append('idnotipersona',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/notificacion_para',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];  
            //_md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());          
          }
         }
      });
    }else{
      /*if(filtros.find('select#tipo_portal').val()!='')
        _md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());*/
    }
  }
});
</script>