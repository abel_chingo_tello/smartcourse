<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_('Back')?></a></li> 
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li-->        
        <li class="active">&nbsp;<?php echo JrTexto::_("Grupoaula"); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="row">
    <div class="col">    
      <div class="row">                                                     
        <div class=" form-group col-xs-6 col-sm-4 col-md-4 " > 
          <label><?php echo JrTexto::_('Type') ?></label>
          <div class="cajaselect">
            <select name="tipo" id="tipo" class="form-control">
            	<option value=""><?php echo JrTexto::_('All') ?></option>
              <?php if(!empty($this->fktipos))
              foreach ($this->fktipos as $tip){ ?>
                <option value="<?php echo $tip["codigo"] ?>"><?php echo ucfirst(JrTexto::_($tip["nombre"]))?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-4 form-group">
          <label><?php echo JrTexto::_('Grade') ?></label>
          <div class="cajaselect">
            <select id="idgrado" name="idgrado" class="form-control" >
               <option value=""><?php echo JrTexto::_('All'); ?></option>
              <option value="0"><?php echo JrTexto::_('Only Grade'); ?></option>
                <?php 
                if(!empty($this->grados))
                foreach ($this->grados as $r) { ?><option value="<?php echo $r["idgrado"]?>" ><?php echo JrTexto::_('Grade')." ".JrTexto::_($r["abrev"])." : ".JrTexto::_($r["descripcion"]) ?></option>
                 <?php } ?>                        
            </select>
          </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-4 form-group">
          <label><?php echo JrTexto::_('Section') ?></label>
          <div class="cajaselect">
            <select id="idsesion" name="idsesion" class="form-control" >
              <option value=""><?php echo JrTexto::_('All'); ?></option>
              <option value="0"><?php echo JrTexto::_('Only Section'); ?></option>
                <?php 
                if(!empty($this->seccion))
                foreach ($this->seccion as $s) { ?><option value="<?php echo $s["idsesion"]?>" ><?php echo JrTexto::_('Section')." ".$s["descripcion"] ?></option>
                 <?php } ?>                        
            </select>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 form-group">
            <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
            <div class="input-group">
              <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
              <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
            </div>
        </div>
        <div class="col-md-6 col-sm-12 text-center">             
           <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/acad_grupoaula/agregar" data-titulo="<?php echo JrTexto::_('Grupo aula')." ".JrTexto::_('add') ?>" ><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
        </div>
      </div>
    </div>
  </div>
	<div class="row">         
    <div class="col table-responsive">
      <table class="table table-striped table-hover">
        <thead>
          <tr class="headings">
            <th>#</th>
            <th><?php echo JrTexto::_("Name") ;?></th>                    
            <th><?php echo JrTexto::_("Nvacantes") ;?></th>
            <th><?php echo JrTexto::_("State") ;?></th>
            <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5da9cafd0e4de='';
function refreshdatos5da9cafd0e4de(){
    tabledatos5da9cafd0e4de.ajax.reload();
}
$(document).ready(function(){  
  var estados5da9cafd0e4de={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5da9cafd0e4de='<?php echo ucfirst(JrTexto::_("Grupo aula"))." - ".JrTexto::_("edit"); ?>';
  var draw5da9cafd0e4de=0;
  var _imgdefecto='';

  



  var ventana_5da9cafd0e4de=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5da9cafd0e4de=ventana_5da9cafd0e4de.find('.table').DataTable(
    { "searching": false,
      "processing": false,
      "ajax":{
        url:_sysUrlBase_+'json/acad_grupoaula',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.tipo=$('#tipo').val();
            d.idgrado=$('#idgrado').val();
            d.idsesion=$('#idsesion').val();
            d.texto=$('#texto').val();                        
            draw5da9cafd0e4de=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5da9cafd0e4de;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                                
            datainfo.push([             
              (i+1),
              data[i].nombre,
              data[i].nvacantes,
              '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idgrupoaula+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5da9cafd0e4de[data[i].estado]+'</a>',
              '<a class="btn btn-xs " href="'+_sysUrlSitio_+'/acad_grupoauladetalle/?idgrupoaula='+data[i].idgrupoaula+'" data-id="'+data[i].idgrupoaula+'"><i class="fa fa-book"></i></a> <a class="btn btn-xs btnmatricular"  href="'+_sysUrlSitio_+'/alumnos/?idgrupoaula='+data[i].idgrupoaula+'" data-titulo="<?php echo JrTexto::_('Assign Students')?>"><i class="fa fa-users"></i></a> <a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/acad_grupoaula/editar/?id='+data[i].idgrupoaula+'" data-titulo="'+tituloedit5da9cafd0e4de+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idgrupoaula+'" ><i class="fa fa-trash"></i></a>'
          	//'<?php echo JrTexto::_("Type") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].tipo==0?'':'active')+' " data-campo="tipo"  data-id="'+data[i].idgrupoaula+'"> <i class="fa fa'+(data[i].tipo=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5da9cafd0e4de[data[i].tipo]+'</a>',
            //'<?php echo JrTexto::_("Comentario") ;?>': data[i].comentario,
                        //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5da9cafd0e4de.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5da9cafd0e4de();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5da9cafd0e4de();
  }).on('change','#tipo',function(ev){refreshdatos5da9cafd0e4de();
  }).on('change','#idgrado',function(ev){refreshdatos5da9cafd0e4de();
  }).on('change','#idsesion',function(ev){refreshdatos5da9cafd0e4de();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5da9cafd0e4de();
  })
 

  ventana_5da9cafd0e4de.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idgrupoaula',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/acad_grupoaula/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5da9cafd0e4de.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/acad_grupoaula/setcampo','masvalores':{idgrupoaula:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idgrupoaula',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_grupoaula/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5da9cafd0e4de.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var titulo=$(this).attr('data-titulo')||'';
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      tabledatos5da9cafd0e4de.ajax.reload();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frm(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_import(_md);
            })
          });
       }           
    })   
  })
});
</script>