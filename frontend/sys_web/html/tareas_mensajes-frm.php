<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/tareas_mensajes">&nbsp;<?php echo JrTexto::_('Tareas_mensajes'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="tareas_mensajes" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idtareamensaje" id="idtareamensaje" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idtarea');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdtarea" name="idtarea" required="required" class="form-control" value="<?php echo @$frm["idtarea"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idusuario');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdusuario" name="idusuario" required="required" class="form-control" value="<?php echo @$frm["idusuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Rol');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtRol" name="rol" required="required" class="form-control" value="<?php echo @$frm["rol"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Mensaje');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtMensaje" name="mensaje" required="required" class="form-control" value="<?php echo @$frm["mensaje"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtEstado" name="estado" required="required" class="form-control" value="<?php echo @$frm["estado"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveTareas_mensajes" type="submit" tb="tareas_mensajes" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('tareas_mensajes'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/tareas_mensajes/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/tareas_mensajes');
                }
              }
          });
       }
  })
})
</script>

