<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Asignar Examen a proyecto"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo URL_BASE ?>static/libs/select2/select2.min.css">
<script type="text/javascript" src="<?php echo URL_BASE ?>static/libs/select2/select2.min.js"></script>
<style type="text/css">
  .btn{font-size: 14px;}
  .estiloreporte{
    background: #fff;
    padding: 1ex;
    margin: 1ex;
    border: 1px solid #e0dddd;
    border-radius: 1ex;
    -webkit-box-shadow: 10px 10px 5px -8px rgba(133,155,199,1);
-moz-box-shadow: 10px 10px 5px -8px rgba(133,155,199,1);
box-shadow: 10px 10px 5px -8px rgba(133,155,199,1);
  }
  .select2-container{
    width: 100% !important;
  }
  .select2-container--open{
    z-index: 99999;
  }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">            
                                                                                                                                        
            <div class="col-xs-6 col-sm-4 col-md-3 " >
              <!--label>Estado</label-->
              <div class="select-ctrl-wrapper select-azul">
              <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                  <option value=""><?php echo ucfirst(JrTexto::_("All Estado"))?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                  <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
              </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 " >
              <!--label>Estado</label-->
              <div class="select-ctrl-wrapper select-azul">
              <select name="cbidioma" id="cbidioma" class="form-control select-ctrl">
                  <option value="ES"><?php echo ucfirst(JrTexto::_("Español"))?></option>
                  <option value="EN"><?php echo ucfirst(JrTexto::_("Ingles"))?></option>
                  <option value="PO"><?php echo ucfirst(JrTexto::_("Portugués"))?></option>                              
              </select>
              </div>
            </div>
                                
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>           
          </div>
        </div>
    </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Id Examen"); ?></th>
                    <th><?php echo JrTexto::_("Enlace"); ?></th>
                    <!--th><?php //echo JrTexto::_("User registro") ;?></th>
                    <th><?php //echo JrTexto::_("Fecha hasta") ;?></th>
                    <th><?php //echo JrTexto::_("Estado") ;?></th-->
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
            </tbody>
            </table>
        </div>
    </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="sinlogin_quizproyecto" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idasignacion" id="idasignacion" value="">
          <input type="hidden" name="estado" id="estado" value="1">
          <div class="row">
          <div class="form-group col-md-12 col-sm-12">
            <label class="control-label"><?php echo JrTexto::_('Examen');?> <span class="required"> * </span></label><br>
            <select name="idquiz" id="idquiz" class="select2 form-control">
              <option value="3266">3266: Placement Test</option>
              <?php if(!empty($this->examen))
              foreach ($this->examen as $k => $exa){  if($exa["idexamen"]==3266) continue;?>
              <option value="<?php echo $exa["idexamen"]; ?>"><?php echo $exa["idexamen"].": ". utf8_encode(ucfirst(strtolower($exa["titulo"])));?></option>
              <?php } ?>
            </select>
            </div>
            </div> 
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="sinlogin_quizproyecto" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">

var tabledatos6006ebbcd7e6e='';
function refreshdatos6006ebbcd7e6e(){
    tabledatos6006ebbcd7e6e.ajax.reload();
}
$(document).ready(function(){  
  //$('.select2').select2();
  var estados6006ebbcd7e6e={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit6006ebbcd7e6e='<?php echo ucfirst(JrTexto::_("Asignar Examen"))." - ".JrTexto::_("edit"); ?>';
  var draw6006ebbcd7e6e=0;
  var _imgdefecto='';
  var ventana_6006ebbcd7e6e=$('#ventana_<?php echo $idgui; ?>');
   tabledatos6006ebbcd7e6e=ventana_6006ebbcd7e6e.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/sinlogin_quizproyecto',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.estado=$('#cbestado').val(),
             //d.texto=$('#texto').val(),
                        
            draw6006ebbcd7e6e=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw6006ebbcd7e6e;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          let idioma=$('select#cbidioma').val();
          var txtidioma='';
          if(idioma!="ES"){ txtidioma='&idioma='+idioma;}
          for(var i=0;i< data.length; i++){
                                
            datainfo.push([            
              (i+1),
              data[i].idquiz+' : '+ data[i].titulo, '<a href="'+_sysUrlBase_+'quiz/registro/?idexamen='+data[i].idquiz+txtidioma+'" target="_blank">'+_sysUrlBase_+'quiz/registro/?idexamen='+data[i].idquiz+txtidioma+'</a>',
             //data[i].idproyecto,
              //data[i].user_registro,
              //data[i].fecha_hasta,
                //'<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idasignacion+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados6006ebbcd7e6e[data[i].estado]+'</a>',
                        //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idasignacion+'" data-titulo="'+tituloedit6006ebbcd7e6e+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idasignacion+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_6006ebbcd7e6e.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos6006ebbcd7e6e();
  }).on('blur','.textosearchlist',function(ev){refreshdatos6006ebbcd7e6e();
  })

  .on('change','#estado',function(ev){refreshdatos6006ebbcd7e6e();
  }).on('click','.btnbuscar',function(ev){ refreshdatos6006ebbcd7e6e();
  })
 

  ventana_6006ebbcd7e6e.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idasignacion',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/sinlogin_quizproyecto/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos6006ebbcd7e6e.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/sinlogin_quizproyecto/setcampo','masvalores':{idasignacion:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idasignacion',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/sinlogin_quizproyecto/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos6006ebbcd7e6e.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modalsinlogin_quizproyecto(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modalsinlogin_quizproyecto(el);
  })

  /*Formulario*/
  var __modalsinlogin_quizproyecto=function(el){
    var titulo = '<?php echo JrTexto::_("Asignar examen al proyecto"); ?>';
    var filtros=$('#ventana_<?php echo $idgui; ?>');
    var frm=$('#frm<?php echo $idgui; ?>').clone();     
    var _md = __sysmodal({'html': frm,'titulo': titulo});

    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/sinlogin_quizproyecto/guardar',
          showmsjok:true,
          callback:function(rs){   tabledatos6006ebbcd7e6e.ajax.reload(); __cerrarmodal(_md);  }
        });
    })

    _md.find('select').select2();
//$('#idquiz').select2();

    var pk=el.attr('pk')||'';
    if(pk!=''){      
      var data=new FormData();
      data.append('idasignacion',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/sinlogin_quizproyecto',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];  
            //_md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());          
          }
         }
      });
    }else{
      /*if(filtros.find('select#tipo_portal').val()!='')
        _md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());*/
    }
  }
  //
});
</script>