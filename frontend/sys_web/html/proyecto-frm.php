<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/proyecto">&nbsp;<?php echo JrTexto::_('Proyecto'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col-md-12">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="proyecto" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idempresa');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdempresa" name="idempresa" required="required" class="form-control" value="<?php echo @$frm["idempresa"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Jsonlogin');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtJsonlogin" name="jsonlogin" required="required" class="form-control" value="<?php echo @$frm["jsonlogin"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Fecha');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtFecha" name="fecha" required="required" class="form-control" value="<?php echo @$frm["fecha"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idioma');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdioma" name="idioma" required="required" class="form-control" value="<?php echo @$frm["idioma"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tipo empresa');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTipo_empresa" name="tipo_empresa" required="required" class="form-control" value="<?php echo @$frm["tipo_empresa"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Logo');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtLogo" name="logo" required="required" class="form-control" value="<?php echo @$frm["logo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nombreurl');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNombreurl" name="nombreurl" required="required" class="form-control" value="<?php echo @$frm["nombreurl"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tipo portal');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTipo_portal" name="tipo_portal" required="required" class="form-control" value="<?php echo @$frm["tipo_portal"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveProyecto" type="submit" tb="proyecto" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('proyecto'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/proyecto/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/proyecto');
                }
              }
          });
       }
  })
})
</script>

