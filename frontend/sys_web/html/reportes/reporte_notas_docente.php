<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Notas"); ?>'
        });
        var __usuario = <?= json_encode($this->usuario) ?>
    </script>
<?php } ?>
<title>HTML Elements Reference</title>
<div class="container my-font-all">
    <div class="row my-x-center">
        <?php if ($this->usuario["idrol"] == 1) { ?>
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="">Grupos</label>
                    <select class="form-control" name="" id="selectGrupos">
                    </select>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectCursos">Cursos</label>
                <select class="form-control" name="" id="selectCursos">
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectGrupoNota">Grupo</label>
                <select class="form-control" name="" id="selectGrupoNota">
                </select>
            </div>
        </div>
    </div>
    <div class="row col-sm-12 my-x-center">
        <div class="box-cont">
            <table id="my-table" class="mdl-data-table my-font my-card my-shadow" style="width:100%">
                <thead id="tb-head" class="my-text-center">
                    <!-- <tr>
                        <th></th>
                        <th></th>
                    </tr> -->
                </thead>
                <tbody id="tbody">
                    <tr class="odd">
                        <td id="tb-msg" valign="top" colspan="4" class="dataTables_empty">Cargando...</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>

</div>
<script>
    $(document).ready(() => {
        const oReporteNota = new ReporteNota();
        oReporteNota.load();
    });
</script>