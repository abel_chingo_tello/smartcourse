<?php
//$this->documento->script('mine-reporte_notas_admin', '/libs/othersLibs/reporte_notas/');
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Notas"); ?>'
        });
        var __usuario = <?= json_encode($this->usuario) ?>
    </script>
<?php } ?>

<div class="container my-font-all">
    <div id="radiobtn" class="row my-x-center ">
        <div class="col-sm-12 col-md-8">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a id="tab-0" onclick="showRpt0()" class="nav-link active" href="#">Por Grupos</a>
                </li>
                <li class="nav-item">
                    <a id="tab-1" onclick="showRpt1()" class="nav-link" href="#">Por Alumnos</a>
                </li>
            </ul>
        </div>
    </div>
    <div id="reporte0" class="row my-x-center my-hide">
        <?php if ($this->usuario["idrol"] == 1) { ?>
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="">Grupos</label>
                    <select class="form-control" name="" id="selectGrupos">
                    </select>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectCursos">Cursos</label>
                <select class="form-control" name="" id="selectCursos">
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectGrupoNota">Bimestre</label>
                <select class="form-control" name="" id="selectGrupoNota">
                </select>
            </div>
        </div>
    </div>
    <!--  -->
    <div id="reporte1" class="row my-x-center my-hide">
        <div class="col-sm-12 col-md-8">
            <div class="form-group">
                <label for="">Alumnos</label>
                <select class="form-control" name="" id="selectAlumnos">
                </select>
            </div>
        </div>
    </div>
    <div class="row col-sm-12 my-x-center">
        <div class="box-cont">
            <table id="my-table" class="mdl-data-table my-font my-card my-shadow" style="width:100%">
                <thead id="tb-head" class="my-text-center">
                    <!-- <tr>
                        <th></th>
                        <th></th>
                    </tr> -->
                </thead>
                <tbody id="tbody">
                    <tr class="odd">
                        <td id="tb-msg" valign="top" colspan="4" class="dataTables_empty">Cargando...</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    var oReporteNotasAdmin, oReporteNota;
    $(document).ready(() => {
        oReporteNotasAdmin = new ReporteNotasAdmin();
        oReporteNota = new ReporteNota();
        showRpt0();
    });

    function msgload() {
        if ($.fn.DataTable.isDataTable('#my-table')) {
            $('#my-table').DataTable().destroy();
        }
        let msg = `
                 <thead id="tb-head" class="my-text-center">
                    <!-- <tr>
                        <th></th>
                        <th></th>
                    </tr> -->
                </thead>
                <tbody id="tbody">
                    <tr class="odd">
                        <td id="tb-msg" valign="top" colspan="4" class="dataTables_empty">Cargando...</td>
                    </tr>
                </tbody>
        `;
        $('#my-table').html(msg);
    }

    function showRpt0() {
        $('#tab-0').addClass('active');
        $('#tab-1').removeClass('active');
        $('#reporte0').removeClass('my-hide');
        $('#reporte1').addClass('my-hide');
        msgload();
        oReporteNota.load();
    }

    function showRpt1() {
        $('#tab-1').addClass('active');
        $('#tab-0').removeClass('active');
        $('#reporte1').removeClass('my-hide');
        $('#reporte0').addClass('my-hide');
        msgload();
        oReporteNotasAdmin.load();
    }
</script>