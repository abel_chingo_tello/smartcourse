<?php

?>
<style>
.row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

select.select-ctrl{ padding-right: 35px; }
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.box_custom{ background-color:white; border:1px solid #4683af; border-radius:0.5em; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<div class="row" id="search_container">
    <div class="col-md-6">
        <div class="table-responsive box_custom">
            <table class="table" id="tabla01">
                <thead>
                    <th><?php echo $this->Entidad; ?></th>
                    <th>A1</th>
                    <th>A2</th>
                    <th>B1</th>
                    <th>B2</th>
                    <th>C1</th>
                </thead>
                <tbody>
                    <?php
                        if(!empty($this->RowTabla)){
                            foreach($this->RowTabla as $r){
                                echo "<tr>";
                                echo "<td>{$r['nombre']}</td>";
                                echo "<td>{$r['A1']}%</td>";
                                echo "<td>{$r['A2']}%</td>";
                                echo "<td>{$r['B1']}%</td>";
                                echo "<td>{$r['B2']}%</td>";
                                echo "<td>{$r['C1']}%</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 box_custom">
        <h4 style="text-align:center; font-weight:bold;">Progresos</h4>
        <div class="chart-container" id="Container_progreso" style="position: relative; margin:0 auto; height:100%; width:100%">
            <canvas id="Chart_progreso"></canvas>
        </div>
        <div style="text-align:center;">
            <button type="button" id="previousChart" class="btn btn-default btn-circle"><i class="fa fa-angle-left"></i></button>
            <div class="panel panel-default" style="display:inline-block;">
                <div class="panel-body"><span id="pag-text">1 / 1</span></div>
            </div>
            <button type="button" id="nextChart" class="btn btn-default btn-circle"><i class="fa fa-angle-right"></i></button>
        </div>
    </div>
</div>

<script type="text/javascript">
var datos = <?php echo !empty($this->RowTabla) ? json_encode($this->RowTabla) : 'null' ?>;
var drawChart = function(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: false,
        text: 'Empty'
      },
      scales:{"yAxes":[{"ticks":{"beginAtZero":true}}]},
      tooltips: {
        mode: 'point',
        intersect: true
      },legend: {
            display: true
         }
    }
  });
};
function displayChart(datos,labeles){
    var A1D = new Array(), A2D = new Array(),B1D = new Array(),B2D = new Array(),C1D = new Array();
 
    if(Object.keys(datos).length > 0 && labeles.length > 0){
        for(var j = 0; j < labeles.length; j++){
            A1D.push(datos[j].A1);
            A2D.push(datos[j].A2);
            B1D.push(datos[j].B1);
            B2D.push(datos[j].B2);
            C1D.push(datos[j].C1);
        }
    }
    var chartData = {
        labels: labeles,
        datasets: [ {
            label:'A1',
            backgroundColor: 'rgba(63,81,181,0.5)',
            data: A1D,
            borderColor: 'white',
            borderWidth: 2
        },{
            label:'A2',
            backgroundColor: 'rgba(233,30,99,0.5)',
            data: A2D,
            borderColor: 'white',
            borderWidth: 2
        },{
            label:'B1',
            backgroundColor: 'rgba(255,193,7,0.5)',
            data: B1D,
            borderColor: 'white',
            borderWidth: 2
        },{
            label:'B2',
            backgroundColor: 'rgba(156,39,176,0.5)',
            data: B2D,
            borderColor: 'white',
            borderWidth: 2
        },{
            label:'C1',
            backgroundColor: 'rgba(139,195,74,0.5)',
            data: C1D,
            borderColor: 'white',
            borderWidth: 2
        }]

    };
    $("#Container_progreso").html(" ").html('<canvas id="Chart_progreso"></canvas>');
    drawChart('Chart_progreso',chartData);
}
$('document').ready(function(){
    $('#search_container').parent().css('margin','5px 0');
    $('#search_container').parent().css('width','100%');
    $('#tabla01').DataTable({
        "language":{
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    });
    
    // Check if is 5 limit of the rows
    var labeles = new Array;
    var _d = new Array;
    var paginas = 0;
    var paginaActual = 1;
    var totales = Object.keys(datos).length;
    // var totales = 54;
    var max = 12;
    if(datos != null){
        if(totales <= 12){
            for(var i = 0; i < totales; i++){
                if(typeof datos[i].nombre != 'undefined'){
                    labeles.push(datos[i].nombre);
                    _d.push({'A1':datos[i].A1,'A2':datos[i].A2,'B1':datos[i].B1,'B2':datos[i].B2,'C1':datos[i].C1});
                }
            }
            displayChart(_d,labeles);
        }else{
            //logica para paginar
            paginas = Math.ceil(totales/max);
            if(paginas > 1){
                $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);                            
                            _d.push({'A1':datos[i].A1,'A2':datos[i].A2,'B1':datos[i].B1,'B2':datos[i].B2,'C1':datos[i].C1});                     
                        }
                    }
                    displayChart(_d,_l);
                }
                
            }//end if checker
        }
    }//end if datos null

    $('#previousChart').on('click',function(){
        if(paginaActual > 1 && paginaActual <= paginas){
            --paginaActual;
            $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
            if(datos != null){
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);                            
                            _d.push({'A1':datos[i].A1,'A2':datos[i].A2,'B1':datos[i].B1,'B2':datos[i].B2,'C1':datos[i].C1});   
                        }
                    }
                    displayChart(_d,_l);
                }
                
            }
        }
    });
    $('#nextChart').on('click',function(){
        if(paginaActual >= 1 && paginaActual < paginas){
            ++paginaActual;
            $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
            if(datos != null){
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);                            
                            _d.push({'A1':datos[i].A1,'A2':datos[i].A2,'B1':datos[i].B1,'B2':datos[i].B2,'C1':datos[i].C1});   
                        }
                    }
                    displayChart(_d,_l);
                }
                
            }
        }
    });
});
</script>
