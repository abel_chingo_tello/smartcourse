<?php

defined("RUTA_BASE") or die(); 
$idgui = uniqid();
$url = $this->documento->getUrlBase();
if(isset($this->user)){
    echo '<input id="iddocente" type="hidden" value="'.$this->user['idpersona'].'" />';
}else{
    echo '<input id="iddocente" type="hidden" value="-1" />';
}
?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: white; font-size: large; }
select.select-ctrl{ padding-right: 35px; }
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){ margin:0 auto; }
.text-nota1{ font-size: x-large; font-weight: bold; color: #b73333; }
.text-custom1 { font-size: large; border-bottom: 1px solid gray; border-radius: 10%; box-shadow: 0px 1px #e20b0b; }
#info-tabla span{ text-decoration: underline; font-weight: bold;}

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}
</style>


<!--start container-->
<div class="container">
    <div class="row " id="levels" style="padding-top: 1ex; ">
    <div class="col-md-12">
        <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
        <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li>                  
        <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes/entrada"><?php echo JrTexto::_("Beginning test")?></a></li>
                         
        <li class="active">
            <?php echo JrTexto::_("comparative report of students"); ?>
        </li>
        </ol>
    </div>
    </div>
    <!--start panel-->
    <div class="panel panel-primary">
        <div class = 'panel-heading' style="text-align: left;">
        <?php echo JrTexto::_("Report");?>
        </div>
        <div class="panel-body" style="text-align:center;">
            <div class="row">
                <div class="col-md-12" style="/*height:90px;*/">
                    
                    <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                    <h2><?php echo $this->fullname ?></h2>
                </div>
                <div class="col-md-12">
                    <div id="idcolegio-container" style="display: inline-block;">
                        
                        <h4 style="display:inline-block;"><?php echo JrTexto::_('School'); ?></h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select id="idcolegio" class="select-docente form-control select-ctrl ">
                                <?php if(!empty($this->miscolegios)){
                                echo '<option value="0">'.JrTexto::_('Select').'</option>';
                                foreach ($this->miscolegios  as $c) {
                                    if(empty($cursos)) $cursos=$c["cursos"];
                                    echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                                }} ?>
                            </select>
                        </div>
                    </div>
                    <div id="idcurso-container" style="display: inline-block;">
                        <h4 style="display:inline-block;"><?php echo JrTexto::_('Courses'); ?></h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="idcurso" id="idcurso" class="select-docente form-control select-ctrl">
                                <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div id="idgrados-container" style="display: inline-block;">
                        <h4 style="display: inline-block;"><?php echo JrTexto::_('Grades'); ?></h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="grados" id="idgrados" class="select-docente form-control select-ctrl">
                                <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div id="idseccion-container" style="display: inline-block;">
                        <h4 style="display: inline-block;"><?php echo JrTexto::_('Section'); ?></h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="seccion" id="idseccion" class="select-docente form-control select-ctrl select-nivel">
                                    <option value="0"><?php echo JrTexto::_('Select'); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end panel--> 
    <div class="panel panel-success">
    
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <h2 style="border-bottom:2px solid gray;"><?php echo JrTexto::_('Students Begining Exam Report'); ?></h2>
                <h4 class="alert alert-info" id="info-tabla"><i class="fa fa-info-circle" aria-hidden="true" style="font-size:50px;"></i>&nbsp;&nbsp;<?php echo JrTexto::_('Report'); ?> <span class="iiee">10888 SEÑOR DE LOS MILAGROS</span>&nbsp; <?php echo JrTexto::_('Grade'); ?>:<span class="grado">Primero</span>&nbsp; <?php echo JrTexto::_('Section'); ?>:<span class="seccion">A</span></h4>
            
            </div>
            <div class="col-md-12" id="tabla-contenedor">
                <table id="tabla01">
                    <thead>
                        <th><?php echo JrTexto::_('Names'); ?></th>
                        <th><?php echo JrTexto::_('Score')." (0 - 20 pts)" ?></th>
                        <th>Listening</th>
                        <th>Reading</th>
                        <th>Writing</th>
                        <th>Speaking</th>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    </div>  
</div>
<!--end container-->

<script type="text/javascript">
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;

function dibujarDatatable(ideobj, contenedor = null){
    if(contenedor != null){
        var tmp_table = $(ideobj).clone();
        $(ideobj+'_wrapper').remove();
        $(contenedor).append(tmp_table);
    }
    if ($.fn.DataTable.isDataTable(ideobj)) {
        $(ideobj).DataTable().clear().destroy();
    }
    $(ideobj).DataTable(
        {
        // "paging":   true,
        // "ordering": false,
        // "info":     false,
        // "displayLength": 25,
        // "pagingType": "simple"
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ]
        }
    );
}
var getnombreselect = function(obj,value){
    var nombre = 'undefined';
    $(obj).find('option').each(function(k,v){
        if($(this).attr('value') == value){
            nombre = $(this).text();
        }
    });
    return nombre;
};
var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');                 
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');                
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
}

$('document').ready(function(){
    var filtros = null;
    $('#info-tabla').hide();
    dibujarDatatable('#tabla01');
    $("#idcolegio").change(function(){
        var idcolegio=$(this).val()||'';
        for (var i = 0; i < datoscurso.length; i++) {
           if(datoscurso[i].idlocal==idcolegio){              
                datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
               actualizarcbo($(this),datoscurso[i].cursos);
           }
        }
    });
    $("#idcurso").change(function(){
        var idcurso=$(this).val()||'';
        var idcolegio=$('#idcolegio').val()||'';
        for (var i = 0; i < datoscurso.length; i++)
        if(datoscurso[i].idlocal==idcolegio)
        for (var j = 0; j < datoscurso[i].cursos.length; j++)
            if(datoscurso[i].cursos[j].idcurso==idcurso){
                var grados=datoscurso[i].cursos[j].grados;                
                actualizarcbo($(this),grados);
            }
    });
    $("#idgrados").change(function(){
        var idgrados=$(this).val()||'';
        var idcolegio=$('#idcolegio').val()||'';
        var idcurso=$('#idcurso').val()||'';
        for (var i = 0; i < datoscurso.length; i++)
        if(datoscurso[i].idlocal==idcolegio)
        for (var j = 0; j < datoscurso[i].cursos.length; j++)
            if(datoscurso[i].cursos[j].idcurso==idcurso){
                var grados=datoscurso[i].cursos[j].grados;
                for (var h = 0; h < grados.length; h++){
                    if(grados[h].idgrado==idgrados){
                        var secciones=grados[h].secciones;
                        secciones.sort(function(a, b){return a.seccion - b.seccion;});
                        actualizarcbo($(this),secciones);
                    }
                }
                
            }
    });
    $('#idseccion').change(function(){

        var iddocente = $('#iddocente').val();
        var idlocal = $('#idcolegio').val();
        var idcurso = $('#idcurso').val();
        var idgrado = $('#idgrados').val();
        var idgrupoauladetalle = $('#idseccion').val();

        if(iddocente != -1 && idlocal != -1 && idcurso != -1 && idgrado != -1 && idgrupoauladetalle != -1){
          filtros = new Object;
          filtros.iddocente = iddocente;
          filtros.idlocal = idlocal;
          filtros.idcurso = idcurso;
          filtros.idgrado = idgrado;
          filtros.idgrupoauladetalle = idgrupoauladetalle;

          $('#info-tabla').show();
          $('#info-tabla').find('.iiee').text(getnombreselect("#idcolegio",$('#idcolegio').val()));
          $('#info-tabla').find('.curso').text(getnombreselect("#idcurso",$('#idcurso').val()));
          $('#info-tabla').find('.grado').text(getnombreselect("#idgrados",$('#idgrados').val()));
          $('#info-tabla').find('.seccion').text(getnombreselect("#idseccion",$('#idseccion').val()));

          $.ajax({
            url: _sysUrlBase_+'/docente/entrada_grupo',
            type: 'POST',
            dataType: 'json',
            data: filtros,
          }).done(function(resp){
            if(resp.code=='ok'){
              
              if(resp.data.tiemposactual != null && Object.keys(resp.data.tiemposactual).length > 0){
                var table = $('#tabla01');
                var table_body = $('#tabla01').find('tbody');
                var row = '';
                
                table_body.html(" ");
                for(var i in resp.data.tiemposactual){

                  if(typeof resp.data.tiemposactual[i] != 'function'){
                    row = "<tr><td>"+resp.data.tiemposactual[i]['alumno']+"</td><td>"+
                    (resp.data.tiemposactual[i]['nota'])+"pts</td><td>"+
                    (resp.data.tiemposactual[i]['4'])+"%</td><td>"+
                    (resp.data.tiemposactual[i]['5'])+"%</td><td>"+
                    (resp.data.tiemposactual[i]['6'])+"%</td><td>"+
                    (resp.data.tiemposactual[i]['7'])+"%</td>";
                  } 
                  table_body.append(row);
                  
                }//end foreach resultado
              }
              
              dibujarDatatable("#tabla01","#tabla-contenedor");
            }else{
              return false;
            }
          }).fail(function(xhr, textStatus, errorThrown) {
            console.log("%c ERROR","font-size:40px;");
            return false;
          });

          
          
        }

    });
});

</script>

