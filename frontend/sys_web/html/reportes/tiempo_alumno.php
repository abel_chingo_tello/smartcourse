<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Time"); ?>'
  });
</script>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
        <div class="x_title">
            <div id="radiobtn" class="row my-x-center ">
                <div class="col-sm-12 col-md-6">                   
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                  <div class="btn-group">
                    <button type="button" class="btn  btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                      <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="area"><i class="fa fa-area-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar" ><i class="fa fa-bar-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="line" ><i class="fa fa-line-chart"></i></button>
                  </div>
              </div>
            </div>
            <div class="clearfix"></div>
        </div> 
      <div class=""> 
        <h5 class="mb-2 text-primary"><?=strtoupper(JrTexto::_("total time on platform"));?></h5>
        <div class="row" id="alumnosolocurso" style="display:none">
            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Study Time in the Virtual Platform'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoenpv">0</h1>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Time in Course'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoencurso" >0</h1>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Time in Exams'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoenexamen">0</h1>
                </div>
              </div>
            </div>

                        
        </div>
        <div class="row" id="impresion" style="display:none">
            <div class="col-md-12 table-responsive" style="display: none">
                <table id="tablaalumnos" class="table table-bordered table-hover mdl-data-table my-font my-card my-shadow no-footer dataTable">
                  <thead>                      
                      <th><?php echo JrTexto::_("Student"); ?></th>
                      <th><?php echo JrTexto::_("Time in course"); ?><small id="nomcurso"></small></th>
                      <th><?php echo JrTexto::_("Time in Exam"); ?></th>
                      <th><?php echo JrTexto::_("Time in platform"); ?></th>             
                  </thead>
                  <tbody></tbody>
                </table>
            </div>
            <div id="aquitablaamostrar" class="col-md-12 table-responive">                
            </div>
        </div>       
        <div class="row" id="alumnosolocurso1" style="display:none">
            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Study Time in the Virtual Platform'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoenpv">0</h1>
                </div>
              </div>
            </div>
             <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Time in Sessions'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoencurso" >0</h1>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Time in exams'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoenexamen">0</h1>
                </div>
              </div>
            </div>

                       
        </div>
        <div class="row" id="pnl-panelreporte"  style="display:none">
         <div class="col-md-12" id="grafico"></div>       
        </div>
        <div class="row" id="impresioncursos" style="display:none">
            <div class="col-12">
              <h5 class="mb-2 text-primary"><?=strtoupper(JrTexto::_("Total course time"));?></h5>
              <div class="row mb-3">
                
                <div class="col-sm-4">
                  <label><?=JrTexto::_("Courses");?></label>
                  <select class="form-control" id="cmbcursos">
                    <option value=""><?=JrTexto::_("All");?></option>
                  </select>
                </div>
                <!-- <div class="col-sm-4">
                  <label><?=JrTexto::_("Session");?></label>
                  <select class="form-control" id="cmbsesiones">
                    <option value=""><?=JrTexto::_("All");?></option>
                  </select>
                </div>
                <div class="col-sm-4">
                  <label><?=JrTexto::_("Test");?></label>
                  <select class="form-control" id="cmbexamenes">
                    <option value=""><?=JrTexto::_("All");?></option>
                  </select>
                </div> -->
              </div>
            </div>
            <div class="col-md-12 table-responsive" style="display: none">
                <table id="tablacursos" class="table table-bordered table-hover mdl-data-table my-font my-card my-shadow no-footer dataTable">
                  <thead>                      
                      <th><?php echo JrTexto::_("Courses"); ?></th>
                      <th><?php echo JrTexto::_("Time in Sessions"); ?></th>
                      <th><?php echo JrTexto::_("Time in Exam"); ?></th>
                      <th><?php echo JrTexto::_("Time in Course"); ?></th>             
                  </thead>
                  <tbody></tbody>
                  <tfoot>
                    <tr class="text-center">
                      <td><?=JrTexto::_("Total");?></td>
                      <td>24h 59m 59s</td>
                      <td>24h 59m 59s</td>
                      <td>24h 59m 59s</td>
                    </tr>
                  </tfoot>
                </table>
            </div>
            <div id="aquitablacursos" class="col-md-12 table-responive">
            </div>
        </div>
        <div id="sindatos" style="display:none">
            <div class="row">
            <div class="container">
                <div class="row">
                    <div class="jumbotron col-md-12 text-center">
                        <h1><strong><?php echo JrTexto::_('Opss'); ?>!</strong></h1>                
                        <div class="disculpa"><?php echo JrTexto::_('We have not found data')?>:</div> 

                    </div>
                </div>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
var __currentHistorialSesion = [];
var __formatotiempo=function(seg){
    var _f=function(t,d){return t.toString().padStart(d,'0')}
    var _m=function(s){var m_=parseInt(s/60); var s_=s%60; return {'m':_f(m_,2),'s':_f(s_,2)}}
    var _h=function(s){var h_=parseInt(s/3600);var m_=_m(s%3600); m_.h=_f(h_,2); return m_; }
    var _d=function(s){var d_=parseInt(s/86400);var h_=_h(s%86400);h_.d=_f(d_,2); return h_; }
    if(seg<60) return {'s':_f(seg,2)}
    else if(seg<3600) return _m(seg); 
    else if(seg<86400) return _h(seg); 
    else return _d(seg);    
}
var __formatodhms=function(seg){
    var t=__formatotiempo(seg);
    return (t.d!=undefined?(t.d+'d '):'')+(t.h!=undefined?(t.h+'h '):'')+(t.m!=undefined?(t.m+"m "):'')+t.s+"s";
}
var formatooption=function(time,options){ 
  options.vAxis.title='<?php echo JrTexto::_('Hours, Minutes and Seconds'); ?>';
  options.vAxis.format='HH:mm:ss';
  if(time<60) {
    options.vAxis.viewWindow={ min: [0,0,0], max: [0,0,time]};  
  }else if(time<3600) {   
    var m=time/60;
    options.vAxis.viewWindow={ min: [0,0,0], max: [0,(m+1),0]};   
  } 
  else if(time<86400) {    
    var h=time/3600;
    options.vAxis.viewWindow={ min: [0,0,0], max: [h+1,0,0]};
  }
  return options;
}
var __formatodhms2=function(seg){
  var t=__formatotiempo(seg);
  var time2=[parseInt((t.h||0)),parseInt((t.m||0)),parseInt((t.s||0))];
  var tt={time:(((t.d||0)+'d ')+((t.h||0)+'h ')+((t.m||0)+'m ')+((t.s||0)+'s ')),time2:time2};
  return tt;
}

var resetSelectsTiempoCursos = ()=>{
  $("#cmbcursos,#cmbsesiones,#cmbexamenes").html('<option value=""><?=JrTexto::_("All");?></option>')
}

var asd = ()=>{}

$(document).ready(function(){
  var idnumgrafico=0;
  google.charts.load('current', {'packages':['bar', 'corechart', 'table']});

  $('#cmbcursos').on('change',function(){
    const select = $(this)
    const SelectIdcurso = select.val()
    var tr='',tfoot = ''; 
    var sumas ={ tiempo_sesiones: 0, tiempo_examenes: 0,tiempo_curso:0 }


    if(__currentHistorialSesion != null && Object.keys(__currentHistorialSesion).length > 0){
      let element = null;
      $.each(__currentHistorialSesion,function(i,value){
        sumas.tiempo_sesiones = 0
        sumas.tiempo_examenes = 0
        sumas.tiempo_curso = 0
        $.each(value.cursos,function(i,c){
            if(c.idcurso==SelectIdcurso || SelectIdcurso == ""){
              tcurso=c.S;
              texa=c.E;
              tnombre=c.nombre||' - ';
              tr+='<tr><td>'+tnombre+'</td><td class="text-center" time="'+tcurso+'">'+__formatodhms(tcurso)+'</td><td class="text-center" time="'+texa+'">'+__formatodhms(texa)+'</td><td class="text-center" time="'+(tcurso+texa)+'">'+__formatodhms(tcurso+texa)+'</td></tr>';
              sumas.tiempo_sesiones += tcurso;
              sumas.tiempo_examenes += texa;
              sumas.tiempo_curso += tcurso+texa;
            }
        })
        tfoot = `<tr class="text-center">
          <td>Total</td>
          <td>${__formatodhms(sumas.tiempo_sesiones)}</td>
          <td>${__formatodhms(sumas.tiempo_examenes)}</td>
          <td>${__formatodhms(sumas.tiempo_curso)}</td>
        </tr>`
      })
      $('#tablacursos').children('tbody').html(tr);
      $('#tablacursos').children('tfoot').html(tfoot);
      cargarcursos();
    }
  })
  // $('#cmbsesiones').on('change',function(){
  //   console.log("cambio sesiones");
  // })
  // $('#cmbexamenes').on('change',function(){
  //   console.log("cambio examenes");
  // })

  var dibujarchart=function(tipo){
    $('#pnl-panelreporte').show();
    $('#aquitablaamostrar').hide();
    $('#alumnosolocurso').hide();
    $('#alumnosolocurso1').hide();
    var verxalumno=$('.verpor.active').attr('id')=='tab-0'?false:true;
    var data = new google.visualization.DataTable();
    var options = { title: '<?php  echo JrTexto::_("Study times ")?>',subtitle:'',
      chart: { title: '<?php  echo JrTexto::_("Study times ")?>',subtitle:'' },
      legend: { position: 'top', maxLines: 3 },
      bar: { groupWidth: '50px' },
      orientation:'horizontal',
      is3D:true,  
      bar:{ groupWidth: "300px" }      ,                
      height:560,
      colors: ['#1b9e77', '#d95f02', '#7570b3'],
      vAxis:{}
    };
    var maxt=0;
    var tipo_='s';
    
      $('#alumnosolocurso1').show();
      $('#impresioncursos').hide();
      data.addColumn('string', '<?php echo JrTexto::_('Courses'); ?>');
      options.title='<?php  echo JrTexto::_("Study times of")?> :<?php echo $this->user["nombre_full"] ?> ';
      options.subtitle='<?php  echo JrTexto::_("Study times of")?> : <?php echo $this->user["nombre_full"] ?>';  

      var table=$('#impresioncursos #aquitablacursos table tbody');
        table.find('tr').each(function(itr,tr){        
          $(tr).find('td').each(function(itd,td){
            var time=$(td).attr('time')||0;
            if(itd>0) maxt=(maxt<parseInt(time)?parseInt(time):maxt);          
          });        
        })     
        if(maxt<86400){
          data.addColumn('timeofday', '<?php echo JrTexto::_("Time in Sessions"); ?>');
          data.addColumn('timeofday', '<?php echo JrTexto::_("Time in Exam"); ?>');
          data.addColumn('timeofday', '<?php echo JrTexto::_("Time in course"); ?>');
          options=formatooption(maxt,options);       
        }else{ 
          data.addColumn('number', '<?php echo JrTexto::_("Time in Sessions");?>');
          data.addColumn('number', '<?php echo JrTexto::_("Time in Exam");?>');
          data.addColumn('number', '<?php echo JrTexto::_("Time in course");?>');
          options.vAxis={minValue: 0,title:'<?php echo JrTexto::_("Days");?>',annotation:' <?php echo JrTexto::_("Days");?>'};
        }

        table.find('tr').each(function(itr,tr){
          var trtmp=[];
          $(tr).find('td').each(function(itd,td){
            var tt='';
            if(itd==0){
              tt=$(td).text();
            }else{
              if(maxt<86400){
                var time=__formatodhms2(parseInt($(td).attr('time')||0));
                tt=time.time2;
              }else tt=parseInt($(td).attr('time')||0)/86400;            
            }          
          trtmp.push(tt);
          });
          data.addRow(trtmp);
        })


    idnumgrafico++;
    var divid='grafico'+idnumgrafico;
    var div='<div id="'+divid+'" style="width:px;"></div>';
    $('#pnl-panelreporte #grafico').html(div); 
    var char=null;
    if(tipo=='area') chart = new google.visualization.AreaChart(document.getElementById(divid));
    else if(tipo=='line')chart = new google.visualization.LineChart(document.getElementById(divid));
    else if(tipo=='pie')chart = new google.visualization.PieChart(document.getElementById(divid));
    else chart = new google.visualization.BarChart(document.getElementById(divid));
    try{
      chart.draw(data, options);
    }catch(ex){ console.log(ex); verpor();}
  }
  var nohaydata=function(){
    $('#sindatos').show();
    $('#impresion').hide();
    $('#impresioncursos').hide();
    $('#alumnosolocurso').hide();
    $('#alumnosolocurso1').hide();
    $('#pnl-panelreporte').hide();
    $('#aquitablaamostrar').hide();
  }
  var verpor=function(){
    $('#sindatos').hide();
      var $obj=$('.verpor.active');
      var poralumno=$obj.attr('id')=='tab-0'?false:true;
      var vistaactive=$('.changevista.active').attr('vista')||'table';  
      $('#pnl-panelreporte').hide();
        $('#porgrupo').hide();
        $('#poralumno').show();
        $('#impresion').hide();
        $('#impresioncursos').show();  
        $('#alumnosolocurso1').show();  
        $('#alumnosolocurso').hide();
        var data=new FormData();
          $this=$(this);
          data.append('idpersona',<?php echo $this->user["idpersona"];?>);
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/historial_sesion/_tiemposxalumno/',
              callback:function(rs){
                  if(rs.code==200){
                      $pln=$('#alumnosolocurso1');
                      $impresion=$('#impresion');
                      $pln.show();    
                      var sumas ={ tiempo_sesiones: 0, tiempo_examenes: 0,tiempo_curso:0 }
                      var tr='',tfoot = '';    
                      var haydata=false;                
                      // console.log(rs.data)
                      __currentHistorialSesion = rs.data;
                      resetSelectsTiempoCursos();
                      $.each(rs.data,function(i,v){
                          var texa=0, tcurso=0;

                          sumas.tiempo_sesiones = 0
                          sumas.tiempo_examenes = 0
                          sumas.tiempo_curso = 0

                          $pln.find('._tiempoenpv').text(__formatodhms(v.P)).attr('time',v.P);
                          $pln.find('._tiempoenexamen').text(__formatodhms(v.E)).attr('time',v.E);
                          $pln.find('._tiempoencurso').text(__formatodhms(v.S)).attr('time',v.S);                            
                          $.each(v.cursos,function(i,c){
                            haydata=true;
                              tcurso=c.S;
                              texa=c.E;
                              tnombre=c.nombre||'- ';
                              tr+='<tr><td>'+tnombre+'</td><td class="text-center" time="'+tcurso+'">'+__formatodhms(tcurso)+'</td><td class="text-center" time="'+texa+'">'+__formatodhms(texa)+'</td><td class="text-center" time="'+(tcurso+texa)+'">'+__formatodhms(tcurso+texa)+'</td></tr>';
                              
                              sumas.tiempo_sesiones += tcurso;
                              sumas.tiempo_examenes += texa;
                              sumas.tiempo_curso += tcurso+texa;

                              if(c.nombre != null && c.nombre != ""){
                                $("#cmbcursos").append(`<option value="${c.idcurso}">${tnombre}</option>`);
                              }
                          })
                          tfoot = `<tr class="text-center">
                            <td>Total</td>
                            <td>${__formatodhms(sumas.tiempo_sesiones)}</td>
                            <td>${__formatodhms(sumas.tiempo_examenes)}</td>
                            <td>${__formatodhms(sumas.tiempo_curso)}</td>
                          </tr>`
                      })
                      if(haydata==true){
                        $('#tablacursos').children('tbody').html(tr);
                        $('#tablacursos').children('tfoot').html(tfoot);
                        cargarcursos();
                      }else nohaydata();
                  }
              }
          });
      if(vistaactive!='table'){
        dibujarchart(vistaactive);
      } 

  }


        $('.changevista').click(function(ev){
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            verpor();
        })

        var cargarcursos=function(){
            var tb=$('#tablacursos').clone();
            var idtable='table'+Date.now();
            tb.attr('id',idtable);
            $('#impresion').hide();
            $('#impresioncursos').show();
            $('#aquitablacursos').html(tb);                       
            $('#aquitablacursos table#'+idtable).DataTable({
                ordering: true,
                searching: false,
                pageLength: 50,
                info: false,
                oLanguage: {
                    "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
                    "oPaginate": {
                        "sFirst": "<?php echo JrTexto::_("First page") ?>", // This is the link to the first page
                        "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                        "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                        "sLast": "<?php echo JrTexto::_("Last page") ?>" // This is the link to the last page
                    }
                },
                dom: 'Bfrtip',
                buttons: [{
                    extend: "excelHtml5",
                    className: 'btn-dt btn-excel',
                    text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
                    messageTop: '',
                    title: '<?php echo JrTexto::_('Report') ?>',
                    customize: function(xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        var col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                        $tr = $('#aquitablacursos '+idtable+' tbody').find('tr');
                        $.each($tr, function(ir, row) {
                            $(this).find('td').each(function(ic, td) {
                                var xcol = col[ic];
                                if (ir == 0) $('row c[r="' + xcol + '1"]', sheet).attr('s', 32);
                                else {
                                    var s = ir % 2 == 0 ? 8 : 20;
                                    $('row c[r="' + xcol + (ir + 1) + '"]', sheet).attr('s', s);
                                }
                            })
                        })
                    }
                }, {
                    extend: "pdfHtml5",
                    messageTop: '',
                    className: 'btn-dt btn-pdf',
                    text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
                    title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Time') . " - " . date('d-m-Y'); ?>',
                    customize: function(doc) {
                        doc.pageMargins = [12, 12, 12, 12];
                         $tr = $('#aquitablacursos '+idtable+' tbody').find('tr');
                        var ntr = $tr.length;
                        var ntd = $tr.eq(0).find('th').length;
                        var ncolA = [30, "*", "*", "*"];
                        /*for(i=0;i<ntd-2;i++){
                            ncolA.push("*");
                        }*/
                        doc.content[1].table.widths = ncolA;
                        var tblBody = doc.content[1].table.body;
                        $tr.each(function(ir, row) {
                            var color = ((ir % 2 == 0) ? '' : '#e4e1e1');
                            if ($(this).hasClass('nomcursos')) {
                                $(this).find('td').each(function(ic, td) {
                                    tblBody[ir][ic].fillColor = '#5c5858';
                                    tblBody[ir][ic].color = "#FFFFFF";
                                })
                            }
                        })
                        tblBody[ntr][0].alignment = 'left';
                        if (_logobase64 != '') {
                            doc.content.splice(0, 0, {
                                margin: [0, 0, 0, 12],
                                alignment: 'center',
                                image: _logobase64
                            });
                        }
                    }

                }, {
                    extend: "print",
                    className: 'btn-dt btn-print',
                    text: '<i class="fa fa-print" aria-hidden="true"></i>',
                    title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Time') . " - " . date('d-m-Y'); ?>',
                    customize: function(win) {
                        var body = $(win.document.body);
                        var title = body.find('h1').first().text();
                        if (_logobase64 != '') {
                            body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="' + _logobase64 + '"/><br>' + title + '</div>');
                        } else {
                            body.find('h1').first().html('<div style="text-align:center; width:100%;">' + title + '</div>');
                        }
                        table = body.find('table');
                        table.find('th').each(function(index) {
                            table.find('th').css('background-color', '#cccccc')
                        });
                    }
                }]
            });
          var vistaactive=$('.changevista.active').attr('vista')||'table';
          if(vistaactive!='table'){
            dibujarchart(vistaactive);
          }

 
        }

        verpor();

    })
</script>