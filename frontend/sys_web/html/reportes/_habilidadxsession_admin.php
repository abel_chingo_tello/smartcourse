<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
?>
<?php if (!$ismodal): ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Skills by sessions"); ?>'
        });
        const __usuario = <?= json_encode($this->user) ?>
    </script>
<?php endif; ?>
<style>
    .dt-table,.mdl-grid { width: 100%!important;}
    .mdl-grid { margin-bottom:1.4em; }
    .step { transition: all ease .2s; opacity:1; }
    .step.d-none { opacity:0; }
    .select2-container { width:100%!important; }
</style>
<div class="container my-font-all">
    <!--START FIND ENTERPRISE-->
    <div class="my-shadow my-card graf-cont mb-3" id="pstep1">
        <h4 class="font-weight-bold"><?php echo ucfirst(JrTexto::_('Step')); ?> 1: <?php echo JrTexto::_('Specify search by Company'); ?></h4>
        <div class="row">
            <div class="col-sm-4">
                <label><?php echo JrTexto::_('Company'); ?></label>
                <select class="form-control" id="slc_empresa"> <!--Dynamic content--> </select>
            </div>
            <div class="col-sm-4">
                <label><?php echo JrTexto::_('Project'); ?></label>
                <select class="form-control" id="slc_proyecto"> <!--Dynamic content--> </select>
            </div>
        </div>
        <div class="text-center" style="padding-top:1em;">
            <button class="btn btn-primary on-continuar" data-step="#pstep2"><?=JrTexto::_("Continue to Step 2");?></button>
        </div>
    </div>
    <!--END FIND ENTERPRISE-->
    <!--START AVISO-->
    <div id="message01" class="text-center my-shadow my-card graf-cont mb-3 d-none">
        <h1><?php echo ucfirst(JrTexto::_('you are not registered in the selected project'));?></h1>
        <h1><i class="fa fa-exclamation-triangle"></i></h1>
    </div>
    <!--END AVISO-->
    <!--START FIND COURSES AND SESION-->
    <div class="my-shadow my-card graf-cont step mb-3 d-none" id="pstep2">
        <?php if($this->user['idrol'] == 1): ?>
            <h4><?php echo JrTexto::_('Step'); ?> 2: <?php echo JrTexto::_('Select course, unit and session'); ?></h4>
        <?php else: ?>
            <h4><?php echo JrTexto::_('Select course, unit and session'); ?></h4>
        <?php endif; ?>
        <div class="row">
            <div class="col-sm-2">
                <label><?php echo JrTexto::_('Group'); ?></label>
                <div>
                    <select class="form-control" id="slc_grupos"> <!--Dynamic content--> </select>
                </div>
            </div>
            <div class="col-sm-2">
                <label><?php echo JrTexto::_('Courses'); ?></label>
                <div>
                    <select class="form-control" id="slc_cursos"> <!--Dynamic content--> </select>
                </div>
            </div>
            <div class="col-sm-4">
                <label><?php echo JrTexto::_('Unit'); ?></label>
                <select class="form-control" id="slc_unidades"> <!--Dynamic content--> </select>
            </div>
            <div class="col-sm-4">
                <label><?php echo JrTexto::_('Sesion'); ?></label>
                <select class="form-control" id="slc_sesiones"> <!--Dynamic content--> </select>
            </div>
        </div>
        <div class="text-center" style="padding-top:1em;">
            <button class="btn btn-success on-find"><?=JrTexto::_("Find");?></button>
        </div>
    </div>
    <!--END FIND COURSES AND SESION-->
    <!--START TABLE RESULT-->
    <div class="my-shadow my-card graf-cont mb-3 step d-none" id="preport">
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="tablereport">
                <thead>
                    <tr>
                        <th><?=JrTexto::_("Document");?></th>
                        <th><?=JrTexto::_("Student name");?></th>
                        <th><?=JrTexto::_("Listening");?></th>
                        <th><?=JrTexto::_("Reading");?></th>
                        <th><?=JrTexto::_("Writing");?></th>
                        <th><?=JrTexto::_("Speaking");?></th>
                        <th><?=JrTexto::_("Total");?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div>DNI</div>
                            <div>123456</div>
                        </td>
                        <td>PEPE</td>
                        <td>
                            <div>0% / 100 %</div>
                            <div>0 pts / 20 pts</div>
                        </td>
                        <td>
                            <div>0% / 100 %</div>
                            <div>0 pts / 20 pts</div>
                        </td>
                        <td>
                            <div>0% / 100 %</div>
                            <div>0 pts / 20 pts</div>
                        </td>
                        <td>
                            <div>0% / 100 %</div>
                            <div>0 pts / 20 pts</div>
                        </td>
                        <td>
                            <b>0 pts / 20 pts</b>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!--END TABLE RESULT-->
</div>