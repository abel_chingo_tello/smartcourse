<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Tareas"); ?>'
    });
    /*addButonNavBar({
      class: 'btn btn-success btn-sm Agreggarforo',
      text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('add') ?>',
      href: '<?php echo $this->documento->getUrlSitio(); ?>/foros/agregar'
    });*/
  </script>
<?php } ?>
<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
<?php } ?>
<style type="text/css">
  .puntaje {
    position: absolute;
    right: 35px;
    top: 35px;
  }

  .puntaje small {
    font-weight: 1.5em;
  }

  #chat #header-chat {
    background-color: #555;
    color: #20f1f14d;
    padding: 10px;
    text-align: center;
    text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.6);
  }

  #chat #mensajes {
    padding: 10px;
    height: 400px;
    width: 100%;
    overflow: hidden;
    overflow-y: scroll
  }

  #chat #mensajes .mensaje-autor {
    margin-bottom: 20px;
    float: left;
  }

  #chat #mensajes .mensaje-autor img,
  #chat #mensajes .mensaje-amigo img {
    display: inline-block;
    vertical-align: top;
    max-width: 50px;
    max-height: 50px;
  }

  #chat #mensajes .mensaje-autor .contenido {
    background-color: #20f1f14d;
    border-radius: 5px;
    box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);
    display: inline-block;
    font-size: 13px;
    padding: 15px;
    vertical-align: top;
  }

  #chat #mensajes .mensaje-autor .fecha {
    color: #777;
    font-style: italic;
    font-size: 13px;
    text-align: right;
    margin-right: 35px;
    margin-top: 10px;
  }

  #chat #mensajes .mensaje-autor .flecha-izquierda {
    display: inline-block;
    margin-right: -6px;
    margin-top: 10px;
    width: 0;
    height: 0;
    border-top: 0px solid transparent;
    border-bottom: 15px solid transparent;
    border-right: 15px solid #20f1f14d;
  }

  #chat #mensajes .mensaje-amigo {
    margin-bottom: 20px;
    float: right;
  }

  #chat #mensajes .mensaje-amigo .contenido {
    background-color: #3990BF;
    border-radius: 5px;
    color: white;
    display: inline-block;
    font-size: 13px;
    padding: 15px;
    vertical-align: top;
  }

  #chat #mensajes .mensaje-amigo .flecha-derecha {
    display: inline-block;
    margin-left: -6px;
    margin-top: 10px;
    width: 0;
    height: 0;
    border-top: 0px solid transparent;
    border-bottom: 15px solid transparent;
    border-left: 15px solid #3990BF;
  }

  #chat #mensajes .mensaje-amigo img,
  #chat #mensajes .mensaje-autor img {
    border-radius: 5px;
  }

  #chat #mensajes .mensaje-amigo .fecha {
    color: #777;
    font-style: italic;
    font-size: 12px;
    text-align: left;
    margin-top: 0px;
  }
</style>
<div class="row">
  <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
    <label class="control-label"><?php echo JrTexto::_('My grupos'); ?></label>
    <select id="idgrupoaula" name="idgrupoaula" class="form-control" idsel="<?php echo @$this->idgrupoaula; ?>">
      <?php if (!empty($this->misgrupos))
        foreach ($this->misgrupos as $fk => $v) {
          //var_dump($v);
      ?>
        <option value="<?php echo $fk; ?>"><?php echo $v["nombre"] ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
    <label class="control-label"><?php echo JrTexto::_('My courses'); ?></label>
    <select id="idcurso" name="idcurso" class="form-control" idsel="<?php echo @$this->idcurso; ?>">
      <?php if (!empty($this->gruposauladetalle))
        foreach ($this->gruposauladetalle as $fk) { ?>
        <option value="<?php echo $fk["idcurso"] ?>" data-idgrupoaula="<?php echo $fk["idgrupoaula"] ?>" data-idgrupoauladetalle="<?php echo $fk["idgrupoauladetalle"] ?>" data-idcomplementario="<?php echo $fk["idcomplementario"] ?>" style="display: none;"><?php echo $fk["strcurso"] ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
    <label class="control-label"><?php echo JrTexto::_('My courses'); ?></label>
    <select id="idalumno" name="idalumno" class="form-control" idsel="<?php echo @$this->idalumno; ?>">
      <option value="" selected="selected"> Todos </option>
      <?php if (!empty($this->alumnosxgrupoaula))
        foreach ($this->alumnosxgrupoaula as $fk => $v) {
          if (!empty($v))
            foreach ($v as $alu) { //var_dump($alu); 
      ?>
          <option value="<?php echo $alu["idalumno"] ?>" data-idgrupoauladetalle="<?php echo $alu["idgrupoauladetalle"] ?>" style="display: none;"><?php echo $alu["stralumno"] ?></option>
      <?php }
        } ?>
    </select>
  </div>
  <div class="col-md-4">

  </div>
  <div class="col-md-12 " id="">
    <div class="accordion" id="vistalisatotareas" role="tablist" aria-multiselectable="true">
      <div class="panel panelclone col-md-6" style="display: none;">
        <a class="panel-heading collapsed" role="tab" id="heading" data-toggle="collapse" href="#collapse" aria-expanded="false" aria-controls="collapse">
          <h4 class="panel-title">Tarea : <small></small> </h4>
        </a>
        <div class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading" style="">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 text-center table-responsive">
                <table class="table">
                  <thead>
                    <tr class="">
                      <th class="text-left">Estudiantes</th>
                      <th>Archivos de Referencia</th>
                      <th>Notas</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="trclone" style="display: none;">
                      <td class="nombre text-left"> Abel chingo Tello</td>
                      <td> <a href="#" target="_blank" class="archivoalumno"><i class="fa fa-cloud-download" style="font-size: 2em"></i><br> ver o descargar </a></td>
                      <td class="vernota">20</td>
                      <td><a href="#" class="subirtarea"><i class="fa fa-comments-o"></i> <br> ver Chat </td></a>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="idcriterios" style="display: none">
      <div class="col-md-12 table-responsive criterio">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Criterio</th>
              <th>Nota 0-100</th>
            </tr>
          </thead>
          <tbody>
            <tr class="trclone" style="display: none">
              <td class="criterio"></td>
              <td><input class="nota" type="number" min="0" max="100"></td>
            </tr>
          </tbody>
        </table>
        <div class="form-group col-md-12">
          <label class="col-md-6 text-right">Nota Final 0-100</label>
          <input type="number" name="notafinal" min="0" max="100" class="notafinal form-control col-md-6" value="0.0">
        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="col-md-12 text-center">
          <a href="#" class="btn btn-warning cerrarmodal">Cerrar</a>
          <a href="#" class="btn btn-primary btnguardar"><i class="fa fa-save"></i> Guardar</a>
        </div>
      </div>
    </div>
    <div style="display: none">
      <div id="subirtarea">
        <div class="x_panel">
          <div class="x_title">
            <h2 style="font-size: 22px;"><i class="fa fa-tasks estarea"></i> <small class="estarea"><?php echo ucfirst(JrTexto::_('Tasks')); ?></small> <span class="estareaproyecto" style="display: none;">/</span>
              <i class="fa fa-folder esproyecto"></i> <small class="esproyecto"><?php echo ucfirst(JrTexto::_('Projects')); ?></small></h2>

            <div class="clearfix"></div>
          </div>
          <div class="x_content" style="border: 0px; padding: 0px; margin: 1ex 0px;">
            <ul class="nav nav-tabs bar_tabs" role="tablist">
              <!--li class="nav-item">
            <a class="nav-link active idtab isa" id="frmsubirarchivo_tab" data-toggle="tab" role="tab" href="#frmsubirarchivo">Subir Archivo</a>
          </li-->
              <li class="nav-item">
                <a class="nav-link active idtab isa" id="listadomensajes-tab" data-toggle="tab" role="tab" href="#listadomensajes">Mensajes</a>
              </li>
              <!--li class="nav-item">
            <a class="nav-link idtab isa" id="listadoarchivos-tab" data-toggle="tab" role="tab" href="#listadoarchivos">Archivos subidos</a>
          </li-->
            </ul>
            <div class="tab-content">
              <!--div class="tab-pane fade show active idtab" id="frmsubirarchivo" role="tabpanel" aria-labelledby="frmsubirarchivo_tab">
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                  Click en el tipo de archivo que deseas enviarle a tu docente, escribele un mensaje y enviale dando click en enviar
                </div>
              </div>
              <div class="col-md-12">
                <form class="row" id="frmarchivosmultimedia" method="post" target="" enctype="multipart/form-data">
                  <div class="col-md-6 form-group">
                    <label><?php echo JrTexto::_("Tipo de Archivo") ?></label>
                    <br>
                    <div class="btn-group" role="group" aria-label="">
                      <button type="button" class="btn btn-secondary btntipofile" title="texto"><i class="fa fa-font"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="imagen"><i class="fa fa-photo"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="audio"><i class="fa fa-file-audio-o"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="video"><i class="fa fa-video"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="pdf"><i class="fa fa-file-pdf"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="html"><i class="fa fa-html5"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="link"><i class="fa fa-link"></i></button>
                    </div>
                  </div>
                  <div class="col-md-6" id="aquitipofile"></div>
                  <div class="col-md-12 form-group">
                    <label style=""><?php echo JrTexto::_('Mensaje'); ?> <i class="fa fa-comment"></i> </label>
                    <textarea id="_mensaje_" class="form-control" rows="4" placeholder="<?php echo  ucfirst(JrTexto::_("Mensaje")) ?>" value="" style="resize: none;"></textarea>
                  </div>
                </form>
              </div>
              <div class="col-md-12 text-center">
                <button class="btnguardartarea btn btn-primary "> <i class="fa fa-envelope-o"></i> Enviar Mensaje</button>
              </div>
            </div>
          </div-->
              <div class="tab-pane fade show active idtab mensajeenviados" id="listadomensajes" role="tabpanel" aria-labelledby="listadomensajes-tab">
                <div id="chat">
                  <div id="mensajes">
                    <h2 class="text-center">No tiene Mensajes enviados</h2>
                  </div>
                </div>
                <div id="caja-mensaje">
                  <textarea id="_mensaje2_" class="form-control" rows="2" placeholder="<?php echo  ucfirst(JrTexto::_("Mensaje")) ?>" value="" style="resize: none;"></textarea>
                  <div class="text-center">
                    <button style="margin: 1ex" class="btn btn-primary btnsendmensaje"> Enviar Mensaje → </button>
                  </div>
                </div>
              </div>
              <!--div class="tab-pane table-responsive fade idtab archivosenviados" id="listadoarchivos" role="tabpanel" aria-labelledby="listadoarchivos-tab">
            <table class="table  table-striped">
              <thead>
                <tr>
                  <th>Archivo</th>
                  <th>Fecha</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="3">Aun no a enviado archivos de este trabajo</td>
                </tr>
              </tbody>
            </table>
          </div-->
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 text-center" style="padding: 1ex;">
              <button class="salirmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar </button>
            </div>
          </div>
        </div>
      </div>

    </div>


  </div>
  <script type="text/javascript">
    /*addItemNavBar({
      text: '<?php echo JrTexto::_("Tareas"); ?>'
    });*/
    $(document).ready(function() {


      $('select#idalumno').on('change', function(ev) {
        var data = new FormData()
        data.append('idcurso', $('select#idcurso').val() || '');
        data.append('tipo', 'T');
        data.append('idcomplementario', $('#idcurso option:selected').data("idcomplementario"));
        if ($('select#idalumno').val() != '') data.append('idalumno', $('select#idalumno').val())
        __sysAyax({
          fromdata: data,
          url: _sysUrlBase_ + 'json/tareas',
          callback: function(rs) {
            html = '';
            $("#vistalisatotareas").children('.panelclone').siblings().remove();
            var hay = false;
            if (rs.code == 200) {
              $.each(rs.data, function(i, item) {
                $panelclone = $('#vistalisatotareas').children('.panelclone').clone().show();
                $panelclone.removeClass('panelclone');
                $panelclone.children('a.panel-heading').attr('id', 'headding' + i).attr('href', '#collapse' + i);
                $panelclone.children('a.panel-heading').children('h4').children('small').text(item.nombre.trim() || '');
                $panelclone.children('.panel-collapse').attr('id', 'collapse' + i);
                var hayalu = item.dt;
                $.each(hayalu, function(ii, v) {
                  $trclone = $panelclone.find('.trclone').clone();
                  $trclone.removeClass('trclone').show();
                  $trclone.attr('id', v.idtarea).attr('tipo', v.tipo).attr('idalumno', v.idalumno)
                  $trclone.children('.nombre').text(v.nombre_alumno);
                  var arch = v.archivos;
                  var hayarchivos = false;
                  $.each(arch, function(iar, arc) {
                    $trclone.find('.archivoalumno').attr('href', _sysUrlBase_ + arc.media);
                    hayarchivos = true;
                  })
                  if (hayarchivos == false) {
                    $trclone.find('.archivoalumno').closest('td').text('-----');
                  }
                  $trclone.find('.vernota').text(v.nota);
                  $trclone.find('a.subirtarea').attr('tipo', v.tipo).attr('idcurso', v.idcurso).attr('idsesion', v.idsesion).attr('idpestania', v.idpestania).attr('data-link', v.recurso).attr('data-tipolink', v.tiporecurso).attr('iddocente', v.iddocente).attr('data-nombre', v.nombre).attr('data-descripcion', v.descripcion).attr('criterios', v.criterios).attr('idtarea', v.idtarea);

                  var cripuntaje = v.criterios_puntaje || 'ninguno';
                  if (cripuntaje != 'ninguno') cripuntaje = btoa(cripuntaje);
                  $trclone.find('.vernota').attr('criterios', v.criterios).attr('criterios_puntaje', cripuntaje);

                  $panelclone.find('tbody').append($trclone);
                })
                //                $panelclone.find('a.archivodocente').attr('href',item.recurso);                  
                $("#vistalisatotareas").append($panelclone);
                hay = true;
              });
            }
            if (hay == false) {
              $("#vistalisatotareas").append('<table class="table"><thead><tr class=""><th class="text-center"><h3>No hay ninguna actividad registrada para este curso.</h3></th></tr></thead></table>');
            }
          }
        });
      })

      $('#vistalisatotareas').on('click', '.vernota', function(ev) {
        var el = $(this);
        var nota = $(this).text();
        var criterios_puntaje = el.attr('criterios_puntaje') || 'ninguno';
        if (criterios_puntaje != '' && criterios_puntaje != 'ninguno') {
          try {
            criterios_puntaje = JSON.parse(atob(criterios_puntaje));
          } catch (ex) {
            console.log(ex)
          }
        } else
          criterios_puntaje = [];
        var idtarea = $(this).closest('tr').attr('id') || 0;
        var htmltarea = $('#idcriterios').clone(true).html();
        var dt = {
          html: htmltarea
        };
        var md = __sysmodal(dt);
        var data = new FormData()
        data.append('idcriterio', $(this).attr('criterios') || 'ninguno');
        __sysAyax({
          fromdata: data,
          url: _sysUrlBase_ + 'json/acad_criterios',
          callback: function(rs) {
            if (rs.data.length > 0) {
              $.each(rs.data, function(rr, vv) {
                var tr = md.find('.trclone').clone().show().removeClass('trclone');
                tr.attr('idcriterio', vv.idcriterio);
                tr.find('.criterio').text(vv.nombre);
                tr.find('input').val(0);
                tr.find('input').attr('idcriterio', vv.idcriterio);
                $.each(criterios_puntaje, function(iii, c) {
                  if (vv.idcriterio == c.id) tr.find('input').val(c.nota);
                })
                md.find('tbody').append(tr);
              })
              md.find('input.nota').last().trigger('change');

            } else {
              md.find('table').remove();
              md.find('input.notafinal').val(parseInt(el.text()))
            }

          }
        })
        md.on('change', 'input.nota', function(ev) {
          var inputs = md.find('input.nota');
          var sum = 0;
          if (inputs.length) {
            $.each(inputs, function(i, v) {
              sum = sum + parseInt($(v).val()) || 0;
            })
            md.find('input.notafinal').val(sum);
            if (sum > 100) md.find('input.notafinal').css({
              "border": "1px solid red"
            }).attr('readonly', 'readonly');
            else md.find('input.notafinal').css({
              "border": "1px"
            })
          } else {
            md.find('input.notafinal').val(nota);
          }
        }).on('click', '.btnguardar', function(ev) {
          var inputs = md.find('input.nota');
          var sum = 0;
          var criterios = [];
          if (inputs.length) {
            $.each(inputs, function(i, v) {
              sum = sum + parseInt($(v).val()) || 0;
              if (!$(v).hasClass('trclone')) criterios.push({
                'id': $(v).attr('idcriterio'),
                "nota": $(v).val() || 0
              });
            })
            if (sum > 100) {
              md.find('input.notafinal').css({
                "border": "1px solid red"
              }).attr('readonly', 'readonly');
              return false;
            } else md.find('input.notafinal').css({
              "border": "1px"
            })
          } else {
            sum = md.find('input.notafinal').val()
          }
          var da = new FormData()
          da.append('idtarea', idtarea);
          da.append('nota', sum);
          if (inputs.length) da.append('criterios', JSON.stringify(criterios));
          __sysAyax({
            fromdata: da,
            url: _sysUrlBase_ + 'json/tareas/updateNota',
            callback: function(rs) {
              el.text(sum);
              __cerrarmodal(md);
            }
          })
        })
      }).on('click', '.subirtarea', function(ev) {
        var htmltarea = $('#subirtarea').clone(true);
        htmltarea.find('.idtab').each(function(i, v) {
          if ($(v).hasClass('isa')) $(v).attr('href', $(v).attr('href') + contab);
          $(v).attr('id', $(v).attr('id') + contab);
        })

        var el = $(this);
        var tipotarea = '';

        if (el.attr('tipo') == 'TP') {
          htmltarea.find('.estareaproyecto').show();
          tipotarea = 'TP';
        } else if (el.attr('tipo') == 'T') {
          htmltarea.find('.esproyecto').remove();
          tipotarea = 'T';
        } else if (el.attr('tipo') == 'P') {
          htmltarea.find('.estarea').remove();
          tipotarea = 'P';
        }
        var dt = {
          html: htmltarea
        };
        //console.log('dt',dt);
        var md = __sysmodal(dt);
        var _cargarmensajesyarchivos = function() {
          var formData = new FormData();
          formData.append('idcurso', parseInt(el.attr('idcurso')));
          formData.append('idsesion', parseInt(el.attr('idsesion')));
          formData.append('idpestania', parseInt(el.attr('idpestania') || '0'));
          formData.append('tipo', tipotarea);
          formData.append('idtarea', el.attr('idtarea'));
          formData.append('idcomplementario', $('#idcurso option:selected').data("idcomplementario"));
          __sysajax({
            fromdata: formData,
            url: _sysUrlBase_ + 'json/tareas/archivosalumnoymensajes',
            showmsjok: false,
            callback: function(rs) {
              $pnlmensajes = md.find('.mensajeenviados #mensajes');
              if (rs.mensajes) {
                $pnlmensajes.html('');
                var yo = '<?php echo $this->usuarioAct["idpersona"] ?>';
                $.each(rs.mensajes, function(i, v) {
                  var fotouser = (v.foto == '' || v.foto == null) ? 'static/media/usuarios/user_avatar.jpg' : v.foto;
                  var ifoto = fotouser.lastIndexOf('static/');
                  if (ifoto == -1) {
                    fotouser = _sysUrlBase_ + 'static/media/usuarios/user_avatar.jpg';
                  } else fotouser = _sysUrlBase_ + fotouser.substring(ifoto);
                  //  console.log('usu'+v.idusuario,"yo:"+yo);
                  if (v.idusuario == yo) {
                    var msj = '<div class="mensaje-autor"><img src="' + fotouser + '" alt="" class="foto img-responsive"><div class="flecha-izquierda"></div><div class="contenido"><b>' + v.usuario + '</b><br>' + v.mensaje + '</div><div class="fecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                  } else {
                    msj = '<div class="mensaje-amigo" ><div class="contenido">' + v.mensaje + '</div><div class="flecha-derecha"></div><img src="' + fotouser + '" alt="" class="foto img_responsive"><div class="fecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                  }
                  $pnlmensajes.append(msj);
                })
              }
              $pnlarchivos = md.find('.archivosenviados table tbody');
              if (rs.archivos) {
                $pnlarchivos.html('');
                $.each(rs.archivos, function(i, v) {
                  var descripcion = v.media.split("/trabajos/")[1].split("?")[0];
                  var html = '<tr><td><a href="' + _sysUrlBase_ + v.media + '" target="_blank" download class="btn btn-sm"> <i class="fa fa-download"></i> ' + descripcion + '</a></td><td>' + v.fecha_registro + '</td><td class="text-center"> <i class="fa fa-trash borrararchivosubido" style="cursor:pointer" id="' + v.idtarearecurso + '"></i></tr>';
                  $pnlarchivos.append(html);
                })
              }
            }
          })
        }
        var _guardartarea = function(solomsj = false) {
          var link = el.attr('data-link');
          var tipolink = el.attr('data-tipolink');
          if (tipolink == 'smartquiz') link = _sysUrlSitio_ + link.substring(link.lastIndexOf("/quiz/"));
          else link = _sysUrlBase_ + link.substring(link.lastIndexOf("static/"));
          var tipomedia = md.find('#infolink').attr('tipolink') || '';
          if (tipomedia == 'texto' || tipomedia == 'link') mediainfo = md.find('#infolink').attr('value') || '';
          else mediainfo = md.find('#infolink').val() || '';
          if (mediainfo == undefined) mediainfo = '';
          $mensaje = md.find('#_mensaje_').val() || '';
          if (mediainfo != '' && $mensaje != '') {
            $mensaje = '<a href="' + _sysUrlBase_ + mediainfo + '" class="btn btn-primary btn-xs" download="" target="_blank"><i class="fa fa-download"></i> File</a><br>' + $mensaje;
          }
          var formData = new FormData();
          iddocente = '<?php echo $this->usuarioAct["idpersona"]; ?>';
          formData.append('tipo', tipotarea);
          formData.append('idcurso', parseInt(el.attr('idcurso')));
          formData.append('idsesion', parseInt(el.attr('idsesion')));
          formData.append('idpestania', parseInt(el.attr('idpestania') || '0'));
          formData.append('idtarea', parseInt(el.attr('idtarea')));
          formData.append('recurso', link);
          formData.append('tiporecurso', tipolink);
          formData.append('iddocente', iddocente);
          formData.append('nombre', el.attr('data-nombre') || '');
          formData.append('descripcion', el.attr('data-descripcion') || '');
          formData.append('media', mediainfo);
          formData.append('tipomedia', tipomedia);
          formData.append('criterios', el.attr('criterios') || '');
          formData.append('idcomplementario', $('#idcurso option:selected').data("idcomplementario"));
          if (solomsj) {
            formData.append('solomensaje', true);
            formData.append('mensaje', md.find('#_mensaje2_').val() || '');
            md.find('#_mensaje2_').val('');
          } else {
            formData.append('mensaje', $mensaje);
            md.find('#_mensaje_').val('');
          }
          __sysajax({
            fromdata: formData,
            url: _sysUrlBase_ + 'json/tareas/guardar',
            callback: function(rs) {
              _cargarmensajesyarchivos();
            }
          })
        }
        md.on('click', '.salirmodal', function(ev) {
          __cerrarmodal(md, true);
        }).on('click', '.btnguardartarea', function(ev) {
          _guardartarea(false);
        }).on('click', '.btntipofile', function(ev) {
          var el_ = $(this);
          el_.siblings().removeClass('btn-primary').addClass('btn-secondary');
          el_.addClass('btn-primary').removeClass('btn-secondary');
          var title = (el_.attr('title') || 'pdf').toLowerCase();
          var aquitipofile = md.find('#aquitipofile');
          if (title == 'texto') {
            aquitipofile.html('<br><textarea id="infolink" tipolink="texto" class="form-control" rows="5" placeholder="Copie o escriba su texto de repuesta"  style="resize: none;"></textarea>');
          } else if (title == 'imagen') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink" tipolink="imagen" ><div><img src="" class="img-responsive" id="' + idimagen + '" style="max-width:130px; max-height:130px;"></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'imagen',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              //console.log(rs);
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('src', url_media + f);
            });

          } else if (title == 'video') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink"  tipolink="video"><div><video controls src="" class="img-responsive" id="' + idimagen + '" style="max-width:130px; max-height:130px;"></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'video',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('src', url_media + f);
            });

          } else if (title == 'audio') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink"  tipolink="audio"><div><audio controls src="" class="img-responsive" id="' + idimagen + '"></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'audio',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('src', url_media + f);
            });

          } else if (title == 'html') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink"  tipolink="html"><div><a target="_blank" src="img" class="img-responsive" id="' + idimagen + '">Link subido</a></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'html',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('href', url_media + f);
            });

          } else if (title == 'pdf') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink"  tipolink="pdf"><div><a target="_blank" src="img" class="img-responsive" id="' + idimagen + '">PDF subido</a></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'pdf',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('href', url_media + f);
            });

          } else if (title == 'link') {
            aquitipofile.html('<br><input id="infolink"  tipolink="link" value="" placeholder="Copie o escriba el enlace " class="form-control"> ');
          }
        }).on('click', '.borrararchivosubido', function(ev) {
          var el_ = $(this);
          var formData = new FormData();
          formData.append('idtarearecurso', parseInt(el_.attr('id')));
          __sysajax({
            fromdata: formData,
            url: _sysUrlBase_ + 'json/tareas_archivosalumno/eliminar',
            showmsjok: false,
            callback: function(rs) {
              el_.closest('tr').remove();
            }
          })
        }).on('click', '.btnsendmensaje', function(ev) {
          _guardartarea(true);
        })
        _cargarmensajesyarchivos();
        contab++;
      })
      var contab = 0;
      $('select#idgrupoaula').change(function(ev) {
        $('select#idcurso').children('option').hide();
        $('select#idcurso option[data-idgrupoaula="' + $(this).val() + '"]').show();
        $('select#idcurso option[data-idgrupoaula="' + $(this).val() + '"]').first().attr('selected', "selected");
        $('select#idcurso').trigger('change');
      })
      $('select#idcurso').change(function(ev) {
        var idgrupoauladetalle = $('select#idcurso option:selected').attr('data-idgrupoauladetalle') || 0;
        $('select#idalumno').children('option').first().siblings().hide();
        $('select#idalumno option[data-idgrupoauladetalle="' + idgrupoauladetalle + '"]').show();
        $('select#idalumno option[data-idgrupoauladetalle="' + idgrupoauladetalle + '"]').first().attr('selected', "selected");
        $('select#idalumno').val('');
        $('select#idalumno').trigger('change');
      })
      $('select#idgrupoaula').trigger('change');

    })
  </script>
  <?php //var_dump($this->gruposauladetalle); 
  ?>