<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Record"); ?>'
        });
        var __usuario = <?= json_encode($this->usuario) ?>
    </script>
<?php } ?>
<style>
    .graficoContainer {
        margin:0 auto; width:100%!important; height:100%!important; max-width:1191px; max-height:595px;
    }
</style>
<div class="container my-font-all">
    <div class="row my-x-center">
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for=""><?php echo JrTexto::_('Courses'); ?></label>
                <select class="form-control" name="" id="selectCursos">
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectGrupoNota"><?php echo JrTexto::_('Bimester'); ?></label>
                <select class="form-control" name="" id="selectGrupoNota">
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-12">
            <div class="form-group">
                <label for="selectTipoExamen"><?php echo ucfirst(JrTexto::_('Exam type')); ?></label>
                <select id="selectTipoExamen" class="form-control">
                    <option value=""><?=JrTexto::_("All")?></option>
                </select>
            </div>
        </div>
    </div>
    <div id="btnVistasContainer" class="row justify-content-end">
        <div class="col-sm-12 col-md-6 text-right my-card">
            <div class="btn-group p-2">
                <button type="button" class="btn  btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                <button type="button" class="btn btn-secondary btn-sm changevista" vista="area"><i class="fa fa-area-chart"></i></button>
                <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar" ><i class="fa fa-bar-chart"></i></button>
                <button type="button" class="btn btn-secondary btn-sm changevista" vista="line" ><i class="fa fa-line-chart"></i></button>
            </div>
        </div>
    </div>
    <div id="area" class="vista-item" style="display:none;">
        <h4 class="text-center"><strong><?php echo JrTexto::_('Study Notes');?></strong></h4>
        <div class="graficoContainer"></div>
    </div>
    <div id="bar" class="vista-item" style="display:none;">
        <h4 class="text-center"><strong><?php echo JrTexto::_('Study Notes');?></strong></h4>
        <div class="graficoContainer"></div>
    </div>
    <div id="line" class="vista-item" style="display:none;">
        <h4 class="text-center"><strong><?php echo JrTexto::_('Study Notes');?></strong></h4>
        <div class="graficoContainer"></div>
    </div>
    <div id="table" class="row col-sm-12 my-x-center vista-item">
        <div class="box-cont">
            <table id="my-table" class="mdl-data-table my-font my-card my-shadow" style="width:100%">
                <thead id="tb-head" class="my-text-center">
                </thead>
                <tbody id="tbody">
                    <tr class="odd">
                        <td id="tb-msg" valign="top" colspan="4" class="dataTables_empty"><?php echo JrTexto::_('Loading');?>...</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row col-sm my-shadow my-card graf-cont">
        <h1 class="title-graf" style="display:none;"><?php echo JrTexto::_('Loading');?>... </h1>
        <div class="row col-sm-12 my-x-center chart-cont">
            <canvas id="myChart" class="doughnut"></canvas>
        </div>
        <div class="row col-sm-12 my-x-center chart-cont">
            <canvas id="myChart2" class="line"></canvas>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script>
    $(document).ready(()=>{
        const oReporteNota=new ReporteNota();
        oReporteNota.str_sincursos='<?php echo JrTexto::_('You have no courses currently assigned'); ?>';
        oReporteNota.str_espere='<?php echo JrTexto::_('Wait a moment please'); ?>';
        oReporteNota.str_yourscore='<?php echo JrTexto::_('Your score'); ?>';
        oReporteNota.str_notpresented='<?php echo JrTexto::_('not presented'); ?>';
        oReporteNota.str_sincursos='<?php echo JrTexto::_('You have no courses currently assigned'); ?>';
        oReporteNota.str_espere='<?php echo JrTexto::_('Wait a moment please'); ?>';
        oReporteNota.str_yourscore='<?php echo JrTexto::_('You Score');  ?>';
        oReporteNota.str_notpresented='<?php echo JrTexto::_('Not Presented');  ?>';
        oReporteNota.str_Reportnotes='<?php echo JrTexto::_('Report of notes');  ?>';
        oReporteNota.str_There_are_no_students_enrolled_in_the_course_yet='<?php echo JrTexto::_('There are no students enrolled in the course yet');  ?>';
        oReporteNota.str_All='<?php echo JrTexto::_('All');  ?>';
        oReporteNota.str_student='<?php echo JrTexto::_('Student');  ?>';
        oReporteNota.str_Not_included_in_the_formula='<?php echo JrTexto::_('Not included in the formula'); ?>';
        oReporteNota.str_notes='<?php echo JrTexto::_('Notes');  ?>';
        oReporteNota.str_my_notes='<?php echo JrTexto::_('My Notes');  ?>';
        oReporteNota.str_finalaverage='<?php echo JrTexto::_('Final average'); ?>';
        oReporteNota.str_search='<?php echo JrTexto::_('Search');  ?>';
        oReporteNota.str_There_is_an_error_in_the_course_formula='<?php echo JrTexto::_('There is an error in the course formula');  ?>';
        oReporteNota.str_approved='<?php echo JrTexto::_('Approved');  ?>';
        oReporteNota.str_disapproved='<?php echo JrTexto::_('Disapproved');  ?>';
        oReporteNota.str_The_Alphabetic_grade_setup_in_this_course_is_not_complete='<?php echo JrTexto::_('str_The_Alphabetic_grade_setup_in_this_course_is_not_complete'); ?>';
        oReporteNota.str_first_page='<?php echo JrTexto::_('First page');  ?>';
        oReporteNota.str_previous='<?php echo JrTexto::_('Previous');  ?>';
        oReporteNota.str_next='<?php echo JrTexto::_('Next');  ?>';
        oReporteNota.str_last_page='<?php echo JrTexto::_('Last page');  ?>';
        oReporteNota.str_Showing_the_MENU_first_posts='<?php echo JrTexto::_('Showing the & nbsp _MENU_ & nbsp first posts');  ?>';
        oReporteNota.str_Showing_START_to_END_posts_out_of_a_total_of_TOTAL_='<?php echo JrTexto::_('Showing _START_ to _END_ posts out of a total of _TOTAL_');  ?>';

        google.charts.load('current', {'packages':['bar', 'corechart', 'table']});
        oReporteNota.load();
        $('.changevista').click(function(ev){
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            oReporteNota.mostrarVista()
        })
    });
</script>