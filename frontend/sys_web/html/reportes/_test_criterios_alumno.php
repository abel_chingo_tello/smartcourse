<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>

<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Habilidades"); ?>'
  });
</script>

<style type="text/css">
    .bootstrap-select > .dropdown-toggle{
        border:1px solid #0070a0;
        border-radius: 0.5ex;
    }
    .bootstrap-select > .dropdown-toggle::before {
        font-family: FontAwesome;
        font-size: 1.3em;
        content: "\f0dd";
        display: inline-block;
        text-align: center;
        width: 30px;
        height: 100%;
        background-color: #0070a0;
        position: absolute;
        top: 0;
        right: 0px;
        pointer-events: none;
        color: #FFF;
        border-radius: 0px 0.3ex 0.3ex 0px;
    }
    .popover {
        max-width:none; 
    }
    .notasdecursos td ,.notasdecursos th{
        text-align: center;        
    }
    .notafinaldecurso{
        font-size: 2em;
        border: 1px solid #3c8cbc;
        padding: 0px !important;
    }
    td.PF{
        font-weight: bold;
        font-size: 14px;
    }

</style>

<input type="hidden" name="idalumno" id="idalumno" value="<?php echo $this->idalumno; ?>">

<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <!--div class="x_title">
          <div class="row">
              <div class="col-md-6">
                  <div class="btn-group">
                      <button type="button" class="btn btn-xs btn-success btnexportar_xlsx"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_1</button>
                      <button type="button" class="btn btn-xs btn-success btnexportar_xlsx2"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_2</button>
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_1</button>
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf2"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_2</button>
                      <button type="button" class="btn btn-xs btn-warning btnimprimir"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_1</button>
                      <button type="button" class="btn btn-xs btn-warning btnimprimir_2"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_2</button>
                  </div>
              </div>
              <div class="col-md-6 text-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                      <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="area_char"><i class="fa fa-area-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar_char" ><i class="fa fa-bar-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="line_char" ><i class="fa fa-line-chart"></i></button>
                  </div>
              </div>
          </div>
        <div class="clearfix"></div>
      </div-->
      <div class="x_title">
      <div class="row">
        <div class=" col-md-12 form-group">
                <select id="idcurso" name="idcurso" class="form-control selectpicker" multiple title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                  <?php if(!empty($this->cursos))
                  foreach ($this->cursos as $fk) { ?><option selected="selected" value="<?php echo $fk["idcurso"]?>" <?php echo @$frm["idcurso"]==$fk["idcurso"]?'selected="selectted"':'';?>><?php echo $fk["strcurso"] ?></option><?php } ?>
                </select>            
            </div>           
        </div> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div clss="row" id="pnl-panelreporte" >
        <div class="col-md-12  table-responsive tableclone " style="display: none">            
            <table class="table notasdecursos table-striped table-hover">
                <thead>
                    <tr style="background: #8a9196; color: #fff;">
                        <th colspan="2" class="nomcurso text-left">Curso: <span id="strcurso">nombre del curso a Mostrar</span></th>
                        <th colspan="2" class="nomalumno text-right">Estudiante: <span id="stralumno">Abel Chingo Tello</span></th>
                    </tr>
                    <tr style="background: #babec1;" id="formula" >
                        <th>Criterios</th>                        
                    </tr>
                </thead>            
                <tbody>
                    <tr class="notasxcriterios notacriterio" style="display: none"><td class="nomcriterio text-left">nombre de criterio</td></tr>                
                </tbody>
                <tfoot>
                    <tr class="promediosfinales notacriterio"><td class="notasfinales">Notas Finales</td></tr>                    
                </tfoot>
            </table>         
        </div>   
        </div>
        <div id="sindatos" style="display:none">
            <div class="container">
            <div class="row">
                <div class="jumbotron">
                    <h1><strong>Opss!</strong></h1>                
                    <div class="disculpa"><?php echo JrTexto::_('Sorry, You do not have students enrolled')?>:</div>                
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    var __notacal=function(pt,ptmax,eq){
        if(parseInt(pt)==0) return 0;
        return parseFloat((parseFloat(pt)*parseFloat(eq))/ptmax).toFixed(2);
    }
    $(document).ready(function(ev){
   // __logoEmpresaimagebase64();
        $('select#idcurso').change(function(){
            obtnerresultado();
        })

        var obtnerresultado=function(){
            var data=new FormData();
            $('#sindatos').hide();
            //data.append('idalumno',$('#idalumno').val());
            var idcurso=$('select#idcurso').val()||'';
            var vcursos=[];
            if(idcurso.length>1){ data.append('varioscursos',true); }
            else{   vcursos=idcurso; if(vcursos==undefined) return false; }            
            data.append('idcurso',idcurso);
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/reportes/notasfinaleshabilidades',
                callback:function(rs){
                  if(rs.code==200){
                  var dt=rs.data;
                  if(rs.total>0)
                    $.each(dt,function(i,v){
                        var $tbclone=$('.tableclone').clone().removeClass('tableclone').show();
                        $tbclone.find('#strcurso').text(v.strcurso);
                        var haytareas=false;
                        var proyectos=false;
                         if(v.formula.tarea!=undefined){
                            haytareas=true;
                            $tbclone.find('tr#formula').append('<th>'+v.formula.tarea.tipo+' ('+v.formula.tarea.puntaje+'%)</th>');
                            $tbclone.find('tr.notasxcriterios').append('<td class="esnota notatarea" data-porc="'+v.formula.tarea.puntaje+'">0</td>');
                            $tbclone.find('tr.promediosfinales').append('<td class="esnota promtarea"  data-porc="'+v.formula.tarea.puntaje+'">0</td>');
                        }else  $tbclone.find('th.nomcurso').removeAttr('colspan');

                        if(v.formula.proyecto!=undefined){
                            hayproyectos=true;
                            $tbclone.find('tr#formula').append('<th>'+v.formula.proyecto.tipo+' ('+v.formula.proyecto.puntaje+'%)</th>');
                            $tbclone.find('tr.notasxcriterios').append('<td class="esnota notaproyecto"  data-porc="'+v.formula.proyecto.puntaje+'">0</td>');
                            $tbclone.find('tr.promediosfinales').append('<td class="esnota promproyecto"  data-porc="'+v.formula.proyecto.puntaje+'">0</td>');
                        }else  $tbclone.find('th.nomalumno').attr('colspan',(parseInt($tbclone.find('th.nomalumno').attr('colspan'))-1));
                       
                        $.each(v.formula.examenes,function(ie,ex){                            
                            $tbclone.find('tr#formula').append('<th>E'+ex.tipo+' ('+ex.porcentaje+'%)</th>');
                            $tbclone.find('tr.notasxcriterios').append('<td class="esnota notaexa'+ex.idexamen+'"  data-porc="'+ex.porcentaje+'" >0</td>');
                            $tbclone.find('tr.promediosfinales').append('<td class="esnota promexa'+ex.idexamen+'"  data-porc="'+ex.porcentaje+'" >0</td>');
                            $tbclone.find('th.nomalumno').attr('colspan',(parseInt($tbclone.find('th.nomalumno').attr('colspan'))+1));
                        })

                        if(v.criterios.length>0){
                            $.each(v.criterios,function(ic,vc){
                                $criterio=$tbclone.find('tr.notasxcriterios').clone().removeClass('notasxcriterios').show();
                                $criterio.attr('idcriterio',vc.idcriterio);
                                $criterio.children('td.nomcriterio').text(vc.nombre);
                                $tbclone.find('tbody').append($criterio);
                            })
                        }else $tbclone.find('tr#formula').children('th').first().text('---');   

                        $tbclone.find('tr#formula').append('<th>PF/<small>'+v.puntaje.puntajemax+'</small></th>');
                        $tbclone.find('tr.notacriterio').append('<td class="PF">0</td>');
                        //$tbclone.find('tr.promediosfinales').append('<td class="PF">0</td>');
                        _nohaycriterio=function(){                            
                            if($tbclone.find('tr.notacriterio[idcriterio="nohay"]').length>0) return;
                            else{
                                $criterio=$tbclone.find('tr.notasxcriterios').clone().removeClass('notasxcriterios').show();
                                $criterio.attr('idcriterio','nohay').css({'color':'red'});
                                $criterio.children('td.nomcriterio').html('<b>Criterio no definido</b>');
                                $tbclone.find('tbody').append($criterio);
                            }
                        }


                        if(v.estudiantes.length){
                            $.each(v.estudiantes,function(ies,es){
                                $tbclone.find('#stralumno').text(es.stralumno);
                                var nt= __notacal(es.promtareas,100,v.puntaje.puntajemax);
                                $tbclone.find('.promtarea').text(nt);
                                var nt= __notacal(es.promproyectos,100,v.puntaje.puntajemax);
                                $tbclone.find('.promproyectos').text(nt);
                                if(haytareas && es.tareas.length>0){
                                    $.each(es.sumatareasxcriterio,function(sic,scc){
                                        var nt= __notacal((scc.sum/scc.n),100,v.puntaje.puntajemax);
                                        if($tbclone.find('tr.notacriterio[idcriterio="'+sic+'"]').length)
                                            $tbclone.find('tr.notacriterio[idcriterio="'+sic+'"]').children('td.notatarea').text(nt);
                                        else{
                                            _nohaycriterio();
                                            nt=parseFloat(nt)+parseFloat($tbclone.find('tr.notacriterio[idcriterio="nohay"]').children('td.notatarea').text());
                                            $tbclone.find('tr.notacriterio[idcriterio="nohay"]').children('td.notatarea').text(nt);
                                        }
                                    })                                    
                                }
                                if(hayproyectos && es.proyectos.length>0){
                                    $.each(es.sumaproyectoxcriterio,function(sic,scc){
                                        var nt= __notacal((scc.sum/scc.n),100,v.puntaje.puntajemax);                                        
                                        if($tbclone.find('tr.notacriterio[idcriterio="'+sic+'"]').length)
                                            $tbclone.find('tr.notacriterio[idcriterio="'+sic+'"]').children('td.notaproyecto').text(nt)
                                        else{
                                            _nohaycriterio();
                                            nt=parseFloat(nt)+parseFloat($tbclone.find('tr.notacriterio[idcriterio="nohay"]').children('td.notaproyecto').text());
                                            $tbclone.find('tr.notacriterio[idcriterio="nohay"]').children('td.notaproyecto').text(nt)
                                        }
                                    })
                                }
                                if(es.examenes.length>0){
                                    $.each(es.examenes,function(sic,scc){
                                        if(scc.criterios_puntaje.length!=0){                                            
                                            $.each(scc.criterios_puntaje,function(sicc,sccc){
                                                var nt= __notacal(sccc,100,v.puntaje.puntajemax);
                                                var nt=nt/scc.criterios.length;
                                                if($tbclone.find('tr.notacriterio[idcriterio="'+sicc+'"]').length)
                                                    $tbclone.find('tr.notacriterio[idcriterio="'+sicc+'"]').children('td.notaexa'+scc.idexamen).text(nt);
                                                else{
                                                    _nohaycriterio();
                                                    nt=parseFloat(nt)+parseFloat($tbclone.find('tr.notacriterio[idcriterio="nohay"]').children('td.notaexa'+scc.idexamen).text());
                                                    $tbclone.find('tr.notacriterio[idcriterio="nohay"]').children('td.notaexa'+scc.idexamen).text(nt);
                                                }
                                            })
                                            var nt=__notacal(scc.nota,scc.nota_enbasea,v.puntaje.puntajemax); 
                                            console.log(scc,scc.nota,scc.nota_enbasea,v.puntaje.puntajemax);
                                            $tbclone.find('tr.promediosfinales').children('td.promexa'+scc.idexamen).text(nt);
                                        }else{ // solo poner la nota del examen xque no tiene criterios
                                            var nt=__notacal(scc.nota,scc.nota_enbasea,v.puntaje.puntajemax);
                                            $tbclone.find('tr.promediosfinales').children('td.promexa'+scc.idexamen).text(nt);
                                        }
                                    })
                                }
                            })
                        }


                        $tbclone.find('tr.notacriterio').each(function(inc,vnc){
                            var nt=0;
                            $(vnc).find('td.esnota').each(function(inn,vnn){
                                nt=nt+ (parseFloat($(vnn).text())*parseInt($(vnn).attr('data-porc')||100)/100);
                            })
                            $(vnc).children('td.PF').text(nt.toFixed(2));
                        })
                        $('#pnl-panelreporte').append($tbclone);
                        //console.log(v);
                    })
                  }else{
                    $("#sindatos").show();
                  }
                }
            });
        }
        obtnerresultado();
    })
</script>
<!--script type="text/javascript">
    $(document).ready(function(){
        __logoEmpresaimagebase64();
        google.charts.load('current', {'packages':['corechart']});
        function charst(tipo,dt,options,idelem){
            var selbase=localStorage.getItem('notabase')||'';
            var bases=[];
            var strcurso=[];
            var data = new google.visualization.DataTable();
            data.addColumn('string', '<?php echo ucfirst(JrTexto::_('Courses')); ?>');
            data.addColumn('number', '<?php  echo ucfirst(JrTexto::_('Beginning test')); ?>');
            var datos=[];        
            if(dt.length)
                $.each(dt,function(i,c){
                    var puntajemax=c.puntaje.puntajemax||100;
                    if($.inArray(puntajemax,bases)==-1){
                        bases.push(puntajemax);
                        selbase=(selbase==''||selbase=='null'||selbase==null)?puntajemax:selbase;
                    } 
                    if($.inArray(selbase,bases)==-1){selbase=bases[0]||100;}           
                    var notaentrada=parseFloat(c.notaentrada).toFixed(2);
                    datos.push({idcurso:c.idcurso,nombrecurso:c.strcurso,nota:notaentrada,notaentrada_enbasea:c.notaentrada_enbasea});
                    
                }) 
            bases.sort((a,b)=>a-b);

            localStorage.setItem('notabase',selbase);               
            if(bases.length)
                $.each(bases,function(i,v){
                    $('#notabase').append('<option values="'+v+'" '+(v==selbase?'selected="selected"':'')+'>'+v+'</option>');
                });

            if(datos.length){
                $.each(datos,function(i,v){
                    var prom=parseFloat(parseFloat(v.nota)*parseFloat(selbase)/parseFloat(v.notaentrada_enbasea)); 
                    data.addRow([v.nombrecurso,prom]);
                })
                options.title=options.title+' /'+selbase;
                //console.log(datos);
                if(tipo=='area') var chart = new google.visualization.AreaChart(document.getElementById(idelem));
                else if(tipo=='line')var chart = new google.visualization.LineChart(document.getElementById(idelem));
                else if(tipo=='pie')var chart = new google.visualization.PieChart(document.getElementById(idelem));
                else var chart = new google.visualization.BarChart(document.getElementById(idelem));
                chart.draw(data, options);       
            }
        }

        var verrptalumno=1;
        localStorage.setItem('rtpalumno','ver_rpt'+verrptalumno);
        var idnumgrafico=0;

        var vistaimpresion=function(){
            var selbase=localStorage.getItem('notabase')||'';                
                var dataexamenes=localStorage.getItem('dataexamenes');
                var dt=JSON.parse(dataexamenes);
                var datos=[];
                var bases=[];
                var nmaxformula=0;
                if(dt.length)
                    $.each(dt,function(i,c){
                        var puntajemax=c.puntaje.puntajemax||100;
                        if($.inArray(puntajemax,bases)==-1){
                            bases.push(puntajemax);
                            selbase=(selbase==''||selbase=='null'||selbase==null)?puntajemax:selbase;
                        } 
                        if($.inArray(selbase,bases)==-1){selbase=bases[0]||100;}
                        var notaentrada=parseFloat(c.notaentrada).toFixed(2);
                       // console.log(c); 
                        datos.push({idcurso:c.idcurso,nombrecurso:c.strcurso,nota:notaentrada,notaentrada_enbasea:c.notaentrada_enbasea});                        
                    }) 
                //if(nmaxformula<2) nmaxformula=3;
                var total=0;
                var tr='';
                var tr1='<thead><tr style="color:#ffffff; background-color:#5c5858;"><th>#</th><th><?php echo JrTexto::_('Course') ?></th><th><?php echo JrTexto::_('Beginning Test'); ?></th><th><?php echo JrTexto::_('Beginning Test'); ?>/('+selbase+')</th></tr></thead><tbody>'; 
                if(datos.length){
                    var tmpdata=[];
                    $.each(datos,function(i,v){
                        var prom=parseFloat(parseFloat(v.nota)*parseFloat(selbase)/v.notaentrada_enbasea);
                        tr+='<tr><td class="text-center">'+(i+1)+'</td><td >'+v.nombrecurso+'</td><td>'+prom.toFixed(2)+'</td></tr>';
                        total++;
                        tr1+='<tr ><td class="text-center">'+(i+1)+'</td><td> '+v.nombrecurso+'</td><td class="text-center">'+parseFloat(v.nota).toFixed(2)+'/('+v.notaentrada_enbasea+')</td><td class="text-center">'+prom.toFixed(2)+'</td></tr>';
                       
                    }) 
                }
                tr1='<table class="table-striped" nmaxformula="'+nmaxformula+'" id="tableimprimir" style="width: 100%">'+tr1+'</tbody></table>';

                tr+='<tr><td></td><td colspan="2" class="text-center"><?php echo JrTexto::_("Total of records"); ?> : '+total+'</td><td style="display:none;"></td></tr>';
                tr='<table class="table-striped" id="tableimprimir2" style="width: 100%"><thead><tr><th>#</th><th><?php echo JrTexto::_('Courses') ?></th><th><?php echo JrTexto::_('Beginning Test'); ?>/('+selbase+')</th></tr></thead><tbody>'+tr+'</tbody></table>';
                $('#impresion2 .table-responsive').html(tr);
                $('#impresion2').find('#tableimprimir2').DataTable({
                        ordering: false,
                        searching: false,
                        paging: false,
                        dom: 'Bfrtip',
                        buttons: [
                          {
                            extend: "excelHtml5",
                            messageTop: '',
                            title:'<?php echo JrTexto::_('Report') ?>',
                            customize: function(xlsx){
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                var col=['A','B','C','D','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                                $tr=$('#tableimprimir2 tbody').find('tr');
                                $.each($tr,function(ir, row){
                                    $(this).find('td').each(function(ic,td){
                                        var xcol=col[ic];
                                        if(ir==0) $('row c[r="'+xcol+'1"]',sheet).attr('s',32);
                                        else{
                                            var s=ir%2==0?8:20;
                                            $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',s);
                                        }   
                                    })
                                })
                            }
                          },{
                            extend: "pdfHtml5",
                            messageTop: '',
                            title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Beginning Test')." - ".date('d-m-Y'); ?>',
                            customize: function(doc){
                                doc.pageMargins = [12,12,12,12];
                                doc.content[1].table.widths = [30, "*", "*"];
                                var tblBody = doc.content[1].table.body;
                                $tr=$('#tableimprimir2 tbody').find('tr');
                                var ntr=$tr.length;
                                $tr.each(function (ir, row){
                                    var color=((ir%2==0)?'':'#e4e1e1');
                                      $(this).find('td').each(function(ic,td){
                                        if(ir==0) color='#7D94D0';                                        
                                        tblBody[ir][ic].fillColor=color;                                      
                                        if(ic==0)tblBody[ir][ic].alignment= 'center';
                                    })
                                })
                                tblBody[ntr][0].alignment= 'center';
                                if(_logobase64!=''){
                                    doc.content.splice(0, 0, {
                                        margin: [ 0, 0, 0, 12 ],
                                        alignment: 'center',
                                        image:_logobase64
                                    });
                                }
                            }
                          },{
                            extend:"print",
                            title: '<?php echo JrTexto::_('Report')." ".JrTexto::_('Beginning Test')." - ".date('d-m-Y'); ?>',
                            customize: function(win){
                                var body=$(win.document.body);
                                    var title=body.find('h1').first().text();
                                    if(_logobase64!=''){
                                        body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="'+_logobase64+'"/><br>'+title+'</div>');
                                    }else{
                                         body.find('h1').first().html('<div style="text-align:center; width:100%;">'+title+'</div>');
                                    }
                                    table=body.find('table');
                                    table.find('th').each(function(index){
                                       table.find('th').css('background-color', '#cccccc')
                                    });
                            }
                          }
                        ]                  
                })

                $('#impresion .table-responsive').html(tr1);
                $('#impresion').find('#tableimprimir').DataTable({
                        ordering: false,                        
                        searching: false,
                        paging: false,
                        dom: 'Bfrtip',
                        buttons: [
                          {
                            extend: "excelHtml5",
                            messageTop: '',
                            title:'<?php echo JrTexto::_('Report') ?>',
                            customize: function(xlsx){
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                var col=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                                $tr=$('#tableimprimir tbody').find('tr');
                                $.each($tr,function(ir, row){
                                    $(this).find('td').each(function(ic,td){
                                        var xcol=col[ic];
                                        if(ir==0) $('row c[r="'+xcol+'1"]',sheet).attr('s',32);
                                        else{
                                            var s=ir%2==0?8:20;
                                            $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',s);
                                        }   
                                    })
                                })
                            }
                          },{
                            extend: "pdfHtml5",
                            messageTop: '',
                            title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Beginning Test')." - ".date('d-m-Y'); ?>',
                            customize: function(doc){
                                doc.pageMargins = [12,12,12,12];
                                $tr=$('#tableimprimir tbody').find('tr');
                                var ntr=$tr.length;
                                var ntd=$tr.eq(0).find('th').length;
                                var ncolA=[30,"*","*","*"];
                                /*for(i=0;i<ntd-2;i++){
                                    ncolA.push("*");
                                }*/
                                doc.content[1].table.widths = ncolA;
                                var tblBody = doc.content[1].table.body;
                                $tr.each(function (ir, row){
                                    var color=((ir%2==0)?'':'#e4e1e1');
                                    if($(this).hasClass('nomcursos')){
                                        $(this).find('td').each(function(ic,td){
                                            tblBody[ir][ic].fillColor='#5c5858';
                                            tblBody[ir][ic].color="#FFFFFF";  
                                        })
                                    }
                                })                                
                                tblBody[ntr][0].alignment= 'left'; 
                                if(_logobase64!=''){
                                    doc.content.splice(0, 0, {
                                        margin: [ 0, 0, 0, 12 ],
                                        alignment: 'center',
                                        image:_logobase64
                                    });
                                }
                            }

                          },{
                            extend:"print",
                            title: '<?php echo JrTexto::_('Report')." ".JrTexto::_('Beginning Test')." - ".date('d-m-Y'); ?>',
                            customize: function(win){
                                var body=$(win.document.body);
                                    var title=body.find('h1').first().text();
                                    if(_logobase64!=''){
                                        body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="'+_logobase64+'"/><br>'+title+'</div>');
                                    }else{
                                         body.find('h1').first().html('<div style="text-align:center; width:100%;">'+title+'</div>');
                                    }
                                    table=body.find('table');
                                    table.find('th').each(function(index){
                                       table.find('th').css('background-color', '#cccccc')
                                    });
                            }
                          }
                        ]                  
                })                

        }
        var mostrarresultado=function(){
            var dataexamenes=localStorage.getItem('dataexamenes');
            var vistareporte=localStorage.getItem('vistaReporte1')||$('.changevista.active').attr('vista');
            var selbase=localStorage.getItem('notabase')||'';
            var dt=JSON.parse(dataexamenes);
            console.log(dt);
            if(vistareporte=='table'){
               var table='<div class="container"> <div class="col-md-12 table-reponsive"><table class="table table-striped table-hover"><thead><tr class="headings"><th>#</th>';
                  table+='<th><?php echo JrTexto::_("Course") ;?></th><th class="text-center"><?php echo JrTexto::_("Beginning test") ;?>/<select id="changepuntaje"></select></th></tr></thead><tbody>';

                  var bases=[];
                  if(dt.length)
                    $.each(dt,function(i,c){                      
                        table+='<tr><td  style="padding:0.5ex 2ex">'+(i+1)+'</td><td style="padding:0.5ex 1ex">'+c.strcurso+'</td>';
                        var notaentrada=c.notaentrada;
                        var formula=c.formula;
                        var table2='<div class="col-md-12 table-reponsive table-sm" style="display:none"><table class="table" style="margin:0px" ><thead >';
                            table2_1='';
                            table2_2='';
                            var iex=1;
                            var tex=formula.length;
                            var prom=0;
                            var de='';
                            var nomexa='<?php echo JrTexto::_('Beginning Test'); ?>';
                            var puntajemax=c.puntaje.puntajemax||100;
                            if($.inArray(puntajemax,bases)==-1){
                                bases.push(puntajemax);
                                selbase=(selbase==''||selbase=='null'||selbase==null)?puntajemax:selbase;
                            }
                            if($.inArray(selbase,bases)==-1){selbase=bases[0]||100;}
                           
                            table2_1+='<th><?php echo JrTexto::_("Beginning test") ;?> ('+c.notaentrada_enbasea+')</th>';
                            table2_2+='<td style="padding:0.5ex" >'+notaentrada+'</td>';
                            //if(notaentrada>0) console.log(selbase,notaentrada,c.notaentrada_enbasea);
                            //if(notaentrada>0) notaentrada=parseFloat(selbase * notaentrada)/c.notaentrada_enbasea;
                            //if(notaentrada>0) console.log(selbase,notaentrada,c.notaentrada_enbasea);
                            table2+=table2_1+'</thead><tbody>'+table2_2+'</tbody></table></div>';
                        table+='<td style="padding:0.5ex 1ex" class="shownota text-center" rel="popover"  de="'+c.notaentrada_enbasea+'" nota="'+parseFloat(notaentrada).toFixed(2)+'"><h3 style="margin:0px; font-size: 1.3rem;"></h3>'+table2+'</td>';    
                        table+='</tr>';/**/
                    })
                table+='</tbody>';
                table+='</table></div></div>';
                $('#pnl-panelreporte').html(table);
                $('[rel="popover"]').click(function(ev){return false}).popover({
                    container: 'body',
                    placement:'top',
                    trigger: 'hover',
                    html: true,
                    content: function () {
                        var clone = $($(this).find('table').clone(true)).show();
                        return clone;
                    }
                })
                $('#pnl-panelreporte').find("#changepuntaje").children().remove();                
                bases.sort((a,b)=>a-b);
                //console.log(bases);
                $.each(bases,function(i,v){
                    $('#pnl-panelreporte').find("#changepuntaje").append('<option values="'+v+'" '+(v==selbase?'selected="selected"':'')+'>'+v+'</option>');
                })
                $('#pnl-panelreporte').find("#changepuntaje").val(selbase).trigger('change');
                $('#pnl-panelreporte table').DataTable(
                    {
                        ordering: false,                        
                            searching: false,
                            pageLength: 50,
                            info:false,
                            oLanguage: {
                                "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
                                "oPaginate": {
                                    "sFirst": "<?php echo JrTexto::_("Next page") ?>", // This is the link to the first page
                                    "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                                    "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                                    "sLast": "<?php echo JrTexto::_("Previous page") ?>" // This is the link to the last page
                                }
                            }
                    }
                );
            }else if(vistareporte=='area_char'){                 
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('area',dt,options,divid));
            }else if(vistareporte=='bar_char'){               
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        orientation:'horizontal',
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('bar',dt,options,divid));
            }else if(vistareporte=='line_char'){                 
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('line',dt,options,divid));      
            }else if(vistareporte=='pie_char'){                
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('pie',dt,options,divid)); 
            }else{
                table='';
                $('#pnl-panelreporte').html(table);
            }        
            vistaimpresion();
            //console.log(vistareporte,dt);
        }

        var obtnerresultado=function(){
            var rtpalumno=localStorage.getItem('rtpalumno');
            if(rtpalumno=='ver_rpt'+verrptalumno) return mostrarresultado();
            localStorage.setItem('rtpalumno','ver_rpt'+verrptalumno);
            var data=new FormData()
            data.append('idalumno',$('#idalumno').val());
            var idcurso=$('select#idcurso').val()||'';
            var vcursos=[];
            if(idcurso.length>1){         
              data.append('varioscursos',true);
            }else{                
                vcursos=idcurso; 
                if(vcursos==undefined) return false;
            }            
            data.append('idcurso',idcurso);
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/reportes/notasfinales',
                callback:function(rs){
                  if(rs.code==200){
                    localStorage.setItem('dataexamenes',JSON.stringify(rs.data));
                    mostrarresultado();
                  }
                }
            });
        }

        $('.changevista').click(function(ev){
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            localStorage.setItem('vistaReporte1',$(this).attr('vista'));
             mostrarresultado();
        })
        $('#pnl-panelreporte').on('change','table select#changepuntaje',function(ev){
            ev.preventDefault();
            _this=$(this);
            var val=_this.val();
            var td=$('#pnl-panelreporte table td.shownota');
            $.each(td,function(vv){
                var c=parseFloat(parseFloat($(this).attr('nota'))*parseFloat(val)/parseFloat($(this).attr('de'))).toFixed(2);
                $(this).children('h3').text(c);
            })
            localStorage.setItem('notabase',val);
            vistaimpresion();
            //ev.stopPropagation();
        }).on('change','#notabase',function(ev){
            //ev.preventDefault();
            localStorage.setItem('notabase',$(this).val());
            mostrarresultado();
            //ev.stopPropagation();
            //console.log('test');
        })

        $('select#idcurso').change(function(ev){
            verrptalumno++;
        })

        $('select#idcurso').on('hide.bs.select', function (e, clickedIndex, isSelected, previousValue){
          obtnerresultado();
        });

        $('.btnexportar_xlsx').click(function(ev){
            setTimeout(function(){$('#impresion2').find('.buttons-excel').trigger('click');},1500); 
        })
        $('.btnexportar_xlsx2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-excel').trigger('click');
        })
        $('.btnexportar_pdf').click(function(ev){
            var vistareporte=localStorage.getItem('vistaReporte1')||$('.changevista.active').attr('vista');           
            if(vistareporte=='table'){                
                setTimeout(function(){$('#impresion2').find('.buttons-pdf').trigger('click');},1500);               
            }else {
                const filename  = 'report_'+vistareporte+'.pdf';
                $('#pnl-panelreporte').children('.col-md-3').hide();
                html2canvas(document.querySelector('#pnl-panelreporte'),{scale: 1}).then(canvas => {
                    let pdf = new jsPDF({orientation: 'landscape'});//
                    pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 320, 180);
                    pdf.save(filename);
                    $('#pnl-panelreporte').children('.col-md-3').show();
                });
            }
        });
        $('.btnexportar_pdf2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-pdf').trigger('click');
        });
        $('.btnimprimir').click(function(ev){
             if(vistareporte=='table'){ 
            setTimeout(function(){$('#impresion2').find('.buttons-print').trigger('click');},1500);
             }else {
                $('#pnl-panelreporte').children('.col-md-3').hide(0).css('display','none');
                $('#pnl-panelreporte').imprimir();
                $('#pnl-panelreporte').children('.col-md-3').show(0).css('display','inline-block');
            }
        });
         $('.btnimprimir_2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-print').trigger('click');            
        });
        $('#tableimprimir').DataTable({
            ordering: false,
            searching: false,
            paging: false,
            dom: 'Bfrtip',
            buttons: [
              {
                extend: "pdfHtml5",
                messageTop: '',
                title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Course notes')." - ".date('d-m-Y'); ?>',
                customize: function(doc){
                    doc.pageMargins = [12,12,12,12];
                    doc.content[1].table.widths = ["*", "*", "*"];
                    var tblBody = doc.content[1].table.body;
                    $('#tableimprimir tbody').find('tr').each(function (ir, row){
                        if($(this).hasClass('nomexamen')){
                            $(this).find('td').each(function(ic,td){
                                tblBody[ir][ic].fillColor='#e4e1e1';
                            })
                        }
                        //var rowElt = row;
                        /*doc.content[1].layout = {
                        hLineWidth: function(i, node) {
                            return (i === 0 || i === node.table.body.length) ? 2 : 1;},
                        vLineWidth: function(i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 2 : 1;},
                        hLineColor: function(i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';},
                        vLineColor: function(i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';}
                        };*/ 
                    })                 
                }
              },{
                extend: "excelHtml5",
                messageTop: '',
                title:'<?php echo JrTexto::_('Report') ?>',
                customize: function(xlsx){
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var col=['A','B','C','D','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                    $('#tableimprimir tbody').find('tr').each(function (ir, row){
                        if($(this).hasClass('nomexamen')){
                            $(this).find('td').each(function(ic,td){
                                var xcol=col[ic];
                                $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',32);
                                //tblBody[ir][ic].fillColor='#17a2b8';
                            })
                        }
                    })
                   // console.log(sheet);
                }
              }
            ]
        });
        $('#pnl-panelreporte .dt-buttons').hide();
        $('#impresion .dt-buttons').hide();
        //$('#impresion').hide();
        verrptalumno++;
        vistareporte=localStorage.getItem('vistaReporte1')||'table';
        obtnerresultado();
        setTimeout(function(){$('.changevista[vista="'+vistareporte+'"]').trigger('click')},2000);
    })


</script-->
