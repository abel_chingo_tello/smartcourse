<?php

?>
<style>
.row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

select.select-ctrl{ padding-right: 35px; }
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.box_custom{ background-color:white; border:1px solid #4683af; border-radius:0.5em; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<div class="row" id="search_container">
    <div class="col-md-12">
        <div class="col-md-6">
            <h4 style="text-align:center; font-weight:bold;"><?php echo JrTexto::_("Notas por bimestre"); ?></h5>
            <h5 style="text-align:center; font-weight:bold;"><?php echo JrTexto::_("Puntaje promedio"); ?></h5>
            <div class="table-responsive box_custom" style="padding: 5px 0;">
                <table class="table" id="tabla01">
                    <thead style="background-color:#d3e5fb;">
                        <th><?php echo $this->Entidad;?></th>
                        <th><?php echo "Bimestre 1"; ?></th>
                        <th><?php echo "Bimestre 2"; ?></th>
                        <th><?php echo "Bimestre 3"; ?></th>
                        <th><?php echo "Bimestre 4"; ?></th>
                    </thead>
                    <tbody>
                        <?php
                            if(!empty($this->RowTabla) && !empty($this->RowTabla['bimestre'])){
                                foreach($this->RowTabla['bimestre'] as $k => $r){
                                    echo "<tr>";
                                    echo "<td>{$r['nombre']}</td>";
                                    foreach($r['promedios'] as $value){
                                        echo "<td>{$value} pts</td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-6">
            <h4 style="text-align:center; font-weight:bold;"><?php echo JrTexto::_("Notas por trimestre"); ?></h5>
            <h5 style="text-align:center; font-weight:bold;"><?php echo JrTexto::_("Puntaje promedio"); ?></h5>
            <div class="table-responsive box_custom" style="padding: 5px 0;">
                <table class="table" id="tabla02">
                    <thead style="background-color:#d3e5fb;">
                        <th><?php echo $this->Entidad;?></th>
                        <th><?php echo "Trimestre 1"; ?></th>
                        <th><?php echo "Trimestre 2"; ?></th>
                        <th><?php echo "Trimestre 3"; ?></th>
                    </thead>
                    <tbody>
                        <?php
                            if(!empty($this->RowTabla) && !empty($this->RowTabla['trimestre'])){
                                foreach($this->RowTabla['trimestre'] as $k => $r){
                                    echo "<tr>";
                                    echo "<td>{$r['nombre']}</td>";
                                    foreach($r['promedios'] as $value){
                                        echo "<td>{$value} pts</td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
    
    <div class="col-md-6">
        <h5 style="text-align:center; font-weight:bold;">Habilidades</h5>
        <div style="text-align:center;">
            <h4 style="display:inline-block;"><?php echo $this->Entidad ?></h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select class="select-docente form-control select-ctrl " id="item1">
                    <option value="0">Seleccionar</option>
                    <?php 
                        if(!empty($this->RowTabla['bimestre'])){
                            foreach($this->RowTabla['bimestre'] as $k => $v){
                                echo "<option value='".($k+1)."'>{$v['nombre']}</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="chart-container" id="Container_Bimestre_habilidad" style="position: relative; margin:0 auto; height:100%; width:100%">
            <canvas id="chartBimestre_habilidad"></canvas>
        </div>
        <div class="table-responsive box_custom" style="padding: 5px 0;">
            <table class="table" id="tableresumen_skill_01">
                <thead>
                    <th>Periodo</th>
                    <th>Listening</th>
                    <th>Reading</th>
                    <th>Writing</th>
                    <th>Speaking</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <h5 style="text-align:center; font-weight:bold;">Habilidades</h5>
        <div style="text-align:center;">
            <h4 style="display:inline-block;"><?php echo $this->Entidad ?></h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select class="select-docente form-control select-ctrl " id="item2">
                    <option value="0">Seleccionar</option>
                    <?php 
                        if(!empty($this->RowTabla['trimestre'])){
                            foreach($this->RowTabla['trimestre'] as $k => $v){
                                echo "<option value='".($k+1)."'>{$v['nombre']}</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="chart-container" id="Container_Trimestre_habilidad" style="position: relative; margin:0 auto; height:100%; width:100%">
            <canvas id="chartTrimestre_habilidad"></canvas>
        </div> 
        <div class="table-responsive box_custom" style="padding: 5px 0;">
            <table class="table" id="tableresumen_skill_02">
                <thead>
                    <th>Periodo</th>
                    <th>Listening</th>
                    <th>Reading</th>
                    <th>Writing</th>
                    <th>Speaking</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">

var datos = <?php echo !empty($this->RowTabla) ? json_encode($this->RowTabla) : 'null' ?>;
var drawChart = function(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: false,
        text: 'Empty'
      },
      scales:{"yAxes":[{"ticks":{"beginAtZero":true}}]},
      tooltips: {
        mode: 'point',
        intersect: true
      },legend: {
            display: true
         }
    }
  });
};

$('document').ready(function(){
    $('#search_container').parent().css('margin','5px 0');
    $('#search_container').parent().css('width','100%');
    $('#tabla01').DataTable({
        "language" : {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    });
    $('#tabla02').DataTable({
        "language" : {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    });
    var chartData = {
        labels: ['Listening','Reading','Writing','Speaking'],
        datasets: [ {
            label: 'Bimestre 1',
            backgroundColor: '#36a2eb',
            borderColor: 'white',
            borderWidth: 2,
            data: [
                0,0,0,0
            ]
        },{
            label: 'Bimestre 2',
            backgroundColor: '#e91e63',
            borderColor: 'white',
            borderWidth: 2,
            data: [
                0,0,0,0
            ]
        },{
            label: 'Bimestre 3',
            backgroundColor: '#ffc107',
            borderColor: 'white',
            borderWidth: 2,
            data: [
                0,0,0,0
            ]
        },{
            label: 'Bimestre 4',
            backgroundColor: '#9c27b0',
            borderColor: 'white',
            borderWidth: 2,
            data: [
                0,0,0,0
            ]
        }]

    };
    drawChart('chartBimestre_habilidad',chartData);
    var chartData = {
        labels: ['Listening','Reading','Writing','Speaking'],
        datasets: [ {
            label: 'Trimestre 1',
            backgroundColor: '#36a2eb',
            borderColor: 'white',
            borderWidth: 2,
            data: [
                0,0,0,0
            ]
        },{
            label: 'Trimestre 2',
            backgroundColor: '#e91e63',
            borderColor: 'white',
            borderWidth: 2,
            data: [
                0,0,0,0
            ]
        },{
            label: 'Trimestre 3',
            backgroundColor: '#ffc107',
            borderColor: 'white',
            borderWidth: 2,
            data: [
                0,0,0,0
            ]
        }]

    };
    drawChart('chartTrimestre_habilidad',chartData);

    /** EVENTOS CHARTS */
    $('#item1').on('change',function(){
        var val = $(this).val();
        if(val != 0){
            var chartData = {
                labels: ['Listening','Reading','Writing','Speaking'],
                datasets: [ {
                    label: 'Bimestre 1',
                    backgroundColor: '#36a2eb',
                    borderColor: 'white',
                    borderWidth: 2,
                    data: [
                        datos.bimestre[val - 1].habilidades.b1['4'],datos.bimestre[val - 1].habilidades.b1['5'],datos.bimestre[val - 1].habilidades.b1['6'],datos.bimestre[val - 1].habilidades.b1['7']
                    ]
                },{
                    label: 'Bimestre 2',
                    backgroundColor: '#e91e63',
                    borderColor: 'white',
                    borderWidth: 2,
                    data: [
                        datos.bimestre[val - 1].habilidades.b2['4'],datos.bimestre[val - 1].habilidades.b2['5'],datos.bimestre[val - 1].habilidades.b2['6'],datos.bimestre[val - 1].habilidades.b2['7']
                    ]
                },{
                    label: 'Bimestre 3',
                    backgroundColor: '#ffc107',
                    borderColor: 'white',
                    borderWidth: 2,
                    data: [
                        datos.bimestre[val - 1].habilidades.b3['4'],datos.bimestre[val - 1].habilidades.b3['5'],datos.bimestre[val - 1].habilidades.b3['6'],datos.bimestre[val - 1].habilidades.b3['7']
                    ]
                },{
                    label: 'Bimestre 4',
                    backgroundColor: '#9c27b0',
                    borderColor: 'white',
                    borderWidth: 2,
                    data: [
                        datos.bimestre[val - 1].habilidades.b4['4'],datos.bimestre[val - 1].habilidades.b4['5'],datos.bimestre[val - 1].habilidades.b4['6'],datos.bimestre[val - 1].habilidades.b4['7']
                    ]
                }]

            };
            var tr = '';
            for(var i = 1; i <= 4;i++){
                tr = tr.concat('<tr><td>Bimestre '+i.toString()+'</td><td>'+datos.bimestre[val - 1].habilidades['b'+i.toString()]['4']+'</td><td>'+datos.bimestre[val - 1].habilidades['b'+i.toString()]['5']+'</td><td>'+datos.bimestre[val - 1].habilidades['b'+i.toString()]['6']+'</td><td>'+datos.bimestre[val - 1].habilidades['b'+i.toString()]['7']+'</td></tr>');
            }
            $('#tableresumen_skill_01').find('tbody').html(' ').html(tr);
            $("#Container_Bimestre_habilidad").html(" ").html('<canvas id="chartBimestre_habilidad"></canvas>');
            drawChart('chartBimestre_habilidad',chartData);
        }

    });
    $('#item2').on('change',function(){
        console.log(datos);
        var val = $(this).val();
        if(val != 0){
            var chartData = {
                labels: ['Listening','Reading','Writing','Speaking'],
                datasets: [ {
                    label: 'Trimestre 1',
                    backgroundColor: '#36a2eb',
                    borderColor: 'white',
                    borderWidth: 2,
                    data: [
                        datos.trimestre[val - 1].habilidades.t1['4'],datos.trimestre[val - 1].habilidades.t1['5'],datos.trimestre[val - 1].habilidades.t1['6'],datos.trimestre[val - 1].habilidades.t1['7']
                    ]
                },{
                    label: 'Trimestre 2',
                    backgroundColor: '#e91e63',
                    borderColor: 'white',
                    borderWidth: 2,
                    data: [
                        datos.trimestre[val - 1].habilidades.t2['4'],datos.trimestre[val - 1].habilidades.t2['5'],datos.trimestre[val - 1].habilidades.t2['6'],datos.trimestre[val - 1].habilidades.t2['7']
                    ]
                },{
                    label: 'Trimestre 3',
                    backgroundColor: '#ffc107',
                    borderColor: 'white',
                    borderWidth: 2,
                    data: [
                        datos.trimestre[val - 1].habilidades.t3['4'],datos.trimestre[val - 1].habilidades.t3['5'],datos.trimestre[val - 1].habilidades.t3['6'],datos.trimestre[val - 1].habilidades.t3['7']
                    ]
                }]

            };
            var tr = '';
            for(var i = 1; i <= 3;i++){
                tr = tr.concat('<tr><td>Trimestre '+i.toString()+'</td><td>'+datos.trimestre[val - 1].habilidades['t'+i.toString()]['4']+'</td><td>'+datos.trimestre[val - 1].habilidades['t'+i.toString()]['5']+'</td><td>'+datos.trimestre[val - 1].habilidades['t'+i.toString()]['6']+'</td><td>'+datos.trimestre[val - 1].habilidades['t'+i.toString()]['7']+'</td></tr>');
            }
            // tr = tr.concat('<tr><td>Trimestre 1</td><td>'+datos.trimestre[val - 1].habilidades.t1['4']+'</td><td>'+datos.trimestre[val - 1].habilidades.t1['5']+'</td><td>'+datos.trimestre[val - 1].habilidades.t1['6']+'</td><td>'+datos.trimestre[val - 1].habilidades.t1['7']+'</td></tr>');
            // tr = tr.concat('<tr><td>Trimestre 2</td><td>'+datos.trimestre[val - 1].habilidades.t2['4']+'</td><td>'+datos.trimestre[val - 1].habilidades.t2['5']+'</td><td>'+datos.trimestre[val - 1].habilidades.t2['6']+'</td><td>'+datos.trimestre[val - 1].habilidades.t2['7']+'</td></tr>');
            // tr = tr.concat('<tr><td>Trimestre 3</td><td>'+datos.trimestre[val - 1].habilidades.t3['4']+'</td><td>'+datos.trimestre[val - 1].habilidades.t3['5']+'</td><td>'+datos.trimestre[val - 1].habilidades.t3['6']+'</td><td>'+datos.trimestre[val - 1].habilidades.t3['7']+'</td></tr>');
            $('#tableresumen_skill_02').find('tbody').html(' ').html(tr);
            $("#Container_Trimestre_habilidad").html(" ").html('<canvas id="chartTrimestre_habilidad"></canvas>');
            drawChart('chartTrimestre_habilidad',chartData);
        }

    });
});
</script>