<?php

?>
<style>
.row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

select.select-ctrl{ padding-right: 35px; }
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.box_custom{ background-color:white; border:1px solid #4683af; border-radius:0.5em; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<div class="row" id="search_container">
    <div class="col-md-6 section-u-dre">
        <div class="box_custom">
            <h4 style="text-align:center; font-weight:bold;">Examen de entrada</h5>
            <h5 style="text-align:center; font-weight:bold;">Puntaje promedio</h5>

            <div class="chart-container" id="Container_Entrada_habilidad01" style="position: relative; margin:0 auto; height:100%; width:100%">
                <canvas id="chartEntrada_habilidad01"></canvas>
            </div>
            <div style="text-align:center;">
                <button type="button" id="previousChart" class="btn btn-default btn-circle"><i class="fa fa-angle-left"></i></button>
                <div class="panel panel-default" style="display:inline-block;">
                    <div class="panel-body"><span id="pag-text">1 / 1</span></div>
                </div>
                <button type="button" id="nextChart" class="btn btn-default btn-circle"><i class="fa fa-angle-right"></i></button>
            </div>
        </div>
    </div>
    <div class="col-md-6 section-u-dre">
        <div class="box_custom">
            <h4 style="text-align:center; font-weight:bold;">Examen de salida</h5>
            <h5 style="text-align:center; font-weight:bold;">Puntaje promedio</h5>

            <div class="chart-container" id="Container_Entrada_habilidad02" style="position: relative; margin:0 auto; height:100%; width:100%">
                <canvas id="chartEntrada_habilidad02"></canvas>
            </div>
            <div style="text-align:center;">
                <button type="button" id="previousChart2" class="btn btn-default btn-circle"><i class="fa fa-angle-left"></i></button>
                <div class="panel panel-default" style="display:inline-block;">
                    <div class="panel-body"><span id="pag-text2">1 / 1</span></div>
                </div>
                <button type="button" id="nextChart2" class="btn btn-default btn-circle"><i class="fa fa-angle-right"></i></button>
            </div>
        
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="table-responsive box_custom" style="padding: 5px 0;">
            <table class="table" id="tabla01">
                <thead style="background-color:#d3e5fb;">
                    <th><?php echo $this->Entidad;?></th>
                    <th>Examen de entrada</th>
                    <th>Examen de salida</th>
                </thead>
                <tbody>
                    <?php
                        if(!empty($this->RowTabla)){
                            foreach($this->RowTabla['entrada'] as $k => $r){
                                echo "<tr>";
                                echo "<td>{$r['nombre']}</td>";
                                echo "<td>{$r['promedio']} pts</td>";
                                if(isset($this->RowTabla['salida'][$k])){
                                    echo "<td>".$this->RowTabla['salida'][$k]['promedio']." pts</td>";
                                }else{
                                    echo "<td>0.00 pts</td>";                                    
                                }
                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <h5 style="text-align:center; font-weight:bold;">Habilidades</h5>
        <div style="text-align:center;">
            <h4 style="display:inline-block;"><?php echo $this->Entidad ?></h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select class="select-docente form-control select-ctrl " id="item1">
                    <option value="0"><?php echo JrTexto::_("Seleccionar"); ?></option>
                    <?php 
                        if(!empty($this->RowTabla['entrada'])){
                            foreach($this->RowTabla['entrada'] as $k => $v){
                                echo "<option value='".($k+1)."'>{$v['nombre']}</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="chart-container" id="Container_Entrada_habilidad03" style="position: relative; margin:0 auto; height:100%; width:100%">
            <canvas id="chartEntrada_habilidad03"></canvas>
        </div>
    </div>
</div>
<script type="text/javascript">
var datos = <?php echo !empty($this->RowTabla) ? json_encode($this->RowTabla) : 'null' ?>;
var drawChart = function(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: false,
        text: 'Empty'
      },
      scales:{"yAxes":[{"ticks":{"beginAtZero":true}}]},
      tooltips: {
        mode: 'point',
        intersect: true
      },legend: {
            display: false
         }
    }
  });
};

function drawChart2(obj,dat,texto  = 'empty' ){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: false,
        text: texto
      },
      tooltips: {
        custom: function(tooltipModel) {
          if(tooltipModel.body){
            var strline = tooltipModel.body[0].lines[0];
            var search = strline.search('hide');
            if(search != -1){
              tooltipModel.width = 125;
              tooltipModel.body[0].lines[0] = strline.replace('hide','Diferencia');
            }
          }
        }
      },
      scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        },
      legend: {
        labels: {
          filter: function(item, chart) {
            // Logic to remove a particular legend item goes here
            return !item.text.includes('hide');
          }
        },
        onHover: function(event, legendItem) {
          document.getElementById(obj).style.cursor = 'pointer';
        },
        onClick: function(e,legendItem){
          var index = legendItem.datasetIndex;
          var ci = this.chart;
          // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
          var meta = ci.getDatasetMeta(index);
          // alert(index);
          if(index == 2){
            var meta2 = ci.getDatasetMeta(3);
            meta2.hidden = meta2.hidden === null? !ci.data.datasets[3].hidden : null;
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;

          }else{
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
          }
          

          ci.update();
        }
      }//end legend
    }
  });
}

function displayChart(datos,labeles,obj){
    var chartData = {
        labels: labeles,
        datasets: [ {
        type: 'bar',
        backgroundColor: ['rgba(233,30,99,0.5)','rgba(255,87,34,0.5)','rgba(33,150,243,0.5)','rgba(25,0,131,0.5)'],
        data: datos,
        borderColor: 'white',
        borderWidth: 2
        }]

    };
    $("#Container_Dominio_habilidad01").html(" ").html('<canvas id="chartDominio_habilidad01"></canvas>');
    drawChart(obj,chartData);
}
$('document').ready(function(){

    $('#search_container').parent().css('margin','5px 0');
    $('#search_container').parent().css('width','100%');
    $('#tabla01').DataTable({
        "language":{
            "url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    });

    // Check if is 5 limit of the rows
    var labeles = new Array;
    var labeles2 = new Array;
    var _d = new Array;
    var _d2 = new Array;
    var paginas = 0;
    var paginas2 = 0;
    var paginaActual = 1;
    var paginaActual2 = 1;
    var totales = Object.keys(datos.entrada).length;
    var totales2 = Object.keys(datos.salida).length;
    // var totales = 54;
    // var totales2 = 54;
    var max = 12;
    if(datos != null){
        if(totales <= 12){
            for(var i = 0; i < totales; i++){
                console.log(datos[i]);
                if(typeof datos.entrada[i].nombre != 'undefined'){
                    labeles.push(datos.entrada[i].nombre);
                    _d.push(datos.entrada[i].promedio);
                }
            }
            displayChart(_d,labeles,"chartEntrada_habilidad01");
        }else{
            //logica para paginar
            paginas = Math.ceil(totales/max);
            if(paginas > 1){
                $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.entrada.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);
                            _d.push(dat[i].promedio);
                        }
                    }
                    displayChart(_d,_l,"chartEntrada_habilidad01");
                }
                
            }//end if checker
        }

        if(totales2 <= 12){
            for(var i = 0; i < totales2; i++){
                if(typeof datos.salida[i].nombre != 'undefined'){
                    labeles2.push(datos.salida[i].nombre);
                    _d2.push(datos.salida[i].promedio);
                }
            }
            displayChart(_d2,labeles2,"chartEntrada_habilidad02");
        }else{
            //logica para paginar
            paginas2 = Math.ceil(totales2/max);
            if(paginas2 > 1){
                $('#pag-text2').text(paginaActual2.toString()+' / '+paginas2.toString());
                var cal1 = ((paginaActual2-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual2 * max) -1);
                var dat = datos.salida.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);
                            _d.push(dat[i].promedio);
                        }
                    }
                    displayChart(_d,_l,"chartEntrada_habilidad02");
                }
                
            }//end if checker
        }
    }//end if datos null

    var datosRadar = new Array();
    datosRadar[0] = 0;
    datosRadar[1] = 0;
    datosRadar[2] = 0;
    datosRadar[3] = 0;
    var datosRadar2 = new Array();
    datosRadar2[0] = 0;
    datosRadar2[1] = 0;
    datosRadar2[2] = 0;
    datosRadar2[3] = 0;
    var datosRadar3 = new Array();
    datosRadar3[0] = 0;
    datosRadar3[1] = 0;
    datosRadar3[2] = 0;
    datosRadar3[3] = 0;
    var datosRadar4 = new Array();
    datosRadar4[0] = 0;
    datosRadar4[1] = 0;
    datosRadar4[2] = 0;
    datosRadar4[3] = 0;
  //datosRadar = entrada . datosRadar2 = salida
    for(var i = 0; i < 4; i++){
        if(datosRadar[i] > datosRadar2[i]){
        datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
        }else if(datosRadar[i] < datosRadar2[i]){
        datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
        }
    }
    var barChartData = {
        labels: ["Listening", "Reading", "Writing", "Speaking"],
        datasets: [{
            label: 'Examen de entrada',
            backgroundColor: '#36a2eb',
            stack:'Stack 0',
            data: [
            datosRadar[0],datosRadar[1] ,datosRadar[2] ,datosRadar[3]
            ]
        }, {
            label: 'Examen de salida',
            backgroundColor: '#ff6384',
            stack:'Stack 1',
            data: [
            datosRadar2[0],datosRadar2[1] ,datosRadar2[2] ,datosRadar2[3]
            ]
        },{
            label: 'Diferencia',
            backgroundColor: '#d4b02f',
            stack: 'Stack 0',
            data: [
            Math.abs(datosRadar3[0]), Math.abs(datosRadar3[1]), Math.abs(datosRadar3[2]) , Math.abs(datosRadar3[3])
            ]
        },{
            label: 'hide',
            backgroundColor: '#d4b02f',
            stack: 'Stack 1',
            
            data: [
                Math.abs(datosRadar4[0]), Math.abs(datosRadar4[1]), Math.abs(datosRadar4[2]) , Math.abs(datosRadar4[3])
            ]
        }]

    };
    drawChart2("chartEntrada_habilidad03",barChartData);

    var drawComparativa = function(){
        var datosRadar = new Array();
        datosRadar[0] = 0;
        datosRadar[1] = 0;
        datosRadar[2] = 0;
        datosRadar[3] = 0;
        var datosRadar2 = new Array();
        datosRadar2[0] = 0;
        datosRadar2[1] = 0;
        datosRadar2[2] = 0;
        datosRadar2[3] = 0;
        var datosRadar3 = new Array();
        datosRadar3[0] = 0;
        datosRadar3[1] = 0;
        datosRadar3[2] = 0;
        datosRadar3[3] = 0;
        var datosRadar4 = new Array();
        datosRadar4[0] = 0;
        datosRadar4[1] = 0;
        datosRadar4[2] = 0;
        datosRadar4[3] = 0;
        if($('#item1').val() !=0){
            var valor1 = $('#item1').val();
            if(datos != null){
                for(var i = 0;i < Object.keys(datos.entrada).length; i++ ){
                    if((i + 1)  == valor1){
                        datosRadar[0] = datos.entrada[i]['4'];
                        datosRadar[1] = datos.entrada[i]['5'];
                        datosRadar[2] = datos.entrada[i]['6'];
                        datosRadar[3] = datos.entrada[i]['7'];
                        break;
                    }//end if condition
                }//end for datos
                for(var i = 0;i < Object.keys(datos.salida).length; i++ ){
                    if((i + 1)  == valor1){
                        datosRadar2[0] = datos.salida[i]['4'];
                        datosRadar2[1] = datos.salida[i]['5'];
                        datosRadar2[2] = datos.salida[i]['6'];
                        datosRadar2[3] = datos.salida[i]['7'];
                        break;
                    }//end if condition
                }//end for datos
            }//end datos
        }
        for(var i = 0; i < 4; i++){
            if(datosRadar[i] > datosRadar2[i]){
            datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
            }else if(datosRadar[i] < datosRadar2[i]){
            datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
            }
        }
        var barChartData = {
            labels: ["Listening", "Reading", "Writing", "Speaking"],
            datasets: [{
                label: 'Examen de entrada',
                backgroundColor: '#36a2eb',
                stack:'Stack 0',
                data: [
                datosRadar[0],datosRadar[1] ,datosRadar[2] ,datosRadar[3]
                ]
            }, {
                label: 'Examen de salida',
                backgroundColor: '#ff6384',
                stack:'Stack 1',
                data: [
                datosRadar2[0],datosRadar2[1] ,datosRadar2[2] ,datosRadar2[3]
                ]
            },{
                label: 'Diferencia',
                backgroundColor: '#d4b02f',
                stack: 'Stack 0',
                data: [
                Math.abs(datosRadar3[0]), Math.abs(datosRadar3[1]), Math.abs(datosRadar3[2]) , Math.abs(datosRadar3[3])
                ]
            },{
                label: 'hide',
                backgroundColor: '#d4b02f',
                stack: 'Stack 1',
                
                data: [
                    Math.abs(datosRadar4[0]), Math.abs(datosRadar4[1]), Math.abs(datosRadar4[2]) , Math.abs(datosRadar4[3])
                ]
            }]

        };
        $("#Container_Entrada_habilidad03").html(" ").html('<canvas id="chartEntrada_habilidad03"></canvas>');
        drawChart2("chartEntrada_habilidad03",barChartData);
    };

    $('#item1').change(function(){
        if($(this).val() != 0){
            drawComparativa();
            
        }
    });

    $('#previousChart').on('click',function(){
        if(paginaActual > 1 && paginaActual <= paginas){
            --paginaActual;
            $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
            if(datos != null){
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.entrada.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].dominio != 'undefined'){
                            _d.push(dat[i].promedio);
                            _l.push(dat[i].nombre);
                        }
                    }
                    displayChart(_d,_l,"chartEntrada_habilidad01");
                }
                
            }
        }
    });
    $('#nextChart').on('click',function(){
        if(paginaActual >= 1 && paginaActual < paginas){
            ++paginaActual;
            $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
            if(datos != null){
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.entrada.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].dominio != 'undefined'){
                            _d.push(dat[i].promedio);
                            _l.push(dat[i].nombre);
                        }
                    }
                    displayChart(_d,_l,"chartEntrada_habilidad01");
                }
                
            }
        }
    });

    


    //EVENTO NUMERO 2

    $('#previousChart2').on('click',function(){
        if(paginaActual2 > 1 && paginaActual2 <= paginas2){
            --paginaActual2;
            $('#pag-text2').text(paginaActual2.toString()+' / '+paginas2.toString());
            if(datos != null){
                var cal1 = ((paginaActual2-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual2 * max) -1);
                var dat = datos.salida.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].dominio != 'undefined'){
                            _d.push(dat[i].dominio);
                            _l.push(dat[i].nombre);
                        }
                    }
                    displayChart(_d,_l,"chartEntrada_habilidad02");
                }
                
            }
        }
    });
    $('#nextChart2').on('click',function(){
        if(paginaActual2 >= 1 && paginaActual2 < paginas2){
            ++paginaActual2;
            $('#pag-text2').text(paginaActual2.toString()+' / '+paginas2.toString());
            if(datos != null){
                var cal1 = ((paginaActual2-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual2 * max) -1);
                var dat = datos.salida.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].dominio != 'undefined'){
                            _d.push(dat[i].dominio);
                            _l.push(dat[i].nombre);
                        }
                    }
                    displayChart(_d,_l,"chartEntrada_habilidad02");
                }
                
            }
        }
    });

});

</script>