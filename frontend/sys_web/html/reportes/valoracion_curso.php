<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Time"); ?>'
  });
</script>
<style type="text/css">
  #tabladatos tbody , #aquidatostabla tbody{
    padding: 1ex 2em;
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  #tabladatos tbody h5 , #aquidatostabla tbody h5 {font-weight: 800;}
  #tibladatos tbody p , #aquidatostabla tbody p{
    padding-left: 20px;
    margin-top: 0px;
    margin-bottom: 1rem;
  }
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
        <div class="x_title">
            <div id="radiobtn" class="row my-x-center ">
                <div class="col-sm-12 col-md-6">                   
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                  <div class="btn-group">
                    <button type="button" class="btn  btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                      <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="area"><i class="fa fa-area-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar" ><i class="fa fa-bar-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="line" ><i class="fa fa-line-chart"></i></button>
                  </div>
              </div>
            </div>
            <div class="clearfix"></div>
        </div>     
        <div class="x_title" id="porgrupo">
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-3 pd2" >
                    <div class="form-group">
                       <label class="control-label"><?php echo JrTexto::_('Study Groups'); ?></label>
                        <select id="idgrupoaula" name="idgrupoaula" class="form-control " data-size="10">
                          <?php 
                          $haygrupo=array();
                          if(!empty($this->gruposauladetalle))
                          foreach ($this->gruposauladetalle as $fk){
                            if(!in_array($fk["idgrupoaula"], $haygrupo)){
                                $haygrupo[]=$fk["idgrupoaula"];  ?>
                                <option  value="<?php echo $fk["idgrupoaula"]?>" <?php echo $this->idgrupoaula==$fk["idgrupoaula"]?'selected="selectted"':'';?>><?php echo $fk["strgrupoaula"] ?></option><?php } ?>
                            <?php } ?>
                        </select> 
                    </div>                    
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3 ">
                    <div class="form-group">
                        <label class="control-label"><?php echo JrTexto::_('Course');?> <span class="required"> * </span></label>
                        <select id="idcursotmp" name="idcursotmp" class="form-control " style="display: none"> 
                        <?php 
                         $haygrupo=array();
                            foreach($this->gruposauladetalle as $cur){ ?>
                                 <option idgrupo="<?php echo $cur["idgrupoaula"] ?>"  value="<?php echo $cur["idgrupoauladetalle"]?>" idcurso="<?php echo @$cur["idcurso"]?>" idcc="<?php echo @$cur["idcomplementario"]?>"><?php echo $cur["strcurso"] ?></option>
                         <?php  }  ?>
                        </select> 
                        <select id="idcurso" name="idcurso" class="form-control " title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3 ">
                    <div class="form-group">
                        <label class="control-label"><?php echo JrTexto::_('Student');?> <span class="required"> * </span></label>
                        <select id="idalumno" name="idalumno" class="form-control " title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                            <option value="">Todos</option>
                        </select>
                    </div>
                </div>         
            </div> 
            <div class="clearfix"></div>
        </div>
        <div class=""> 
          <div class="row" id="impresion" style="display:none">
            <div class="col-md-12 table-responsive" >
              <table id="tabladatos" class="table table-bordered table-hover mdl-data-table my-font my-card my-shadow no-footer dataTable">
                <thead>
                    <tr>                    
                      <th>
                        <div class="row">
                          <div class="col-md-12 text-center">
                            <h1 id="nombre-curso" style="margin: 1em;"></h1>
                            <br>
                          </div>
                        </div>
                        <div class="row  text-center">
                          <div class="estrellas col-6"><br>
                            <h5>Valoracion de los alumnos:</h5>
                            <p class="lead text-center"><i class="fa fa-star" data-value="1"></i><i class="fa fa-star" data-value="2"></i><i class="fa fa-star" data-value="3"></i><i class="fa fa-star" data-value="4"></i><i class="fa fa-star" data-value="5"></i><span style="display:none" id="start">0</span></p>
                          </div>
                          <div class="num-votaciones col-6"><br>
                            <h5>Cantidad de valoraciones:</h5>
                            <p id="num-val">0</p>
                          </div>
                        </div>                        
                      </th>
                    </tr>                                 
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
          <div id="aquidatostabla" class="col-md-12 table-responive">                
          </div>       
     
          <div class="row" id="pnl-panelreporte"  style="display:none">
           <div class="col-md-12" id="grafico"></div>       
          </div>

          <div id="sindatos" style="display:none">
              <div class="row">
              <div class="container">
                  <div class="row">
                      <div class="jumbotron col-md-12 text-center">
                          <h1><strong>Opss!</strong></h1>                
                          <div class="disculpa"><?php echo JrTexto::_('No hemos encontrado data')?>:</div>
                      </div>
                  </div>
              </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
function stars(puntuacionPromedio) {
  clearStars()
  for (var i = 1; i <= puntuacionPromedio; i++) {
    $('i.fa-star[data-value="' + i + '"]').css({
      color: '#F00'
    });
  }
}

function clearStars() {
  for (var i = 1; i <= 5; i++) {
    $('i.fa-star[data-value="' + i + '"]').css({
      color: '#555'
    });
  }
}

var formatooption=function(time,options){ 
  options.vAxis.title='Horas , minutos y segundos';
  options.vAxis.format='HH:mm:ss';
  if(time<60) {
    options.vAxis.viewWindow={ min: [0,0,0], max: [0,0,time]};  
  }else if(time<3600) {   
    var m=time/60;
    options.vAxis.viewWindow={ min: [0,0,0], max: [0,(m+1),0]};   
  } 
  else if(time<86400) {    
    var h=time/3600;
    options.vAxis.viewWindow={ min: [0,0,0], max: [h+1,0,0]};
  }
  return options;
}


$(document).ready(function(){
  var idnumgrafico=0;
  google.charts.load('current', {'packages':['bar', 'corechart', 'table']});


  var dibujarchart=function(tipo){
    $('#impresion').hide();   
    $('#pnl-panelreporte').show();
    $('#aquidatostabla').hide();

    var verxalumno=$('.verpor.active').attr('id')=='tab-0'?false:true;
    var data = new google.visualization.DataTable();
    var options = { title: 'Tiempo de Estudio',subtitle:'',
      chart: { title: 'Tiempo de Estudio',subtitle:'' },
      legend: { position: 'top', maxLines: 3 },
      bar: { groupWidth: '50px' },
      orientation:'horizontal',
      is3D:true,  
      bar:{ groupWidth: "300px" }      ,                
      height:560,
      colors: ['#1b9e77', '#d95f02', '#7570b3'],
      vAxis:{minValue: 0}
    };   
    var maxt=0;
    var tipo_='s';
      options.title='Valoracion de curso : '+ $('#impresion #nombre-curso').text(); 
      if($('#idalumno').val()==''){ // por grupo todos los alumnos
        data.addColumn('string', 'Estudiantes');
        data.addColumn('number', 'valoracion');        
        $('#impresion table tbody').find('tr.usar').each(function(itr,tr){
          var trtmp=[];
          trtmp.push($(tr).attr('alumno'));
          trtmp.push(parseInt($(tr).attr('valoracion')||0));         
          data.addRow(trtmp);
        })      
      }

    idnumgrafico++;
    var divid='grafico'+idnumgrafico;
    var div='<div id="'+divid+'" style="width:px;"></div>';
    $('#pnl-panelreporte #grafico').html(div); 
    var char=null;
    if(tipo=='area') chart = new google.visualization.AreaChart(document.getElementById(divid));
    else if(tipo=='line')chart = new google.visualization.LineChart(document.getElementById(divid));
    else if(tipo=='pie')chart = new google.visualization.PieChart(document.getElementById(divid));
    else chart = new google.visualization.BarChart(document.getElementById(divid));
    try{
      chart.draw(data, options);
    }catch(ex){ console.log(ex); verpor();}
  }
  var nohaydata=function(){
    $('#sindatos').show();
    $('#impresion').hide();   
    $('#alumnosolocurso').hide();    
    $('#pnl-panelreporte').hide();
    $('#aquitablaamostrar').hide();
  }
  var verpor=function(){
    $('#sindatos').hide();
      var $obj=$('.verpor.active');
      var poralumno=$obj.attr('id')=='tab-0'?false:true;
      var vistaactive=$('.changevista.active').attr('vista')||'table';  
      $('#pnl-panelreporte').hide();
      $('#poralumno').hide();
      $('#porgrupo').show();
      $('#pnl-panelreporte').hide();
      $('#aquitablaamostrar').show();
            
      if(vistaactive!='table'){
        dibujarchart(vistaactive);
      }else {
        $('#aquidatostabla').show();
      } 
  }       


  $('#idgrupoaula').select2();
  $('#idgrupoaula').on('change',function(ev){
      var v=$(this).val();
      $('#idcurso').children('option').remove();            
      $('select#idcurso').html($('#idcursotmp').children('option[idgrupo="'+v+'"]').clone());           
      $('#idcurso').select2();
      $('#idcurso').trigger('change');
  });
  $('#idcurso').on('change',function(ev){
      var data=new FormData()
      $cursos=$(this);            
      data.append('idgrupoauladetalle',$cursos.val());
      data.append('sql','sql-soloalumnos');      
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/acad_matricula/',
          callback:function(rs){
              $('#idalumno').children('option:first').nextAll().remove();                    
              if(rs.code==200){
                  if(rs.data.length){
                      $.each(rs.data,function(i,v){
                           $('#idalumno').append(`<option value="`+v.idalumno+`">`+v.stralumno+`</option>`);
                      })                        
                  }
              }
              $('#idalumno').select2().trigger('change');
          }
      });
  });

  $('#idgrupoaula').trigger('change');
  $('#idalumno').on('change',function(ev){

      var data=new FormData();
      $this=$(this);  
      $curso=$('#idcurso').val();
      $('h1#nombre-curso').text($('#idcurso option[value="'+$curso+'"]').text());
      var idcurso=$('#idcurso option[value="'+$curso+'"]').attr('idcurso')||0;
      var idcc=$('#idcurso option[value="'+$curso+'"]').attr('idcc')||0;
      var idalumno=$this.val()||'';
      var idalumnos=[];
      if($this.val()==''){
          $('#idalumno').find('option:first').nextAll().each(function(i,a){
              idalumnos.push($(a).attr('value')||'');
          })
          if(idalumnos.length>0) idalumno=idalumnos;
      }

      data.append('idcurso',idcurso);
      data.append('idcomplementario',idcc);
      data.append('idalumno',idalumno);
      data.append('idgrupoauladetalle',$curso);
      data.append('sql','sql-soloalumnos');
      //data.append('idgrupoaula',$('select#idgrupoaula').val());
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/valoracion/', //json/historial_sesion/_tiempos/',
          callback:function(rs){                   
              if(rs.code==200){ 
                  $tabladatos=$('#impresion');
                  $tabladatos.show();
                  j=0;
                  var tr='';
                  var haydata=false;
                  var puntuacionPromedio = 0;                  
                  $.each(rs.data,function(i,v){
                    puntuacionPromedio += parseInt(v.puntuacion);
                    j++;                    

                    var htmlstart='<span>';
                    var jj=1;
                    for(var ii=1;ii<=parseInt(v.puntuacion);ii++){
                      htmlstart+='<i class="fa fa-star" data-value="'+ii+'" style="color: rgb(255, 0, 0);"></i>';
                    }
                    for(var ii=parseInt(v.puntuacion);ii<5;ii++){
                      htmlstart+='<i class="fa fa-star" data-value="'+ii+'"></i>';
                    }

                    htmlstart+='<span style="display:none"> ('+v.puntuacion+')</span><span>';
                      tr += ` 
                            <tr class="usar" alumno="${v.nombre_alumno}" valoracion="${v.puntuacion}">
                              <td style="border-left: 1px #dee2e6 solid; border-right: 1px #dee2e6 solid; border-bottom: 0px;   border-top: 0px; ">
                                <div class="media mb-4">
                                  <div class="media-body">
                                    <h5 class="mt-0">${v.nombre_alumno}${htmlstart}</h5>
                                    <p class="cont-comentario">${v.comentario}</p> 
                                  </div>
                                </div>
                              </td>
                            </tr>`;
                            haydata=true;
                     
                  })
                  if(haydata==false){
                    tr += ` 
                            <tr class="novale">
                              <td style="border-left: 1px #dee2e6 solid; border-right: 1px #dee2e6 solid; border-bottom: 0px;   border-top: 0px; ">
                                <div class="media mb-4">
                                  <div class="media-body">
                                    <h5 class="mt-0">Ups ! </h5>                                    
                                    <p class="cont-comentario">Este curso no tiene comentarios</p> 
                                  </div>
                                </div>
                              </td>
                            </tr>`;
                  }
                  puntuacionPromedio = Math.round(puntuacionPromedio/j)||0;
                  $('#num-val').html(j);
                  $('span#start').text(puntuacionPromedio);               
                  stars(puntuacionPromedio);                  
                  $('table#tabladatos').children('tbody').html(tr);                  
                  cargartabla();               
              }
          }
      });
  })

  var cargartabla=function(){
    var tb=$('table#tabladatos').clone();
    var idtable='table'+Date.now();
    tb.attr('id',idtable);
      $('#impresion').hide();
      $('#aquidatostabla').show();
      $('#aquidatostabla').html(tb);                       
      $('#aquidatostabla table#'+idtable).DataTable({
        ordering: true,
        searching: false,
        pageLength: 50,
        info: false,
        oLanguage: {
            "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
            "oPaginate": {
                "sFirst": "<?php echo JrTexto::_("Next page") ?>", // This is the link to the first page
                "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                "sLast": "<?php echo JrTexto::_("Previous page") ?>" // This is the link to the last page
            }
        },
        dom: 'Bfrtip',
        buttons: [{
            extend: "excelHtml5",
            className: 'btn-dt btn-excel',
            text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
            messageTop: '',
            title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Valoracion de curso') . " - " . date('d-m-Y'); ?>',
            customize: function(xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                var col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                $tr = $('table#tabladatos tbody').find('tr');
                $.each($tr, function(ir, row) {
                    $(this).find('td').each(function(ic, td) {
                        var xcol = col[ic];
                        if (ir == 0) $('row c[r="' + xcol + '1"]', sheet).attr('s', 32);
                        else {
                            var s = ir % 2 == 0 ? 8 : 20;
                            $('row c[r="' + xcol + (ir + 1) + '"]', sheet).attr('s', s);
                        }
                    })
                })
            }
        }, {
            extend: "pdfHtml5",
            messageTop: '',
            className: 'btn-dt btn-pdf',
            text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
            title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Valoracion de curso') . " - " . date('d-m-Y'); ?>',
            customize: function(doc) {
                doc.pageMargins = [12, 12, 12, 12];
                 $tr = $('table#tabladatos tbody').find('tr');
                var ntr = $tr.length;
                var ntd = $tr.eq(0).find('th').length;
                var ncolA = ["*"];
                /*for(i=0;i<ntd-2;i++){
                    ncolA.push("*");
                }*/
                doc.content[1].table.widths = ncolA;
                var tblBody = doc.content[1].table.body;
                $tr.each(function(ir, row) {
                    var color = ((ir % 2 == 0) ? '' : '#e4e1e1');
                    if ($(this).hasClass('nomcursos')) {
                        $(this).find('td').each(function(ic, td) {
                            tblBody[ir][ic].fillColor = '#5c5858';
                            tblBody[ir][ic].color = "#FFFFFF";
                        })
                    }
                })
                tblBody[ntr][0].alignment = 'left';
                if (_logobase64 != '') {
                    doc.content.splice(0, 0, {
                        margin: [0, 0, 0, 12],
                        alignment: 'center',
                        image: _logobase64
                    });
                }
            }
        }, {
            extend: "print",
            className: 'btn-dt btn-print',
            text: '<i class="fa fa-print" aria-hidden="true"></i>',
            title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Valoración de curso ') . " - " . date('d-m-Y'); ?>',
            customize: function(win) {
                var body = $(win.document.body);
                var title = body.find('h1').first().text();
                if (_logobase64 != '') {
                    body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="' + _logobase64 + '"/><br>' + title + '</div>');
                } else {
                    body.find('h1').first().html('<div style="text-align:center; width:100%;">' + title + '</div>');
                }
                table = body.find('table');
                table.find('th').each(function(index) {
                    table.find('th').css('background-color', '#cccccc')
                });
            }
        }]
    });
    verpor(); 
  }

        $('.changevista').click(function(ev){
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            verpor();
        })
        $('.verpor').click(function(ev){  
          $(this).closest('ul').find('.verpor').removeClass('active');
          $(this).addClass('active');        
          verpor();
        })
    })
</script>