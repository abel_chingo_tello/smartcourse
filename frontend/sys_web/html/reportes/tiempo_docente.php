<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Time"); ?>'
  });
</script>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
        <div class="x_title">
            <div id="radiobtn" class="row my-x-center ">
                <div class="col-sm-12 col-md-6">
                    <ul class="nav nav-tabs" style="display: none;">
                        <li class="nav-item">
                            <a id="tab-0" class="verpor nav-link active" href="#">Por Grupos</a>
                        </li>
                        <li class="nav-item" >
                            <a id="tab-1" class="verpor nav-link" href="#" >Por Alumnos</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                  <div class="btn-group">
                    <button type="button" class="btn  btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                      <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="area"><i class="fa fa-area-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar" ><i class="fa fa-bar-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="line" ><i class="fa fa-line-chart"></i></button>
                  </div>
              </div>
            </div>
            <div class="clearfix"></div>
        </div>     
        <div class="x_title" id="porgrupo">
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-3 pd2" >
                    <div class="form-group">
                       <label class="control-label"><?php echo JrTexto::_('Study Groups'); ?></label>
                        <select id="idgrupoaula" name="idgrupoaula" class="form-control " data-size="10">
                          <?php 
                          $haygrupo=array();
                          if(!empty($this->gruposauladetalle))
                          foreach ($this->gruposauladetalle as $fk){
                            if(!in_array($fk["idgrupoaula"], $haygrupo)){
                                $haygrupo[]=$fk["idgrupoaula"];  ?>
                                <option  value="<?php echo $fk["idgrupoaula"]?>" <?php echo $this->idgrupoaula==$fk["idgrupoaula"]?'selected="selectted"':'';?>><?php echo $fk["strgrupoaula"] ?></option><?php } ?>
                            <?php } ?>
                        </select> 
                    </div>                    
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3 ">
                    <div class="form-group">
                        <label class="control-label"><?php echo JrTexto::_('Course');?> <span class="required"> * </span></label>
                        <select id="idcursotmp" name="idcursotmp" class="form-control " style="display: none"> 
                        <?php 
                         $haygrupo=array();
                            foreach($this->gruposauladetalle as $cur){
                                //if(!in_array($cur["idcurso"]."_".$cur["idcomplementario"], $haygrupo)){
                                //$haygrupo[]=$cur["idcurso"]."_".$cur["idcomplementario"]; ?>
                                 <option idgrupo="<?php echo $cur["idgrupoaula"] ?>"  value="<?php echo $cur["idgrupoauladetalle"]?>" idcurso="<?php echo @$cur["idcurso"]?>" idcc="<?php echo @$cur["idcomplementario"]?>"><?php echo $cur["strcurso"] ?></option>
                         <?php  // } 
                            }
                        ?>
                        </select> 
                        <select id="idcurso" name="idcurso" class="form-control " title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3 ">
                    <div class="form-group">
                        <label class="control-label"><?php echo JrTexto::_('Student');?> <span class="required"> * </span></label>
                        <select id="idalumno" name="idalumno" class="form-control " title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                            <option value="">Todos</option>
                        </select>
                    </div>
                </div>         
            </div> 
            <div class="clearfix"></div>
        </div>
        <div class="x_title" id="poralumno" style="display: none;">
            <div class="col-xs-6 col-sm-4 col-md-3 ">
                <div class="form-group">
                    <label class="control-label"><?php echo JrTexto::_('Student');?> <span class="required"> * </span></label>
                    <select id="idpersona" name="idpersona" class="form-control " title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                    </select>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
      <div class="">        
        <div class="row" id="alumnosolocurso" style="display:none">
            <div class="col-12">
              <h5 class="mb-2 text-primary"><?=JrTexto::_("TIEMPO TOTAL EN PLATAFORMA");?></h5>
            </div>
            
            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Study Time in the Virtual Platform'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoenpv">0</h1>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Tiempo en Curso'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoencurso" >0</h1>
                </div>
              </div>
            </div> 

            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Tiempo en Exámenes'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoenexamen">0</h1>
                </div>
              </div>
            </div>

                       
        </div>
        <div class="row" id="impresion" style="display:none">
            <div class="col-12">
              <h5 class="mb-2 text-primary"><?=JrTexto::_("TIEMPO TOTAL EN CURSO");?></h5>  
            </div>
            <div class="col-md-12 table-responsive" style="display: none">
                <table id="tablaalumnos" class="table table-bordered table-hover mdl-data-table my-font my-card my-shadow no-footer dataTable">
                  <thead>                      
                      <th><?php echo JrTexto::_("Student"); ?></th>
                      <th><?php echo JrTexto::_("Tiempo en curso"); ?><small id="nomcurso"></small></th>
                      <th><?php echo JrTexto::_("Tiempo en Examen"); ?></th>
                      <th><?php echo JrTexto::_("Tiempo en Plataforma"); ?></th>             
                  </thead>
                  <tbody></tbody>
                  <tfoot>
                      <tr class="text-center">
                        <td>Total</td>
                        <td>24h 59m 59s</td>
                        <td>24h 59m 59s</td>
                        <td>24h 59m 59s</td>
                      </tr>
                  </tfoot>
                </table>
            </div>
            <div id="aquitablaamostrar" class="col-md-12 table-responive">                
            </div>
        </div>       
        <div class="row" id="alumnosolocurso1" style="display:none">
            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Study Time in the Virtual Platform'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoenpv">0</h1>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Tiempo en Sesiones'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoencurso" >0</h1>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
              <div class="x_panel">
                <div class="x_title">
                  <h6><?php echo JrTexto::_('Tiempo en Exámenes'); ?></h6>
                </div>
                <div class="">
                  <h1 class="changetiempo _tiempoenexamen">0</h1>
                </div>
              </div>
            </div>

                        
        </div>
        <div class="row" id="pnl-panelreporte"  style="display:none">
         <div class="col-md-12" id="grafico"></div>       
        </div>

        <div class="row" id="impresioncursos" style="display:none">
            <div class="col-12">
              <h5 class="mb-2 text-primary"><?=JrTexto::_("TIEMPO TOTAL EN CURSO");?></h5>
            </div>
            <div class="col-md-12 table-responsive" style="display:none">
                <table id="tablacursos" class="table table-bordered table-hover mdl-data-table my-font my-card my-shadow no-footer dataTable">
                  <thead>                      
                      <th><?php echo JrTexto::_("Courses"); ?></th>
                      <th><?php echo JrTexto::_("Tiempo en sesiones"); ?></th>
                      <th><?php echo JrTexto::_("Tiempo en Examen"); ?></th>
                      <th><?php echo JrTexto::_("Tiempo en Curso"); ?></th>             
                  </thead>
                  <tbody></tbody>
                  <tfoot></tfoot>
                </table>
            </div>
            <div id="aquitablacursos" class="col-md-12 table-responive">                
            </div>
        </div>
        <div id="sindatos" style="display:none">
            <div class="row">
            <div class="container">
                <div class="row">
                    <div class="jumbotron col-md-12 text-center">
                        <h1><strong>Opss!</strong></h1>                
                        <div class="disculpa"><?php echo JrTexto::_('No hemos encontrado data')?>:</div> 

                    </div>
                </div>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
var __formatotiempo=function(seg){
    var _f=function(t,d){return t.toString().padStart(d,'0')}
    var _m=function(s){var m_=parseInt(s/60); var s_=s%60; return {'m':_f(m_,2),'s':_f(s_,2)}}
    var _h=function(s){var h_=parseInt(s/3600);var m_=_m(s%3600); m_.h=_f(h_,2); return m_; }
    var _d=function(s){var d_=parseInt(s/86400);var h_=_h(s%86400);h_.d=_f(d_,2); return h_; }
    if(seg<60) return {'s':_f(seg,2)}
    else if(seg<3600) return _m(seg); 
    else if(seg<86400) return _h(seg); 
    else return _d(seg);    
}
var __formatodhms=function(seg){
    var t=__formatotiempo(seg);
    return (t.d!=undefined?(t.d+'d '):'')+(t.h!=undefined?(t.h+'h '):'')+(t.m!=undefined?(t.m+"m "):'')+t.s+"s";
}
var formatooption=function(time,options){ 
  options.vAxis.title='Horas , minutos y segundos';
  options.vAxis.format='HH:mm:ss';
  if(time<60) {
    options.vAxis.viewWindow={ min: [0,0,0], max: [0,0,time]};  
  }else if(time<3600) {   
    var m=time/60;
    options.vAxis.viewWindow={ min: [0,0,0], max: [0,(m+1),0]};   
  } 
  else if(time<86400) {    
    var h=time/3600;
    options.vAxis.viewWindow={ min: [0,0,0], max: [h+1,0,0]};
  }
  return options;
}
var __formatodhms2=function(seg){
  var t=__formatotiempo(seg);
  var time2=[parseInt((t.h||0)),parseInt((t.m||0)),parseInt((t.s||0))];
  var tt={time:(((t.d||0)+'d ')+((t.h||0)+'h ')+((t.m||0)+'m ')+((t.s||0)+'s ')),time2:time2};
  return tt;
}

var pintarTabla = (tiempos)=>{
  var tr='';
}

var buscarTiemposAlumnos = (idpersona = null) =>{
  var data = new FormData()
  idpersona = idpersona || $('#idalumno').val()
  data.append('idpersona',idpersona);
  __sysAyax({ 
    fromdata:data,
      url:_sysUrlBase_+'json/historial_sesion/_tiemposxalumno/',
      callback:function(rs){
          if(rs.code==200){
              $pln=$('#alumnosolocurso1');
              $impresion=$('#impresion');
              $pln.show();    
              var tnombre = ''
              var tr='';    
              var haydata=false;                
              $.each(rs.data,function(i,v){
                  var texa=0, tcurso=0;
                  $pln.find('._tiempoenpv').text(__formatodhms(v.P)).attr('time',v.P);
                  $pln.find('._tiempoenexamen').text(__formatodhms(v.E)).attr('time',v.E);
                  $pln.find('._tiempoencurso').text(__formatodhms(v.S)).attr('time',v.S);                            
                  $.each(v.cursos,function(i,c){
                      haydata=true;
                      tcurso=c.S;
                      texa=c.E;
                      tnombre=c.nombre||'- curso no existe -';
                        tr+='<tr><td>'+tnombre+'</td><td class="text-center" time="'+tcurso+'">'+__formatodhms(tcurso)+'</td><td class="text-center" time="'+texa+'">'+__formatodhms(texa)+'</td><td class="text-center" time="'+(tcurso+texa)+'">'+__formatodhms(tcurso+texa)+'</td></tr>';
                  })                            
              })
              if(haydata==true){
                $('#tablacursos').children('tbody').html(tr);
                console.log(tr)
                cargarcursos();
              }else nohaydata();
          }
      }
  });  
}

$(document).ready(function(){
var idnumgrafico=0;
google.charts.load('current', {'packages':['bar', 'corechart', 'table']});


var dibujarchart=function(tipo){
  $('#pnl-panelreporte').show();
  $('#aquitablaamostrar').hide();
  $('#alumnosolocurso').hide();
  $('#alumnosolocurso1').hide();
  var verxalumno=$('.verpor.active').attr('id')=='tab-0'?false:true;
  var data = new google.visualization.DataTable();
  var options = { title: 'Tiempo de Estudio',subtitle:'',
    chart: { title: 'Tiempo de Estudio',subtitle:'' },
    legend: { position: 'top', maxLines: 3 },
    bar: { groupWidth: '50px' },
    orientation:'horizontal',
    is3D:true,  
    bar:{ groupWidth: "300px" }      ,                
    height:560,
    colors: ['#1b9e77', '#d95f02', '#7570b3'],
    vAxis:{}
  };
  var maxt=0;
  var tipo_='s';
  if(verxalumno==false){ 
    /*options.chart.subtitle='Curso : '+ $('#idcurso option:selected').text();*/
    options.title='Tiempo de estudio en curso : '+ $('#idcurso option:selected').text(); 
    if($('#idalumno').val()==''){ // por grupo todos los alumnos
      data.addColumn('string', 'Estudiantes');
      var table=$('#impresion #aquitablaamostrar table tbody');
      table.find('tr').each(function(itr,tr){
        $(tr).find('td').each(function(itd,td){
          var time=$(td).attr('time')||0;
          if(itd>0) maxt=(maxt<parseInt(time)?parseInt(time):maxt);          
        })
      })
      if(maxt<86400){
        data.addColumn('timeofday', 'Tiempo en curso');
        data.addColumn('timeofday', 'Tiempo en Exámen');
        data.addColumn('timeofday', 'Tiempo en Plataforma');
        options=formatooption(maxt,options);        
      }else{ 
        data.addColumn('number', 'Tiempo en curso');
        data.addColumn('number', 'Tiempo en Exámen');
        data.addColumn('number', 'Tiempo en Plataforma');
        options.vAxis={minValue: 0,title:'dias',annotation:' dias'};
      }
      table.find('tr').each(function(itr,tr){
        var trtmp=[];
        $(tr).find('td').each(function(itd,td){
          var tt='';
          if(itd==0){
            tt=$(td).text();
          }else{
            if(maxt<86400){
              var time=__formatodhms2(parseInt($(td).attr('time')||0));
              tt=time.time2;
            }else tt=parseInt($(td).attr('time')||0)/86400;            
          }          
          trtmp.push(tt);
        });
        data.addRow(trtmp);
      })      
    }else{ // por grupo , seleccionando un alumno
      data.addColumn('string', 'Estudiante');     
      var infoalumno=$('#alumnosolocurso');
      var _tiempoencurso=parseInt(infoalumno.find('._tiempoencurso').attr('time')||0);
      var _tiempoenexamen=parseInt(infoalumno.find('._tiempoenexamen').attr('time')||0);
      var _tiempoenpv=parseInt(infoalumno.find('._tiempoenpv').attr('time')||0);
      maxt=(maxt<parseInt(_tiempoencurso)?parseInt(_tiempoencurso):maxt);
      maxt=(maxt<parseInt(_tiempoenexamen)?parseInt(_tiempoenexamen):maxt);
      maxt=(maxt<parseInt(_tiempoenpv)?parseInt(_tiempoenpv):maxt);
      if(maxt<86400){
        data.addColumn('timeofday', 'Tiempo en curso');
        data.addColumn('timeofday', 'Tiempo en Exámen');
        data.addColumn('timeofday', 'Tiempo en Plataforma');        
        options=formatooption(maxt,options);        
        _tiempoencurso=__formatodhms2(_tiempoencurso);
        _tiempoencurso=_tiempoencurso.time2;
        _tiempoenexamen=__formatodhms2(_tiempoenexamen);
        _tiempoenexamen=_tiempoenexamen.time2;
        _tiempoenpv=__formatodhms2(_tiempoenpv);
        _tiempoenpv=_tiempoenpv.time2;
        var trtmp=[$('#idalumno option:selected').text(),_tiempoencurso,_tiempoenexamen,_tiempoenpv];
      }else{ 
        data.addColumn('number', 'Tiempo en curso');
        data.addColumn('number', 'Tiempo en Examen');
        data.addColumn('number', 'Tiempo en Plataforma');
        options.vAxis={minValue: 0,title:'dias',annotation:' dias'};
        _tiempoencurso=_tiempoencurso/86400;
        _tiempoenexamen=_tiempoenexamen/86400;
        _tiempoenpv=_tiempoenpv/86400;
        var trtmp=[$('#idalumno option:selected').text(),_tiempoencurso,_tiempoenexamen,_tiempoenpv];
      }          
      data.addRow(trtmp);
    }
  }else{ // por alumno    
    $('#alumnosolocurso1').show();
    $('#impresioncursos').hide();
    data.addColumn('string', 'Cursos');
    options.title='Tiempos de Estudio de : '+ $('#idpersona option:selected').text();
    options.subtitle='Tiempos de Estudio de : '+ $('#idpersona option:selected').text();  
    var table=$('#impresioncursos #aquitablacursos table tbody');
      table.find('tr').each(function(itr,tr){        
        $(tr).find('td').each(function(itd,td){
          var time=$(td).attr('time')||0;
          if(itd>0) maxt=(maxt<parseInt(time)?parseInt(time):maxt);          
        });        
      })     
      if(maxt<86400){
        data.addColumn('timeofday', 'Tiempo en sesiones');
        data.addColumn('timeofday', 'Tiempo en Examen');
        data.addColumn('timeofday', 'Tiempo en curso');
        options=formatooption(maxt,options);       
      }else{ 
        data.addColumn('number', 'Tiempo en sesiones');
        data.addColumn('number', 'Tiempo en Examen');
        data.addColumn('number', 'Tiempo en curso');
        options.vAxis={minValue: 0,title:'dias',annotation:' dias'};
      }

      table.find('tr').each(function(itr,tr){
        var trtmp=[];
        $(tr).find('td').each(function(itd,td){
          var tt='';
          if(itd==0){
            tt=$(td).text();
          }else{
            if(maxt<86400){
              var time=__formatodhms2(parseInt($(td).attr('time')||0));
              tt=time.time2;
            }else tt=parseInt($(td).attr('time')||0)/86400;            
          }          
         trtmp.push(tt);
        });
        data.addRow(trtmp);
      })
  }


  idnumgrafico++;
  var divid='grafico'+idnumgrafico;
  var div='<div id="'+divid+'" style="width:px;"></div>';
  $('#pnl-panelreporte #grafico').html(div); 
  var char=null;
  if(tipo=='area') chart = new google.visualization.AreaChart(document.getElementById(divid));
  else if(tipo=='line')chart = new google.visualization.LineChart(document.getElementById(divid));
  else if(tipo=='pie')chart = new google.visualization.PieChart(document.getElementById(divid));
  else chart = new google.visualization.BarChart(document.getElementById(divid));
  try{
    chart.draw(data, options);
  }catch(ex){ console.log(ex); verpor();}
}
var nohaydata=function(){
  $('#sindatos').show();
  $('#impresion').hide();
  $('#impresioncursos').hide();
  $('#alumnosolocurso').hide();
  $('#alumnosolocurso1').hide();
  $('#pnl-panelreporte').hide();
  $('#aquitablaamostrar').hide();
}
var verpor=function(){
  $('#sindatos').hide();
    var $obj=$('.verpor.active');
    var poralumno=$obj.attr('id')=='tab-0'?false:true;
    var vistaactive=$('.changevista.active').attr('vista')||'table';  
     $('#pnl-panelreporte').hide();
    if(poralumno==false){
        $('#poralumno').hide();
        $('#porgrupo').show();                
        // $('#impresioncursos').hide();
        $('#alumnosolocurso1').hide();
        $('#pnl-panelreporte').hide();
        $('#aquitablaamostrar').show();
        if($('#idalumno').val()==''){   
            $('#alumnosolocurso').hide();
            $('#impresion').show();
        }else{           
            $('#impresion').hide();
            $('#alumnosolocurso').show();
        }        
    }else{
        $('#porgrupo').hide();
        $('#poralumno').show();
        $('#impresion').hide();
        $('#impresioncursos').show();  
        $('#alumnosolocurso1').show();  
        $('#alumnosolocurso').hide();  
        if($('select#idpersona').children().length==0){
            var data=new FormData()
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/reportes/getAlumnosAdmin',
                callback:function(rs){
                    $('#idpersona').children('option').remove();                    
                    if(rs.code==200){
                        if(rs.data.length){
                            $.each(rs.data,function(i,v){
                                 $('#idpersona').append(`<option value="`+v.idalumno+`">`+v.stralumno+`</option>`);
                            })                        
                        }
                    }
                    $('#idpersona').select2().trigger('change');
                }
            });
        }
    }    
    if(vistaactive!='table'){
      dibujarchart(vistaactive);
    }  
}       


        $('#idgrupoaula').select2();
        $('#idgrupoaula').on('change',function(ev){
            var v=$(this).val();
            $('#idcurso').children('option').remove();            
            $('select#idcurso').html($('#idcursotmp').children('option[idgrupo="'+v+'"]').clone());           
            $('#idcurso').select2();
            $('#idcurso').trigger('change');
        });
        $('#idcurso').on('change',function(ev){
            var data=new FormData()
            $cursos=$(this);            
            data.append('idgrupoauladetalle',$cursos.val());
            data.append('sql','sql-soloalumnos');
            //data.append('idgrupoaula',$('select#idgrupoaula').val());
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/acad_matricula/',
                callback:function(rs){
                    $('#idalumno').children('option:first').nextAll().remove();                    
                    if(rs.code==200){
                        if(rs.data.length){
                            $.each(rs.data,function(i,v){
                                 $('#idalumno').append(`<option value="`+v.idalumno+`">`+v.stralumno+`</option>`);
                            })                        
                        }
                    }
                    $('#idalumno').select2().trigger('change');
                }
            });
        });

        $('#idgrupoaula').trigger('change');
        $('#idalumno').on('change',function(ev){
            var data=new FormData();
            $this=$(this);  
            $idgrupoauladetalle=$('#idcurso').val();           
            var idcurso=$('#idcurso option[value="'+$idgrupoauladetalle+'"]').attr('idcurso')||0;
            var idcc=$('#idcurso option[value="'+$idgrupoauladetalle+'"]').attr('idcc')||0;
            var idalumno=$this.val()||'';
            var idalumnos=[];
            if($this.val()==''){
                $('#idalumno').find('option:first').nextAll().each(function(i,a){
                    idalumnos.push($(a).attr('value')||'');
                })
                if(idalumnos.length>0) idalumno=idalumnos;
            }

            data.append('idcurso',idcurso);
            data.append('idcc',idcc);
            data.append('idpersona',idalumno);
            data.append('sql','sql-soloalumnos');
            data.append('idgrupoauladetalle',$idgrupoauladetalle);
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/historial_sesion/_tiempos/',
                callback:function(rs){                    
                    if(rs.code==200){                        
                        $pln=$('#alumnosolocurso'); 
                        $impresion=$('#impresion');                 
                        if($this.val()!=''){                            
                            $pln.show();
                            $impresion.hide();
                            var haydata=false;
                            var texa=0, tcurso=0;
                            var tr='', tfoot = "";
                            var tnombre = "";   
                            var sumas ={ tiempo_sesiones: 0, tiempo_examenes: 0,tiempo_curso:0 }

                            $.each(rs.data,function(i,v){
                                if(i==$this.val()){
                                    
                                    haydata=true;
                                    $pln.find('._tiempoenpv').text(__formatodhms(v.P)).attr('time',v.P);
                                    if(v.cursos[$idgrupoauladetalle]!=undefined){
                                        console.log("entre",2)
                                        var timeE=v.cursos[$idgrupoauladetalle]['E']||0;
                                        var timeS=v.cursos[$idgrupoauladetalle]['S']||0;

                                        $pln.find('._tiempoenexamen').text(__formatodhms(timeE)).attr('time',timeE);
                                        $pln.find('._tiempoencurso').text(__formatodhms(timeS)).attr('time',timeS);
                                        tnombre=v.cursos[$idgrupoauladetalle]['nombre']||'- curso no existe -';
                                        
                                        tr+= `<tr><td>${tnombre}</td><td class="text-center" time="${timeS}">${__formatodhms(timeS)}</td><td class="text-center" time="${timeE}">${__formatodhms(timeE)} </td><td class="text-center" time="${(timeS+timeE)}">${__formatodhms(timeS+timeE)}</td></tr>`
                                        sumas.tiempo_sesiones += timeS;
                                        sumas.tiempo_examenes += timeE;
                                        sumas.tiempo_curso += (timeS+timeE);
                                    }
                                }                            
                            })
                            tfoot = `<tr class="text-center">
                              <td>Total</td>
                              <td>${__formatodhms(sumas.tiempo_sesiones)}</td>
                              <td>${__formatodhms(sumas.tiempo_examenes)}</td>
                              <td>${__formatodhms(sumas.tiempo_curso)}</td>
                            </tr>`
                            if(haydata==true){
                                console.log(tr)
                                $('#tablacursos').children('tbody').html(tr);
                                $('#tablacursos').children('tfoot').html(tfoot);
                                
                                cargarcursos();
                                verpor();
                              }else {
                                nohaydata(); 
                              }
                        }else{
                            $pln.hide();
                            $impresion.show();                            
                            j=0;
                            var tr='',tfoot='';
                            var haydata=false;
                            var sumas ={ tiempo_sesiones: 0, tiempo_examenes: 0,tiempo_plataforma:0 }

                            sumas.tiempo_sesiones = 0
                            sumas.tiempo_examenes = 0
                            sumas.tiempo_plataforma = 0
                            $.each(rs.data,function(i,v){
                                var texa=0, tcurso=0;
                                haydata=true;
                                if(v.cursos[$idgrupoauladetalle]!=undefined){
                                    tcurso=v.cursos[$idgrupoauladetalle]['S'];
                                    texa=v.cursos[$idgrupoauladetalle]['E'];
                                }
                                sumas.tiempo_sesiones += tcurso;
                                sumas.tiempo_examenes += texa;
                                sumas.tiempo_plataforma += v.P;
                                j++;
                                tr+='<tr><td>'+v.persona+'</td><td class="text-center" time="'+tcurso+'">'+__formatodhms(tcurso)+'</td><td class="text-center" time="'+texa+'">'+__formatodhms(texa)+'</td><td class="text-center" time="'+v.P+'" >'+__formatodhms(v.P)+'</td></tr>';
                            })
                            tfoot = `<tr class="text-center">
                              <td>Total</td>
                              <td>${__formatodhms(sumas.tiempo_sesiones)}</td>
                              <td>${__formatodhms(sumas.tiempo_examenes)}</td>
                              <td>${__formatodhms(sumas.tiempo_plataforma)}</td>
                            </tr>`
                            if(haydata==true){
                              $('#tablaalumnos').children('tbody').html(tr);
                              $('#tablaalumnos').children('tfoot').html(tfoot);
                              cargartabla();
                            }else{
                              nohaydata();
                            }
                        }
                    }
                }
            });
        })

        $('#idpersona').on('change',function(ev){
            buscarTiemposAlumnos($(this).val())    
        })

        var cargarcursos=function(){
            var tb=$('#tablacursos').clone();
            var idtable='table'+Date.now();
            tb.attr('id',idtable);
            $('#impresion').hide();
            $('#impresioncursos').show();
            $('#aquitablacursos').html(tb);                       
            $('#aquitablacursos table#'+idtable).DataTable({
                ordering: true,
                searching: false,
                pageLength: 50,
                info: false,
                oLanguage: {
                    "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
                    "oPaginate": {
                        "sFirst": "<?php echo JrTexto::_("Next page") ?>", // This is the link to the first page
                        "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                        "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                        "sLast": "<?php echo JrTexto::_("Previous page") ?>" // This is the link to the last page
                    }
                },
                dom: 'Bfrtip',
                buttons: [{
                    extend: "excelHtml5",
                    className: 'btn-dt btn-excel',
                    text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
                    messageTop: '',
                    title: '<?php echo JrTexto::_('Report') ?>',
                    customize: function(xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        var col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                        $tr = $('#aquitablacursos '+idtable+' tbody').find('tr');
                        $.each($tr, function(ir, row) {
                            $(this).find('td').each(function(ic, td) {
                                var xcol = col[ic];
                                if (ir == 0) $('row c[r="' + xcol + '1"]', sheet).attr('s', 32);
                                else {
                                    var s = ir % 2 == 0 ? 8 : 20;
                                    $('row c[r="' + xcol + (ir + 1) + '"]', sheet).attr('s', s);
                                }
                            })
                        })
                    }
                }, {
                    extend: "pdfHtml5",
                    messageTop: '',
                    className: 'btn-dt btn-pdf',
                    text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
                    title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Time') . " - " . date('d-m-Y'); ?>',
                    customize: function(doc) {
                        doc.pageMargins = [12, 12, 12, 12];
                         $tr = $('#aquitablacursos '+idtable+' tbody').find('tr');
                        var ntr = $tr.length;
                        var ntd = $tr.eq(0).find('th').length;
                        var ncolA = [30, "*", "*", "*"];
                        /*for(i=0;i<ntd-2;i++){
                            ncolA.push("*");
                        }*/
                        doc.content[1].table.widths = ncolA;
                        var tblBody = doc.content[1].table.body;
                        $tr.each(function(ir, row) {
                            var color = ((ir % 2 == 0) ? '' : '#e4e1e1');
                            if ($(this).hasClass('nomcursos')) {
                                $(this).find('td').each(function(ic, td) {
                                    tblBody[ir][ic].fillColor = '#5c5858';
                                    tblBody[ir][ic].color = "#FFFFFF";
                                })
                            }
                        })
                        tblBody[ntr][0].alignment = 'left';
                        if (_logobase64 != '') {
                            doc.content.splice(0, 0, {
                                margin: [0, 0, 0, 12],
                                alignment: 'center',
                                image: _logobase64
                            });
                        }
                    }

                }, {
                    extend: "print",
                    className: 'btn-dt btn-print',
                    text: '<i class="fa fa-print" aria-hidden="true"></i>',
                    title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Time') . " - " . date('d-m-Y'); ?>',
                    customize: function(win) {
                        var body = $(win.document.body);
                        var title = body.find('h1').first().text();
                        if (_logobase64 != '') {
                            body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="' + _logobase64 + '"/><br>' + title + '</div>');
                        } else {
                            body.find('h1').first().html('<div style="text-align:center; width:100%;">' + title + '</div>');
                        }
                        table = body.find('table');
                        table.find('th').each(function(index) {
                            table.find('th').css('background-color', '#cccccc')
                        });
                    }
                }]
            });
            verpor(); 
        }

        var cargartabla=function(){
            var tb=$('#tablaalumnos').clone();
            var idtable='table'+Date.now();
            tb.attr('id',idtable);
            $('#impresioncursos').hide();
            $('#impresion').show();
            $('#aquitablaamostrar').html(tb);
            $('#aquitablaamostrar table#'+idtable).DataTable({
                ordering: true,
                searching: false,
                pageLength: 50,
                info: false,
                oLanguage: {
                    "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
                    "oPaginate": {
                        "sFirst": "<?php echo JrTexto::_("Next page") ?>", // This is the link to the first page
                        "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                        "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                        "sLast": "<?php echo JrTexto::_("Previous page") ?>" // This is the link to the last page
                    }
                },
                dom: 'Bfrtip',
                buttons: [{
                    extend: "excelHtml5",
                    className: 'btn-dt btn-excel',
                    text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
                    messageTop: '',
                    title: '<?php echo JrTexto::_('Report') ?>',
                    customize: function(xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        var col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                        $tr = $('#aquitablaamostrar '+idtable+' tbody').find('tr');
                        $.each($tr, function(ir, row) {
                            $(this).find('td').each(function(ic, td) {
                                var xcol = col[ic];
                                if (ir == 0) $('row c[r="' + xcol + '1"]', sheet).attr('s', 32);
                                else {
                                    var s = ir % 2 == 0 ? 8 : 20;
                                    $('row c[r="' + xcol + (ir + 1) + '"]', sheet).attr('s', s);
                                }
                            })
                        })
                    }
                }, {
                    extend: "pdfHtml5",
                    messageTop: '',
                    className: 'btn-dt btn-pdf',
                    text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
                    title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Time') . " - " . date('d-m-Y'); ?>',
                    customize: function(doc) {
                        doc.pageMargins = [12, 12, 12, 12];
                         $tr = $('#aquitablaamostrar '+idtable+' tbody').find('tr');
                        var ntr = $tr.length;
                        var ntd = $tr.eq(0).find('th').length;
                        var ncolA = [30, "*", "*", "*"];
                        /*for(i=0;i<ntd-2;i++){
                            ncolA.push("*");
                        }*/
                        doc.content[1].table.widths = ncolA;
                        var tblBody = doc.content[1].table.body;
                        $tr.each(function(ir, row) {
                            var color = ((ir % 2 == 0) ? '' : '#e4e1e1');
                            if ($(this).hasClass('nomcursos')) {
                                $(this).find('td').each(function(ic, td) {
                                    tblBody[ir][ic].fillColor = '#5c5858';
                                    tblBody[ir][ic].color = "#FFFFFF";
                                })
                            }
                        })
                        tblBody[ntr][0].alignment = 'left';
                        if (_logobase64 != '') {
                            doc.content.splice(0, 0, {
                                margin: [0, 0, 0, 12],
                                alignment: 'center',
                                image: _logobase64
                            });
                        }
                    }

                }, {
                    extend: "print",
                    className: 'btn-dt btn-print',
                    text: '<i class="fa fa-print" aria-hidden="true"></i>',
                    title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Time') . " - " . date('d-m-Y'); ?>',
                    customize: function(win) {
                        var body = $(win.document.body);
                        var title = body.find('h1').first().text();
                        if (_logobase64 != '') {
                            body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="' + _logobase64 + '"/><br>' + title + '</div>');
                        } else {
                            body.find('h1').first().html('<div style="text-align:center; width:100%;">' + title + '</div>');
                        }
                        table = body.find('table');
                        table.find('th').each(function(index) {
                            table.find('th').css('background-color', '#cccccc')
                        });
                    }
                }]
            });
            verpor(); 
        }
        $('.changevista').click(function(ev){
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            verpor();
        })
        $('.verpor').click(function(ev){  
          $(this).closest('ul').find('.verpor').removeClass('active');
          $(this).addClass('active');        
          verpor();
        })
    })
</script>