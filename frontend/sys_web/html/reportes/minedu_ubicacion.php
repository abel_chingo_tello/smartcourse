<?php

?>
<style>
.row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

select.select-ctrl{ padding-right: 35px; }
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.box_custom{ background-color:white; border:1px solid #4683af; border-radius:0.5em; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<div class="row" id="search_container">
    <div class="col-md-5">
        <div class="table-responsive box_custom" style="padding: 5px 0;">
            <table class="table" id="tabla01">
                <thead style="background-color:#d3e5fb;">
                    <th><?php echo $this->Entidad;?></th>
                    <th>A1</th>
                    <th>A2</th>
                    <th>B1</th>
                    <th>B2</th>
                    <th>C1</th>
                </thead>
                <tbody>
                    <?php
                        if(!empty($this->RowTabla)){
                            foreach($this->RowTabla as $r){
                                echo "<tr>";
                                echo "<td>{$r['nombre']}</td>";
                                echo "<td>{$r['A1']}%</td>";
                                echo "<td>{$r['A2']}%</td>";
                                echo "<td>{$r['B1']}%</td>";
                                echo "<td>{$r['B2']}%</td>";
                                echo "<td>{$r['C1']}%</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-7" >
        <div class="col-sm-6" style="">
            <div  style="text-align:center; background-color:white; border:1px solid #4683af; border-radius:0.5em;">
                <h5 style="text-align:center; font-weight:bold;"><?php echo $this->Entidad2; ?> 1</h5>
                <h4 style="display:inline-block;"><?php echo $this->Entidad; ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select class="select-docente form-control select-ctrl " id="item1">
                        <option value="0">Seleccionar</option>
                        <?php 
                            if(!empty($this->RowTabla)){
                                foreach($this->RowTabla as $k => $v){
                                    echo "<option value='".($k+1)."'>{$v['nombre']}</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="box_custom">
                <h5 style="text-align:center; font-weight:bold;">Habilidades</h5>
                <div class="chart-container" id="Container_Ubicacion_habilidad01" style="position: relative; margin:0 auto; height:100%; width:100%">
                    <canvas id="chartUbicacion_habilidad01"></canvas>
                </div>            
            </div>
        </div>
        <div class="col-sm-6" style="">
            <div style="text-align:center; background-color:white; border:1px solid #4683af; border-radius:0.5em;">
                <h5 style="text-align:center; font-weight:bold;"><?php echo $this->Entidad2; ?> 2</h5>
                <h4 style="display:inline-block;"><?php echo $this->Entidad; ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select class="select-docente form-control select-ctrl " id="item2">
                        <option value="0">Seleccionar</option>
                        <?php 
                            if(!empty($this->RowTabla)){
                                foreach($this->RowTabla as $k => $v){
                                    echo "<option value='".($k+1)."'>{$v['nombre']}</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="box_custom">
                <h5 style="text-align:center; font-weight:bold;">Habilidades</h5>
                <div class="chart-container" id="Container_Ubicacion_habilidad02" style="position: relative; margin:0 auto; height:100%; width:100%">
                    <canvas id="chartUbicacion_habilidad02"></canvas>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="box_custom">
                <h4 style="text-align:center; font-weight:bold;">Comparativo</h4>
                <div class="chart-container" id="Container_Ubicacion_habilidad03" style="position: relative; margin:0 auto; height:100%; width:100%">
                    <canvas id="chartUbicacion_habilidad03"></canvas>
                </div>            
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
/**VARIABLE GLOBALES */
var entidad = '<?php echo $this->Entidad2; ?>';
var datos = <?php echo !empty($this->RowTabla) ? json_encode($this->RowTabla) : 'null' ?>;
var drawChart = function(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: false,
        text: 'Empty'
      },
      scales:{"yAxes":[{"ticks":{"beginAtZero":true}}]},
      tooltips: {
        mode: 'point',
        intersect: true
      },legend: {
            display: false
         }
    }
  });
};

function drawChart2(obj,dat,texto  = 'empty' ){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: false,
        text: texto
      },
      tooltips: {
        custom: function(tooltipModel) {
          if(tooltipModel.body){
            var strline = tooltipModel.body[0].lines[0];
            var search = strline.search('hide');
            if(search != -1){
              tooltipModel.width = 125;
              tooltipModel.body[0].lines[0] = strline.replace('hide','Diferencia');
            }
          }
        }
      },
      scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        },
      legend: {
        labels: {
          filter: function(item, chart) {
            // Logic to remove a particular legend item goes here
            return !item.text.includes('hide');
          }
        },
        onHover: function(event, legendItem) {
          document.getElementById(obj).style.cursor = 'pointer';
        },
        onClick: function(e,legendItem){
          var index = legendItem.datasetIndex;
          var ci = this.chart;
          // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
          var meta = ci.getDatasetMeta(index);
          // alert(index);
          if(index == 2){
            var meta2 = ci.getDatasetMeta(3);
            meta2.hidden = meta2.hidden === null? !ci.data.datasets[3].hidden : null;
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;

          }else{
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
          }
          

          ci.update();
        }
      }//end legend
    }
  });
}

$('document').ready(function(){
    $('#search_container').parent().css('margin','5px 0');
    $('#search_container').parent().css('width','100%');
    $('#tabla01').DataTable({
        "language":{
            "url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    });
    var chartData = {
        labels: ['Listening','Reading','Writing','Speaking'],
        datasets: [ {
        type: 'bar',
        backgroundColor: ['rgba(233,30,99,0.5)','rgba(255,87,34,0.5)','rgba(33,150,243,0.5)','rgba(25,0,131,0.5)'],
        data: [
            0,0,0,0
        ],
        borderColor: 'white',
        borderWidth: 2
        }]

    };
    drawChart('chartUbicacion_habilidad01',chartData);
    var chartData = {
        labels: ['Listening','Reading','Writing','Speaking'],
        datasets: [ {
        type: 'bar',
        backgroundColor: ['rgba(233,30,99,0.5)','rgba(255,87,34,0.5)','rgba(33,150,243,0.5)','rgba(25,0,131,0.5)'],
        data: [
            0,0,0,0
        ],
        borderColor: 'white',
        borderWidth: 2
        }]

    };
    drawChart('chartUbicacion_habilidad02',chartData);
    var datosRadar = new Array();
    datosRadar[0] = 0;
    datosRadar[1] = 0;
    datosRadar[2] = 0;
    datosRadar[3] = 0;
    var datosRadar2 = new Array();
    datosRadar2[0] = 0;
    datosRadar2[1] = 0;
    datosRadar2[2] = 0;
    datosRadar2[3] = 0;
    var datosRadar3 = new Array();
    datosRadar3[0] = 0;
    datosRadar3[1] = 0;
    datosRadar3[2] = 0;
    datosRadar3[3] = 0;
    var datosRadar4 = new Array();
    datosRadar4[0] = 0;
    datosRadar4[1] = 0;
    datosRadar4[2] = 0;
    datosRadar4[3] = 0;
  //datosRadar = entrada . datosRadar2 = salida
    for(var i = 0; i < 4; i++){
        if(datosRadar[i] > datosRadar2[i]){
        datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
        }else if(datosRadar[i] < datosRadar2[i]){
        datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
        }
    }
    var barChartData = {
        labels: ["Listening", "Reading", "Writing", "Speaking"],
        datasets: [{
            label: entidad + ' 1',
            backgroundColor: '#36a2eb',
            stack:'Stack 0',
            data: [
            datosRadar[0],datosRadar[1] ,datosRadar[2] ,datosRadar[3]
            ]
        }, {
            label: entidad + ' 2',
            backgroundColor: '#ff6384',
            stack:'Stack 1',
            data: [
            datosRadar2[0],datosRadar2[1] ,datosRadar2[2] ,datosRadar2[3]
            ]
        },{
            label: 'Diferencia',
            backgroundColor: '#d4b02f',
            stack: 'Stack 0',
            data: [
            Math.abs(datosRadar3[0]), Math.abs(datosRadar3[1]), Math.abs(datosRadar3[2]) , Math.abs(datosRadar3[3])
            ]
        },{
            label: 'hide',
            backgroundColor: '#d4b02f',
            stack: 'Stack 1',
            
            data: [
                Math.abs(datosRadar4[0]), Math.abs(datosRadar4[1]), Math.abs(datosRadar4[2]) , Math.abs(datosRadar4[3])
            ]
        }]

    };
    drawChart2("chartUbicacion_habilidad03",barChartData);

    /** FUNCIONES */
    var drawComparativa = function(){
        var datosRadar = new Array();
        datosRadar[0] = 0;
        datosRadar[1] = 0;
        datosRadar[2] = 0;
        datosRadar[3] = 0;
        var datosRadar2 = new Array();
        datosRadar2[0] = 0;
        datosRadar2[1] = 0;
        datosRadar2[2] = 0;
        datosRadar2[3] = 0;
        var datosRadar3 = new Array();
        datosRadar3[0] = 0;
        datosRadar3[1] = 0;
        datosRadar3[2] = 0;
        datosRadar3[3] = 0;
        var datosRadar4 = new Array();
        datosRadar4[0] = 0;
        datosRadar4[1] = 0;
        datosRadar4[2] = 0;
        datosRadar4[3] = 0;
        if($('#item1').val() !=0 && $('#item2').val() !=0){
            var valor1 = $('#item1').val();
            var valor2 = $('#item2').val();
            if(datos != null){
                for(var i = 0;i < Object.keys(datos).length; i++ ){
                    if((i + 1)  == valor1){
                        datosRadar[0] = datos[i]['4'];
                        datosRadar[1] = datos[i]['5'];
                        datosRadar[2] = datos[i]['6'];
                        datosRadar[3] = datos[i]['7'];
                        break;
                    }//end if condition
                }//end for datos
                for(var i = 0;i < Object.keys(datos).length; i++ ){
                    if((i + 1)  == valor2){
                        datosRadar2[0] = datos[i]['4'];
                        datosRadar2[1] = datos[i]['5'];
                        datosRadar2[2] = datos[i]['6'];
                        datosRadar2[3] = datos[i]['7'];
                        break;
                    }//end if condition
                }//end for datos
            }//end datos
        }
        for(var i = 0; i < 4; i++){
            if(datosRadar[i] > datosRadar2[i]){
            datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
            }else if(datosRadar[i] < datosRadar2[i]){
            datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
            }
        }
        var barChartData = {
            labels: ["Listening", "Reading", "Writing", "Speaking"],
            datasets: [{
                label: entidad + ' 1',
                backgroundColor: '#36a2eb',
                stack:'Stack 0',
                data: [
                datosRadar[0],datosRadar[1] ,datosRadar[2] ,datosRadar[3]
                ]
            }, {
                label: entidad + ' 2',
                backgroundColor: '#ff6384',
                stack:'Stack 1',
                data: [
                datosRadar2[0],datosRadar2[1] ,datosRadar2[2] ,datosRadar2[3]
                ]
            },{
                label: 'Diferencia',
                backgroundColor: '#d4b02f',
                stack: 'Stack 0',
                data: [
                Math.abs(datosRadar3[0]), Math.abs(datosRadar3[1]), Math.abs(datosRadar3[2]) , Math.abs(datosRadar3[3])
                ]
            },{
                label: 'hide',
                backgroundColor: '#d4b02f',
                stack: 'Stack 1',
                
                data: [
                    Math.abs(datosRadar4[0]), Math.abs(datosRadar4[1]), Math.abs(datosRadar4[2]) , Math.abs(datosRadar4[3])
                ]
            }]

        };
        $("#Container_Ubicacion_habilidad03").html(" ").html('<canvas id="chartUbicacion_habilidad03"></canvas>');
        drawChart2("chartUbicacion_habilidad03",barChartData);
    };
    /** EVENTOS */
    $('#item1').change(function(){
        if($(this).val() != 0){
            if(datos != null){
                for(var i = 0;i < Object.keys(datos).length; i++ ){
                    if((i + 1)  == $(this).val()){
                        var chartData = {
                            labels: ['Listening','Reading','Writing','Speaking'],
                            datasets: [ {
                            type: 'bar',
                            backgroundColor: ['rgba(233,30,99,0.5)','rgba(255,87,34,0.5)','rgba(33,150,243,0.5)','rgba(25,0,131,0.5)'],
                            data: [
                                datos[i]['4'],datos[i]['5'],datos[i]['6'],datos[i]['7']
                            ],
                            borderColor: 'white',
                            borderWidth: 2
                            }]

                        };
                        $("#Container_Ubicacion_habilidad01").html(" ").html('<canvas id="chartUbicacion_habilidad01"></canvas>');
                        drawChart('chartUbicacion_habilidad01',chartData);
                        break;
                    }//end if condition
                }//end for datos
            }//end datos
        }
        drawComparativa();
    });
    $('#item2').change(function(){
        if($(this).val() != 0){
            if(datos != null){
                for(var i = 0;i < Object.keys(datos).length; i++ ){
                    if((i + 1)  == $(this).val()){
                        var chartData = {
                            labels: ['Listening','Reading','Writing','Speaking'],
                            datasets: [ {
                            type: 'bar',
                            backgroundColor: ['rgba(233,30,99,0.5)','rgba(255,87,34,0.5)','rgba(33,150,243,0.5)','rgba(25,0,131,0.5)'],
                            data: [
                                datos[i]['4'],datos[i]['5'],datos[i]['6'],datos[i]['7']
                            ],
                            borderColor: 'white',
                            borderWidth: 2
                            }]

                        };
                        $("#Container_Ubicacion_habilidad02").html(" ").html('<canvas id="chartUbicacion_habilidad02"></canvas>');
                        drawChart('chartUbicacion_habilidad02',chartData);
                        break;
                    }//end if condition
                }//end for datos
            }//end datos
        }
        drawComparativa();        
    });
});
</script>