<?php defined("RUTA_BASE") or die();
$urlreportes = $this->documento->getUrlBase() . "reportes/";
?>
<div class="paris-navbar-container"></div>
<script>
    addItemNavBar({
        text: '<?php echo JrTexto::_("Progresos"); ?>'
    });
</script>
<style type="text/css">
    .bootstrap-select>.dropdown-toggle {
        border: 1px solid #0070a0;
        border-radius: 0.5ex;
    }

    .bootstrap-select>.dropdown-toggle::before {
        font-family: FontAwesome;
        font-size: 1.3em;
        content: "\f0dd";
        display: inline-block;
        text-align: center;
        width: 30px;
        height: 100%;
        background-color: #0070a0;
        position: absolute;
        top: 0;
        right: 0px;
        pointer-events: none;
        color: #FFF;
        border-radius: 0px 0.3ex 0.3ex 0px;
    }

    .popover {
        max-width: none;
    }
</style>
<input type="hidden" name="idalumno" id="idalumno" value="<?php echo $this->idalumno; ?>">
<div class="row soft-rw my-font-all">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel my-card my-shadow">
            <div id="cont-btn" class="x_title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <!--button type="button" class="btn btn-xs btn-success btnexportar_xlsx"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_1</button-->
                            <button type="button" class="btn btn-xs btn-success btnexportar_xlsx2"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx</button>
                            <!--button type="button" class="btn btn-xs btn-primary btnexportar_pdf"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_1</button-->
                            <button type="button" class="btn btn-xs btn-primary btnexportar_pdf2"><i class="fa fa-file-pdf-o" title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF</button>
                            <button type="button" class="btn btn-xs btn-warning btnimprimir"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_1</button>
                            <button type="button" class="btn btn-xs btn-warning btnimprimir_2"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_2</button>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                            <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                            <button type="button" class="btn btn-secondary btn-sm changevista" vista="area_char"><i class="fa fa-area-chart"></i></button>
                            <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar_char"><i class="fa fa-bar-chart"></i></button>
                            <button type="button" class="btn btn-secondary btn-sm changevista" vista="line_char"><i class="fa fa-line-chart"></i></button>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_title">
                <div class="row">
                    <div class=" col-md-12 form-group">
                        <select id="idcurso" name="idcurso" class="form-control selectpicker" multiple title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                            <?php if (!empty($this->cursos)) : ?>
                                <?php foreach ($this->cursos as $fk) : ?>
                                    <option selected="selected" value="<?php echo $fk["idgrupoauladetalle"] ?>" <?php echo @$frm["idgrupoauladetalle"] == $fk["idgrupoauladetalle"] ? 'selected="selectted"' : ''; ?>><?php echo $fk["nombrecurso"] ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="x_content">
                <div class="row" id="pnl-panelreporte">
                </div>
                <div class="row" id="impresion" style="display: none;">
                    <div class="table-responsive">
                    </div>
                </div>
                <div id="impresion2" style="display: none;">
                    <div class="table-responsive">
                    </div>
                </div>
                <div id="sindatos" style="display:none">
                    <div class="container">
                        <div class="row">
                            <div class="jumbotron">
                                <h1><strong>Opss!</strong></h1>
                                <div class="disculpa"><?php echo JrTexto::_('Sorry, You do not have students enrolled') ?>:</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        const oMine = new Mine();
        __logoEmpresaimagebase64();
        google.charts.load('current', {
            'packages': ['corechart']
        });

        function charst(tipo, dt, options, idelem) {
            var strcurso = [];
            var data = new google.visualization.DataTable();
            data.addColumn('string', '<?php echo ucfirst(JrTexto::_('Courses')); ?>');
            data.addColumn('number', '<?php echo ucfirst(JrTexto::_('Progreso')); ?>%');
            var datos = dt
            if (datos != null && datos.length) {
                $.each(datos, function(i, v) {
                    data.addRow([v.nombre, v.progreso]);
                })
                options.title = options.title;

                //console.log(datos);
                if (tipo == 'area') var chart = new google.visualization.AreaChart(document.getElementById(idelem));
                else if (tipo == 'line') var chart = new google.visualization.LineChart(document.getElementById(idelem));
                else if (tipo == 'pie') var chart = new google.visualization.PieChart(document.getElementById(idelem));
                else var chart = new google.visualization.BarChart(document.getElementById(idelem));
                chart.draw(data, options);
            }
        }

        var verrptalumno = 1;
        localStorage.setItem('rtpalumno', 'ver_rpt' + verrptalumno);
        var idnumgrafico = 0;

        var vistaimpresion = function() {
            var dataprogreso = localStorage.getItem('dataprogreso');
            var dt = JSON.parse(dataprogreso);
            var datos = dt;
            var nmaxformula = 1;
            var bases = [];


            //if(nmaxformula<2) nmaxformula=3;
            var total = 0;
            var tr = '';
            //console.log(nmaxformula);
            var tr1 = '<thead><tr style="color:#ffffff; background-color:#5c5858;"><th>#</th><th ><?php echo JrTexto::_('Course') ?></th><th>Progreso % </th></tr><tbody>';

            if (datos && datos.length) {
                var tmpdata = [];
                $.each(datos, function(i, v) {
                    tr1 += '<tr><td class="text-center">' + (i + 1) + '</td><td >' + v.nombre + '</td><td class="text-center"><b>' + v.progreso + '</b></td></tr>';
                    total++;
                })
            }

            tr1 = '<table class="table-striped" id="tableimprimir" style="width: 100%">' + tr1 + '<tr><td></td><td class="text-center"><?php echo JrTexto::_("Total of records"); ?> : ' + total + '</td><td></td><td></td></tr></tbody></table>';

            $('#impresion .table-responsive').html(tr1);
            $('#impresion').find('#tableimprimir').DataTable({
                ordering: false,
                searching: false,
                paging: false,
                dom: 'Bfrtip',
                buttons: [{
                    extend: "excelHtml5",
                    messageTop: '',
                    title: '<?php echo JrTexto::_('Report') ?>',
                    customize: function(xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        var col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                        $tr = $('#tableimprimir tbody').find('tr');
                        $.each($tr, function(ir, row) {
                            $(this).find('td').each(function(ic, td) {
                                var xcol = col[ic];
                                if (ir == 0) $('row c[r="' + xcol + '1"]', sheet).attr('s', 32);
                                else {
                                    var s = ir % 2 == 0 ? 8 : 20;
                                    $('row c[r="' + xcol + (ir + 1) + '"]', sheet).attr('s', s);
                                }
                            })
                        })
                    }
                }, {
                    extend: "pdfHtml5",
                    messageTop: '',
                    title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Progreso') . " - " . date('d-m-Y'); ?>',
                    customize: function(doc) {
                        doc.pageMargins = [12, 12, 12, 12];
                        $tr = $('#tableimprimir tbody').find('tr');
                        var ntr = $tr.length;
                        var ntd = $tr.eq(0).find('th').length;
                        var ncolA = [30, "*", "*", "*"];
                        /*for(i=0;i<ntd-2;i++){
                            ncolA.push("*");
                        }*/
                        doc.content[1].table.widths = ncolA;
                        var tblBody = doc.content[1].table.body;
                        $tr.each(function(ir, row) {
                            var color = ((ir % 2 == 0) ? '' : '#e4e1e1');
                            if ($(this).hasClass('nomcursos')) {
                                $(this).find('td').each(function(ic, td) {
                                    tblBody[ir][ic].fillColor = '#5c5858';
                                    tblBody[ir][ic].color = "#FFFFFF";
                                })
                            }
                        })
                        tblBody[ntr][0].alignment = 'left';
                        if (_logobase64 != '') {
                            doc.content.splice(0, 0, {
                                margin: [0, 0, 0, 12],
                                alignment: 'center',
                                image: _logobase64
                            });
                        }
                    }

                }, {
                    extend: "print",
                    title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Progreso') . " - " . date('d-m-Y'); ?>',
                    customize: function(win) {
                        var body = $(win.document.body);
                        var title = body.find('h1').first().text();
                        if (_logobase64 != '') {
                            body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="' + _logobase64 + '"/><br>' + title + '</div>');
                        } else {
                            body.find('h1').first().html('<div style="text-align:center; width:100%;">' + title + '</div>');
                        }
                        table = body.find('table');
                        table.find('th').each(function(index) {
                            table.find('th').css('background-color', '#cccccc')
                        });
                    }
                }]
            })
        }
        var mostrarresultado = function() {
            var dataprogreso = localStorage.getItem('dataprogreso');
            var vistareporte = localStorage.getItem('vistaReporte1') || $('.changevista.active').attr('vista');
            var dt = JSON.parse(dataprogreso);
            if (vistareporte == 'table') {
                var table = '<div class="container"> <div class="col-md-12 table-reponsive"><table class="table table-striped table-hover"><thead><tr class="headings"><th>#</th>';
                table += '<th><?php echo JrTexto::_("Course"); ?></th><th class="text-center"><?php echo JrTexto::_("Progreso"); ?>%</th></tr></thead><tbody>';

                if (dt.length)
                    $.each(dt, function(i, c) {
                        table += '<tr><td  style="padding:0.5ex 2ex">' + (i + 1) + '</td><td style="padding:0.5ex 1ex">' + c.nombre + '</td><td class="text-center">' + parseFloat(c.progreso) + '</td>';
                    })
                table += '</tbody>';
                table += '</table></div></div>';
                $('#pnl-panelreporte').html(table);
                $('[rel="popover"]').click(function(ev) {
                    return false
                }).popover({
                    container: 'body',
                    placement: 'top',
                    trigger: 'hover',
                    html: true,
                    content: function() {
                        var clone = $($(this).find('table').clone(true)).show();
                        return clone;
                    }
                })
                $('#pnl-panelreporte').find("#changepuntaje").children().remove();
                $('#pnl-panelreporte table').DataTable({
                    ordering: false,
                    searching: false,
                    pageLength: 50,
                    info: false,
                    oLanguage: {
                        "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
                        "oPaginate": {
                            "sFirst": "<?php echo JrTexto::_("Next page") ?>", // This is the link to the first page
                            "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                            "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                            "sLast": "<?php echo JrTexto::_("Previous page") ?>" // This is the link to the last page
                        }
                    }
                });
            } else if (vistareporte == 'area_char') {
                var divid = 'grafico' + idnumgrafico;
                var div = '<div id="' + divid + '" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                    'legend': 'none',
                    'is3D': true,
                    height: 560,
                    title: '<?php echo ucfirst(JrTexto::_("Progreso de avance")); ?>',
                    hAxis: {
                        title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',
                        titleTextStyle: {
                            color: '#333'
                        },
                        textStyle: {
                            fontSize: 12
                        }
                    },
                    vAxis: {
                        title: '<?php echo ucfirst(JrTexto::_('Progreso')); ?>'
                    },
                    vAxis: {
                        minValue: 0
                    }
                };
                google.setOnLoadCallback(charst('area', dt, options, divid));
            } else if (vistareporte == 'bar_char') {
                var divid = 'grafico' + idnumgrafico;
                var div = '<div id="' + divid + '" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                    'legend': 'none',
                    'is3D': true,
                    height: 560,
                    orientation: 'horizontal',
                    title: '<?php echo ucfirst(JrTexto::_("Progreso de avance")); ?>',
                    hAxis: {
                        title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',
                        titleTextStyle: {
                            color: '#333'
                        },
                        textStyle: {
                            fontSize: 12
                        }
                    },
                    vAxis: {
                        title: '<?php echo ucfirst(JrTexto::_('Progreso')); ?>'
                    },
                    vAxis: {
                        minValue: 0
                    }
                };
                google.setOnLoadCallback(charst('bar', dt, options, divid));
            } else if (vistareporte == 'line_char') {
                var divid = 'grafico' + idnumgrafico;
                var div = '<div id="' + divid + '" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                    'legend': 'none',
                    'is3D': true,
                    height: 560,
                    title: '<?php echo ucfirst(JrTexto::_("Progreso de avance")); ?>',
                    hAxis: {
                        title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',
                        titleTextStyle: {
                            color: '#333'
                        },
                        textStyle: {
                            fontSize: 12
                        }
                    },
                    vAxis: {
                        title: '<?php echo ucfirst(JrTexto::_('Progreso')); ?>'
                    },
                    vAxis: {
                        minValue: 0
                    }
                };
                google.setOnLoadCallback(charst('line', dt, options, divid));
            } else if (vistareporte == 'pie_char') {
                var divid = 'grafico' + idnumgrafico;
                var div = '<div id="' + divid + '" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                    'legend': 'none',
                    'is3D': true,
                    height: 560,
                    title: '<?php echo ucfirst(JrTexto::_("Progreso de avance")); ?>',
                    hAxis: {
                        title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',
                        titleTextStyle: {
                            color: '#333'
                        },
                        textStyle: {
                            fontSize: 12
                        }
                    },
                    vAxis: {
                        title: '<?php echo ucfirst(JrTexto::_('Progreso')); ?>'
                    },
                    vAxis: {
                        minValue: 0
                    }
                };
                google.setOnLoadCallback(charst('pie', dt, options, divid));
            } else {
                table = '';
                $('#pnl-panelreporte').html(table);
            }
            vistaimpresion();
            //console.log(vistareporte,dt);
        }

        var obtnerresultado = function() {
            var rtpalumno = localStorage.getItem('rtpalumno');
            if (rtpalumno == 'ver_rpt' + verrptalumno) return mostrarresultado();
            localStorage.setItem('rtpalumno', 'ver_rpt' + verrptalumno);
            var data = new FormData()
            data.append('idalumno', $('#idalumno').val());
            var idcurso = $('select#idcurso').val() || '';
            var vcursos = [];
            if (idcurso.length > 1) {
                data.append('varioscursos', true);
            } else {
                vcursos = idcurso;
                if (vcursos == undefined) return false;
            }
            data.append('idgrupoauladetalle', idcurso);
            oMine.showLoading();
            __sysAyax({
                fromdata: data,
                url: _sysUrlBase_ + 'json/reportes/progreso',
                callback: function(rs) {
                    oMine.hideLoading();
                    if (rs.code == 200&&rs.data.length>0) {
                        localStorage.setItem('dataprogreso', JSON.stringify(rs.data));
                        enableBtn();
                        $('.changevista[vista="table"]').trigger('click');
                        mostrarresultado();
                    }else{
                        clearRpt();
                        disableBtn();
                    }
                },
                callbackerror: () => {
                    clearRpt();
                }
            });
        }

        $('.changevista').click(function(ev) {
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            localStorage.setItem('vistaReporte1', $(this).attr('vista'));
            mostrarresultado();
        })
        $('#pnl-panelreporte').on('change', 'select#changepuntaje', function(ev) {
            ev.preventDefault();
            _this = $(this);
            var val = _this.val();
            var td = $('#pnl-panelreporte table td.shownota');
            $.each(td, function(i, vv) {
                console.log($(vv));
                var c = parseFloat(parseFloat($(this).attr('nota')) * parseFloat(val) / parseFloat($(this).attr('de'))).toFixed(2);
                $(this).children('h3').text(c);
            })
            localStorage.setItem('notabase', val);
            vistaimpresion();
            //ev.stopPropagation();
        })

        $('select#idcurso').change(function(ev) {
            verrptalumno++;
        })

        $('select#idcurso').on('hide.bs.select', function(e, clickedIndex, isSelected, previousValue) {
            obtnerresultado();
        });

        $('.btnexportar_xlsx').click(function(ev) {
            setTimeout(function() {
                $('#impresion2').find('.buttons-excel').trigger('click');
            }, 1500);
        })
        $('.btnexportar_xlsx2').click(function(ev) {
            $('#impresion .dt-buttons').find('.buttons-excel').trigger('click');
        })
        $('.btnexportar_pdf').click(function(ev) {
            var vistareporte = localStorage.getItem('vistaReporte1') || $('.changevista.active').attr('vista');
            if (vistareporte == 'table') {
                setTimeout(function() {
                    $('#impresion2').find('.buttons-pdf').trigger('click');
                }, 1500);
            } else {
                const filename = 'report_' + vistareporte + '.pdf';
                $('#pnl-panelreporte').children('.col-md-3').hide();
                html2canvas(document.querySelector('#pnl-panelreporte'), {
                    scale: 1
                }).then(canvas => {
                    let pdf = new jsPDF({
                        orientation: 'landscape'
                    }); //
                    pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 320, 180);
                    pdf.save(filename);
                    $('#pnl-panelreporte').children('.col-md-3').show();
                });
            }
        });
        $('.btnexportar_pdf2').click(function(ev) {
            $('#impresion .dt-buttons').find('.buttons-pdf').trigger('click');
        });
        $('.btnimprimir').click(function(ev) {
            if (vistareporte == 'table') {
                setTimeout(function() {
                    $('#impresion2').find('.buttons-print').trigger('click');
                }, 1500);
            } else {
                $('#pnl-panelreporte').children('.col-md-3').hide(0).css('display', 'none');
                $('#pnl-panelreporte').imprimir();
                $('#pnl-panelreporte').children('.col-md-3').show(0).css('display', 'inline-block');
            }
        });
        $('.btnimprimir_2').click(function(ev) {
            $('#impresion .dt-buttons').find('.buttons-print').trigger('click');
        });
        $('#tableimprimir').DataTable({
            ordering: false,
            searching: false,
            paging: false,
            dom: 'Bfrtip',
            buttons: [{
                extend: "pdfHtml5",
                messageTop: '',
                title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('Course notes') . " - " . date('d-m-Y'); ?>',
                customize: function(doc) {
                    doc.pageMargins = [12, 12, 12, 12];
                    doc.content[1].table.widths = ["*", "*", "*"];
                    var tblBody = doc.content[1].table.body;
                    $('#tableimprimir tbody').find('tr').each(function(ir, row) {
                        if ($(this).hasClass('nomexamen')) {
                            $(this).find('td').each(function(ic, td) {
                                tblBody[ir][ic].fillColor = '#e4e1e1';
                            })
                        }
                    })
                }
            }, {
                extend: "excelHtml5",
                messageTop: '',
                title: '<?php echo JrTexto::_('Report') ?>',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var col = ['A', 'B', 'C', 'D', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                    $('#tableimprimir tbody').find('tr').each(function(ir, row) {
                        if ($(this).hasClass('nomexamen')) {
                            $(this).find('td').each(function(ic, td) {
                                var xcol = col[ic];
                                $('row c[r="' + xcol + (ir + 1) + '"]', sheet).attr('s', 32);
                                //tblBody[ir][ic].fillColor='#17a2b8';
                            })
                        }
                    })
                    // console.log(sheet);
                }
            }]
        });
        $('#pnl-panelreporte .dt-buttons').hide();
        $('#impresion .dt-buttons').hide();
        //$('#impresion').hide();
        verrptalumno++;
        vistareporte = localStorage.getItem('vistaReporte1') || 'table';
        obtnerresultado();
        // setTimeout(function() {
        //     $('.changevista[vista="' + vistareporte + '"]').trigger('click')
        // }, 2000);









        /*exportfile();
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawchart);
	var idnumgrafico=0;
	
	tablaprogresocursos = $('#tableReport').DataTable(
    { 
    	"searching": false,
		"processing": false,
		//"serverSide": true,
		"columns" : [
			{'data': '#'},        
			{'data': '<?php echo JrTexto::_("Courses") ?>'},
			{'data': '<?php echo JrTexto::_("Progress") ?>'},
		],
		"ajax":{
		url:_sysUrlBase_+'json/reportes/progreso',
		type: "post",                
		data:function(d){
		    d.json=true
		    //  d.tipo=$('#cbtiporepresentante').val(),
		    //  d.idpais=$('#cbpais').val()
		    d.idcurso = $('#idcurso').val()

		    draw5d7a9d14d8a18=d.draw;
		   // console.log(d);
		},
		dataSrc:function(json){
			var data=json.data;             
			json.draw = draw5d7a9d14d8a18;
			json.recordsTotal = json.data.length;
			json.recordsFiltered = json.data.length;
			var datainfo = new Array();
			if(Object.keys(data).length > 0){
				resultAsync = data;
				pintarGrafico1();
				pintarGrafico2();
				pintarGrafico3();
			}
			for(var i=0;i< data.length; i++){
				datainfo.push({             
				  '#':(i+1),
				  '<?php echo JrTexto::_("Courses") ?>' : data[i].nombre,
				  '<?php echo JrTexto::_("Progress") ?>' : data[i].progreso +"% / 100%"
				});
			}
			return datainfo }, error: function(d){console.log(d)}
		},"language": { "url": _sysIdioma_.toLowerCase()=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });
    
    $('body').on('click','.changevista',function(){
    	var vista = $(this).attr('vista');
    	$(this).parent().find('.active').removeClass('active btn-primary').addClass('btn-secondary');
    	$(this).addClass('active btn-primary').removeClass('btn-secondary');
    	$('.active_report').hide().removeClass('active_report');
    	$('#'+vista).show().addClass('active_report');
    });
    $('#idcurso').on('change',function(){
    	refresDatetable();
    });
    $('body').on('click','.btnexportar_xlsx',function(ev){
        setTimeout(function(){
        	$('#impresion').find('.buttons-excel').trigger('click');
        },1500);
    });
    $('body').on('click','.btnexportar_pdf',function(ev){
       
            setTimeout(function(){
            	$('#impresion').find('.buttons-pdf').trigger('click');
            },1500);
       
    });
    $('body').on('click','.btnimprimir',function(ev){
        setTimeout(function(){
        	$('#impresion').find('.buttons-print').trigger('click');
        },1500);
    });
});

function refresDatetable(){
	tablaprogresocursosExport.ajax.reload();
    tablaprogresocursos.ajax.reload();
}

function drawchart(type,dt,strelement,options = null){
	var data = new google.visualization.DataTable();
	var chart = null;
	var DOMobject = document.querySelector(strelement);

	data.addColumn('string', '<?php echo ucfirst(JrTexto::_('Courses')); ?>');
	data.addColumn('number', '<?php echo ucfirst(JrTexto::_('Progress')); ?>');
	if(dt.length > 0){
		for(var val of dt){
			data.addRow([val.nombre,val.progreso]);
		}
	}
	if(options == null){
		options = {'title':'<?php echo JrTexto::_("My progress") ?>', 'width':400, 'height':300};
	}
	switch(type){
		case 'area':{ 
			chart = new google.visualization.AreaChart(DOMobject);
		}
		break;
		case 'line':{ 
			chart = new google.visualization.LineChart(DOMobject);
		}
		break;
		case 'pie':{ 
			chart = new google.visualization.PieChart(DOMobject);
		}
		break;
		default: {
			chart = new google.visualization.BarChart(DOMobject);
		}
	}
    chart.draw(data, options);	
}

function pintarGrafico1(){
	var _width = $('#report_grafico_1').width();
	var options = {
        'legend':'none',
        'is3D':true,                        
        height:560,
        width: _width,
        title: '<?php echo ucfirst(JrTexto::_("My Progress")); ?>',
        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Progress')); ?>'},
        vAxis: {minValue: 0,maxValue:100}
    };
    google.setOnLoadCallback(drawchart('area',resultAsync,'#chart_g1',options));
}
function pintarGrafico2(){
	var _width = $('#report_grafico_1').width();
	var options = {
        'is3D':true,                        
        height:560,
        width: _width,
        orientation:'horizontal',
        title: '<?php echo ucfirst(JrTexto::_("My Progress")); ?>',
        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Progress')); ?>'},
        vAxis: {minValue: 0,maxValue:100}
    };

    google.setOnLoadCallback(drawchart('bar',resultAsync,'#chart_g2',options));
}
function pintarGrafico3(){
	var _width = $('#report_grafico_1').width();
	var options = {
        'legend':'none',
        'is3D':true,                        
        height:560,
        width: _width,
        title: '<?php echo ucfirst(JrTexto::_("My Progress")); ?>',
        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Progress')); ?>'},
        vAxis: {minValue: 0,maxValue:100}
    };

    google.setOnLoadCallback(drawchart('line',resultAsync,'#chart_g3',options));
}

//exportar reporte en pdf, excel o para imprimir directamente, en forma de tabla.
function exportfile(){
	tablaprogresocursosExport = $('#tableimprimir').DataTable({
        ordering: false,
        searching: false,
        paging: false,
        "columns" : [
			{'data': '#'},        
			{'data': '<?php echo JrTexto::_("Courses") ?>'},
			{'data': '<?php echo JrTexto::_("Progress") ?>'},
		],
		"ajax":{
		url:_sysUrlBase_+'json/reportes/progreso',
		type: "post",                
		data:function(d){
		    d.json=true
		    //  d.tipo=$('#cbtiporepresentante').val(),
		    //  d.idpais=$('#cbpais').val()
		    d.idcurso = $('#idcurso').val()

		    draw5d7a9d14d8a18=d.draw;
		   // console.log(d);
		},
		dataSrc:function(json){
			var data=json.data;             
			json.draw = draw5d7a9d14d8a18;
			json.recordsTotal = json.data.length;
			json.recordsFiltered = json.data.length;
			var datainfo = new Array();
			for(var i=0;i< data.length; i++){
				datainfo.push({             
				  '#':(i+1),
				  '<?php echo JrTexto::_("Courses") ?>' : data[i].nombre,
				  '<?php echo JrTexto::_("Progress") ?>' : data[i].progreso +"% / 100%"
				});
			}
			return datainfo }, error: function(d){console.log(d)}
		},
        dom: 'Bfrtip',
        buttons: [
          {
            extend: "excelHtml5",
            messageTop: '',
            title:'<?php echo JrTexto::_('Report') ?>',
            customize: function(xlsx){
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                var col=['A','B','C','D','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                $tr=$('#tableimprimir2 tbody').find('tr');
                $.each($tr,function(ir, row){
                    $(this).find('td').each(function(ic,td){
                        var xcol=col[ic];
                        if(ir==0) $('row c[r="'+xcol+'1"]',sheet).attr('s',32);
                        else{
                            var s=ir%2==0?8:20;
                            $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',s);
                        }   
                    })
                })
            }
          },{
            extend: "pdfHtml5",
            messageTop: '',
            title:'<?php echo JrTexto::_('Report') . " " . JrTexto::_('my progress') . " - " . date('d-m-Y'); ?>',
            customize: function(doc){
                doc.pageMargins = [12,12,12,12];
                doc.content[1].table.widths = ["*", "*", "*"];
                var tblBody = doc.content[1].table.body;
                $tr=$('#tableimprimir2 tbody').find('tr');
                var ntr=$tr.length;
                $tr.each(function (ir, row){
                    var color=((ir%2==0)?'':'#e4e1e1');
                      $(this).find('td').each(function(ic,td){
                        if(ir==0) color='#7D94D0';                                        
                        tblBody[ir][ic].fillColor=color;                                      
                        if(ic==0)tblBody[ir][ic].alignment= 'center';
                    })
                })
                tblBody[ntr][0].alignment= 'center'; 
            }
          },{
            extend:"print",
            title: '<?php echo JrTexto::_('Report') . " " . JrTexto::_('my progress') . " - " . date('d-m-Y'); ?>',
            customize: function(win){
                var table=$(win.document.body);
                  $(win.document.body).find('th').each(function(index){
                   table.find('th').css('background-color', '#cccccc')
                });
            }
          }
        ]                  
	});*/
    })

    function clearRpt() {
        $('#pnl-panelreporte').html(' <h1 class="text-center center my-100-w"><i>No hay información</i></h1>');
    }

    function disableBtn() {
        $('#cont-btn button').attr('disabled', 'disabled');
    }

    function enableBtn() {
        $('#cont-btn button').removeAttr('disabled');
    }
</script>