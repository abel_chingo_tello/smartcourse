<?php

?>
<style>
.row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

select.select-ctrl{ padding-right: 35px; }
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.box_custom{ background-color:white; border:1px solid #4683af; border-radius:0.5em; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<div class="row" id="search_container">
    <div class="col-md-12">
        <!-- <h4 style="text-align:center; font-weight:bold;"><?php echo JrTexto::_("Select"); ?></h4> -->
        <div style="text-align:center;">
            <h4 style="display:inline-block;">Cursos</h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select class="select-docente form-control select-ctrl " id="item1">
                    <option value="A1"><?php echo JrTexto::_("A1"); ?></option>
                    <option value="A2"><?php echo JrTexto::_("A2"); ?></option>
                    <option value="B1"><?php echo JrTexto::_("B1"); ?></option>
                    <option value="B2"><?php echo JrTexto::_("B2"); ?></option>
                    <option value="C1"><?php echo JrTexto::_("C1"); ?></option>
                </select>
            </div>
            <div style="padding:5px 0;"></div>
        </div>
    </div>
    <div class="col-md-6">
    <div class="table-responsive box_custom">
            <table class="table" id="tabla01">
                <thead>
                    <th><?php echo $this->Entidad; ?></th>
                    <th>Listening</th>
                    <th>Reading</th>
                    <th>Writing</th>
                    <th>Speaking</th>
                </thead>
                <tbody>
                    <?php
                        if(!empty($this->RowTabla)){
                            foreach($this->RowTabla as $r){
                                echo "<tr>";
                                echo "<td>{$r['nombre']}</td>";
                                echo "<td>{$r['A1']['4']}%</td>";
                                echo "<td>{$r['A1']['5']}%</td>";
                                echo "<td>{$r['A1']['6']}%</td>";
                                echo "<td>{$r['A1']['7']}%</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 box_custom">
        <h5 style="text-align:center; font-weight:bold;">Progreso por habilidades</h5>
        <div class="chart-container" id="Container_progreso_habilidad" style="position: relative; margin:0 auto; height:100%; width:100%">
            <canvas id="Chart_progreso_habilidad"></canvas>
        </div>
        <div style="text-align:center;">
            <button type="button" id="previousChart" class="btn btn-default btn-circle"><i class="fa fa-angle-left"></i></button>
            <div class="panel panel-default" style="display:inline-block;">
                <div class="panel-body"><span id="pag-text">1 / 1</span></div>
            </div>
            <button type="button" id="nextChart" class="btn btn-default btn-circle"><i class="fa fa-angle-right"></i></button>
        </div>
    </div>
</div>

<script type="text/javascript">
var datos = <?php echo !empty($this->RowTabla) ? json_encode($this->RowTabla) : 'null' ?>;
var drawChart = function(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: false,
        text: 'Empty'
      },
      scales:{"yAxes":[{"ticks":{"beginAtZero":true}}]},
      tooltips: {
        mode: 'point',
        intersect: true
      },legend: {
            display: true
         }
    }
  });
};
function displayChart(_datos,labeles){
    var L1D = new Array(), R2D = new Array(),W1D = new Array(),S2D = new Array();
 
    if(Object.keys(_datos).length > 0 && labeles.length > 0){
        for(var j = 0; j < labeles.length; j++){
            L1D.push(_datos[j]['4']);
            R2D.push(_datos[j]['5']);
            W1D.push(_datos[j]['6']);
            S2D.push(_datos[j]['7']);
        }
    }
    var chartData = {
        labels: labeles,
        datasets: [ {
            label:'Listening',
            backgroundColor: 'rgba(63,81,181,0.5)',
            data: L1D,
            borderColor: 'white',
            borderWidth: 2
        },{
            label:'Reading',
            backgroundColor: 'rgba(233,30,99,0.5)',
            data: R2D,
            borderColor: 'white',
            borderWidth: 2
        },{
            label:'Writing',
            backgroundColor: 'rgba(255,193,7,0.5)',
            data: W1D,
            borderColor: 'white',
            borderWidth: 2
        },{
            label:'Speaking',
            backgroundColor: 'rgba(156,39,176,0.5)',
            data: S2D,
            borderColor: 'white',
            borderWidth: 2
        }]

    };
    $("#Container_progreso_habilidad").html(" ").html('<canvas id="Chart_progreso_habilidad"></canvas>');
    drawChart('Chart_progreso_habilidad',chartData);
}
$('document').ready(function(){
    $('#search_container').parent().css('margin','5px 0');
    $('#search_container').parent().css('width','100%');
    $('#tabla01').DataTable();
    // Check if is 5 limit of the rows
    var labeles = new Array;
    var _d = new Array;
    var paginas = 0;
    var paginaActual = 1;
    var totales = (datos != null) ? Object.keys(datos).length : 0;
    // var totales = 54;
    var max = 12;
    if(datos != null){
        if(totales <= 12){
            for(var i = 0; i < totales; i++){
                if(typeof datos[i].nombre != 'undefined'){
                    labeles.push(datos[i].nombre);
                    _d.push(datos[i][$('#item1').val()]);
                }
            }
            displayChart(_d,labeles);
        }else{
            //logica para paginar
            paginas = Math.ceil(totales/max);
            if(paginas > 1){
                $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);
                            _d.push(datos[i][$('#item1').val()]);
                        }
                    }
                    displayChart(_d,_l);
                }
                
            }//end if checker
        }
    }//end if datos null

    /**EVENTOS */
    $('#previousChart').on('click',function(){
        if(paginaActual > 1 && paginaActual <= paginas){
            --paginaActual;
            $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
            if(datos != null){
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);                            
                            _d.push(datos[i][$('#item1').val()]);
                        }
                    }
                    displayChart(_d,_l);
                }
                
            }
        }
    });
    $('#nextChart').on('click',function(){
        if(paginaActual >= 1 && paginaActual < paginas){
            ++paginaActual;
            $('#pag-text').text(paginaActual.toString()+' / '+paginas.toString());
            if(datos != null){
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);                            
                            _d.push(datos[i][$('#item1').val()]);
                        }
                    }
                    displayChart(_d,_l);
                }
                
            }
        }
    });
    $('#item1').change(function(){
        if($(this).val() != 0){
            if(datos != null){
                //table 
                var row = '';
                for(var i = 0; i < totales; i++){
                    row = row.concat('<tr><td>'+datos[i].nombre+'</td><td>'+datos[i][$(this).val()]['4']+'</td><td>'+datos[i][$(this).val()]['5']+'</td><td>'+datos[i][$(this).val()]['6']+'</td><td>'+datos[i][$(this).val()]['7']+'</td></tr>');
                }
                $('#tabla01').find('tbody').html(' ').html(row);
                //draw chart
                var cal1 = ((paginaActual-1) * max) - 1;
                if(cal1 == -1) cal1=0;
                var cal2 = ((paginaActual * max) -1);
                var dat = datos.slice(cal1,cal2);
                var _d = new Array();
                var _l = new Array();
                var count_dat = Object.keys(dat).length;
                if(count_dat > 0 ){
                    for(var i = 0; i < count_dat; i++){
                        if(typeof dat[i].nombre != 'undefined'){
                            _l.push(dat[i].nombre);                            
                            _d.push(datos[i][$('#item1').val()]);
                        }
                    }
                    displayChart(_d,_l);
                }
                
            }
        }
    });
});
</script>