<?php defined("RUTA_BASE") or die(); 
$urlreportes=$this->documento->getUrlBase()."reportes/";
?>

<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Records"); ?>'
  });
</script>

<style type="text/css">
    .bootstrap-select > .dropdown-toggle{
        border:1px solid #0070a0;
        border-radius: 0.5ex;
    }
    .bootstrap-select > .dropdown-toggle::before {
        font-family: FontAwesome;
        font-size: 1.3em;
        content: "\f0dd";
        display: inline-block;
        text-align: center;
        width: 30px;
        height: 100%;
        background-color: #0070a0;
        position: absolute;
        top: 0;
        right: 0px;
        pointer-events: none;
        color: #FFF;
        border-radius: 0px 0.3ex 0.3ex 0px;
    }
    .popover {
        max-width:none; 
    }
</style>

<input type="hidden" name="idalumno" id="idalumno" value="<?php echo $this->idalumno; ?>">
<input type="hidden" name="idrol" id="idrol" value="<?php echo @$this->idrol; ?>">
<input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$this->idproyecto; ?>">
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
          <div class="row">
              <div class="col-md-6">
                  <div class="btn-group">
                      <button type="button" class="btn btn-xs btn-success btnexportar_xlsx"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_1</button>
                      <button type="button" class="btn btn-xs btn-success btnexportar_xlsx2"><i class="fa fa-file-excel-o" title="<?php echo JrTexto::_('Export to Excel') ?>"></i> xlsx_2</button>
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_1</button>
                      <button type="button" class="btn btn-xs btn-primary btnexportar_pdf2"><i class="fa fa-file-pdf-o"  title="<?php echo JrTexto::_('Export to PDF') ?>"></i> PDF_2</button>
                      <button type="button" class="btn btn-xs btn-warning btnimprimir"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_1</button>
                      <button type="button" class="btn btn-xs btn-warning btnimprimir_2"><i class="fa fa-print" title="print"></i> <?php echo JrTexto::_('Print') ?>_2</button>
                  </div>
              </div>
              <div class="col-md-6 text-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary btn-sm" style="color: black;background-color: transparent;"><?php echo JrTexto::_('To See how') ?></button>
                      <button type="button" class="btn btn-primary btn-sm active changevista" vista="table"><i class="fa fa-table"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="area_char"><i class="fa fa-area-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="bar_char" ><i class="fa fa-bar-chart"></i></button>
                      <button type="button" class="btn btn-secondary btn-sm changevista" vista="line_char" ><i class="fa fa-line-chart"></i></button>
                  </div>
              </div>
          </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_title">
      <div class="row">
        <div class=" col-md-12 form-group">
                <select id="idcurso" name="idcurso" class="form-control selectpicker" multiple title="<?php echo JrTexto::_('Selected Courses'); ?>" data-actions-box="true" data-size="10">
                  <?php if(!empty($this->cursos))
                  foreach ($this->cursos as $fk) { ?><option selected="selected" value="<?php echo $fk["idcurso"]?>" <?php echo @$frm["idcurso"]==$fk["idcurso"]?'selected="selectted"':'';?>><?php echo $fk["strcurso"] ?></option><?php } ?>
                </select>            
            </div>           
        </div> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="row" id="pnl-panelreporte" >            
        </div>
        <div class="row" id="impresion" style="display: none;">
            <div class="table-responsive">
            </div>
        </div>
        <div id="impresion2" style="display: none;">
            <div class="table-responsive">    
            </div>
        </div>
        <div id="sindatos" style="display:none">
            <div class="container">
                <div class="row">
                    <div class="jumbotron">
                        <h1><strong>Opss!</strong></h1>                
                        <div class="disculpa"><?php echo JrTexto::_('Sorry, You do not have students enrolled')?>:</div>                
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        __logoEmpresaimagebase64();
        google.charts.load('current', {'packages':['corechart']});
        function charst(tipo,dt,options,idelem){
            var selbase=localStorage.getItem('notabase')||'';
            var bases=[];
            var strcurso=[];
            var data = new google.visualization.DataTable();
            data.addColumn('string', '<?php echo ucfirst(JrTexto::_('Courses')); ?>');
            data.addColumn('number', '<?php  echo ucfirst(JrTexto::_('notas')); ?>');
            var datos=[];        
            if(dt.length)
                $.each(dt,function(i,c){                       
                   // var formula=c.formula;
                    var puntajemax=c.puntaje.puntajemax||100;
                        if($.inArray(puntajemax,bases)==-1){
                            bases.push(puntajemax);
                            selbase=(selbase==''||selbase=='null'||selbase==null)?puntajemax:selbase;
                        }
                    if($.inArray(selbase,bases)==-1){selbase=bases[0]||100;}

                    var iex=1;                  
                    promedio=parseFloat(c.promedio).toFixed(2);
                    datos.push({idcurso:c.idcurso,nombrecurso:c.strcurso,promedio:promedio,promedioenbasea:c.promedioenbasea,promedionotastexto:c.promedionotastexto});
                    
                }) 

            localStorage.setItem('notabase',selbase);               
            if(bases.length)
                $.each(bases,function(i,v){
                    $('#notabase').append('<option values="'+v+'" '+(v==selbase?'selected="selected"':'')+'>'+v+'</option>');
                });

            if(datos.length){
                $.each(datos,function(i,v){
                    var prom=parseFloat(parseFloat(v.promedio)*parseFloat(selbase)/parseFloat(v.promedioenbasea)); 
                    data.addRow([v.nombrecurso,prom]);
                })
                options.title=options.title+' /'+selbase;

                //console.log(datos);
                if(tipo=='area') var chart = new google.visualization.AreaChart(document.getElementById(idelem));
                else if(tipo=='line')var chart = new google.visualization.LineChart(document.getElementById(idelem));
                else if(tipo=='pie')var chart = new google.visualization.PieChart(document.getElementById(idelem));
                else var chart = new google.visualization.BarChart(document.getElementById(idelem));
                chart.draw(data, options);       
            }
        }

        var verrptalumno=1;
        localStorage.setItem('rtpalumno','ver_rpt'+verrptalumno);
        var idnumgrafico=0;

        var vistaimpresion=function(){
            var selbase=localStorage.getItem('notabase')||'';                
                var dataexamenes=localStorage.getItem('dataexamenes');
                var dt=JSON.parse(dataexamenes);
                var datos=[];
                var nmaxformula=1;
                var bases=[];

                if(dt.length)
                    $.each(dt,function(i,c){
                        var puntajemax=c.puntaje.puntajemax||100;
                        if($.inArray(puntajemax,bases)==-1){
                            bases.push(puntajemax);
                            selbase=(selbase==''||selbase=='null'||selbase==null)?puntajemax:selbase;
                        } 
                        if($.inArray(selbase,bases)==-1){selbase=bases[0]||100;}                       
                        nmaxformula=c.promedios.length>nmaxformula?c.promedios.length:nmaxformula;
                        var iex=1;                  
                        promedio=parseFloat(c.promedio).toFixed(2);
                        datos.push({idcurso:c.idcurso,nombrecurso:c.strcurso,promedio:promedio,promedioenbasea:c.promedioenbasea,promedionotastexto:c.promedionotastexto,promedios:c.promedios});                        
                    }) 
                //if(nmaxformula<2) nmaxformula=3;
                var total=0;
                var tr='';
                //console.log(nmaxformula);
                var tr1='<thead><tr style="color:#ffffff; background-color:#5c5858;"><th>#</th><th ><?php echo JrTexto::_('Course') ?></th>';
                var tr2=tr1;
                for(i=0;i<nmaxformula;i++) tr2+='<th></th>';
                tr2+='<th class="text-center"><?php echo JrTexto::_('Notas'); ?>/('+selbase+')</th><th></th></tr></thead><tbody>';   
                tr1+='<th class="text-center"><?php echo JrTexto::_('Notas'); ?>/('+selbase+')</th><th></th></tr></thead><tbody>';

                if(datos.length){
                    var tmpdata=[];
                    $.each(datos,function(i,v){
                        var prom=parseFloat(parseFloat(v.promedio)*parseFloat(selbase)/v.promedioenbasea).toFixed(2);

                        tr1+='<tr><td class="text-center">'+(i+1)+'</td><td >'+v.nombrecurso+'</td><td class="text-center"><b>'+prom+'</b></td><td class="text-center">'+v.promedionotastexto+'</td></tr>';

                        tr2+='<tr><td class="text-center">'+(i+1)+'</td><td >'+v.nombrecurso+'</td>';
                        var tr='';
                        if(v.promedios.length!=undefined){
                            $.each(v.promedios,function(i,p){
                                tr+='<td>'+p.tipo+'('+p.porcentaje+'%): '+parseFloat(p.nota).toFixed(2)+'</td>';                               
                            })
                            for(i=0;i<nmaxformula-v.promedios.length;i++) tr+='<td></td>';
                        }
                        tr2=tr2+tr+'<td class="text-center"><b>'+prom+'</b></td><td class="text-center">'+v.promedionotastexto+'</td></tr>';

                        total++;
                        //tr1+='<tr ><td class="text-center">'+(i+1)+'</td><td> '+v.nombrecurso+'</td><td class="text-center">'+v.promedio+'/('+v.promedioenbasea+')</td><td class="text-center">'+v.promedionotastexto+'</td></tr>';
                       
                    }) 
                }

                tr+='<tr><td></td><td colspan="2" class="text-center"><?php echo JrTexto::_("Total of records"); ?> : '+total+'</td><td style="display:none;"></td></tr>';

                tr1='<table class="table-striped" id="tableimprimir" style="width: 100%">'+tr1+'<tr><td></td><td class="text-center"><?php echo JrTexto::_("Total of records"); ?> : '+total+'</td><td></td><td></td></tr></tbody></table>';
                tr2='<table class="table-striped" id="tableimprimir2" style="width: 100%">'+tr2+'<tr><td></td><td class="text-center"><?php echo JrTexto::_("Total of records"); ?> : '+total+'</td>';

                 for(i=0;i<nmaxformula;i++) tr2+='<td></td>';
                    tr2+='<td></td><td></td></tr></tbody></table>'; 
                $('#impresion2 .table-responsive').html(tr2);
                $('#impresion2').find('#tableimprimir2').DataTable({
                        ordering: false,
                        searching: false,
                        paging: false,
                        dom: 'Bfrtip',
                        buttons: [
                          {
                            extend: "excelHtml5",
                            messageTop: '',
                            title:'<?php echo JrTexto::_('Report') ?>',
                            customize: function(xlsx){
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                var col=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                                $tr=$('#tableimprimir2 tbody').find('tr');
                                $.each($tr,function(ir, row){
                                    $(this).find('td').each(function(ic,td){
                                        var xcol=col[ic];
                                        if(ir==0) $('row c[r="'+xcol+'1"]',sheet).attr('s',32);
                                        else{
                                            var s=ir%2==0?8:20;
                                            $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',s);
                                        }   
                                    })
                                })
                            }
                          },{
                            extend: "pdfHtml5",
                            messageTop: '',
                            title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Record')." - ".date('d-m-Y'); ?>',
                            customize: function(doc){
                                doc.pageMargins = [12,12,12,12];
                                doc.content[1].table.widths = [30, "*", "*"];
                                var tblBody = doc.content[1].table.body;
                                $tr=$('#tableimprimir2 tbody').find('tr');
                                var ntr=$tr.length;
                                $tr.each(function (ir, row){
                                    var color=((ir%2==0)?'':'#e4e1e1');
                                      $(this).find('td').each(function(ic,td){
                                        if(ir==0) color='#7D94D0';                                        
                                        tblBody[ir][ic].fillColor=color;                                      
                                        if(ic==0)tblBody[ir][ic].alignment= 'center';
                                    })
                                })
                                tblBody[ntr][0].alignment= 'center';
                                if(_logobase64!=''){
                                    doc.content.splice(0, 0, {
                                        margin: [ 0, 0, 0, 12 ],
                                        alignment: 'center',
                                        image:_logobase64
                                    });
                                }
                            }
                          },{
                            extend:"print",
                            title: '<?php echo JrTexto::_('Report')." ".JrTexto::_('Notas')." - ".date('d-m-Y'); ?>',
                            customize: function(win){
                                var body=$(win.document.body);
                                    var title=body.find('h1').first().text();
                                    if(_logobase64!=''){
                                        body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="'+_logobase64+'"/><br>'+title+'</div>');
                                    }else{
                                         body.find('h1').first().html('<div style="text-align:center; width:100%;">'+title+'</div>');
                                    }
                                    table=body.find('table');
                                    table.find('th').each(function(index){
                                       table.find('th').css('background-color', '#cccccc')
                                    });
                            }
                          }
                        ]                  
                })

                $('#impresion .table-responsive').html(tr1);
                $('#impresion').find('#tableimprimir').DataTable({
                        ordering: false,                        
                        searching: false,
                        paging: false,
                        dom: 'Bfrtip',
                        buttons: [
                          {
                            extend: "excelHtml5",
                            messageTop: '',
                            title:'<?php echo JrTexto::_('Report') ?>',
                            customize: function(xlsx){
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                var col=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                                $tr=$('#tableimprimir tbody').find('tr');
                                $.each($tr,function(ir, row){
                                    $(this).find('td').each(function(ic,td){
                                        var xcol=col[ic];
                                        if(ir==0) $('row c[r="'+xcol+'1"]',sheet).attr('s',32);
                                        else{
                                            var s=ir%2==0?8:20;
                                            $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',s);
                                        }   
                                    })
                                })
                            }
                          },{
                            extend: "pdfHtml5",
                            messageTop: '',
                            title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Notas')." - ".date('d-m-Y'); ?>',
                            customize: function(doc){
                                doc.pageMargins = [12,12,12,12];
                                $tr=$('#tableimprimir tbody').find('tr');
                                var ntr=$tr.length;
                                var ntd=$tr.eq(0).find('th').length;
                                var ncolA=[30,"*","*","*"];
                                /*for(i=0;i<ntd-2;i++){
                                    ncolA.push("*");
                                }*/
                                doc.content[1].table.widths = ncolA;
                                var tblBody = doc.content[1].table.body;
                                $tr.each(function (ir, row){
                                    var color=((ir%2==0)?'':'#e4e1e1');
                                    if($(this).hasClass('nomcursos')){
                                        $(this).find('td').each(function(ic,td){
                                            tblBody[ir][ic].fillColor='#5c5858';
                                            tblBody[ir][ic].color="#FFFFFF";  
                                        })
                                    }
                                })                                
                                tblBody[ntr][0].alignment= 'left'; 
                                if(_logobase64!=''){
                                    doc.content.splice(0, 0, {
                                        margin: [ 0, 0, 0, 12 ],
                                        alignment: 'center',
                                        image:_logobase64
                                    });
                                }
                            }

                          },{
                            extend:"print",
                            title: '<?php echo JrTexto::_('Report')." ".JrTexto::_('Notas')." - ".date('d-m-Y'); ?>',
                            customize: function(win){
                                var body=$(win.document.body);
                                    var title=body.find('h1').first().text();
                                    if(_logobase64!=''){
                                        body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="'+_logobase64+'"/><br>'+title+'</div>');
                                    }else{
                                         body.find('h1').first().html('<div style="text-align:center; width:100%;">'+title+'</div>');
                                    }
                                    table=body.find('table');
                                    table.find('th').each(function(index){
                                       table.find('th').css('background-color', '#cccccc')
                                    });
                            }
                          }
                        ]                  
                })


                $('[rel="popover"]').click(function(ev){return false}).popover({
                    container: 'body',
                    placement:'top',
                    trigger: 'hover',
                    html: true,
                    content: function () {
                        var clone = $($(this).find('table').clone(true)).show();
                        return clone;
                    }
                })              

        }
        var mostrarresultado=function(){
            var dataexamenes=localStorage.getItem('dataexamenes');
            var vistareporte=localStorage.getItem('vistaReporte1')||$('.changevista.active').attr('vista');
            var selbase=localStorage.getItem('notabase')||'';
            var dt=JSON.parse(dataexamenes);
            var bases=[];
            if(vistareporte=='table'){
               var table='<div class="container"> <div class="col-md-12 table-reponsive"><table class="table table-striped table-hover"><thead><tr class="headings"><th>#</th>';
                  table+='<th><?php echo JrTexto::_("Course") ;?></th><th class="text-center"><?php echo JrTexto::_("Nota") ;?>/<select id="changepuntaje"></select></th><th></th></tr></thead><tbody>';
                  if(dt.length)
                    $.each(dt,function(i,c){ 
                        table+='<tr><td  style="padding:0.5ex 2ex">'+(i+1)+'</td><td style="padding:0.5ex 1ex">'+c.strcurso+'</td>';
                            var puntajemax=c.puntaje.puntajemax||100;                           
                            if($.inArray(puntajemax,bases)==-1){
                                bases.push(puntajemax);
                                selbase=(selbase==''||selbase=='null'||selbase==null||selbase==100)?puntajemax:selbase;                                
                            }
                            if($.inArray(selbase,bases)==-1){selbase=bases[0]||100;}

                        var prom=parseFloat(parseFloat(c.promedio)*parseFloat(selbase)/c.promedioenbasea).toFixed(2);
                        var table2='';
                         if(c.promedios.length!=undefined){
                            if(c.promedios.length>0){
                              table2='<div class="col-md-12 table-reponsive table-sm" style="display:none"><table class="table" style="margin:0px" >';
                            
                            table2_th='<thead><tr>';
                            table2_td='<tbody><tr>';
                            $.each(c.promedios,function(i,p){
                                table2_th+='<th >'+p.tipo+'('+p.porcentaje+'%)</th>';
                                var _notadet=parseFloat(parseFloat(p.nota)*parseFloat(selbase)/100).toFixed(2);
                                table2_td+='<td class="shownota text-center" nota="'+p.nota+'" de="100"><h3 style="font-size:14px;">'+_notadet+'</h3></td>';                                
                            })
                            table2_th+='</tr></thead>';
                            table2_td+='</tr></tbody>';
                            table2+=table2_th+table2_td+'</table></div>';
                            }
                        }


                        table+='<td class="text-center shownota" nota="'+c.promedio+'" '+(table2!=''?'  rel="popover" ':'')+' de="'+c.promedioenbasea+'" ><h3 style="font-size:14px;">'+prom+'</h3>'+table2+'</td>';
                        table+='<td class="text-center">'+c.promedionotastexto+'</td></tr>';
                    })
                table+='</tbody>';
                table+='</table></div></div>';
                $('#pnl-panelreporte').html(table);
                $('[rel="popover"]').click(function(ev){return false}).popover({
                    container: 'body',
                    placement:'top',
                    trigger: 'hover',
                    html: true,
                    content: function () {
                        var clone = $($(this).find('table').clone(true)).show();
                        return clone;
                    }
                })
                $('#pnl-panelreporte').find("#changepuntaje").children().remove();                
                bases.sort((a,b)=>a-b);
                $.each(bases,function(i,v){
                    $('#pnl-panelreporte').find("#changepuntaje").append('<option values="'+v+'" '+(v==selbase?'selected="selected"':'')+'>'+v+'</option>');
                })
                $('#pnl-panelreporte').find("#changepuntaje").val(selbase).trigger('change');
                $('#pnl-panelreporte table').DataTable(
                    {
                        ordering: false,                        
                            searching: false,
                            pageLength: 50,
                            info:false,
                            oLanguage: {
                                "sLengthMenu": "<?php echo JrTexto::_("Showing") ?> _MENU_ <?php echo JrTexto::_("records") ?>",
                                "oPaginate": {
                                    "sFirst": "<?php echo JrTexto::_("Next page") ?>", // This is the link to the first page
                                    "sPrevious": "<?php echo JrTexto::_("Previous") ?>", // This is the link to the previous page
                                    "sNext": "<?php echo JrTexto::_("Next") ?>", // This is the link to the next page
                                    "sLast": "<?php echo JrTexto::_("Previous page") ?>" // This is the link to the last page
                                }
                            }
                    }
                );
            }else if(vistareporte=='area_char'){                 
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('area',dt,options,divid));
            }else if(vistareporte=='bar_char'){               
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        orientation:'horizontal',
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('bar',dt,options,divid));
            }else if(vistareporte=='line_char'){                 
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('line',dt,options,divid));      
            }else if(vistareporte=='pie_char'){                
                var divid='grafico'+idnumgrafico;
                var div='<div class="col-md-3"><label class="control-label"><?php echo JrTexto::_('Note based on');?></label><select id="notabase" name="notabase" class="form-control"></select></div><div id="'+divid+'" style="width:100%"></div>';
                $('#pnl-panelreporte').html(div);
                var options = {
                        'legend':'none',
                        'is3D':true,                        
                        height:560,
                        title: '<?php echo ucfirst(JrTexto::_("Note chart")); ?>',
                        hAxis: {title: '<?php echo ucfirst(JrTexto::_('Courses')); ?>',  titleTextStyle: {color: '#333'},textStyle:{fontSize:12}},
                        vAxis: {title: '<?php echo ucfirst(JrTexto::_('Notas')); ?>'},
                        vAxis: {minValue: 0,maxValue:selbase}
                    };
                google.setOnLoadCallback(charst('pie',dt,options,divid)); 
            }else{
                table='';
                $('#pnl-panelreporte').html(table);
            }        
            vistaimpresion();
            //console.log(vistareporte,dt);
        }

        var obtnerresultado=function(){
            var rtpalumno=localStorage.getItem('rtpalumno');
            if(rtpalumno=='ver_rpt'+verrptalumno) return mostrarresultado();
            localStorage.setItem('rtpalumno','ver_rpt'+verrptalumno);
            var data=new FormData()
            data.append('idalumno',$('#idalumno').val());
            if($('#idrol').val() != 0 && $('#idrol').val() != ''){
                data.append('idrol',$('#idrol').val());
            }
            if($('#idrol').val() != 0 && $('#idrol').val() != ''){
                data.append('idproyecto',$('#idproyecto').val());
            }
            var idcurso=$('select#idcurso').val()||'';
            var vcursos=[];
            if(idcurso.length>1){         
              data.append('varioscursos',true);
            }else{                
                vcursos=idcurso; 
                if(vcursos==undefined) return false;
            }            
            data.append('idcurso',idcurso);
            __sysAyax({ 
              fromdata:data,
                url:_sysUrlBase_+'json/reportes/notasfinales',
                callback:function(rs){
                  if(rs.code==200){
                    localStorage.setItem('dataexamenes',JSON.stringify(rs.data));
                    mostrarresultado();
                  }
                }
            });
        }

        $('.changevista').click(function(ev){
            $(this).siblings().removeClass('active btn-primary').addClass('btn-secondary');
            $(this).addClass('active btn-primary').removeClass('btn-secondary');
            localStorage.setItem('vistaReporte1',$(this).attr('vista'));
             mostrarresultado();
        })
        $('#pnl-panelreporte').on('change','select#changepuntaje',function(ev){
            ev.preventDefault();
            _this=$(this);
            var val=_this.val();
            var td=$('#pnl-panelreporte table td.shownota');
            $.each(td,function(i,vv){
                var c=parseFloat(parseFloat($(this).attr('nota'))*parseFloat(val)/parseFloat($(this).attr('de'))).toFixed(2);
                $(this).children('h3').text(c);
            })
            localStorage.setItem('notabase',val);
            vistaimpresion();
            //ev.stopPropagation();
        }).on('change','#notabase',function(ev){
            //ev.preventDefault();
            localStorage.setItem('notabase',$(this).val());
            mostrarresultado();
            //ev.stopPropagation();
            //console.log('test');
        })

        $('select#idcurso').change(function(ev){
            verrptalumno++;
        })

        $('select#idcurso').on('hide.bs.select', function (e, clickedIndex, isSelected, previousValue){
          obtnerresultado();
        });

        $('.btnexportar_xlsx').click(function(ev){
            setTimeout(function(){$('#impresion2').find('.buttons-excel').trigger('click');},1500); 
        })
        $('.btnexportar_xlsx2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-excel').trigger('click');
        })
        $('.btnexportar_pdf').click(function(ev){
            var vistareporte=localStorage.getItem('vistaReporte1')||$('.changevista.active').attr('vista');           
            if(vistareporte=='table'){                
                setTimeout(function(){$('#impresion2').find('.buttons-pdf').trigger('click');},1500);               
            }else {
                const filename  = 'report_'+vistareporte+'.pdf';
                $('#pnl-panelreporte').children('.col-md-3').hide();
                html2canvas(document.querySelector('#pnl-panelreporte'),{scale: 1}).then(canvas => {
                    let pdf = new jsPDF({orientation: 'landscape'});//
                    pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 320, 180);
                    pdf.save(filename);
                    $('#pnl-panelreporte').children('.col-md-3').show();
                });
            }
        });
        $('.btnexportar_pdf2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-pdf').trigger('click');
        });
        $('.btnimprimir').click(function(ev){
             if(vistareporte=='table'){ 
            setTimeout(function(){$('#impresion2').find('.buttons-print').trigger('click');},1500);
             }else {
                $('#pnl-panelreporte').children('.col-md-3').hide(0).css('display','none');
                $('#pnl-panelreporte').imprimir();
                $('#pnl-panelreporte').children('.col-md-3').show(0).css('display','inline-block');
            }
        });
         $('.btnimprimir_2').click(function(ev){
            $('#impresion .dt-buttons').find('.buttons-print').trigger('click');            
        });
        $('#tableimprimir').DataTable({
            ordering: false,
            searching: false,
            paging: false,
            dom: 'Bfrtip',
            buttons: [
              {
                extend: "pdfHtml5",
                messageTop: '',
                title:'<?php echo JrTexto::_('Report')." ".JrTexto::_('Course notes')." - ".date('d-m-Y'); ?>',
                customize: function(doc){
                    doc.pageMargins = [12,12,12,12];
                    doc.content[1].table.widths = ["*", "*", "*"];
                    var tblBody = doc.content[1].table.body;
                    $('#tableimprimir tbody').find('tr').each(function (ir, row){
                        if($(this).hasClass('nomexamen')){
                            $(this).find('td').each(function(ic,td){
                                tblBody[ir][ic].fillColor='#e4e1e1';
                            })
                        }
                        //var rowElt = row;
                        /*doc.content[1].layout = {
                        hLineWidth: function(i, node) {
                            return (i === 0 || i === node.table.body.length) ? 2 : 1;},
                        vLineWidth: function(i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 2 : 1;},
                        hLineColor: function(i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';},
                        vLineColor: function(i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';}
                        };*/ 
                    })                 
                }
              },{
                extend: "excelHtml5",
                messageTop: '',
                title:'<?php echo JrTexto::_('Report') ?>',
                customize: function(xlsx){
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var col=['A','B','C','D','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                    $('#tableimprimir tbody').find('tr').each(function (ir, row){
                        if($(this).hasClass('nomexamen')){
                            $(this).find('td').each(function(ic,td){
                                var xcol=col[ic];
                                $('row c[r="'+xcol+(ir+1)+'"]',sheet).attr('s',32);
                                //tblBody[ir][ic].fillColor='#17a2b8';
                            })
                        }
                    })
                   // console.log(sheet);
                }
              }
            ]
        });
        $('#pnl-panelreporte .dt-buttons').hide();
        $('#impresion .dt-buttons').hide();
        //$('#impresion').hide();
        verrptalumno++;
        vistareporte=localStorage.getItem('vistaReporte1')||'table';
        obtnerresultado();
        setTimeout(function(){$('.changevista[vista="'+vistareporte+'"]').trigger('click')},2000);
    })


</script>
