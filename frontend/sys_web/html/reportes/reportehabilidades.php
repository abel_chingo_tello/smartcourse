<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <?php if($this->documento->plantilla!='mantenimientos-out'){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Skills Report'); ?></li>
        <?php }else{?>
          <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
          <li class="active"><?php echo JrTexto::_('Certificate'); ?></li>
        <?php } ?>             
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">
         <form id="frmEstudiantes<?php echo $idgui; ?>">
          <input type="hidden" name="plt" value="blanco">
          <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
          <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">
          <input type="hidden" name="fkcbrol" value="3">
          <input type="hidden" name="idgrupoauladetalle" id="idgrupoauladetalle" value="">
          <div class="row">
                        
            <div class="col-xs-6 col-sm-4 col-md-4 form-group">
              <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
              <div class="cajaselect">
                <select  name="fkcbcurso" class="form-control fkcbcurso">
                <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                    <?php if(!empty($this->cursos))
                    foreach ($this->cursos as $fk) { ?><option value="<?php echo $fk["idcurso"]?>" ><?php echo $fk["nombre"] ?></option><?php } ?>
                </select>
              </div>
            </div>
</div>
<button id="actualuarmatricula123" class="hide">actualizar</button>
           </div>
          </form>
      </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 hide " >
      <div class="panel" id="vista<?php echo $idgui; ?>">
      </div>
    </div>
  </div>
<div class="form-view" id="ventanatabla_<?php echo $idgui; ?>" >
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="panel">        
          <div class="panel-body table-striped table-responsive">
            <!-- <div class="col-md-12 col-sm-12 col-xs-12 contenidohabilidad">
                
            </div> -->
            <table id="preguntas" class="table table-stripped" >
              <thead style="font-size: 18px;">
                <tr>
                  <th>Unidades</th>
                  <th>Listen</th>
                  <th>Read</th>
                  <th>Write</th>
                  <th>Speak</th>
                </tr>
              </thead>
              <tbody class="contenidohabilidad">


              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});
$(document).ready(function(){
  $('.fkcbcurso').on('change',function(e){
     e.preventDefault();
     var filtro = $('.fkcbcurso').val();
     // console.log(filtro)
      $.ajax({
        url: _sysUrlBase_+'/reportes/listarhabilidades/',
        type: 'POST',
        dataType: 'json',
        data: {'valor':filtro},
    }).done(function(resp) {
      var valor = resp;
      // console.log(valor.data.Ejercicios)
      var cont = 0;
      var cont1 = 0;
      var cont2 = 0;
      // var cont3 = 0;
      html = '';
      console.log(valor.data.Niveles);
      console.log(valor.data.Ejercicios);
      for (var i = 0; i < valor.data.Niveles.length; i++) {
        if (valor.data.Niveles[i].tiporecurso ==='U') {
          cont1++;
          html += '<tr>';
          html += '<td colspan="5">';
          html += '<label style="font-size: 18px;">'+'<?php echo ucfirst(JrTexto::_('Units')); ?>'+' '+cont1+': '+valor.data.Niveles[i].nombre+'</label>'; 
          html += '</td>';
          html += '</tr>';
          var cont3 = 0;
          for (var j = 0; j < valor.data.Niveles.length; j++) {
            if (valor.data.Niveles[j].tiporecurso ==='L' && valor.data.Niveles[j].idpadre === valor.data.Niveles[i].idcursodetalle) {
              cont3++;
              html += '<tr>';
              html += '<td colspan="5">';
                html += '<label style="font-size: 18px;"><?php echo ucfirst(JrTexto::_('Sessions')); ?>'+' '+cont3+': '+valor.data.Niveles[j].nombre+'<label>'; 
              html += '</td>';
              html += '</tr>';
              html += '<tr>';
              html += '<td >';
                html += '<label style="font-size: 18px;"><?php echo ucfirst(JrTexto::_('Practice')); ?></label>'; 
                // html += '<?php echo ucfirst(JrTexto::_('Sessions')); ?>'+' '+cont3+': '+valor.data.Niveles[j].nombre; 
              html += '</td>';
              var band_0=0,band_1=0,band_2=0,band_3=0;
              for (var k = 0; k < valor.data.Ejercicios.length; k++) {
                if (valor.data.Ejercicios[k].sesion===valor.data.Niveles[j].idrecurso) {
                  // console.log(valor.data.Ejercicios[k].det);
                   var cont_4 = 0;
                  for (var l = 0; l < valor.data.Ejercicios[k].det.length; l++) {
                    cont_4++;
                    if (valor.data.Ejercicios[k].det[l].tipo === 'Practice') {
                      
                      var res1 = valor.data.Ejercicios[k].det[l].idhabilidad.split("|").unique();
                      // var ordenar = [];
                      
                      for (var n = 0; n < res1.length; n++) {
                        if (res1[n]==='4') {
                          band_0++;
                        }else{  
                          if (res1[n]==='5') {
                            band_1++;
                          }else{
                            if (res1[n]==='6') {
                              band_2++;
                            }else{
                              if (res1[n]==='7') {
                                band_3++;
                              }  
                            } 
                          }
                          
                        }
                      }
                         
                    }
                  }
                }  
              } 
              html += '<td >';
              html += '<label style="font-size: 18px;">'+band_0+'<label>';
              html += '</td>';
              html += '<td >';
              html += '<label style="font-size: 18px;">'+band_1+'<label>';
              html += '</td>';
              html += '<td >';
              html += '<label style="font-size: 18px;">'+band_2+'<label>';
              html += '</td>';
              html += '<td >';
              html += '<label style="font-size: 18px;">'+band_3+'<label>';
              html += '</td>'; 
              html += '</tr>';
              
                for (var k = 0; k < valor.data.Ejercicios.length; k++) {
                  
                  // console.log(valor.data.Niveles[j]);

                  
                    // console.log(valor.data.Niveles[j]);
                    if (valor.data.Ejercicios[k].sesion===valor.data.Niveles[j].idrecurso) {
                      // console.log(valor.data.Ejercicios[k].det);
                       var cont4 = 0;
                      for (var l = 0; l < valor.data.Ejercicios[k].det.length; l++) {
                        cont4++;
                        if (valor.data.Ejercicios[k].det[l].tipo === 'Practice') {
                          
                          html += '<tr>';
                          html += '<td >';
                            html += '<?php echo ucfirst(JrTexto::_('Exercise')); ?>'+' '+cont4+': '+valor.data.Ejercicios[k].det[l].titulo;
                            // html += '<?php echo ucfirst(JrTexto::_('Sessions')); ?>'+' '+cont3+': '+valor.data.Niveles[j].nombre; 
                          html += '</td>';
                          var res = valor.data.Ejercicios[k].det[l].idhabilidad.split("|").unique();
                          // var ordenar = [];
                          var band=0,band1=0,band2=0,band3=0;
                          for (var m = 0; m < res.length; m++) {
                            if (res[m]==='4') {
                              band=1;
                            }else{  
                              if (res[m]==='5') {
                                band1=1;
                              }else{
                                if (res[m]==='6') {
                                  band2=1;
                                }else{
                                  if (res[m]==='7') {
                                    band3=1;
                                  }  
                                } 
                              }
                              
                            }
                          }
                          if (band==1) {
                              html += '<td >';
                              html += '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>';
                              html += '</td>';
                            }else{
                              html += '<td >';
                              html += '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                              html += '</td>';
                            }
                            if (band1==1) {
                              html += '<td >';
                              html += '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>';
                              html += '</td>';
                            }else{
                              html += '<td >';
                              html += '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                              html += '</td>';
                            }
                            if (band2==1) {
                              html += '<td >';
                              html += '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>';
                              html += '</td>';
                            }else{
                              html += '<td >';
                              html += '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                              html += '</td>';
                            }
                            if (band3==1) {
                              html += '<td >';
                              html += '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>';
                              html += '</td>';
                            }else{
                              html += '<td >';
                              html += '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                              html += '</td>';
                            }

                          
                          html += '</tr>';
                          // console.log(valor.data.Ejercicios[k].det[l]);
                        }
                      }
                      
                      // cont4++;
                        // html += '<label>'+cont4+'</label>'; 
                    }  
                }  
                
                // html += '<?php echo ucfirst(JrTexto::_('Sessions')); ?>'+' '+cont3+': '+valor.data.Niveles[j].nombre; 
              html += '</td>';
              
              html += '<tr>';
              html += '<td >';
                html += '<label style="font-size: 18px;"><?php echo ucfirst(JrTexto::_('Do it by yourself')); ?></label>'; 
                // html += '<?php echo ucfirst(JrTexto::_('Sessions')); ?>'+' '+cont3+': '+valor.data.Niveles[j].nombre; 
              html += '</td>';
              var band_0=0,band_1=0,band_2=0,band_3=0;
              for (var k = 0; k < valor.data.Ejercicios.length; k++) {
                if (valor.data.Ejercicios[k].sesion===valor.data.Niveles[j].idrecurso) {
                  // console.log(valor.data.Ejercicios[k].det);
                   var cont_4 = 0;
                  for (var l = 0; l < valor.data.Ejercicios[k].det.length; l++) {
                    cont_4++;
                    if (valor.data.Ejercicios[k].det[l].tipo === 'Do it by yourself') {
                      
                      var res1 = valor.data.Ejercicios[k].det[l].idhabilidad.split("|").unique();
                      // var ordenar = [];
                      
                      for (var n = 0; n < res1.length; n++) {
                        if (res1[n]==='4') {
                          band_0++;
                        }else{  
                          if (res1[n]==='5') {
                            band_1++;
                          }else{
                            if (res1[n]==='6') {
                              band_2++;
                            }else{
                              if (res1[n]==='7') {
                                band_3++;
                              }  
                            } 
                          }
                          
                        }
                      }
                         
                    }
                  }
                }  
              } 
              html += '<td >';
              html += '<label style="font-size: 18px;">'+band_0+'<label>';
              html += '</td>';
              html += '<td >';
              html += '<label style="font-size: 18px;">'+band_1+'<label>';
              html += '</td>';
              html += '<td >';
              html += '<label style="font-size: 18px;">'+band_2+'<label>';
              html += '</td>';
              html += '<td >';
              html += '<label style="font-size: 18px;">'+band_3+'<label>';
              html += '</td>'; 
              html += '</tr>';
              for (var k = 0; k < valor.data.Ejercicios.length; k++) {
                  
                  // console.log(valor.data.Niveles[j]);

                  
                    // console.log(valor.data.Niveles[j]);
                    if (valor.data.Ejercicios[k].sesion===valor.data.Niveles[j].idrecurso) {
                      // console.log(valor.data.Ejercicios[k].det);
                       var cont4 = 0;
                      for (var l = 0; l < valor.data.Ejercicios[k].det.length; l++) {
                        cont4++;
                        if (valor.data.Ejercicios[k].det[l].tipo === 'Do it by yourself') {
                          
                          html += '<tr>';
                          html += '<td >';
                            html += '<?php echo ucfirst(JrTexto::_('Exercise')); ?>'+' '+cont4+': '+valor.data.Ejercicios[k].det[l].titulo;
                            // html += '<?php echo ucfirst(JrTexto::_('Sessions')); ?>'+' '+cont3+': '+valor.data.Niveles[j].nombre; 
                          html += '</td>';
                          var res = valor.data.Ejercicios[k].det[l].idhabilidad.split("|").unique();
                          // var ordenar = [];
                          var band=0,band1=0,band2=0,band3=0;
                          for (var m = 0; m < res.length; m++) {
                            if (res[m]==='4') {
                              band=1;
                            }else{  
                              if (res[m]==='5') {
                                band1=1;
                              }else{
                                if (res[m]==='6') {
                                  band2=1;
                                }else{
                                  if (res[m]==='7') {
                                    band3=1;
                                  }  
                                } 
                              }
                              
                            }
                          }
                          if (band==1) {
                              html += '<td >';
                              html += '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>';
                              html += '</td>';
                            }else{
                              html += '<td >';
                              html += '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                              html += '</td>';
                            }
                            if (band1==1) {
                              html += '<td >';
                              html += '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>';
                              html += '</td>';
                            }else{
                              html += '<td >';
                              html += '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                              html += '</td>';
                            }
                            if (band2==1) {
                              html += '<td >';
                              html += '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>';
                              html += '</td>';
                            }else{
                              html += '<td >';
                              html += '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                              html += '</td>';
                            }
                            if (band3==1) {
                              html += '<td >';
                              html += '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>';
                              html += '</td>';
                            }else{
                              html += '<td >';
                              html += '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                              html += '</td>';
                            }
                          html += '</tr>';
                          // console.log(valor.data.Ejercicios[k].det[l]);
                        }
                      }
                      
                      // cont4++;
                        // html += '<label>'+cont4+'</label>'; 
                    }  
                }  
            }
          }
          
          // html += '</tr>';
        } 
      }
      $('.contenidohabilidad').html(html);
      // for()
    });
  });
  $('.fkcbcurso').trigger('change');
});
</script> 