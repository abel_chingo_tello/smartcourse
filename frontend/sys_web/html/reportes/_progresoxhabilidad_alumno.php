<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal): ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Progress by Skills and Competition"); ?>'
        });
        const __usuario = <?= json_encode($this->user) ?>
    </script>
<?php endif; ?>
<style>
    #barListening { background-color:#ffbb04; color:black; }
    #barReading { background-color:#c42d1b; }
    #barWriting { background-color:#d9b679; }
    #barSpeaking { background-color:#2196f3; }
    label { font-size: 1.4em; }
    .form-control, .input-group-addon{ border:1px solid #98aab7; border-radius:0.4em; }
    .tab-pane { border: 2px solid #e9ecef; background: #eff2f5;}
    .nav-link.active { border-color: #98aab7!important;}
    .circleProgressBar1 div:first-child{ margin:0 auto; }
</style>
<div class="container my-font-all">
    <!--MAIN SELECT-->
    <div class="row mb-3">
        <div class="col-md-3 col-sm-4">
            <label><?=JrTexto::_("Courses");?></label>
            <select class="form-control" id="slc_cursos">
            </select>
        </div>
        <div class="col-md-3 col-sm-4">
            <label><?php echo JrTexto::_('By skills') ?>:</label>
            <select class="form-control" id="slc_tipohabilidad">
                <option value="1"><?php echo JrTexto::_('Exams'); ?></option>
                <option value="2"><?php echo JrTexto::_('Practice');?></option>
            </select>
        </div>
    </div>
    <!--TAB NAV-->
    <div>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" id="tab_skill" data-toggle="tab" href="#skill"><?=JrTexto::_("Progress by Skill");?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab_competition" data-toggle="tab" href="#competition"><?=JrTexto::_("Progress by Competition");?></a>
            </li>
        </ul>
    </div>
    <!--END TAB NAV-->
    <!--START TAB CONTENT-->
    <div class="tab-content" id="tab_reportes">
        <!--START REPORT BY SKILL-->
        <div class="tab-pane fade show active p-2" id="skill" role="tabpanel" aria-labelledby="home-tab">
            <h1 class="title-graf"><?php echo JrTexto::_('Skills'); ?></h1>
            
            <div class="row my-shadow my-card graf-cont mb-3">
                <div class="col-md-6 col-sm-6">
                    <!--START CHART GENERAL-->
                    <div id="chart_container_general" style="position: relative; margin:0 auto; ; width:100%;max-width:500px">
                        <canvas id="chart_general"></canvas>
                    </div>
                    <!--END CHART GENERAL-->
                </div>
                <div class="col-md-6 col-sm-6">
                    <!--START PROGRESS BAR HABILITY-->
                    <div class="row m-0">
                        <div class="col-sm-12 pb-3">
                            <h5 class="p-1"><?php echo JrTexto::_('Listening');?></h5>
                            <div class="progress">
                                <div class="progress-bar" id="barListening" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                            </div>
                        </div>
                        <div class="col-sm-12 pb-3">
                            <h5 class="p-1"><?php echo JrTexto::_('Reading');?></h5>
                            <div class="progress">
                                <div class="progress-bar" id="barReading" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                            </div>
                        </div>
                        <div class="col-sm-12 pb-3">
                            <h5 class="p-1"><?php echo JrTexto::_('Writing');?></h5>
                            <div class="progress">
                                <div class="progress-bar" id="barWriting" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                            </div>
                        </div>
                        <div class="col-sm-12 pb-3">
                            <h5 class="p-1"><?php echo JrTexto::_('Speaking');?></h5>
                            <div class="progress">
                                <div class="progress-bar" id="barSpeaking" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                            </div>
                        </div>
                    </div>
                    <!--END PROGRESS BAR HABILITY-->
                </div>
            </div>
            <!--START COMPARE SKILL-->
            <h1 class="title-graf"><?php echo JrTexto::_('Compare skills'); ?></h1>
            <div class="row my-shadow my-card graf-cont mb-3">
                <div class="col-auto">
                    <label id="lbl_unidad"><?php echo JrTexto::_('Unit'); ?></label>
                    <select class="form-control" id="slc_unidad">
                        <option value="0"><?php echo JrTexto::_('Unit test'); ?></option>
                    </select>
                </div>
                <div class="col-auto d-none">
                    <label><?php echo JrTexto::_('Session'); ?></label>
                    <select class="form-control" id="slc_sesion">
                        <option value="0"><?php echo JrTexto::_('All') ?></option>
                    </select>
                </div>
            </div>
            <div class="row my-shadow my-card graf-cont mb-3">
                <div class="col-sm-6">
                    <h4><?php echo JrTexto::_('Skills per unit'); ?></h4>
                    <!--CHART Comparison-->
                    <div class="row m-0">
                        <div class="col-sm-12">
                            <div class="chart-container" style="position: relative; margin:0 auto; width:100%">
                                <canvas id="radar1" width="506" height="253" style="margin-bottom:5px;"></canvas>
                            </div>
                        </div>
                        <div class="col-sm-3" style="text-align:center;">
                            <h4><?php echo JrTexto::_('Listening');?></h4>
                            <div id="barListeningC1" class="circleProgressBar1" style="font-size:0.5em;"></div>
                        </div>
                        <div class="col-sm-3" style="text-align:center;">
                            <h4><?php echo JrTexto::_('Reading');?></h4>
                            <div id="barReadingC1" class="circleProgressBar1" style="font-size:0.5em;"></div>
                        </div>
                        <div class="col-sm-3" style="text-align:center;">
                            <h4><?php echo JrTexto::_('Writing');?></h4>
                            <div id="barWritingC1" class="circleProgressBar1" style="font-size:0.5em;"></div>
                        </div>
                        <div class="col-sm-3" style="text-align:center;">
                            <h4><?php echo JrTexto::_('Speaking');?></h4>
                            <div id="barSpeakingC1" class="circleProgressBar1" style="font-size:0.5em;"></div>
                        </div>
                    </div>
                    <!--END CHART Comparison-->

                </div>
                <div class="col-sm-6">
                    <h4><?php echo JrTexto::_('Skills per level'); ?></h4>
                    <!--CHART CURRENT-->
                    <div class="row m-0">
                        <div class="col-sm-12">
                            <div class="chart-container" style="position: relative; margin:0 auto;  width:100%">
                                <canvas id="radar2" width="506" height="253" style="margin-bottom:5px;"></canvas>
                            </div>
                        </div>
                        <div class="col-sm-3" style="text-align:center;">
                            <h4><?php echo JrTexto::_('Listening');?></h4>
                            <div id="barListeningC2" class="circleProgressBar1" style="font-size:0.5em;"></div>
                        </div>
                        <div class="col-sm-3" style="text-align:center;">
                            <h4><?php echo JrTexto::_('Reading');?></h4>
                            <div id="barReadingC2" class="circleProgressBar1" style="font-size:0.5em;"></div>
                        </div>
                        <div class="col-sm-3" style="text-align:center;">
                            <h4><?php echo JrTexto::_('Writing');?></h4>
                            <div id="barWritingC2" class="circleProgressBar1" style="font-size:0.5em;"></div>
                        </div>
                        <div class="col-sm-3" style="text-align:center;">
                            <h4><?php echo JrTexto::_('Speaking');?></h4>
                            <div id="barSpeakingC2" class="circleProgressBar1" style="font-size:0.5em;"></div>
                        </div>
                    </div>
                </div>
                <!--END CHART CURRENT-->

            </div>
            <!--END COMPARE SKILL-->

        </div>
        <!--END REPORT BY SKILL-->
        <!--START REPORT BY COMPETITION-->
        <div class="tab-pane fade" id="competition" role="tabpanel" aria-labelledby="profile-tab">
            <h1 class="title-graf mb-3"><?php echo JrTexto::_('Competencies'); ?></h1>
            <!--START COMPETENCIAS BAR--> 
            <div class="row my-card graf-cont mb-3">
                <!--START COMPETENCIAS-->
                <div class="col-sm-6">
                    <div id="container-competencias"> <!--Dynamic Content--> </div>
                </div>
                <!--END COMPETENCIAS-->
            </div>
            <!--END COMPETENCIAS BAR--> 
            <!--START CAPACIDADES-->
            <!-- <div class="row">
                <div class="col-sm-12">
                    <label>Competencia</label>
                    <select class="form-control" id="slc_competencia">
                        <option value="0">Seleccionar</option>
                    </select>
                </div>
            </div> -->
            <!--END CAPACIDADES-->
        </div>
        <!--END REPORT BY COMPETITION-->
    </div>
    <!--END TAB CONTENT-->
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>