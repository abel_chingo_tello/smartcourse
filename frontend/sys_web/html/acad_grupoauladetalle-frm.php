<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php //echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/acad_grupoauladetalle">&nbsp;<?php echo JrTexto::_('Acad_grupoauladetalle'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
  .pd2{margin-bottom: 1ex;}
  .bootstrap-select > .dropdown-toggle{
    border-color: rgb(70, 131, 175);
    border-radius: 0.5ex;
  }

  .bootstrap-select > .dropdown-toggle::before {
    font-family: FontAwesome;
    font-size: 1.3em;
    content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #0070a0;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: #FFF;
    border-radius: 0px 0.3ex 0.3ex 0px;
  }
  .bootstrap-timepicker-widget.dropdown-menu.open {
    display: inline-block;
    z-index:1151;
  }
  .swal-container {
        z-index: 20000;
      }
</style>


<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_grupoauladetalle" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idgrupoauladetalle" id="idgrupoauladetalle" value="<?php echo $this->pk;?>">
          <input type="hidden"  id="txtNombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Grupo aula');?> <span class="required"> * </span></label>
              <div class="select-ctrl-wrapper select-azul">
                <select name="idgrupoaula" id="idgrupoaula" class="form-control select-ctrl" >
                    <?php 
                    if(!empty($this->gruposaula))
                    foreach ($this->gruposaula as $r) { ?>
                      <option value="<?php echo $r["idgrupoaula"]?>" <?php echo $this->idgrupoaula==$r["idgrupoaula"]?'selected="selectted"':'';?> idgrado="<?php echo $r["idgrado"]; ?>" idseccion="<?php echo $r["idsesion"]; ?>"><?php echo $r["nombre"] ?></option>
                     <?php } ?>
                </select>
              </div>             
            </div>
           <div class="form-group">
            <label class="control-label"><?php echo JrTexto::_('Course');?> <span class="required"> * </span></label>
            <!--div class="cajaselect" style="border: 1px solid #4683af !important"-->
              <select id="idcurso" name="idcurso" class="form-control selectpicker" multiple title="<?php echo JrTexto::_('Selected Courses'); ?>" data-size="10">
                  <?php if(!empty($this->fkcursos))
                  foreach ($this->fkcursos as $fk) { ?><option value="<?php echo $fk["idcurso"]?>" <?php echo @$frm["idcurso"]==$fk["idcurso"]?'selected="selectted"':'';?>><?php echo $fk["nombre"] ?></option><?php } ?>
              </select>
            <!--/div-->
          </div> 

          <div class="form-group">
            <label><?php echo  ucfirst(JrTexto::_("Teacher"))?></label>
            <div class="input-group">
              <input type="hidden" name="iddocente" id="idpersona" value="<?php echo @$frm["iddocente"]; ?>" >
              <input type="text" name="dni" id="dni" class="form-control border0" value="<?php echo @$this->docente["dni"]; ?>" placeholder="<?php echo  ucfirst(JrTexto::_("N° Doc."))?>" style="max-width: 150px">
              <input type="text" name="personanombre" id="personanombre" disabled class="disabled form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Teacher Name"))?>">
              <span class="input-group-addon btn btnbuscarteacher" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Search'); ?>"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
            </div>
            <small id="emailHelp" class="form-text text-muted" ><?php echo JrTexto::_('Enter N° Doc and enter to search, or press the search button to see the list of teachers'); ?></small>
          </div>
          <div class="row">
            <div class="form-group col-md-6 col-sm-12 tipoempresa1">
              <label class="control-label"><?php echo JrTexto::_('Dre');?> <span class="required"> * </span></label>
              <div class="cajaselect">
                <select name="iddre" id="iddre" class="form-control" iddre='<?php echo @$this->iddre;?>'>
                  <option value="0"><?php echo JrTexto::_('Only') ?></option>
                </select>
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-12 tipoempresa1">
              <label class="control-label"><?php echo JrTexto::_('Ugel');?> <span class="required"> * </span></label>
              <div class="cajaselect">
                <select name="idugel" id="idugel" class="form-control" idugel='<?php echo @$frm["idugel"]; ?>'>
                  <option value="0"><?php echo JrTexto::_('Only') ?></option>
                </select>
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-12 tipoempresa1">
              <label class="control-label"><?php echo JrTexto::_('Local');?> <span class="required"> * </span></label>
              <div class="cajaselect">
                <select name="idlocal" id="idlocal" class="form-control" idlocal="<?php echo @$frm["idlocal"]; ?>">
                  <option value="0"><?php echo JrTexto::_('Only') ?></option>
                    <?php 
                    if(!empty($this->local))
                    foreach ($this->local as $r) { ?>
                      <option value="<?php echo $r["idlocal"]?>" <?php echo $this->idgrupoaula==$r["idlocal"]?'selected="selectted"':'';?> idgrado="<?php echo $r["idlocal"]; ?>" idseccion="<?php echo $r["idsesion"]; ?>"><?php echo $r["nombre"] ?></option>
                     <?php } ?>
                </select>
              </div> 
            </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Ambient');?> <span class="required"> * </span></label>
              <div class="cajaselect">
                <select name="idambiente" id="idambiente" class="form-control" idambiente="<?php echo @$frm["idambiente"]; ?>">
                  <option value="0"><?php echo JrTexto::_('Only') ?></option>
                </select>
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Start Date');?> <span class="required"> * </span></label>
              <div class="form-group">
                <div class="input-group date datetimepicker">
                  <input type="text" class="form-control" required="required" name="fecha_inicio" id="fecha_inicio" value="<?php echo !empty($frm["fecha_inicio"])?$frm["fecha_inicio"]:date('Y-m-d'); ?>" autocomplete="off"> 
                  <span class="input-group-addon"><i class="fa fa-calendar" style="    padding-left: 1ex; padding-top: 1ex;"></i></span>
                </div>
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Finish Date');?> <span class="required"> * </span></label>
              <div class="form-group">
                <div class="input-group date datetimepicker">
                  <input type="text" class="form-control" required="required" name="fecha_final" id="fecha_final" value="<?php echo !empty($frm["fecha_final"])?$frm["fecha_final"]:date('Y-m-d'); ?>" autocomplete="off"> 
                  <span class="input-group-addon"><i class="fa fa-calendar" style="    padding-left: 1ex; padding-top: 1ex;"></i></span>
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveAcad_grupoauladetalle" type="submit" tb="acad_grupoauladetalle" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('acad_grupoauladetalle'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/grupoauladetalle.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var _frm=$('#frm-<?php echo $idgui;?>');
    if(!_frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>