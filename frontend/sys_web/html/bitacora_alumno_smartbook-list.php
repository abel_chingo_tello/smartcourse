<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_('Back')?></a></li> 
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li-->        
        <li class="active">&nbsp;<?php echo JrTexto::_("Bitacora_alumno_smartbook"); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">            
                                                                                                                                          
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 text-center">             
               <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/bitacora_alumno_smartbook/agregar" ><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
     <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idcurso") ;?></th>
                    <th><?php echo JrTexto::_("Idsesion") ;?></th>
                    <th><?php echo JrTexto::_("Idusuario") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th><?php echo JrTexto::_("Regfecha") ;?></th>
                    <th><?php echo JrTexto::_("Oldid") ;?></th>
                    <th><?php echo JrTexto::_("Oldido") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5de0327dcfe5d='';
function refreshdatos5de0327dcfe5d(){
    tabledatos5de0327dcfe5d.ajax.reload();
}
$(document).ready(function(){  
  var estados5de0327dcfe5d={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5de0327dcfe5d='<?php echo ucfirst(JrTexto::_("bitacora_alumno_smartbook"))." - ".JrTexto::_("edit"); ?>';
  var draw5de0327dcfe5d=0;
  var _imgdefecto='';
  var ventana_5de0327dcfe5d=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5de0327dcfe5d=ventana_5de0327dcfe5d.find('.table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Idcurso") ;?>'},
          {'data': '<?php echo JrTexto::_("Idsesion") ;?>'},
          {'data': '<?php echo JrTexto::_("Idusuario") ;?>'},
          {'data': '<?php echo JrTexto::_("Estado") ;?>'},
          {'data': '<?php echo JrTexto::_("Regfecha") ;?>'},
          {'data': '<?php echo JrTexto::_("Oldid") ;?>'},
          {'data': '<?php echo JrTexto::_("Oldido") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'json/bitacora_alumno_smartbook',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw5de0327dcfe5d=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5de0327dcfe5d;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push({             
              '#':(i+1),
              '<?php echo JrTexto::_("Idcurso") ;?>': data[i].idcurso,
          '<?php echo JrTexto::_("Idsesion") ;?>': data[i].idsesion,
          '<?php echo JrTexto::_("Idusuario") ;?>': data[i].idusuario,
          '<?php echo JrTexto::_("Estado") ;?>': data[i].estado,
          '<?php echo JrTexto::_("Regfecha") ;?>': data[i].regfecha,
          '<?php echo JrTexto::_("Oldid") ;?>': data[i].oldid,
          '<?php echo JrTexto::_("Oldido") ;?>': data[i].oldido,
                        //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/bitacora_alumno_smartbook/editar/?id='+data[i].idbitacora_smartbook+'" data-titulo="'+tituloedit5de0327dcfe5d+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idbitacora_smartbook+'" ><i class="fa fa-trash"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5de0327dcfe5d.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5de0327dcfe5d();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5de0327dcfe5d();
  })

  .on('click','.btnbuscar',function(ev){ refreshdatos5de0327dcfe5d();
  })
 

  ventana_5de0327dcfe5d.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idbitacora_smartbook',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/bitacora_alumno_smartbook/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5de0327dcfe5d.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/bitacora_alumno_smartbook/setcampo','masvalores':{idbitacora_smartbook:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idbitacora_smartbook',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/bitacora_alumno_smartbook/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5de0327dcfe5d.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var titulo=$(this).attr('data-titulo')||'';
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      tabledatos5de0327dcfe5d.ajax.reload();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frm(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_import(_md);
            })
          });
       }           
    })   
  })
});
</script>