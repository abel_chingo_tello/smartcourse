<style>
    #cmp-filtro {
        display: block;
    }
    select {
        cursor: pointer;
    }
    .select2 {
        width: 100% !important;
    }
    .boton1{
        margin-right: 20px;
    }
    .content1{
        padding: 7px 15px !important;
    }
    #btnInicio{
        margin-bottom: 0;
    }
</style>
    <?php 
        $usuarioAct = NegSesion::getUsuario();
        $idDocente = $usuarioAct['idpersona'];
    ?>
<section id="cmp-filtro">
    <div class="col-md-12 my-card my-shadow form-group content1">
        <div class="col-md-1 boton1">
            <button class="btn btn-primary" id="btnInicio"><?php echo JrTexto::_('home') ?></button>
        </div>
        <div class="col-md-4">
            <select id="selectCursos" class="form-control">
                <option value="0">-<?php echo JrTexto::_('selected courses') ?>-</option>
            </select>
        </div>
        <div class="col-md-6">
            <select id="selectAumnos" class="form-control ">
                <option value="0">-<?php echo JrTexto::_('select student') ?>-</option>
            </select>
        </div>
    </div>
</section>
<script>
    
    $(document).ready(() => {
        let selectCursos = $('#selectCursos');
        let selectAumnos = $('#selectAumnos');
        let btnInicio = $('#btnInicio');
        
        let idAlum = "";
        selectAumnos.select2();
        let urlModulo = _sysUrlBase_ + `json/participantes`;
        
        postData(urlModulo + `/listado`).then(arrCursos => {
            let arrTmp = arrCursos.map((curso) => {
                return {
                    id: curso.idcurso,
                    text: curso.strcurso + " - " + curso.strgrupoaula,
                    arrAlumnos: curso.arrAlumnos
                }
            });
            arrCursos = arrTmp;
            selectCursos.select2({
                data: arrCursos
            });
        })

        selectCursos.on("change", () => { //Añadido evento al cambiar
            if (selectCursos.val() == '0') {
                selectAumnos.val('0')
                selectAumnos.find('option').not(':first').remove();
                breadCrum.loadAlumnos(<?php echo $idDocente ?>);
            } else {
                let curso = selectCursos.select2('data')[0]; //obteniendo elemento seleccionado
                let alumnos = curso.arrAlumnos
                console.log('alumnos: ', alumnos);
                let arrTmp2 = alumnos.map((alumno) => {
                    return {
                        id: alumno.idalumno,
                        text: alumno.stralumno
                    }
                });
                alumnos = arrTmp2;
                selectAumnos.val('0');
                selectAumnos.find('option').not(':first').remove();
                selectAumnos.select2({ data: alumnos });
            }
        })

        selectAumnos.on("change", () => {
            idAlum = selectAumnos.val();
            console.log("idAlumno: " + idAlum);
            breadCrum.loadAlumnos(idAlum);
        })

        btnInicio.on("click", ()=>{
            selectCursos.val('0').trigger('change.select2');
            selectAumnos.val('0').trigger('change.select2');
            breadCrum.loadAlumnos(<?php echo $idDocente ?>);
        })

    });
</script>