<style>
    #cmp-breadCrum .row-cont>i {
        font-size: 2rem;
        color: #6fa6db;
        cursor: pointer;
        margin-right: 1rem;
    }

    #cmp-breadCrum .path-cont {
        font-size: 1rem;
    }

    #cmp-breadCrum .separator {
        font-weight: bolder;
        color: black;
    }

    #cmp-breadCrum .item-path {
        cursor: pointer;
    }
    #my-seccion{
        padding: 5px 15px !important;
    }
    #back-btn{
        background-color: white;
        padding: 0px 2px;
        border-radius: 50px;
    }
    #div-estilos{
        text-align: right;
        padding: 0;
    }
    #idbtnestilos{
        font-family: Helvetica Neue, Roboto, Arial, Droid Sans;
        padding: 0;
        margin: 0px;
        height: 32px;
        font-size: 14px;
    }

    .espaciospan {
        display: inline-block;
        margin-left: 6px;
    }
    .estilos{
        width: 180px !important;
        right: 180px;
    }
    .estilos-a {
        width: 100%;
        height: 100%;
        overflow: hidden;
        position: relative;
        right: 0;
        top: 0;
    }
    .estilos-a-file {
        top: 0;
        bottom: 0;
        right: 0;
        cursor: pointer;
        opacity: 0;
        padding: 0;
        position: absolute;
    }
    .estilos-item{
        z-index: 1;
    }
    .estilos>a>i{
        border-radius: 50px;
        font-size: 17px;
    }
    .estilos>a>i.fa-circle{
        color: #cccccc;
    }
    .estilos>a>i.fa-circle-o{
        background-color: black;
        color: black;
    }
    .estilos>a>i.fa-sun-o{
        background-color: blue;
        color: blue;
    }
    .estilos>a>i.fa-circle-thin{
        background-color: #FA58D0;
        color: #FA58D0;
    }
    .estilos>a>span{
        color: #212529;
        font-weight: 400;
        font-family: Helvetica Neue, Roboto, Arial, Droid Sans;
    }

</style>
<section id="cmp-breadCrum">
    <div class="col-sm-12 my-card my-shadow" id="my-seccion">
        <div class="col-md-10 row-cont my-y-center">
            <i id="back-btn" class="fa fa-arrow-circle-left" aria-hidden="true"></i>
            <span class="path-cont" m-render m-for="$.path,folder,index">
                <span class="item-path" index="{{index}}">{{folder.nombre}}</span> <span class="separator">></span>
            </span>
        </div>

        <div class="col-md-2" id="div-estilos">
            <button class="btn btn-info btn-sm dropdown-toggle" id="idbtnestilos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo JrTexto::_('bottom') ?></button>
            <div id="" class="dropdown-menu estilos" aria-labelledby="idbtnestilos">
                <a id="idNormal" class="dropdown-item estilos-item" href="#"> <i class="fa fa-circle" aria-hidden="true"></i> <span class="espaciospan"><?php echo JrTexto::_('predefined') ?></span> </a>
                <a id="idOscuro" class="dropdown-item " href="#"> <i class="fa fa-circle-o" aria-hidden="true"></i> <span class="espaciospan"><?php echo JrTexto::_('dark') ?></span> </a>
                <a id="idClaro" class="dropdown-item " href="#"> <i class="fa fa-sun-o" aria-hidden="true"></i> <span class="espaciospan"><?php echo JrTexto::_('sure') ?></span> </a>
                <a id="idPink" class="dropdown-item " href="#"> <i class="fa fa-circle-thin" aria-hidden="true"></i> <span class="espaciospan"><?php echo JrTexto::_('pink') ?></span> </a>
                <a class="dropdown-item estilos-a" href="#" > <i class="fa fa-camera" aria-hidden="true"></i> <span class="espaciospan">  <?php echo JrTexto::_('add image') ?></span>
                    <input accept="image/*, image/heic, image/heif" id="idFileImg" class="estilos-a-file" title="Elige una imagen" type="file"/>
                </a>
            </div>
        </div>
    </div>
</section>
<script>
    var datos2;
    class BreadCrum {
        constructor(params) {
            this.render = new Render({
                selector: '#cmp-breadCrum',
                instance: this
            });
            this.explorer = params.explorer;
            this.render.model.path = [];
            this.attachEvents();
        }
        onRendered() {
            // console.log('renderizando BreadCrum...');
            let arrSpanPath = $('.item-path').get();
            arrSpanPath.forEach(spanPath => {
                $(spanPath).on('click', (e) => {
                    let index = parseInt($(e.target).attr('index'));
                    this.jump(index);
                })
            });
        }
        attachEvents() {
            $('#back-btn').on('click', (e) => {
                this.back();
            })
        }
        back() {
            if (this.render.model.path.length > 1) {
                this.render.model.path.pop();
                this.explorer.render.model.folder = this.render.model.path[this.render.model.path.length - 1];
                this.explorer.render.render();
                this.render.render();
            }
        }
        jump(index) {
            let folder = this.render.model.path[index];
            this.explorer.render.model.folder = folder;
            this.explorer.render.render();

            this.render.model.path.length = index + 1;
            this.render.render();
        }
        load() {
            postData(_sysUrlBase_ + 'json/portafolio', {}).then((data) => {
                this.explorer.render.model.folder = data.folder;
                this.explorer.render.render();
                this.render.model.path.push(data.folder);
                this.render.render();
                datos2 = data;
                if(data.estilo != null){
                    let estilo = JSON.parse(data.estilo);
                    if(estilo.tipo == 1){
                        $("#idcontenedor").css("background-image",`radial-gradient(#cccccc, ${estilo.fondo})`);
                    }else if(estilo.tipo == 2){
                        let imgenruta = `url(${_sysUrlBase_}static/portafolio-img/${estilo.fondo})`;
                        $("#idcontenedor").css( "background-image" ,imgenruta );
                    }
                }
                //console.log('data', data);
            }).catch(e => console.log('error', e))
        }
        loadAlumnos(idalumno) {
            postData(_sysUrlBase_ + 'json/portafolio', {
                idalumno
            }).then((data) => {
                this.explorer.render.model.folder = data.folder;
                this.explorer.render.render();
                this.render.model.path = [];
                this.render.model.path.push(data.folder);
                this.render.render();
                //console.log('dataAlumnos', data);
            }).catch(e => console.log('error', e))
        }
        refresh(folder) {
            postData(_sysUrlBase_ + 'json/portafolio_folder/getFolder', {
                idfolder: folder.idfolder,
                idportafolio: folder.idportafolio,
            }).then((folder) => {
                this.render.model.path[this.render.model.path.length-1]=folder;
                this.explorer.render.model.folder = folder;
                this.explorer.render.render();
                console.log('refresh()', folder);
            }).catch(e => console.log('error', e))
        }
        getFolder(folder) {
            console.log('folder',folder)
            postData(_sysUrlBase_ + 'json/portafolio_folder/getFolder', {
                idfolder: folder.idfolder,
                idportafolio: folder.idportafolio,
            }).then((rsFolder) => {
                // console.log('this.render.model.path', this.render.model.path)
                this.render.model.path.push(rsFolder);
                this.render.render();
                this.explorer.render.model.folder = rsFolder;
                this.explorer.render.render();
                console.log('getFolder()', rsFolder);
            }).catch(e => console.log('error', e))
        }

    }
</script>