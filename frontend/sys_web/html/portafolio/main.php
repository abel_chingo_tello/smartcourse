<?php
defined("RUTA_BASE") or die();
$usuarioAct = NegSesion::getUsuario();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Portafolio"); ?>'
        });
        addButonNavBar({
            id: 'dropdownMenuButton',
            class: 'btn btn-success btn-sm dropdown-toggle',
            text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('New') ?>',
            attr: {
                "data-toggle": "dropdown",
                "aria-haspopup": "true",
                "aria-expanded": "false"
            },
            // click: `modalRubrica()`
        });
    </script>
<?php } ?>
<style>
    section {
        visibility: visible;
    }
    #menu-dropdown {
        width: 180px !important;
        right: 180px;
    }
    .my-card {
        margin-top: 15px !important;
        margin-bottom: 10px !important;
        padding: 10px 15px !important;
        background-color: rgba(255,255,255,.75) !important;
        font-weight: bold !important;
    }
    #idcontenedor{
        border-radius: 1.5em;
        height: 447px;
        background-color: #ebebeb;
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
    }
    .x_panel{
        margin-bottom: 0 ;
    }
</style>

<main class="container my-font-all">
    <div class="col-sm-12" id="idcontenedor">
        <?php if ($usuarioAct['idrol'] == "2") { include("filtroAlumno.php"); } ?>
        <?php include("breadCrum.php") ?>
        <?php include("explorer.php") ?>
    </div>
</main>

<script>
    var explorer;
    $(document).ready(() => {
        explorer = new Explorer({
            autor: true
        });
        breadCrum = new BreadCrum({
            explorer
        });
        explorer.breadCrum = breadCrum;
        breadCrum.load();

        const idcontenedor = $("#idcontenedor");
        const idbtnestilos = $("#idbtnestilos");
        const idFileImg = $("#idFileImg");
        const idNormal = $("#idNormal");
        const idOscuro = $("#idOscuro");
        const idClaro = $("#idClaro");
        const idPink = $("#idPink");
        const ruta = _sysUrlBase_ + "static/portafolio-img/";

        function subirImg(imagen){
            Swal.showLoading();
            postData(_sysUrlBase_ + 'json/portafolio/guardarImagen', { fondo: imagen }).then( rs => {
                if(rs.estado == 1){
                    actualizarEstilo(rs.nombre, 2);
                    idbtnestilos.click();
                    Swal.close();
                }else{
                    Swal.fire(rs.msj,'las extensiones correctas son: jpg, jpeg, png, git', 'warning');
                }
            })
        }

        function renderImg(nomImagen){
            let img2 = `url(${ruta}${nomImagen})`;
            idcontenedor.css( "background-image" ,img2 );
        }

        function renderColor(color){
            idcontenedor.css("background-image",`radial-gradient(#cccccc, ${color})`);
        }

        function actualizarEstilo(color, tipo){
            Swal.showLoading();
            let fondo = `{ "fondo": "${color}", "tipo": ${tipo}}`;
            let idPorta = datos2.idportafolio;
            let idAlum = datos2.idalumno;
            let idProy = datos2.idproyecto;
            postData(_sysUrlBase_ + 'json/portafolio/guardarEstilo', { fondo: fondo, idPorta: idPorta, idAlum: idAlum, idProy: idProy }).then( rs => {
                console.log('rs: ', rs)
                if(tipo == 1){
                    renderColor(color);
                }else{
                    renderImg(color);
                }
                Swal.close();
            })
        }

        idNormal.click(function(){
            actualizarEstilo("#ebebeb", 1);
        });
        idOscuro.click(function(){
            actualizarEstilo("black", 1);
        });
        idClaro.click(function(){
            actualizarEstilo("blue", 1);
        });
        idPink.click(function(){
            actualizarEstilo("#FA58D0", 1);
        });
        idFileImg.change(function(){
            const imagen = idFileImg.prop("files")[0];
            if(imagen != null){
                subirImg(imagen);
            }
        });
    })
</script>