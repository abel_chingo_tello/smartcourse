<style>
    #cmp-explorer .row-contenido {
        border-bottom: 1px solid #ebebeb;
    }

    #cmp-explorer .row-contenido {
        padding-top: 0.7rem;
        padding-bottom: 0.7rem;
    }

    #cmp-explorer .row-contenido:hover {
        /* background-color: #b9b9b91f; */
        background-color: rgb(185,185,185, .35);
    }

    #cmp-explorer .item.folder,
    #cmp-explorer .item.file {
        display: inline-block;
    }

    #cmp-explorer .item {
        cursor: pointer;
        font-size: 1.1rem;
        width: fit-content;
        padding-right: 1rem;
    }

    #cmp-explorer .item>i {
        font-size: 2rem;
        padding-right: 12px;
        line-height: 1.5rem;
    }

    #cmp-explorer .item>i.fa-folder {
        color: #3b64ff;
    }

    #cmp-explorer .item>i.fa-file {
        color: #cbcbcb;
    }

    #cmp-explorer .file-wrapper {
        height: 50vh;
        overflow: auto;
    }

    #cmp-explorer i.fa.fa-pencil {
        margin-left: 3rem;
        font-size: 1rem;
        color: #ffa200;
        display: none;
        cursor: pointer;
    }

    #cmp-explorer .row-contenido:hover i.fa.fa-pencil {
        display: inline-block;
    }

    .msj-noportafolio{
        font-size: 2em;
        font-weight: 600;
        font-style: italic;
        color: #868e96 ;
        text-align: center;
        letter-spacing: 2px;
    }
</style>
<section id="cmp-explorer">
    <div class="col-sm-12 my-card my-shadow">
        <div class="file-wrapper">
            <div m-render m-for="$.folder.arrContenido,file,i">
                <div class="row-contenido">
                    <a href="{{(file.kind=='file'?file.ubicacion:'#!')}}" class="item {{(file.kind=='folder'?'folder':'file')}}" idfolder="{{file.idfolder}}" target="{{(file.kind=='file'?'_blank':'')}}" rel="noopener noreferrer">
                        <i class="fa {{(file.kind=='folder'?'fa-folder':this.options.instance.setIcon(file.extension))}}" idfolder="{{file.idfolder}}" aria-hidden="true"> </i>
                        {{file.nombre}}
                    </a>
                    <span class="{{($.autor?'':'my-hide')}}">
                        <i id="btn-edit" kind="{{file.kind}}" idfolder="{{file.idfolder}}" idarchivo="{{file.idarchivo}}" class="fa fa-pencil" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div m-render>
                <div class="{{(($.folder.arrContenido.length>0)?'my-hide':'no-items')}}">
                    <p class="msj-noportafolio"><?php echo JrTexto::_('no files found in the portfolio') ?>...</P>
                </div>
            </div>
            <div m-render>
                <div class="my-color-green my-loader {{((!$.loading)?'my-hide':'')}}">
                </div>
            </div>
        </div>
    </div>
</section>
<div id="modal-cont">
    <!-- Modal Carpeta-->
    <div class="modal fade my-modal" id="md-carpeta" tabindex="-1" role="dialog" aria-labelledby="md-carpetaLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        <i class="fa fa-folder" aria-hidden="true"> </i> <?php echo JrTexto::_('folder') ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-carpeta">
                        <input id="input-idcarpeta" class="my-hide" disabled type="text">
                        <div class="form-group">
                            <label for="nombre_folder"><?php echo JrTexto::_('folder name') ?></label>
                            <input required type="text" name="nombre_folder" id="input_nombre_folder" class="form-control" placeholder="<?php echo JrTexto::_('folder name')?>">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button id="btn-eliminar-carpeta" type="button" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> <?php echo JrTexto::_('delete') ?></button>
                                <button id="btn-save" type="submit" form="form-add-carpeta" class="btn btn-primary"><?php echo JrTexto::_('save') ?></button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal"><?php echo JrTexto::_('close') ?></button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal"><?php echo JrTexto::_('cancel') ?></button>
                                    <button type="submit" form="form-add-carpeta" class="btn btn-primary"><?php echo JrTexto::_('create') ?></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Archivo-->
    <div class="modal fade my-modal" id="md-archivo" tabindex="-1" role="dialog" aria-labelledby="md-archivoLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        <i class="fa fa-link" aria-hidden="true"> </i> <?php echo JrTexto::_('file') ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-archivo">
                        <input id="input-idarchivo" class="my-hide" disabled type="text">
                        <div class="form-group">
                            <label for="input_nombre_archivo"><?php echo JrTexto::_('file name') ?></label>
                            <input required type="text" name="input_nombre_archivo" id="input_nombre_archivo" class="form-control" placeholder="<?php echo JrTexto::_('file name')?>...">
                        </div>
                        <div class="form-group">
                            <label for="input_nombre_extension"><?php echo JrTexto::_('extension') ?></label>
                            <input required type="text" name="input_nombre_extension" id="input_nombre_extension" class="form-control" placeholder="<?php echo JrTexto::_('file extension')?>...">
                        </div>
                        <div class="form-group">
                            <label for="input_url">Url</label>
                            <input required type="text" name="input_url" id="input_url" class="form-control" placeholder="<?php echo JrTexto::_('file url')?>...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button id="btn-eliminar-archivo" type="button" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> <?php echo JrTexto::_('delete') ?></button>
                                <button id="btn-save" type="submit" form="form-add-archivo" class="btn btn-primary"><?php echo JrTexto::_('save') ?></button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal"><?php echo JrTexto::_('close') ?></button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal"><?php echo JrTexto::_('cancel') ?></button>
                                    <button type="submit" form="form-add-archivo" class="btn btn-primary"><?php echo JrTexto::_('create') ?></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    class Explorer {
        constructor(model) {
            this.render = new Render({
                instance: this,
                selector: '#cmp-explorer',
                model
            });
            this.showDropDown();
            this._breadCrum = null;
            this.render.model.loading = false;
            addOnModalClose();
            $('#btn-eliminar-archivo').on('click', () => {
                this.removeFile();
            })
            $('#btn-eliminar-carpeta').on('click', () => {
                this.removeFolder();
            })

        }
        set breadCrum(breadCrum) {
            this._breadCrum = breadCrum;
        }
        onRendered(rendered) {
            // console.log('renderizando Explorer...')
            if (rendered) {
                let arrDivFolder = $('.item.folder').get();
                arrDivFolder.forEach(divFolder => {
                    $(divFolder).on('dblclick', (e) => {
                        console.log('e.target',e.target);
                        if ($(e.target).attr('id') != 'btn-edit') {
                            this.openFolder(e.target);
                        }
                    })
                });
                let arrBtnEdit = $('.fa.fa-pencil').get()
                arrBtnEdit.forEach(btnEdit => {
                    $(btnEdit).on('click', (e) => {
                        if ($(e.target).attr('kind') == 'folder') {
                            this.modalCarpeta(e.target);
                        } else {
                            this.modalArchivo(e.target);
                        }
                    })
                });
                this.render.model.loading = false;
            }
        }
        modalCarpeta(el = null) {
            if (el) {
                this.$ = this.render.model;
                let arrContenido = this.$.folder.arrContenido;
                let idfolder = $(el).attr('idfolder');
                let folder = arrContenido.filter((contenido) => {
                    if (contenido.idfolder == idfolder) {
                        return contenido;
                    }
                })[0];
                $('#input_nombre_folder').val(folder.nombre);
                $('#input-idcarpeta').val(folder.idfolder);
                $('#md-carpeta').find('#autor-btn').removeClass('my-hide');
                $('#md-carpeta').find("#new-btn").addClass('my-hide');
                $('#md-carpeta').find("#default-btn").removeClass('my-hide');
            } else {
                $('#md-carpeta').find('#autor-btn').addClass('my-hide');
                $('#md-carpeta').find("#new-btn").removeClass('my-hide');
            }
            $('#md-carpeta').modal('show');
        }
        modalArchivo(el) {
            if (el) {
                this.$ = this.render.model;
                let arrContenido = this.$.folder.arrContenido;
                let idarchivo = $(el).attr('idarchivo');
                let archivo = arrContenido.filter((contenido) => {
                    if (contenido.idarchivo == idarchivo) {
                        return contenido;
                    }
                })[0];
                $('#input_nombre_archivo').val(archivo.nombre);
                $('#input_nombre_extension').val(archivo.extension);
                $('#input_url').val(archivo.ubicacion);
                $('#input-idarchivo').val(archivo.idarchivo);
                $('#md-archivo').find('#autor-btn').removeClass('my-hide');
                $('#md-archivo').find("#new-btn").addClass('my-hide');
                $('#md-archivo').find("#default-btn").removeClass('my-hide');
            } else {
                $('#md-archivo').find('#autor-btn').addClass('my-hide');
                $('#md-archivo').find("#new-btn").removeClass('my-hide');
            }
            $('#md-archivo').modal('show');
        }
        showDropDown() {
            if (this.render.model.autor) {
                let content = $('#dropdownMenuButton').parent();
                let dropdown = `  <div id="menu-dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a id="btn-new-folder" class="dropdown-item" href="#"> <i class="fa fa-folder" aria-hidden="true"> </i> Nueva carpeta</a>
                            <a id="btn-new-file" class="dropdown-item" href="#"> <i class="fa fa-link" aria-hidden="true"> </i> Archivo enlace</a>
                        </div>`;
                content.append(dropdown);
                this.attachEvents();
            }
        }
        attachEvents() {
            $('#form-add-carpeta').on('submit', (e) => {
                e.preventDefault();
                this.saveFolder();
            });
            $('#btn-new-folder').on('click', (e) => {
                this.modalCarpeta();
            });
            $('#form-add-archivo').on('submit', (e) => {
                e.preventDefault();
                this.saveFile();
            });
            $('#btn-new-file').on('click', (e) => {
                this.modalArchivo();
            });

        }
        refresh() {
            this.$ = this.render.model;
            this._breadCrum.refresh(this.$.folder);
        }
        openFolder(divFolder) {
            this.$ = this.render.model;
            let idfolder = $(divFolder).attr('idfolder');
            let arrContenido = this.$.folder.arrContenido;
            let folder = arrContenido.filter((contenido) => {
                if (contenido.idfolder == idfolder) {
                    return contenido;
                }
            })[0];
            this.render.model.folder = {
                arrContenido: []
            };
            this.render.model.loading = true;
            this.render.render();
            console.log('openFolder()', folder)
            this._breadCrum.getFolder(folder);
        }
        saveFolder() {
            this.$ = this.render.model;
            let inputNombreFolder = $('#input_nombre_folder');
            $('#md-carpeta').modal('hide');
            Swal.showLoading();
            postData(_sysUrlBase_ + 'json/portafolio_folder/guardar', {
                idfolder: $('#input-idcarpeta').val(),
                nombre: inputNombreFolder.val(),
                idportafolio: this.$.folder.idportafolio,
                idfolder_padre: this.$.folder.idfolder
            }).then((rs) => {
                console.log('rs', rs)
                if (rs.code == 200) {
                    this.refresh();
                    Swal.close();
                } else {
                    swalWrong()
                }

            })
            return false;
        }
        removeFolder() {
            //

            //
            this.$ = this.render.model;
            $('#md-carpeta').modal('hide');
            Swal.showLoading();
            postData(_sysUrlBase_ + 'json/portafolio_folder/eliminar', {
                idfolder: $('#input-idcarpeta').val(),
                idportafolio: this.$.folder.idportafolio,
            }).then((rs) => {
                console.log('rs', rs)
                if (rs.code == 200) {
                    this.refresh();
                    Swal.close();
                } else {
                    swalWrong()
                }

            })
        }
        saveFile() {
            this.$ = this.render.model;
            let inputNombreArchivo = $('#input_nombre_archivo');
            let inputUbicacion = $('#input_url');
            let inputExtension = $('#input_nombre_extension');
            $('#md-archivo').modal('hide');
            Swal.showLoading();
            postData(_sysUrlBase_ + 'json/portafolio_archivo/guardar', {
                idarchivo: $('#input-idarchivo').val(),
                nombre: inputNombreArchivo.val(),
                ubicacion: inputUbicacion.val(),
                extension: inputExtension.val(),
                idportafolio: this.$.folder.idportafolio,
                idfolder: this.$.folder.idfolder
            }).then((rs) => {
                console.log('rs', rs)
                if (rs.code == 200) {
                    this.refresh();
                    Swal.close();
                } else {
                    swalWrong()
                }
            })
            return false;
        }
        removeFile() {
            this.$ = this.render.model;
            $('#md-archivo').modal('hide');
            Swal.showLoading();
            postData(_sysUrlBase_ + 'json/portafolio_archivo/eliminar', {
                idarchivo: $('#input-idarchivo').val(),
                idportafolio: this.$.folder.idportafolio,
            }).then((rs) => {
                console.log('rs', rs)
                if (rs.code == 200) {
                    this.refresh();
                    Swal.close();
                } else {
                    swalWrong()
                }
            })
        }
        setIcon(extension) {
            let strIcon = '';
            switch (extension) {
                case 'pdf':
                    strIcon = 'fa-file-pdf-o';
                    break;
                case 'doc':
                    strIcon = 'fa-file-word-o';
                    break;
                case 'docx':
                    strIcon = 'fa-file-word-o';
                    break;
                case 'ppt':
                    strIcon = 'fa-file-powerpoint-o';
                    break;
                case 'pptx':
                    strIcon = 'fa-file-powerpoint-o';
                    break;
                case 'xls':
                    strIcon = 'fa-file-excel-o';
                    break;
                case 'xlsx':
                    strIcon = 'fa-file-excel-o';
                    break;
                case 'rar':
                    strIcon = 'fa-file-archive-o';
                    break;
                case 'zip':
                    strIcon = 'fa-file-archive-o';
                    break;
                case 'mp3':
                    strIcon = 'fa-music';
                    break;
                case 'mp4':
                    strIcon = 'fa-file-video-o';
                    break;
                case 'avi':
                    strIcon = 'fa-file-video-o';
                    break;
                default:
                    strIcon = 'fa-file';
                    break;
            }
            return strIcon;
        }
    }
</script>