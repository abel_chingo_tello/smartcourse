<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/usuarios/user_avatar.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/personal">&nbsp;<?php echo JrTexto::_('Personal'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<style type="text/css">
  .bootstrap-timepicker-widget.dropdown-menu.open{
  display: inline-block;
  z-index:1151;
}
</style>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="persona" target="" enctype="" accion="adicionar" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idpersona" id="idpersona" value="<?php echo $this->pk;?>">
          <input type="hidden" name="idrol" id="idrol" value="<?php echo !empty($this->idrol)?$this->idrol:3;?>">
          <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo !empty($this->idproyecto)?$this->idproyecto:'';?>">
          <div class="row">
            <div class="col-md-9 col-sm-8 colxs-12">
              <div class="row">
                  <div class="form-group col-md-6 col-sm-6 colxs-12">
                    <label><?php echo ucfirst(JrTexto::_("Tipo de documento")); ?></label>
                    <div class="cajaselect">
                      <select name="tipodoc" id="tipodoc" class="form-control">                     
                        <?php if(!empty($this->fktipodoc)) foreach ($this->fktipodoc as $fk) { ?>
                          <option value="<?php echo $fk["codigo"]?>"  >
                            <?php echo $fk["nombre"] ?>
                            </option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-6 col-sm-6 colxs-12">
                    <label class="text-left">N° <?php echo ucfirst(JrTexto::_('Identificación')); ?></label>                    
                      <input type="text" class="form-control" name="dni" id="dni" required="required" value="<?php echo @$frm["dni"]; ?>" placeholder=""> 
                  </div>
                  <div class="form-group col-md-12">
                    <label class="text-left"><?php echo ucfirst(JrTexto::_('Nombres')); ?></label>
                      <input type="text" class="form-control"  required="required" name="nombre" id="nombre" value="<?php echo ucfirst(@$frm["nombre"]); ?>" placeholder=""> 
                  </div>  
                  <div class="form-group col-md-6 col-sm-6 colxs-12">
                    <label class="control-label"><?php echo JrTexto::_('Apellido paterno');?> <span class="required"> * </span></label>
                    <div class="">
                      <input type="text"  id="ape_paterno" name="ape_paterno" required="required" class="form-control" value="<?php echo @$frm["ape_paterno"];?>">
                    </div>
                  </div>

                  <div class="form-group col-md-6 col-sm-6 colxs-12">
                    <label class="control-label"><?php echo JrTexto::_('Apellido materno');?> <span class="required"> * </span></label>
                    <div class="">
                      <input type="text"  id="ape_materno" name="ape_materno" required="required" class="form-control" value="<?php echo @$frm["ape_materno"];?>">
                    </div>
                  </div>
                  <div class="form-group  col-md-6 col-sm-6 colxs-12">
                    <label><?php echo ucfirst(JrTexto::_("Genero")); ?></label>
                    <div class="cajaselect">
                      <select name="sexo" id="sexo" class="form-control">
                        <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                          <option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> >
                            <?php echo $fksexo["nombre"] ?>
                            </option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group  col-md-6 col-sm-6 colxs-12">
                    <label><?php echo ucfirst(JrTexto::_("Estado civil")); ?></label>
                    <div class="cajaselect">
                      <select name="estado_civil" id="estado_civil" class="form-control">
                        <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                          <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                            <?php echo $fkestado_civil["nombre"] ?>
                            </option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group  col-md-6 col-sm-6 colxs-12">
                    <label class="control-label"><?php echo JrTexto::_('Número de telefono');?> <span class="required"> * </span></label>
                    <div class="">
                      <input type="text"  id="telefono" name="telefono" required="required" class="form-control" value="<?php echo @$frm["telefono"];?>">                                        
                    </div>
                  </div>
                  <div class="form-group  col-md-6 col-sm-6 colxs-12">
                    <label class="control-label"><?php echo JrTexto::_('Número de celular');?> <span class="required"> * </span></label>
                    <div class="">
                      <input type="text"  id="celular" name="celular" required="required" class="form-control" value="<?php echo @$frm["celular"];?>">                                        
                    </div>
                  </div>
                  
              </div>              
            </div>
            <div class="col-md-3 col-sm-4 colxs-12">
              <div class="form-group">
                <label class="control-label"><?php echo JrTexto::_('Foto');?> <span class="required"> * </span></label>
                <div class="text-center">               
                    <img src="<?php echo URL_BASE.(!empty($frm["foto"])?$frm["foto"]:$_imgdefecto);?>" alt="fotofile" id="fotofile" file="foto" class="img-fluid subirfile img-thumbnail">
                    <input type="hidden" id="foto" name="foto" value="<?php echo !empty($frm["foto"])?URL_BASE.$frm["foto"]:"";?>">
                </div>
              </div>
              <div class="form-group">
                <label><?php echo ucfirst(JrTexto::_("Cumpleaño")); ?></label>
                <div class="form-group">
                  <div class="input-group date datetimepicker">
                    <input type="text" class="form-control" required="required" name="fechanac" id="fechanac" value="<?php echo !empty($frm["fechanac"])?$frm["fechanac"]:date('Y-m-d'); ?>" autocomplete="off"> 
                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('Estatus');?></label>
                  <div class="">
                    <a  style="cursor:pointer;" href="javascript:;" id="aestado" class="btn-activar <?php echo @$frm["estado"]==1?"active":"";?>"> <i class="fa fa<?php echo @$frm["estado"]=="1"?"-check":"";?>-circle-o fa-lg"></i> 
                      <input type="hidden" id="estado" name="estado" value="<?php echo @$frm["estado"]?1:0;?>">
                    </a>                                 
                  </div>
                </div>
              </div>
            </div> 
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-savePersonal" type="submit" tb="persona" class="btn btn-success" ><i class=" fa fa-hand-o-down"></i> <?php echo JrTexto::_('Guardar y seleccionar');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('personal'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancelar');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/persona_buscar.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frmpersonabuscar($('#vent-<?php echo $idgui;?>'));
  })
</script>