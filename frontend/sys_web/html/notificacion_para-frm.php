<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/notificacion_para">&nbsp;<?php echo JrTexto::_('Notificacion_para'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col-md-12">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="notificacion_para" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idnotipersona" id="idnotipersona" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idnotificacion');?> <span class="required"> * </span></label>
              <div class="">
              <select id="txtidnotificacion" name="idnotificacion" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkidnotificacion))
                foreach ($this->fkidnotificacion as $fkidnotificacion) { ?><option value="<?php echo $fkidnotificacion["idnotificacion"]?>" <?php echo $fkidnotificacion["idnotificacion"]==@$frm["idnotificacion"]?"selected":""; ?> ><?php echo $fkidnotificacion["texto"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idpersona');?> <span class="required"> * </span></label>
              <div class="">
              <select id="txtidpersona" name="idpersona" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkidpersona))
                foreach ($this->fkidpersona as $fkidpersona) { ?><option value="<?php echo $fkidpersona["idpersona"]?>" <?php echo $fkidpersona["idpersona"]==@$frm["idpersona"]?"selected":""; ?> ><?php echo $fkidpersona["nombre"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
              <div class="">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle";?>" 
                data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="estado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveNotificacion_para" type="submit" tb="notificacion_para" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('notificacion_para'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/notificacion_para/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/notificacion_para');
                }
              }
          });
       }
  })
})
</script>

