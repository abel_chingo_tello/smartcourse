<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Notes"); ?> Smartbook'
        });
        var __usuario = <?= json_encode($this->usuario) ?>
    </script>
<?php } ?>
<script>
    const __url_ADULTOS = "<?= $this->url_ADULTOS ?>";
    const __url_ADOLESCENTES = "<?= $this->url_ADOLESCENTES ?>";
</script>
<link rel="stylesheet" href="<?= URL_RAIZ ?>smartbook/plantillas/smartbook/css/ver/ver_adultos.css" type="text/css">
<link rel="stylesheet" href="<?= URL_RAIZ ?>smartbook/plantillas/smartbook/css/inicio.css" type="text/css">
<style>
    #smartbook-ver .row {
        display: block;
    }

    .col-xs-12 {
        width: 100%;
    }

    a.save-progreso {
        display: none !important;
    }

    .calificacion-docente {
        display: none;
    }

    a.btnenformatohtml,
    textarea.txtrespuestaAlumno {
        display: none !important;
    }

    label.showonpreview_nlsw {
        font-weight: 600;
        font-size: 1rem;
    }
    body>div.container-fluid{
        width: 99vw;
    }
</style>
<div class="container my-font-all">
    <div class="row my-x-center">
        <?php if ($this->usuario["idrol"] == 1) { ?>
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="">Grupos</label>
                    <select class="form-control" name="" id="selectGrupos">
                    </select>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectCursos"><?=JrTexto::_("Courses")?></label>
                <select class="form-control" name="" id="selectCursos">
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectAlumnos"><?=ucfirst(JrTexto::_("Students"))?></label>
                <select class="form-control" name="" id="selectAlumnos">
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectSmartbooks">SmartBook</label>
                <select class="form-control" name="" id="selectSmartbooks">
                </select>
            </div>
        </div>
    </div>
    <div class="row my-x-center">
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="selectTipo"><?=JrTexto::_("Type")?></label>
                <select class="form-control" name="" id="selectTipo">
                </select>
            </div>
        </div>
    </div>
    <div class="row col-sm-12 my-x-center">
        <div class="box-cont">
            <div id="smartbook-ver">
                <div id="div-msg" class="my-card my-shadow msg my-mg-bottom">
                    <?=JrTexto::_("Loading")?>...
                </div>

            </div>
            <!-- <div class="col-sm-12">
                <button id="btn-save" class="btn btn-success my-center">
                    Guardar
                </button>
            </div> -->
        </div>

    </div>

</div>
<!-- Modal crudNotas-->
<div class="modal fade my-modal soft-rw my-font-all" id="md-crudNotas" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">
                    <?=JrTexto::_("Assign note")?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-no-scroll">
                <form id="form-add-crudNotas">
                    <input id="input-idcrudNotas" class="my-hide" type="text" disabled>
                    <div class="form-group">
                        <label for="detalle-crudNotas"><?=JrTexto::_("Note")?></label>
                        <input required  pattern="^([0-9]{1,})((,|\.){0,1}[0-9]{1,}){0,1}" title="<?=JrTexto::_("Integers or decimals only")?>" type="text" name="nombre-crudNotas" id="input-nota" class="form-control" placeholder="<?=JrTexto::_("Note")?>..."></input>
                        <!-- <input required type="text" name="detalle-crudNotas" id="detalle-crudNotas" class="form-control" placeholder="Detalle..."> -->
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12  col-md-6">
                        <div id="autor-btn" class="my-hide">
                            <button type="button" onclick="eliminarcrudNotas()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> <?=JrTexto::_("Delete")?></button>
                            <button name="btn-save" type="submit" form="form-add-crudNotas" class="btn btn-primary"><?=JrTexto::_("Save")?></button>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div id="default-btn" class="my-hide">
                            <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal"><?=JrTexto::_("Close")?></button>
                        </div>
                    </div>
                    <div name="modo-crear" class="col-sm-12 my-hide">
                        <div class="row my-block">
                            <div class="my-float-r">
                                <button type="button" class="btn  btn-secondary" data-dismiss="modal"><?=JrTexto::_("Cancel")?></button>
                                <button name="btn-save" type="submit" form="form-add-crudNotas" class="btn btn-primary"><?=JrTexto::_("Save")?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(() => {
        const oCrudNotasSmartbook = new CrudNotasSmartbook();
        oCrudNotasSmartbook.launch();
    });

    function iniciarCompletar_DBY() {
        console.log('iniciarCompletar_DBY');
    }
</script>