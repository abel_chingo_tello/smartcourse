<?php
/* defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : ""; */
defined("RUTA_BASE") or die();
$idgui = uniqid();
//$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$ventanapadre = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
//if (!empty($this->datos)) $personal = $this->datos;
if (!$ismodal) { ?>
  <!--div class="row" id="breadcrumb">
    <div class="col">
      <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;< ?php echo JrTexto::_('Back') ?></a></li>
        <a href="< ?php echo $this->documento->getUrlSitio(); ?>"><i class="fa fa-home"></i>&nbsp;< ?php echo JrTexto::_("Home"); ?></a>
        <li class="active">&nbsp;< ?php echo JrTexto::_("Tareas"); ?></li>
      </ol>
    </div>
  </div-->
<?php } ?>
<!--style type="text/css">
  .btn {
    font-size: 14px;
  }
</style-->
<!--div class="form-view" id="ventana_< ?php echo $idgui; ?>">
  <div class="row">
    <div class="col">
      <div class="row">

        <div class="col-xs-6 col-sm-6 col-md-6">
          <div class="form-group">
            < ?php echo  ucfirst(JrTexto::_("text to search")) ?>
            <div class="input-group">
              <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="< ?php echo  ucfirst(JrTexto::_("text to search")) ?>">
              <span class="input-group-addon btn btnbuscar">< ?php echo  ucfirst(JrTexto::_("Search")) ?> <i class="fa fa-search"></i></span>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 text-center">
          <a class="btn btn-success btnvermodal" href="< ?php echo $this->documento->getUrlSitio(); ?>/tareas/agregar"><i class="fa fa-plus"></i> < ?php echo JrTexto::_('add') ?></a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col table-responsive">
      <table class="table table-striped table-hover">
        <thead>
          <tr class="headings">
            <th>#</th>
            <th>< ?php echo JrTexto::_("Tipo"); ?></th>
            <th>< ?php echo JrTexto::_("Idcurso"); ?></th>
            <th>< ?php echo JrTexto::_("Idsesion"); ?></th>
            <th>< ?php echo JrTexto::_("Idpestania"); ?></th>
            <th>< ?php echo JrTexto::_("Idalumno"); ?></th>
            <th>< ?php echo JrTexto::_("Recurso"); ?></th>
            <th>< ?php echo JrTexto::_("Tiporecurso"); ?></th>
            <th>< ?php echo JrTexto::_("Nota"); ?></th>
            <th>< ?php echo JrTexto::_("Notabaseen"); ?></th>
            <th>< ?php echo JrTexto::_("Iddocente"); ?></th>
            <th>< ?php echo JrTexto::_("Idproyecto"); ?></th>
            <th>< ?php echo JrTexto::_("Nombre"); ?></th>
            <th>< ?php echo JrTexto::_("Descripcion"); ?></th>
            <th>< ?php echo JrTexto::_("Fecha calificacion"); ?></th>
            <th class="sorting_disabled"><span class="nobr">< ?php echo JrTexto::_('Actions'); ?></span></th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div-->
<div class="panel">
  <div class="panel-body">
    <div class="col-md-12">
      <select></select>
    </div>
  </div>
  <div class="panel-body">
    <div class="col-md-12 text-center">
      <h4><?php echo JrTexto::_('Actividad por Curso'); ?></h4>
    </div>
    <table class="table table-striped table-responsive" id="table_<?php echo $idgui; ?>">
      <thead>
        <tr class="headings">
          <th>#</th>
          <th>DNI: <?php echo ucfirst(JrTexto::_("Student")); ?></th>
          <th><?php echo JrTexto::_("Date"); ?></th>
          <th><?php echo JrTexto::_("Course"); ?></th>
          <th><?php echo JrTexto::_("Teacher"); ?></th>
          <th><?php echo JrTexto::_("Ubication"); ?></th>
          <th><?php echo JrTexto::_("Foto"); ?></th>
          <th><?php echo JrTexto::_("Estado"); ?></th>
          <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 0;
        $url = $this->documento->getUrlBase();
        if (!empty($personal))
          foreach ($personal as $per) {
            $dt = $this->datosAlumno($per["idalumno"]);
            $i++;
        ?>
          <tr data-id="<?php echo @$per["idalumno"]; ?>">
            <td><?php echo $i; ?></td>
            <td class="fullnombre"><b><?php echo @$per["idalumno"] . "</b><br>" . @$per["stralumno"]; ?></td>
            <td><?php echo @$dt["telefono"] . "<br>" . @$dt["celular"]; ?></td>
            <td><?php echo @$per["strcurso"]; ?></td>
            <td><?php echo @$per["strdocente"]; ?></td>
            <?php if (empty($ventanapadre)) { ?>
              <!--td><?php //echo @$dt["email"]; 
                      ?></td--><?php } ?>
            <td><?php echo @$dt["strlocal"] . "<br>" . @$dt["strambiente"]; ?></td>
            <td><img class="img-circle img-responsive" src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo !empty($dt["foto"]) ? $dt["foto"] : 'user_avatar.jpg' ?>" style="max-height:40px; max-width:40px;"></td>
            <td><a href="javascript:;" class="btn-chkoption" campo="estado" data-id="<?php echo @$per["idalumno"]; ?>"> <i class="fa fa<?php echo @$dt["estado"] == 1 ? '-check' : ''; ?>-circle-o fa-lg"></i> <?php echo @$estados[$dt["estado"]]; ?></a>
            </td>
            <td class="text-center">
              <?php if ($ismodal && !empty($ventanapadre)) { ?>
                <a class="btn-selected btn btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected')); ?>"><i class="fa fa-hand-o-down"></i></a>
              <?php } else { ?>
                <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/perfil/?idpersona=<?php echo $per["idalumno"]; ?>" data-titulo="<?php echo JrTexto::_('Ficha') . " " . @$per["stralumno"]; ?>"><i class="fa fa-user"></i><i class="fa fa-eye"></i></a>
                <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/formulario/?idpersona=<?php echo $per["idalumno"]; ?>&rol=alumno&datareturn=false;" data-titulo="<?php echo JrTexto::_('Personal') . " " . JrTexto::_('Edit'); ?>"><i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>
                <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?idpersona=<?php echo $per["idalumno"]; ?>" data-titulo="<?php echo JrTexto::_('Change') . " " . JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
                <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?idpersona=<?php echo $per["idalumno"]; ?>" data-titulo="<?php echo JrTexto::_('View') . " " . JrTexto::_('notes'); ?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>
                <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horarios/?idpersona=<?php echo $per["idalumno"]; ?>" data-titulo="<?php echo JrTexto::_('View') . " " . JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a>
                <!--a class="btn-eliminar btn btn-xs" href="javascript:;" data-titulo="<?php echo JrTexto::_('delete'); ?>" data-id="<?php //echo @$per["idalumno"]; 
                                                                                                                                      ?>" ><i class="fa fa-trash-o"></i></a-->
              <?php } ?>
            </td>
          </tr>
        <?php }    ?>
      </tbody>
    </table>
  </div>
</div>

<!-- <script type="text/javascript">
  var tabledatos5e42ce6b9d73c = '';

  function refreshdatos5e42ce6b9d73c() {
    tabledatos5e42ce6b9d73c.ajax.reload();
  }
  $(document).ready(function() {
    var estados5e42ce6b9d73c = {
      '1': '<?php echo JrTexto::_("Active") ?>',
      '0': '<?php echo JrTexto::_("Inactive") ?>',
      'C': '<?php echo JrTexto::_("Cancelled") ?>'
    }
    var tituloedit5e42ce6b9d73c = '<?php echo ucfirst(JrTexto::_("tareas")) . " - " . JrTexto::_("edit"); ?>';
    var draw5e42ce6b9d73c = 0;
    var _imgdefecto = '';
    var ventana_5e42ce6b9d73c = $('#ventana_<?php echo $idgui; ?>');
    tabledatos5e42ce6b9d73c = ventana_5e42ce6b9d73c.find('.table').DataTable({
      "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns": [{
          'data': '#'
        },
        {
          'data': '<?php echo JrTexto::_("Tipo"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Idcurso"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Idsesion"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Idpestania"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Idalumno"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Recurso"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Tiporecurso"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Nota"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Notabaseen"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Iddocente"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Idproyecto"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Nombre"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Descripcion"); ?>'
        },
        {
          'data': '<?php echo JrTexto::_("Fecha_calificacion"); ?>'
        },

        {
          'data': '<?php echo JrTexto::_("Actions"); ?>'
        },
      ],
      "ajax": {
        url: _sysUrlBase_ + 'json/tareas',
        type: "post",
        data: function(d) {
          d.json = true
          //d.texto=$('#texto').val(),

          draw5e42ce6b9d73c = d.draw;
          // console.log(d);
        },
        dataSrc: function(json) {
          var data = json.data;
          json.draw = draw5e42ce6b9d73c;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for (var i = 0; i < data.length; i++) {

            datainfo.push({
              '#': (i + 1),
              '<?php echo JrTexto::_("Tipo"); ?>': data[i].tipo,
              '<?php echo JrTexto::_("Idcurso"); ?>': data[i].idcurso,
              '<?php echo JrTexto::_("Idsesion"); ?>': data[i].idsesion,
              '<?php echo JrTexto::_("Idpestania"); ?>': data[i].idpestania,
              '<?php echo JrTexto::_("Idalumno"); ?>': data[i].idalumno,
              '<?php echo JrTexto::_("Recurso"); ?>': data[i].recurso,
              '<?php echo JrTexto::_("Tiporecurso"); ?>': data[i].tiporecurso,
              '<?php echo JrTexto::_("Nota"); ?>': data[i].nota,
              '<?php echo JrTexto::_("Notabaseen"); ?>': data[i].notabaseen,
              '<?php echo JrTexto::_("Iddocente"); ?>': data[i].iddocente,
              '<?php echo JrTexto::_("Idproyecto"); ?>': data[i].idproyecto,
              '<?php echo JrTexto::_("Nombre"); ?>': data[i].nombre,
              '<?php echo JrTexto::_("Descripcion"); ?>': data[i].descripcion,
              '<?php echo JrTexto::_("Fecha_calificacion"); ?>': data[i].fecha_calificacion,
              //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<?php echo JrTexto::_("Actions"); ?>': '<a class="btn btn-xs btnvermodal" data-modal="si" href="' + _sysUrlSitio_ + '/tareas/editar/?id=' + data[i].idtarea + '" data-titulo="' + tituloedit5e42ce6b9d73c + '"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="' + data[i].idtarea + '" ><i class="fa fa-trash"></i></a>'
            });
          }
          return datainfo
        },
        error: function(d) {
          console.log(d)
        }
      },
      "language": {
        "url": _sysIdioma_ == 'es' ? _sysUrlStatic_ + "/libs/datatable1.10/idiomas/es.json" : ''
      }
    });

    ventana_5e42ce6b9d73c.on('keydown', '.textosearchlist', function(ev) {
        if (ev.keyCode === 13) refreshdatos5e42ce6b9d73c();
      }).on('blur', '.textosearchlist', function(ev) {
        refreshdatos5e42ce6b9d73c();
      })

      .on('click', '.btnbuscar', function(ev) {
        refreshdatos5e42ce6b9d73c();
      })


    ventana_5e42ce6b9d73c.on('click', '.btn-eliminar', function() {
      var id = $(this).attr('data-id');
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action'); ?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept'); ?>',
        cancelButton: '<?php echo JrTexto::_('Cancel'); ?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function() {
          var data = new FormData()
          data.append('idtarea', id);
          __sysAyax({
            fromdata: data,
            url: _sysUrlBase_ + 'json/tareas/eliminar',
            callback: function(rs) {
              if (rs.code == 200) {
                tabledatos5e42ce6b9d73c.ajax.reload();
              }
            }
          });
        }
      })
    }).on('click', '.subirfile', function(ev) {
      __subirfile({
        file: $(this),
        dataurlsave: 'json/tareas/setcampo',
        'masvalores': {
          idtarea: $(this).attr('idpk'),
          campo: 'imagen'
        }
      });
    }).on('click', '.btn-activar', function() {
      _this = $(this);
      var activo = 1;
      if (_this.hasClass('active')) {
        activo = 0;
      }
      var data = new FormData();
      data.append('idtarea', _this.attr('data-id'));
      data.append('campo', _this.attr('data-campo'));
      data.append('valor', activo);
      __sysAyax({
        fromdata: data,
        url: _sysUrlBase_ + 'json/tareas/setcampo',
        showmsjok: true,
        callback: function(rs) {
          if (rs.code == 200) {
            tabledatos5e42ce6b9d73c.ajax.reload();
          }
        }
      });
    }).on('click', '.btnvermodal', function(e) {
      e.preventDefault();
      var url = $(this).attr('href');
      if (url.indexOf('?') != -1) url += '&plt=modal';
      else url += '?plt=modal';
      var titulo = $(this).attr('data-titulo') || '';
      var _md = __sysmodal({
        'url': url,
        'titulo': titulo
      });
      var refresh = 'refresh' + __idgui();
      _md.on(refresh, function(ev) {
        tabledatos5e42ce6b9d73c.ajax.reload();
        _md.trigger('cerramodal');
      })
      _md.on('shown.bs.modal', function() {
        var formventana = _md.find('.formventana');
        formventana.attr('datareturn', refresh).addClass('ismodaljs');
        if (formventana.hasClass('form-horizontal')) {
          if (_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'), function() {
              frm(_md);
            });
        } else {
          if (_md.find('#cargaimportar').length)
            $.getScript(_md.find('#cargaimportar').attr('src'), function() {
              if (_md.find('#cargarscript').length)
                $.getScript(_md.find('#cargarscript').attr('src'), function() {
                  frm_import(_md);
                })
            });
        }
      })
    })
  });
</script> -->