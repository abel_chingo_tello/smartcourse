<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("Rubricas"); ?>'
        });
        addButonNavBar({
            id: 'compose',
            class: 'btn btn-success btn-sm Agreggarforo',
            text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('Nuevo') ?>',
            attr: {
                "data-toggle": "modal"
            },
            attr: {
                "data-target": "#md-nueva-rubrica"
            },
            click: `modalRubrica()`
        });
    </script>
<?php } ?>
<div class="container my-font-all">
    <div class="col-sm-12 my-x-center">
        <select id="selectRubrica">
        </select>
    </div>
    <div id="main-cont" class="row col-sm-12">
        <div id="row-cont-rubrica-x" class="col-sm-12 row-cont-rubrica">
            <div class="cont-rubrica my-card my-shadow">
                <h3 class="text-center my-full-w my-rm-link my-italic">Cargando...</h3>
            </div>
        </div>

    </div>
    <div class="my-hide row col-sm-12">
        <div id="row-cont-rubrica" class="col-sm-12 row-cont-rubrica">
            <div class="cont-rubrica my-card my-shadow">
                <h3 onclick="modalRubrica(this)" class="text-center my-full-w">Titulo de rubrica</h3>
                <div id="out-cont" class="out-cont">
                    <div id="body-rubrica" class="body-cont-rubrica rubrica-black tema-4">
                        <div id="rubrica-code" class="cod-rubrica">
                            <div class="code-text">Código</div>
                            <div class="code-num"></div>
                        </div>
                        <div id="cont-btn-crit" onclick="modalCriterio(this)" class="cont-btn-crit">
                            <div class="criterio-text">Detalle</div>
                            <div class="criterio-num">5%</div>
                        </div>
                        <div id="cont-btn-nivel" onclick="modalNivel(this)" name="cont-btn-nivel" class="cont-btn-nivel">
                            <div class="nivel-text">Excelente</div>
                            <div class="nivel-num">20</div>
                        </div>
                        <div id="cont-btn-indicador" onclick="modalIndicador(this)" class="cont-btn-indicador">
                            <div class="indicador-text">Detalle</div>
                        </div>
                        <button id="btn-new-indicador" onclick="modalIndicador(this)" class="btn-new cont-btn-indic">
                            <span class="btn-icon">+</span>
                            <span class="btn-text">Descriptor</span>
                        </button>
                        <button id="btn-new-criterio" onclick="modalCriterio(this)" class="cont-btn-crit btn-new">
                            <span class="btn-icon">+</span>
                            <span class="btn-text">Criterio</span>
                        </button>
                        <button id="btn-new-nivel" onclick="modalNivel(this)" class="cont-btn-nivel btn-new">
                            <span class="btn-icon">+</span>
                            <span class="btn-text">Nivel</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Nueva Rubrica-->
    <div class="modal fade my-modal" id="md-nueva-rubrica" tabindex="-1" role="dialog" aria-labelledby="md-nueva-rubricaLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Rúbrica
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-rubrica" onsubmit="return saveRubrica();">
                        <input id="input-idrubrica" class="my-hide" type="text" disabled>
                        <div class="form-group">
                            <label for="nombre-rubrica">Nombre</label>
                            <input required type="text" name="nombre-rubrica" id="nombre-rubrica" class="form-control" placeholder="Nombre de la rúbrica...">
                        </div>
                        <div class="form-group my-hide">
                            <select class="form-control" name="" id="selectCompetencia">
                                <option disabled value="">Competencia</option>
                            </select>
                        </div>
                        <div class="form-group my-hide">
                            <button id="btn-save" onclick="addSelectCompetencia()" type="button" class="btn btn-primary my-float-r">+ Competencia</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarRubrica()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button id="btn-save" type="submit" form="form-add-rubrica" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" form="form-add-rubrica" class="btn btn-primary">Crear</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Nivel-->
    <div class="modal fade my-modal" id="md-nivel" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Nivel
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-nivel" onsubmit="return saveNivel();">
                        <input id="input-idnivel" class="my-hide" disabled type="text">
                        <div class="form-group">
                            <label for="cuantitativo">Nivel cualitativo</label>
                            <input required type="text" name="cualitativo" id="cualitativo" class="form-control" placeholder="Calificación cualitativa...">
                        </div>
                        <div class="form-group">
                            <label for="cuantitativo">Nivel cuantitativo</label>
                            <input required type="number" name="cuantitativo" id="cuantitativo" class="form-control" placeholder="Calificación cuantitativa...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarNivel()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button id="btn-save" type="submit" form="form-add-nivel" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" form="form-add-nivel" class="btn btn-primary">Crear</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Criterio-->
    <div class="modal fade my-modal" id="md-criterio" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Criterio
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-criterio" onsubmit="return saveCriterio();">
                        <input id="input-idcriterio" class="my-hide" disabled type="text">
                        <div class="form-group my-hide">
                            <select class="form-control" name="" id="selectCapacidad">
                                <option disabled value="">Capacidad</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nombre-criterio">Nombre</label>
                            <textarea required type="text" name="nombre-criterio" id="nombre-criterio" class="form-control" placeholder="Nombre del criterio..."></textarea>
                            <!-- <input required type="text" name="nombre-criterio" id="nombre-criterio" class="form-control" placeholder="Nombre del criterio..."> -->
                        </div>
                        <div class="form-group">
                            <label for="peso-criterio">Peso</label>
                            <input type="number" name="peso-criterio" id="peso-criterio" class="form-control" placeholder="Peso del criterio...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarCriterio()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button id="btn-save" type="submit" form="form-add-criterio" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" form="form-add-criterio" class="btn btn-primary">Crear</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Indicador-->
    <div class="modal fade my-modal" id="md-indicador" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Indicador
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-no-scroll">
                    <form id="form-add-indicador" onsubmit="return saveIndicador(this);">
                        <input id="input-idindicador" class="my-hide" type="text" disabled>
                        <div class="form-group my-hide">
                            <select class="form-control" name="" id="selectCriterios">
                                <option disabled value="">Buscar Criterio</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="detalle-indicador">Detalle</label>
                            <textarea required type="text" name="detalle-indicador" id="detalle-indicador" class="form-control" placeholder="Detalle..."></textarea>
                            <!-- <input required type="text" name="detalle-indicador" id="detalle-indicador" class="form-control" placeholder="Detalle..."> -->
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <div id="autor-btn" class="my-hide">
                                <button type="button" onclick="eliminarIndicador()" class="btn-delete btn btn-danger  my-float-l" data-dismiss="modal"><i class="fa fa-trash my-color-white" aria-hidden="true"></i> Eliminar</button>
                                <button id="btn-save" type="submit" form="form-add-indicador" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="default-btn" class="my-hide">
                                <button type="button" class="btn btn-secondary my-float-r" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div id="new-btn" class="col-sm-12 my-hide">
                            <div class="row my-block">
                                <div class="my-float-r">
                                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" form="form-add-indicador" class="btn btn-primary">Crear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(() => {
        // getRubrica();
        getSelectRubrica();
        $('#md-nueva-rubrica').on('hidden.bs.modal', function(e) {
            clearForm(e);
        })
        $('#md-nivel').on('hidden.bs.modal', function(e) {
            clearForm(e);
        })
        $('#md-criterio').on('hidden.bs.modal', function(e) {
            clearForm(e);
        })
        $('#md-indicador').on('hidden.bs.modal', function(e) {
            clearForm(e);
        })
    })
</script>