<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php //echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/local">&nbsp;<?php echo JrTexto::_('Local'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
         <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="formventana form-horizontal form-label-left"  idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idlocal" id="idlocal" value="<?php echo $this->pk;?>">
          <div class="row">
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Dre');?> <span class="required"> * </span></label>
              <div class="">
                <div class="select-ctrl-wrapper select-azul">
                <select id="iddre" class="form-control select-ctrl " name="iddre" datavalue="<?php echo @$this->iddre;?>" ></select>
                </div>
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Ugel');?> <span class="required"> * </span></label>
              <div class="">
                <div class="select-ctrl-wrapper select-azul">
                <select id="idugel" class="form-control select-ctrl " name="idugel" datavalue="<?php echo @$frm["idugel"];?>" ></select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>
            <div class="">
              <input type="text"  id="txtNombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
            </div>
          </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Direccion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtDireccion" name="direccion" required="required" class="form-control" value="<?php echo @$frm["direccion"];?>">
                                  
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-4 col-sm-12">
                <label class="control-label"><?php echo JrTexto::_('Distrito');?> <span class="required"> * </span></label>
                <div class="">
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="id_ubigeo" class="form-control select-ctrl " name="id_ubigeo" datavalue="<?php echo @$frm["id_ubigeo"];?>" ></select>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-4 col-sm-12">
                <label class="control-label"><?php echo JrTexto::_('Type');?> <span class="required"> * </span></label>
                <div class="">
                  <div class="select-ctrl-wrapper select-azul">
                  <select id="tipo" class="form-control select-ctrl " name="tipo" datavalue="<?php echo @$frm["tipo"];?>" >
                    <option value="C"  <?php echo @$frm["tipo"]=='C'?'selected="selected"':''; ?>><?php echo JrTexto::_('IIEE');?></option>
                    <option value="L" <?php echo @$frm["tipo"]=='L'?'selected="selected"':''; ?>><?php echo JrTexto::_('local'); ?></option>
                  </select>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-4 col-sm-12">
                <label class="control-label"><?php echo JrTexto::_('N° Vacantes');?> </label>
                <input type="text"  id="txtVacantes" name="vacantes" required="required" class="form-control" value="<?php echo @$frm["vacantes"];?>">
              </div>
            </div>

        
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveLocal" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('local'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>

<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm_local.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui; ?>');
    if(!frm.hasClass('ismodaljs')) frm_local($('#vent-<?php echo $idgui; ?>'));
  })
</script>