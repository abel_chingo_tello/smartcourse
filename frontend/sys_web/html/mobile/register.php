<div class="wf-container wf-img_background">
      <div id="wf-mask-background">
        <div id="svg-login-top"></div>
        <div class="wf-container-form">
          <div id="wf-logo"></div>
          <h3 class="wf-title white"><?php echo JrTexto::_('register')?></h3>
          <form action="">
            <div class="wf-input-group">
              <label for="input_user" class="label_input white"><?php echo JrTexto::_('country')?>*</label>
              <div class="container-input">
                <select class="wf-select" name="cars" id="cars">
                  <option value="volvo">Volvo</option>
                  <option value="saab">Saab</option>
                  <option value="opel">Opel</option>
                  <option value="audi">Audi</option>
                </select>
              </div>
            </div>
            <div class="wf-input-group">
              <label for="input_user" class="label_input white"
                ><?php echo JrTexto::_('document type')?>*</label
              >
              <div class="container-input">
                <select class="wf-select" name="cars" id="cars">
                  <option value="volvo">Volvo</option>
                  <option value="saab">Saab</option>
                  <option value="opel">Opel</option>
                  <option value="audi">Audi</option>
                </select>
              </div>
            </div>
            <div class="wf-input-group">
              <label for="input_user" class="label_input white"
                ><?php echo JrTexto::_('document number')?>*</label
              >
              <div class="container-input">
                <input id="input_code" class="wf-input" type="text" />
              </div>
            </div>

            <div class="wf-divider"></div>

            <div class="wf-input-group">
              <label for="input_user" class="label_input white"><?php echo JrTexto::_('first name')?>*</label>
              <div class="container-input">
                <input id="input_code" class="wf-input" type="text" />
              </div>
            </div>
            <div class="wf-input-group">
              <label for="input_user" class="label_input white"
                ><?php echo JrTexto::_('first surname')?>*</label
              >
              <div class="container-input">
                <input id="input_code" class="wf-input" type="text" />
              </div>
            </div>
            <div class="wf-input-group">
              <label for="input_user" class="label_input white"
                ><?php echo JrTexto::_('second surname')?>*</label
              >
              <div class="container-input">
                <input id="input_code" class="wf-input" type="text" />
              </div>
            </div>

            <div class="wf-divider"></div>

            <div class="wf-input-group">
              <label for="input_user" class="label_input white"
                ><?php echo JrTexto::_('cell phone number')?>*</label
              >
              <div class="container-input">
                <input id="input_code" class="wf-input" type="text" />
              </div>
            </div>
            <div class="wf-input-group">
              <label for="input_user" class="label_input white"><?php echo JrTexto::_('email')?></label>
              <div class="container-input">
                <input id="input_code" class="wf-input" type="text" />
              </div>
            </div>
            <button class="wf-btn wf-btn-primary"><?php echo JrTexto::_('to register')?></button>
            <button class="wf-btn wf-btn-outline">
              <?php echo JrTexto::_('i am already registered, log in')?>
            </button>
          </form>
        </div>
        <button id="btn-bottom" class="wf-btn wf-btn-link">
          <?php echo JrTexto::_('i wont use my code just login')?>
        </button>
        <div id="svg-login-bottom"></div>
      </div>
    </div>