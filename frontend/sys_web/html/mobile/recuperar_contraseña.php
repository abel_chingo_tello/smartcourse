<div class="wf-container wf-img_background">
      <div id="wf-mask-background">
        <div id="svg-login-top"></div>
        <div class="wf-container-form">
          <div id="wf-logo"></div>
          <h3 class="wf-title white"><?php echo JrTexto::_('recover password')?></h3>
          <form action="">
            <div class="wf-input-group">
              <label for="input_user" class="label_input white"><?php echo JrTexto::_('User or email')?></label>
              <div class="container-input">
                <span class="icon icon-code"></span>
                <input id="input_code" class="wf-input" type="text" />
              </div>
            </div>
            <button class="wf-btn wf-btn-primary"><?php echo JrTexto::_('recover password')?></button>
          </form>
        </div>
        <button id="btn-bottom" class="wf-btn wf-btn-link">
          <?php echo JrTexto::_('return to login')?>
        </button>
        <div id="svg-login-bottom"></div>
      </div>
    </div>