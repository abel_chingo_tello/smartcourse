<?php  
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->proyecto)) $frm=$this->proyecto;
$ruta="static/media/web/nofoto.jpg";
$imgdefecto=$ruta;
$logodefecto=URL_MEDIA.'static/media/sinlogo.png';
$logo='';
if(!empty($frm["logoempresa"])) $logo=$frm["logoempresa"];
if(!empty($frm["logo"])) $logo=$frm["logo"];
$ifoto=strripos($logo, "static/");
if($ifoto>-1){
    $logo=substr($logo,$ifoto);
    //if(is_file(RUTA_BASE.$logo) && is_readable(RUTA_BASE.$logo)){
        $logo=URL_BASE.$logo;
    //}else 
       // $logo=$logodefecto;
} else $logo=$logodefecto;
$jsonlogin=!empty($frm["jsonlogin"])?json_decode($frm["jsonlogin"],true):array();
?>
<style>
#wf-logo {   
    background-image: url(<?php echo $logo; ?>);
}
.wf-img_background {
    <?php if(!empty($jsonlogin["tipofondo"]) && !empty($jsonlogin["imagenfondo"])){ ?>
        background-image: url(<?php echo $jsonlogin["imagenfondo"];?>);
    <?php } ?>    
}
.wf-container {
    <?php if(!empty($jsonlogin["tipofondo"]) && !empty($jsonlogin["imagenfondo"])){ ?>
        background-color: <?php echo $jsonlogin["colorfondo"];?>;
    <?php } ?>
}
</style>
<div class="wf-container wf-img_background" id="panellogin">
     <div id="wf-mask-background">
         <div id="svg-login-top"></div>
         <div class="wf-container-form">
             <div id="wf-logo"></div>
             <h3 class="wf-title white"><?php echo JrTexto::_('Log in');?></h3>
             <form method="post" id="frmlogin">
                <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
                <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
                <input type="hidden" name="idpersona" id="idpersona" value="">
                 <div class="wf-input-group">
                     <label for="input_user" class="label_input white"><?php echo JrTexto::_('user')?></label>
                     <div class="container-input">
                         <span class="icon icon-user"></span>
                         <input  placeholder="<?php echo JrTexto::_('user')?>" required="required" name="usuario" id="usuario" class="wf-input" type="text" />
                     </div>
                 </div>
                 <div class="wf-input-group">
                     <label for="input_pass" class="label_input white"><?php echo JrTexto::_('Password')?></label>
                     <div class="container-input">
                         <span class="icon icon-pass"></span>
                         <input placeholder="<?php echo JrTexto::_('Password')?>" required="required" name="clave" id="clave" class="wf-input" type="password" />
                     </div>
                 </div>

                 <button class="wf-btn wf-btn-primary" type="submit" id="#btn-enviar-formInfoLogin" ><?php echo JrTexto::_('login')?></button>
                
             </form>
             <button class="wf-btn wf-btn-outline btnactivarcodigosuscripcion"><?php echo JrTexto::_('Activate subscription code')?></button>
         </div>
         <button id="btn-bottom" class="wf-btn wf-btn-link btnactivarsuscripcion">
         <?php echo JrTexto::_('Restore password')?>
         </button>
         <div id="svg-login-bottom"></div>
     </div>
</div>
<div class="wf-container wf-img_background" id="panelvalidarcodigo" style="display:none;">
   <div id="wf-mask-background">
     <div id="svg-login-top"></div>
     <div class="wf-container-form">
          <div id="wf-logo"></div>
          <h3 class="wf-title white"><?php echo JrTexto::_('Valida tu código aquí')?></h3>
          <form method="post" id="formcodigosuscripcion" onsubmit="return false;">
            <input type="hidden" name="idempresa" value="<?php echo @$frm["idempresa"];?>">
            <input type="hidden" name="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
            <div class="wf-input-group">
              <label for="input_user" class="label_input white"><?php echo JrTexto::_('Code')?></label>
              <div class="container-input">
                <span class="icon icon-code"></span>
                <input name="codigo" id="codigo" class="wf-input" type="text"  style="text-transform:uppercase;" onkeypress="return validarcodigos(event)" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="<?php echo JrTexto::_('Code')?>" required="required" maxlength="150"  />
              </div>
            </div>
            <button class="wf-btn wf-btn-primary" id="#btn-enviar-formcodigosuscripcion"><?php echo JrTexto::_('Validate code')?></button>
          </form>
        </div>
     <button id="btn-bottom" class="wf-btn wf-btn-link btnvolveralogin">
     <?php echo JrTexto::_('Return to login')?>
     </button>
     <div id="svg-login-bottom"></div>
   </div>
</div>
<div class="wf-container wf-img_background" id="panelregistrar" style="display:none;">
      <div id="wf-mask-background">
        <div id="svg-login-top"></div>
        <div class="wf-container-form">
          <div id="wf-logo"></div>
          <h3 class="wf-title white"><?php echo JrTexto::_(' Register')?></h3>
          <form method="post" id="formnewregistro" onsubmit="return false;">
            <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
            <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
            <input type="hidden" name="idpersona" id="idpersona" value="">
            <div class="wf-input-group pnlshow">
              <label id="label_pais" for="idpais" class="label_input white"><?php echo JrTexto::_('Country'); ?>*</label>
              <div class="container-input">
                <select name="idpais" id="idpais"  class="wf-select idpais w3-small data-required">                   
                </select>
              </div>
            </div>
            <div class="wf-input-group pnlshow">
              <label for="tipodocumento"  id="label_tipodocumento" class="label_input white"
                ><?php echo JrTexto::_('Document type'); ?>*</label>
              <div class="container-input">
                <select name="tipodocumento" id="tipodocumento" class="form-control cmbtipodocumento w3-small data-required wf-select"   >
                   
                </select>
              </div>
            </div>
            <div class="wf-input-group pnlshow">
              <label  id="label_numdocumento" for="numdocumento" class="label_input white"
                ><?php echo JrTexto::_('Document number'); ?>*</label>
              <div class="container-input">
                <input type="text" name="numdocumento" id="numdocumento" class="wf-input txtdocumento data-required" type="text" maxlength="8" placeholder="99999999" onkeyup="/*buscarPersonaExterna(this, 2, 8)*/" data-longitud="8" onblur="/*fillzerofieldDNI(this)*/"/>
              </div>
            </div>

            <div class="wf-divider"></div>

            <div class="wf-input-group pnlshow">
              <label id="label_primer_apellido" for="apellido1" class="label_input white"><?php echo JrTexto::_('Last name'); ?>*</label>
              <div class="container-input">
                <input name="apellido1" id="apellido1"  class="wf-input txtapellido1 data-required" type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Paterno" autocomplete="off"/>
              </div>
            </div>
            <div class="wf-input-group pnlshow">
              <label id="label_apellido2" for="apellido2" class="label_input white"
                ><?php echo JrTexto::_('Second surname'); ?>*</label
              >
              <div class="container-input">
                <input name="apellido2" id="apellido2" class="wf-input txtapellido1 data-required" type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Materno" autocomplete="off" />
              </div>
            </div>
            <div class="wf-input-group">
              <label id="label_nombres" for="nombres" class="label_input white"
                ><?php echo JrTexto::_('First name'); ?>*</label
              >
              <div class="container-input">
                <input name="nombres" id="nombres" class="wf-input txtnombres data-required" type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Nombres" autocomplete="off"/>
              </div>
            </div>
            <div class="wf-input-group pnlshow" style="display: none;">
              <label for="sexo" class="label_input white"><?php echo JrTexto::_('Gender'); ?>*</label>
              <div class="container-input">
                <select name="sexo" id="sexo"  class="wf-select txtsexo w3-small data-required"  >
                <option value="M"><?php echo JrTexto::_("Male");?></option>
                <option value="F"><?php echo JrTexto::_("Female");?></option>
                </select>
              </div>
            </div>
            <div class="wf-input-group pnlshow" style="display: none;">
              <label for="fecha_nacimiento" class="label_input white"
                ><?php echo JrTexto::_('Birth date'); ?>*</label
              >
              <div class="container-input">
                <input type="date" name="fecha_nacimiento"  id="fecha_nacimiento" class="wf-input"  max="<?php echo (date('Y')-5).date('-m-d'); ?>" min="<?php echo (date('Y')-70)."-01-01"; ?>" value=""/>
              </div>
            </div>
            <div class="wf-input-group pnlshow" style="display: none;">
              <label for="edad" class="label_input white"
                ><?php echo JrTexto::_('Age'); ?>*</label
              >
              <div class="container-input">
                <input type="text" class="form-control" name="edad" id="edad" value="" autocomplete="off" readonly="readonly"/>
              </div>
            </div>

            <div class="wf-divider"></div>

            <div class="wf-input-group pnlshow">
              <label id="label_telefono" for="telefono" class="label_input white"
                ><?php echo JrTexto::_('Cell phone number'); ?>*</label
              >
              <div class="container-input">
                <div style="position: relative; width: 100px;">                                    
                    <span class="" style="position: absolute; bottom: 2ex; left:1ex !important;" id="paistelcode"></span>
                </div>
                <input name="telefono" id="telefono" value="" class="wf-input" type="text" autocomplete="off"  data-longitud="15" maxlength="15" placeholder="99999999"  />                
              </div>
            </div>
            <div class="wf-input-group pnlshow">
              <label id="label_correo" for="input_user" class="label_input white"><?php echo JrTexto::_('Email'); ?>*</label>
              <div class="container-input">
                <input  class="wf-input"  type="email" class="form-control" name="correo" id="correo" value="" autocomplete="off" placeholder="Ej: tucorreo@empresa.com"/>
              </div>
            </div>
            <div class="wf-input-group pnlshow" style="display: none;">
              <label  for="gradoinstruccion" class="label_input white"><?php echo JrTexto::_('Academic level'); ?>*</label>
              <div class="container-input">
                <input  class="wf-input"  name="gradoinstruccion" id="gradoinstruccion" class="form-control"   value="" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="" autocomplete="off" placeholder="<?php echo JrTexto::_('teacher');  ?>" />
              </div>
            </div>
            <button type="submit" id="#btn-enviar-newregistro" class="wf-btn wf-btn-primary"><?php echo JrTexto::_(' Register')?></button>
            <button href="#tologin" class="to_recuperar" class="wf-btn wf-btn-outline returntologinconcodigo" style="z-index: 999">
            <?php echo JrTexto::_('I am already registered, log in');?>
            </button>
          </form>
        </div>
        <button id="btn-bottom" href="#tologin"  class="wf-btn wf-btn-link returntologinsincodigo to_recuperar ">
        <?php echo JrTexto::_("I won't use my code, just log in") ?>.
        </button>
        <div id="svg-login-bottom"></div>
      </div>
</div>
<div class="wf-container wf-img_background" id="panelrecuperarcontraseña" style="display:none;">
  <div id="wf-mask-background">
    <div id="svg-login-top"></div>
    <div class="wf-container-form">
      <div id="wf-logo"></div>
      <h3 class="wf-title white"><?php echo JrTexto::_('Recover password')?></h3>
      <form method="post" id="formRecupClave" onsubmit="return false;">
        <input type="hidden" name="idempresa" value="<?php echo @$frm["idempresa"];?>">
        <input type="hidden" name="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
        <div class="wf-input-group">
          <label for="input_user" class="label_input white"><?php echo JrTexto::_('User or email')?></label>
          <div class="container-input">
            <span class="icon icon-code"></span>
            <input placeholder="" name="usuario" id="usuariorecover" class="wf-input" type="text" />
          </div>
        </div>
        <button class="wf-btn wf-btn-primary" id="#btn-enviar-formInforecuperarclave"><?php echo JrTexto::_('Recover password')?></button>
      </form>
    </div>
    <button id="btn-bottom" class="wf-btn wf-btn-link btnvolveralogin">
    <?php echo JrTexto::_('Return to login')?>
    </button>
    <div id="svg-login-bottom"></div>
  </div>
</div>

 <script type="text/javascript" src="<?php echo URL_BASE; ?>static/libs/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
    var jsonvalidarcodigo=null;
    var _sysUrlsks_='<?php echo _http_.DOMINIO_SKS; ?>/json/';
    var fechaservidor='<?php echo date('Y-m-d');?>';

    var cargopaises=false;
    var idrepresentante=null;
    var cantidad_descarga=null;
    var label_pais='<?php echo JrTexto::_('Country'); ?> (*)';
    var label_tipodocumento='<?php echo JrTexto::_('Document type'); ?> (*)';
    var label_numdocumento='<?php echo JrTexto::_('Document number'); ?> (*)';
    var label_primer_apellido='<?php echo JrTexto::_('Last name'); ?> (*)';
    var label_apellido2='<?php echo JrTexto::_('Second surname'); ?>';
    var label_nombres='<?php echo JrTexto::_('First name'); ?> (*)';
    var label_sexo='<?php echo JrTexto::_('Gender'); ?>';
    var label_telefono='<?php echo JrTexto::_('Cell phone number'); ?> (*)';
    var label_correo='<?php echo JrTexto::_('Email'); ?> (*)';
    var idempresa='<?php echo @$frm["idempresa"];?>';
    var idproyecto='<?php echo @$frm["idproyecto"];?>';

    var activarsuscripcion=function(datos){   
        var data=new FormData();
            data.append('usuario',datos.usuario); 
            data.append('numerodocumento',datos.dni);
            data.append('tipodocumento',datos.tipodocumento);
            data.append('codigosuscripcion',datos.codigosuscripcion);
            data.append('idempresa',idempresa);
            data.append('idproyecto',idproyecto);
            if(datos.idpersona!='' && datos.idpersona!=undefined) data.append('idpersona',datos.idpersona);
            data.append('cantidad_descarga',cantidad_descarga);
            __sysAyax({ 
                fromdata:data, //http://localhost/smartmanager/json/?idrepresentante=2&buscardocumentos=1
                url:_sysUrlBase_+'json/descargas_asignadas/asignar'
            });
    }


    function isNumber(value){ return typeof value === 'number' && !isNaN(value); }
    function isObject(value){ return _typeof(value) === 'object' && value !== null; }
    function isFunction(value){ return typeof value === 'function'; }
    function toArray(value){ return Array.from ? Array.from(value) : slice.call(value); }
    function forEach(data, callback){
      if(data && isFunction(callback)){
        if(Array.isArray(data) || isNumber(data.length) /* array-like */){
            toArray(data).forEach(function(value, key){callback.call(data, value, key, data); });
        }else if(isObject(data)){
          Object.keys(data).forEach(function(key){callback.call(data, data[key], key, data); });
        }
      }
      return data;
    }

    function validarcodigos(e){
         tecla = (document.all) ? e.keyCode : e.which;
        //Tecla de retroceso para borrar, siempre la permite
        if(tecla == 8) {
            return true;
        }
        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9\-]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    function calcularAnios2(fechaNacimiento){
        let fechaNac=new Date(fechaNacimiento);
        var today = new Date();    
        let anios = today.getFullYear() - fechaNac.getFullYear();        
        if (fechaNac.getMonth() > (today.getMonth()) || fechaNac.getDay() > today.getDay())
            anios--;
        return anios;
    }

    function solonumerosyletras(e){
        tecla = (document.all) ? e.keyCode : e.which;
        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }
        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    var cargarpaises=function(){
        //console.log(idrepresentante);
        //idrepresentante=12;
        console.log('idrepresentante',idrepresentante);
        if(idrepresentante==null){  
            $('.returntologinsincodigo').trigger('click');
        }

        //if(cargopaises==false){
            Swal.fire({type:'info', html:'Procesando',willOpen:()=>{Swal.showLoading();}});            
            var data=new FormData();
            data.append('estado',1); 
            data.append('idrepresentante',idrepresentante);
            data.append('buscardocumentos',1);          
              __sysAyax({ 
                fromdata:data, //http://localhost/smartmanager/json/?idrepresentante=2&buscardocumentos=1
                  url:_sysUrlsks_+'representantes_paises/buscardocumentos/?idrepresentante='+idrepresentante+'&buscardocumentos=1',                  
                  callback:function(rs){                    
                    if(rs.code==200){
                        let data=rs.data;
                        $('select#idpais').children('option').remove();
                        $.each(data,function(i,v){
                            if($('#idpais option[value="'+v.idpais+'"]').length==0)
                            $('select#idpais').append(`<option value="${v.idpais}" abrev="${v.abreviacion}" telefono-prefijo="${v.prefijo_telefono||''}" telefono_fijo="${v.cant_telfijo||''}" cant_telmovil="${v.cant_telmovil||''}" >${v.nombre_pais}</option>`);
                        })
                        cargopaises=true;
                        Swal.close(); 
                        $('select#idpais').trigger('change');
                    }else{
                        __notificar({
                            type:'danger',
                            title:'Error',
                            html:rs.msj                     
                        });
                    }
                  }
            });            
        //}
    }

    $('#frmlogin').bind({
         submit: function(ev){
            ev.preventDefault();
            procesandoguardando=true;
            var btn=$(this).find('#btn-enviar-formInforecuperarclave');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            
            var data=new FormData();
            var frm=$('#frmlogin');
            var clave=frm.find('#clave').val();
            var useringresado=frm.find('#usuario').val();
                data.append('usuario',useringresado);
                data.append('clave',clave);
                data.append('idempresa',frm.find('#idempresa').val());
                data.append('idproyecto',frm.find('#idproyecto').val());
                data.append('showmsjok',true);
                __sysAyax({ 
                    fromdata:data,
                    url:_sysUrlBase_+'json/sesion/login',                  
                    callback:function(rs){                                       
                        if(rs.code==200){
                            var datosalumno=rs.data;
                            localStorage.setItem("usersesion", JSON.stringify(rs.data));
                            localStorage.setItem("userempresa", JSON.stringify(<?php echo json_encode($this->proyecto); ?>));
                            localStorage.setItem("userproyecto", JSON.stringify(<?php echo json_encode($this->proyecto['jsonlogin'])?>));
                            __cambiarinfouser();

                            let codigo=frm.find('input#codigosuscripcion').val()||'';
                            let idgrupoaula=frm.find('input#idgrupoaula').val()||'';
                            if(codigo!='' && idgrupoaula!=''){
                                var data=new FormData();
                                    data.append('idgrupoaula',$('#frmlogin').find('#idgrupoaula').val());
                                    data.append('keycode',codigo);
                                __sysAyax({ 
                                    fromdata:data,
                                    url:_sysUrlBase_+'json/api/marticulasxgrupo',
                                    callback:function(rs){
                                        if(rs.code==200){
                                            activarsuscripcion({
                                                usuario:datosalumno.usuario,
                                                dni:datosalumno.dni,
                                                tipodocumento:datosalumno.tipodoc,
                                                codigosuscripcion:codigo,
                                                idpersona:datosalumno.idpersona,
                                            });

                                            let matricula_faltantes=[];
                                            if(rs.datos.matricula_faltantes!=undefined && rs.datos.matricula_faltantes!='')
                                                forEach(rs.datos.matricula_faltantes,function(v,i){
                                                    matricula_faltantes.push(v);
                                                })

                                            let matricula_sobrantes=[];
                                            if(rs.datos.matricula_sobrantes!=undefined && rs.datos.matricula_sobrantes!='')
                                                forEach(rs.datos.matricula_sobrantes,function(v,i){
                                                    matricula_sobrantes.push(v);
                                                })


                                            let cursosdelgrupo=[];
                                            if(rs.datos.cursosdelgrupo!=undefined && rs.datos.cursosdelgrupo!='')
                                                forEach(rs.datos.cursosdelgrupo,function(v,i){
                                                    cursosdelgrupo.push(v);
                                                })

                                                
                                            let mismatriculas=[];
                                            if(rs.datos.mismatriculas!=undefined && rs.datos.mismatriculas!='')
                                                forEach(rs.datos.mismatriculas,function(v,i){
                                                    mismatriculas.push(v.idmatricula);
                                                })
                                            
                                            var data1=new FormData();
                                            if(mismatriculas.length>0) data1.append('idmatricula',JSON.stringify(mismatriculas));
                                            if(matricula_faltantes.length>0) data1.append('matricula_faltantes',JSON.stringify(matricula_faltantes));
                                            if(matricula_sobrantes.length>0) data1.append('matricula_sobrantes',JSON.stringify(matricula_sobrantes));
                                            if(cursosdelgrupo.length>0) data1.append('cursosdelgrupo',JSON.stringify(cursosdelgrupo));

                                            data1.append('target',_sysUrlBase_+'json/');
                                            data1.append('idproyecto',idproyecto);
                                            data1.append('idempresa',idempresa);            
                                            data1.append('keycode',$('#frmlogin').find('#codigosuscripcion').val());
                                            let urlsksextender=_sysUrlsks_+'matriculas/extendersuscripcion';
                                            if(mismatriculas.length==0){
                                                let alumnos=[{                                
                                                    "tipodoc": datosalumno.tipodoc, //este valor debe coincidir con SKS                                                    
                                                    //"tipodoc_str": tipodoc_str, //este nombre debe coincidir con SKS
                                                    "nombres": datosalumno.nombre, //falta
                                                    "apellido1": datosalumno.apellido1, //falta
                                                    "apellido2": datosalumno.apellido2, //falta
                                                    "documento": datosalumno.dni,
                                                    "sexo" : datosalumno.sexo,
                                                    "fechanac": datosalumno.fechanac, //"2020-03-03",
                                                    "instruccion": '', //opcional
                                                    "email": datosalumno.email,
                                                    //"edad" : datosalumno, //opcional en el formulario, pero que sea un valor > 0
                                                    "tipogrupo": 1, //tipo de grupo que lo puede obtener en SC con el idgrupoaula. Si quiere lo deja en constante 1
                                                    "codigoCursoSence": 0, //esto es para sence, siempre en 0 si no es para sence
                                                    "usuario": useringresado, //INicial del nombre Inicial del primer apellido
                                                    "clave": clave,
                                                    //"telefono":telefono,
                                                }];
                                                data1.append('alumnos',JSON.stringify(alumnos));
                                                data1.append('tipo_matricula',2);
                                                urlsksextender=_sysUrlsks_+'matriculas/_matricular';
                                            }else{
                                                data1.append('idpersona',datosalumno.idpersona);
                                                data1.append('tipodoc',datosalumno.tipodoc);
                                            }

                                            __sysAyax({ 
                                                fromdata:data1,
                                                url:urlsksextender,    
                                                callback:function(rs){                                                     
                                                    if(rs.code==200){                                                        
                                                        __cambiarinfouser();                                       
                                                        redir(_sysUrlBase_);
                                                    }else if(rs.code==404){
                                                        Swal.fire({title:`<?php echo JrTexto::_("The code isn't valid or has already been used"); ?>`, icon:'error' }).then(rs=>{
                                                            $('#formcodigosuscripcion').find('#codigo').val('');
                                                            window.location.hash ='#tovalidarcodigo';
                                                        });
                                                    }else{
                                                        Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later'); ?>.`, icon:'error' });
                                                    }                                           
                                                }
                                            });
                                      }                    
                                    }
                                }); 

                            }else redir(_sysUrlBase_);
                        }                        
                    }
                });            
        }
    });

    $('#formcodigosuscripcion').bind({
        submit:function(ev){
            ev.preventDefault();
            var pnl=$(this).closest('.wf-container');
            var btn=$(this).find('#btn-enviar-newregistro');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            var frm=$('#formcodigosuscripcion');
            let codigo=frm.find('#codigo').val();
            var data=new FormData();
            data.append('codigo',codigo);
            data.append('estado',2); //descomentar para validar solño tipo2
            data.append('idempresa',idempresa);
            data.append('idproyecto',idproyecto);
            //b54aad92-cd8c-46f0-b02f-4af7b48574b1
            data.append('showmsjok',true);
              __sysAyax({ 
                fromdata:data,
                  url:_sysUrlsks_+'codigos/buscarcompleto',                  
                  callback:function(rs){                  
                    if(rs.code==200){
                        //console.log(rs.data);
                       // var data=Object.values(rs.data[0]);
                        if(rs.data[0]==null || rs.data==undefined){
                            Swal.fire({
                                title:'<?php echo JrTexto::_("The entered code is not correct"); ?>.',
                                icon:'error'
                            })
                            return ;
                        }
                        if(rs.data[0].codigo!==codigo){
                            Swal.fire({
                                title:'<?php echo JrTexto::_("The entered code is not correct"); ?>.',
                                icon:'error'
                            })
                            return ;
                        }
                        idrepresentante=rs.data[0].idrepresentante||null;
                        cantidad_descarga=rs.data[0].cantidad_descarga||1;
                        Swal.fire({
                          icon:'success',
                          title: '<?php echo JrTexto::_("The entered code is correct, would you like to continue?"); ?>',
                          showDenyButton: true,
                          showCancelButton: true,
                          confirmButtonText: `<?php echo JrTexto::_("Yes, I'd like to log in"); ?>`,
                          denyButtonText: `<?php echo JrTexto::_("Yes, I'd like to register");?>`,
                          cancelButtonText:'<?php echo JrTexto::_("No, cancel it") ?>.',
                        }).then((result) => {
                            if(result.isConfirmed){                        
                                $('#frmlogin').find('#codigosuscripcion').val(codigo);
                                $('#frmlogin').find('#idgrupoaula').val(rs.data[0].idgrupoaula);
                                //window.location.hash ='#tologin';
                                $('#panellogin').show();
                                pnl.hide();
                            }else if(result.isDenied){                              
                                pnl.hide();
                                cargarpaises();
                                $('#panelregistrar').show();
                            }
                        })
                    }else{
                        Swal.fire({
                          title:'<?php echo JrTexto::_("The entered code is not correct"); ?>',
                          icon:'error'
                        })
                    }
                }
            });
        }
    })

    $('#formnewregistro').bind({
        submit:function(ev){
            ev.preventDefault();
            var btn=$(this).find('#btn-enviar-formcodigosuscripcion');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            var frm=$('#formnewregistro');
            let tipodoc=frm.find('select#tipodocumento').val()||'';
            let tipodoc_str=frm.find('select#tipodocumento').children('option[value="'+tipodoc+'"]').text();
            let numdocumento=frm.find('input#numdocumento').val()||'';
            numdocumento=numdocumento.split('_').join('');

            let txttelefono=frm.find('#paistelcode').text();            
            let telefono=frm.find('input#telefono').val()||'';           
            let nombre=frm.find('input#nombres').val()||'';
            let apellido1=frm.find('input#apellido1').val()||'';
            let apellido2=frm.find('input#apellido2').val()||'';

            let user=nombre.substring(0,1).toLowerCase()+apellido1.substring(0,1).toLowerCase()+numdocumento;
            var tmpidpersona=frm.find('input#idpersona').val()||'';

            let pais=frm.find('#idpais');
            let idpais=pais.val()||'';
            let nompais=(pais.children('option[value="'+idpais+'"]').attr('abrev')||'').toLowerCase();
            let fecha_nacimiento=frm.find('input#fecha_nacimiento').val()||'<?php echo date('Y-m-d'); ?>';
            let edad=frm.find('input#edad').val()||1;

            //console.log(nompais);                   
            let revalidar=true;
            if(edad=='') edad=1;
            if(telefono=='') telefono='0000000';
            if(nompais=='usa' || nompais=='br'){
                revalidar=false;
                numdocumento=nompais.toString().toUpperCase()+Date.now();
                tipodoc=14;
                tipodoc_str='Unico';
                fecha_nacimiento='<?php echo date('Y-m-d'); ?>';               
            }else if(nompais=='mex' || nompais=='pe'){
                revalidar=false;
                if(numdocumento=='') numdocumento=nompais.toString().toUpperCase()+Date.now();
                if(apellido2=='') apellido2='-';
            }
            
            
            if(telefono!='') telefono=txttelefono+telefono;
            else telefono=txttelefono+'0000000';

            if(numdocumento=='' || numdocumento == null){
                if(frm.find('input#numdocumento').parent().is(':visible')==true){
                    Swal.fire({title:'<?php echo JrTexto::_('The document number is not valid'); ?>.', icon:'error' }).then(r1=>{
                        frm.find('input#numdocumento').focus();
                    });                
                    return ;
                }else{
                    numdocumento=nompais.toString().toUpperCase()+Date.now();
                }
            }

            var codigo=$('#formcodigosuscripcion').find('#codigo').val();            
            if(codigo==''){
                Swal.fire({title:`<?php echo JrTexto::_('The code is not valid'); ?>.`, icon:'error' }).then(r1=>{
                     window.location.hash ='#tovalidarcodigo';
                });
                return ;
            }

            if(nombre==''){
                Swal.fire({title:`<?php echo JrTexto::_('The name is not valid'); ?>.`, icon:'error' }).then(r1=>{
                    frm.find('input#nombres').focus();
                });
                return ;
            }

            if(apellido1==''){
                Swal.fire({title:`<?php echo JrTexto::_("The first surname isn't valid"); ?>.`, icon:'error' }).then(r1=>{
                    frm.find('input#apellido1').focus();
                });
                return ;
            }
            
            if(apellido2 && revalidar==true){
                if(frm.find('input#apellido2').parent().is(':visible')==true){
                    Swal.fire({title:`<?php echo JrTexto::_("The second surname isn't valid"); ?>.`, icon:'error' }).then(r1=>{
                        frm.find('input#apellido2').focus();
                    });
                    return ;
                }else{
                    apellido2='';
                }
            }
            if(frm.find('input#correo').val()==''){
                Swal.fire({title:`<?php echo JrTexto::_("The email address isn't valid"); ?>.`, icon:'error' }).then(r1=>{
                    frm.find('input#correo').focus()    
                });                
                return ;
            }

            let alumnos=[{
                "idpais":  frm.find('select#idpais').val()||'',              
                "tipodoc": tipodoc, //este valor debe coincidir con SKS
                "tipodoc_str": tipodoc_str, //este nombre debe coincidir con SKS
                "nombres": nombre,
                "apellido1": frm.find('input#apellido1').val()||'',
                "apellido2": apellido2||'-',
                "documento": numdocumento,
                "sexo" : frm.find('select#sexo').val()||'',
                "fechanac": fecha_nacimiento, //"2020-03-03",
                "instruccion": frm.find('input#gradoinstruccion').val()||'', //opcional
                "email": frm.find('input#correo').val()||'',
                "edad" : edad, //opcional en el formulario, pero que sea un valor > 0
                "tipogrupo": 1, //tipo de grupo que lo puede obtener en SC con el idgrupoaula. Si quiere lo deja en constante 1
                "codigoCursoSence": 0, //esto es para sence, siempre en 0 si no es para sence
                "usuario": user, //INicial del nombre Inicial del primer apellido
                "clave": user,
                "telefono":telefono,
            }];
            var data=new FormData();
            var keycode=codigo;
            data.append('alumnos',JSON.stringify(alumnos));
            data.append('target',_sysUrlBase_+'json/');
            data.append('idproyecto',idproyecto);
            data.append('idempresa',idempresa);
            data.append('tipo_matricula',2);
            data.append('keycode',keycode);
            data.append('enviaremail',true);
            data.append('idpais',idpais);
            __sysAyax({ 
                fromdata:data,
                url:_sysUrlsks_+'matriculas/_matricular',
                callback:function(rs){
                    if(rs.code==200){
                        if(rs.correos==undefined || rs.correos==''){
                            return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no mailing list found'); ?>.`, icon:'error'}); 
                        }
                        let cor=rs.correos;
                        if(cor.estudiantes==undefined){
                            return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no student found'); ?>.`, icon:'error'});    
                        }
                        let alumno=cor.estudiantes[0];
                        if(tmpidpersona!=''){
                            Swal.fire({title:'<?php echo JrTexto::_("Your code has been redeemed. Log in using your username and password");?>.', icon:'success'});
                            $('#frmlogin').find('#codigosuscripcion').val('');
                            $('#frmlogin').find('#idgrupoaula').val('');

                            activarsuscripcion({
                                usuario:rs.usuarios[0].usuario,
                                dni:numdocumento,
                                tipodocumento:tipodoc,
                                codigosuscripcion:keycode,
                                //idpersona:datosalumno.idpersona,
                            });
                            $('#panellogin').show();
                            //window.location.hash ='#tologin';
                            return;       
                        }
                       
                        if(alumno==undefined   || alumno=='' || alumno=={} ){
                            let data=rs.data[0]||'';
                            if(data=='' || data==undefined)
                                return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no student found'); ?>.`, icon:'error'});    
                            let per=data.personal[0]||''
                            if(per=='' || per==undefined)
                                return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no student found'); ?>.`, icon:'error'});    
                            let per1=per.personal||''
                            if(per1=='' || per1==undefined)
                                return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no student found'); ?>.`, icon:'error'}); 
                            alumno={
                                usuario:per1.usuario,
                                clave:per1.clavesinmd5
                            }
                        }

                        // su codigo ha sido cangeado, inicie sesion con sus datos de usuario y clave.
                        var data1=new FormData();
                        var frm=$('#frmlogin');
                            data1.append('usuario',alumno.usuario);
                            data1.append('clave',alumno.clave);
                            data1.append('idempresa',idempresa);
                            data1.append('idproyecto',idproyecto);
                            data1.append('showmsjok',true);                           
                            __sysAyax({ 
                                fromdata:data1,
                                url:_sysUrlBase_+'json/sesion/login',                  
                                callback:function(rs){  
                                    $('#frmlogin').find('#codigosuscripcion').val('');
                                    $('#frmlogin').find('#idgrupoaula').val(''); 
                                    var datosalumno=rs.data;
                                    if(rs.code==200){                                     
                                        localStorage.setItem("usersesion", JSON.stringify(rs.data));
                                        localStorage.setItem("userempresa", JSON.stringify(<?php echo json_encode($this->proyecto); ?>));
                                        localStorage.setItem("userproyecto", JSON.stringify(<?php echo $this->proyecto['jsonlogin']; ?>));
                                        __cambiarinfouser();   
                                        activarsuscripcion({
                                            usuario:datosalumno.usuario,
                                            dni:datosalumno.dni,
                                            tipodocumento:datosalumno.tipodoc,
                                            codigosuscripcion:keycode,
                                            idpersona:datosalumno.idpersona,
                                        });
                                        redir(_sysUrlBase_);
                                    }
                                    procesandoguardando=false; 
                                }
                            });
                    }else if(rs.code==404){
                        Swal.fire({title:`<?php echo JrTexto::_("The code isn't valid or has already been used") ?>.`, icon:'error' }).then(rs=>{
                          $(this).closest('.wf-container').hide();
                          $('#panelvalidarcodigo').show();
                            //window.location.hash ='#tovalidarcodigo';
                        });
                    }else{
                        Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later'); ?>.`, icon:'error' });
                    }                    
                }
            });
        }
    })

    $('#formnewregistro #idpais').change(function(e){
        let pais=$(this);
        let idpais=$(this).val();
        Swal.fire({type:'info', html:'Procesando',willOpen:()=>{Swal.showLoading();}});            
        var data=new FormData();
        data.append('estado',1);
        data.append('idpais',idpais);
        __sysAyax({ 
            fromdata:data,
              url:_sysUrlsks_+'tipo_documento',
              callback:function(rs){                    
                if(rs.code==200){
                    let data=rs.data;
                    $('select#tipodocumento').children('option').remove();
                    $.each(data,function(i,v){
                        $('select#tipodocumento').append(`<option value="${v.id}" mask="${v.mask}" longitud="${v.longitud}">${v.nombre}</option>`);
                    })
                    cargopaises=true;
                    let nompais=(pais.children('option[value="'+idpais+'"]').attr('abrev')||'').toLowerCase();

                    $('#label_pais').text(label_pais);
                    $('#label_tipodocumento').text(label_tipodocumento);
                    $('#label_numdocumento').text(label_numdocumento);
                    $('#label_primer_apellido').text(label_primer_apellido);
                    $('#label_apellido2').text(label_apellido2);
                    $('#label_nombres').text(label_nombres);
                    $('#label_sexo').text(label_sexo);
                    $('#label_telefono').text(label_telefono);
                    $('#label_correo').text(label_correo);
                    //console.log(nompais);                   
                   // if(nompais=='usa' || nompais=='mex'){
                        $('#formnewregistro #apellido2').closest('.pnlshow').hide();
                        $('#formnewregistro #apellido2').removeAttr('required').val('-');
                        $('#formnewregistro #gradoinstruccion').closest('.pnlshow').hide();
                        $('#formnewregistro #gradoinstruccion').removeAttr('required').val('-');
                        $('#formnewregistro #telefono').removeAttr('required');
                        $('#formnewregistro #edad').closest('.pnlshow').hide();
                        $('#formnewregistro #tipodocumento').closest('.pnlshow').hide();
                        $('#formnewregistro #numdocumento').closest('.pnlshow').hide();
                        $('#formnewregistro #sexo').closest('.pnlshow').hide();
                        $('#formnewregistro #fecha_nacimiento').closest('.pnlshow').hide();
                        //$('#formnewregistro #correo').closest('.pnlshow').removeClass('col-md-4').addClass('col-md-8');                        
                    //}else{
                    //    $('#newregistro #apellido2').closest('div').show();



                    if(nompais=='mex'){
                        $('#formnewregistro #apellido2').val('').closest('.pnlshow').show()
                        $('#formnewregistro #apellido2').removeAttr('required').removeClass('data-required');
                        $('#label_primer_apellido').text('<?php echo JrTexto::_('First surname'); ?> (*)');
                        $('#formnewregistro #tipodocumento').closest('.pnlshow').show();
                        $('#formnewregistro #numdocumento').closest('.pnlshow').show();
                         $('#label_telefono').text('<?php echo JrTexto::_('Cell phone number'); ?>');

                    }else if(nompais=='usa'){
                        $('#label_pais').text('País / Country (*)');
                        $('#label_primer_apellido').text('Apellido / Last Name (*)');
                        $('#label_nombres').text('Nombre / Name (*)');
                        //$('#label_tipodocumento').text(label_tipodocumento);
                        //$('#label_numdocumento').text(label_numdocumento);
                        //$('#label_apellido2').text(label_apellido2);
                        //$('#label_sexo').text(label_sexo);
                        $('#label_telefono').text('Teléfono / Phone number');
                        $('#label_correo').text('Email / E-mail (*) ');
                    }else if(nompais=='br'){
                        $('#label_pais').text('País / Country (*)');
                        $('#label_primer_apellido').text('Apellidos/ Sobrenomes (*)');
                        $('#label_nombres').text('Nombre / Nome (*)');
                        $('#label_telefono').text('Teléfono / Telefone');
                        $('#label_correo').text('Email / E-mail (*)');
                    }else if(nompais=='pe'){
                        $('#formnewregistro #tipodocumento').closest('.pnlshow').show();
                        $('#formnewregistro #numdocumento').closest('.pnlshow').show();
                        $('#formnewregistro #apellido2').val('').closest('.pnlshow').show()
                        $('#formnewregistro #apellido2').removeAttr('required').removeClass('data-required');


                        //$('#label_pais').text('País / Country (*)');
                        //$('#label_primer_apellido').text('Apellidos/ Sobrenomes (*)');
                        //$('#label_nombres').text('Nombre / Nome (*)');
                        //$('#label_telefono').text('Teléfono / Telefone');
                        //$('#label_correo').text('Email / E-mail');

                   }
                    //    $('#newregistro #gradoinstruccion').closest('div').show();
                    //    $('#newregistro #edad').closest('div').show();
                    
                    //    $('#newregistro #sexo').closest('div').show();
                    //    $('#newregistro #fecha_nacimiento').closest('div').show();
                    //    $('#newregistro #correo').closest('div').removeClass('col-md-8').addClass('col-md-4');                        
                    //    $('#newregistro #telefono').attr('required');
                    //    $('#newregistro #gradoinstruccion').attr('required');
                    //    $('#newregistro #apellido2').val('-');
                    //}*/
                    Swal.close();
                    let cbo=pais.children('option[value="'+pais.val()+'"]');
                    let nmaxtel=parseInt(cbo.attr('telefono_fijo')||10);
                    let nmaxcel=parseInt(cbo.attr('cant_telmovil')||9);
                    let elmax=nmaxtel>nmaxcel?nmaxtel:nmaxcel;
                    let mask='';
                    for(i=1;i<=elmax;i++){
                        mask=mask+'0';
                    }

                    $('#formnewregistro #telefono').attr('placeholder',mask).attr('maxlength',cbo.attr('longitud'));//.inputmask({"mask": mask});
                    $('#paistelcode').text(cbo.attr('telefono-prefijo')||'');
                    $('#formnewregistro #tipodocumento').trigger('change');
                }else{
                    __notificar({
                        type:'danger',
                        title:'Error',
                        html:rs.msj                     
                    });
                }
              }
        });
    })

    $('#formnewregistro #tipodocumento').change(function(e){
        let iddocumento=$(this).val();
        let option=$(this).children('option[value="'+iddocumento+'"]');
        let mask=option.attr('mask')||'********************';
        if(mask=='' || mask=='null' || mask==null) mask='********************';
        $('#formnewregistro #numdocumento').attr('placeholder',mask).attr('maxlength',option.attr('longitud')).inputmask({"mask": mask});
        let numdoc=$('#formnewregistro #numdocumento').val()||'';
        if(numdoc!='') $('#formnewregistro #numdocumento').trigger('blur');
    })

    $('#formnewregistro #numdocumento').on('blur',function(e){
        let dni=$(this).val()||'';
        let tipodoc=$('#formnewregistro #tipodocumento').val();
         dni=dni.split('_').join('');
        if(dni==''|| dni==0 || dni == null){
            Swal.fire({title:'<?php echo JrTexto::_('Please, enter your ID number') ?>.', icon:'warning' });
            return;
        }
        Swal.showLoading({type:'info', html:'<?php echo JrTexto::_('Processing'); ?> ...'});
        var data1=new FormData();        
            data1.append('sqlsolopersona',true);
            data1.append('dni',dni);
            data1.append('tipo',tipodoc);
            //data1.append('idproyecto',idproyecto);
            data1.append('showmsjok',true);
            __sysAyax({ 
                fromdata:data1,
                url:_sysUrlsks_+'persona/infodniexistentes',                  
                callback:function(rs){                                       
                    Swal.close();
                    let frm=$('#formnewregistro');
                    let nohaypersona=false;
                    if(rs.code==200){
                        let persona=rs.data||'';                      
                        if(rs.data!=undefined && rs.data!=''){
                            persona.infopersona;
                            let per1=persona.infopersona||'';
                            if(per1!='' && per1!=undefined){
                                frm.find('input#nombres').attr('readonly',true).val(per1.nombre);
                                frm.find('input#apellido1').attr('readonly',true).val(per1.primer_ape);
                                frm.find('input#apellido2').attr('readonly',true).val(per1.segundo_ape);
                                frm.find('input#correo').attr('readonly',true).val(per1.email);
                                frm.find('input#gradoinstruccion').attr('readonly',true).val(per1.instruccion);
                                frm.find('select#sexo').attr('readonly',true).val(per1.sexo);
                                frm.find('input#fecha_nacimiento').attr('readonly',true).val(per1.fechanac);
                                frm.find('input#telefono').attr('readonly',true).val(per1.telefono);
                                frm.find('input#idpersona').attr('readonly',true).val(per1.idpersona);
                                frm.find('input#edad').attr('readonly',true).val(calcularAnios2(per1.fechanac));
                            }else nohaypersona=true;
                        }else nohaypersona=true;                      
                    }else nohaypersona=true;

                    if(nohaypersona==true){
                        frm.find('input#nombres').removeAttr('readonly').val('');
                        frm.find('input#apellido1').removeAttr('readonly').val('');
                        frm.find('input#apellido2').removeAttr('readonly').val('');
                        frm.find('input#correo').removeAttr('readonly').val('');
                        frm.find('input#gradoinstruccion').removeAttr('readonly').val('');
                        frm.find('select#sexo').removeAttr('readonly').val('M');
                        frm.find('input#fecha_nacimiento').removeAttr('readonly').val('');
                        frm.find('input#telefono').removeAttr('readonly').val('');
                        frm.find('input#idpersona').val('');
                    }
                }
            });
    })

     $('#formRecupClave').bind({
         submit: function(){
            var btn=$(this).find('#btn-enviar-formInforecuperarclave');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            var fele = document.getElementById("formRecupClave");
            var data=new FormData(fele);          
              __sysAyax({ 
                fromdata:data,
                  url:_sysUrlBase_+'sesion/cambiarclave',                  
                  callback:function(rs){
                    btn.removeClass('disabled').removeAttr('disabled','disabled');
                    if(rs.code==200){
                       __notificar({
                            type:'success',
                            //title:'Revise su correo',
                            html:rs.msj                     
                        });
                    }else{
                        __notificar({
                            type:'danger',
                            title:'Error',
                            html:rs.msj                     
                        });
                    }
                  }
            });             
         }
    });

    $('.btnactivarcodigosuscripcion').on('click',function(ev){
        $('#panellogin').hide();
        $('#panelvalidarcodigo').show();
    })

    $('.btnvolveralogin').on('click',function(ev){
        let pnl=$(this).closest('.wf-container');
        pnl.hide();
        $('#panellogin').show();
    })

    $('.btnactivarsuscripcion').on('click',function(ev){
        let pnl=$(this).closest('.wf-container');
        pnl.hide();
        $('#panelrecuperarcontraseña').show();
    })

    $('.returntologinsincodigo').click(function(ev){
        let pnl=$(this).closest('.wf-container');
        pnl.hide();
        $('#formcodigosuscripcion').find('#codigo').val('');
        $('#panellogin').show();
    })
    $('.returntologinconcodigo').click(function(ev){
      ev.preventDefault();
        let pnl=$(this).closest('.wf-container');
        pnl.hide();        
        $('#panellogin').show();
    })
</script>