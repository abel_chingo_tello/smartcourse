<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Ugel"); ?>'
    });
    /*addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });*/
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;} 
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="form-group">
                <div class="cajaselect">
                  <select name="iddre" id="iddre" class="form-control">
                    <option value=""><?php echo JrTexto::_('Selected') ?></option>
                  </select>
                </div>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="form-group">
                <!--label><?php //echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-12 text-center">             
               <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/ugel/agregar" data-titulo="<?php echo ucfirst(JrTexto::_('Add'))." ".JrTexto::_('Ugel') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
               <a class="btn btn-warning btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/ugel/importar" data-titulo="<?php echo ucfirst(JrTexto::_('Import'))." ".JrTexto::_('Ugel') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Import')?></a>
            </div>            
          </div>
      </div>
    </div>
	   <div class="row">         
         <div class="col table-responsive ">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Descripcion") ;?></th>
                  <th><?php echo JrTexto::_("Abrev") ;?></th>
                  <!--th><?php //echo JrTexto::_("Iddepartamento") ;?></th>
                  <th><?php //echo JrTexto::_("Idprovincia") ;?></th>
                  <th><?php //echo JrTexto::_("Idproyecto") ;?></th-->
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
        </table>
        </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5d8e3cd74cf50='';
function refreshdatos5d8e3cd74cf50(){
    tabledatos5d8e3cd74cf50.ajax.reload();
}
$(document).ready(function(){  
  var estados5d8e3cd74cf50={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5d8e3cd74cf50='<?php echo ucfirst(JrTexto::_("ugel"))." - ".JrTexto::_("edit"); ?>';
  var draw5d8e3cd74cf50=0;
  var _imgdefecto='';
  var ventana_5d8e3cd74cf50=$('#ventana_<?php echo $idgui; ?>');

  var _cargarcombos5d8e3cd74cf50=function(){
    var fd = new FormData();   
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/min_dre',
      callback:function(rs){
        $sel=$('#iddre');
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.iddre+'" '+(v.iddre==iddre?'selected="selected"':'')+ '>'+v.iddre +" : "+ v.descripcion+'</option>');
          })
      }})}
  _cargarcombos5d8e3cd74cf50();

  ventana_5d8e3cd74cf50.find('.textosearchlist').on('keydown',function(ev){
    if(ev.keyCode===13) refreshdatos5d8e3cd74cf50();
  }).blur(function(ev){ refreshdatos5d8e3cd74cf50()})
  
  $('.btnbuscar').click(function(ev){
    refreshdatos5d8e3cd74cf50();
  });
  $('#iddre').change(function(ev){
    refreshdatos5d8e3cd74cf50();
  });

  tabledatos5d8e3cd74cf50=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Descripcion") ;?>'},
        {'data': '<?php echo JrTexto::_("Abrev") ;?>'},
        /*{'data': '<?php //echo JrTexto::_("Iddepartamento") ;?>'},
        {'data': '<?php //echo JrTexto::_("Idprovincia") ;?>'},
        {'data': '<?php //echo JrTexto::_("Idproyecto") ;?>'},*/ 
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'json/ugel',
        type: "post",
        data:function(d){
            d.json=true 
            d.texto=$('#texto').val(),
            d.iddre=$('#iddre').val(),
            draw5d8e3cd74cf50=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5d8e3cd74cf50;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push({             
              '#':(i+1),
              '<?php echo JrTexto::_("Descripcion") ;?>': data[i].descripcion,
              '<?php echo JrTexto::_("Abrev") ;?>': data[i].abrev,
              /*'<?php //echo JrTexto::_("Iddepartamento") ;?>': data[i].iddepartamento,
              '<?php //echo JrTexto::_("Idprovincia") ;?>': data[i].idprovincia,
              '<?php //echo JrTexto::_("Idproyecto") ;?>': data[i].idproyecto,*/
              //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/ugel/editar/?id='+data[i].idugel+'" data-titulo="'+tituloedit5d8e3cd74cf50+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idugel+'" ><i class="fa fa-trash"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
        var data=new FormData()
        data.append('idugel',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/ugel/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5d8e3cd74cf50.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/ugel/setcampo','masvalores':{idugel:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idugel',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/ugel/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5d8e3cd74cf50.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var titulo=$(this).attr('data-titulo')||'';
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      tabledatos5d8e3cd74cf50.ajax.reload();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frm_ugel(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_ugelimport(_md);
            })
          });
       }           
    })   
  })
});
</script>