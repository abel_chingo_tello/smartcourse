<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$datoslistadoheader='';
$datoslistado='';
$addjschk=false;
$jsdate=false;
$jsdatetime=false;
$jstime=false;
$jsverchkeditor=false; 
$jsverwysihtml5 =false;
$jsvertinymce=false;
$jsfile=false;
$jschkformulario=false;
$jschkformulariomultiple=false;
$addimgsave=null;
$jsdatetxt=null;
$jsdatetimetxt=null;
?>
<?php
echo '<?php 
defined(\'RUTA_BASE\') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!=\'modal\'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:\'eeeexzx-1\';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
';?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo '<?php echo $this->documento->getUrlSitio();?>';?>"><i class="fa fa-home"></i>&nbsp;<?php echo '<?php echo JrTexto::_(\'Home\'); ?>';?></a></li>
        <!--li><a href="<?php echo '<?php echo $this->documento->getUrlSitio();?>';?>/academico">&nbsp;<?php echo '<?php echo JrTexto::_(\'Academic\'); ?>';?></a></li-->
        <li><a href="<?php echo '<?php echo $this->documento->getUrlSitio();?>/'.$this->tabla;?>">&nbsp;<?php echo '<?php echo JrTexto::_(\''.ucfirst($tb).'\'); ?>';?></a></li>
        <li class="active">&nbsp;<?php echo '<?php echo JrTexto::_($this->frmaccion);?>';?></li>
    </ol>
  </div>
</div>
<?php echo '<?php } ?>
';?>
<div class="row ventana"  id="vent-<?php echo '<?php echo $idgui;?>'; ?>" idgui="<?php echo '<?php echo $idgui;?>'; ?>"  >
  <div class="col-md-12">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo '<?php echo $idgui;?>'; ?>" tb="<?php echo $this->tabla; ?>" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo '<?php echo $idgui;?>'; ?>">
          <input type="hidden" name="<?php echo $campopk; ?>" id="<?php echo $campopk; ?>" value="<?php echo '<?php echo $this->pk;?>';?>">
          <?php if(!empty($campos))
          foreach ($campos as $campo){
            $Campo=ucfirst($campo);
            $verenlistado='_chk'.$campo;
            if(empty($frm[$verenlistado])) continue;
            $tipo=@$frm['tipo_'.$campo];
            if($campo!=$campopk){
              //if($tipo!='fk'){
                if(@$frm["tipo2_".$campo]!='system'){ 
            ?><div class="form-group">
              <label class="control-label"><?php echo "<?php echo JrTexto::_('".str_replace('_', ' ',$Campo)."');?>"; ?> <span class="required"> * </span></label>
              <div class="">
              <?php }
//////////////////////////////////////////////------------text -------------------
              if($tipo=='text'||$tipo=='double'||$tipo=='decimal'||$tipo=='money'){
              ?>  <input type="text"  id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" required="required" class="form-control" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">
              <?php 

//////////////////////////////////////////////------------email -------------------
              }elseif($tipo=='email'){
              ?>  <input type="email" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" required="required" class="form-control" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">
              <?php 

//////////////////////////////////////////////------------date -------------------
              }elseif($tipo=='date'){
                    if($frm["tipo2_".$campo]=='user'){$jsdate=true;
                      $jsdatetxt.="
  $('#".$campo."').datetimepicker({
    lang:'es',
    timepicker:false,
    format:'Y/m/d'
  });";
              ?>  <input name="<?php echo $campo ?>" id="txt<?php echo $Campo ?>" class="verdate form-control" required="required" type="date" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
              <?php }elseif($frm["tipo2_".$campo]=='system'){
              ?>  <input type="hidden" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("Y/m/d") ?>' ?>">

              <?php } 

//////////////////////////////////////////////------------datetime -------------------
              }elseif($tipo=='datetime'){
                    if($frm["tipo2_".$campo]=='user'){$jsdatetime=true;
                      $jsdatetimetxt.="
  $('#".$campo."').datetimepicker({
    datepicker:false,
    format:'H:i',
    value:'12:00'
  });
  ";  
              ?>  <input name="<?php echo $campo ?>" id="txt<?php echo $Campo ?>" class="verdatetime form-control" required="required" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
              <?php }elseif($frm["tipo2_".$campo]=='system'){
              ?>  <input type="hidden" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("Y/m/d H:i:s"); ?>' ?>">

              <?php }

//////////////////////////////////////////////------------time -------------------
              }elseif($tipo=='time'){
                      if($frm["tipo2_".$campo]=='user'){$jstime=true;
                ?>  <input name="<?php echo $campo ?>" id="txt<?php echo $Campo ?>" class="vertime form-control" required="required" type="date" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
                <?php }elseif($frm["tipo2_".$campo]=='system'){
                ?>  <input type="hidden"  id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("H:i") ?>' ?>">

                <?php }

//////////////////////////////////////////////------------combobox -------------------
              }elseif($tipo=='combobox'){
              ?> <select id="txt<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control" required>
              <option value=""><?php echo '<?php JrTexto::_("Seleccione");?>';?></option>
                <?php if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); ?>

              <option value=<?php echo $valoresopt[0]; ?>><?php echo "<?php echo JrTexto::_(".$valoresopt[0].");?>"; ?></option>
              <option value=<?php echo $valoresopt[1]; ?>><?php echo "<?php echo JrTexto::_(".$valoresopt[1].");?>"; ?></option>
                  <?php }else{ ?>
              <option value="1"><?php echo "<?php echo JrTexto::_('Activo');?>"; ?></option>
              <option value="0"><?php echo "<?php echo JrTexto::_('Inactivo');?>"; ?></option>
                  <?php } ?>
                <?php }else{ 
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ ?> 
              <option value="<?php echo $i;?>"> <?php echo "<?php echo JrTexto::_('".$i."');?> ".$i; ?></option>
              <?php } }
              ?> 
              </select>
              <?php  

//////////////////////////////////////////////------------checkbox -------------------
              }elseif($tipo=='checkbox'){
                $jschkformulario=true;
                if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); 
                  $jschkformulario=true;
              ?> 

              <a  style="cursor:pointer;" href="javascript:;"  class="btn-activar <?php echo '<?php echo @$frm["'.$campo.'"]==1?"active":"";?>';?>"> <i class="fa fa<?php echo '<?php echo @$frm["'.$campo.'"]=="1"?"-check":"";?>';?>-circle fa-lg"></i> <span></span>
              <input type="hidden" name="<?php echo $campo ?>" value="<?php echo '<?php echo @$frm["'.$campo.'"]?1:0;?>'; ?>"> 
              </a>
                  <?php }else{ ?> 
                    <a  style="cursor:pointer;" href="javascript:;"  class="btn-activar <?php echo '<?php echo @$frm["'.$campo.'"]==1?"active":"";?>';?>"> <i class="fa fa<?php echo '<?php echo @$frm["'.$campo.'"]=="1"?"-check":"";?>';?>-circle fa-lg"></i> 
              <input type="hidden" name="<?php echo $campo ?>" value="<?php echo '<?php echo @$frm["'.$campo.'"]?1:0;?>'; ?>"> 
              </a>

                  <?php } ?>
                <?php }else{ 
                     $jschkformulariomultiple=true;
                    if($frm["dattype".$campo]=="enum"){ 
                      $txtchk=str_replace("'", "", substr($frm["coltype".$campo], 5,-1));
                      $valoresopt=@explode(",",$txtchk); 
                      for($i=0;$i<count($valoresopt);$i++){                       
                       ?>

                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle";?>'; ?>" 
                data-value="<?php echo $valoresopt[$i]; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("'.$valoresopt[$i].'");?>'; ?></span>                 
                 </a><br>
                  <?php    }
                      
                  }else{
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ 
              ?> 
                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle";?>'; ?>" 
                data-value="<?php echo $i; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("Opcion - '.$i.'");?>'; ?></span>                 
                 </a><br>
              <?php 
                  }  
                  }?>
                  <input type="hidden" name="<?php echo $campo ?>" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>" >
                  <?php
                }                    

//////////////////////////////////////////////------------radiobutton -------------------
              }elseif($tipo=='radiobutton'){                
                if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                  $jschkformulario=true;
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); 
                  $jschkformulario=true;

              ?>  
               <a  style="cursor:pointer;" href="javascript:;"  class="btn-activar"> <i class="fa fa<?php echo '<?php echo @$frm["'.$campo.'"]=="1"?"-check":"";?>';?>-circle-o fa-lg"></i> <span><?php echo '<?php echo @$frm["'.$campo.'"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive");?>';?></span>
              <input type="hidden" name="<?php echo $campo ?>" value="<?php echo '<?php echo @$frm["'.$campo.'"]?1:0;?>'; ?>"> 
              </a>

                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[0].':'.$valoresopt[1].');?>'; ?></span>
                 <input type="hidden" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" > 
                 </a>
                  <?php }else{ ?>
                  <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]==1?"fa-check-circle":"fa-circle";?>'; ?>" 
                data-value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>"  data-valueno="0" data-value2="<?php echo '<?php echo @$frm["'.$campo.'"]==1?1:0;?>';?>">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]==1?"Activo":"Inactivo");?>'; ?></span>
                 <input type="hidden" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" > 
                 </a>
                  <?php } ?>
                <?php }else{ 
                 $jschkformulariomultiple=true;
                    if($frm["dattype".$campo]=="enum"){ 
                      $txtchk=str_replace("'", "", substr($frm["coltype".$campo], 5,-1));
                      $valoresopt=@explode(",",$txtchk); 
                      for($i=0;$i<count($valoresopt);$i++){                       
                       ?>
  <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle";?>'; ?>" 
                data-value="<?php echo $valoresopt[$i]; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("'.$valoresopt[$i].'");?>'; ?></span>
                 
                 </a><br>
                  <?php    }
                      
                  }else{
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ 
              ?> 
                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle";?>'; ?>" 
                data-value="<?php echo $i; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("Opcion - '.$i.'");?>'; ?></span>
                 
                 </a><br>
              <?php 
                  }  
                  }?>
                  <input type="hidden" name="<?php echo $campo ?>" value="<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" >
                  <?php } 
                   

//////////////////////////////////////////////------------Number -------------------
              }elseif($tipo=='int'||$tipo=='tinyint'||$tipo=='bigint'){
              ?> <input type="number" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" step="1" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:1;?>'; ?>"  min="1" class="form-control" required />
              <?php 

//////////////////////////////////////////////------------password -------------------
              }elseif($tipo=='password'){
                if($frm["tipo2_".$campo]=='simple'){
              ?> <input type="password" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" class="form-control" required />    
              <?php  }else{ 
              ?> <input type="password" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" class="form-control" required /> 
              <?php  }

//////////////////////////////////////////////------------file -------------------
              }elseif($tipo=='file'){
                $jsfile=true;
                if($frm["tipo2_".$campo]=='all'){
                  $tipo='all'; $txtfile=''; 
                ?> <a href="" data-type="all" class="img-thumbnail" id="id<?php echo $Campo ?>"> <?php echo "<?php echo JrTexto::_('ver');?>"; ?> </a>
                <?php }elseif($frm["tipo2_".$campo]=='imagen'){
                  $tipo='img';$txtfile='Imagen';
                ?> 
                <?php }elseif($frm["tipo2_".$campo]=='video'){
                  $tipo='vid';$txtfile='Video';
                ?> <video src=""  data-type="video" id="id<?php echo $Campo ?>"><?php echo "<?php echo JrTexto::_('Descargar video');?>"; ?> </video>
                <?php }elseif($frm["tipo2_".$campo]=='documentos'){
                  $tipo='doc';$txtfile='Documento';
                ?> <a href=""  data-type="archivo" class="img-thumbnail" id="id<?php echo $campo ?>"> <?php echo "<?php echo JrTexto::_('ver Archivo');?>"; ?></a>
                <?php }elseif($frm["tipo2_".$campo]=='audio'){
                  $tipo='aud';$txtfile='Audio';
                ?> <audio src=""  controls  data-type="audio" class="img-thumbnail" id="id<?php echo $campo ?>"><?php echo "<?php echo JrTexto::_('Descargar Audio');?>"; ?></audio>
                <?php }
                if($txtfile!='Imagen'){
              ?> <span class="btn btn-info btn-file"> <i class="fa fa-upload"></i> <?php echo "<?php echo JrTexto::_('Seleccionar ".$txtfile."');?>"; ?> 
                 <input data-texto="<?php echo $txtfile; ?>" data-campo="<?php echo $campo; ?>" data-action="<?php echo "<?php echo JrAplicacion::getJrUrl(array('".$this->tabla."', 'subir_archivo'));?>?arc=".$campo."&tipo=".$tipo; ?>" type="file" name="file<?php echo $Campo ?>" id="file<?php echo $Campo ?>" class="cargarfile" data-type="<?php  echo $tipo; ?>"></span>
                 <input type="hidden" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" class="form-control" required />
                 <iframe name="if_cargar_file" style="display:none"></iframe>
              <?php }else{ 
                $addimgsave.='
                $("#txt'.$Campo.'").val($("img[alt=\'img'.$campo.'\']").cropper("getDataURL", "image/png", 1.0));
                ';
                ?>
            <img src="<?php echo '<?php echo URL_BASE.(!empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:$_imgdefecto);?>'; ?>" alt="imagenfile" file="<?php echo $campo ?>" class="subirfile img-thumbnail" style="max-height:200px; max-width:200px;">
            <input type="hidden" name="<?php echo $campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?URL_BASE.$frm["'.$campo.'"]:"";?>'; ?>">

                <!--div class="row">
                  <input type="hidden" name="<?php //echo $campo ?>" id="txt<?php //echo $Campo ?>">
                  <input type="hidden" name="<?php //echo $campo ?>_old" value="<?php //echo '<?php echo @$frm["'.$campo.'"];?>';?>">
                  <div class="col-md-12">
                    <div class="img-container">
                      <img src="<?php //echo '<?php echo !empty($frm["'.$campo.'"])?$this->documento->getUrlBase().$frm["'.$campo.'"]:($this->documento->getUrlStatic()."/media/'.strtolower($tb).'/default.png");?>';?>" alt="img<?php //echo $campo ?>" data-ancho="100"  data-alto="100">
                    </div>
                  </div>
                </div-->
             <?php }

//////////////////////////////////////////////------------textArea -------------------
              }elseif($tipo=='textArea'){
                if($frm["tipo2_".$campo]=='simple'){
              ?> <textarea id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" class="form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='wysihtml5'){
              $jsverwysihtml5 =true;
              ?> <textarea id="txt<?php echo $Campo ?>"name="<?php echo $campo ?>" class="verwysihtml5 form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='chkeditor'){
              $jsverchkeditor=true; 
              $js.='CKEDITOR.replace("txt'.$campo.'");'
              ?> <textarea id="txt<?php echo $Campo ?>"name="<?php echo $campo ?>" class="verchkeditor form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='tinymce'){
              $jsvertinymce=true; 
              ?> <textarea id="txt<?php echo $Campo ?>"name="t<?php echo $campo ?>" class="vertinymce form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }

//////////////////////////////////////////////------------foreign key -------------------
              }elseif($tipo=='fk'){
                if($frm["tipofkcomo_".$campo]=='combobox'){
              ?><select id="txt<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control">
               <option value=""><?php echo "<?php echo JrTexto::_('Seleccione'); ?>"; ?></option>
              <?php
              echo '<?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') { ?>';
              echo '<option value="<?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]?>" <?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]==@$frm["'.$campo.'"]?"selected":""; ?> ><?php echo $fk'.$campo.'["'.$frm["tipofkver_".$campo].'"] ?></option>';
              echo '<?php } ?>';
              ?>
              </select>
              <?php }elseif($frm["tipofkcomo_".$campo]=='radiobutton'){
                echo '
              <?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                 ?>';
              ?><input type="radio" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" >
              <?php 
              echo ' <?php } ?>';
              }elseif($frm["tipofkcomo_".$campo]=='checkbox'){
                echo '
              <?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                 ?>';
              ?><input type="checkbox" id="txt<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control">
              <?php echo ' <?php } ?>';
              }elseif($frm["tipofkcomo_".$campo]=='text'){
              ?><input type="text" id="txt<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control">
              <?php }elseif($frm["tipofkcomo_".$campo]=='listado'){
              ?><input type="button" id="txt<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control listarfk">
              <?php }elseif($frm["tipofkcomo_".$campo]=='system'){
              ?><?php echo '<?php echo $this->fk'.$campo.'["usuario"];?>';?>
              <?php }

//////////////////////////////////////////////------------otraopcion -------------------
              }elseif($frm["tipofkcomo_".$campo]=='money'){?>
              <input type="text"  id="txt<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control"
              value="<?php echo '<?php echo @$frm["txt'.$Campo.'"] ?>'?>">
              <?php }else{
              ?>   <input type="text"  id="txt<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control" value="<?php echo '<?php echo @$frm["txt'.$Campo.'"] ?>'?>">
              <?php } if(@$frm["tipo2_".$campo]!='system'){ 
              ?>
                    
              </div>
            </div>

            <?php }
            }
          }?>

          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-save<?php echo $tb ?>" type="submit" tb="<?php echo $this->tabla; ?>" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo "<?php echo JrTexto::_('Save');?>"; ?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo "<?php echo JrAplicacion::getJrUrl(array('".strtolower($tb)."'))?>"; ?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo "<?php echo JrTexto::_('Cancel');?>"; ?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo '<?php echo $this->documento->getUrlStatic().\'/js/web/frm.js\' ?>';?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo '<?php echo $idgui;?>'; ?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo '<?php echo $idgui;?>'; ?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
  <?php echo $jsdatetxt; ?>
  <?php echo $jsdatetimetxt; ?>
  <?php if($jsverwysihtml5==true){?>
    $(".verwysihtml5").wysihtml5();
  <?php } ?>
  <?php echo @$js; ?>

  $('#frm-<?php echo '<?php echo $idgui;?>'; ?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo '<?php echo $idgui;?>'; ?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/<?php echo strtolower($tb);?>/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/<?php echo strtolower($tb);?>');
                }
              }
          });
       }
  })
})
</script>

