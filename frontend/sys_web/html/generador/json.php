<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$txtcamposave='';
$txtcamposavexajax='';
$file=false;
$fileimg=false;
$jsradiolistadosimple=false;
$addjs='';
$addfk='';
$addtools=null;
$jsfileimg=null;
$txtcamposaveimgnuevo=null;
$txtcamposaveimgeditar=null;
$txtcamposaveajax=null;
$txtaddfiltros=null;
//fk variables
$txtaddclase=null;
$txtadddec=null;
$txtadddec2=null;
foreach ($campos as $campo) {
	$verenlistado='_chk'.$campo;
			if(empty($frm[$verenlistado])) continue;
	if(@$frm["tipo_".$campo]=='file') {
		if($frm["tipo2_".$campo]=='imagen'){
			/*if($fileimg==false){
				$jsfileimg.='
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/cropper.min.js");
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/main3.js");
				';
				$fileimg=true;
			}*/

		$addtools='JrCargador::clase(\'sys_negocio::NegTools\', RUTA_RAIZ);
		';
$txtcamposaveimgnuevo.='
		if(!empty($_FILES["'.$campo.'file"])){            	
		    $resi = NegTools::subirFile($_FILES["'.$campo.'file"],\''.strtolower($tb).'/\',\'\',\'imagen\',\'img_\'.$res);
		    if($resi[\'code\']==200) $this->oNeg'.ucfirst($tb).'->setCampo($res,\''.$campo.'\',$resi["rutaStatic"]."?id=".date(\'YmdHis\'));		            	
		}
		';


			/*$txtcamposaveimgeditar.='
					$archivo=basename($frm["txt'.ucfirst($campo).'_old"]);
					if(!empty($frm["txt'.ucfirst($campo).'"])) $txt'.ucfirst($campo).'=NegTools::subirImagen("'.$campo.'_",@$frm["txt'.ucfirst($campo).'"], "'.strtolower($tb).'",false,100,100 );
						$this->oNeg'.$tb."->__set('".$campo."',".'@$txt'.ucfirst($campo).');
						@unlink(RUTA_SITIO . SD ."static/media/'.strtolower($tb).'/".$archivo);
						';*/

		}else{
			if($campo!=$campopk){
			$txtcamposave.='$this->oNeg'.$tb."->".$campo.'=@$'.$campo.';
				';
			$txtcamposaveajax.='$this->oNeg'.$tb."->".$campo.'=@frm["'.$campo.'"];
				';

			}
			
		}

	}else{
		if(@$frm["tipo_".$campo]=='fk'&&@$frm["tipofkcomo_".$campo]=='system'){
			if($campo!=$campopk){
			$txtcamposave.='$this->oNeg'.$tb."->".$campo.'=@$this->usuario["idusuario"];
				';
			$txtcamposaveajax.='$this->oNeg'.$tb."->".$campo.'=@$this->usuario["idusuario"];
				';

			}		
		}else{
			if($campo!=$campopk){
			$txtcamposave.='$this->oNeg'.$tb."->".$campo.'=@$'.$campo.';
				';
			$txtcamposaveajax.='$this->oNeg'.$tb."->".$campo.'=@$frm["'.$campo.'"];
				';
				
			}

		
		}
		$txtaddfiltros.='if(isset($_REQUEST["'.$campo.'"])&&@$_REQUEST["'.$campo.'"]!=\'\')$filtros["'.$campo.'"]=$_REQUEST["'.$campo.'"];
			';

	}

	if((@$frm["tipo_".$campo]=='radiobutton'||  @$frm["tipo_".$campo]=='checkbox' || @$frm["tipo_".$campo]=='combobox')&& $frm["tipo2_".$campo]=='simple'){
		$jsradiolistadosimple=true;
	}
	if(@$frm["tipo_".$campo]=='date'&& @$frm["tipo2_".$campo]=='user'){
		$addjs.='
		$this->documento->script(\'jquery.datetimepicker\', \'/libs/datetimepicker-master/\');
		$this->documento->stylesheet(\'jquery.datetimepicker\', \'/libs/datetimepicker-master/\');
		';
	}
	if(@$frm["tipo_".$campo]=='textArea'){
		if(@$frm["tipo2_".$campo]=='tinymce'){
			$addjs.='
			$this->documento->script(null, \'//tinymce.cachefly.net/4.0/tinymce.min.js\');
			';
		}
		if(@$frm["tipo2_".$campo]=='chkeditor'){
			$addjs.='
			$this->documento->script(null, \'//cdn.ckeditor.com/4.5.1/standard-all/ckeditor.js\');
			$this->documento->script(null, \'//cdn.ckeditor.com/4.5.1/standard-all/samples/old/assets/uilanguages/languages.js\');
			';
		}
	}

	if(@$frm["tipo_".$campo]=='fk'){
		$tb2=$frm["tipo2_".$campo];
		if(@$frm["tipofkcomo_".$campo]=='system'){
			$addfk.='
			$this->fk'.$campo.'=$this->usuario;';
		}else{
		$addfk.='$this->fk'.$campo.'=$this->oNeg'.ucfirst($tb2).'->buscar();
			';
		}
		$txtaddclase.='JrCargador::clase(\'sys_negocio::Neg'.ucfirst($tb2).'\', RUTA_BASE);
';
		$txtadddec.='private $oNeg'.ucfirst($tb2).';
	';
		$txtadddec2.='$this->oNeg'.ucfirst($tb2).' = new Neg'.ucfirst($tb2).';
	';

	}

}
echo '<?php
'; ?>
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		<?php echo date("d-m-Y") ?>
 
 * @copyright	Copyright (C) <?php echo date("d-m-Y") ?>. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::Neg<?php echo  $tb ?>', RUTA_BASE);
<?php echo $txtaddclase; ?>
class Web<?php echo  $tb ?> extends JrWeb
{
	private $oNeg<?php echo  $tb ?>;
	<?php echo $txtadddec;?>
	
	public function __construct()
	{
		parent::__construct();		
		$this->oNeg<?php echo  $tb ?> = new Neg<?php echo  $tb ?>;
		<?php echo $txtadddec2; ?>
		
	}

	public function defecto(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('<?php echo $tb; ?>', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			<?php echo $txtaddfiltros; ?>
			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNeg<?php echo  $tb ?>->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$<?php echo $campopk; ?>)) {
				$this->oNeg<?php echo $tb;?>-><?php echo $campopk; ?> = $<?php echo $campopk; ?>;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	        
			<?php echo $txtcamposave; ?>

            if($accion=='_add') {
            	$res=$this->oNeg<?php echo $tb;?>->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('<?php echo $tb;?>')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNeg<?php echo $tb;?>->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('<?php echo $tb;?>')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            <?php echo $addtools;?>
			<?php echo $txtcamposaveimgnuevo;?>

            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
					<?php echo $txtcamposave; ?>
					$newid=$this->oNegUgel->agregar();
					$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNeg<?php echo $tb; ?>->__set('<?php echo $campopk; ?>', $_REQUEST['<?php echo $campopk; ?>']);
			$res=$this->oNeg<?php echo $tb;?>->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNeg<?php echo $tb; ?>->setCampo($_REQUEST['<?php echo $campopk; ?>'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}