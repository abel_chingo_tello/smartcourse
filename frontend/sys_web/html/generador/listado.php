<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$datoslistadoheader='';
$datoslistadoheaderajax='';
$datoslistadoheaderajax2='';
$datosrefh='';
$datoslistado='';
$datoslistadoajax='';
$addjschk=false;
$jsfile=false;
$idguisys=uniqid();
$fileslist='';
$jsdate=false;
$jsdatetime=false;
$jstime=false;
$jsverchkeditor=false; 
$jsverwysihtml5 =false;
$jsvertinymce=false;
$jsfile=false;
$jschkformulario=false;
$jschkformulariomultiple=false;
$addimgsave=null;
$jsdatetxt=null;
$jsdatetimetxt=null;
?>
<?php
echo '<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
';
echo 'if(!$ismodal){?>
';?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo '<?php echo JrTexto::_("'.$tb.'"); ?>'?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> <?php echo JrTexto::_('add') ?>',
      href: '#' // btnvermodal
    });
  </script>
<?php echo '<?php } ?>
';?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo '<?php echo $idgui; ?>';?>" >
<div class="row">
  <div class="col">    
         <div class="row">            
            <?php if(!empty($campos))
            foreach ($campos as $campo){ ?>
              <?php
              $tipo=@$frm['tipo_'.$campo];                      
              if($tipo=='fk'){
              if(@$frm["tipofkcomo_".$campo]=='combobox'){?>

            <div class="col-xs-6 col-sm-4 col-md-3">             
            <div class="select-ctrl-wrapper select-azul">
              <select id="fkcb<?php echo $campo ?>" name="fkcb<?php echo $campo ?>" class="form-control select-ctrl" >
                <option value=""><?php echo "<?php echo JrTexto::_('Seleccione ".ucfirst($campo)."'); ?>"; ?></option>
                  <?php echo '<?php 
                          if(!empty($this->fk'.$campo.'))
                            foreach ($this->fk'.$campo.' as $fk'.$campo.') { ?>';
                        echo '<option value="<?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]?>" <?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]==@$frm["'.$campo.'"]?"selected":""; ?> ><?php echo $fk'.$campo.'["'.@$frm["tipofkver_".$campo].'"] ?></option>';
                        echo '<?php } ?>'; ?>
                        
              </select>
            </div>
            </div>
            <?php $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#fkcb'.$campo.'\').val(),
            ';
  $datosrefh.='.on(\'change\',\'#fkcb'.$campo.'\',function(ev){refreshdatos'.$idguisys.'();
  })';
              } /////////////////////////////////////////////////////
              elseif($frm["tipofkcomo_".$campo]=='radiobutton'){?>
            <div class="col-xs-6 col-sm-4 col-md-3">
              <?php  echo '
                <?php 
                if(!empty($this->fk'.$campo.'))
                  foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                   ?>';
              ?>
                <input type="radio" id="rb<?php echo $campo ?>" name="rb<?php echo $campo ?>" >
              <?php echo ' <?php } ?>';?>
            </div>
            <?php $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#fkrb'.$campo.'\').val(),
            ';
$datosrefh.='.on(\'change\',\'#fkrb'.$campo.'\',function(ev){ refreshdatos'.$idguisys.'();
  })';
              }/////////////////////////////////////////////////
              elseif($frm["tipofkcomo_".$campo]=='checkbox'){?>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <?php echo '
                  <?php 
                  if(!empty($this->fk'.$campo.'))
                    foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                     ?>';
                ?>
                  <input type="checkbox" id="ck<?php echo $campo ?>" name="ck<?php echo $campo ?>" class="form-control">
                <?php echo ' <?php } ?>';?>
            </div>                          
            <?php $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#fkck'.$campo.'\').val(),
            ';
$datosrefh.='.on(\'change\',\'#fkck'.$campo.'\',function(ev){ refreshdatos'.$idguisys.'();
  })';
              } ///////////////////////////////////////////////// terminca chekbox
              } /////////////////////////////////////////////////
              else{
                if($campo!=$campopk)
                  if($tipo=='date'||$tipo=='datetime'||$tipo=='time'){?>
                    <div class="col-xs-6 col-sm-4 col-md-3">                     
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo '<?php echo  ucfirst(JrTexto::_("'.$campo.'"))?>';?> </span>
                            <input type='text' class="form-control border0" name="date<?php echo $campo; ?>" id="date<?php echo $campo; ?>" />           
                          </div>
                      </div>
                    </div>
                <?php  $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#date'.$campo.'\').val(),
            ';
$datosrefh.='.on(\'change\',\'#date'.$campo.'\',function(ev){ refreshdatos'.$idguisys.'();
  })';
                  } /////////////////////////////////////////////////
                  elseif($tipo=='combobox'||$tipo=='radiobutton'){?>                          
                    <div class="col-xs-6 col-sm-4 col-md-3 " >
                      <!--label><?php echo ucfirst($campo); ?></label-->
                      <div class="select-ctrl-wrapper select-azul">
                      <select name="cb<?php echo $campo; ?>" id="cb<?php echo $campo; ?>" class="form-control select-ctrl">
                          <option value=""><?php echo '<?php echo ucfirst(JrTexto::_("All '.ucfirst($campo).'"))';?>?></option>
                          <option value="1"><?php echo '<?php echo ucfirst(JrTexto::_("Active"))';?>?></option>
                          <option value="0"><?php echo '<?php echo ucfirst(JrTexto::_("Inactive"))';?>?></option>                              
                      </select>
                    </div>
                    </div>
                  <?php $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#cb'.$campo.'\').val(),
            ';
$datosrefh.='.on(\'change\',\'#'.$campo.'\',function(ev){refreshdatos'.$idguisys.'();
  })';
                  }                        
              }
            } ?>              
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php echo '<?php echo  ucfirst(JrTexto::_("text to search"))?>';?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo '<?php echo  ucfirst(JrTexto::_("text to search"))?>';?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo '<?php echo  ucfirst(JrTexto::_("Search"))?>';?> <i class="fa fa-search"></i></span>  
                    <?php $datoslistadoheaderajax.=' //d.texto=$(\'#texto\').val(),
            ';
 $datosrefh.='.on(\'click\',\'.btnbuscar\',function(ev){ refreshdatos'.$idguisys.'();
  })';?>
                </div>
              </div>
            </div>           
          </div>
        </div>
    </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <?php if(!empty($campos))
                    foreach ($campos as $campo){
                      $verenlistado='_chk'.$campo;
                      $tipo=@$frm['tipo_'.$campo];
                      if($tipo=='file') $jsfile=true;
                      if($campo!=$campopk&&!empty($frm[$verenlistado])){
                        if($tipo=='fk'){
                      $datoslistadoheader.='<th><?php echo JrTexto::_("'.ucfirst(substr(str_replace('_', ' ', $frm["tipo2_".$campo]),4)).'")."(".JrTexto::_("'.ucfirst(str_replace('_', ' ', $frm["tipofkver_".$campo])).'").")"; ?></th>
                    ';
                      $datoslistado.='<td><?php echo !empty($reg["_'.$frm["tipofkver_".$campo].'"])?$reg["_'.$frm["tipofkver_".$campo].'"]:null; ?></td>
                    ';
                    $datoslistadoajax.='data[i].'.$frm["tipofkver_".$campo].',';
           
                  }else{
                      $datoslistadoheader.='<th><?php echo JrTexto::_("'.ucfirst(str_replace('_', ' ', $campo)).'") ;?></th>
                    ';

                      if(($tipo=='textArea'||$tipo=='text')&&$frm["maximocaracteres".$campo]>100){
                      $datoslistado.='<td><?php echo substr($reg["'.$campo.'"],0,100)."..."; ?></td>
                    ';
                  $datoslistadoajax.='data[i].'.$campo.',';
                       }elseif($tipo=='int'){
                      $datoslistado.='<td class="text-right"><?php echo $reg["'.$campo.'"] ;?></td>
                    ';
                  $datoslistadoajax.='data[i].'.$campo.',';
                        }elseif($tipo=='decimal'){
                      $datoslistado.='<td class="text-right"><?php echo number_format($reg["'.$campo.'"],'.$frm["partedecimal".$campo].',"."," "); ?></td>
                    ';
                  $datoslistadoajax.='data[i].'.$campo.',';
                        }elseif($tipo=='money'){
                      $datoslistado.='<td class="text-right"><span style="float:left">$.</span><?php echo number_format($reg["'.$campo.'"],'.$frm["partedecimal".$campo].',"."," "); ?></td>
                    ';
                  $datoslistadoajax.='data[i].'.$campo.',';
                        }elseif($tipo=='file'){
                          $tipofile=$frm["tipo2_".$campo];
                          $fileslist.='
                          var var'.$campo.'=data[i].'.$campo.'||\'\';
                          var'.$campo.'=var'.$campo.'.replace(_imgdefecto,\'\');                            
                          ';
                          if($tipofile=='imagen'){
                          $fileslist.='if(var'.$campo.'!=\'\') var'.$campo.'=\'<img src="\'+_sysUrlBase_+var'.$campo.'+\'" class="img-thumbnail" style="max-height:70px; max-width:50px;">\';
                          ';  
                      $datoslistado.='<td class="text-center"><img src="<?php echo $this->documento->getUrlBase().$reg["'.$campo.'"]; ?>" class="img-thumbnail" style="max-height:70px; max-width:50px;"></td>
                    ';
                       }elseif($tipofile=='video'){
                      $datoslistado.='<td class="text-center"><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver video"); ?></a></td>
                    ';

                     $fileslist.='if(var'.$campo.'!=\'\') var'.$campo.'=\'<a href="\'+data[i].'.$campo.'+\' target="_blank"> <?php echo JrTexto::_("ver video"); ?></a>\';
                          ';  
                      }elseif($tipofile=='documentos'){
                      $datoslistado.='<td class="text-center"><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver documento"); ?></a></td>
                  ';
                  $fileslist.='if(var'.$campo.'!=\'\') var'.$campo.'=\'<a href="\'+data[i].'.$campo.'+\' target="_blank"> <?php echo JrTexto::_("ver file"); ?></a>\';
                  ';

                      }else{
                      $datoslistado.='<td class="text-center"><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver"); ?></a></td>
                    '; 
                     $fileslist.='if(var'.$campo.'!=\'\') var'.$campo.'=\'<a href="\'+data[i].'.$campo.'+\' target="_blank"> <?php echo JrTexto::_("ver"); ?></a>\';
                  ';
                      }
                      $datoslistadoajax.='var'.$campo.',';

                        }elseif($tipo=='combobox'||$tipo=='radiobutton'||$tipo=='checkbox'){
                          $addjschk=true;
                          if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){
                             if($frm["dattype".$campo]=="enum"){                              
                              $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1));
                              $datoslistado.='<td class="text-center">
                              <a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o"; ?> " data-value="<?php echo $reg["'.$campo.'"]; ?>"
                        data-campo="'.$campo.'" data-txtid="<?php echo $reg["'.$campopk.'"]; ?>" data-valueno='.$valoresopt[0].' data-value2="<?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[1].':'.$valoresopt[0].';?>" >
                        <?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?JrTexto::_('.$valoresopt[0].'):JrTexto::_('.$valoresopt[1].'); ?></a></td>
                    ';
                
                $datoslistadoajax.='
                \'<a style="cursor:pointer;" class="chklistado fa <?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o"; ?> " href="\'+data[i].'.$campo.'+\' target="_blank"> <?php echo JrTexto::_("ver documento"); ?></a>\',
          ';
                             }else{
                              $datoslistado.='<td class="text-center"><a href="javascript:;"  class="btn-activar  \'+(data[i].'.$campo.'==0?\'\':\'active\')+\' " data-campo="'.$campo.'"  data-id="<?php echo $reg["'.$campopk.'"]; ?>"> <i class="fa fa<?php echo !empty($reg["'.$campo.'"])?"-check":""; ?>-circle fa-lg"></i> <?php echo $reg["'.$campo.'"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    ';
                $datoslistadoajax.='
                \'<a href="javascript:;"  class="btn-activar  \'+(data[i].'.$campo.'==0?\'\':\'active\')+\' " data-campo="'.$campo.'"  data-id="\'+data[i].'.$campopk.'+\'"> <i class="fa fa\'+(data[i].'.$campo.'==\'1\'?\'-check\':\'\')+\'-circle-o fa-lg"></i> \'+estados'.$idguisys.'[data[i].'.$campo.']+\'</a>\',
          ';
                             }
                          }else{
                        $datoslistado.='<td><?php echo $reg["'.$campo.'"] ;?></td>
                    ';
          $datoslistadoajax.='data[i].'.$campo.',';
                          }
                        }else{
                        $datoslistado.='<td><?php echo $reg["'.$campo.'"] ;?></td>
                    ';
          $datoslistadoajax.='data[i].'.$campo.',';
                          }
                            }
                        }
                      }                      
                echo $datoslistadoheader;
              ?><th class="sorting_disabled"><span class="nobr"><?php echo "<?php echo JrTexto::_('Actions');?>"; ?></span></th>
                </tr>
              </thead>
              <tbody>
            </tbody>
            </table>
        </div>
    </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo '<?php echo $idgui; ?>';?>" idgui="<?php echo '<?php echo $idgui; ?>';?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo '<?php echo $idgui;?>'; ?>" tb="<?php echo $this->tabla; ?>" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo '<?php echo $idgui;?>'; ?>">
          <input type="hidden" name="<?php echo $campopk; ?>" id="<?php echo $campopk; ?>" value="">
          <?php if(!empty($campos))
          foreach ($campos as $campo){
            $Campo=ucfirst($campo);
            $verenlistado='_chk'.$campo;
            if(empty($frm[$verenlistado])) continue;
            $tipo=@$frm['tipo_'.$campo];
            if($campo!=$campopk){
              //if($tipo!='fk'){
                if(@$frm["tipo2_".$campo]!='system'){ 
            ?><div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo "<?php echo JrTexto::_('".str_replace('_', ' ',$Campo)."');?>"; ?> <span class="required"> * </span></label>
              <?php }
//////////////////////////////////////////////------------text -------------------
              if($tipo=='text'||$tipo=='double'||$tipo=='decimal'||$tipo=='money'){
              ?>  <input type="text"  id="<?php echo $campo ?>" name="<?php echo $campo ?>" required="required" class="form-control" value="">
              <?php 

//////////////////////////////////////////////------------email -------------------
              }elseif($tipo=='email'){
              ?>  <input type="email" id="<?php echo $campo ?>" name="<?php echo $campo ?>" required="required" class="form-control" value="">
              <?php 

//////////////////////////////////////////////------------date -------------------
              }elseif($tipo=='date'){
                    if($frm["tipo2_".$campo]=='user'){$jsdate=true;
                      $jsdatetxt.="
                      $('#".$campo."').datetimepicker({
    lang:'es',
    timepicker:false,
    format:'Y/m/d'
  });";
              ?>  <input name="<?php echo $campo ?>" id="<?php echo $campo ?>" class="verdate form-control" required="required" type="date" value="<?php echo '<?php echo date(\'Y-m-d\') ?>';?>">   
              <?php }elseif($frm["tipo2_".$campo]=='system'){
              ?>  <input type="hidden" id="<?php echo $campo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo date(\'Y-m-d\') ?>';?>">

              <?php } 

//////////////////////////////////////////////------------datetime -------------------
              }elseif($tipo=='datetime'){
                    if($frm["tipo2_".$campo]=='user'){$jsdatetime=true;
                      $jsdatetimetxt.="
  $('#".$campo."').datetimepicker({
    datepicker:false,
    format:'H:i',
    value:'12:00'
  });
  ";  
              ?>  <input name="<?php echo $campo ?>" id="<?php echo $campo ?>" class="verdatetime form-control" required="required" value="">   
              <?php }elseif($frm["tipo2_".$campo]=='system'){
              ?>  <input type="hidden" id="<?php echo $campo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo date(\'Y-m-d\') ?>';?>">

              <?php }

//////////////////////////////////////////////------------time -------------------
              }elseif($tipo=='time'){
                      if($frm["tipo2_".$campo]=='user'){$jstime=true;
                ?>  <input name="<?php echo $campo ?>" id="<?php echo $campo ?>" class="vertime form-control" required="required" type="date" value="<?php echo '<?php echo date(\'H:i\') ?>';?>">   
                <?php }elseif($frm["tipo2_".$campo]=='system'){
                ?>  <input type="hidden"  id="<?php echo $campo ?>" name="<?php echo $campo ?>" value="<?php echo '<?php echo date(\'Y-m-d\') ?>';?>">

                <?php }

//////////////////////////////////////////////------------combobox -------------------
              }elseif($tipo=='combobox'){
              ?> <select id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control" required>
              <option value=""><?php echo '<?php JrTexto::_("Seleccione");?>';?></option>
                <?php if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); ?>

              <option value=<?php echo $valoresopt[0]; ?>><?php echo "<?php echo JrTexto::_(".$valoresopt[0].");?>"; ?></option>
              <option value=<?php echo $valoresopt[1]; ?>><?php echo "<?php echo JrTexto::_(".$valoresopt[1].");?>"; ?></option>
                  <?php }else{ ?>
              <option value="1"><?php echo "<?php echo JrTexto::_('Active');?>"; ?></option>
              <option value="0"><?php echo "<?php echo JrTexto::_('Inactive');?>"; ?></option>
                  <?php } ?>
                <?php }else{ 
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ ?> 
              <option value="<?php echo $i;?>"> <?php echo "<?php echo JrTexto::_('".$i."');?> ".$i; ?></option>
              <?php } }
              ?> 
              </select>
              <?php  

//////////////////////////////////////////////------------checkbox -------------------
              }elseif($tipo=='checkbox'){
                $jschkformulario=true;
                if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); 
                  $jschkformulario=true;
              ?> 

              <a  style="cursor:pointer;" href="javascript:;"  class="btn-activar <?php echo '<?php echo @$frm["'.$campo.'"]==1?"active":"";?>';?>"> <i class="fa fa<?php echo '<?php echo @$frm["'.$campo.'"]=="1"?"-check":"";?>';?>-circle fa-lg"></i> <span></span>
              <input type="hidden"  name="<?php echo $campo ?>" name="<?php echo $campo ?>" value=""> 
              </a>
                  <?php }else{ ?> 
                    <a  style="cursor:pointer;" href="javascript:;"  class="btn-activar <?php echo '<?php echo @$frm["'.$campo.'"]==1?"active":"";?>';?>"> <i class="fa fa<?php echo '<?php echo @$frm["'.$campo.'"]=="1"?"-check":"";?>';?>-circle fa-lg"></i> 
              <input type="hidden" id="<?php echo $campo ?>" name="<?php echo $campo ?>" value=""> 
              </a>

                  <?php } ?>
                <?php }else{ 
                     $jschkformulariomultiple=true;
                    if($frm["dattype".$campo]=="enum"){ 
                      $txtchk=str_replace("'", "", substr($frm["coltype".$campo], 5,-1));
                      $valoresopt=@explode(",",$txtchk); 
                      for($i=0;$i<count($valoresopt);$i++){                       
                       ?>

                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle";?>'; ?>" 
                data-value="<?php echo $valoresopt[$i]; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("'.$valoresopt[$i].'");?>'; ?></span>                 
                 </a><br>
                  <?php    }
                      
                  }else{
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ 
              ?> 
                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle";?>'; ?>" 
                data-value="<?php echo $i; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("Opcion - '.$i.'");?>'; ?></span>                 
                 </a><br>
              <?php 
                  }  
                  }?>
                  <input type="hidden" id="<?php echo $campo ?>" name="<?php echo $campo ?>" value="" >
                  <?php
                }                    

//////////////////////////////////////////////------------radiobutton -------------------
              }elseif($tipo=='radiobutton'){                
                if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                  $jschkformulario=true;
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); 
                  $jschkformulario=true;

              ?>  
               <a  style="cursor:pointer;" href="javascript:;"  class="btn-activar"> <i class="fa fa<?php echo '<?php echo @$frm["'.$campo.'"]=="1"?"-check":"";?>';?>-circle-o fa-lg"></i> <span><?php echo '<?php echo @$frm["'.$campo.'"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive");?>';?></span>
              <input type="hidden" id="<?php echo $campo ?>" name="<?php echo $campo ?>" value=""> 
              </a>

                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[0].':'.$valoresopt[1].');?>'; ?></span>
                 <input type="hidden" id="<?php echo $campo ?>" name="<?php echo $campo ?>" value="" > 
                 </a>
                  <?php }else{ ?>
                  <a style="cursor:pointer;" class="chkformulario fa fa-check-circle-o" data-value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>"  data-valueno="0" data-value2="<?php echo '<?php echo @$frm["'.$campo.'"]==1?1:0;?>';?>">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]==1?"Activo":"Inactivo");?>'; ?></span>
                 <input type="hidden" name="<?php echo $campo ?>" id="<?php echo $campo ?>"  value="" > 
                 </a>
                  <?php } ?>
                <?php }else{ 
                 $jschkformulariomultiple=true;
                    if($frm["dattype".$campo]=="enum"){ 
                      $txtchk=str_replace("'", "", substr($frm["coltype".$campo], 5,-1));
                      $valoresopt=@explode(",",$txtchk); 
                      for($i=0;$i<count($valoresopt);$i++){                       
                       ?>
  <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle";?>'; ?>"  data-value="<?php echo $valoresopt[$i]; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("'.$valoresopt[$i].'");?>'; ?></span>
                 
                 </a><br>
                  <?php    }
                      
                  }else{
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ 
              ?> 
                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle";?>'; ?>" 
                data-value="<?php echo $i; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("Opcion - '.$i.'");?>'; ?></span>
                 
                 </a><br>
              <?php 
                  }  
                  }?>
                  <input type="hidden" name="<?php echo $campo ?>" id="<?php echo $campo ?>" value="" >
                  <?php } 
                   

//////////////////////////////////////////////------------Number -------------------
              }elseif($tipo=='int'||$tipo=='tinyint'||$tipo=='bigint'){
              ?> <input type="number" id="<?php echo $campo ?>" name="<?php echo $campo ?>" step="1" value=""  min="1" class="form-control" required />
              <?php 

//////////////////////////////////////////////------------password -------------------
              }elseif($tipo=='password'){
                if($frm["tipo2_".$campo]=='simple'){
              ?> <input type="password" id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control" required />    
              <?php  }else{ 
              ?> <input type="password" id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control" required /> 
              <?php  }

//////////////////////////////////////////////------------file -------------------
              }elseif($tipo=='file'){
                $jsfile=true;
                if($frm["tipo2_".$campo]=='all'){
                  $tipo='all'; $txtfile=''; 
                ?> <a href="" data-type="all" class="img-thumbnail" id="id<?php echo $campo ?>"> <?php echo "<?php echo JrTexto::_('ver');?>"; ?> </a>
                <?php }elseif($frm["tipo2_".$campo]=='imagen'){
                  $tipo='img';$txtfile='Imagen';
                ?> 
                <?php }elseif($frm["tipo2_".$campo]=='video'){
                  $tipo='vid';$txtfile='Video';
                ?> <video src=""  data-type="video" id="id<?php echo $campo ?>"><?php echo "<?php echo JrTexto::_('Descargar video');?>"; ?> </video>
                <?php }elseif($frm["tipo2_".$campo]=='documentos'){
                  $tipo='doc';$txtfile='Documento';
                ?> <a href=""  data-type="archivo" class="img-thumbnail" id="id<?php echo $campo ?>"> <?php echo "<?php echo JrTexto::_('ver Archivo');?>"; ?></a>
                <?php }elseif($frm["tipo2_".$campo]=='audio'){
                  $tipo='aud';$txtfile='Audio';
                ?> <audio src=""  controls  data-type="audio" class="img-thumbnail" id="id<?php echo $campo ?>"><?php echo "<?php echo JrTexto::_('Descargar Audio');?>"; ?></audio>
                <?php }
                if($txtfile!='Imagen'){
              ?> <span class="btn btn-info btn-file"> <i class="fa fa-upload"></i> <?php echo "<?php echo JrTexto::_('Seleccionar ".$txtfile."');?>"; ?> 
                 <input data-texto="<?php echo $txtfile; ?>" data-campo="<?php echo $campo; ?>" data-action="<?php echo "<?php echo JrAplicacion::getJrUrl(array('".$this->tabla."', 'subir_archivo'));?>?arc=".$campo."&tipo=".$tipo; ?>" type="file" name="file<?php echo $Campo ?>" id="file<?php echo $Campo ?>" class="cargarfile" data-type="<?php  echo $tipo; ?>"></span>
                 <input type="hidden" id="txt<?php echo $Campo ?>" name="<?php echo $campo ?>" class="form-control" required />
                 <iframe name="if_cargar_file" style="display:none"></iframe>
              <?php }else{ 
                $addimgsave.='
                $("#txt'.$Campo.'").val($("img[alt=\'img'.$campo.'\']").cropper("getDataURL", "image/png", 1.0));
                ';
                ?>
            <img src="<?php echo '<?php echo URL_BASE.(!empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:$_imgdefecto);?>'; ?>" alt="imagenfile" file="<?php echo $campo ?>" class="subirfile img-thumbnail" style="max-height:200px; max-width:200px;">
            <input type="hidden" id="<?php echo $campo ?>"  name="<?php echo $campo ?>" value="">

                <!--div class="row">
                  <input type="hidden" name="<?php //echo $campo ?>" id="txt<?php //echo $Campo ?>">
                  <input type="hidden" name="<?php //echo $campo ?>_old" value="<?php //echo '<?php echo @$frm["'.$campo.'"];?>';?>">
                  <div class="col-md-12">
                    <div class="img-container">
                      <img src="<?php //echo '<?php echo !empty($frm["'.$campo.'"])?$this->documento->getUrlBase().$frm["'.$campo.'"]:($this->documento->getUrlStatic()."/media/'.strtolower($tb).'/default.png");?>';?>" alt="img<?php //echo $campo ?>" data-ancho="100"  data-alto="100">
                    </div>
                  </div>
                </div-->
             <?php }

//////////////////////////////////////////////------------textArea -------------------
              }elseif($tipo=='textArea'){
                if($frm["tipo2_".$campo]=='simple'){
              ?> <textarea id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control" ></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='wysihtml5'){
              $jsverwysihtml5 =true;
              ?> <textarea id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="verwysihtml5 form-control" ></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='chkeditor'){
              $jsverchkeditor=true; 
              $js.='CKEDITOR.replace("'.$campo.'");'
              ?> <textarea id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="verchkeditor form-control" ></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='tinymce'){
              $jsvertinymce=true; 
              ?> <textarea id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="vertinymce form-control" ></textarea>
              <?php }

//////////////////////////////////////////////------------foreign key -------------------
              }elseif($tipo=='fk'){
                if($frm["tipofkcomo_".$campo]=='combobox'){
              ?><select id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control">
               <option value=""><?php echo "<?php echo JrTexto::_('Seleccione'); ?>"; ?></option>
              <?php
              echo '<?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') { ?>';
              echo '<option value="<?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]?>" <?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]==@$frm["'.$campo.'"]?"selected":""; ?> ><?php echo $fk'.$campo.'["'.$frm["tipofkver_".$campo].'"] ?></option>';
              echo '<?php } ?>';
              ?>
              </select>
              <?php }elseif($frm["tipofkcomo_".$campo]=='radiobutton'){
                echo '
              <?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                 ?>';
              ?><input type="radio" id="<?php echo $campo ?>" name="<?php echo $campo ?>" >
              <?php 
              echo ' <?php } ?>';
              }elseif($frm["tipofkcomo_".$campo]=='checkbox'){
                echo '
              <?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                 ?>';
              ?><input type="checkbox" id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control">
              <?php echo ' <?php } ?>';
              }elseif($frm["tipofkcomo_".$campo]=='text'){
              ?><input type="text" id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control">
              <?php }elseif($frm["tipofkcomo_".$campo]=='listado'){
              ?><input type="button" id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control listarfk">
              <?php }elseif($frm["tipofkcomo_".$campo]=='system'){
              ?><?php echo '<?php echo $this->fk'.$campo.'["usuario"];?>';?>
              <?php }

//////////////////////////////////////////////------------otraopcion -------------------
              }elseif($frm["tipofkcomo_".$campo]=='money'){?>
              <input type="text"  id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control"
              value="">
              <?php }else{
              ?>   <input type="text"  id="<?php echo $campo ?>" name="<?php echo $campo ?>" class="form-control" value="">
              <?php } if(@$frm["tipo2_".$campo]!='system'){ 
              ?>
            </div>

            <?php }
            }
          }?>

          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="<?php echo $this->tabla; ?>" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo "<?php echo JrTexto::_('Save');?>"; ?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo "<?php echo JrTexto::_('Cancel');?>"; ?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos<?php echo $idguisys ?>='';
function refreshdatos<?php echo $idguisys ?>(){
    tabledatos<?php echo $idguisys ?>.ajax.reload();
}
$(document).ready(function(){  
  var estados<?php echo $idguisys ?>={'1':'<?php echo '<?php echo JrTexto::_("Active") ?>'; ?>','0':'<?php echo '<?php echo JrTexto::_("Inactive") ?>';?>','C':'<?php echo '<?php echo JrTexto::_("Cancelled") ?>';?>'}
  var tituloedit<?php echo $idguisys ?>='<?php echo '<?php echo ucfirst(JrTexto::_("'.$this->tabla.'"))." - ".JrTexto::_("edit"); ?>';?>';
  var draw<?php echo $idguisys ?>=0;
  var _imgdefecto='';
  var ventana_<?php echo $idguisys ?>=$('#ventana_<?php echo '<?php echo $idgui; ?>';?>');
   tabledatos<?php echo $idguisys ?>=ventana_<?php echo $idguisys ?>.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/<?php echo $this->tabla; ?>',
        type: "post",                
        data:function(d){
            d.json=true                   
            <?php echo $datoslistadoheaderajax; ?>
            
            draw<?php echo $idguisys ?>=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw<?php echo $idguisys ?>;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
             <?php echo $fileslist; ?>                   
            datainfo.push([            
              (i+1),
              <?php echo $datoslistadoajax; ?>
              //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].<?php echo $campopk; ?>+'" data-titulo="'+tituloedit<?php echo $idguisys ?>+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].<?php echo $campopk; ?>+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_<?php echo $idguisys ?>.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos<?php echo $idguisys ?>();
  }).on('blur','.textosearchlist',function(ev){refreshdatos<?php echo $idguisys ?>();
  })

  <?php echo  $datosrefh; ?>

 

  ventana_<?php echo $idguisys ?>.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo "<?php echo JrTexto::_('Confirm action');?>";?>',
      content: '<?php echo "<?php echo JrTexto::_('It is sure to delete this record ?'); ?>";?>',
      confirmButton: '<?php echo "<?php echo JrTexto::_('Accept');?>";?>',
      cancelButton: '<?php echo "<?php echo JrTexto::_('Cancel');?>";?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('<?php echo $campopk; ?>',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/<?php echo $this->tabla?>/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos<?php echo $idguisys ?>.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/<?php echo $this->tabla?>/setcampo','masvalores':{<?php echo $campopk;?>:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('<?php echo $campopk;?>',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/<?php echo $this->tabla?>/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos<?php echo $idguisys ?>.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modal<?php echo $this->tabla; ?>(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modal<?php echo $this->tabla; ?>(el);
  })

  /*Formulario*/
  var __modal<?php echo $this->tabla; ?>=function(el){
    var titulo = '<?php echo '<?php echo JrTexto::_("'.ucfirst($this->tabla).'"); ?>'?>';
    var filtros=$('#ventana_<?php echo '<?php echo $idgui; ?>'; ?>');
    var frm=$('#frm<?php echo '<?php echo $idgui; ?>'?>').clone();     
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('<?php echo JrTexto::_('Inactive'); ?>')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('<?php echo JrTexto::_('Active'); ?>');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/<?php echo $this->tabla; ?>/guardar',
          showmsjok:true,
          callback:function(rs){   tabledatos<?php echo $idguisys ?>.ajax.reload(); __cerrarmodal(_md);  }
        });
    })

    var pk=el.attr('pk')||'';
    if(pk!=''){      
      var data=new FormData();
      data.append('<?php echo $campopk;?>',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/<?php echo $this->tabla; ?>',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];  
            //_md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());          
          }
         }
      });
    }else{
      /*if(filtros.find('select#tipo_portal').val()!='')
        _md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());*/
    }
  }
});
<?php echo @$jsout; ?>
</script>