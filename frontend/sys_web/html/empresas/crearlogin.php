<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->infoempresa)) $frm=$this->infoempresa;
$ruta=URL_MEDIA."/static/media/nofoto.jpg";
$imgdefecto=$ruta;
if(!empty($frm["logo"])) $ruta=URL_MEDIA.$frm["logo"];
?>
<style type="text/css">
    .titulo-p{
        float: none !important;
        font-size: 24px !important;
        font-weight: bold !important;
        text-align: center !important;
        width: 100% !important;
    }
    .x_title{
        border-bottom: 2px solid #E6E9ED;
        padding: 1px 5px 6px;
        margin-bottom: 10px;
    }
</style>
<form method="post" id="frmproyecto"  target="" enctype="multipart/form-data" class="form-horizontal form-label-left" >

    <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
    <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
    <input type="hidden" name="fecha" id="fecha" value="<?php echo !empty($frm["fecha"])?$frm["fecha"]:date('Y-m-d');?>">
    <input type="hidden" name="logo" id="logo" value="<?php echo @$frm["logo"];?>">
    <input type="hidden" name="imagenfondo" id="imagenfondo" value="">
    <input type="hidden" name="videofondo" id="videofondo" value="">

    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
                <h2 class="titulo-p text-center">
                    <?php 
                    if($this->paris){
                        echo JrTexto::_('Product Configuration');
                    } else {
                        echo JrTexto::_('Paso 02 : Configuracion de Proyecto');
                    }
                    ?>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label><?php echo JrTexto::_('Name') ?></label>
                        <input type="" name="nombre" class="form-control" placeholder="<?php echo JrTexto::_('Name of the project, headquarters, branch') ?>">
                    </div>
                    <div class="form-group col-md-3">
                        <label><?php echo JrTexto::_('Type Bussiness') ?></label>
                        <div class="cajaselect">
                            <select name="tipoempresa" id="tipoempresa" class="form-control">                     
                                <option value="1" <?php echo @$frm["tipo_empresa"]==1?'selected="selcted"':'';?>>Coorporativo</option>
                                <option value="0" <?php echo @$frm["tipo_empresa"]==0?'selected="selcted"':'';?>>IIEE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label><?php echo JrTexto::_('Language') ?></label>
                        <div class="cajaselect">
                            <select name="idioma" id="changeidioma" class="form-control" idioma="<?php echo $frm["idioma"]; ?>">
                                <option value="ES" <?php echo @$frm["idioma"]=='ES'?'selected="selcted"':'';?>><?php echo ucfirst(JrTexto::_('Spanish')) ?> (ES)</option>
                                <option value="EN" <?php echo @$frm["idioma"]=='EN'?'selected="selcted"':'';?>><?php echo ucfirst(JrTexto::_('English')) ?> (EN)</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label><?php echo JrTexto::_('Vista de listado de cursos') ?></label>
                        <div class="cajaselect">
                            <select name="tipo_portal" id="tipo_portal" class="form-control" tipoportal="<?php echo $frm["tipo_portal"]; ?>">
                                <option value="1" <?php echo @$frm["tipo_portal"]=='1'?'selected="selcted"':'';?>><?php echo ucfirst(JrTexto::_('01 - Candado')) ?></option>
                                <option value="2" <?php echo @$frm["tipo_portal"]=='2'?'selected="selcted"':'';?>><?php echo ucfirst(JrTexto::_('02 - Campus')) ?></option>
                                <!--option value="3" <?php //echo @$frm["tipo_portal"]=='3'?'selected="selcted"':'';?>><?php //echo ucfirst(JrTexto::_('03 - Categorias')) ?></option-->
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label><?php echo JrTexto::_('Link') ?>:</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <?php
                            if($this->paris){
                            ?>
                            <input type="text" value="<?php echo $this->documento->getUrlBase() . @$frm["nombreurl"]; ?>" class="form-control" readonly="">
                            <input type="hidden" name="nombreurl" id="nombreurl" value="<?php echo @$frm["nombreurl"]; ?>">
                            <?php
                            } else {
                            ?>
                            <div class="input-group-prepend">
                            <div class="input-group-text"><?php echo $this->documento->getUrlBase(); ?></div>
                            </div>
                            <input type="text" name="nombreurl" id="nombreurl" value="<?php echo @$frm["nombreurl"]; ?>" class="form-control" placeholder="<?php echo JrTexto::_('EMP001') ?>">
                            <?php
                            }
                            ?>
                        </div>               
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 form-group">
                        <label><?php echo JrTexto::_('Para el admin: Mostrar') ?></label>
                        <div class="cajaselect">
                            <select name="solocursosprincipales" id="solocursosprincipales" class="form-control" tipoportal="<?php echo $frm["solocursosprincipales"]; ?>">
                                <option value="1" <?php echo @$frm["solocursosprincipales"]=='1'?'selected="selcted"':'';?>><?php echo ucfirst(JrTexto::_('Todos los cursos')) ?></option>
                                <option value="2" <?php echo @$frm["solocursosprincipales"]=='2'?'selected="selcted"':'';?>><?php echo ucfirst(JrTexto::_('Solo cursos principales')) ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 form-group">
                        <label style="width: 100%">
                            <?php echo JrTexto::_("Tipo de Mensaje de Bienvenida") ?><br> <?php echo JrTexto::_("Tipo de Archivo") ?></label>                        
                        <div class="btn-group" role="group" aria-label="">
                          <button type="button" class="btn btn-secondary btntipofile" title="texto"><i class="fa fa-font"></i></button>
                          <button type="button" class="btn btn-secondary btntipofile" title="imagen"><i class="fa fa-photo"></i></button>
                          <button type="button" class="btn btn-secondary btntipofile" title="audio"><i class="fa fa-file-audio-o"></i></button>
                          <button type="button" class="btn btn-secondary btntipofile" title="video"><i class="fa fa-video-camera"></i></button>
                          <button type="button" class="btn btn-secondary btntipofile" title="html"><i class="fa fa-html5"></i></button>
                          <button type="button" class="btn btn-secondary btntipofile" title="youtube"><i class="fa fa-youtube"></i></button>
                          <button type="button" class="btn btn-secondary btntipofile" title="link"><i class="fa fa-link"></i></button>
                        </div>
                        <div id="aquitipofile"> 
                        </div> 
                        
                    </div>
                       
                </div>
            </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
                <h2 class="titulo-p text-center">
                    <?php 
                    if($this->paris){
                        echo JrTexto::_('Login Settings');
                    } else {
                        echo JrTexto::_('Formulario de Inicio de Sesion');
                    }
                    ?>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 form-group">
                        <label><?php echo JrTexto::_("Tipo de fondo") ?></label>
                        <div class="cajaselect">
                            <select name="tipofondo" id="tipofondo" class="form-control">                     
                                <option value="imagen">Imagen</option>
                                <option value="video">Video</option>
                                <option value="color">Color de fondo</option>                    
                            </select>
                        </div>
                        <br>
                        <div style="position: relative;" class="frmchangeimage text-center" id="archivofondo">
                            <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                            <div id="sysfileimg" class="aquiseveimagen" data-old='imagen' data-file='<?php echo $imgdefecto; ?>'>
                            <img src="<?php echo $imgdefecto;?>" nombreguardar="<?php echo !empty($frm["idempresa"])?('Emp'.$frm["idempresa"].(!empty($frm["idproyecto"])?('_Pr'.$frm["idproyecto"]):'')):'';?>_fondo" class="__subirfile img-fluid center-block" data-type="imagen" id="imagenfondo" style="max-width: 200px; max-height: 150px;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 form-group text-center"><br>
                        <label><?php echo JrTexto::_("logo") ?>(*)</label>                
                        <div style="position: relative;" class="frmchangeimage text-center" id="archivologo1">
                            <div class="toolbarmouse text-center"><span class="btn btnremoveimagelogo"><i class="fa fa-trash"></i></span></div>
                            <div id="sysfilelogo">
                            <img src="<?php echo $ruta;?>" nombreguardar="<?php echo !empty($frm["idempresa"])?('Emp'.$frm["idempresa"].(!empty($frm["idproyecto"])?('_Pr'.$frm["idproyecto"]):'')):'';?>_logo"  class="__subirfilelogo img-fluid center-block" data-type="imagen" id='logo' style="max-width: 200px; max-height: 150px;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 form-group" style="display: none;" >
                        <label><?php echo JrTexto::_("Bloquear Examenes para continuar con sesiones") ?></label>
                        <div class="cajaselect">
                            <select name="bloqueodeexamenes" id="bloqueodeexamenes" class="form-control">                     
                                <option value="no">No Bloquear</option>
                                <option value="sf">Bloquear solo Examen Final</option>
                                <option value="si">Bloquear todos los Examenes</option>                    
                            </select>
                        </div>
                        <br>
                        <div style="position: relative; display: none;" id="notabloqueoexamenes">
                            <label>Nota aprobatoria minima para desbloquear 1% al 100%</label>
                             <input type="number" min="1" max="100" name="notaminima" id="_notabloqueoexamenes" value="1" class="form-control">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group text-center">
            <button type="submit" class="btn btn-success" >
                <?php 
                if($this->paris){
                    echo '<i class=" fa fa-save"></i> ';
                    echo ucfirst(JrTexto::_('Save'));
                } else {
                    echo ucfirst(JrTexto::_('Continuar'));
                    echo ' <i class=" fa fa-arrow-right"></i>';
                }
                ?> 
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">
 var url_media='<?php echo $this->documento->getUrlBase();?>';
 var imgdefecto='<?php echo $imgdefecto; ?>';
    $(document).ready(function(){
        var jsonlogin={tipologin:1,tipofondo:'imagen',colorfondo:'rgba(20, 92, 186, 0.93)',imagenfondo:imgdefecto,videofondo:'',logo1:''};
        var idempresa=parseInt('<?php echo $frm["idempresa"]; ?>');
        var idproyecto=parseInt('<?php echo $frm["idproyecto"];?>');
   
        $('.imgseleccionaruna').on('click',function(ev){
            let $img=$(this);
            let v=$img.attr('data-value');          
            if($img.hasClass('nosel')){
                $img.closest('.selimagen').find('img').removeClass('Selected').addClass('nosel');
                $img.removeClass('nosel').addClass('Selected');
                if(v=='2') $('.logoopcional').show();
                else $('.logoopcional').hide();
                jsonlogin.tipologin=parseInt(v);
            }else{
                $img.addClass('nosel').removeClass('Selected');
                jsonlogin.tipologin=0;
            }
        });
        var htmltmp={'imagen':'','video':'','color':''};

        $('#tipofondo').on('change',function(ev){
            let tipo=$(this).val();
            let sysfile=$('#sysfileimg');
            let oldtipo=sysfile.attr('data-old');
            let oldfile=sysfile.attr('data-file');
            htmltmp[oldtipo]= sysfile.html(); 
            sysfile.attr('data-old',tipo);
            jsonlogin.tipofondo=tipo; 
            if(htmltmp[tipo]!=""){               
                sysfile.html(htmltmp[tipo]);
                if(tipo=='color') sysfile.siblings('.toolbarmouse').hide(); 
                else sysfile.siblings('.toolbarmouse').show(); 
            }else if(tipo=='imagen'){               
                sysfile.siblings('.toolbarmouse').show();
                let imgf=$('input#imagenfondo').val();
                let imgfondo=imgf!=''?(url_media+imgf):imgdefecto;               
                sysfile.html('<img src="'+imgfondo+'" id="imagenfondo" data-type="imagen" class="__subirfile img-fluid center-block" style="max-width: 200px; max-height: 150px;">')
            }else if(tipo=='video'){               
                sysfile.siblings('.toolbarmouse').show();
                let vif=$('input#videofondo').val();
                if(vif!=''){
                    sysfile.html('<video src="'+url_media+vif+'" id="videofondo" data-type="video" controls="true" style="max-width: 250px; max-height: 200px;"></video><img src="'+_sysUrlBase_+'/static/media/novideo.png" data-type="video" class="invisible img-fluid center-block" style="display:none; max-width: 200px; max-height: 150px;">')
                }else
                sysfile.html('<video src="" id="videofondo" controls="true" data-type="video" class="invisible" style="display:none; max-width: 250px; max-height: 200px;"></video><img src="'+_sysUrlBase_+'/static/media/novideo.png" data-type="video" class=" img-fluid center-block" style="max-width: 200px; max-height: 150px;">');
            }else if(tipo=='color'){
                sysfile.siblings('.toolbarmouse').hide();          
                let colortmp=jsonlogin.colorfondo!=''?jsonlogin.colorfondo:'rgba(20, 92, 186, 0.93)';
                sysfile.html('<input type="" name="colorfondo" value="'+colortmp+'" class="vercolor colorfondo form-control" >');
                sysfile.find('input[name="colorfondo"]').val(colortmp);
                sysfile.find('input[name="colorfondo"]').minicolors({opacity: true,format:'rgb'});
                sysfile.find('input[name="colorfondo"]').minicolors('settings',{value:colortmp});
                $('.fondologin').css('background',colortmp);
            }
        });

        $('.__subirfilelogo').on('click',function(){
            let $img=$(this);            
            __subirfile({file:$img,typefile:'imagen',uploadtmp:true,guardar:true},function(rs){                    
                let f=rs.media.replace(url_media, '');
                $('input#'+$img.attr('id')).val(f);
                $img.attr('src',url_media+f);
            });
        });

        $('#sysfileimg').on('click','img,video',function(){
            let $img=$(this);           
            let ct=$img.closest('#sysfileimg');
            let tf=$img.attr('data-type');
            if(tf=='video')$img=ct.children('video');               
            __subirfile({file:$img,typefile:tf,uploadtmp:true,guardar:true,dirmedia:'empresa/emp_'+idempresa+'/'},function(rs){
                let f=rs.media.replace(url_media, '');               
                if(tf=='video'){
                    $('input#videofondo').val(f);
                    $img.siblings().hide()
                }else{
                    $('input#imagenfondo').val(f); 
                } 
                $img.attr('src',url_media+f);  
                $img.removeClass('invisible').show();
            });
        });

        $('.btnremoveimagelogo').on('click',function(){
            let $img= $(this).closest('.frmchangeimage').find('img');
            $img.attr('src',_sysUrlBase_+'/static/media/nofoto.jpg');
            $('input#'+$img.attr('id')).val('');
        })

        $('.btnremoveimage').on('click',function(){
            let $img= $(this).closest('#archivofondo').find('img');
            if($img.parent().attr('data-old')=='video'){
                $img.attr('src',_sysUrlBase_+'/static/media/novideo.png');
                $img.siblings('video').hide();
                $('input#videofondo').val('');
            }else{
                $img.attr('src',_sysUrlBase_+'/static/media/nofoto.jpg');
                $('input#imagenfondo').val('');
            }                
            $img.removeClass('invisible').show();                     
        })

        $('select#bloqueodeexamenes').on('change',function(ev){
            var v=$(this).val();
            if(v!='no'){
                $('#notabloqueoexamenes').show();
            }else $('#notabloqueoexamenes').hide();
        })

        $('#frmproyecto').bind({    
            submit: function(ev){
                ev.preventDefault();
                var jsontmp1=jsonlogin;
                if(jsonlogin.tipofondo=='video'){ 
                    jsontmp1.videofondo=$('input#videofondo').val();
                    jsontmp1.imagenfondo='';
                  
                }else if(jsonlogin.tipofondo=='imagen'){  
                    jsontmp1.imagenfondo=$('input#imagenfondo').val();
                    jsontmp1.videofondo='';
                }               
                jsontmp1.logo1=$('input#logo').val();

                if($('#aquitipofile').find('#infolink').length){
                    jsonlogin.msjbienvenida=$('#aquitipofile').find('#infolink').val()||'';
                    jsonlogin.msjbienvenidatipo=$('#aquitipofile').find('#infolink').attr('tipolink');
                    if(jsonlogin.msjbienvenida=='undefined'){
                        jsonlogin.msjbienvenida='';
                        jsonlogin.msjbienvenidatipo='';
                    }

                }
                if(jsonlogin.msjbienvenidatipo.toLowerCase()=='texto'){                   
                    let msj=jsontmp1.msjbienvenida;
                    jsontmp1.msjbienvenida=msj.replace(/(\r?\n|\r)/gm, '<br>');                   
                }

                jsonlogin.bloqueodeexamenes=$('select#bloqueodeexamenes').val();
                if( jsonlogin.bloqueodeexamenes=='no'){
                    jsonlogin.notabloqueoexamenes='';
                }else
                    jsonlogin.notabloqueoexamenes=$('#_notabloqueoexamenes').val();

                //jsontmp1.logo2=$('#logo2i').val();
                var jsontmp= JSON.stringify(jsontmp1);
                var tmpfrm=document.getElementById('frmproyecto');
                var formData = new FormData(tmpfrm);                    
                    formData.append('jsonlogin',jsontmp); 
                    formData.append('idiomaold',$('#changeidioma').attr('idioma')||'ES');
                    formData.append('tipo_portal',$('#tipo_portal').val()||2);  
                __sysAyax({
                    fromdata:formData,
                    showmsjok:true,
                    url:_sysUrlBase_+'json/proyecto/guardar',
                    callback:function(rs){
                       let idproyecto=rs.newid;
                       $('#idproyecto').val(idproyecto);
                        //window.top.location.reload();
                       //window.location=_sysUrlBase_+'proyectoempresa/configurarmodulos/?idempresa='+$('#idempresa').val()+'&idproyecto='+idproyecto;
                    }
                });
                return false;
            }
        });

        $('.btntipofile').on('click',function(ev){
            var el_=$(this);
                el_.siblings().removeClass('btn-primary').addClass('btn-secondary');
                el_.addClass('btn-primary').removeClass('btn-secondary');
                var title=el_.attr('title').toLowerCase();
                var aquitipofile=el_.parent().parent().find('#aquitipofile');

                if(title=='texto'){
                    aquitipofile.html('<br><textarea id="infolink" tipolink="Texto" class="form-control" rows="5" placeholder="Copie o escriba su texto de repuesta"  style="resize: none;"></textarea>');  
                }else if(title=='imagen'){
                    var idimagen=__idgui();
                    aquitipofile.html('<input type="hidden" id="infolink" tipolink="imagen" ><div><img src="" class="img-responsive" id="'+idimagen+'" style="max-width:130px; max-height:130px;"></div>');
                    $img=aquitipofile.find('#'+idimagen);
                    __subirfile({file:$img,typefile:'imagen',uploadtmp:true,guardar:true,dirmedia:'empresa/emp_'+idempresa+'/'},function(rs){
                        console.log(rs);              
                        let f=rs.media.replace(url_media, '');
                        aquitipofile.find('#infolink').attr('value',f);
                        $img.attr('src',url_media+f);
                    });
                }else if(title=='video'){
                    var idimagen=__idgui();
                    aquitipofile.html('<input type="hidden" id="infolink"  tipolink="video"><div><video controls src="" class="img-responsive" id="'+idimagen+'" style="max-width:130px; max-height:130px;"></div>');
                    $img=aquitipofile.find('#'+idimagen);
                    __subirfile({file:$img,typefile:'video',uploadtmp:true,guardar:true,dirmedia:'empresa/emp_'+idempresa+'/'},function(rs){                    
                        let f=rs.media.replace(url_media, '');
                        aquitipofile.find('#infolink').attr('value',f);
                        $img.attr('src',url_media+f);
                    });
                }else if(title=='audio'){
                    var idimagen=__idgui();
                    aquitipofile.html('<input type="hidden" id="infolink"  tipolink="audio"><div><audio controls src="" class="img-responsive" id="'+idimagen+'"></div>');
                    $img=aquitipofile.find('#'+idimagen);
                    __subirfile({file:$img,typefile:'audio',uploadtmp:true,guardar:true,dirmedia:'empresa/emp_'+idempresa+'/'},function(rs){                    
                        let f=rs.media.replace(url_media, '');
                        aquitipofile.find('#infolink').attr('value',f);
                        $img.attr('src',url_media+f);
                    });
                }else if(title=='html'){
                    var idimagen=__idgui();
                    aquitipofile.html('<input type="hidden" id="infolink" target="_blank" tipolink="html"><div><a src="img" class="img-responsive" id="'+idimagen+'">Link subido</a></div>');
                    $img=aquitipofile.find('#'+idimagen);
                    __subirfile({file:$img,typefile:'html',uploadtmp:true,guardar:true,dirmedia:'empresa/emp_'+idempresa+'/'},function(rs){                    
                        let f=rs.media.replace(url_media, '');
                        aquitipofile.find('#infolink').attr('value',f);
                        $img.attr('href',url_media+f);
                    });                    
                }else if(title=='link'){
                    aquitipofile.html('<br><input id="infolink"  tipolink="link" value="" placeholder="Copie o escriba el enlace " class="form-control"> ');
                }else if(title=='youtube'){
                    aquitipofile.html('<br><input id="infolink"  tipolink="youtube" value="" placeholder="Copie o escriba el enlace " class="form-control"> ');
                }
        })

                
        if(idempresa!=''){
            var formData = new FormData();
            formData.append('idempresa',idempresa);
            formData.append('idproyecto',idproyecto);
            __sysAyax({
                fromdata:formData,
                showmsjok:false,
                url:_sysUrlBase_+'json/proyecto',                   
                callback:function(rs){
                    if(rs.data.length>0){
                        let info=rs.data[0];
                        $('#idempresa').val(info.idempresa);
                        $('#idproyecto').val(info.idproyecto);
                        $('#fecha').val(info.fecha);
                        var jsontmp=JSON.parse(info.jsonlogin);
                        $.extend(jsonlogin,jsontmp);
                    } 
                    //console.log(jsonlogin);
                    if(jsonlogin.imagenfondo!=''){
                        $('input#imagenfondo').val(jsonlogin.imagenfondo);
                        $('img#imagenfondo').attr('src',url_media+jsonlogin.imagenfondo);
                    }
                    if(jsonlogin.videofondo!=''){
                        $('input#videofondo').val(jsonlogin.videofondo);
                        $('video#videofondo').attr('src',url_media+jsonlogin.videofondo);
                    }
                    if(jsonlogin.bloqueodeexamenes=='no'||jsonlogin.bloqueodeexamenes==''||jsonlogin.bloqueodeexamenes==undefined){
                        $('#bloqueodeexamenes').val('no');
                    }else{
                        //console.log(jsonlogin.notabloqueoexamenes);
                        $('#bloqueodeexamenes').val(jsonlogin.bloqueodeexamenes);
                        $('#_notabloqueoexamenes').val(jsonlogin.notabloqueoexamenes);
                    }
                    $('#bloqueodeexamenes').trigger('change');

                    $('.imgseleccionaruna[data-value="' + parseInt(jsonlogin.tipologin) + '"]').trigger('click');
                    $('#tipofondo').val(jsonlogin.tipofondo).trigger('change');

                    var aquitipofile=$('#aquitipofile');
                    let txtbienvenida=((jsonlogin.msjbienvenida||'')=='undefined')?'':jsonlogin.msjbienvenida;
                    if(jsonlogin.msjbienvenidatipo!=''&& jsonlogin.msjbienvenidatipo!=undefined && jsonlogin.msjbienvenidatipo!='undefined' && txtbienvenida!=''){
                        var title=(jsonlogin.msjbienvenidatipo||'texto').toLowerCase();
                        if(title=='texto'){
                            let txtbienvenida=((jsonlogin.msjbienvenida||'')=='undefined')?'':jsonlogin.msjbienvenida;
                            txtbienvenida=txtbienvenida.replace(/<br>/g,`\n`);
                            aquitipofile.html('<br><textarea id="infolink" tipolink="texto" class="form-control" rows="5" placeholder="Copie o escriba su texto de repuesta"  style="resize: none;">'+txtbienvenida+'</textarea>');  
                        }else if(title=='imagen'){
                        var idimagen=__idgui();
                        aquitipofile.html('<input type="hidden" id="infolink" tipolink="imagen" value="'+jsonlogin.msjbienvenida+'" ><div><img src="'+(jsonlogin.msjbienvenida||'')+'" class="img-responsive" id="'+idimagen+'" style="max-width:130px; max-height:130px;"></div>');
                        }else if(title=='video'){
                            var idimagen=__idgui();
                            aquitipofile.html('<input type="hidden" id="infolink" tipolink="video" value="'+jsonlogin.msjbienvenida+'" ><div><video controls src="'+(jsonlogin.msjbienvenida||'')+'" class="img-responsive" id="'+idimagen+'" style="max-width:130px; max-height:130px;"></div>');
                            $img=aquitipofile.find('#'+idimagen);
                        }else if(title=='audio'){
                            var idimagen=__idgui();
                            aquitipofile.html('<input type="hidden" id="infolink" tipolink="audio" value="'+jsonlogin.msjbienvenida+'" ><div><audio controls src="'+(jsonlogin.msjbienvenida||'')+'" class="img-responsive" id="'+idimagen+'"></div>');
                            $img=aquitipofile.find('#'+idimagen);
                        }else if(title=='html'){
                            var idimagen=__idgui();
                            aquitipofile.html('<input type="hidden" id="infolink" tipolink="html" value="'+jsonlogin.msjbienvenida+'" ><div><a target="_blank"  href="<?php echo $this->documento->getUrlBase(); ?>'+(jsonlogin.msjbienvenida||'')+'" class="img-responsive" id="'+idimagen+'">Link subido</a></div>');
                            $img=aquitipofile.find('#'+idimagen); 
                        }else if(title=='link'){
                            aquitipofile.html('<br><input id="infolink"  tipolink="link" value="'+jsonlogin.msjbienvenida+'"  placeholder="Copie o escriba el enlace " class="form-control"> ');
                        }else if(title=='youtube'){
                            aquitipofile.html('<br><input id="infolink"  tipolink="youtube" value="'+jsonlogin.msjbienvenida+'"  placeholder="Copie o escriba el enlace " class="form-control"> ');
                        }
                        $('.btntipofile[title="'+(title)+'"]').addClass('btn-primary').removeClass('btn-secondary');
                    }

                                    
                    if(jsonlogin.logo1.trim()!=""){$('img#logo1').attr('src',url_media+jsonlogin.logo1);}
                    else{$('img#logo1').attr('src',url_media+($('input#logo1i').val()||imgdefecto));}
                    //if(jsonlogin.logo2.trim()!="") $('img#logo2').attr('src',url_media+jsonlogin.logo2);
                   // else $('img#logo2').attr('src',url_media+($('input#logo2i').val()||imgdefecto));
                }
            });
        }
    });
</script>