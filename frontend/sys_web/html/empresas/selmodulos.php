<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->infoempresa)) $frm=$this->infoempresa;
$ruta=URL_MEDIA."/static/media/nofoto.jpg";
$imgdefecto=$ruta;
if(!empty($frm["logo"])) $ruta=URL_MEDIA.$frm["logo"];
?>

<style type="text/css">
    .span_mnu{
        border: 1px solid #ccc;
        display: block;
        padding: 1.5em 1ex;
        text-align: center;
        background: #cccccc4d;
    }
    .ui-state-highlight{
        display:block;
    }
    .ui-sortable{
        padding-left: 0 !important;
    }
    .titulo-p{
        float: none !important;
        font-size: 24px !important;
        font-weight: bold !important;
        text-align: center !important;
        width: 100% !important;
    }
    .x_title{
        border-bottom: 2px solid #E6E9ED;
        padding: 1px 5px 6px;
        margin-bottom: 10px;
    }
    .mod_mnu{
        display:inline-block;
        padding:1ex;
        margin-right: 0 !important;
        margin-left: 0 !important;
    }
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2 class="titulo-p text-center"><?php echo JrTexto::_('Platform Modules');?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <ul id="mostrarmodulos" class="row"></ul>
        <div class="row">
            <div class="col-md-12 text-center">
                <hr>
                <button id="savesetting" class="btn btn-success"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
           
<div class="modal" tabindex="-1" id="settingpanel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title">Estilo de la pagina</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-6 col-md-6 form-group">
                    <label><?php echo JrTexto::_("Color de fondo") ?></label>
                    <input type="" name="colorfondo" value="" class="vercolor colorfondo form-control" >
                </div>        
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btnaplicarestilo btn btn-success">Aplicar</button>       
            </div>
        </div>
    </div>
</div>

<script>
var py=JSON.parse('<?php echo $frm["jsonlogin"] ?>');
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgdefecto; ?>';
var fotodefecto=imgdefecto;
var jsonproyecto=py;
var idproyecto=parseInt('<?php echo $frm["idproyecto"]; ?>');
var idempresa=parseInt('<?php echo $frm["idempresa"]; ?>');
var dtuserlogin=<?php echo json_encode($this->curusuario); ?>;
var userempresa=JSON.parse(localStorage.getItem("userempresa"));
var userproyecto=JSON.parse(localStorage.getItem("userproyecto"));
$(document).ready(function(){

   
    __sysAyax({ // carga la edicion de los modulos
        showmsjok:false,
        url:_sysUrlBase_+'json/modulos/listado',
        callback:function(rs){            
            var md=rs.data;
            $.each(md,function(i,v){
               $('#mostrarmodulos').append('<li class="col-md-4 btn mod_mnu" data-id="'+v.idmodulo+'" data-link="'+v.link+'" data-nombre="'+v.nombre+'" data-icon="'+v.icono+'" data-nombretraducido="'+v.nombretraducido+'"><span class="span_mnu"><i class="fa '+v.icono+'"></i><br>'+v.nombretraducido+'</span></li>');
            })
            $( "#mostrarmodulos" ).sortable({
                placeholder: "ui-state-highlight",
                stop:function(ev,iu){                   
                   editarmenus();
                }
            });
            $( "#mostrarmodulos" ).disableSelection();
            updatemenus();
        }
    });

    var editarmenus=function(){
        var lis=$('#mostrarmodulos').find('li');
        var smenu=$('.side-menu');
        smenu.find('.smenu').remove();
        var ultimoli=smenu.children('li').last();
        var menus={};
        var idrol=parseInt(dtuserlogin.idrol);
        $.each(lis,function(i,k){
            var v=$(k);
            if(v.hasClass('active')){
                var idmodulo=v.attr('data-id')||0;
                var add=true
                if(idmodulo==7 || idmodulo==1)
                 if(idrol!=10&&idrol!=1)add=false;
               // if((idrol!=10&&idrol!=1)&&idmodulo==7)continue;
               if (add==true){
                menus[i]={id:v.attr('data-id'),nombre:v.attr('data-nombre'),link:v.attr('data-link'),icono:v.attr('data-icon'),texto:v.attr('data-texto')};
                var html='<li  class="ini_mod'+v.attr('data-nombre').toLowerCase()+'  hvr-bounce-to-right smenu smenu'+v.attr('data-id')+'" data-id="'+v.attr('data-id')+'">';
                    html+='<a  href="javascript:void(0);" data-href="'+v.attr('data-link')+'" ><i class="fa '+v.attr('data-icon')+' fa-fw" style="font-size: 20px"></i> ';
                    html+=v.attr('data-nombretraducido')+'</a></li>';
                $(html).insertBefore(ultimoli);
               } 
            }
        });
        jsonproyecto.menus=menus;        
    }

    var updatemenus=function(){       
        if(jsonproyecto.menus!=''&&jsonproyecto.menus!=undefined){ 
            var ultimoliadd=$('#mostrarmodulos').find('li').first();
            $.each(jsonproyecto.menus,function(i,v){
                $($('#mostrarmodulos li[data-id="'+v.id+'"]')).insertBefore(ultimoliadd);              
                $('#mostrarmodulos li[data-id="'+v.id+'"]').addClass('active btn-success');                
            })
            editarmenus();
        }
    }

    $('#mostrarmodulos').on('click','li',function(){
        var li=$(this);
        li.toggleClass('active');
        var id=li.attr('data-id');
        if(li.hasClass('active')){
            li.addClass('btn-success');            
        }else{
            $('.side-menu li.smenu'+id).remove();
            li.removeClass('btn-success');
        }
        editarmenus();
    });

    $('ul.listatabtop').on('click','a',function(ev){
        var url=_sysUrlBase_+'proyecto/cursos/?plt=sintop&idproyecto='+idproyecto+'&idioma='+_sysIdioma_;
        _this=$(this);
        var idcat=_this.attr('idcat')||'';       
        url=url.indexOf('?')==-1?(url+'?plt=sintop'):(url+'&plt=sintop');
        url+='&idcategoria='+idcat;
        $('#iframecontent').attr('src',url);
        _this.addClass('active');
        _this.parent().siblings('li').children('a').removeClass('active');
    })

   
    $('.btnaplicarestilo').click(function(){
        $('.fondocolor').css('background-color',$('#settingpanel').find('input[name="colorfondo"]').val());
    });

    var guardarjsoproyecto=function(){
         //var colortmp=$('.fondocolor').css('background-color');
        //jsonproyecto.colorfondoadmin=colortmp;
        var jsontmp= JSON.stringify(jsonproyecto);
        var formData = new FormData();       
        formData.append('idproyecto',idproyecto);
        formData.append('campo','jsonlogin');
        formData.append('valor',jsontmp);
         __sysAyax({
            fromdata:formData,
            showmsjok:true,
            url:_sysUrlBase_+'json/proyecto/setcampo',
            callback:function(rs){
                window.top.location.reload();
            }           
        });
    }
    $('#savesetting').on('click', function (e) {
        guardarjsoproyecto();                                
    })
    
    //iniciarver();
})
</script>