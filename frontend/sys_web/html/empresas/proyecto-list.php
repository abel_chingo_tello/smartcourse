<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$imgdefecto=URL_MEDIA."/static/media/nofoto.jpg";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Proyecto"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">  
          <div class="row filtros"> 
            <div class="col-xs-12 col-sm-6 col-md-3 " >
              <!--label>Idioma</label-->
              <div class="select-ctrl-wrapper select-azul">
                <select name="idioma" id="idioma" class="form-control select-ctrl">
                    <option value=""><?php echo ucfirst(JrTexto::_("All Idioma"))?></option>
                    <option value="ES"><?php echo ucfirst(JrTexto::_("Español"))?></option>
                    <option value="EN"><?php echo ucfirst(JrTexto::_("Ingles"))?></option>                              
                </select>
              </div>
            </div>                                                                        
            <div class="col-xs-12 col-sm-6 col-md-3 " >
              <!--label>Tipo_empresa</label-->
              <div class="select-ctrl-wrapper select-azul">
                <select name="tipo_empresa" id="tipo_empresa" class="form-control select-ctrl">
                    <option value=""><?php echo ucfirst(JrTexto::_("Tipo Empresa"))?></option>
                    <option value="1"><?php echo ucfirst(JrTexto::_("Ugel / Dre"))?></option>
                    <option value="0"><?php echo ucfirst(JrTexto::_("Corporativo"))?></option>                              
                </select>
              </div>
            </div>                                                                                      
            <div class="col-xs-12 col-sm-6 col-md-3 " >
                <!--label>Tipo_portal</label-->
              <div class="select-ctrl-wrapper select-azul">
                <select name="tipo_portal" id="tipo_portal" class="form-control select-ctrl">
                    <option value=""><?php echo ucfirst(JrTexto::_("Tipo Portal"))?></option>
                    <option value="1"><?php echo ucfirst(JrTexto::_("Modulos"))?></option>
                    <option value="2"><?php echo ucfirst(JrTexto::_("Campus"))?></option>                              
                </select>
              </div>
            </div>                                
            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="form-group">
                <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="x_content">
    	    <div class="row">         
            <div class="col table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr class="headings">
                    <th>#</th>
                    <th><?php echo JrTexto::_("Empresa") ;?></th>                   
                    <th><?php echo JrTexto::_("Proyecto") ;?></th>
                    <th><?php echo JrTexto::_("Nombreurl") ;?></th>
                    <th><?php echo JrTexto::_("Logo") ;?></th>
                    <th><?php echo JrTexto::_("Idioma") ;?></th>
                    <th><?php echo JrTexto::_("Tipo empresa") ;?></th>
                    <th><?php echo JrTexto::_("Tipo portal") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                   </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="proyecto" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idproyecto" id="idproyecto" value="">         
          <input type="hidden" name="fecha" id="fecha" value="<?php echo date('Y-m-d'); ?>">
          <input type="hidden" name="logo" id="logo" value="">
          <input type="hidden" name="imagenfondo" id="imagenfondo" value="">
          <input type="hidden" name="videofondo" id="videofondo" value="">
          
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <label><?php echo JrTexto::_('Empresa');?> <span class="required"> * </span></label>
            <div class="select-ctrl-wrapper select-azul">
              <select id="idempresa" name="idempresa" class="form-control" required>
                <?php if(!empty($this->empresas)){
                  foreach ($this->empresas as $k => $v){ ?>                    
                  <option value="<?php echo $v["idempresa"]; ?>"><?php echo $v["nombre"]; ?></option>
                  <?php }} ?>
              </select>
            </div>              
          </div>

          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <label><?php echo JrTexto::_('Type Bussiness') ?></label>
            <div class="select-ctrl-wrapper select-azul">
              <select name="tipo_empresa" id="tipo_empresa" class="form-control">                     
                  <option value="1">Coorporativo</option>
                  <option value="0">IIEE</option>
              </select>
            </div>
          </div>

          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <label><?php echo JrTexto::_('Tipo portal') ?></label>
            <div class="select-ctrl-wrapper select-azul">
              <select name="tipo_portal" id="tipo_portal" class="form-control">                     
                  <option value="1">Vista Modulos</option>
                  <option value="2">Vista Campus</option>
              </select>
            </div>
          </div>


          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <label><?php echo JrTexto::_('Language') ?></label>
            <div class="select-ctrl-wrapper select-azul">
              <select name="idioma" id="idioma" class="form-control">                     
                  <option value="ES">Español</option>
                  <option value="EN">Ingles</option>
              </select>
            </div>
          </div>

          <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>             
              <input type="text"  id="nombre" name="nombre" required="required" class="form-control" value="" placeholder="Nombre de proyecto - sucursal">
          </div>

          <div class="form-group col-md-6 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('link');?> <span class="required"> * </span></label>             
              <input type="text"  id="nombreurl" name="nombreurl" required="required" class="form-control" value="" placeholder="Ej. Emp01">
          </div>

          <div class="col-md-6 col-sm-12 form-group">
              <label><?php echo JrTexto::_("Tipo de fondo") ?></label>
              <div class="select-ctrl-wrapper select-azul">
                  <select name="tipofondo" id="tipofondo" class="form-control">                     
                      <option value="imagen">Imagen</option>
                      <option value="video">Video</option>
                      <option value="color">Color de fondo</option>                    
                  </select>
              </div>
              <div style="position: relative;" class="frmchangeimage text-center" id="archivofondo">
                  <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                  <div id="sysfileimg" class="aquiseveimagen" data-old='imagen' data-file='<?php echo $imgdefecto; ?>'>
                  <img src="<?php echo $imgdefecto;?>" nombreguardar="_fondo" class="__subirfile img-fluid center-block" data-type="imagen" id="imagenfondo" style="max-width: 200px; max-height: 150px;">
                  </div>
              </div>
          </div>
          <div class="col-sm-12 col-md-6 form-group">
             <label><?php echo JrTexto::_("Bloquear Examenes") ?></label> 
              <div class="select-ctrl-wrapper select-azul">
                  <select name="bloqueodeexamenes" id="bloqueodeexamenes" class="form-control">                     
                    <option value="no">No Bloquear</option>
                    <option value="sf">Bloquear solo Examen Final</option>
                    <option value="si">Bloquear todos los Examenes</option>                
                  </select>
              </div>              
              <div style="position: relative;" class="frmchangeimage text-center" id="archivologo1">
                  <div class="toolbarmouse text-center">Logo : <span class="btn btnremoveimagelogo"><i class="fa fa-trash"></i></span></div>
                  <div id="sysfilelogo">
                  <img src="<?php echo $imgdefecto;?>" nombreguardar="_logo"  class="__subirfilelogo img-fluid center-block" data-type="imagen" id='logo' style="max-width: 200px; max-height: 150px;">
                  </div>
              </div>
          </div>
          <div class="clearfix"></div> 
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="proyecto" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5eab07e159884='';
function refreshdatos5eab07e159884(){
    tabledatos5eab07e159884.ajax.reload();
}
$(document).ready(function(){  
  var estados5eab07e159884={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5eab07e159884='<?php echo ucfirst(JrTexto::_("proyecto"))." - ".JrTexto::_("edit"); ?>';
  var draw5eab07e159884=0;
  var _imgdefecto='';
  var ventana_5eab07e159884=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5eab07e159884=ventana_5eab07e159884.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/proyecto',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.idempresa=$('#cbidempresa').val();           
            d.idioma=$('#idioma').val();
            d.tipo_empresa=$('#tipo_empresa').val();
            d.tipo_portal=$('#tipo_portal').val();
            d.texto=$('#texto').val();                        
            draw5eab07e159884=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5eab07e159884;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
             
            var varlogo=data[i].logo||'';
            varlogo=varlogo.replace(_imgdefecto,'');                            
            if(varlogo!='') varlogo='<img src="'+_sysUrlBase_+varlogo+'" class="img-thumbnail" style="max-height:70px; max-width:50px;">';
                                             
            datainfo.push([            
              (i+1),data[i].empresa,data[i].nombre,data[i].nombreurl,varlogo,data[i].idioma,
               (data[i].tipo_empresa=='1'?'IIEE':'Empresa'),(data[i].tipo_portal=='1'?'Modulos':'Campus'),
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idproyecto+'" idempresa="'+data[i].idempresa+'" data-titulo="'+tituloedit5eab07e159884+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idproyecto+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5eab07e159884.on('keydown','.textosearchlist',function(ev){
    if(ev.keyCode===13) refreshdatos5eab07e159884();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5eab07e159884();
  }).on('change','#idioma',function(ev){refreshdatos5eab07e159884();
  }).on('change','#tipo_empresa',function(ev){refreshdatos5eab07e159884();
  }).on('change','#tipo_portal',function(ev){refreshdatos5eab07e159884();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5eab07e159884();
  })
 

  ventana_5eab07e159884.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idproyecto',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/proyecto/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5eab07e159884.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idproyecto',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/proyecto/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5eab07e159884.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    _modalproyecto(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    _modalproyecto(el);
  })

  /*Formulario*/
  var _modalproyecto=function(el){    
    var titulo = '<?php echo JrTexto::_("Proyecto"); ?>';
    var frm=$('#frm<?php echo $idgui; ?>').clone();
    var htmltmp={'imagen':'','video':'','color':''};
    var jsonlogin={tipologin:1,tipofondo:'imagen',colorfondo:'rgba(20, 92, 186, 0.93)',imagenfondo:_imgdefecto,videofondo:'',logo1:''};
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
      ev.preventDefault();      
      var id='frm'+__idgui();
      $(this).attr('id',id);
      var fele = document.getElementById(id);
      var jsontmp1=jsonlogin;
      if(jsonlogin.tipofondo=='video'){ 
        jsontmp1.videofondo=_md.find('input#videofondo').val();
         jsontmp1.imagenfondo='';          
      }else if(jsonlogin.tipofondo=='imagen'){  
        jsontmp1.imagenfondo=_md.find('input#imagenfondo').val();
        jsontmp1.videofondo='';
      }

      if(_md.find('#aquitipofile #infolink').length){
        jsonlogin.msjbienvenida=_md.find('#aquitipofile #infolink').val();
        jsonlogin.msjbienvenidatipo=_md.find('#aquitipofile #infolink').attr('tipolink')||'';
      }
      _md.find('select#idempresa').removeAttr('disabled');
      jsonlogin.bloqueodeexamenes=_md.find('#bloqueodeexamenes').val();
      var jsontmp= JSON.stringify(jsontmp1);
      var data=new FormData(fele);
      data.append('jsonlogin',jsontmp);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/proyecto/guardar',
        showmsjok:true,
        callback:function(rs){   
          var filtros=$('#ventana_<?php echo $idgui; ?>');
          filtros.find('select#idioma').val(_md.find('select#idioma').val());
          filtros.find('select#tipo_empresa').val(_md.find('select#tipo_empresa').val());
          filtros.find('select#tipo_portal').val(_md.find('select#tipo_portal').val());
          tabledatos5eab07e159884.ajax.reload(); __cerrarmodal(_md);  
        }
      });
    }).on('change','select#tipofondo',function(ev){
      let tipo=$(this).val();
      var sysfile=_md.find('#sysfileimg');
      let oldtipo=sysfile.attr('data-old');
      let oldfile=sysfile.attr('data-file');
      htmltmp[oldtipo]= sysfile.html();
      sysfile.attr('data-old',tipo);
      jsonlogin.tipofondo=tipo; 
      if(htmltmp[tipo]!=""){               
        sysfile.html(htmltmp[tipo]);
        if(tipo=='color') sysfile.siblings('.toolbarmouse').hide(); 
        else sysfile.siblings('.toolbarmouse').show(); 
      }else if(tipo=='imagen'){               
        sysfile.siblings('.toolbarmouse').show();
        let imgf=_md.find('input#imagenfondo').val();
        let imgfondo=imgf!=''?(_sysUrlBase_+imgf):_imgdefecto;               
          sysfile.html('<img src="'+imgfondo+'" id="imagenfondo" data-type="imagen" class="__subirfile img-fluid center-block" style="max-width: 200px; max-height: 150px;">')
      }else if(tipo=='video'){               
          sysfile.siblings('.toolbarmouse').show();
          let vif=_md.find('input#videofondo').val();
          if(vif!=''){
            sysfile.html('<video src="'+_sysUrlBase_+vif+'" id="videofondo" data-type="video" controls="true" style="max-width: 200px; max-height: 200px;"></video><img src="'+_sysUrlBase_+'/static/media/novideo.png" data-type="video" class="invisible img-fluid center-block" style="display:none; max-width: 200px; max-height: 150px;">')
          }else
          sysfile.html('<video src="" id="videofondo" controls="true" data-type="video" class="invisible" style="display:none; max-width: 200px; max-height: 200px;"></video><img src="'+_sysUrlBase_+'/static/media/novideo.png" data-type="video" class=" img-fluid center-block" style="max-width: 200px; max-height: 150px;">');
      }else if(tipo=='color'){
          sysfile.siblings('.toolbarmouse').hide();          
          let colortmp=jsonlogin.colorfondo!=''?jsonlogin.colorfondo:'rgba(20, 92, 186, 0.93)';
          sysfile.html('<input type="" name="colorfondo" value="'+colortmp+'" class="vercolor colorfondo form-control" >');
          sysfile.find('input[name="colorfondo"]').val(colortmp);
          sysfile.find('input[name="colorfondo"]').minicolors({opacity: true,format:'rgb'});
          sysfile.find('input[name="colorfondo"]').minicolors('settings',{value:colortmp});
          _md.find('.fondologin').css('background',colortmp);
      }
    }).on('click','.__subirfilelogo',function(ev){
      let $img=$(this);            
      __subirfile({file:$img,typefile:'imagen',uploadtmp:true,guardar:true},function(rs){                    
        let f=rs.media.replace(_sysUrlBase_, '');
          _md.find('input#'+$img.attr('id')).val(f);
          $img.attr('src',_sysUrlBase_+f);
      });
    }).on('click','#sysfileimg img, #sysfileimg video',function(){
      let $img=$(this);           
      let ct=$img.closest('#sysfileimg');
      let tf=$img.attr('data-type');
      if(tf=='video')$img=ct.children('video');               
      __subirfile({file:$img,typefile:tf,uploadtmp:true,guardar:true,dirmedia:'empresa/emp_'+idempresa+'/'},function(rs){
          let f=rs.media.replace(_sysUrlBase_, '');               
          if(tf=='video'){
              _md.find('input#videofondo').val(f);
              $img.siblings().hide()
          }else{ _md.find('input#imagenfondo').val(f);  } 
          $img.attr('src',_sysUrlBase_+f);  
          $img.removeClass('invisible').show();
     });
    }).on('click','.btnremoveimagelogo',function(){
      let $img= $(this).closest('.frmchangeimage').find('img');
      $img.attr('src',_sysUrlBase_+'/static/media/nofoto.jpg');
      _md.find('input#'+$img.attr('id')).val('');
    }).on('click','.btnremoveimage',function(){
      let $img= $(this).closest('#archivofondo').find('img');
      if($img.parent().attr('data-old')=='video'){
          $img.attr('src',_sysUrlBase_+'/static/media/novideo.png');
          $img.siblings('video').hide();
          _md.find('input#videofondo').val('');
      }else{
          $img.attr('src',_sysUrlBase_+'/static/media/nofoto.jpg');
          _md.find('input#imagenfondo').val('');
      }                
      $img.removeClass('invisible').show();                     
    })

    var pk=el.attr('pk')||'';
    if(pk!=''){      
      var idempresa=el.attr('idempresa')||0;
      var data=new FormData();
      data.append('idproyecto',pk);
      data.append('idempresa',idempresa);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/proyecto',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];
            _md.find('input#idproyecto').val(dt.idproyecto);
            _md.find('select#idempresa').val(dt.idempresa);
            _md.find('select#idempresa').attr('disabled','disabled');
            _md.find('select#tipo_empresa').val(dt.tipo_empresa);
            _md.find('select#tipo_portal').val(dt.tipo_portal);
            _md.find('select#idioma').val(dt.idioma);
            _md.find('input#fecha').val(dt.fecha);
            _md.find('input#logo').val(dt.logo);
            _md.find('input#nombreurl').val(dt.nombreurl);
            _md.find('input#nombre').val(dt.nombre);
            _md.find('img.__subirfile').attr('nombreguardar','E'+idempresa+'_P'+idproyecto+'_'+_md.find('img.__subirfile').attr('id')||'fondo');
            _md.find('img.__subirfilelogo').attr('nombreguardar','E'+idempresa+'_P'+idproyecto+'_'+_md.find('img.__subirfilelogo').attr('id')||'logo');
            if(dt.jsonlogin!=''){
              var jsontmp=JSON.parse(dt.jsonlogin);
              $.extend(jsonlogin,jsontmp);
            }
            if(jsonlogin.imagenfondo!=''){
              _md.find('input#imagenfondo').val(jsonlogin.imagenfondo);
              _md.find('img#imagenfondo').attr('src',_sysUrlBase_+jsonlogin.imagenfondo);
            }
            if(jsonlogin.videofondo!=''){
              _md.find('input#videofondo').val(jsonlogin.videofondo);
              _md.find('video#videofondo').attr('src',_sysUrlBase_+jsonlogin.videofondo);              
            }
            if(jsonlogin.bloqueodeexamenes=='no'||jsonlogin.bloqueodeexamenes==''||jsonlogin.bloqueodeexamenes==undefined){
              _md.find('#bloqueodeexamenes').val('no');
            }else{
              _md.find('#bloqueodeexamenes').val(jsonlogin.bloqueodeexamenes);
            }

            if(dt.logo.trim()!=""){_md.find('img#logo').attr('src',_sysUrlBase_+dt.logo);}
            _md.find('select#tipofondo').val(jsonlogin.tipofondo).trigger('change');
          }
         }
      });
    }else{
      var filtros=$('#ventana_<?php echo $idgui; ?>');
      if(filtros.find('select#idioma').val()!='')
        _md.find('select#idioma').val(filtros.find('select#idioma').val());
      if(filtros.find('select#tipo_empresa').val()!='')
        _md.find('select#tipo_empresa').val(filtros.find('select#tipo_empresa').val());
      if(filtros.find('select#tipo_portal').val()!='')
        _md.find('select#tipo_portal').val(filtros.find('select#tipo_portal').val());
    }
  }
});
</script>