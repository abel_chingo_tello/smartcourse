<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$imgdefecto=$this->documento->getUrlStatic()."/media/nofoto.jpg";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Empresa"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> <?php echo JrTexto::_('add') ?>',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <div class="row" id="addselect">
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="select-ctrl-wrapper select-azul">
                <select name="estado" id="estado" class="form-control select-ctrl">
                    <option value=""><?php echo ucfirst(JrTexto::_("All Estado"))?></option>
                    <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                </select>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="select-ctrl-wrapper select-azul">
                <select name="tipo_matricula" id="tipo_matricula" class="form-control select-ctrl">
                  <option value=""><?php echo ucfirst(JrTexto::_("All Tipo_matricula"))?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_("Groups"))?></option>
                  <option value="2"><?php echo ucfirst(JrTexto::_("Areas"))?></option>                              
                </select>
              </div>
            </div> 
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="form-group">              
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>           
          </div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th>                 
                    <th><?php echo JrTexto::_("Ruc") ;?></th>
                    <th><?php echo JrTexto::_("Logo") ;?></th>                  
                    <th><?php echo JrTexto::_("Telefono") ;?></th>
                    <th><?php echo JrTexto::_("Representante") ;?></th>                  
                    <th><?php echo JrTexto::_("Correo") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>                  
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>            
            </div>
          </div>
        </div>
      </div>     
    </div>
  </div>
</div>
<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frmempresa" id="frm-<?php echo $idgui; ?>"  target="" tb="bolsa_empresas" enctype="multipart/form-data" class="frmventana form-horizontal form-label-left" idgui="<?php echo $idgui; ?>">
        <input type="hidden" name="idempresa" id="idempresa" value="">       
        <input type="hidden" name="logo" id="logo" value="">
        <div class="row">
          <div class="col-12 col-sm-7 col-md-7 form-group">
            <div class="row">
              <div class="col-12 form-group">
                <label><?php echo JrTexto::_('Nombre');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="nombre" name="nombre" required="required" class="form-control" value="" placeholder="<?php echo JrTexto::_('Nombre de la empresa') ?>">
              </div>
              <div class="col-12 form-group">
                <label><?php echo JrTexto::_('Razon social');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="rason_social" name="rason_social" class="form-control" value=""  placeholder="<?php echo JrTexto::_('Razon social de la empresa') ?>">
              </div>
              <div class="col-12 form-group">
                <label><?php echo JrTexto::_('Dirección');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="direccion" name="direccion" class="form-control" value=""  placeholder="<?php echo JrTexto::_('Dirección de la empresa') ?>">
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-5 col-md-5 form-group">
            <div class="row">
              <div class="col form-group text-center">
                <label><?php echo JrTexto::_('Logo');?> </label>              
                <div style="position: relative;" class="frmchangeimage">
                  <div class="toolbarmouse text-center">                  
                    <span class="btn btnremoveimage"><i class="fa fa-trash"></i></span>
                  </div>
                  <div id="sysfilelogo<?php echo $idgui; ?>" class="aquiseveimagen">
                <img src="<?php echo $imgdefecto;?>" nombreguardar="" class="__subirfile img-fluid center-block" style="max-width: 200px; max-height: 150px;">
              </div>                
              </div>
              </div>          
            </div>
          </div>
          <div class="col-12">
            <div class="row">
              <div class="col-12 col-sm-6 col-md-6 form-group">
                <label><?php echo JrTexto::_('Ruc');?> <span class="required"> </span></label>              
                <input type="text"  id="ruc" name="ruc"  class="form-control" value="<?php echo @$frm["ruc"];?>" placeholder="<?php echo JrTexto::_('RUC Ej:12345678901') ?>">
              </div>
              <div class="col-12 col-sm-6 col-md-6 form-group">
                <label><?php echo JrTexto::_('Telefono');?> <span class="required"> </span></label>              
                <input type="text"  id="telefono" name="telefono"  class="form-control" value="<?php echo @$frm["telefono"];?>" placeholder="<?php echo JrTexto::_('Telefono de la empresa Ej: 2014 215 1215') ?>">
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-12  form-group">
                <label><?php echo JrTexto::_('Representante');?> <span class="required"> </span></label>              
                <input type="text"  id="representante" name="representante"  class="form-control" value="<?php echo @$frm["representante"];?>" placeholder="<?php echo JrTexto::_('Representante de  la empresa Ej. Abel chingo') ?>">
              </div>
              <div class="col-12  form-group">
                <label><?php echo JrTexto::_('Correo');?> <span class="required"> </span></label>              
                <input type="email" id="correo" name="correo"  class="form-control" value="<?php echo @$frm["correoempresa"];?>"    placeholder="<?php echo JrTexto::_('Correo de la empresa o representante Ej. soyusuario@miempresa.com') ?>">
              </div>                  
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col-12 form-group text-center">
          <button id="btn-save" type="submit" tb="bolsa_empresas" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save'); ?> </button>
          <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel'); ?></a>
        </div>
      </div>
      </form>     
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5ea365eaf3f11='';
function refreshdatos5ea365eaf3f11(){
    tabledatos5ea365eaf3f11.ajax.reload();
}
$(document).ready(function(){  
  var estados5ea365eaf3f11={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5ea365eaf3f11='<?php echo ucfirst(JrTexto::_("bolsa_empresas"))." - ".JrTexto::_("edit"); ?>';
  var draw5ea365eaf3f11=0;
  var _imgdefecto='<?php echo $imgdefecto; ?>';
  var ventana_5ea365eaf3f11=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5ea365eaf3f11=ventana_5ea365eaf3f11.find('.table').DataTable(
    { "searching": false,
      "processing": false,
      "ajax":{
        url:_sysUrlBase_+'json/bolsa_empresas',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.estado=$('#estado').val(),
            d.tipo_matricula=$('#tipo_matricula').val(),
            d.texto=$('#texto').val(),                        
            draw5ea365eaf3f11=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5ea365eaf3f11;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){             
            var varlogo=data[i].logo||'';
            varlogo=varlogo.replace(_imgdefecto,'');                            
            if(varlogo!='') varlogo='<img src="'+_sysUrlBase_+varlogo+'" class="img-thumbnail" style="max-height:70px; max-width:50px;">';
            datainfo.push([            
             (i+1), data[i].nombre,  data[i].ruc,  varlogo,  data[i].telefono,data[i].representante, data[i].correo,
             '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idempresa+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5ea365eaf3f11[data[i].estado]+'</a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si" pk="'+data[i].idempresa+'" href="#" data-titulo="'+tituloedit5ea365eaf3f11+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idempresa+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5ea365eaf3f11.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5ea365eaf3f11();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5ea365eaf3f11();
  }).on('change','#estado',function(ev){refreshdatos5ea365eaf3f11();
  }).on('change','#tipo_matricula',function(ev){refreshdatos5ea365eaf3f11();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5ea365eaf3f11();
  })
 

  ventana_5ea365eaf3f11.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idempresa',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/bolsa_empresas/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5ea365eaf3f11.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/bolsa_empresas/setcampo','masvalores':{idempresa:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idempresa',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/bolsa_empresas/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5ea365eaf3f11.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var el=$(this);
    _modalfrmempresa(el);
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    _modalfrmempresa(el);
  })

  var _modalfrmempresa=function(el){
        var titulo = '<?php echo JrTexto::_('Empresa') ?>';
        var frm=$('#frm<?php echo $idgui; ?>').clone();
        var _md = __sysmodal({'html': frm,'titulo': titulo});        
        var cbo=$('#addselect').clone();
        cbo.children('div.col-md-4').removeClass('col-md-4').addClass('col-md-6');
        _md.find('#addfrmselect').html(cbo.html());
        _md.find('input#idcompetencia').val(el.attr('pk')||'');
        var estado=el.attr('estado')||'0';
        var nombre=el.attr('nombre')||'';
        if(estado=='0'){
          _md.find('a.chkformulario').removeClass('fa-check-circle-o').addClass('fa-circle-o');
          _md.find('a.chkformulario').children('span').text('<?php echo JrTexto::_('Inactive') ?>');
          _md.find('a.chkformulario').children('input').val(0);
        }
        if(nombre!=''){
          _md.find('textarea[name="nombre"]').val(nombre);
        }
        _md.on('change','select.selchange',function(ev){
          ev.preventDefault();
          var el=$(this);
          var id=el.attr('id');
          $('#addselect select#'+id).val(el.val()).trigger('change');
          el.parent().nextAll().remove();
          var vv=$('#addselect select#'+id).parent().nextAll().clone().removeClass('col-md-4').addClass('col-md-6');
          _md.find('#addfrmselect').append(vv);
        }).on('change','select#idcategoria',function(ev){
          ev.preventDefault();
          $('#addselect select#idcategoria').val($(this).val()).trigger('change');
        }).on('click','.chkformulario',function(ev){
          _this = $(this);           
          if(_this.hasClass('fa-check-circle-o')) {            
            _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
            _this.children('span').text('<?php echo JrTexto::_('Inactive'); ?>')
            _this.children('input').val(0);
          }else{            
            _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
            _this.children('span').text('<?php echo JrTexto::_('Active'); ?>');
            _this.children('input').val(1);
          }
        }).on('click','.__subirfile',function(){
          $img=$(this);
          __subirfile({file:$img,typefile:'imagen',guardar:true,dirmedia:'empresa/'},function(rs){
            let f=rs.media.replace(_sysUrlBase_, '');
            $('input#logo').val(f);
          });
        }).on('submit','form.frmventana',function(ev){
            ev.preventDefault();
            var id=__idgui();
            $(this).attr('id',id);
            var fele = document.getElementById(id);
            var data=new FormData(fele);
            __sysAyax({ 
              fromdata:data,
              url:_sysUrlBase_+'json/bolsa_empresas/guardar',
              showmsjok:true,
              callback:function(rs){   tabledatos5ea365eaf3f11.ajax.reload(); __cerrarmodal(_md);  }
            });
        })

        var pk=el.attr('pk')||'';
        var cargardatos=function(){
          if(pk!=''){
            var dt=new FormData();
            dt.append('idempresa',pk)
            __sysAyax({ 
              fromdata:dt,
              url:_sysUrlBase_+'json/bolsa_empresas',
              showmsjok:false,
              async:false,
              callback:function(rs){
                if(rs.data.length==1){
                  var dinfo=rs.data[0];
                  _md.find('input#idempresa').val(dinfo.idempresa);
                  _md.find('input#nombre').val(dinfo.nombre);
                  _md.find('input#rason_social').val(dinfo.rason_social);
                  _md.find('input#direccion').val(dinfo.direccion);
                  _md.find('input#ruc').val(dinfo.ruc);
                  _md.find('input#telefono').val(dinfo.telefono);
                  _md.find('input#representante').val(dinfo.representante);
                  _md.find('input#correo').val(dinfo.correo);
                }
              }
            });
          }
        }
        cargardatos();
  }



});
</script>