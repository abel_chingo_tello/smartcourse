<?php
defined('RUTA_BASE') or die();
?>
<div class="container">
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12  text-center">
<?php /*
        	<div id="wp-excepcion-error" class="wp-excepcion">
                <h2><strong>Opss!</strong></h2>
                <div class="disculpa"><?php echo JrTexto::_('Sorry, there has-been the following errors')?>.</div>                
                <p class="mensaje"><?php echo $this->pasarHtml($this->msj)?></p>
            </div>
*/ ?>
            <div class="jumbotron my-vh-full my-font my-column my-center">
                <h1><strong>Opss!</strong></h1>
                
                
                
                <div class="alert alert-danger" >
                    <p class="mensaje my-rm-margin"><?=JrTexto::_("In process")?>...</p>
                </div>  

                <div class="">
                    <a class="btn btn-default" href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Go back')?></a>
                </div>
            </div>
        </div>
	</div>
</div>