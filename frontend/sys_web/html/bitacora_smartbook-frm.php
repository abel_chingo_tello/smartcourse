<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/bitacora_smartbook">&nbsp;<?php echo JrTexto::_('Bitacora_smartbook'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="bitacora_smartbook" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idbitacora" id="idbitacora" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idbitacora alum smartbook');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdbitacora_alum_smartbook" name="idbitacora_alum_smartbook" required="required" class="form-control" value="<?php echo @$frm["idbitacora_alum_smartbook"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idcurso');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdcurso" name="idcurso" required="required" class="form-control" value="<?php echo @$frm["idcurso"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idsesion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdsesion" name="idsesion" required="required" class="form-control" value="<?php echo @$frm["idsesion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Idusuario');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtIdusuario" name="idusuario" required="required" class="form-control" value="<?php echo @$frm["idusuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Pestania');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtPestania" name="pestania" required="required" class="form-control" value="<?php echo @$frm["pestania"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Total pestanias');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtTotal_pestanias" name="total_pestanias" required="required" class="form-control" value="<?php echo @$frm["total_pestanias"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Fechahora');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtFechahora" name="fechahora" required="required" class="form-control" value="<?php echo @$frm["fechahora"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Progreso');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtProgreso" name="progreso" required="required" class="form-control" value="<?php echo @$frm["progreso"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Otros datos');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtOtros_datos" name="otros_datos" required="required" class="form-control" value="<?php echo @$frm["otros_datos"];?>">
                                  
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveBitacora_smartbook" type="submit" tb="bitacora_smartbook" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('bitacora_smartbook'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/bitacora_smartbook/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/bitacora_smartbook');
                }
              }
          });
       }
  })
})
</script>

