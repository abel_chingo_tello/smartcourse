<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/ambiente">&nbsp;<?php echo JrTexto::_('Ambiente'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idambiente" id="idambiente" value="<?php echo $this->pk;?>">
          <div class="form-group tipoempresa1">
            <label class="control-label"><?php echo JrTexto::_('local');?> <span class="required"> * </span></label>
            <div class="select-ctrl-wrapper select-azul">
            <select id="txtidlocal" name="idlocal" required="required" class="form-control">
             <option value="0"><?php echo JrTexto::_('Único'); ?></option>
            <?php 
            if(!empty($this->fkidlocal))
              foreach ($this->fkidlocal as $fkidlocal) { ?><option value="<?php echo $fkidlocal["idlocal"]?>" <?php echo $fkidlocal["idlocal"]==@$frm["idlocal"]?"selected":""; ?> ><?php echo $fkidlocal["nombre"] ?></option><?php } ?>
            </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Número');?> <span class="required"> * </span></label>
                <div class="">
                  <input type="number"  id="txtNumero" name="numero" required="required" class="form-control" value="<?php echo @$frm["numero"];?>">
                                    
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Capacidad');?> <span class="required"> * </span></label>
              <div class="">
               <input type="number" id="txtCapacidad" name="capacidad" step="1" value="<?php echo !empty($frm["capacidad"])?$frm["capacidad"]:1;?>"  min="1" class="form-control" required />
                                  
              </div>
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Tipo');?> <span class="required"> * </span></label>
              <div class="select-ctrl-wrapper select-azul">
                <select id="txttipo" name="tipo" class="form-control" required="required">
                 <option value=""><?php echo JrTexto::_('Seleccionar'); ?></option>
                <?php 
                if(!empty($this->fktipo))
                  foreach ($this->fktipo as $fktipo) { ?><option value="<?php echo $fktipo["codigo"]?>" <?php echo $fktipo["codigo"]==@$frm["tipo"]?"selected":""; ?> ><?php echo $fktipo["nombre"] ?></option><?php } ?>
                </select></div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Turno');?> <span class="required"> * </span></label>
              <div class="select-ctrl-wrapper select-azul">
              <select id="txtturno" name="turno" class="form-control" required="required">
               <option value=""><?php echo JrTexto::_('Seleccionar'); ?></option>
              <?php 
              if(!empty($this->fkturno))
                foreach ($this->fkturno as $fkturno) { ?><option value="<?php echo $fkturno["codigo"]?>" <?php echo $fkturno["codigo"]==@$frm["turno"]?"selected":""; ?> ><?php echo $fkturno["nombre"] ?></option><?php } ?>
              </select></div>
            </div>
            </div>
          </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
              <div class="">
                <a dataactive="<?php echo JrTexto::_('Activo');?>" datainactive="<?php echo JrTexto::_('Inactivo');?>" style="cursor:pointer;" class="btn-activar <?php echo @$frm["estado"]==1?"active":"";?>"  data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                  <i class="fa fa<?php echo @$frm["estado"]?'-check':''; ?>-circle-o fa-lg"></i>
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="estado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" >
               </a>                                  
              </div>
            </div>

         
            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveAmbiente" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Guardar');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('ambiente'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm_ambiente.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui; ?>');
    if(!frm.hasClass('ismodaljs')) frm_ambiente($('#vent-<?php echo $idgui; ?>'));
  })
</script>