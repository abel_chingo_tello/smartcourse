<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>        
        <li class="active">&nbsp;<?php echo JrTexto::_("Roles"); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?><div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">            
                                                                                                                            
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 text-center">             
               <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/roles/agregar"" ><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
	   <div class="row">         
         <div class="col">
            <table class="table table-striped ">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th>
                    <th><?php echo JrTexto::_("Abreviatura") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th><?php echo JrTexto::_("Imagen") ;?></th>
                    <th><?php echo JrTexto::_("Fecha creacion") ;?></th>
                    <th><?php echo JrTexto::_("Fecha actualizacion") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5d7a5408b53a9='';
function refreshdatos5d7a5408b53a9(){
    tabledatos5d7a5408b53a9.ajax.reload();
}
$(document).ready(function(){  
  var estados5d7a5408b53a9={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5d7a5408b53a9='<?php echo ucfirst(JrTexto::_("roles"))." - ".JrTexto::_("edit"); ?>';
  var draw5d7a5408b53a9=0;
  var _imgdefecto='';
  $('.textosearchlist').on('keydown',function(ev){
    if(ev.keyCode===13) refreshdatos5d7a5408b53a9();
  }).blur(function(ev){ refreshdatos5d7a5408b53a9()})

  
  $('.btnbuscar').click(function(ev){
    refreshdatos5d7a5408b53a9();
  });
  tabledatos5d7a5408b53a9=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
            {'data': '<?php echo JrTexto::_("Abreviatura") ;?>'},
            {'data': '<?php echo JrTexto::_("Estado") ;?>'},
            {'data': '<?php echo JrTexto::_("Imagen") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecha_creacion") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecha_actualizacion") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'json/roles',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw5d7a5408b53a9=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5d7a5408b53a9;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            var varimagen=data[i].imagen||'';
            varimagen=varimagen.replace(_imgdefecto,'');
            if(varimagen!='') varimagen='<img src="'+_sysUrlBase_+varimagen+'" class="img-thumbnail" style="max-height:70px; max-width:50px;">';
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,
                '<?php echo JrTexto::_("Abreviatura") ;?>': data[i].abreviatura,
              '<?php echo JrTexto::_("Estado") ;?>': '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idrol+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5d7a5408b53a9[data[i].estado]+'</a>',
              '<?php echo JrTexto::_("Imagen") ;?>':varimagen,
                '<?php echo JrTexto::_("Fecha_creacion") ;?>': data[i].fecha_creacion,
              '<?php echo JrTexto::_("Fecha_actualizacion") ;?>': data[i].fecha_actualizacion,
                            //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/roles/editar/?id='+data[i].idrol+'" data-titulo="'+tituloedit5d7a5408b53a9+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idrol+'" ><i class="fa fa-trash"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idrol',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/roles/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5d7a5408b53a9.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/roles/setcampo','masvalores':{idrol:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idrol',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/roles/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5d7a5408b53a9.ajax.reload();
          }
        }
    });
  })/*.on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5d7a5408b53a9';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Roles';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  }) */
});
</script>