<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->proyecto)) $frm=$this->proyecto;
$ruta="static/media/web/nofoto.jpg";
$imgdefecto=$ruta;
$logodefecto=URL_MEDIA.'static/media/sinlogo.png';
$logo='';
if(!empty($frm["logoempresa"])) $logo=$frm["logoempresa"];
if(!empty($frm["logo"])) $logo=$frm["logo"];
$ifoto=strripos($logo, "static/");
if($ifoto>-1){
    $logo=substr($logo,$ifoto);
    //if(is_file(RUTA_BASE.$logo) && is_readable(RUTA_BASE.$logo)){
        $logo=URL_BASE.$logo;
    //}else 
       // $logo=$logodefecto;
} else $logo=$logodefecto;
$jsonlogin=!empty($frm["jsonlogin"])?$frm["jsonlogin"]:"";

?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.css">
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.min.js"></script>
<script>
    if(this.location.href!=top.location.href){
        window.top.location.href=window.top.location.href;
    }
</script>

<style>
    .fondologin{   
        height:99.9vh;
        width:100%;
        position:relative;
        overflow:hidden;
    }
    .contentmedia {
        position: absolute;
        height: 100%;
        width: 100%;
        /*opacity: 0.95;*/
    }
    .contentmedia img, .contentmedia video{
        margin: auto;
        position: absolute;
        z-index: 1;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        min-width: 100%;
        min-height: 100%;
        opacity:0.85;
    }
</style>

<div class="fondologin">
    <div class="fondo contentmedia" style=""></div>
    <div class="">
        <a class="hiddenanchor" id="torecuperar"></a>
        <a class="hiddenanchor" id="tologin"></a>
        <a class="hiddenanchor" id="tovalidarcodigo"></a>
        <a class="hiddenanchor" id="tonewregistro"></a>
        
        <div id="wrapper">
            <div id="login" class="animate form ">
                <section class="login_content text-center">
                    <?php if(!empty($this->msjErrorLogin)):?>
                    <div class="alert alert-warning" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only"><?php echo ucfirst(JrTexto::_('attention'));?>:</span> <?php echo JrTexto::_('combination of email and password incorrect')?>
                    </div>
                    <?php endif;?>
                    <img src="<?php echo $logo; ?>" class="img-fluid img-responsive" alt="_logo_" id="logo">
                    
                    <form method="post" id="frmlogin">
                        <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
                        <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
                        <input type="hidden" name="codigosuscripcion" id="codigosuscripcion" value="" >
                         <input type="hidden" name="idgrupoaula" id="idgrupoaula" value="">
                        <h1><?php echo ucfirst(JrTexto::_('Log in'));?></h1>
                        
                        <div class="form-group has-feedback" style="position: relative;">
                            <input type="text" class="form-control" placeholder="<?php echo JrTexto::_('User')?>" required="required" name="usuario" id="usuario" value="<?php echo $this->pasarHtml($this->usuario)?>" maxlength="80" autocomplete="off"/>
                            <span class="fa fa-user form-control-feedback" style="position: absolute; top: 1ex; left:1ex !important;"></span>
                        </div>
                        <div class="form-group has-feedback" style="position: relative;">
                            <input type="password" autocomplete="off" class="form-control" placeholder="<?php echo JrTexto::_('Password')?>" required="required" name="clave" id="clave"/>
                            <span class="fa fa-lock form-control-feedback" style="position: absolute; top: 1ex; left:1ex !important;"></span>
                        </div>
                        <div>
                            <button type="submit" id="#btn-enviar-formInfoLogin" class="btn btn-blue" style="width: 100% !important; border-radius: 5px !important;">
                            <i class="fa fa-key"></i> <?php echo ucfirst(JrTexto::_('login'));?></button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <p class="change_link" style="margin-bottom: 0px;"><?php echo ucfirst(JrTexto::_('have you forgotten your password?'))?><br>
                                <a href="#torecuperar" class="to_recuperar"> <?php echo ucfirst(JrTexto::_('restore password'))?></a>
                            </p>
                            <div class="clearfix"></div>                           
                            <!--div>
                                <h1></h1>

                                <p>© 2016 <?php //echo JrTexto::_('All rights reserved').' <br>'.$this->pasarHtml($this->oNegConfig->get('nombre_sitio'));?> </p>
                            </div-->
                        </div>
                        <div><hr>
                            <a href="#tovalidarcodigo" class="btn btn-success" style="font-size: 16px;  font-size: 12px;  font-weight: 200;  text-transform: uppercase;">
                            <i class="fa fa-code" ></i> <span style="   font-weight: 500; text-shadow: none;"><?php echo ucfirst(JrTexto::_('activate subscription code'));?></span></a>
                        </div>
                        <div class="separator" >
                            <p class="change_link" style="margin-bottom: 0px;"><?php echo ucfirst(JrTexto::_('Change language'));?><br>
                                <a href="?idioma=ES" class="">(ES) <?php echo JrTexto::_('Spanish'); ?></a>|
                                <a href="?idioma=EN" class="">(EN) <?php echo JrTexto::_('English');?></a>|
                                <a href="?idioma=PT" class="">(PT) <?php echo JrTexto::_('Portuguese');?></a>
                            </p>

                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="recuperar" class="animate form">
                <section class="login_content">
                    <img src="<?php echo $logo; ?>" class="img-fluid img-responsive" alt="_logo_" id="logo">
                    <form method="post" id="formRecupClave" onsubmit="return false;">
                        <input type="hidden" name="idempresa" value="<?php echo @$frm["idempresa"];?>">
                        <input type="hidden" name="idproyecto" value="<?php echo @$frm["idproyecto"];?>">                       
                        <h1><?php echo ucfirst(JrTexto::_('recover password'));?></h1>
                        <div class="form-group has-feedback">
                            <input type="text" name="usuario" id="usuariorecover" class="form-control" placeholder="<?php echo ucfirst(JrTexto::_('user or email'));?>" required="required" maxlength="80" />
                            <span class="glyphicon glyphicon-envelope form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div>
                            <button type="submit" id="#btn-enviar-formInforecuperarclave" class="btn btn-primary submits"> <?php echo ucfirst(JrTexto::_('recover password'));?></button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <p class="change_link">
                                <a href="#tologin" class="to_recuperar"><?php echo ucfirst(JrTexto::_('Return to login'));?></a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <!-- <div>
                                <h1></h1>

                                <p>© 2016 <?php //echo JrTexto::_('All rights reserved').' <br>'.$this->pasarHtml($this->oNegConfig->get('nombre_sitio'));?> </p>
                            </div> -->
                        </div>
                        <div class="separator" >
                            <p class="change_link" style="margin-bottom: 0px;"><?php echo ucfirst(JrTexto::_('Change language'));?><br>
                                <a href="?idioma=ES#torecuperar" class="">(ES) <?php echo JrTexto::_('Spanish'); ?></a>|
                                <a href="?idioma=EN#torecuperar" class="">(EN) <?php echo JrTexto::_('English');?></a>|
                                <a href="?idioma=PT#torecuperar" class="">(PT) <?php echo JrTexto::_('Portuguese');?></a>
                            </p>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="validarcodigo" class="animate form">
                <section class="login_content">
                    <img src="<?php echo $logo; ?>" class="img-fluid img-responsive" alt="_logo_" id="logo">
                    <form method="post" id="formcodigosuscripcion" onsubmit="return false;"> 
                        <input type="hidden" name="idempresa" value="<?php echo @$frm["idempresa"];?>">
                        <input type="hidden" name="idproyecto" value="<?php echo @$frm["idproyecto"];?>">                      
                        <h1><?php echo JrTexto::_('validate your code here')?></h1>
                        <div class="form-group has-feedback" style="position: relative;">
                            <input type="text" name="codigo" id="codigo" class="form-control" style="text-transform:uppercase;" onkeypress="return validarcodigos(event)" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="<?php echo JrTexto::_('Code')?>" required="required" maxlength="150" />
                            <span class="fa fa-code form-control-feedback" aria-hidden="true" style="position: absolute; top: 1.25ex; left:1.25ex !important;"></span>
                        </div>
                        <div>
                            <button type="submit" id="#btn-enviar-formcodigosuscripcion" class="btn btn-primary submits"> <?php echo ucfirst(JrTexto::_('validate code'));?></button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <p class="change_link">
                                <a id="returntologin" href="#tologin" class="to_recuperar"><?php echo ucfirst(JrTexto::_('return to login'));?></a>
                                <a href="#tonewregistro" style="display: none;"><?php echo ucfirst(JrTexto::_('new registration'));?></a>
                            </p>
                            <div class="clearfix"></div>
                        </div>
                        <div class="separator" >
                            <p class="change_link" style="margin-bottom: 0px;"><?php echo JrTexto::_('Change language')?><br>
                                <a href="?idioma=ES#tovalidarcodigo" class="">(ES) <?php echo JrTexto::_('Spanish'); ?></a>|
                                <a href="?idioma=EN#tovalidarcodigo" class="">(EN) <?php echo JrTexto::_('English');?></a>|
                                <a href="?idioma=PT#tovalidarcodigo" class="">(PT) <?php echo JrTexto::_('Portuguese');?></a>
                            </p>
                        </div>
                    </form>
                    
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="newregistro" class="animate form" style="    max-height: 100vh; overflow-y: auto;">
                <section class="login_content" >
                    <img src="<?php echo $logo; ?>" class="img-fluid img-responsive" alt="_logo_" id="logo">
                    <form method="post" id="formnewregistro" onsubmit="return false;">
                        <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
                        <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
                        <input type="hidden" name="idpersona" id="idpersona" value="">
                        <h1><?php echo JrTexto::_(' Register')?></h1>
                        <div class="row">
                            <div class="col-md-4  col-sm-6 col-xs-12  form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" id="label_pais"><?php echo JrTexto::_('Country'); ?> (<span>*</span>)</label><br>
                                <select name="idpais" id="idpais" class="form-control idpais w3-small data-required" onchange="/*cboTipoDoc(this)*/" style="text-transform:uppercase;"> 
                                    <!--option value="1" data-longitud="8" data-mask="99999999"></option-->
                                </select>
                                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
                            </div>
                            <div class="col-md-4  col-sm-6 col-xs-12  form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" id="label_tipodocumento"><?php echo JrTexto::_('Document type'); ?> (<span>*</span>)</label><br>
                                <select name="tipodocumento" id="tipodocumento" class="form-control cmbtipodocumento w3-small data-required" onchange="/*cboTipoDoc(this)*/">                                
                                </select>
                                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
                            </div>
                            <div class="col-md-4  col-sm-6 col-xs-12  form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" id="label_numdocumento"><?php echo JrTexto::_('document number'); ?></label><br>
                                <input type="text" name="numdocumento" id="numdocumento" class="form-control w3-small txtdocumento data-required" maxlength="8" placeholder="99999999" onkeyup="/*buscarPersonaExterna(this, 2, 8)*/" data-longitud="8" onblur="/*fillzerofieldDNI(this)*/">
                                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
                            </div>

                            <div class="col-md-4  col-sm-6 col-xs-12 form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" id="label_primer_apellido" ><?php echo JrTexto::_('Last name'); ?></label><br>
                                <input type="text" class="form-control w3-small txtapellido1 data-required" name="apellido1" id="apellido1" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="<?php echo JrTexto::_('paternal'); ?>" autocomplete="off">
                                <!--input type="text" class="form-control" name="compania" id="compania" value="" autocomplete="off" /-->
                                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
                            </div>
                            <div class="col-md-4  col-sm-6 col-xs-12  form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" id="label_apellido2"><?php echo JrTexto::_('Second surname'); ?></label><br>
                                <input type="text" class="form-control w3-small txtapellido2 data-required" name="apellido2" id="apellido2" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="<?php echo JrTexto::_('maternal'); ?>" autocomplete="off">
                                <!--input type="text" class="form-control" name="compania" id="compania" value="" autocomplete="off" /-->
                                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
                            </div>
                            <div class="col-md-4  col-sm-6 col-xs-12  form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" id="label_nombres" ><?php echo JrTexto::_('First name'); ?> </label><br>
                                <input type="text" class="form-control w3-small txtnombres data-required" name="nombres" id="nombres" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="<?php echo JrTexto::_('first name'); ?>" autocomplete="off">
                                <!--input type="text" class="form-control" name="compania" id="compania" value="" autocomplete="off" /-->
                                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
                            </div>
                            <div class="col-md-4  col-sm-6 col-xs-12  form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" label="label_sexo"><?php echo JrTexto::_('Gender'); ?></label><br>
                                <select class="form-control w3-small txtsexo data-required" name="sexo" id="sexo" style="text-transform:uppercase;">
                                    <option value="M"><?php echo JrTexto::_("Male");?></option>
                                    <option value="F"><?php echo JrTexto::_("Female");?></option>
                                </select>                                
                            </div>
                            <div class="col-md-4  col-sm-6 col-xs-12  form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;"><?php echo JrTexto::_('Birth date'); ?> (*)</label><br>
                                <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control w3-small txtsexo data-required" max="<?php echo (date('Y')-5).date('-m-d'); ?>" min="<?php echo (date('Y')-70)."-01-01"; ?>" value="" >     
                            </div>
                            <div class="col-md-4  col-sm-6 col-xs-12  form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;"><?php echo JrTexto::_('Age'); ?></label><br>
                                <input type="text" class="form-control" name="edad" id="edad" value="" autocomplete="off" readonly="readonly" />                                
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" id="label_telefono"><?php echo JrTexto::_('Cell phone number'); ?> (*)</label><br>
                                <input type="tel" class="form-control" name="telefono" id="telefono" value="" autocomplete="off" max placeholder="" data-longitud="15" maxlength="15" placeholder="99999999" style="padding-left: 50px;" />
                                <div style="position: relative; width: 100%;">                                    
                                    <span class="" style="position: absolute; bottom: 1ex; left:1ex !important;" id="paistelcode"></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;" id="label_correo"><?php echo JrTexto::_('Email'); ?> (*)</label><br>
                                <input type="email" class="form-control" name="correo" id="correo" value="" autocomplete="off" placeholder="Ej: tucorreo@empresa.com" />
                                <!--div style="position: relative; width: 100%;">                                    
                                <span class="fa fa-envelope form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span>
                                </div-->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback" style="position: relative;">
                                <label style="text-align: left; float: left;"><?php echo JrTexto::_('Academic level'); ?></label><br>
                                <input type="text" class="form-control" name="gradoinstruccion" id="gradoinstruccion" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="" autocomplete="off" placeholder="<?php echo JrTexto::_('teacher');  ?>" />
                                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
                            </div>
                        </div>
                        <div>
                            <button type="submit" id="#btn-enviar-newregistro" class="btn btn-primary submits"> <?php echo JrTexto::_(' Register')?></button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <p class="change_link">
                                <a href="#tologin" class="to_recuperar"><?php echo JrTexto::_('I am already registered, log in');?>.</a>
                                <a href="#tologin" class="returntologinsincodigo to_recuperar"><?php echo JrTexto::_("I won't use my code, just log in") ?>.</a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <!--div>
                                <p>© 2016 <?php //echo JrTexto::_('All rights reserved').' <br>'.$this->pasarHtml($this->oNegConfig->get('nombre_sitio'));?> </p>
                            </div-->
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>            
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo URL_BASE; ?>static/libs/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
function isNumber(value){ return typeof value === 'number' && !isNaN(value); }
function isObject(value){ return _typeof(value) === 'object' && value !== null; }
function isFunction(value){ return typeof value === 'function'; }
function toArray(value){ return Array.from ? Array.from(value) : slice.call(value); }
function forEach(data, callback){
  if(data && isFunction(callback)){
    if(Array.isArray(data) || isNumber(data.length) /* array-like */){
        toArray(data).forEach(function(value, key){callback.call(data, value, key, data); });
    }else if(isObject(data)){
      Object.keys(data).forEach(function(key){callback.call(data, data[key], key, data); });
    }
  }
  return data;
}
function validarcodigos(e){
     tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9\-]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function solonumerosyletras(e){
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
var url_media=_sysUrlBase_;
var imgdefecto='<?php echo $imgdefecto; ?>';
var jsonlogin={tipologin:1,tipofondo:'imagen',colorfondo:'rgba(155, 146, 178, 0.41)',imagenfondo:imgdefecto,videofondo:'',logo1:''};
var idempresa='<?php echo !empty($frm["idempresa"])?$frm["idempresa"]:0; ?>';
var idproyecto='<?php echo !empty($frm["idproyecto"])?$frm["idproyecto"]:0; ?>';
var jsonproyecto=JSON.parse(`<?php echo !empty($jsonlogin)?str_replace('\n','<br>',$jsonlogin):'{}';?>`);
var jsonvalidarcodigo=null;
var _sysUrlsks_='<?php echo _http_.DOMINIO_SKS; ?>/json/';
var fechaservidor='<?php echo date('Y-m-d');?>';

var cargopaises=false;
var idrepresentante=null;
var cantidad_descarga=null;
var label_pais='<?php echo JrTexto::_('Country'); ?> (*)';
var label_tipodocumento='<?php echo JrTexto::_('Document type'); ?> (*)';
var label_numdocumento='<?php echo JrTexto::_('Document number'); ?> (*)';
var label_primer_apellido='<?php echo JrTexto::_('Last name'); ?> (*)';
var label_apellido2='<?php echo JrTexto::_('Second surname'); ?>';
var label_nombres='<?php echo JrTexto::_('First name'); ?> (*)';
var label_sexo='<?php echo JrTexto::_('Gender'); ?>';
var label_telefono='<?php echo JrTexto::_('Cell phone number'); ?> (*)';
var label_correo='<?php echo JrTexto::_('Email'); ?> (*)';

//var _sysIdioma_='ES';
$(document).ready(function(){
    $('.returntologinsincodigo').click(function(ev){
        $('#formcodigosuscripcion').find('#codigo').val('');
        $('#frmlogin #codigosuscripcion').val('');
        window.location.hash ='#tologin';
    });

    var desactivarboton=function(){
        let user=$('#frmlogin #usuario').val()||'';
        let clave=$('#frmlogin #clave').val()||'';
        if(user=='' && clave==''){
            $('a[href="#tovalidarcodigo"]').parent().show();
        }else{
            $('a[href="#tovalidarcodigo"]').parent().hide();
        }
    }
    $('#frmlogin #usuario').on('change',function(){ desactivarboton(); });
    $('#frmlogin #clave').on('change',function(){ desactivarboton(); });
    desactivarboton();
    var cargarpaises=function(){
        //console.log(idrepresentante);
        //idrepresentante=12;
        if(idrepresentante==null){  
            $('.returntologinsincodigo').trigger('click');
        }

        //if(cargopaises==false){
            Swal.fire({type:'info', html:'Procesando',willOpen:()=>{Swal.showLoading();}});            
            var data=new FormData();
            data.append('estado',1); 
            data.append('idrepresentante',idrepresentante);
            data.append('buscardocumentos',1);          
              __sysAyax({ 
                fromdata:data, //http://localhost/smartmanager/json/?idrepresentante=2&buscardocumentos=1
                  url:_sysUrlsks_+'representantes_paises/buscardocumentos/?idrepresentante='+idrepresentante+'&buscardocumentos=1',                  
                  callback:function(rs){                    
                    if(rs.code==200){
                        let data=rs.data;
                        $('#newregistro #idpais').children('option').remove();
                        $.each(data,function(i,v){
                            if($('#newregistro #idpais option[value="'+v.idpais+'"]').length==0)
                            $('#newregistro #idpais').append(`<option value="${v.idpais}" abrev="${v.abreviacion}" telefono-prefijo="${v.prefijo_telefono||''}" telefono_fijo="${v.cant_telfijo||''}" cant_telmovil="${v.cant_telmovil||''}" >${v.nombre_pais}</option>`);
                        })
                        cargopaises=true;
                        Swal.close(); 
                        $('#newregistro #idpais').trigger('change');
                    }else{
                        __notificar({
                            type:'danger',
                            title:'Error',
                            html:rs.msj                     
                        });
                    }
                  }
            });            
        //}
    }

    var activarsuscripcion=function(datos){   
        var data=new FormData();
            data.append('usuario',datos.usuario); 
            data.append('numerodocumento',datos.dni);
            data.append('tipodocumento',datos.tipodocumento);
            data.append('codigosuscripcion',datos.codigosuscripcion);
            data.append('idempresa',idempresa);
            data.append('idproyecto',idproyecto);
            if(datos.idpersona!='' && datos.idpersona!=undefined) data.append('idpersona',datos.idpersona);
            data.append('cantidad_descarga',cantidad_descarga);
            __sysAyax({ 
                fromdata:data, //http://localhost/smartmanager/json/?idrepresentante=2&buscardocumentos=1
                url:_sysUrlBase_+'json/descargas_asignadas/asignar'
            });
    }

    $('#newregistro #idpais').change(function(e){
        let pais=$(this);
        let idpais=$(this).val();
        Swal.fire({type:'info', html:'Procesando',willOpen:()=>{Swal.showLoading();}});            
        var data=new FormData();
        data.append('estado',1);
        data.append('idpais',idpais);
        __sysAyax({ 
            fromdata:data,
              url:_sysUrlsks_+'tipo_documento',
              callback:function(rs){                    
                if(rs.code==200){
                    let data=rs.data;
                    $('#newregistro #tipodocumento').children('option').remove();
                    $.each(data,function(i,v){
                        $('#newregistro #tipodocumento').append(`<option value="${v.id}" mask="${v.mask}" longitud="${v.longitud}">${v.nombre}</option>`);
                    })
                    cargopaises=true;
                    let nompais=(pais.children('option[value="'+idpais+'"]').attr('abrev')||'').toLowerCase();

                    $('#label_pais').text(label_pais);
                    $('#label_tipodocumento').text(label_tipodocumento);
                    $('#label_numdocumento').text(label_numdocumento);
                    $('#label_primer_apellido').text(label_primer_apellido);
                    $('#label_apellido2').text(label_apellido2);
                    $('#label_nombres').text(label_nombres);
                    $('#label_sexo').text(label_sexo);
                    $('#label_telefono').text(label_telefono);
                    $('#label_correo').text(label_correo);
                    //console.log(nompais);                   
                   // if(nompais=='usa' || nompais=='mex'){
                        $('#newregistro #apellido2').closest('div').hide();
                        $('#newregistro #apellido2').removeAttr('required').val('-');
                        $('#newregistro #gradoinstruccion').closest('div').hide();
                        $('#newregistro #gradoinstruccion').removeAttr('required').val('-');
                        $('#newregistro #telefono').removeAttr('required');
                        $('#newregistro #edad').closest('div').hide();
                        $('#newregistro #tipodocumento').closest('div').hide();
                        $('#newregistro #numdocumento').closest('div').hide();
                        $('#newregistro #sexo').closest('div').hide();
                        $('#newregistro #fecha_nacimiento').closest('div').hide();
                        $('#newregistro #correo').closest('div').removeClass('col-md-4').addClass('col-md-8');                        
                    //}else{
                    //    $('#newregistro #apellido2').closest('div').show();



                    if(nompais=='mex'){
                        $('#newregistro #apellido2').val('').closest('div').show()
                        $('#newregistro #apellido2').removeAttr('required').removeClass('data-required');
                        $('#label_primer_apellido').text('<?php echo JrTexto::_('First surname'); ?> (*)');
                        $('#newregistro #tipodocumento').closest('div').show();
                        $('#newregistro #numdocumento').closest('div').show();
                         $('#label_telefono').text('<?php echo JrTexto::_('Cell phone number'); ?>');

                    }else if(nompais=='usa'){
                        $('#label_pais').text('País / Country (*)');
                        $('#label_primer_apellido').text('Apellido / Last Name (*)');
                        $('#label_nombres').text('Nombre / Name (*)');
                        //$('#label_tipodocumento').text(label_tipodocumento);
                        //$('#label_numdocumento').text(label_numdocumento);
                        //$('#label_apellido2').text(label_apellido2);
                        //$('#label_sexo').text(label_sexo);
                        $('#label_telefono').text('Teléfono / Phone number');
                        $('#label_correo').text('Email / E-mail (*) ');
                    }else if(nompais=='br'){
                        $('#label_pais').text('País / Country (*)');
                        $('#label_primer_apellido').text('Apellidos/ Sobrenomes (*)');
                        $('#label_nombres').text('Nombre / Nome (*)');
                        $('#label_telefono').text('Teléfono / Telefone');
                        $('#label_correo').text('Email / E-mail (*)');
                    }else if(nompais=='pe'){
                        $('#newregistro #tipodocumento').closest('div').show();
                        $('#newregistro #numdocumento').closest('div').show();
                        $('#newregistro #apellido2').val('').closest('div').show()
                        $('#newregistro #apellido2').removeAttr('required').removeClass('data-required');


                        //$('#label_pais').text('País / Country (*)');
                        //$('#label_primer_apellido').text('Apellidos/ Sobrenomes (*)');
                        //$('#label_nombres').text('Nombre / Nome (*)');
                        //$('#label_telefono').text('Teléfono / Telefone');
                        //$('#label_correo').text('Email / E-mail');

                   }
                    //    $('#newregistro #gradoinstruccion').closest('div').show();
                    //    $('#newregistro #edad').closest('div').show();
                    
                    //    $('#newregistro #sexo').closest('div').show();
                    //    $('#newregistro #fecha_nacimiento').closest('div').show();
                    //    $('#newregistro #correo').closest('div').removeClass('col-md-8').addClass('col-md-4');                        
                    //    $('#newregistro #telefono').attr('required');
                    //    $('#newregistro #gradoinstruccion').attr('required');
                    //    $('#newregistro #apellido2').val('-');
                    //}*/
                    Swal.close();
                    let cbo=pais.children('option[value="'+pais.val()+'"]');
                    let nmaxtel=parseInt(cbo.attr('telefono_fijo')||10);
                    let nmaxcel=parseInt(cbo.attr('cant_telmovil')||9);
                    let elmax=nmaxtel>nmaxcel?nmaxtel:nmaxcel;
                    let mask='';
                    for(i=1;i<=elmax;i++){
                        mask=mask+'0';
                    }

                    $('#newregistro #telefono').attr('placeholder',mask).attr('maxlength',cbo.attr('longitud'));//.inputmask({"mask": mask});
                    $('#paistelcode').text(cbo.attr('telefono-prefijo')||'');
                    $('#newregistro #tipodocumento').trigger('change');
                }else{
                    __notificar({
                        type:'danger',
                        title:'Error',
                        html:rs.msj                     
                    });
                }
              }
        });
    })

    $('#newregistro #tipodocumento').change(function(e){
        let iddocumento=$(this).val();
        let option=$(this).children('option[value="'+iddocumento+'"]');
        let mask=option.attr('mask')||'********************';
        if(mask=='' || mask=='null' || mask==null) mask='********************';
        $('#newregistro #numdocumento').attr('placeholder',mask).attr('maxlength',option.attr('longitud')).inputmask({"mask": mask});
        let numdoc=$('#newregistro #numdocumento').val()||'';
        if(numdoc!='') $('#newregistro #numdocumento').trigger('blur');
    })

    $('#newregistro #numdocumento').on('blur',function(e){
        let dni=$(this).val()||'';
        let tipodoc=$('#newregistro #tipodocumento').val();
         dni=dni.split('_').join('');
        if(dni==''|| dni==0 || dni == null){
            Swal.fire({title:'<?php echo JrTexto::_('Please, enter your ID number') ?>.', icon:'warning' });
            return;
        }
        Swal.showLoading({type:'info', html:'<?php echo JrTexto::_('Processing'); ?> ...'});
        var data1=new FormData();        
            data1.append('sqlsolopersona',true);
            data1.append('dni',dni);
            data1.append('tipo',tipodoc);
            //data1.append('idproyecto',idproyecto);
            data1.append('showmsjok',true);
            __sysAyax({ 
                fromdata:data1,
                url:_sysUrlsks_+'persona/infodniexistentes',                  
                callback:function(rs){                                       
                    Swal.close();
                    let frm=$('#formnewregistro');
                    let nohaypersona=false;
                    if(rs.code==200){
                        let persona=rs.data||'';                      
                        if(rs.data!=undefined && rs.data!=''){
                            persona.infopersona;
                            let per1=persona.infopersona||'';
                            if(per1!='' && per1!=undefined){
                                frm.find('input#nombres').attr('readonly',true).val(per1.nombre);
                                frm.find('input#apellido1').attr('readonly',true).val(per1.primer_ape);
                                frm.find('input#apellido2').attr('readonly',true).val(per1.segundo_ape);
                                frm.find('input#correo').attr('readonly',true).val(per1.email);
                                frm.find('input#gradoinstruccion').attr('readonly',true).val(per1.instruccion);
                                frm.find('select#sexo').attr('readonly',true).val(per1.sexo);
                                frm.find('input#fecha_nacimiento').attr('readonly',true).val(per1.fechanac);
                                frm.find('input#telefono').attr('readonly',true).val(per1.telefono);
                                frm.find('input#idpersona').attr('readonly',true).val(per1.idpersona);
                                frm.find('input#edad').attr('readonly',true).val(calcularAnios2(per1.fechanac));
                            }else nohaypersona=true;
                        }else nohaypersona=true;                      
                    }else nohaypersona=true;

                    if(nohaypersona==true){
                        frm.find('input#nombres').removeAttr('readonly').val('');
                        frm.find('input#apellido1').removeAttr('readonly').val('');
                        frm.find('input#apellido2').removeAttr('readonly').val('');
                        frm.find('input#correo').removeAttr('readonly').val('');
                        frm.find('input#gradoinstruccion').removeAttr('readonly').val('');
                        frm.find('select#sexo').removeAttr('readonly').val('M');
                        frm.find('input#fecha_nacimiento').removeAttr('readonly').val('');
                        frm.find('input#telefono').removeAttr('readonly').val('');
                        frm.find('input#idpersona').val('');
                    }
                }
            });
    })

    window.onhashchange = function(ev){
        if(window.location.hash=='#tonewregistro'){
            cargarpaises();
        }
    }

    if(window.location.hash=='#tonewregistro'){
        cargarpaises();
    }

    $.extend(jsonlogin,jsonproyecto);  
    $('.contentmedia').css('background',jsonlogin.colorfondo);
    htmlfondo='';
    if(jsonlogin.tipofondo=='imagen'){
        htmlfondo='<img src="'+url_media+jsonlogin.imagenfondo+'">'
    }else if(jsonlogin.tipofondo=='video'){
        htmlfondo='<video autoplay="true" loop="" muted="" src="'+url_media+jsonlogin.videofondo+'"></video>'
    }
    
    $('.contentmedia').html(htmlfondo);
    $('#formRecupClave').bind({
         submit: function(){
            var btn=$(this).find('#btn-enviar-formInforecuperarclave');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            var fele = document.getElementById("formRecupClave");
            var data=new FormData(fele);          
              __sysAyax({ 
                fromdata:data,
                  url:_sysUrlBase_+'sesion/cambiarclave',                  
                  callback:function(rs){
                    btn.removeClass('disabled').removeAttr('disabled','disabled');
                    if(rs.code==200){
                       __notificar({
                            type:'success',
                            //title:'Revise su correo',
                            html:rs.msj                     
                        });
                    }else{
                        __notificar({
                            type:'danger',
                            title:'Error',
                            html:rs.msj                     
                        });
                    }
                  }
            });             
         }
    });

    $('#frmlogin').bind({
         submit: function(ev){
            ev.preventDefault();
            procesandoguardando=true;
            var btn=$(this).find('#btn-enviar-formInforecuperarclave');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            
            var data=new FormData();
            var frm=$('#frmlogin');
            var clave=frm.find('#clave').val();
            var useringresado=frm.find('#usuario').val();
                data.append('usuario',useringresado);
                data.append('clave',clave);
                data.append('idempresa',frm.find('#idempresa').val());
                data.append('idproyecto',frm.find('#idproyecto').val());
                data.append('showmsjok',true);
                __sysAyax({ 
                    fromdata:data,
                    url:_sysUrlBase_+'json/sesion/login',                  
                    callback:function(rs){                                       
                        if(rs.code==200){
                            var datosalumno=rs.data;
                            localStorage.setItem("usersesion", JSON.stringify(rs.data));
                            localStorage.setItem("userempresa", JSON.stringify(<?php echo json_encode($this->proyecto); ?>));
                            localStorage.setItem("userproyecto", JSON.stringify(<?php echo json_encode($this->proyecto['jsonlogin'])?>));
                            __cambiarinfouser();

                            let codigo=frm.find('input#codigosuscripcion').val()||'';
                            let idgrupoaula=frm.find('input#idgrupoaula').val()||'';
                            if(codigo!='' && idgrupoaula!=''){
                                var data=new FormData();
                                    data.append('idgrupoaula',$('#frmlogin').find('#idgrupoaula').val());
                                    data.append('keycode',codigo);
                                __sysAyax({ 
                                    fromdata:data,
                                    url:_sysUrlBase_+'json/api/marticulasxgrupo',
                                    callback:function(rs){
                                        if(rs.code==200){
                                            activarsuscripcion({
                                                usuario:datosalumno.usuario,
                                                dni:datosalumno.dni,
                                                tipodocumento:datosalumno.tipodoc,
                                                codigosuscripcion:codigo,
                                                idpersona:datosalumno.idpersona,
                                            });

                                            let matricula_faltantes=[];
                                            if(rs.datos.matricula_faltantes!=undefined && rs.datos.matricula_faltantes!='')
                                                forEach(rs.datos.matricula_faltantes,function(v,i){
                                                    matricula_faltantes.push(v);
                                                })

                                            let matricula_sobrantes=[];
                                            if(rs.datos.matricula_sobrantes!=undefined && rs.datos.matricula_sobrantes!='')
                                                forEach(rs.datos.matricula_sobrantes,function(v,i){
                                                    matricula_sobrantes.push(v);
                                                })


                                            let cursosdelgrupo=[];
                                            if(rs.datos.cursosdelgrupo!=undefined && rs.datos.cursosdelgrupo!='')
                                                forEach(rs.datos.cursosdelgrupo,function(v,i){
                                                    cursosdelgrupo.push(v);
                                                })

                                                
                                            let mismatriculas=[];
                                            if(rs.datos.mismatriculas!=undefined && rs.datos.mismatriculas!='')
                                                forEach(rs.datos.mismatriculas,function(v,i){
                                                    mismatriculas.push(v.idmatricula);
                                                })
                                            
                                            var data1=new FormData();
                                            if(mismatriculas.length>0) data1.append('idmatricula',JSON.stringify(mismatriculas));
                                            if(matricula_faltantes.length>0) data1.append('matricula_faltantes',JSON.stringify(matricula_faltantes));
                                            if(matricula_sobrantes.length>0) data1.append('matricula_sobrantes',JSON.stringify(matricula_sobrantes));
                                            if(cursosdelgrupo.length>0) data1.append('cursosdelgrupo',JSON.stringify(cursosdelgrupo));

                                            data1.append('target',_sysUrlBase_+'json/');
                                            data1.append('idproyecto',idproyecto);
                                            data1.append('idempresa',idempresa);            
                                            data1.append('keycode',$('#frmlogin').find('#codigosuscripcion').val());
                                            let urlsksextender=_sysUrlsks_+'matriculas/extendersuscripcion';
                                            if(mismatriculas.length==0){
                                                let alumnos=[{                                
                                                    "tipodoc": datosalumno.tipodoc, //este valor debe coincidir con SKS                                                    
                                                    //"tipodoc_str": tipodoc_str, //este nombre debe coincidir con SKS
                                                    "nombres": datosalumno.nombre, //falta
                                                    "apellido1": datosalumno.apellido1, //falta
                                                    "apellido2": datosalumno.apellido2, //falta
                                                    "documento": datosalumno.dni,
                                                    "sexo" : datosalumno.sexo,
                                                    "fechanac": datosalumno.fechanac, //"2020-03-03",
                                                    "instruccion": '', //opcional
                                                    "email": datosalumno.email,
                                                    //"edad" : datosalumno, //opcional en el formulario, pero que sea un valor > 0
                                                    "tipogrupo": 1, //tipo de grupo que lo puede obtener en SC con el idgrupoaula. Si quiere lo deja en constante 1
                                                    "codigoCursoSence": 0, //esto es para sence, siempre en 0 si no es para sence
                                                    "usuario": useringresado, //INicial del nombre Inicial del primer apellido
                                                    "clave": clave,
                                                    //"telefono":telefono,
                                                }];
                                                data1.append('alumnos',JSON.stringify(alumnos));
                                                data1.append('tipo_matricula',2);
                                                urlsksextender=_sysUrlsks_+'matriculas/_matricular';
                                            }else{
                                                data1.append('idpersona',datosalumno.idpersona);
                                                data1.append('tipodoc',datosalumno.tipodoc);
                                            }

                                            __sysAyax({ 
                                                fromdata:data1,
                                                url:urlsksextender,    
                                                callback:function(rs){                                                     
                                                    if(rs.code==200){                                                        
                                                        __cambiarinfouser();                                       
                                                        redir(_sysUrlBase_);
                                                    }else if(rs.code==404){
                                                        Swal.fire({title:`<?php echo JrTexto::_("The code isn't valid or has already been used"); ?>`, icon:'error' }).then(rs=>{
                                                            $('#formcodigosuscripcion').find('#codigo').val('');
                                                            window.location.hash ='#tovalidarcodigo';
                                                        });
                                                    }else{
                                                        Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later'); ?>.`, icon:'error' });
                                                    }                                           
                                                }
                                            });
                                      }                    
                                    }
                                }); 

                            }else redir(_sysUrlBase_);
                        }                        
                    }
                });            
        }
    });

    $('#formcodigosuscripcion').bind({
        submit:function(ev){
            ev.preventDefault();
            var btn=$(this).find('#btn-enviar-newregistro');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            var frm=$('#formcodigosuscripcion');
            let codigo=frm.find('#codigo').val();
            var data=new FormData();
            data.append('codigo',codigo);
            data.append('estado',2); //descomentar para validar solño tipo2
            data.append('idempresa',idempresa);
            data.append('idproyecto',idproyecto);
            //b54aad92-cd8c-46f0-b02f-4af7b48574b1
            data.append('showmsjok',true);
              __sysAyax({ 
                fromdata:data,
                  url:_sysUrlsks_+'codigos/buscarcompleto',                  
                  callback:function(rs){                  
                    if(rs.code==200){
                        //console.log(rs.data);
                       // var data=Object.values(rs.data[0]);
                        if(rs.data[0]==null || rs.data==undefined){
                            Swal.fire({
                                title:'<?php echo JrTexto::_("The entered code is not correct"); ?>.',
                                icon:'error'
                            })
                            return ;
                        }
                        if(rs.data[0].codigo!==codigo){
                            Swal.fire({
                                title:'<?php echo JrTexto::_("The entered code is not correct"); ?>.',
                                icon:'error'
                            })
                            return ;
                        }
                        idrepresentante=rs.data[0].idrepresentante||null;
                        cantidad_descarga=rs.data[0].cantidad_descarga||1;
                        Swal.fire({
                          icon:'success',
                          title: '<?php echo JrTexto::_("The entered code is correct, would you like to continue?"); ?>',
                          showDenyButton: true,
                          showCancelButton: true,
                          confirmButtonText: `<?php echo JrTexto::_("Yes, I'd like to log in"); ?>`,
                          denyButtonText: `<?php echo JrTexto::_("Yes, I'd like to register");?>`,
                          cancelButtonText:'<?php echo JrTexto::_("No, cancel it") ?>.',
                        }).then((result) => {
                            //console.log(result);
                            //let rs={};
                            //jsonvalidarcodigo=rs||{};
                            /* Read more about isConfirmed, isDenied below */     
                            if(result.isConfirmed){                        
                                $('#frmlogin').find('#codigosuscripcion').val(codigo);
                                $('#frmlogin').find('#idgrupoaula').val(rs.data[0].idgrupoaula);
                                window.location.hash ='#tologin';
                                //Swal.close();
                            }else if(result.isDenied){
                                cargarpaises();
                                window.location.hash ='#tonewregistro';
                                //Swal.fire('Changes are not saved', '', 'info')
                            }
                        })
                    }else{
                        Swal.fire({
                                title:'<?php echo JrTexto::_("The entered code is not correct"); ?>',
                                icon:'error'
                        })
                    }
                }
            });

        }
    })


    $('#formnewregistro').bind({
        submit:function(ev){
            ev.preventDefault();
            var btn=$(this).find('#btn-enviar-formcodigosuscripcion');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            var frm=$('#formnewregistro');
            let tipodoc=frm.find('select#tipodocumento').val()||'';
            let tipodoc_str=frm.find('select#tipodocumento').children('option[value="'+tipodoc+'"]').text();
            let numdocumento=frm.find('input#numdocumento').val()||'';
            numdocumento=numdocumento.split('_').join('');

            let txttelefono=frm.find('#paistelcode').text();            
            let telefono=frm.find('input#telefono').val()||'';           
            let nombre=frm.find('input#nombres').val()||'';
            let apellido1=frm.find('input#apellido1').val()||'';
            let apellido2=frm.find('input#apellido2').val()||'';

            let user=nombre.substring(0,1).toLowerCase()+apellido1.substring(0,1).toLowerCase()+numdocumento;
            var tmpidpersona=frm.find('input#idpersona').val()||'';

            let pais=frm.find('#idpais');
            let idpais=pais.val()||'';
            let nompais=(pais.children('option[value="'+idpais+'"]').attr('abrev')||'').toLowerCase();
            let fecha_nacimiento=frm.find('input#fecha_nacimiento').val()||'<?php echo date('Y-m-d'); ?>';
            let edad=frm.find('input#edad').val()||1;

            //console.log(nompais);                   
            let revalidar=true;
            if(edad=='') edad=1;
            if(telefono=='') telefono='0000000';
            if(nompais=='usa' || nompais=='br'){
                revalidar=false;
                numdocumento=nompais.toString().toUpperCase()+Date.now();
                tipodoc=14;
                tipodoc_str='Unico';
                fecha_nacimiento='<?php echo date('Y-m-d'); ?>';               
            }else if(nompais=='mex' || nompais=='pe'){
                revalidar=false;
                if(numdocumento=='') numdocumento=nompais.toString().toUpperCase()+Date.now();
                if(apellido2=='') apellido2='-';
            }
            
            
            if(telefono!='') telefono=txttelefono+telefono;
            else telefono=txttelefono+'0000000';

            if(numdocumento=='' || numdocumento == null){
                if(frm.find('input#numdocumento').parent().is(':visible')==true){
                    Swal.fire({title:'<?php echo JrTexto::_('The document number is not valid'); ?>.', icon:'error' }).then(r1=>{
                        frm.find('input#numdocumento').focus();
                    });                
                    return ;
                }else{
                    numdocumento=nompais.toString().toUpperCase()+Date.now();
                }
            }

            var codigo=$('#formcodigosuscripcion').find('#codigo').val();            
            if(codigo==''){
                Swal.fire({title:`<?php echo JrTexto::_('The code is not valid'); ?>.`, icon:'error' }).then(r1=>{
                     window.location.hash ='#tovalidarcodigo';
                });
                return ;
            }

            if(nombre==''){
                Swal.fire({title:`<?php echo JrTexto::_('The name is not valid'); ?>.`, icon:'error' }).then(r1=>{
                    frm.find('input#nombres').focus();
                });
                return ;
            }

            if(apellido1==''){
                Swal.fire({title:`<?php echo JrTexto::_("The first surname isn't valid"); ?>.`, icon:'error' }).then(r1=>{
                    frm.find('input#apellido1').focus();
                });
                return ;
            }
            
            if(apellido2 && revalidar==true){
                if(frm.find('input#apellido2').parent().is(':visible')==true){
                    Swal.fire({title:`<?php echo JrTexto::_("The second surname isn't valid"); ?>.`, icon:'error' }).then(r1=>{
                        frm.find('input#apellido2').focus();
                    });
                    return ;
                }else{
                    apellido2='';
                }
            }
            if(frm.find('input#correo').val()==''){
                Swal.fire({title:`<?php echo JrTexto::_("The email address isn't valid"); ?>.`, icon:'error' }).then(r1=>{
                    frm.find('input#correo').focus()    
                });                
                return ;
            }

            let alumnos=[{
                "idpais":  frm.find('select#idpais').val()||'',              
                "tipodoc": tipodoc, //este valor debe coincidir con SKS
                "tipodoc_str": tipodoc_str, //este nombre debe coincidir con SKS
                "nombres": nombre,
                "apellido1": frm.find('input#apellido1').val()||'',
                "apellido2": apellido2||'-',
                "documento": numdocumento,
                "sexo" : frm.find('select#sexo').val()||'',
                "fechanac": fecha_nacimiento, //"2020-03-03",
                "instruccion": frm.find('input#gradoinstruccion').val()||'', //opcional
                "email": frm.find('input#correo').val()||'',
                "edad" : edad, //opcional en el formulario, pero que sea un valor > 0
                "tipogrupo": 1, //tipo de grupo que lo puede obtener en SC con el idgrupoaula. Si quiere lo deja en constante 1
                "codigoCursoSence": 0, //esto es para sence, siempre en 0 si no es para sence
                "usuario": user, //INicial del nombre Inicial del primer apellido
                "clave": user,
                "telefono":telefono,
            }];
            var data=new FormData();
            var keycode=codigo;
            data.append('alumnos',JSON.stringify(alumnos));
            data.append('target',_sysUrlBase_+'json/');
            data.append('idproyecto',idproyecto);
            data.append('idempresa',idempresa);
            data.append('tipo_matricula',2);
            data.append('keycode',keycode);
            data.append('enviaremail',true);
            data.append('idpais',idpais);
            __sysAyax({ 
                fromdata:data,
                url:_sysUrlsks_+'matriculas/_matricular',
                callback:function(rs){
                    if(rs.code==200){
                        if(rs.correos==undefined || rs.correos==''){
                            return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no mailing list found'); ?>.`, icon:'error'}); 
                        }
                        let cor=rs.correos;
                        if(cor.estudiantes==undefined){
                            return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no student found'); ?>.`, icon:'error'});    
                        }
                        let alumno=cor.estudiantes[0];
                        if(tmpidpersona!=''){
                            Swal.fire({title:'<?php echo JrTexto::_("Your code has been redeemed. Log in using your username and password");?>.', icon:'success'});
                            $('#frmlogin').find('#codigosuscripcion').val('');
                            $('#frmlogin').find('#idgrupoaula').val('');

                            activarsuscripcion({
                                usuario:rs.usuarios[0].usuario,
                                dni:numdocumento,
                                tipodocumento:tipodoc,
                                codigosuscripcion:keycode,
                                //idpersona:datosalumno.idpersona,
                            });
                            window.location.hash ='#tologin';
                            return;       
                        }
                       
                        if(alumno==undefined   || alumno=='' || alumno=={} ){
                            let data=rs.data[0]||'';
                            if(data=='' || data==undefined)
                                return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no student found'); ?>.`, icon:'error'});    
                            let per=data.personal[0]||''
                            if(per=='' || per==undefined)
                                return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no student found'); ?>.`, icon:'error'});    
                            let per1=per.personal||''
                            if(per1=='' || per1==undefined)
                                return Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later, no student found'); ?>.`, icon:'error'}); 
                            alumno={
                                usuario:per1.usuario,
                                clave:per1.clavesinmd5
                            }
                        }

                        // su codigo ha sido cangeado, inicie sesion con sus datos de usuario y clave.
                        var data1=new FormData();
                        var frm=$('#frmlogin');
                            data1.append('usuario',alumno.usuario);
                            data1.append('clave',alumno.clave);
                            data1.append('idempresa',idempresa);
                            data1.append('idproyecto',idproyecto);
                            data1.append('showmsjok',true);                           
                            __sysAyax({ 
                                fromdata:data1,
                                url:_sysUrlBase_+'json/sesion/login',                  
                                callback:function(rs){  
                                    $('#frmlogin').find('#codigosuscripcion').val('');
                                    $('#frmlogin').find('#idgrupoaula').val(''); 
                                    var datosalumno=rs.data;
                                    if(rs.code==200){                                     
                                        localStorage.setItem("usersesion", JSON.stringify(rs.data));
                                        localStorage.setItem("userempresa", JSON.stringify(<?php echo json_encode($this->proyecto); ?>));
                                        localStorage.setItem("userproyecto", JSON.stringify(<?php echo $this->proyecto['jsonlogin']; ?>));
                                        __cambiarinfouser();   
                                        activarsuscripcion({
                                            usuario:datosalumno.usuario,
                                            dni:datosalumno.dni,
                                            tipodocumento:datosalumno.tipodoc,
                                            codigosuscripcion:keycode,
                                            idpersona:datosalumno.idpersona,
                                        });
                                        redir(_sysUrlBase_);
                                    }
                                    procesandoguardando=false; 
                                }
                            });
                    }else if(rs.code==404){
                        Swal.fire({title:`<?php echo JrTexto::_("The code isn't valid or has already been used") ?>.`, icon:'error' }).then(rs=>{
                            window.location.hash ='#tovalidarcodigo';
                        });
                    }else{
                        Swal.fire({title:`<?php echo JrTexto::_('Unexpected error. Try again later'); ?>.`, icon:'error' });
                    }                    
                }
            });
        }
    })

    $('#formnewregistro').on('change','input#fecha_nacimiento',function(ev){
        let edad=calcularAnios2($(this).val());
        $('#formnewregistro').find('input#edad').val(edad);        
    })

    function calcularAnios2(fechaNacimiento){
        let fechaNac=new Date(fechaNacimiento);
        var today = new Date();    
        let anios = today.getFullYear() - fechaNac.getFullYear();        
        if (fechaNac.getMonth() > (today.getMonth()) || fechaNac.getDay() > today.getDay())
            anios--;
        return anios;
    }
});
</script>