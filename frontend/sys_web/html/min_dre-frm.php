<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/min_dre">&nbsp;<?php echo JrTexto::_('Min_dre'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="iddre" id="iddre" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Descripcion');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtDescripcion" name="descripcion" required="required" class="form-control" value="<?php echo @$frm["descripcion"];?>">
                                  
              </div>
            </div>
            <div class="form-group">
               <label class="control-label" for="txtUbigeo">
              <?php echo JrTexto::_('Ubigeo');?> <span class="required"> * </span></label>
              <div class="cajaselect">
              <select name="ubigeo" id="txtUbigeo" class="form-control" datavalue="<?php echo !empty($frm["ubigeo"])?$frm["ubigeo"]:"010000" ?>"></select>
            </div>            
            </div>
            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Opcional');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtOpcional" name="opcional"  class="form-control" value="<?php echo @$frm["opcional"];?>">
                                  
              </div>
            </div>
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveMin_dre" type="submit" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('min_dre'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/min_dre.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui; ?>');
    if(!frm.hasClass('ismodaljs')) min_dre($('#vent-<?php echo $idgui; ?>'));
  })
</script>