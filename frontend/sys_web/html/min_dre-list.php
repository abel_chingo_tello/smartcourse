<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Dre"); ?>'
    });
    /*addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });*/
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;} 
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">
          <!--div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <div class="cajaselect">
              <select name="idproyecto" id="idproyecto" class="form-control">                     
                <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                  <option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> >
                    <?php echo $fksexo["nombre"] ?>
                    </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div-->                                                                                             
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php //echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 text-center">             
               <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/min_dre/agregar" data-titulo="<?php echo ucfirst(JrTexto::_('Add'))." ".JrTexto::_('Dre') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
               <a class="btn btn-warning btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/min_dre/importar" data-titulo="<?php echo ucfirst(JrTexto::_('Import'))." ".JrTexto::_('Dre') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Import')?></a>
              </div>
            </div>
          </div>
      </div>
	   <div class="row">         
         <div class="col table-responsive ">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Descripcion") ;?></th>
                    <!--th><?php //echo JrTexto::_("Ubigeo") ;?></th>
                    <th><?php //echo JrTexto::_("Opcional") ;?></th>
                    <th><?php //echo JrTexto::_("Idproyecto") ;?></th-->
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5d8e3ca89a04d='';
function refreshdatos5d8e3ca89a04d(){
    tabledatos5d8e3ca89a04d.ajax.reload();
}
$(document).ready(function(){  
  var estados5d8e3ca89a04d={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5d8e3ca89a04d='<?php echo ucfirst(JrTexto::_("Dre"))." - ".JrTexto::_("edit"); ?>';
  var draw5d8e3ca89a04d=0;

  var _imgdefecto='';
  $('.textosearchlist').on('keydown',function(ev){
    if(ev.keyCode===13) refreshdatos5d8e3ca89a04d();
  }).blur(function(ev){ refreshdatos5d8e3ca89a04d()})

  
  $('.btnbuscar').click(function(ev){
    refreshdatos5d8e3ca89a04d();
  });
  tabledatos5d8e3ca89a04d=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Descripcion") ;?>'},
        //{'data': '<?php //echo JrTexto::_("Ubigeo") ;?>'},
        //{'data': '<?php //echo JrTexto::_("Opcional") ;?>'},
        //{'data': '<?php //echo JrTexto::_("Idproyecto") ;?>'},            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'json/min_dre',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.texto=$('#texto').val(),                        
            draw5d8e3ca89a04d=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5d8e3ca89a04d;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                                
            datainfo.push({             
              '#':(i+1),
              '<?php echo JrTexto::_("Descripcion") ;?>': data[i].descripcion,
              //'<?php //echo JrTexto::_("Ubigeo") ;?>': data[i].ubigeo,
              //'<?php //echo JrTexto::_("Opcional") ;?>': data[i].opcional,
              //'<?php //echo JrTexto::_("Idproyecto") ;?>': data[i].idproyecto,
              //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/min_dre/editar/?id='+data[i].iddre+'" data-titulo="'+tituloedit5d8e3ca89a04d+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].iddre+'" ><i class="fa fa-trash"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('iddre',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/min_dre/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5d8e3ca89a04d.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/min_dre/setcampo','masvalores':{iddre:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('iddre',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/min_dre/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5d8e3ca89a04d.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var titulo=$(this).attr('data-titulo')||'';
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      refreshdatos5d8e3ca89a04d();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){min_dre(_md);});
       }else{
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              min_dreimport(_md);
            })
          });
       }           
    })   
  }) 
});
</script>