<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_('Back')?></a></li> 
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li-->        
        <li class="active">&nbsp;<?php echo JrTexto::_("Personal"); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
  .pd2{margin-bottom: 1ex;}
</style>
<div id="vent-<?php echo $idgui; ?>">
<div class="form-view formventana" id="ventana_<?php echo $idgui; ?>" accion="listado" >
  <input type="hidden" id="idrol" name="idrol" value="<?php echo $this->idrol; ?>">
  <input type="hidden" id="idproyecto" name="idproyecto" value="<?php echo $this->idproyecto; ?>">
  <div class="row">
    <div class="col">    
      <div class="row"> 
        <div class="col-xs-12 col-sm-6 col-md-4 pd2">                     
          <div class="cajaselect">
            <select id="sexo" name="sexo" class="form-control" >
              <option value=""><?php echo JrTexto::_('All Gender'); ?></option>
                <?php 
                if(!empty($this->fksexo))
                foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
                 <?php } ?>
            </select>
          </div>
        </div>                               
        <div class="col-xs-12 col-sm-6 col-md-4 pd2" >
          <div class="cajaselect">
            <select id="estado_civil" name="estado_civil" class="form-control" >
              <option value=""><?php echo JrTexto::_('All marital status'); ?></option>
                <?php 
                if(!empty($this->fkestado_civil))
                foreach ($this->fkestado_civil as $fkestado_civil) { ?><option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> ><?php echo $fkestado_civil["nombre"] ?></option><?php } ?>
            </select>
          </div>
        </div>                              
        <div class="col-xs-12 col-sm-6 col-md-4 pd2" >
          <div class="cajaselect">
            <select name="estado" id="estado" class="form-control">
                <option value=""><?php echo JrTexto::_('All status'); ?></option>
                <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
            </select>
          </div>
        </div>         
                                                        
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <!--label><?php //echo  ucfirst(JrTexto::_("text to search"))?></label-->
            <div class="input-group">
              <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
              <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 text-center">             
           <a class="btn btn-success btnagregarpersona" href="<?php echo $this->documento->getUrlSitio();?>/personal/adicionar" data-titulo="<?php echo ucfirst(JrTexto::_('Add')) ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">         
    <div class="col table-responsive">
          <table class="table table-striped table-hover">
            <thead>
              <tr class="headings">
                <th>#</th>                  
                  <th id="thname"><?php echo JrTexto::_("Name") ;?></th>                    
                  <th id="thsexo"><?php echo JrTexto::_("Gender") ;?></th>                   
                  <th id="thtelefono"><?php echo JrTexto::_("Telephone") ;?></th>
                  <!--th><?php //echo JrTexto::_("Cellphone") ;?></th-->
                  <th id="themail"><?php echo JrTexto::_("E-mail") ;?></th>                   
                  <th id="thuser"><?php echo JrTexto::_("User") ;?></th>                   
                  <th id="thfoto"><?php echo JrTexto::_("Photo") ;?></th>
                  <th id="thestado"><?php echo JrTexto::_("Status") ;?></th>
                  <!--th><?php //echo JrTexto::_("Situacion") ;?></th-->
                  <th id="thacciones" class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
  </div>  
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/persona_buscar.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#ventana_<?php echo $idgui;?>');
        frm.addClass('ismodaljs');
    if(!frm.hasClass('ismodaljs')) frmpersonabuscar($('#vent-<?php echo $idgui;?>'));
  })
</script>