<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  #listadoinvitados, #listadoainvitar{
    background: white; padding: 10px; border: 1px solid gray; border-radius: 0.4em;
  }
</style>
<?php if(!$ismodal){?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("activate account"); ?>',
      link: _sysUrlBase_+'videosconferencia'
    });  
  </script>
<?php } ?>
<div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron">
                <h1><strong>Opss!</strong></h1>                
                <div class="disculpa"><h3><?php echo JrTexto::_('a thousand apologies, you must validate your zoom account from your email')?>.</h3><br>
                <h2><strong><?php echo JrTexto::_('email')?></strong><br><?php echo $this->datos["email"]; ?><br><br>
                <strong><?php echo JrTexto::_('password')?></strong><br>2020userid<?php echo $this->Curusuario["idpersona"]; ?>@</h2>
                <div class="disculpa"><h3><?php echo JrTexto::_('if you already have a zoom account')?> : <br> <?php echo JrTexto::_('you only have to activate the integration with the platform, in the email sent by zoom.')?></h3><br>
                </div>
                <div class=""><br>
                    <a class="btn btn-default close cerrarmodal" href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('go back')?></a>
                </div>
            </div>
        </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  

    let htmlcorreo='<?php echo JrTexto::_('user data')?><br>';
    htmlcorreo+='<b><?php echo JrTexto::_('email')?><b>: <?php echo $this->datos["email"]; ?><br>';
    htmlcorreo+='<b><?php echo JrTexto::_('password')?></b>: <?php echo $this->datos["clave"]; ?><br>';
    htmlcorreo+='<b><?php echo JrTexto::_('zoom video conference access data')?> <b><br>  <?php echo JrTexto::_('if you already have an account: you only have to activate the integration in email sent by zoom')?> ';
    //htmlcorreo+='<a href="'+_sysUrlBase_+'">'+_sysUrlBase_+'</a>';

   /* var fd2 = new FormData();                       
    fd2.append('mensaje', htmlcorreo);
    fd2.append('paratodos', false);
    fd2.append('asunto', 'Cuenta de Acceso a plataforma');
    fd2.append('paraemail', JSON.stringify([{nombre:'',email:'<?php echo $this->datos["email"];?>'}]));
    $.ajax({
      url: _sysUrlBase_+'/sendemail/enviarcorreoall',
      type: "POST",
      data:  fd2,
      contentType: false,
      processData: false,
      dataType:'json',
      cache: false, 
      success: function(data){ 
        mostrar_notificacion('<?php //echo JrTexto::_('Attention');?>',data.msj,'success');
      },
       error: function(e){ console.log(e); }
    });    */  
  })

</script>
