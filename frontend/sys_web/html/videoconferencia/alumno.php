<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<div class="paris-navbar-container"></div>

<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("video conference"); ?>'
  });
</script>
<div class="container p-4">
  <div class="row" id="ventana_<?php echo $idgui; ?>">
    <div class="col-md-12">    
       <div class="row">
       	<div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
              <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
              <div class="input-group" style="margin-top: 1px">
                <input type="date" name="busfecha" id="busfecha" class="form-control border0" value="<?php echo date("Y-m-d") ?>" >
                <span class="input-group-addon btn btnbuscar"> <i class="fa fa-calendar"></i></span>  
              </div>
            </div>
          </div>                                                                                                             
          <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
              <div class="input-group" style="margin-top: 1px">
                <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
  			</div>
            </div>
          </div>
          </div>
        </div>
         
      <div class="col-md-12 ">
      <div class="table-responsive">
          <table class="table table-striped " style="width: 100%;">
            <thead>
              <tr class="bg-info">
                <th>#</th>
                  <th><?php echo JrTexto::_("date") ;?></th>
                  <th><?php echo JrTexto::_("title") ;?></th>
                  <th><?php echo JrTexto::_("url participants") ;?></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
      	</table>
      </div>
  </div>
  </div>

</div>
<script type="text/javascript">
const IDALUMNO = <?php echo !empty($this->usuario['idpersona']) ? $this->usuario['idpersona'] : 0 ?>;
console.log(IDALUMNO);
var tabledatos5d7aaa0998bb8='';
function refreshdatos5d7aaa0998bb8(){
    tabledatos5d7aaa0998bb8.ajax.reload();
}
$(document).ready(function(){  
  var estados5d7aaa0998bb8={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("cancelled") ?>'}
  var tituloedit5d7aaa0998bb8='<?php echo ucfirst(JrTexto::_("zoom"))." - ".JrTexto::_("edit"); ?>';
  var draw5d7aaa0998bb8=0;
  var _imgdefecto='';


  $('.textosearchlist').on('keydown',function(ev){  if(ev.keyCode===13) refreshdatos5d7aaa0998bb8();
  }).blur(function(ev){ refreshdatos5d7aaa0998bb8()})
  $('#busfecha').on('change',function(ev){refreshdatos5d7aaa0998bb8();})

  
  $('.btnbuscar').click(function(ev){refreshdatos5d7aaa0998bb8(); });

  tabledatos5d7aaa0998bb8=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("date") ;?>'},
        {'data': '<?php echo JrTexto::_("title") ;?>'},   
        {'data': '<?php echo JrTexto::_("url participants") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'json/videosconferencia/listado',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.sql=2,
            d.idalumno=IDALUMNO,
            d.texto=$('#texto').val(),
            d.fecha=$('#busfecha').val(),            
            draw5d7aaa0998bb8=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5d7aaa0998bb8;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
          var fecha='<?php echo date('Y-m-d'); ?>';
          var fechareg=data[i].fecha.substring(0,10);
          var urlmod='-';
          var urlinvi='-';
          console.log(fecha,fechareg);
          if(fecha==fechareg){
          var urlmod='<a target="_blank"  href="'+data[i].urlmoderador+'"><?php echo JrTexto::_("start") ;?></a>';
          var urlinvi= '<a target="_blank"  href="'+data[i].urlparticipantes+'"><?php echo JrTexto::_("start") ;?></a>';                               
          }
            datainfo.push({             
              '#':(i+1),
              '<?php echo JrTexto::_("date") ;?>': data[i].fecha,
              '<?php echo JrTexto::_("tittle") ;?>': data[i].titulo,
              '<?php echo JrTexto::_("url participants") ;?>': urlinvi
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/ES.json":''}
    });
});
</script>