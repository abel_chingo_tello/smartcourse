<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  #listadoinvitados, #listadoainvitar{
    background: white; padding: 10px; border: 1px solid gray; border-radius: 0.4em;
  }
</style>
<?php if(!$ismodal){?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("video conference"); ?>',
      link: _sysUrlBase_+'videosconferencia'
    });
    addItemNavBar({
      text: '<?php echo JrTexto::_("invite"); ?>'
    });
  </script>
<?php } ?>
<div class="container p-4">
  <div class="row"  id="vent-<?php echo $idgui;?>"  >
    <div class="col-md-12">
          <div id="msj-interno"></div>
          <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class=" form-label-left" >
            <input type="hidden" name="idzoom" id="idzoom" value="<?php echo $frm["idzoom"];?>">
             <div class="row">
              <div class="col-md-12 col-xs-12 text-center">
                <h2><?php echo @$frm["titulo"];?></h2>
              </div>
              <div class="col-md-12 col-xs-12 text-center">
                <strong><?php echo JrTexto::_('date');?>: </strong> <?php echo !empty($frm["fecha"])?substr($frm["fecha"],0,10):date('Y-m-d')." ".!empty($frm["fecha"])?substr($frm["fecha"],11):date('H:i:00');?> <br><strong><?php echo JrTexto::_('duration in minutes');?>: </strong> <?php echo !empty($frm["duracion"])?$frm["duracion"]:40;?>              
              </div>
              <div class="col-md-12 col-xs-12">
                <strong><?php echo JrTexto::_('scheducle');?>:</strong><br>
                <?php echo @$frm["agenda"];?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12"><hr>
                <?php 
                if(!empty($this->miscursos)){ ?>
                  <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('my groups');?></label>
                  <div class="cajaselect">                  
                  <select name="grupos" id="grupos" class="form-control">
                    <option value="0"><?php echo JrTexto::_("selected group");?></option>
                  <?php foreach ($this->miscursos as $cur){?>
                    <option value="<?php echo $cur["idgrupoauladetalle"]?>" ><?php echo $cur["strgrupoaula"]." - ".$cur["strcurso"]." : ".$cur["strlocal"]." - ".$cur["strambiente"]." - ".$cur["idgrado"]; ?></option>
                  <?php }?>
                  </select>
                  </div>
                 </div>
                <?php } ?>
              </div>
              <div class="col-md-12">
                <fieldset>
                  <legend><?php echo JrTexto::_("members invited to the Video Conference");?></legend>
                  
                  <div class="row" id="listadoinvitados">
                    <?php
                    if(!empty($this->invitados))
                      foreach ($this->invitados as $k => $v){?>
                        <a href="javascript:void(0)" data-email="<?php echo @$v["email"]; ?>" data-id="<?php echo $v["idalumno"] ?>" class="alumnosel col-md-3 col-sm-6 col-xs-12 btn-activar text-left active"><i class="fa fa-check-circle"></i> <?php echo @$v["stralumno"] ?> </a>
                      <?php } ?>
                  </div>
                  <div class="p-2"></div>
                  <legend><?php echo JrTexto::_("select members to invite to the video conference");?></legend>
                  <div class="row" id="listadoainvitar"></div>
                </fieldset>
              </div>          
            </div>        
            <hr>
            <div class="col-md-12 form-group text-center">
                <!--button id="btn-saveZoom" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('save');?> </button-->
                <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('videosconferencia'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Back');?></a>
            </div>
          </form>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new Object;
        data.idzoom = $('#idzoom').val();
        data.grupos = $('#grupos').val();
          $.post(_sysUrlBase_+'json/videosconferencia/guardar', data, function(rs, textStatus, xhr) {
            if(rs.code == 200){
              redir(_sysUrlBase_+'/videosconferencia');
            }
          },'json');
       }
  }).on('click','.btn-activar',function(){
    var i=$("i",this);
    v=0;  
    if(i.hasClass('fa-circle')){
      $(this).addClass('active');
      $('input',this).val(1);
      i.removeClass('fa-circle').addClass('fa-check-circle');
      v=1;
    }else{
      $(this).removeClass('active');
      $('input',this).val(0);
      i.addClass('fa-circle').removeClass('fa-check-circle');
      $(this).hide();
    }
    var data=new Object;
    data.idzoom = $('#idzoom').val();
    data.idalumno = $(this).attr('data-id');
    data.invitar = v;
    $.post(_sysUrlBase_+'json/videosconferencia/setInvitar', data, function(rs, textStatus, xhr) {
      if(rs.code == 200 ||rs.code=='ok'){
        Swal.fire( 'Proceso completo', rs.msj, 'success');
        var dt=rs.data;
      }
      $('#grupos').trigger('change');
    },'json');
  }).on('click','.subirfile',function(ev){
      __subirfile($(this));
  }).on('change','#grupos',function(ev){
    ev.preventDefault();
    var v=$(this).val()||'';
    var data=new Object;
    var dt = []; 
    var yaesta=[];   
    if(v != '' || v!='0'){
      data.idgrupoauladetalle = v;
      $('#listadoainvitar').html('');
      $('#listadoinvitados').html('');
      $.post(_sysUrlBase_+'json/acad_matricula/listado', data, function(rs, textStatus, xhr) {
        if(rs.code == 200 ||rs.code=='ok'){
          dt = rs.data;
          $.post(_sysUrlBase_+'json/videosconferencia/listado', { sql:2,idzoom:$('#idzoom').val()}, function(rs2, textStatus2, xhr2) {
            if(rs2.code == 200 ||rs2.code=='ok'){
              var obj = rs2.data;
              $.each(dt,function(i,v){ 
                    if(obj.findIndex((el) => el.idalumno == v["idalumno"] ) == -1){
                      $('#listadoainvitar').append('<a href="javascript:void(0)" data-campo="estado" data-id="'+v["idalumno"]+'" class="alumnosel col-md-3 col-sm-6 col-xs-12 btn-activar text-left"><i class="fa fa-circle"></i> '+v["stralumno"]+'</a>');
                    }else{
                      if($('#listadoinvitados').find('a[data-id="'+v["idalumno"]+'"]').length==0)
                        $('#listadoinvitados').append('<a href="javascript:void(0)" data-campo="estado" data-id="'+v["idalumno"]+'" class="alumnosel col-md-3 col-sm-6 col-xs-12 btn-activar text-left active"><i class="fa fa-check-circle"></i> '+v["stralumno"]+'</a>');
                    }
                  }
              );//endforeach
            }
          },'json'); //end ajax interno
        }
      },'json');//end ajax externo

    }
    if(v=='0'){
      $('#listadoainvitar').html('');
    }
  })

})
</script>

