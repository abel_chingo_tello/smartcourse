<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<div id="ventana_<?php echo $idgui; ?>">
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("video conference"); ?>'
  });
  addButonNavBar({
    class: 'btn btn-success btnvermodal',
    href: '<?php echo $this->documento->getUrlSitio();?>/videosconferencia/agregar',
    text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?>',
    data: {titulo: 'Agregar Video Conferencia', modal: 'si', href: '<?php echo $this->documento->getUrlSitio();?>/videosconferencia/agregar'}
  });
</script>
<div class="container p-4">
  
  <div class="row">
    <div class="col-md-12">    
       <div class="row">
       	<div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
              <div class="input-group" style="margin-top: 1px">
                <input type="date" name="busfecha" id="busfecha" class="form-control border0" value="<?php echo date("Y-m-d") ?>" >
                <span class="input-group-addon btn btnbuscar"> <i class="fa fa-calendar"></i></span>  
              </div>
            </div>
          </div>                                                                                                             
          <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
              <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
              <div class="input-group" style="margin-top: 1px">
                <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                  </div>
            </div>
          </div>
          <!-- <div class="col-md-4 col-sm-12 text-center">  
             <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/videosconferencia/agregar"" ><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
            </div> -->
          </div>
        </div>
         
      <div class="col-md-12 ">
      <div class="table-responsive">
          <table class="table table-striped " style="width: 100%;">
            <thead>
              <tr class="bg-info">
                <th>#</th>
                  <th><?php echo JrTexto::_("date") ;?></th>
                  <th><?php echo JrTexto::_("tittle) ;?></th>
                  <th><?php echo JrTexto::_("Url moderator") ;?></th>
                  <th><?php echo JrTexto::_("url participants") ;?></th>
                  <!--th><?php //echo JrTexto::_("Agenda") ;?></th-->
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
      	</table>
      </div>
  </div>
  </div>
</div>
</div>
<script type="text/javascript">
const IDPERSONA = <?php echo !empty($this->usuario['idpersona']) ? $this->usuario['idpersona']: 0 ; ?>;
const ROL = <?php echo !empty($this->usuario['idrol']) ? $this->usuario['idrol']: 0 ; ?>;
var tabledatos5d7aaa0998bb8='';
function refreshdatos5d7aaa0998bb8(){
    tabledatos5d7aaa0998bb8.ajax.reload();
}
$(document).ready(function(){  
  var estados5d7aaa0998bb8={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5d7aaa0998bb8='<?php echo ucfirst(JrTexto::_("zoom"))." - ".JrTexto::_("edit"); ?>';
  var draw5d7aaa0998bb8=0;
  var _imgdefecto='';


  $('.textosearchlist').on('keydown',function(ev){  if(ev.keyCode===13) refreshdatos5d7aaa0998bb8();
  }).blur(function(ev){ refreshdatos5d7aaa0998bb8()})
 $('#busfecha').on('change',function(ev){refreshdatos5d7aaa0998bb8();})
  
  $('.btnbuscar').click(function(ev){refreshdatos5d7aaa0998bb8(); });

  tabledatos5d7aaa0998bb8=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      "ajax":{
        url:_sysUrlBase_+'json/videosconferencia/listado',
        type: "post",                
        data:function(d){
            d.json=true;
            if(ROL != 1){
              d.idpersona = IDPERSONA;
            }                 
            d.texto=$('#texto').val(),
            d.fecha=$('#busfecha').val(),
            draw5d7aaa0998bb8=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5d7aaa0998bb8;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            var fecha='<?php echo date('Y-m-d'); ?>';
            var fechareg=data[i].fecha.substring(0,10);
            var urlmod='-';
            var urlinvi='-';
            if(fecha==fechareg){
              var urlmod='<a target="_blank"  href="'+data[i].urlmoderador+'"><?php echo JrTexto::_('start');?><br></a>';
              var urlinvi= '<a target="_blank"  href="'+data[i].urlparticipantes+'"><?php echo JrTexto::_('start');?><br></a>';
            }                               
            datainfo.push([           
              (i+1),data[i].fecha, data[i].titulo, urlmod, urlinvi,               
             '<a class="btn btn-xs " data-modal="si" href="'+_sysUrlBase_+'videosconferencia/invitar/?id='+data[i].idzoom+'" data-titulo="'+tituloedit5d7aaa0998bb8+'"><i class="fa fa-envelope"></i></a><a class="btn btn-xs " data-modal="si" href="'+_sysUrlBase_+'videosconferencia/editar/?id='+data[i].idzoom+'" data-titulo="'+tituloedit5d7aaa0998bb8+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idzoom+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new Object;
        data.idzoom = id;
        $.post(_sysUrlBase_+'json/videosconferencia/eliminar', data, function(rs, textStatus, xhr) {
          if(rs.code == 200){
            Swal.fire( 'Proceso completo', "Se ha eliminado correctamente", 'success');
            tabledatos5d7aaa0998bb8.ajax.reload();
          }
        },'json');
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'/json/videosconferencia/setcampo','masvalores':{idzoom:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new Object;
    data.idzoom = _this.attr('data-id');
    data.campo = _this.attr('data-campo');
    data.valor = activo;
    $.post(_sysUrlBase_+'json/videosconferencia/setcampo', data, function(rs, textStatus, xhr) {
      if(rs.code==200){
        Swal.fire( 'Proceso completo', "Se ha activado correctamente", 'success');
        tabledatos5d7aaa0998bb8.ajax.reload();
      }
    },'json');
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var titulo=$(this).attr('data-titulo')||'';
    // agregar el tipo de curso del grupo
    // url = String.prototype.concat(url,'&tipodecurso=',$('#cbproyectos option:selected').data('tipocurso'));
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      tabledatos5d7aaa0998bb8.ajax.reload();
      //_md.trigger('cerramodal');
    }).on('submit','#frmzoom',function(ev){
      ev.preventDefault();
       event.preventDefault();
     
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new Object;        
        data.idzoom = $('#idzoom').val();
        data.titulo = $('#txtTitulo').val();
        data.agenda = $('#txtAgenda').val();
        data.fecha = $('#txtFecha').val();
        data.time = $('#txtTime').val();
        data.duracion = $('#txtDuracion').val();
        $.post(_sysUrlBase_+'json/videosconferencia/guardar', data, function(rs, textStatus, xhr) {
          if(rs.code == 200){
          tabledatos5d7aaa0998bb8.ajax.reload();
          __cerramodal(_md);
          }
        },'json');      

    }).on('click','.btn-activar',function(){
      var i=$("i",this);  
      if(i.hasClass('fa-circle')){
        $(this).addClass('active');
        $('input',this).val(1);
        i.removeClass('fa-circle').addClass('fa-check-circle');
      }else{
        $(this).removeClass('active');
        $('input',this).val(0);
        i.addClass('fa-circle').removeClass('fa-check-circle');
      }     
    }).on('click','.subirfile',function(ev){
        __subirfile($(this));
    })
     
  })
});
</script>