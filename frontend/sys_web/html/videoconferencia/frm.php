<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("video conference"); ?>',
      link: _sysUrlBase_+'videosconferencia'
    });
    addItemNavBar({
      text: '<?php echo JrTexto::_("new"); ?>'
    });
  </script>
<?php } ?>
<div class="container p-4">
  
  <div class="row"  id="vent-<?php echo $idgui;?>"  >
    <div class="col-md-12">
          <div id="msj-interno"></div>
          <form method="post" id="frmzoom"  target="#" enctype="" class=" form-label-left" >
            <input type="hidden" name="idzoom" id="idzoom" value="<?php echo $this->pk;?>">
            <div class="form-group">
                <label class="control-label"><?php echo JrTexto::_('tittle');?> <span class="required"> * </span></label>
                <div class="">
                  <input type="text"  id="txtTitulo" name="titulo" required="required" class="form-control" value="<?php echo @$frm["titulo"];?>"> 
                </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('moderator email');?> <span class="required"> * </span></label>
              <div class="">
                <input type="email" name="email"  required="required" class="form-control" style="resize: none;" value="<?php echo !empty($frm["correo"])?$frm["correo"]:$this->curuserID; ;?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('schedule');?> <span class="required"> * </span></label>
              <div class="">
                <textarea id="txtAgenda" name="agenda" required="required" class="form-control" style="resize: none;"><?php echo @$frm["agenda"];?></textarea>
                             
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('date');?> <span class="required"> * </span></label>
                  <div class="">
                    <input type="date"  id="txtFecha" name="fecha" required="required" class="form-control" value="<?php echo !empty($frm["fecha"])?substr($frm["fecha"],0,10):date('Y-m-d');?>">
                                      
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('hour');?> <span class="required"> * </span></label>
                  <div class="">
                    <input type="time"  id="txtTime" name="time" required="required" class="form-control" value="<?php echo !empty($frm["fecha"])?substr($frm["fecha"],11):date('H:i:00');?>">
                                      
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('duration in minutes');?><span class="required"> * </span></label>
                  <div class="">
                    <input type="number"  id="txtDuracion" name="duracion" required="required" class="form-control" value="<?php echo !empty($frm["duracion"])?$frm["duracion"]:40;?>">
                                      
                  </div>
                </div>
              </div>           
            </div>
            <hr>
            <div class="col-md-12 form-group text-center">
                <button id="btn-saveZoom" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('save');?> </button>
                <a type="button" class="btn btn-warning " href="<?php echo JrAplicacion::getJrUrl(array('videosconferencia'))?>"   ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('cancel');?></a>
            </div>
          </form>
    </div>
  </div> 
</div>