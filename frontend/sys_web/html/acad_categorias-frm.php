<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/acad_categorias">&nbsp;<?php echo JrTexto::_('Acad_categorias'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idcategoria" id="idcategoria" value="<?php echo $this->pk;?>">
          <div class="row">
            <div class="col-md-8 col-sm-9 col-xs-12">
                <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('Parent category');?> </label>
                  <div class="cajaselect">
                    <select name="idpadre" id="idpadre" class="form-control">
                      <option value=""><?php echo ucfirst(JrTexto::_("empty"))?></option>
                      <?php if(!empty($this->categorias))
                      foreach ($this->categorias as $tip){ ?>
                        <option value="<?php echo $tip["idcategoria"] ?>"><?php echo ucfirst(JrTexto::_($tip["nombre"]))?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>
                  <div class="">
                    <input type="text"  id="txtNombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">                                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('Order');?> </label>
                  <div class="">
                   <input type="number" id="txtOrden" name="orden" value="<?php echo !empty($frm["orden"])?$frm["orden"]:0;?>"  min="0" class="form-control" required />
                                      
                  </div>
                </div>                
              
            </div>
            <div class="col-md-4 col-sm-5 col-xs-12">
              <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('Imagen');?> <span class="required"> * </span></label>
                  <div class="">            
                    <img src="<?php echo URL_BASE.(!empty($frm["imagen"])?$frm["imagen"]:$_imgdefecto);?>" alt="imagenfile" file="imagen" class="subirfile img-thumbnail" style="max-height:200px; max-width:200px;">
                    <input type="hidden" name="imagen" value="<?php echo !empty($frm["imagen"])?URL_BASE.$frm["imagen"]:"";?>">
                  </div>
                </div>
            </div>            
          </div>
            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Descripcion');?> <span class="required"> * </span></label>
              <div class="">
               <textarea id="txtDescripcion" name="descripcion" class="form-control" ><?php echo @trim($frm["descripcion"]); ?></textarea>                                  
              </div>
            </div>
                <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('State');?> <span class="required"> * </span></label>
                  <div class="">
                    <a style="cursor:pointer;" class="btn-activar <?php echo !empty($frm["estado"])?'active':'';?>" dataactive="<?php echo JrTexto::_('Active') ?>" datainactive="<?php echo JrTexto::_('Inactive') ?>"><i class="fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle-o";?>" ></i>
                     <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Active":"Inactive");?></span>
                     <input type="hidden" name="estado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" > 
                     </a>
                                                          
                  </div>
                </div>
                       
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveAcad_categorias" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('acad_categorias'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm_categoria.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm_categoria($('#vent-<?php echo $idgui;?>'));
  })
</script>