<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$imgdefecto=URL_MEDIA."/static/media/nofoto.jpg";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Courses"); ?>'
    });
    /*addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });*/
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;} 
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>">  
    <div class="row">                                                           
        <div class="col-xs-6 col-sm-4 col-md-3 " >
          <!--label>Estado</label-->
	          <div class="select-ctrl-wrapper select-azul">
	          <select name="cbestado" id="cbestado" class="form-control select-ctrl">
	              <option value=""><?php echo ucfirst(JrTexto::_("All Status"))?></option>
	              <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
	              <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
	          </select>
	        </div>
        </div>
                                                                                       
	    <div class="col-xs-6 col-sm-6 col-md-6">
	      <div class="form-group">
	        <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
	        <div class="input-group">
	          <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
	          <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
	        </div>
	      </div>
	    </div>
            <!--div class="col-md-6 col-sm-12 text-center">             
               <a class="btn btn-success btnvermodal" href="<?php //echo $this->documento->getUrlSitio();?>/foros/agregar" ><i class="fa fa-plus"></i> <?php //echo JrTexto::_('add')?></a>
          	</div-->
          	 
    	<div class="col-md-12 col-sm-12 col-xs-12">  
        	<div class="x_content">
	          <div class="row">
	            <div class="col-md-12 table-responsive">
	              <table class="table table-striped table-hover">
	                <thead>
	                  <tr class="headings">
	                    <th>#</th>                             
	                    <th class="nombrecat"><?php echo JrTexto::_("Course"); ?></th>
	                    <!--th><?php //echo JrTexto::_("Estado"); ?></th-->
	                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
	                  </tr>
	                </thead>
	                <tbody>
	                	<?php 
	                	$i=0;
	                	if(!empty($this->cursos)) 
	                	foreach($this->cursos as $key => $cur){ $i++; ?>
	                		<tr>
	                			<td><?php echo $i; ?></td>
	                			<td><?php echo $cur["nombre"]; ?></td>
	                			<td><buttom class="btn btn-primary btndescargar" style="cursor:pointer;" idcurso="<?php echo $cur["idcurso"]; ?>" nombredecurso="<?php echo $cur["nombre"]; ?>"  idcomplementario="<?php echo $cur["idcomplementario"]; ?>" > <i class="fa fa-download"></i> </buttom> </td>
	                		</tr>
	                	<?php } ?>
	                </tbody>
	              </table>
	            </div>
	          </div>
	        </div>
        </div>
    </div> 
</div>
<div class="modal fade" id="modalcurso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?=JrTexto::_("Course download")?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3 class="nombredecurso"></h3>
        <div class="row">
        	<div class="col-md-12 panelresultadodescarga">
      			<p><?=JrTexto::_("Starting process")?></p>  		
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=JrTexto::_("Close")?></button>       
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
	$('.btndescargar').on('click',function(ev){
		$modal=$('#modalcurso');
		let nombredecurso=$(this).attr('nombredecurso')||'';
		let idcurso=$(this).attr('idcurso')||'';
		let idcomplementario=$(this).attr('idcomplementario')||'';

		$modal.find('.nombredecurso').text(nombredecurso);
		$modal.find('.panelresultadodescarga').html('<p><?=JrTexto::_("Starting process")?> </p>');

		$('#modalcurso').modal('show');
		var procesar=function(){
			$.ajax({url:_sysUrlBase_+'json/descargar',method:'POST',data:{idcurso:idcurso,idcomplementario:idcomplementario} }).done(function(){
			//    alert( "success");
			}).fail(function() {
			//    alert( "error" );
			}).always(function() {
			//    alert( "complete" );
			});
		}
		procesar();
	})
})
	
</script>