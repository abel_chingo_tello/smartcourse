<style css>
    #formulario label { font-weight:bold; }
    #formulario .formulario-subtitle { border-bottom:2px solid gray; }
</style>
<div class="row" id="breadcrumb"> 
    <div class="col">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>        
            <li><a href="<?php echo $this->documento->getUrlSitio();?>/representantes"><?php echo JrTexto::_("Representantes"); ?></a></li>       
            <li class="active">&nbsp;<?php echo JrTexto::_("Agregar"); ?></li>       
            <li class="active">&nbsp;<?php echo JrTexto::_("Pagina"); ?> <?php echo $this->page ?></li>       
        </ol>
    </div> 
</div>
<div class="container">
    <h5><?php echo JrTexto::_("Pagina"); ?>: <?php echo $this->page ?></h5>
    <div id="msjerror"></div>
    <h4 class="text-center"><?php echo JrTexto::_("Información del representante"); ?></h4>
    <!--START PANEL-->
    <div class="card bg-light">
        <div class="card-header"><h5><?php echo JrTexto::_("Información") ?></h5></div>
        <div class="card-body">
            <div id="formulario">
                <button type="button" class="btn btn-secondary clearform"><?php echo JrTexto::_("limpiar formulario") ?></button>
                <h5 class="formulario-subtitle w3-text-gray font-weight-bold"><?php echo JrTexto::_("Representante"); ?></h5>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("País"); ?></label>
                            <select id="cmbpais" name="cmbpais" class="form-control data-required">
                                <option value="0"><?php echo JrTexto::_("Seleccionar") ?></option>
                                <?php foreach($this->_pais as $value): ?>
                                    <option value="<?php echo $value['id'] ?>"><?php echo $value['nombre'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Departamento"); ?></label>
                            <select id="cmbdepartamento" name="cmbdepartamento" class="form-control data-required"><option value="0"><?php echo JrTexto::_("Seleccionar") ?></option></select>
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Provincia"); ?></label>
                            <select id="cmbprovincia" name="cmbdepartamento" class="form-control data-required"><option value="0"><?php echo JrTexto::_("Seleccionar") ?></option></select>
                        </div>
                        <div class="col-sm-12"><hr/></div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Nombres"); ?></label>
                            <input type="text" id="txtnombres" name="txtnombres" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Primer apellido"); ?></label>
                            <input type="text" id="txtprimerape" name="txtprimerape" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Segundo apellido"); ?></label>
                            <input type="text" id="txtsegundoape" name="txtsegundoape" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Tipo de identificación"); ?></label>
                            <select id="cmbtipodni" name="cmbtipodni" class="form-control"><option value="1">DNI</option> <option value="2"><?php echo JrTexto::_("Pasaporte") ?></option></select>
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Número de identificación"); ?></label>
                            <input id="cmbnumerodni" name="cmbnumerodni" type="text" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Teléfono"); ?></label>
                            <input id="txttelefonoper" name="txttelefonoper" type="text" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Correo electronico"); ?></label>
                            <input id="txtemailper" name="txtemailper" type="text" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Tipo de Representante"); ?></label>
                            <select id="cmbtiporepresentante" name="cmbtiporepresentante" class="form-control">
                                <option value="0"><?php echo JrTexto::_("Seleccionar"); ?></option>
                                <?php foreach($this->tiporepresentante as $value): ?>
                                    <option data-rol="<?php echo $value['idrol'] ?>" value="<?php $value['id'] ?>"><?php echo JrTexto::_($value['nombre']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <h5 class="formulario-subtitle w3-text-gray font-weight-bold"><?php echo JrTexto::_("Empresa"); ?></h5>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Razón Social"); ?></label>
                            <input type="text" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Identificación tributaria"); ?></label>
                            <input type="text" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Dirección de la entidad"); ?></label>
                            <textarea rowspan="3" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>"></textarea>
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Teléfono"); ?></label>
                            <input type="text" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo JrTexto::_("Correo electronico"); ?></label>
                            <input type="text" class="form-control" placeholder="<?php echo JrTexto::_("Escribir aquí"); ?>" />
                        </div>
                    </div>
                </div>
                <div id="ambitoempresa" class="w3-hide">
                    <h5 class="formulario-subtitle w3-text-gray font-weight-bold"><?php echo JrTexto::_("Ambito de la empresa"); ?></h5>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label><?php echo JrTexto::_("Región") ?></label>
                                <select class="form-control"><option value="0"><?php echo JrTexto::_("Nacional"); ?></option></select>
                            </div>
                            <div class="col-sm-4">
                                <label><?php echo JrTexto::_("Departamentos") ?></label>
                                <select class="form-control"><option value="0"><?php echo JrTexto::_("Todos"); ?></option></select>
                            </div>
                            <div class="col-sm-4">
                                <label><?php echo JrTexto::_("Provincias") ?></label>
                                <select class="form-control"><option value="0"><?php echo JrTexto::_("Todos"); ?></option></select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-sm-4 text-center pt-2"><a class="btn btn-primary" href="<?php echo $this->documento->getUrlSitio();?>/representantes"><?php echo JrTexto::_("Regresar"); ?></a></div>
                <div class="col-sm-4 text-center"><div class="p-3 w3-white w3-border w3-border-gray w3-round"><?php echo JrTexto::_("Pagina"); ?>: <span class="font-weight-bold w3-text-green"><?php echo $this->page ?></span> <span class="w3-large">/</span> <?php echo $this->maxpage ?></div></div>
                <div class="col-sm-4 text-center pt-2"><a id="btnnextpage" class="btn btn-success" href="<?php echo $this->documento->getUrlSitio();?>/representantes/agregar/2"><?php echo JrTexto::_("Siguiente"); ?></a></div>
            </div>
        </div>
    </div>
    <!--END PANEL-->
</div>

<script type="text/javascript">
var historial = null;
var persona = null, representantes = null, empresa = null, ambito = null;

function initObjects(){
    historial = {
            informacion:{
                persona: null, representantes: null, empresa: null, ambito: null
            },
            contrato:{
                empty:null
            },
            accesos:{
                empty:null
            }
            
        }
    persona = { tipodoc: null, dni: null, primer_ape: null, segundo_ape: null, nombre:null,sexo:null,telefono:null,image:null,email:null,idpais:null,iddepartamento:null,idprovincia:null,usuario:null,clave:null }
    representantes = {idpersona:null,idempresa:null,idambito:null,idpadre:null,idcontrato:null,tipo: null}
    sessionStorage.setItem("sessionhistorialrepresentante", "true");
}

$(document).ready(function(){
    /*/check if exist a before session */
    var historialjson = localStorage.getItem("historialrepresentante");
    var lastsession = sessionStorage.getItem("sessionhistorialrepresentante");

    var valueCMBReset = '<option value="0">'+'<?php echo JrTexto::_("Seleccionar") ?>'+'</option>';

    if(!historialjson || historialjson == null){
        /**Init */
        initObjects();
    }else{
        if(!lastsession || lastsession == null){
            Swal.fire({
                title: '¿Desea continuar la configuración anterior?',
                text: "Tiene una configuración anterior incompleta, ¿Desea recuperarla y continuar?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.value) {
                    historialjson = JSON.parse(historialjson);
                }else{
                    initObjects();            
                }
            });
        }else{
            //rellenar todo los campos
        }
    }
    /**Set default */
    persona.tipodoc = $('#cmbtipodni').val();
    /**
    * Events
     */
    $('.clearform').click(function(){
        console.log("limpiar el formulario");
    });
    $('#cmbpais').on('change',function(){
        var cmbtarget = '#cmbdepartamento';
        if($(this).val() != 0){
            $.post(_sysUrlBase_+'json/departamentos',{'idpais':$(this).val()}, function(data, status){
                $(cmbtarget).html(valueCMBReset);
                for(var valor of data.data){
                    $(cmbtarget).append('<option value="'+valor.id+'">'+valor.nombre+'</option>');
                }   
            },'json');
            
            persona.idpais = $(this).val();
        }//endif;
    });
    $('#cmbdepartamento').on('change',function(){
        var cmbtarget = '#cmbprovincia';
        if($(this).val() != 0){
            $.post(_sysUrlBase_+'json/provincias',{'idpais':$('#cmbpais').val(),'iddepartamento':$(this).val()}, function(data, status){
                $(cmbtarget).html(valueCMBReset);
                for(var valor of data.data){
                    $(cmbtarget).append('<option value="'+valor.id+'">'+valor.nombre+'</option>');
                }
            },'json');
            persona.iddepartamento = $(this).val();
        }//endif;
    });
    $('#cmbprovincia').on('change',function(){
        if($(this).val() != 0){
            persona.idprovincia = $(this).val();
        }
    });
    $('#cmbtipodni').on('change',function(){
        if($(this).val() != 0){
            persona.tipodoc = $(this).val();
        }
    });
    $('#cmbtiporepresentante').on('change',function(){
        if($(this).val() != 0){
            persona.idprovincia = $(this).val();
        }
    });
    $('#txtnombres').on('focusout',function(){
        if($(this).val().length > 0 ||  $(this).val() != ''){
            persona.nombre = $(this).val();
        }
    });
    $('#txtprimerape').on('focusout',function(){
        if($(this).val().length > 0 ||  $(this).val() != ''){
            persona.primer_ape = $(this).val();
        }
    });
    $('#txtsegundoape').on('focusout',function(){
        if($(this).val().length > 0 ||  $(this).val() != ''){
            persona.segundo_ape = $(this).val();
        }
    });
    $('#cmbnumerodni').on('focusout',function(){
        if($(this).val().length > 0 ||  $(this).val() != ''){
            persona.dni = $(this).val();
        }
    });
    $('#txttelefonoper').on('focusout',function(){
        if($(this).val().length > 0 ||  $(this).val() != ''){
            persona.telefono = $(this).val();
        }
    });
    $('#txtemailper').on('focusout',function(){
        if($(this).val().length > 0 ||  $(this).val() != ''){
            persona.email = $(this).val();
        }
    });
    $('#btnnextpage').click(function(ev){
        ev.preventDefault();
        var _continue = true;
        console.log("guardar informacion");
        //check required values
        $('#msjerror').html('').hide();
        $('select,input,textarea').each(function(){
            if($(this).hasClass('data-required')){
                $(this).css({border:'',boxShadow:""});
                if($(this).val().length == 0 || $(this).val() == 0 ||  $(this).val() == ''){
                    _continue = false;
                    $(this).css({border:'1px solid red',boxShadow:"1px 1px 5px red"});
                }
            }
        });
        if(!_continue){
            $('#msjerror').append('<p class="alert alert-danger">'+'<?php echo JrTexto::_("Algunos campos no fueron definidos y son requeridos, por favor completar los campos señalados") ?>'+'</p>').show();
            return false;
        }
        if(!historial){
            console.error("No existe el objeto historial, no esta definido historial");
            return false;
        }
        historial.informacion.persona = persona;
        historial.informacion.representantes = representantes;
        historial.informacion.empresa = empresa;
        historial.informacion.ambito = ambito;
        console.log(historial);
        /*/set new session */
        localStorage.setItem("historialrepresentante", JSON.stringify(historial));
        //window.location.href = $(this).attr("href"); 
    });
});
</script>