<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <button type="button" id="read" class="btn btn-block btn-info">Procesar Data</button>
            </div>
            <div class="col-md-4">
                <button type="button" id="save" class="btn btn-block btn-info">Guardar Data</button>
            </div>
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-body panelbody">
        <div class="pnlpregunta">
            <?php foreach ($this->datos as $i => $value) : ?>
                <?php $value["preguntas"] = json_decode($value["preguntas"], true); ?>
                <?php foreach ($value["preguntas"] as $j => $preg) : ?>
                    <div class="tmppregsav" data-id="<?php echo $value["idnota"] ?>"><?php echo /*htmlspecialchars($preg)*/ $preg ?></div>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script>
    var id = 0;
    var id_ = 0;
    var i = 0;
    var jsonPreguntas = new Array();
    var notas = new Array();

    $("#read").click(function() {
        procesar();
    });
    $("#save").click(function() {

    });

    function save(notas) {
        var adds = localStorage.getItem('notQuizProc')
        var formData = new FormData();
        formData.append('notas', JSON.stringify(notas));
        formData.append('notasProc', adds);
        $.ajax({
            url: _sysUrlBase_ + "tools/save_nota_quiz",
            type: "POST",
            data: formData,
            contentType: false,
            dataType: 'json',
            cache: false,
            processData: false,
            success: function(resultado) {
                // console.log(resultado);
                if (resultado.code == 200) {
                    localStorage.setItem('notQuizProc', JSON.stringify(resultado.data));
                    console.log("Guardado correctamente");
                    let elote=parseInt('<?=$_REQUEST["l"]?>')+1;
                    windows.location.href= _sysUrlBase_ + "tools/nota_quiz?l="+elote;
                }
            }
        });
    }
    $(document).ready(function() {
        console.log("Carga Completa");
        procesar().then((notas) => {
            save(notas);
        });
        
    });

    function procesar() {
        return new Promise((resolve, reject) => {
            id = 0;
            id_ = 0;
            i = 0;
            var html = "";
            jsonPreguntas = {};
            notas = new Array();
            var total = parseInt($('.panelbody .pnlpregunta .tmppregsav').length) - 1;
            $('.panelbody .pnlpregunta .tmppregsav').each(function(index, elem) {
                id_ = parseInt($(elem).attr("data-id"));
                html = $(elem).html();
                // console.log(html);
                // console.log("id_", id_);
                if (id_ == id) {
                    // console.log("if", id, id_);
                    jsonPreguntas[i] = window.btoa(unescape(encodeURIComponent(html)));
                    i++;
                    // jsonPreguntas.push(window.btoa(unescape(encodeURIComponent(html))));
                    if (index == total) {
                        // console.log("if index == total", index, total);
                        // jsonPreguntas[i] = window.btoa(unescape(encodeURIComponent(html)));
                        // i++;
                        notas.push({
                            "id": id,
                            "preguntas": JSON.stringify(jsonPreguntas)
                        });
                        // notas.push({"id": id, "preguntas": jsonPreguntas});
                    }
                } else {
                    // console.log("else", id, id_);
                    if (index != 0) {
                        // console.log("index != 0", index);
                        notas.push({
                            "id": id,
                            "preguntas": JSON.stringify(jsonPreguntas)
                        });
                        // notas.push({"id": id, "preguntas": jsonPreguntas});
                    }
                    if (index == total) {
                        // console.log("index == total", index, total);
                        jsonPreguntas[i] = window.btoa(unescape(encodeURIComponent(html)));
                        i++;
                        // jsonPreguntas.push(window.btoa(unescape(encodeURIComponent(html))));
                        notas.push({
                            "id": id_,
                            "preguntas": JSON.stringify(jsonPreguntas)
                        });
                        // notas.push({"id": id, "preguntas": jsonPreguntas});
                    }
                    jsonPreguntas = {};
                    i = 0;
                    id = id_;
                    jsonPreguntas[i] = window.btoa(unescape(encodeURIComponent(html)));
                    i++;
                    // jsonPreguntas.push(window.btoa(unescape(encodeURIComponent(html))));
                }
                // console.log("jsonPreguntas", jsonPreguntas);
            });
            console.log('PROCESADO')
            resolve(notas);
        })
    }
</script>