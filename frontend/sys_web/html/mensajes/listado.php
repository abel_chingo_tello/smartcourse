<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("posts"); ?>'
        });
        addButonNavBar({
            click: 'iniciarMensajes();',
            class: 'btn btn-info btn-sm',
            text: '<i class="fa fa-refresh"></i> <?php echo JrTexto::_('upgrade') ?>'
        });
        addButonNavBar({
            id: 'compose',
            class: 'btn btn-success btn-sm Agreggarforo',
            text: '<i class="fa fa-envelope"></i> <?php echo JrTexto::_('write') ?>'
        });
    </script>
<?php } ?>
<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" /> -->
<style>
    #tb-contenido_wrapper>div>a.paginate_button.previous.disabled,
    #tb-contenido_wrapper>div>a.paginate_button.next.disabled {
        padding: 3px !important;
        color: #a2a2a2 !important;
        background-color: white !important;
    }

    .compose {
        z-index: 1 !important;
    }

    div.tox-toolbar__overflow[data-alloy-vertical-dir="toptobottom"] {
        overflow: visible !important;
    }

    td {
        padding: 0 !important;
    }

    table,
    th {
        border: none !important;
    }

    .dataTables_wrapper {
        margin-bottom: 20px;
    }

    .dataTables_length {
        font-style: italic;
    }

    table.dataTable tbody tr {
        background-color: transparent;
    }

    .dataTables_length>label>select {
        background-color: #f4f4f4;
        border: 1px solid gainsboro;
        border-radius: 4px;
    }

    .no-mensajes {
        font-style: italic;
        font-size: 20px;
        text-align: center;
        width: 100%;
        padding: 0;
        margin: 0;
        color: #2b739c;
    }

    .para {
        width: 100%;
        border: none !important;
        outline: none;
        height: 27px;
        font-size: 15px;
        padding-left: 10px;
    }

    .select2-container {
        z-index: 99999;
    }

    .my-layout {
        background: #F7F7F7;
        border: 1px solid #EFEFEF;
        margin: 0;
    }

    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border: solid #ced4da 1px;

    }

    .select2-container--default .select2-selection--single {
        border: 1px solid #ced4da;
        padding: .46875rem .75rem;
        height: calc(2.25rem + 2px);

    }

    .select2-selection {
        display: flex !important;
        align-items: center;
        /* line-height: initial !important;
        margin-top: 0 !important; */
    }

    .select2-selection__choice {
        margin-top: .18rem !important;
    }

    .select2-search__field {
        margin-top: 0;
    }

    .select2-container--default .select2-selection--multiple {
        border: 1px solid #ced4da;
        min-height: calc(2.25rem + 2px);

    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #007bff;
        border-color: #006fe6;
        color: #fff;
        padding: 0 10px;
        /* margin-top: .31rem; */
    }

    div.x_title>h2 {
        font-size: 1.6rem;
    }

    #lista_contenido .mail_list {
        color: #393939 !important;
    }

    #lista_contenido .mail_list.msg-active {
        color: #0081ff !important;
    }

    .mail_list:hover {
        background-color: #00000008;
    }
</style>

<div class="soft-rw my-font-all">
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel my-card my-shadow">
                <div class="x_title">
                    <h2><?php echo JrTexto::_('received messages') ?>:
                        <!-- <small>User Mail</small> -->
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div id="preview" class="col-sm-3 mail_list_column">
                            <!-- <button id="compose" class="btn btn-sm btn-success btn-block" type="button">REDACTAR</button> -->
                            <!-- (yo) -->
                        </div>
                        <!-- /MAIL LIST -->

                        <!-- CONTENT MAIL -->
                        <div class="col-sm-9 mail_view">
                            <div class="inbox-body">

                            </div>

                        </div>
                        <!-- /CONTENT MAIL -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- compose -->
    <div class="compose col-md-8">
        <div class="compose-header">
            <?php echo JrTexto::_('new message') ?>
            <button id="btncerrar" type="button" class="close compose-close">
                <span>×</span>
            </button>
        </div>

        <div class="compose-body">
            <div id="alerts"></div>
            <!-- (yo) -->
            <!-- Para -->
            <div class="row my-layout">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?php echo JrTexto::_('search in:') ?></label>
                        <select class="form-control group-select2" style="width: 100%;">
                            <?php
                            $options = "";
                            $ids = array();
                            foreach ($this->grupos as $key => $value) {
                                $options .= '<option value="' . $value["idgrupoaula"] . '">' . $value["nombre"] . '</option>';
                                $ids[] = $value["idgrupoaula"];
                            }
                            echo '<option value="' . implode(",", $ids) . '">Todos</option>';
                            echo $options;
                            ?>
                        </select>
                    </div>

                </div>
                <!-- /.col -->
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="person-select2" multiple="multiple" data-placeholder="Enviar a..." style="width: 100%;">
                        </select>
                    </div>

                </div>
                <!-- /.col -->
                <div class="col-md-12">
                    <input id="asunto" placeholder="<?php echo JrTexto::_('subject')?>" class="form-control" type="text">
                </div>
                <div class="col-md-12">
                    <textarea id="mytextarea" placeholder="<?php echo JrTexto::_('start editing here')?>..."></textarea>
                </div>
            </div>
            <!-- Para -->

        </div>

        <div class="compose-footer pull-right">
            <button id="send" class="btn btn-sm btn-success" type="button"><?php echo JrTexto::_('send') ?> <i class="fa fa-send"></i></button>
        </div>
    </div>
    <!-- /compose -->
</div>
<script>
    var table = null;
    var arrCorreos = new Array();
    $(document).ready(function() {
        iniciarMensajes();
        $('.group-select2').trigger("change");
        tinymce.init({
            selector: '#mytextarea',
            language: 'es_MX',
            height: "170",
            plugins: [
                'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'table emoticons template paste'
            ],
            menu: {
                file: {
                    title: 'File',
                    items: 'newdocument restoredraft | preview | print '
                },
                edit: {
                    title: 'Edit',
                    items: 'undo redo | cut copy paste | selectall | searchreplace'
                },
                view: {
                    title: 'View',
                    items: 'code | visualaid visualchars visualblocks | spellchecker | preview fullscreen'
                },
                insert: {
                    title: 'Insert',
                    items: 'image link codesample inserttable | charmap emoticons hr | pagebreak nonbreaking anchor toc | insertdatetime'
                },
                format: {
                    title: 'Format',
                    items: 'bold italic underline strikethrough superscript subscript codeformat | formats blockformats fontformats fontsizes align | forecolor backcolor | removeformat'
                },
                tools: {
                    title: 'Tools',
                    items: 'code wordcount'
                },
                table: {
                    title: 'Table',
                    items: 'inserttable | cell row column | tableprops deletetable'
                },

            },
            menubar: 'edit insert view format table tools help',
            toolbar: "undo redo  | fontselect |styleselect | bold  italic| link image | alignleft aligncenter alignright alignjustify | outdent indent | preview ",
            image_title: true,
            automatic_uploads: true,
            file_picker_types: 'image',
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function() {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), {
                            title: file.name
                        });
                    };
                    reader.readAsDataURL(file);
                };

                input.click();
            }
        });
    });

    function drawTable() {
        table = $('#tb-contenido').DataTable({
            searching: false,
            ordering: false,
            paging: true,
            info: false,
            bLengthChange: false,
            // pagingType: 'numbers',
            language: {
                search: "Buscar:",
                lengthMenu: "Mostrando los &nbsp _MENU_ &nbsp primeros comentarios",
                // info: "Mostrando _START_ a _END_ publicaciones de un total de _TOTAL_ ",
                paginate: {
                    first: "Primera página",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Última página"
                }
            }
        });
        return table;
    }

    function iniciarMensajes() {

        $.post(_sysUrlBase_ + "json/acad_mensaje", {
            "para": "mi",
            "estado": 1
        }, function(data) {
            if (data.code === 200) {
                arrCorreos = data.data;
                if (table != null) {
                    table.destroy();
                }
                $('#preview').html("");
                if (arrCorreos.length > 0) {
                    let htmlPreview = ``;
                    arrCorreos.forEach((correo, index) => {
                        htmlPreview += `
                            <tr><td>
                                <a id="msj${correo.id}" class="cont-msg" onclick="visualizarCorreo(${index},this)" title="${correo.leido == '0' ? 'Sin Leer' : 'Leído'}" style="cursor: pointer;">
                                    <div class="mail_list" style="color: ${correo.leido == '0' ? 'gray' : 'blue'};">
                                        <div class="left">
                                            <i class="fa fa-circle my-hidden"></i> 
                                            <!--
                                            <i class="fa fa-edit"></i>
                                            -->
                                        </div>
                                        <div class="right">
                                            <h3>${correo.nombreAutor} <small>${correo.hora}</small></h3>
                                            <p>${correo.asunto}</p>
                                        </div>
                                    </div>
                                </a>
                            </td></tr>`;
                    });
                    let htmlTable = `<table id="tb-contenido">
                                        <thead>
                                        <tr>
                                            <th> </th>
                                        </tr>
                                        </thead>
                                        <tbody id="lista_contenido">
                                        </tbody>
                                    </table>`;
                    $('#preview').html(htmlTable);
                    $('#lista_contenido').html(htmlPreview);
                    table = drawTable();
                    visualizarCorreo(0);
                } else {
                    htmlPreview = `
                        <h3 class="no-mensajes">
                        <?php echo JrTexto::_('it seems you do not have messages yet') ?>...
                        </h3>
                    `;
                    $('#preview').parent().html(htmlPreview);
                }
            }
        }, "json");
    }

    function resaltar(el) {
        console.log('el', el);
        $('#lista_contenido').find('.fa-circle').addClass('my-hidden');
        $('#lista_contenido').find('.mail_list').removeClass('msg-active');
        $(el).find('.mail_list').addClass('msg-active');
        $(el).find('.fa-circle').removeClass('my-hidden');
    }

    function visualizarCorreo(index, el = null) {
        if (el == null) {
            resaltar($('.cont-msg')[0]);
        } else {
            resaltar(el);
        }

        let correo = arrCorreos[index];
        console.log(correo);
        if (correo.leido == '0') {
            $.post(_sysUrlBase_ + "json/acad_mensaje/setCampo", {
                "tabla": 'acad_mensaje_para',
                "id": correo.id2,
                "campo": 'leido',
                "valor": 1
            }, function(data) {
                if (data.code === 200) {
                    correo.leido = '1';
                    arrCorreos[index] = correo;
                    $("#msj" + correo.id).attr("title", "Leído");
                    $("#msj" + correo.id).children().attr("style", "color: blue;");
                }
            }, "json");
        }
        htmlVisualizar = `
                        <div class="mail_heading row">
                            <div class="col-md-8">
                                <div class="btn-group">
                                    <!--
                                    <button class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Responder</button>
                                    <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i></button>
                                    -->
                                    <!-- button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></button-->
                                    <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash" title="Eliminar Mensaje" onclick="eliminar(${correo.id})"><i class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                <p class="date"><i class="fa fa-calendar"></i> ${correo.fecha} <i class="fa fa-clock-o"></i> ${correo.hora} </p>
                            </div>
                            <div class="col-md-12">
                                <h4><strong>Asunto:</strong> ${correo.asunto}</h4>
                            </div>
                        </div>
                        <div class="sender-info">
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>${correo.nombreAutor}</strong>
                                    <span>(${correo.correoAutor})</span> para
                                    <strong>mí</strong>
                                    <!-- a class="sender-dropdown"><i class="fa fa-chevron-down"></i></a -->
                                </div>
                            </div>
                        </div>
                        <div class="view-mail">
                            <p>${correo.mensaje}</p>
                        </div>
                        <!--
                        <div class="btn-group">
                            <button class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Responder</button>
                            <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i></button>
                            <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></button>
                            <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                        </div>
                        -->
                        `;
        $('.inbox-body').html(htmlVisualizar);
    }

    $('.group-select2').change(function() {
        $.post(_sysUrlBase_ + "json/acad_matricula", {
            "idgrupoaula": $('.group-select2').val(),
            "sql": "1",
            "fechaactiva": true
        }, function(data) {
            if (data.code === 200) {
                var html = '';
                $.each(data.data, function(i, item) {
                    html += '<option value="' + item.idpersona + '">' + item.nombre + ' ' + item.ape_paterno + ' ' + item.ape_materno + ' (' + item.email + ')' + '</option>';
                });
                $('.person-select2').html(html);
                $('.person-select2').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        }, "json");
    });

    $("#send").click(function() {
        Swal.showLoading();
        $.post(_sysUrlBase_ + "json/acad_mensaje/guardar", {
            "id": '',
            "asunto": $('.group-select2').val(),
            "asunto": $("#asunto").val(),
            "mensaje": tinymce.get('mytextarea').getContent(),
            'para': $(".person-select2").val()
        }, function(data) {
            if (data.code === 200) {
                iniciarMensajes();
                Swal.fire('Exito', data.msj, 'success');
                $("#btncerrar").click();
                tinymce.get('mytextarea').setContent("");
                $('.group-select2').val("<?php echo implode(",", $ids); ?>");
                $('.group-select2').trigger("change");
                $('#asunto').val("");
            }
        }, "json");
    });

    function eliminar(id) {
        $.post(_sysUrlBase_ + "json/acad_mensaje/setCampo", {
            "tabla": 'acad_mensaje',
            "id": id,
            "campo": 'estado',
            "valor": 0
        }, function(data) {
            if (data.code === 200) {
                iniciarMensajes();
            }
        }, "json");
    }
</script>