<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"eeeexzx-1";
if(!empty($this->datos)) $frm=$this->datos;
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php //echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/personal">&nbsp;<?php echo JrTexto::_('Personal'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
    <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>" tb="persona" accion="cambiarclave" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idpersona" id="idpersona" value="<?php echo $frm["idpersona"];?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="row">
          <div class="form-group col-md-6 col-sm-12">
              <label><?php echo JrTexto::_('Email');?></label>             
              <input type="email"  id="txtemail" name="email" required="required" class="form-control" value="<?php echo @$frm["email"];?>" placeholder="email@empresa.com" autofocus>              
          </div>
          <div class="form-group col-md-6 col-sm-12">
              <label><?php echo JrTexto::_('User');?></label>            
              <input type="text" id="txtusuario" name="usuario" required="required"  error="<?php echo JrTexto::_('User exist') ?>" class="form-control" value="<?php echo @$frm["usuario"];?>" placeholder="<?php echo JrTexto::_('user');?>">           
          </div>
          
          <div class="form-group col-md-6 col-sm-12">
              <label><?php echo JrTexto::_('Password');?></label>
              <input type="password"  autocomplete="false" id="txtclave" name="clave" Attention="<?php echo JrTexto::_('Attention');?>" error="<?php echo JrTexto::_('Password incorrect') ?>" required="required" class="form-control" placeholder="<?php echo JrTexto::_('Password');?>" >
          </div>
          <div class="form-group col-md-6 col-sm-12">
              <label > <?php echo JrTexto::_('Repit password');?> </label>           
              <input type="password" autocomplete="false"  id="txtclave2" name="reclave" required="required" class="form-control" placeholder="<?php echo JrTexto::_('repit password');?>" >
          </div>
        </div>
          <div class="text-center">
            <hr><br>
            <button id="btn-cambiarclave" type="button"  class="btn btn-success"  ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>           
            <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('personal'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/persona.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frmpersona($('#vent-<?php echo $idgui;?>'));
  })
</script>