<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<style type="text/css">
  .btn{font-size: 14px;}
  <style>
.topnom{
    padding:0.5ex 1ex;
    color:#fff;
}
.bloqmenu{
    position:absolute;
    top:1ex;
    right:1ex;
}
.item-curso{
    height: 230px;
    display: block;
    margin: 1em 1.5em;
    border-radius: 1ex;
    -webkit-box-shadow: 4px 0px 5px 3px rgba(1, 6, 18, 0.15);
    -moz-box-shadow: 4px 0px 5px 3px rgba(1, 6, 18, 0.2);
    background-color: 4px 0px 5px 3px rgba(1, 6, 18, 0.2);
    position:relative;
    cursor:pointer;
}
.item-curso.disabled{
  cursor: no-drop;
  background: #ccc;
}
.item-curso a{
    /*color: #fff;
    position: absolute;
    right:0px;*/
    display:none;

}
.item-curso:hover a{
    display:inline-block;
    position:relative;
    z-index:999;
    /*position: absolute;
    color:#ff9;*/
}
.cursoimage{
    position: absolute;   
    width: 100%;
    height: 100%;
    background-position: center;
    background-size: 75% 90%;
    background-repeat: no-repeat;
    top: -10px;
}
.nomcurso{
    position: absolute;
    bottom: -10px;
    width: 100%;
    background: #6799e62e;
    color: #b70707;
    padding: 0.25ex;
    border-bottom-left-radius: 0.8ex;
    border-bottom-right-radius: 0.8ex;
}
.btn-asignar{
    font-size: 1.2em;
  position: absolute;
  right: 0px;
  bottom: 0px;
  cursor: pointer;
}
.btn-asignar.active{
  color: #76f994;

}
.btn-asignar.inactive{
  color: red;
}
</style>
</style>
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;Atras</a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li-->
        <li class="active">&nbsp;<?php echo JrTexto::_("Course"); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="row">
    <div class="col">    
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <div class="cajaselect">
              <select name="idcategoria" id="idcategoria" class="form-control">
                <option value=""><?php echo JrTexto::_('All') ?></option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <!--label><?php //echo  ucfirst(JrTexto::_("text to search"))?></label-->
            <div class="input-group">
              <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
              <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 text-center">             
           <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlBase();?>proyecto/cursos/crear" data-titulo="<?php echo ucfirst(JrTexto::_('Add'))." ".JrTexto::_('Ugel') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
        </div>            
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <div class="panel-body" id="cursosAsignados"></div>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5d8e3cd74cf50='';
function refreshdatos5d8e3cd74cf50(){
    tabledatos5d8e3cd74cf50();
}
$(document).ready(function(){  
  var estados5d8e3cd74cf50={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5d8e3cd74cf50='<?php echo ucfirst(JrTexto::_("ugel"))." - ".JrTexto::_("edit"); ?>';
  var draw5d8e3cd74cf50=0;
  var _imgdefecto='';
  var ventana_5d8e3cd74cf50=$('#ventana_<?php echo $idgui; ?>');

  var _cargarcombos5d8e3cd74cf50=function(){
    var fd = new FormData();   
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/acad_categorias',
      callback:function(rs){
        $sel=ventana_5d8e3cd74cf50.find('select#idcategoria');
        idsel=$sel.attr('datavalue')||'';
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.idcategoria+'" '+(v.idcategoria==idsel?'selected="selected"':'')+ '>'+v.nombre+'</option>');
          })
          $sel.trigger('change');
      }})}
  _cargarcombos5d8e3cd74cf50();

  ventana_5d8e3cd74cf50.find('.textosearchlist').on('keydown',function(ev){
    if(ev.keyCode===13) refreshdatos5d8e3cd74cf50();
  }).blur(function(ev){ refreshdatos5d8e3cd74cf50()
  })
  ventana_5d8e3cd74cf50.on('change','select#idcategoria',function(ev){
    ventana_5d8e3cd74cf50.find('.btnbuscar').trigger('click');
  }).on('click','.btnbuscar',function(ev){
    refreshdatos5d8e3cd74cf50();
  }).on('click','.btn-eliminar',function(ev){
     var id=$(this).attr('idcurso');
     var el=$(this).attr('eliminar');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
        var data=new FormData()
        data.append('idcurso',id);
        data.append('eliminar',el);     
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/acad_curso/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5d8e3cd74cf50();
              }
            }
        });
      }
    })
  }).on('click','.btn-cambiarcampo',function(ev){
    var id=$(this).attr('idcurso');
    var data=new FormData()
    data.append('idcurso',id);
    data.append('campo',$(this).attr('campo'));
    data.append('valor',$(this).attr('valor'));     
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_curso/setcampo',
        callback:function(rs){
          if(rs.code==200){
            tabledatos5d8e3cd74cf50();
          }
        }
    }); 
  }).on('click','.btn-asignar',function(ev){
    var id=$(this).attr('idcurso');
    var tipo=$(this).attr('tipo');
    var sks=$(this).attr('sks');
    var data=new FormData()
    data.append('idcurso',id);
    data.append('tipo',tipo);
    data.append('sks',sks);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/proyecto_cursos/guardar',
        callback:function(rs){
          if(rs.code==200){
            tabledatos5d8e3cd74cf50();
          }
        }
    });   
  })


  tabledatos5d8e3cd74cf50=function(){
    var data=new FormData();
    var idcategoria=ventana_5d8e3cd74cf50.find('select#idcategoria').val()||'';
    data.append('idcategoria',idcategoria);
    data.append('edukt',true);
    data.append('texto',ventana_5d8e3cd74cf50.find('input.textosearchlist').val()||'');

    var infouser=__cambiarinfouser();
    if(infouser.tuser=='s') data.append('noidproyecto',idcategoria==''?'todos':'solo');  
    var user=JSON.parse(localStorage.getItem('usersesion'));
    if(user.tuser=='n')data.append('estado',1);    
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_curso',
        callback:function(rs){
          if(rs.code==200){
             dt=rs.data||[];
            if(dt.length>0){
                html='';
               
            $.each(dt,function(i,v){
              console.log(v);
                  //var asignado=v.asignado==true?'bg-success':'bg-warning';
                  var imgtemp=_sysUrlBase_+v.imagen||'';
                  var ultimostaticp=imgtemp.lastIndexOf('static');
                  var asignado=v.idproycurso||'';
                  if(ultimostaticp>0){
                    imgtemp=_sysUrlBase_+imgtemp.substr(ultimostaticp);
                  }else{
                    imgtemp+='static/media/nofoto.jpg';
                  }
                   html+='<div class="col-md-3 col-sm-4 col-xs-6 text-center  "><div class="">';
                   html+='<div class="item-curso '+(v.estado==0?'disabled':' hvr-grow ')+'" style="background-color:'+(v.estado==1?v.color:'')+'" idcurso="'+v.idcurso+'">'
                   html+='<div class="cursoimage" style="background-image:url('+imgtemp+');"></div>';

                   


                   html+='<a href="'+_sysUrlBase_+'cursos/formular/?idcurso='+v.idcurso+'" class="btn btn-sm btn-success edit"  title="<?php echo ucfirst(JrTexto::_('Formula')).' '.JrTexto::_('Course'); ?>"><i class="fa fa-calculator"></i></a>';
                   //html+='<a href="#" class="btn btn-sm btn-eliminar" idcurso="'+v.idcurso+'" alt="Eliminar curso"><i class="fa fa-trash"></i></a>';

                   if(v.idusuario==user.idpersona||user.tuser=='s'){

                  html+='<a href="'+_sysUrlBase_+'proyecto/cursos/crear/?idcurso='+v.idcurso+'" class="btn btn-sm btn-warning edit" idcurso="'+v.idcurso+'" title="<?php echo ucfirst(JrTexto::_('Edit')).' '.JrTexto::_('Course'); ?>"><i class="fa fa-pencil"></i></a>';
                    //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',

                    html+='<a href="#" class="btn btn-sm btn-info btn-cambiarcampo" idcurso="'+v.idcurso+'" campo="estado" valor="'+(v.estado==1?0:1)+'" title="'+estados5d8e3cd74cf50[v.estado==1?0:1]+'"><i class="fa fa-'+(v.estado==1?'check-':'')+'circle"></i></a>';
                    html+='<a href="#" class="btn btn-sm btn-danger btn-eliminar" eliminar="si" idcurso="'+v.idcurso+'" title="<?php echo ucfirst(JrTexto::_('Remove')).' '.JrTexto::_('Course'); ?>" ><i class="fa fa-trash"></i></a>';
                   }

                   html+='<h4 class="nomcurso">'+v.nombre+ '<span class="btn-asignar '+(asignado!=''?'active':'inactive')+'" idcurso="'+v.idcurso+'" tipo="'+v.tipo+'" sks="'+v.sks+'">'+(asignado!=''?'<i class="fa fa-check-circle"></i>':'<i class="fa fa-circle"></i>')+'</span> </h4></div>';
                   html+='</div></div>';
                })
                $('#cursosAsignados').html('<div class="row">'+html+'</html>');
              }
          }
        }
    });


    /*var formData = new FormData();       
    //formData.append('idempresa', idempresa);
    //  formData.append('sql2', true);
       sysajax({
        fromdata:formData,
        url:_sysUrlBase_+'/proyecto_cursos/buscarmiscursosjson',
        msjatencion:msjatencion,
        type:'json',
        callback:function(rs){
            dt=rs.data||[];
            if(dt.length>0){
                html='';
                _idproyecto=rs.idproyecto;
                $.each(dt,function(i,v){
                    var asignado=v.asignado==true?'bg-success':'bg-warning';
                    html+='<div class="col-md-4 col-md-6 col-sm-12 "><div class="img-thumbnail '+asignado+' btn-asignarcurso" data-idcurso="'+v.idcurso+'"  style="margin-top:2ex; width:100%; cursor:pointer;">';
                    html+='<div class="text-center" style="min-height:180px;"><img class="img img-responsive" src="'+_sysUrlBase_+(v.imagen||'/static/media/nofoto.jpg')+'" style=" max-height: 180px; display: inline;"></div>';
                    html+='<div class="titulo text-center" style="height: 50px; overflow: hidden; color:#000; flex-wrap: wrap-reverse;"><h3>'+v.nombre+'</h3></div>';
                    html+='<div class="subtitulo text-center" style="height: 45px; overflow: hidden; color: #555; flex-wrap: wrap-reverse;"><small>'+v.descripcion+'</small></div>';
                    html+='</div></div>';
                })
                $('#cursosAsignados').html(html);
            }
        }
      });*/
  }
  tabledatos5d8e3cd74cf50();
  $('#ventana_<?php echo $idgui; ?>').on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/ugel/setcampo','masvalores':{idugel:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idugel',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/ugel/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5d8e3cd74cf50.ajax.reload();
          }
        }
    });
  })
});
</script>