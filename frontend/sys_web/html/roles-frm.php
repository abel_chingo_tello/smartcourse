<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/roles">&nbsp;<?php echo JrTexto::_('Roles'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="idrol" id="idrol" value="<?php echo $this->pk;?>">
          <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtNombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Abreviatura');?> <span class="required"> * </span></label>
              <div class="">
                <input type="text"  id="txtAbreviatura" name="abreviatura" required="required" class="form-control" value="<?php echo @$frm["abreviatura"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
              <div class="">
               
                    <a  style="cursor:pointer;" href="javascript:;"  class="btn-activar <?php echo @$frm["estado"]==1?"active":"";?>"> <i class="fa fa<?php echo @$frm["estado"]=="1"?"-check":"";?>-circle fa-lg"></i> 
              <input type="hidden" name="estado" value="<?php echo @$frm["estado"]?1:0;?>"> 
              </a>

                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Imagen');?> <span class="required"> * </span></label>
              <div class="">
               
                            <img src="<?php echo URL_BASE.(!empty($frm["imagen"])?$frm["imagen"]:$_imgdefecto);?>" alt="imagenfile" file="imagen" class="subirfile img-thumbnail" style="max-height:200px; max-width:200px;">
            <input type="hidden" name="imagen" value="<?php echo !empty($frm["imagen"])?URL_BASE.$frm["imagen"]:"";?>">

                <!--div class="row">
                  <input type="hidden" name="" id="txt">
                  <input type="hidden" name="_old" value="<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Use of undefined constant e - assumed 'e' in F:\UwAmp\www\smart\smartmanager\frontend\sys_web\html\generador\frm.php on line <i>284</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0002</td><td bgcolor='#eeeeec' align='right'>191008</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='F:\UwAmp\www\smart\smartmanager\frontend\index.php' bgcolor='#eeeeec'>...\index.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0204</td><td bgcolor='#eeeeec' align='right'>1070064</td><td bgcolor='#eeeeec'>Sitio->enrutar(  )</td><td title='F:\UwAmp\www\smart\smartmanager\frontend\index.php' bgcolor='#eeeeec'>...\index.php<b>:</b>26</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0206</td><td bgcolor='#eeeeec' align='right'>1071728</td><td bgcolor='#eeeeec'>JrAplicacion->enrutar(  )</td><td title='F:\UwAmp\www\smart\smartmanager\sys_inc\cls.Sitio.php' bgcolor='#eeeeec'>...\cls.Sitio.php<b>:</b>43</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0216</td><td bgcolor='#eeeeec' align='right'>1168904</td><td bgcolor='#eeeeec'>WebGenerador->generarPagina(  )</td><td title='F:\UwAmp\www\smart\smartmanager\sys_lib\jrAdwen\cls.JrAplicacion.php' bgcolor='#eeeeec'>...\cls.JrAplicacion.php<b>:</b>99</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0226</td><td bgcolor='#eeeeec' align='right'>1170432</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='F:\UwAmp\www\smart\smartmanager\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>140</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0233</td><td bgcolor='#eeeeec' align='right'>1271736</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'F:\UwAmp\www\smart\smartmanager\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='F:\UwAmp\www\smart\smartmanager\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>35</td></tr>
</table></font>
';?>">
                  <div class="col-md-12">
                    <div class="img-container">
                      <img src="';?>" alt="img" data-ancho="100"  data-alto="100">
                    </div>
                  </div>
                </div-->
                                 
              </div>
            </div>

            
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveRoles" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('roles'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
        
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        var fele = document.getElementById("frm-<?php echo $idgui;?>");
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/roles/guardar',
              //showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                  redir(_sysUrlSitio_+'/roles');
                }
              }
          });
       }
  }).on('click','.btn-activar',function(){
    var i=$("i",this);  
    if(i.hasClass('fa-circle')){
      $(this).addClass('active');
      $('input',this).val(1);
      i.removeClass('fa-circle').addClass('fa-check-circle');
    }else{
      $(this).removeClass('active');
      $('input',this).val(0);
      i.addClass('fa-circle').removeClass('fa-check-circle');
    }     
  }).on('click','.subirfile',function(ev){
      __subirfile($(this));
  })

})
</script>

