<div class="container">
	<div class="row">
		<div class="col-md-12">						
			<a href="/" class="btn btn-primary"> <?=JrTexto::_("Back")?> </a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/historial" class="btn btn-primary" style="width: 100%; min-height: 120px; padding-top: 1.5em">
			<?=JrTexto::_("Session History")?> </a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/valoracion" class="btn btn-primary" style="width: 100%; min-height: 120px; padding-top: 1.5em">
			<?=JrTexto::_("Assessment")?> </a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/examenes" class="btn btn-primary" style="width: 100%; min-height: 120px; padding-top: 1.5em">
				<?=JrTexto::_("Tests")?> </a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/tareas" class="btn btn-primary" style="width: 100%; min-height: 120px; padding-top: 1.5em">
				<?=JrTexto::_("Tasks")?> </a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/bitacora_alumno_smartbook" class="btn btn-primary" style="width: 100%; min-height: 120px; padding-top: 1.5em">
				bitacora alumno smartbook</a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/bitacora_alumno_smartbook_se" class="btn btn-primary" style="width: 100%; min-height: 120px; padding-top: 1.5em">
				bitacora Alumno smartbook SE </a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/actividad_alumno" class="btn btn-primary" style="width: 100%; min-height: 120px; padding-top: 1.5em">
				<?=JrTexto::_("Student Activity")?> </a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/transferencias" class="btn btn-primary" style="width: 100%; min-height: 120px; padding-top: 1.5em">
				<?=JrTexto::_("By transfers")?> </a>			
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">						
			<a href="<?php echo URL_BASE; ?>corregir/eliminarinfouser" class="btn btn-warning" style="width: 100%; min-height: 120px; padding-top: 1.5em">
				<?=JrTexto::_("Delete data")?> </a>			
		</div>

	</div>
</div>