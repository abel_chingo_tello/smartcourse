<style type="text/css">
	#procesando{
		background: #000;
	    color: #fff;
	    padding: 1ex;
	    margin: 1ex;
	    min-height: 200px;
	    max-height: 450px;
	    overflow: scroll;
	}
</style>
<div class="col-md-12">
	<div class="row">
		<div class="col-md-12">						
			<a href="/" class="btn btn-primary"> <?=JrTexto::_("Back")?> </a>			
		</div>
		<div id="procesando" class="col-md-12" style="">
			<div id="inicial" class="text-center"><?=JrTexto::_("Here you will show what is being processed")?>;</div>
		</div>
		<div class="col-md-12 text-center"><?=JrTexto::_("Total corrected")?> 
			<span id="totalcorregidos" style="background: #000;  padding: 1ex; color: #fff;">0</span>
		</div>
		<?php if(!empty($this->enviaridalumno)){?>
			<div class="col-md-12 text-center">	
		<br>					
			<label><?=JrTexto::_("Enter the Student ID")?> </label>
			<input type="text" name="idalumno" id="idalumno">
		</div>
		<?php } ?>
		<div class="col-md-12 text-center">	
		<br>					
			<a href="#" desde="0" class="procesar btn btn-primary"> <?=JrTexto::_("Process and correct data")?></a>
			<!--a href="#" class="procesar btn btn-primary"> Procesar donde me quede </a-->
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(ev){
		var desde=localStorage.getItem('desde<?php echo $this->tabla; ?>')||0;
		var procesando=false;
		$('.procesar').click(function(ev){
			if(procesando==true) return;
			$btn=$(this);
			$btn.addClass('active');
			$btn.siblings('a').removeClass('active');
			procesando=true;
			var divid="div"+Date.now();
			$('#procesando').children('#inicial').remove();
			$('#procesando').append('<div id="'+divid+'"><?=JrTexto::_("Processing")?> <i class="fa fa-spinner fa-spin"></i></div>');
			var _desde=$(this).attr('desde')||desde;
			var data = new FormData()
			 data.append('desde',_desde);
			 data.append('tabla','<?php echo $this->tabla; ?>');  
			 <?php if(!empty($this->enviaridalumno)){?>
			 data.append('idalumno',$('#idalumno').val());  
			 <?php } ?>
			__sysAyax({
	          fromdata: data,
	          url: _sysUrlBase_ + 'json/corregir',
	          callback: function(rs) {
	          	procesando=false;
	          	let sql1=rs.sql1+"<br>";  
	          	let total=parseInt($('#totalcorregidos').text())+rs.ncorregidos;
	          	let html='---------------------------------------------------------------------------------------------<br>';
	          	html+='<?=JrTexto::_("Records successfully processed from")?> '+_desde+' hasta '+rs.desde+' : <?=JrTexto::_("total corrected")?> : '+rs.ncorregidos;
	          	html+='<br>==============================================================================================<br>';
	            $('#'+divid).html(html+sql1);
	            $('#procesando').scrollTop($('#procesando')[0].scrollHeight);
	            localStorage.setItem('desde<?php echo $this->tabla; ?>',rs.desde);
	            $btn.attr('desde',rs.desde);
	            $('#totalcorregidos').text(total);
	            if((rs.termino||0)==0) volveraprocesar(0);
	          }
	        });
		})
		var volveraprocesar=function(en){
			setTimeout(function(ev){
				procesando=false;
				$('.procesar.active').trigger('click');
			},3000)
		}
	})
</script>
