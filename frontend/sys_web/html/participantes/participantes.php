<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
    <div class="paris-navbar-container"></div>
    <script>
        addItemNavBar({
            text: '<?php echo JrTexto::_("participants"); ?>'
        });
    </script>
<?php } ?>

<div class="container my-font-all">

    <div class="row my-x-center">
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for=""><?php echo JrTexto::_("smartcourse"); ?></label>
                <select class="form-control" name="" id="selectCursos">
                </select>
            </div>
        </div>
    </div>
    <div class="row col-sm-12 my-x-center">
        <div class="my-card my-shadow">
            <table id="my-table" class="mdl-data-table my-font" style="width:100%">
                <thead id="tb-head">
                    <tr>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="tbody" class="">
                    <tr class="odd">
                        <td valign="top" colspan="4" class="dataTables_empty"><?php echo JrTexto::_("There are no students in this course yet"); ?>!</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12 my-mg-top my-mg-bottom">
            <button id="btn-save" class="btn btn-success my-center ">
                <?php echo JrTexto::_("save"); ?>
            </button>
        </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>