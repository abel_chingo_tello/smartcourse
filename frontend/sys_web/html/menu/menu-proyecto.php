<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       12-11-2020 
 * @copyright   Copyright (C) 12-11-2020. Todos los derechos reservados.
 */
?>
<style type="text/css">
  li.escontenedor, li.escontenedor >div >div >span.dd-handle{
    background: #ccc;
  }
</style>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tuser"]; ?>" idempresa="<?php echo $this->user["idempresa"]; ?>" idproyecto="<?php echo $this->idproyecto ?>">
    <div class="paris-navbar-container"></div>
    <script>
      addItemNavBar({
        class:'breadcrumbactive',
        text: '<?php echo JrTexto::_("company menu"); ?>'
      });     
    </script>
  <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
          <div class="grid-body vistaordenar">
            <form id="frmbuscar" name="frmbuscar">              
              <div class="row"> 
                <div class="col-md-4 col-sm-6 col-xs-12 col-12">
                  <div class="form-group">
                    <label class="form-label"><?php echo JrTexto::_('role') ?></label>                      
                    <select name="busidrol" id="busidrol" class="select2 form-control"  >
                      <?php if(!empty($this->fkIdrol))
                        foreach($this->fkIdrol as $k=>$v){ ?>         
                        <option value="<?php echo $v["idrol"] ?>"><?php echo ucfirst($v["rol"]); ?></option>
                      <?php } ?>  
                    </select>                        
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 col-12 text-right">
                  <button class="btn btn-warning btnAgregar"> <?php echo JrTexto::_('add') ?> <i class="fa fa-plus"></i> </button>
                </div>
              </div>
            </form>
          </div>
          <div class="grid-body" id="aquiordenar">
            <h3><?php echo JrTexto::_('here add your modules') ?>/<?php echo JrTexto::_('menu') ?></h3>
          </div>
           <div class="grid-body" id="aquiformulario" style="display: none;">
            <form name="frmmenu_proyecto" id="frmmenu_proyecto">
              <input type="hidden" name="idproyecto" id="idproyecto">
              <input type="hidden" name="idrol" id="idrol">
              <input type="hidden" name="idmenuproyecto" id="idmenuproyecto">
              <input type="hidden" name="idpadre" id="idpadre">
              <input type="hidden" name="orden" id="orden" value="1">
              <input type="hidden" name="insertar" id="insertar" value="1">
              <input type="hidden" name="modificar" id="modificar" value="1">
              <input type="hidden" name="eliminar" id="eliminar" value="1">
              <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 col-12">
                  <div class="form-group">
                    <label class="form-label"><?php echo JrTexto::_('container') ?>/<?php echo JrTexto::_('module') ?>/<?php echo JrTexto::_('menu') ?> <span class="help"></span></label>
                    <select name="idmenu" id="idmenu" class="select2 form-control"  >
                      <option value="">.:. <?php echo JrTexto::_('container') ?>.:. </option>
                      <?php if(!empty($this->fkIdmenu))
                        foreach($this->fkIdmenu as $k=>$v){ ?>         
                        <option value="<?php echo $v["idmenu"] ?>"><?php echo $v["nombre"] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 col-12 sinmenu">
                  <div class="form-group">
                    <label class="form-label"><?php echo JrTexto::_('container name') ?> <span class="help"></span></label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <input type="text" name="nomtemporal" id="nomtemporal" class="form-control">
                      <span class="error"></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 col-12 sinmenu">
                  <div class="form-group">
                    <label class="form-label"><?php echo JrTexto::_('icon') ?> <span class="help"></span></label>
                    <select name="icono" id="icono" class="select2icon form-control"  >
                      <option value="fa-address-book">fa-address-book</option>
                      <option value="fa-address-book-o">fa-address-book-o</option>
                      <option value="fa-address-card">fa-address-card</option>
                      <option value="fa-address-card-o">fa-address-card-o</option>
                      <option value="fa-adjust">fa-adjust</option>
                      <option value="fa-american-sign-language-interpreting">fa-american-sign-language-interpreting</option>
                      <option value="fa-anchor">fa-anchor</option>
                      <option value="fa-archive">fa-archive</option>
                      <option value="fa-area-chart">fa-area-chart</option>
                      <option value="fa-arrows">fa-arrows</option>
                      <option value="fa-arrows-h">fa-arrows-h</option>
                      <option value="fa-arrows-v">fa-arrows-v</option>
                      <option value="fa-asl-interpreting">fa-asl-interpreting</option>
                      <option value="fa-assistive-listening-systems">fa-assistive-listening-systems</option>
                      <option value="fa-asterisk">fa-asterisk</option>
                      <option value="fa-at">fa-at</option>
                      <option value="fa-audio-description">fa-audio-description</option>
                      <option value="fa-automobile">fa-automobile</option>
                      <option value="fa-balance-scale">fa-balance-scale</option>
                      <option value="fa-ban">fa-ban</option>
                      <option value="fa-bank">fa-bank</option>
                      <option value="fa-bar-chart">fa-bar-chart</option>
                      <option value="fa-bar-chart-o">fa-bar-chart-o</option>
                      <option value="fa-barcode">fa-barcode</option>
                      <option value="fa-bars">fa-bars</option>
                      <option value="fa-bath">fa-bath</option>
                      <option value="fa-bathtub">fa-bathtub</option>
                      <option value="fa-battery">fa-battery</option>
                      <option value="fa-battery-0">fa-battery-0</option>
                      <option value="fa-battery-1">fa-battery-1</option>
                      <option value="fa-battery-2">fa-battery-2</option>
                      <option value="fa-battery-3">fa-battery-3</option>
                      <option value="fa-battery-4">fa-battery-4</option>
                      <option value="fa-battery-empty">fa-battery-empty</option>
                      <option value="fa-battery-full">fa-battery-full</option>
                      <option value="fa-battery-half">fa-battery-half</option>
                      <option value="fa-battery-quarter">fa-battery-quarter</option>
                      <option value="fa-battery-three-quarters">fa-battery-three-quarters</option>
                      <option value="fa-bed">fa-bed</option>
                      <option value="fa-beer">fa-beer</option>
                      <option value="fa-bell">fa-bell</option>
                      <option value="fa-bell-o">fa-bell-o</option>
                      <option value="fa-bell-slash">fa-bell-slash</option>
                      <option value="fa-bell-slash-o">fa-bell-slash-o</option>
                      <option value="fa-bicycle">fa-bicycle</option>
                      <option value="fa-binoculars">fa-binoculars</option>
                      <option value="fa-birthday-cake">fa-birthday-cake</option>
                      <option value="fa-blind">fa-blind</option>
                      <option value="fa-bluetooth">fa-bluetooth</option>
                      <option value="fa-bluetooth-b">fa-bluetooth-b</option>
                      <option value="fa-bolt">fa-bolt</option>
                      <option value="fa-bomb">fa-bomb</option>
                      <option value="fa-book">fa-book</option>
                      <option value="fa-bookmark">fa-bookmark</option>
                      <option value="fa-bookmark-o">fa-bookmark-o</option>
                      <option value="fa-braille">fa-braille</option>
                      <option value="fa-briefcase">fa-briefcase</option>
                      <option value="fa-bug">fa-bug</option>
                      <option value="fa-building">fa-building</option>
                      <option value="fa-building-o">fa-building-o</option>
                      <option value="fa-bullhorn">fa-bullhorn</option>
                      <option value="fa-bullseye">fa-bullseye</option>
                      <option value="fa-bus">fa-bus</option>
                      <option value="fa-cab">fa-cab</option>
                      <option value="fa-calculator">fa-calculator</option>
                      <option value="fa-calendar">fa-calendar</option>
                      <option value="fa-calendar-check-o">fa-calendar-check-o</option>
                      <option value="fa-calendar-minus-o">fa-calendar-minus-o</option>
                      <option value="fa-calendar-o">fa-calendar-o</option>
                      <option value="fa-calendar-plus-o">fa-calendar-plus-o</option>
                      <option value="fa-calendar-times-o">fa-calendar-times-o</option>
                      <option value="fa-camera">fa-camera</option>
                      <option value="fa-camera-retro">fa-camera-retro</option>
                      <option value="fa-car">fa-car</option>
                      <option value="fa-caret-square-o-down">fa-caret-square-o-down</option>
                      <option value="fa-caret-square-o-left">fa-caret-square-o-left</option>
                      <option value="fa-caret-square-o-right">fa-caret-square-o-right</option>
                      <option value="fa-caret-square-o-up">fa-caret-square-o-up</option>
                      <option value="fa-cart-arrow-down">fa-cart-arrow-down</option>
                      <option value="fa-cart-plus">fa-cart-plus</option>
                      <option value="fa-cc">fa-cc</option>
                      <option value="fa-certificate">fa-certificate</option>
                      <option value="fa-check">fa-check</option>
                      <option value="fa-check-circle">fa-check-circle</option>
                      <option value="fa-check-circle-o">fa-check-circle-o</option>
                      <option value="fa-check-square">fa-check-square</option>
                      <option value="fa-check-square-o">fa-check-square-o</option>
                      <option value="fa-child">fa-child</option>
                      <option value="fa-circle">fa-circle</option>
                      <option value="fa-circle-o">fa-circle-o</option>
                      <option value="fa-circle-o-notch">fa-circle-o-notch</option>
                      <option value="fa-circle-thin">fa-circle-thin</option>
                      <option value="fa-clock-o">fa-clock-o</option>
                      <option value="fa-clone">fa-clone</option>
                      <option value="fa-close">fa-close</option>
                      <option value="fa-cloud">fa-cloud</option>
                      <option value="fa-cloud-download">fa-cloud-download</option>
                      <option value="fa-cloud-upload">fa-cloud-upload</option>
                      <option value="fa-code">fa-code</option>
                      <option value="fa-code-fork">fa-code-fork</option>
                      <option value="fa-coffee">fa-coffee</option>
                      <option value="fa-cog">fa-cog</option>
                      <option value="fa-cogs">fa-cogs</option>
                      <option value="fa-comment">fa-comment</option>
                      <option value="fa-comment-o">fa-comment-o</option>
                      <option value="fa-commenting">fa-commenting</option>
                      <option value="fa-commenting-o">fa-commenting-o</option>
                      <option value="fa-comments">fa-comments</option>
                      <option value="fa-comments-o">fa-comments-o</option>
                      <option value="fa-compass">fa-compass</option>
                      <option value="fa-copyright">fa-copyright</option>
                      <option value="fa-creative-commons">fa-creative-commons</option>
                      <option value="fa-credit-card">fa-credit-card</option>
                      <option value="fa-credit-card-alt">fa-credit-card-alt</option>
                      <option value="fa-crop">fa-crop</option>
                      <option value="fa-crosshairs">fa-crosshairs</option>
                      <option value="fa-cube">fa-cube</option>
                      <option value="fa-cubes">fa-cubes</option>
                      <option value="fa-cutlery">fa-cutlery</option>
                      <option value="fa-dashboard">fa-dashboard</option>
                      <option value="fa-database">fa-database</option>
                      <option value="fa-deaf">fa-deaf</option>
                      <option value="fa-deafness">fa-deafness</option>
                      <option value="fa-desktop">fa-desktop</option>
                      <option value="fa-diamond">fa-diamond</option>
                      <option value="fa-dot-circle-o">fa-dot-circle-o</option>
                      <option value="fa-download">fa-download</option>
                      <option value="fa-drivers-license">fa-drivers-license</option>
                      <option value="fa-drivers-license-o">fa-drivers-license-o</option>
                      <option value="fa-edit">fa-edit</option>
                      <option value="fa-ellipsis-h">fa-ellipsis-h</option>
                      <option value="fa-ellipsis-v">fa-ellipsis-v</option>
                      <option value="fa-envelope">fa-envelope</option>
                      <option value="fa-envelope-o">fa-envelope-o</option>
                      <option value="fa-envelope-open">fa-envelope-open</option>
                      <option value="fa-envelope-open-o">fa-envelope-open-o</option>
                      <option value="fa-envelope-square">fa-envelope-square</option>
                      <option value="fa-eraser">fa-eraser</option>
                      <option value="fa-exchange">fa-exchange</option>
                      <option value="fa-exclamation">fa-exclamation</option>
                      <option value="fa-exclamation-circle">fa-exclamation-circle</option>
                      <option value="fa-exclamation-triangle">fa-exclamation-triangle</option>
                      <option value="fa-external-link">fa-external-link</option>
                      <option value="fa-external-link-square">fa-external-link-square</option>
                      <option value="fa-eye">fa-eye</option>
                      <option value="fa-eye-slash">fa-eye-slash</option>
                      <option value="fa-eyedropper">fa-eyedropper</option>
                      <option value="fa-fax">fa-fax</option>
                      <option value="fa-feed">fa-feed</option>
                      <option value="fa-female">fa-female</option>
                      <option value="fa-fighter-jet">fa-fighter-jet</option>
                      <option value="fa-file-archive-o">fa-file-archive-o</option>
                      <option value="fa-file-audio-o">fa-file-audio-o</option>
                      <option value="fa-file-code-o">fa-file-code-o</option>
                      <option value="fa-file-excel-o">fa-file-excel-o</option>
                      <option value="fa-file-image-o">fa-file-image-o</option>
                      <option value="fa-file-movie-o">fa-file-movie-o</option>
                      <option value="fa-file-pdf-o">fa-file-pdf-o</option>
                      <option value="fa-file-photo-o">fa-file-photo-o</option>
                      <option value="fa-file-picture-o">fa-file-picture-o</option>
                      <option value="fa-file-powerpoint-o">fa-file-powerpoint-o</option>
                      <option value="fa-file-sound-o">fa-file-sound-o</option>
                      <option value="fa-file-video-o">fa-file-video-o</option>
                      <option value="fa-file-word-o">fa-file-word-o</option>
                      <option value="fa-file-zip-o">fa-file-zip-o</option>
                      <option value="fa-film">fa-film</option>
                      <option value="fa-filter">fa-filter</option>
                      <option value="fa-fire">fa-fire</option>
                      <option value="fa-fire-extinguisher">fa-fire-extinguisher</option>
                      <option value="fa-flag">fa-flag</option>
                      <option value="fa-flag-checkered">fa-flag-checkered</option>
                      <option value="fa-flag-o">fa-flag-o</option>
                      <option value="fa-flash">fa-flash</option>
                      <option value="fa-flask">fa-flask</option>
                      <option value="fa-folder">fa-folder</option>
                      <option value="fa-folder-o">fa-folder-o</option>
                      <option value="fa-folder-open">fa-folder-open</option>
                      <option value="fa-folder-open-o">fa-folder-open-o</option>
                      <option value="fa-frown-o">fa-frown-o</option>
                      <option value="fa-futbol-o">fa-futbol-o</option>
                      <option value="fa-gamepad">fa-gamepad</option>
                      <option value="fa-gavel">fa-gavel</option>
                      <option value="fa-gear">fa-gear</option>
                      <option value="fa-gears">fa-gears</option>
                      <option value="fa-gift">fa-gift</option>
                      <option value="fa-glass">fa-glass</option>
                      <option value="fa-globe">fa-globe</option>
                      <option value="fa-graduation-cap">fa-graduation-cap</option>
                      <option value="fa-group">fa-group</option>
                      <option value="fa-hand-grab-o">fa-hand-grab-o</option>
                      <option value="fa-hand-lizard-o">fa-hand-lizard-o</option>
                      <option value="fa-hand-paper-o">fa-hand-paper-o</option>
                      <option value="fa-hand-peace-o">fa-hand-peace-o</option>
                      <option value="fa-hand-pointer-o">fa-hand-pointer-o</option>
                      <option value="fa-hand-rock-o">fa-hand-rock-o</option>
                      <option value="fa-hand-scissors-o">fa-hand-scissors-o</option>
                      <option value="fa-hand-spock-o">fa-hand-spock-o</option>
                      <option value="fa-hand-stop-o">fa-hand-stop-o</option>
                      <option value="fa-handshake-o">fa-handshake-o</option>
                      <option value="fa-hard-of-hearing">fa-hard-of-hearing</option>
                      <option value="fa-hashtag">fa-hashtag</option>
                      <option value="fa-hdd-o">fa-hdd-o</option>
                      <option value="fa-headphones">fa-headphones</option>
                      <option value="fa-heart">fa-heart</option>
                      <option value="fa-heart-o">fa-heart-o</option>
                      <option value="fa-heartbeat">fa-heartbeat</option>
                      <option value="fa-history">fa-history</option>
                      <option value="fa-home">fa-home</option>
                      <option value="fa-hotel">fa-hotel</option>
                      <option value="fa-hourglass">fa-hourglass</option>
                      <option value="fa-hourglass-1">fa-hourglass-1</option>
                      <option value="fa-hourglass-2">fa-hourglass-2</option>
                      <option value="fa-hourglass-3">fa-hourglass-3</option>
                      <option value="fa-hourglass-end">fa-hourglass-end</option>
                      <option value="fa-hourglass-half">fa-hourglass-half</option>
                      <option value="fa-hourglass-o">fa-hourglass-o</option>
                      <option value="fa-hourglass-start">fa-hourglass-start</option>
                      <option value="fa-i-cursor">fa-i-cursor</option>
                      <option value="fa-id-badge">fa-id-badge</option>
                      <option value="fa-id-card">fa-id-card</option>
                      <option value="fa-id-card-o">fa-id-card-o</option>
                      <option value="fa-image">fa-image</option>
                      <option value="fa-inbox">fa-inbox</option>
                      <option value="fa-industry">fa-industry</option>
                      <option value="fa-info">fa-info</option>
                      <option value="fa-info-circle">fa-info-circle</option>
                      <option value="fa-institution">fa-institution</option>
                      <option value="fa-key">fa-key</option>
                      <option value="fa-keyboard-o">fa-keyboard-o</option>
                      <option value="fa-language">fa-language</option>
                      <option value="fa-laptop">fa-laptop</option>
                      <option value="fa-leaf">fa-leaf</option>
                      <option value="fa-legal">fa-legal</option>
                      <option value="fa-lemon-o">fa-lemon-o</option>
                      <option value="fa-level-down">fa-level-down</option>
                      <option value="fa-level-up">fa-level-up</option>
                      <option value="fa-life-bouy">fa-life-bouy</option>
                      <option value="fa-life-buoy">fa-life-buoy</option>
                      <option value="fa-life-ring">fa-life-ring</option>
                      <option value="fa-life-saver">fa-life-saver</option>
                      <option value="fa-lightbulb-o">fa-lightbulb-o</option>
                      <option value="fa-line-chart">fa-line-chart</option>
                      <option value="fa-location-arrow">fa-location-arrow</option>
                      <option value="fa-lock">fa-lock</option>
                      <option value="fa-low-vision">fa-low-vision</option>
                      <option value="fa-magic">fa-magic</option>
                      <option value="fa-magnet">fa-magnet</option>
                      <option value="fa-mail-forward">fa-mail-forward</option>
                      <option value="fa-mail-reply">fa-mail-reply</option>
                      <option value="fa-mail-reply-all">fa-mail-reply-all</option>
                      <option value="fa-male">fa-male</option>
                      <option value="fa-map">fa-map</option>
                      <option value="fa-map-marker">fa-map-marker</option>
                      <option value="fa-map-o">fa-map-o</option>
                      <option value="fa-map-pin">fa-map-pin</option>
                      <option value="fa-map-signs">fa-map-signs</option>
                      <option value="fa-meh-o">fa-meh-o</option>
                      <option value="fa-microchip">fa-microchip</option>
                      <option value="fa-microphone">fa-microphone</option>
                      <option value="fa-microphone-slash">fa-microphone-slash</option>
                      <option value="fa-minus">fa-minus</option>
                      <option value="fa-minus-circle">fa-minus-circle</option>
                      <option value="fa-minus-square">fa-minus-square</option>
                      <option value="fa-minus-square-o">fa-minus-square-o</option>
                      <option value="fa-mobile">fa-mobile</option>
                      <option value="fa-mobile-phone">fa-mobile-phone</option>
                      <option value="fa-money">fa-money</option>
                      <option value="fa-moon-o">fa-moon-o</option>
                      <option value="fa-mortar-board">fa-mortar-board</option>
                      <option value="fa-motorcycle">fa-motorcycle</option>
                      <option value="fa-mouse-pointer">fa-mouse-pointer</option>
                      <option value="fa-music">fa-music</option>
                      <option value="fa-navicon">fa-navicon</option>
                      <option value="fa-newspaper-o">fa-newspaper-o</option>
                      <option value="fa-object-group">fa-object-group</option>
                      <option value="fa-object-ungroup">fa-object-ungroup</option>
                      <option value="fa-paint-brush">fa-paint-brush</option>
                      <option value="fa-paper-plane">fa-paper-plane</option>
                      <option value="fa-paper-plane-o">fa-paper-plane-o</option>
                      <option value="fa-paw">fa-paw</option>
                      <option value="fa-pencil">fa-pencil</option>
                      <option value="fa-pencil-square">fa-pencil-square</option>
                      <option value="fa-pencil-square-o">fa-pencil-square-o</option>
                      <option value="fa-percent">fa-percent</option>
                      <option value="fa-phone">fa-phone</option>
                      <option value="fa-phone-square">fa-phone-square</option>
                      <option value="fa-photo">fa-photo</option>
                      <option value="fa-picture-o">fa-picture-o</option>
                      <option value="fa-pie-chart">fa-pie-chart</option>
                      <option value="fa-plane">fa-plane</option>
                      <option value="fa-plug">fa-plug</option>
                      <option value="fa-plus">fa-plus</option>
                      <option value="fa-plus-circle">fa-plus-circle</option>
                      <option value="fa-plus-square">fa-plus-square</option>
                      <option value="fa-plus-square-o">fa-plus-square-o</option>
                      <option value="fa-podcast">fa-podcast</option>
                      <option value="fa-power-off">fa-power-off</option>
                      <option value="fa-print">fa-print</option>
                      <option value="fa-puzzle-piece">fa-puzzle-piece</option>
                      <option value="fa-qrcode">fa-qrcode</option>
                      <option value="fa-question">fa-question</option>
                      <option value="fa-question-circle">fa-question-circle</option>
                      <option value="fa-question-circle-o">fa-question-circle-o</option>
                      <option value="fa-quote-left">fa-quote-left</option>
                      <option value="fa-quote-right">fa-quote-right</option>
                      <option value="fa-random">fa-random</option>
                      <option value="fa-recycle">fa-recycle</option>
                      <option value="fa-refresh">fa-refresh</option>
                      <option value="fa-registered">fa-registered</option>
                      <option value="fa-remove">fa-remove</option>
                      <option value="fa-reorder">fa-reorder</option>
                      <option value="fa-reply">fa-reply</option>
                      <option value="fa-reply-all">fa-reply-all</option>
                      <option value="fa-retweet">fa-retweet</option>
                      <option value="fa-road">fa-road</option>
                      <option value="fa-rocket">fa-rocket</option>
                      <option value="fa-rss">fa-rss</option>
                      <option value="fa-rss-square">fa-rss-square</option>
                      <option value="fa-s15">fa-s15</option>
                      <option value="fa-search">fa-search</option>
                      <option value="fa-search-minus">fa-search-minus</option>
                      <option value="fa-search-plus">fa-search-plus</option>
                      <option value="fa-send">fa-send</option>
                      <option value="fa-send-o">fa-send-o</option>
                      <option value="fa-server">fa-server</option>
                      <option value="fa-share">fa-share</option>
                      <option value="fa-share-alt">fa-share-alt</option>
                      <option value="fa-share-alt-square">fa-share-alt-square</option>
                      <option value="fa-share-square">fa-share-square</option>
                      <option value="fa-share-square-o">fa-share-square-o</option>
                      <option value="fa-shield">fa-shield</option>
                      <option value="fa-ship">fa-ship</option>
                      <option value="fa-shopping-bag">fa-shopping-bag</option>
                      <option value="fa-shopping-basket">fa-shopping-basket</option>
                      <option value="fa-shopping-cart">fa-shopping-cart</option>
                      <option value="fa-shower">fa-shower</option>
                      <option value="fa-sign-in">fa-sign-in</option>
                      <option value="fa-sign-language">fa-sign-language</option>
                      <option value="fa-sign-out">fa-sign-out</option>
                      <option value="fa-signal">fa-signal</option>
                      <option value="fa-signing">fa-signing</option>
                      <option value="fa-sitemap">fa-sitemap</option>
                      <option value="fa-sliders">fa-sliders</option>
                      <option value="fa-smile-o">fa-smile-o</option>
                      <option value="fa-snowflake-o">fa-snowflake-o</option>
                      <option value="fa-soccer-ball-o">fa-soccer-ball-o</option>
                      <option value="fa-sort">fa-sort</option>
                      <option value="fa-sort-alpha-asc">fa-sort-alpha-asc</option>
                      <option value="fa-sort-alpha-desc">fa-sort-alpha-desc</option>
                      <option value="fa-sort-amount-asc">fa-sort-amount-asc</option>
                      <option value="fa-sort-amount-desc">fa-sort-amount-desc</option>
                      <option value="fa-sort-asc">fa-sort-asc</option>
                      <option value="fa-sort-desc">fa-sort-desc</option>
                      <option value="fa-sort-down">fa-sort-down</option>
                      <option value="fa-sort-numeric-asc">fa-sort-numeric-asc</option>
                      <option value="fa-sort-numeric-desc">fa-sort-numeric-desc</option>
                      <option value="fa-sort-up">fa-sort-up</option>
                      <option value="fa-space-shuttle">fa-space-shuttle</option>
                      <option value="fa-spinner">fa-spinner</option>
                      <option value="fa-spoon">fa-spoon</option>
                      <option value="fa-square">fa-square</option>
                      <option value="fa-square-o">fa-square-o</option>
                      <option value="fa-star">fa-star</option>
                      <option value="fa-star-half">fa-star-half</option>
                      <option value="fa-star-half-empty">fa-star-half-empty</option>
                      <option value="fa-star-half-full">fa-star-half-full</option>
                      <option value="fa-star-half-o">fa-star-half-o</option>
                      <option value="fa-star-o">fa-star-o</option>
                      <option value="fa-sticky-note">fa-sticky-note</option>
                      <option value="fa-sticky-note-o">fa-sticky-note-o</option>
                      <option value="fa-street-view">fa-street-view</option>
                      <option value="fa-suitcase">fa-suitcase</option>
                      <option value="fa-sun-o">fa-sun-o</option>
                      <option value="fa-support">fa-support</option>
                      <option value="fa-tablet">fa-tablet</option>
                      <option value="fa-tachometer">fa-tachometer</option>
                      <option value="fa-tag">fa-tag</option>
                      <option value="fa-tags">fa-tags</option>
                      <option value="fa-tasks">fa-tasks</option>
                      <option value="fa-taxi">fa-taxi</option>
                      <option value="fa-television">fa-television</option>
                      <option value="fa-terminal">fa-terminal</option>
                      <option value="fa-thermometer">fa-thermometer</option>
                      <option value="fa-thermometer-0">fa-thermometer-0</option>
                      <option value="fa-thermometer-1">fa-thermometer-1</option>
                      <option value="fa-thermometer-2">fa-thermometer-2</option>
                      <option value="fa-thermometer-3">fa-thermometer-3</option>
                      <option value="fa-thermometer-4">fa-thermometer-4</option>
                      <option value="fa-thermometer-empty">fa-thermometer-empty</option>
                      <option value="fa-thermometer-full">fa-thermometer-full</option>
                      <option value="fa-thermometer-half">fa-thermometer-half</option>
                      <option value="fa-thermometer-quarter">fa-thermometer-quarter</option>
                      <option value="fa-thermometer-three-quarters">fa-thermometer-three-quarters</option>
                      <option value="fa-thumb-tack">fa-thumb-tack</option>
                      <option value="fa-thumbs-down">fa-thumbs-down</option>
                      <option value="fa-thumbs-o-down">fa-thumbs-o-down</option>
                      <option value="fa-thumbs-o-up">fa-thumbs-o-up</option>
                      <option value="fa-thumbs-up">fa-thumbs-up</option>
                      <option value="fa-ticket">fa-ticket</option>
                      <option value="fa-times">fa-times</option>
                      <option value="fa-times-circle">fa-times-circle</option>
                      <option value="fa-times-circle-o">fa-times-circle-o</option>
                      <option value="fa-times-rectangle">fa-times-rectangle</option>
                      <option value="fa-times-rectangle-o">fa-times-rectangle-o</option>
                      <option value="fa-tint">fa-tint</option>
                      <option value="fa-toggle-down">fa-toggle-down</option>
                      <option value="fa-toggle-left">fa-toggle-left</option>
                      <option value="fa-toggle-off">fa-toggle-off</option>
                      <option value="fa-toggle-on">fa-toggle-on</option>
                      <option value="fa-toggle-right">fa-toggle-right</option>
                      <option value="fa-toggle-up">fa-toggle-up</option>
                      <option value="fa-trademark">fa-trademark</option>
                      <option value="fa-trash">fa-trash</option>
                      <option value="fa-trash-o">fa-trash-o</option>
                      <option value="fa-tree">fa-tree</option>
                      <option value="fa-trophy">fa-trophy</option>
                      <option value="fa-truck">fa-truck</option>
                      <option value="fa-tty">fa-tty</option>
                      <option value="fa-tv">fa-tv</option>
                      <option value="fa-umbrella">fa-umbrella</option>
                      <option value="fa-universal-access">fa-universal-access</option>
                      <option value="fa-university">fa-university</option>
                      <option value="fa-unlock">fa-unlock</option>
                      <option value="fa-unlock-alt">fa-unlock-alt</option>
                      <option value="fa-unsorted">fa-unsorted</option>
                      <option value="fa-upload">fa-upload</option>
                      <option value="fa-user">fa-user</option>
                      <option value="fa-user-circle">fa-user-circle</option>
                      <option value="fa-user-circle-o">fa-user-circle-o</option>
                      <option value="fa-user-o">fa-user-o</option>
                      <option value="fa-user-plus">fa-user-plus</option>
                      <option value="fa-user-secret">fa-user-secret</option>
                      <option value="fa-user-times">fa-user-times</option>
                      <option value="fa-users">fa-users</option>
                      <option value="fa-vcard">fa-vcard</option>
                      <option value="fa-vcard-o">fa-vcard-o</option>
                      <option value="fa-video-camera">fa-video-camera</option>
                      <option value="fa-volume-control-phone">fa-volume-control-phone</option>
                      <option value="fa-volume-down">fa-volume-down</option>
                      <option value="fa-volume-off">fa-volume-off</option>
                      <option value="fa-volume-up">fa-volume-up</option>
                      <option value="fa-warning">fa-warning</option>
                      <option value="fa-wheelchair">fa-wheelchair</option>
                      <option value="fa-wheelchair-alt">fa-wheelchair-alt</option>
                      <option value="fa-wifi">fa-wifi</option>
                      <option value="fa-window-close">fa-window-close</option>
                      <option value="fa-window-close-o">fa-window-close-o</option>
                      <option value="fa-window-maximize">fa-window-maximize</option>
                      <option value="fa-window-minimize">fa-window-minimize</option>
                      <option value="fa-window-restore">fa-window-restore</option>
                      <option value="fa-wrench">fa-wrench</option>
                    </select>
                  </div>
                </div>
               
                <div class="col-md-12 col-sm-12 col-12">
                  <div class="form-actions">  
                    <div class="text-center">
                      <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> G<?php echo JrTexto::_('save menu') ?> </button>
                      <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> <?php echo JrTexto::_('cancel') ?></button>
                    </div>
                  </div>
                </div>
              </div>              
            </form>
          </div>
        </div>    
      </div>
    </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var oMenu_proyecto=new BDMenu_proyecto('<?php echo $ventana; ?>');
    //oMenu_proyecto.vista_ordenar();
  });
</script>