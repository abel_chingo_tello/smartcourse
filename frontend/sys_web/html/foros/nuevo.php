<?php
defined("RUTA_BASE") or die();
$usuarioAct = NegSesion::getUsuario();
$idRol = $usuarioAct['idrol'];
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Forums"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm Agreggarforo',
      text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('Post') ?>',
      click: "guardar();",
      type: 'submit',
      href: '<?php echo $this->documento->getUrlSitio(); ?>/foros/agregar'
    });
  </script>
<?php } ?>
<style>
  .btn {
    font-weight: 500;
  }

  .titulo {
    width: 100%;
    outline: none;
    border: 1px solid #ccc;
  }

  #tit-contenido {
    font-style: italic;
    color: black;
  }

  #enviar {
    float: right;
  }

  .input-group {
    display: grid;
    grid-template-columns: 8fr auto;
  }

  .select2-container {
    width: 100% !important;
  }

  .btn-buscar {
    height: 100%;
    padding: 0px 15px;
    box-sizing: border-box;
  }

  .paris-navbar-container {
    margin: 0px;
    font-size: 8px !important;
  }

  button.btn {
    padding: 4px 8px;

  }

  .select2 {
    width: 50% !important;
  }
</style>
<div>
  <div class="container">
    <div class="row">
      <div class="col-lg-11 col-md-12 mx-auto">
        <form id="post-form" method="post" onsubmit="guardar();return false;">
          <h4><?=JrTexto::_("Title of the publication")?>:</h4>
          <input class="titulo" required type="text" value="<?= $this->publicacion == null ? '' : $this->publicacion['titulo'] ?>" id="titulo">
          <br><br>
          <h5><?=JrTexto::_("Subtitle")?>:</h5>
          <input class="titulo" type="text" value="<?= $this->publicacion == null ? '' : $this->publicacion['subtitulo'] ?>" id="subtitulo">
          <br><br>
          <div id="contentSelect">
            <h5><?=JrTexto::_("Course")?>:</h5>
            <select id="idGruAulaDet" class="titulo">
              <option value="0">-<?=JrTexto::_("Select Course")?>-</option>
            </select>
          </div>
          <br><br>
          <h5 id="tit-contenido"><?=JrTexto::_("Content")?></h5>
          <textarea id="contenido" placeholder="<?=JrTexto::_("Edit here")?>..."><?= $this->publicacion == null ? "" : htmlspecialchars_decode($this->publicacion['contenido']) ?></textarea>
          <br>
        </form>


      </div>

      <div class="col-12">
        <!-- (yo) -->
        <!-- <div class="row">
          <div class="col-lg-8 col-md-9 mx-auto">
            <input id="enviar" form="post-form" class="btn btn-success float-right" type="submit" value="Enivar">
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 "></div>
        </div> -->
        <br>
      </div>

    </div>

  </div>

</div>
<script type="text/javascript">
  function guardar() {

    return new Promise((resolve, reject) => {
      $url = `${_sysUrlBase_}json/foros/guardar?<?= $this->publicacion == null ? '' : "idpublicacion=" . $this->publicacion['idpublicacion'] ?>`;
      publicacion = {
        titulo: $('#titulo').val(),
        subtitulo: $('#subtitulo').val(),
        idgruauladet: $('#idGruAulaDet').val(),
        estado: 1,
        contenido: tinymce.get('contenido').getContent()
      }
      console.log("publicacion: ", publicacion);
      if (publicacion.titulo && publicacion.subtitulo && publicacion.contenido) {
        Swal.showLoading()
        fetch($url, {
          method: 'POST',
          body: JSON.stringify(publicacion)
        }).then(respuestaHttp => {
          rptHttp = respuestaHttp;
          return respuestaHttp.json()
        }).then(respuestaJson => {
          Swal.hideLoading();
          if (respuestaJson.code == 200) {
            Swal.fire({
              title: '<?=JrTexto::_("Success")?>',
              icon: 'success',
              text: "<?=JrTexto::_("Successfully published")?>!",
              confirmButtonText: '<?=JrTexto::_("See recent")?>'
            }).then((result) => {
              window.location.href = `${_sysUrlBase_}foros/`;
            })
            console.log(respuestaJson);
            resolve({
              data: respuestaJson,
              status: rptHttp.status,
              statusText: rptHttp.statusText
            })
          } else {
            Swal.fire({
              title: 'Error',
              icon: 'error',
              text: respuestaJson.msj,
              // confirmButtonText: 'Ver en recientes'
            })
          }
        }).catch(e => {
          console.warn(e);
          reject(e);
        })
      } else {
        Swal.fire('<?=JrTexto::_("You must fill in all fields")?>!', '<?=JrTexto::_("There are empty fields")?>.', 'warning')
      }
    })
  }

  $(document).ready(() => {
    const selectCursos = $('#idGruAulaDet');
    const contentSelect = $('#contentSelect');
    selectCursos.select2();

    if (<?= $idRol ?> == "2") {
      let urlModulo = _sysUrlBase_ + `json/participantes`;

      postData(urlModulo + `/listado`).then(arrCursos => {
        let arrTmp = arrCursos.map((curso) => {
          return {
            id: curso.idgrupoauladetalle,
            text: curso.strcurso + " - " + curso.strgrupoaula,
            idCurso: curso.idcurso
          }
        });
        arrCursos = arrTmp;
        selectCursos.select2({
          data: arrCursos
        });

        if (("<?= $this->publicacion['idgrupoauladetalle'] ?>" != "")) {
          selectCursos.val('<?= $this->publicacion['idgrupoauladetalle'] ?>').trigger('change.select2');
          selectCursos.attr("disabled", true);
        }
      })
    } else {
      contentSelect.hide();
    }



    $('.group-select2').select2({
      data: [{
        id: '1',
        text: '<?=JrTexto::_("All")?>',
        selected: true
      }, {
        id: '2',
        text: '<?=JrTexto::_("Group")?> A'
      }, {
        id: '3',
        text: '<?=JrTexto::_("Group")?> B'
      }, {
        id: '4',
        text: '<?=JrTexto::_("Group")?> C'
      }, ]
    });

    tinymce.init({
      images_upload_url: `${_sysUrlBase_}json/foros/guardarimagen`,
      images_upload_base_path: _sysUrlBase_,
      images_upload_credentials: true,
      relative_urls: false,
      remove_script_host: false,
      document_base_url: _sysUrlBase_,
      selector: '#contenido',
      language: 'es_MX',
      min_height: 500,
      plugins: [
        'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
        'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
        'table emoticons template paste', 'autoresize'
      ],
      menu: {
        file: {
          title: 'File',
          items: 'newdocument restoredraft | preview | print '
        },
        edit: {
          title: 'Edit',
          items: 'undo redo | cut copy paste | selectall | searchreplace'
        },
        view: {
          title: 'View',
          items: 'code | visualaid visualchars visualblocks | spellchecker | preview fullscreen'
        },
        insert: {
          title: 'Insert',
          items: 'image link media template codesample inserttable | charmap emoticons hr | pagebreak nonbreaking anchor toc | insertdatetime'
        },
        format: {
          title: 'Format',
          items: 'bold italic underline strikethrough superscript subscript codeformat | formats blockformats fontformats fontsizes align | forecolor backcolor | removeformat'
        },
        tools: {
          title: 'Tools',
          items: 'spellchecker spellcheckerlanguage | code wordcount'
        },
        table: {
          title: 'Table',
          items: 'inserttable | cell row column | tableprops deletetable'
        },

      },

      toolbar: "undo redo  | fontselect |styleselect | bold  italic| link image | alignleft aligncenter alignright alignjustify | outdent indent | preview ",
      /* enable title field in the Image dialog*/
      image_title: true,
      /* enable automatic uploads of images represented by blob or data URIs*/
      automatic_uploads: true,
      /*
        URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: 'postAcceptor.php',
        here we add custom filepicker only to Image dialog
      */
      file_picker_types: 'image',
      /* and here's our custom image picker*/
      file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        /*
          Note: In modern browsers input[type="file"] is functional without
          even adding it to the DOM, but that might not be the case in some older
          or quirky browsers like IE, so you might want to add it to the DOM
          just in case, and visually hide it. And do not forget do remove it
          once you do not need it anymore.
        */
        input.onchange = function() {
          var file = this.files[0];

          var reader = new FileReader();
          reader.onload = function() {
            /*
              Note: Now we need to register the blob in TinyMCEs image blob
              registry. In the next release this part hopefully won't be
              necessary, as we are looking to handle it internally.
            */
            var id = 'blobid' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            /* call the callback and populate the Title field with the file name */
            cb(blobInfo.blobUri(), {
              title: file.name
            });
          };
          reader.readAsDataURL(file);
        };

        input.click();
      }
    });

    // tinymce.get('contenido').setContent('')
  });
</script>