<?php
defined("RUTA_BASE") or die();
$usuarioAct = NegSesion::getUsuario();
$idDocente = $usuarioAct['idpersona'];
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;

$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Forums"); ?>'
    });
    addButonNavBar({
      class: 'btn  btn-success btn-sm Agreggarforo',
      text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('add') ?>',
      link: _sysUrlBase_ + 'foros/agregar'
    });
  </script>
<?php } ?>
<style>
  #tb-contenido_wrapper>div>a.paginate_button.previous.disabled,
  #tb-contenido_wrapper>div>a.paginate_button.next.disabled {
    padding: 3px !important;
    color: #a2a2a2 !important;
    background-color: white !important;
  }

  .dataTables_length>label>select {
    background-color: #f4f4f4;
    border: 1px solid gainsboro;
    border-radius: 4px;
  }

  td {
    padding: 0 !important;
  }

  table,
  th {
    border: none !important;
  }

  .dataTables_wrapper {
    margin-bottom: 20px;
  }

  .dataTables_length {
    font-style: italic;
    color: #656565 !important;
  }

  table.dataTable tbody tr {
    background-color: transparent;
  }

  .btn {
    font-weight: 500;
  }

  .post-subtitle {
    font-weight: 300;
    margin: 0 0 10px;
    font-size: 1.75rem;
  }

  .no-publicaciones {
    font-style: italic;
  }

  .btn-buscar {
    height: 100%;
    padding: 0px 15px;
    box-sizing: border-box;
  }

  .post-preview>a>.post-title {
    font-size: 30px;
    font-weight: 600;
  }

  .post-meta>a>i {
    float: right;
    color: #444444;
    cursor: pointer;
    margin-left: 19px;
    font-size: 20px;
  }

  .post-meta>i:hover {
    color: black;
  }
  .paris-navbar-container{
    margin: 0px;
    font-size: 8px !important;
  }
  button.btn{
    padding: 4px 8px;
    
  }
  .div-foro{
    background: white;
    /* box-shadow: 0px 3px 0px 0px rgba(0,0,0,0.75); */
  }
</style>
<div>
  <!-- Main Content -->
  <div class="container">
    <div class="row">

      <!-- <div class="col-lg-8 col-md-10 mx-auto">
        <div class="clearfix">
          <a id="linkNuevo" class="btn btn-success float-right" href=_sysUrlBase_>Publicar <i class="fa fa-chevron-right"></i></a>
        </div>
      </div> -->
    </div>
    <div class="row">
      <div id="listaPosts" class="col-md-8 sm-col-12 mx-auto"></div>
      <!-- Sidebar Widgets Column -->
      <div class="col-md-3">

        <!-- Search Widget -->
        <div class="card my-4">
          <h5 class="card-header"><?=JrTexto::_("Search")?></h5>
          <div class="card-body">
            <form id="frm-busqueda" onsubmit="return buscar()" class="input-group">
              <input id="busqueda" type="text" class="form-control" placeholder="<?=JrTexto::_("Search")?>...">
              <span class="input-group-btn">
                <button class="btn btn-secondary btn-buscar" type="subit" form="frm-busqueda">Ok</button>
              </span>
            </form>
          </div>
        </div>

        <!-- Categories Widget -->
        <!-- <div class="card my-4">
          <h5 class="card-header">Categorías</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">Web Design</a>
                  </li>
                  <li>
                    <a href="#">HTML</a>
                  </li>
                  <li>
                    <a href="#">Freebies</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">JavaScript</a>
                  </li>
                  <li>
                    <a href="#">CSS</a>
                  </li>
                  <li>
                    <a href="#">Tutorials</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div> -->


      </div>
      <div class="col-12">
        <div class="row">
          <!-- Pager -->
          <div class="col-lg-8 col-md-8 mx-auto">
            <div class="clearfix">
              <!-- <a class="btn btn-primary float-right" href="#">Ver Anteriores <i class="fa fa-chevron-right"></i></a> -->
            </div>
          </div>
          <div class="col-3"></div>
        </div>
      </div>
    </div>
  </div>

  <br>
</div>
<script type="text/javascript">
  var table = null;
  $(document).ready(function(ev) {
    listar();

  });

  function getPublicaciones() {
    return new Promise((resolve, reject) => {
      $url = `${_sysUrlBase_}json/foros`;
      fetch($url, {
        method: 'GET',
      }).then(respuestaHttp => {
        rptHttp = respuestaHttp;
        return respuestaHttp.json()
      }).then(respuestaJson => {
        // console.log(respuestaJson);
        resolve({
          data: respuestaJson,
          status: rptHttp.status,
          statusText: rptHttp.statusText
        })
      }).catch(e => {
        // console.warn(e);
        reject(e);
      })
    })
  }

  function searchPublicaciones(busqueda) {
    return new Promise((resolve, reject) => {
      $url = `${_sysUrlBase_}json/foros/?busqueda=` + busqueda;
      fetch($url, {
        method: 'GET',
      }).then(respuestaHttp => {
        rptHttp = respuestaHttp;
        return respuestaHttp.json()
      }).then(respuestaJson => {
        // console.log(respuestaJson);
        resolve({
          data: respuestaJson,
          status: rptHttp.status,
          statusText: rptHttp.statusText
        })
      }).catch(e => {
        // console.warn(e);
        reject(e);
      })
    })
  }

  function eliminar(idpublicacion) {
    Swal.fire({
      title: '<?=JrTexto::_("Are you sure?")?>',
      text: "<?=JrTexto::_("This action cannot be reversed")?>!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '<?=JrTexto::_("Yes")?>'
    }).then((result) => {
      if (result.value) {
        return new Promise((resolve, reject) => {
          $url = `${_sysUrlBase_}json/foros/eliminar`;
          const data = new FormData();
          // console.log("id a eliminar: " + idpublicacion);
          data.append('idpublicacion', idpublicacion);
          fetch($url, {
            method: 'POST',
            body: data
          }).then(respuestaHttp => {
            rptHttp = respuestaHttp;
            return respuestaHttp.json()
          }).then(respuestaJson => {
            // console.log(respuestaJson);
            Swal.fire(
              '<?=JrTexto::_("Success")?>',
              '<?=JrTexto::_("Your post has been deleted")?>.',
              'success'
            )
            listar();
            resolve({
              data: respuestaJson,
              status: rptHttp.status,
              statusText: rptHttp.statusText
            })
          }).catch(e => {
            // console.warn(e);
            reject(e);
          })
        })

      }
    });

  }

  function listar() {
    var arrPosts = null;
    getPublicaciones().then(response => {
      arrPosts = response.data.data;
      if (table != null) {
        table.destroy();
      }
      if (arrPosts.length) {
        let html = ``;
        arrPosts.forEach(post => {
          if(post.idgrupoauladetalle > 0 && post.idpersona != <?= $idDocente ?>){
            return;
          }
          html += `
            <tr>
              <td>
                <div class="row mx-auto">
                  <div class="col-lg-12 col-md-12 mx-auto div-foro">
                    <div class="post-preview">
                      <a href="${_sysUrlBase_}foros/visualizar?idpublicacion=${post.idpublicacion}">
                        <h2 class="post-title">
                          ${post.titulo}
                        </h2>
                        <h3 class="post-subtitle">
                        ${post.subtitulo}
                        </h3>
                      </a>
                      <p class="post-meta">Publicado por
                        <a href="#">${post.autor}</a>
                        el ${post.fecha_hora}` +
            (post.idpersona == <?= $this->idpersona ?> ? (`<a href="${_sysUrlBase_}foros/agregar?idpublicacion=${post.idpublicacion}"><i class="fa fa-edit"></i></a>
                      <a href="#!" onclick="eliminar(${post.idpublicacion})" ><i class="fa fa-trash"></i></a>
                      `) : (``)) + `
                      </p>
                    </div>
                    <hr>
                  </div>
                </div>
              </td>
            </tr>`;
        });
        let htmlTable = `<table id="tb-contenido">
                                <thead>
                                  <tr>
                                    <th> </th>
                                  </tr>
                                </thead>
                                <tbody id="lista_contenido">
                                </tbody>
                              </table>`;
        $('#listaPosts').html(htmlTable);
        $('#lista_contenido').html(html);
        table = drawTable();
      } else {
        let html = `
            <div class="row mx-auto">
              <div class="col-lg-12 col-md-12 mx-auto">
                <div class="post-preview">
                  <span >
                    <h2 class="post-title">
                    </h2>
                    <h3 class="post-subtitle no-publicaciones">
                    <?=JrTexto::_("No posts for now")?>...
                    </h3>
                  </span>
                  <p class="post-meta">
                  </p>
                </div>
                <hr>
              </div>
            </div>`;
        $('#listaPosts').html(html);
      }

    })
  }

  function drawTable() {
    table = $('#tb-contenido').DataTable({
      searching: false,
      ordering: false,
      paging: true,
      info: false,
      language: {
        search: "<?=JrTexto::_("Search")?>:",
        lengthMenu: "<?=JrTexto::_("Show")?> &nbsp _MENU_ &nbsp <?=JrTexto::_("first publications")?>",
        info: "<?=JrTexto::_("Showing")?> _START_ a _END_ <?=JrTexto::_("total of records")?> _TOTAL_ ",
        paginate: {
          first: "<?=JrTexto::_("First page")?>",
          previous: "<?=JrTexto::_("Previous")?>",
          next: "<?=JrTexto::_("Next")?>",
          last: "<?=JrTexto::_("Last page")?>"
        }
      }
    });
    return table;
  }

  function buscar() {
    var arrPosts = null;
    let busqueda = $('#busqueda').val();
    searchPublicaciones(busqueda).then(response => {
      arrPosts = response.data.data;
      if (arrPosts.length) {
        let html = ``;
        arrPosts.forEach(post => {
          if(post.idgrupoauladetalle > 0 && post.idpersona != <?= $idDocente ?>){
            return;
          }
          html += `
            <div class="row mx-auto">
              <div class="col-lg-12 col-md-12 mx-auto">
                <div class="post-preview">
                  <a href="${_sysUrlBase_}foros/visualizar?idpublicacion=${post.idpublicacion}">
                    <h2 class="post-title">
                      ${post.titulo}
                    </h2>
                    <h3 class="post-subtitle">
                    ${post.subtitulo}
                    </h3>
                  </a>
                  <p class="post-meta">Publicado por
                    <a href="#">${post.autor}</a>
                    el ${post.fecha_hora}` +
            (post.idpersona == <?= $this->idpersona ?> ? (`<a href="${_sysUrlBase_}foros/agregar?idpublicacion=${post.idpublicacion}"><i class="fa fa-edit"></i></a>
                  <a href="#!" onclick="eliminar(${post.idpublicacion})" ><i class="fa fa-trash"></i></a>
                  `) : (``)) + `
                  </p>
                </div>
                <hr>
              </div>
            </div>`;
          $('#listaPosts').html(html);
        });
      } else {
        let html = `
            <div class="row mx-auto">
              <div class="col-lg-12 col-md-12 mx-auto">
                <div class="post-preview">
                  <span >
                    <h2 class="post-title">
                    </h2>
                    <h3 class="post-subtitle no-publicaciones">
                    <?=JrTexto::_("No results found related to your search")?>.
                    </h3>
                  </span>
                  <p class="post-meta">
                  </p>
                </div>
                <hr>
              </div>
            </div>`;
        $('#listaPosts').html(html);
      }

    })
    return false;
  }
</script>