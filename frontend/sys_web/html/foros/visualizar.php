<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("forums"); ?>'
    });
  </script>
<?php } ?>
<!-- Custom styles for this template -->
<!-- <link href="css/blog-post.css" rel="stylesheet"> -->
<style>
  #tb-contenido_wrapper>div>a.paginate_button.previous.disabled,
  #tb-contenido_wrapper>div>a.paginate_button.next.disabled {
    padding: 3px !important;
    color: #a2a2a2 !important;
    background-color: white !important;
  }

  td {
    padding: 0 !important;
  }

  table,
  th {
    border: none !important;
  }

  .dataTables_wrapper {
    margin-bottom: 20px;
  }

  .dataTables_length {
    font-style: italic;
  }

  table.dataTable tbody tr {
    background-color: transparent;
  }

  .dataTables_length>label>select {
    background-color: #f4f4f4;
    border: 1px solid gainsboro;
    border-radius: 4px;
  }

  .btn {
    font-weight: 500;
  }

  .title-puntaje-g {
    font-style: italic;
  }

  .media,
  .mb-4 {
    background-color: #ffffff66;
    ;
    padding: 15px;
    border-radius: 10px;
  }

  .media:hover,
  .mb-4:hover {
    background-color: white;
  }

  .widgets {
    display: none;
  }

  .autor {
    /* font-style: italic; */
  }

  .fecha {
    font-style: italic;
  }

  .cont-puntuar {
    display: grid;
    grid-template-columns: auto auto;
    padding: 0;
  }

  .cont-puntuar>h5 {
    margin: 0;
    /* padding: 0; */
    display: flex;
    align-items: center;
    background-color: inherit;
    border-bottom: none;
  }

  .rate {
    float: left;
    height: 46px;
    padding: 0 10px;
  }

  .rate:not(:checked)>input {
    position: absolute;
    display: none;
  }

  .rate:not(:checked)>label {
    float: right;
    width: 1em;
    overflow: hidden;
    white-space: nowrap;
    cursor: pointer;
    font-size: 30px;
    color: #ccc;
  }

  .rate:not(:checked)>label:before {
    content: '★ ';
  }

  .rate>input:checked~label {
    color: #ffc700;
  }

  .rate:not(:checked)>label:hover,
  .rate:not(:checked)>label:hover~label {
    color: #deb217;
  }

  .rate>input:checked+label:hover,
  .rate>input:checked+label:hover~label,
  .rate>input:checked~label:hover,
  .rate>input:checked~label:hover~label,
  .rate>label:hover~input:checked~label {
    color: #c59b08;
  }

  .star-size {
    font-size: 50px !important;
  }

  .g-rate {
    height: auto;
    float: left;
    /* height: 46px; */
    padding: 0 10px;
  }

  .g-rate:not(:checked)>input {
    position: absolute;
    display: none;
  }

  .g-rate:not(:checked)>label {
    float: right;
    width: 1em;
    overflow: hidden;
    white-space: nowrap;
    cursor: pointer;
    font-size: 30px;
    color: #ccc;
  }

  .g-rate:not(:checked)>label:before {
    content: '★ ';
  }

  .g-rate>input:checked~label {
    color: #ffc700;
  }
  button.btn{
    padding: 4px 8px;
    
  }
  @media only screen and (max-width: 390px) {
    .dataTables_info {
      width: 100% !important;
    }
  }
</style>

<div>

  <div class="container">

    <div class="row">
      <div class="col-lg-10 col-md-10 mx-auto">
        <div class="row">

          <!-- Post Content Column -->
          <div id="post" class="col-lg-12">

            <h1 class="mt-4"><?= $this->data['titulo'] ?></h1>
            <p class="lead">
              por
              <a class="autor" href="#"><?= $this->data['autor'] ?></a>
            </p>
            <hr>
            <p class="fecha"><?php echo JrTexto::_("posted on"); ?> <?= $this->data['fecha_hora'] ?>
              <!-- a las <?= $this->data['hora'] ?> -->
            </p>
            <hr>
            <div id="contenido">
              <?= htmlspecialchars_decode($this->data['contenido']) ?>
            </div>
            <hr>
            <div class="col cont-puntuar">
              <h5 class="card-header title-puntaje-g"><?php echo JrTexto::_("user rating"); ?> </h5>
              <div class="g-rate">
                <input type="radio" id="gstar5" name="g-rate" disabled value="5" />
                <label for="gstar5" class="star-size" title="text"></label>
                <input type="radio" id="gstar4" name="g-rate" disabled value="4" />
                <label for="gstar4" class="star-size" title="text"></label>
                <input type="radio" id="gstar3" name="g-rate" disabled value="3" />
                <label for="gstar3" class="star-size" title="text"></label>
                <input type="radio" id="gstar2" name="g-rate" disabled value="2" />
                <label for="gstar2" class="star-size" title="text"></label>
                <input type="radio" id="gstar1" name="g-rate" disabled value="1" />
                <label for="gstar1" class="star-size" title="text"></label>
              </div>
            </div>
            <hr>
            <hr>
            <div class="col cont-puntuar">
              <h5 class="card-header"><?php echo JrTexto::_("score"); ?> </h5>
              <div class="rate">
                <input type="radio" onchange="postInteraccion(5)" id="star5" name="rate" value="5" />
                <label for="star5" title="text">5 stars</label>
                <input type="radio" onchange="postInteraccion(4)" id="star4" name="rate" value="4" />
                <label for="star4" title="text">4 stars</label>
                <input type="radio" onchange="postInteraccion(3)" id="star3" name="rate" value="3" />
                <label for="star3" title="text">3 stars</label>
                <input type="radio" onchange="postInteraccion(2)" id="star2" name="rate" value="2" />
                <label for="star2" title="text">2 stars</label>
                <input type="radio" onchange="postInteraccion(1)" id="star1" name="rate" value="1" />
                <label for="star1" title="text">1 star</label>
              </div>
            </div>
            <!-- <hr> -->

            <!-- Comments Form -->
            <div class="card my-4">
              <h5 class="card-header">Deja un comentario:</h5>
              <div class="card-body">
                <form onsubmit="postComentario();return false">
                  <div class="form-group">
                    <textarea id="contenido_comentario" class="form-control" rows="3"></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
              </div>
            </div>
            <!-- Comment with nested comments -->
            <div id="lista_comentarios">

            </div>
          </div>

          <!-- Sidebar Widgets Column -->
          <div class="col-md-4 widgets">

            <!-- Search Widget -->
            <div class="card my-4">
              <h5 class="card-header">Buscar</h5>
              <div class="card-body">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="buscar...">
                  <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button">Ok</button>
                  </span>
                </div>
              </div>
            </div>

            <!-- Categories Widget -->
            <div class="card my-4">
              <h5 class="card-header">Categorías</h5>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6">
                    <ul class="list-unstyled mb-0">
                      <li>
                        <a href="#">Web Design</a>
                      </li>
                      <li>
                        <a href="#">HTML</a>
                      </li>
                      <li>
                        <a href="#">Freebies</a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-lg-6">
                    <ul class="list-unstyled mb-0">
                      <li>
                        <a href="#">JavaScript</a>
                      </li>
                      <li>
                        <a href="#">CSS</a>
                      </li>
                      <li>
                        <a href="#">Tutorials</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>


          </div>

        </div>
      </div>
    </div>
  </div>

</div>
<!-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->
<script type="text/javascript">
  var table = null;
  $(document).ready(function(ev) {
    listarComentarios();
    dibujarInteraccion();
  });

  function listarComentarios() {
    getComentarios().then(respuesta => {
      arrComentarios = respuesta.data.data;
      if (table != null) {
        table.destroy();
      }
      let html = ``;
      arrComentarios.forEach(comentario => {
        html += ` <tr><td>
                    <div class="media mb-4">
                    <!-- <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""> -->
                      <div class="media-body">
                        <h5 class="mt-0">${comentario.autor}</h5>
                        ${comentario.contenido}
                        <div class="media mt-4" style="display:none">
                        <!-- <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""> -->
                          <div class="media-body">
                            <h5 class="mt-0">Commenter Name 2</h5>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                  </tr>`;
      });
      let htmlTable = `<table id="tb-contenido">
                                <thead>
                                  <tr> <th> </th> </tr>
                                </thead>
                                <tbody id="lista_contenido">
                                <tr><td>No hay comentarios...</td></tr>
                                </tbody>
                              </table>`;
      $('#lista_comentarios').html(htmlTable);
      if (arrComentarios.length > 0) {
        $('#lista_contenido').html(html);
      }

      table = drawTable();
    });
    
  }

  function postComentario() {
    return new Promise((resolve, reject) => {
      comentario = {
        contenido: $('#contenido_comentario').val(),
        idpublicacion: <?= $this->data['idpublicacion'] ?>,
        idcomentariopadre: null,
      }
      $url = `${_sysUrlBase_}json/foroComentario/guardar`;
      if (comentario.contenido) {
        Swal.showLoading()
        fetch($url, {
          method: 'POST',
          body: JSON.stringify(comentario)
        }).then(respuestaHttp => {
          rptHttp = respuestaHttp;
          return respuestaHttp.json()
        }).then(respuestaJson => {
          // console.log(respuestaJson);
          $('#contenido_comentario').val("");
          Swal.hideLoading();
          Swal.fire(
            'Exito!',
            'Comentario enviado.',
            'success'
          )
          listarComentarios();
          resolve({
            data: respuestaJson,
            status: rptHttp.status,
            statusText: rptHttp.statusText
          })
        }).catch(e => {
          // console.warn(e);
          reject(e);
        })

        // console.log(comentario);
      } else {
        Swal.fire(
          'Debe llenar todos los campos!',
          'Hay campos vacíos.',
          'warning'
        )
      }

    })
  }

  function getComentarios() {
    return new Promise((resolve, reject) => {
      $url = `${_sysUrlBase_}json/foroComentario/buscar?idpublicacion=<?= $this->data['idpublicacion'] ?>`;
      fetch($url, {
        method: 'GET',
      }).then(respuestaHttp => {
        rptHttp = respuestaHttp;
        return respuestaHttp.json()
      }).then(respuestaJson => {
        // console.log(respuestaJson);
        resolve({
          data: respuestaJson,
          status: rptHttp.status,
          statusText: rptHttp.statusText
        })
      }).catch(e => {
        // console.warn(e);
        reject(e);
      })

    })
  }

  function postInteraccion(puntuacion) {
    return new Promise((resolve, reject) => {
      interaccion = {
        idpublicacion: <?= $this->data['idpublicacion'] ?>,
        idforo_interaccion: '<?= $this->data['idforo_interaccion'] ?>',
        puntuacion: puntuacion
      }
      console.log("interaccion a enviar ", interaccion);
      $url = `${_sysUrlBase_}json/foroInteraccion/guardar`;
      if (interaccion) {

        fetch($url, {
          method: 'POST',
          body: JSON.stringify(interaccion)
        }).then(respuestaHttp => {
          rptHttp = respuestaHttp;
          return respuestaHttp.json()
        }).then(respuestaJson => {
          console.log("Respuesa al enviar POST: ", respuestaJson);
          dibujarInteraccion();
          Swal.fire({
            position: 'bottom-end',
            timer: 800,
            icon: 'success',
            title: 'Puntuación asignada',
            showConfirmButton: false,
            backdrop: false,
            allowOutsideClick: false
          })

          resolve({
            data: respuestaJson,
            status: rptHttp.status,
            statusText: rptHttp.statusText
          })
        }).catch(e => {
          // console.warn(e);
          reject(e);
        })
      } else {
        Swal.fire(
          'Debe llenar todos los campos!',
          'Hay campos vacíos.',
          'warning'
        )
      }

    })
  }

  function getPromedio() {
    return new Promise((resolve, reject) => {
      $url = `${_sysUrlBase_}json/foroInteraccion/?idpublicacion=<?= $this->data['idpublicacion'] ?>`;
      fetch($url, {
        method: 'GET',
      }).then(respuestaHttp => {
        rptHttp = respuestaHttp;
        return respuestaHttp.json()
      }).then(respuestaJson => {
        console.log("Get promedio: ", respuestaJson);

        resolve({
          data: respuestaJson,
          status: rptHttp.status,
          statusText: rptHttp.statusText
        })
      }).catch(e => {
        // console.warn(e);
        reject(e);
      })

    })
  }

  function getInteraccion() {
    return new Promise((resolve, reject) => {
      $url = `${_sysUrlBase_}json/foroInteraccion/buscar?idforo_interaccion=<?= $this->data['idforo_interaccion'] ?>`;
      fetch($url, {
        method: 'GET',
      }).then(respuestaHttp => {
        rptHttp = respuestaHttp;
        return respuestaHttp.json()
      }).then(respuestaJson => {
        console.log("Get interaccion: ", respuestaJson);

        resolve({
          data: respuestaJson,
          status: rptHttp.status,
          statusText: rptHttp.statusText
        })
      }).catch(e => {
        // console.warn(e);
        reject(e);
      })

    })
  }

  function dibujarInteraccion() {
    getInteraccion().then(respuesta => {
      let interaccion = respuesta.data.data;
      if (interaccion && interaccion.puntuación) {
        $('#star' + interaccion.puntuación).prop("checked", true);
      }
      console.log("iteracion dibujada: ");
      console.log(interaccion);
    })
    getPromedio().then(respuesta => {
      let interaccion = respuesta.data.data;
      if (interaccion && interaccion.puntuacion_total) {

        $('#gstar' + interaccion.puntuacion_total).prop("checked", true);
      }
      console.log("iteracion promedio dibujada: ");
      console.log(interaccion);
    })
  }

  function drawTable() {
    table = $('#tb-contenido').DataTable({
      searching: false,
      ordering: false,
      paging: true,
      info: false,
      // pagingType: 'numbers',
      language: {
        search: "Buscar:",
        lengthMenu: "Mostrando los &nbsp _MENU_ &nbsp primeros comentarios",
        // info: "Mostrando _START_ a _END_ publicaciones de un total de _TOTAL_ ",
        paginate: {
          first: "Primera página",
          previous: "Anterior",
          next: "Siguiente",
          last: "Última página"
        }
      }
    });
    return table;
  }

  

</script>