<?php
//var_dump($this->examen); 
?>
<div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12  text-center">
        <div class="jumbotron">
          
        <?php if(!empty($this->examen)){ ?>
          <h1><strong><?php echo JrTexto::_('Result') ?></strong></h1>
          <?php
            echo $this->examen["notatexto"];
            echo '<b>'.JrTexto::_('Date').":</b> ".$this->examen["regfecha"].'<br>';
            echo '<b>'.JrTexto::_('Time').":</b> ".$this->examen["tiempo_realizado"];  ?>        
           <?php }else{ ?>
            
                <h1><strong>Opss!</strong></h1>
                
                <div class="disculpa"><?php echo JrTexto::_('Sorry, there have been the following errors')?>:</div>
                
                <div class="alert alert-danger" >
                    <p class="mensaje"><?php echo JrTexto::_('retest or contact support')?></p>
                </div>  

                <div class="">
                    <a class="btn btn-default" href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Go back')?></a>
                </div>
            
          <?php } ?>
          </div>
        </div>
  </div>
</div>
<script type="text/javascript">
  function readCookie(name) {
      return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    }
  var terminoexamen='<?php echo !empty($this->terminoprogreso)?'ok':'no'; ?>';
  var dandoexamen2=readCookie('dandoidexamen');
  document.cookie = "dandoidexamen=_"+dandoexamen2+"_"+terminoexamen+"_; path=/";
  //document.cookie = "idpestania=_"+dandoexamen2+"_"+terminoexamen+"_; path=/";
  var dandoexamen2=readCookie('dandoidexamen');
</script>