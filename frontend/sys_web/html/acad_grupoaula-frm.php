<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/nofoto.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/acad_grupoaula">&nbsp;<?php echo JrTexto::_('Acad_grupoaula'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>"  >
  <div class="col">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" tb="acad_grupoaula"  class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idgrupoaula" id="idgrupoaula" value="<?php echo $this->pk;?>">
          <input type="hidden" id="txtNvacantes" name="nvacantes" step="1" value="1000"  min="1" class="form-control" required />
            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>
              <input type="text"  id="txtNombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
            </div>
            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Comentario');?> <span class="required"> * </span></label>
              <textarea id="txtComentario" name="comentario" class="form-control" ><?php echo @trim($frm["comentario"]); ?></textarea>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                <label class="control-label"><?php echo JrTexto::_('Tipo');?> <span class="required"> * </span></label>
                  <div class="cajaselect">
                    <select name="tipo" id="tipo" class="form-control">
                      <!--option><?php echo JrTexto::_('All type') ?></option-->
                      <?php if(!empty($this->fktipos))
                      foreach ($this->fktipos as $tip){ ?>
                        <option value="<?php echo $tip["codigo"] ?>" <?php echo $tip["codigo"]==@$frm["tipo"]?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_($tip["nombre"]))?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                <label class="control-label"><?php echo JrTexto::_('Grade');?> <span class="required"> * </span></label>
                <div class="cajaselect">
                  <select id="idgrado" name="idgrado" class="form-control" > 
                  <option value="0"><?php echo JrTexto::_('Único'); ?></option>                  
                      <?php 
                      if(!empty($this->grados))
                      foreach ($this->grados as $r) { ?><option value="<?php echo $r["idgrado"]?>" <?php echo $r["idgrado"]==@$frm["idgrado"]?'selected="selected"':'';?> ><?php echo JrTexto::_('Grado')." ".$r["abrev"]." : ".JrTexto::_($r["descripcion"]); ?></option>
                       <?php } ?>                        
                  </select>
                </div>
              </div>
              <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                <label class="control-label"><?php echo JrTexto::_('Sección');?> <span class="required"> * </span></label>
                <div class="cajaselect">
                  <select id="idsesion" name="idsesion" class="form-control" >  
                  <option value="0"><?php echo JrTexto::_('Único'); ?></option>                  
                      <?php 
                      if(!empty($this->seccion))
                      foreach ($this->seccion as $s) { ?><option value="<?php echo $s["idsesion"]?>" <?php echo $s["idsesion"]==@$frm["idsesion"]?'selected="selected"':'';?> ><?php echo JrTexto::_('Sección')." ".$s["descripcion"] ?></option>
                       <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
                    <a  style="cursor:pointer;" href="javascript:;"  class="btn-activar <?php echo @$frm["estado"]==1?"active":"";?>"><i class="fa fa<?php echo @$frm["estado"]=="1"?"-check":"";?>-circle-o fa-lg"></i> 
                    <input type="hidden" name="estado" value="<?php echo @$frm["estado"]?1:0;?>"></a>
                </div>
              </div>              
            </div>       
        
          <hr>
          <div class="col-md-12 form-group text-center">
              <button id="btn-saveAcad_grupoaula" type="submit" class="btn btn-success" tb="acad_grupoaula" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Guardar');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('acad_grupoaula'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancelar');?></a>
          </div>
        </form>
  </div>
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/frm.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frm($('#vent-<?php echo $idgui;?>'));
  })
</script>