<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Tareas"); ?>'
    });
    /*addButonNavBar({
      class: 'btn btn-success btn-sm Agreggarforo',
      text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('add') ?>',
      href: '<?php echo $this->documento->getUrlSitio(); ?>/foros/agregar'
    });*/
  </script>
<?php } ?>
<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
<?php } ?>

<style type="text/css">
  .puntaje {
    position: absolute;
    right: 35px;
    top: 35px;
  }

  .puntaje small {
    font-weight: 1.5em;
  }

  #chat #header-chat {
    background-color: #555;
    color: #20f1f14d;
    padding: 10px;
    text-align: center;
    text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.6);
  }

  #chat #mensajes {
    padding: 10px;
    height: 400px;
    width: 100%;
    overflow: hidden;
    overflow-y: scroll
  }

  #chat #mensajes .mensaje-autor {
    margin-bottom: 20px;
    float: left;
  }

  #chat #mensajes .mensaje-autor img,
  #chat #mensajes .mensaje-amigo img {
    display: inline-block;
    vertical-align: top;
    max-width: 50px;
    max-height: 50px;
  }

  #chat #mensajes .mensaje-autor .contenido {
    background-color: #20f1f14d;
    border-radius: 5px;
    box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);
    display: inline-block;
    font-size: 13px;
    padding: 15px;
    vertical-align: top;
  }

  #chat #mensajes .mensaje-autor .fecha {
    color: #777;
    font-style: italic;
    font-size: 13px;
    text-align: right;
    margin-right: 35px;
    margin-top: 10px;
  }

  #chat #mensajes .mensaje-autor .flecha-izquierda {
    display: inline-block;
    margin-right: -6px;
    margin-top: 10px;
    width: 0;
    height: 0;
    border-top: 0px solid transparent;
    border-bottom: 15px solid transparent;
    border-right: 15px solid #20f1f14d;
  }

  #chat #mensajes .mensaje-amigo {
    margin-bottom: 20px;
    float: right;
  }

  #chat #mensajes .mensaje-amigo .contenido {
    background-color: #3990BF;
    border-radius: 5px;
    color: white;
    display: inline-block;
    font-size: 13px;
    padding: 15px;
    vertical-align: top;
  }

  #chat #mensajes .mensaje-amigo .flecha-derecha {
    display: inline-block;
    margin-left: -6px;
    margin-top: 10px;
    width: 0;
    height: 0;
    border-top: 0px solid transparent;
    border-bottom: 15px solid transparent;
    border-left: 15px solid #3990BF;
  }

  #chat #mensajes .mensaje-amigo img,
  #chat #mensajes .mensaje-autor img {
    border-radius: 5px;
  }

  #chat #mensajes .mensaje-amigo .fecha {
    color: #777;
    font-style: italic;
    font-size: 12px;
    text-align: left;
    margin-top: 0px;
  }
</style>
<style>
  /* mine */
  .txt-fecha-limite {
    font-size: .8rem !important;
    font-weight: 500;
    display: inline !important;
    font-style: italic;
  }
</style>
<div class="row my-font-all soft-rw">
  <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
    <label class="control-label"><?php echo JrTexto::_('Mis grupos'); ?></label>
    <select id="idgrupoaula" name="idgrupoaula" class="form-control" idsel="<?php echo @$this->idgrupoaula; ?>">
      <?php if (!empty($this->misgrupos))
        foreach ($this->misgrupos as $fk => $v) {
          //var_dump($v);
      ?>
        <option value="<?php echo $fk; ?>"><?php echo $v["nombre"] ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
    <label class="control-label"><?php echo JrTexto::_('My courses'); ?></label>
    <select id="idcurso" name="idcurso" class="form-control" idsel="<?php echo @$this->idcurso; ?>">
      <?php if (!empty($this->gruposauladetalle))
        foreach ($this->gruposauladetalle as $fk) { ?>
        <option value="<?php echo $fk["idcurso"] ?>" data-idgrupoaula="<?php echo $fk["idgrupoaula"] ?>" data-idgrupoauladetalle="<?php echo $fk["idgrupoauladetalle"] ?>" data-idcomplementario="<?php echo $fk["idcomplementario"] ?>" style="display: none;"><?php echo $fk["nombrecurso"] ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
    <label class="control-label"><?php echo JrTexto::_('Mis alumnos'); ?></label>
    <select id="idalumno" name="idalumno" class="form-control" idsel="<?php echo @$this->idalumno; ?>">
      <option value="" selected="selected"> Todos </option>
      <?php if (!empty($this->alumnosxgrupoaula))
        foreach ($this->alumnosxgrupoaula as $fk => $v) {
          if (!empty($v))
            foreach ($v as $alu) { //var_dump($alu); 
      ?>
          <option value="<?php echo $alu["idalumno"] ?>" data-idgrupoauladetalle="<?php echo $alu["idgrupoauladetalle"] ?>" style="display: none;"><?php echo $alu["stralumno"] ?></option>
      <?php }
        } ?>
    </select>
  </div>
  <div class="col-md-4">

  </div>
  <div class="col-md-12 " id="">
    <div class="accordion" id="vistalisatotareas" role="tablist" aria-multiselectable="true">
      <div class="my-card my-shadow panel panelclone col-md-6 " style="display: none;">
        <a class="panel-heading collapsed my-card" role="tab" id="heading" data-toggle="collapse" href="#collapse" aria-expanded="false" aria-controls="collapse">
          <h4 class="panel-title"><span class="rem-2">Tarea</span> <span class="txt-fecha-limite"></span><i name="btn-eliminar-tarea" class="fa fa-trash my-inline my-float-r my-color-blue" aria-hidden="true"></i><small class="sutbtitle rem-1"></small> </h4>
        </a>
        <div class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading" style="">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 text-center table-responsive">
                <table class="table">
                  <thead>
                    <tr class="">
                      <th class="text-left">Estudiantes</th>
                      <th>Archivos de Referencia</th>
                      <th>Notas</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="trclone" style="display: none;">
                      <td class="nombre text-left"> Abel chingo Tello</td>
                      <td> <a href="#" target="_blank" class="archivoalumno"><i class="fa fa-cloud-download" style="font-size: 2em"></i><br> ver o descargar </a></td>
                      <td class="vernota">20</td>
                      <td><a href="#" class="subirtarea"><i class="fa fa-comments-o"></i> <br> ver Chat </td></a>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- working -->
    <div id="idcriterios" style="display: none">
      <div id="notas-body" class="col-md-12 table-responsive criterio">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Criterio</th>
              <th>Nota 0-100</th>
            </tr>
          </thead>
          <tbody>
            <tr class="trclone" style="display: none">
              <td class="criterio"></td>
              <td><input class="nota" type="number" min="0" max="100"></td>
            </tr>
          </tbody>
        </table>
        <!-- aqui se dibujará la rubrica -->
        <div id="main-cont" class="row col-sm-12 my-font-all">
          <div id="my-loader" class="my-loader my-color-green"></div>
        </div>
        <div id="campo-nota-rubrica" class="form-group col-md-12 my-hide">
          <label class="col-md-6 text-right">Nota de rúbrica 0-<span class="span-nota-rubrica">100</span></label>
          <input type="number" disabled name="nota-rubrica" min="0" max="100" class="input-nota-rubrica form-control col-md-6" value="0.0">
        </div>
        <div id="campo-default" class="form-group col-md-12">
          <label class="col-md-6 text-right">Nota Final 0-<span id="span-my-base">10</span></label>
          <input type="number" name="notafinal" min="0" max="100" class="notafinal form-control col-md-6" value="0.0">
        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="col-md-12 text-center">
          <a href="#" class="btn btn-warning cerrarmodal">Cerrar</a>
          <a href="#" class="btn btn-primary btnguardar"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div id="to-clone" class="my-hide row col-sm-12">
          <div id="row-cont-rubrica" class="col-sm-12 row-cont-rubrica">
            <div class="cont-rubrica my-card my-shadow">
              <h3 onclick="modalRubrica(this)" class="text-center my-full-w">Titulo de rubrica</h3>
              <div id="out-cont" class="out-cont">
                <div id="body-rubrica" class="body-cont-rubrica rubrica-black tema-4">
                  <div id="rubrica-code" class="cod-rubrica">
                    <div class="code-text">Código</div>
                    <div class="code-num"></div>
                  </div>
                  <div id="cont-btn-crit" onclick="modalCriterio(this)" class="cont-btn-crit">
                    <div class="criterio-text">Detalle</div>
                    <div class="criterio-num">5%</div>
                  </div>
                  <div id="cont-btn-nivel" onclick="modalNivel(this)" name="cont-btn-nivel" class="cont-btn-nivel">
                    <div class="nivel-text">Excelente</div>
                    <div class="nivel-num">20</div>
                  </div>
                  <div id="cont-btn-indicador" onclick="modalIndicador(this)" class="cont-btn-indicador">
                    <div class="indicador-text">Detalle</div>
                  </div>
                  <div id="btn-new-indicador" onclick="modalIndicador(this)" class="btn-new cont-btn-indic">
                    <span class="btn-icon">+</span>
                    <span class="btn-text">Indicador</span>
                  </div>
                  <div id="btn-new-criterio" onclick="modalCriterio(this)" class="cont-btn-crit btn-new">
                    <span class="btn-icon">+</span>
                    <span class="btn-text">Criterio</span>
                  </div>
                  <div id="btn-new-nivel" onclick="modalNivel(this)" class="cont-btn-nivel btn-new">
                    <span class="btn-icon">+</span>
                    <span class="btn-text">Nivel</span>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div id="check-instancia" class="check-instancia my-center">
            <i class="fa fa-check-circle" aria-hidden="true"></i>
          </div>
        </div>
      </div>
    </div>
    <div style="display: none">
      <div id="subirtarea">
        <div class="x_panel">
          <div class="x_title">
            <h2 style="font-size: 22px;"><i class="fa fa-tasks estarea"></i> <small class="estarea"><?php echo ucfirst(JrTexto::_('Tasks')); ?></small> <span class="estareaproyecto" style="display: none;">/</span>
              <i class="fa fa-folder esproyecto"></i> <small class="esproyecto"><?php echo ucfirst(JrTexto::_('Projects')); ?></small></h2>

            <div class="clearfix"></div>
          </div>
          <div class="x_content" style="border: 0px; padding: 0px; margin: 1ex 0px;">
            <ul class="nav nav-tabs bar_tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active idtab isa" id="listadomensajes-tab" data-toggle="tab" role="tab" href="#listadomensajes">Mensajes</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade show active idtab mensajeenviados" id="listadomensajes" role="tabpanel" aria-labelledby="listadomensajes-tab">
                <div id="chat">
                  <div id="mensajes">
                    <h2 class="text-center">No tiene Mensajes enviados</h2>
                  </div>
                </div>
                <div id="caja-mensaje">
                  <textarea id="_mensaje2_" class="form-control" rows="2" placeholder="<?php echo  ucfirst(JrTexto::_("Mensaje")) ?>" value="" style="resize: none;"></textarea>
                  <div class="text-center">
                    <button style="margin: 1ex" class="btn btn-primary btnsendmensaje"> Enviar Mensaje → </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 text-center" style="padding: 1ex;">
              <button class="salirmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar </button>
            </div>
          </div>
        </div>
      </div>

    </div>


  </div>

  <script type="text/javascript">
    /*addItemNavBar({
      text: '<?php echo JrTexto::_("Tareas"); ?>'
    });*/
    var idgrupoauladetalle;
    var formula_curso = {};
    $(document).ready(function() {
      $('select#idalumno').on('change', function(ev) {
        Swal.showLoading()
        var data = new FormData()
        data.append('idcurso', $('select#idcurso').val() || '');
        data.append('tipo', 'T');
        data.append('idcomplementario', $('#idcurso option:selected').data("idcomplementario"));
        data.append('idgrupoaula', $('#idcurso option:selected').attr("data-idgrupoaula"));
        data.append('idgrupoauladetalle', $('#idcurso option:selected').attr("data-idgrupoauladetalle"));
        if ($('select#idalumno').val() != '') data.append('idalumno', $('select#idalumno').val());
        __sysAyax({
          fromdata: data,
          url: _sysUrlBase_ + 'json/tareas',
          callback: function(rs) {
            html = '';
            // working
            $("#vistalisatotareas").children('.panelclone').siblings().remove();
            var haytareas = false;
            if (rs.code == 200) {
              $.each(rs.data, function(i, item) {
                haytareas = true;
                $panelclone = $('#vistalisatotareas').children('.panelclone').clone().show();
                $panelclone.removeClass('panelclone');
                $panelclone.children('a.panel-heading').attr('id', 'headding' + i).attr('href', '#collapse' + i);
                $panelclone.children('a.panel-heading').children('h4').children('small').text(item.nombre.trim() || '');
                $panelclone.children('.panel-collapse').attr('id', 'collapse' + i);
                formula_curso = item.formula_curso || {};
                var hayalu = item.dt;
                var hay = false;
                //let tarea = hayalu[0];
                var itmp = 0;
                $.each(hayalu, function(ii, v) {
                  let tarea = v;
                  if (itmp == 0) {
                    if (tarea.hasOwnProperty('fecha_limite') && typeof tarea.fecha_limite != 'undefined' && tarea.fecha_limite != '' && tarea.fecha_limite != null && tarea.fecha_limite != 0) {
                      $panelclone.find('.txt-fecha-limite').html('(' + tarea.fecha_limite + ')')
                    }
                    $panelclone.find('[name="btn-eliminar-tarea"]').attr('idpestania', tarea.idpestania || 0);
                    $panelclone.find('[name="btn-eliminar-tarea"]').attr('idsesion', tarea.idsesion);
                    $panelclone.find('[name="btn-eliminar-tarea"]').attr('idcurso', tarea.idcurso);
                    $panelclone.find('[name="btn-eliminar-tarea"]').attr('idcomplementario', tarea.idcomplementario);
                    $panelclone.find('[name="btn-eliminar-tarea"]').attr('idgrupoauladetalle', tarea.idgrupoauladetalle);
                  }
                  itmp++;
                  // working

                  // console.log('info que necesito', v);
                  $trclone = $panelclone.find('.trclone').clone();
                  $trclone.removeClass('trclone').show();
                  $trclone.attr('id', v.idtarea).attr('tipo', v.tipo).attr('idalumno', v.idalumno)

                  $trclone.children('.nombre').text(v.nombre_alumno);
                  var arch = v.archivos;
                  var hayarchivos = false;
                  $.each(arch, function(iar, arc) {
                    $trclone.find('.archivoalumno').attr('href', _sysUrlBase_ + arc.media);
                    hayarchivos = true;
                  })
                  if (hayarchivos == false) {
                    $trclone.find('.archivoalumno').closest('td').text('-----');
                  }
                  let strVencido = v.vencido == 1 ? '<div>(N.P.)</div>' : '';
                  $trclone.find('.vernota').html(v.nota + strVencido);
                  //mine
                  $trclone.find('.vernota').attr('idcursodetalle', v.idsesion);
                  $trclone.find('.vernota').attr('idpestania', v.idpestania);
                  $trclone.find('.vernota').attr('idcomplementario', v.idcomplementario);
                  $trclone.find('.vernota').attr('idalumno', v.idalumno);
                  $trclone.find('.vernota').attr('idcurso', v.idcurso);
                  //
                  $trclone.find('a.subirtarea').attr('tipo', v.tipo).attr('idcurso', v.idcurso).attr('idsesion', v.idsesion).attr('idpestania', v.idpestania).attr('data-link', v.recurso).attr('data-tipolink', v.tiporecurso).attr('iddocente', v.iddocente).attr('data-nombre', v.nombre).attr('data-descripcion', v.descripcion).attr('criterios', v.criterios).attr('idtarea', v.idtarea);

                  var cripuntaje = v.criterios_puntaje || 'ninguno';
                  if (cripuntaje != 'ninguno') cripuntaje = btoa(cripuntaje);
                  $trclone.find('.vernota').attr('criterios', v.criterios).attr('criterios_puntaje', cripuntaje);
                  $panelclone.find('tbody').append($trclone);
                  hay = true;
                })
                if (hay == false) $panelclone.find('table').children('tbody').html('<tr ><th class="text-center" colspan="3"><h4>Ningun estudiante a realizado esta tarea.</h4></th></tr>');
                //$panelclone.find('a.archivodocente').attr('href',item.recurso);                  
                $("#vistalisatotareas").append($panelclone);

              });
              const oCrudTarea = new CrudTarea();
              oCrudTarea.loadModuloTareas({
                section: document,
                name: 'btn-eliminar-tarea'
              })
              Swal.close();
            }
            if (haytareas == false) {
              $("#vistalisatotareas").append('<table class="table"><thead><tr class=""><th class="text-center"><h3>Este curso no tiene Tareas asignadas.</h3></th></tr></thead></table>');
            }
          }
        });
      })

      $('#vistalisatotareas').on('click', '.vernota', function(ev) { //cuando da click en la nota
        var el = $(this);
        var nota = $(this).text();
        var criterios_puntaje = el.attr('criterios_puntaje') || 'ninguno';
        if (criterios_puntaje != '' && criterios_puntaje != 'ninguno') {
          try {
            criterios_puntaje = JSON.parse(atob(criterios_puntaje));
          } catch (ex) {
            console.log(ex)
          }
        } else
          criterios_puntaje = [];
        var idtarea = $(this).closest('tr').attr('id') || 0;
        // working modal //busca criterios
        var htmltarea = $('#idcriterios').find('#notas-body').clone(true);
        var dt = {
          html: htmltarea
        };
        var md = __sysmodal(dt);
        //mine
        let searchInstancia = {
          idcursodetalle: $(el).attr('idcursodetalle'),
          idpestania: $(el).attr('idpestania'),
          idcomplementario: $(el).attr('idcomplementario'),
          idcurso: $(el).attr('idcurso')
        }
        _idalumno = $(el).attr('idalumno');
        _idcurso = $(el).attr('idcurso');
        getConfiguracionNota(_idcurso);
        _instancia = null;
        hasInstance(searchInstancia).then(
          instanciaRubrica => {
            _instancia = instanciaRubrica;
            postData(_sysUrlBase_ + 'json/rubrica/listado', {
                idrubrica: instanciaRubrica.idrubrica,
                listAll:true
              })
              .then((arrRubrica) => {
                $(md).find('.modal-body').addClass('soft-rw my-font-all');
                renderCalificarRubrica(arrRubrica, md, 2);
              })
            // console.log('instanciaRubrica', instanciaRubrica);
          }
        ).catch(e => {
          $(md).find('#my-loader').addClass('my-hide'); // console.log('no hay instancias');          
        });
        //
        let tipocalificacion = formula_curso.tipocalificacion || 'N';
        let maxcal = parseInt(formula_curso.maxcal || 20);
        let mixcal = parseInt(formula_curso.mixcal || 0);
        if (tipocalificacion == 'A') {
          let puntuacion = formula_curso.formula_curso || [{
            maxcal: 100
          }];
          puntuacion.forEach(function(v, i) {
            maxcal = parseInt(v.maxcal);
          })
        }
        md.find('#span-my-base').text(maxcal);
        md.find('input.notafinal').attr('max', maxcal);
        var data = new FormData()
        data.append('idcriterio', $(this).attr('criterios') || 'ninguno');
        __sysAyax({
          fromdata: data,
          url: _sysUrlBase_ + 'json/acad_criterios',
          callback: function(rs) {
            if (rs.data.length > 0) { //si hay criterios los dibuja en la tabla
              $.each(rs.data, function(rr, vv) {
                var tr = md.find('.trclone').clone().show().removeClass('trclone');
                tr.attr('idcriterio', vv.idcriterio);
                tr.find('.criterio').text(vv.nombre);
                tr.find('input').val(0);
                tr.find('input').attr('idcriterio', vv.idcriterio);
                $.each(criterios_puntaje, function(iii, c) {
                  if (vv.idcriterio == c.id) tr.find('input').val(c.nota);
                })
                md.find('tbody').append(tr);
              })
              md.find('input.nota').last().trigger('change');
              md.find('input.notafinal').attr('readonly', 'readonly');
            } else { //sino borra la tabla
              md.find('input.notafinal').removeAttr('readonly');
              md.find('table').remove();
              md.find('input.notafinal').val(parseInt(el.text()))
            }
          }
        })
        md.on('change', 'input.nota', function(ev) { //cuando digita una nota dentro de la tabla de criterios
          var inputs = md.find('input.nota');
          var sum = 0;
          if (inputs.length) {
            let n = inputs.length > 0 ? inputs.length - 1 : 1;
            $.each(inputs, function(i, v) {
              sum = sum + parseInt($(v).val()) || 0;
            })

            let notafinal = sum / n;
            notafinal = (notafinal * maxcal) / 100;
            md.find('input.notafinal').val(notafinal.toFixed(2)); //modifica la nota final            
          } else {
            md.find('input.notafinal').val(nota); //deja la nota tal como está
          }
        }).on('click', '.btnguardar', function(ev) { //boton de modal al guardar nota
          var inputs = md.find('input.nota');
          var hayerror = false;
          var criterios = [];
          if (inputs.length) {
            $.each(inputs, function(i, v) {
              //sum = sum + parseInt($(v).val())||0;
              if (!$(v).closest('tr').hasClass('trclone')) {
                var notatmp = $(v).val() || 0;
                criterios.push({
                  'id': $(v).attr('idcriterio'),
                  "nota": notatmp
                });
                if (notatmp < 0 && notatmp > 100) {
                  $(v).css({
                    "border": "1px solid red"
                  });
                  hayerror = true;
                } else $(v).css({
                  "border": "1px solid blue"
                });
              }
            })
            if (hayerror == false)
              el.attr('criterios_puntaje', btoa(JSON.stringify(criterios)));
            /*if (sum > 100) {
              md.find('input.notafinal').css({"border": "1px solid red"}).attr('readonly', 'readonly');
              return false;
            } else md.find('input.notafinal').css({"border": "1px solid blue" })
            */
          } /*else {*/
          let notafinal = md.find('input.notafinal').val();
          if (notafinal < 0 && notafinal > maxcal) hayerror = true;
          if (hayerror == true) {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Verifique los valores ingresados',
              // footer: '<a href>Why do I have this issue?</a>'
            })
            return false;
          }

          //}
          var da = new FormData()
          da.append('idtarea', idtarea);
          da.append('nota', notafinal);
          da.append('notabaseen', maxcal);
          if (_instancia) {
            saveIndicadorAlumno();
          }
          // working mine return null;
          if (inputs.length) da.append('criterios', JSON.stringify(criterios));
          __sysAyax({
            fromdata: da,
            url: _sysUrlBase_ + 'json/tareas/updateNota',
            callback: function(rs) {
              el.text(notafinal);
              __cerrarmodal(md);
            }
          })
        }).on('blur', 'input.notafinal', function(ev) {
          let nf = parseFloat($(this).val());
          let nmax = parseFloat($(this).attr('max'));
          console.log(nf, nmax);
          if (nf < 0 || nmax < nf) {
            $(this).css({
              "border": "1px solid red"
            });
            $(this).focus()
          } else $(this).css({
            "border": "1px solid #4683af"
          });
        })
      }).on('click', '.subirtarea', function(ev) {
        var htmltarea = $('#subirtarea').clone(true);
        htmltarea.find('.idtab').each(function(i, v) {
          if ($(v).hasClass('isa')) $(v).attr('href', $(v).attr('href') + contab);
          $(v).attr('id', $(v).attr('id') + contab);
        })

        var el = $(this);
        var tipotarea = '';
        if (el.attr('tipo') == 'TP') {
          htmltarea.find('.estareaproyecto').show();
          tipotarea = 'TP';
        } else if (el.attr('tipo') == 'T') {
          htmltarea.find('.esproyecto').remove();
          tipotarea = 'T';
        } else if (el.attr('tipo') == 'P') {
          htmltarea.find('.estarea').remove();
          tipotarea = 'P';
        }
        var dt = {
          html: htmltarea
        };
        //console.log('dt',dt);
        //2do modal
        var md = __sysmodal(dt);
        var _cargarmensajesyarchivos = function() {
          var formData = new FormData();
          formData.append('idcurso', parseInt(el.attr('idcurso')));
          formData.append('idsesion', parseInt(el.attr('idsesion')));
          formData.append('idpestania', parseInt(el.attr('idpestania') || '0'));
          formData.append('tipo', tipotarea);
          formData.append('idtarea', el.attr('idtarea'));
          formData.append('idcomplementario', $('#idcurso option:selected').data("idcomplementario"));
          formData.append('idgrupoaula', $('#idcurso option:selected').attr("data-idgrupoaula"));
          formData.append('idgrupoauladetalle', $('#idcurso option:selected').attr("data-idgrupoauladetalle"));
          __sysajax({
            fromdata: formData,
            url: _sysUrlBase_ + 'json/tareas/archivosalumnoymensajes',
            showmsjok: false,
            callback: function(rs) {
              $pnlmensajes = md.find('.mensajeenviados #mensajes');
              if (rs.mensajes) {
                $pnlmensajes.html('');
                var yo = '<?php echo $this->usuarioAct["idpersona"] ?>';
                $.each(rs.mensajes, function(i, v) {
                  var fotouser = (v.foto == '' || v.foto == null) ? 'static/media/usuarios/user_avatar.jpg' : v.foto;
                  var ifoto = fotouser.lastIndexOf('static/');
                  if (ifoto == -1) {
                    fotouser = _sysUrlBase_ + 'static/media/usuarios/user_avatar.jpg';
                  } else fotouser = _sysUrlBase_ + fotouser.substring(ifoto);
                  //  console.log('usu'+v.idusuario,"yo:"+yo);
                  if (v.idusuario == yo) {
                    var msj = '<div class="mensaje-autor"><img src="' + fotouser + '" alt="" class="foto img-responsive"><div class="flecha-izquierda"></div><div class="contenido"><b>' + v.usuario + '</b><br>' + v.mensaje + '</div><div class="fecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                  } else {
                    msj = '<div class="mensaje-amigo" ><div class="contenido">' + v.mensaje + '</div><div class="flecha-derecha"></div><img src="' + fotouser + '" alt="" class="foto img_responsive"><div class="fecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                  }
                  $pnlmensajes.append(msj);
                })
              }
              $pnlarchivos = md.find('.archivosenviados table tbody');
              if (rs.archivos) {
                $pnlarchivos.html('');
                $.each(rs.archivos, function(i, v) {
                  var descripcion = v.media.split("/trabajos/")[1].split("?")[0];
                  var html = '<tr><td><a href="' + _sysUrlBase_ + v.media + '" target="_blank" download class="btn btn-sm"> <i class="fa fa-download"></i> ' + descripcion + '</a></td><td>' + v.fecha_registro + '</td><td class="text-center"> <i class="fa fa-trash borrararchivosubido" style="cursor:pointer" id="' + v.idtarearecurso + '"></i></tr>';
                  $pnlarchivos.append(html);
                })
              }
            }
          })
        }
        var _guardartarea = function(solomsj = false) {
          var link = el.attr('data-link');
          var tipolink = el.attr('data-tipolink');
          if (tipolink == 'smartquiz') link = _sysUrlSitio_ + link.substring(link.lastIndexOf("/quiz/"));
          else link = _sysUrlBase_ + link.substring(link.lastIndexOf("static/"));
          var tipomedia = md.find('#infolink').attr('tipolink') || '';
          if (tipomedia == 'texto' || tipomedia == 'link') mediainfo = md.find('#infolink').attr('value') || '';
          else mediainfo = md.find('#infolink').val() || '';
          if (mediainfo == undefined) mediainfo = '';
          $mensaje = md.find('#_mensaje_').val() || '';
          if (mediainfo != '' && $mensaje != '') {
            $mensaje = '<a href="' + _sysUrlBase_ + mediainfo + '" class="btn btn-primary btn-xs" download="" target="_blank"><i class="fa fa-download"></i> File</a><br>' + $mensaje;
          }
          var formData = new FormData();
          iddocente = '<?php echo $this->usuarioAct["idpersona"]; ?>';
          formData.append('tipo', tipotarea);
          formData.append('idcurso', parseInt(el.attr('idcurso')));
          formData.append('idsesion', parseInt(el.attr('idsesion')));
          formData.append('idpestania', parseInt(el.attr('idpestania') || '0'));
          formData.append('idtarea', parseInt(el.attr('idtarea')));
          formData.append('recurso', link);
          formData.append('tiporecurso', tipolink);
          formData.append('iddocente', iddocente);
          formData.append('nombre', el.attr('data-nombre') || '');
          formData.append('descripcion', el.attr('data-descripcion') || '');
          formData.append('media', mediainfo);
          formData.append('tipomedia', tipomedia);
          formData.append('criterios', el.attr('criterios') || '');
          formData.append('idcomplementario', $('#idcurso option:selected').data("idcomplementario"));
          formData.append('idgrupoaula', $('#idcurso option:selected').attr("data-idgrupoaula"));
          formData.append('idgrupoauladetalle', $('#idcurso option:selected').attr("data-idgrupoauladetalle"));
          if (solomsj) {
            formData.append('solomensaje', true);
            formData.append('mensaje', md.find('#_mensaje2_').val() || '');
            md.find('#_mensaje2_').val('');
          } else {
            formData.append('mensaje', $mensaje);
            md.find('#_mensaje_').val('');
          }
          __sysajax({
            fromdata: formData,
            url: _sysUrlBase_ + 'json/tareas/guardar',
            callback: function(rs) {
              _cargarmensajesyarchivos();
            }
          })
        }
        md.on('click', '.salirmodal', function(ev) {
          __cerrarmodal(md, true);
        }).on('click', '.btnguardartarea', function(ev) {
          _guardartarea(false);
        }).on('click', '.btntipofile', function(ev) {
          var el_ = $(this);
          el_.siblings().removeClass('btn-primary').addClass('btn-secondary');
          el_.addClass('btn-primary').removeClass('btn-secondary');
          var title = (el_.attr('title') || 'pdf').toLowerCase();
          var aquitipofile = md.find('#aquitipofile');
          if (title == 'texto') {
            aquitipofile.html('<br><textarea id="infolink" tipolink="texto" class="form-control" rows="5" placeholder="Copie o escriba su texto de repuesta"  style="resize: none;"></textarea>');
          } else if (title == 'imagen') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink" tipolink="imagen" ><div><img src="" class="img-responsive" id="' + idimagen + '" style="max-width:130px; max-height:130px;"></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'imagen',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              //console.log(rs);
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('src', url_media + f);
            });

          } else if (title == 'video') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink"  tipolink="video"><div><video controls src="" class="img-responsive" id="' + idimagen + '" style="max-width:130px; max-height:130px;"></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'video',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('src', url_media + f);
            });

          } else if (title == 'audio') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink"  tipolink="audio"><div><audio controls src="" class="img-responsive" id="' + idimagen + '"></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'audio',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('src', url_media + f);
            });

          } else if (title == 'html') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink"  tipolink="html"><div><a target="_blank" src="img" class="img-responsive" id="' + idimagen + '">Link subido</a></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'html',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('href', url_media + f);
            });

          } else if (title == 'pdf') {
            var idimagen = __idgui();
            aquitipofile.html('<input type="hidden" id="infolink"  tipolink="pdf"><div><a target="_blank" src="img" class="img-responsive" id="' + idimagen + '">PDF subido</a></div>');
            $img = aquitipofile.find('#' + idimagen);
            __subirfile_({
              file: $img,
              typefile: 'pdf',
              uploadtmp: true,
              guardar: true,
              dirmedia: 'trabajos/'
            }, function(rs) {
              let f = rs.media.replace(url_media, '');
              aquitipofile.find('#infolink').attr('value', f);
              $img.attr('href', url_media + f);
            });

          } else if (title == 'link') {
            aquitipofile.html('<br><input id="infolink"  tipolink="link" value="" placeholder="Copie o escriba el enlace " class="form-control"> ');
          }
        }).on('click', '.borrararchivosubido', function(ev) {
          var el_ = $(this);
          var formData = new FormData();
          formData.append('idtarearecurso', parseInt(el_.attr('id')));
          __sysajax({
            fromdata: formData,
            url: _sysUrlBase_ + 'json/tareas_archivosalumno/eliminar',
            showmsjok: false,
            callback: function(rs) {
              el_.closest('tr').remove();
            }
          })
        }).on('click', '.btnsendmensaje', function(ev) {
          _guardartarea(true);
        })
        _cargarmensajesyarchivos();
        contab++;
      })
      var contab = 0;
      $('select#idgrupoaula').change(function(ev) {
        Swal.close();
        Swal.showLoading()
        $('select#idcurso').children('option').hide();
        $('select#idcurso option[data-idgrupoaula="' + $(this).val() + '"]').show();
        $('select#idcurso option[data-idgrupoaula="' + $(this).val() + '"]').first().attr('selected', "selected");
        $('select#idcurso').trigger('change');
      })
      $('select#idcurso').change(function(ev) {
        Swal.close();
        Swal.showLoading();
        idgrupoauladetalle = $('select#idcurso option:selected').attr('data-idgrupoauladetalle') || 0;
        $('select#idalumno').children('option').first().siblings().hide();
        $('select#idalumno option[data-idgrupoauladetalle="' + idgrupoauladetalle + '"]').show();
        $('select#idalumno option[data-idgrupoauladetalle="' + idgrupoauladetalle + '"]').first().attr('selected', "selected");
        $('select#idalumno').val('');
        $('select#idalumno').trigger('change');
      })
      $('select#idgrupoaula').trigger('change');
    })
  </script>
  <?php //var_dump($this->gruposauladetalle); 
  ?>