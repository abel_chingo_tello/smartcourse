<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
<?php } ?>
<style type="text/css">
  .puntaje{
    position: absolute; right: 35px; top: 35px;
  }
  .puntaje small{
    font-weight: 1.5em;
  }
  #chat #header-chat {
    background-color: #555;
    color: #20f1f14d;
    padding: 10px;
    text-align: center;
    text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.6);
  }

  #chat #mensajes {
    padding: 10px;
    min-height: 200px;
    max-height: 400px;
    width: 100%;
    overflow: hidden;
    overflow-y: scroll
  }

  #chat #mensajes .mensaje-autor {
    margin-bottom: 20px;
    float: left;
  }

  #chat #mensajes .mensaje-autor img,
  #chat #mensajes .mensaje-amigo img {
    display: inline-block;
    vertical-align: top;
    max-width: 50px;
    max-height: 50px;
  }

  #chat #mensajes .mensaje-autor .contenido {
    background-color: #20f1f14d;
    border-radius: 5px;
    box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);
    display: inline-block;
    font-size: 13px;
    padding: 15px;
    vertical-align: top;
  }

  #chat #mensajes .mensaje-autor .fecha {
    color: #777;
    font-style: italic;
    font-size: 13px;
    text-align: right;
    margin-right: 35px;
    margin-top: 10px;
  }

  #chat #mensajes .mensaje-autor .flecha-izquierda {
    display: inline-block;
    margin-right: -6px;
    margin-top: 10px;
    width: 0;
    height: 0;
    border-top: 0px solid transparent;
    border-bottom: 15px solid transparent;
    border-right: 15px solid #20f1f14d;
  }

  #chat #mensajes .mensaje-amigo {
    margin-bottom: 20px;
    float: right;
  }

  #chat #mensajes .mensaje-amigo .contenido {
    background-color: #3990BF;
    border-radius: 5px;
    color: white;
    display: inline-block;
    font-size: 13px;
    padding: 15px;
    vertical-align: top;
  }

  #chat #mensajes .mensaje-amigo .flecha-derecha {
    display: inline-block;
    margin-left: -6px;
    margin-top: 10px;
    width: 0;
    height: 0;
    border-top: 0px solid transparent;
    border-bottom: 15px solid transparent;
    border-left: 15px solid #3990BF;
  }

  #chat #mensajes .mensaje-amigo img,
  #chat #mensajes .mensaje-autor img {
    border-radius: 5px;
  }

  #chat #mensajes .mensaje-amigo .fecha {
    color: #777;
    font-style: italic;
    font-size: 11px;
    text-align: left;
    margin-top: 0px;
  }
</style>
<div id="working-v2" class="row my-font-all">
  <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
    <label class="control-label"><?php echo JrTexto::_('Mis cursos'); ?></label>
    <select id="idcurso" name="idcurso" class="form-control" idsel="<?php echo @$this->idcurso; ?>" >        
      <?php if(!empty($this->gruposauladetalle))
        foreach ($this->gruposauladetalle as $fk){
        $idcursos=array('207');
          if(!in_array($fk["idcurso"], $idcursos)){?>
        <option value="<?php echo $fk["idcurso"]?>"  data-idgrupoaula="<?php echo $fk["idgrupoaula"] ?>" data-idgrupoauladetalle="<?php echo $fk["idgrupoauladetalle"] ?>" iddocente="<?php echo $fk["iddocente"]; ?>" docente="<?php echo $fk["strdocente"]; ?>"  data-idcomplementario="<?php echo $fk["idcomplementario"]?>"><?php echo $fk["nombrecurso"] ?></option>
      <?php } } ?>
    </select>
  </div>
  <div class="col-md-4">
    
  </div>
  <div class="col-md-12 " id="infopnltareas">
    <div class="row">
    <div class="accordion" id="vistalisatotareas" role="tablist" aria-multiselectable="true" style="width: 100%">
      <div class="panel panelclone col-md-12" style="display: none;">
        <a  onclick="my_toogle()" class="panel-heading collapsed  my-card my-shadow my-del-mw" role="tab" id="heading" data-toggle="collapse" href="#collapse" aria-expanded="false" aria-controls="collapse">
          <h4 class="panel-title my-y-center"><span class="label"><?=JrTexto::_("Project")?>: </span><small></small> <span class="puntaje"> <span class="my-color-black"><?=JrTexto::_("Score")?>:</span><small class="notapuntaje">20</small></span><span class="m-drop"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></h4>
        </a>
        <div class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading" style="">
          <div class="panel-body  my-card my-shadow my-del-mw"> 
            <div class="row">
              <div class="col-md-6 col-sm-12 text-center table-responsive">
                <table class="table ">
                  <thead><tr class="table-secondary"><th colspan="3" class="text-center"><strong><?=JrTexto::_("Project information")?></strong></th></tr></thead>
                  <tbody>
                    <tr class="table-secondary"><th class="text-left my-second-text"><?=JrTexto::_("Teacher")?>:</th><td> </td><td class="nombre_docente nombres"> Abel  chingo Tello</td></tr>
                    <tr class="table-secondary"><th class="text-left  my-second-text"><?=JrTexto::_("Project archive")?>:</th><td>  </td><td> <a href="#" class="archivodocente" target="_blank"><i class="fa fa-cloud-download" style="font-size: 2em"></i><br><?=JrTexto::_("View or download")?></a></td></tr>
                  </tbody>
                </table>            
              </div>
              <div class="col-md-6 col-sm-12 text-center table-responsive">
                <table class="table my-left-separator">
                  <thead><tr class=""><th colspan="4" class="text-center"><strong><?=JrTexto::_("Your answer")?></strong></th></tr></thead>
                  <tbody>
                    <tr><th class="text-left  my-second-text"><?=JrTexto::_("Student")?>:</th><td>  </td><td class="nombre_alumno nombres"> Abel  chingo Tello</td></tr>
                    <tr><th class="text-left  my-second-text"><?=JrTexto::_("Resolution file")?>:</th><td> </td><td> <a href="#" class="archivoalumno" target="_blank"><i class="fa fa-cloud-download" style="font-size: 2em"></i><br> <?=JrTexto::_("View or download")?> </a></td> <td> <a href="#" class="subirtarea"><i class="fa fa-cloud-upload" style="font-size: 2em"></i><br> <?=JrTexto::_("Upload file")?></a></td></tr>                   
                  </tbody>
                </table>            
              </div>
              <div class="col-md-12" style="padding: 3ex 3ex 0 3ex"><b><?=JrTexto::_("Last message")?></b> <small class="fechaultimomensaje hora"> - - </small> <a href="#" class=" btn subirtarea" modo="chat" style="border: 1px solid rgb(131 180 216);  box-shadow: 5px -5px 10px -4px rgba(168,166,168,1);"> <?=JrTexto::_("See Chat")?> <i class="fa fa-comments-o"></i> </a><br>
                <div class="alert alert-info ultimomensaje"><i><?=JrTexto::_("You don't have messages yet")?>!</i></div>
              </div>
            </div>         
          </div>
      </div>    
    </div>
</div>
        
  </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center" id="sindatos" style="display:none">
  <div class="container">
    <div class="row">
      <div class="col-md-12 my-x-center">
        <div class="jumbotron my-card my-shadow">
            <!-- <h1><strong>Opss!</strong></h1>                 -->
            <h1 class="disculpa my-second-text"><?php echo JrTexto::_('We have not found projects assigned to the course')?></h1>                
        </div>
      </div>
    </div>
  </div>
</div>

<div id="idcriterios" style="display: none">
    <div class="col-md-12 table-responsive criterio">
      <table class="table table-striped">
        <thead><tr><th><?=JrTexto::_("Criterion")?></th><th><?=JrTexto::_("Note")?> 0-100</th></tr></thead>
        <tbody><tr class="trclone" style="display: none"><td class="criterio"></td><td ><input readonly="readonly" style="border:0;" class="nota" type="number" min="0" max="100" ></td></tr></tbody>
      </table>
      <div class="form-group col-md-12">
        <label class="col-md-6 text-right"><?=JrTexto::_("Final note")?>  0-100</label>
        <input type="number" name="notafinal" min="0" max="100" class="notafinal form-control col-md-6" value="0.0">
      </div>
      <div class="clearfix"></div><hr>
      <div class="col-md-12 text-center">
         <a href="#" class="btn btn-warning cerrarmodal"><?=JrTexto::_("Close")?></a>
         <a href="#" class="btn btn-primary btnguardar"><i class="fa fa-save"></i> <?=JrTexto::_("Save")?></a>
      </div>      
    </div>    
  </div>
  <div style="display: none">
    <div id="subirtarea">
    <div class="x_panel">
      <div class="x_title">
        <h2 style="font-size: 22px;"><i class="fa fa-tasks estarea"></i> <small class="estarea"><?php echo ucfirst(JrTexto::_('Tasks')); ?></small> <span class="estareaproyecto" style="display: none;">/</span>
          <i class="fa fa-folder esproyecto"></i> <small class="esproyecto"><?php echo ucfirst(JrTexto::_('Projects')); ?></small></h2>

        <div class="clearfix"></div>
      </div>
      <div class="x_content" style="padding: 0; margin: 1ex 0; border: 0px;">
        <ul class="nav nav-tabs bar_tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active idtab isa" id="frmsubirarchivo_tab" data-toggle="tab" role="tab" href="#frmsubirarchivo"><?=JrTexto::_("Upload File")?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link idtab isa" id="listadomensajes-tab" data-toggle="tab" role="tab" href="#listadomensajes"><?=JrTexto::_("Message")?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link idtab isa" id="listadoarchivos-tab" data-toggle="tab" role="tab" href="#listadoarchivos"><?=JrTexto::_("Uploaded files")?></a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade show active idtab" id="frmsubirarchivo" role="tabpanel" aria-labelledby="frmsubirarchivo_tab">
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                  <?=JrTexto::_("Select the type of file you want to send to your teacher, write a message and press the send button")?>
                </div>
              </div>
              <div class="col-md-12">
                <form class="row" id="frmarchivosmultimedia" method="post" target="" enctype="multipart/form-data">
                  <div class="col-md-6 form-group">
                    <label><?php echo JrTexto::_("Type file") ?></label>
                    <br>
                    <div class="btn-group" role="group" aria-label="">
                      <!--button type="button" class="btn btn-secondary btntipofile" title="texto"><i class="fa fa-font"></i></button-->
                      <button type="button" class="btn btn-secondary btntipofile" title="imagen"><i class="fa fa-photo"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="audio"><i class="fa fa-file-audio-o"></i></button>
                        <button type="button" class="btn btn-secondary btntipofile" title="video"><i class="fa fa-file-video-o"></i></button>
                        <button type="button" class="btn btn-secondary btntipofile" title="pdf"><i class="fa fa-file-pdf-o"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="html"><i class="fa fa-html5"></i></button>
                      <button type="button" class="btn btn-secondary btntipofile" title="link"><i class="fa fa-link"></i></button>
                    </div>
                  </div>
                  <div class="col-md-6" id="aquitipofile"></div>
                  <div class="col-md-12 form-group">
                    <label style=""><?php echo JrTexto::_('Message'); ?> <i class="fa fa-comment"></i> </label>
                    <textarea id="_mensaje_" class="form-control" rows="4" placeholder="<?php echo  ucfirst(JrTexto::_("Message")) ?>" value="" style="resize: none;"></textarea>
                  </div>
                </form>
              </div>
              <div class="col-md-12 text-center">
                <button class="btnguardartarea btn btn-primary "> <i class="fa fa-envelope-o"></i> <?=JrTexto::_("Send Message")?></button>
              </div>
            </div>
          </div>
          <div class="tab-pane fade idtab mensajeenviados" id="listadomensajes" role="tabpanel" aria-labelledby="listadomensajes-tab">
            <div id="chat">
              <div id="mensajes">
                <h2 class="text-center"><?=JrTexto::_("You have no messages sent")?></h2>
              </div>
            </div>
            <div id="caja-mensaje">
              <textarea id="_mensaje2_" class="form-control" rows="2" placeholder="<?php echo  ucfirst(JrTexto::_("Message")) ?>" value="" style="resize: none;"></textarea>
              <div class="text-center">
                <button style="margin: 1ex" class="btn btn-primary btnsendmensaje"> <?=JrTexto::_("Send Message")?> → </button>
              </div>
            </div>
          </div>
          <div class="tab-pane table-responsive fade idtab archivosenviados" id="listadoarchivos" role="tabpanel" aria-labelledby="listadoarchivos-tab">
            <table class="table  table-striped">
              <thead>
                <tr>
                  <th><?=JrTexto::_("File")?></th>
                  <th><?=JrTexto::_("Date")?></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="3"><?=JrTexto::_("You have not yet submitted files from this job")?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 text-center" style="padding: 1ex;">
          <button class="salirmodal btn btn-warning"><i class="fa fa-close"></i> <?=JrTexto::_("Close")?> </button>
        </div>
      </div>
    </div>
  </div>
    
  </div>


<script type="text/javascript">
    var cardOpened = false;
    var boxBody=document.querySelectorAll('.panel-collapse');
    var boxHead;
    function my_toogle() {
      document.querySelector('#headding0').classList.add("my-no-shadow");
    }
   addItemNavBar({
      text: '<?php echo JrTexto::_("Projects"); ?>'
    });
  $(document).ready(function(){
    $('select#idcurso').change(function(ev){
      cargardatos();
    })
    var cargardatos=function(){
      $('#sindatos').hide();
      $("#vistalisatotareas").find('.panelclone').siblings('.panel').remove();
    var data=new FormData()           
      data.append('idcurso',$('select#idcurso').val()||'');
      data.append('tipo','P');
      data.append('idcomplementario', $('#idcurso option:selected').attr("data-idcomplementario"));
      data.append('idgrupoaula', $('#idcurso option:selected').attr("data-idgrupoaula"));
      data.append('idgrupoauladetalle', $('#idcurso option:selected').attr("data-idgrupoauladetalle"));
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/tareas/ver',
          callback:function(rs){
            html='';
            if(rs.code==200){
              if(rs.data!=undefined)
              if(rs.data.length>0){
                $.each(rs.data, function(i, item){
                  let cursoselec=$('select#idcurso option:selected');            
                  let idcurso=item.idcurso||$('select#idcurso').val();
                  let nomtarea=(item.nombre||item.nombreunico||'Proyecto sin nombre').trim();
                  $panelclone=$('#vistalisatotareas').children('.panelclone').clone().show();
                  $panelclone.removeClass('panelclone');
                  $panelclone.children('a.panel-heading').attr('id','headding'+i).attr('href','#collapse'+i);
                  $panelclone.children('a.panel-heading').children('h4').children('small').text(nomtarea);
                  $panelclone.children('.panel-collapse').attr('id','collapse'+i);
                   let recurso=item.recurso||item.link;
                  $panelclone.find('a.archivodocente').attr('href',recurso);
                  let docente=item.nombre_docente||cursoselec.attr('docente');
                  let iddocente=item.iddocente||cursoselec.attr('iddocente');
                  $panelclone.find('.nombre_docente').text(docente);
                  $panelclone.find('.nombre_alumno').text(item.nombre_alumno);
                   item.archivos=item.archivos||'';
                  item.tipo=(item.tipo=='T'||item.tipo=='estarea')?'T':'P';  
                  if(item.archivos.length){
                    $.each(item.archivos,function(ii,a){                  
                      $panelclone.find('a.archivoalumno').attr('href',_sysUrlBase_+a.media);
                    })
                  } else $panelclone.find('a.archivoalumno').closest('td').text('----');
                  $panelclone.find('.subirtarea').attr('tipo',item.tipo).attr('idcurso',idcurso).attr('idsesion',item.idsesion).attr('idpestania',item.idpestania).attr('data-link',recurso).attr('data-tipolink',item.tiporecurso).attr('iddocente',iddocente).attr('data-nombre',nomtarea).attr('data-descripcion',item.descripcion||'').attr('criterios',item.criterios||'');
  
                  item.mensajes=item.mensajes||'';
                  if(item.mensajes.length)
                  $.each(item.mensajes,function(ii,a){
                    if(a.rol==3){
                      $panelclone.find('.ultimomensaje').html(a.mensaje);
                      $panelclone.find('.fechaultimomensaje').text('('+a.fecha_registro+')');
                    }
                  })
                  if (item.nota >= 0&&item.fecha_calificacion!=null) {
                  var criterios_puntaje=item.criterios_puntaje||'ninguno';
                  if(criterios_puntaje!='' && criterios_puntaje!='ninguno') criterios_puntaje=btoa(criterios_puntaje);
                  $panelclone.find('.notapuntaje').attr('criterios', item.criterios).attr('idtarea', item.idtarea).attr('nota', item.nota).attr('criterios_puntaje', criterios_puntaje).addClass('verpuntajecriterios').text(item.nota);
                    $panelclone.find('.notapuntaje').parent().prepend(`<i class="fa fa-check-circle entregada" aria-hidden="true"> </i> `);
                  } else $panelclone.find('.notapuntaje').parent().html(`<span class="second-text"><i class="fa fa-exclamation-circle pendiente" aria-hidden="true"></i> <?=JrTexto::_("Uns qualified")?></span>`);
                    $("#vistalisatotareas").append($panelclone);
                });
              }else{
                $('#sindatos').show();
              }
            }
          }
      });
    }
    cargardatos();
var contab = 0;

  $('#infopnltareas').on('click','.verpuntajecriterios',function(ev){
    var el =$(this);
    var nota=el.attr('nota')||0;
    var criterios_puntaje=el.attr('criterios_puntaje')||'ninguno';
    if(criterios_puntaje!='' && criterios_puntaje!='ninguno'){
      try{
        criterios_puntaje=JSON.parse(atob(criterios_puntaje));
      }catch(ex){console.log(ex)}
    }else 
      criterios_puntaje=[];
    var idtarea=el.attr('idtarea')||0;
    var htmltarea = $('#idcriterios').clone(true).html();
    var dt = { html: htmltarea };
    var md = __sysmodal(dt);
    var data=new FormData()           
      data.append('idcriterio',el.attr('criterios')||'ninguno');
     __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/acad_criterios',
          callback:function(rs){            
            if(rs.data.length>0){
              $.each(rs.data,function(rr,vv){
                var tr=md.find('.trclone').clone().show().removeClass('trclone');
                tr.attr('idcriterio',vv.idcriterio);
                tr.find('.criterio').text(vv.nombre);
                tr.find('input').val(0);
                tr.find('input').attr('idcriterio',vv.idcriterio);
                $.each(criterios_puntaje,function(iii,c){
                  if(vv.idcriterio==c.id) tr.find('input').val(c.nota);  
                })
                md.find('tbody').append(tr);    
              })
              md.find('input.nota').last().trigger('change');

            }else{
              md.find('table').remove();
              md.find('input.notafinal').val(parseInt(el.text()))
            }          
            
          }
        })
     md.on('change','input.nota',function(ev){
      var inputs=md.find('input.nota');
      var sum=0;      
      if(inputs.length){
        $.each(inputs,function(i,v){
          sum=sum+parseInt($(v).val())||0;
        })
        md.find('input.notafinal').val(sum);
        if(sum>100) md.find('input.notafinal').css({"border":"1px solid red"}).attr('readonly','readonly');
        else md.find('input.notafinal').css({"border":"1px"})
      }else{
        md.find('input.notafinal').val(nota);
      }
     })
  }).on('click', '.subirtarea', function(ev) {
      var htmltarea = $('#subirtarea').clone(true);
      htmltarea.find('.idtab').each(function(i, v) {
        if ($(v).hasClass('isa')) $(v).attr('href', $(v).attr('href') + contab);
        $(v).attr('id', $(v).attr('id') + contab);
      })
      var modochat=$(this).attr('modo')=='chat'?true:false;
      if(modochat){
        htmltarea.find('.bar_tabs').children('li').hide().children('a').removeClass('active');
        htmltarea.find('.bar_tabs').children('li').eq(1).show().children('a').addClass('active').removeClass('hide');
        htmltarea.find('.tab-content').children('.tab-pane').hide().removeClass('show active');
        htmltarea.find('.tab-content').children('.tab-pane').eq(1).show().addClass('show active');
      }

      var el = $(this);
      var tipotarea = '';
      if (el.attr('tipo')=='TP') {
        htmltarea.find('.estareaproyecto').show();
        tipotarea = 'TP';
      } else if (el.attr('tipo')=='T') {
        htmltarea.find('.esproyecto').remove();
        tipotarea = 'T';
      } else if (el.attr('tipo')=='P') {
        htmltarea.find('.estarea').remove();
        tipotarea = 'P';
      }
      var dt = {  html: htmltarea  };
      //console.log('dt',dt);
      var md = __sysmodal(dt);
      var _cargarmensajesyarchivos = function() {
        var formData = new FormData();
        formData.append('idcurso', parseInt(el.attr('idcurso')));
        formData.append('idsesion', parseInt(el.attr('idsesion')));
        formData.append('idpestania', parseInt(el.attr('idpestania') || '0'));
        formData.append('tipo', tipotarea);
        formData.append('idcomplementario', $('#idcurso option:selected').attr("data-idcomplementario"));
        formData.append('idgrupoaula', $('#idcurso option:selected').attr("data-idgrupoaula"));
        formData.append('idgrupoauladetalle', $('#idcurso option:selected').attr("data-idgrupoauladetalle"));

        __sysajax({
          fromdata: formData,
          url: _sysUrlBase_ + 'json/tareas/archivosalumnoymensajes',
          showmsjok: false,
          callback: function(rs) {
            $pnlmensajes = md.find('.mensajeenviados #mensajes');
            if (rs.mensajes) {
              $pnlmensajes.html('');
              var yo = '<?php echo $this->usuarioAct["idpersona"] ?>';
              $.each(rs.mensajes, function(i, v) {
                var fotouser = (v.foto == '' || v.foto == null) ? 'static/media/usuarios/user_avatar.jpg' : v.foto;
                var ifoto = fotouser.lastIndexOf('static/');
                if (ifoto == -1) {
                  fotouser = _sysUrlBase_ + 'static/media/usuarios/user_avatar.jpg';
                } else fotouser = _sysUrlBase_ + fotouser.substring(ifoto);
                if (v.idusuario == yo) {
                  var msj = '<div class="mensaje-autor"><img src="' + fotouser + '" alt="" class="foto img-responsive"><div class="flecha-izquierda"></div><div class="contenido"><b>' + v.usuario + '</b><br>' + v.mensaje + '</div><div class="fecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                } else {
                  msj = '<div class="mensaje-amigo" ><div class="contenido">' + v.mensaje + '</div><div class="flecha-derecha"></div><img src="' + fotouser + '" alt="" class="foto img_responsive"><div class="fecha">' + v.fecha_registro + '</div></div><div class="clearfix"></div>';
                }
                $pnlmensajes.append(msj);
              })
            }
            $pnlarchivos = md.find('.archivosenviados table tbody');
            if (rs.archivos) {
              $pnlarchivos.html('');
              $.each(rs.archivos, function(i, v) {
                var descripcion = v.media.split("/trabajos/")[1].split("?")[0];
                var html = '<tr><td><a href="' + _sysUrlBase_ + v.media + '" target="_blank" download class="btn btn-sm"> <i class="fa fa-download"></i> ' + descripcion + '</a></td><td>' + v.fecha_registro + '</td><td class="text-center"> <i class="fa fa-trash borrararchivosubido" style="cursor:pointer" id="' + v.idtarearecurso + '"></i></tr>';
                $pnlarchivos.append(html);
              })
            }
          }
        })
      }
      var _guardartarea = function(solomsj = false) {
        var link = el.attr('data-link')||'';
        var tipolink = el.attr('data-tipolink')||'';
        if (tipolink == 'smartquiz') link = _sysUrlSitio_ + link.substring(link.lastIndexOf("/quiz/"));
        else link = _sysUrlBase_ + link.substring(link.lastIndexOf("static/"));
        var tipomedia = md.find('#infolink').attr('tipolink') || '';
        if (tipomedia == 'texto' || tipomedia == 'link') mediainfo = md.find('#infolink').attr('value') || '';
        else mediainfo = md.find('#infolink').val() || '';
        if (mediainfo == undefined) mediainfo = '';
        $mensaje = md.find('#_mensaje_').val() || '';
        if (mediainfo != '' && $mensaje != '') {
          $mensaje = '<a href="' + _sysUrlBase_ + mediainfo + '" class="btn btn-primary btn-xs" download="" target="_blank"><i class="fa fa-download"></i> <?=JrTexto::_("File")?></a><br>' + $mensaje;
        }
        var formData = new FormData();
        formData.append('tipo', tipotarea);
        formData.append('idcurso', parseInt(el.attr('idcurso')));
        formData.append('idsesion', parseInt(el.attr('idsesion')));
        formData.append('idpestania', parseInt(el.attr('idpestania') || '0'));
        formData.append('recurso', link);
        formData.append('tiporecurso', tipolink);
        formData.append('iddocente', el.attr('iddocente'));
        formData.append('nombre', el.attr('data-nombre') || '');
        formData.append('descripcion', el.attr('data-descripcion') || '');
        formData.append('media', mediainfo);
        formData.append('tipomedia', tipomedia);
        formData.append('criterios', el.attr('criterios') || '');
        formData.append('idcomplementario', $('#idcurso option:selected').attr("data-idcomplementario"));
        formData.append('idgrupoaula', $('#idcurso option:selected').attr("data-idgrupoaula"));
        formData.append('idgrupoauladetalle', $('#idcurso option:selected').attr("data-idgrupoauladetalle"));
        if (solomsj) {
          formData.append('solomensaje', true);
          formData.append('mensaje', md.find('#_mensaje2_').val() || '');
          md.find('#_mensaje2_').val('');
        } else {
          formData.append('mensaje', $mensaje);
          md.find('#_mensaje_').val('');
        }
        __sysajax({
          fromdata: formData,
          url: _sysUrlBase_ + 'json/tareas/guardar',
          showmsjok: false,
          callback: function(rs) {
            if(solomsj==false) Swal.fire('Tarea guardada correctamente', '', 'success');
              else  Swal.fire('Mensaje guardado correctamente', '', 'success');
            _cargarmensajesyarchivos();
          }
        })
      }
      md.on('click', '.salirmodal', function(ev) {
        Swal.showLoading();
        __cerrarmodal(md, true);
      }).on('click', '.btnguardartarea', function(ev) {
        _guardartarea(false);
      }).on('click', '.btntipofile', function(ev) {
        var el_ = $(this);
        el_.siblings().removeClass('btn-primary').addClass('btn-secondary');
        el_.addClass('btn-primary').removeClass('btn-secondary');
        var title = (el_.attr('title') || 'pdf').toLowerCase();
        var aquitipofile = md.find('#aquitipofile');
        if (title == 'texto') {
          aquitipofile.html('<br><textarea id="infolink" tipolink="texto" class="form-control" rows="5" placeholder="<?=JrTexto::_("Copy or type your reply text")?>"  style="resize: none;"></textarea>');
        } else if (title == 'imagen') {
          var idimagen = __idgui();
          aquitipofile.html('<input type="hidden" id="infolink" tipolink="imagen" ><div><img src="" class="img-responsive" id="' + idimagen + '"  style="display:none; max-width:180px; max-height:180px;"></div>');
          $img = aquitipofile.find('#' + idimagen);
          __subirfile_({
            file: $img,
            typefile: 'imagen',
            uploadtmp: true,
            guardar: true,
            dirmedia: 'trabajos/'
          }, function(rs) {
            //console.log(rs);
            let f = rs.media.replace(_sysUrlBase_, '');
            aquitipofile.find('#infolink').attr('value', f)
              aquitipofile.find('img').show();
            $img.attr('src', _sysUrlBase_ + f);
          });

        } else if (title == 'video') {
          var idimagen = __idgui();
          aquitipofile.html('<input type="hidden" id="infolink"  tipolink="video"><div><video controls src="" class="img-responsive" id="' + idimagen + '" style="display:none; max-width:130px; max-height:130px;"></div>');
          $img = aquitipofile.find('#' + idimagen);
          __subirfile_({
            file: $img,
            typefile: 'video',
            uploadtmp: true,
            guardar: true,
            dirmedia: 'trabajos/'
          }, function(rs) {
            let f = rs.media.replace(_sysUrlBase_, '');
            aquitipofile.find('#infolink').attr('value', f);
             aquitipofile.find('video').show();
            $img.attr('src', _sysUrlBase_ + f);
          });

        } else if (title == 'audio') {
          var idimagen = __idgui();
          aquitipofile.html('<input type="hidden" id="infolink"  tipolink="audio"><div><audio controls src="" class="img-responsive" style="display:none; max-width:180px;" id="' + idimagen + '"></div>');
          $img = aquitipofile.find('#' + idimagen);
          __subirfile_({
            file: $img,
            typefile: 'audio',
            uploadtmp: true,
            guardar: true,
            dirmedia: 'trabajos/'
          }, function(rs) {
            let f = rs.media.replace(_sysUrlBase_, '');
            aquitipofile.find('#infolink').attr('value', f);
            aquitipofile.find('audio').show();
            $img.attr('src', _sysUrlBase_ + f);
          });

        } else if (title == 'html') {
          var idimagen = __idgui();
          aquitipofile.html('<input type="hidden" id="infolink"  tipolink="html"><div><a target="_blank" style="display:none; " src="img" class="img-responsive" id="' + idimagen + '"><?=JrTexto::_("Uploaded link")?></a></div>');
          $img = aquitipofile.find('#' + idimagen);
          __subirfile_({
            file: $img,
            typefile: 'html',
            uploadtmp: true,
            guardar: true,
            dirmedia: 'trabajos/'
          }, function(rs) {
            let f = rs.media.replace(_sysUrlBase_, '');
            aquitipofile.find('#infolink').attr('value', f);
            aquitipofile.find('a').show();
            $img.attr('href', _sysUrlBase_ + f);
          });

        } else if (title == 'pdf') {
          var idimagen = __idgui();
          aquitipofile.html('<input type="hidden" id="infolink"  tipolink="pdf"><div><a target="_blank" style="display:none; " src="img" class="img-responsive" id="' + idimagen + '">PDF <?=JrTexto::_("Uploaded")?></a></div>');
          $img = aquitipofile.find('#' + idimagen);
          __subirfile_({
            file: $img,
            typefile: 'pdf',
            uploadtmp: true,
            guardar: true,
            dirmedia: 'trabajos/'
          }, function(rs) {
            let f = rs.media.replace(_sysUrlBase_, '');
            aquitipofile.find('#infolink').attr('value', f);
             aquitipofile.find('a').show();
            $img.attr('href', _sysUrlBase_ + f);
          });

        } else if (title == 'link') {
          aquitipofile.html('<br><input id="infolink"  tipolink="link" value="" placeholder="Copie o escriba el enlace " class="form-control"> ');
        }
      }).on('click', '.borrararchivosubido', function(ev) {
        var el_ = $(this);
        var formData = new FormData();
        formData.append('idtarearecurso', parseInt(el_.attr('id')));
        __sysajax({
          fromdata: formData,
          url: _sysUrlBase_ + 'json/tareas_archivosalumno/eliminar',
          showmsjok: false,
          callback: function(rs) {
            el_.closest('tr').remove();
          }
        })
      }).on('click', '.btnsendmensaje', function(ev) {
        if(md.find('#_mensaje2_').val()==''){
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: '<?=JrTexto::_("The message cannot be empty")?>',             
            })
            Swal.fire('<?=JrTexto::_("Success")?>!', '', 'success');
            return false;
          }
          Swal.showLoading();
        _guardartarea(true);
      })
      _cargarmensajesyarchivos();
      contab++;
    })



  })
</script>
<?php //var_dump($this->gruposauladetalle); ?>