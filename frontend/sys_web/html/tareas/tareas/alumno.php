<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
<?php } ?>
<style type="text/css">
  .puntaje{
    position: absolute; right: 35px; top: 35px;
  }
  .puntaje small{
    font-weight: 1.5em;
  }
</style>
<div class="row">
  <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
    <label class="control-label"><?php echo JrTexto::_('My courses'); ?></label>
    <select id="idcurso" name="idcurso" class="form-control" idsel="<?php echo @$this->idcurso; ?>" >        
      <?php if(!empty($this->gruposauladetalle))
        foreach ($this->gruposauladetalle as $fk){?>
          <option value="<?php echo $fk["idcurso"]?>" ><?php echo $fk["strcurso"] ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="col-md-4">
    
  </div>
  <div class="col-md-12 " id="">
    <div class="accordion" id="vistalisatotareas" role="tablist" aria-multiselectable="true">
      <div class="panel panelclone col-md-6" style="display: none;">
        <a class="panel-heading collapsed" role="tab" id="heading" data-toggle="collapse" href="#collapse" aria-expanded="false" aria-controls="collapse">
          <h4 class="panel-title"><?=JrTexto::_("Task")?> : <small></small> <span class="puntaje"><?=JrTexto::_("Score")?>:<small>20</small></span></h4>
        </a>
        <div class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading" style="">
          <div class="panel-body"> 
            <div class="row">
              <div class="col-md-6 col-sm-12 text-center table-responsive">
                <table class="table ">
                  <thead><tr class="table-secondary"><th colspan="3" class="text-center"><strong><?=JrTexto::_("Reference information")?></strong></th></tr></thead>
                  <tbody>
                    <tr class="table-secondary"><th class="text-left"><?=JrTexto::_("Teacher")?></th><td> : </td><td> Abel  chingo Tello</td></tr>
                    <tr class="table-secondary"><th class="text-left"><?=JrTexto::_("Reference file")?></th><td> : </td><td> <a href="#" class="archivodocente"><i class="fa fa-cloud-download" style="font-size: 2em"></i><br><?=JrTexto::_("View or download")?></a></td></tr>
                  </tbody>
                </table>            
              </div>
              <div class="col-md-6 col-sm-12 text-center table-responsive">
                <table class="table">
                  <thead><tr class=""><th colspan="4" class="text-center"><strong><?=JrTexto::_("Student response")?></strong></th></tr></thead>
                  <tbody>
                    <tr><th class="text-left"><?=JrTexto::_("Student")?></th><td> : </td><td> Abel  chingo Tello</td></tr>
                    <tr><th class="text-left"><?=JrTexto::_("Reference file")?></th><td> : </td><td> <a href="#" class="archivoalumno"><i class="fa fa-cloud-download" style="font-size: 2em"></i><br> <?=JrTexto::_("View or download")?> </a></td> <td> <a href="#" class="archivodocente"><i class="fa fa-cloud-upload" style="font-size: 2em"></i><br> <?=JrTexto::_("Upload file")?></a></td></tr>                   
                  </tbody>
                </table>            
              </div>
              <div class="col-md-12" style="padding: 3ex 3ex 0 3ex"><b><?=JrTexto::_("Last message")?></b> <small>2018-02-2020</small><br>
                <div class="alert alert-info"></div>
              </div>
            </div>         
          </div>
      </div>    
    </div>

        
  </div>
</div>
<script type="text/javascript">
   addItemNavBar({
      text: '<?php echo JrTexto::_("Tasks"); ?>'
    });
  $(document).ready(function(){



    var data=new FormData()           
      data.append('idcurso',$('select#idcurso').val()||'');
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/tareas',
          callback:function(rs){
            html='';
            if(rs.code==200){
              $.each(rs.data, function(i, item) {
                console.log(item);
                $panelclone=$('#vistalisatotareas').children('.panelclone').clone().show();
                $panelclone.removeClass('panelclone');
                $panelclone.children('a.panel-heading').attr('id','headding'+i).attr('href','#collapse'+i);
                $panelclone.children('a.panel-heading').children('h4').children('small').text(item.nombre.trim()||'');
                $panelclone.children('.panel-collapse').attr('id','collapse'+i)
                $panelclone.find('a.archivodocente').attr('href',item.recurso);
                  /*if (item.tipo == 'TP' || item.tipo == 'P') {
                      html += '<div class="panel">';
                      html += '<a class="panel-heading collapsed" role="tab" id="heading' + i + '" data-toggle="collapse" data-parent="#lista2" href="#collapse' + i + '" aria-expanded="false" aria-controls="collapse' + i + '">';
                      html += '<h4 class="panel-title">' + ((item.idsesion != 0) ? 'SESION' : 'PESTAÑA') + ' - ' + item.nombre + '</h4>';
                      html += '</a>';
                      html += '<div id="collapse' + i + '" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="heading' + i + '" style="">';
                      html += '<div class="panel-body">';
                      html += '<ul class="list-unstyled timeline">';
                      html += '<li style="background:#fcffff;">';
                      html += '<div class="block">';
                      html += '<div class="tags">';
                      html += '<a class="tag" style="text-decoration:none;">';
                      html += '<span>Descripcion</span>';
                      html += '</a>';
                      html += '</div>';
                      html += '<div class="block_content">';
                      html += '<h2 class="title">';
                      html += '<a>' + item.descripcion + '</a>';
                      html += '</h2>';
                      html += '<div class="byline">';
                      html += '<span>- Recurso</span>: <a>' + item.recurso + '</a><br>';
                      html += '<span>- Tipo recurso</span>: <a>' + item.tiporecurso + '</a><br>';
                      html += '<span>- Nota</span>: ' + item.nota + ' en Base a ' + maxcal;
                      html += '<div class="col-md-2 contenido btnsubirfilealumno estarea" style="cursor: pointer;    border-radius: 15px 2px;background-color: #1abb9c;" idcurso="' + item.idcurso + '" idcursopestania="' + item.idpestania + '" idcursodetalle="' + item.idsesion + '" iddocente="' + item.iddocente + '" tiporecurso="' + item.tiporecurso + '" recurso="' + item.recurso + '" tipo="' + item.tipo + '" data-link="static/media/cursos/curso_171/ses_1591/file_1591_5_1580940389570.pdf?id=20200205170643" data-tipolink="pdf" data-nombre="' + item.nombre + '" data-descripcion="' + item.descripcion + '" id="contenido' + i + '"><i class="fa fa-upload"  style="cursor: pointer;font-size: 30px;color: white;"></i></div>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      html += '</li>';
                      html += '</ul>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                  }*/
                  $("#vistalisatotareas").append($panelclone);
              });
              //$("#vistalisatotareas").html(html);


              console.log('tarea',rs);
              //localStorage.setItem('dataexamenes',JSON.stringify(rs.data));
              //mostrarresultado();
            }
          }
      });

  })
</script>
<?php //var_dump($this->gruposauladetalle); ?>