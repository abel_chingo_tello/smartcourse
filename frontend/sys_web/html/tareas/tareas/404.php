<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
if (!$ismodal) { ?>
  <div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Tasks"); ?>'
    });
    /*addButonNavBar({
      class: 'btn btn-success btn-sm Agreggarforo',
      text: '<i class="fa fa-plus"></i> <?php echo JrTexto::_('add') ?>',
      href: '<?php echo $this->documento->getUrlSitio(); ?>/foros/agregar'
    });*/
  </script>
<?php } ?>
<div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron">
                <h1><strong>Opss!</strong></h1>                
                <div class="disculpa"><?=JrTexto::_("Sorry we haven't found tasks")?>.</div>
                <div class=""><br>
                    <a class="btn btn-default close cerrarmodal" href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Go back')?></a>
                </div>
            </div>
        </div>
  </div>
</div>