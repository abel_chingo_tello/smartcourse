<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_imgdefecto="static/media/usuarios/user_avatar.jpg";
if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li-->
        <li><a href="<?php echo $this->documento->getUrlSitio();?>/personal">&nbsp;<?php echo JrTexto::_('Personal'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row ventana ventanaroles"  id="vent-<?php echo $idgui;?>" idgui="<?php echo $idgui;?>">
  <form method="post" id="frm-<?php echo $idgui;?>" tb="persona" accion="agregarrol" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
    <div id="msj-interno"></div>
    <div class="col-md-12 table-responsive" id="pnlbotonesde">
    <table class="table table-striped table-hover tableimport">
          <thead>
            <tr class="tr01 headings">              
              <th><?php echo ucfirst(JrTexto::_("Rol")); ?></th>
              <th><?php echo ucfirst(JrTexto::_('Actions')); ?></th>
            </tr>
          </thead>
          <tbody>
          <?php 
          $hayroles=array();
          if(!empty($this->roles)){
            foreach($this->roles as $rol){
              $hayroles[]=$rol["idrol"];
              ?>
              <tr class="tr01" data-iddetalle="<?php echo $rol["iddetalle"]; ?>" data-idrol="<?php echo $rol["idrol"]; ?>" data-rolname="<?php echo $rol["rol"]; ?>" data-idpersona="<?php echo $rol["idpersonal"] ?>">
              <?php if($rol["idrol"]==1||$rol["idrol"]==5||$rol["idrol"]==9){?>
                <td><?php echo $rol["rol"];?></td>
                <td><a class="btn btn-xs btneliminarrol"  href="#"><i class="fa fa-trash"></i></a></td>
              <?php }elseif($rol["idrol"]==2){ ?>
                <td><?php echo $rol["rol"];?></td>
                <td><a class="btn btn-xs btnverroldocente" href="#"  ><i class="fa fa-eye"></i></a></td>
               
              <?php }elseif($rol["idrol"]==3){ ?>
                <td><?php echo $rol["rol"];?></td>
                <td><a class="btn btn-xs btnverrolalumno" href="#" ><i class="fa fa-eye"></i></a></td>
              <?php }elseif($rol["idrol"]==4||$rol["idrol"]==6){ ?>
                <td><?php echo $rol["rol"]." - ".@$this->datospersona["strlocal"];?></td>
                <td><!--a class="btn btn-xs btnveriiee" href="#" ><i class="fa fa-eye"></i></a-->
                <a class="btn btn-xs btneditiee" href="#" ><i class="fa fa-pencil"></i></a>
                <a class="btn btn-xs btneliminarrol" href="#" ><i class="fa fa-trash"></i></a></td>
              <?php }elseif($rol["idrol"]==7){ ?>
                <td><?php echo $rol["rol"]." - ".@$this->datospersona["strugel"];?></td>
                <td><!--a class="btn btn-xs btnverugel" href="#" ><i class="fa fa-eye"></i></a-->
                <a class="btn btn-xs btneditugel" href="#" ><i class="fa fa-pencil"></i></a>
                <a class="btn btn-xs btneliminarrol" href="#" ><i class="fa fa-trash"></i></a></td>
              <?php }elseif($rol["idrol"]==8){ ?>
                <td><?php echo $rol["rol"]." - ".@$this->strdre;?></td>
                <td><!--a class="btn btn-xs btnverdre" href="#" ><i class="fa fa-eye"></i></a-->
                <a class="btn btn-xs btneditdre" href="#" ><i class="fa fa-pencil"></i></a>
                <a class="btn btn-xs btneliminarrol" href="#" ><i class="fa fa-trash"></i></a></td>
              <?php } ?>
              
            </tr>
          <?php }} ?>
          </tbody>
    </table>
    </div>
    <div class="col-md-12 text-center"><hr>
      <a class="btn btn-success btnagregarrol" href="javascript:void(0)" ><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add')?></a>
      <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('personal'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
      <hr>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 hide" id="paneldatosaimportar" style="display:none;">
      <input type="hidden" id="
      " value="0">
      <input type="hidden" id="idpersonarol_" value="<?php echo $this->datospersona["idpersona"]; ?>">
      <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 form-group">
              <label><?php echo JrTexto::_('Rol'); ?></label>
              <div class="cajaselect">            
              <select id="_fkcbrol" name="rol" class="form-control" >                  
                <?php if(!empty($this->fkrol))
                  foreach ($this->fkrol as $fkrol){
                    if($fkrol['idrol'] == '1' && $this->user['idrol'] != '1') continue;
                    if(in_array($fkrol["idrol"],$hayroles)) continue;                         
                    ?>
                    <option value="<?php echo $fkrol["idrol"]?>" <?php echo $fkrol["idrol"]==@$this->idrol?"selected":""; ?> ><?php echo ucfirst($fkrol["rol"]); ?></option><?php } ?>                        
              </select>
          </div>
        </div>
        <?php if($this->user['idproyecto'] == 3): ?>
        <div class="col-xs-6 col-sm-4 col-md-3 form-group _pnldocente" >
          <label><?php echo ucfirst(JrTexto::_('Dre')); ?></label>
          <div class="cajaselect">
            <select id="_fkcbdre" name="dre" class="form-control" ><?php 
                if(!empty($this->dress))
                foreach ($this->dress as $fk) { ?><option <?php echo @$this->iddre==$fk["ubigeo"]?'selected="selected"':'';?> value="<?php echo $fk["ubigeo"]?>" ><?php echo $fk["descripcion"] ?></option><?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 form-group _pnldocente" >
          <label><?php echo ucfirst(JrTexto::_('UGEL')); ?></label>
          <div class="cajaselect">
            <select id="_fkcbugel" name="ugel" class="form-control" ></select>
          </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 form-group _pnldocente" >
          <label><?php echo ucfirst(JrTexto::_('IIEE')); ?></label>
          <div class="cajaselect">
            <select id="_fkcbiiee" name="iiee" class="form-control" ></select>
          </div>
        </div>
        <?php endif; ?>
        <div class="col-xs-12 col-sm-6 col-md-6 text-center _pnlnodocente  form-group">  <br>
            <a class="btn btn-primary  btnsaverolpersona" href="javascript:void(0)"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save')?></a>
          </div>
        </div>
    </div>
  </form>
  <div id="traducciones" style="display: none">
    <span id="atention"><?php echo JrTexto::_('Attention');?> </span>
    <span id="iiee"><?php echo JrTexto::_('IIEE');?></span>
    <span id="curso"><?php echo JrTexto::_('Course');?> </span>
    <span id="grado"><?php echo JrTexto::_('Grade');?> </span>
    <span id="seccion"><?php echo JrTexto::_('Section');?> </span>
    <span id="confirm_action"><?php echo JrTexto::_('Confirm action');?> </span>
    <span id="delete_this_record"><?php echo JrTexto::_('It is sure to delete this record ?');?></span>
    <span id="cancel"><?php echo JrTexto::_('Cancel');?></span>
    <span id="accept"><?php echo JrTexto::_('Accept');?></span>
  </div>
  <input type="hidden" name="idlocaltmp" value="<?php echo !empty($this->datospersona["idlocal"])?$this->datospersona["idlocal"]:'0';?>">
  <input type="hidden" name="idugeltmp" value="<?php echo !empty($this->datospersona["idugel"])?$this->datospersona["idugel"]:'0';?>">
  <input type="hidden" name="iddretmp" value="<?php echo !empty($this->iddre)?$this->iddre:'0';?>">
</div>
<script id="cargarscript" src="<?php echo $this->documento->getUrlStatic().'/js/web/persona.js' ?>"></script>
<script type="text/javascript">
  $(document).ready(function(ev){
    var frm=$('#frm-<?php echo $idgui;?>');
    if(!frm.hasClass('ismodaljs')) frmpersona($('#vent-<?php echo $idgui;?>'));
  })
</script>

<script type="text/javascript">
/*$(document).ready(function(){
  $('#vent-<?php echo $idgui;?>').on('click','.btneditiee',function(ev){
    var idrol=$(this).closest('tr').attr('data-idrol');
    var idrolname=$(this).closest('tr').attr('data-rolname');
    $('#_fkcbrol').append('<option value="'+idrol+'" selected="selected">'+idrolname+'</option>');
    $('#_fkcbrol').trigger('change');
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide'); 
    $('.btnagregarrol<?php echo $idgui; ?>').hide();
  }).on('click','.btneditugel',function(ev){
    var idrol=$(this).closest('tr').attr('data-idrol');
    $('#_fkcbrol').val(idrol);
    $('#_fkcbrol').trigger('change');
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide'); 
    $('.btnagregarrol<?php echo $idgui; ?>').hide();
  }).on('click','.btneditdre',function(ev){
    var idrol=$(this).closest('tr').attr('data-idrol');
    $('#_fkcbrol').val(idrol);
    $('#_fkcbrol').trigger('change');
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide'); 
    $('.btnagregarrol<?php echo $idgui; ?>').hide();
  })
 


  $('select#_fkcbdre').change(function(ev){
      ev.preventDefault();
      var iddre=$(this).val();
      var fd2= new FormData();
      fd2.append("iddepartamento", iddre);
      sysajax({
        fromdata:fd2,
        url:_sysUrlBase_+'/ugel/buscarjson',
        callback:function(rs){
            $sel=$('#_fkcbugel');
            $sel.children('option').remove();
            dt=rs.data;
            $.each(dt,function(i,v){
              var selop=v.idugel==idugeltmp?'selected="selected"':'';
              $sel.append('<option value="'+v.idugel+'" '+selop+' >'+v.descripcion+'</option>');
            })
            $sel.trigger('change');
        }
      })
  })
  
  $('select#_fkcbugel').change(function(ev){
      ev.preventDefault();
      var idugel=$(this).val();
      var fd2= new FormData();
      fd2.append("idugel", idugel);
      sysajax({
      fromdata:fd2,
      url:_sysUrlBase_+'/local/buscarjson',
      callback:function(rs){
          $sel=$('#_fkcbiiee');
          $sel.children('option').remove();
          dt=rs.data;
          $.each(dt,function(i,v){
            var selop=v.idlocal==idlocaltmp?'selected="selected"':'';
            $sel.append('<option value="'+v.idlocal+'" '+selop+'>'+v.nombre+'</option>');
          })
      }})
  }) 
  
  $('#_fkcbrol').trigger('change');

  
});
*/

</script>

