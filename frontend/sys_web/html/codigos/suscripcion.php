<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : ""; ?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Subscription codes"); ?>'
  });
</script>
<style>
    .fondologin{   
        height:99.9vh;
        width:100%;
        position:relative;
        overflow:hidden;
    }
    .contentmedia {
        position: absolute;
        height: 100%;
        width: 100%;
        /*opacity: 0.95;*/
    }
    .contentmedia img, .contentmedia video{
        margin: auto;
        position: absolute;
        z-index: 1;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        min-width: 100%;
        min-height: 100%;
        opacity:0.85;
    }
</style>
<div class="fondologin">
  <div id="wrapper">
    <div id="validarcodigo" class="animate form">
      <section class="login_content">
          <br>
          <form method="post" id="formcodigosuscripcion" onsubmit="return false;"> 
              <input type="hidden" name="idempresa" value="<?php echo @$frm["idempresa"];?>">
              <input type="hidden" name="idproyecto" value="<?php echo @$frm["idproyecto"];?>">                      
              <h1><?php echo JrTexto::_('Activate your new code')?></h1>

              <div class="container">
                <div class="row">
                  <div class="col-md-3 col-sm-2 col-xs-12"></div>
                  <div class="col-md-6 col-sm-8 col-xs-12">
                    <div class="form-group has-feedback" style="position: relative;">
                        <input type="text" name="codigo" id="codigo" class="form-control" placeholder="<?php echo JrTexto::_('Write or copy your code here')?>" required="required" maxlength="150" style="padding-left: 2em;" />
                        <span class="fa fa-code form-control-feedback" aria-hidden="true" style="position: absolute; top: -0.25ex; left: .25ex !important;"></span>
                    </div>

                  </div>
                  <div class="col-md-3 col-sm-2 col-xs-12"></div>
                </div>
              </div>             

              <div>
                  <button type="submit" id="#btn-enviar-formcodigosuscripcion" class="btn btn-primary submits"> <i class="fa fa-key"></i> <?php echo JrTexto::_('Validate code')?></button>
              </div>
              <div class="clearfix"></div>
             
          </form>
          <!-- form -->
      </section>
      <!-- content -->
    </div>
  </div>
</div>
<script type="text/javascript">
function isNumber(value){ return typeof value === 'number' && !isNaN(value); }
function isObject(value){ return _typeof(value) === 'object' && value !== null; }
function isFunction(value){ return typeof value === 'function'; }
function toArray(value){ return Array.from ? Array.from(value) : slice.call(value); }
function forEach(data, callback){
  if(data && isFunction(callback)){
    if(Array.isArray(data) || isNumber(data.length) /* array-like */){
        toArray(data).forEach(function(value, key){callback.call(data, value, key, data); });
    }else if(isObject(data)){
      Object.keys(data).forEach(function(key){callback.call(data, data[key], key, data); });
    }
  }
  return data;
}
  $(document).ready(function(){
    var idempresa=<?php echo !empty($this->usuario["idempresa"])?$this->usuario["idempresa"]:0; ?>;
    var idproyecto=<?php echo !empty($this->usuario["idproyecto"])?$this->usuario["idproyecto"]:0; ?>;
    var datosalumno=<?php echo json_encode($this->usuario); ?>;
     var _sysUrlsks_='<?php echo _http_.DOMINIO_SKS; ?>/json/';
     $('#formcodigosuscripcion').bind({
        submit:function(ev){
            ev.preventDefault();
            var btn=$(this).find('#btn-enviar-newregistro');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            var frm=$('#formcodigosuscripcion');
            var codigo=frm.find('#codigo').val();
            var data=new FormData();
            data.append('codigo',codigo);
            data.append('estado',2); //descomentar para validar solo tipo2
            data.append('idempresa',idempresa);
            data.append('idproyecto',idproyecto);
            //b54aad92-cd8c-46f0-b02f-4af7b48574b1
            data.append('showmsjok',true);
              __sysAyax({ 
                fromdata:data,
                  url:_sysUrlsks_+'codigos/buscarcompleto',                  
                  callback:function(rs){                  
                    if(rs.code==200 && rs.data.length>0){
                        if(rs.data[0].codigo!==codigo){
                            Swal.fire({
                                title:'<?=JrTexto::_("The entered code is not correct")?>',
                                icon:'error'
                            })
                            return ;
                        }
                        var cantidad_descarga=rs.data[0].cantidad_descarga||1;
                        Swal.fire({
                          icon:'success',
                          title: '<?=JrTexto::_("Your entered code is correct")?> <br> <?=JrTexto::_("do you want to activate it?")?>',
                         // showDenyButton: true,
                          showCancelButton: true,
                          confirmButtonText: `<?=JrTexto::_("Yes")?>`,
                          //denyButtonText: `Si, Registrarme`,
                          cancelButtonText:'<?=JrTexto::_("No, cancel it")?>',
                        }).then((result) => {
                            if(result.value==true){
                              let idgrupoaula=rs.data[0].idgrupoaula;
                                var data=new FormData();
                                    data.append('idgrupoaula',idgrupoaula);
                                    data.append('keycode',codigo);
                                  __sysAyax({ 
                                    fromdata:data,
                                    url:_sysUrlBase_+'json/api/marticulasxgrupo',
                                    callback:function(rs){
                                        if(rs.code==200){
                                            let matricula_faltantes=[];
                                            if(rs.datos.matricula_faltantes!=undefined && rs.datos.matricula_faltantes!='')
                                                forEach(rs.datos.matricula_faltantes,function(v,i){
                                                    matricula_faltantes.push(v);
                                                })

                                            let matricula_sobrantes=[];
                                            if(rs.datos.matricula_sobrantes!=undefined && rs.datos.matricula_sobrantes!='')
                                                forEach(rs.datos.matricula_sobrantes,function(v,i){
                                                    matricula_sobrantes.push(v);
                                                })


                                            let cursosdelgrupo=[];
                                            if(rs.datos.cursosdelgrupo!=undefined && rs.datos.cursosdelgrupo!='')
                                                forEach(rs.datos.cursosdelgrupo,function(v,i){
                                                    cursosdelgrupo.push(v);
                                                })

                                                
                                            let mismatriculas=[];
                                            if(rs.datos.mismatriculas!=undefined && rs.datos.mismatriculas!='')
                                                forEach(rs.datos.mismatriculas,function(v,i){
                                                    mismatriculas.push(v.idmatricula);
                                                })
                                            
                                            var data1=new FormData();
                                            if(mismatriculas.length>0) data1.append('idmatricula',JSON.stringify(mismatriculas));
                                            if(matricula_faltantes.length>0) data1.append('matricula_faltantes',JSON.stringify(matricula_faltantes));
                                            if(matricula_sobrantes.length>0) data1.append('matricula_sobrantes',JSON.stringify(matricula_sobrantes));
                                            if(cursosdelgrupo.length>0) data1.append('cursosdelgrupo',JSON.stringify(cursosdelgrupo));

                                            data1.append('target',_sysUrlBase_+'json/');
                                            data1.append('idproyecto',idproyecto);
                                            data1.append('idempresa',idempresa);            
                                            data1.append('keycode',codigo);
                                            let urlsksextender=_sysUrlsks_+'matriculas/extendersuscripcion';
                                            if(mismatriculas.length==0){
                                                let alumnos=[{                                
                                                    "tipodoc": datosalumno.tipodoc, //este valor debe coincidir con SKS
                                                    //"tipodoc_str": tipodoc_str, //este nombre debe coincidir con SKS
                                                    "nombres": datosalumno.nombre, //falta
                                                    "apellido1": datosalumno.apellido1, //falta
                                                    "apellido2": datosalumno.apellido2, //falta
                                                    "documento": datosalumno.dni,
                                                    "sexo" : datosalumno.sexo,
                                                    "fechanac": datosalumno.fechanac, //"2020-03-03",
                                                    "instruccion": '', //opcional
                                                    "email": datosalumno.email,
                                                    //"edad" : datosalumno, //opcional en el formulario, pero que sea un valor > 0
                                                    "tipogrupo": 1, //tipo de grupo que lo puede obtener en SC con el idgrupoaula. Si quiere lo deja en constante 1
                                                    "codigoCursoSence": 0, //esto es para sence, siempre en 0 si no es para sence
                                                    "usuario": datosalumno.usuario, //INicial del nombre Inicial del primer apellido
                                                    "clave": datosalumno.clave,
                                                    //"telefono":telefono,
                                                }];
                                                data1.append('alumnos',JSON.stringify(alumnos));
                                                data1.append('tipo_matricula',2);
                                                urlsksextender=_sysUrlsks_+'matriculas/_matricular';
                                            }else{
                                                data1.append('idpersona',datosalumno.idpersona);
                                                data1.append('tipodoc',datosalumno.tipodoc);
                                            }

                                            __sysAyax({ 
                                                fromdata:data1,
                                                url:urlsksextender,    
                                                callback:function(rs){                                                     
                                                    if(rs.code==200){  
                                                      var dataasignar=new FormData();
                                                      dataasignar.append('usuario',datosalumno.usuario); 
                                                      dataasignar.append('numerodocumento',datosalumno.dni);
                                                      dataasignar.append('tipodocumento',datosalumno.tipodoc);
                                                      dataasignar.append('codigosuscripcion',codigo);
                                                      dataasignar.append('idempresa',idempresa);
                                                      dataasignar.append('idproyecto',idproyecto);
                                                      if(datosalumno.idpersona!='' && datosalumno.idpersona!=undefined) dataasignar.append('idpersona',datosalumno.idpersona);
                                                      data.append('cantidad_descarga',cantidad_descarga||1);
                                                      __sysAyax({ 
                                                          fromdata:dataasignar, //http://localhost/smartmanager/json/?idrepresentante=2&buscardocumentos=1
                                                          url:_sysUrlBase_+'json/descargas_asignadas/asignar'
                                                      });
                                                        //__cambiarinfouser();   
                                                        Swal.fire({title:'<?=JrTexto::_("Your code has been used successfully")?>', icon:'success' }); 
                                                    }else if(rs.code==404){
                                                        Swal.fire({title:'<?=JrTexto::_("The code is not valid or has already been used")?>', icon:'error' }).then(rs=>{
                                                            $('#formcodigosuscripcion').find('#codigo').val('');
                                                            window.location.hash ='#tovalidarcodigo';
                                                        });
                                                    }else{
                                                        Swal.fire({title:'<?=JrTexto::_("Unexpected error, try later")?>', icon:'error' });
                                                    }                                           
                                                }
                                            });
                                      }                    
                                    }
                                }); 
                                //Swal.close();*/
                            }
                        })
                    }else{
                        Swal.fire({
                                title:'<?=JrTexto::_("The entered code is not correct")?>',
                                icon:'error'
                        })
                    }
                }
            });

        }
    })
})


                           

                           


</script>