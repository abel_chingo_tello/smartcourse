<?php
defined("RUTA_BASE") or die();
$idgui = uniqid();
$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : ""; ?>
<div class="paris-navbar-container"></div>
<script>
  addItemNavBar({
    text: '<?php echo JrTexto::_("Renew subscription"); ?>'
  });
</script>
<style>
    .fondologin{   
        height:99.9vh;
        width:100%;
        position:relative;
        overflow:hidden;
    }
    .contentmedia {
        position: absolute;
        height: 100%;
        width: 100%;
        /*opacity: 0.95;*/
    }
    .contentmedia img, .contentmedia video{
        margin: auto;
        position: absolute;
        z-index: 1;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        min-width: 100%;
        min-height: 100%;
        opacity:0.85;
    }
</style>
<div class="fondologin">
  <div id="wrapper">
    <div id="validarcodigo" class="animate form">
      <section class="login_content">
        <h1 style=" width: 100%; text-align: center;"><?php echo JrTexto::_("Renew subscription"); ?></h1><br>
        <div class="container">
          <input type="hidden" name="idempresa" value="<?php echo @$frm["idempresa"];?>">
          <input type="hidden" name="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
          <div class="col-md-12 col-sm-12 col-xs-12 infopagotmp"><?=JrTexto::_("Total payable")?> </div>            
            <div class="col-md-12 col-sm-12 col-xs-12 text-center" id="montoapagar">
              <?=JrTexto::_("Processing")?> ...
            </div>            
            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div id="smart-button-container">
						      <div style="text-align: center;">
						        <div id="paypal-button-container" style="max-width:350px; margin:auto;"></div>
						      </div>
						    </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 infopagotmp">
              <!-- <button class="btn btn-primary testerproceso ">Procesar test</button> -->
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- form -->
      </section>
      <!-- content -->
    </div>
  </div>
</div>
<script type="text/javascript">
var URLPAYPAL="https://www.paypal.com/sdk/js?client-id=sb&currency=";
	URLPAYPAL="https://www.paypal.com/sdk/js?client-id=AUzk-lOn-KFnHNGHO0xPk-6Wyd_JdFhqxJ_Z943F7_bAfXuB3_rCGMf2LvOm-omrMD0SrTH5bi2kSQeq&currency="; 

var _sysUrlsks_='<?php echo _http_.DOMINIO_SKS; ?>/json/';

var idempresa=<?php echo !empty($this->usuario["idempresa"])?$this->usuario["idempresa"]:0; ?>;
var idproyecto=<?php echo !empty($this->usuario["idproyecto"])?$this->usuario["idproyecto"]:0; ?>;
var datosalumno=<?php echo json_encode($this->usuario); ?>;
console.log(datosalumno);
var deudawebcar=[];
var idmatricula='<?php echo $this->idmatricula; ?>';

function posDataOK(params){
	let showLoading=params.showLoading||false;
	if(showLoading) Swal.showLoading();
	return new Promise((resolver,rechazar)=>{
		header=params.header||{};
		fetch(params.url, {  method: 'POST',  headers: header,  body: params.formData, }).then(rsjson=>{
		   return rsjson.json();
		}).then((rs)=>{
			if(showLoading) Swal.close();			
			if(rs.code==200){
				resolver(rs);
			}else{
				Swal.fire({
					title: 'Oops !',
		  			text: rs.msj||'<?=JrTexto::_("Error registering client")?>',
		  			icon: 'error',
					timer:10000,
				});
			return false;
			}
		}).catch(e=>{
			if(showLoading) Swal.close();
			console.log(e);
		    Swal.fire({
				title: 'Oops !',
	  			text: '<?=JrTexto::_("Unexpected error, try later")?>',
	  			icon: 'error',
				timer:10000,
			});
			return false; 
		});
	})
}


function initPayPalButton(params){
 	if(cargopaypal==false){
 		return setTimeout(function(){initPayPalButton(params)},2000);
 	}
  	paypal.Buttons({
    style: {
      	shape: 'pill',
      	color: 'blue',
      	layout: 'vertical',
      	label: 'buynow',
     	lang:'es',
    },
    onClick:function(data,actions){

    /*	let  pagos_cart_list=document.querySelector('.chingo_cart_list');
    	let datosdecliente=pagos_cart_list.querySelector('form.datosdecliente');
    	let persona_nombre=datosdecliente.querySelector('input#persona_nombre').value||'';
			if(persona_nombre==''){
				Swal.fire({
					title: _t('Oops')+' !',
		  			text: _t("El nombre no puede estar vacío"),
		  			icon: 'error',
					timer:10000,
				}).then((rs)=>{
					datosdecliente.querySelector('input#persona_nombre').focus();
				});				
				return false;
			}
			let persona_apellidos=datosdecliente.querySelector('input#persona_apellidos').value||'';
			if(persona_apellidos==''){
				Swal.fire({
					title: _t('Oops')+' !',
		  			text: _t("El apellido no puede estar vacío"),
		  			icon: 'error',
					timer:10000,
				}).then((rs)=>{
				datosdecliente.querySelector('input#persona_apellidos').focus();
				});
				return false;
			}
			let persona_email=datosdecliente.querySelector('input#persona_email').value||'';
			if(persona_email==''){
				Swal.fire({
					title: _t('Oops')+' !',
		  			text: _t("El correo no puede estar vacío"),
		  			icon: 'error',
					timer:10000,
				}).then((rs)=>{
				datosdecliente.querySelector('input#persona_email').focus();
				});
				return false;
			}
			if(isEmail(persona_email)==false){
				Swal.fire({
					title: _t('Oops')+' !',
		  			text: _t("El correo no es válido"),
		  			icon: 'error',
					timer:10000,
				}).then((rs)=>{
				datosdecliente.querySelector('input#persona_email').focus();
				});
				return false;
			}

			let persona_telefono=datosdecliente.querySelector('input#persona_telefono').value||'';
			if(persona_telefono==''){
				Swal.fire({
					title: _t('Oops')+' !',
		  			text: _t("el teléfono no puede estar vacío"),
		  			icon: 'error',
					timer:10000,
				}).then((rs)=>{
				datosdecliente.querySelector('input#persona_telefono').focus();
				});
				return false;
			}
			let persona_direccion=datosdecliente.querySelector('input#persona_direccion').value||'';
			if(persona_direccion==''){
				Swal.fire({
					title: _t('Oops')+' !',
		  			text: _t("La dirección no puede estar vacío"),
		  			icon: 'error',
					timer:10000,
				}).then((rs)=>{
					datosdecliente.querySelector('input#persona_direccion').focus();
				});
				return false;
			}

			let formData = new FormData();
			    formData.append('nombre',persona_nombre); 
			    formData.append('apellidos',persona_apellidos); 
			    formData.append('correo',persona_email); 
			    formData.append('telefono',persona_telefono); 
			    formData.append('direccion',persona_direccion); 
			    formData.append('desde','web4u'); 
			    formData.append('idioma',infoWeb.idioma||'ES'); 
			    formData.append('pais',infoWeb.pais||'USA'); 			   
				//procesar con js
				var seproceso=false;*/

				/*let xy=posDataOK({url:URLAPI,formData:formData,header:{'Accept': 'application/json','token':TOKENAPI}}).then((rs)=>{
					persona_user={
						nombre:persona_nombre,
						apellidos:persona_apellidos,
						email:persona_email,
						telefono:persona_telefono,
						direccion:persona_direccion,
					}					
					return true;
				}).catch((ev)=>{
					console.log(ev);
					return false;
				});*/
				//console.log(actions);
				//console.log(xy);
				return true;
    },

    createOrder: function(data, actions){
    	if(params.moneda=='PEN'){
    		params.moneda='USD';
    		params.monto=parseFloat(parseFloat(params.monto)*soladolares).toFixed(2);    		
    	}
    	else if(params.moneda=='COP'){
    		params.moneda='USD';
    		params.monto=parseFloat(parseFloat(params.monto)*pesoscolobianosadollares).toFixed(2);    		
    	}
     return actions.order.create({
        purchase_units: [{"amount":{"currency_code":params.moneda,"value":params.monto}}]
      });
    },

    onApprove: function(data, actions) {
      return actions.order.capture().then(function(details){
      	console.log(details);
      	if(details.status=="COMPLETED"){          
          return procesarDatos(details.payer);
      	}else{
      		Swal.fire({
					  title: 'Oops !',
		  			text: "<?=JrTexto::_("Your payment has not been processed, please try again")?>",
		  			icon: 'warning',
					  timer:10000,
				  });
      	}      	
      });
    },

    onError: function(err) {
      //console.log(err);
    }
  }).render('#paypal-button-container');
 } 


function isNumber(value){ return typeof value === 'number' && !isNaN(value); }
function isObject(value){ return _typeof(value) === 'object' && value !== null; }
function isFunction(value){ return typeof value === 'function'; }
function toArray(value){ return Array.from ? Array.from(value) : slice.call(value); }
function forEach(data, callback){
  if(data && isFunction(callback)){
    if(Array.isArray(data) || isNumber(data.length) /* array-like */){
        toArray(data).forEach(function(value, key){callback.call(data, value, key, data); });
    }else if(isObject(data)){
      Object.keys(data).forEach(function(key){callback.call(data, data[key], key, data); });
    }
  }
  return data;
}

var soladolares=0.27;// 1 sol a dollares;
var pesoscolobianosadollares=0.00027;
/*****funciones de pasarela de pagos ****/
var cargopaypal=false;
function cargarpaypal(currency,params){
	if(currency=='PEN'||currency=='PE') currency='USD';
	else if(currency=='COP'||currency=='COP$') currency='USD';
	var url = URLPAYPAL + (currency||'USD');
 	var paypal_script = document.createElement("script"),
        head = document.head || document.getElementsByTagName("head")[0];
    paypal_script.onload = function () {
        // do whatever you want after it has been loaded
        // createPaypalButton() -- example here created the paypal button
        cargopaypal=true;
    };
    paypal_script.src = url;
    paypal_script.async = true;
    head.insertBefore(paypal_script, head.firstChild);
    document.querySelector('#paypal-button-container').innerHTML="";
    initPayPalButton(params);
}

function procesarDatos(infopago){
  var data=new FormData();     
      data.append('idempresa',idempresa);
      data.append('idproyecto',idproyecto);
      data.append('idpersona',datosalumno.idpersona);
      data.append('idmatricula_sc',idmatricula);
      data.append('monto_pago',deudawebcar.monto_base);
      data.append('cantidad_pago',1);
      //data.append('idproducto_sc',1);
      
  posDataOK({url:_sysUrlsks_+'codigos_pagos/actualizar',formData:data}).then((rs)=>{
       console.log(rs);
    if(rs.code==200){	   
      Swal.fire({
          title: '<?=JrTexto::_("Congratulations, Payment made correctly")?>',
          html: '<?=JrTexto::_("We will close this window for safety and return to the course")?>.',
          icon: 'success',
        //timer:10000,
      }).then((result) => {
        window.close();
      });  
    }else{
      Swal.fire({
        title: 'Oops !',
          html: `${'<?=JrTexto::_("Your payment has been processed correctly")?>'}, 
              ${'<?=JrTexto::_("but the subscription is not concrete")?>.'},<br> 
              ${'<?=JrTexto::_("contact a representative to register your applicable Payment")?>'} <br><br>`,
          icon: 'warning',
        //timer:4000,
      });				
    }					
    return true;
  }).catch((ev)=>{	
    console.log('error',ev);			
    return false;
  });
}


  $(document).ready(function(){ 
    
     var data=new FormData();
      data.append('idmatricula_externa',idmatricula);
      //data.append('idmatricula',idmatricula);
      //data.append('idempresa',idempresa);
      //data.append('idproyecto',idproyecto);
      //data.append('idpersona',datosalumno.idpersona);
      data.append('estado',1);
      posDataOK({url:_sysUrlsks_+'codigos_pagos/buscarcompleto',formData:data}).then((rs)=>{
            deudawebcar=rs.data[0];
				if(rs.code==200 && deudawebcar!=undefined){	       
            console.log(deudawebcar);
            var params={
              monto:parseFloat(deudawebcar.monto_base||'0').toFixed(2),
              moneda:deudawebcar.codigo_moneda||'USD'
            }
            if(params.moneda==null){
              params.moneda='USD';
            }            
            if(params.monto==0.00||deudawebcar.cantidad_pagos==deudawebcar.pagos_total){
              $('#montoapagar').html('<h3 style="display:inline-block; padding: 0.5ex; color: #2d9426;"><?=JrTexto::_("You no longer have outstanding debts on record")?></h3><br><button class="regresar btn btn-secondary"><i class="fa fa-refresh"></i> <?=JrTexto::_("Back")?></button>');
              $('.infopagotmp').hide();
            }else{
              $('.infopagotmp').show();
              $('#montoapagar').html('<strong>'+params.moneda+' </strong> <h3 style="display:inline-block; padding: 0.5ex; color: #2d9426;">'+params.monto+'</h3>');
              cargarpaypal(deudawebcar.codigo_moneda,params); 
            }
				}else{
					$('#montoapagar').html('<h3 style="display:inline-block; padding: 0.5ex; color: #2d9426;"><?=JrTexto::_("You have no outstanding debts on record")?></h3><br><button class="regresar btn btn-secondary"><i class="fa fa-refresh"></i> <?=JrTexto::_("Back")?></button>');		
          $('.infopagotmp').hide();
				}					
				return true;
			}).catch((ev)=>{	
				console.log('error',ev);			
				return false;
			});

    $('.testerproceso').on('click',function(ev){
      procesarDatos(false);
    })
    $('body').on('click','.regresar',function(ev){
      window.close();
    })
})
</script>