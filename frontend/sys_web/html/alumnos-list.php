<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_('Back')?></a></li> 
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li-->        
        <li class="active">&nbsp;<?php echo ucfirst(JrTexto::_("Students")); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
  .pd2{margin-bottom: 1ex;}
  .bootstrap-select > .dropdown-toggle{
    border-color: rgb(70, 131, 175);
    border-radius: 0.5ex;
  }

   .bootstrap-select > .dropdown-toggle::before {
    font-family: FontAwesome;
    font-size: 1.3em;
    content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #0070a0;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: #FFF;
    border-radius: 0px 0.3ex 0.3ex 0px;
}

#ventana_<?php echo $idgui; ?> label {
   margin-bottom: 1px; 
}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
    <div class="row">
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1" >
        <label class="control-label"><?php echo JrTexto::_('DRE'); ?></label>
        <div class="cajaselect">
          <select id="iddre" name="iddre" class="form-control" idsel="<?php echo @$this->iddre; ?>" >
            <option value="0"><?php echo JrTexto::_('Only'); ?></option>
              <?php 
              if(!empty($this->dress))
              foreach ($this->dress as $fk) { ?>
                <option value="<?php echo $fk["iddre"]?>" <?php echo @$this->iddre==$fk["iddre"]?'selected="selected"':'';?> ><?php echo $fk["descripcion"] ?></option>
              <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1" >
        <label class="control-label"><?php echo JrTexto::_('Ugel'); ?></label>
        <div class="cajaselect">
          <select id="idugel" name="idugel" class="form-control" idsel="<?php echo @$this->idugel; ?>">
            <option value="0"><?php echo JrTexto::_('Only'); ?></option>
          </select>
        </div>
      </div>
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1" >
        <label class="control-label"><?php echo JrTexto::_('Local/IIEE'); ?></label>
        <div class="cajaselect">
          <select id="idlocal" name="idlocal" class="form-control" idsel="<?php echo @$this->idlocal; ?>">
            <option value="0"><?php echo JrTexto::_('Only'); ?></option>
          </select>
        </div>
      </div>
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2 tipoempresa1">
        <label class="control-label"><?php echo JrTexto::_('Classroom'); ?></label>
        <div class="cajaselect">
          <select id="idambiente" name="idambiente" class="form-control" idsel="<?php echo @$this->idambiente; ?>" >
            <option value="0"><?php echo JrTexto::_('Only'); ?></option>
          </select>
        </div>
      </div>
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2" >
         <label class="control-label"><?php echo JrTexto::_('Groups/Area'); ?></label>
        <div class="cajaselect">
          <select id="idgrupoaula" name="idgrupoaula" class="form-control" idsel="<?php echo @$this->idgrupoaula; ?>" >
            <option value="0"><?php echo JrTexto::_('New'); ?></option>
          </select>
        </div>

        <!--div class="cajaselect" style="border: 1px solid #4683af !important">
          <select id="idgrupoaula" name="idgrupoaula" class="form-control selectpicker" multiple title="<?php //echo JrTexto::_('Free groups'); ?>" data-size="10">            
          </select>
        </div-->
      </div>
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2">
        <label class="control-label"><?php echo JrTexto::_('Course'); ?></label>
        <!--div class="cajaselect" style="border: 1px solid #4683af !important"!-->
          <select id="idcurso" name="idcurso" class="form-control selectpicker" idsel="<?php echo @$this->idcurso; ?>" multiple title="<?php echo JrTexto::_('Selected Courses'); ?>" data-size="10">
              <?php if(!empty($this->fkcursos))
              foreach ($this->fkcursos as $fk) { ?>
                <option value="<?php echo $fk["idcurso"]?>" <?php echo @$this->idcurso==$fk["idcurso"]?'selected="selected"':'';?>><?php echo $fk["nombre"] ?></option>
              <?php } ?>
          </select>
        <!--/div-->
      </div> 
      <div class="form-group col-xs-6 col-sm-4 col-md-3">
        <label class="control-label"><?php echo JrTexto::_('Grade'); ?></label>
        <div class="cajaselect">
          <select  name="idgrado" id="idgrado" idsel="<?php echo @$this->idgrado; ?>" class="form-control fkcbgrado" >
            <option value="0"><?php echo JrTexto::_('Only Grade'); ?></option>
              <?php 
              if(!empty($this->grados))
              foreach ($this->grados as $r) { ?>
                <option value="<?php echo $r["idgrado"]?>" <?php echo @$this->idgrado==$r["idgrado"]?'selected="selected"':'';?>>
                  <?php echo JrTexto::_('Grade').": ".JrTexto::_($r["abrev"])." : ".JrTexto::_($r["descripcion"]) ?></option>
               <?php } ?>                        
          </select>
        </div>
      </div>
      <div class="col-xs-6 col-sm-4 col-md-3 form-group">
        <label class="control-label"><?php echo JrTexto::_('Section'); ?></label>
        <div class="cajaselect">
          <select  name="idsesion" id="idsesion" idsel="<?php echo @$this->idseccion; ?>" class="form-control fkcbseccion" >
            <option value="0"><?php echo JrTexto::_('Only Section'); ?></option>
              <?php 
              if(!empty($this->seccion))
              foreach ($this->seccion as $s){ ?>
                <option value="<?php echo $s["idsesion"]?>" <?php echo @$this->idseccion==$s["idsesion"]?'selected="selected"':'';?>><?php echo JrTexto::_('Section')." ".$s["descripcion"] ?></option>
               <?php } ?>                        
          </select>
        </div>
      </div>      
           
      <div class="form-group col-xs-6 col-sm-4 col-md-3 pd2">
        <label><?php echo  ucfirst(JrTexto::_("Gender"))?></label>
        <div class="cajaselect">
          <select id="sexo" name="sexo" class="form-control" >
            <option value=""><?php echo JrTexto::_('All Gender'); ?></option>
              <?php 
              if(!empty($this->fksexo))
              foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
               <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group  col-xs-6 col-sm-4 col-md-3 pd2" >
        <label><?php echo  ucfirst(JrTexto::_("Marital status"))?></label>
         <div class="cajaselect">
          <select id="estado_civil" name="estado_civil" class="form-control" >
            <option value=""><?php echo JrTexto::_('All marital status'); ?></option>
              <?php 
              if(!empty($this->fkestado_civil))
              foreach ($this->fkestado_civil as $fkestado_civil) { ?><option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> ><?php echo $fkestado_civil["nombre"] ?></option><?php } ?>
            </select>
        </div>
      </div>                                                           
      <!--div class="col-xs-6 col-sm-4 col-md-3 pd2" >
        <div class="cajaselect">
          <select name="estado" id="estado" class="form-control">
              <option value=""><?php echo JrTexto::_('All status'); ?></option>
              <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
              <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
          </select>
        </div>
      </div-->                                             
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label><?php echo  ucfirst(JrTexto::_("Search")).": N° Doc/".JrTexto::_("Name");?></label>
          <div class="input-group">
            <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
            <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
          </div>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 text-center">             
         <a class="btn btn-success btnagregarpersona" nuser="1" href="<?php echo $this->documento->getUrlSitio();?>/personal/adicionar" data-titulo="<?php echo ucfirst(JrTexto::_('Add')) ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
         <a class="btn btn-warning btnagregarpersona" nuser="varios" href="<?php echo $this->documento->getUrlSitio();?>/personal/importar" data-titulo="<?php echo ucfirst(JrTexto::_('Import'))." ".JrTexto::_('Students') ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Import')?></a>
      </div>

    </div>
  </div>
  <hr>
</div>
  <div class="row">         
     <div class="col table-responsive">
        <table class="table table-striped table-hover">
          <thead>
            <tr class="headings">
              <th>#</th>
                <th><?php echo JrTexto::_("Course") ;?></th>
                <th style="min-width: 200px;"><?php echo JrTexto::_("Student") ;?></th>
                <!--th><?php //echo JrTexto::_("Gender") ;?></th-->
                <th style="min-width: 200px;"><?php echo JrTexto::_("Contact") ;?></th>
                <!--th><?php //echo JrTexto::_("Cellphone") ;?></th>
                <th><?php //echo JrTexto::_("E-mail") ;?></th>                   
                <th><?php //echo JrTexto::_("User") ;?></th-->                   
                <th><?php echo JrTexto::_("Photo") ;?></th>
                <th><?php echo JrTexto::_("Status") ;?></th>
                <th><?php echo JrTexto::_("Is Demo") ;?></th>
                <th><?php echo JrTexto::_("Expiration date") ;?></th>
                <!--th><?php //echo JrTexto::_("Situacion") ;?></th-->
                <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5dae03704f6f0='';
function refreshdatos5dae03704f6f0(){
  tabledatos5dae03704f6f0.ajax.reload();
}
$(document).ready(function(){  
  var infoemp=__infoempresa();
var _imgdefecto='static/media/usuarios/user_avatar.jpg';
var ini=true;

  var estados5dae03704f6f0={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5dae03704f6f0='<?php echo ucfirst(JrTexto::_("personal"))." - ".JrTexto::_("edit"); ?>';
  var draw5dae03704f6f0=0;
  var ventana_5dae03704f6f0=$('#ventana_<?php echo $idgui; ?>');
  //if(ventana_5dae03704f6f0.find('select#idcurso').val()=='0')ventana_5dae03704f6f0.find('.table').DataTable();
  
  ventana_5dae03704f6f0.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5dae03704f6f0()
  }).on('blur','.textosearchlist',function(ev){refreshdatos5dae03704f6f0()
  }).on('change','#sexo',function(ev){refreshdatos5dae03704f6f0()
  }).on('change','#estado_civil',function(ev){refreshdatos5dae03704f6f0() 
  }).on('click','.btnbuscar',function(ev){ refreshdatos5dae03704f6f0()
  }).on("click",'.btn-eliminarhistorial',function(){
      var formData = new FormData();
      idpersona=$(this).attr('data-id');
      formData.append('idpersona',idpersona);
    
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete history ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           __sysAyax({ 
            fromdata:formData,
              url:_sysUrlBase_+'json/persona/borrarhistorial',
              showmsjok:true   
          });
        }
      }); 
    })
  //}).on('change','#ubigeo',function(ev){refreshdatos5dae03704f6f0();
  //}).on('change','#tipodoc',function(ev){refreshdatos5dae03704f6f0();
  //}).on('change','#estado',function(ev){refreshdatos5dae03704f6f0();
  //}).on('change','#datefechanac',function(ev){ refreshdatos5dae03704f6f0();
  //ventana_5dae03704f6f0.find('select#idrol').trigger('change');

  var adicionar__=function(_this){
    var url=_this.attr('href');
    var nuser=_this.attr('nuser')||1;
      if(url.indexOf('?')!=-1) url+='&plt=modal';
      else url+='?plt=modal';
      url=url+'&idrol=3&idproyecto='+$('input#idproyecto').val();
      var titulo=_this.attr('data-titulo')||'';
      var _md=__sysmodal({'url':url,'titulo':titulo});
      var refresh='refresh'+__idgui();
      //_this.addClass(refresh);
      _md.on(refresh, function(ev){
        var id=$(this).attr('data-id');
        var dni=$(this).attr('data-dni');
        var nombre=$(this).attr('data-nombre');
         var selectidcursodetalle=ventana_5dae03704f6f0.find('select#idcurso option:selected');
         var idgruposdetalle=[];
         var idalumnos=[];
         $.each(selectidcursodetalle,function(i,v){
          var iddet=$(v).attr('idgrupoauladetalle')||undefined;         
          if(iddet!=undefined){
            idgruposdetalle.push($(v).attr('idgrupoauladetalle'));
          }          
        })

         if(nuser==1) idalumnos.push(id); // cuando es solo un alumno 
         else{
          var idpersonas=_md.attr('idpersonas').split(',');
          $.each(idpersonas,function(i,v){if(v!='') idalumnos.push(v);});
         }
         var fd = new FormData();
          fd.append('idalumnos',JSON.stringify(idalumnos));
          fd.append('idgruposauladetalle',JSON.stringify(idgruposdetalle));
          fd.append('variosgrupos',true);        
        __sysAyax({
          fromdata:fd,
          async:false,
          url:_sysUrlBase_+'json/acad_matricula/guardar',
          showmsjok:true,
          callback:function(rs){
            if(rs.code==200){
              dt=JSON.parse(rs.data);
            }           
           refreshdatos5dae03704f6f0();
        }})        
        _md.trigger('cerramodal');        
      })
      _md.on('shown.bs.modal',function(){
        var formventana=_md.find('.formventana');
        formventana.attr('datareturn',refresh).addClass('ismodaljs');
        if(nuser==1){ if(_md.find('#cargarscript').length)  $.getScript(_md.find('#cargarscript').attr('src'),function(){ frmpersonabuscar(_md);});
        }else{
          if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_personaimport(_md);
            })
          });
        } 
      })
  }
  ventana_5dae03704f6f0.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idmatricula',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/acad_matricula/eliminar',
            callback:function(rs){
              if(rs.code==200){
                refreshdatos5dae03704f6f0()
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/persona/setcampo','masvalores':{idpersona:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idmatricula',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_matricula/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            refreshdatos5dae03704f6f0()
          }
        }
    });
  }).on('click','.btn-activardemo',function(){
     _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idpersona',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/persona/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5dae03704f6f0.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var idrol=ventana_5dae03704f6f0.find('#idrol').val()||3;
    var url=url+'&idrol='+idrol;
    var titulo=$(this).attr('data-titulo')||'';    
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      refreshdatos5dae03704f6f0()
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frmpersona(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_personaimport(_md);
            })
          });
       }
    })
  }).on('click','.btnagregarpersona',function(ev){
      ev.preventDefault();
      var _this=$(this); 
      var idgrupo=ventana_5dae03704f6f0.find('select#idgrupoaula').val();
      var selectidcursodetalle=ventana_5dae03704f6f0.find('select#idcurso option:selected');
      var idcursosdetalle=[];
        $.each(selectidcursodetalle,function(i,v){
          var iddet=$(v).attr('idgrupoauladetalle')||undefined;
          if(idgrupo!=0){// nuevo
            if(iddet!=undefined){
              idcursosdetalle.push($(v).attr('idgrupoauladetalle'));
            }
          }else{ // grupo ya existe
            idcursosdetalle.push($(v).val());
          }
        })
      if(idcursosdetalle.length<1){
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>','<?php echo JrTexto::_('Selected courses');?>','error');
       return;
      }
      if(idgrupo==0){
        __notificar({
          title:'<?php echo JrTexto::_('Group does not exist') ?>',
          text:'<?php echo JrTexto::_('You want us to create a group / Area with the selected courses ?') ?>',
          showCancelButton:true,
          callback:function(r){
          if(r.value==true){
            var fd = new FormData();
            fd.append('idlocal',ventana_5dae03704f6f0.find('select#idgrupoaula').val());
            fd.append('idambiente',ventana_5dae03704f6f0.find('select#idambiente').val());
            fd.append('idgrado',ventana_5dae03704f6f0.find('select#idgrado').val());
            fd.append('idsesion',ventana_5dae03704f6f0.find('select#idsesion').val());
            fd.append('varioscursos',true);
            fd.append('idcurso',JSON.stringify(idcursosdetalle));
            __sysAyax({
              fromdata:fd,
              async:false,
              url:_sysUrlBase_+'json/acad_grupoauladetalle/guardar',
              callback:function(rs){
                var idgrupoaula=rs.idgrupoaula;
               ventana_5dae03704f6f0.find('select#idgrupoaula').attr('idsel',idgrupoaula);
               ventana_5dae03704f6f0.find('select#idambiente').trigger('change');
               adicionar__(_this);
            }})
          }
        }});      
      }else adicionar__(_this);     
  }).on("click",".btn-cambiarfecha span",function(e){
    _s=$(this);
    _s.hide();
    _e=_s.parent();
    _i=document.createElement("input");
    _i.value=_s.text();
    _i.type='date';
    _i.min='<?php echo date('Y-m-d') ?>'; 
    _e.append(_i);    
    _i.addEventListener('change',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
      v=$(this).val();
      _s.text(v).show();
      var dt=new FormData();
      dt.append('idmatricula',_e.attr('data-id'));
      dt.append('campo','fecha_vencimiento');
      dt.append('valor',v);
      __sysAyax({ fromdata:dt, url:_sysUrlBase_+'json/acad_matricula/setcampo', showmsjok:true });    
      $(this).remove();
    })    
  })

  // corregido
  var grupos=[];
  var gruposauladetalle=[];
  ventana_5dae03704f6f0
  .on('change','select#iddre',function(ev){  
    console.log(1);  
    $sel=ventana_5dae03704f6f0.find('select#idugel');
    $sel.children('option:eq(0)').siblings().remove();
    if($(this).val()==''||$(this).val()=='0') {
      return $sel.trigger('change');
    }
    var fd = new FormData();
    fd.append('iddre',$(this).val()); 
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/ugel',
      callback:function(rs){        
        var idsel=$sel.attr('idsel');
        dt=rs.data;
        $.each(dt,function(i,v){
          $sel.append('<option value="'+v.idugel+'" '+(v.idugel==idsel?'selected="selected"':'')+ '>'+v.idugel+" : "+ v.descripcion+'</option>');
        })
        $sel.trigger('change');
    }})
  }).on('change','select#idugel',function(ev){
    var $selugel=ventana_5dae03704f6f0.find('select#idlocal');
    $selugel.children('option:eq(0)').siblings().remove();
    if($(this).val()==''||$(this).val()=='0') {
      return $selugel.trigger('change');
    }     
    var fd = new FormData();
    fd.append('idugel',$(this).val()); 
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/local',
      callback:function(rs){
        var idsel=$selugel.attr('idsel');
        dt=rs.data;
        $.each(dt,function(i,v){
          $selugel.append('<option value="'+v.idlocal+'" '+(v.idlocal==idsel?'selected="selected"':'')+ '>'+v.idlocal+" : "+ v.nombre+'</option>');
        })
        $selugel.trigger('change');
    }})
  }).on('change','select#idlocal',function(ev){
    var $sello=ventana_5dae03704f6f0.find('select#idambiente');
    $sello.children('option:eq(0)').siblings().remove();
    if($(this).val()==''||$(this).val()=='0') {
      return $sello.trigger('change');
    }     
    var fd = new FormData();
    fd.append('idlocal',$(this).val()); 
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/ambiente',
      callback:function(rs){
        var idsel=$sello.attr('idsel');
        dt=rs.data;
        $.each(dt,function(i,v){
           $sello.append('<option value="'+v.idambiente+'" '+(v.idambiente==idsel?'selected="selected"':'')+ '>'+v.tipo_ambiente+" : "+ v.numero+'</option>');
        })
        $sello.trigger('change');
    }})
  }).on('change','select#idambiente',function(ev){
    var $selg=ventana_5dae03704f6f0.find('select#idgrupoaula');    
    $selg.children('option:eq(0)').siblings().remove();      
    if($(this).val()=='') {
      return $selg.trigger('change');
    } 
    var fd = new FormData();
    fd.append('idlocal', ventana_5dae03704f6f0.find('select#idlocal').val());
    fd.append('idambiente',$(this).val()); 
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/acad_grupoauladetalle',
      callback:function(rs){
        var idsel=$selg.attr('idsel');
        dt=rs.data;
        grupos=[];
        $.each(dt,function(i,v){
          if(grupos[v.idgrupoaula]==undefined){
            $selg.append('<option value="'+v.idgrupoaula+'" '+(v.idgrupoaula==idsel?'selected="selected"':'')+ '>'+v.idgrupoaula+" : "+ v.strgrupoaula+'</option>');
            var tt=[];
            tt[v.idgrupoauladetalle]=v;
            grupos[v.idgrupoaula]={'nombre':v.nombre,'cursossel':tt};       
          }else{
            var tt=grupos[v.idgrupoaula].cursossel;
            tt[v.idgrupoauladetalle]=v;
             grupos[v.idgrupoaula].cursossel=tt;
          }
        })
        $selg.trigger('change');
    }})
  }).on('change','select#idgrupoaula',function(ev){
    $selcursos=ventana_5dae03704f6f0.find('select#idcurso');
    $selcursos.children('option').removeAttr('selected').removeAttr('idgrupoauladetalle');
    gruposauladetalle=[];
    var _grupossel=grupos[$(this).val()]||{nombre:'',cursossel:[]};
    if(_grupossel.cursossel.length){
      $.each(_grupossel.cursossel,function(i,v){
        if(v!=undefined){
          var opt=$selcursos.find('option[value="'+v.idcurso+'"]');
          if(opt.length){
            gruposauladetalle.push(v.idgrupoauladetalle);
            opt.attr('selected','selected').attr('idgrupoauladetalle',v.idgrupoauladetalle);
          }        
        }
      })
    }
    $selcursos.selectpicker('render');
    refreshdatos5dae03704f6f0()
  })

  tabledatos5dae03704f6f0=ventana_5dae03704f6f0.find('.table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,      
      "ajax":{
        url:_sysUrlBase_+'json/acad_matricula',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.sexo=ventana_5dae03704f6f0.find('#sexo').val();
            d.estado_civil=ventana_5dae03704f6f0.find('#estado_civil').val();
            d.texto=ventana_5dae03704f6f0.find('input.textosearchlist').val();
            d.orderby='strcurso ASC, stralumno ASC'
            var selectidcursodetalle=ventana_5dae03704f6f0.find('select#idcurso option:selected');
            var idcursosdetalle=[];
            $.each(selectidcursodetalle,function(i,v){
              var iddet=$(v).attr('idgrupoauladetalle')||undefined;
              if(iddet!=undefined)
              idcursosdetalle.push($(v).attr('idgrupoauladetalle'));
            })
            if(idcursosdetalle.length>0)
            d.idgrupoauladetalle=idcursosdetalle;
            var idgrupoaula=ventana_5dae03704f6f0.find('select#idgrupoaula').val();
            if(idgrupoaula==0)d.idalumno=-1;
            if(ini==true) d.idalumno=-1;
            draw5dae03704f6f0=d.draw;
        },
        dataSrc:function(json){
          if(ini==true)ini=false;
          var data=json.data;  

          json.draw = draw5dae03704f6f0;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
             var varfoto=data[i].foto||'static/media/usuarios/user_avatar.jpg';
              var ifoto=varfoto.lastIndexOf("static/");
              if(ifoto>=0) varfoto=_sysUrlBase_+varfoto.substring(ifoto);
              if(ifoto==-1){
                var ifoto=varfoto.lastIndexOf("/");
                var ifoto2=varfoto.lastIndexOf(".");
                if(ifoto>=0 && ifoto2>=0) varfoto=_sysUrlBase_+'static/media/usuarios/'+varfoto.substring(ifoto);
                else if(ifoto2>=0) varfoto=_sysUrlBase_+'static/media/usuarios/'+varfoto;
                else varfoto=_sysUrlBase_+'static/media/usuarios/user_avatar.jpg';
              }
            varfoto='<img src="'+varfoto+'" class="img-thumbnail" style="max-height:70px; max-width:50px;">';
            var fv=data[i].fecha_vencimiento||'<?php echo date('Y-m-d');?>';
            fv=fv.substring(0,10);
            var tel=data[i].telefono!=''?('<i class="fa fa-phone"></i> '+data[i].telefono):'';
            var cel=(data[i].celular!=''&&tel!='')?(tel+'/ <i class="fa fa-mobile"></i> '+data[i].celular+'<br>'):'';
            cel=(data[i].celular!=''&&tel=='')?('<i class="fa fa-mobile"></i> '+data[i].celular+'<br>'):cel;
            var ema=data[i].email!=''?('<i class="fa fa-envelope"></i> '+data[i].email)+'<br>':'';
            datainfo.push([
              (i+1),
              data[i].strcurso,
              (data[i].dni+'<br>'+data[i].stralumno),
              //data[i].sexo, 
              cel+ema+'<i class="fa fa-user"></i> '+data[i].usuario,              
              varfoto,
              '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idmatricula+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5dae03704f6f0[data[i].estado]+'</a>',
               '<a href="javascript:;"  class="btn-activardemo  '+(data[i].esdemo==0?'':'active')+' " data-campo="esdemo"  data-id="'+data[i].idalumno+'"> <i class="fa fa'+(data[i].esdemo=='1'?'-check':'')+'-circle-o fa-lg"></i></a>','<a href="javascript:;" class="btn-cambiarfecha" data-id="'+data[i].idmatricula+'"><span>'+fv+'</span></a>',
              '<a class="btn btn-sm btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/personal/perfil/?idpersona='+data[i].idalumno+'&rol='+(ventana_5dae03704f6f0.find('#idrol').val())+'" data-titulo="'+tituloedit5dae03704f6f0+'"><i class="fa fa-eye"></i></a>  <a class="btn btn-sm btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/personal/editar/?idpersona='+data[i].idalumno+'&rol=3" data-titulo="'+tituloedit5dae03704f6f0+'"><i class="fa fa-edit"></i></a>  <a class="btn btn-sm btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/personal/cambiarclave/?idpersona='+data[i].idalumno+'&rol=3" data-titulo="<?php echo JrTexto::_('Change Password');?>"><i class="fa fa-key"></i></a>  <a class="btn-eliminarhistorial btn btn-sm" href="javascript:;" data-titulo="<?php echo JrTexto::_('Delete history');?>" data-id="'+data[i].idalumno+'"><i class="fa fa-history"></i></a> <a class="btn-eliminar btn btn-sm" href="javascript:;" data-id="'+data[i].idmatricula+'" ><i class="fa fa-trash"></i></a>'
              //<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/personal/roles/?idpersona='+data[i].idalumno+'" data-titulo="Roles"><i class="fa fa-user-secret"></i></a>
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });
  if(infoemp.tipoempresa1==1) $('#iddre').trigger('change');
  else $('#idlocal').trigger('change');
});
</script>