<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$imgdefecto=URL_MEDIA."/static/media/nofoto.jpg";
if(!$ismodal){?>
<div class="paris-navbar-container"></div>
  <script>
    addItemNavBar({
      text: '<?php echo JrTexto::_("Categorias"); ?>'
    });
    addButonNavBar({
      class: 'btn btn-success btn-sm btnvermodal',
      text: '<i class="fa fa-plus" pk=""></i> agregar',
      href: '#' // btnvermodal
    });
  </script>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;} 
  input::placeholder {color: #c0bebd !important;  font-size: 12px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>">
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <div class="row" id="addselect">            
            <div class="col-md-3 col-sm-4 col-xs-6">   
                <label>Nivel/Carrera/Categoria</label>         
                <div class="select-ctrl-wrapper select-azul">
                <select id="idpadre"  class=" form-control selchange">
                  <option value="0">Unico</option>
                <?php
                if(!empty($this->datos))
                foreach ($this->datos as $k=>$v){ if($v["idpadre"]==0){?>
                  <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
                <?php }   } ?>                
              </select>
            </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 " > 
              <label>Estado</label>             
              <div class="select-ctrl-wrapper select-azul">
                <select name="estado" id="estado" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All Estado"))?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                  <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label><?php echo  ucfirst(JrTexto::_("Text to search"))?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr class="headings">
                    <th>#</th>                             
                    <th class="nombrecat"><?php echo JrTexto::_("Categoria / Nivel / Carrera"); ?></th>
                    <th><?php echo JrTexto::_("Estado"); ?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions'); ?></span></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>     
    </div>
  </div>
</div>

<div style="display: none;">
  <div class="row frmventana" id="frm<?php echo $idgui; ?>" idgui="<?php echo $idgui; ?>">
    <div class="col-md-12">
      <div id="msj-interno"></div>
      <form method="post" id="frm-<?php echo $idgui;?>" tb="acad_categorias" target="" enctype="" class="formventana form-horizontal form-label-left" idgui="<?php echo $idgui;?>">
          <input type="hidden" name="idcategoria" id="idcategoria" value="">
          <input type="hidden" id="orden" name="orden" step="1" value="0"  min="1" class="form-control" />
          <div class="form-group col-md-12 col-sm-12">   
            <label>Nivel/Carrera/Categoria</label>         
            <div class="select-ctrl-wrapper select-azul">
              <select id="idpadre" name="idpadre"  class=" form-control selchange">
                <option value="0">Unico</option>
              <?php
              if(!empty($this->datos))
              foreach ($this->datos as $k=>$v){ if($v["idpadre"]==0){?>
                <option value="<?php echo $v["idcategoria"]; ?>" padre="<?php echo $v["idpadre"]; ?>" ><?php echo ucfirst($v["nombre"]); ?></option>
              <?php }   } ?>                
              </select>
            </div>
          </div>


          <div class="form-group col-md-12 col-sm-12">
              <label class="control-label"><?php echo JrTexto::_('Nombre');?> <span class="required"> * </span></label>            
              <input type="text"  id="nombre" name="nombre" required="required" class="form-control" value="">                                  
          </div>

          <div class="form-group col-md-12 col-sm-12">
            <label class="control-label"><?php echo JrTexto::_('Descripcion');?> <span class="required"> * </span></label>
            <textarea id="descripcion" name="descripcion" class="form-control" ></textarea>
          </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Imagen');?> <span class="required"> * </span></label>
              <div style="position: relative;" class="frmchangeimage text-center" id="archivofondo">
                  <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                  <div id="sysfileimg" class="aquiseveimagen" data-old='imagen' data-file='<?php echo $imgdefecto; ?>'>
                  <img src="<?php echo $imgdefecto;?>" nombreguardar="_cat" class="__subirfile img-fluid center-block" data-type="imagen" id="imagen" style="max-width: 200px; max-height: 150px;">
                  <input type="hidden" name="imagen" value="<?php echo $imgdefecto;?>">
                  </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label"><?php echo JrTexto::_('Estado');?> <span class="required"> * </span></label>
              <div class="">
                <a style="cursor:pointer;" class="chkformulario fa  fa-check-circle-o" data-value="1"  data-valueno="0" data-value2="">
                 <span> <?php echo JrTexto::_("Active");?></span>
                 <input type="hidden" id="estado" name="estado" value="1"> 
                 </a>                              
              </div>
            </div>
            <div class="clearfix"></div>
            
          <hr>
          <div class="col-md-12 form-group text-center">
            <button id="btn-save" type="submit" tb="acad_categorias" class="btn btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            <a type="button" class="btn btn-warning cerrarmodal" href="#" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos5eaba8b672583='';
function refreshdatos5eaba8b672583(){
    tabledatos5eaba8b672583.ajax.reload();
}
$(document).ready(function(){  
  var estados5eaba8b672583={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5eaba8b672583='<?php echo ucfirst(JrTexto::_("Categorias"))." - ".JrTexto::_("edit"); ?>';
  var draw5eaba8b672583=0;
  var _imgdefecto='<?php echo $imgdefecto; ?>';
  var ventana_5eaba8b672583=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5eaba8b672583=ventana_5eaba8b672583.find('.table').DataTable(
    { "searching": false,
      "processing": false,      
      "ajax":{
        url:_sysUrlBase_+'json/acad_categorias',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.idpadre=$('#idpadre').val();
            if(d.idpadre==0){
              $('#ventana_<?php echo $idgui; ?> .nombrecat').text('<?php echo JrTexto::_("Categoria / Nivel / Carrera"); ?>');
            }else{
              $('#ventana_<?php echo $idgui; ?> .nombrecat').text('<?php echo JrTexto::_("Sub categoria / Sub Nivel / Modulo"); ?>');
            }
            d.estado=$('#estado').val();
            d.texto=$('#texto').val();
            draw5eaba8b672583=d.draw;
           
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5eaba8b672583;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){             
            var varimagen=data[i].imagen||'';
            varimagen=varimagen.replace(_imgdefecto,'');                            
            if(varimagen!='') varimagen='<img src="'+_sysUrlBase_+varimagen+'" class="img-thumbnail" style="max-height:70px; max-width:50px;">';                                          
            datainfo.push([            
              (i+1),
              data[i].nombre,                
                '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idcategoria+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5eaba8b672583[data[i].estado]+'</a>',
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="#" pk="'+data[i].idcategoria+'" data-titulo="'+tituloedit5eaba8b672583+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idcategoria+'" idpadre="'+data[i].idpadre+'" ><i class="fa fa-trash"></i></a>'
            ]);
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5eaba8b672583.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5eaba8b672583();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5eaba8b672583();
  }).on('change','select#idpadre',function(ev){refreshdatos5eaba8b672583();
  }).on('change','select#estado',function(ev){refreshdatos5eaba8b672583();
  }).on('click','.btnbuscar',function(ev){ refreshdatos5eaba8b672583();
  })
 

  ventana_5eaba8b672583.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idcategoria',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/acad_categorias/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5eaba8b672583.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/acad_categorias/setcampo','masvalores':{idcategoria:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idcategoria',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/acad_categorias/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5eaba8b672583.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
   e.preventDefault();        
    var el=$(this);
    __modalacad_categorias(el);   
  })

  $(".paris-navbar-container").on('click', '.btnvermodal', function(e){
    e.preventDefault();        
    var el=$(this);
    __modalacad_categorias(el);
  })

  /*Formulario*/
  var __modalacad_categorias=function(el){
    var titulo = '<?php echo JrTexto::_("Categorias"); ?>';
    var frm=$('#frm<?php echo $idgui; ?>').clone(); 
    var filtros=$('#ventana_<?php echo $idgui; ?>');    
    var _md = __sysmodal({'html': frm,'titulo': titulo});
    _md.on('click','.chkformulario',function(ev){
      _this = $(this);           
      if(_this.hasClass('fa-check-circle-o')) {            
        _this.removeClass('fa-check-circle-o').addClass('fa-circle-o');
        _this.children('span').text('Inactivo')
        _this.children('input').val(0);
      }else{            
        _this.removeClass('fa-circle-o').addClass('fa-check-circle-o');
        _this.children('span').text('Activo');
        _this.children('input').val(1);
      }
    }).on('submit','form.formventana',function(ev){
        ev.preventDefault();
        var id=__idgui();
        $(this).attr('id',id);
        var fele = document.getElementById(id);
        var data=new FormData(fele);
        __sysAyax({ 
          fromdata:data,
          url:_sysUrlBase_+'json/acad_categorias/guardar',
          showmsjok:true,
          callback:function(rs){  
            var datacat=new FormData();    
            datacat.append('idpadre',0);
            __sysAyax({ 
              fromdata:datacat,
              url:_sysUrlBase_+'json/acad_categorias',
              callback:function(rs){ 
                if(rs.data.length){
                  filtros.find('select#idpadre').children('option').first().nextAll().remove();
                  $.each(rs.data,function(i,v){
                    filtros.find('select#idpadre').append('<option value="'+v.idcategoria+'">'+v.nombre+'</option>')
                  })
                  filtros.find('select#idpadre').val(_md.find('select#idpadre').val());
                }
               }
            });
            filtros.find('select#idpadre').val(_md.find('select#idpadre').val());
            filtros.find('select#estado').val(_md.find('input#estado').val());
            tabledatos5eaba8b672583.ajax.reload(); __cerrarmodal(_md);  
          }
        });
    })    
    _md.find('select#idpadre').html(filtros.find('select#idpadre').html());
    _md.find('select#idpadre').children('option').first().text('Ninguno');
    var pk=el.attr('pk')||'';    
    if(pk!=''){
      var data=new FormData();
      data.append('idcategoria',pk);
      __sysAyax({ 
        fromdata:data,
        url:_sysUrlBase_+'json/acad_categorias',
        callback:function(rs){ 
          if(rs.data.length){
            var dt=rs.data[0];
            _md.find('input#idcategoria').val(dt.idcategoria);
            _md.find('input#nombre').val(dt.nombre);
            _md.find('textarea#descripcion').val(dt.descripcion);
            _md.find('select#idpadre').val(dt.idpadre);
            _md.find('input#estado').val(dt.estado);
            if(dt.estado==0){
              _md.find('input#estado').siblings('span').text('<?php echo JrTexto::_('Active'); ?>');
              _md.find('input#estado').parent().removeClass('fa-check-circle-o').addClass('fa-circle');
            }
            _md.find('img#imagen').attr('nombreguardar','cat'+pk+'_'+_md.find('img#imagen').attr('id')||'cat');
            var img=dt.imagen==''?_imgdefecto:_sysUrlBase_+dt.imagen;
            _md.find('img#imagen').attr('src',img);
            _md.find('img#imagen').siblings('input').val(dt.imagen);            
          }
         }
      });
    }else{            
      if(filtros.find('select#idpadre').val()!='')
        _md.find('select#idpadre').val(filtros.find('select#idpadre').val());
      if(filtros.find('select#estado').val()!='')
        _md.find('select#estado').val(filtros.find('select#estado').val());
    }
  }
});
</script>