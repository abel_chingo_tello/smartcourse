<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!$ismodal){?>
<div class="row" id="breadcrumb"> 
  <div class="col">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_('Back')?></a></li> 
        <!--li><a href="<?php echo $this->documento->getUrlSitio();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li-->        
        <li class="active">&nbsp;<?php echo JrTexto::_("Tareas_archivosalumno"); ?></li>       
    </ol>
  </div> 
</div>
<?php } ?>
<style type="text/css">
  .btn{font-size: 14px;}
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col">    
         <div class="row">            
                                                                                                              
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <!--label><?php echo  ucfirst(JrTexto::_("text to search"))?></label-->
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0 textosearchlist" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 text-center">             
               <a class="btn btn-success btnvermodal" href="<?php echo $this->documento->getUrlSitio();?>/tareas_archivosalumno/agregar" ><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
	   <div class="row">         
         <div class="col table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idtarea") ;?></th>
                    <th><?php echo JrTexto::_("Media") ;?></th>
                    <th><?php echo JrTexto::_("Tipo") ;?></th>
                    <th><?php echo JrTexto::_("Fecha registro") ;?></th>
                    <th><?php echo JrTexto::_("Idalumno") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5e42d02e2b77a='';
function refreshdatos5e42d02e2b77a(){
    tabledatos5e42d02e2b77a.ajax.reload();
}
$(document).ready(function(){  
  var estados5e42d02e2b77a={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5e42d02e2b77a='<?php echo ucfirst(JrTexto::_("tareas_archivosalumno"))." - ".JrTexto::_("edit"); ?>';
  var draw5e42d02e2b77a=0;
  var _imgdefecto='';
  var ventana_5e42d02e2b77a=$('#ventana_<?php echo $idgui; ?>');
   tabledatos5e42d02e2b77a=ventana_5e42d02e2b77a.find('.table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Idtarea") ;?>'},
            {'data': '<?php echo JrTexto::_("Media") ;?>'},
            {'data': '<?php echo JrTexto::_("Tipo") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecha_registro") ;?>'},
            {'data': '<?php echo JrTexto::_("Idalumno") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'json/tareas_archivosalumno',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw5e42d02e2b77a=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw5e42d02e2b77a;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
                                
            datainfo.push({             
              '#':(i+1),
              '<?php echo JrTexto::_("Idtarea") ;?>': data[i].idtarea,
          '<?php echo JrTexto::_("Media") ;?>': data[i].media,
            '<?php echo JrTexto::_("Tipo") ;?>': data[i].tipo,
          '<?php echo JrTexto::_("Fecha_registro") ;?>': data[i].fecha_registro,
          '<?php echo JrTexto::_("Idalumno") ;?>': data[i].idalumno,
                        //'flag': '<a href="javascript:void(0)" data-campo="estado" data-id="'+data[i].id+'" class="btn-activar text-center '+(data[i].estado==0?'':'active')+'"><i class="fa fa-'+(data[i].estado==0?'':'check-')+'circle"></i></a>',
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlSitio_+'/tareas_archivosalumno/editar/?id='+data[i].idtarearecurso+'" data-titulo="'+tituloedit5e42d02e2b77a+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idtarearecurso+'" ><i class="fa fa-trash"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });

  ventana_5e42d02e2b77a.on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) refreshdatos5e42d02e2b77a();
  }).on('blur','.textosearchlist',function(ev){refreshdatos5e42d02e2b77a();
  })

  .on('click','.btnbuscar',function(ev){ refreshdatos5e42d02e2b77a();
  })
 

  ventana_5e42d02e2b77a.on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){ 
      var data=new FormData()
        data.append('idtarearecurso',id);        
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/tareas_archivosalumno/eliminar',
            callback:function(rs){
              if(rs.code==200){
                tabledatos5e42d02e2b77a.ajax.reload();
              }
            }
        });
      }
    })
  }).on('click','.subirfile',function(ev){
      __subirfile({file:$(this),dataurlsave:'json/tareas_archivosalumno/setcampo','masvalores':{idtarearecurso:$(this).attr('idpk'),campo:'imagen'}});
  }).on('click','.btn-activar',function(){
    _this=$(this);
    var activo=1;
    if(_this.hasClass('active')){ activo=0;}
    var data=new FormData();
    data.append('idtarearecurso',_this.attr('data-id'));
    data.append('campo',_this.attr('data-campo'));
    data.append('valor',activo);
    __sysAyax({ 
      fromdata:data,
        url:_sysUrlBase_+'json/tareas_archivosalumno/setcampo',
        showmsjok:true,
        callback:function(rs){
          if(rs.code==200){
            tabledatos5e42d02e2b77a.ajax.reload();
          }
        }
    });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&plt=modal';
    else url+='?plt=modal';
    var titulo=$(this).attr('data-titulo')||'';
    var _md=__sysmodal({'url':url,'titulo':titulo});
    var refresh='refresh'+__idgui();
    _md.on(refresh, function(ev){     
      tabledatos5e42d02e2b77a.ajax.reload();
      _md.trigger('cerramodal');
    })
    _md.on('shown.bs.modal',function(){
      var formventana=_md.find('.formventana');
       formventana.attr('datareturn',refresh).addClass('ismodaljs');
       if(formventana.hasClass('form-horizontal')){
          if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){frm(_md);});
       }else{
        if(_md.find('#cargaimportar').length)
          $.getScript(_md.find('#cargaimportar').attr('src'),function(){
            if(_md.find('#cargarscript').length)
            $.getScript(_md.find('#cargarscript').attr('src'),function(){
              frm_import(_md);
            })
          });
       }           
    })   
  })
});
</script>