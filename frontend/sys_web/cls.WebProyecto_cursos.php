<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-12-2018 
 * @copyright	Copyright (C) 19-12-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto_cursos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebProyecto_cursos extends JrWeb
{
	private $oNegProyecto_cursos;
	private $oNegProyecto;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegProyecto_cursos = new NegProyecto_cursos;
		$this->oNegCursos = new NegAcad_curso;
		$this->oNegProyecto = new NegProyecto;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idproycurso"])&&@$_REQUEST["idproycurso"]!='')$filtros["idproycurso"]=$_REQUEST["idproycurso"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["sql2"])&&@$_REQUEST["sql2"]!='')$filtros["sql2"]=$_REQUEST["sql2"];
			
			$this->datos=$this->oNegProyecto_cursos->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Proyecto_cursos'), true);
			$this->esquema = 'proyecto_cursos-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Proyecto_cursos').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegProyecto_cursos->idproycurso = @$_GET['id'];
			$this->datos = $this->oNegProyecto_cursos->dataProyecto_cursos;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Proyecto_cursos').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'proyecto_cursos-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Proyecto_cursos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idproycurso"])&&@$_REQUEST["idproycurso"]!='')$filtros["idproycurso"]=$_REQUEST["idproycurso"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["sql2"])&&@$_REQUEST["sql2"]!='')$filtros["sql2"]=$_REQUEST["sql2"];

			$this->datos=$this->oNegProyecto_cursos->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function buscarmiscursosjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();
			$idempresa=!empty($_REQUEST["idempresa"])?$_REQUEST["idempresa"]:-2222;
			$hayproyecto=$this->oNegProyecto->buscar(array('idempresa'=>$idempresa));
			if(empty($hayproyecto[0])){
				$this->oNegProyecto->idempresa=@$_REQUEST["idempresa"];
				$this->oNegProyecto->jsonlogin='{}';
				$this->oNegProyecto->fecha=date('Y-m-d');
				$this->oNegProyecto->idioma=!empty($idioma)?$idioma:'EN';
				$idproyecto=$this->oNegProyecto->agregar();
			}else{
				$idproyecto=$hayproyecto[0]["idproyecto"];
			}
			$cursos=$this->oNegCursos->buscarXproyecto(array('estado'=>1,'idproyecto'=>$idproyecto));
			$cursos2=$this->oNegCursos->buscar(array('estado'=>1,'sql2'=>true));
			$mostrarcursos=array();
			$addcurso=array();
			if(!empty($cursos))
			foreach($cursos as $cur){
				$addcurso[]=$cur["idcurso"];
				$cur["asignado"]=true;
				$mostrarcursos[]=$cur;
			}

			if(!empty($cursos2))
			foreach($cursos2 as $cur){
				if(!in_array($cur["idcurso"],$addcurso)){
					$addcurso[]=$cur["idcurso"];
					$cur["asignado"]=false;
					$mostrarcursos[]=$cur;
				}
			}
			echo json_encode(array('code'=>'ok','data'=>$mostrarcursos,'idproyecto'=>$idproyecto));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarProyecto_cursos(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdproycurso)) {
				$this->oNegProyecto_cursos->idproycurso = $frm['pkIdproycurso'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegProyecto_cursos->idcurso=@$txtIdcurso;
			$this->oNegProyecto_cursos->idproyecto=@$txtIdproyecto;
            if($accion=='_add') {
            	$res=$this->oNegProyecto_cursos->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegProyecto_cursos->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function cambiarestado(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
			$this->oNegProyecto_cursos->idcurso=@$idcurso;
			$this->oNegProyecto_cursos->idproyecto=@$idproyecto;
			$estado=@$isasignado;
			$hayProyectocurso=$this->oNegProyecto_cursos->buscar(array('idcurso'=>$idcurso,'idproyecto'=>$idproyecto));
			if(!empty($hayProyectocurso[0])){
				if($estado==0){
					$this->oNegProyecto_cursos->__set('idproycurso', $hayProyectocurso[0]["idproycurso"]);
					$res=$this->oNegProyecto_cursos->eliminar();
					echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('saved successfully'),'newid'=>$res,'asignado'=>0)); 
				}
			}else{
				$this->oNegProyecto_cursos->idcurso=@$idcurso;
				$this->oNegProyecto_cursos->idproyecto=@$idproyecto;
				$res=$this->oNegProyecto_cursos->agregar();
				$estado=1;
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Proyecto_cursos')).' '.JrTexto::_('saved successfully'),'newid'=>$res,'asignado'=>1)); 
			}			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}





	
	// ========================== Funciones xajax ========================== //
	public function xSaveProyecto_cursos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdproycurso'])) {
					$this->oNegProyecto_cursos->idproycurso = $frm['pkIdproycurso'];
				}
				
				$this->oNegProyecto_cursos->idcurso=@$frm["txtIdcurso"];
					$this->oNegProyecto_cursos->idproyecto=@$frm["txtIdproyecto"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegProyecto_cursos->agregar();
					}else{
									    $res=$this->oNegProyecto_cursos->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegProyecto_cursos->idproycurso);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDProyecto_cursos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegProyecto_cursos->__set('idproycurso', $pk);
				$this->datos = $this->oNegProyecto_cursos->dataProyecto_cursos;
				$res=$this->oNegProyecto_cursos->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegProyecto_cursos->__set('idproycurso', $pk);
				$res=$this->oNegProyecto_cursos->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegProyecto_cursos->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}