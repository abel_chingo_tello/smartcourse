<?php
/**
 * @autor       Abel Chingo Tello, ACHT
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSesion', RUTA_SITIO, 'sys_negocio');
class WebIdioma extends JrWeb
{
    private $oNegSesion;
    public function __construct()
    {
        parent::__construct();      
        $this->oNegSesion = new NegSesion;
    }

    public function cambiar(){
        try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            global $aplicacion;
            @extract($_POST);
            if(empty($idioma)) echo json_encode(array('code'=>'error','idioma'=>'incorrecto'));
            $documento = &JrInstancia::getDocumento();
            $documento->setIdioma($idioma);
            NegSesion::set('idioma', $idioma, 'idioma__');
            echo json_encode(array('code'=>200,'idioma'=>$idioma));
            exit(0);

        } catch(Exception $e) {
            exit('error idioma'.$e);
        }          
    }
        
    public function xCambiaridioma(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
                global $aplicacion;
                $uri = JrURI::getInstance();
                if(!empty($args)){
                    
                    $documento = &JrInstancia::getDocumento();
                    $documento->setIdioma($args[0]);
                    NegSesion::set('idioma', $args[0], 'idioma__');
                   // $ira='<meta http-equiv="Refresh" content="0; url="./" />';
                    $oRespAjax->call('redir',$uri);
                }           
            } catch(Exception $e) {
               exit('error idioma'.$e);
            } 
        }
    }
}