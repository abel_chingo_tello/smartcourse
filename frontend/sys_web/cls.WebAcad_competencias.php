<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020 
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_competencias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
class WebAcad_competencias extends JrWeb
{
	private $oNegAcad_competencias;
	private $oNegAcad_categorias;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_competencias = new NegAcad_competencias;
		$this->oNegAcad_categorias = new NegAcad_categorias;				
	}

	public function defecto(){
		return $this->listado();
	}

	private function padres($dt,$idpadre){
		$dts=array();
		if(!empty($dt))
			foreach ($dt as $k => $v){
				if($v["idpadre"]==$idpadre){
					$v["hijos"]=$this->padres($dt,$v["idcategoria"]);
					$dts[]=$v;
				}
			}
		return $dts;
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_competencias', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			$usuarioAct = NegSesion::getUsuario();
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$this->categorias = $this->oNegAcad_categorias->buscar(array('idproyecto'=>$filtros["idproyecto"]));			
			//$this->datos=$this->oNegAcad_competencias->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Competencias'), true);
			$this->esquema = 'curriculo/acad_competencias-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_competencias', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Expertise').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_competencias', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_competencias->idcompetencia = @$_GET['id'];
			$this->datos = $this->oNegAcad_competencias->dataAcad_competencias;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Expertise').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_competencias-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}