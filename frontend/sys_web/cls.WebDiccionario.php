<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		13-02-2017 
 * @copyright	Copyright (C) 13-02-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegDiccionario', RUTA_BASE, 'sys_negocio');
class WebDiccionario extends JrWeb
{
	private $oNegDiccionario;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegDiccionario = new NegDiccionario;
	}

	public function defecto(){
		return $this->ver();
	}
/*
	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Diccionario', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegDiccionario->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Diccionario'), true);
			$this->esquema = 'diccionario-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Diccionario', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Diccionario').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Diccionario', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegDiccionario->id = @$_GET['id'];
			$this->datos = $this->oNegDiccionario->dataDiccionario;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Diccionario').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/

	public function ver(){
		try{
			$word = trim(strtoupper(@$_GET['palabra']));
			$this->datos = $this->oNegDiccionario->buscar(["palabra"=>$word]);
			$this->documento->plantilla = 'returnjson';
			$response = [
				"msje" => 'success',
				"data" => $this->datos,
			];
			return json_encode($response);
		}catch(Exception $e) {
			$response = [
				"msje" => JrTexto::_($e->getMessage()),
				"data" => null,
			];
			return json_encode($response);
		}
	}
/*
	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'diccionario-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/

	// ========================== Funciones xajax ========================== //
/*
	public function xSaveDiccionario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId'])) {
					$this->oNegDiccionario->id = $frm['pkId'];
				}
				
				$this->oNegDiccionario->__set('palabra',@$frm["txtPalabra"]);
					$this->oNegDiccionario->__set('definicion',@$frm["txtDefinicion"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegDiccionario->agregar();
					}else{
									    $res=$this->oNegDiccionario->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegDiccionario->id);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDDiccionario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegDiccionario->__set('id', $pk);
				$this->datos = $this->oNegDiccionario->dataDiccionario;
				$res=$this->oNegDiccionario->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegDiccionario->__set('id', $pk);
				$res=$this->oNegDiccionario->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
*/
	     
}