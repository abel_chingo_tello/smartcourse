<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020 
 * @copyright	Copyright (C) 22-04-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPortafolio', RUTA_BASE);
class WebPortafolio extends JrWeb
{
	private $oNegPortafolio;

	public function __construct()
	{
		parent::__construct();
		$this->oNegPortafolio = new NegPortafolio;
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('es', '/libs/select2/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			$this->documento->script('mine-render', '/libs/othersLibs/mine/js/');

			$this->documento->stylesheet('mine-portafolio', '/libs/othersLibs/portafolio/');
			$this->documento->script('mine-portafolio', '/libs/othersLibs/portafolio/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros = array();
			// if (isset($_REQUEST["idportafolio"]) && @$_REQUEST["idPortafolio"] != '') $filtros["idPortafolio"] = $_REQUEST["idPortafolio"];
			$this->datos = $this->oNegPortafolio->buscar($filtros);
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Portafolio'), true);
			$this->esquema = 'portafolio/main';
			$usuarioAct = NegSesion::getUsuario();
			// if ($usuarioAct['idrol'] == "1") {
			// 	$this->msj = "SU ROL NO PUEDE ACCEDER A ESTE MÓDULO";
			// 	$this->esquema = 'error/general';
			// }else{
				$this->esquema = 'portafolio/main';
			// }
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
