<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		20-04-2019 
 * @copyright	Copyright (C) 20-04-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE);
class WebUbigeo extends JrWeb
{
	private $oNegUbigeo;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegUbigeo = new NegUbigeo;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ubigeo', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["pais"])&&@$_REQUEST["pais"]!='')$filtros["pais"]=$_REQUEST["pais"];
			if(isset($_REQUEST["departamento"])&&@$_REQUEST["departamento"]!='')$filtros["departamento"]=$_REQUEST["departamento"];
			if(isset($_REQUEST["provincia"])&&@$_REQUEST["provincia"]!='')$filtros["provincia"]=$_REQUEST["provincia"];
			if(isset($_REQUEST["distrito"])&&@$_REQUEST["distrito"]!='')$filtros["distrito"]=$_REQUEST["distrito"];
			if(isset($_REQUEST["ciudad"])&&@$_REQUEST["ciudad"]!='')$filtros["ciudad"]=$_REQUEST["ciudad"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["solo"])&&@$_REQUEST["solo"]!='')$filtros["solo"]=$_REQUEST["solo"];
			
			$this->datos=$this->oNegUbigeo->buscar($filtros);
			$filtros['solo']='pais';
			$this->paises=$this->oNegUbigeo->buscar($filtros);
			$this->departamentos=array();
			if(!empty($this->paises[0])){
				$filtros['pais']=$this->paises[0]["pais"];
				$filtros['solo']='departamento';
				$this->departamentos=$this->oNegUbigeo->buscar($filtros);
			}

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Ubigeo'), true);
			$this->esquema = 'ubigeo-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ubigeo', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Ubigeo').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ubigeo', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegUbigeo->id_ubigeo = @$_GET['id'];
			$this->datos = $this->oNegUbigeo->dataUbigeo;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Ubigeo').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'ubigeo-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}