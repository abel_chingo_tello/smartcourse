<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2019 
 * @copyright	Copyright (C) 16-10-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursocategoria', RUTA_BASE);
class WebAcad_categorias extends JrWeb
{
	private $oNegAcad_categorias;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_categorias = new NegAcad_categorias;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function buscarjson()
	{
		try {
			global $aplicacion;			
			$categorias = $this->oNegAcad_categorias->buscar(array());
			echo json_encode(array('code'=>200,'data'=>$categorias));
		 	exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code'=>500,'data'=>$categorias));
		 	exit(0);
		}
	}
	
	public function cursos()
	{
		try {
			global $aplicacion;
			if(empty($_REQUEST["idcategoria"])) {
				throw new Exception(JrTexto::_("Categoria not found"));
			}
			$this->oNegAcad_categorias->idcategoria = $_REQUEST["idcategoria"];
			$this->categoria = $this->oNegAcad_categorias->dataAcad_categorias;
			$this->cursos = $this->oNegAcad_cursocategoria->buscarCursos(array("idcategoria"=>$this->categoria['idcategoria']));

			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');

			$this->esquema = 'alumno/cursos_diplomado';
			$this->documento->plantilla = 'alumno/diplomado';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_categorias', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(empty($filtros["idproyecto"])){
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idproyecto"]=$usuarioAct["idproyecto"];
			}		
			$this->datos=$this->oNegAcad_categorias->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			// var_dump($this->documento->plantilla);
			$this->documento->setTitulo(JrTexto::_('Acad_categorias'), true);
			$this->esquema = 'acad_categorias-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_categorias', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_categorias').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_categorias', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_categorias->idcategoria = @$_GET['id'];
			$this->datos = $this->oNegAcad_categorias->dataAcad_categorias;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_categorias').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_categorias-frm';
			$usuarioAct = NegSesion::getUsuario();
			$this->categorias=$this->oNegAcad_categorias->buscar(array('idproyecto'=>$usuarioAct["idproyecto"]));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}