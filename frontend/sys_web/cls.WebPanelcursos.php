<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020 
 * @copyright	Copyright (C) 27-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);

class WebPanelcursos extends JrWeb
{
	private $oNegAcad_categorias;
	private $oNegAcad_curso;		
	public function __construct()
	{
		parent::__construct();				
		$this->oNegAcad_curso = new NegAcad_curso;		
		$this->oNegAcad_categorias = new NegAcad_categorias;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_capacidades', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->stylesheet('abelchingo', '/libs/abelchingo/');
			$filtros=array();
			$usuarioAct = NegSesion::getUsuario();
			$filtros["idproyecto"]=$usuarioAct["idproyecto"];
			$this->categorias = $this->oNegAcad_categorias->buscar($filtros);
			$cursos = $this->oNegAcad_curso->cursos($filtros);
			if (!empty($this->categorias)) {
				$cate_ = array();
				foreach ($this->categorias as $k => $v) {
					if ($v["idpadre"] == 0) {
						$cate_[$v["idcategoria"]] = $v;
						if (empty($cate_[$v["idcategoria"]]["tienecursos"])) {
							$cate_[$v["idcategoria"]]["tienecursos"] = false;
						}
						$cate_[$v["idcategoria"]]["cursos"] = array();
						if (!empty($cursos["miscursos"]))
							foreach ($cursos["miscursos"] as $kc => $vc) {
								if (@$vc["idcategoria"] == @$v["idcategoria"]) {
									$cate_[$v["idcategoria"]]["cursos"][] = $vc;
									$cate_[$v["idcategoria"]]["tienecursos"] = true;
								}
							}
					}
				}
				foreach ($this->categorias as $k => $v) {
					if ($v["idpadre"] != 0) {
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]] = $v;
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["tienecursos"] = false;
						$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["cursos"] = array();
						//$grupos[$v["idpadre"]]['hijos'][$v["idcategoria"]]["grupos"] = array();						
						if (!empty($cursos["miscursos"]))
							foreach ($cursos["miscursos"] as $kc => $vc) {
								if (@$vc["idcategoria"] == @$v["idcategoria"]) {
									if ($v['idcategoria'] == 31) {
										$haytutorialcomputacion = true;
									} else if ($v['idcategoria'] == 32) {
										$haytutorialingles = true;
									} else if ($v['idpadre'] == 15) $haycomputacion = true;
									else if ($v['idpadre'] == 24) $hayingles = true;
									$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["cursos"][] = $vc;
									$cate_[$v["idpadre"]]["tienecursos"] = true;
									$cate_[$v["idpadre"]]['hijos'][$v["idcategoria"]]["tienecursos"] = true;
								}
							}
					}
				}
			}
			$this->cursos=$cursos["miscursos"];
				//var_dump($cursos);	
			//$this->datos=$this->oNegAcad_capacidades->buscar($filtros);
			//$this->fkidcurso=$this->oNegAcad_curso->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Listado de Cursos'), true);
			$this->esquema = 'cursos/listado';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}