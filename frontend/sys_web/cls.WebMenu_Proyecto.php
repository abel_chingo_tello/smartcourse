<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016 
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegMenu_proyecto', RUTA_BASE);
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRoles', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMenu', RUTA_BASE);
class WebMenu_proyecto extends JrWeb
{
	private $oNegMenu;
	//private $oNegProyecto;
	private $oNegRoles;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegMenu = new NegMenu;
		//$this->oNegProyecto = new NegProyecto;
		$this->oNegRoles = new NegRoles;

	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->plantilla = 'reportes';
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');		
            $this->documento->stylesheet('tablas', '/libs/abelchingo/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->stylesheet('select2.min', '/libs/select2/');
            $this->documento->script('select2.min', '/libs/select2/');

            $this->documento->stylesheet('jquery.nestable.min', '/libs/jquery-nestable/');
            $this->documento->script('jquery.nestable.min', '/libs/jquery-nestable/');

            $this->documento->script('abelchingo', '/libs/abelchingo/');
            $this->documento->script('bdmenu_proyecto', '/libs/abelchingo/');            
			$this->documento->setTitulo(JrTexto::_('Menu_proyecto'), true);
			$this->esquema = 'menu/menu-proyecto';
			
			$this->user=NegSesion::getUsuario();
			$this->idproyecto=$this->user["idrol"];
			//$this->Proyectos=$this->oNegProyecto->buscar();
			$this->fkIdmenu=$this->oNegMenu->buscar();
			//$this->fkIdproyecto=$this->oNegEmpresa->buscar();
			$this->fkIdrol=$this->oNegRoles->buscar();		
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}
}