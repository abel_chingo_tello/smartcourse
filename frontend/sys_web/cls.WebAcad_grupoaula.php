<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		14-10-2019 
 * @copyright	Copyright (C) 14-10-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_grado', RUTA_BASE);
JrCargador::clase('sys_negocio::NegMin_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE);
class WebAcad_grupoaula extends JrWeb
{
	private $oNegAcad_grupoaula;
	private $oNegGeneral;
	private $oNegGrado;
	private $oNegSeccion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegGeneral = new NegGeneral;
		$this->oNegGrado = new NegMin_grado;
		$this->oNegSeccion = new NegMin_sesion;
		$this->oNegAcad_categorias = new NegAcad_categorias;
		$this->oNegLocal = new NegLocal;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_grupoaula', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->script('datetimepicker.min', '/libs/datetimepicker/js/');
			$this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('bootstrap-select.min', '/libs/select/js/');
			$this->documento->stylesheet('bootstrap-select.min', '/libs/select/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["comentario"])&&@$_REQUEST["comentario"]!='')$filtros["comentario"]=$_REQUEST["comentario"];
			if(isset($_REQUEST["nvacantes"])&&@$_REQUEST["nvacantes"]!='')$filtros["nvacantes"]=$_REQUEST["nvacantes"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idgrado"])&&@$_REQUEST["idgrado"]!='')$filtros["idgrado"]=$_REQUEST["idgrado"];
			if(isset($_REQUEST["idsesion"])&&@$_REQUEST["idsesion"]!='')$filtros["idsesion"]=$_REQUEST["idsesion"];			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$usuarioAct = NegSesion::getUsuario();
			$filtros["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];
			$this->grados=$this->oNegGrado->buscar();
			$this->seccion=$this->oNegSeccion->buscar();
			$this->fktipos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			$this->categorias = $this->oNegAcad_categorias->buscar(array('idproyecto'=>$filtros["idproyecto"]));
			$this->local=$this->oNegLocal->buscar(array('idproyecto'=>$filtros["idproyecto"]));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Grupos Aula'), true);
			$this->esquema = 'academico/acad_grupoaula-list';
			if($usuarioAct["tuser"]=='n')
				$this->esquema = 'academico/acad_grupoaula';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_grupoaula', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Grupo aula').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_grupoaula', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_grupoaula->idgrupoaula = @$_GET['id'];
			$this->datos = $this->oNegAcad_grupoaula->dataAcad_grupoaula;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Grupo aula').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fktipos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			$this->grados=$this->oNegGrado->buscar();
			$this->seccion=$this->oNegSeccion->buscar();
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');	
			$this->esquema = 'acad_grupoaula-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}