<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2017 
 * @copyright	Copyright (C) 16-10-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPronunciacion', RUTA_BASE, 'sys_negocio');
class WebPronunciacion extends JrWeb
{
	private $oNegPronunciacion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPronunciacion = new NegPronunciacion;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')$filtros["id"]=$_REQUEST["id"];
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			if(isset($_REQUEST["pron"])&&@$_REQUEST["pron"]!='')$filtros["pron"]=$_REQUEST["pron"];
			
			$this->datos=$this->oNegPronunciacion->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Pronunciacion'), true);
			$this->esquema = 'pronunciacion-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;		
			
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Pronunciacion').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			$this->frmaccion='Editar';
			$this->oNegPronunciacion->id = @$_GET['id'];
			$this->datos = $this->oNegPronunciacion->dataPronunciacion;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Pronunciacion').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'pronunciacion-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$filtros=array();
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')$filtros["id"]=$_REQUEST["id"];
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			if(isset($_REQUEST["pron"])&&@$_REQUEST["pron"]!='')$filtros["pron"]=$_REQUEST["pron"];
			if(!empty($_REQUEST["palabra"])){				
				$palabras = explode(" ",trim($_REQUEST["palabra"]));
				if(count($palabras)-1==0&& $palabras[0]!=''){
					$this->datos=$this->oNegPronunciacion->buscar($filtros);
				}else if(count($palabras)>1){
					$frase=[];
					$pal=array();
					foreach ($palabras as $p){
						if(trim($p)!=''){
							if(!in_array($p,$pal)){
								$pal[]=$p;
								$filtros["palabra"]=$p;
								$dt=$this->oNegPronunciacion->buscar($filtros);								
								if(!empty($dt)){									
									$frase[$dt["palabra"]]=$dt;
								}else{
									$frase[$p]=str_replace(array("\r","\n","\r\n"),"",$p);
								}
							}
						}
					}
					$this->datos=$frase;
				}
			}else if(!empty($_REQUEST["pron"])){
				$pronun = explode("\\r\\n",$_REQUEST["pron"]);
				if(count($pronun)-1==0&& $pronun[0]!=''){
					$this->datos=$this->oNegPronunciacion->buscar($filtros);
				}else if(count($pronun)>1){
					$frase=[];					
					foreach ($pronun as $p){
						if(trim($p)!=''){
							$filtros["pron"]=$p;
							$dt=$this->oNegPronunciacion->buscar($filtros);
							if(!empty($dt[0])){
								$frase[$dt[0]["palabra"]]=$dt[0]["pron"];
							}else{
								$frase[$p]=$p;
							}
						}
					}
					$this->datos=$frase;
				}
			}else
				$this->datos=$this->oNegPronunciacion->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function buscarpronunciacionjson(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			$this->datos=$this->oNegPronunciacion->buscarpronunciacion($_REQUEST["palabra"]);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }

	}



	public function guardarPronunciacion(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkId)) {
				$this->oNegPronunciacion->id = $frm['pkId'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	
				$this->oNegPronunciacion->palabra=@$txtPalabra;
					$this->oNegPronunciacion->pron=@$txtPron;
					
            if($accion=='_add') {
            	$res=$this->oNegPronunciacion->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Pronunciacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPronunciacion->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Pronunciacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePronunciacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId'])) {
					$this->oNegPronunciacion->id = $frm['pkId'];
				}
				
				$this->oNegPronunciacion->palabra=@$frm["txtPalabra"];
					$this->oNegPronunciacion->pron=@$frm["txtPron"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegPronunciacion->agregar();
					}else{
									    $res=$this->oNegPronunciacion->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPronunciacion->id);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPronunciacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPronunciacion->__set('id', $pk);
				$this->datos = $this->oNegPronunciacion->dataPronunciacion;
				$res=$this->oNegPronunciacion->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPronunciacion->__set('id', $pk);
				$res=$this->oNegPronunciacion->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPronunciacion->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	
	public function agregarcampos()
	{
		$this->oNegPronunciacion->guardarpron("14","F AO R T IY N");
		$this->oNegPronunciacion->guardarpron("15","F IH F T IY N");
		$this->oNegPronunciacion->guardarpron("16","S IH K S T IY N");
		$this->oNegPronunciacion->guardarpron("17","S EH V AH N T IY N");
		$this->oNegPronunciacion->guardarpron("18","EY T IY N");
		$this->oNegPronunciacion->guardarpron("19","N AY N T IY N");
		$this->oNegPronunciacion->guardarpron("3","TH R IY");
		$this->oNegPronunciacion->guardarpron("0","Z IH R OW");
		$this->oNegPronunciacion->guardarpron("1","W AH N");
		$this->oNegPronunciacion->guardarpron("2","T UW");
		$this->oNegPronunciacion->guardarpron("4","F AO R");
		$this->oNegPronunciacion->guardarpron("5","F AY V");
		$this->oNegPronunciacion->guardarpron("6","S IH K S");
		$this->oNegPronunciacion->guardarpron("7","S EH V AH N");
		$this->oNegPronunciacion->guardarpron("8","EY T");
		$this->oNegPronunciacion->guardarpron("9","N AY N");
		$this->oNegPronunciacion->guardarpron("10","T EH N");
		$this->oNegPronunciacion->guardarpron("11","IH L EH V AH N");
		$this->oNegPronunciacion->guardarpron("12","T W EH L V");
		$this->oNegPronunciacion->guardarpron("13","TH ER T IY N");
		$this->oNegPronunciacion->guardarpron("308","TH R IY OW EY T");
		$this->oNegPronunciacion->guardarpron("18095760234","W AH N EY T Z IH R OW N AY N F AY V S EH V AH N S IH K S Z IH R OW T UW TH R IY F AO R");
		$this->oNegPronunciacion->guardarpron("25","T W EH N T IY F AY V");
		$this->oNegPronunciacion->guardarpron("830","EY T TH ER D IY");
		$this->oNegPronunciacion->guardarpron("915","N AY N F IH F T IY N");
		$this->oNegPronunciacion->guardarpron("1145","IH L EH V AH N F AO R T IY F AY V");
		$this->oNegPronunciacion->guardarpron("36","TH ER D IY S IH K S");
		$this->oNegPronunciacion->guardarpron("50","F IH F T IY");
		$this->oNegPronunciacion->guardarpron("three-course","TH R IY K AO R S");
		$this->oNegPronunciacion->guardarpron("threecourse","TH R IY K AO R S");
		$this->oNegPronunciacion->guardarpron("flipflops","F L IH P F L AA P S");
		$this->oNegPronunciacion->guardarpron("zookeeper","Z UW K IY P ER");
		$this->oNegPronunciacion->guardarpron("70","S EH V AH N T IY");
		$this->oNegPronunciacion->guardarpron("30","TH ER D IY");
		$this->oNegPronunciacion->guardarpron("2019","T UW TH AW Z AH N D AH N D N AY N T IY N");
		$this->oNegPronunciacion->guardarpron("2020","T UW TH AW Z AH N D AH N D T W EH N T IY");
		$this->oNegPronunciacion->guardarpron("2021","T UW TH AW Z AH N D AH N D T W EH N T IY W AO N");
		$this->oNegPronunciacion->guardarpron("openminded","OW P AH N M AY N D AH D");
		$this->oNegPronunciacion->guardarpron("strongwilled","S T R AO NG W IH L D");
		$this->oNegPronunciacion->guardarpron("levelheaded","L EH V AH L HH EH D AH D");
		$this->oNegPronunciacion->guardarpron("badtempered","B AE D T EH M P ER D");
		$this->oNegPronunciacion->guardarpron("absentminded","AE B S AH N T M AY N D AH D");
		$this->oNegPronunciacion->guardarpron("selfservice","S EH L F S ER V AH S");
		$this->oNegPronunciacion->guardarpron("unfurnished","AH N F ER N IH SH T");
		$this->oNegPronunciacion->guardarpron("walltowall","W AO L T UW W AO L");
		$this->oNegPronunciacion->guardarpron("21","T W EH N T IY W AO N");
		$this->oNegPronunciacion->guardarpron("15year","F IH F T IY N Y IH R");
		$this->oNegPronunciacion->guardarpron("oneday","W AH N D EY");
		$this->oNegPronunciacion->guardarpron("mailin","M EY L IH N");
		$this->oNegPronunciacion->guardarpron("24th","T W EH N T IY F AO R TH");
		$this->oNegPronunciacion->guardarpron("45","F AO R T IY F AY V Z");
		$this->oNegPronunciacion->guardarpron("2015","T UW TH AW Z AH N D AH N D F IH F T IY N");
		$this->oNegPronunciacion->guardarpron("crossreference","K R AO S R EH F ER AH N S");
		$this->oNegPronunciacion->guardarpron("2014","T UW TH AW Z AH N D AH N D F AO R T IY N");
		$this->oNegPronunciacion->guardarpron("decrypt","D IY K R IH P T");
		$this->oNegPronunciacion->guardarpron("brainwaves","B R EY N W EY V Z");
		$this->oNegPronunciacion->guardarpron("spatially","S P AE SH AH L IY");
		$this->oNegPronunciacion->guardarpron("lowcost","L OW K AO S T");
		$this->oNegPronunciacion->guardarpron("budgetfriendly","B AH JH IH T F R EH N D L IY");
		$this->oNegPronunciacion->guardarpron("crowdsourcing","K R AW D S AO R S IH NG");
		$this->oNegPronunciacion->guardarpron("hypnotizing","HH IH P N AH T AY Z IH NG");
		$this->oNegPronunciacion->guardarpron("insufficiencies","IH N S AH F IH SH AH N S IY Z");
		$this->oNegPronunciacion->guardarpron("blendable","B L EH N D AH B AH L");
		$this->oNegPronunciacion->guardarpron("paragliding","P EH R AH G L AY D IH NG");
		$this->oNegPronunciacion->guardarpron("multiday","M AH L T IY D EY");
		$this->oNegPronunciacion->guardarpron("oneofakind","W AH N AH V AH K AY N D");
		$this->oNegPronunciacion->guardarpron("halffull","HH AE F F UH L");
		$this->oNegPronunciacion->guardarpron("halfempty","HH AE F EH M P T IY");
		$this->oNegPronunciacion->guardarpron("positivity","P AA Z AH T IH V IH T IY");
		$this->oNegPronunciacion->guardarpron("similes","S IY M IY L IY Z");
		$this->oNegPronunciacion->guardarpron("35355149","TH R IY F AY V TH R IY F AY V F AY V W AH N F AO R N AY N");
		$this->oNegPronunciacion->guardarpron("16TH","S IH K S T IY N TH");
		$this->oNegPronunciacion->guardarpron("3859147","TH R IY EY T F AY V N AY N W AH N F AO R S EH V AH N");
		$this->oNegPronunciacion->guardarpron("elevational","EH L AH V EY SH AH N AH L");
	}     
}