<?php
defined('RUTA_BASE') or die();
class WebReporteMd extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->script('es', '/libs/select2/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/');
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('soft-rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine2', '/libs/othersLibs/mine/js/');
			$this->documento->script('mine-render', '/libs/othersLibs/mine/js/');
			$this->documento->script('mine-modal', '/libs/othersLibs/mine/js/');
			$this->documento->script('mine-selects', '/libs/othersLibs/mine/js/');
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->stylesheet('slick', '/libs/slick-1.8.1/');
			$this->documento->script('slick.min', '/libs/slick-1.8.1/');

			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros = array();
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Report'), true);
			$usuarioAct = NegSesion::getUsuario();
			// if ($usuarioAct['idrol'] == "1") {
			// 	$this->msj = "SU ROL NO PUEDE ACCEDER A ESTE MÓDULO";
			// 	$this->esquema = 'error/general';
			// }else{
				$this->esquema = 'reporteModular/main';
			// }
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
