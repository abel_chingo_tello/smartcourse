<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017 
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_subcategoria', RUTA_BASE, 'sys_negocio');
class WebBib_subcategoria extends JrWeb
{
	private $oNegBib_subcategoria;
	private $oNegBib_categoria;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_subcategoria = new NegBib_subcategoria;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_subcategoria', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegBib_subcategoria->buscar();
		//	$this->datos=$this->oNegBib_categoria->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_subcategoria'), true);
			$this->esquema = 'bib_subcategoria-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_subcategoria', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			//$this->categoria=$this->oNegBib_categoria->buscar();
			$this->documento->setTitulo(JrTexto::_('Bib_subcategoria').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Bib_subcategoria', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegBib_subcategoria->id_subcategoria = @$_GET['id'];
			$this->datos = $this->oNegBib_subcategoria->dataBib_subcategoria;
			$this->categoria=$this->oNegBib_categoria->buscar();
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_subcategoria').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_subcategoria->id_subcategoria = @$_GET['id'];
			$this->datos = $this->oNegBib_subcategoria->dataBib_subcategoria;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_subcategoria').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_subcategoria-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_subcategoria-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveBib_subcategoria(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_subcategoria'])) {
					$this->oNegBib_subcategoria->id_subcategoria = $frm['pkId_subcategoria'];
				}
				
					$this->oNegBib_subcategoria->__set('id_categoria',@$frm["txtId_categoria"]);
					$this->oNegBib_subcategoria->__set('descripcion',@$frm["txtDescripcion"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegBib_subcategoria->agregar();
					}else{
									    $res=$this->oNegBib_subcategoria->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBib_subcategoria->id_subcategoria);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBib_subcategoria(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_subcategoria->__set('id_subcategoria', $pk);
				$this->datos = $this->oNegBib_subcategoria->dataBib_subcategoria;
				$res=$this->oNegBib_subcategoria->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_subcategoria->__set('id_subcategoria', $pk);
				$res=$this->oNegBib_subcategoria->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSubcategoria(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$this->oNegBib_subcategoria->setLimite(0,99999);
			$this->datos =$this->oNegBib_subcategoria->listar();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function xRegistrarSubCategoria(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);
			#JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
            $this->oNegBib_subcategoria->__set('descripcion',@$request['descripcion']);
            $this->oNegBib_subcategoria->__set('id_categoria',@$request['id_categoria']);
			$res=$this->oNegBib_subcategoria->agregar();
			echo json_encode(array('code'=>'ok','data'=>$res));
		}catch(Exception $e){
			$data=array('code'=>'Error xRegistrarCategoria','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}        
}