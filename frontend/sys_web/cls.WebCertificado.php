<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-10-2018 
 * @copyright	Copyright (C) 27-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_grado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_sesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
class WebCertificado extends JrWeb
{
	private $oNegAcad_matricula;
	private $oNegDre;
	private $oNegUgel;
	private $oNegIiee;
	private $oNegGrado;
	private $oNegSeccion;
	private $oNegCursos;
	private $oNegGrupoauladetalle;
	private $oNegGrupoaula;
	private $oNegPersonal;
	private $oNegPersonalrol;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegDre = new NegMin_dre;
		$this->oNegUgel = new NegUgel;
		$this->oNegIiee = new NegLocal;
		$this->oNegGrado = new NegMin_grado;
		$this->oNegSeccion = new NegMin_sesion;
		$this->oNegCurso = new NegAcad_curso;
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegGrupoauladetalle= New NegAcad_grupoauladetalle;
		$this->oNegGrupoaula= New NegAcad_grupoaula;
		$this->oNegPersonal= New NegPersonal;
		$this->oNegPersonalrol= New NegPersona_rol;
	}

	public function defecto(){
		return $this->filtros();
	}

	public function filtros(){
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			
			$this->fkcursos=$this->oNegCurso->buscarxproyecto(array('idproyecto'=>$this->usuarioAct["idproyecto"],'orderby'=>'nombre','estado'=>1));
			$this->dress=$this->oNegDre->buscar();
			$this->grados=$this->oNegGrado->buscar();
			$this->seccion=$this->oNegSeccion->buscar($filtros);
			$this->datos=$this->oNegAcad_matricula->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Certificate'), true);
			$this->esquema = 'academico/acad_certificado';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function generar_certificado(){
		
		try {
			global $aplicacion;	
			$this->id = $_REQUEST["id"];
			$this->idrol = $_REQUEST["idrol"];
			$filtros=array();
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')$filtros["idpersona"]=$this->id;
			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$this->idrol;
			$this->persona=$this->oNegPersonal->buscar_paracertificado($filtros);
			// var_dump($this->persona);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Certificate'), true);
			$this->esquema = 'academico/mostrar_certificado';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
   
}