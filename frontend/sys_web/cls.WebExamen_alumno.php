<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
class WebExamen_alumno extends JrWeb
{
	private $oNegExamen_alumno;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamen_alumno = new NegExamen_alumno;
	}

	public function defecto(){
		return $this->listado();
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegExamen_alumno->idexaalumno = @$_GET['id'];
			$this->datos = $this->oNegExamen_alumno->dataExamen_alumno;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Examen_alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'examen_alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function verjson()
    {
        try{
            global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();            
            $this->documento->plantilla = 'returnjson';
            $filtros=array();
            //$filtros["titulo"]=!empty($_POST["txtBuscar"])?$_POST["txtBuscar"]:'';
            $filtros["idexamen"]=!empty($_POST["idexamen"])?$_POST["idexamen"]:0;
            $filtros["idalumno"]=!empty($_POST["idalumno"])?$_POST["idalumno"]:0;
            $examenAlumno=$this->oNegExamen_alumno->buscar($filtros);
            $data=array('code'=>'ok','data'=>$examenAlumno);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }


    public function guardar()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario(); 			
			$intento=1;	
			if(!empty($idexamen)&&!empty($idexaalumno)){			
				$res=$this->oNegExamen_alumno->idexaalumno = $idexaalumno;
				$accion="editar";
			}
			$preguntas=@trim(str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$preguntas));
			$resultado=@trim(str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$resultado));
			$this->oNegExamen_alumno->__set('idexamen',@$idexamen);
			$this->oNegExamen_alumno->__set('idalumno',@$usuarioAct["dni"]);
			$this->oNegExamen_alumno->__set('preguntas',@$preguntas); //html
			$this->oNegExamen_alumno->__set('resultado',@$resultado); //html
			$this->oNegExamen_alumno->__set('puntajehabilidad',@$puntajehabilidad);
			$this->oNegExamen_alumno->__set('puntaje',@$puntaje);
			$this->oNegExamen_alumno->__set('resultadojson',@$resultadojson);
			$this->oNegExamen_alumno->__set('tiempoduracion',@$tiempoduracion);
			$this->oNegExamen_alumno->__set('intento',@$intento);

		    if(@$accion=="editar"){
				$res=$this->oNegExamen_alumno->editar();
			}else{
				$res=$this->oNegExamen_alumno->agregar();
		    }
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Assessment')).' '.JrTexto::_('saved successfully'),'new'=>$res)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 	
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveExamen_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdexaalumno'])) {
					$this->oNegExamen_alumno->idexaalumno = $frm['pkIdexaalumno'];
				}
				
				    $this->oNegExamen_alumno->__set('idexamen',@$frm["txtIdexamen"]);
					$this->oNegExamen_alumno->__set('idalumno',@$frm["txtIdalumno"]);
					$this->oNegExamen_alumno->__set('preguntas',@$frm["txtPreguntas"]);
					$this->oNegExamen_alumno->__set('resultado',@$frm["txtResultado"]);
					$this->oNegExamen_alumno->__set('puntajehabilidad',@$frm["txtPuntajehabilidad"]);
					$this->oNegExamen_alumno->__set('puntaje',@$frm["txtPuntaje"]);
					$this->oNegExamen_alumno->__set('resultadojson',@$frm["txtResultadojson"]);
					$this->oNegExamen_alumno->__set('tiempoduracion',@$frm["txtTiempoduracion"]);
					$this->oNegExamen_alumno->__set('fecha',@$frm["txtFecha"]);
					$this->oNegExamen_alumno->__set('intento',@$frm["txtIntento"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegExamen_alumno->agregar();
					}else{
									    $res=$this->oNegExamen_alumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegExamen_alumno->idexaalumno);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	  
}