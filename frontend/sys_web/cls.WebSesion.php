<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSesion', RUTA_RAIZ);
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_RAIZ);
JrCargador::clase('sys_negocio::NegSence_usuario', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegSence_cursovalor', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol',RUTA_BASE, 'sys_negocio');

//JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
class WebSesion extends JrWeb
{
	private $oNegSesion;
	protected $oNegConfig;
	protected $oNegSence_usuario;
	protected $oNegSence_cursovalor;
	protected $oNegPersona_rol;
	//public $oNegHistorialSesion;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSesion = new NegSesion;
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegSence_usuario = new NegSence_usuario;
		$this->oNegSence_cursovalor = new NegSence_cursovalor;
		$this->oNegPersona_rol = new NegPersona_rol;
	}
	public function defecto()
	{
		return $this->login();
	}
	public function login()
	{		
		try {			
			global $aplicacion;
			if(true === NegSesion::existeSesion()){
				return $aplicacion->redir();
			}elseif(!empty($_REQUEST['skiplogin']) && !empty($_REQUEST['idproyecto'])){
				$route_self=str_replace('index.php','',$_SERVER['PHP_SELF']);
				$redirected_url = _http_ . $_SERVER['HTTP_HOST'] . $route_self;
				$datos = array();
				$quitparams = array("_clave","_user","_dir","skiplogin");

				$datos['idproyecto'] = $_REQUEST['idproyecto'];
				if(!empty($_REQUEST['_user']) && !empty($_REQUEST['_clave'])){
					$datos['usuario'] = $_REQUEST['_user'];
					$datos['clave'] = $_REQUEST['_clave'];
				}
				if(!empty($_REQUEST['idempresa'])){
					$datos['idempresa'] = $_REQUEST['idempresa'];
				}
				$res = $this->oNegSesion->ingresoForzado($datos);

				if($res['code']==200){
					NegSesion::set('localstorage_flag', true, '__admin_m3c');
					if(!empty($_REQUEST['_dir'])){
						$redirected_url .= $_REQUEST['_dir'];
					}
					$_TMP_GET = !empty($_GET) ? $_GET : array();
					$cond = array();
					if(!empty($_TMP_GET)){
						$_TMP_GET = array_flip($_TMP_GET);
						foreach ($quitparams as $val) {
							$find = array_search($val, $_TMP_GET);
							if($find !== false){
								unset($_TMP_GET[$find]);
							}
						}
						if(!empty($_TMP_GET)){
							$_TMP_GET = array_flip($_TMP_GET);
							foreach ($_TMP_GET as $key => $value) {
								$cond[] = "{$key}={$value}";
							}
							$redirected_url = $redirected_url."?".implode('&', $cond);
						}//endif
					}//endif empty
					header('Location: '. $redirected_url );
					exit();
				}
			}elseif(!empty($_REQUEST['t'])){
				$res=$this->oNegSesion->ingresar(array('token'=>$_REQUEST['t'],'idproyecto'=>$_REQUEST['P'],'idempresa'=>$_REQUEST["E"]));
				if($res['code']==200) {	
					return $aplicacion->redir();				
					//return $aplicacion->redir(JrAplicacion::getJrUrl(array('perfil', 'cambiar-clave')), false);
				}
				$this->msjErrorLogin=$res['msj'];				
			}elseif(!empty($_REQUEST['token'])){
				$res=$this->oNegSesion->ingresar(array('token'=>$_REQUEST['token']));
			   	if($res['code']==200) return $aplicacion->redir();
				$this->msjErrorLogin=$res['msj'];	
			}elseif(!empty($_REQUEST['usuario'])&&!empty($_REQUEST['clave'])){
				$res=$this->oNegSesion->ingresar(array('usuario'=>$_REQUEST['usuario'],'clave'=>$_REQUEST['clave']));
				if($res['code']==200){
					NegSesion::set('localstorage_flag', true, '__admin_m3c');
					return $aplicacion->redir();	
				} 
				$this->msjErrorLogin=$res['msj'];	
			}
			return $this->iniciar(true);
		} catch(Exception $e) {			
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			return $this->form(@$usuario);
		}
	}
	public function iniciar($entro=false){
		try {		
			global $aplicacion;
			if(false === NegSesion::existeSesion() && $entro==false){ 
				return $this->login();
			}
			//$aplicacion->typereturn();						
			return $this->form(@$usuario);
		} catch(Exception $e) {			
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			return $this->form(@$usuario);
		}
	}

	public function sence(){
		global $aplicacion;

		if(empty($_REQUEST['i']) || empty($_REQUEST['t'])){ throw new Exception("variable: i, o la variable: t, no estan definidas"); }
		
		$_id = $_REQUEST['i'];
		$_type = $_REQUEST['t'];
		$rs = $this->oNegSence_usuario->buscar(['id'=>$_id]);
		
		if(empty($rs)){ throw new Exception("Registro no encontrado"); }
		
		$sence_usuario = end($rs);
		
		if(false === NegSesion::existeSesion()){
			JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE);			
			$sesion=New NegSesion;
			$dt=$sesion->ingresar(
				array(
					'_user_' => $sence_usuario['codusuario'],
					'idempresa' => $sence_usuario['idempresa'],
					'idproyecto' => $sence_usuario['idproyecto']
				)
			);
			if($dt["code"] != 200){			
				echo json_encode(array('code'=>201,'msj'=>ucfirst(JrTexto::_('Error autologue'))));
				exit();
			}
			//setear rol alumno si este lo tiene, fixea el bug de varios roles para que mantenga el rol actual
			$roles = $this->oNegPersona_rol->buscar(['idpersonal' => $sence_usuario['idpersona'], 'idproyecto' => $sence_usuario['idproyecto'], 'idempresa' => $sence_usuario['idempresa']]);
			if(!empty($roles)){
				$i = array_search('3',array_column($roles,'idrol'));
				if($i !== false){
					$this->oNegSesion->cambiar_rol(3,'Alumno',$sence_usuario['idpersona'],$sence_usuario['idproyecto'],$sence_usuario['idempresa']);
				}
			}//endif roles
		}
		//capturar errores
		if(!empty($_POST['GlosaError']) ){
			switch((int)$_POST['GlosaError']){
				case 210: {
					//Expiró el tiempo
					$_url = ((int)$_type == 1) ? $sence_usuario['url'] : $sence_usuario['url_cierre']."?msj=1&id=".$_id;
					header("Location: " . _HOST_ . $_url);
					exit(0);
				}
				break;
			}
		}
		$_sence = @NegSesion::get("sence");
		$sence = null;
		$_URL = $sence_usuario['url'];
		$POSTDATA = array_merge($_POST,['idsence_usuario'=>$_id]);
		$rowDB = ['id' => $_id,'tipo'=> $_type];
		
		switch ((int)$_type) {
			case 1: //iniciar sesion
				if(empty($_sence)){
					$sence = [$sence_usuario['codigocurso']=>$POSTDATA];
					$this->sence = $POSTDATA;
				} else {
					$sence = $_sence;
					$tmp_snc = @$_sence[$sence_usuario['codigocurso']];
					if(empty($tmp_snc) || isset($tmp_snc["GlosaError"])){
						$sence[$sence_usuario['codigocurso']] = $POSTDATA;
						$this->sence = $POSTDATA;
					}
				}

				$rowDB['fecha_iniciosesion'] = date('Y-m-d H:i:s');
				$rowInSence_cursovalor = [
					'idusuariosence'=> $_id
					,'CodSence' => $_POST['CodSence']
					,'CodigoCurso' => $_POST['CodigoCurso']
					,'IdSesionAlumno' => $_POST['IdSesionAlumno']
					,'IdSesionSence' => $_POST['IdSesionSence']
					,'RunAlumno' => $_POST['RunAlumno']
					,'FechaHora' => $_POST['FechaHora']
					,'ZonaHoraria' => $_POST['ZonaHoraria']
					,'LineaCapacitacion' => $_POST['LineaCapacitacion']
					,'fecha_creacion' => date('Y-m-d H:i:s')
					,'fecha_actualizacion' => date('Y-m-d H:i:s')
				];

				if(!empty($_POST['GlosaError'])){ $rowInSence_cursovalor['GlosaError'] = $_POST['GlosaError']; }
				
				$this->oNegSence_cursovalor->insert([$rowInSence_cursovalor]);
				break;
			case 2: //cerrar sesion
				$sence = $_sence;
				$_URL = $sence_usuario['url_cierre'];
				if(!empty(@$_sence[$_POST["CodigoCurso"]])){
					unset($sence[$_POST["CodigoCurso"]]);
				}
				
				$rowDB['fecha_cierresesion'] = date('Y-m-d H:i:s');
				break;
		}

		$this->oNegSence_usuario->update([$rowDB]);
		NegSesion::set("sence", (!empty($sence) ? $sence : null));
		
		header("Location: " . _HOST_ . $_URL);
		exit();
	}
	public function noroles(){
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Change empty'), true);
			$this->documento->plantilla = 'excepcion';
			$this->msj=JrTexto::_('Rol empty').'!!';
			$this->esquema = 'error/general';
			if(true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}
			return parent::getEsquema();
		} catch(Exception $e){	
			$aplicacion->redir();
		}		
	}
	public function cambiarrol(){
		try {
			global $aplicacion;			
			$this->documento->setTitulo(JrTexto::_('Change Rol'), true);
			$plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->plantilla = $plantilla;//!empty($plantilla) ? $plantilla : 'excepcion';		
			$this->esquema = 'academico/roles-cambiar';
			$usuarioAct = NegSesion::getUsuario();
			$this->roles=$this->oNegSesion->verroles($usuarioAct["idpersona"],$usuarioAct["idproyecto"],$usuarioAct["idempresa"]);
			if($usuarioAct["tuser"]=='s'){
				$hayadmin=false;
				foreach ($this->roles as $k => $v) {if($v["idrol"]==1) $hayadmin=true;}
				if($hayadmin==false)
					$this->roles[]=array('rol'=>'Administrador','idrol'=>1,'idproyecto'=>$usuarioAct["idproyecto"],'idempresa'=>$usuarioAct["idempresa"]);
				$hayadmin=false;
				foreach ($this->roles as $k => $v) {if($v["idrol"]==2) $hayadmin=true;}
				if($hayadmin==false)
					$this->roles[]=array('rol'=>'Docente','idrol'=>2,'idproyecto'=>$usuarioAct["idproyecto"],'idempresa'=>$usuarioAct["idempresa"]);
				$hayadmin=false;
				foreach ($this->roles as $k => $v) {if($v["idrol"]==3) $hayadmin=true;}
				if($hayadmin==false)
					$this->roles[]=array('rol'=>'Alumno','idrol'=>3,'idproyecto'=>$usuarioAct["idproyecto"],'idempresa'=>$usuarioAct["idempresa"]);
				//var_dump($roles);
			}

			$this->noroles=$this->oNegSesion->misrolesallempresas($usuarioAct["idpersona"],$usuarioAct["idproyecto"],$usuarioAct["idempresa"]);
			return parent::getEsquema();
		} catch(Exception $e){	
			$aplicacion->redir();
		}
	}
	protected function form($usuario = null)
	{
		try {
			global $aplicacion;						
			if(true == NegSesion::existeSesion()) {
				$aplicacion->redir();
			}
			JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
			$oNegProyecto = new NegProyecto;
			$idproyecto=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:(!empty($_COOKIE["SC_idproyecto"])?$_COOKIE["SC_idproyecto"]:7);
			@setcookie("sesionidproyecto",$idproyecto, time()+60*60*24*180,URL_BASE);
			$proyecto=$oNegProyecto->buscar(array('idproyecto'=>$idproyecto));
			if(empty($proyecto[0])){
				exit("La Empresa que esta intentando ingresar no existe, Valide su url");
			}
			$this->proyecto=$proyecto[0];
			@setcookie("userproyecto",json_encode($this->proyecto), time()+60*60*24*180,URL_BASE."; SameSite=None");
			$_COOKIE['sesionidproyecto']=$this->proyecto["idproyecto"];
			$_COOKIE['userproyecto']=json_encode($this->proyecto);
			@setcookie("sesionidempresa",$this->proyecto["idempresa"],time()+60*60*24*180,URL_BASE."; SameSite=None");
			$_COOKIE['sesionidempresa']=$this->proyecto["idempresa"];
			@setcookie("sesionnomempresa",$this->proyecto["nombre"], time()+60*60*24*180,URL_BASE."; SameSite=None");
			$_COOKIE['sesionnomempresa']=$this->proyecto["nombre"];
			$titulo=!empty($this->proyecto["nombre"])?($this->proyecto["nombre"])." - ".JrTexto::_('Start of section'):'login';
            $this->documento->setTitulo($titulo,false);
			$this->usuario = $usuario;			
			$this->documento->plantilla = 'login';
			$this->esquema = 'paris/login';
			if (JrTools::ismobile()) {
				$this->documento->plantilla = 'mobile/login';
				$this->esquema = 'mobile/login';
				// echo "is mobile success";
			}
			
			// $this->esquema = 'login/1'; 
			return parent::getEsquema();
		} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}
	}	
	public function salir()
	{		
		global $aplicacion;
		try {			
			if(true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}			
			$aplicacion->redir();
		} catch(Exception $e){
			if(true === NegSesion::existeSesion()) {
				$this->oNegSesion->salir();
			}
			$aplicacion->redir();
		}
	}	
	public function cambiar_ambito()
	{
		try{
			global $aplicacion;			
			$usuarioAct = NegSesion::getUsuario();	
			//exit($usuarioAct);
			if(!empty($_GET['rol'])&&!empty($_GET['idrol'])) {
				$oNegSesion = new NegSesion;
				$cambio=$oNegSesion->cambiar_rol($_GET['idrol'],$_GET['rol'],$usuarioAct["idpersona"],$usuarioAct["idproyecto"],$usuarioAct["idempresa"]);
				$usuarioAct = NegSesion::getUsuario();
			}
			$aplicacion->redir();
		} catch(Exception $e){			
			$aplicacion->redir();
		}
	}

	protected function iniciarHistorialSesion($lugar)
	{
		
		$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->tipousuario = ($usuarioAct['rol']=='Alumno')?'A':'P';
		$this->oNegHistorialSesion->idusuario = $usuarioAct['idpersona'];
		$this->oNegHistorialSesion->lugar = $lugar;
		$this->oNegHistorialSesion->fechaentrada = date('Y-m-d H:i:s');
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$idHistSesion = $this->oNegHistorialSesion->agregar();

		$sesion = JrSession::getInstancia();
		$sesion->set('idHistorialSesion', $idHistSesion, '__admin_m3c');
		$sesion->set('fechaentrada',date('Y-m-d H:i:s') , '__admin_m3c');
	}
	protected function terminarHistorialSesion($lugar)
	{
		$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->idhistorialsesion = $usuarioAct['idHistorialSesion'];
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$resp = $this->oNegHistorialSesion->editar();
	}

	public function cambiarclave(){ // genera un acceso directo por token
		try {	
			global $aplicacion;
			JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE);
			$oNegPersonal = new NegPersonal;
			$usuario = $oNegPersonal->procesarSolicitudCambioClave($_REQUEST['usuario'],$_REQUEST["idproyecto"]);
			if($usuario["code"]==200){
				$this->admin=$usuario["usuario"];
				$this->admin["nombre_full"]=$this->admin["ape_paterno"]." ".$this->admin["ape_materno"].", ".$this->admin["nombre"];
				JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
				$oNegEmpresa= new NegProyecto;

				JrCargador::clase('jrAdwen::JrCorreo');
				$oCorreo = new JrCorreo;
				$asunto=JrTexto::_('Acceso Directo por unica vez');			
				$empresa=$oNegEmpresa->buscar(array('idempresa'=>$_REQUEST["idempresa"],'idproyecto'=>$_REQUEST["idproyecto"]));
				if(empty($empresa[0])){
					echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Proyecto / empresa no encontrado')));
				}
				$this->empresa=$empresa[0];			
				$deemail=!empty($this->empresa["correoempresa"])?$this->empresa["correoempresa"]:$this->admin["email"];
				$denombre=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:$this->admin["nombre_full"];
				$oCorreo->setRemitente($deemail,$denombre);

				$this->titulo=JrTexto::_('Acceso directo por unica vez');
				$this->Asunto=$this->titulo;
				$oCorreo->setAsunto($this->Asunto);				
				$oCorreo->addDestinarioPhpmailer($this->admin["email"],$this->admin["nombre_full"]);
				$filelogo=!empty($this->empresa["logo"])?$this->empresa["logo"]:$this->empresa["logoempresa"];
				if(!empty($filelogo)){
					$ipost=stripos($filelogo,'?');
					if($ipost===false) $filelogo=$filelogo;
					else $filelogo=substr($filelogo,0,$ipost);
				}

				if(!empty($filelogo) && is_file(RUTA_BASE.$filelogo)){
					$ipost=stripos($filelogo,'?');
					if($ipost===false) $this->logoempresa=$filelogo;
					else $this->logoempresa=substr($filelogo,0,$ipost);
					$img=str_replace('\\', '/', RUTA_BASE.$this->logoempresa);
					$oCorreo->addadjuntos(RUTA_BASE.$this->logoempresa,'logoempresa');
				}			
				$this->para='';
				$this->esquema = 'correos/syscorreo-recuperar-clave';
				$oCorreo->setMensaje(parent::getEsquema());
				try{
					$envio=$oCorreo->sendPhpmailer();
					echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Revisa tu correo,"<b>'.(str_replace('/[aeiou]/i','*',$this->admin["email"])).'</b>", te hemos enviado un enlace por el cual pordras iniciar sesion'))));
				}catch(Exception $ex){
					echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('No hemos podido enviar el correo'))));
				}
			}else{
				echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('Usuario no existe'))));
			}
			exit();
		} catch(Exception $e) {			
			echo json_encode(array('code'=>'Error','msj'=>$e->message()));
			exit();
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSolicitarCambioClave(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0]['usuario'])) { return;}
				JrCargador::clase('sys_negocio::NegUsuario', RUTA_BASE, 'sys_negocio');
				$oNegUsuario = new NegUsuario;			
				$this->admin = $oNegUsuario->procesarSolicitudCambioClave($args[0]['usuario']);
				if(!empty($this->admin)) {
					try {
						$isonline=NegTools::isonline();
						if(!empty($isonline)){						
							global $configSitio;
							JrCargador::clase('jrAdwen::JrCorreo');
							$oCorreo = new JrCorreo;
							$oCorreo->setRemitente('desarrollo@abacoeducacion.org', JrTexto::_('System management'));
							$oCorreo->setAsunto(JrTexto::_('Change password'));						
							$this->esquema = 'syscorreo-recuperar-clave';
							$oCorreo->setMensaje(parent::getEsquema());
							$oCorreo->addDestinarioPhpmailer($this->admin['email'], $this->admin['nombre']);						
							$oCorreo->sendPhpmailer();
							$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention'))
								, $this->pasarHtml(JrTexto::_('If the email is correct you will receive a message with a link to change your password'))
								, 'success');
						}else{
							$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention'))
								, $this->pasarHtml(JrTexto::_('This option is only active in online mode security'))
								, 'info');
						}
					} catch(Exception $e) {}
				}				
				
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'error');
			} 
		}
	}

	public function xCambiarRol(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try{
				global $aplicacion;
				$usu = NegSesion::getUsuario();
				if($usu["rol"]==$usu["rol_original"]){
					$x=NegSesion::set('rol','Alumno');
				}else{
					$y=NegSesion::set('rol',$usu["rol_original"]);
				}
				$oRespAjax->call('redir', $this->documento->getUrlBase());
			}catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'error');
			} 
		}
	}
}