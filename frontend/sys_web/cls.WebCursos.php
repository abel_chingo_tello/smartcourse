<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
class WebCursos extends JrWeb
{
	private $oNegCursos;
	public function __construct()
	{
		parent::__construct();
		$this->oNegCursos = new NegAcad_curso;
		$this->oNegCursoDetalle = new NegAcad_cursodetalle;
	}

	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			return $aplicacion->redir('proyecto/cursos/asignar');
			//if(!NegSesion::tiene_acceso('Ugel', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros = array();
			$usuarioAct = NegSesion::getUsuario();
			$this->idrol = $usuarioAct["idrol"];
			//$this->datos=$this->oNegUgel->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Courses'), true);
			$this->esquema = 'cursos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function formular()
	{
		try {
			global $aplicacion;
			$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : 0;
			$icc = !empty($_REQUEST["icc"]) ? $_REQUEST["icc"] : 0;
			$tipo = !empty($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : 1;
			$idcategoria = !empty($_REQUEST["idcategoria"]) ? $_REQUEST["idcategoria"] : '';
			$this->curso = $this->oNegCursoDetalle->temasacalificar($idcurso, $icc, $tipo, $idcategoria);

			$this->documento->setTitulo(JrTexto::_('Calificación'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			//$this->esquema = 'curso-formula2';
			//mine

			$this->documento->stylesheet('select2.min', '/libs/select2/');
			$this->documento->script('select2.min', '/libs/select2/');
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			$this->documento->stylesheet('mine-interfaz_formula', '/libs/othersLibs/interfaz_formula/');
			$this->documento->script('mine-interfaz_formula', '/libs/othersLibs/interfaz_formula/');
			$this->esquema = 'curso-formula4';
			//mine

			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function guardarformula()
	{
		try {
			global $aplicacion;
			$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : 0;
			$icc = !empty($_REQUEST["idcc"]) ? $_REQUEST["idcc"] : 0;
			$tipo = !empty($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : 1;
			$configuracion_nota = !empty($_REQUEST["configuracion_nota"]) ? $_REQUEST["configuracion_nota"] : '';
			if (empty($idcurso)) {
				echo json_encode(array('code' => 'Error', 'msj' => 'No ha seleccionado curso'));
				exit();
			}
			if (empty($icc)) {
				$user = NegSesion::getUsuario();
				JrCargador::clase('sys_negocio::NegProyecto_cursos', RUTA_BASE);
				$oNegProyectocurso = new NegProyecto_cursos;
				$proycurso = $oNegProyectocurso->buscar(array('idcurso' => $idcurso, 'idproyecto' => $user["idproyecto"]));
				if (empty($proycurso[0])) {
					echo json_encode(array('code' => 'Error', 'msj' => 'Este curso no esta asignado a este proyecto'));
					exit();
				}
				$oNegProyectocurso->setCampo($proycurso[0]["idproycurso"], 'configuracion_nota', $configuracion_nota);
				echo json_encode(array('code' => 200, 'msj' => 'Formula de notas Actualizado'));
				exit();
			} else {
				$this->oNegCursos->setCampo_($icc, 'formula_evaluacion', $configuracion_nota, 'acad_curso_complementario');
				echo json_encode(array('code' => 200, 'msj' => 'Formula de notas Actualizado'));
				exit();
			}
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function actualizarnotas()
	{
		try{
			global $aplicacion;
			$idcurso = !empty($_REQUEST["idcurso"]) ? $_REQUEST["idcurso"] : 0;
			$icc = !empty($_REQUEST["icc"]) ? $_REQUEST["icc"] : 0;
			$tipo = !empty($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : 1;
			$idcategoria = !empty($_REQUEST["idcategoria"]) ? $_REQUEST["idcategoria"] : '';
			$this->curso = $this->oNegCursoDetalle->temasacalificar($idcurso, $icc, $tipo, $idcategoria);
			//var_dump($this->curso);
		} catch(Exception $ex){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
