<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		01-03-2018 
 * @copyright	Copyright (C) 01-03-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamen_ubicacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_ubicacion_alumno', RUTA_BASE, 'sys_negocio');
class WebExamen_ubicacion extends JrWeb
{
	private $oNegExamen_ubicacion;
	private $oNegExam_ubicacion_alum;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamen_ubicacion = new NegExamen_ubicacion;
		$this->oNegExam_ubicacion_alum = new NegExamen_ubicacion_alumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idexamen_ubicacion"])&&@$_REQUEST["idexamen_ubicacion"]!='')$filtros["idexamen_ubicacion"]=$_REQUEST["idexamen_ubicacion"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["activo"])&&@$_REQUEST["activo"]!='')$filtros["activo"]=$_REQUEST["activo"];
			if(isset($_REQUEST["rango_min"])&&@$_REQUEST["rango_min"]!='')$filtros["rango_min"]=$_REQUEST["rango_min"];
			if(isset($_REQUEST["rango_max"])&&@$_REQUEST["rango_max"]!='')$filtros["rango_max"]=$_REQUEST["rango_max"];
			if(isset($_REQUEST["idexam_prerequisito"])&&@$_REQUEST["idexam_prerequisito"]!='')$filtros["idexam_prerequisito"]=$_REQUEST["idexam_prerequisito"];
			
			$this->datos=$this->oNegExamen_ubicacion->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Examen_ubicacion'), true);
			$this->esquema = 'examen_ubicacion-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Examen_ubicacion').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegExamen_ubicacion->idexamen_ubicacion = @$_GET['id'];
			$this->datos = $this->oNegExamen_ubicacion->dataExamen_ubicacion;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Examen_ubicacion').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'examen_ubicacion-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Examen_ubicacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idexamen_ubicacion"])&&@$_REQUEST["idexamen_ubicacion"]!='')$filtros["idexamen_ubicacion"]=$_REQUEST["idexamen_ubicacion"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["activo"])&&@$_REQUEST["activo"]!='')$filtros["activo"]=$_REQUEST["activo"];
			if(isset($_REQUEST["rango_min"])&&@$_REQUEST["rango_min"]!='')$filtros["rango_min"]=$_REQUEST["rango_min"];
			if(isset($_REQUEST["rango_max"])&&@$_REQUEST["rango_max"]!='')$filtros["rango_max"]=$_REQUEST["rango_max"];
			if(isset($_REQUEST["idexam_prerequisito"])&&@$_REQUEST["idexam_prerequisito"]!='')$filtros["idexam_prerequisito"]=$_REQUEST["idexam_prerequisito"];
						
			$this->datos=$this->oNegExamen_ubicacion->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarExamen_ubicacion(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdexamen_ubicacion)) {
            	$this->oNegExamen_ubicacion->idexamen_ubicacion = $frm['pkIdexamen_ubicacion'];
            	$accion='_edit';
            }
            $usuarioAct = NegSesion::getUsuario();


            $this->oNegExamen_ubicacion->idrecurso=@$txtIdrecurso;
            $this->oNegExamen_ubicacion->idcurso=@$txtIdcurso;
            $this->oNegExamen_ubicacion->tipo=@$txtTipo;
            $this->oNegExamen_ubicacion->activo=@$txtActivo;
            $this->oNegExamen_ubicacion->rango_min=@$txtRango_min;
            $this->oNegExamen_ubicacion->rango_max=@$txtRango_max;
            $this->oNegExamen_ubicacion->idexam_prerequisito=@$txtIdexam_prerequisito;

            if($accion=='_add') {
            	$res=$this->oNegExamen_ubicacion->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Examen_ubicacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegExamen_ubicacion->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Examen_ubicacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	// ========================== Funciones xajax ========================== //
	public function xSaveExamen_ubicacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdexamen_ubicacion'])) {
					$this->oNegExamen_ubicacion->idexamen_ubicacion = $frm['pkIdexamen_ubicacion'];
				}
				
				$this->oNegExamen_ubicacion->idrecurso=@$frm["txtIdrecurso"];
					$this->oNegExamen_ubicacion->idcurso=@$frm["txtIdcurso"];
					$this->oNegExamen_ubicacion->tipo=@$frm["txtTipo"];
					$this->oNegExamen_ubicacion->activo=@$frm["txtActivo"];
					$this->oNegExamen_ubicacion->rango_min=@$frm["txtRango_min"];
					$this->oNegExamen_ubicacion->rango_max=@$frm["txtRango_max"];
					$this->oNegExamen_ubicacion->idexam_prerequisito=@$frm["txtIdexam_prerequisito"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegExamen_ubicacion->agregar();
					}else{
									    $res=$this->oNegExamen_ubicacion->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegExamen_ubicacion->idexamen_ubicacion);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDExamen_ubicacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegExamen_ubicacion->__set('idexamen_ubicacion', $pk);
				$this->datos = $this->oNegExamen_ubicacion->dataExamen_ubicacion;
				$res=$this->oNegExamen_ubicacion->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegExamen_ubicacion->__set('idexamen_ubicacion', $pk);
				$res=$this->oNegExamen_ubicacion->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegExamen_ubicacion->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}