<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-04-2017 
 * @copyright	Copyright (C) 18-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegMatricula_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegDetalle_matricula_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
class WebMatricula_alumno extends JrWeb
{
	private $oNegMatricula_alumno;
	private $oNegDetalle_matricula_alumno;
	private $oNegActividad_detalle;
    private $oNegGrupo_matricula;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegMatricula_alumno = new NegMatricula_alumno;
		$this->oNegDetalle_matricula_alumno = new NegDetalle_matricula_alumno;
		$this->oNegActividad_detalle = new NegActividad_detalle;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Matricula_alumno', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegMatricula_alumno->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Matricula_alumno'), true);
			$this->esquema = 'matricula_alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Matricula_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Matricula_alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Matricula_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegMatricula_alumno->idmatriculaalumno = @$_GET['id'];
			$this->datos = $this->oNegMatricula_alumno->dataMatricula_alumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Matricula_alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver()
	{
		try{
			global $aplicacion;						
			$this->oNegMatricula_alumno->idmatriculaalumno = @$_GET['id'];
			$this->datos = $this->oNegMatricula_alumno->dataMatricula_alumno;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Matricula_alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'matricula_alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'matricula_alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function auto_asignacion($idalumno=null, $habAMejorar=[])
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;	
			if(empty($_POST['hab_mejorar']) && empty($_POST['id'])){
				throw new Exception(JrTexto::_('Error in filtering'));
			}
			$filtros = [];
			$det_matXMetod = [];
			$habMejorar = ($idalumno==null)?json_decode($_POST['hab_mejorar']):$habAMejorar;
			$this->oNegMatricula_alumno->idalumno = ($idalumno==null)?$_POST['id']:$idalumno;
			$filtros['idalumno'] = ($idalumno==null)?$_POST['id']:$idalumno;
			$filtros['tipo'] = 'L';
			$det_matricula=$this->oNegMatricula_alumno->auto_mejora($filtros);
			if(empty($det_matricula)) { return true; }
			foreach ($det_matricula as $dm) {
				$met=$dm['metodologia'];
				if(!isset($det_matXMetod[$met])){
					$det_matXMetod[$met] = [
						'subir'=>[],
						'reordenar'=>[],
					];
				}

				$arrHab = explode('|', $dm['idhabilidad']);
				foreach ($arrHab as $hab) {
					if(in_array($hab, $habMejorar) && 
						!in_array($dm['iddetalle'], $det_matXMetod[$met]['subir'])){
						$det_matXMetod[$met]['subir'][]=$dm['iddetalle'];
					}
				}
				foreach ($arrHab as $hab) {
					if(!in_array($hab, $habMejorar) && 
						!in_array($dm['iddetalle'], $det_matXMetod[$met]['reordenar']) && 
						!in_array($dm['iddetalle'], $det_matXMetod[$met]['subir'])){
						$det_matXMetod[$met]['reordenar'][]=$dm['iddetalle'];
					}
				}
			}

			/***** reordenando *****/
			foreach ($det_matXMetod as $metod) {
				$i=0;
				foreach ($metod['subir'] as $idDet) {
					$i++;
					$this->oNegDetalle_matricula_alumno->iddetalle=$idDet;
					$this->oNegDetalle_matricula_alumno->orden=$i;
					$resp = $this->oNegDetalle_matricula_alumno->editar();
				}
				foreach ($metod['reordenar'] as $idDet) {
					$i++;
					$this->oNegDetalle_matricula_alumno->iddetalle=$idDet;
					$this->oNegDetalle_matricula_alumno->orden=$i;
					$resp = $this->oNegDetalle_matricula_alumno->editar();
				}
			}
			/***** fin reordenar *****/

			if($idalumno!=null){
				return true;
			}
			$data=array('code'=>'ok','data'=>['mensaje'=>JrTexto::_('Exercises are now emphasized in the skills selected for improvement')]);
            echo json_encode($data);
            return parent::getEsquema();
		} catch(Exception $e) {
			if($idalumno==null){
				$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
	            echo json_encode($data);
            }else{
				return false;
			}
		}
	}

	public function auto_asignacionxgrupo()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;	
			if(empty($_POST['hab_mejorar']) && empty($_POST['id'])){
				throw new Exception(JrTexto::_('Error in filtering'));
			}
			$filtros = array();
			$filtros['idgrupo'] = $_POST['id'];
			$habMejorar = json_decode($_POST['hab_mejorar']);
			$alum_matric = $this->oNegGrupo_matricula->buscar($filtros);
			foreach ($alum_matric as $al) {
				$resp = $this->auto_asignacion($al['idalumno'], $habMejorar);
				if(!$resp) throw new Exception(JrTexto::_('Error when improve learning'));
			}
			$data=array('code'=>'ok','data'=>['mensaje'=>JrTexto::_('Exercises are now emphasized in the skills selected for improvement')]);
            echo json_encode($data);
            return parent::getEsquema();
		} catch(Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function asignacion_automatica($idalumno=null, $arrPorcentXHab=[])
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;	
			if(empty($_POST['id']) && empty($_POST['porcentajeXHab'])){
				throw new Exception(JrTexto::_('Error in filtering'));
			}
			$arr = ($idalumno==null)?$_POST['porcentajeXHab']:$arrPorcentXHab;
			$porcentajeXHab = json_decode($arr,true);
			$arrPorcent = $porcentajeXHab;
			$det_matXMetod = array();
			$arrNuevoOrden = array();
			$filtros['idalumno'] = ($idalumno==null)?$_POST['id']:$idalumno;
			$filtros['tipo'] = 'L';
			$det_matricula=$this->oNegMatricula_alumno->auto_mejora($filtros);
			if(empty($det_matricula)) { return true; }
			foreach ($det_matricula as $dm) {
				$met=$dm['metodologia'];
				if(!isset($det_matXMetod[$met])){
					$det_matXMetod[$met] = array();
				}
				$det_matXMetod[$met][] = $dm;
			}
			foreach ($det_matXMetod as $idMetod=>$arrDetMat) {
				$porcentajeXHab=$arrPorcent;
				if(!isset($arrNuevoOrden[$idMetod])){
					$arrNuevoOrden[$idMetod] = array();
				}
				foreach ($arrPorcent as $p) {
					$porcent_min = 100.0;
					$idHab_min = -1;
					foreach ($porcentajeXHab as $idHab=>$porcentaje) {
						if((float)$porcentaje<$porcent_min){
							$idHab_min=$idHab;
							$porcent_min=$porcentaje;
						}
					}
					unset($porcentajeXHab[$idHab_min]);
					foreach ($arrDetMat as $key=>$detMat) {
						$arrHab = explode('|', $detMat['idhabilidad']);
						if(in_array($idHab_min, $arrHab) && !in_array($detMat['iddetalle'], $arrNuevoOrden[$idMetod])){
							$arrNuevoOrden[$idMetod][]=$detMat['iddetalle'];
							unset($det_matXMetod[$idMetod][$key]); /*<- para que en el array $det_matXMetod solo queden las det_matricula q NO se van a ordenar y que se agregan en orden al final (x Metodologia) */
						}
					}
				}
			}

			/*****  ordenamiento *****/
			foreach ($arrNuevoOrden as $idMet=>$arrIdsDetMatr) {
				$i=0;
				foreach ($arrIdsDetMatr as $key=>$idDetMatr) {
					$i++;
					$this->oNegDetalle_matricula_alumno->iddetalle=$idDetMatr;
					$this->oNegDetalle_matricula_alumno->orden=$i;
					$resp = $this->oNegDetalle_matricula_alumno->editar();
				}
				if(!empty($det_matXMetod[$idMet])){
					foreach ($det_matXMetod[$idMet] as $key=>$detMatr) {
						$i++;
						$this->oNegDetalle_matricula_alumno->iddetalle=$detMatr['iddetalle'];
						$this->oNegDetalle_matricula_alumno->orden=$i;
						$resp = $this->oNegDetalle_matricula_alumno->editar();
					}
				}
			}
			/**** fin ordenamiento ****/

			if($idalumno!=null){
				return true;
			}
			$data=array('code'=>'ok','data'=>['mensaje'=>JrTexto::_('Exercises are now emphasized in the skills selected for improvement')]);
            echo json_encode($data);
		} catch (Exception $e) {
            if($idalumno==null){
				$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
	            echo json_encode($data);
            }else{
				return false;
			}
		}
	}

	public function asignacion_automaticaxgrupo()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;	
			if(empty($_POST['id']) && empty($_POST['porcentajeXHab'])){
				throw new Exception(JrTexto::_('Error in filtering'));
			}
			$filtros = array();
			$filtros['idgrupo'] = $_POST['id'];
			$arrPorcentXHab = $_POST['porcentajeXHab'];
			$alum_matric = $this->oNegGrupo_matricula->buscar($filtros);
			foreach ($alum_matric as $al) {
				$resp = $this->asignacion_automatica($al['idalumno'], $arrPorcentXHab);
				if(!$resp) throw new Exception(JrTexto::_('Error when improve learning'));
			}
			$data=array('code'=>'ok','data'=>['mensaje'=>JrTexto::_('Exercises are now emphasized in the skills selected for improvement')]);
            echo json_encode($data);
		} catch (Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	/*public function ordenar_actividades()
	{
		try {
			global $aplicacion;
			if(empty($_GET['id']) && empty($_GET['target'])){
				throw new Exception(JrTexto::_('There is no Id or Target.'));
			}
			$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];

			$this->esquema = 'ordenar_actividades';
			$this->documento->plantilla = 'modal';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}*/
	// ========================== Funciones xajax ========================== //
	public function xSaveMatricula_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdmatriculaalumno'])) {
					$this->oNegMatricula_alumno->idmatriculaalumno = $frm['pkIdmatriculaalumno'];
				}
				
				$this->oNegMatricula_alumno->__set('idalumno',@$frm["txtIdalumno"]);
					$this->oNegMatricula_alumno->__set('codigo',@$frm["txtCodigo"]);
					$this->oNegMatricula_alumno->__set('tipo',@$frm["txtTipo"]);
					$this->oNegMatricula_alumno->__set('orden',@$frm["txtOrden"]);
					$this->oNegMatricula_alumno->__set('fechamatricula',@$frm["txtFechamatricula"]);
					$this->oNegMatricula_alumno->__set('fechacaduca',@$frm["txtFechacaduca"]);
					$this->oNegMatricula_alumno->__set('regusuario',@$frm["txtRegusuario"]);
					$this->oNegMatricula_alumno->__set('fecharegistro',@$frm["txtFecharegistro"]);
					
				   if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegMatricula_alumno->agregar();
					}else{
						$res=$this->oNegMatricula_alumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegMatricula_alumno->idmatriculaalumno);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDMatricula_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegMatricula_alumno->__set('idmatriculaalumno', $pk);
				$this->datos = $this->oNegMatricula_alumno->dataMatricula_alumno;
				$res=$this->oNegMatricula_alumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegMatricula_alumno->__set('idmatriculaalumno', $pk);
				$res=$this->oNegMatricula_alumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

}