<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016 
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebGuiapedagogica extends JrWeb
{
	private $oNegAcad_curso;
	private $oNegMatricula;
	public function __construct()
	{
		parent::__construct();
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->usuarioAct = NegSesion::getUsuario();
		
	}
	public function defecto(){
		return $this->ver();
	}
	
	public function ver(){
		$this->breadcrumb = [
			[ 'texto'=> ucfirst(JrTexto::_('Guide of pedagogical orientations')) ],
		];
			$this->documento->plantilla = 'alumno/general';
			$this->documento->setTitulo( ucfirst(JrTexto::_('Guide of pedagogical orientations')), true);
			$rol=$this->usuarioAct["idrol"];
			$this->idrol=$rol;			
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$this->cursos=$cursosProyecto;
			$this->esquema = 'libre_tema/guiapedagogica';
			// if ($rol==3) {
			// 	$this->cursosMatric = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], "idproyecto"=>@$this->usuarioAct["idproyecto"]));					
			// }
			return parent::getEsquema();
	}
}