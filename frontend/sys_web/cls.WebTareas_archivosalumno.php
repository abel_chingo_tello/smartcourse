<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-02-2020 
 * @copyright	Copyright (C) 11-02-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTareas_archivosalumno', RUTA_BASE);
class WebTareas_archivosalumno extends JrWeb
{
	private $oNegTareas_archivosalumno;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTareas_archivosalumno = new NegTareas_archivosalumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tareas_archivosalumno', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idtarea"])&&@$_REQUEST["idtarea"]!='')$filtros["idtarea"]=$_REQUEST["idtarea"];
			if(isset($_REQUEST["media"])&&@$_REQUEST["media"]!='')$filtros["media"]=$_REQUEST["media"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			$this->datos=$this->oNegTareas_archivosalumno->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tareas_archivosalumno'), true);
			$this->esquema = 'tareas_archivosalumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tareas_archivosalumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Tareas_archivosalumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tareas_archivosalumno', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegTareas_archivosalumno->idtarearecurso = @$_GET['id'];
			$this->datos = $this->oNegTareas_archivosalumno->dataTareas_archivosalumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Tareas_archivosalumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'tareas_archivosalumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}