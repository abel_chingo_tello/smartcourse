<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-10-2019 
 * @copyright	Copyright (C) 15-10-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE);
class WebAcad_matricula extends JrWeb
{
	private $oNegAcad_matricula;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_categorias = new NegAcad_categorias;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["oldid"])&&@$_REQUEST["oldid"]!='')$filtros["oldid"]=$_REQUEST["oldid"];						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["orderby"])&&@$_REQUEST["orderby"]!='')$filtros["orderby"]=$_REQUEST["orderby"];	
			$usuarioAct = NegSesion::getUsuario();
			$filtrosgrupo=array();
			$filtrosgrupo["idproyecto"]=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$usuarioAct["idproyecto"];			
			$this->gruposaula=$this->oNegAcad_grupoaula->buscar($filtrosgrupo);
			$this->categorias = $this->oNegAcad_categorias->buscar(array('idproyecto'=>$usuarioAct["idproyecto"]));
			//var_dump($this->gruposaula);



			$this->datos=$this->oNegAcad_matricula->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Acad_matricula'), true);
			$this->esquema = 'academico/alumnos-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_matricula').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_matricula->idmatricula = @$_GET['id'];
			$this->datos = $this->oNegAcad_matricula->dataAcad_matricula;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_matricula').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_matricula-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}