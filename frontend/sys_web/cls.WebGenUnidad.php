<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');

class WebGenUnidad extends JrWeb
{	
    private $oNegMatricula;
    private $oNegAcad_cursodetalle;


	public function __construct()
	{
		parent::__construct();
		$this->oNegReportealumno = new NegReportealumno;
		$this->oNegAcad_curso = new NegAcad_curso;
        $this->oNegMatricula = new NegAcad_matricula;
        $this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;

	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Dashboard'), true);
			// $this->documento->plantilla = 'mantenimientos';
            // $this->esquema = 'menu_reportealumno';
            // return "hola";
            //31


            $unidades = array('1' => array(), '2' => array(),'3' => array(),'4' => array(),'5' => array(),'6' => array(),'7' => array(), '8' => array());
            $bimestres = array();
            $this->lista_unidades=$this->oNegReportealumno->buscar(array('tipo'=>'lista_unidades','idpadre'=> 1));
            for($i = 0; $i < count($this->lista_unidades); $i++ ){
                $index = strval($i+1);
                switch($i){
                    case 0: $unidades['1'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    case 1 :
                    case 2 : $unidades['2'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    case 3 :
                    case 4 : $unidades['3'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    case 5 : $unidades['4'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    case 6 : $unidades['5'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    case 7 :
                    case 8 : $unidades['6'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    case 9 :
                    case 10 : $unidades['7'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    case 11 : $unidades['8'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    case 12 :
                    case 14 : $unidades['9'][$index] = $this->lista_unidades[$i]['idnivel']; 
                        break;
                    }
            }
            $j=1;
            for($i=0; $i < count($unidades); $i++){
                if(($i+1) % 2 == 0){
                    $bimestres[$j][$i] = $unidades[$i];
                    $bimestres[$j][$i+1] = $unidades[$i+1];
                    $j++;
                }
                
            }
            var_dump($unidades);
            var_dump($bimestres);
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
    }
    public function vercurso(){
        try{
			global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Dashboard'), true);
            $datos= $this->oNegAcad_cursodetalle->sesiones('26',0);
            var_dump($datos);
			return parent::getEsquema();
        }catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
}
?>