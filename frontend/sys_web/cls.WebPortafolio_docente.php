<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAgenda', RUTA_BASE, 'sys_negocio');
class WebPortafolio_docente extends JrWeb
{	
	private $oNegAgenda;

	public function __construct()
	{
		parent::__construct();
        $this->oNegAgenda = new NegAgenda;
        $this->usuarioAct = NegSesion::getUsuario();
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			if(strtolower($this->usuarioAct["rol"])!==strtolower("Administrador")){
				return $aplicacion->redir();
			}
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_("teacher's portfolio")) /*, 'link'=> '/portafolio_docente/'.$this->idCurso*/ ]
            ];

			$this->esquema = 'docente/portafolio_docente';
			$this->documento->plantilla = 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}