<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-10-2017 
 * @copyright	Copyright (C) 25-10-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAgenda', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebAgenda extends JrWeb
{
	private $oNegCurso;
	private $oNegAgenda;
	private $oNegMatricula;
		
	public function __construct()
	{
		parent::__construct();		
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegAgenda = new NegAgenda;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegCurso = new NegAcad_curso;
				
	}


	public function defecto()
    {
        try {
            global $aplicacion;
            /*$this->documento->stylesheet('bootstrap.min', '/libs/fullcalendar/');
            $this->documento->script('jquery.min', '/libs/fullcalendar/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->script('bootstrap.min', '/libs/fullcalendar/');*/
            $this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
            $this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
            $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
            $this->documento->script('agenda', '/js/alumno/');
            
            $this->documento->setTitulo(JrTexto::_('Agenda'), true);
            $this->usuarioAct = NegSesion::getUsuario();
            $rol=$this->usuarioAct["rol"];
            $this->titulo = ucfirst(JrTexto::_('Agenda'));

            $this->grupoBtns = [
            	['nombre'=> ucfirst(JrTexto::_('Full Schedule')), 'link'=>'/agenda/horario/', 'activo'=>'' ],
            	['nombre'=> ucfirst(JrTexto::_('Agenda')), 'link'=>'/agenda/', 'activo'=>'active' ],
            ];

            $this->breadcrumb = [
	            [ 'texto'=> ucfirst(JrTexto::_('Agenda')) ],
	        ];

            /*if(strtolower($rol)=="alumno"){*/
            	$this->agenda_listado=false;
            	$this->opcionesCalendario();
                $this->documento->plantilla = 'alumno/general';
                $this->esquema = 'alumno/horario';
            /*}else{
                return $aplicacion->redir();
            }*/
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function horario()
    {
    	try {
            global $aplicacion;
            /*$this->documento->stylesheet('bootstrap.min', '/libs/fullcalendar/');
            $this->documento->script('jquery.min', '/libs/fullcalendar/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->script('bootstrap.min', '/libs/fullcalendar/');*/
            $this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
            $this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
            $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
            $this->documento->script('horario', '/js/alumno/');
            
            $this->documento->setTitulo(ucfirst(JrTexto::_('Full Schedule')), true);
            $rol=$this->usuarioAct["rol"];
            $this->titulo = ucfirst(JrTexto::_('Full schedule'));

            $this->grupoBtns = [
            	['nombre'=> ucfirst(JrTexto::_('Full Schedule')), 'link'=>'/agenda/horario/', 'activo'=>'active' ],
            	['nombre'=> ucfirst(JrTexto::_('Agenda')), 'link'=>'/agenda/', 'activo'=>'' ],
            ];

            $this->breadcrumb = [
	            [ 'texto'=> ucfirst(JrTexto::_('Full Schedule')) ],
	        ];

            /*if(strtolower($rol)=="alumno"){*/
            	$this->opcionesCalendario();
                $this->documento->plantilla = 'alumno/general';
                $this->esquema = 'alumno/horario';
            /*}else{
                return $aplicacion->redir();
            }*/
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
/*
	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Agenda', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')$filtros["id"]=$_REQUEST["id"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			if(isset($_REQUEST["alumno_id"])&&@$_REQUEST["alumno_id"]!='')$filtros["alumno_id"]=$_REQUEST["alumno_id"];
			
			$this->datos=$this->oNegAgenda->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Agenda'), true);
			$this->esquema = 'agenda-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Agenda', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Agenda').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			//if(!NegSesion::tiene_acceso('Agenda', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAgenda->id = @$_GET['id'];
			$this->datos = $this->oNegAgenda->dataAgenda;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Agenda').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'agenda-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Agenda', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}*/
			$filtros=array();
			$this->usuarioAct = NegSesion::getUsuario();
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')$filtros["id"]=$_REQUEST["id"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			$filtros["alumno_id"]=$this->usuarioAct['dni'];
			$datos=$this->oNegAgenda->buscar($filtros);
			$calendario = array();
			foreach($datos as $d){
				$elemento=array(
					'id'=> $d['id'],
					'title'=> $d['titulo'],
			        'start'=> $d['fecha_inicio'],
			        'end'=> $d['fecha_final'],
			        'backgroundColor'=> $d['color'],
			        'borderColor'=> $d['color']
				);

				$calendario[]=$elemento;
			}	
			
			echo json_encode(array('code'=>'ok','data'=>$calendario));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function xHorario(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Agenda', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}*/
			$filtros=array("idalumno"=>$this->usuarioAct['dni'], "estado"=>1);
			if(!empty($_REQUEST['idcurso'])){
				$filtros['idcurso']=$_REQUEST['idcurso'];
			}
			$calendario = array();

			if($this->usuarioAct['rol']=='alumno') {
				$this->datos=$this->oNegMatricula->horarioAlumno( array("idalumno"=>$this->usuarioAct['dni'], "estado"=>1));
			} else {
				$this->datos=$this->oNegMatricula->horarioDocente(array("iddocente"=>$this->usuarioAct['dni'], "estado"=>1));
			}
			if(!empty($this->datos)) {
				$this->fechaI=$this->datos[0]['fecha_inicioG'];
	            $this->fechaF=$this->datos[0]['fecha_finalG'];
	            $d_start    = new DateTime(date($this->fechaI)); 
	            $d_end      = new DateTime(date( $this->fechaF)); 
	            $diff = $d_start->diff($d_end); 
	            $this->weeks=floor(($diff->days/7)+1);
				foreach($this->datos as $datos){
					$elemento=array(
						'title'=> $datos['nombre'],
				        'start'=> $datos['fecha_inicio'],
				        'end'=> $datos['fecha_final'],
				        'backgroundColor'=> '#75706b',
				        'borderColor'=> '#75706b',
				        'semanas'=>$this->weeks,
				        'finCiclo'=>$this->datos[0]['fecha_finalG'],
					);
					$calendario[]=$elemento;
				}
			}
			
			echo json_encode(array('code'=>'ok','data'=>$calendario));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function eliminar(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

			@extract($_POST);
        	$res=$this->oNegAgenda->eliminar(@$id);
        	if(!empty($res)){
        		echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Agenda')).' '.JrTexto::_('delete successfully'),'id'=>$res)); 
			}else{
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error').' '.JrTexto::_('Delete Record'))); 
			}

        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarAgenda(){
		$this->documento->plantilla = 'blanco';
		try {
            global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
			@extract($_POST);
            if(!empty(@$id)) {
				$accion='_edit';
				$this->oNegAgenda->id=@$id;
			}
        	$usuarioAct = NegSesion::getUsuario();

			$this->oNegAgenda->titulo=@$txtTitulo;
			$this->oNegAgenda->descripcion=@$txtDescripcion;
			$this->oNegAgenda->fecha_inicio=@$txtFecha_inicio;
			$this->oNegAgenda->fecha_final=@$txtFecha_final;
			if(!empty(@$txtColor)){
				$this->oNegAgenda->color=@$txtColor;
			} else{
				$this->oNegAgenda->color='#1062B5';
			}
			$this->oNegAgenda->fecha_inicio=date("Y-m-d H:i:s", strtotime(@$txtFecha_inicio));
			$this->oNegAgenda->fecha_final=date("Y-m-d H:i:s", strtotime(@$txtFecha_final));

			$this->oNegAgenda->alumno_id=@$usuarioAct['dni'];
            if($accion=='_add') {
            	$res=$this->oNegAgenda->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Agenda')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAgenda->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Agenda')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	
	public function xSaveAgenda(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId'])) {
					$this->oNegAgenda->id = $frm['pkId'];
				}
				
					$this->oNegAgenda->titulo=@$frm["txtTitulo"];
					$this->oNegAgenda->descripcion=@$frm["txtDescripcion"];
					$this->oNegAgenda->color=@$frm["txtColor"];
					$this->oNegAgenda->fecha_inicio=@$frm["txtFecha_inicio"];
					$this->oNegAgenda->fecha_final=@$frm["txtFecha_final"];
					$this->oNegAgenda->alumno_id=@$frm["txtAlumno_id"];					

				   if(@$frm["accion"]=="Nuevo"){
					    $res=$this->oNegAgenda->agregar();
					}else{
					    $res=$this->oNegAgenda->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAgenda->id);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAgenda(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAgenda->__set('id', $pk);
				$this->datos = $this->oNegAgenda->dataAgenda;
				$res=$this->oNegAgenda->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAgenda->__set('id', $pk);
				$res=$this->oNegAgenda->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAgenda->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
	// ========================== Funciones Privadas ========================== //
	private function opcionesCalendario()
	{
		$this->arrColores=[
			['rgb'=> '#2ECC71','btn'=> 'btn-green'], ['rgb'=> '#27AE60','btn'=> 'btn-green-dark'], 
			['rgb'=> '#1ABC9C','btn'=> 'btn-turquoise'], ['rgb'=> '#16A085','btn'=> 'btn-turquoise-dark'], 
			['rgb'=> '#2f81d4','btn'=> 'btn-blue'], ['rgb'=> '#1062B5','btn'=> 'btn-blue-dark'], 
			['rgb'=> '#3498DB','btn'=> 'btn-aqua'], ['rgb'=> '#2980B9','btn'=> 'btn-aqua-dark'], 
			['rgb'=> '#9B59B6','btn'=> 'btn-purple'], ['rgb'=> '#8E44AD','btn'=> 'btn-purple-dark'], 
			['rgb'=> '#34495E','btn'=> 'btn-black'], ['rgb'=> '#2C3E50','btn'=> 'btn-black-dark'], 
			['rgb'=> '#2f81d4','btn'=> 'btn-yellow'], ['rgb'=> '#F39C12','btn'=> 'btn-yellow-dark'], 
			['rgb'=> '#E67E22','btn'=> 'btn-orange'], ['rgb'=> '#D35400','btn'=> 'btn-orange-dark'], 
			['rgb'=> '#E74C3C','btn'=> 'btn-red'], ['rgb'=> '#C0392B','btn'=> 'btn-red-dark'], 
			['rgb'=> '#BDC3C7','btn'=> 'btn-white-dark'], ['rgb'=> '#95A5A6','btn'=> 'btn-grey'], 
			['rgb'=> '#7F8C8D','btn'=> 'btn-grey-dark'], ['rgb'=> '#A38F84','btn'=> 'btn-brown'], 
			['rgb'=> '#75706B','btn'=> 'btn-brown-dark'], 
		];
	}

}