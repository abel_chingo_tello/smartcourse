<?php
defined('RUTA_BASE') or die();
class WebCodigos extends JrWeb
{
	//private $oNegTareas;
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto()
	{
		return $this->listado();
	}



	public function listado()
	{
		try {
			global $aplicacion;
			$this->documento->stylesheet('list-theme', '/tema/css/list/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('funciones', '/js/');
			// mine
			$this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
			$this->documento->script('mine', '/libs/othersLibs/mine/js/');
			
			$this->documento->stylesheet('soft-rw-theme-paris', '/libs/othersLibs/mine/css/');
			$this->documento->stylesheet('mine-rubrica', '/libs/othersLibs/rubrica/');
			
			$this->documento->script('mine-rubrica', '/libs/othersLibs/rubrica/');
			$this->documento->script('mine-crud-tarea', '/libs/othersLibs/crud-tarea/');
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/v2.10/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/v2.10/');
			// mine

			$this->usuario = NegSesion::getUsuario();
			
			$this->esquema = 'codigos/suscripcion';
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tareas'), true);
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function renovarsuscripcion(){
		try {
			global $aplicacion;
			$this->documento->stylesheet('list-theme', '/tema/css/list/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('funciones', '/js/');
		
		
			$this->documento->script('sweetalert2.min', '/libs/sweetalert/v2.10/');
			$this->documento->stylesheet('sweetalert2.min', '/libs/sweetalert/v2.10/');

			$this->usuario = NegSesion::getUsuario();
			
			$this->esquema = 'codigos/renovarsuscripcion';
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Renovar suscripción'), true);

			$idmatricula=!empty($_REQUEST["idmatricula"])?$_REQUEST["idmatricula"]:false;
			if($idmatricula==false){
				return $aplicacion->error(JrTexto::_($e->getMessage()));
			}else{
				$this->idmatricula=$idmatricula;
				return parent::getEsquema();
			}
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}

	}
}
