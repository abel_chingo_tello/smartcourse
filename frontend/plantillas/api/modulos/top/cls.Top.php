<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_RAIZ') or die();
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_RAIZ);
JrCargador::clase('sys_negocio::NegRoles', RUTA_RAIZ);
class Top extends JrModulo
{
	protected $oNegConfig;
	protected $oNegRoles;
	protected $oNegPaginas;
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegRoles =new NegRoles();
		$this->modulo = 'top';
	}
	
	public function mostrar($html=null)
	{
		try {			
			if(empty($html)){
				$this->esquema = 'top';
			}else $this->esquema = $html;			
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}