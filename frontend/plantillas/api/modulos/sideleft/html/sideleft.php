<?php 
defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';
$rutastatic=$this->documento->getUrlStatic();
?>
<figure class="user-photo text-center">
  <img src="https://abacoeducacion.org/web/static/media/imagenes/user_avatar.jpg" alt="user" class="img-fluid" style="width:100px; border-radius: 50%; border: 2px solid #464646; box-shadow: 1px 2px 5px black;" >
</figure>
<p class="w3-large text-center w3-text-white"><?php echo $this->usuario['nombre_full'] ?></p>
<p class="w3-medium text-center w3-round w3-text-white" style="background: #083b6f;"><?php echo $this->usuario['rol'] ?></p>
<div class="p-1 w3-round" style="background: #083b6f;">
  <ul class="w3-ul">
    <li class="p-0">
    <a class="btn btn-primary w3-small w3-block w3-round" href="<?php echo $this->documento->getUrlSitio() ?>/representantes" >Administrar Representante</a>
    </li>
    <li class="p-0">
    <a class="btn btn-primary w3-small w3-block w3-round" href="#" >Accesos a las plataformas</a>
    </li>
    <li class="p-0">
    <a class="btn btn-primary w3-small w3-block w3-round" href="#" >Administrar Licencias</a>
    </li>
    <li class="p-0">
    <a class="btn btn-primary w3-small w3-block w3-round" href="#" >Administrar Matriculas</a>
    </li>
  </ul>
</div>