<script src="<?php echo $this->documento->getUrlStatic() ?>/libs/chat/fancywebsocket.js"></script>
<style type="text/css">
.pnlchat, .pnlchatuser{
	z-index: 999;
	right: 0.5ex;
	bottom: 0px;
	margin-bottom:0px;
	position: fixed;
	display: inline-block;
	min-width: 240px;
	max-height: 100%;
	border: 1px solid #ced0d2;
	background: #fff;
}

.pnlchatuser.userchat1 {
	right: 250px;
}
.pnlchatuser.userchat2 {
	right: 495px;
}
.pnlchatuser.userchat3 {
	right: 740px;
}
.pnlchatuser.userchat4 {
	right: 985px;
}
.pnlchatuser.userchat5 {
	right: 1230px;
}
.pnlchatuser.userchat6 {
	right: 1475px;
}
.pnlchatuser.userchat7 {
	right: 1720px;
}

#vchatclone.pnlchatuser{
	display: none;
}
.pnlchat .panel-heading,.liuser, .pnlchatuser .panel-heading{
	cursor: pointer;	
}
.liuser{
	padding-bottom: 0.15ex;
}
.pnlchat .panel-body, .pnlchatuser .panel-body{
	display: none;
}
.pnlchat.active .panel-body, .pnlchatuser.active .panel-body{
	display: block;
}
.bg-white-dark, .btn-white-dark{ background: #BDC3C7; color: #34495E; }

</style>
<div class="panel pnlchatuser" id="vchatclone">
	<div class="panel-heading bg-blue">
		<h3 class="panel-title" style="font-size: 1em; margin:0px; padding: 1ex;">Chat</h3>
		<span class="pull-right cerrarvchat"><i class="fa fa-close"></i></span>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body " style="margin: 0px; padding: 0.2ex;">
	    <div class="chatAll" style="max-height: 300px; min-height: 300px; padding: 1ex;">                                                
            <div class="mensajes text-center">
              <div class="text-center">Wellcomme to chat<br><span class="userid"></span></div>
            </div>             		
		</div>		
        <textarea id="txtnewmensaje" placeholder="Escriba su Mensaje" style="width:100%; height:50px; overflow: auto;"></textarea> 
	</div>
</div>
<div class="panel pnlchat" >
	<div class="panel-heading bg-blue" id="chatparticipantes">
		<h3 class="panel-title"  style="font-size: 1em;"><span><?php echo JrTexto::_('chat'); ?></span> <i class="fa fa-comment-o pull-right"></i></h3>
   	    <span class="pull-right  creargrupo" style="display: none" title="Crear grupo"><!--i class="fa fa-plus"></i--></span>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body userchat" style="overflow: auto;">
		<div  data-user='abel' id="userclone" class="liuser" style="display: none;">
           <i class="fa fa-user active"></i>
 			<span class="nombre">abel</span>     			
     		<!--div class="btn-group pull-right">
     			<button type="button" class="btn btn-default btn-xs btnmicroprivado" title="Activar/desactivar microfono private"><i class="fa fa-phone animated infinite"></i></button>					
		 	</div-->	
		 	<div class="clearfix"></div>		 	
		 </div>			
	</div>
</div>
<script type="text/javascript">
    var socketchat;
    var logchat=false;
    var userlogin={user:'<?php echo $this->usuario["usuario"]; ?>',dni:'<?php echo $this->usuario["dni"]; ?>'}
    var verlog=function(txt){
    	if(logchat)console.log(txt)
    }
	var _chataddmensaje=function(data,user){
		var texto=data.texto;		
		$('.pnlchat .userchat #'+dni+'.liuser').trigger('click');
		var currentTime = new Date();
    	var time=currentTime.getHours()+':'+currentTime.getMinutes()+':'+currentTime.getSeconds();
		console.log(data);
		if(user){
			var de=data.para;
    		txt='<div class="mensaje" id="'+userlogin.dni+'"><div class="pull-right text-left globoright"><br>'+texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
    	}else{
    		//console.log(data);
    		var de='chat_'+data.useremit.user;
		    var dni=data.useremit.useremitdatos.dni;    		
    		txt='<div class=""  id="'+dni+'"><div class="pull-left text-left globoleft"><br>'+texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
    	}


    	if($('#'+de+' .mensajes').length){
    		var scrollHeight=$('#'+de+' .mensajes')[0].scrollHeight;
    		$('#'+de+' .mensajes').append(txt).animate({scrollTop:scrollHeight}, 500);
    	}
	}

	var _newuser=function(data){
		verlog(data);
		var pnchat=$('.pnlchat .userchat');		
		var useradd=data.useremit;
		if(!pnchat.find('#'+useradd.useremitdatos.dni).length){
			var newuser=$('#userclone',pnchat).clone(true).show();
			newuser.attr('id',useradd.useremitdatos.dni).attr('data-user',useradd.user);
			newuser.find('.nombre').text(useradd.user);
			pnchat.append(newuser);
		}		
	}

	var _listarconectados=function(data){
		verlog(data);
		var pnchat=$('.pnlchat .userchat');
		var newuser=$('#userclone',pnchat).clone(true).show();
		var usuarios=data.usuarios;
		$.each(usuarios,function(i,v){			
			if(v.useremitdatos.dni!=userlogin.dni){
				newuser.attr('id',v.useremitdatos.dni).attr('data-user',v.user);
				newuser.find('.nombre').text(v.user);
				pnchat.append(newuser);
			}
		});

	}
	var verchatparticipantes=function(){
		$('#chatparticipantes').trigger('click');
	}
	function chatsend(data) {
		data.useremit='<?php echo $this->usuario["usuario"]; ?>';
		data.useremitdatos={
			user:'<?php echo $this->usuario["usuario"]; ?>',
			dni:'<?php echo $this->usuario["dni"]; ?>',
			nombre:'<?php echo $this->usuario["nombre_full"]; ?>',
			email:'<?php echo $this->usuario["email"]; ?>'};			
		Server.send('message',JSON.stringify(data));
	}
	function chatreturnsocket(fn,data){
		if(fn=='chataddmensaje') _chataddmensaje(data,false);
		else if(fn=='listarconectados') _listarconectados(data);
		else if(fn=='newuser') _newuser(data);
	}

	Server = new FancyWebSocket('ws://127.0.0.1:8080');
	var chatconectado=false;	
	Server.bind('open', function(){
		var msj={accion:'listarconectados'};
		chatsend(msj);
		chatsend({accion:'newuser'});
	});
	Server.bind('close', function(){
		verlog('desconectado');
	});
	Server.bind('message', function(data){		
		verlog(data);
		var resultado=JSON.parse(data);
    	var accion=resultado.accion||'';
       	if(accion!='') chatreturnsocket(accion,resultado);
    	else verlog(data);
	});
	Server.connect();	
	$(document).ready(function(){
		//var addchatuser=function(tipo){}
		var showchats=function(){			
			var nc=nshowchats();
			j=0;
			for(i=1;i<=nc;i++){				
				if($('.pnlchatuser').hasClass('userchat'+i)){
					j++;
					$('.pnlchatuser.userchat'+i).removeClass('userchat'+i).addClass('userchat'+j);
				}
			}			
		}
		var activarvchat=function(obj){
			$('.pnlchatuser .panel-heading').removeClass('bg-blue').addClass('bg-white-dark');
			var pnlb=$('.panel-body',obj);
			var pnlh=$('.panel-heading',obj);
			pnlh.removeClass('bg-white-dark').addClass('bg-blue');
			obj.addClass('active');
			$('.txtnewmensaje',obj).focus();
		}
		var nshowchats=function(){
			var width = window.innerWidth;
	        if(width < 540) return 1;
	        else{
	            width = width - 245; 
	            return parseInt(width/245);
	        }
		}

		$('body').on('click','.pnlchatuser .panel-heading',function(){
			var pn=$(this).closest('.panel').toggleClass('active');			
		}).on('click','.pnlchatuser textarea',function(){			
			activarvchat($(this).closest('.panel'));
		}).on('keypress','.pnlchatuser textarea',function(ev){	//escribe texto 		
			var pn=$(this).closest('.panel');
			if(ev.keyCode == 13 && this.value){
				var msj={
					texto:this.value,
					para:pn.attr('id'),
					accion:'chataddmensaje'
				};
				_chataddmensaje(msj,true);
				chatsend(msj);
				$(this).val('');
				$(this).focus();
				return;
			}
			
		}).on('click','.pnlchatuser .cerrarvchat',function(){			
			$(this).closest('.panel').remove();			
			showchats();
		});

		$('.pnlchat').on('click','.userchat .liuser',function(ev){
			var iduser=$(this).attr('data-user');			
			$('.pnlchatuser .panel-heading').removeClass('bg-blue').addClass('bg-white-dark');
			var vchat=$('.pnlchatuser').length;
			var _nshowchats=nshowchats();

			if(vchat>_nshowchats){
				vchat=_nshowchats;
				$('.userchat'+vchat).removeClass('userchat'+vchat);				
			}
			if($('#chat_'+iduser).length>0){
				$('#chat_'+iduser).show();
				var addClass='userchat'+vchat;
				for(i=1;i<vchat;i++){
					if($('#chat_'+iduser).hasClass('userchat'+i)){
						addClass='';
					}
				}
				$('#chat_'+iduser).addClass('active '+addClass).find('.panel-heading').removeClass('bg-white-dark').addClass('bg-blue');
				return;
			}

			var vn=$('#vchatclone').clone(true);
			vn.addClass('active userchat'+vchat).find('.panel-heading').removeClass('bg-white-dark').addClass('bg-blue').find('h3').text(iduser);
			vn.attr('id','chat_'+iduser);
			$('.pnlchatuser:first').before(vn);
		}).on('click','#chatparticipantes h3',function(){
			var obj=$(this).closest('.panel');
			var pnlb=$('.panel-body',obj);
			var pnlh=$('.panel-heading',obj);
			pnlb.toggle('slow');
			if(!pnlb.hasClass('active')){//mostrar chat
				pnlb.addClass('active');
				$('h3 span',pnlh).text('Participantes');
				$('h3 i',pnlh).hide();				
				$('.creargrupo',pnlh).show();
			}else{//ocultarchat
				pnlb.removeClass('active');
				$('h3 span',pnlh).text('Chat');
				$('h3 i',pnlh).show();
				$('.creargrupo',pnlh).hide();
			}	
		}).on('click','.creargrupo',function(ev){			
			ev.preventDefault()
			ev.stopPropagation()
			return false;
		});
	});
</script>
