<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_RAIZ') or die();
class Pie extends JrModulo
{
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		//$this->oNegServicios = new NegServicios;
		$this->modulo = 'pie';
		//$this->oNegPaginas = new NegPaginas();
	}
	
	public function mostrar($html=null)
	{
		try{		
			$this->esquema = 'pie'.$html;				
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}