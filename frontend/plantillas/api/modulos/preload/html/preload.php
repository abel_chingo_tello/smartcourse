<link href="<?php echo $this->documento->getUrlTema()?>/css/preload.css" rel="stylesheet">
<div id="loader-wrapper">
    <div id="loader" class="col-lg-10 col-md-10 col-sm-9 offset-lg-2 offset-md-2 offset-sm-3 text-center">
    	<i class="fa fa-spinner fa-pulse" style="color:#005b95; font-size: 8em"></i>
    </div>
</div>
<div class="modal fade" id="modalclone" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h5 class="modal-title" id="modaltitle" style="width: 100%"></h5>
        <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>       
      </div>
      <div class="modal-body" id="modalcontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/cargando.gif"><br><span style="color: #006E84;"><?php echo JrTexto::_("Loading");?></span></div>
      </div>
      <div class="modal-footer" id="modalfooter">        
        <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>
  </div>
</div>
<?php $fecha = date('Y-m-d H:i:s'); ?>
<script type="text/javascript">
$(document).ready(function() {
    if(userinfo = localStorage.getItem("userinfo")){
        var fecha = '<?php echo $fecha ?>';
        var jsonParseado = JSON.parse(userinfo);
        jsonParseado.fecha =fecha;
        var stringJSON = JSON.stringify(jsonParseado);
        localStorage.setItem("userinfo", stringJSON);
    }
    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 1000);    
});
</script>