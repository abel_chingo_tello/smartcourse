<?php defined('_BOOT_') or die(''); $version=!empty(_version_)?_version_:'1.0';?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/libs/pnotify/pnotify.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/proyecto/general.css?vs=<?php echo $version; ?>" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/tabpasos.css?vs=<?php echo $version; ?>" rel="stylesheet">
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery-ui.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script src="<?php echo $documento->getUrlStatic()?>/libs/pnotify/pnotify.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js?vs=<?php echo $version; ?>"></script>
    <script> var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>';
            var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';
            var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
        </script>
    <jrdoc:incluir tipo="cabecera" />
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/libs/moment/moment.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/libs/chingo/achttiempopasado.js?vs=<?php echo $version; ?>"></script>
</head>
<body >
<link rel="stylesheet" href="<?php echo $documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.css">
<script type="text/javascript" src="<?php echo $documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.min.js"></script>
<jrdoc:incluir tipo="modulo" nombre="preload" /></jrdoc:incluir>
<div class="container-fluid">       
        <jrdoc:incluir tipo="mensaje" />    
        <jrdoc:incluir tipo="recurso" />           
</div>
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<jrdoc:incluir tipo="docsJs" />
</body>
</html>
