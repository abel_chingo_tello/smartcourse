<?php
defined('_BOOT_') or die('');
$version = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $documento->getUrlStatic();
?>
<!DOCTYPE html>
<html>

<head>
  <jrdoc:incluir tipo="modulo" nombre="metadata" />
  <meta name="google" content="notranslate" />
  </jrdoc:incluir>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/fontawesome-free/css/all.min.css">
  <link href="<?php echo $rutastatic; ?>/tema/css/font-awesome.min.css" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script type="text/javascript" src="<?php echo $rutastatic; ?>/libs/sweetalert/sweetalert2.all.min.js"></script>
  <!-- jQuery -->
  <script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/jquery/jquery.min.js"></script>
  <script src="<?php echo $rutastatic; ?>/libs/poppers/popper.min2.js"></script>
  <!-- Bootstrap 4 -->  
  <script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/dist/js/adminlte.js?vs=<?php echo $version; ?>"></script>
  <!-- <script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/dist/js/adminlte.min.js?vs=<?php echo $version; ?>"></script> -->
  <script type="text/javascript" src="<?php echo $rutastatic; ?>/tema/js/funciones.js?vs=<?php echo $version; ?>"></script>
  <script>
    var _sysUrlBase_ = '<?php echo $documento->getUrlBase() ?>';
    var sitio_url_base = _sysUrlSitio_ = '<?php echo $documento->getUrlSitio() ?>';
    var _sysIdioma_ = '<?php echo $documento->getIdioma() ?>';
    var _sysUrlStatic_ = '<?php echo $rutastatic; ?>';
    var _menus_ = new Array();
  </script>
  <style>
    .iframecontent {
      border: 0px;
      width: 100%;
      height: calc(100vh - 120px);
      margin: 0px;
      padding: 0px;
      border-top-left-radius: 0.8ex;
    }

    .control-sidebar {
      height: calc(100vh - 112px);
      overflow-y: auto;
    }
  </style>
</head>
<!-- hold-transition sidebar-mini accent-warning -->

<body class="hold-transition sidebar-mini _texto_">
  <div class="wrapper">
    <!-- Navbar -->
    <jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <jrdoc:incluir tipo="modulo" nombre="sideleft" /></jrdoc:incluir>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content">
        <jrdoc:incluir tipo="recurso" />
        <!-- /.content -->
      </div>
    </div>
    <jrdoc:incluir tipo="modulo" nombre="pie" />
    </jrdoc:incluir>
    <!-- /.content-wrapper -->
    <!-- Control Sidebar -->
    <jrdoc:incluir tipo="modulo" nombre="sideright" />
    </jrdoc:incluir>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo $rutastatic; ?>/tema/paris/adminLTE3/dist/js/demo.js?vs=<?php echo $version; ?>"></script>
  <?php
  $configs = JrConfiguracion::get_();
  require_once(RUTA_SITIO . 'ServerxAjax.php');
  echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
  ?>
  <script>
    var popoverinlist=function(){


    }


    $(document).ready(function() {
      $("._menu_").addClass(py.paris.menu);
      $("._logo_").addClass(py.paris.logo);
      $("._texto_").addClass(py.paris.texto);
      $("._navbar_").addClass(py.paris.navbar);      
    });

    function __url__(enlace, mnu = null, isSubMenu = false) {

      $("#iFrmContent").attr("src", enlace);
      $(".mnus").removeClass("active");
      if (isSubMenu) {
        let li = $("#mnu" + mnu).parent().parent().parent();

        li.addClass("menu-open").children("ul").show();
      }
      if (mnu !== null) {
        $("#mnu" + mnu).addClass("active");
        window.location.href = "#" + window.btoa(mnu);
      }  
      if(isMobile.any()!=null ||($('body').width()<=580 && $('body').hasClass('sidebar-open'))){
        $('body').removeClass('sidebar-open').addClass('sidebar-collapse');
      }
    }

    $(window).bind('hashchange', function(e) {
      var url_ = window.location.href.split("#");
      mnu_active(url_);
    });

  </script>
  <div class="modal fade" id="modalclone" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="modaltitle" style="width: 100%"></h5>
          <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" id="modalcontent">
          <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $documento->getUrlStatic() ?>/media/cargando.gif"><br><span style="color: #006E84;"><?php echo JrTexto::_("Loading"); ?></span></div>
        </div>
        <div class="modal-footer" id="modalfooter">
          <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
        </div>
      </div>
    </div>
  </div>
 <script type="text/javascript" src="<?php echo $rutastatic; ?>/js/asistentetooltip.js?vs=<?php echo $version; ?>"></script> 

<?php 
  $user=NegSesion::getUsuario();
  $empresasconchat=array(27,46);
  if(in_array($user["idempresa"],$empresasconchat)){ ?>
  <script defer  src="https://widget.tochat.be/bundle.js?key=742ebb89-adcc-4f34-a930-0cd897ef6328"></script>
<?php } ?>
<style>
.bs-tooltip-auto[x-placement^=bottom] .arrow::before, .bs-tooltip-bottom .arrow::before {
    border-bottom-color: #f40505;
}

.tooltip-inner {
    color: #fff;
    background-color: #f40505;
}
</style>
</body>
</html>