<?php

defined('_BOOT_') or die('');
$version = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $documento->getUrlStatic();
// include '.sys_datos\sistema\cls.WebGrupoAula.php';
JrCargador::clase('sys_datos\sistema::DatGrupoAula', RUTA_BASE);
$oGrupoAula = new DatGrupoAula();
$arr = $oGrupoAula->listarGrupoAula();
// echo json_encode($arr);

?>

<!DOCTYPE html>
<html>

<head>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   <style>
      .my_table {
         text-align: center;
      }
   </style>
</head>

<body style="padding-top: 10px;">



   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Editando</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <form>
                  <div class="form-group">
                     <label for="nombre">Nombre: </label>
                     <input type="text" class="form-control" id="nombre" aria-describedby="emailHelp">
                     <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                  </div>
                  <div class="form-group">
                     <label for="comentario">Comentario: </label>
                     <input type="text" class="form-control" id="comentario">
                  </div>
                  <div class="form-group">
                     <label for="comentario">Nº Vacantes: </label>
                     <input type="text" class="form-control" id="comentario">
                  </div>

               </form>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               <button type="button" class="btn btn-primary">Save changes</button>
            </div>
         </div>
      </div>
   </div>

   <table id="my_table" class="my_table" style="width:100%">
      <tr>
         <th>id</th>
         <th>nombre</th>
         <th>tipo</th>
         <th>comentario</th>
         <th>nvacantes</th>
         <th>estado</th>
         <th>idproyecto</th>
         <th></th>
         <th></th>

      </tr>
      <?php for ($i = 0; $i < sizeof($arr); $i++) { ?>
         <tr>
            <td><?php echo $arr[$i]["idgrupoaula"] ?></td>
            <td><?php echo $arr[$i]["nombre"] ?></td>
            <td><?php echo $arr[$i]["tipo"] ?></td>
            <td><?php echo $arr[$i]["comentario"] ?></td>
            <td><?php echo $arr[$i]["nvacantes"] ?></td>
            <td><?php echo $arr[$i]["estado"] ?></td>
            <td><?php echo $arr[$i]["idproyecto"] ?></td>
            <td> <button type="button" onclick="editar(<?php echo $arr[$i]['idproyecto'] ?>,<?php echo $arr[$i]['nombre'] ?>,<?php echo $arr[$i]['comentario'] ?>)" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  Editar
               </button>
            </td>
            <td><button>eliminar</button></td>

         </tr>
      <?php } ?>
   </table>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   <script>
      function editar(obj){
         console.log(obj);
         document.querySelector("#nombre").value="holi";
      }
   </script>
</body>

</html>