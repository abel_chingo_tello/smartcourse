<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="assets/css/index.css" />
  <title>Document</title>
</head>

<body>
  <!-- ===============  TOP  =================== -->
  <nav class="navbar">
    <div class="container_icon_menu_nav">
      <span class="icon icon-menu"></span>
    </div>
    <div class="container_rigth_nav">
      <span class="icon icon-home"></span>
      <span class="icon icon-exit"></span>
    </div>

  </nav>
  <div class="home-course-section-1">
    <div id="wf-mask-background-course"></div>
    <div class="grid">
      <div class="">
        <p class="white txt-name-course">A2 Adolescente presentación Blended adolecente</p>
      </div>
      <div class="txt-right">
        <p class="white txt-date-course">Inicio:02-09-2021</p>
        <p class="white txt-date-course">Fin:02-09-2021</p>
      </div>
    </div>
    <div class="content-blank bg-default-course">

    </div>
  </div>
  <!-- ===============  MODAL CONFIGURATION    =================== -->
  <div class="modal-container" id="modal_config">
    <div class="modal"  >
      <span class="icon-close-modal" onclick="closeModal('modal_config')">X</span>
      <div class="config-container">
        <ul class="config-list">
          <li><a href="#">Mi perfil</a></li>
          <li><a href="#">Cambiar contraseña</a></li>
          <li><a href="/">Cerrar sesión</a></li>
        </ul>
      </div>
    </div>

</body>