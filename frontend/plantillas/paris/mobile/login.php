<?php defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo URL_BASE ?>frontend/plantillas/paris/mobile/assets/css/index.css" />
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/sweetalert/v2.10/sweetalert2.all.min.js"></script>
    <script>
            var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>';
            var _sysUrlSitio_ = '<?php echo $documento->getUrlSitio()?>';            
            var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getIdioma()?>';</script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js?vs=<?php echo $verion; ?>"></script>    
    <jrdoc:incluir tipo="cabecera" />
</head>
<body>
<jrdoc:incluir tipo="recurso" />
<script> sitio_url_base = '<?php echo $documento->getUrlSitio()?>';</script>
<jrdoc:incluir tipo="docsJs" />
<jrdoc:incluir tipo="docsCss" />    
</body>
</html>
