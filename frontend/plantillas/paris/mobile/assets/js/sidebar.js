try {
    let sidebar = document.getElementById("sidebar"); 
    let itemChild = document.getElementsByClassName('item-parent');
    var body = document.getElementsByTagName("body")[0];
    var htmltmp = document.getElementsByTagName("html")[0];

    window.addEventListener("load",main)

    if (sidebar != undefined)
        sidebar.style.display = "none";

    function main(){
        for (let i = 0;i<=itemChild.length-1;i++){
            itemChild[i].firstElementChild.onclick= ()=> toogleItem(itemChild[i]);
        }
    }
    
    function opensidebar(){ 
        sidebar.classList.remove("sidebar-close") ;
        sidebar.style.display ="block";
    }
    function closesidebar(){ 
        
        sidebar.classList.add("sidebar-close") ;
        setTimeout(() =>{ 
            sidebar.style.display ="none";
        },700)  
    }


    // ==================  MODAL ===================

    function openModal(id){ 
        let idModal = document.getElementById(id); 
        // console.log(id);
        idModal.style.display ="block";
    
    }
    function closeModal(id){
        let idModal = document.getElementById(id); 
        // console.log(id);
        idModal.style.display ="none";
        
    }
    function open_closeModal(idOpen,idClose){
        openModal(idOpen)
        closeModal(idClose)
    }
    // ==================  LIST ITEM  ===================

    function toogleItem (nodeItem){ 
    let ul_items = nodeItem.lastElementChild 
    let icon_arrow = nodeItem.querySelectorAll(".icon-toogle");
        icon_arrow.forEach(element => {
            // console.log('2element',element)
            element.classList.toggle("icon-toogle-active");
        });
    
        // console.log("lastChild =>",ul_items)
        ul_items.style.marginLeft ='15px' 
        ul_items.classList.toggle("visible");
        nodeItem.classList.toggle("select-item");

        
    }
    body.addEventListener("mousemove", function() {
        localStorage.setItem("conteo", localStorage.getItem("conteoInicial"));
    })
    htmltmp.addEventListener("touchmove", function() {
        localStorage.setItem("conteo", localStorage.getItem("conteoInicial"));
    })
} catch (ex) {
    console.log(ex);
}
