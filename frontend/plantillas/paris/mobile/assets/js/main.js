import configuracionUser from "./funcionalidades/configuracionUser.js"

window.addEventListener("load",main)

function main(){
    Eventos()
}
function Eventos(){
    $("body").on("click",".onCerrarSesion",function(){
        configuracionUser.cerrarsesion()
    })
    $("body").on("click",".linkto",function(){ 
        const element = $(this)
        redireccionar(element.data("url"),element.data("mnu")) 
    })
    $("#passActual").on("blur",function(){ configuracionUser.validarActualContrasena($(this).val()) })
    $("#frmSvPass").submit(function (ev) { configuracionUser.cambiarcontrasena(ev) });
}
/**
 * Funcion Copia de __url__ : Redirecciona a un nuevo modulo de la plataforma
 * @param {*} enlace URL del modulo
 * @param {*} mnu Indice del menu, por ejemplo (-1), siempre en negativo por logica actual
 * @param {*} isSubMenu 
 */
function redireccionar(enlace, mnu = null, isSubMenu = false) {

    $("#iFrmContent").attr("src", enlace);
    $(".mnus").removeClass("active");
    if (isSubMenu) {
      let li = $("#mnu" + mnu).parent().parent().parent();

      li.addClass("menu-open").children("ul").show();
    }
    if (mnu !== null) {
      $("#mnu" + mnu).addClass("active");
      window.location.href = "#" + window.btoa(mnu);
    }  
    if(isMobile.any()!=null ||($('body').width()<=580 && $('body').hasClass('sidebar-open'))){
      $('body').removeClass('sidebar-open').addClass('sidebar-collapse');
    }
}