class configuracionUser{
    constructor(){}
    cerrarsesion(){
        $.post(String.prototype.concat(_sysUrlBase_,"json/sesion/salir"), function(data, status){
            if(data.code==200){  
                window.location.href=_sysUrlSitio_;
            }
        },'json').fail(function(errObj,textStatus,error){
            console.error("Error ajax cerrarsesion: ",error)
        });
    }
    cambiarcontrasena(evento){
        evento.preventDefault();
        if($("#passValidar").val() == "0"){
          Swal.fire('Alerta!','Contraseña actual incorrecta','danger');
        }else{
          Swal.fire({
              title: TRADUCCIONPHP_PLANTILLA.Confirme,
              text: '¿Seguro de cambiar contraseña?',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: TRADUCCIONPHP_PLANTILLA.Accept,
              cancelButtonText: TRADUCCIONPHP_PLANTILLA.Cancel,
              }).then((result) => {
                  if (result.value) {            
                    $.post(_sysUrlBase_ + 'json/persona/changePassAct',
                      {p_datos: $("#frmSvPass").serialize()},
                      function(data, status) {
                        if (data.code === 200) {
                          Swal.fire('Exito!','Contraseña cambiada','success');
                          $("#frmPassCerrar").click();
                        }
                      }, 'json');
                  }
              });
        }
    }
    validarActualContrasena(v){
      $.post(_sysUrlBase_ + "json/persona/listado",{sql4: 1, clave: v},function(data,status){
        if(data.code == 200){
          var numero = data.data[0]["numero"];
          var color = "green";
          if(numero == "0"){
            color = "red";
          }
          $("#passActual").css("border-color", color);
          $("#passValidar").val(numero);
        }
      },'json');
    }
}
const objeto = new configuracionUser()

export default objeto