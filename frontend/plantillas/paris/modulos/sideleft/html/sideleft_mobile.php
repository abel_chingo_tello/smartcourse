<?php
defined('_BOOT_') or die('');
$verion = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $this->documento->getUrlStatic();

$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
$imgdefecto = $rutastatic . "/media/web/nofoto.jpg";
$imgfoto = $rutastatic . "/media/usuarios/user_avatar.jpg";
$jsonlogin = '{logo1:""}';
$usu = '';
if (!empty($this->usuario))$usu = $this->usuario;
$strrole=array(1=>'Administrator',2=>'Teacher',3=>'Student');
if(!empty($strrole[@$usu["idrol"]])){
  $strrole=ucfirst(JrTexto::_($strrole[@$usu["idrol"]]));
}else
  $strrole=ucfirst(@$usu["rol"]);
?>
<div id="sidebar" class="full-width">
  <div class="sidebar_section_header">
    <div class="sidebar-container-img-header"><img id="sidebar-img-logo" class="img-rounded img-small"
        src="<?php echo $this->logo_emp; ?>" alt=""></div>
    <p class="sidebar-txt-title white"><?php echo $this->nomempresa;?></p>
    <a onclick="closesidebar()" class="btn-close"><span class="icon icon-close"></span></a>
  </div>
  <div class="sidebar_section_middle" style="margin-top:82px;">
    <div class="sidebar-middle-container-top-left">
      <div>
        <p class="miga bg-green-dark txt-rol white"><?php echo @$strrole; ?></p>
        <p class="txt-user"><b><?php echo ucfirst(JrTexto::_('Hello')); ?></b>, <?php echo $usu["nombre_full"]; ?></p>
      </div>
    </div>
    <div class="sidebar-middle-container-top-rigth">
      <div id="svg-sidebar-middle-top"></div>
    </div>
  </div>
  <div class="sidebar_section_menu " style="margin-top:200px;">
    <ul class="sidebar-menu">
      <?php 
      foreach($this->menus as $menu){
        $hayhijos=!empty($menu["espadre"]) && !empty($menu["arrhijos"])?true:false;       
      ?>
      <li class="sidebar-menu-item <?php echo $hayhijos?'item-parent':''; ?>">
        <a <?php echo $hayhijos==false?('href="'.URL_BASE.$menu["link"].'" class=""'):''; ?>  >
          <!--span class="icon icon-graduation"></span-->
          <i class="fa <?php echo trim($menu["icono"])=='fa-chart-line'?'fa-line-chart':$menu["icono"]; ?>"></i> 
          <?php echo JrTexto::_($menu["nombretraducido"]);?></a>
          <?php if($hayhijos){ //var_dump($menu); ?>
            <span class="icon icon-arrow-top icon-toogle"></span>
            <ul class="sidebar-menu-subitem">
              <?php foreach ($menu["arrhijos"] as $key => $hh) { //var_dump($hh);?>
              <li class="sidebar-menu-item"><a href="<?php echo URL_BASE.$hh["link"]; ?>"><i class="fa <?php echo trim($hh["icono"])=='fa-chart-line'?'fa-line-chart':$hh["icono"]; ?>"></i> <?php echo $hh["nombretraducido"]; ?></a></li>                
            
              <?php } ?>                          
            </ul>
          <?php } ?>          
      </li>
      <?php } ?>     
    </ul>
  </div>
</div>