<?php
defined('_BOOT_') or die('');
$verion = !empty(_version_) ? _version_ : '1.0';
$rutastatic = $this->documento->getUrlStatic();

$ismodal = $this->documento->plantilla == "modal" || $this->documento->plantilla == "blanco" ? true : false;
$fcall = !empty($_REQUEST["fcall"]) ? $_REQUEST["fcall"] : "";
$imgdefecto = $rutastatic . "/media/web/nofoto.jpg";
$imgfoto = $rutastatic . "/media/usuarios/user_avatar.jpg";
$jsonlogin = '{logo1:""}';
$usu = '';
if (!empty($this->usuario)) $usu = $this->usuario;
if (!empty($this->proyecto)) {
  $py = $this->proyecto;
  $jsonlogin = $py["jsonlogin"];
}
$frm = !empty($this->datos) ? $this->datos : "";
$fotouser = !empty($frm["foto"]) ? $frm["foto"] : 'static/media/usuarios/user_avatar.jpg';
$ifoto = strripos($fotouser, "static/");
if ($ifoto != '') $fotouser = $this->documento->getUrlBase() . substr($fotouser, $ifoto);
if ($ifoto == '') {
  $ifoto = strripos($fotouser, "/");
  $ifoto2 = strripos($fotouser, ".");
  if ($ifoto != '' && $ifoto2 != '') $fotouser = $this->documento->getUrlBase() . 'static/media/usuarios' . substr($fotouser, $ifoto);
  elseif ($ifoto2 != '') $fotouser = $this->documento->getUrlBase() . 'static/media/usuarios/' . $fotouser;
  else $fotouser = $this->documento->getUrlBase() . 'static/media/usuarios/user_avatar.jpg';
}
$strrole=array(1=>'Administrator',2=>'Teacher',3=>'Student');
if(!empty($strrole[@$usu["idrol"]])){
  $strrole=ucfirst(JrTexto::_($strrole[@$usu["idrol"]]));
}else
  $strrole=ucfirst(@$usu["rol"]);
?>
<!-- mine -->
<style>
  a.d-block {
    width: 90%;
    min-width: 140px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }
</style>
<!-- mine -->
<aside class="main-sidebar elevation-4 _menu_">
  <!-- <aside class="main-sidebar elevation-4 sidebar-light-info"> -->
  <!-- Brand Logo -->
  <a href="./" class="brand-link _logo_">
    <!-- <a href="./" class="brand-link navbar-cyan"> -->
    <img src="<?php echo URL_BASE . $this->logo_emp; ?>" class="brand-image img-circle elevation-1" style="height: 2.1rem; width: 2.1rem; background-color: white;">
    <span class="brand-text font-weight-light"><strong style="font-weight: bold; font-size: 13px; "><?php echo $this->nomempresa; ?></strong></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo $fotouser; ?>" class="img-circle elevation-2 imgUsuActual" style="height: 2.1rem; width: 2.1rem;">
      </div>
      <div class="info">
        <a class="d-block"><?php echo $usu["nombre_full"]; ?></a>
      </div>
    </div>
    <div class="user-panel mt-3 pb-3 mb-3 text-center" style="margin-top: 0 !important;">
      <!-- <div class="user-panel mt-3 pb-3 mb-3 text-center" style="margin-top: 0 !important;"> -->
      <a class="nav-header" style="padding-bottom: 0; margin-bottom: 0;"><b><?php echo ucfirst(JrTexto::_('Role')); ?>:</b> <?php echo @$strrole; ?></a>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <!-- Aquì se inyectan los modulos del sidebar -->
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" id="mostrarmodulos">
        <!-- (yo) -->

      </ul>
      <?php
       if(!empty($this->empresa["Token"]) && !empty($this->empresa["RutOtec"])){?>
      <style type="text/css">
        body.sidebar-mini._texto_.sidebar-collapse .modalsensetmp{
          display: none;
        }
      </style>
      <div >
        <div class="modalsensetmp" style="padding: 1ex;    border: 1px solid #3c8dbc;" >
          <div class="Title" style=" margin: -1ex -1ex 1ex -1ex;  padding: 1ex;    background: #ec7db0ab;    font-weight: 600;    font-size: 1.1em;">Integración Sence</div>
        <div><b>Gestor, aquí los usuarios se integrarán con sence</b></div>
        <div class="alert alert-warning" style="background: #ffc1073d;">
          Debes configurar el codigo Sence del Curso
        </div>     

        <div>Recuerda asignar el ID de Acción en el nombre del grupo  de tus participantes, así : <strong>SENSE-XXXXXX</strong> </div>
        <div><b>No</b> existen grupos de becarios configurados.</div>
        <div><b>No</b> se enviarán correos de alerta en caso de errores. </div>
        <div><b>No</b> se pedirá el cierre de sesión al participante. </div>       
        </div>
      </div>
    <?php } ?>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
<style>
  .my-hide {
    display: none;
  }

  /* #dropdown {
    tr
  } */

  .my-show {
    display: block;
  }

  #mostrarmodulos {
    display: grid;
  }
</style>
<script>
  //info del backend:
  var py = JSON.parse(`<?php echo !empty($jsonlogin)?str_replace('\n','<br>',$jsonlogin):'{}';?>`);
 
  var url_media = '<?php echo URL_MEDIA; ?>';
  var imgdefecto = '<?php echo $imgdefecto; ?>';
  var fotodefecto = '<?php echo $imgfoto ?>';
  var jsonproyecto = py;
  var idproyecto = parseInt('<?php echo $py["idproyecto"]; ?>');
  var idempresa = parseInt('<?php echo $py["idempresa"]; ?>');
  var dtuserlogin = <?php echo json_encode($this->usuario); ?>;
  var idrol = parseInt(dtuserlogin.idrol);
  var tuser = dtuserlogin.tuser;
  //define a qué modulos tendrá acceso (array de modulos):
  _menus_ = py.menus;
  if (py.paris == undefined) {
    py.paris = new Object();
    py.paris.navbar = "navbar-white navbar-light";
    py.paris.texto = "";
    py.paris.menu = "sidebar-dark-primary";
    py.paris.logo = "";
  }

  function guardarJsonLogin() {
    $.post(_sysUrlBase_ + 'json/proyecto/setCampo', {
      'idproyecto': idproyecto,
      'campo': 'jsonlogin',
      'valor': JSON.stringify(py)
    }, function(data, status) {
      if (data.code == 200) {
        Swal.fire('<?php echo JrTexto::_('Success'); ?>', '<?php echo JrTexto::_('System colors, updated')?>', 'success');
      }
    }, 'json');
  }

  function resetJsonLogin() {
    py.paris.navbar = "navbar-white navbar-light";
    py.paris.texto = "";
    py.paris.menu = "sidebar-dark-primary";
    py.paris.logo = "";
    $.post(_sysUrlBase_ + 'json/proyecto/setCampo', {
      'idproyecto': idproyecto,
      'campo': 'jsonlogin',
      'valor': JSON.stringify(py)
    }, function(data, status) {
      if (data.code == 200) {
        location.reload();
      }
    }, 'json');
  }

  function buscarArray(array, key, value, retorno = "boolean") {
    for (i in array) {
      if (array[i][key] == value) {
        if (retorno === "boolean") {
          return true;
        } else if (retorno === "int") {
          return i;
        }
      }
    }
    if (retorno === "boolean") {
      return false;
    } else if (retorno === "int") {
      return -1;
    }
  }

  function mnu_active(url_, link = null, idmodulo = null, isSubMenu = null) {
    if (url_.length > 1) {
      var id = parseInt(window.atob(url_[1]));
      if (idmodulo === null && isSubMenu !== null) {
        var pos = buscarArray(_menus_, "id", id, "int");
        __url__(_menus_[pos]["link"], _menus_[pos]["id"]);
      } else if (parseInt(idmodulo) == id) {
        __url__(link, id, isSubMenu);
      } else {
        if (id == -2) {
          __url__("<?php echo URL_BASE; ?>sesion/cambiarrol", id);
        } else if (id == -3) {
          __url__("<?php echo URL_BASE; ?>personal/perfil", id);
        } else if (id == -4) {
          __url__("<?php echo URL_BASE; ?>proyectoempresa?paris=1", id);
        } else if (id == -5) {
          __url__("<?php echo URL_BASE; ?>proyectoempresa/configurarlogin?paris=1", id);
        } else if (id == -6) {
          __url__("<?php echo URL_BASE; ?>proyectoempresa/configurarmodulos?paris=1", id);
        } else if (id == -7) {
          __url__("<?php echo URL_BASE; ?>mensajes", id);
        }
      }
    } else {
      __url__("<?php echo URL_BASE; ?>proyecto/cursos/", 8);
    }
  }

  var done = false;
  function obtenerMenus(){
    let menus=<?php echo json_encode($this->menus);?>;
    let htmlhijos_=function(rs){
        let htmlorder=``;
        $.each(rs,function(i,v){
          let nombre=v.strmenu||'';
          let strmenu=v.strmenu||'Sin nombre';
          let icono=(v.icono==undefined||v.icono==null||v.icono=='')?'':('<i class="nav-icon fa fas '+(v.icono)+'"></i>');
          if(v.idmenu==null && v.strmenu==null && v.url==null){
            strmenu=v.insertar;
            icono='<i class="nav-icon fa fas  '+v.modificar+'"></i>';
          }
          let imagen=(v.imagen==undefined||v.imagen==null||v.imagen=='')?'':('<img src="'+_sysUrlBase_+v.imagen+'" width="25px" height="25px" >');
          let htmlhijo=(v.hijos==undefined||v.hijos==null)?'':('<ul class="nav nav-treeview">'+htmlhijos_(v.hijos)+'</ul>');
          let iscontainer=((v.idmenu==null)?true:false);
          htmlorder+=`
          <li  class="nav-item ${iscontainer==true?'escontenedor has-treeview':''}" data-id="${v.idmenuproyecto}" data-nombre="${(strmenu||'sin nombre')}" data-icon="${v.icono||''}" data-nombretraducido="${(strmenu||'sin nombre')}">
            <a id="mnu${v.idmenuproyecto}" class="mnus nav-link" style="cursor: pointer;" 
            ${iscontainer==true?'href="#"':`onclick="__url__('${_sysUrlBase_+v.url}','${v.idmenuproyecto}', false)"`}>
              ${imagen+icono}
                <p>${(strmenu||'sin nombre')}
                 ${iscontainer==true?'<i class="right fas fa-angle-left"></i>':''}
                </p>
            </a>
            ${iscontainer==true?htmlhijo:''}
          </li>`;
        })
        return htmlorder;
      }
    let html=htmlhijos_(menus);
  $('#mostrarmodulos').append(html);
    let htmlMenuAnidado = `
        <li class="nav-item has-treeview" data-id="00" data-nombre="Setting" data-icon="fa-cogs" data-nombretraducido="configuracion">
          <a  id="mnu00" href="#!" style="cursor: pointer;" class="mnu nav-link">
            <i class="nav-icon fas fa fa-cogs fa-fw"></i>
            <p>Configuración<i class="right fas fa-angle-left"></i></p>
          </a>
          <ul class="nav nav-treeview">`;
        <?php if($this->usuario["tuser"]=='s'){?>
       htmlMenuAnidado += `
            <li class="nav-item"  data-id="001" data-nombre="Menús" data-icon="fa-list" data-nombretraducido="Menús">
              <a id="mnu001" onclick="__url__('${_sysUrlBase_}menu','001', true)" style="cursor: pointer;"  class="mnus nav-link">
                <i class="nav-icon fas fa fa-list fa-fw"></i>
                  <p><?=JrTexto::_("Menu")?></p>
             </a>
            </li>
            <li class="nav-item"  data-id="002" data-nombre="Menús Proyecto" data-icon="fa-list" data-nombretraducido="Menús">
              <a id="mnu002" onclick="__url__('${_sysUrlBase_}menu_proyecto','002', true)" style="cursor: pointer;"  class="mnus nav-link">
                <i class="nav-icon fas fa fa-list fa-fw"></i>
                  <p><?=JrTexto::_("Company menu")?></p>
             </a>
            </li>`;
      <?php } ?>
       htmlMenuAnidado += `<li class="nav-item"  data-id="003" data-nombre="Categorías" data-icon="fa-cc" data-nombretraducido="Categorías">
              <a id="mnu003" onclick="__url__('${_sysUrlBase_}acad_categorias','003', true)" style="cursor: pointer;"  class="mnus nav-link">
                <i class="nav-icon fas fa fa-cc fa-fw"></i>
                  <p><?=JrTexto::_("Categories")?></p>
             </a>
            </li></ul>
      </li>`;
      <?php if($this->usuario["idrol"]=='1'){?>
      $('#mostrarmodulos').append(htmlMenuAnidado);
      <?php }?>
  }
  $(document).ready(function(ev){
    obtenerMenus();
  })
  function actualizarFotoUsuActual(foto) {
    $(".imgUsuActual").attr("src", _sysUrlBase_ + "static/media/usuarios/" + foto);
  }
</script>