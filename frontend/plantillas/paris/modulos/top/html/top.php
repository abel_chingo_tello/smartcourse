<?php 
defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';
$rutastatic=$this->documento->getUrlStatic();
$idrol = $this->usuario["idrol"];
?>
<form id="frmSvPass">
    <div id="modalSvPass" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><?php echo ucfirst(JrTexto::_('Change password')); ?></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="hidden" name="passValidar" id="passValidar" value="0" required="">
            <div class="row">
                <div class="col-md-6">
                    <p><?php echo ucfirst(JrTexto::_('Current password')); ?>
                        <input type="password" id="passActual" name="passActual" maxlength="50" class="form-control input-sm" placeholder="<?php echo ucfirst(JrTexto::_('Current password')); ?>" required="" autocomplete="off">
                    </p>
                </div>
                <div class="col-md-6">
                    <p><?php echo ucfirst(JrTexto::_('New password')); ?>
                        <input type="password" id="passNueva" name="passNueva" maxlength="50" class="form-control input-sm" placeholder="<?php echo ucfirst(JrTexto::_('New password')); ?>" required="" autocomplete="off">
                    </p>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button id="frmPassCerrar" type="button" class="btn btn-default" data-dismiss="modal"><?php echo ucfirst(JrTexto::_('Close')); ?></button>
            <button type="submit" class="btn btn-primary"><?php echo ucfirst(JrTexto::_('Save')); ?></button>
          </div>

        </div>
      </div>
    </div>
  </form>
<nav class="main-header navbar navbar-expand _navbar_">
<!-- <nav class="main-header navbar navbar-expand navbar-dark navbar-lightblue"> -->
<!-- Left navbar links -->
<style>
#tiempoenplataforma{
  color: white;
  background: #0000004d;
  font-weight: bold;
  border-radius: 30px;
  /* border-radius: 50%; */
}
</style>
<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link asistentetooltip tooltip1" shownexttooltip="tooltip2"  data-widget="pushmenu" href="#" data-toggle="tooltip" data-placement="top" title="<?php echo ucfirst(JrTexto::_('Start Here')); ?>" ><i class="fas fa-bars" ></i></a>
  </li>
</ul>
<div class="form-inline ml-3" style="margin-left: 0 !important;">
  <span id="tiempoenplataforma" class="nav-link"></span>
</div>

<!-- SEARCH FORM -->
<!-- <form class="form-inline ml-3">
  <div class="input-group input-group-sm">
    <input class="form-control form-control-navbar" type="search" placeholder="Buscar" aria-label="Search">
    <div class="input-group-append">
      <button class="btn btn-navbar" type="submit">
        <i class="fas fa-search"></i>
      </button>
    </div>
  </div>
</form> -->

<!-- Right navbar links -->
<ul class="navbar-nav ml-auto">

  <!-- Notifications Dropdown Menu -->
  <li class="nav-item" title="<?php echo JrTexto::_('Minimum requirements'); ?>">
    <a class="nav-link" href="<?php echo $rutastatic ?>/requisitos.pdf" download="<?php echo ucfirst(JrTexto::_('Hardware and Software minimum requirements')); ?>" data-toggle="tooltip" data-placement="top" title="<?php echo ucfirst(JrTexto::_('Hardware and Software minimum requirements')); ?>">
      <i class="fa fa-download"></i>
    </a>
  </li>
  <li class="nav-item dropdown" title="<?php echo JrTexto::_('Change language'); ?>">  
        <?php 
          $idioma  =!empty(@NegSesion::get('idioma','idioma__'))?@NegSesion::get('idioma','idioma__'):'ES';
          ?>
        <a class="nav-link dropdown-toggle cambiaridioma" idioma="<?php echo $idioma;?>" style="cursor: pointer;" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="fa fa-language"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item newcambiaridioma" idioma="ES" href="?idioma=ES"> (ES) <?php echo JrTexto::_('Spanish'); ?></a>
          <a class="dropdown-item newcambiaridioma" idioma="EN" href="?idioma=EN"> (EN) <?php echo JrTexto::_('English');?></a>
          <a class="dropdown-item newcambiaridioma" idioma="PT" href="?idioma=PT"> (PT) <?php echo JrTexto::_('Portuguese');?></a>
        </div>
    <!--a class="nav-link cambiaridioma"  onclick="cambiaridioma()" data-toggle="tooltip" data-placement="top" title="Cambiar Idioma">
      
    </a-->
  </li>
  <?php if($this->usuario["idrol"] == "1" || $this->usuario["idrol"] == "10"){ ?>
    <li class="nav-item" title="<?php echo JrTexto::_('Company Configuration'); ?>">
      <a class="nav-link" style="cursor: pointer;" onclick="__url__('<?php echo URL_BASE . 'proyectoempresa?paris=1'; ?>', -4)" data-toggle="tooltip" data-placement="top" title="Menu <?php echo JrTexto::_('Company Configuration'); ?>"> 
        <i class="fas fa-building"></i>
      </a>
    </li>
    <li class="nav-item" title="<?php echo JrTexto::_('Login Settings'); ?>">
      <a class="nav-link" style="cursor: pointer;" onclick="__url__('<?php echo URL_BASE . 'proyectoempresa/configurarlogin?paris=1'; ?>', -5)"  data-toggle="tooltip" data-placement="top" title="Menu <?php echo JrTexto::_('Login Settings'); ?>">
        <i class="fas fa-laptop"></i>
      </a>
    </li>
    <?php if($this->usuario["tuser"] == "s"){ ?>
    <li class="nav-item" title="<?php echo JrTexto::_('Platform Modules'); ?>">
      <a class="nav-link" style="cursor: pointer;" onclick="__url__('<?php echo URL_BASE . 'proyectoempresa/configurarmodulos?paris=1'; ?>', -6)"  data-toggle="tooltip" data-placement="top" title="Menu <?php echo JrTexto::_('Platform Modules'); ?>">
        <i class="fas fa-th-list"></i>
      </a>
    </li>
    <?php } ?>
    <li class="nav-item"  title="<?php echo JrTexto::_('Platform Colors'); ?>">
      <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" data-toggle="tooltip" data-placement="top" title="Menu <?php echo JrTexto::_('Platform Colors'); ?>">
        <i class="fas fa-charging-station"></i>
      </a>
    </li>
  <?php } ?>
  <li class="nav-item dropdown"  title="<?php echo JrTexto::_('User Options'); ?>">
    <a class="nav-link" data-toggle="dropdown" href="#">
      <i class="fa fa-cogs"></i>
      <!-- <span class="badge badge-warning navbar-badge">15</span> -->
    </a>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
    <?php 
      if(count($this->usuario['roles']) > 1 || $this->usuario["tuser"]=='s'){ 
    ?>
        <a class="dropdown-item" style="cursor: pointer;" onclick="__url__('<?php echo URL_BASE . 'sesion/cambiarrol'; ?>', -2)">
          <i class="mr-2 fa fa-cubes fa-fw"></i> <?php echo ucfirst(JrTexto::_("Change rol"));?>
        </a>
    <?php 
      } 
    ?>
      <a class="dropdown-item" style="cursor: pointer;" onclick="__url__('<?php echo URL_BASE . 'personal/perfil'; ?>', -3)">
        <i class="mr-2 fa fa-address-book fa-fw"></i> <?php echo ucfirst(JrTexto::_("My profile"));?>
      </a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" style="cursor: pointer;" id="aSvPass" data-toggle="modal" data-target="#modalSvPass">
        <i class="mr-2 fa fa-unlock-alt fa-fw"></i> <?php echo ucfirst(JrTexto::_("Change Password"));?>
      </a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" style="cursor: pointer;" onclick="cerrarSesion();">
        <i class="mr-2 fa fa-power-off fa-fw"></i> <?php echo ucfirst(JrTexto::_("Sign off"));?>
      </a>
    </div>
  </li>
</ul>
</nav>
<script type="text/javascript">

<?php 
if($idrol==3){
?>
  var _con_ = {horas: 0, minutos: 20, segundos: 0};
  localStorage.setItem("conteo", JSON.stringify(_con_));
  localStorage.setItem("conteoInicial", JSON.stringify(_con_));
<?php
} else if($idrol==2){
?>
  var _con_ = {horas: 0, minutos: 40, segundos: 0};
  localStorage.setItem("conteo", JSON.stringify(_con_));
  localStorage.setItem("conteoInicial", JSON.stringify(_con_));
<?php
} else{
?>
  $("#tiempoenplataforma").parent().hide();
  var conteo = {horas: 0, minutos: 0, segundos: 0};
<?php
}
?>

var cronometro;

function cronometro_() {
  var conteo = JSON.parse(localStorage.getItem("conteo"));
  var text = '';
  var tiempo = (conteo.horas * 60 * 60) + (conteo.minutos * 60) + conteo.segundos;
  if (tiempo > 0) {
    tiempo--;
    conteo = {
      horas: Math.floor(tiempo / 3600 % 24), 
      minutos: Math.floor(tiempo / 60 % 60), 
      segundos: Math.floor(tiempo % 60)
    }
    text += ('0' + conteo.horas).slice(-2) + ":";
    text += ('0' + conteo.minutos).slice(-2) + ":";
    text += ('0' + conteo.segundos).slice(-2);
    $("#tiempoenplataforma").html(text);
    localStorage.setItem("conteo", JSON.stringify(conteo));
    cronometro = setTimeout('cronometro_()', 1000);
  } else {
    Swal.fire('<?php echo JrTexto::_('Warning'); ?>', '<?php echo JrTexto::_('Timeout'); ?>', 'warning').then((result) => {
        cerrarSesion();
    });
  }
}

<?php 
if($idrol==3 || $idrol==2){
?>
cronometro_();
$(window).mousemove(function(){
  localStorage.setItem("conteo", localStorage.getItem("conteoInicial"));
});
// var body = document.getElementsByTagName("body")[0];
// body.addEventListener('mousemove', function(){
//   localStorage.setItem("conteo", localStorage.getItem("conteoInicial"));
// });
<?php
}
?>
function agregarFuncionCronometro(){
  <?php 
  if($idrol==3 || $idrol==2){
  ?>
  var html_ = "";
    // html_ += $('iframe').contents().find("body").html();
    html_ += '<script>';
    html_ += 'var body = document.getElementsByTagName("body")[0];';
    html_ += 'body.addEventListener("mousemove", function(){';
    html_ += 'localStorage.setItem("conteo", localStorage.getItem("conteoInicial"));';
    html_ += '});';
    // html_ += '$(window).mousemove(function(){';
    // html_ += '  localStorage.setItem("conteo", localStorage.getItem("conteoInicial"))';
    // html_ += '})';
    html_ += '<\/script>';
    // $('iframe').contents().find("body").html(html_);
    $('iframe').contents().find("body").append(html_);
  <?php
  }
  ?>
}

$("#aSvPass").click(function(){
  $("#passValidar").val("0");
  $("#passActual").val("").removeAttr("style");
  $("#passNueva").val("");
  $("#frmSvPass").on("shown.bs.modal", function () {
      $("#passActual").focus();
  });
});
$("#frmSvPass").submit(function (evento) {
    evento.preventDefault();
    if($("#passValidar").val() == "0"){
      Swal.fire('<?php echo JrTexto::_('Warning'); ?>','<?php echo JrTexto::_('Incorrect current password'); ?>','danger');
    }else{
      Swal.fire({
          title: '<?php echo JrTexto::_('Confirme');?>',
          text: '<?php echo JrTexto::_('Are you sure to change your password?'); ?>',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: '<?php echo JrTexto::_('Accept');?>',
          cancelButtonText: '<?php echo JrTexto::_('Cancel');?>',
          }).then((result) => {
              if (result.value) {            
                $.post(_sysUrlBase_ + 'json/persona/changePassAct',
                  {p_datos: $("#frmSvPass").serialize()},
                  function(data, status) {
                    if (data.code === 200) {
                      Swal.fire('<?php echo JrTexto::_('Success');?>','<?php echo ucfirst(JrTexto::_('Password changed')); ?>','success');
                      $("#frmPassCerrar").click();
                    }
                  }, 'json');
              }
          });
    }
});
$("#passActual").keyup(function(){
  $.post(_sysUrlBase_ + "json/persona/listado",{sql4: 1, clave: $(this).val()},function(data,status){
        if(data.code == 200){
          var numero = data.data[0]["numero"];
          var color = "green";
          if(numero == "0"){
            color = "red";
          }
          $("#passActual").attr("style", "border-color: " + color + ";");
          $("#passValidar").val(numero);
        }
      },'json');
});

function cambiaridioma(){
  $.post(_sysUrlBase_+'idioma/cambiar',{idioma:$('.cambiaridioma').attr('idioma')} ,function(data, status){
  if(data.code==200){
    window.top.location.reload();
  }
  },'json');
}


function cerrarSesion(){
  $.post(_sysUrlBase_+'json/sesion/salir', function(data, status){
    if(data.code==200){  window.location.href=_sysUrlSitio_; }},'json');
}
</script>