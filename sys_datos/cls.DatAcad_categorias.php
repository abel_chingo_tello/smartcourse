<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatAcad_categorias extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT 	idcategoria,nombre,descripcion,imagen,
							idpadre,estado,orden,idproyecto 
					FROM acad_categorias";


			$cond = array();
			if (isset($filtros["idcategoria"])) {
				$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["imagen"])) {
				$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if (isset($filtros["idpadre"])) {
				$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else $cond[] = "estado >= 0 ";


			if (isset($filtros["orden"])) {
				$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY orden ASC";

			//var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}
	/**
	 * Metodo para buscar en la tabla con la maxima información posible
	 * @param Array Filtros de la busqueda
	 * @return Array resultado de la busqueda
	 */
	public function buscarcompleto($filtros = null)
	{
		try {
			$sql = "SELECT 
						ac.idcategoria , ac.nombre, ac.idpadre,ac.idproyecto,
						IF(ac.idpadre <> 0,(SELECT nombre FROM acad_categorias WHERE idcategoria = ac.idpadre),NULL) AS carrera 
					FROM acad_categorias ac";


			$cond = array();
			if (isset($filtros["idcategoria"])) {
				$cond[] = "ac.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "ac.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "ac.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "ac.descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["imagen"])) {
				$cond[] = "ac.imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if (isset($filtros["idpadre"])) {
				$cond[] = "ac.idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "ac.estado = " . $this->oBD->escapar($filtros["estado"]);
			}else $cond[] = "ac.estado >= 0 ";


			if (isset($filtros["orden"])) {
				$cond[] = "ac.orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "ac.nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY orden ASC";

			//var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}

	public function buscarxIDs($filtros = null)
	{
		try {
			$sql = "SELECT idcategoria,nombre,descripcion,imagen,idpadre,estado,orden,idproyecto FROM acad_categorias";

			$cond = array();


			if (isset($filtros["idcategoria"])) {
				if (is_array($filtros["idcategoria"])) {
					$cond[] = "idcategoria IN (" . implode(',', $filtros["idcategoria"]) . ")";
				} else {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
				}
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}

	public function getCursoCategoria($filtros)
	{
		try {
			$sql = "SELECT id,idcategoria,idcurso,idproy
					FROM acad_cursocategoria";

			$cond = array();


			if (isset($filtros["idcategoria"])) {
				if (is_array($filtros["idcategoria"])) {
					$cond[] = "idcategoria IN (" . implode(',', $filtros["idcategoria"]) . ")";
				} else {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
				}
			}
			if (isset($filtros["id"])) {
				$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproy = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idproy"])) {
				$cond[] = "idproy = " . $this->oBD->escapar($filtros["idproy"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}
	public function insertar($nombre, $descripcion, $imagen, $idpadre, $estado, $orden, $idproyecto = 0)
	{
		try {

			$this->iniciarTransaccion('dat_acad_categorias_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT idcategoria FROM acad_categorias ORDER BY idcategoria DESC LIMIT 0,1");
			++$id;

			$estados = array(
				'idcategoria' => $id, 'nombre' => $nombre, 'descripcion' => $descripcion, 'imagen' => $imagen, 'idpadre' => !empty($idpadre) ? $idpadre : 0, 'estado' => !isset($estado) ? $estado : 1, 'orden' => $orden, 'idproyecto' => $idproyecto
			);

			$this->oBD->insert('acad_categorias', $estados);
			$this->terminarTransaccion('dat_acad_categorias_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_acad_categorias_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre, $descripcion, $imagen, $idpadre, $estado, $orden, $idproyecto = 0)
	{
		try {
			$this->iniciarTransaccion('dat_acad_categorias_update');
			$estados = array(
				'nombre' => $nombre, 'descripcion' => $descripcion, 'imagen' => $imagen, 'idpadre' => !empty($idpadre) ? $idpadre : 0, 'estado' => !isset($estado) ? $estado : 1, 'orden' => $orden, 'idproyecto' => $idproyecto
			);

			$this->oBD->update('acad_categorias ', $estados, array('idcategoria' => $id));
			$this->terminarTransaccion('dat_acad_categorias_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idcategoria,nombre,descripcion,imagen,idpadre,estado,orden,idproyecto  FROM acad_categorias  "
				. " WHERE idcategoria = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_categorias', array('idcategoria' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('acad_categorias', array($propiedad => $valor), array('idcategoria' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_categorias") . ": " . $e->getMessage());
		}
	}
}
