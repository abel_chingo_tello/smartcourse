<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-11-2020  
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
class DatMenu_proyecto extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Menu") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idmenuproyecto,me.idmenu,nombre as strmenu,url,icono,descripcion ,idproyecto ,idrol ,orden ,idpadre ,insertar ,modificar ,eliminar ,mp.usuario_registro ,mp.fecha_registro FROM menu_proyecto mp LEFT JOIN menu me on	mp.idmenu=me.idmenu";
			$cond = array();
			
			if(isset($filtros["idmenuproyecto"])) {
					$cond[] = "idmenuproyecto = " . $this->oBD->escapar($filtros["idmenuproyecto"]);
			}
			if(isset($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idmenu"])) {
					$cond[] = "mp.idmenu = " . $this->oBD->escapar($filtros["idmenu"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "( estado >=0 OR estado IS NULL )";}
			
			if(isset($filtros["url"])) {
					$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if(isset($filtros["icono"])) {
					$cond[] = "icono = " . $this->oBD->escapar($filtros["icono"]);
			}
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "mp.usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "mp.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}

			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM menu";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			$sql .= " ORDER BY idpadre,orden ASC";	
			//echo $sql;		
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Menu proyecto").": " . $e->getMessage());
		}
	}

	public function insertar($idmenu,$idproyecto,$idrol,$orden,$idpadre,$insertar,$modificar,$eliminar,$usuario_registro)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idmodulo+1 FROM modulos ORDER BY idmodulo DESC limit 0,1 ");			
			$estados = array('idmenu'=>!empty($idmenu)?$idmenu:'null'
							,'idproyecto'=>$idproyecto
							,'idrol'=>$idrol							
							,'orden'=>!empty($orden)?$orden:1
							,'idpadre'=>!empty($idpadre)?$idpadre:'null'
							,'insertar'=>$insertar
							,'modificar'=>$modificar
							,'eliminar'=>$eliminar
							,'usuario_registro'=>$usuario_registro							
							);
			return $this->oBD->insert('menu_proyecto', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_modulos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Menu proyecto").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idmenu,$idproyecto,$idrol,$orden,$idpadre,$insertar,$modificar,$eliminar,$usuario_registro)
	{
		try {			
			$estados = array('idmenu'=>!empty($idmenu)?$idmenu:'null'
							,'idproyecto'=>$idproyecto
							,'idrol'=>$idrol							
							,'orden'=>!empty($orden)?$orden:1
							,'idpadre'=>!empty($idpadre)?$idpadre:'null'
							,'insertar'=>$insertar
							,'modificar'=>$modificar
							,'eliminar'=>$eliminar
							,'usuario_registro'=>$usuario_registro
							);
			$this->oBD->update('menu_proyecto ', $estados, array('idmenuproyecto' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Menu proyecto").": " . $e->getMessage());
		}
	}

	public function eliminar($id,$deBD=false)
	{
		try {		
			return $this->oBD->delete('menu_proyecto', array('idmenuproyecto' => $id));			
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("menu_proyecto").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('menu_proyecto', array($propiedad => $valor), array('idmenuproyecto' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update set") . " " . JrTexto::_("Menu proyecto") . ": " . $e->getMessage());
		}
	}
	
	public function guardarorden($datos)
	{
		try {
			$data=json_decode($datos,true);
			foreach ($data as $key => $va){	
				$estados=array('orden' =>$va["orden"],'idpadre'=>!empty($va["idpadre"])?$va["idpadre"]:'null');
				$this->oBD->update('menu_proyecto', $estados, array('idmenuproyecto' => $va["idmenuproyecto"]));				
			}	
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("menu_proyecto").": " . $e->getMessage());
		}
	}

}
