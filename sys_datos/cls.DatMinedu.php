<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-01-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */

class DatMinedu extends DatBase
{
    public function __construct()
    {
        try {
            parent::conectar();
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Minedu") . ": " . $e->getMessage());
        }
    }
    public function buscarubigeo($filtros = null)
    {
        try {

            $sql = "SELECT U.id_ubigeo, U.pais, U.departamento, U.provincia, U.distrito, U.ciudad FROM ubigeo U";

            $cond = array();
            if (isset($filtros["id_ubigeo"])) {
                $cond[] = "U.id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
            }
            if (isset($filtros["provincia"])) {
                $cond[] = "U.provincia = " . $this->oBD->escapar($filtros["provincia"]);
            }
            if (isset($filtros["distrito"])) {
                $cond[] = "U.distrito = " . $this->oBD->escapar($filtros["distrito"]);
            }
            if (isset($filtros["diferente_provincia"])) {
                $cond[] = "U.provincia <> " . $this->oBD->escapar($filtros["diferente_provincia"]);
            }
            if (isset($filtros["diferente_distrito"])) {
                $cond[] = "U.distrito <> " . $this->oBD->escapar($filtros["diferente_distrito"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function buscarubigeougel($filtros)
    {
        try {

            $sql = "SELECT ubigeo.id_ubigeo,ubigeo.ciudad,ugel.idugel,ugel.descripcion,ugel.iddepartamento FROM ubigeo INNER JOIN ugel ON ubigeo.id_ubigeo = ugel.iddepartamento";

            $cond = array();
            if (isset($filtros["iddepartamento"])) {
                $cond[] = "ugel.iddepartamento = " . $this->oBD->escapar($filtros["iddepartamento"]);
            }
            if (isset($filtros["idugel"])) {
                $cond[] = "ugel.idugel = " . $this->oBD->escapar($filtros["idugel"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            $sql .= " ORDER BY ubigeo.ciudad ASC";
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function competencias($filtros)
    {
        try {
            $sql = "SELECT c.id, c.nombre, c.descripcion, c.tipo, c.idpadre, c.habilidad FROM competencia c";
            $cond = array();
            if (isset($filtros["tipo"])) {
                $cond[] = "c.tipo = " . $this->oBD->escapar($filtros["tipo"]);
            }
            if (isset($filtros["id"])) {
                $cond[] = "c.id = " . $this->oBD->escapar($filtros["id"]);
            }
            if (isset($filtros["idpadre"])) {
                $cond[] = "c.idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
            }
            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function buscartiempos($filtros = null)
    {
        try {
            $sql = "SELECT p.nombre,am.idalumno,gd.idlocal,gd.idgrado,gd.idsesion,(SELECT sum(TIME_TO_SEC(TIMEDIFF(fechasalida, fechaentrada))) as tiempo FROM historial_sesion where idusuario = am.idalumno AND lugar = 'P') AS tiempopv, (SELECT sum(TIME_TO_SEC(TIMEDIFF(fechasalida, fechaentrada))) as tiempo FROM historial_sesion where idusuario = am.idalumno AND lugar = 'E' AND (idcurso=gd.idcurso OR idcurso is NULL )) AS smartquiz, (SELECT sum(TIME_TO_SEC(TIMEDIFF(fechasalida, fechaentrada))) as tiempo FROM historial_sesion where idusuario = am.idalumno AND lugar = 'TR' AND (idcurso=gd.idcurso OR idcurso is NULL )) AS smartbook, (SELECT sum(TIME_TO_SEC(TIMEDIFF(fechasalida, fechaentrada))) as tiempo FROM historial_sesion where idusuario = am.idalumno AND lugar = 'T' AND (idcurso=gd.idcurso OR idcurso is NULL )) AS homework, (SELECT sum(TIME_TO_SEC(TIMEDIFF(fechasalida, fechaentrada))) as tiempo FROM historial_sesion where idusuario = am.idalumno AND lugar = 'A' AND (idcurso=gd.idcurso OR idcurso is NULL)) AS activity FROM acad_matricula am INNER JOIN acad_grupoauladetalle gd ON am.idgrupoauladetalle = gd.idgrupoauladetalle inner join personal p on am.idalumno = p.idpersona";

            $cond = array();
            if (isset($filtros["idgrupoauladetalle"])) {
                $cond[] = "gd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
            }
            if (isset($filtros["idlocal"])) {
                $cond[] = "gd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
            }
            if (isset($filtros["idalumno"])) {
                $cond[] = "am.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
            }
            if (isset($filtros["idcurso"])) {
                $cond[] = "gd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
            }
            if (isset($filtros["idgrado"])) {
                $cond[] = "gd.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
            }
            if (isset($filtros["idsesion"])) {
                $cond[] = "gd.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
            }
            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            $sql .= " ORDER BY am.idalumno ASC";

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function buscariiee($filtros = null)
    {
        try {

            $sql = "SELECT L.idlocal, L.nombre, L.direccion, L.id_ubigeo, L.tipo, L.vacantes, L.idugel, L.idproyecto FROM local L INNER JOIN UGEL U ON L.idugel = U.idugel";
            if (isset($filtros["SQL_sinugel"])) {
                $sql = "SELECT L.* FROM local L";
            }

            $cond = array();
            if (isset($filtros["id_ubigeo"])) {
                $cond[] = "L.id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
            }
            if (isset($filtros["like_id_ubigeo"])) {
                $cond[] = "L.id_ubigeo like '{$filtros["like_id_ubigeo"]}%'";
            }
            if (isset($filtros["tipo"])) {
                $cond[] = "L.tipo = " . $this->oBD->escapar($filtros["tipo"]);
            }
            if (isset($filtros["idproyecto"])) {
                $cond[] = "L.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
            }
            if (isset($filtros["idugel"])) {
                $cond[] = "L.idugel = " . $this->oBD->escapar($filtros["idugel"]);
            }
            if (isset($filtros["idlocal"])) {
                $cond[] = "L.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
            }
            if (isset($filtros["id_ubigeo_departamento"])) {
                $cond[] = "L.idlocal like '" . $this->oBD->escapar($filtros["idlocal"]) . "%'";
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function GetnumRow()
    {
        try {
            $sql = "SELECT count(idlocal) AS total FROM local;";
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function progresosresumen($filtros = null)
    {
        try {
            $sql = "SELECT r.idresumen, r.idalumno, r.idlocal, r.fecha_modificacion, r.prog_curso_A1, r.prog_hab_L_A1, r.prog_hab_R_A1, r.prog_hab_W_A1, r.prog_hab_S_A1, r.prog_curso_A2, r.prog_hab_L_A2, r.prog_hab_R_A2, r.prog_hab_W_A2, r.prog_hab_S_A2, r.prog_curso_B1, r.prog_hab_L_B1, r.prog_hab_R_B1, r.prog_hab_W_B1, r.prog_hab_S_B1, r.prog_curso_B2, r.prog_hab_L_B2, r.prog_hab_R_B2, r.prog_hab_W_B2, r.prog_hab_S_B2, r.prog_curso_C1, r.prog_hab_L_C1, r.prog_hab_R_C1, r.prog_hab_W_C1, r.prog_hab_S_C1 FROM reportes_resumen r";
            $cond = array();
            if (isset($filtros["idalumno"])) {
                $cond[] = "r.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function buscardocentes($filtros = null)
    {
        try {
            $sql = 'SELECT gad.idgrupoauladetalle, gad.idgrupoaula, gad.idcurso, gad.iddocente, gad.idlocal, gad.idambiente, gad.nombre, gad.fecha_inicio, gad.fecha_final, gad.idgrado, gad.idsesion, gad.fechamodificacion, gad.idcomplementario, gad.orden,(select CONCAT(pe.nombre," ",pe.ape_paterno)) as nombredocente FROM acad_grupoauladetalle gad INNER JOIN personal pe ON gad.iddocente = pe.idpersona';

            $cond = array();

            if (isset($filtros["idlocal"])) {
                $cond[] = "gad.idlocal =" . $this->oBD->escapar($filtros["idlocal"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function buscarentradasalida($filtros = null)
    {
        try {
            $sql = "SELECT pe.idugel,(select gad.idgrado from acad_matricula ma INNER JOIN acad_grupoauladetalle gad ON gad.idgrupoauladetalle = ma.idgrupoauladetalle WHERE ma.idalumno = nq.idalumno LIMIT 1) AS idgrado,c.idcurso,nq.idrecurso,nq.idalumno,nq.nota, nq.habilidad_puntaje, (SELECT nota FROM notas_quiz WHERE idalumno = nq.idalumno AND datos like '%\"tipo\":\"S\"%') AS nota_salida, (SELECT habilidad_puntaje FROM notas_quiz WHERE idalumno = nq.idalumno AND datos like '%\"tipo\":\"S\"%') AS habilidades_salida FROM notas_quiz nq INNER JOIN acad_cursodetalle cd ON cd.idrecurso=nq.idrecurso INNER JOIN acad_curso c ON c.idcurso = cd.idcurso INNER JOIN personal pe ON pe.idpersona = nq.idalumno";
            $cond = array();

            if (isset($filtros["tipo"])) {
                $cond[] = "nq.tipo = " . $this->oBD->escapar($filtros["tipo"]);
            }
            if (isset($filtros["estado"])) {
                $cond[] = "c.estado = " . $this->oBD->escapar($filtros["estado"]);
            }
            $cond[] = "nq.datos like '%\"tipo\":\"E\"%'";

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function buscarbimestre($filtros = null)
    {
        try {
            $sql = "SELECT per.ubigeo,per.idugel,per.idlocal,(SELECT r.rol FROM  roles r WHERE per.rol = r.idrol LIMIT 1) AS persona
            ,(select gad.idgrado from notas_quiz nq inner join acad_matricula ma on ma.idalumno = nq.idalumno inner join acad_grupoauladetalle gad on gad.idgrupoauladetalle = ma.idgrupoauladetalle where gad.idcurso = _curso and nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"1\"%'  and nq.idrecurso = _id1 ) AS idgrado,
            (select nq.nota from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"1\"%' and nq.idrecurso = _id1) AS notaBi01,
            (select nq.nota from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"2\"%' and nq.idrecurso = _id2) AS notaBi02,
            (select nq.nota from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"3\"%' and nq.idrecurso = _id3) AS notaBi03,
            (select nq.nota from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"4\"%' and nq.idrecurso = _id4) AS notaBi04,
            (select nq.habilidad_puntaje from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"1\"%' and nq.idrecurso = _id1) AS habilidadBi01,
            (select nq.habilidad_puntaje from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"2\"%' and nq.idrecurso = _id2) AS habilidadBi02, 
            (select nq.habilidad_puntaje from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"3\"%' and nq.idrecurso = _id3) AS habilidadBi03, 
            (select nq.habilidad_puntaje from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"B\"%' and nq.datos like '%\"num_examen\":\"4\"%' and nq.idrecurso = _id4) AS habilidadBi04 from personal per";

            $cond = array();

            if (isset($filtros["rol"])) {
                $cond[] = "per.rol = " . $this->oBD->escapar($filtros["rol"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            $filtros['recurso1'] = (isset($filtros['recurso1'])) ? $filtros['recurso1'] : 0;
            $filtros['recurso2'] = (isset($filtros['recurso2'])) ? $filtros['recurso2'] : 0;
            $filtros['recurso3'] = (isset($filtros['recurso3'])) ? $filtros['recurso3'] : 0;
            $filtros['recurso4'] = (isset($filtros['recurso4'])) ? $filtros['recurso4'] : 0;

            $sql = str_replace("_curso", $filtros['idcurso'], $sql);
            $sql = str_replace("_id1", $filtros['recurso1'], $sql);
            $sql = str_replace("_id2", $filtros['recurso2'], $sql);
            $sql = str_replace("_id3", $filtros['recurso3'], $sql);
            $sql = str_replace("_id4", $filtros['recurso4'], $sql);

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    // public function countseccionxgrado(){
    //     try{
    //         $sql= "select distinct sec.*, ( SELECT count(*) from acad_matricula ma2 inner join acad_grupoauladetalle gad2 on ma2.idgrupoauladetalle = gad2.idgrupoauladetalle where gad.idcurso = gad2.idcurso and gad.idgrado = gad2.idgrado and gad.idlocal = gad2.idlocal and gad2.idsesion = sec.idsesion) AS total FROM min_sesion sec INNER JOIN acad_grupoauladetalle gad ON gad.idsesion = sec.idsesion inner join acad_matricula ma ON gad.idgrupoauladetalle = ma.idgrupoauladetalle";



    //         return $this->oBD->consultarSQL($sql);
    //     }catch(Exception $e){
    //         throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Personal").": ". $e->getMessage());
    //     }
    // }
    public function notasUbicacionxSeccion($filtros = null)
    {
        try {
            $sql = "select (SELECT nq.idnota FROM notas_quiz nq where nq.idalumno = ma.idalumno and nq.idrecurso = 326 and nq.tipo = 'U') as notaexamen, ma.* from acad_grupoauladetalle gad inner join acad_matricula ma on gad.idgrupoauladetalle = ma.idgrupoauladetalle";

            $cond = array();

            if (isset($filtros["idlocal"])) {
                $cond[] = "gad.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
            }
            if (isset($filtros["idcurso"])) {
                $cond[] = "gad.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
            }
            if (isset($filtros["idgrado"])) {
                $cond[] = "gad.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
            }
            if (isset($filtros["idsesion"])) {
                $cond[] = "gad.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
            }
            if (isset($filtros["iddocente"])) {
                $cond[] = "gad.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
            }

            if (isset($filtros["idgrupoauladetalle"])) {
                $cond[] = "gad.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function buscartrimestre($filtros = null)
    {
        try {
            $sql = "SELECT per.ubigeo,per.idugel,per.idlocal,(SELECT r.rol FROM  roles r WHERE per.rol = r.idrol LIMIT 1) AS persona
            ,(select gad.idgrado from notas_quiz nq inner join acad_matricula ma on ma.idalumno = nq.idalumno inner join acad_grupoauladetalle gad on gad.idgrupoauladetalle = ma.idgrupoauladetalle where gad.idcurso = _curso and nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"T\"%' and nq.datos like '%\"num_examen\":\"1\"%'  and nq.idrecurso = _id1 ) AS idgrado,
            (select nq.nota from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"T\"%' and nq.datos like '%\"num_examen\":\"1\"%' and nq.idrecurso = _id1) AS notaBi01,
            (select nq.nota from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"T\"%' and nq.datos like '%\"num_examen\":\"2\"%' and nq.idrecurso = _id2) AS notaBi02,
            (select nq.nota from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"T\"%' and nq.datos like '%\"num_examen\":\"3\"%' and nq.idrecurso = _id3) AS notaBi03,
            (select nq.habilidad_puntaje from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"T\"%' and nq.datos like '%\"num_examen\":\"1\"%' and nq.idrecurso = _id1) AS habilidadBi01,
            (select nq.habilidad_puntaje from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"T\"%' and nq.datos like '%\"num_examen\":\"2\"%' and nq.idrecurso = _id2) AS habilidadBi02, 
            (select nq.habilidad_puntaje from notas_quiz nq where nq.idalumno = per.idpersona and nq.datos like '%\"tipo\":\"T\"%' and nq.datos like '%\"num_examen\":\"3\"%' and nq.idrecurso = _id3) AS habilidadBi03 
             from personal per";

            $cond = array();

            if (isset($filtros["rol"])) {
                $cond[] = "per.rol = " . $this->oBD->escapar($filtros["rol"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            $filtros['recurso1'] = (isset($filtros['recurso1'])) ? $filtros['recurso1'] : 0;
            $filtros['recurso2'] = (isset($filtros['recurso2'])) ? $filtros['recurso2'] : 0;
            $filtros['recurso3'] = (isset($filtros['recurso3'])) ? $filtros['recurso3'] : 0;

            $sql = str_replace("_curso", $filtros['idcurso'], $sql);
            $sql = str_replace("_id1", $filtros['recurso1'], $sql);
            $sql = str_replace("_id2", $filtros['recurso2'], $sql);
            $sql = str_replace("_id3", $filtros['recurso3'], $sql);

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function recursos($cursos = null, $filtros = null)
    {
        try {
            $sql = "SELECT cd.idcursodetalle, cd.idcurso, cd.orden, cd.idrecurso, cd.tiporecurso, cd.idlogro, cd.url, cd.idpadre, cd.color, cd.esfinal, cd.txtjson, cd.espadre FROM acad_cursodetalle cd ";
            $cond = array();


            if (isset($filtros['tiporecurso'])) {
                $cond[] = "cd.tiporecurso = " . $this->oBD->escapar($filtros['tiporecurso']) . " ";
            } else {
                $cond[] = "cd.tiporecurso ='E' ";
            }

            if (isset($filtros['trimestre'])) {
                $cond[] = "txtjson LIKE '%\"tipo\":\"T\"%' ";
            }
            if (isset($filtros['bimestre'])) {
                $cond[] = "txtjson LIKE '%\"tipo\":\"B\"%' ";
            }
            if (isset($filtros['entrada'])) {
                $cond[] = "txtjson LIKE '%\"tipo\":\"E\"%' ";
            }
            if (isset($filtros['salida'])) {
                $cond[] = "txtjson LIKE '%\"tipo\":\"S\"%' ";
            }

            if (!empty($cond)) {
                $sql .= " WHERE  " . implode(' AND ', $cond);
            }

            $sql .= " AND cd.idcurso";
            if (is_null($cursos)) {
                $sql .= " IN (31,35,36,4,5)";
            } else {
                $generate = (is_array($cursos)) ? implode(",", $cursos) : $cursos;
                $sql .= " IN ({$generate})";
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function cantsesiones($filtros = null)
    {
        try {
            $sql = "SELECT (SELECT count(idcursodetalle) from acad_cursodetalle cd2 where cd2.idpadre = cd.idcursodetalle ) AS cantidadsesiones from acad_cursodetalle cd";
            $cond = array();

            if (isset($filtros["tiporecurso"])) {
                $cond[] = "cd.tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
            }
            if (isset($filtros["idcurso"])) {
                $cond[] = "cd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    /**
     * 
     * SELECT * from acad_matricula m inner join acad_grupoauladetalle gad ON gad.idgrupoauladetalle = m.idgrupoauladetalle INNER JOIN local l ON l.idlocal = gad.idlocal
     *where l.idlocal = 44;
     *SELECT * FROM actividades a INNER JOIN acad_cursodetalle cd ON cd.idrecurso = a.unidad WHERE cd.idcurso = 31 AND a.metodologia >= 2 AND cd.tiporecurso = 'U';
     *SELECT *, (SELECT SUM(aa.porcentajeprogreso) FROM actividad_alumno aa WHERE ad2.iddetalle = aa.iddetalleactividad AND aa.idalumno = aa2.idalumno) FROM actividad_detalle ad2 INNER JOIN actividad_alumno aa2 ON ad2.iddetalle = aa2.iddetalleactividad   WHERE ad2.idactividad = 14 AND aa2.idalumno = 1
     * 
     */
    public function progresohabilidad($filtros = null)
    {
        try {
            // $sql="SELECT *, (SELECT SUM(aa.porcentajeprogreso) FROM actividad_alumno aa WHERE ad2.iddetalle = aa.iddetalleactividad AND aa.idalumno = aa2.idalumno) as progreso FROM actividad_detalle ad2 INNER JOIN actividad_alumno aa2 ON ad2.iddetalle = aa2.iddetalleactividad";
            $sql = "SELECT aa2.idalumno,aa2.habilidades,aa2.porcentajeprogreso FROM actividad_detalle ad2 INNER JOIN actividad_alumno aa2 ON ad2.iddetalle = aa2.iddetalleactividad";

            $cond = array();
            if (isset($filtros["idactividad"])) {
                $cond[] = "ad2.idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
            }
            if (isset($filtros["idalumno"])) {
                $cond[] = "aa2.idalumno = " . $this->oBD->escapar($filtros["idactividad"]);
            }


            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function actividades($filtros = null)
    {
        try {
            $sql = "SELECT a.idactividad, a.nivel, a.unidad, a.sesion, a.metodologia, a.habilidad, a.titulo, a.descripcion, a.estado, a.url, a.idioma, a.idpersonal FROM actividades a INNER JOIN acad_cursodetalle cd ON cd.idrecurso = a.unidad";

            $cond = array();
            if (isset($filtros["idcurso"])) {
                $cond[] = "cd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
            }
            $cond[] = "a.metodologia >= 2 ";
            $cond[] = "cd.tiporecurso = 'U' ";

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function countmatriculalocal($filtros = null)
    {
        try {
            $sql = "SELECT count(idmatricula) AS total FROM acad_matricula ma inner join acad_grupoauladetalle gad ON gad.idgrupoauladetalle = ma.idgrupoauladetalle";

            $cond = array();
            if (isset($filtros["idlocal"])) {
                $cond[] = "gad.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
            }
            if (isset($filtros["idcurso"])) {
                $cond[] = "gad.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }
    public function matriculalocal($filtros = null)
    {
        try {
            $sql = "SELECT m.idmatricula, m.idgrupoauladetalle, m.idalumno, m.fecha_registro, m.estado, m.idusuario, m.fecha_matricula, m.fecha_vencimiento, m.notafinal, m.formulajson, m.fecha_termino, m.pagorecurso, m.tipomatricula, m.idcomplementario, m.idcategoria,
             gad.idgrupoauladetalle, gad.idgrupoaula, gad.idcurso, gad.iddocente, gad.idlocal, gad.idambiente, gad.nombre, gad.fecha_inicio, gad.fecha_final, gad.idgrado, gad.idsesion, gad.fechamodificacion, gad.idcomplementario, gad.orden,
             l.idlocal, l.nombre, l.direccion, l.id_ubigeo, l.tipo, l.vacantes, l.idugel, l.idproyecto
            from acad_matricula m 
            inner join acad_grupoauladetalle gad ON gad.idgrupoauladetalle = m.idgrupoauladetalle 
            INNER JOIN local l ON l.idlocal = gad.idlocal";

            $cond = array();
            if (isset($filtros["idlocal"])) {
                $cond[] = "l.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function progresolocal($filtros = null)
    {
        try {
            $sql = "SELECT m.idmatricula, m.idgrupoauladetalle, m.idalumno, m.fecha_registro, m.estado, m.idusuario, m.fecha_matricula, m.fecha_vencimiento, m.notafinal, m.formulajson, m.fecha_termino, m.pagorecurso, m.tipomatricula, m.idcomplementario, m.idcategoria,
            gad.idgrupoauladetalle, gad.idgrupoaula, gad.idcurso, gad.iddocente, gad.idlocal, gad.idambiente, gad.nombre, gad.fecha_inicio, gad.fecha_final, gad.idgrado, gad.idsesion, gad.fechamodificacion, gad.idcomplementario, gad.orden,
            l.idlocal, l.nombre, l.direccion, l.id_ubigeo, l.tipo, l.vacantes, l.idugel, l.idproyecto,
             (SELECT sum(b.progreso) from bitacora_smartbook b where b.idusuario = m.idalumno) AS progreso  
            from acad_matricula m 
            inner join acad_grupoauladetalle gad ON gad.idgrupoauladetalle = m.idgrupoauladetalle 
            INNER JOIN local l ON l.idlocal = gad.idlocal";
            $cond = array();
            if (isset($filtros["idlocal"])) {
                $cond[] = "l.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
            }
            if (isset($filtros["idcurso"])) {
                $cond[] = "gad.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function buscarexamen($filtros = null)
    {
        try {
            $sql = "SELECT per.ubigeo,per.idugel,ubi.ciudad,per.idlocal, (SELECT r.rol FROM personal p INNER JOIN roles r ON p.rol = r.idrol LIMIT 1) AS persona,(select acad_cursodetalle.idcurso from acad_cursodetalle inner join acad_curso on acad_curso.idcurso = acad_cursodetalle.idcurso where idrecurso = nq.idrecurso and estado = 1 limit 1) AS idcurso,nq.tipo,nq.nota,nq.calificacion_total,nq.notatexto,nq.habilidades,nq.habilidad_puntaje,nq.datos FROM personal per INNER JOIN ubigeo ubi ON per.ubigeo = ubi.id_ubigeo INNER JOIN notas_quiz nq ON nq.idalumno = per.idpersona";

            $cond = array();
            if (isset($filtros["tipo"])) {
                $cond[] = "nq.tipo = " . $this->oBD->escapar($filtros["tipo"]);
            }
            if (isset($filtros["likeEntrada"])) {
                $cond[] = "nq.datos like '%\"tipo\":\"E\"%'";
            }
            if (isset($filtros["likeSalida"])) {
                $cond[] = "nq.datos like '%\"tipo\":\"S\"%'";
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
        }
    }

    public function buscarTotalProgresoxUnidad($filtros)
    {
        try {
            $sql = "SELECT count(iddetalle) AS total FROM actividad_detalle ad INNER JOIN (SELECT a.idactividad FROM actividades a INNER JOIN acad_cursodetalle cd ON cd.idrecurso = a.sesion WHERE cd.idcurso = @curso AND cd.idpadre = @recurso AND a.metodologia >= 2  and cd.tiporecurso = 'L') t ON ad.idactividad = t.idactividad";
            $cond = array();

            if (isset($filtros['habilidad'])) {
                $cond[] = "ad.idhabilidad LIKE '%" . $filtros['habilidad'] . "%'";
            }
            if (isset($filtros['idcurso'])) {
                $sql = str_replace('@curso', $filtros['idcurso'], $sql);
            } else {
                throw new Exception("No existe idcurso especificado");
            }
            if (isset($filtros['idrecurso'])) {
                $sql = str_replace('@recurso', $filtros['idrecurso'], $sql);
            } else {
                throw new Exception("No existe idrecurso especificado");
            }

            if (!empty($cond)) {
                $sql .= " WHERE " . implode(" AND ", $cond);
            }
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("buscarTotalProgresoxUnidad") . ": " . $e->getMessage());
        }
    }

    public function buscarActualProgresoxUnidad($filtros)
    {
        try {
            $sql = "SELECT count(idactalumno) AS total FROM actividad_alumno a INNER JOIN (SELECT ad.iddetalle FROM actividad_detalle ad INNER JOIN (SELECT a.idactividad FROM actividades a INNER JOIN acad_cursodetalle cd ON cd.idrecurso = a.sesion WHERE cd.idcurso = @curso AND cd.idpadre = @recurso AND a.metodologia >= 2  and cd.tiporecurso = 'L') t ON ad.idactividad = t.idactividad WHERE ad.idhabilidad LIKE @habilidad) AS tablon ON tablon.iddetalle = a.iddetalleactividad";
            $cond = array();

            if (isset($filtros['idcurso'])) {
                $sql = str_replace("@curso", $filtros['idcurso'], $sql);
            } else {
                throw new Exception("No se especifico el recurso");
            }
            if (isset($filtros['idrecurso'])) {
                $sql = str_replace("@recurso", $filtros['idrecurso'], $sql);
            } else {
                throw new Exception("No se especifico el recurso");
            }

            if (isset($filtros['habilidad'])) {
                $cond[] = "a.habilidades LIKE '%" . $filtros['habilidad'] . "%'";
                $sql = str_replace("@habilidad", "'%" . $filtros['habilidad'] . "%'", $sql);
            } else {
                throw new Exception("No se especifico la habilidad");
            }
            if (isset($filtros['idalumno'])) {
                $cond[] = "a.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
            }

            $cond[] = "a.porcentajeprogreso >= 100";
            if (!empty($cond)) {
                $sql .= " WHERE " . implode(" AND ", $cond);
            }
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("buscarTotalProgresoxUnidad") . ": " . $e->getMessage());
        }
    }
}
