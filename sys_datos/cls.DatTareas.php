<?php
require_once(RUTA_BASE . SD . "json" . SD . "sys_web" . SD . "cls.WebCron_tarea_proy.php");

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-02-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */

class DatTareas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$this->runCron();
			$this->setLimite(0, 999992);
			//$sql = "SELECT *,(select p.nombre FROM personal p where p.idpersona = ta.idalumno) as nombre_alumno FROM tareas ta";			
			// $sql = "SELECT *,(select concat(d.ape_paterno,' ',d.ape_materno,', ',d.nombre) FROM personal d where d.idpersona = ta.iddocente) as nombre_docente,(select concat(p.ape_paterno,' ',ape_materno,', ',p.nombre) FROM personal p where p.idpersona = ta.idalumno) as nombre_alumno,(select tar.media from tareas_archivosalumno tar where tar.idtarea = ta.idtarea order by tar.idtarearecurso desc limit 1) as media,(select tar.tipo from tareas_archivosalumno tar where tar.idtarea = ta.idtarea order by tar.idtarearecurso desc limit 1) as tipo_media FROM tareas ta";
			$sql = "
			SELECT cron_tarea_proy.fecha_hora fecha_limite, ta.idtarea,ta.tipo,ta.idcurso,ta.idsesion,ta.idpestania,ta.idalumno,ta.recurso,ta.tiporecurso,ta.nota,ta.notabaseen,ta.iddocente,ta.idproyecto,ta.nombre,ta.descripcion,ta.fecha_calificacion,ta.criterios,ta.criterios_puntaje,ta.idcomplementario,ta.fecha_creacion,ta.vencido, ta.idgrupoauladetalle,
				(select concat(d.ape_paterno,' ',d.ape_materno,', ',d.nombre) FROM personal d where d.idpersona = ta.iddocente) as nombre_docente,
				(select concat(p.ape_paterno,' ',ape_materno,', ',p.nombre) FROM personal p where p.idpersona = ta.idalumno) as nombre_alumno,
				(select tar.media from tareas_archivosalumno tar where tar.idtarea = ta.idtarea order by tar.idtarearecurso desc limit 1) as media,
				(select tar.tipo from tareas_archivosalumno tar where tar.idtarea = ta.idtarea order by tar.idtarearecurso desc limit 1) as tipo_media 
			FROM tareas ta
			left join cron_tarea_proy on cron_tarea_proy.idproyecto=ta.idproyecto
				and cron_tarea_proy.idcurso=ta.idcurso
				and cron_tarea_proy.idcomplementario=ta.idcomplementario
				and cron_tarea_proy.idsesion=ta.idsesion
				and 
				IF(cron_tarea_proy.idpestania=-1, 
				0, 
				CONCAT(cron_tarea_proy.idsesion,cron_tarea_proy.idpestania))=ta.idpestania
			";

			if (isset($filtros["sql"])) {
				switch ($filtros["sql"]) {
					case '1':
						$sql = "SELECT  ta.idtarea,ta.tipo,ta.idcurso,ta.idsesion,ta.idpestania,ta.idalumno,ta.recurso,ta.tiporecurso,ta.nota,ta.notabaseen,ta.iddocente,ta.idproyecto,ta.nombre,ta.descripcion,ta.fecha_calificacion,ta.criterios,ta.criterios_puntaje,ta.idcomplementario,ta.fecha_creacion,ta.vencido, ta.idgrupoauladetalle, concat(personal.ape_paterno,' ', personal.ape_materno,', ', personal.nombre ) as nombre_docente 
								FROM tareas ta
								left join personal on ta.iddocente=personal.idpersona";
						break;
				}
			}
			$cond = array();
			if (isset($filtros["idtarea"])) {
				$cond[] = "ta.idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}

			if (isset($filtros["tipo"])) {
				$cond[] = "ta.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "ta.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "ta.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idpestania"])) {
				$cond[] = "ta.idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "ta.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["recurso"])) {
				$cond[] = "ta.recurso = " . $this->oBD->escapar($filtros["recurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "ta.tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["nota"])) {
				$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if (isset($filtros["notabaseen"])) {
				$cond[] = "ta.notabaseen = " . $this->oBD->escapar($filtros["notabaseen"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "ta.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "ta.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "ta.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "ta.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "ta.descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["fecha_calificacion"])) {
				$cond[] = "ta.fecha_calificacion = " . $this->oBD->escapar($filtros["fecha_calificacion"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "ta.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " GROUP BY ta.idtarea ";
			// echo $sql;exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			// print_r($e);exit(0);
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}

	public function eliminarTodo($filtros)
	{
		try {
			$sql = "DELETE ta, tareas_archivosalumno, tareas_mensajes FROM tareas ta 
					left join 	tareas_archivosalumno on tareas_archivosalumno.idtarea=ta.idtarea
					left join 	tareas_mensajes on tareas_mensajes.idtarea=ta.idtarea
					";
			$cond = array();
			if (isset($filtros["idtarea"])) {
				$cond[] = "ta.idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}

			if (isset($filtros["tipo"])) {
				$cond[] = "ta.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "ta.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "ta.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idpestania"])) {
				$cond[] = "ta.idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "ta.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["recurso"])) {
				$cond[] = "ta.recurso = " . $this->oBD->escapar($filtros["recurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "ta.tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["nota"])) {
				$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if (isset($filtros["notabaseen"])) {
				$cond[] = "ta.notabaseen = " . $this->oBD->escapar($filtros["notabaseen"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "ta.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "ta.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "ta.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "ta.descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["fecha_calificacion"])) {
				$cond[] = "ta.fecha_calificacion = " . $this->oBD->escapar($filtros["fecha_calificacion"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "ta.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "ta.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// echo "$sql"; exit();
			return $this->oBD->ejecutarSQL($sql);
			// return $this->oBD->delete('cron_tarea_proy', array('idcron_tapr' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}
	public function insertar($tipo, $idcurso, $idsesion, $idpestania, $idalumno, $recurso, $tiporecurso, $nota, $notabaseen, $iddocente, $idproyecto, $nombre, $descripcion, $fecha_calificacion = '', $criterios = '', $idcomplementario = 0,$idgrupoauladetalle=null )
	{
		try {

			$this->iniciarTransaccion('dat_tareas_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT idtarea FROM tareas ORDER BY idtarea DESC limit 0,1");
			++$id;

			$estados = array(
				'idtarea' => $id, 'tipo' => !empty($tipo) ? $tipo : 'T', 'idcurso' => $idcurso, 'idsesion' => $idsesion, 'idpestania' => $idpestania, 'idalumno' => $idalumno, 'recurso' => $recurso, 'tiporecurso' => $tiporecurso, 'nota' => !empty($nota) ? $nota : 0, 'notabaseen' => !empty($notabaseen) ? $notabaseen : 0, 'iddocente' => $iddocente, 'idproyecto' => $idproyecto, 'nombre' => !empty($nombre) ? $nombre : '', 'descripcion' => !empty($descripcion) ? $descripcion : '', 'criterios' => $criterios, 'idcomplementario' => $idcomplementario
			);
			if(!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"]=$idgrupoauladetalle;

			$this->oBD->insert('tareas', $estados);
			$this->terminarTransaccion('dat_tareas_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_tareas_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tipo, $idcurso, $idsesion, $idpestania, $idalumno, $recurso, $tiporecurso, $nota, $notabaseen, $iddocente, $idproyecto, $nombre, $descripcion, $fecha_calificacion = '', $criterios = '', $criterios_puntaje = '', $idcomplementario = null,$idgrupoauladetalle=null)
	{
		try {
			$this->iniciarTransaccion('dat_tareas_update');
			$estados = array(
				'tipo' => $tipo, 'idcurso' => $idcurso, 'idsesion' => $idsesion, 'idpestania' => $idpestania, 'idalumno' => $idalumno, 'recurso' => $recurso, 'tiporecurso' => $tiporecurso
				//,'nota'=>!empty($nota)?$nota:0
				//,'notabaseen'=>!empty($notabaseen)?$notabaseen:0
				//,'iddocente'=>$iddocente
				, 'idproyecto' => $idproyecto, 'nombre' => $nombre, 'descripcion' => $descripcion, 'criterios' => $criterios
				//,'criterios_puntaje'=>$criterios_puntaje
			);
			if (!empty($fecha_calificacion)) {
				$estados["fecha_calificacion"] = $fecha_calificacion;
				$estados['nota'] = !empty($nota) ? $nota : 0;
				$estados["notabaseen"] = !empty($notabaseen) ? $notabaseen : 0;
				$estados["iddocente"] = !empty($iddocente) ? $iddocente : 0;
				$estados["criterios_puntaje"] = !empty($criterios_puntaje) ? $criterios_puntaje : '';
			}
			if(!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"]=$idgrupoauladetalle;
			if(!empty($idcomplementario)) $estados["idcomplementario"]=$idcomplementario;
			$this->oBD->update('tareas ', $estados, array('idtarea' => $id));
			$this->terminarTransaccion('dat_tareas_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$this->runCron();
			$sql = "SELECT   idtarea,tipo,idcurso,idsesion,idpestania,idalumno,recurso,tiporecurso,nota,notabaseen,iddocente,idproyecto,nombre,descripcion,fecha_calificacion,criterios,criterios_puntaje,idcomplementario,fecha_creacion,vencido,idgrupoauladetalle  FROM tareas  "
				. " WHERE idtarea = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tareas', array('idtarea' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('tareas', array($propiedad => $valor), array('idtarea' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}
	private function runCron()
	{
		$oWebCron_tarea_proy = new WebCron_tarea_proy;
		$oWebCron_tarea_proy->runCron();
		$oWebCron_tarea_proy = null;
	}
}
