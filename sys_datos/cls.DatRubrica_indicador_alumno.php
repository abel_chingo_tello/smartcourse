<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-04-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatRubrica_indicador_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}
	public function contar($filtros = null)
	{
		try {
			$sql = "SELECT count(rubrica_indicador_alumno.idrubrica_indicador_alumno) total FROM rubrica_indicador_alumno";

			$cond = array();


			if (isset($filtros["idrubrica_indicador_alumno"])) {
				$cond[] = "idrubrica_indicador_alumno = " . $this->oBD->escapar($filtros["idrubrica_indicador_alumno"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["idrubrica_indicador"])) {
				$cond[] = "idrubrica_indicador = " . $this->oBD->escapar($filtros["idrubrica_indicador"]);
			}
			if (isset($filtros["idrubrica"])) {
				$cond[] = "idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if (isset($filtros["idrubrica_instancia"])) {
				$cond[] = "idrubrica_instancia = " . $this->oBD->escapar($filtros["idrubrica_instancia"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT idrubrica_indicador_alumno,idalumno,idrubrica_indicador,idrubrica,idrubrica_instancia, idgrupoauladetalle FROM rubrica_indicador_alumno";

			$cond = array();


			if (isset($filtros["idrubrica_indicador_alumno"])) {
				$cond[] = "idrubrica_indicador_alumno = " . $this->oBD->escapar($filtros["idrubrica_indicador_alumno"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["idrubrica_indicador"])) {
				$cond[] = "idrubrica_indicador = " . $this->oBD->escapar($filtros["idrubrica_indicador"]);
			}
			if (isset($filtros["idrubrica"])) {
				$cond[] = "idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if (isset($filtros["idrubrica_instancia"])) {
				$cond[] = "idrubrica_instancia = " . $this->oBD->escapar($filtros["idrubrica_instancia"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}

	public function buscarCriterio($filtros = null)
	{
		try {
			$sql = "SELECT rubrica_indicador.idrubrica_criterio, rubrica_indicador_alumno.* 
					FROM rubrica_indicador_alumno
					inner join rubrica_indicador on rubrica_indicador.idrubrica_indicador=rubrica_indicador_alumno.idrubrica_indicador";

			$cond = array();
			if (isset($filtros["idrubrica_indicador_alumno"])) {
				$cond[] = "idrubrica_indicador_alumno = " . $this->oBD->escapar($filtros["idrubrica_indicador_alumno"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["idrubrica_criterio"])) {
				$cond[] = "rubrica_indicador.idrubrica_criterio = " . $this->oBD->escapar($filtros["idrubrica_criterio"]);
			}
			if (isset($filtros["idrubrica_indicador"])) {
				$cond[] = "rubrica_indicador_alumno.idrubrica_indicador = " . $this->oBD->escapar($filtros["idrubrica_indicador"]);
			}
			if (isset($filtros["idrubrica"])) {
				$cond[] = "rubrica_indicador_alumno.idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if (isset($filtros["idrubrica_instancia"])) {
				$cond[] = "idrubrica_instancia = " . $this->oBD->escapar($filtros["idrubrica_instancia"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			// print_r($e);exit();
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}

	public function insertar($idalumno, $idrubrica_indicador, $idrubrica, $idrubrica_instancia, $idgrupoauladetalle)
	{
		try {

			$this->iniciarTransaccion('dat_rubrica_indicador_alumno_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrubrica_indicador_alumno) FROM rubrica_indicador_alumno");
			++$id;

			$estados = array(
				'idrubrica_indicador_alumno' => $id, 'idalumno' => $idalumno, 'idrubrica_indicador' => $idrubrica_indicador, 'idrubrica' => $idrubrica, 'idrubrica_instancia' => $idrubrica_instancia, 'idgrupoauladetalle' => $idgrupoauladetalle
			);

			$this->oBD->insert('rubrica_indicador_alumno', $estados);
			$this->terminarTransaccion('dat_rubrica_indicador_alumno_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_rubrica_indicador_alumno_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idalumno, $idrubrica_indicador, $idrubrica, $idrubrica_instancia, $idgrupoauladetalle)
	{
		try {
			$this->iniciarTransaccion('dat_rubrica_indicador_alumno_update');
			$estados = array(
				'idalumno' => $idalumno, 'idrubrica_indicador' => $idrubrica_indicador, 'idrubrica' => $idrubrica, 'idrubrica_instancia' => $idrubrica_instancia, 'idgrupoauladetalle' => $idgrupoauladetalle
			);

			$this->oBD->update('rubrica_indicador_alumno ', $estados, array('idrubrica_indicador_alumno' => $id));
			$this->terminarTransaccion('dat_rubrica_indicador_alumno_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT idrubrica_indicador_alumno,idalumno,idrubrica_indicador,idrubrica,idrubrica_instancia, idgrupoauladetalle FROM rubrica_indicador_alumno  "
				. " WHERE idrubrica_indicador_alumno = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rubrica_indicador_alumno', array('idrubrica_indicador_alumno' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('rubrica_indicador_alumno', array($propiedad => $valor), array('idrubrica_indicador_alumno' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_indicador_alumno") . ": " . $e->getMessage());
		}
	}
}
