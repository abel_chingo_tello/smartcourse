<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-02-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatAcad_cursodetalle extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM acad_cursodetalle";

			$cond = array();

			if (isset($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["orden"])) {
				$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if (isset($filtros["idrecurso"])) {
				$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["idlogro"])) {
				$cond[] = "idlogro = " . $this->oBD->escapar($filtros["idlogro"]);
			}
			if (isset($filtros["url"])) {
				$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if (isset($filtros["idpadre"])) {
				$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (isset($filtros["esfinal"])) {
				$cond[] = "esfinal = " . $this->oBD->escapar($filtros["esfinal"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}
	public function getCursosDetalles($filtros = null)
	{
		try {#niveles guarda los nombres de las sesiones
			$sql = "SELECT acd.idcursodetalle,acd.idcurso,acd.orden,acd.idrecurso,acd.tiporecurso,acd.idlogro,acd.url,acd.idpadre,acd.color,acd.esfinal,acd.txtjson, n.nombre as nombre_detalle FROM acad_cursodetalle acd INNER JOIN niveles n ON (n.idnivel = acd.idrecurso)";
			if (isset($filtros["idcomplementario"])&&!empty($filtros["idcomplementario"])) {
				$sql = "SELECT acd.idcursodetalle,acd.idcurso,acd.orden,acd.idrecurso,acd.tiporecurso,acd.idlogro,acd.url,acd.idpadre,acd.color,acd.esfinal,acd.txtjson, n.nombre as nombre_detalle FROM acad_cursodetalle_complementario acd INNER JOIN niveles_complementario n ON (n.idnivel = acd.idrecurso)";
			}
			$cond = array();

			if (isset($filtros["idcursodetalle"])) {
				$cond[] = "acd.idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if (isset($filtros["idcurso"])&&empty($filtros["idcomplementario"])) {
				$cond[] = "acd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (!empty($filtros["idcomplementario"])) {
				$cond[] = "acd.idcurso = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["orden"])) {
				$cond[] = "acd.orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if (isset($filtros["idrecurso"])) {
				$cond[] = "acd.idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "acd.tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["idlogro"])) {
				$cond[] = "acd.idlogro = " . $this->oBD->escapar($filtros["idlogro"]);
			}
			if (isset($filtros["url"])) {
				$cond[] = "acd.url = " . $this->oBD->escapar($filtros["url"]);
			}
			if (isset($filtros["idpadre"])) {
				$cond[] = "acd.idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (isset($filtros["espadre"])) {
				$cond[] = "acd.espadre = " . $this->oBD->escapar($filtros["espadre"]);
			}
			if (isset($filtros["esfinal"])) {
				$cond[] = "acd.esfinal = " . $this->oBD->escapar($filtros["esfinal"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (isset($filtros["sql"]) && !empty($filtros["sql"]) && isset($filtros['idcurso'])) {
				if ($filtros["sql"] == 'sql1') {
					$sql = "SELECT  acd.idcursodetalle,acd.idcurso,acd.orden,acd.idrecurso,acd.tiporecurso,acd.idlogro,acd.url,acd.idpacdre,acd.color,acd.esfinal,acd.txtjson,ad.espadre, n.nombre as nombre_detalle 
							FROM acad_curso_complementario ac 
								INNER JOIN acad_cursodetalle_complementario acd ON (ac.idcurso=acd.idcurso) 
								INNER JOIN niveles_complementario n ON (n.idnivel = acd.idrecurso)
							where ac.idcursoprincipal=" . $this->oBD->escapar($filtros["idcurso"]);
				}
			}
			// print_r($this->oBD->consultarSQL($sql));
			// echo "$sql";exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT n.nombre, ad.idcursodetalle,ad.idcurso,ad.orden,ad.idrecurso,ad.tiporecurso,ad.idlogro,ad.url,ad.idpadre,ad.color,ad.esfinal,ad.txtjson,ad.espadre  FROM acad_cursodetalle ad inner JOIN niveles n ON ad.idrecurso=idnivel";
			if (isset($filtros["complementario"])) {
				if ($filtros["complementario"]) {
					$sql = "SELECT n.nombre,  ad.idcursodetalle,ad.idcurso,ad.orden,ad.idrecurso,ad.tiporecurso,ad.idlogro,ad.url,ad.idpadre,ad.color,ad.esfinal,ad.txtjson,ad.espadre FROM acad_cursodetalle_complementario ad INNER JOIN niveles_complementario n ON ad.idrecurso=idnivel";
				}
			}
			$cond = array();
			if (isset($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["orden"])) {
				$cond[] = "ad.orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if (isset($filtros["idrecurso"])) {
				$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["idlogro"])) {
				$cond[] = "idlogro = " . $this->oBD->escapar($filtros["idlogro"]);
			}
			if (isset($filtros["url"])) {
				$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if (isset($filtros["idpadre"])) {
				$cond[] = "ad.idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (isset($filtros["color"])) {
				$cond[] = "color = " . $this->oBD->escapar($filtros["color"]);
			}
			if (isset($filtros["esfinal"])) {
				$cond[] = "esfinal = " . $this->oBD->escapar($filtros["esfinal"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (isset($filtros["orderby"])) {
				$sql .= " ORDER BY " . implode(' , ', $filtros["orderby"]);
			} else {
				$sql .= " ORDER BY ad.orden ASC ";
			}
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}



	/*
SELECT l.*,gad.idgrado
FROM local l INNER JOIN acad_grupoauladetalle gad ON gad.idlocal = l.idlocal INNER JOIN acad_matricula ma ON gad.idgrupoauladetalle = ma.idgrupoauladetalle
WHERE l.idlocal = 44;
(SELECT  SUM(tablita.progreso) FROM (
SELECT (SELECT (SUM(b.progreso) / 4) FROM bitacora_smartbook b WHERE b.idsesion = acd.idcursodetalle AND b.idusuario = 1) AS progreso FROM (SELECT * FROM acad_cursodetalle cd WHERE cd.idcurso = 31 AND cd.idpadre = 0 AND cd.tiporecurso = 'U') AS tabla1 
INNER JOIN acad_cursodetalle acd ON acd.idpadre = tabla1.idcursodetalle 
INNER JOIN bitacora_smartbook bs ON bs.idsesion = acd.idcursodetalle
WHERE bs.idusuario = 1 group by tabla1.idrecurso) AS tablita);
*/

	public function getprogresounidad($filtros = null)
	{
		try {
			$sql = "SELECT DISTINCT tabla1.idrecurso,tabla1.tiporecurso,acd.idcurso,(SELECT (SUM(b.progreso) / 4) FROM bitacora_smartbook b WHERE b.idsesion = acd.idcursodetalle _idusuario) AS progreso FROM (SELECT cd.idcursodetalle,cd.idcurso,cd.orden,cd.idrecurso,cd.tiporecurso,cd.idlogro,cd.url,cd.idpadre,cd.color,cd.esfinal,cd.txtjson,cd.espadre FROM acad_cursodetalle cd WHERE cd.idcurso = _idcurso AND cd.idpadre = 0 AND cd.tiporecurso = 'U') AS tabla1 INNER JOIN acad_cursodetalle acd ON acd.idpadre = tabla1.idcursodetalle INNER JOIN bitacora_smartbook bs ON bs.idsesion = acd.idcursodetalle";

			if (isset($filtros['idcurso'])) {
				//$this->oBD->escapar
				$sql = str_replace('_idcurso', strval($filtros['idcurso']), $sql);
			} else {
				$sql = str_replace('_idcurso', '31', $sql);
			}

			$cond = array();

			if (isset($filtros['idusuario'])) {
				$sql = str_replace('_idusuario', " AND b.idusuario = " . strval($filtros['idusuario']) . " ", $sql);
			} else {
				$sql = str_replace('_idusuario', ' ', $sql);
			}

			if (isset($filtros['idusuario'])) {
				$cond[] = "bs.idusuario = " . $this->oBD->escapar($filtros['idusuario']);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function buscarconnivel($filtros = null)
	{
		try {
			$sql = "SELECT cd.idcursodetalle,cd.idcurso,cd.orden,cd.idrecurso,cd.tiporecurso,cd.idlogro,cd.url,cd.idpadre,cd.color,cd.esfinal,cd.txtjson,cd.espadre, ni.idnivel, ni.nombre, ni.idpadre as padrenivel, descripcion, imagen, 
						(SELECT count(1) from acad_cursodetalle acd WHERE acd.idpadre=cd.idcursodetalle ) as nhijos,txtjson 
						FROM acad_cursodetalle cd 
						LEFT JOIN niveles ni ON idrecurso=idnivel ";
			if (isset($filtros["complementario"])) {
				$sql = "SELECT cd.idcursodetalle,cd.idcurso,cd.orden,cd.idrecurso,cd.tiporecurso,cd.idlogro,cd.url,cd.idpadre,cd.color,cd.esfinal,cd.txtjson,cd.espadre, ni.idnivel, ni.nombre, ni.idpadre as padrenivel, descripcion, imagen, (SELECT count(1) from acad_cursodetalle_complementario acd WHERE acd.idpadre=cd.idcursodetalle ) as nhijos,txtjson 
					FROM acad_cursodetalle_complementario cd 
					LEFT JOIN niveles_complementario ni ON idrecurso=idnivel ";
			}
			$cond = array();

			if (isset($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["orden"])) {
				$cond[] = "cd.orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if (isset($filtros["idrecurso"])) {
				$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["idlogro"])) {
				$cond[] = "idlogro = " . $this->oBD->escapar($filtros["idlogro"]);
			}
			if (isset($filtros["url"])) {
				$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if (isset($filtros["idpadre"])) {
				$cond[] = "cd.idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;		
			if(!empty($filtros["neworden"])){
				$sql .= " ORDER BY cd.idpadre ASC, cd.orden ASC ";
			}else if (!empty($filtros["orderBy"]))				
				$sql .= " ORDER BY cd.orden ASC , " . $filtros["orderBy"];
			else
				$sql .= " ORDER BY cd.orden ASC ";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function maxorden($idcurso, $idpadre, $tabla = "acad_cursodetalle")
	{
		try {
			$sql = "SELECT max(orden) as orden FROM " . $tabla;
			$cond = array();
			if (isset($idcurso)) {
				$cond[] = "idcurso = " . $this->oBD->escapar($idcurso);
			}
			if (isset($idpadre)) {
				$cond[] = "idpadre = " . $this->oBD->escapar($idpadre);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function insertoupdate($id, $idcurso, $orden, $idrecurso, $tiporecurso, $idlogro, $url, $idpadre = 0, $color, $esfinal = 0, $idcomplementario = 0, $espadre = 0)
	{
		try {
			$this->iniciarTransaccion('dat_acad_insertupdate');
			$acc = 'add';
			if (!empty($id)) {
				$tabla = "acad_cursodetalle";
				if ($idcomplementario != 0) {
					$tabla = "acad_cursodetalle_complementario";
					$idcurso = $idcomplementario;
				}
				$datos = $this->get($id, $tabla);
				if (!empty($datos)) {
					$acc = 'editar';
					$id = $this->actualizar($id, $idcurso, $orden, $idrecurso, $tiporecurso, $idlogro, $url, $idpadre, $color, $esfinal, $espadre, $tabla);
				}
			}
			if ($acc == 'add') {
				$idpadre = !empty($idpadre) ? $idpadre : 0;
				$tabla = "acad_cursodetalle";
				if ($idcomplementario != 0) {
					$tabla = "acad_cursodetalle_complementario";
					$idcurso = $idcomplementario;
				}
				$orden = $this->maxorden($idcurso, $idpadre, $tabla);
				$orden++;
				$id = $this->insertar($idcurso, $orden, $idrecurso, $tiporecurso, $idlogro, $url, $idpadre, $color, $esfinal, $espadre, $tabla);
			}
			$this->terminarTransaccion('dat_acad_insertupdate');
			return array('idcursodetalle' => $id, 'idpadre' => $idpadre, 'orden' => $orden, 'espadre' => $espadre);
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_niveles_insertupdate');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}


	public function insertar($idcurso, $orden, $idrecurso, $tiporecurso, $idlogro, $url, $idpadre, $color, $esfinal, $espadre = 0, $tabla = "acad_cursodetalle")
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursodetalle_insert');
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcursodetalle) FROM " . $tabla);
			++$id;
			$estados = array(
				'idcursodetalle' => $id, 'idcurso' => $idcurso, 'orden' => $orden, 'idrecurso' => $idrecurso, 'tiporecurso' => $tiporecurso, 'idlogro' => $idlogro, 'url' => $url, 'idpadre' => !empty($idpadre) ? $idpadre : 0, 'color' => $color, 'esfinal' => !empty($esfinal) ? $esfinal : 0, 'espadre' => $espadre, 'txtjson' => ''
			);

			$this->oBD->insert($tabla, $estados);
			$this->terminarTransaccion('dat_acad_cursodetalle_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursodetalle_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function duplicar($idcursodetalle, $data)
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursodetalle_insert');
			$dt = $this->get($idcursodetalle);
			if ($dt == null) {
				throw new Exception("ERROR\n " . JrTexto::_("curso detalle no existe"));
				return false;
			}
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcursodetalle) FROM acad_cursodetalle");
			++$id;
			$estados = array(
				'idcursodetalle' => $id, 'idcurso' => !empty($data["idcurso"]) ? $data["idcurso"] : $dt["idcurso"], 'orden' => !empty($data["orden"]) ? $data["orden"] : $dt["orden"], 'idrecurso' => !empty($data["idrecurso"]) ? $data["idrecurso"] : $dt["idrecurso"], 'tiporecurso' => !empty($data["tiporecurso"]) ? $data["tiporecurso"] : $dt["tiporecurso"], 'idlogro' => !empty($data["idlogro"]) ? $data["idlogro"] : $dt["idlogro"], 'url' => !empty($data["url"]) ? $data["url"] : $dt["url"], 'idpadre' => !empty($data["idpadre"]) ? $data["idpadre"] : $dt["idpadre"], 'color' => !empty($data["color"]) ? $data["color"] : $dt["color"], 'esfinal' => !empty($data["esfinal"]) ? $data["esfinal"] : $dt["esfinal"], 'txtjson' => !empty($data["txtjson"]) ? $data["txtjson"] : $dt["txtjson"]
			);
			$this->oBD->insert('acad_cursodetalle', $estados);
			$this->terminarTransaccion('dat_acad_cursodetalle_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursodetalle_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function actualizar($id, $idcurso, $orden, $idrecurso, $tiporecurso, $idlogro, $url, $idpadre, $color, $esfinal, $espadre = 0, $tabla = "acad_cursodetalle")
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursodetalle_update');
			$estados = array(
				'idcurso' => $idcurso, 'orden' => $orden, 'idrecurso' => $idrecurso, 'tiporecurso' => $tiporecurso, 'idlogro' => $idlogro, 'url' => $url, 'idpadre' => !empty($idpadre) ? $idpadre : 0, 'color' => $color, 'esfinal' => !empty($esfinal) ? $esfinal : 0, 'espadre' => $espadre
			);

			$this->oBD->update($tabla, $estados, array('idcursodetalle' => $id));
			$this->terminarTransaccion('dat_acad_cursodetalle_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}
	public function get($id, $tabla = "acad_cursodetalle")
	{
		try {
			$sql = "SELECT idcursodetalle,idcurso,orden,idrecurso,tiporecurso,idlogro,url,idpadre,color,esfinal,txtjson,espadre FROM " . $tabla . " WHERE idcursodetalle = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id, $tabla = "acad_cursodetalle")
	{
		try {
			return $this->oBD->delete($tabla, array('idcursodetalle' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function eliminar2($campo, $valor)
	{
		try {
			return $this->oBD->delete('acad_cursodetalle', array($campo => $valor));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('acad_cursodetalle', array($propiedad => $valor), array('idcursodetalle' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}
	public function set_($id, $propiedad, $valor, $tabla = "acad_cursodetalle")
	{ //02.01.13
		try {
			$this->oBD->update($tabla, array($propiedad => $valor), array('idcursodetalle' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}
}
