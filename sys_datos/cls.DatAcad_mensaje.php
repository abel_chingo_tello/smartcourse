<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-03-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatAcad_mensaje extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_mensaje").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT CONCAT(p.nombre, ' ', p.ape_paterno, ' ', p.ape_materno) as nombreAutor, p.email as correoAutor, m.mensaje, m.asunto, DATE_FORMAT(m.fecha_hora, '%h:%i %p') as hora, DATE_FORMAT(m.fecha_hora, '%d/%m/%Y') as fecha FROM acad_mensaje m INNER JOIN personal p ON (p.idpersona = m.de)";			
			
			$cond = array();		
			
			if(isset($filtros["para"])) {
				$sql = "SELECT m.id, CONCAT(p.nombre, ' ', p.ape_paterno, ' ', p.ape_materno) as nombreAutor, p.email as correoAutor, m.mensaje, m.asunto, DATE_FORMAT(m.fecha_hora, '%h:%i %p') as hora, DATE_FORMAT(m.fecha_hora, '%d/%m/%Y') as fecha, mp.id as id2, mp.leido FROM acad_mensaje m INNER JOIN acad_mensaje_para mp ON (m.id = mp.idmensaje) INNER JOIN personal p ON (p.idpersona = m.de)";
				$cond[] = "mp.para = " . $this->oBD->escapar($filtros["para"]);
			}
			
			if(isset($filtros["id"])) {
					$cond[] = "m.id IN (" . implode(",", $filtros["id"]) . ")";
			}
			if(isset($filtros["de"])) {
					$cond[] = "m.de = " . $this->oBD->escapar($filtros["de"]);
			}
			if(isset($filtros["asunto"])) {
					$cond[] = "m.asunto = " . $this->oBD->escapar($filtros["asunto"]);
			}
			if(isset($filtros["mensaje"])) {
					$cond[] = "m.mensaje = " . $this->oBD->escapar($filtros["mensaje"]);
			}
			if(isset($filtros["fecha_hora"])) {
					$cond[] = "m.fecha_hora = " . $this->oBD->escapar($filtros["fecha_hora"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "m.estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY m.id DESC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_mensaje").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($estados,$para)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_mensaje_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM acad_mensaje");
			++$id;
			$estados["id"] = $id;
			$this->oBD->insert('acad_mensaje', $estados);
			foreach ($para as $key => $value) {
				$mensaje_para = array(
					"idmensaje"=>$id
					,"para"=>$value
				);
				$this->oBD->insert('acad_mensaje_para', $mensaje_para);
			}
			$this->terminarTransaccion('dat_acad_mensaje_insert');			
			return $id;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_mensaje_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_mensaje").": " . $e->getMessage());
		}
	}
	public function actualizar($estados)
	{
		try {
			$this->iniciarTransaccion('dat_acad_mensaje_update');
			$id = array('id' => $estados["id"]);
			unset($estados["id"]);
			$this->oBD->update('acad_mensaje ', $estados, $id);
		    $this->terminarTransaccion('dat_acad_mensaje_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_mensaje").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT `id`,`de`,`asunto`,`mensaje`,`fecha_hora`,`estado`  FROM acad_mensaje  "
					. " WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_mensaje").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_mensaje', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_mensaje").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor, $tabla = 'acad_mensaje')
	{//02.01.13
		try {
			$this->oBD->update($tabla, array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_mensaje").": " . $e->getMessage());
		}
	}
		
}