<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-09-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatMin_dre extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Min_dre") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT min_dre.iddre, min_dre.descripcion, min_dre.ubigeo, min_dre.opcional, min_dre.idproyecto FROM min_dre";

			$cond = array();

			if (isset($filtros["iddre"])) {
				$cond[] = "iddre = " . $this->oBD->escapar($filtros["iddre"]);
			}

			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion " . $this->oBD->like($filtros["descripcion"]);
			}
			if (isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if (isset($filtros["opcional"])) {
				$cond[] = "opcional = " . $this->oBD->escapar($filtros["opcional"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "descripcion " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY descripcion ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Min_dre") . ": " . $e->getMessage());
		}
	}

	public function buscarv2($filtros = null)
	{
		try {
			$sql = "SELECT min_dre.iddre, min_dre.descripcion, min_dre.ubigeo, min_dre.opcional, min_dre.idproyecto FROM min_dre";

			$cond = array();

			if (isset($filtros["iddre"])) {
				$cond[] = "iddre = " . $this->oBD->escapar($filtros["iddre"]);
			}

			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if (isset($filtros["opcional"])) {
				$cond[] = "opcional = " . $this->oBD->escapar($filtros["opcional"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "descripcion " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY descripcion ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Min_dre") . ": " . $e->getMessage());
		}
	}

	public function insertar($descripcion, $ubigeo, $opcional, $idproyecto)
	{
		try {

			$this->iniciarTransaccion('dat_min_dre_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(iddre) FROM min_dre");
			++$id;

			$estados = array(
				'iddre' => $id, 'descripcion' => $descripcion, 'ubigeo' => $ubigeo, 'opcional' => $opcional, 'idproyecto' => $idproyecto
			);

			$this->oBD->insert('min_dre', $estados);
			$this->terminarTransaccion('dat_min_dre_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_min_dre_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Min_dre") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $descripcion, $ubigeo, $opcional, $idproyecto)
	{
		try {
			$this->iniciarTransaccion('dat_min_dre_update');
			$estados = array(
				'descripcion' => $descripcion, 'ubigeo' => $ubigeo, 'opcional' => $opcional, 'idproyecto' => $idproyecto
			);

			$this->oBD->update('min_dre ', $estados, array('iddre' => $id));
			$this->terminarTransaccion('dat_min_dre_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Min_dre") . ": " . $e->getMessage());
		}
	}

	public function importar($id, $descripcion, $ubigeo, $opcional, $idproyecto = 0)
	{
		try {
			if (empty($id)) return $this->insertar($descripcion, $ubigeo, $opcional, $idproyecto);
			else {
				$this->iniciarTransaccion('dat_min_dre_insert2');
				$estados = array(
					'iddre' => $id, 'descripcion' => $descripcion, 'ubigeo' => $ubigeo, 'opcional' => $opcional, 'idproyecto' => $idproyecto
				);
				$this->oBD->insert('min_dre', $estados);
				$this->terminarTransaccion('dat_min_dre_insert2');
				return $id;
			}
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_min_dre_insert');
			return -1;
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT  min_dre.iddre, min_dre.descripcion, min_dre.ubigeo, min_dre.opcional, min_dre.idproyecto  FROM min_dre  "
				. " WHERE iddre = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Min_dre") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('min_dre', array('iddre' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Min_dre") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('min_dre', array($propiedad => $valor), array('iddre' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Min_dre") . ": " . $e->getMessage());
		}
	}
}
