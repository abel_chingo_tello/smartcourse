<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatRubrica_instancia extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Rubrica_instancia") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT idrubrica_instancia,idrubrica,idcursodetalle,idpestania,idcomplementario,idcurso FROM rubrica_instancia";

			$cond = array();


			if (isset($filtros["idrubrica_instancia"])) {
				$cond[] = "idrubrica_instancia = " . $this->oBD->escapar($filtros["idrubrica_instancia"]);
			}
			if (isset($filtros["idrubrica"])) {
				$cond[] = "idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if (isset($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if (isset($filtros["idpestania"])) {
				$cond[] = "idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Rubrica_instancia") . ": " . $e->getMessage());
		}
	}


	public function insertar($idrubrica, $idcursodetalle, $idpestania, $idcomplementario, $idcurso)
	{
		try {

			$this->iniciarTransaccion('dat_rubrica_instancia_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrubrica_instancia) FROM rubrica_instancia");
			++$id;

			$estados = array(
				'idrubrica_instancia' => $id, 'idrubrica' => $idrubrica, 'idcursodetalle' => $idcursodetalle, 'idpestania' => $idpestania, 'idcomplementario' => $idcomplementario, 'idcurso' => $idcurso
			);

			$this->oBD->insert('rubrica_instancia', $estados);
			$this->terminarTransaccion('dat_rubrica_instancia_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_rubrica_instancia_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Rubrica_instancia") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idrubrica, $idcursodetalle, $idpestania, $idcomplementario, $idcurso)
	{
		try {
			$this->iniciarTransaccion('dat_rubrica_instancia_update');
			$estados = array(
				'idrubrica' => $idrubrica, 'idcursodetalle' => $idcursodetalle, 'idpestania' => $idpestania, 'idcomplementario' => $idcomplementario, 'idcurso' => $idcurso
			);

			$this->oBD->update('rubrica_instancia ', $estados, array('idrubrica_instancia' => $id));
			$this->terminarTransaccion('dat_rubrica_instancia_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_instancia") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idrubrica_instancia,idrubrica,idcursodetalle,idpestania,idcomplementario,idcurso  FROM rubrica_instancia  WHERE idrubrica_instancia = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Rubrica_instancia") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rubrica_instancia', array('idrubrica_instancia' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Rubrica_instancia") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('rubrica_instancia', array($propiedad => $valor), array('idrubrica_instancia' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_instancia") . ": " . $e->getMessage());
		}
	}
}
