<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-12-2016  
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
class DatHerramientas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM herramientas";

			$cond = array();

			if (!empty($filtros["id"])) {
				$cond[] = "idtool = " . $this->oBD->escapar($filtros["id"]);
			}
			if (!empty($filtros["idnivel"])) {
				$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if (!empty($filtros["idunidad"])) {
				$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if (!empty($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if (!empty($filtros["idpersonal"])) {
				$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}

			if (!empty($filtros["tool"])) {
				$cond[] = "tool = " . $this->oBD->escapar($filtros["tool"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT herramientas.idtool, herramientas.idnivel, herramientas.idunidad, herramientas.idactividad, herramientas.idpersonal, herramientas.titulo, herramientas.descripcion, herramientas.texto, herramientas.orden, herramientas.tool FROM herramientas";

			$cond = array();


			if (!empty($filtros["id"])) {
				$cond[] = "idtool = " . $this->oBD->escapar($filtros["id"]);
			}
			if (!empty($filtros["idnivel"])) {
				$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if (!empty($filtros["idunidad"])) {
				$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if (!empty($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if (!empty($filtros["titulo"])) {
				$cond[] = "titulo LIKE '%" . $filtros["titulo"] . "%'";
			}
			if (!empty($filtros["idpersonal"])) {
				$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}

			if (!empty($filtros["tool"])) {
				$cond[] = "tool = " . $this->oBD->escapar($filtros["tool"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY orden ASC, idtool ASC";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  herramientas.idtool, herramientas.idnivel, herramientas.idunidad, herramientas.idactividad, herramientas.idpersonal, herramientas.titulo, herramientas.descripcion, herramientas.texto, herramientas.orden, herramientas.tool  FROM herramientas  ";

			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("List all") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}

	public function insertar($idnivel, $idunidad, $idactividad, $idpersonal, $titulo, $descripcion, $texto, $orden, $tool)
	{
		try {

			$this->iniciarTransaccion('dat_herramientas_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtool) FROM herramientas");
			++$id;

			$estados = array(
				'idtool' => $id, 'idnivel' => $idnivel, 'idunidad' => $idunidad, 'idactividad' => $idactividad, 'idpersonal' => $idpersonal, 'titulo' => $titulo, 'descripcion' => $descripcion, 'texto' => $texto, 'orden' => $orden, 'tool' => $tool
			);

			$this->oBD->insert('herramientas', $estados);
			$this->terminarTransaccion('dat_herramientas_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_herramientas_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel, $idunidad, $idactividad, $idpersonal, $titulo, $descripcion, $texto, $orden, $tool)
	{
		try {
			$this->iniciarTransaccion('dat_herramientas_update');
			$estados = array(
				'idnivel' => $idnivel, 'idunidad' => $idunidad, 'idactividad' => $idactividad, 'idpersonal' => $idpersonal, 'titulo' => $titulo, 'descripcion' => $descripcion, 'texto' => $texto, 'orden' => $orden, 'tool' => $tool
			);

			$this->oBD->update('herramientas ', $estados, array('idtool' => $id));
			$this->terminarTransaccion('dat_herramientas_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  herramientas.idtool, herramientas.idnivel, herramientas.idunidad, herramientas.idactividad, herramientas.idpersonal, herramientas.titulo, herramientas.descripcion, herramientas.texto, herramientas.orden, herramientas.tool  FROM herramientas  "
				. " WHERE idtool = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('herramientas', array('idtool' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}
	public function eliminartoolxactividad($idnivel, $idunidad, $idactividad, $tool)
	{
		try {
			return $this->oBD->delete('herramientas', array('idnivel' => $idnivel, 'idunidad' => $idunidad, 'idactividad' => $idactividad, 'tool' => $tool));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}
	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('herramientas', array($propiedad => $valor), array('idtool' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Herramientas") . ": " . $e->getMessage());
		}
	}
}
