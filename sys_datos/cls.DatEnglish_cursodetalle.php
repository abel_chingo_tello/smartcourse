<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatEnglish_cursodetalle extends DatBase
{
	public function __construct($BD = '')
	{
		try {
			if (!empty($BD)) $BD = NAME_BDSEAdolecentes;
			parent::conectar($BD);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT `idcursodetalle`,`idcurso`,`orden`,`idrecurso`,`tiporecurso`,`idlogro`,`url`,`idpadre`,`color`,`esfinal`,`txtjson`,`nhoras` FROM acad_cursodetalle";
			$cond = array();

			if (isset($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["orden"])) {
				$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if (isset($filtros["idrecurso"])) {
				$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["idlogro"])) {
				$cond[] = "idlogro = " . $this->oBD->escapar($filtros["idlogro"]);
			}
			if (isset($filtros["url"])) {
				$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if (isset($filtros["idpadre"])) {
				$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (isset($filtros["color"])) {
				$cond[] = "color = " . $this->oBD->escapar($filtros["color"]);
			}
			if (isset($filtros["esfinal"])) {
				$cond[] = "esfinal = " . $this->oBD->escapar($filtros["esfinal"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (isset($filtros["orderby"])) {
				$sql .= " ORDER BY " . implode(' , ', $filtros["orderby"]);
			} else {
				$sql .= " ORDER BY orden ASC ";
			}
			if (isset($filtros["BD"])) {
				parent::conectar($filtros["BD"]);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function buscarcurso($filtros = null)
	{
		try {
			$sql = "SELECT c.idcurso, c.nombre, c.imagen, c.descripcion, c.estado, c.fecharegistro, c.idusuario, c.vinculosaprendizajes, c.materialesyrecursos, c.color, c.objetivos, c.certificacion, c.costo, c.silabo, c.e_ini, c.pdf, c.abreviado, c.e_fin, c.aniopublicacion, c.autor, c.txtjson, pc.idproycurso, pc.idcurso, pc.idproyecto , (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
						(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
						(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
						(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes 
					from acad_curso c LEFT JOIN proyecto_cursos pc ON c.idcurso=pc.idcurso";

			$cond = array();
			if (isset($filtros["sql2"])) {
				$sql = "SELECT *, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
				(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
				(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
				(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes from acad_curso c";
			}
			if (isset($filtros['sql3'])) {
				$sql = "SELECT * FROM acad_curso c";
				if (isset($filtros['idcurso'])){
					if (is_array($filtros['idcurso'])) {
						$cond[] = "c.idcurso IN (" . implode(',', $filtros['idcurso']) . ")";
					} else {
						$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
					}
				}
			} else {
				if (isset($filtros["idcurso"])) {
					$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
			}

			if (isset($filtros["nombre"])) {
				$cond[] = "c.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["imagen"])) {
				$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "c.descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "c.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " ASC";
				}
			}else{
				$sql .= " ORDER BY c.nombre ASC";
			}

			if (isset($filtros["BD"])) {
				parent::conectar($filtros["BD"]);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search ingles ") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function copiar($filtros){
		try {
			//var_dump($filtros);
			$this->iniciarTransaccion('dat_copiar_curso');
			$sql="SELECT * FROM niveles WHERE  idnivel=".$filtros["idrecurso"];
			parent::conectar($filtros["BD"]);
			$nivel=$this->oBD->consultarSQL($sql);
			if(empty($nivel[0])){
				echo json_encode(array('code'=>'Error','msj'=>'no se encontro nivel'));
		 		$this->terminarTransaccion('dat_copiar_curso');
		 		exit();
			}
			$nivel=$nivel[0];
			$newnivel=array();
			foreach ($nivel as $k => $v){
				$newnivel[$k]=$v;
			}
			$newnivel["nombre"]=$filtros["nombrecurso"];
			$newnivel["idpersonal"]=$filtros["idpersona"];
			$newnivel["idnivel"]=$this->oBD->consultarEscalarSQL("SELECT idnivel+1 FROM niveles ORDER BY idnivel DESC limit 0,1");
			$this->oBD->insert('niveles', $newnivel);
		


			$sql="SELECT * FROM acad_cursodetalle WHERE  idcurso=".$filtros["idcurso"]." AND idrecurso=".$filtros["idrecurso"];
			parent::conectar($filtros["BD"]);
			$cursodetalle=$this->oBD->consultarSQL($sql);

		 	if(empty($cursodetalle[0])){
		 		echo json_encode(array('code'=>'Error','msj'=>'no se encontro curso detalle '));
		 		$this->terminarTransaccion('dat_copiar_curso');
		 		exit();	
		 	}
		 	$cursodetalle=$cursodetalle[0];
		 	$new=array();
			foreach ($cursodetalle as $k => $v){
				$new[$k]=$v;
			}
			$new["idrecurso"]=$newnivel["idnivel"];			
			$new["idcursodetalle"]=$this->oBD->consultarEscalarSQL("SELECT idcursodetalle+1 FROM acad_cursodetalle ORDER BY idcursodetalle DESC limit 0,1");
			$this->oBD->insert('acad_cursodetalle', $new);
     		

     		//copiar actividades
			$sql="SELECT * FROM actividades WHERE  sesion=".$nivel["idnivel"];
			parent::conectar($filtros["BD"]);
			$actividades=$this->oBD->consultarSQL($sql);
			if(empty($actividades)){
				echo json_encode(array('code'=>'Error','msj'=>'no se encontro actividades'));
		 		$this->terminarTransaccion('dat_copiar_curso');
		 		exit();
			}
			$newidactiviad=$this->oBD->consultarEscalarSQL("SELECT idactividad FROM actividades ORDER BY idactividad DESC limit 0,1");
			foreach ($actividades as $k => $act){
				$newactiviad=array();
				foreach ($act as $cp => $va){
					$newactiviad[$cp]=$va;
				}
				$newidactiviad++;
				$newactiviad["idactividad"]=$newidactiviad;
				$newactiviad["sesion"]=$newnivel["idnivel"];
				$newactiviad["idpersonal"]=$filtros["idpersona"];
				$this->oBD->insert('actividades', $newactiviad);

				/***Actividad detalle **/
				$sql="SELECT * FROM actividad_detalle WHERE  idactividad=".$act["idactividad"];
				parent::conectar($filtros["BD"]);
				$actividades_detalle=$this->oBD->consultarSQL($sql);
				$newidactiviaddet=$this->oBD->consultarEscalarSQL("SELECT iddetalle FROM actividad_detalle ORDER BY iddetalle DESC limit 0,1");
				foreach ($actividades_detalle as $cp => $actdet){
					$newactiviad_detalle=array();
					foreach($actdet as $cpat => $vaact){
						$newactiviad_detalle[$cpat]=$vaact;
					}
					$newidactiviaddet++;
					$newactiviad_detalle['iddetalle']=$newidactiviaddet;
					$newactiviad_detalle['idactividad']=$newidactiviad;
					$newactiviad_detalle['idpersonal']=$filtros["idpersona"];
					$this->oBD->insert('actividad_detalle', $newactiviad_detalle);
				}
			}

			$sql="SELECT * FROM herramientas WHERE idactividad=".$nivel["idnivel"];
			parent::conectar($filtros["BD"]);
			$herramientas=$this->oBD->consultarSQL($sql);
			$newid=$this->oBD->consultarEscalarSQL("SELECT idtool FROM herramientas ORDER BY idtool DESC limit 0,1");
			//var_dump($herramientas);
			foreach ($herramientas as $cp => $her){
				$newnewidherramienta=array();
				foreach($her as $cher => $vher){
					$newnewidherramienta[$cher]=$vher;
				}
				$newid++;
				$newnewidherramienta['idtool']=$newid;
				$newnewidherramienta['idactividad']=$newnivel["idnivel"];
				$newnewidherramienta['idpersonal']=$filtros["idpersona"];
				$this->oBD->insert('herramientas', $newnewidherramienta);				
			}			
			$this->terminarTransaccion('dat_copiar_curso');
		} catch (Exception $e) {
			$this->truncarTransaccion('dat_copiar_curso');
			throw new Exception("ERROR\n al copiar el Smartbook");
		}
	}

	/*Registrar persona*/
	public function personaingles($datos,$bd){
		try {			
			parent::conectar($bd);		
			$sql="SELECT idpersona FROM personal WHERE  usuario='".$datos["usuario"]."' OR dni='".$datos["dni"]."' LIMIT 0,1 ";
			$idpersona=$this->oBD->consultarEscalarSQL($sql);

			if(!empty($datos["getpersona"]) && !empty($idpersona)){
				$sql="SELECT * FROM personal INNER JOIN persona_rol on idpersona=idpersonal WHERE idpersona=".$datos["iduseringles"];				
				return $this->oBD->consultarSQL($sql);
			}

			if(!empty($idpersona)){
				$sql="SELECT idrol FROM persona_rol WHERE  idpersonal='".$idpersona."' AND idproyecto='3' AND idrol=1 LIMIT 0,1 ";
				$idrol=$this->oBD->consultarEscalarSQL($sql);
				if(empty($idrol)){
					$iddetalle=$this->oBD->consultarEscalarSQL("SELECT iddetalle+1 FROM persona_rol ORDER BY iddetalle DESC limit 0,1");
					if(empty($iddetalle)) $iddetalle=1;
					$estado_rol=array('iddetalle'=>$iddetalle,'idrol'=>1,'idpersonal'=>$idpersona,'idempresa'=>4,'idproyecto'=>3);
					$this->oBD->insert('persona_rol', $estado_rol);
				}
				return $idpersona;				
			}
			


			$idpersona=$this->oBD->consultarEscalarSQL("SELECT idpersona+1 FROM personal ORDER BY idpersona DESC limit 0,1");
			if(empty($idpersona)) $idpersona=1;
			$estado_persona=array('idpersona'=>$idpersona
				,'tipodoc'=>1
				,'dni'=>$datos["dni"]
				,'ape_paterno'=>''
				,'ape_materno'=>''
				,'nombre'=>$datos["nombre_full"]
				,'fechanac'=>date('Y-m-d')
				,'sexo'=>$datos["sexo"]
				,'estado_civil'=>'S'
				,'ubigeo'=>''
				,'urbanizacion'=>''
				,'direccion'=>''
				,'telefono'=>''
				,'celular'=>''
				,'email'=>$datos["email"]
				,'idugel'=>'1'
				,'regusuario'=>0
				,'regfecha'=>date('Y-m-d')
				,'usuario'=>$datos["usuario"]
				,'clave'=>md5($datos["usuario"])
				,'token'=>md5($datos["usuario"])
				//,'rol'=>''
				,'foto'=>''
				,'estado'=>1
				,'situacion'=>1
				,'idioma'=>'EN'
				,'tipousuario'=>'n'
			);
			$this->oBD->insert('personal', $estado_persona);
			$iddetalle=$this->oBD->consultarEscalarSQL("SELECT iddetalle+1 FROM persona_rol ORDER BY iddetalle DESC limit 0,1");
			if(empty($iddetalle)) $iddetalle=1;
			$estado_rol=array('iddetalle'=>$iddetalle,'idrol'=>1,'idpersonal'=>$idpersona,'idempresa'=>4,'idproyecto'=>3);					
			$this->oBD->insert('persona_rol', $estado_rol);
			return $idpersona;
		} catch (Exception $e) {
			//$this->truncarTransaccion('dat_copiar_curso');
			throw new Exception("ERROR\n al copiar el Smartbook". $e->getMessage());
		}
	}

	public function eliminarsmartbook($datos,$bd){
		try {			
			parent::conectar($bd);
			if(!empty($datos["idrecurso"]))
				return $this->oBD->update('acad_cursodetalle', array('idcurso' => 1), array('idrecurso' => $datos["idrecurso"],'idcursodetalle'=>$datos["idcursodetalle"]));
		} catch (Exception $e){			
			throw new Exception("ERROR\n eliminar smartbook ");
		}

	}
}