<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatMin_sesion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Min_sesion") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(idsesion) FROM min_sesion";

			$cond = array();

			if (isset($filtros["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["abrev"])) {
				$cond[] = "abrev = " . $this->oBD->escapar($filtros["abrev"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Min_sesion") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT min_sesion.idsesion, min_sesion.descripcion, min_sesion.abrev FROM min_sesion";

			$cond = array();


			if (isset($filtros["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["abrev"])) {
				$cond[] = "abrev = " . $this->oBD->escapar($filtros["abrev"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Min_sesion") . ": " . $e->getMessage());
		}
	}


	public function insertar($descripcion, $abrev)
	{
		try {

			$this->iniciarTransaccion('dat_min_sesion_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idsesion) FROM min_sesion");
			++$id;

			$estados = array(
				'idsesion' => $id, 'descripcion' => $descripcion, 'abrev' => $abrev
			);

			$this->oBD->insert('min_sesion', $estados);
			$this->terminarTransaccion('dat_min_sesion_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_min_sesion_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Min_sesion") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $descripcion, $abrev)
	{
		try {
			$this->iniciarTransaccion('dat_min_sesion_update');
			$estados = array(
				'descripcion' => $descripcion, 'abrev' => $abrev
			);

			$this->oBD->update('min_sesion ', $estados, array('idsesion' => $id));
			$this->terminarTransaccion('dat_min_sesion_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Min_sesion") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  min_sesion.idsesion, min_sesion.descripcion, min_sesion.abrev FROM min_sesion  "
				. " WHERE idsesion = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Min_sesion") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('min_sesion', array('idsesion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Min_sesion") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('min_sesion', array($propiedad => $valor), array('idsesion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Min_sesion") . ": " . $e->getMessage());
		}
	}
}
