<?php
class DatNegReportesgeneral extends DatBase
{
    public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("reportes").": " . $e->getMessage());
		}
    }
    public function buscar($filtros = array()){
        try{
            $cond = [];
            $sql = "SELECT rg.* FROM reportesgeneral rg";

            if(!empty($filtros['id'])){
                if(is_array($filtros['id'])){
                    $cond[] = "rg.id IN (".implode(',',$filtros['id']).")";
                }else{
                    $cond[] = "rg.id = ".$this->oBD->escapar($filtros['id']);
                }
            }
            if(!empty($filtros['nombre'])){
                $cond[] = "rg.nombre = ".$this->oBD->escapar($filtros['nombre']);
            }
            if(!empty($filtros['likenombre'])){
                $cond[] = "rg.nombre = ".$this->oBD->like($filtros["likenombre"]);
            }
            if(!empty($filtros['descripcion'])){
                $cond[] = "rg.descripcion = ".$this->oBD->escapar($filtros['descripcion']);
            }
            if(!empty($filtros['icono'])){
                $cond[] = "rg.icono = ".$this->oBD->escapar($filtros['icono']);
            }
            if(!empty($filtros['likeicono'])){
                $cond[] = "rg.icono = ".$this->oBD->like($filtros["icono"]);
            }
            if(!empty($filtros['estado'])){
                $cond[] = "rg.estado = ".$this->oBD->escapar($filtros['estado']);
            }
            

            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
            }
            
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("Error en datos- buscar ". $e->getMessage());
        }
    }

    public function insertar($params){
        try{
            $id = 0;
            
            if(empty($params)){ throw new Exception("Se requiere parametros para insertar"); }

            $this->iniciarTransaccion('dat_multiple_insert');
            $id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM reportesgeneral");
            ++$id;
            $params['id'] = $id;
			$this->oBD->insert('reportesgeneral', $params);
            $this->terminarTransaccion('dat_multiple_insert');
            return $id;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("insertar")." ".JrTexto::_("dat").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
        }
    }

}