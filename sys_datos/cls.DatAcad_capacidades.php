<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatAcad_capacidades extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_capacidades").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT cap.idcapacidad,cap.idcompetencia,cap.idcurso,cap.nombre,cap.idproyecto,(SELECT nombre FROM acad_curso c WHERE c.idcurso = cap.idcurso) as nombre_curso, (SELECT nombre FROM acad_competencias co WHERE co.idcompetencia = cap.idcompetencia) as nombre_competencia FROM acad_capacidades cap";			
			
			$cond = array();

			
			if(isset($filtros["idcompetencia"])) {
					$cond[] = "cap.idcompetencia = " . $this->oBD->escapar($filtros["idcompetencia"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "cap.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "cap.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "cap.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}			
			if(isset($filtros["texto"])) {
					$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY nombre ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_capacidades").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcompetencia,$idcurso,$nombre,$idproyecto=0)
	{
		try {
			
			//$this->iniciarTransaccion('dat_acad_capacidades_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT idcapacidad FROM acad_capacidades ORDER BY idcapacidad DESC limit 0,1");
			++$id;
			
			$estados = array('idcapacidad' => $id
							
							,'idcompetencia'=>$idcompetencia
							,'idcurso'=>$idcurso
							,'nombre'=>$nombre
							,'idproyecto'=>$idproyecto						
							);
			
			$this->oBD->insert('acad_capacidades', $estados);			
			//$this->terminarTransaccion('dat_acad_capacidades_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_capacidades_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_capacidades").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcompetencia,$idcurso,$nombre,$idproyecto)
	{
		try {
			//$this->iniciarTransaccion('dat_acad_capacidades_update');
			$estados = array('idcompetencia'=>$idcompetencia
							,'idcurso'=>$idcurso
							,'nombre'=>$nombre
							,'idproyecto'=>$idproyecto							
							);
			
			$this->oBD->update('acad_capacidades ', $estados, array('idcapacidad' => $id));
		    //$this->terminarTransaccion('dat_acad_capacidades_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_capacidades").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.idcapacidad,tb1.idcompetencia,tb1.idcurso,tb1.nombre,tb1.idproyecto,tb2.idcurso AS _idcurso  FROM acad_capacidades tb1 LEFT JOIN acad_curso tb2 ON tb1.idcurso=tb2.idcurso  "
					. " WHERE tb1.idcapacidad = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_capacidades").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_capacidades', array('idcapacidad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_capacidades").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_capacidades', array($propiedad => $valor), array('idcapacidad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_capacidades").": " . $e->getMessage());
		}
	}
   
		
}