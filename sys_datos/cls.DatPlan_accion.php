<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatPlan_accion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Plan_accion").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM plan_accion";			
			
			$cond = array();		
					
			
			if(isset($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Plan_accion").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($valor)
	{
		try {
			
			$this->iniciarTransaccion('dat_plan_accion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM plan_accion");
			++$id;
			
			$estados = array('idpersona' => $id
							
							,'valor'=>$valor							
							);
			
			$this->oBD->insert('plan_accion', $estados);			
			$this->terminarTransaccion('dat_plan_accion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_plan_accion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Plan_accion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $valor)
	{
		try {
			$this->iniciarTransaccion('dat_plan_accion_update');
			$estados = array('valor'=>$valor								
							);
			
			$this->oBD->update('plan_accion ', $estados, array('idpersona' => $id));
		    $this->terminarTransaccion('dat_plan_accion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Plan_accion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM plan_accion  "
					. " WHERE idpersona = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Plan_accion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('plan_accion', array('idpersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Plan_accion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('plan_accion', array($propiedad => $valor), array('idpersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Plan_accion").": " . $e->getMessage());
		}
	}
   
		
}