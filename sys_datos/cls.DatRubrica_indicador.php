<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatRubrica_indicador extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Rubrica_indicador") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT (@i := @i + 1) as orden, 
						rubrica_indicador.idrubrica_indicador, rubrica_indicador.detalle, rubrica_indicador.idrubrica, rubrica_indicador.idrubrica_criterio, rubrica_indicador.idrubrica_nivel, rubrica_indicador.idcriterio 
					FROM rubrica_indicador
						cross join (select @i := 0) rubrica_indicador";

			$cond = array();


			if (isset($filtros["idrubrica_indicador"])) {
				$cond[] = "idrubrica_indicador = " . $this->oBD->escapar($filtros["idrubrica_indicador"]);
			}
			if (isset($filtros["detalle"])) {
				$cond[] = "detalle = " . $this->oBD->escapar($filtros["detalle"]);
			}
			if (isset($filtros["idrubrica"])) {
				$cond[] = "idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if (isset($filtros["idrubrica_criterio"])) {
				$cond[] = "idrubrica_criterio = " . $this->oBD->escapar($filtros["idrubrica_criterio"]);
			}
			if (isset($filtros["idrubrica_nivel"])) {
				$cond[] = "idrubrica_nivel = " . $this->oBD->escapar($filtros["idrubrica_nivel"]);
			}
			if (isset($filtros["idcriterio"])) {
				$cond[] = "idcriterio = " . $this->oBD->escapar($filtros["idcriterio"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Rubrica_indicador") . ": " . $e->getMessage());
		}
	}

	public function insertar($detalle, $idrubrica, $idrubrica_criterio, $idrubrica_nivel, $idcriterio)
	{
		try {

			$this->iniciarTransaccion('dat_rubrica_indicador_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrubrica_indicador) FROM rubrica_indicador");
			++$id;

			$estados = array(
				'idrubrica_indicador' => $id, 'detalle' => $detalle, 'idrubrica' => $idrubrica, 'idrubrica_criterio' => $idrubrica_criterio, 'idrubrica_nivel' => $idrubrica_nivel
				// , 'idcriterio' => $idcriterio
			);
			// echo json_encode($estados);exit();

			$this->oBD->insert('rubrica_indicador', $estados);
			$this->terminarTransaccion('dat_rubrica_indicador_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_rubrica_indicador_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Rubrica_indicador") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $detalle, $idrubrica, $idrubrica_criterio, $idrubrica_nivel, $idcriterio)
	{
		try {
			$this->iniciarTransaccion('dat_rubrica_indicador_update');
			$estados = array(
				'detalle' => $detalle, 'idrubrica' => $idrubrica, 'idrubrica_criterio' => $idrubrica_criterio, 'idrubrica_nivel' => $idrubrica_nivel
				// , 'idcriterio' => $idcriterio
			);

			$this->oBD->update('rubrica_indicador ', $estados, array('idrubrica_indicador' => $id));
			$this->terminarTransaccion('dat_rubrica_indicador_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_indicador") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idrubrica_indicador,detalle,idrubrica,idrubrica_criterio,idrubrica_nivel,idcriterio  FROM rubrica_indicador  "
				. " WHERE idrubrica_indicador = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Rubrica_indicador") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rubrica_indicador', array('idrubrica_indicador' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Rubrica_indicador") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('rubrica_indicador', array($propiedad => $valor), array('idrubrica_indicador' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_indicador") . ": " . $e->getMessage());
		}
	}
}
