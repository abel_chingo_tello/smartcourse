<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-11-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatTb_grupo_trabajo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Tb_grupo_trabajo") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT * FROM tb_grupo_trabajo";

			$cond = array();


			if (isset($filtros["id_grupo_trabajo"])) {
				$cond[] = "id_grupo_trabajo = " . $this->oBD->escapar($filtros["id_grupo_trabajo"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Tb_grupo_trabajo") . ": " . $e->getMessage());
		}
	}
	public function listarConMatriculas($filtros = null)
	{
		try {
			$sql = "SELECT  tb_grupo_trabajo.idgrupoauladetalle, tb_grupo_trabajo.id_grupo_trabajo, tb_grupo_trabajo.nombre nombre_grupo, 
							acad_matricula.idmatricula,acad_matricula.idcomplementario,acad_matricula.idalumno,
							concat(personal.ape_paterno , ' ',personal.ape_materno,', ',personal.nombre ) nombre_alumno
					FROM tb_grupo_trabajo
						left join acad_matricula on tb_grupo_trabajo.id_grupo_trabajo=acad_matricula.id_grupo_trabajo
							and tb_grupo_trabajo.idgrupoauladetalle=acad_matricula.idgrupoauladetalle
						inner join personal on acad_matricula.idalumno=personal.idpersona";

			$cond = array();
			if (isset($filtros["id_grupo_trabajo"])) {
				$cond[] = "id_grupo_trabajo = " . $this->oBD->escapar($filtros["id_grupo_trabajo"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "tb_grupo_trabajo.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "tb_grupo_trabajo.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Tb_grupo_trabajo") . ": " . $e->getMessage());
		}
	}

	public function insertar($idgrupoauladetalle, $nombre)
	{
		try {

			//$this->iniciarTransaccion('dat_tb_grupo_trabajo_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_grupo_trabajo) FROM tb_grupo_trabajo");
			++$id;

			$estados = array(
				'id_grupo_trabajo' => $id, 'idgrupoauladetalle' => $idgrupoauladetalle, 'nombre' => $nombre
			);

			$this->oBD->insert('tb_grupo_trabajo', $estados);
			//$this->terminarTransaccion('dat_tb_grupo_trabajo_insert');			
			return $id;
		} catch (Exception $e) {
			//$this->cancelarTransaccion('dat_tb_grupo_trabajo_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Tb_grupo_trabajo") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupoauladetalle, $nombre)
	{
		try {
			$this->iniciarTransaccion('dat_tb_grupo_trabajo_update');
			$estados = array(
				'idgrupoauladetalle' => $idgrupoauladetalle, 'nombre' => $nombre
			);

			$this->oBD->update('tb_grupo_trabajo ', $estados, array('id_grupo_trabajo' => $id));
			$this->terminarTransaccion('dat_tb_grupo_trabajo_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Tb_grupo_trabajo") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM tb_grupo_trabajo  "
				. " WHERE id_grupo_trabajo = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Tb_grupo_trabajo") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tb_grupo_trabajo', array('id_grupo_trabajo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Tb_grupo_trabajo") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('tb_grupo_trabajo', array($propiedad => $valor), array('id_grupo_trabajo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Tb_grupo_trabajo") . ": " . $e->getMessage());
		}
	}
}
