<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatForos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null) //para la vista visualizar post de cualquier autor, con la interaccion de la persona que visualiza (de existir)
	{
		try {
			$sql = "SELECT f.idpublicacion, f.titulo, f.subtitulo, f.estado, f.contenido, f.fecha_hora, f.idgrupoauladetalle, f.idpersona, f.idcategoria, fi.idforo_interaccion, fi.puntuación as puntuación_usuario, concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as autor 
					FROM foro_publicacion f
						inner join personal p on f.idpersona=p.idpersona
						left join foro_interaccion fi on f.idpublicacion=fi.idpublicacion and fi.idpersona= $filtros[idpersona]
					";


			$cond = array();


			if (isset($filtros["idpublicacion"])) {
				$cond[] = "f.idpublicacion = " . $this->oBD->escapar($filtros["idpublicacion"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY fecha_hora DESC";
			// echo "$sql<hr>";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function listar($filtros = null) //lista todos los post con su titulo y autor para todos los usuarios
	{
		try {

			$sql = "
						SELECT persona_rol.idproyecto, f.idpublicacion, f.titulo, f.subtitulo, f.estado, f.contenido, f.fecha_hora, f.idgrupoauladetalle, f.idpersona, f.idcategoria, concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as autor 
						FROM foro_publicacion f
							inner join personal p on f.idpersona=p.idpersona
							inner join persona_rol on p.idpersona=persona_rol.idpersonal
						
			";
			if (isset($filtros["busqueda"])) {
				$cond[] = "f.titulo " . $this->oBD->like($filtros["busqueda"]);
				$cond[] = "f.subtitulo " . $this->oBD->like($filtros["busqueda"]);
				$cond[] = " concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) " . $this->oBD->like($filtros["busqueda"]);
			}
			if (isset($filtros["idproyecto"])) {
				// $sql .=" WHERE (persona_rol.idproyecto =" . $this->oBD->escapar($filtros["idproyecto"])." OR f.idproyecto=".$this->oBD->escapar($filtros["idproyecto"]).") ";
				$sql .= " WHERE (f.idproyecto=" . $this->oBD->escapar($filtros["idproyecto"]) . ") ";
			}


			if (!empty($cond)) {
				$sql .= " AND ( " . implode(' OR ', $cond) . " ) ";
			}
			$sql .= " group by f.idpublicacion";
			$sql .= " ORDER BY f.fecha_hora DESC";
			// echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function getForoAula($filtros = null) //obtener foros de un docente de un curso determinado
	{
		try {
			//"SELECT g.idgrupoauladetalle from acad_grupoauladetalle g where g.iddocente = 281 and g.idcomplementario = 228"
			$sql = "SELECT p.idpublicacion, p.titulo from foro_publicacion p where p.idpersona = " . $this->oBD->escapar($filtros["idPersona"]) .
				" and idgrupoauladetalle = (SELECT g.idgrupoauladetalle from acad_grupoauladetalle g where g.idcurso = " .
				$this->oBD->escapar($filtros["idCurso"]) . " and g.iddocente = " .
				$this->oBD->escapar($filtros["idPersona"]) . " and g.idcomplementario = " . $this->oBD->escapar($filtros["idComplentario"]) . " )";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function insertar($titulo, $subtitulo, $estado, $contenido, $idpersona, $idgruauladet, $idproyecto)
	{
		try {

			$this->iniciarTransaccion('dat_foros_insert');

			// $id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpublicacion) FROM foro_publicacion");
			// ++$id;

			$estados = array(
				'titulo' => $titulo,
				'subtitulo' => $subtitulo,
				'estado' => $estado,
				'contenido' => $contenido,
				// 'idgrupoauladetalle' => empty($idgruauladet) ? null : $idgruauladet,
				'idpersona' => $idpersona,
				'idproyecto' => $idproyecto
				// 'idcategoria' => null
			);
			if (!empty($idgruauladet)) {
				$estados["idgrupoauladetalle"] = $idgruauladet;
			}

			// var_dump($estados);exit();

			$this->oBD->insert('foro_publicacion', $estados);
			$this->terminarTransaccion('dat_foros_insert');
			// return $id;
			return "ok";
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_foros_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function actualizar($idpublicacion, $titulo, $subtitulo, $estado, $contenido, $idpersona, $idgruauladet)
	{
		try {
			$this->iniciarTransaccion('dat_foros_update');
			$estados = array(
				'titulo' => $titulo, 'subtitulo' => $subtitulo, 'estado' => $estado, 'contenido' => $contenido, 'idgrupoauladetalle' => $idgruauladet, 'idpersona' => $idpersona, 'idcategoria' => 0
			);

			$this->oBD->update('foro_publicacion ', $estados, array('idpublicacion' => $idpublicacion));
			$this->terminarTransaccion('dat_foros_update');
			return $idpublicacion;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT f.idpublicacion, f.titulo, f.subtitulo, f.estado, f.contenido, f.fecha_hora, f.idgrupoauladetalle, f.idpersona, f.idcategoria, concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as autor FROM foro_publicacion f
					inner join personal p on f.idpersona=p.idpersona"
				. " WHERE f.idpublicacion = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('foro_publicacion', array('idpublicacion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('foros', array($propiedad => $valor), array('idforo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}
}
