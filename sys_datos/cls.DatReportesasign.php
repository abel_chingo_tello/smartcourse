<?php
class DatReportesasign extends DatBase
{
    public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Reportesasign").": " . $e->getMessage());
		}
    }
    public function buscar($filtros = array()){
        try{
            $cond = [];
            $sql = "SELECT rap.id, rap.idreportegeneral,rap.idbolsa_empresa,rap.idproyecto,rap.estado,rap.fecha_creacion, rg.nombre AS nombre_reporte, rg.icono,rg.ruta,rg.descripcion, IF(rap.idproyecto IS NULL,'general','especifico') AS tipo FROM reportesasign_proyecto rap INNER JOIN reportesgeneral rg ON rap.idreportegeneral = rg.id";

            if(!empty($filtros['id'])){
                if(is_array($filtros['id'])){
                    $cond[] = "rap.id IN (".implode(',',$filtros['id']).")";
                }else{
                    $cond[] = "rap.id = ".$this->oBD->escapar($filtros['id']);
                }
            }
            if(!empty($filtros['idbolsa_empresa'])){
                $cond[] = "rap.idbolsa_empresa = ".$this->oBD->escapar($filtros['idbolsa_empresa']);
            }
            if(!empty($filtros['idreportegeneral'])){
                $cond[] = "rap.idreportegeneral = ".$this->oBD->like($filtros["idreportegeneral"]);
            }
            if(!empty($filtros['estado'])){
                $cond[] = "rap.estado = ".$this->oBD->escapar($filtros['estado']);
            }
            if(!empty($filtros['idproyecto'])){
                $cond[] = "rap.idproyecto = ".$this->oBD->escapar($filtros['idproyecto']);
            }
            if(!empty($filtros['nullidproyecto'])){
                $cond[] = "rap.idproyecto IS NULL";
            }

            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
            }

            if(!empty($filtros['groupby'])){
                if($filtros['groupby'] == 'idbolsa_empresa'){ $sql .= " GROUP BY rap.idbolsa_empresa"; }
                if($filtros['groupby'] == 'idproyecto'){ $sql .= " GROUP BY rap.idproyecto"; }
                if($filtros['groupby'] == 'idreportegeneral'){ $sql .= " GROUP BY rap.idreportegeneral"; }
            }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("Error en datos- buscar empresa ". $e->getMessage());
        }
    }
    public function insertarproyecto($params){
        try{
            $id = 0;
            
            if(empty($params)){ throw new Exception("Se requiere parametros para insertar"); }

            $this->iniciarTransaccion('dat_multiple_insert');
            $id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM reportesasign_proyecto");
            ++$id;
            $params['id'] = $id;
			$this->oBD->insert('reportesasign_proyecto', $params);
            $this->terminarTransaccion('dat_multiple_insert');
            return $id;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("insertar")." ".JrTexto::_("dat").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
        }
    }
    public function delete($id){
        try{
            $this->iniciarTransaccion('dat_multiple_delete');
            if(!is_array($id)){
                $id = [$id];
            }
            foreach($id as $v){
                $this->oBD->delete('reportesasign_proyecto', array('id' => $v));
            }
            $this->terminarTransaccion('dat_multiple_delete');
            return $id;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_delete');
			throw new Exception("ERROR\n".JrTexto::_("delete")." ".JrTexto::_("dat").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
        }
    }

}