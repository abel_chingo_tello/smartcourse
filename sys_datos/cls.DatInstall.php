<?php
class DatInstall extends DatBase{
    public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Install").": " . $e->getMessage());
		}
    }
    public function transacInsert($list){
        $v = '';
        try{
            $this->iniciarTransaccion('dat_install_insert');
			foreach($list as $value){
                $v = $value;
                $this->oBD->ejecutarSQL($value);
            }
			$this->terminarTransaccion('dat_install_insert');
        }catch(Exception $e){
            $this->cancelarTransaccion('dat_install_insert');
            var_dump($list);
            echo base64_encode($v);
            echo "<br>";
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Install").": " . $e->getMessage());
        }
    }
}

?>