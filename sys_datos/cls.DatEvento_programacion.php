<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-04-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatEvento_programacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Evento_programacion") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT '1' tipo_evento, evento_programacion.*, concat(personal.ape_paterno,' ', personal.ape_materno,', ', personal.nombre) autor,  
						   acad_grupoaula.nombre nombre_grupo, acad_curso.nombre nombre_curso
					FROM evento_programacion
						inner join personal on evento_programacion.idpersona=personal.idpersona
						left join acad_grupoaula on evento_programacion.idgrupoaula=acad_grupoaula.idgrupoaula
						left join acad_grupoauladetalle on evento_programacion.idgrupoauladetalle=acad_grupoauladetalle.idgrupoauladetalle
						left join acad_curso on acad_grupoauladetalle.idcurso=acad_curso.idcurso";

			$cond = array();
			if (isset($filtros["end"]) && isset($filtros["start"])) {
				$cond[] = "fecha between " . $this->oBD->escapar($filtros["start"]) . " and " . $this->oBD->escapar($filtros["end"]);
			}
			if (isset($filtros["id_programacion"])) {
				$cond[] = "id_programacion = " . $this->oBD->escapar($filtros["id_programacion"]);
			}
			if (isset($filtros["fecha"])) {
				$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if (isset($filtros["titulo"])) {
				$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if (isset($filtros["detalle"])) {
				$cond[] = "detalle = " . $this->oBD->escapar($filtros["detalle"]);
			}
			if (isset($filtros["hora_comienzo"])) {
				$cond[] = "hora_comienzo = " . $this->oBD->escapar($filtros["hora_comienzo"]);
			}
			if (isset($filtros["hora_fin"])) {
				$cond[] = "hora_fin = " . $this->oBD->escapar($filtros["hora_fin"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "evento_programacion.idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if (isset($filtros["range"])) {
				$cond[] = "evento_programacion.idpersona  in ($filtros[range] )";
			}
			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY fecha ASC, hora_comienzo asc";
			// echo $sql;
			// exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Evento_programacion") . ": " . $e->getMessage());
		}
	}


	public function insertar($fecha, $titulo, $detalle, $hora_comienzo, $hora_fin, $idpersona, $idgrupoaula, $idgrupoauladetalle)
	{
		try {

			$this->iniciarTransaccion('dat_evento_programacion_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_programacion) FROM evento_programacion");
			++$id;

			$estados = array(
				'id_programacion' => $id, 'fecha' => $fecha, 'titulo' => $titulo, 'detalle' => $detalle, 'hora_comienzo' => $hora_comienzo, 'hora_fin' => $hora_fin, 'idpersona' => $idpersona, 'idgrupoaula' => $idgrupoaula, 'idgrupoauladetalle' => $idgrupoauladetalle
			);

			$this->oBD->insert('evento_programacion', $estados);
			$this->terminarTransaccion('dat_evento_programacion_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_evento_programacion_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Evento_programacion") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $fecha, $titulo, $detalle, $hora_comienzo, $hora_fin, $idpersona, $idgrupoaula, $idgrupoauladetalle)
	{
		try {
			$this->iniciarTransaccion('dat_evento_programacion_update');
			$estados = array(
				'fecha' => $fecha, 'titulo' => $titulo, 'detalle' => $detalle, 'hora_comienzo' => $hora_comienzo, 'hora_fin' => $hora_fin, 'idpersona' => $idpersona, 'idgrupoaula' => $idgrupoaula, 'idgrupoauladetalle' => $idgrupoauladetalle
			);

			$this->oBD->update('evento_programacion ', $estados, array('id_programacion' => $id));
			$this->terminarTransaccion('dat_evento_programacion_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Evento_programacion") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  evento_programacion.id_programacion, evento_programacion.fecha, evento_programacion.titulo, evento_programacion.detalle, evento_programacion.hora_comienzo, evento_programacion.hora_fin, evento_programacion.idpersona, evento_programacion.idgrupoaula, evento_programacion.idgrupoauladetalle  FROM evento_programacion  "
				. " WHERE id_programacion = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Evento_programacion") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('evento_programacion', array('id_programacion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Evento_programacion") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('evento_programacion', array($propiedad => $valor), array('id_programacion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Evento_programacion") . ": " . $e->getMessage());
		}
	}
}
