<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		06-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_grupoauladetalle extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM acad_grupoauladetalle";
			
			$cond = array();		
			
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = " (select orden from proyecto_cursos WHERE idproyecto=ag.idproyecto AND idcurso=agd.idcurso limit 0,1) as ordencursoproyecto , 
			ag.idgrupoaula, ag.idcategoria, ag.idlocal, ag.idproducto, ag.tipodecurso, cu.imagen, cu.descripcion, cu.estado, cu.fecharegistro, cu.idusuario, cu.vinculosaprendizajes, cu.materialesyrecursos, cu.color, cu.objetivos, cu.certificacion, cu.costo, cu.silabo, cu.e_ini, cu.pdf, cu.abreviado, cu.e_fin, cu.aniopublicacion, cu.autor, cu.txtjson, cu.formula_evaluacion,  cu.sks, cu.idioma, cu.avance_secuencial, cu.tipo as tipocurso,ag.nombre as strgrupoaula, ag.idproyecto,comentario,nvacantes, idgrupoauladetalle, agd.nombre ,agd.fecha_inicio,agd.fecha_final, ag.tipo, 
			(SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, ag.estado, 
			(SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, 
			cu.idcurso, cu.nombre as strcurso ,IFNULL(acad_curso_complementario.nombre,cu.nombre) as nombrecurso,IFNULL(acad_curso_complementario.imagen,cu.`imagen`) as imagen, iddocente, (SELECT dni FROM personal pe WHERE pe.dni=agd.iddocente OR pe.idpersona=agd.iddocente Limit 1) AS dnidocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE pe.dni=agd.iddocente OR pe.idpersona=agd.iddocente Limit 1) AS strdocente, agd.idlocal, l.nombre AS strlocal,l.idugel, idambiente,agd.idgrado ,agd.idsesion, agd.dias_extradocente,
			(SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente,
			(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='U') as nunidades,
			(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='L') as nactividades,
			(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='N') as nnieles,
			(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='M') as nmenus,
			(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='E') as nexamenes, agd.idcomplementario, sac.codsence,agd.dias_extradocente
			FROM acad_grupoaula ag 
			INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula 
			left join acad_curso_complementario on agd.idcomplementario=acad_curso_complementario.idcurso
			Right JOIN acad_curso cu ON cu.idcurso=agd.idcurso 
			LEFT JOIN local l ON agd.idlocal=l.idlocal
			LEFT JOIN sence_assigncursos sac ON agd.idsence_assigncursos = sac.id";

			if(isset($filtros["sql2"])){
				$sql = " (select orden from proyecto_cursos WHERE idproyecto=ag.idproyecto AND idcurso=agd.idcurso limit 0,1) as ordencursoproyecto , ag.idgrupoaula, ag.idcategoria, ag.idlocal, ag.idproducto, ag.tipodecurso, cu.imagen, cu.descripcion, cu.estado, cu.fecharegistro, cu.idusuario, cu.vinculosaprendizajes, cu.materialesyrecursos, cu.color, cu.objetivos, cu.certificacion, cu.costo, cu.silabo, cu.e_ini, cu.pdf, cu.abreviado, cu.e_fin, cu.aniopublicacion, cu.autor, cu.txtjson, cu.formula_evaluacion,  cu.sks, cu.idioma, cu.avance_secuencial ,ag.nombre as strgrupoaula, ag.idproyecto,comentario,nvacantes, idgrupoauladetalle, agd.nombre ,agd.fecha_inicio,agd.fecha_final, ag.tipo, 
					(SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, ag.estado, 
					(SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, 
					cu.idcurso, cu.nombre as strcurso ,IFNULL(acad_curso_complementario.nombre,cu.nombre) as nombrecurso, iddocente, (SELECT dni FROM personal pe WHERE pe.dni=agd.iddocente OR pe.idpersona=agd.iddocente Limit 1) AS dnidocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE pe.dni=agd.iddocente OR pe.idpersona=agd.iddocente Limit 1) AS strdocente, agd.idlocal,l.nombre AS strlocal,l.idugel, idambiente,agd.idgrado ,agd.idsesion , agd.idcomplementario
					FROM acad_grupoaula ag 
						INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula 
						left join acad_curso_complementario on agd.idcomplementario=acad_curso_complementario.idcurso
						Right JOIN acad_curso cu ON cu.idcurso=agd.idcurso 
						LEFT JOIN local l ON agd.idlocal=l.idlocal ";
			}
			if(isset($filtros["sql3"])){
				$sql = " (select orden from proyecto_cursos WHERE idproyecto=ag.idproyecto AND idcurso=agd.idcurso limit 0,1) as ordencursoproyecto , agd.idgrupoauladetalle,agd.idgrupoaula,agd.idcurso,agd.iddocente,agd.idlocal,agd.idambiente,agd.nombre,agd.fecha_inicio,agd.fecha_final,agd.idgrado,agd.idsesion,agd.fechamodificacion,agd.orden,ag.nombre,ag.idproyecto, agd.idcomplementario FROM acad_grupoauladetalle agd LEFT JOIN acad_grupoaula ag ON agd.idgrupoaula = ag.idgrupoaula";
			}

		    $cond = array();
		    if(!empty($filtros["idcategoria"])) {
				$sql='SELECT acc.idcategoria, '.$sql." LEFT JOIN acad_cursocategoria acc ON  agd.idcurso=acc.idcurso ";
				$cond[] = "acc.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}else{
				$sql='SELECT '.$sql;
			}

			if(isset($filtros["sql4"])){
				$sql = "SELECT agd.idgrupoauladetalle,agd.idgrupoaula,agd.idcurso,agd.iddocente,agd.idlocal,agd.idambiente,agd.nombre,agd.fecha_inicio,agd.fecha_final,agd.idgrado,agd.idsesion,agd.fechamodificacion,agd.idcomplementario,agd.orden FROM acad_grupoauladetalle agd";
				$cond = array();
			}elseif(isset($filtros["sql5"])){
				$sql = "SELECT agd.idgrupoauladetalle,agd.idgrupoaula,agd.idcurso,agd.iddocente,agd.idlocal,agd.idambiente,agd.nombre,agd.fecha_inicio,agd.fecha_final,agd.idgrado,agd.idsesion,agd.fechamodificacion,agd.idcomplementario,agd.orden,ag.nombre as strgrupoaula FROM acad_grupoauladetalle agd INNER JOIN acad_grupoaula ag on ag.idgrupoaula=agd.idgrupoaula ";
				$cond = array();
			}
			

			if(isset($filtros["tipo"])) {
					$cond[] = "ag.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idproducto"])) {
				$cond[] = "ag.idproducto = " . $this->oBD->escapar($filtros["idproducto"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "ag.estado = " . $this->oBD->escapar($filtros["estado"]);
			}		
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idcomplementario"])) {
					$cond[] = "agd.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
				if(is_array($filtros["idcurso"])){
					$cond[] = "agd.idcurso IN (" . implode(",", $filtros["idcurso"]) . ")";
				} else {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "agd.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "agd.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "agd.fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "agd.fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["fecha_inicio_mayorigual"])) {
					$cond[] = "agd.fecha_inicio >= " . $this->oBD->escapar($filtros["fecha_inicio_mayorigual"]);
			}
			if(isset($filtros["fecha_inicio_menorigual"])) {
					$cond[] = "agd.fecha_inicio <= " . $this->oBD->escapar($filtros["fecha_inicio_menorigual"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "agd.fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idgrado"])) {
				$cond[] = "agd.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "agd.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}	
			if(isset($filtros["fecha_final_menorigual"])) {
					$cond[] = "agd.fecha_final <= " . $this->oBD->escapar($filtros["fecha_final_menorigual"]);
			}	
			if(isset($filtros["fecha_final_mayorigual"])) {
					$cond[] = "agd.fecha_final >= " . $this->oBD->escapar($filtros["fecha_final_mayorigual"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	

			if(!empty($filtros["orderby"])){
				if($filtros["orderby"]!='no'){
					if($filtros["orderby"]=='orden') $sql .=" ORDER BY ordencursoproyecto ";
					//elseif($filtros["orderby"]=='nombre') $sql .=" ORDER BY stralumno ASC ";
					else $sql .=" ORDER BY ".$filtros["orderby"];
				}
			}else $sql .= " ORDER BY ordencursoproyecto ASC";

			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql . "<br>";
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function buscargruposestudios($filtros = null){
		try{
		    $cond = array();

			$sql = "SELECT agd.*, ag.nombre AS nombregrupo, c.nombre AS nombrecurso, cc.nombre AS nombrecursocomplementario FROM acad_grupoauladetalle agd INNER JOIN acad_grupoaula ag ON ag.idgrupoaula = agd.idgrupoaula LEFT JOIN acad_curso c ON agd.idcurso = c.idcurso LEFT JOIN acad_curso_complementario cc ON agd.idcomplementario = cc.idcurso";
			
			if(isset($filtros["tipo"])) {
				$cond[] = "ag.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idproducto"])) {
				$cond[] = "ag.idproducto = " . $this->oBD->escapar($filtros["idproducto"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "ag.estado = " . $this->oBD->escapar($filtros["estado"]);
			}		
			if(isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idcomplementario"])) {
				$cond[] = "agd.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if(isset($filtros["idgrupoaula"])) {
				$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
				if(is_array($filtros["idcurso"])){
					$cond[] = "agd.idcurso IN (" . implode(",", $filtros["idcurso"]) . ")";
				} else {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
			}
			if(isset($filtros["iddocente"])) {
				$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
				$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
				$cond[] = "agd.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "agd.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_inicio"])) {
				$cond[] = "agd.fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
				$cond[] = "agd.fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["fecha_inicio_mayorigual"])) {
				$cond[] = "agd.fecha_inicio >= " . $this->oBD->escapar($filtros["fecha_inicio_mayorigual"]);
			}
			if(isset($filtros["fecha_inicio_menorigual"])) {
				$cond[] = "agd.fecha_inicio <= " . $this->oBD->escapar($filtros["fecha_inicio_menorigual"]);
			}
			if(isset($filtros["fecha_final"])) {
				$cond[] = "agd.fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idgrado"])) {
				$cond[] = "agd.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "agd.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["fecha_final_menorigual"])) {
				$cond[] = "agd.fecha_final <= " . $this->oBD->escapar($filtros["fecha_final_menorigual"]);
			}
			if(isset($filtros["fecha_final_mayorigual"])) {
				$cond[] = "agd.fecha_final >= " . $this->oBD->escapar($filtros["fecha_final_mayorigual"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	

			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("buscargruposestudios")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function buscarGrupoAulaDetalle($filtros=null)
	{
		try {
			$sql = "SELECT ag.idgrupoaula, ag.tipo, ag.comentario, ag.nvacantes, ag.estado, ag.idproyecto, ag.idgrado, ag.idsesion, ag.idcategoria,ag.fecha_inicio, ag.fecha_fin, ag.idproducto, ag.tipodecurso , cu.idcurso, cu.nombre, cu.imagen, cu.descripcion, cu.estado, cu.fecharegistro, cu.idusuario, cu.vinculosaprendizajes, cu.materialesyrecursos, cu.color, cu.objetivos, cu.certificacion, cu.costo, cu.silabo, cu.e_ini, cu.pdf, cu.abreviado, cu.e_fin, cu.aniopublicacion, cu.autor, cu.txtjson, cu.formula_evaluacion, cu.tipo, cu.sks, cu.idioma, cu.avance_secuencial ,ag.nombre as strgrupoaula, idproyecto,comentario,nvacantes, idgrupoauladetalle, agd.nombre ,agd.fecha_inicio,agd.fecha_final, ag.tipo, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, ag.estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, 
					cu.idcurso, cu.nombre as strcurso , iddocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE pe.dni=agd.iddocente OR pe.idpersona=agd.iddocente Limit 1) AS strdocente, agd.idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente,agd.idgrado ,agd.idsesion, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente,
					(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='U') as nunidades,
					(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='L') as nactividades,
					(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='N') as nnieles,
					(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='M') as nmenus,
					(select count(idcurso) FROM acad_cursodetalle WHERE idcurso=agd.idcurso AND tiporecurso='E') as nexamenes
					FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula Right JOIN acad_curso cu ON cu.idcurso=agd.idcurso ";

		    $cond = array();
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "agd.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["fecha_inicio_mayorigual"])) {
					$cond[] = "fecha_inicio >= " . $this->oBD->escapar($filtros["fecha_inicio_mayorigual"]);
			}
			if(isset($filtros["fecha_inicio_menorigual"])) {
					$cond[] = "fecha_inicio <= " . $this->oBD->escapar($filtros["fecha_inicio_menorigual"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idgrado"])) {
				$cond[] = "agd.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "agd.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}	
			if(isset($filtros["fecha_final_menorigual"])) {
					$cond[] = "fecha_final <= " . $this->oBD->escapar($filtros["fecha_final_menorigual"]);
			}	
			if(isset($filtros["fecha_final_mayorigual"])) {
					$cond[] = "fecha_final >= " . $this->oBD->escapar($filtros["fecha_final_mayorigual"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}						
			//$sql .= " ORDER BY fecha_creado ASC";
			//  echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("buscarGrupoAulaDetalle")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	
	public function mialumnos($filtros = null){
		try{
			$sql = "SELECT ma.idmatricula, ma.idgrupoauladetalle, ma.idalumno, ma.fecha_registro, ma.estado, ma.idusuario, ma.fecha_matricula, ma.fecha_vencimiento, ma.notafinal, ma.formulajson, ma.fecha_termino, ma.pagorecurso, ma.tipomatricula, ma.idcomplementario, ma.idcategoria, (SELECT CONCAT(pe.nombre,\" \", pe.ape_paterno) FROM personal pe WHERE pe.idpersona = ma.idalumno) AS nombreAlumno, (SELECT pe.email FROM personal pe WHERE pe.idpersona = ma.idalumno) AS email,(SELECT pe.emailpadre FROM personal pe WHERE pe.idpersona = ma.idalumno) AS emailpadre FROM acad_grupoauladetalle gad INNER JOIN acad_matricula ma ON ma.idgrupoauladetalle = gad.idgrupoauladetalle ";

			$cond = array();
			if(isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "gad.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "gad.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["estadomatricula"])) {
				$cond[] = "ma.estado = " . $this->oBD->escapar($filtros["estadomatricula"]);
			}
			if(isset($filtros["idgrado"])) {
				$cond[] = "gad.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "gad.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "gad.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	

			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("mialumnos")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function micolegio($filtros=null){
		try {
			$sql = "SELECT idgrupoauladetalle, ag.idcurso,ac.nombre AS strcurso,iddocente,(SELECT CONCAT(ape_paterno,' ',ape_materno,', ',nombre) FROM personal WHERE idpersona=iddocente LIMIT 0,1 ) AS docente,idlocal,(SELECT nombre FROM local WHERE idlocal=ag.idlocal LIMIT 0,1 ) AS striiee ,ag.idgrado,(SELECT descripcion FROM min_grado WHERE idgrado=ag.idgrado LIMIT 0,1 ) AS grado,idsesion,(SELECT descripcion FROM min_sesion WHERE idsesion=ag.idsesion LIMIT 0,1 ) AS seccion FROM acad_grupoauladetalle as ag INNER JOIN acad_curso ac ON ac.idcurso=ag.idcurso INNER JOIN proyecto_cursos pc ON ag.idcurso=pc.idcurso";
			$cond = array();
			if(isset($filtros["idproyecto"])) {
					$cond[] = "pc.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}else $cond[] = "pc.idproyecto = 3";
			if(isset($filtros["estado"])) {
				$cond[] = "ac.estado = " . $this->oBD->escapar($filtros["estado"]);
			}else $cond[] = "ac.estado = 1";
			
			if(isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "ag.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
				$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
				$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idgrado"])) {
				$cond[] = "ag.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "ag.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .="ORDER BY striiee ASC , strcurso ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("micolegio")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}

	}

	public function cursosDocente($filtros=null)
	{
		try {
			$sql = "SELECT GAD.idgrupoauladetalle, GAD.iddocente, GAD.fecha_inicio, GAD.fecha_final, C.idcurso, C.nombre, C.imagen, C.descripcion, C.estado, C.fecharegistro, C.idusuario, C.vinculosaprendizajes, C.materialesyrecursos, C.color, C.objetivos, C.certificacion, C.costo, C.silabo, C.e_ini, C.pdf, C.abreviado, C.e_fin, C.aniopublicacion, C.autor, C.txtjson, C.formula_evaluacion, C.tipo, C.sks, C.idioma, C.avance_secuencial, PC.idproyecto FROM acad_grupoauladetalle GAD JOIN acad_curso C ON GAD.idcurso=C.idcurso JOIN proyecto_cursos PC ON PC.idcurso=C.idcurso";	
			
			$cond = array();

			if(isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "GAD.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
				$cond[] = "GAD.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "PC.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
				$cond[] = "GAD.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
				$cond[] = "GAD.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
				$cond[] = "GAD.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "PC.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			//$cond[] = " GA.estado = 1 ";
			$cond[] = " C.estado = 1 ";
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo '<p><b>' . $sql . '</b></p>';
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("cursosDocente")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}

	public function anios(){
		try{
			$sql='SELECT DISTINCT(YEAR(fecha_inicio)) as anios FROM `acad_grupoauladetalle` Order By anios DESC';
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("anios")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function filtrogrupos($filtros){
		try{
			$sql='SELECT DISTINCT(ag.idgrupoaula),ag.nombre as strgrupoaula, agd.nombre ,year(fecha_inicio) as anio, tipo, estado, idlocal FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula';

			$cond = array();
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}

			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}

			if(isset($filtros["anio"])) {
					$cond[] = "year(fecha_inicio) = " . $this->oBD->escapar($filtros["anio"]);
			}

			if(isset($filtros["idproyecto"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(!empty($cond)){
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql; exit();
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("filtrogrupos")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function mis_ambientes($filtros){
		try{
			$sql="SELECT DISTINCT A.idambiente, A.idlocal, A.numero, A.capacidad, A.tipo, A.estado, A.turno, A.idproyecto FROM  acad_grupoaula GA INNER JOIN acad_grupoauladetalle GAD ON GA.idgrupoaula = GAD.idgrupoaula INNER JOIN ambiente A ON GAD.idambiente = A.idambiente";

			$cond = array();
			if(isset($filtros["tipo"])) {
					$cond[] = "GA.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}

			if(isset($filtros["estado"])) {
					$cond[] = "GA.estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if(isset($filtros["idlocal"])) {
					$cond[] = "GAD.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}

			if(isset($filtros["idambiente"])) {
					$cond[] = "GAD.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}

			if(isset($filtros["anio"])) {
					$cond[] = "year(GAD.fecha_inicio) = " . $this->oBD->escapar($filtros["anio"]);
			}

			if(!empty($cond)){
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//echo $sql;

			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("mis_ambientes")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function vermarcacion($filtros){
		try{
			$sql="SELECT ag.idgrupoaula,ag.nombre AS strgrupoaula, agd.idgrupoauladetalle, agd.nombre ,agd.fecha_inicio,agd.fecha_final, tipo, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, idcurso, (SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , iddocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE agd.iddocente=dni) AS strdocente, idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente,hg.fecha_finicio as hora_entrada, hg.fecha_final as hora_salida,hg.idhorario FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula LEFT JOIN acad_horariogrupodetalle hg on agd.idgrupoauladetalle=hg.idgrupoauladetalle";
			
			if(isset($filtros["iddocente"])) {
					$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			//$order='';
			if(isset($filtros["fechainicio"])){
					$fechamayor=new DateTime($filtros["fechainicio"]);
					$fechamayor->modify('+15 minutes');
					$f1=$fechamayor->format('Y-m-d H:i:s');
					$fechamenor=new DateTime($filtros["fechainicio"]);
					$fechamenor->modify('-15 minutes');
					$f2=$fechamenor->format('Y-m-d H:i:s');
					$cond[] = "(hg.fecha_finicio between " . $this->oBD->escapar($f2)." AND " . $this->oBD->escapar($f1).")";
			}

			if(isset($filtros["fechafinal"])){
					$fechamayor=new DateTime($filtros["fechafinal"]);
					$fechamayor->modify('+5 minutes');
					$f1=$fechamayor->format('Y-m-d H:i:s');
					$fechamenor=new DateTime($filtros["fechafinal"]);
					$fechamenor->modify('-5 minutes');
					$f2=$fechamenor->format('Y-m-d H:i:s');
					$cond[] = "(hg.fecha_final between " . $this->oBD->escapar($f2)." AND " . $this->oBD->escapar($f1).")";
			}

			if(isset($filtros["fechanext"])){
					$f1=$filtros["fechanext"];					
					$cond[] = "hg.fecha_final > " . $this->oBD->escapar($f1);
			}

			$cond[] = "estado =1 ";
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//echo $sql;
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("vermarcacion")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
		
	public function insertar($idgrupoaula,$idcurso,$iddocente,$idlocal,$idambiente,$nombre,$fecha_inicio,$fecha_final,$idgrado=0,$idseccion=0,$tipodecurso_,$idcategoria,$idsence_assigncursos,$dias_extradocente=null)
	{
		try {			
			// var_dump($tipodecurso_);exit();
			$this->iniciarTransaccion('dat_acad_grupoauladetalle_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoauladetalle) FROM acad_grupoauladetalle");
			++$id;			
			$estados = array(
				'idgrupoauladetalle' => $id							
				,'idgrupoaula'=>$idgrupoaula
				,'idcurso'=>$idcurso
				,'iddocente'=>$iddocente
				,'idlocal'=>$idlocal
				,'idambiente'=>$idambiente
				,'nombre'=>$nombre
				,'fecha_inicio'=>$fecha_inicio
				,'fecha_final'=>$fecha_final
				,'idgrado'=>$idgrado
				,'idsesion'=>$idseccion
			);
			
			if(!empty($idsence_assigncursos)){ $estados['idsence_assigncursos'] = $idsence_assigncursos; }
			if(!empty($dias_extradocente)){ $estados["dias_extradocente"] = $dias_extradocente; }
			
			$estados = NegTools::_empty($estados);
			$this->oBD->insert('acad_grupoauladetalle', $estados);
			// var_dump($tipodecurso_);
			$sql = "SELECT MAX(fecha_final) as fecha_fin FROM acad_grupoauladetalle WHERE idgrupoaula = " . $this->oBD->escapar($idgrupoaula);
			$fecha_fin = $this->oBD->consultarSQL($sql)[0]["fecha_fin"];
			// var_dump($fecha_fin);
			$estados = array("fecha_fin" => $fecha_fin);
			$this->oBD->update('acad_grupoaula', $estados, array('idgrupoaula' => $idgrupoaula));
			// var_dump("tipodecurso_",$tipodecurso_);
			if($tipodecurso_){
				$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.vinculosaprendizajes,c.materialesyrecursos,c.color,c.objetivos,c.certificacion,c.costo,c.silabo,c.e_ini,c.pdf,c.abreviado,c.e_fin,c.aniopublicacion,c.autor,c.txtjson,c.formula_evaluacion,c.tipo,c.sks,c.idioma,c.avance_secuencial FROM acad_curso c";
				$cond = array();
				$cond[] = "c.tipo = " . $this->oBD->escapar(2);
				$cond[] = "c.idcurso = " . $this->oBD->escapar($idcurso);
				// $cond[] = "ca.idcategoria = " . $this->oBD->escapar($idcategoria);
				if(!empty($cond)) {
					$sql .= " WHERE " . implode(' AND ', $cond);
				}
				$complementarios = $this->oBD->consultarSQL($sql);
				// var_dump("complementarios",$complementarios);
				// var_dump("complementarios_",count($complementarios));
				// var_dump($complementarios);exit();
				if(count($complementarios) > 0){
					$sql = "SELECT ga.idgrupoaula,ga.nombre,ga.tipo,ga.comentario,ga.nvacantes,ga.estado,ga.idproyecto,ga.idgrado,ga.idsesion,ga.idcategoria,ga.idlocal,ga.fecha_inicio,ga.fecha_fin,ga.idproducto,ga.tipodecurso FROM acad_grupoaula ga";
					$cond = array();
					$cond[] = "ga.idgrupoaula = " . $this->oBD->escapar($idgrupoaula);
					if(!empty($cond)) {
						$sql .= " WHERE " . implode(' AND ', $cond);
					}
					$grupoaula = $this->oBD->consultarSQL($sql)[0];
					// foreach ($complementarios as $key => $complementario) {
					// 	var_dump("aa",$complementario);
					// }
					// var_dump($complementarios);exit();
					foreach ($complementarios as $key => $complementario) {
						// var_dump("key",$key);
						$idcurso = $this->oBD->consultarEscalarSQL("SELECT MAX(idcurso) FROM acad_curso_complementario");
						++$idcurso;
		
						$idcurso_ = $complementario["idcurso"];
						$complementario["idcurso"] = $idcurso;
						$complementario["idcursoprincipal"] = $idcurso_;
						$complementario["idgrupoaula"] = $idgrupoaula;
						$complementario["idcategoria"] = $idcategoria;
						$complementario["nombre"] .= " " . $grupoaula["nombre"];
						$complementario["e_fin"] = (empty($complementario["e_fin"]) ? 0 : $complementario["e_fin"]);
						$complementario["fecharegistro"] = date("Y-m-d H:i:s");
						unset($complementario["sks"]);
						// var_dump($complementario);
						// $txtjson = $complementario["txtjson"];
						// $origen = RUTA_BASE . "static" . SD . "media" . SD . "cursos" . SD . "curso_" . $idcurso_;
						// $destino = RUTA_BASE . "static" . SD . "media" . SD . "cursos_complementarios" . SD . "curso_" . $idcurso;
						// NegTools::full_copy($origen, $destino);
						$complementario = NegTools::_empty($complementario);
						// var_dump($complementario);
						$complementario["abreviado"] = !isset($complementario["abreviado"]) ? "" : $complementario["abreviado"];
						$this->oBD->insert('acad_curso_complementario', $complementario);
	
						$sql = "SELECT idcursodetalle,idcurso,orden,idrecurso,tiporecurso,idlogro,url,idpadre,color,esfinal,txtjson,espadre FROM acad_cursodetalle WHERE idcurso = " . $this->oBD->escapar($idcurso_);
						$cursodetalle = $this->oBD->consultarSQL($sql);
						// var_dump("cursodetalle",count($cursodetalle));
						// var_dump($cursodetalle);exit();
						$det = array();
						// foreach ($cursodetalle as $key_ => $value) {
						// 	$det[$value["idcursodetalle"]] = $idcursodetalle;
						// }

						// var_dump("det_",$det);

						foreach ($cursodetalle as $key_ => $value){
							// $id_ = $value["idcursodetalle"];
							// $i_ = array_search($value["idpadre"], array_column($det, "id_"));
							// if($i_ !== false){
							// 	$value["idpadre"] = $det[$i_]["idcursodetalle"];
							// }
							$idcursodetalle = $this->oBD->consultarEscalarSQL("SELECT MAX(idcursodetalle) FROM acad_cursodetalle_complementario");
							++$idcursodetalle;
							// var_dump("exist", array_key_exists($value["idcursodetalle"], $det));
							// var_dump("isset", isset($det[$value["idcursodetalle"]]));
							// if(!array_key_exists($value["idcursodetalle"], $det)){
							$det[$value["idcursodetalle"]] = $idcursodetalle;
							// var_dump("det_",$det);
							// }
							$value["idcursodetalle"] = $idcursodetalle;
							$value["idcurso"] = $idcurso;
							$nivel = $this->oBD->consultarSQL("SELECT idnivel, nombre, tipo, idpadre, idpersonal, estado, orden, imagen, descripcion FROM niveles WHERE idnivel = " . $value["idrecurso"])[0];
							$idnivel = $this->oBD->consultarEscalarSQL("SELECT MAX(idnivel) FROM niveles_complementario");
							++$idnivel;
							$nivel["idnivel"] = $idnivel;
							$value["idrecurso"] = $idnivel;

							// var_dump("ip ".$value["idpadre"]);
							// if($value["idpadre"] != 0){
							// 	// if(!array_key_exists($value["idpadre"], $det)){
							// 		$value["idpadre"] = $det[$value["idpadre"]];
							// 	// }
							// }
							// var_dump($nivel);
							$this->oBD->insert('niveles_complementario', $nivel);
							$this->oBD->insert('acad_cursodetalle_complementario', $value);
							// $value["id_"] = $id_;
							// $cursodetalle[$key] = $value;
							// array_push($det, $value);
							$cursodetalle[$key_] = $value;
						}
						// var_dump("det",$det);
						foreach ($cursodetalle as $key_ => $value) {
							if($value["idpadre"] != 0){
								// if(!array_key_exists($value["idpadre"], $det)){
									$value["idpadre"] = $det[$value["idpadre"]];
								// }
								$this->oBD->update('acad_cursodetalle_complementario', array("idpadre" => $value["idpadre"]), array('idcursodetalle' => $value["idcursodetalle"]));
							}
						}
						// var_dump("det",$det);
						// var_dump("det",$det);exit();
						$this->oBD->update('acad_grupoauladetalle', array("idcomplementario" => $idcurso), array('idgrupoauladetalle' => $id));
					}
				}
			}
			$this->terminarTransaccion('dat_acad_grupoauladetalle_insert');			
			return $id;
		} catch(Exception $e){
			$this->cancelarTransaccion('dat_acad_grupoauladetalle_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage() . " - " . $e->getLine());
		}
	}
	public function actualizar($id, $idgrupoaula,$idcurso,$iddocente,$idlocal,$idambiente,$nombre,$fecha_inicio,$fecha_final,$idgrado=0,$idseccion=0,$idsence_assigncursos = null,$dias_extradocente = null)
	{
		try {
			$this->iniciarTransaccion('dat_acad_grupoauladetalle_update');
			$estados = array('idgrupoaula'=>$idgrupoaula
							,'idcurso'=>$idcurso
							,'iddocente'=>$iddocente
							,'idlocal'=>$idlocal
							,'idambiente'=>$idambiente
							,'nombre'=>$nombre
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'idgrado'=>$idgrado
							,'idsesion'=>$idseccion
							);
			if(!empty($dias_extradocente)){
				$estados["dias_extradocente"] = $dias_extradocente;
			}
			if(!empty($idsence_assigncursos)){
				$estados["idsence_assigncursos"] = $idsence_assigncursos;
			}
			$this->oBD->update('acad_grupoauladetalle ', $estados, array('idgrupoauladetalle' => $id));

			$sql = "SELECT MAX(fecha_final) as fecha_fin FROM acad_grupoauladetalle WHERE idgrupoaula = " . $this->oBD->escapar($idgrupoaula);
			$fecha_fin = $this->oBD->consultarSQL($sql)[0]["fecha_fin"];
			$estados = array("fecha_fin" => $fecha_fin);
			$this->oBD->update('acad_grupoaula', $estados, array('idgrupoaula' => $idgrupoaula));
		    $this->terminarTransaccion('dat_acad_grupoauladetalle_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT ag.idgrupoaula,ag.nombre as strgrupoaula, comentario,nvacantes, idgrupoauladetalle, agd.dias_extradocente, agd.nombre ,agd.fecha_inicio,agd.fecha_final, tipo, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, idcurso, (SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , iddocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE agd.iddocente=dni Limit 1) AS strdocente, agd.idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula WHERE idgrupoauladetalle = " . $this->oBD->escapar($id);
			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_grupoauladetalle', array('idgrupoauladetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function eliminar_($id)
	{
		try {
			$sql = "SELECT idmatricula FROM acad_matricula WHERE idgrupoauladetalle = " . $this->oBD->escapar($id);
			$res = $this->oBD->consultarSQL($sql);
			if(count($res) == 0){
				$sql = "SELECT c.idcurso FROM acad_grupoauladetalle agd INNER JOIN acad_curso_complementario c ON (c.idcursoprincipal = agd.idcurso AND agd.idgrupoaula = c.idgrupoaula) WHERE agd.idgrupoauladetalle = " . $this->oBD->escapar($id);
				$res = $this->oBD->consultarSQL($sql);
				if(count($res) > 0){
					$sql = "SELECT idrecurso,idcursodetalle,idcurso FROM acad_cursodetalle_complementario WHERE idcurso = " . $this->oBD->escapar($res[0]["idcurso"]);
					$res = $this->oBD->consultarSQL($sql);
					if(count($res) > 0){
						foreach ($res as $key => $cdc) {
							$this->oBD->delete('niveles_complementario', array('idnivel' => $cdc["idrecurso"]));
							$this->oBD->delete('acad_cursodetalle_complementario', array('idcursodetalle' => $cdc["idcursodetalle"]));
						}
					}
					// $this->oBD->delete('acad_cursodetalle_complementario', array('idcurso' => $res[0]["idcurso"]));
					$this->oBD->delete('acad_curso_complementario', array('idcurso' => $res[0]["idcurso"]));
				}
				return $this->oBD->delete('acad_grupoauladetalle', array('idgrupoauladetalle' => $id));
			}
			return false;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_grupoauladetalle', array($propiedad => $valor), array('idgrupoauladetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function cursos($filtros){
		try {
			$cond = array();
			$sql="SELECT cu.nombre as strcurso ,cu.tipo, cu.imagen  as imagen, ag.idproyecto, agd.idgrupoauladetalle,agd.idgrupoaula,agd.idcurso,agd.iddocente,agd.idlocal,agd.idambiente,agd.nombre,agd.fecha_inicio,agd.fecha_final,agd.idgrado,agd.idsesion,agd.fechamodificacion,agd.idcomplementario,agd.orden, ag.nombre as strgrupoaula, (SELECT dni FROM personal pe WHERE pe.dni=agd.iddocente OR pe.idpersona=agd.iddocente Limit 1) AS dnidocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE  pe.idpersona=agd.iddocente Limit 1) AS strdocente,dias_extradocente FROM acad_grupoauladetalle agd INNER JOIN acad_grupoaula ag ON ag.idgrupoaula=agd.idgrupoaula Right JOIN acad_curso cu ON cu.idcurso=agd.idcurso ";

			if(isset($filtros["idgrupoauladetalle"])){
				if(is_array($filtros["idgrupoauladetalle"]))
					$cond[]="agd.idgrupoauladetalle IN ('".implode("','",$filtros["idgrupoauladetalle"])."')";
				else $cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(isset($filtros["idgrupoaula"])) {
				$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "agd.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["idgrado"])) {
				$cond[] = "agd.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "agd.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idcomplementario"])) {
				$cond[] = "agd.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if(!empty($filtros["orderby"])){
				if($filtros["orderby"]!='no'){
					$sql.=" ORDER BY ".$filtros["orderby"]." ASC";
				}
			}else{
				$sql.=" ORDER BY idgrupoauladetalle ASC";
			}
			//var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}


	public function listadogruposdeestudioxcurso($filtros){
		try {
			$cond = array();
			$sql="SELECT idgrupoauladetalle,ga.nombre as strgrupoaula,idcurso,idcomplementario,fecha_final,iddocente FROM acad_grupoauladetalle agd INNER JOIN acad_grupoaula ga ON agd.idgrupoaula= ga.idgrupoaula";

			if(isset($filtros["iddocente"])) {
					$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}

			if(isset($filtros["idproyecto"])) {
				$cond[] = "ga.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(isset($filtros["idcurso"])) {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["fecha_final"])){
				$fecha_actual = date('Y-m-d');
				$cond[] = "agd.fecha_final = " . $this->oBD->escapar($fecha_actual);
			}

			if(isset($filtros["idcomplementario"])) {
				$cond[] = "agd.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//echo $sql;

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
}