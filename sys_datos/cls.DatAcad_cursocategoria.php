<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_cursocategoria extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM acad_cursocategoria";
			
			$cond = array();		
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}

			if(isset($filtros["idcategoria"])){
				if(is_array($filtros["idcategoria"]))
					$cond[]="idcategoria IN ('".implode("','",$filtros["idcategoria"])."')";
				else $cond[] = "idcategoria= " . $this->oBD->escapar($filtros["idcategoria"]);
			}

			/*if(isset($filtros["idsubcategoria"])) {
					$cond[] = "idsubcategoria = " . $this->oBD->escapar($filtros["idsubcategoria"]);
			}*/
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	/**
	 * Funcion generica con busqueda relacional para encontrar las asignaciones de categorias en los cursos de un determinado proyecto
	 * @param Array Filtros de busquedas
	 * @return Array El resultado de la consulta SQL
	 */
	public function buscarcompleto($filtros=null){
		try{
			$sql = "SELECT cc.id,cc.idcategoria,ac.nombre,ac.idpadre,(SELECT nombre FROM acad_categorias WHERE idcategoria = ac.idpadre LIMIT 1) AS nombrecarrera,cc.idcurso,c.nombre AS nombrecurso,cc.idproy  FROM acad_cursocategoria cc LEFT JOIN acad_categorias ac ON ac.idcategoria = cc.idcategoria INNER JOIN acad_curso c ON c.idcurso = cc.idcurso";
			if(isset($filtros["id"])) { $cond[] = "id = " . $this->oBD->escapar($filtros["id"]); }
			if(isset($filtros["idproy"])) { $cond[] = "idproy = " . $this->oBD->escapar($filtros["idproy"]); }
			if(isset($filtros["idcategoria"])){
				if(is_array($filtros["idcategoria"])){ $cond[]="idcategoria IN ('".implode("','",$filtros["idcategoria"])."')"; }
				else { $cond[] = "idcategoria= " . $this->oBD->escapar($filtros["idcategoria"]); }
			}
			if(isset($filtros["idcurso"])) { $cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]); }			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e){
			throw new Exception("ERROR\n".JrTexto::_("buscarcompleto")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT id,idcategoria,idcurso,idproy  FROM acad_cursocategoria";	
			$cond = array();	
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["idcategoria"])){
				if(is_array($filtros["idcategoria"]))
					$cond[]="idcategoria IN ('".implode("','",$filtros["idcategoria"])."')";
				else $cond[] = "idcategoria= " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			/*if(isset($filtros["idsubcategoria"])) {
					$cond[] = "idsubcategoria = " . $this->oBD->escapar($filtros["idsubcategoria"]);
			}*/
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY idcurso ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function buscarCursos($filtros=null)
	{
		try {
			$sql = "SELECT CCAT.idcurso, c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks, CCAT.id,CCAT.idcategoria,CCAT.idproy FROM acad_cursocategoria CCAT JOIN acad_curso c ON CCAT.idcurso=c.idcurso";			
			
			$cond = array();		
					
			
			if(isset($filtros["id"])) {
					$cond[] = "CCAT.id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["idcategoria"])){
				if(is_array($filtros["idcategoria"]))
					$cond[]="idcategoria IN ('".implode("','",$filtros["idcategoria"])."')";
				else $cond[] = "idcategoria= " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["idsubcategoria"])) {
					$cond[] = "CCAT.idsubcategoria = " . $this->oBD->escapar($filtros["idsubcategoria"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "CCAT.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcategoria,$idsubcategoria,$idcurso)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_cursocategoria_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT id FROM acad_cursocategoria ORDER BY id DESC limit 0,1");
			++$id;
			
			$estados = array('id' => $id
							
							,'idcategoria'=>$idcategoria
							,'idsubcategoria'=>$idsubcategoria
							,'idcurso'=>$idcurso							
							);
			
			$this->oBD->insert('acad_cursocategoria', $estados);			
			$this->terminarTransaccion('dat_acad_cursocategoria_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursocategoria_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcategoria,$idsubcategoria,$idcurso)
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursocategoria_update');
			$estados = array('idcategoria'=>$idcategoria
							,'idsubcategoria'=>$idsubcategoria
							,'idcurso'=>$idcurso								
							);
			
			$this->oBD->update('acad_cursocategoria ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_acad_cursocategoria_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  id,idcategoria,idcurso,idproy    FROM acad_cursocategoria  WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_cursocategoria', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_cursocategoria', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
   
		
}