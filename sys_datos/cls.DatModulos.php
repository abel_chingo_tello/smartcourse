<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatModulos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Modulos") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT padres.idmodulo, padres.nombre, padres.icono, padres.link, padres.estado, padres.modulopadre, ifnull(hijos.modulopadre,0) as espadre from (SELECT modulos.idmodulo, modulos.nombre, modulos.icono, modulos.link, modulos.estado, modulos.modulopadre FROM modulos WHERE modulopadre=0) padres LEFT JOIN (SELECT distinct modulopadre FROM `modulos` where modulopadre!=0 ORDER BY `idmodulo` ASC) hijos	on padres.idmodulo=hijos.modulopadre";

			$cond = array();
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["icono"])) {
				$cond[] = "icono = " . $this->oBD->escapar($filtros["icono"]);
			}
			if (isset($filtros["link"])) {
				$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Modulos") . ": " . $e->getMessage());
		}
	}
	public function buscarModulosHijos($idpadre, $filtros = null)
	{
		try {
			$sql = "SELECT modulos.idmodulo, modulos.nombre, modulos.icono, modulos.link, modulos.estado, modulos.modulopadre FROM `modulos`";

			$cond = array();


			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["icono"])) {
				$cond[] = "icono = " . $this->oBD->escapar($filtros["icono"]);
			}
			if (isset($filtros["link"])) {
				$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			$cond[] = "modulopadre = " . $this->oBD->escapar($idpadre);
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// echo "<hr>$sql<hr>";
			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Modulos") . ": " . $e->getMessage());
		}
	}


	public function insertar($nombre, $icono, $link, $estado)
	{
		try {

			$this->iniciarTransaccion('dat_modulos_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmodulo) FROM modulos");
			++$id;

			$estados = array(
				'idmodulo' => $id, 'nombre' => $nombre, 'icono' => $icono, 'link' => $link, 'estado' => $estado
			);

			$this->oBD->insert('modulos', $estados);
			$this->terminarTransaccion('dat_modulos_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_modulos_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Modulos") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre, $icono, $link, $estado)
	{
		try {
			$this->iniciarTransaccion('dat_modulos_update');
			$estados = array(
				'nombre' => $nombre, 'icono' => $icono, 'link' => $link, 'estado' => $estado
			);

			$this->oBD->update('modulos ', $estados, array('idmodulo' => $id));
			$this->terminarTransaccion('dat_modulos_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Modulos") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  modulos.idmodulo, modulos.nombre, modulos.icono, modulos.link, modulos.estado, modulos.modulopadre  FROM modulos  "
				. " WHERE idmodulo = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Modulos") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('modulos', array('idmodulo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Modulos") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('modulos', array($propiedad => $valor), array('idmodulo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Modulos") . ": " . $e->getMessage());
		}
	}
}
