<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-12-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatDescargas_consumidas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Recursos_colaborativos").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT iddescargaconsumida,idasignado,archivo,idcurso,strcurso,fecha_registro,idcc FROM descargas_consumidas";
			$cond = array();
			if(isset($filtros["sqlconuser"])){
				$sql="SELECT iddescargaconsumida,idasignado,archivo,idcurso,strcurso,fecha_registro,idcc,idpersona,idempresa,idproyecto FROM descargas_consumidas INNER JOIN descargas_asignadas ON idasignado=id";
			}
			
			if(isset($filtros["iddescargaconsumida"])) {
					$cond[] = "iddescargaconsumida = " . $this->oBD->escapar($filtros["iddescargaconsumida"]);
			}
			if(isset($filtros["idasignado"])) {
					$cond[] = "idasignado = " . $this->oBD->escapar($filtros["idasignado"]);
			}
			if(isset($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}

			if(isset($filtros["strcurso"])) {
					$cond[] = "strcurso = " . $this->oBD->escapar($filtros["strcurso"]);
			}
			if(isset($filtros["idcc"])) {
					$cond[] = "idcc = " . $this->oBD->escapar($filtros["idcc"]);
			}

			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(isset($filtros["texto"])) {
				$cond[] = "idpersona " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM descargas_consumidas";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("descargas_consumidas").": " . $e->getMessage());
		}
	}
		
	public function insertar($idasignado,$archivo,$idcurso,$strcurso,$idcc)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT id+1 FROM descargas_consumidas ORDER BY id DESC limit 0,1 ");			
			$estados = array('idasignado'=>$idasignado
							,'archivo'=>$archivo							
							,'idcurso'=>$idcurso
							,'strcurso'=>$strcurso
							,'idcc'=>$idcc
							);			
			return $this->oBD->insertAI('descargas_consumidas', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_descargas_consumidas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("descargas_consumidas").": " . $e->getMessage());
		}
	}
	public function actualizar($iddescargaconsumida, $idasignado,$archivo,$idcurso,$strcurso,$idcc)
	{
		try {			
			$estados = array('idasignado'=>$idasignado
							,'archivo'=>$archivo							
							,'idcurso'=>$idcurso
							,'strcurso'=>$strcurso
							,'idcc'=>$idcc
							);
			$this->oBD->update('descargas_consumidas ', $estados, array('iddescargaconsumida' => $iddescargaconsumida));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("descargas_consumidas").": " . $e->getMessage());
		}
	}

	public function actualizar2($estados,$condicion){
		try {
			return $this->oBD->update('descargas_consumidas ', $estados, $condicion);		    
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("descargas_consumidasasignacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('descargas_consumidas', array('iddescargaconsumida' => $id));
			else 
				return $this->oBD->update('descargas_consumidas', array('estado' => -1), array('iddescargaconsumida' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("descargas_consumidas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('descargas_consumidas', array($propiedad => $valor), array('iddescargaconsumida' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("descargas_consumidas").": " . $e->getMessage());
		}
	}  	 
		
}