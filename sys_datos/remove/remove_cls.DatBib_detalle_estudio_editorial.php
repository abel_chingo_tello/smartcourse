<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_detalle_estudio_editorial extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_detalle_estudio_editorial";
			
			$cond = array();		
			
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_detalle_estudio_editorial";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_detalle_estudio_editorial  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}

	public function insertar($id_editorial,$id_estudio)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_detalle_estudio_editorial_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_detalle) FROM bib_detalle_estudio_editorial");
			++$id;
			
			$estados = array('id_detalle' => $id
							
							,'id_editorial'=>$id_editorial
							,'id_estudio'=>$id_estudio							
							);
			
			$this->oBD->insert('bib_detalle_estudio_editorial', $estados);			
			$this->terminarTransaccion('dat_bib_detalle_estudio_editorial_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_detalle_estudio_editorial_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_editorial,$id_estudio)
	{
		try {
			$this->iniciarTransaccion('dat_bib_detalle_estudio_editorial_update');
			$estados = array('id_editorial'=>$id_editorial
							,'id_estudio'=>$id_estudio								
							);
			
			$this->oBD->update('bib_detalle_estudio_editorial ', $estados, array('id_detalle' => $id));
		    $this->terminarTransaccion('dat_bib_detalle_estudio_editorial_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_detalle_estudio_editorial  "
					. " WHERE id_detalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_detalle_estudio_editorial', array('id_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_detalle_estudio_editorial', array($propiedad => $valor), array('id_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_detalle_estudio_editorial").": " . $e->getMessage());
		}
	}
   
		
}