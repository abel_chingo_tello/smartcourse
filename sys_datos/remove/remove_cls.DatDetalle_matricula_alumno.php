<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-04-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
 class DatDetalle_matricula_alumno extends DatBase
 {
 	public function __construct()
 	{
 		try {
 			parent::conectar();
 		} catch(Exception $e) {
 			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
 		}
 	}
 	public function getNumRegistros($filtros=null)
 	{
 		try {
 			$sql = "SELECT COUNT(*) FROM detalle_matricula_alumno";

 			$cond = array();		

 			if(!empty($filtros["iddetalle"])) {
 				$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
 			}
 			if(!empty($filtros["idmatricula"])) {
 				$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
 			}
 			if(!empty($filtros["idejercicio"])) {
 				$cond[] = "idejercicio = " . $this->oBD->escapar($filtros["idejercicio"]);
 			}
 			if(!empty($filtros["orden"])) {
 				$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
 			}
 			if(!empty($filtros["estado"])) {
 				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
 			}			
 			if(!empty($cond)) {
 				$sql .= " WHERE " . implode(' AND ', $cond);
 			}

 			return $this->oBD->consultarEscalarSQL($sql);
 		} catch(Exception $e) {
 			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
 		}
 	}
 	public function buscar($filtros=null)
 	{
 		try {
 			$sql = "SELECT * FROM detalle_matricula_alumno";			

 			$cond = array();		


 			if(!empty($filtros["iddetalle"])) {
 				$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
 			}
 			if(!empty($filtros["idmatricula"])) {
 				$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
 			}
 			if(!empty($filtros["idejercicio"])) {
 				$cond[] = "idejercicio = " . $this->oBD->escapar($filtros["idejercicio"]);
 			}
 			if(!empty($filtros["orden"])) {
 				$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
 			}
 			if(!empty($filtros["estado"])) {
 				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
 			}			
 			if(!empty($cond)) {
 				$sql .= " WHERE " . implode(' AND ', $cond);
 			}

			//$sql .= " ORDER BY fecha_creado ASC";

 			return $this->oBD->consultarSQL($sql);
 		} catch(Exception $e) {
 			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
 		}
 	}
 	public function listarall()
 	{
 		try {
 			$sql = "SELECT  *  FROM detalle_matricula_alumno  ";

 			$res = $this->oBD->consultarSQL($sql);			
 			return empty($res) ? null : $res;
 		} catch(Exception $e) {
 			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
 		}
 	}

 	public function insertar($idmatricula,$idejercicio,$orden,$estado)
 	{
 		try {

 			$this->iniciarTransaccion('dat_detalle_matricula_alumno_insert');

 			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM detalle_matricula_alumno");
 			++$id;

 			$estados = array('iddetalle' => $id

 				,'idmatricula'=>$idmatricula
 				,'idejercicio'=>$idejercicio
 				,'orden'=>$orden
 				,'estado'=>$estado							
 				);

 			$this->oBD->insert('detalle_matricula_alumno', $estados);			
 			$this->terminarTransaccion('dat_detalle_matricula_alumno_insert');			
 			return $id;

 		} catch(Exception $e) {
 			$this->cancelarTransaccion('dat_detalle_matricula_alumno_insert');
 			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
 		}
 	}
 	public function actualizar($id, $idmatricula,$idejercicio,$orden,$estado)
 	{
 		try {
 			$this->iniciarTransaccion('dat_detalle_matricula_alumno_update');
 			$estados = array('idmatricula'=>$idmatricula
 				,'idejercicio'=>$idejercicio
 				,'orden'=>$orden
 				,'estado'=>$estado								
 				);

 			$this->oBD->update('detalle_matricula_alumno ', $estados, array('iddetalle' => $id));
 			$this->terminarTransaccion('dat_detalle_matricula_alumno_update');
 			return $id;
 		} catch(Exception $e) {
 			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
 		}
 	}
 	public function get($id)
 	{
 		try {
 			$sql = "SELECT  *  FROM detalle_matricula_alumno  "
 			. " WHERE iddetalle = " . $this->oBD->escapar($id);

 			$res = $this->oBD->consultarSQL($sql);

 			return empty($res) ? null : $res[0];
 		} catch(Exception $e) {
 			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
 		}
 	}

 	public function eliminar($id)
 	{
 		try {
 			return $this->oBD->delete('detalle_matricula_alumno', array('iddetalle' => $id));
 		} catch(Exception $e) {
 			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
 		}
 	}

 	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('detalle_matricula_alumno', array($propiedad => $valor), array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
		}
	}	
}