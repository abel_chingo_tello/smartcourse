<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_idioma extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_idioma";
			
			$cond = array();		
			
			if(!empty($filtros["id_idioma"])) {
					$cond[] = "id_idioma = " . $this->oBD->escapar($filtros["id_idioma"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_idioma";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_idioma"])) {
					$cond[] = "id_idioma = " . $this->oBD->escapar($filtros["id_idioma"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_idioma  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}
	
	public function insertar($descripcion)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_idioma_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_idioma) FROM bib_idioma");
			++$id;
			
			$estados = array('id_idioma' => $id
							
							,'descripcion'=>$descripcion							
							);
			
			$this->oBD->insert('bib_idioma', $estados);			
			$this->terminarTransaccion('dat_bib_idioma_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_idioma_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $descripcion)
	{
		try {
			$this->iniciarTransaccion('dat_bib_idioma_update');
			$estados = array('descripcion'=>$descripcion								
							);
			
			$this->oBD->update('bib_idioma ', $estados, array('id_idioma' => $id));
		    $this->terminarTransaccion('dat_bib_idioma_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_idioma  "
					. " WHERE id_idioma = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_idioma', array('id_idioma' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_idioma', array($propiedad => $valor), array('id_idioma' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_idioma").": " . $e->getMessage());
		}
	}
   
		
}