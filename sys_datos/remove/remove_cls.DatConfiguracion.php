<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatConfiguracion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Configuracion").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM configuracion";			
			
			$cond = array();		
					
			
			if(isset($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Configuracion").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($valor)
	{
		try {
			
			$this->iniciarTransaccion('dat_configuracion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(atributo) FROM configuracion");
			++$id;
			
			$estados = array('atributo' => $id
							
							,'valor'=>$valor							
							);
			
			$this->oBD->insert('configuracion', $estados);			
			$this->terminarTransaccion('dat_configuracion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_configuracion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Configuracion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $valor)
	{
		try {
			$this->iniciarTransaccion('dat_configuracion_update');
			$estados = array('valor'=>$valor								
							);
			
			$this->oBD->update('configuracion ', $estados, array('atributo' => $id));
		    $this->terminarTransaccion('dat_configuracion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Configuracion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM configuracion  "
					. " WHERE atributo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Configuracion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('configuracion', array('atributo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Configuracion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('configuracion', array($propiedad => $valor), array('atributo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Configuracion").": " . $e->getMessage());
		}
	}
   
		
}