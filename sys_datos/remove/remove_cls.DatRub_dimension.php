<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatRub_dimension extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rub_dimension").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM rub_dimension";			
			
			$cond = array();		
					
			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["tipo_pregunta"])) {
					$cond[] = "tipo_pregunta = " . $this->oBD->escapar($filtros["tipo_pregunta"]);
			}
			if(isset($filtros["escalas_evaluacion"])) {
					$cond[] = "escalas_evaluacion = " . $this->oBD->escapar($filtros["escalas_evaluacion"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["otros_datos"])) {
					$cond[] = "otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if(isset($filtros["id_rubrica"])) {
					$cond[] = "id_rubrica = " . $this->oBD->escapar($filtros["id_rubrica"]);
			}
			if(isset($filtros["activo"])) {
					$cond[] = "activo = " . $this->oBD->escapar($filtros["activo"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rub_dimension").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$tipo_pregunta,$escalas_evaluacion,$tipo,$otros_datos,$id_rubrica,$activo)
	{
		try {
			
			$this->iniciarTransaccion('dat_rub_dimension_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_dimension) FROM rub_dimension");
			++$id;
			
			$estados = array('id_dimension' => $id
							
							,'nombre'=>$nombre
							,'tipo_pregunta'=>$tipo_pregunta
							,'escalas_evaluacion'=>$escalas_evaluacion
							,'tipo'=>$tipo
							,'otros_datos'=>$otros_datos
							,'id_rubrica'=>$id_rubrica
							,'activo'=>$activo							
							);
			
			$this->oBD->insert('rub_dimension', $estados);			
			$this->terminarTransaccion('dat_rub_dimension_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rub_dimension_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rub_dimension").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$tipo_pregunta,$escalas_evaluacion,$tipo,$otros_datos,$id_rubrica,$activo)
	{
		try {
			$this->iniciarTransaccion('dat_rub_dimension_update');
			$estados = array('nombre'=>$nombre
							,'tipo_pregunta'=>$tipo_pregunta
							,'escalas_evaluacion'=>$escalas_evaluacion
							,'tipo'=>$tipo
							,'otros_datos'=>$otros_datos
							,'id_rubrica'=>$id_rubrica
							,'activo'=>$activo								
							);
			
			$this->oBD->update('rub_dimension ', $estados, array('id_dimension' => $id));
		    $this->terminarTransaccion('dat_rub_dimension_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_dimension").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM rub_dimension  "
					. " WHERE id_dimension = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rub_dimension").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rub_dimension', array('id_dimension' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rub_dimension").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rub_dimension', array($propiedad => $valor), array('id_dimension' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_dimension").": " . $e->getMessage());
		}
	}
   
		
}