<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatHistorial_subidos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Historial_subidos").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM historial_subidos";			
			
			$cond = array();		
					
			
			if(isset($filtros["nombre_archivo"])) {
					$cond[] = "nombre_archivo = " . $this->oBD->escapar($filtros["nombre_archivo"]);
			}
			if(isset($filtros["fechasubida"])) {
					$cond[] = "fechasubida = " . $this->oBD->escapar($filtros["fechasubida"]);
			}
			if(isset($filtros["tabla"])) {
					$cond[] = "tabla = " . $this->oBD->escapar($filtros["tabla"]);
			}
			if(isset($filtros["descomprimido"])) {
					$cond[] = "descomprimido = " . $this->oBD->escapar($filtros["descomprimido"]);
			}
			if(isset($filtros["importacion_total"])) {
					$cond[] = "importacion_total = " . $this->oBD->escapar($filtros["importacion_total"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Historial_subidos").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre_archivo,$fechasubida,$tabla,$descomprimido,$importacion_total)
	{
		try {
			
			$this->iniciarTransaccion('dat_historial_subidos_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idsubido) FROM historial_subidos");
			++$id;
			
			$estados = array('idsubido' => $id
							
							,'nombre_archivo'=>$nombre_archivo
							,'fechasubida'=>$fechasubida
							,'tabla'=>$tabla
							,'descomprimido'=>$descomprimido
							,'importacion_total'=>$importacion_total							
							);
			
			$this->oBD->insert('historial_subidos', $estados);			
			$this->terminarTransaccion('dat_historial_subidos_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_historial_subidos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Historial_subidos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre_archivo,$fechasubida,$tabla,$descomprimido,$importacion_total)
	{
		try {
			$this->iniciarTransaccion('dat_historial_subidos_update');
			$estados = array('nombre_archivo'=>$nombre_archivo
							,'fechasubida'=>$fechasubida
							,'tabla'=>$tabla
							,'descomprimido'=>$descomprimido
							,'importacion_total'=>$importacion_total								
							);
			
			$this->oBD->update('historial_subidos ', $estados, array('idsubido' => $id));
		    $this->terminarTransaccion('dat_historial_subidos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Historial_subidos").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM historial_subidos  "
					. " WHERE idsubido = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Historial_subidos").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('historial_subidos', array('idsubido' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Historial_subidos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('historial_subidos', array($propiedad => $valor), array('idsubido' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Historial_subidos").": " . $e->getMessage());
		}
	}
   
		
}