<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatResumen_dre extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen_dre").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM resumen_dre";			
			
			$cond = array();		
					
			
			if(isset($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(isset($filtros["iddre"])) {
					$cond[] = "iddre = " . $this->oBD->escapar($filtros["iddre"]);
			}
			if(isset($filtros["dre"])) {
					$cond[] = "dre = " . $this->oBD->escapar($filtros["dre"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["alumno_ubicacion"])) {
					$cond[] = "alumno_ubicacion = " . $this->oBD->escapar($filtros["alumno_ubicacion"]);
			}
			if(isset($filtros["ubicacion_A1"])) {
					$cond[] = "ubicacion_A1 = " . $this->oBD->escapar($filtros["ubicacion_A1"]);
			}
			if(isset($filtros["ubicacion_A2"])) {
					$cond[] = "ubicacion_A2 = " . $this->oBD->escapar($filtros["ubicacion_A2"]);
			}
			if(isset($filtros["ubicacion_B1"])) {
					$cond[] = "ubicacion_B1 = " . $this->oBD->escapar($filtros["ubicacion_B1"]);
			}
			if(isset($filtros["ubicacion_B2"])) {
					$cond[] = "ubicacion_B2 = " . $this->oBD->escapar($filtros["ubicacion_B2"]);
			}
			if(isset($filtros["ubicacion_C1"])) {
					$cond[] = "ubicacion_C1 = " . $this->oBD->escapar($filtros["ubicacion_C1"]);
			}
			if(isset($filtros["entrada_prom"])) {
					$cond[] = "entrada_prom = " . $this->oBD->escapar($filtros["entrada_prom"]);
			}
			if(isset($filtros["alumno_entrada"])) {
					$cond[] = "alumno_entrada = " . $this->oBD->escapar($filtros["alumno_entrada"]);
			}
			if(isset($filtros["salida_prom"])) {
					$cond[] = "salida_prom = " . $this->oBD->escapar($filtros["salida_prom"]);
			}
			if(isset($filtros["alumno_salida"])) {
					$cond[] = "alumno_salida = " . $this->oBD->escapar($filtros["alumno_salida"]);
			}
			if(isset($filtros["examen_b1_prom"])) {
					$cond[] = "examen_b1_prom = " . $this->oBD->escapar($filtros["examen_b1_prom"]);
			}
			if(isset($filtros["alumno_examen_b1"])) {
					$cond[] = "alumno_examen_b1 = " . $this->oBD->escapar($filtros["alumno_examen_b1"]);
			}
			if(isset($filtros["examen_b2_prom"])) {
					$cond[] = "examen_b2_prom = " . $this->oBD->escapar($filtros["examen_b2_prom"]);
			}
			if(isset($filtros["alumno_examen_b2"])) {
					$cond[] = "alumno_examen_b2 = " . $this->oBD->escapar($filtros["alumno_examen_b2"]);
			}
			if(isset($filtros["examen_b3_prom"])) {
					$cond[] = "examen_b3_prom = " . $this->oBD->escapar($filtros["examen_b3_prom"]);
			}
			if(isset($filtros["alumno_examen_b3"])) {
					$cond[] = "alumno_examen_b3 = " . $this->oBD->escapar($filtros["alumno_examen_b3"]);
			}
			if(isset($filtros["examen_b4_prom"])) {
					$cond[] = "examen_b4_prom = " . $this->oBD->escapar($filtros["examen_b4_prom"]);
			}
			if(isset($filtros["alumno_examen_b4"])) {
					$cond[] = "alumno_examen_b4 = " . $this->oBD->escapar($filtros["alumno_examen_b4"]);
			}
			if(isset($filtros["examen_t1_prom"])) {
					$cond[] = "examen_t1_prom = " . $this->oBD->escapar($filtros["examen_t1_prom"]);
			}
			if(isset($filtros["alumno_examen_t1"])) {
					$cond[] = "alumno_examen_t1 = " . $this->oBD->escapar($filtros["alumno_examen_t1"]);
			}
			if(isset($filtros["examen_t2_prom"])) {
					$cond[] = "examen_t2_prom = " . $this->oBD->escapar($filtros["examen_t2_prom"]);
			}
			if(isset($filtros["alumno_examen_t2"])) {
					$cond[] = "alumno_examen_t2 = " . $this->oBD->escapar($filtros["alumno_examen_t2"]);
			}
			if(isset($filtros["examen_t3_prom"])) {
					$cond[] = "examen_t3_prom = " . $this->oBD->escapar($filtros["examen_t3_prom"]);
			}
			if(isset($filtros["alumno_examen_t3"])) {
					$cond[] = "alumno_examen_t3 = " . $this->oBD->escapar($filtros["alumno_examen_t3"]);
			}
			if(isset($filtros["tiempopv"])) {
					$cond[] = "tiempopv = " . $this->oBD->escapar($filtros["tiempopv"]);
			}
			if(isset($filtros["tiempo_exam"])) {
					$cond[] = "tiempo_exam = " . $this->oBD->escapar($filtros["tiempo_exam"]);
			}
			if(isset($filtros["tiempo_task"])) {
					$cond[] = "tiempo_task = " . $this->oBD->escapar($filtros["tiempo_task"]);
			}
			if(isset($filtros["tiempo_smartbook"])) {
					$cond[] = "tiempo_smartbook = " . $this->oBD->escapar($filtros["tiempo_smartbook"]);
			}
			if(isset($filtros["tiempo_practice"])) {
					$cond[] = "tiempo_practice = " . $this->oBD->escapar($filtros["tiempo_practice"]);
			}
			if(isset($filtros["prog_A1"])) {
					$cond[] = "prog_A1 = " . $this->oBD->escapar($filtros["prog_A1"]);
			}
			if(isset($filtros["prog_A2"])) {
					$cond[] = "prog_A2 = " . $this->oBD->escapar($filtros["prog_A2"]);
			}
			if(isset($filtros["prog_B1"])) {
					$cond[] = "prog_B1 = " . $this->oBD->escapar($filtros["prog_B1"]);
			}
			if(isset($filtros["prog_B2"])) {
					$cond[] = "prog_B2 = " . $this->oBD->escapar($filtros["prog_B2"]);
			}
			if(isset($filtros["prog_C1"])) {
					$cond[] = "prog_C1 = " . $this->oBD->escapar($filtros["prog_C1"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Resumen_dre").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($id_ubigeo,$iddre,$dre,$tipo,$alumno_ubicacion,$ubicacion_A1,$ubicacion_A2,$ubicacion_B1,$ubicacion_B2,$ubicacion_C1,$entrada_prom,$alumno_entrada,$salida_prom,$alumno_salida,$examen_b1_prom,$alumno_examen_b1,$examen_b2_prom,$alumno_examen_b2,$examen_b3_prom,$alumno_examen_b3,$examen_b4_prom,$alumno_examen_b4,$examen_t1_prom,$alumno_examen_t1,$examen_t2_prom,$alumno_examen_t2,$examen_t3_prom,$alumno_examen_t3,$tiempopv,$tiempo_exam,$tiempo_task,$tiempo_smartbook,$tiempo_practice,$prog_A1,$prog_A2,$prog_B1,$prog_B2,$prog_C1)
	{
		try {
			
			$this->iniciarTransaccion('dat_resumen_dre_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM resumen_dre");
			++$id;
			
			$estados = array('id' => $id
							
							,'id_ubigeo'=>$id_ubigeo
							,'iddre'=>$iddre
							,'dre'=>$dre
							,'tipo'=>$tipo
							,'alumno_ubicacion'=>$alumno_ubicacion
							,'ubicacion_A1'=>$ubicacion_A1
							,'ubicacion_A2'=>$ubicacion_A2
							,'ubicacion_B1'=>$ubicacion_B1
							,'ubicacion_B2'=>$ubicacion_B2
							,'ubicacion_C1'=>$ubicacion_C1
							,'entrada_prom'=>$entrada_prom
							,'alumno_entrada'=>$alumno_entrada
							,'salida_prom'=>$salida_prom
							,'alumno_salida'=>$alumno_salida
							,'examen_b1_prom'=>$examen_b1_prom
							,'alumno_examen_b1'=>$alumno_examen_b1
							,'examen_b2_prom'=>$examen_b2_prom
							,'alumno_examen_b2'=>$alumno_examen_b2
							,'examen_b3_prom'=>$examen_b3_prom
							,'alumno_examen_b3'=>$alumno_examen_b3
							,'examen_b4_prom'=>$examen_b4_prom
							,'alumno_examen_b4'=>$alumno_examen_b4
							,'examen_t1_prom'=>$examen_t1_prom
							,'alumno_examen_t1'=>$alumno_examen_t1
							,'examen_t2_prom'=>$examen_t2_prom
							,'alumno_examen_t2'=>$alumno_examen_t2
							,'examen_t3_prom'=>$examen_t3_prom
							,'alumno_examen_t3'=>$alumno_examen_t3
							,'tiempopv'=>$tiempopv
							,'tiempo_exam'=>$tiempo_exam
							,'tiempo_task'=>$tiempo_task
							,'tiempo_smartbook'=>$tiempo_smartbook
							,'tiempo_practice'=>$tiempo_practice
							,'prog_A1'=>$prog_A1
							,'prog_A2'=>$prog_A2
							,'prog_B1'=>$prog_B1
							,'prog_B2'=>$prog_B2
							,'prog_C1'=>$prog_C1							
							);
			
			$this->oBD->insert('resumen_dre', $estados);			
			$this->terminarTransaccion('dat_resumen_dre_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_resumen_dre_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Resumen_dre").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_ubigeo,$iddre,$dre,$tipo,$alumno_ubicacion,$ubicacion_A1,$ubicacion_A2,$ubicacion_B1,$ubicacion_B2,$ubicacion_C1,$entrada_prom,$alumno_entrada,$salida_prom,$alumno_salida,$examen_b1_prom,$alumno_examen_b1,$examen_b2_prom,$alumno_examen_b2,$examen_b3_prom,$alumno_examen_b3,$examen_b4_prom,$alumno_examen_b4,$examen_t1_prom,$alumno_examen_t1,$examen_t2_prom,$alumno_examen_t2,$examen_t3_prom,$alumno_examen_t3,$tiempopv,$tiempo_exam,$tiempo_task,$tiempo_smartbook,$tiempo_practice,$prog_A1,$prog_A2,$prog_B1,$prog_B2,$prog_C1)
	{
		try {
			$this->iniciarTransaccion('dat_resumen_dre_update');
			$estados = array('id_ubigeo'=>$id_ubigeo
							,'iddre'=>$iddre
							,'dre'=>$dre
							,'tipo'=>$tipo
							,'alumno_ubicacion'=>$alumno_ubicacion
							,'ubicacion_A1'=>$ubicacion_A1
							,'ubicacion_A2'=>$ubicacion_A2
							,'ubicacion_B1'=>$ubicacion_B1
							,'ubicacion_B2'=>$ubicacion_B2
							,'ubicacion_C1'=>$ubicacion_C1
							,'entrada_prom'=>$entrada_prom
							,'alumno_entrada'=>$alumno_entrada
							,'salida_prom'=>$salida_prom
							,'alumno_salida'=>$alumno_salida
							,'examen_b1_prom'=>$examen_b1_prom
							,'alumno_examen_b1'=>$alumno_examen_b1
							,'examen_b2_prom'=>$examen_b2_prom
							,'alumno_examen_b2'=>$alumno_examen_b2
							,'examen_b3_prom'=>$examen_b3_prom
							,'alumno_examen_b3'=>$alumno_examen_b3
							,'examen_b4_prom'=>$examen_b4_prom
							,'alumno_examen_b4'=>$alumno_examen_b4
							,'examen_t1_prom'=>$examen_t1_prom
							,'alumno_examen_t1'=>$alumno_examen_t1
							,'examen_t2_prom'=>$examen_t2_prom
							,'alumno_examen_t2'=>$alumno_examen_t2
							,'examen_t3_prom'=>$examen_t3_prom
							,'alumno_examen_t3'=>$alumno_examen_t3
							,'tiempopv'=>$tiempopv
							,'tiempo_exam'=>$tiempo_exam
							,'tiempo_task'=>$tiempo_task
							,'tiempo_smartbook'=>$tiempo_smartbook
							,'tiempo_practice'=>$tiempo_practice
							,'prog_A1'=>$prog_A1
							,'prog_A2'=>$prog_A2
							,'prog_B1'=>$prog_B1
							,'prog_B2'=>$prog_B2
							,'prog_C1'=>$prog_C1								
							);
			
			$this->oBD->update('resumen_dre ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_resumen_dre_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Resumen_dre").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM resumen_dre  "
					. " WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Resumen_dre").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('resumen_dre', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Resumen_dre").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('resumen_dre', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Resumen_dre").": " . $e->getMessage());
		}
	}
   
		
}