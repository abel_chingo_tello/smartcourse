<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatNotas_archivo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Notas_archivo").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM notas_archivo";			
			
			$cond = array();		
					
			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["identificador"])) {
					$cond[] = "identificador = " . $this->oBD->escapar($filtros["identificador"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["fechareg"])) {
					$cond[] = "fechareg = " . $this->oBD->escapar($filtros["fechareg"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Notas_archivo").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$imagen,$identificador,$iddocente,$fechareg)
	{
		try {
			
			$this->iniciarTransaccion('dat_notas_archivo_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idarchivo) FROM notas_archivo");
			++$id;
			
			$estados = array('idarchivo' => $id
							
							,'nombre'=>$nombre
							,'imagen'=>$imagen
							,'identificador'=>$identificador
							,'iddocente'=>$iddocente
							,'fechareg'=>$fechareg							
							);
			
			$this->oBD->insert('notas_archivo', $estados);			
			$this->terminarTransaccion('dat_notas_archivo_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_notas_archivo_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Notas_archivo").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$imagen,$identificador,$iddocente,$fechareg)
	{
		try {
			$this->iniciarTransaccion('dat_notas_archivo_update');
			$estados = array('nombre'=>$nombre
							,'imagen'=>$imagen
							,'identificador'=>$identificador
							,'iddocente'=>$iddocente
							,'fechareg'=>$fechareg								
							);
			
			$this->oBD->update('notas_archivo ', $estados, array('idarchivo' => $id));
		    $this->terminarTransaccion('dat_notas_archivo_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas_archivo").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM notas_archivo  "
					. " WHERE idarchivo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Notas_archivo").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('notas_archivo', array('idarchivo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Notas_archivo").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('notas_archivo', array($propiedad => $valor), array('idarchivo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas_archivo").": " . $e->getMessage());
		}
	}
   
		
}