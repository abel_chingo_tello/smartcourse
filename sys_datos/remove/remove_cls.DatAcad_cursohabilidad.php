<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_cursohabilidad extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursohabilidad").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_cursohabilidad";
			
			$cond = array();		
			
			if(isset($filtros["idcursohabilidad"])) {
					$cond[] = "idcursohabilidad = " . $this->oBD->escapar($filtros["idcursohabilidad"]);
			}
			if(isset($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_cursohabilidad").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_cursohabilidad";			
			
			$cond = array();		
					
			
			if(isset($filtros["idcursohabilidad"])) {
					$cond[] = "idcursohabilidad = " . $this->oBD->escapar($filtros["idcursohabilidad"]);
			}
			if(isset($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursohabilidad").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($texto,$tipo,$idcurso,$idcursodetalle,$idpadre)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_cursohabilidad_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcursohabilidad) FROM acad_cursohabilidad");
			++$id;
			
			$estados = array('idcursohabilidad' => $id
							
							,'texto'=>$texto
							,'tipo'=>$tipo
							,'idcurso'=>$idcurso
							,'idcursodetalle'=>$idcursodetalle
							,'idpadre'=>$idpadre							
							);
			
			$this->oBD->insert('acad_cursohabilidad', $estados);			
			$this->terminarTransaccion('dat_acad_cursohabilidad_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursohabilidad_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_cursohabilidad").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $texto,$tipo,$idcurso,$idcursodetalle,$idpadre)
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursohabilidad_update');
			$estados = array('texto'=>$texto
							,'tipo'=>$tipo
							,'idcurso'=>$idcurso
							,'idcursodetalle'=>$idcursodetalle
							,'idpadre'=>$idpadre								
							);
			
			$this->oBD->update('acad_cursohabilidad ', $estados, array('idcursohabilidad' => $id));
		    $this->terminarTransaccion('dat_acad_cursohabilidad_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursohabilidad").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_cursohabilidad  "
					. " WHERE idcursohabilidad = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_cursohabilidad").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_cursohabilidad', array('idcursohabilidad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_cursohabilidad").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_cursohabilidad', array($propiedad => $valor), array('idcursohabilidad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursohabilidad").": " . $e->getMessage());
		}
	}
   
		
}