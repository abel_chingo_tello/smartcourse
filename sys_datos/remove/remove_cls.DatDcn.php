<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatDcn extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Dcn").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM dcn";			
			
			$cond = array();		
					
						
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Dcn").": " . $e->getMessage());
		}
	}
	
	
	public function insertar()
	{
		try {
			
			$this->iniciarTransaccion('dat_dcn_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(iddcn) FROM dcn");
			++$id;
			
			$estados = array('iddcn' => $id
														
							);
			
			$this->oBD->insert('dcn', $estados);			
			$this->terminarTransaccion('dat_dcn_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_dcn_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Dcn").": " . $e->getMessage());
		}
	}
	public function actualizar($id, )
	{
		try {
			$this->iniciarTransaccion('dat_dcn_update');
			$estados = array(								
							);
			
			$this->oBD->update('dcn ', $estados, array('iddcn' => $id));
		    $this->terminarTransaccion('dat_dcn_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Dcn").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM dcn  "
					. " WHERE iddcn = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Dcn").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('dcn', array('iddcn' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Dcn").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('dcn', array($propiedad => $valor), array('iddcn' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Dcn").": " . $e->getMessage());
		}
	}
   
		
}