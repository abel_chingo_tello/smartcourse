<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatRub_respuestas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rub_respuestas").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM rub_respuestas";			
			
			$cond = array();		
					
			
			if(isset($filtros["id_rubrica"])) {
					$cond[] = "id_rubrica = " . $this->oBD->escapar($filtros["id_rubrica"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["institucion"])) {
					$cond[] = "institucion = " . $this->oBD->escapar($filtros["institucion"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["activo"])) {
					$cond[] = "activo = " . $this->oBD->escapar($filtros["activo"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rub_respuestas").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($id_rubrica,$nombre,$institucion,$idlocal,$iddocente,$fecha_registro,$activo)
	{
		try {
			
			$this->iniciarTransaccion('dat_rub_respuestas_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_respuestas) FROM rub_respuestas");
			++$id;
			
			$estados = array('id_respuestas' => $id
							
							,'id_rubrica'=>$id_rubrica
							,'nombre'=>$nombre
							,'institucion'=>$institucion
							,'idlocal'=>$idlocal
							,'iddocente'=>$iddocente
							,'fecha_registro'=>$fecha_registro
							,'activo'=>$activo							
							);
			
			$this->oBD->insert('rub_respuestas', $estados);			
			$this->terminarTransaccion('dat_rub_respuestas_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rub_respuestas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rub_respuestas").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_rubrica,$nombre,$institucion,$idlocal,$iddocente,$fecha_registro,$activo)
	{
		try {
			$this->iniciarTransaccion('dat_rub_respuestas_update');
			$estados = array('id_rubrica'=>$id_rubrica
							,'nombre'=>$nombre
							,'institucion'=>$institucion
							,'idlocal'=>$idlocal
							,'iddocente'=>$iddocente
							,'fecha_registro'=>$fecha_registro
							,'activo'=>$activo								
							);
			
			$this->oBD->update('rub_respuestas ', $estados, array('id_respuestas' => $id));
		    $this->terminarTransaccion('dat_rub_respuestas_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_respuestas").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM rub_respuestas  "
					. " WHERE id_respuestas = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rub_respuestas").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rub_respuestas', array('id_respuestas' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rub_respuestas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rub_respuestas', array($propiedad => $valor), array('id_respuestas' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_respuestas").": " . $e->getMessage());
		}
	}
   
		
}