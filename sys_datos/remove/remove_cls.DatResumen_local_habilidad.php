<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatResumen_local_habilidad extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen_local_habilidad").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM resumen_local_habilidad";			
			
			$cond = array();		
					
			
			if(isset($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(isset($filtros["iddre"])) {
					$cond[] = "iddre = " . $this->oBD->escapar($filtros["iddre"]);
			}
			if(isset($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["ubicacion_hab_L"])) {
					$cond[] = "ubicacion_hab_L = " . $this->oBD->escapar($filtros["ubicacion_hab_L"]);
			}
			if(isset($filtros["ubicacion_hab_R"])) {
					$cond[] = "ubicacion_hab_R = " . $this->oBD->escapar($filtros["ubicacion_hab_R"]);
			}
			if(isset($filtros["ubicacion_hab_W"])) {
					$cond[] = "ubicacion_hab_W = " . $this->oBD->escapar($filtros["ubicacion_hab_W"]);
			}
			if(isset($filtros["ubicacion_hab_S"])) {
					$cond[] = "ubicacion_hab_S = " . $this->oBD->escapar($filtros["ubicacion_hab_S"]);
			}
			if(isset($filtros["entrada_hab_L"])) {
					$cond[] = "entrada_hab_L = " . $this->oBD->escapar($filtros["entrada_hab_L"]);
			}
			if(isset($filtros["entrada_hab_R"])) {
					$cond[] = "entrada_hab_R = " . $this->oBD->escapar($filtros["entrada_hab_R"]);
			}
			if(isset($filtros["entrada_hab_W"])) {
					$cond[] = "entrada_hab_W = " . $this->oBD->escapar($filtros["entrada_hab_W"]);
			}
			if(isset($filtros["entrada_hab_S"])) {
					$cond[] = "entrada_hab_S = " . $this->oBD->escapar($filtros["entrada_hab_S"]);
			}
			if(isset($filtros["salida_hab_L"])) {
					$cond[] = "salida_hab_L = " . $this->oBD->escapar($filtros["salida_hab_L"]);
			}
			if(isset($filtros["salida_hab_R"])) {
					$cond[] = "salida_hab_R = " . $this->oBD->escapar($filtros["salida_hab_R"]);
			}
			if(isset($filtros["salida_hab_W"])) {
					$cond[] = "salida_hab_W = " . $this->oBD->escapar($filtros["salida_hab_W"]);
			}
			if(isset($filtros["salida_hab_S"])) {
					$cond[] = "salida_hab_S = " . $this->oBD->escapar($filtros["salida_hab_S"]);
			}
			if(isset($filtros["examen_b1_hab_L"])) {
					$cond[] = "examen_b1_hab_L = " . $this->oBD->escapar($filtros["examen_b1_hab_L"]);
			}
			if(isset($filtros["examen_b1_hab_R"])) {
					$cond[] = "examen_b1_hab_R = " . $this->oBD->escapar($filtros["examen_b1_hab_R"]);
			}
			if(isset($filtros["examen_b1_hab_W"])) {
					$cond[] = "examen_b1_hab_W = " . $this->oBD->escapar($filtros["examen_b1_hab_W"]);
			}
			if(isset($filtros["examen_b1_hab_S"])) {
					$cond[] = "examen_b1_hab_S = " . $this->oBD->escapar($filtros["examen_b1_hab_S"]);
			}
			if(isset($filtros["examen_b2_hab_L"])) {
					$cond[] = "examen_b2_hab_L = " . $this->oBD->escapar($filtros["examen_b2_hab_L"]);
			}
			if(isset($filtros["examen_b2_hab_R"])) {
					$cond[] = "examen_b2_hab_R = " . $this->oBD->escapar($filtros["examen_b2_hab_R"]);
			}
			if(isset($filtros["examen_b2_hab_W"])) {
					$cond[] = "examen_b2_hab_W = " . $this->oBD->escapar($filtros["examen_b2_hab_W"]);
			}
			if(isset($filtros["examen_b2_hab_S"])) {
					$cond[] = "examen_b2_hab_S = " . $this->oBD->escapar($filtros["examen_b2_hab_S"]);
			}
			if(isset($filtros["examen_b3_hab_L"])) {
					$cond[] = "examen_b3_hab_L = " . $this->oBD->escapar($filtros["examen_b3_hab_L"]);
			}
			if(isset($filtros["examen_b3_hab_R"])) {
					$cond[] = "examen_b3_hab_R = " . $this->oBD->escapar($filtros["examen_b3_hab_R"]);
			}
			if(isset($filtros["examen_b3_hab_W"])) {
					$cond[] = "examen_b3_hab_W = " . $this->oBD->escapar($filtros["examen_b3_hab_W"]);
			}
			if(isset($filtros["examen_b3_hab_S"])) {
					$cond[] = "examen_b3_hab_S = " . $this->oBD->escapar($filtros["examen_b3_hab_S"]);
			}
			if(isset($filtros["examen_b4_hab_L"])) {
					$cond[] = "examen_b4_hab_L = " . $this->oBD->escapar($filtros["examen_b4_hab_L"]);
			}
			if(isset($filtros["examen_b4_hab_R"])) {
					$cond[] = "examen_b4_hab_R = " . $this->oBD->escapar($filtros["examen_b4_hab_R"]);
			}
			if(isset($filtros["examen_b4_hab_W"])) {
					$cond[] = "examen_b4_hab_W = " . $this->oBD->escapar($filtros["examen_b4_hab_W"]);
			}
			if(isset($filtros["examen_b4_hab_S"])) {
					$cond[] = "examen_b4_hab_S = " . $this->oBD->escapar($filtros["examen_b4_hab_S"]);
			}
			if(isset($filtros["examen_t1_hab_L"])) {
					$cond[] = "examen_t1_hab_L = " . $this->oBD->escapar($filtros["examen_t1_hab_L"]);
			}
			if(isset($filtros["examen_t1_hab_R"])) {
					$cond[] = "examen_t1_hab_R = " . $this->oBD->escapar($filtros["examen_t1_hab_R"]);
			}
			if(isset($filtros["examen_t1_hab_W"])) {
					$cond[] = "examen_t1_hab_W = " . $this->oBD->escapar($filtros["examen_t1_hab_W"]);
			}
			if(isset($filtros["examen_t1_hab_S"])) {
					$cond[] = "examen_t1_hab_S = " . $this->oBD->escapar($filtros["examen_t1_hab_S"]);
			}
			if(isset($filtros["examen_t2_hab_L"])) {
					$cond[] = "examen_t2_hab_L = " . $this->oBD->escapar($filtros["examen_t2_hab_L"]);
			}
			if(isset($filtros["examen_t2_hab_R"])) {
					$cond[] = "examen_t2_hab_R = " . $this->oBD->escapar($filtros["examen_t2_hab_R"]);
			}
			if(isset($filtros["examen_t2_hab_W"])) {
					$cond[] = "examen_t2_hab_W = " . $this->oBD->escapar($filtros["examen_t2_hab_W"]);
			}
			if(isset($filtros["examen_t2_hab_S"])) {
					$cond[] = "examen_t2_hab_S = " . $this->oBD->escapar($filtros["examen_t2_hab_S"]);
			}
			if(isset($filtros["examen_t3_hab_L"])) {
					$cond[] = "examen_t3_hab_L = " . $this->oBD->escapar($filtros["examen_t3_hab_L"]);
			}
			if(isset($filtros["examen_t3_hab_R"])) {
					$cond[] = "examen_t3_hab_R = " . $this->oBD->escapar($filtros["examen_t3_hab_R"]);
			}
			if(isset($filtros["examen_t3_hab_W"])) {
					$cond[] = "examen_t3_hab_W = " . $this->oBD->escapar($filtros["examen_t3_hab_W"]);
			}
			if(isset($filtros["examen_t3_hab_S"])) {
					$cond[] = "examen_t3_hab_S = " . $this->oBD->escapar($filtros["examen_t3_hab_S"]);
			}
			if(isset($filtros["prog_hab_A1_L"])) {
					$cond[] = "prog_hab_A1_L = " . $this->oBD->escapar($filtros["prog_hab_A1_L"]);
			}
			if(isset($filtros["prog_hab_A1_R"])) {
					$cond[] = "prog_hab_A1_R = " . $this->oBD->escapar($filtros["prog_hab_A1_R"]);
			}
			if(isset($filtros["prog_hab_A1_W"])) {
					$cond[] = "prog_hab_A1_W = " . $this->oBD->escapar($filtros["prog_hab_A1_W"]);
			}
			if(isset($filtros["prog_hab_A1_S"])) {
					$cond[] = "prog_hab_A1_S = " . $this->oBD->escapar($filtros["prog_hab_A1_S"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Resumen_local_habilidad").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($id_ubigeo,$iddre,$idugel,$idlocal,$tipo,$ubicacion_hab_L,$ubicacion_hab_R,$ubicacion_hab_W,$ubicacion_hab_S,$entrada_hab_L,$entrada_hab_R,$entrada_hab_W,$entrada_hab_S,$salida_hab_L,$salida_hab_R,$salida_hab_W,$salida_hab_S,$examen_b1_hab_L,$examen_b1_hab_R,$examen_b1_hab_W,$examen_b1_hab_S,$examen_b2_hab_L,$examen_b2_hab_R,$examen_b2_hab_W,$examen_b2_hab_S,$examen_b3_hab_L,$examen_b3_hab_R,$examen_b3_hab_W,$examen_b3_hab_S,$examen_b4_hab_L,$examen_b4_hab_R,$examen_b4_hab_W,$examen_b4_hab_S,$examen_t1_hab_L,$examen_t1_hab_R,$examen_t1_hab_W,$examen_t1_hab_S,$examen_t2_hab_L,$examen_t2_hab_R,$examen_t2_hab_W,$examen_t2_hab_S,$examen_t3_hab_L,$examen_t3_hab_R,$examen_t3_hab_W,$examen_t3_hab_S,$prog_hab_A1_L,$prog_hab_A1_R,$prog_hab_A1_W,$prog_hab_A1_S)
	{
		try {
			
			$this->iniciarTransaccion('dat_resumen_local_habilidad_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM resumen_local_habilidad");
			++$id;
			
			$estados = array('id' => $id
							
							,'id_ubigeo'=>$id_ubigeo
							,'iddre'=>$iddre
							,'idugel'=>$idugel
							,'idlocal'=>$idlocal
							,'tipo'=>$tipo
							,'ubicacion_hab_L'=>$ubicacion_hab_L
							,'ubicacion_hab_R'=>$ubicacion_hab_R
							,'ubicacion_hab_W'=>$ubicacion_hab_W
							,'ubicacion_hab_S'=>$ubicacion_hab_S
							,'entrada_hab_L'=>$entrada_hab_L
							,'entrada_hab_R'=>$entrada_hab_R
							,'entrada_hab_W'=>$entrada_hab_W
							,'entrada_hab_S'=>$entrada_hab_S
							,'salida_hab_L'=>$salida_hab_L
							,'salida_hab_R'=>$salida_hab_R
							,'salida_hab_W'=>$salida_hab_W
							,'salida_hab_S'=>$salida_hab_S
							,'examen_b1_hab_L'=>$examen_b1_hab_L
							,'examen_b1_hab_R'=>$examen_b1_hab_R
							,'examen_b1_hab_W'=>$examen_b1_hab_W
							,'examen_b1_hab_S'=>$examen_b1_hab_S
							,'examen_b2_hab_L'=>$examen_b2_hab_L
							,'examen_b2_hab_R'=>$examen_b2_hab_R
							,'examen_b2_hab_W'=>$examen_b2_hab_W
							,'examen_b2_hab_S'=>$examen_b2_hab_S
							,'examen_b3_hab_L'=>$examen_b3_hab_L
							,'examen_b3_hab_R'=>$examen_b3_hab_R
							,'examen_b3_hab_W'=>$examen_b3_hab_W
							,'examen_b3_hab_S'=>$examen_b3_hab_S
							,'examen_b4_hab_L'=>$examen_b4_hab_L
							,'examen_b4_hab_R'=>$examen_b4_hab_R
							,'examen_b4_hab_W'=>$examen_b4_hab_W
							,'examen_b4_hab_S'=>$examen_b4_hab_S
							,'examen_t1_hab_L'=>$examen_t1_hab_L
							,'examen_t1_hab_R'=>$examen_t1_hab_R
							,'examen_t1_hab_W'=>$examen_t1_hab_W
							,'examen_t1_hab_S'=>$examen_t1_hab_S
							,'examen_t2_hab_L'=>$examen_t2_hab_L
							,'examen_t2_hab_R'=>$examen_t2_hab_R
							,'examen_t2_hab_W'=>$examen_t2_hab_W
							,'examen_t2_hab_S'=>$examen_t2_hab_S
							,'examen_t3_hab_L'=>$examen_t3_hab_L
							,'examen_t3_hab_R'=>$examen_t3_hab_R
							,'examen_t3_hab_W'=>$examen_t3_hab_W
							,'examen_t3_hab_S'=>$examen_t3_hab_S
							,'prog_hab_A1_L'=>$prog_hab_A1_L
							,'prog_hab_A1_R'=>$prog_hab_A1_R
							,'prog_hab_A1_W'=>$prog_hab_A1_W
							,'prog_hab_A1_S'=>$prog_hab_A1_S							
							);
			
			$this->oBD->insert('resumen_local_habilidad', $estados);			
			$this->terminarTransaccion('dat_resumen_local_habilidad_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_resumen_local_habilidad_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Resumen_local_habilidad").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_ubigeo,$iddre,$idugel,$idlocal,$tipo,$ubicacion_hab_L,$ubicacion_hab_R,$ubicacion_hab_W,$ubicacion_hab_S,$entrada_hab_L,$entrada_hab_R,$entrada_hab_W,$entrada_hab_S,$salida_hab_L,$salida_hab_R,$salida_hab_W,$salida_hab_S,$examen_b1_hab_L,$examen_b1_hab_R,$examen_b1_hab_W,$examen_b1_hab_S,$examen_b2_hab_L,$examen_b2_hab_R,$examen_b2_hab_W,$examen_b2_hab_S,$examen_b3_hab_L,$examen_b3_hab_R,$examen_b3_hab_W,$examen_b3_hab_S,$examen_b4_hab_L,$examen_b4_hab_R,$examen_b4_hab_W,$examen_b4_hab_S,$examen_t1_hab_L,$examen_t1_hab_R,$examen_t1_hab_W,$examen_t1_hab_S,$examen_t2_hab_L,$examen_t2_hab_R,$examen_t2_hab_W,$examen_t2_hab_S,$examen_t3_hab_L,$examen_t3_hab_R,$examen_t3_hab_W,$examen_t3_hab_S,$prog_hab_A1_L,$prog_hab_A1_R,$prog_hab_A1_W,$prog_hab_A1_S)
	{
		try {
			$this->iniciarTransaccion('dat_resumen_local_habilidad_update');
			$estados = array('id_ubigeo'=>$id_ubigeo
							,'iddre'=>$iddre
							,'idugel'=>$idugel
							,'idlocal'=>$idlocal
							,'tipo'=>$tipo
							,'ubicacion_hab_L'=>$ubicacion_hab_L
							,'ubicacion_hab_R'=>$ubicacion_hab_R
							,'ubicacion_hab_W'=>$ubicacion_hab_W
							,'ubicacion_hab_S'=>$ubicacion_hab_S
							,'entrada_hab_L'=>$entrada_hab_L
							,'entrada_hab_R'=>$entrada_hab_R
							,'entrada_hab_W'=>$entrada_hab_W
							,'entrada_hab_S'=>$entrada_hab_S
							,'salida_hab_L'=>$salida_hab_L
							,'salida_hab_R'=>$salida_hab_R
							,'salida_hab_W'=>$salida_hab_W
							,'salida_hab_S'=>$salida_hab_S
							,'examen_b1_hab_L'=>$examen_b1_hab_L
							,'examen_b1_hab_R'=>$examen_b1_hab_R
							,'examen_b1_hab_W'=>$examen_b1_hab_W
							,'examen_b1_hab_S'=>$examen_b1_hab_S
							,'examen_b2_hab_L'=>$examen_b2_hab_L
							,'examen_b2_hab_R'=>$examen_b2_hab_R
							,'examen_b2_hab_W'=>$examen_b2_hab_W
							,'examen_b2_hab_S'=>$examen_b2_hab_S
							,'examen_b3_hab_L'=>$examen_b3_hab_L
							,'examen_b3_hab_R'=>$examen_b3_hab_R
							,'examen_b3_hab_W'=>$examen_b3_hab_W
							,'examen_b3_hab_S'=>$examen_b3_hab_S
							,'examen_b4_hab_L'=>$examen_b4_hab_L
							,'examen_b4_hab_R'=>$examen_b4_hab_R
							,'examen_b4_hab_W'=>$examen_b4_hab_W
							,'examen_b4_hab_S'=>$examen_b4_hab_S
							,'examen_t1_hab_L'=>$examen_t1_hab_L
							,'examen_t1_hab_R'=>$examen_t1_hab_R
							,'examen_t1_hab_W'=>$examen_t1_hab_W
							,'examen_t1_hab_S'=>$examen_t1_hab_S
							,'examen_t2_hab_L'=>$examen_t2_hab_L
							,'examen_t2_hab_R'=>$examen_t2_hab_R
							,'examen_t2_hab_W'=>$examen_t2_hab_W
							,'examen_t2_hab_S'=>$examen_t2_hab_S
							,'examen_t3_hab_L'=>$examen_t3_hab_L
							,'examen_t3_hab_R'=>$examen_t3_hab_R
							,'examen_t3_hab_W'=>$examen_t3_hab_W
							,'examen_t3_hab_S'=>$examen_t3_hab_S
							,'prog_hab_A1_L'=>$prog_hab_A1_L
							,'prog_hab_A1_R'=>$prog_hab_A1_R
							,'prog_hab_A1_W'=>$prog_hab_A1_W
							,'prog_hab_A1_S'=>$prog_hab_A1_S								
							);
			
			$this->oBD->update('resumen_local_habilidad ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_resumen_local_habilidad_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Resumen_local_habilidad").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM resumen_local_habilidad  "
					. " WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Resumen_local_habilidad").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('resumen_local_habilidad', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Resumen_local_habilidad").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('resumen_local_habilidad', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Resumen_local_habilidad").": " . $e->getMessage());
		}
	}
   
		
}