<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_estudio extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_estudio";
			
			$cond = array();		
			
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($filtros["paginas"])) {
					$cond[] = "paginas = " . $this->oBD->escapar($filtros["paginas"]);
			}
			if(!empty($filtros["codigo_tipo"])) {
					$cond[] = "codigo_tipo = " . $this->oBD->escapar($filtros["codigo_tipo"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["nombre_abreviado"])) {
					$cond[] = "nombre_abreviado = " . $this->oBD->escapar($filtros["nombre_abreviado"]);
			}
			if(!empty($filtros["precio"])) {
					$cond[] = "precio = " . $this->oBD->escapar($filtros["precio"]);
			}
			if(!empty($filtros["edicion"])) {
					$cond[] = "edicion = " . $this->oBD->escapar($filtros["edicion"]);
			}
			if(!empty($filtros["fec_publicacion"])) {
					$cond[] = "fec_publicacion = " . $this->oBD->escapar($filtros["fec_publicacion"]);
			}
			if(!empty($filtros["fec_creacion"])) {
					$cond[] = "fec_creacion = " . $this->oBD->escapar($filtros["fec_creacion"]);
			}
			if(!empty($filtros["fec_modificacion"])) {
					$cond[] = "fec_modificacion = " . $this->oBD->escapar($filtros["fec_modificacion"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(!empty($filtros["lugar"])) {
					$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if(!empty($filtros["condicion"])) {
					$cond[] = "condicion = " . $this->oBD->escapar($filtros["condicion"]);
			}
			if(!empty($filtros["padre"])) {
					$cond[] = "padre = " . $this->oBD->escapar($filtros["padre"]);
			}
			if(!empty($filtros["resumen"])) {
					$cond[] = "resumen = " . $this->oBD->escapar($filtros["resumen"]);
			}
			if(!empty($filtros["id_idioma"])) {
					$cond[] = "id_idioma = " . $this->oBD->escapar($filtros["id_idioma"]);
			}
			if(!empty($filtros["id_tipo"])) {
					$cond[] = "id_tipo = " . $this->oBD->escapar($filtros["id_tipo"]);
			}
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_setting"])) {
					$cond[] = "id_setting = " . $this->oBD->escapar($filtros["id_setting"]);
			}
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_estudio b left join bib_setting s on s.id_setting=b.id_setting";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($filtros["paginas"])) {
					$cond[] = "paginas = " . $this->oBD->escapar($filtros["paginas"]);
			}
			if(!empty($filtros["codigo_tipo"])) {
					$cond[] = "codigo_tipo = " . $this->oBD->escapar($filtros["codigo_tipo"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["nombre_abreviado"])) {
					$cond[] = "nombre_abreviado = " . $this->oBD->escapar($filtros["nombre_abreviado"]);
			}
			if(!empty($filtros["precio"])) {
					$cond[] = "precio = " . $this->oBD->escapar($filtros["precio"]);
			}
			if(!empty($filtros["edicion"])) {
					$cond[] = "edicion = " . $this->oBD->escapar($filtros["edicion"]);
			}
			if(!empty($filtros["fec_publicacion"])) {
					$cond[] = "fec_publicacion = " . $this->oBD->escapar($filtros["fec_publicacion"]);
			}
			if(!empty($filtros["fec_creacion"])) {
					$cond[] = "fec_creacion = " . $this->oBD->escapar($filtros["fec_creacion"]);
			}
			if(!empty($filtros["fec_modificacion"])) {
					$cond[] = "fec_modificacion = " . $this->oBD->escapar($filtros["fec_modificacion"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(!empty($filtros["lugar"])) {
					$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if(!empty($filtros["condicion"])) {
					$cond[] = "condicion = " . $this->oBD->escapar($filtros["condicion"]);
			}
			if(!empty($filtros["resumen"])) {
					$cond[] = "resumen = " . $this->oBD->escapar($filtros["resumen"]);
			}
			if(isset($filtros["padre"])) {
					$cond[] = "padre = " . $this->oBD->escapar($filtros["padre"]);
			}
			if(!empty($filtros["id_idioma"])) {
					$cond[] = "id_idioma = " . $this->oBD->escapar($filtros["id_idioma"]);
			}
			if(!empty($filtros["id_tipo"])) {
					$cond[] = "id_tipo = " . $this->oBD->escapar($filtros["id_tipo"]);
			}
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_setting"])) {
					$cond[] = "id_setting = " . $this->oBD->escapar($filtros["id_setting"]);
			}	
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	public function buscarmaterial($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_estudio b left join bib_setting s on s.id_setting=b.id_setting";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($filtros["paginas"])) {
					$cond[] = "paginas = " . $this->oBD->escapar($filtros["paginas"]);
			}
			if(!empty($filtros["codigo_tipo"])) {
					$cond[] = "codigo_tipo = " . $this->oBD->escapar($filtros["codigo_tipo"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre LIKE '" . $filtros["nombre"]."%'";
			}
			if(!empty($filtros["nombre_abreviado"])) {
					$cond[] = "nombre_abreviado = " . $this->oBD->escapar($filtros["nombre_abreviado"]);
			}
			if(!empty($filtros["precio"])) {
					$cond[] = "precio = " . $this->oBD->escapar($filtros["precio"]);
			}
			if(!empty($filtros["edicion"])) {
					$cond[] = "edicion = " . $this->oBD->escapar($filtros["edicion"]);
			}
			if(!empty($filtros["fec_publicacion"])) {
					$cond[] = "fec_publicacion = " . $this->oBD->escapar($filtros["fec_publicacion"]);
			}
			if(!empty($filtros["fec_creacion"])) {
					$cond[] = "fec_creacion = " . $this->oBD->escapar($filtros["fec_creacion"]);
			}
			if(!empty($filtros["fec_modificacion"])) {
					$cond[] = "fec_modificacion = " . $this->oBD->escapar($filtros["fec_modificacion"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(!empty($filtros["lugar"])) {
					$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if(!empty($filtros["condicion"])) {
					$cond[] = "condicion = " . $this->oBD->escapar($filtros["condicion"]);
			}
			if(!empty($filtros["resumen"])) {
					$cond[] = "resumen = " . $this->oBD->escapar($filtros["resumen"]);
			}
			if(isset($filtros["padre"])) {
					$cond[] = "padre = " . $this->oBD->escapar($filtros["padre"]);
			}
			if(!empty($filtros["id_idioma"])) {
					$cond[] = "id_idioma = " . $this->oBD->escapar($filtros["id_idioma"]);
			}
			if(!empty($filtros["id_tipo"]) || $filtros['id_tipo']==0) {
					$cond[] = "id_tipo = " . $this->oBD->escapar($filtros["id_tipo"]);
			}
			if(!empty($filtros["id_detalle"]) || $filtros['id_detalle']==0) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_setting"])) {
					$cond[] = "id_setting = " . $this->oBD->escapar($filtros["id_setting"]);
			}	
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}
			if(!empty($filtros["orden"]) || $filtros['orden']==0) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond)." GROUP BY nombre ORDER BY nombre ASC ";
			}
			// var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	public function buscarmusica($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_estudio b left join bib_setting s on s.id_setting=b.id_setting";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($filtros["paginas"])) {
					$cond[] = "paginas = " . $this->oBD->escapar($filtros["paginas"]);
			}
			if(!empty($filtros["codigo_tipo"])) {
					$cond[] = "codigo_tipo = " . $this->oBD->escapar($filtros["codigo_tipo"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre LIKE '" . $filtros["nombre"]."%'";
					// $cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["nombre_abreviado"])) {
					$cond[] = "nombre_abreviado = " . $this->oBD->escapar($filtros["nombre_abreviado"]);
			}
			if(!empty($filtros["precio"])) {
					$cond[] = "precio = " . $this->oBD->escapar($filtros["precio"]);
			}
			if(!empty($filtros["edicion"])) {
					$cond[] = "edicion = " . $this->oBD->escapar($filtros["edicion"]);
			}
			if(!empty($filtros["fec_publicacion"])) {
					$cond[] = "fec_publicacion = " . $this->oBD->escapar($filtros["fec_publicacion"]);
			}
			if(!empty($filtros["fec_creacion"])) {
					$cond[] = "fec_creacion = " . $this->oBD->escapar($filtros["fec_creacion"]);
			}
			if(!empty($filtros["fec_modificacion"])) {
					$cond[] = "fec_modificacion = " . $this->oBD->escapar($filtros["fec_modificacion"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(!empty($filtros["lugar"])) {
					$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if(!empty($filtros["condicion"])) {
					$cond[] = "condicion = " . $this->oBD->escapar($filtros["condicion"]);
			}
			if(!empty($filtros["resumen"])) {
					$cond[] = "resumen = " . $this->oBD->escapar($filtros["resumen"]);
			}
			if(isset($filtros["padre"])) {
					$cond[] = "padre = " . $this->oBD->escapar($filtros["padre"]);
			}
			if(!empty($filtros["id_idioma"])) {
					$cond[] = "id_idioma = " . $this->oBD->escapar($filtros["id_idioma"]);
			}
			if(!empty($filtros["id_tipo"]) || $filtros['id_tipo']==0) {
					$cond[] = "id_tipo = " . $this->oBD->escapar($filtros["id_tipo"]);
			}
			if(!empty($filtros["id_detalle"]) || $filtros['id_detalle']==0) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_setting"])) {
					$cond[] = "id_setting = " . $this->oBD->escapar($filtros["id_setting"]);
			}	
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}
			if(!empty($filtros["orden"]) || $filtros['orden']==0) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond)." GROUP BY archivo ORDER BY nombre ASC";
			}
			// var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	public function buscaraudios($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_estudio b left join bib_setting s on s.id_setting=b.id_setting";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($filtros["paginas"])) {
					$cond[] = "paginas = " . $this->oBD->escapar($filtros["paginas"]);
			}
			if(!empty($filtros["codigo_tipo"])) {
					$cond[] = "codigo_tipo = " . $this->oBD->escapar($filtros["codigo_tipo"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["nombre_abreviado"])) {
					$cond[] = "nombre_abreviado = " . $this->oBD->escapar($filtros["nombre_abreviado"]);
			}
			if(!empty($filtros["precio"])) {
					$cond[] = "precio = " . $this->oBD->escapar($filtros["precio"]);
			}
			if(!empty($filtros["edicion"])) {
					$cond[] = "edicion = " . $this->oBD->escapar($filtros["edicion"]);
			}
			if(!empty($filtros["fec_publicacion"])) {
					$cond[] = "fec_publicacion = " . $this->oBD->escapar($filtros["fec_publicacion"]);
			}
			if(!empty($filtros["fec_creacion"])) {
					$cond[] = "fec_creacion = " . $this->oBD->escapar($filtros["fec_creacion"]);
			}
			if(!empty($filtros["fec_modificacion"])) {
					$cond[] = "fec_modificacion = " . $this->oBD->escapar($filtros["fec_modificacion"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(!empty($filtros["lugar"])) {
					$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if(!empty($filtros["condicion"])) {
					$cond[] = "condicion = " . $this->oBD->escapar($filtros["condicion"]);
			}
			if(!empty($filtros["resumen"])) {
					$cond[] = "resumen = " . $this->oBD->escapar($filtros["resumen"]);
			}
			if(isset($filtros["padre"])) {
					$cond[] = "padre = " . $this->oBD->escapar($filtros["padre"]);
			}
			if(!empty($filtros["id_idioma"])) {
					$cond[] = "id_idioma = " . $this->oBD->escapar($filtros["id_idioma"]);
			}
			if(!empty($filtros["id_tipo"])) {
					$cond[] = "id_tipo = " . $this->oBD->escapar($filtros["id_tipo"]);
			}
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_setting"])) {
					$cond[] = "id_setting = " . $this->oBD->escapar($filtros["id_setting"]);
			}	
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond)." GROUP BY orden ORDER BY orden ASC ";
			}

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	public function buscarAvanzada($filtros=null)
		{
		try {

			$sql = "SELECT b.*,c.descripcion FROM bib_estudio b 
					inner join bib_det_estudio_autor d on d.id_estudio=b.id_estudio 
					inner join bib_autor aut on aut.id_autor=d.id_autor 
					inner join bib_detalle_subcategoria_estudio det on det.id_estudio=b.id_estudio 
					inner join bib_subcategoria s on s.id_subcategoria=det.id_subcategoria 
					inner join bib_categoria c on c.id_categoria=s.id_categoria";
						
		
			$cond = array();		

			if(!empty($filtros["id_categoria"])) {
					$cond[] = "c.id_categoria = " . $this->oBD->escapar($filtros["id_categoria"]);
			}
			if(!empty($filtros["id_autor"])) {
					$cond[] = "aut.id_autor  = " . $this->oBD->escapar($filtros["id_autor"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "b.nombre  " . $this->oBD->like($filtros["texto"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' OR ', $cond);
			}

			$sql.=" GROUP BY b.id_estudio";	
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function buscarFavoritos($filtros=null)
	{
		try {

			$sql = "SELECT b.id_estudio,b.id_tipo,b.foto FROM bib_estudio b inner join bib_detalle_usuario_estudio u on u.id_estudio=b.id_estudio";
						
		
			$cond = array();			

			if(!empty($filtros["id_usuario"])) {
					$cond[] = "b.id_usuario  = " . $this->oBD->escapar($filtros["id_usuario"]);
			}

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function buscarDescargados($filtros=null)
	{
		try {

			$sql = "SELECT * FROM bib_estudio b inner join bib_historial h on h.id_estudio=b.id_estudio where h.descripcion='descargar' ";
						
		
			$cond = array();		

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}

	public function buscarAudiolibros($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_estudio";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_estudio"])) {
				$cond[] = "padre = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql.=" GROUP BY nombre";	
			//$sql .= " ORDER BY fecha_creado ASC";
			#echo $sql; exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function reporte1($filtros=null)
	{
		try {
			$sql = "SELECT b.nombre, a.nombre as nombreAutor FROM bib_estudio b left join bib_det_estudio_autor det on det.id_estudio=b.id_estudio left join bib_autor a on a.id_autor=det.id_autor ";
						
			$cond = array();	
			if(!empty($filtros["id_autor"])) {
					$cond[] = "a.id_autor = " . $this->oBD->escapar($filtros["id_autor"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
				
			$sql.=" GROUP BY b.nombre";

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function reporte2($filtros=null)
	{
		try {
			$sql = "SELECT b.nombre, c.descripcion as categoria FROM bib_estudio b left join bib_detalle_subcategoria_estudio det on det.id_estudio=b.id_estudio left join bib_categoria c on c.id_categoria=det.id_categoria ";
						
			$cond = array();	
			if(!empty($filtros["id_categoria"])) {
					$cond[] = "c.id_categoria = " . $this->oBD->escapar($filtros["id_categoria"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
				
			$sql.=" GROUP BY b.nombre";

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function Audiolibros($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_estudio";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_tipo"])) {
					$cond[] = "id_tipo = " . $this->oBD->escapar($filtros["id_tipo"]);
			}
			if(isset($filtros["padre"])) {
					$cond[] = "padre = " . $this->oBD->escapar($filtros["padre"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql.=" GROUP BY nombre";
			//$sql .= " ORDER BY fecha_creado ASC";
			#echo  $sql;  exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}

	public function listarall($nombre)
	{
		try {
			$sql = "SELECT * FROM bib_estudio where nombre like '%".$nombre."%'";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function insertar($paginas,$codigo_tipo,$codigo,$nombre,$nombre_abreviado,$precio,$edicion,$fec_publicacion,$fec_creacion,$fec_modificacion,$foto,$archivo,$link,$lugar,$condicion,$resumen,$id_idioma,$id_tipo,$id_detalle,$id_setting,$id_editorial,$padre,$orden)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_estudio_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_estudio) FROM bib_estudio");
			++$id;
			
			$estados = array('id_estudio' => $id
							
							,'paginas'=>$paginas
							,'codigo_tipo'=>$codigo_tipo
							,'codigo'=>$codigo
							,'nombre'=>$nombre
							,'nombre_abreviado'=>$nombre_abreviado
							,'precio'=>$precio
							,'edicion'=>$edicion
							,'fec_publicacion'=>$fec_publicacion
							#,'fec_creacion'=>$fec_creacion
							#,'fec_modificacion'=>$fec_modificacion
							,'foto'=>$foto
							,'archivo'=>$archivo
							,'link'=>$link
							,'lugar'=>$lugar
							,'condicion'=>$condicion
							,'resumen'=>$resumen
							,'id_idioma'=>$id_idioma
							,'id_tipo'=>$id_tipo
							,'id_detalle'=>$id_detalle
							,'id_setting'=>$id_setting
							,'id_editorial'=>$id_editorial							
							,'padre'=>$padre							
							,'orden'=>$orden							
							);
			
			$this->oBD->insert('bib_estudio', $estados);			
			$this->terminarTransaccion('dat_bib_estudio_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_estudio_insert');
			throw new Exception("ERRORRRR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function actualizar($id, $paginas,$codigo_tipo,$codigo,$nombre,$nombre_abreviado,$precio,$edicion,$fec_publicacion,$fec_creacion,$fec_modificacion,$foto,$archivo,$link,$lugar,$condicion,$resumen,$id_idioma,$id_tipo,$id_detalle,$id_setting,$id_editorial,$padre,$orden)
	{
		try {
			$this->iniciarTransaccion('dat_bib_estudio_update');
			$estados = array('paginas'=>$paginas
							,'codigo_tipo'=>$codigo_tipo
							,'codigo'=>$codigo
							,'nombre'=>$nombre
							,'nombre_abreviado'=>$nombre_abreviado
							,'precio'=>$precio
							,'edicion'=>$edicion
							,'fec_publicacion'=>$fec_publicacion
							,'fec_creacion'=>$fec_creacion
							,'fec_modificacion'=>$fec_modificacion
							,'foto'=>$foto
							,'archivo'=>$archivo
							,'link'=>$link
							,'lugar'=>$lugar
							,'condicion'=>$condicion
							,'resumen'=>$resumen
							,'id_idioma'=>$id_idioma
							,'id_tipo'=>$id_tipo
							,'id_detalle'=>$id_detalle
							,'id_setting'=>$id_setting
							,'id_editorial'=>$id_editorial		
							,'padre'=>$padre							
							,'orden'=>$orden							
							);
			
			$this->oBD->update('bib_estudio ', $estados, array('id_estudio' => $id));
		    $this->terminarTransaccion('dat_bib_estudio_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_estudio  "
					. " WHERE id_estudio = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			var_dump($id);
			return $this->oBD->delete('bib_estudio', array('id_estudio' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_estudio', array($propiedad => $valor), array('id_estudio' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	public function eliminarbookstabla($tabla,$campo){
		$dt=$this->oBD->consultarSQL("SELECT * FROM bib_estudio ORDER BY id_estudio ASC");
		
		foreach($dt as $per){
			$rest = substr($per['archivo'], -4); 
			if ($rest == '.pdf') {
				// echo $per['archivo'].'<br>';
				// $x=$this->oBD->delete()
				$x=$this->oBD->delete($tabla, array($campo => $per['id_estudio']));
				echo $x."- ".$per["archivo"].'<br>';
			}
		}
	}	
}