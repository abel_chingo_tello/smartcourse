<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatReportes_resumen extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Reportes_resumen").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM reportes_resumen";			
			
			$cond = array();		
					
			
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["fecha_modificacion"])) {
					$cond[] = "fecha_modificacion = " . $this->oBD->escapar($filtros["fecha_modificacion"]);
			}
			if(isset($filtros["prog_curso_A1"])) {
					$cond[] = "prog_curso_A1 = " . $this->oBD->escapar($filtros["prog_curso_A1"]);
			}
			if(isset($filtros["prog_hab_L_A1"])) {
					$cond[] = "prog_hab_L_A1 = " . $this->oBD->escapar($filtros["prog_hab_L_A1"]);
			}
			if(isset($filtros["prog_hab_R_A1"])) {
					$cond[] = "prog_hab_R_A1 = " . $this->oBD->escapar($filtros["prog_hab_R_A1"]);
			}
			if(isset($filtros["prog_hab_W_A1"])) {
					$cond[] = "prog_hab_W_A1 = " . $this->oBD->escapar($filtros["prog_hab_W_A1"]);
			}
			if(isset($filtros["prog_hab_S_A1"])) {
					$cond[] = "prog_hab_S_A1 = " . $this->oBD->escapar($filtros["prog_hab_S_A1"]);
			}
			if(isset($filtros["prog_curso_A2"])) {
					$cond[] = "prog_curso_A2 = " . $this->oBD->escapar($filtros["prog_curso_A2"]);
			}
			if(isset($filtros["prog_hab_L_A2"])) {
					$cond[] = "prog_hab_L_A2 = " . $this->oBD->escapar($filtros["prog_hab_L_A2"]);
			}
			if(isset($filtros["prog_hab_R_A2"])) {
					$cond[] = "prog_hab_R_A2 = " . $this->oBD->escapar($filtros["prog_hab_R_A2"]);
			}
			if(isset($filtros["prog_hab_W_A2"])) {
					$cond[] = "prog_hab_W_A2 = " . $this->oBD->escapar($filtros["prog_hab_W_A2"]);
			}
			if(isset($filtros["prog_hab_S_A2"])) {
					$cond[] = "prog_hab_S_A2 = " . $this->oBD->escapar($filtros["prog_hab_S_A2"]);
			}
			if(isset($filtros["prog_curso_B1"])) {
					$cond[] = "prog_curso_B1 = " . $this->oBD->escapar($filtros["prog_curso_B1"]);
			}
			if(isset($filtros["prog_hab_L_B1"])) {
					$cond[] = "prog_hab_L_B1 = " . $this->oBD->escapar($filtros["prog_hab_L_B1"]);
			}
			if(isset($filtros["prog_hab_R_B1"])) {
					$cond[] = "prog_hab_R_B1 = " . $this->oBD->escapar($filtros["prog_hab_R_B1"]);
			}
			if(isset($filtros["prog_hab_W_B1"])) {
					$cond[] = "prog_hab_W_B1 = " . $this->oBD->escapar($filtros["prog_hab_W_B1"]);
			}
			if(isset($filtros["prog_hab_S_B1"])) {
					$cond[] = "prog_hab_S_B1 = " . $this->oBD->escapar($filtros["prog_hab_S_B1"]);
			}
			if(isset($filtros["prog_curso_B2"])) {
					$cond[] = "prog_curso_B2 = " . $this->oBD->escapar($filtros["prog_curso_B2"]);
			}
			if(isset($filtros["prog_hab_L_B2"])) {
					$cond[] = "prog_hab_L_B2 = " . $this->oBD->escapar($filtros["prog_hab_L_B2"]);
			}
			if(isset($filtros["prog_hab_R_B2"])) {
					$cond[] = "prog_hab_R_B2 = " . $this->oBD->escapar($filtros["prog_hab_R_B2"]);
			}
			if(isset($filtros["prog_hab_W_B2"])) {
					$cond[] = "prog_hab_W_B2 = " . $this->oBD->escapar($filtros["prog_hab_W_B2"]);
			}
			if(isset($filtros["prog_hab_S_B2"])) {
					$cond[] = "prog_hab_S_B2 = " . $this->oBD->escapar($filtros["prog_hab_S_B2"]);
			}
			if(isset($filtros["prog_curso_C1"])) {
					$cond[] = "prog_curso_C1 = " . $this->oBD->escapar($filtros["prog_curso_C1"]);
			}
			if(isset($filtros["prog_hab_L_C1"])) {
					$cond[] = "prog_hab_L_C1 = " . $this->oBD->escapar($filtros["prog_hab_L_C1"]);
			}
			if(isset($filtros["prog_hab_R_C1"])) {
					$cond[] = "prog_hab_R_C1 = " . $this->oBD->escapar($filtros["prog_hab_R_C1"]);
			}
			if(isset($filtros["prog_hab_W_C1"])) {
					$cond[] = "prog_hab_W_C1 = " . $this->oBD->escapar($filtros["prog_hab_W_C1"]);
			}
			if(isset($filtros["prog_hab_S_C1"])) {
					$cond[] = "prog_hab_S_C1 = " . $this->oBD->escapar($filtros["prog_hab_S_C1"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Reportes_resumen").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idalumno,$idlocal,$fecha_modificacion,$prog_curso_A1,$prog_hab_L_A1,$prog_hab_R_A1,$prog_hab_W_A1,$prog_hab_S_A1,$prog_curso_A2,$prog_hab_L_A2,$prog_hab_R_A2,$prog_hab_W_A2,$prog_hab_S_A2,$prog_curso_B1,$prog_hab_L_B1,$prog_hab_R_B1,$prog_hab_W_B1,$prog_hab_S_B1,$prog_curso_B2,$prog_hab_L_B2,$prog_hab_R_B2,$prog_hab_W_B2,$prog_hab_S_B2,$prog_curso_C1,$prog_hab_L_C1,$prog_hab_R_C1,$prog_hab_W_C1,$prog_hab_S_C1)
	{
		try {
			
			$this->iniciarTransaccion('dat_reportes_resumen_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idresumen) FROM reportes_resumen");
			++$id;
			
			$estados = array('idresumen' => $id
							
							,'idalumno'=>$idalumno
							,'idlocal'=>$idlocal
							,'fecha_modificacion'=>$fecha_modificacion
							,'prog_curso_A1'=>$prog_curso_A1
							,'prog_hab_L_A1'=>$prog_hab_L_A1
							,'prog_hab_R_A1'=>$prog_hab_R_A1
							,'prog_hab_W_A1'=>$prog_hab_W_A1
							,'prog_hab_S_A1'=>$prog_hab_S_A1
							,'prog_curso_A2'=>$prog_curso_A2
							,'prog_hab_L_A2'=>$prog_hab_L_A2
							,'prog_hab_R_A2'=>$prog_hab_R_A2
							,'prog_hab_W_A2'=>$prog_hab_W_A2
							,'prog_hab_S_A2'=>$prog_hab_S_A2
							,'prog_curso_B1'=>$prog_curso_B1
							,'prog_hab_L_B1'=>$prog_hab_L_B1
							,'prog_hab_R_B1'=>$prog_hab_R_B1
							,'prog_hab_W_B1'=>$prog_hab_W_B1
							,'prog_hab_S_B1'=>$prog_hab_S_B1
							,'prog_curso_B2'=>$prog_curso_B2
							,'prog_hab_L_B2'=>$prog_hab_L_B2
							,'prog_hab_R_B2'=>$prog_hab_R_B2
							,'prog_hab_W_B2'=>$prog_hab_W_B2
							,'prog_hab_S_B2'=>$prog_hab_S_B2
							,'prog_curso_C1'=>$prog_curso_C1
							,'prog_hab_L_C1'=>$prog_hab_L_C1
							,'prog_hab_R_C1'=>$prog_hab_R_C1
							,'prog_hab_W_C1'=>$prog_hab_W_C1
							,'prog_hab_S_C1'=>$prog_hab_S_C1							
							);
			
			$this->oBD->insert('reportes_resumen', $estados);			
			$this->terminarTransaccion('dat_reportes_resumen_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_reportes_resumen_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Reportes_resumen").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idalumno,$idlocal,$fecha_modificacion,$prog_curso_A1,$prog_hab_L_A1,$prog_hab_R_A1,$prog_hab_W_A1,$prog_hab_S_A1,$prog_curso_A2,$prog_hab_L_A2,$prog_hab_R_A2,$prog_hab_W_A2,$prog_hab_S_A2,$prog_curso_B1,$prog_hab_L_B1,$prog_hab_R_B1,$prog_hab_W_B1,$prog_hab_S_B1,$prog_curso_B2,$prog_hab_L_B2,$prog_hab_R_B2,$prog_hab_W_B2,$prog_hab_S_B2,$prog_curso_C1,$prog_hab_L_C1,$prog_hab_R_C1,$prog_hab_W_C1,$prog_hab_S_C1)
	{
		try {
			$this->iniciarTransaccion('dat_reportes_resumen_update');
			$estados = array('idalumno'=>$idalumno
							,'idlocal'=>$idlocal
							,'fecha_modificacion'=>$fecha_modificacion
							,'prog_curso_A1'=>$prog_curso_A1
							,'prog_hab_L_A1'=>$prog_hab_L_A1
							,'prog_hab_R_A1'=>$prog_hab_R_A1
							,'prog_hab_W_A1'=>$prog_hab_W_A1
							,'prog_hab_S_A1'=>$prog_hab_S_A1
							,'prog_curso_A2'=>$prog_curso_A2
							,'prog_hab_L_A2'=>$prog_hab_L_A2
							,'prog_hab_R_A2'=>$prog_hab_R_A2
							,'prog_hab_W_A2'=>$prog_hab_W_A2
							,'prog_hab_S_A2'=>$prog_hab_S_A2
							,'prog_curso_B1'=>$prog_curso_B1
							,'prog_hab_L_B1'=>$prog_hab_L_B1
							,'prog_hab_R_B1'=>$prog_hab_R_B1
							,'prog_hab_W_B1'=>$prog_hab_W_B1
							,'prog_hab_S_B1'=>$prog_hab_S_B1
							,'prog_curso_B2'=>$prog_curso_B2
							,'prog_hab_L_B2'=>$prog_hab_L_B2
							,'prog_hab_R_B2'=>$prog_hab_R_B2
							,'prog_hab_W_B2'=>$prog_hab_W_B2
							,'prog_hab_S_B2'=>$prog_hab_S_B2
							,'prog_curso_C1'=>$prog_curso_C1
							,'prog_hab_L_C1'=>$prog_hab_L_C1
							,'prog_hab_R_C1'=>$prog_hab_R_C1
							,'prog_hab_W_C1'=>$prog_hab_W_C1
							,'prog_hab_S_C1'=>$prog_hab_S_C1								
							);
			
			$this->oBD->update('reportes_resumen ', $estados, array('idresumen' => $id));
		    $this->terminarTransaccion('dat_reportes_resumen_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Reportes_resumen").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM reportes_resumen  "
					. " WHERE idresumen = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Reportes_resumen").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('reportes_resumen', array('idresumen' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Reportes_resumen").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('reportes_resumen', array($propiedad => $valor), array('idresumen' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Reportes_resumen").": " . $e->getMessage());
		}
	}
   
		
}