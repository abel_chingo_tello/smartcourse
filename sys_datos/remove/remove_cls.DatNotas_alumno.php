<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatNotas_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Notas_alumno").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM notas_alumno";			
			
			$cond = array();		
					
			
			if(isset($filtros["nombres"])) {
					$cond[] = "nombres = " . $this->oBD->escapar($filtros["nombres"]);
			}
			if(isset($filtros["apellidos"])) {
					$cond[] = "apellidos = " . $this->oBD->escapar($filtros["apellidos"]);
			}
			if(isset($filtros["identificador"])) {
					$cond[] = "identificador = " . $this->oBD->escapar($filtros["identificador"]);
			}
			if(isset($filtros["idarchivo"])) {
					$cond[] = "idarchivo = " . $this->oBD->escapar($filtros["idarchivo"]);
			}
			if(isset($filtros["origen"])) {
					$cond[] = "origen = " . $this->oBD->escapar($filtros["origen"]);
			}
			if(isset($filtros["fechareg"])) {
					$cond[] = "fechareg = " . $this->oBD->escapar($filtros["fechareg"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Notas_alumno").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombres,$apellidos,$identificador,$idarchivo,$origen,$fechareg)
	{
		try {
			
			$this->iniciarTransaccion('dat_notas_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idalumno) FROM notas_alumno");
			++$id;
			
			$estados = array('idalumno' => $id
							
							,'nombres'=>$nombres
							,'apellidos'=>$apellidos
							,'identificador'=>$identificador
							,'idarchivo'=>$idarchivo
							,'origen'=>$origen
							,'fechareg'=>$fechareg							
							);
			
			$this->oBD->insert('notas_alumno', $estados);			
			$this->terminarTransaccion('dat_notas_alumno_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_notas_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Notas_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombres,$apellidos,$identificador,$idarchivo,$origen,$fechareg)
	{
		try {
			$this->iniciarTransaccion('dat_notas_alumno_update');
			$estados = array('nombres'=>$nombres
							,'apellidos'=>$apellidos
							,'identificador'=>$identificador
							,'idarchivo'=>$idarchivo
							,'origen'=>$origen
							,'fechareg'=>$fechareg								
							);
			
			$this->oBD->update('notas_alumno ', $estados, array('idalumno' => $id));
		    $this->terminarTransaccion('dat_notas_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM notas_alumno  "
					. " WHERE idalumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Notas_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('notas_alumno', array('idalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Notas_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('notas_alumno', array($propiedad => $valor), array('idalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas_alumno").": " . $e->getMessage());
		}
	}
   
		
}