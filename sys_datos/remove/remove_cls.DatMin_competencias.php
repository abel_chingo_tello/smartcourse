<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatMin_competencias extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM min_competencias";
			
			$cond = array();		
			
			if(isset($filtros["idcompetencia"])) {
					$cond[] = "idcompetencia = " . $this->oBD->escapar($filtros["idcompetencia"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}
	public function competencias($filtros = null){
		try{
			$sql = "SELECT c.idcompetencia ,c.titulo AS competencia, c.descripcion AS descripcion_competencia,c.habilidad,cc.idcapacidad,cc.titulo AS capacidad,cc.descripcion AS descripcion_capacidad FROM min_competencias c LEFT JOIN min_capacidades cc ON c.idcompetencia = cc.idcompetencia";
			$cond = array();

			if(isset($filtros['estado'])){
				$cond[] = "c.estado = ".$this->oBD->escapar($filtros["estado"]);
			}

			if(!empty($cond)){
				$sql .= " WHERE ".implode(' AND ', $cond);
			}

			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e){
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM min_competencias";			
			
			$cond = array();		
					
			
			if(isset($filtros["idcompetencia"])) {
					$cond[] = "idcompetencia = " . $this->oBD->escapar($filtros["idcompetencia"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($titulo,$descripcion,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_min_competencias_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcompetencia) FROM min_competencias");
			++$id;
			
			$estados = array('idcompetencia' => $id
							
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('min_competencias', $estados);			
			$this->terminarTransaccion('dat_min_competencias_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_min_competencias_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $titulo,$descripcion,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_min_competencias_update');
			$estados = array('titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'estado'=>$estado								
							);
			
			$this->oBD->update('min_competencias ', $estados, array('idcompetencia' => $id));
		    $this->terminarTransaccion('dat_min_competencias_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM min_competencias  "
					. " WHERE idcompetencia = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('min_competencias', array('idcompetencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('min_competencias', array($propiedad => $valor), array('idcompetencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}
   
		
}