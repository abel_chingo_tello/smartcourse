<?php

class DatResumen extends DatBase{
    public function __construct(){
        try{
            parent::conectar();
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    
    public function ubicacion_docente($filtros = null){
        try{
            $sql = "SELECT nq.* FROM notas_quiz nq INNER JOIN acad_grupoauladetalle gad ON nq.idalumno = gad.iddocente";
            $cond = array();
            
            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "nq.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }
            if(isset($filtros['idrecurso'])){
                $cond[] = "nq.idrecurso = ".$this->oBD->escapar($filtros["idrecurso"]);
            }
            if(isset($filtros['idproyecto'])){
                $cond[] = "nq.idproyecto = ".$this->oBD->escapar($filtros["idproyecto"]);
            }

            if(!empty($cond)){
                $sql .= " WHERE ".implode(" AND ",$cond);
            }
            $sql .= " GROUP BY nq.idnota";
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function ubicacion($filtros = null){
        try{
            $sql = "SELECT nq.* FROM notas_quiz nq INNER JOIN acad_matricula ma ON ma.idalumno = nq.idalumno INNER JOIN acad_grupoauladetalle gad ON ma.idgrupoauladetalle = gad.idgrupoauladetalle";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "nq.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }
            if(isset($filtros['idrecurso'])){
                $cond[] = "nq.idrecurso = ".$this->oBD->escapar($filtros["idrecurso"]);
            }
            if(isset($filtros['idproyecto'])){
                $cond[] = "nq.idproyecto = ".$this->oBD->escapar($filtros["idproyecto"]);
            }
            
            if(!empty($cond)){
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function examenes_doc($filtros = null){
        try{
            $sql = "SELECT nq.* FROM notas_quiz nq INNER JOIN acad_grupoauladetalle gad ON gad.iddocente = nq.idalumno INNER JOIN acad_cursodetalle cd ON cd.idcurso = gad.idcurso";
            $cond = array();
            
            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "nq.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }
            if(isset($filtros['idrecurso'])){
                $cond[] = "nq.idrecurso = ".$this->oBD->escapar($filtros["idrecurso"]);
            }
            if(isset($filtros['idproyecto'])){
                $cond[] = "nq.idproyecto = ".$this->oBD->escapar($filtros["idproyecto"]);
            }
            if(isset($filtros['entrada'])){
                $cond[] = "nq.datos like '%\"tipo\":\"E\"%'";
            }
            if(isset($filtros['salida'])){
                $cond[] = "nq.datos like '%\"tipo\":\"S\"%'";
            }
            if(isset($filtros['bimestre'])){
                $cond[] = "nq.datos like '%\"tipo\":\"B\",\"num_examen\":\"".$filtros['bimestre']."\"%'";
            }
            if(isset($filtros['trimestre'])){
                $cond[] = "nq.datos like '%\"tipo\":\"T\",\"num_examen\":\"".$filtros['trimestre']."\"%'";
            }
            $cond[] = "nq.idrecurso = cd.idrecurso";

            if(!empty($cond)){
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function examenes($filtros = null){
        try{
            $sql = "SELECT nq.* FROM notas_quiz nq INNER JOIN acad_matricula ma ON ma.idalumno = nq.idalumno INNER JOIN acad_grupoauladetalle gad ON ma.idgrupoauladetalle = gad.idgrupoauladetalle";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "nq.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }
            if(isset($filtros['idrecurso'])){
                $cond[] = "nq.idrecurso = ".$this->oBD->escapar($filtros["idrecurso"]);
            }
            if(isset($filtros['idproyecto'])){
                $cond[] = "nq.idproyecto = ".$this->oBD->escapar($filtros["idproyecto"]);
            }
            if(isset($filtros['entrada'])){
                $cond[] = "nq.datos like '%\"tipo\":\"E\"%'";
            }
            if(isset($filtros['salida'])){
                $cond[] = "nq.datos like '%\"tipo\":\"S\"%'";
            }
            if(isset($filtros['bimestre'])){
                $cond[] = "nq.datos like '%\"tipo\":\"B\",\"num_examen\":\"".$filtros['bimestre']."\"%'";
            }
            if(isset($filtros['trimestre'])){
                $cond[] = "nq.datos like '%\"tipo\":\"T\",\"num_examen\":\"".$filtros['trimestre']."\"%'";
            }
            
            if(!empty($cond)){
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function promedio_doc($filtros = null){
        try{
            $sql = "SELECT (SELECT round(SUM(nq.nota)/count(nq.nota),2)) as resultado, (SELECT count(nq.nota)) as cantidad FROM notas_quiz nq INNER JOIN acad_grupoauladetalle gad ON nq.idalumno = gad.iddocente INNER JOIN acad_cursodetalle cd ON cd.idcurso = gad.idcurso";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "nq.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }
            if(isset($filtros['idrecurso'])){
                $cond[] = "nq.idrecurso = ".$this->oBD->escapar($filtros["idrecurso"]);
            }
            if(isset($filtros['idproyecto'])){
                $cond[] = "nq.idproyecto = ".$this->oBD->escapar($filtros["idproyecto"]);
            }
            if(isset($filtros['entrada'])){
                $cond[] = "nq.datos like '%\"tipo\":\"E\"%'";
            }
            if(isset($filtros['salida'])){
                $cond[] = "nq.datos like '%\"tipo\":\"S\"%'";
            }
            if(isset($filtros['bimestre'])){
                $cond[] = "nq.datos like '%\"tipo\":\"B\",\"num_examen\":\"".$filtros['bimestre']."\"%'";
            }
            if(isset($filtros['trimestre'])){
                $cond[] = "nq.datos like '%\"tipo\":\"T\",\"num_examen\":\"".$filtros['trimestre']."\"%'";
            }
            $cond[] = "nq.idrecurso = cd.idrecurso";


            if(!empty($cond)){
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function promedio($filtros = null){
        try{
            $sql = "SELECT (SELECT round(SUM(nq.nota)/count(nq.nota),2)) as resultado, (SELECT count(nq.nota)) as cantidad FROM notas_quiz nq INNER JOIN acad_matricula ma ON ma.idalumno = nq.idalumno INNER JOIN acad_grupoauladetalle gad ON ma.idgrupoauladetalle = gad.idgrupoauladetalle";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "nq.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }
            if(isset($filtros['idrecurso'])){
                $cond[] = "nq.idrecurso = ".$this->oBD->escapar($filtros["idrecurso"]);
            }
            if(isset($filtros['idproyecto'])){
                $cond[] = "nq.idproyecto = ".$this->oBD->escapar($filtros["idproyecto"]);
            }
            if(isset($filtros['entrada'])){
                $cond[] = "nq.datos like '%\"tipo\":\"E\"%'";
            }
            if(isset($filtros['salida'])){
                $cond[] = "nq.datos like '%\"tipo\":\"S\"%'";
            }
            if(isset($filtros['bimestre'])){
                $cond[] = "nq.datos like '%\"tipo\":\"B\",\"num_examen\":\"".$filtros['bimestre']."\"%'";
            }
            if(isset($filtros['trimestre'])){
                $cond[] = "nq.datos like '%\"tipo\":\"T\",\"num_examen\":\"".$filtros['trimestre']."\"%'";
            }
            
            if(!empty($cond)){
                $sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function tiempos_doc($filtros = null){
        try{
            $sql = "SELECT SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'P')) AS tiempopv,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'P')) AS total_tiempopv, SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'E')) AS tiempoe,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'E')) AS total_tiempoe, SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'T')) AS tiempot,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'T')) AS total_tiempot,SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'TR')) AS tiempos,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'TR')) AS total_tiempos,SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'A')) AS tiempop,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = gad.iddocente AND hs.lugar = 'A')) AS total_tiempop FROM acad_grupoauladetalle gad";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(!empty($cond)){
                $sql .= " WHERE ".implode(' AND ',$cond);
            }
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function tiempos($filtros = null){
        try{
            $sql = "SELECT SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'P')) AS tiempopv,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'P')) AS total_tiempopv, SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'E')) AS tiempoe,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'E')) AS total_tiempoe, SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'T')) AS tiempot,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'T')) AS total_tiempot,SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'TR')) AS tiempos,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'TR')) AS total_tiempos,SUM((SELECT SUM(TIME_TO_SEC(TIMEDIFF(hs.fechasalida, hs.fechaentrada))) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'A')) AS tiempop,SUM((SELECT COUNT(hs.idhistorialsesion) FROM historial_sesion hs WHERE hs.idusuario = ma.idalumno AND hs.lugar = 'A')) AS total_tiempop FROM acad_matricula ma INNER JOIN acad_grupoauladetalle gad ON ma.idgrupoauladetalle = gad.idgrupoauladetalle";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "nq.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }
            if(isset($filtros['idrecurso'])){
                $cond[] = "nq.idrecurso = ".$this->oBD->escapar($filtros["idrecurso"]);
            }
            if(isset($filtros['idproyecto'])){
                $cond[] = "nq.idproyecto = ".$this->oBD->escapar($filtros["idproyecto"]);
            }

            if(!empty($cond)){
                $sql .= " WHERE ".implode(' AND ',$cond);
            }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function progresos_doc($filtros = null){
        try{
            $sql = "SELECT (SELECT ROUND(SUM(rs.prog_curso_A1) / count(IF(rs.prog_curso_A1>0,1,0)),2)) AS prog_curso_A1,
            (SELECT ROUND(SUM(rs.prog_hab_L_A1) / count(IF(rs.prog_hab_L_A1>0,1,NULL)),2)) AS prog_hab_L_A1,
            (SELECT ROUND(SUM(rs.prog_hab_R_A1) / count(IF(rs.prog_hab_R_A1>0,1,NULL)),2)) AS prog_hab_R_A1,
            (SELECT ROUND(SUM(rs.prog_hab_W_A1) / count(IF(rs.prog_hab_W_A1>0,1,NULL)),2)) AS prog_hab_W_A1,
            (SELECT ROUND(SUM(rs.prog_hab_S_A1) / count(IF(rs.prog_hab_S_A1>0,1,NULL)),2)) AS prog_hab_S_A1,
            (SELECT ROUND(SUM(rs.prog_curso_A2) / count(IF(rs.prog_curso_A2>0,1,NULL)),2)) AS prog_curso_A2,
            (SELECT ROUND(SUM(rs.prog_hab_L_A2) / count(IF(rs.prog_hab_L_A2>0,1,NULL)),2)) AS prog_hab_L_A2,
            (SELECT ROUND(SUM(rs.prog_hab_R_A2) / count(IF(rs.prog_hab_R_A2>0,1,NULL)),2)) AS prog_hab_R_A2,
            (SELECT ROUND(SUM(rs.prog_hab_W_A2) / count(IF(rs.prog_hab_W_A2>0,1,NULL)),2)) AS prog_hab_W_A2,
            (SELECT ROUND(SUM(rs.prog_hab_S_A2) / count(IF(rs.prog_hab_S_A2>0,1,NULL)),2)) AS prog_hab_S_A2,
            (SELECT ROUND(SUM(rs.prog_curso_B1) / count(IF(rs.prog_curso_B1>0,1,NULL)),2)) AS prog_curso_B1,
            (SELECT ROUND(SUM(rs.prog_hab_L_B1) / count(IF(rs.prog_hab_L_B1>0,1,NULL)),2)) AS prog_hab_L_B1,
            (SELECT ROUND(SUM(rs.prog_hab_R_B1) / count(IF(rs.prog_hab_R_B1>0,1,NULL)),2)) AS prog_hab_R_B1,
            (SELECT ROUND(SUM(rs.prog_hab_W_B1) / count(IF(rs.prog_hab_W_B1>0,1,NULL)),2)) AS prog_hab_W_B1,
            (SELECT ROUND(SUM(rs.prog_hab_S_B1) / count(IF(rs.prog_hab_S_B1>0,1,NULL)),2)) AS prog_hab_S_B1,
            (SELECT ROUND(SUM(rs.prog_curso_B2) / count(IF(rs.prog_curso_B2>0,1,NULL)),2)) AS prog_curso_B2,
            (SELECT ROUND(SUM(rs.prog_hab_L_B2) / count(IF(rs.prog_hab_L_B2>0,1,NULL)),2)) AS prog_hab_L_B2,
            (SELECT ROUND(SUM(rs.prog_hab_R_B2) / count(IF(rs.prog_hab_R_B2>0,1,NULL)),2)) AS prog_hab_R_B2,
            (SELECT ROUND(SUM(rs.prog_hab_W_B2) / count(IF(rs.prog_hab_W_B2>0,1,NULL)),2)) AS prog_hab_W_B2,
            (SELECT ROUND(SUM(rs.prog_hab_S_B2) / count(IF(rs.prog_hab_S_B2>0,1,NULL)),2)) AS prog_hab_S_B2,
            (SELECT ROUND(SUM(rs.prog_curso_C1) / count(IF(rs.prog_curso_C1>0,1,NULL)),2)) AS prog_curso_C1,
            (SELECT ROUND(SUM(rs.prog_hab_L_C1) / count(IF(rs.prog_hab_L_C1>0,1,NULL)),2)) AS prog_hab_L_C1,
            (SELECT ROUND(SUM(rs.prog_hab_R_C1) / count(IF(rs.prog_hab_R_C1>0,1,NULL)),2)) AS prog_hab_R_C1,
            (SELECT ROUND(SUM(rs.prog_hab_W_C1) / count(IF(rs.prog_hab_W_C1>0,1,NULL)),2)) AS prog_hab_W_C1,
            (SELECT ROUND(SUM(rs.prog_hab_S_C1) / count(IF(rs.prog_hab_S_C1>0,1,NULL)),2)) AS prog_hab_S_C1
            FROM reportes_resumen rs INNER JOIN acad_grupoauladetalle gad ON rs.idalumno = gad.iddocente";
            $cond = array();
            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(!empty($cond)){
                $sql .= " WHERE ".implode(" AND ", $cond);
            }
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function progresos($filtros = null){
        try{
            $sql = "SELECT (SELECT ROUND(SUM(rs.prog_curso_A1) / count(IF(rs.prog_curso_A1>0,1,0)),2)) AS prog_curso_A1,
            (SELECT ROUND(SUM(rs.prog_hab_L_A1) / count(IF(rs.prog_hab_L_A1>0,1,NULL)),2)) AS prog_hab_L_A1,
            (SELECT ROUND(SUM(rs.prog_hab_R_A1) / count(IF(rs.prog_hab_R_A1>0,1,NULL)),2)) AS prog_hab_R_A1,
            (SELECT ROUND(SUM(rs.prog_hab_W_A1) / count(IF(rs.prog_hab_W_A1>0,1,NULL)),2)) AS prog_hab_W_A1,
            (SELECT ROUND(SUM(rs.prog_hab_S_A1) / count(IF(rs.prog_hab_S_A1>0,1,NULL)),2)) AS prog_hab_S_A1,
            (SELECT ROUND(SUM(rs.prog_curso_A2) / count(IF(rs.prog_curso_A2>0,1,NULL)),2)) AS prog_curso_A2,
            (SELECT ROUND(SUM(rs.prog_hab_L_A2) / count(IF(rs.prog_hab_L_A2>0,1,NULL)),2)) AS prog_hab_L_A2,
            (SELECT ROUND(SUM(rs.prog_hab_R_A2) / count(IF(rs.prog_hab_R_A2>0,1,NULL)),2)) AS prog_hab_R_A2,
            (SELECT ROUND(SUM(rs.prog_hab_W_A2) / count(IF(rs.prog_hab_W_A2>0,1,NULL)),2)) AS prog_hab_W_A2,
            (SELECT ROUND(SUM(rs.prog_hab_S_A2) / count(IF(rs.prog_hab_S_A2>0,1,NULL)),2)) AS prog_hab_S_A2,
            (SELECT ROUND(SUM(rs.prog_curso_B1) / count(IF(rs.prog_curso_B1>0,1,NULL)),2)) AS prog_curso_B1,
            (SELECT ROUND(SUM(rs.prog_hab_L_B1) / count(IF(rs.prog_hab_L_B1>0,1,NULL)),2)) AS prog_hab_L_B1,
            (SELECT ROUND(SUM(rs.prog_hab_R_B1) / count(IF(rs.prog_hab_R_B1>0,1,NULL)),2)) AS prog_hab_R_B1,
            (SELECT ROUND(SUM(rs.prog_hab_W_B1) / count(IF(rs.prog_hab_W_B1>0,1,NULL)),2)) AS prog_hab_W_B1,
            (SELECT ROUND(SUM(rs.prog_hab_S_B1) / count(IF(rs.prog_hab_S_B1>0,1,NULL)),2)) AS prog_hab_S_B1,
            (SELECT ROUND(SUM(rs.prog_curso_B2) / count(IF(rs.prog_curso_B2>0,1,NULL)),2)) AS prog_curso_B2,
            (SELECT ROUND(SUM(rs.prog_hab_L_B2) / count(IF(rs.prog_hab_L_B2>0,1,NULL)),2)) AS prog_hab_L_B2,
            (SELECT ROUND(SUM(rs.prog_hab_R_B2) / count(IF(rs.prog_hab_R_B2>0,1,NULL)),2)) AS prog_hab_R_B2,
            (SELECT ROUND(SUM(rs.prog_hab_W_B2) / count(IF(rs.prog_hab_W_B2>0,1,NULL)),2)) AS prog_hab_W_B2,
            (SELECT ROUND(SUM(rs.prog_hab_S_B2) / count(IF(rs.prog_hab_S_B2>0,1,NULL)),2)) AS prog_hab_S_B2,
            (SELECT ROUND(SUM(rs.prog_curso_C1) / count(IF(rs.prog_curso_C1>0,1,NULL)),2)) AS prog_curso_C1,
            (SELECT ROUND(SUM(rs.prog_hab_L_C1) / count(IF(rs.prog_hab_L_C1>0,1,NULL)),2)) AS prog_hab_L_C1,
            (SELECT ROUND(SUM(rs.prog_hab_R_C1) / count(IF(rs.prog_hab_R_C1>0,1,NULL)),2)) AS prog_hab_R_C1,
            (SELECT ROUND(SUM(rs.prog_hab_W_C1) / count(IF(rs.prog_hab_W_C1>0,1,NULL)),2)) AS prog_hab_W_C1,
            (SELECT ROUND(SUM(rs.prog_hab_S_C1) / count(IF(rs.prog_hab_S_C1>0,1,NULL)),2)) AS prog_hab_S_C1
        FROM reportes_resumen rs INNER JOIN acad_matricula ma ON ma.idalumno = rs.idalumno INNER JOIN acad_grupoauladetalle gad ON ma.idgrupoauladetalle = gad.idgrupoauladetalle";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "gad.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "gad.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "gad.idsesion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "nq.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }
            if(isset($filtros['idrecurso'])){
                $cond[] = "nq.idrecurso = ".$this->oBD->escapar($filtros["idrecurso"]);
            }
            if(isset($filtros['idproyecto'])){
                $cond[] = "nq.idproyecto = ".$this->oBD->escapar($filtros["idproyecto"]);
            }

            if(!empty($cond)){
                $sql .= " WHERE ".implode(' AND ',$cond);
            }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function buscarhabilidad($filtros = null){
        try{
            $tabla = "resumen_seccion_habilidad";
            if(isset($filtros['SQL_DRE'])){
                $tabla = 'resumen_dre_habilidad';
            }
            if(isset($filtros['SQL_UGEL'])){
                $tabla = 'resumen_ugel_habilidad';
            }
            if(isset($filtros['SQL_LOCAL'])){
                $tabla = "resumen_local_habilidad";
            }
            if(isset($filtros['SQL_GRADO'])){
                $tabla = "resumen_grado_habilidad";
            }
            if(isset($filtros['SQL_SECCION'])){
                $tabla = 'resumen_seccion_habilidad';
            }
            $sql = "SELECT * FROM ".$tabla;
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idugel'])){
                $cond[] = "idugel = ".$this->oBD->escapar($filtros["idugel"]);
            }
            if(isset($filtros['iddre'])){
                $cond[] = "iddre = ".$this->oBD->escapar($filtros["iddre"]);
            }
            if(isset($filtros['id_ubigeo'])){
                $cond[] = "id_ubigeo = ".$this->oBD->escapar($filtros["id_ubigeo"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "idseccion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }else{
                $cond[] = "tipo = ".$this->oBD->escapar("A");
            }

            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
            }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function filtrar($filtros = null){
        try{
            $tabla = "resumen_dre";
            $tabla2 = "resumen_dre_habilidad";
            if(isset($filtros['SQL_DRE'])){
                $tabla = "resumen_dre";
                $tabla2 = "resumen_dre_habilidad";
            }
            if(isset($filtros['SQL_UGEL'])){
                $tabla = "resumen_ugel";
                $tabla2 = "resumen_ugel_habilidad";
            }
            if(isset($filtros['SQL_LOCAL'])){
                $tabla = "resumen_local";
                $tabla2 = "resumen_local_habilidad";
            }
            if(isset($filtros['SQL_GRADO'])){
                $tabla = "resumen_grado";
                $tabla2 = "resumen_grado_habilidad";
            }
            if(isset($filtros['SQL_SECCION'])){
                $tabla = "resumen_seccion";
                $tabla2 = "resumen_seccion_habilidad";
            }
            $campos = "*";
            if(isset($filtros['filtro']) && !empty($filtros['filtro'])){
                $campos = implode(',',$filtros['filtro']);
            }
            $sql = "SELECT ".$campos." FROM ".$tabla." rs INNER JOIN ".$tabla2." rsh ON rsh.iddre = rs.iddre";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "rs.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idugel'])){
                $cond[] = "rs.idugel = ".$this->oBD->escapar($filtros["idugel"]);
            }
            if(isset($filtros['iddre'])){
                $cond[] = "rs.iddre = ".$this->oBD->escapar($filtros["iddre"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "rs.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "rs.idseccion = ".$this->oBD->escapar($filtros["idsesion"]);
            }

            if(isset($filtros['id_ubigeo'])){
                $cond[] = "id_ubigeo = ".$this->oBD->escapar($filtros["id_ubigeo"]);
            }

            if(isset($filtros['SQL_UGEL'])){
                $cond[] = "rs.idugel = rsh.idugel";
            }
            if(isset($filtros['SQL_LOCAL'])){
                $cond[] = "rs.idlocal = rsh.idlocal";
            }
            if(isset($filtros['SQL_GRADO'])){
                $cond[] = "rs.idlocal = rsh.idlocal AND rs.idgrado = rsh.idgrado";
            }
            if(isset($filtros['tipo'])){
                $cond[] = "rs.tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }else{
                $cond[] = "rs.tipo = ".$this->oBD->escapar("D");
            }

            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
            }
            $sql .= " GROUP BY rs.id";
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function buscar($filtros = null){
        try{
            $tabla = "resumen_dre";
            if(isset($filtros['SQL_DRE'])){
                $tabla = "  resumen_dre";
            }
            if(isset($filtros['SQL_UGEL'])){
                $tabla = "resumen_ugel";
            }
            if(isset($filtros['SQL_LOCAL'])){
                $tabla = "resumen_local";
            }
            if(isset($filtros['SQL_GRADO'])){
                $tabla = "resumen_grado";
            }
            if(isset($filtros['SQL_SECCION'])){
                $tabla = "resumen_seccion";
            }
            $sql = "SELECT * FROM ".$tabla;
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idugel'])){
                $cond[] = "idugel = ".$this->oBD->escapar($filtros["idugel"]);
            }
            if(isset($filtros['iddre'])){
                $cond[] = "iddre = ".$this->oBD->escapar($filtros["iddre"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idsesion'])){
                $cond[] = "idseccion = ".$this->oBD->escapar($filtros["idsesion"]);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "tipo = ".$this->oBD->escapar($filtros["tipo"]);
            }else{
                $cond[] = "tipo = ".$this->oBD->escapar("A");
            }

            if(isset($filtros['id_ubigeo'])){
                $cond[] = "id_ubigeo = ".$this->oBD->escapar($filtros["id_ubigeo"]);
            }
            

            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
            }
            
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    public function getresumen($filtros = null){
        try{
            $tabla = "resumen_seccion";
            if(isset($filtros['SQL_SECCION'])){
                $tabla = "resumen_seccion";
            }
            if(isset($filtros['SQL_GRADO'])){
                $tabla = "resumen_grado";
            }
            if(isset($filtros['SQL_LOCAL'])){
                $tabla = "resumen_local";
            }
            if(isset($filtros['SQL_UGEL'])){
                $tabla = "resumen_ugel";
            }
            if(isset($filtros['SQL_DRE'])){
                $tabla = "resumen_dre";
            }
            $sql = "SELECT  
            (SELECT SUM(rs.alumno_ubicacion)) AS alumno_ubicacion,
            (SELECT SUM(rs.ubicacion_A1)) AS ubicacion_A1,
            (SELECT SUM(rs.ubicacion_A2)) AS ubicacion_A2,
            (SELECT SUM(rs.ubicacion_B1)) AS ubicacion_B1,
            (SELECT SUM(rs.ubicacion_B2)) AS ubicacion_B2,
            (SELECT SUM(rs.ubicacion_C1)) AS ubicacion_C1,
            (SELECT SUM(rs.entrada_prom) / COUNT(IF(rs.entrada_prom>0,1,NULL)) ) AS entrada_prom,
            (SELECT SUM(rs.alumno_entrada) ) AS alumno_entrada,
            (SELECT SUM(rs.salida_prom) / COUNT(IF(rs.salida_prom>0,1,NULL)) ) AS salida_prom,
            (SELECT SUM(rs.alumno_salida) ) AS alumno_salida,
            (SELECT SUM(rs.examen_b1_prom) / COUNT(IF(rs.examen_b1_prom>0,1,NULL)) ) AS examen_b1_prom,
            (SELECT SUM(rs.alumno_examen_b1) ) AS alumno_examen_b1,
            (SELECT SUM(rs.examen_b2_prom) / COUNT(IF(rs.examen_b2_prom>0,1,NULL)) ) AS examen_b2_prom,
            (SELECT SUM(rs.alumno_examen_b2) ) AS alumno_examen_b2,
            (SELECT SUM(rs.examen_b3_prom) / COUNT(IF(rs.examen_b3_prom>0,1,NULL)) ) AS examen_b3_prom,
            (SELECT SUM(rs.alumno_examen_b3) ) AS alumno_examen_b3,
            (SELECT SUM(rs.examen_b4_prom) / COUNT(IF(rs.examen_b4_prom>0,1,NULL)) ) AS examen_b4_prom,
            (SELECT SUM(rs.alumno_examen_b4) ) AS alumno_examen_b4,
            (SELECT SUM(rs.examen_t1_prom) / COUNT(IF(rs.examen_t1_prom>0,1,NULL)) ) AS examen_t1_prom,
            (SELECT SUM(rs.alumno_examen_t1) ) AS alumno_examen_t1,
            (SELECT SUM(rs.examen_t2_prom) / COUNT(IF(rs.examen_t2_prom>0,1,NULL)) ) AS examen_t2_prom,
            (SELECT SUM(rs.alumno_examen_t2) ) AS alumno_examen_t2,
            (SELECT SUM(rs.examen_t3_prom) / COUNT(IF(rs.examen_t3_prom>0,1,NULL)) ) AS examen_t3_prom,
            (SELECT SUM(rs.alumno_examen_t3) ) AS alumno_examen_t3,
            (SELECT SUM(rs.tiempopv) / COUNT(IF(rs.tiempopv>0,1,NULL)) ) AS tiempopv,
            (SELECT SUM(rs.tiempo_exam) / COUNT(IF(rs.tiempo_exam>0,1,NULL)) ) AS tiempo_exam,
            (SELECT SUM(rs.tiempo_task) / COUNT(IF(rs.tiempo_task>0,1,NULL)) ) AS tiempo_task,
            (SELECT SUM(rs.tiempo_smartbook) / COUNT(IF(rs.tiempo_smartbook>0,1,NULL)) ) AS tiempo_smartbook,
            (SELECT SUM(rs.tiempo_practice) / COUNT(IF(rs.tiempo_practice>0,1,NULL)) ) AS tiempo_practice,
            (SELECT SUM(rs.prog_A1) / COUNT(IF(rs.prog_A1>0,1,NULL)) ) AS prog_A1,
            (SELECT SUM(rs.prog_A2) / COUNT(IF(rs.prog_A2>0,1,NULL)) ) AS prog_A2,
            (SELECT SUM(rs.prog_B1) / COUNT(IF(rs.prog_B1>0,1,NULL)) ) AS prog_B1,
            (SELECT SUM(rs.prog_B2) / COUNT(IF(rs.prog_B2>0,1,NULL)) ) AS prog_B2,
            (SELECT SUM(rs.prog_C1) / COUNT(IF(rs.prog_C1>0,1,NULL)) ) AS prog_C1
            
            FROM ".$tabla." rs ";
            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "rs.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "rs.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idugel'])){
                $cond[]= "rs.idugel = ".$this->oBD->escapar($filtros['idugel']);
            }
            if(isset($filtros['iddre'])){
                $cond[]= "rs.iddre = ".$this->oBD->escapar($filtros['iddre']);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "rs.tipo = ".$this->oBD->escapar($filtros['tipo']);
            }else{
                $cond[] = "rs.tipo = ".$this->oBD->escapar("A");                
            }

            if(!empty($cond)){
                $sql .= " WHERE ".implode(' AND ',$cond);
            }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }

    public function getresumenhab($filtros = null){
        try{
            $tabla = "resumen_seccion_habilidad";
            if(isset($filtros['SQL_SECCION'])){
                $tabla = "resumen_seccion_habilidad";
            }
            if(isset($filtros['SQL_GRADO'])){
                $tabla = "resumen_grado_habilidad";
            }
            if(isset($filtros['SQL_LOCAL'])){
                $tabla = "resumen_local_habilidad";
            }
            if(isset($filtros['SQL_UGEL'])){
                $tabla = "resumen_ugel_habilidad";
            }
            if(isset($filtros['SQL_DRE'])){
                $tabla = "resumen_dre_habilidad";
            }

            $sql = "SELECT
            (SELECT SUM(rs.ubicacion_hab_L) / COUNT(IF(rs.ubicacion_hab_L>0,1,NULL)) ) AS ubicacion_hab_L,
            (SELECT SUM(rs.ubicacion_hab_R) / COUNT(IF(rs.ubicacion_hab_R>0,1,NULL)) ) AS ubicacion_hab_R,
            (SELECT SUM(rs.ubicacion_hab_W) / COUNT(IF(rs.ubicacion_hab_W>0,1,NULL)) ) AS ubicacion_hab_W,
            (SELECT SUM(rs.ubicacion_hab_S) / COUNT(IF(rs.ubicacion_hab_S>0,1,NULL)) ) AS ubicacion_hab_S,
            (SELECT SUM(rs.entrada_hab_L) / COUNT(IF(rs.entrada_hab_L>0,1,NULL)) ) AS entrada_hab_L,
            (SELECT SUM(rs.entrada_hab_R) / COUNT(IF(rs.entrada_hab_R>0,1,NULL)) ) AS entrada_hab_R,
            (SELECT SUM(rs.entrada_hab_W) / COUNT(IF(rs.entrada_hab_W>0,1,NULL)) ) AS entrada_hab_W,
            (SELECT SUM(rs.entrada_hab_S) / COUNT(IF(rs.entrada_hab_S>0,1,NULL)) ) AS entrada_hab_S,
            (SELECT SUM(rs.salida_hab_L) / COUNT(IF(rs.salida_hab_L>0,1,NULL)) ) AS salida_hab_L,
            (SELECT SUM(rs.salida_hab_R) / COUNT(IF(rs.salida_hab_R>0,1,NULL)) ) AS salida_hab_R,
            (SELECT SUM(rs.salida_hab_W) / COUNT(IF(rs.salida_hab_W>0,1,NULL)) ) AS salida_hab_W,
            (SELECT SUM(rs.salida_hab_S) / COUNT(IF(rs.salida_hab_S>0,1,NULL)) ) AS salida_hab_S,
            (SELECT SUM(rs.examen_b1_hab_L) / COUNT(IF(rs.examen_b1_hab_L>0,1,NULL)) ) AS examen_b1_hab_L,
            (SELECT SUM(rs.examen_b1_hab_R) / COUNT(IF(rs.examen_b1_hab_R>0,1,NULL)) ) AS examen_b1_hab_R,
            (SELECT SUM(rs.examen_b1_hab_W) / COUNT(IF(rs.examen_b1_hab_W>0,1,NULL)) ) AS examen_b1_hab_W,
            (SELECT SUM(rs.examen_b1_hab_S) / COUNT(IF(rs.examen_b1_hab_S>0,1,NULL)) ) AS examen_b1_hab_S,
            (SELECT SUM(rs.examen_b2_hab_L) / COUNT(IF(rs.examen_b2_hab_L>0,1,NULL)) ) AS examen_b2_hab_L,
            (SELECT SUM(rs.examen_b2_hab_R) / COUNT(IF(rs.examen_b2_hab_R>0,1,NULL)) ) AS examen_b2_hab_R,
            (SELECT SUM(rs.examen_b2_hab_W) / COUNT(IF(rs.examen_b2_hab_W>0,1,NULL)) ) AS examen_b2_hab_W,
            (SELECT SUM(rs.examen_b2_hab_S) / COUNT(IF(rs.examen_b2_hab_S>0,1,NULL)) ) AS examen_b2_hab_S,
            (SELECT SUM(rs.examen_b3_hab_L) / COUNT(IF(rs.examen_b3_hab_L>0,1,NULL)) ) AS examen_b3_hab_L,
            (SELECT SUM(rs.examen_b3_hab_R) / COUNT(IF(rs.examen_b3_hab_R>0,1,NULL)) ) AS examen_b3_hab_R,
            (SELECT SUM(rs.examen_b3_hab_W) / COUNT(IF(rs.examen_b3_hab_W>0,1,NULL)) ) AS examen_b3_hab_W,
            (SELECT SUM(rs.examen_b3_hab_S) / COUNT(IF(rs.examen_b3_hab_S>0,1,NULL)) ) AS examen_b3_hab_S,
            (SELECT SUM(rs.examen_b4_hab_L) / COUNT(IF(rs.examen_b4_hab_L>0,1,NULL)) ) AS examen_b4_hab_L,
            (SELECT SUM(rs.examen_b4_hab_R) / COUNT(IF(rs.examen_b4_hab_R>0,1,NULL)) ) AS examen_b4_hab_R,
            (SELECT SUM(rs.examen_b4_hab_W) / COUNT(IF(rs.examen_b4_hab_W>0,1,NULL)) ) AS examen_b4_hab_W,
            (SELECT SUM(rs.examen_b4_hab_S) / COUNT(IF(rs.examen_b4_hab_S>0,1,NULL)) ) AS examen_b4_hab_S,
            (SELECT SUM(rs.examen_t1_hab_L) / COUNT(IF(rs.examen_t1_hab_L>0,1,NULL)) ) AS examen_t1_hab_L,
            (SELECT SUM(rs.examen_t1_hab_R) / COUNT(IF(rs.examen_t1_hab_R>0,1,NULL)) ) AS examen_t1_hab_R,
            (SELECT SUM(rs.examen_t1_hab_W) / COUNT(IF(rs.examen_t1_hab_W>0,1,NULL)) ) AS examen_t1_hab_W,
            (SELECT SUM(rs.examen_t1_hab_S) / COUNT(IF(rs.examen_t1_hab_S>0,1,NULL)) ) AS examen_t1_hab_S,
            (SELECT SUM(rs.examen_t2_hab_L) / COUNT(IF(rs.examen_t2_hab_L>0,1,NULL)) ) AS examen_t2_hab_L,
            (SELECT SUM(rs.examen_t2_hab_R) / COUNT(IF(rs.examen_t2_hab_R>0,1,NULL)) ) AS examen_t2_hab_R,
            (SELECT SUM(rs.examen_t2_hab_W) / COUNT(IF(rs.examen_t2_hab_W>0,1,NULL)) ) AS examen_t2_hab_W,
            (SELECT SUM(rs.examen_t2_hab_S) / COUNT(IF(rs.examen_t2_hab_S>0,1,NULL)) ) AS examen_t2_hab_S,
            (SELECT SUM(rs.examen_t3_hab_L) / COUNT(IF(rs.examen_t3_hab_L>0,1,NULL)) ) AS examen_t3_hab_L,
            (SELECT SUM(rs.examen_t3_hab_R) / COUNT(IF(rs.examen_t3_hab_R>0,1,NULL)) ) AS examen_t3_hab_R,
            (SELECT SUM(rs.examen_t3_hab_W) / COUNT(IF(rs.examen_t3_hab_W>0,1,NULL)) ) AS examen_t3_hab_W,
            (SELECT SUM(rs.examen_t3_hab_S) / COUNT(IF(rs.examen_t3_hab_S>0,1,NULL)) ) AS examen_t3_hab_S,
            (SELECT SUM(rs.prog_hab_A1_L) / COUNT(IF(rs.prog_hab_A1_L>0,1,NULL)) ) AS prog_hab_A1_L,
            (SELECT SUM(rs.prog_hab_A1_R) / COUNT(IF(rs.prog_hab_A1_R>0,1,NULL)) ) AS prog_hab_A1_R,
            (SELECT SUM(rs.prog_hab_A1_W) / COUNT(IF(rs.prog_hab_A1_W>0,1,NULL)) ) AS prog_hab_A1_W,
            (SELECT SUM(rs.prog_hab_A1_S) / COUNT(IF(rs.prog_hab_A1_S>0,1,NULL)) ) AS prog_hab_A1_S,
            (SELECT SUM(rs.prog_hab_A2_L) / COUNT(IF(rs.prog_hab_A2_L>0,1,NULL)) ) AS prog_hab_A2_L,
            (SELECT SUM(rs.prog_hab_A2_R) / COUNT(IF(rs.prog_hab_A2_R>0,1,NULL)) ) AS prog_hab_A2_R,
            (SELECT SUM(rs.prog_hab_A2_W) / COUNT(IF(rs.prog_hab_A2_W>0,1,NULL)) ) AS prog_hab_A2_W,
            (SELECT SUM(rs.prog_hab_A2_S) / COUNT(IF(rs.prog_hab_A2_S>0,1,NULL)) ) AS prog_hab_A2_S,
            (SELECT SUM(rs.prog_hab_B1_L) / COUNT(IF(rs.prog_hab_B1_L>0,1,NULL)) ) AS prog_hab_B1_L,
            (SELECT SUM(rs.prog_hab_B1_R) / COUNT(IF(rs.prog_hab_B1_R>0,1,NULL)) ) AS prog_hab_B1_R,
            (SELECT SUM(rs.prog_hab_B1_W) / COUNT(IF(rs.prog_hab_B1_W>0,1,NULL)) ) AS prog_hab_B1_W,
            (SELECT SUM(rs.prog_hab_B1_S) / COUNT(IF(rs.prog_hab_B1_S>0,1,NULL)) ) AS prog_hab_B1_S,
            (SELECT SUM(rs.prog_hab_B2_L) / COUNT(IF(rs.prog_hab_B2_L>0,1,NULL)) ) AS prog_hab_B2_L,
            (SELECT SUM(rs.prog_hab_B2_R) / COUNT(IF(rs.prog_hab_B2_R>0,1,NULL)) ) AS prog_hab_B2_R,
            (SELECT SUM(rs.prog_hab_B2_W) / COUNT(IF(rs.prog_hab_B2_W>0,1,NULL)) ) AS prog_hab_B2_W,
            (SELECT SUM(rs.prog_hab_B2_S) / COUNT(IF(rs.prog_hab_B2_S>0,1,NULL)) ) AS prog_hab_B2_S,
            (SELECT SUM(rs.prog_hab_C1_L) / COUNT(IF(rs.prog_hab_C1_L>0,1,NULL)) ) AS prog_hab_C1_L,
            (SELECT SUM(rs.prog_hab_C1_R) / COUNT(IF(rs.prog_hab_C1_R>0,1,NULL)) ) AS prog_hab_C1_R,
            (SELECT SUM(rs.prog_hab_C1_W) / COUNT(IF(rs.prog_hab_C1_W>0,1,NULL)) ) AS prog_hab_C1_W,
            (SELECT SUM(rs.prog_hab_C1_S) / COUNT(IF(rs.prog_hab_C1_S>0,1,NULL)) ) AS prog_hab_C1_S
            
            FROM ".$tabla." rs ";

            $cond = array();

            if(isset($filtros['idlocal'])){
                $cond[] = "rs.idlocal = ".$this->oBD->escapar($filtros["idlocal"]);
            }
            if(isset($filtros['idgrado'])){
                $cond[] = "rs.idgrado = ".$this->oBD->escapar($filtros["idgrado"]);
            }
            if(isset($filtros['idugel'])){
                $cond[]= "rs.idugel = ".$this->oBD->escapar($filtros['idugel']);
            }
            if(isset($filtros['iddre'])){
                $cond[]= "rs.iddre = ".$this->oBD->escapar($filtros['iddre']);
            }
            if(isset($filtros['tipo'])){
                $cond[] = "rs.tipo = ".$this->oBD->escapar($filtros['tipo']);
            }else{
                $cond[] = "rs.tipo = ".$this->oBD->escapar("A");                
            }

            if(!empty($cond)){
                $sql .= " WHERE ".implode(' AND ',$cond);
            }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resumen").": " . $e->getMessage());
        }
    }
    
    /**INSERT RESUMEN */
    
    public function insertar($tabla,$estados)
	{
		try {
			$this->iniciarTransaccion('dat_'.$tabla.'_insert');
			
            $this->oBD->insert($tabla, $estados);
            			
            $this->terminarTransaccion('dat_'.$tabla.'_insert');			
            return true;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_'.$tabla.'_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_($tabla).": " . $e->getMessage());
		}
    }
    public function insertar_seccion($estados)
	{
		try {
			$this->iniciarTransaccion('dat_resumen_seccion_insert');
			
            $this->oBD->insert('resumen_seccion', $estados);
            			
            $this->terminarTransaccion('dat_resumen_seccion_insert');			
            return true;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_notas_quiz_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("resumen_seccion").": " . $e->getMessage());
		}
    }
    public function insertar_seccion_hab($estados)
	{
		try {
			$this->iniciarTransaccion('dat_resumen_seccion_insert');
			
            $this->oBD->insert('resumen_seccion_habilidad', $estados);
            			
            $this->terminarTransaccion('dat_resumen_seccion_insert');			
            return true;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_notas_quiz_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("resumen_seccion").": " . $e->getMessage());
		}
    }
    
    /**UPDATE RESUMEN */
    
    public function actualizar($tabla,$id,$estados)
	{
		try {
			$this->iniciarTransaccion('dat_'.$tabla.'_update');
			
			$this->oBD->update($tabla, $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_'.$tabla.'_update');
		    return $id;
		}catch(Exception $e){
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_($tabla).": " . $e->getMessage());
		}
    }
    public function actualizar_seccion($id,$estados)
	{
		try {
			$this->iniciarTransaccion('dat_resumen_seccion_update');
			
			$this->oBD->update('resumen_seccion ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_resumen_seccion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
    }
    
    public function actualizar_seccion_hab($id,$estados)
	{
		try {
			$this->iniciarTransaccion('dat_resumen_seccion_update');
			
			$this->oBD->update('resumen_seccion_habilidad ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_resumen_seccion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
    }
    
}

?>