<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatRub_rubrica extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rub_rubrica").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT (@i := @i + 1) as orden, 
						rubrica_criterio.* 
					FROM rubrica_criterio
						cross join (select @i := 0) rubrica_criterio";				
			
			$cond = array();		
					
			
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(isset($filtros["tipo_encuestado"])) {
					$cond[] = "tipo_encuestado = " . $this->oBD->escapar($filtros["tipo_encuestado"]);
			}
			if(isset($filtros["opcion_publico"])) {
					$cond[] = "opcion_publico = " . $this->oBD->escapar($filtros["opcion_publico"]);
			}
			if(isset($filtros["autor"])) {
					$cond[] = "autor = " . $this->oBD->escapar($filtros["autor"]);
			}
			if(isset($filtros["tipo_letra"])) {
					$cond[] = "tipo_letra = " . $this->oBD->escapar($filtros["tipo_letra"]);
			}
			if(isset($filtros["tamanio_letra"])) {
					$cond[] = "tamanio_letra = " . $this->oBD->escapar($filtros["tamanio_letra"]);
			}
			if(isset($filtros["fecha_creacion"])) {
					$cond[] = "fecha_creacion = " . $this->oBD->escapar($filtros["fecha_creacion"]);
			}
			if(isset($filtros["tipo_rubrica"])) {
					$cond[] = "tipo_rubrica = " . $this->oBD->escapar($filtros["tipo_rubrica"]);
			}
			if(isset($filtros["puntuacion_general"])) {
					$cond[] = "puntuacion_general = " . $this->oBD->escapar($filtros["puntuacion_general"]);
			}
			if(isset($filtros["tipo_pregunta"])) {
					$cond[] = "tipo_pregunta = " . $this->oBD->escapar($filtros["tipo_pregunta"]);
			}
			if(isset($filtros["id_usuario"])) {
					$cond[] = "id_usuario = " . $this->oBD->escapar($filtros["id_usuario"]);
			}
			if(isset($filtros["activo"])) {
					$cond[] = "activo = " . $this->oBD->escapar($filtros["activo"]);
			}
			if(isset($filtros["registros"])) {
					$cond[] = "registros = " . $this->oBD->escapar($filtros["registros"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rub_rubrica").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($titulo,$descripcion,$foto,$tipo_encuestado,$opcion_publico,$autor,$tipo_letra,$tamanio_letra,$fecha_creacion,$tipo_rubrica,$puntuacion_general,$tipo_pregunta,$id_usuario,$activo,$registros)
	{
		try {
			
			$this->iniciarTransaccion('dat_rub_rubrica_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_rubrica) FROM rub_rubrica");
			++$id;
			
			$estados = array('id_rubrica' => $id
							
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'foto'=>$foto
							,'tipo_encuestado'=>$tipo_encuestado
							,'opcion_publico'=>$opcion_publico
							,'autor'=>$autor
							,'tipo_letra'=>$tipo_letra
							,'tamanio_letra'=>$tamanio_letra
							,'fecha_creacion'=>$fecha_creacion
							,'tipo_rubrica'=>$tipo_rubrica
							,'puntuacion_general'=>$puntuacion_general
							,'tipo_pregunta'=>$tipo_pregunta
							,'id_usuario'=>$id_usuario
							,'activo'=>$activo
							,'registros'=>$registros							
							);
			
			$this->oBD->insert('rub_rubrica', $estados);			
			$this->terminarTransaccion('dat_rub_rubrica_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rub_rubrica_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rub_rubrica").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $titulo,$descripcion,$foto,$tipo_encuestado,$opcion_publico,$autor,$tipo_letra,$tamanio_letra,$fecha_creacion,$tipo_rubrica,$puntuacion_general,$tipo_pregunta,$id_usuario,$activo,$registros)
	{
		try {
			$this->iniciarTransaccion('dat_rub_rubrica_update');
			$estados = array('titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'foto'=>$foto
							,'tipo_encuestado'=>$tipo_encuestado
							,'opcion_publico'=>$opcion_publico
							,'autor'=>$autor
							,'tipo_letra'=>$tipo_letra
							,'tamanio_letra'=>$tamanio_letra
							,'fecha_creacion'=>$fecha_creacion
							,'tipo_rubrica'=>$tipo_rubrica
							,'puntuacion_general'=>$puntuacion_general
							,'tipo_pregunta'=>$tipo_pregunta
							,'id_usuario'=>$id_usuario
							,'activo'=>$activo
							,'registros'=>$registros								
							);
			
			$this->oBD->update('rub_rubrica ', $estados, array('id_rubrica' => $id));
		    $this->terminarTransaccion('dat_rub_rubrica_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_rubrica").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM rub_rubrica  "
					. " WHERE id_rubrica = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rub_rubrica").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rub_rubrica', array('id_rubrica' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rub_rubrica").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rub_rubrica', array($propiedad => $valor), array('id_rubrica' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_rubrica").": " . $e->getMessage());
		}
	}
   
		
}