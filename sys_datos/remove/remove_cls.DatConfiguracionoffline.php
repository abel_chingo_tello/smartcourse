<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatConfiguracionoffline extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Configuracionoffline").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM configuracionoffline";			
			
			$cond = array();		
					
			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Configuracionoffline").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$valor)
	{
		try {
			
			$this->iniciarTransaccion('dat_configuracionoffline_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idconfoffline) FROM configuracionoffline");
			++$id;
			
			$estados = array('idconfoffline' => $id
							
							,'nombre'=>$nombre
							,'valor'=>$valor							
							);
			
			$this->oBD->insert('configuracionoffline', $estados);			
			$this->terminarTransaccion('dat_configuracionoffline_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_configuracionoffline_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Configuracionoffline").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$valor)
	{
		try {
			$this->iniciarTransaccion('dat_configuracionoffline_update');
			$estados = array('nombre'=>$nombre
							,'valor'=>$valor								
							);
			
			$this->oBD->update('configuracionoffline ', $estados, array('idconfoffline' => $id));
		    $this->terminarTransaccion('dat_configuracionoffline_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Configuracionoffline").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM configuracionoffline  "
					. " WHERE idconfoffline = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Configuracionoffline").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('configuracionoffline', array('idconfoffline' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Configuracionoffline").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('configuracionoffline', array($propiedad => $valor), array('idconfoffline' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Configuracionoffline").": " . $e->getMessage());
		}
	}
   
		
}