<?php

class DatAcademico extends DatBase{
    public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Academico").": " . $e->getMessage());
		}
    }
    
    public function preguntas($id){
        try{
            $resultado = array();
            $option = array( PDO::ATTR_CASE => PDO::CASE_NATURAL, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING );
            $conn = new PDO("mysql:host=".$this->getHost().";dbname=smartquiz",$this->getUser(),$this->getClave(),$option);
            if($conn){
                $sql = "SELECT e.idexamen ,e.titulo AS examen, ep.pregunta AS pregunta, ep.habilidades FROM examenes e INNER JOIN examenes_preguntas ep ON e.idexamen = ep.idexamen where ep.idpadre = 0 AND e.idexamen in (".implode(',',$id).") ORDER BY e.idexamen ASC";
                $r = $conn->query($sql, PDO::FETCH_ASSOC);
                foreach($r as $row) {
                    $resultado[] = $row;
                }
                return $resultado;
            }else{
                throw new Exception("Error: No connected to database");
            }
            $conn = null;
			return true;
        }catch(Exception $e){
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Academico").": " . $e->getMessage());
        }
    }
    public function totalHab($id){
        
        $resultado = array();
        $option = array( PDO::ATTR_CASE => PDO::CASE_NATURAL, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING );
        $conn = new PDO("mysql:host=".$this->getHost().";dbname=smartquiz",$this->getUser(),$this->getClave(),$option);
        if($conn){
            $sql = "SELECT e.idexamen, e.titulo AS examen,
        (SELECT COUNT(*) FROM examenes_preguntas ep WHERE ep.idexamen = e.idexamen AND ep.idpadre = 0 AND ep.habilidades like '%4%') AS L,
         (SELECT COUNT(*) FROM examenes_preguntas ep WHERE ep.idexamen = e.idexamen AND ep.idpadre = 0 AND ep.habilidades like '%5%') AS R,
         (SELECT COUNT(*) FROM examenes_preguntas ep WHERE ep.idexamen = e.idexamen AND ep.idpadre = 0 AND ep.habilidades like '%6%') AS W,
         (SELECT COUNT(*) FROM examenes_preguntas ep WHERE ep.idexamen = e.idexamen AND ep.idpadre = 0 AND ep.habilidades like '%7%') AS S
        FROM examenes e WHERE e.idexamen = {$id}";
            $r = $conn->query($sql, PDO::FETCH_ASSOC);
            foreach($r as $row) {
                $resultado[] = $row;
            }
            $conn = null;
            return $resultado;
        }else{
            throw new Exception("Error: No connected to database");
        }
        return true;
    }
}

?>