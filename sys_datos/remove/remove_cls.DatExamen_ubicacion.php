<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		01-03-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatExamen_ubicacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Examen_ubicacion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM examen_ubicacion";
			
			$cond = array();		
			
			if(isset($filtros["idexamen_ubicacion"])) {
					$cond[] = "idexamen_ubicacion = " . $this->oBD->escapar($filtros["idexamen_ubicacion"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["activo"])) {
					$cond[] = "activo = " . $this->oBD->escapar($filtros["activo"]);
			}
			if(isset($filtros["rango_min"])) {
					$cond[] = "rango_min = " . $this->oBD->escapar($filtros["rango_min"]);
			}
			if(isset($filtros["idexam_prerequisito"])) {
					$cond[] = "idexam_prerequisito = " . $this->oBD->escapar($filtros["idexam_prerequisito"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Examen_ubicacion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM examen_ubicacion";			
			
			$cond = array();		
					
			
			if(isset($filtros["idexamen_ubicacion"])) {
					$cond[] = "idexamen_ubicacion = " . $this->oBD->escapar($filtros["idexamen_ubicacion"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["activo"])) {
					$cond[] = "activo = " . $this->oBD->escapar($filtros["activo"]);
			}
			if(isset($filtros["rango_min"])) {
					$cond[] = "rango_min = " . $this->oBD->escapar($filtros["rango_min"]);
			}
			if(isset($filtros["rango_min_menorA"])) {
					$cond[] = "rango_min < " . $this->oBD->escapar($filtros["rango_min_menorA"]);
			}
			if(isset($filtros["rango_max"])) {
					$cond[] = "rango_max = " . $this->oBD->escapar($filtros["rango_max"]);
			}
			if(isset($filtros["rango_max_mayorA"])) {
					$cond[] = "rango_max >= " . $this->oBD->escapar($filtros["rango_max_mayorA"]);
			}
			if(isset($filtros["idexam_prerequisito"])) {
					$cond[] = "idexam_prerequisito = " . $this->oBD->escapar($filtros["idexam_prerequisito"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Examen_ubicacion").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idrecurso,$idcurso,$tipo,$activo,$rango_min,$idexam_prerequisito)
	{
		try {
			
			$this->iniciarTransaccion('dat_examen_ubicacion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idexamen_ubicacion) FROM examen_ubicacion");
			++$id;
			
			$estados = array('idexamen_ubicacion' => $id
							
							,'idrecurso'=>$idrecurso
							,'idcurso'=>$idcurso
							,'tipo'=>$tipo
							,'activo'=>$activo
							,'rango_min'=>$rango_min
							,'idexam_prerequisito'=>$idexam_prerequisito							
							);
			
			$this->oBD->insert('examen_ubicacion', $estados);			
			$this->terminarTransaccion('dat_examen_ubicacion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_examen_ubicacion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Examen_ubicacion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idrecurso,$idcurso,$tipo,$activo,$rango_min,$idexam_prerequisito)
	{
		try {
			$this->iniciarTransaccion('dat_examen_ubicacion_update');
			$estados = array('idrecurso'=>$idrecurso
							,'idcurso'=>$idcurso
							,'tipo'=>$tipo
							,'activo'=>$activo
							,'rango_min'=>$rango_min
							,'idexam_prerequisito'=>$idexam_prerequisito								
							);
			
			$this->oBD->update('examen_ubicacion ', $estados, array('idexamen_ubicacion' => $id));
		    $this->terminarTransaccion('dat_examen_ubicacion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examen_ubicacion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM examen_ubicacion  "
					. " WHERE idexamen_ubicacion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Examen_ubicacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('examen_ubicacion', array('idexamen_ubicacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Examen_ubicacion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('examen_ubicacion', array($propiedad => $valor), array('idexamen_ubicacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examen_ubicacion").": " . $e->getMessage());
		}
	}
   
		
}