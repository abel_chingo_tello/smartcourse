<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatAcad_cursohabilidadtipo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursohabilidadtipo").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_cursohabilidadtipo";			
			
			$cond = array();		
					
			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursohabilidadtipo").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_cursohabilidadtipo_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtipo) FROM acad_cursohabilidadtipo");
			++$id;
			
			$estados = array('idtipo' => $id
							
							,'nombre'=>$nombre							
							);
			
			$this->oBD->insert('acad_cursohabilidadtipo', $estados);			
			$this->terminarTransaccion('dat_acad_cursohabilidadtipo_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursohabilidadtipo_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_cursohabilidadtipo").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre)
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursohabilidadtipo_update');
			$estados = array('nombre'=>$nombre								
							);
			
			$this->oBD->update('acad_cursohabilidadtipo ', $estados, array('idtipo' => $id));
		    $this->terminarTransaccion('dat_acad_cursohabilidadtipo_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursohabilidadtipo").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_cursohabilidadtipo  "
					. " WHERE idtipo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_cursohabilidadtipo").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_cursohabilidadtipo', array('idtipo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_cursohabilidadtipo").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_cursohabilidadtipo', array($propiedad => $valor), array('idtipo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursohabilidadtipo").": " . $e->getMessage());
		}
	}
   
		
}