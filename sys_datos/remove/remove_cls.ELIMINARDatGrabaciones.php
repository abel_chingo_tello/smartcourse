<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatGrabaciones extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Grabaciones").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM grabaciones";			
			
			$cond = array();		
					
						
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Grabaciones").": " . $e->getMessage());
		}
	}
	
	
	public function insertar()
	{
		try {
			
			$this->iniciarTransaccion('dat_grabaciones_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM grabaciones");
			++$id;
			
			$estados = array('id' => $id
														
							);
			
			$this->oBD->insert('grabaciones', $estados);			
			$this->terminarTransaccion('dat_grabaciones_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_grabaciones_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Grabaciones").": " . $e->getMessage());
		}
	}
	public function actualizar($id, )
	{
		try {
			$this->iniciarTransaccion('dat_grabaciones_update');
			$estados = array(								
							);
			
			$this->oBD->update('grabaciones ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_grabaciones_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Grabaciones").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM grabaciones  "
					. " WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Grabaciones").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('grabaciones', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Grabaciones").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('grabaciones', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Grabaciones").": " . $e->getMessage());
		}
	}
   
		
}