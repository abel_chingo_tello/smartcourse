<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatNota_detalle extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Nota_detalle").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM nota_detalle";
			
			$cond = array();		
			
			if(isset($filtros["idnota_detalle"])) {
					$cond[] = "idnota_detalle = " . $this->oBD->escapar($filtros["idnota_detalle"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idmatricula"])) {
					$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["nota"])) {
					$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["observacion"])) {
					$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Nota_detalle").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {

			$sql = "SELECT n.*, c.idrecurso,a.titulo FROM nota_detalle n left join acad_cursodetalle c on n.idcursodetalle=c.idcursodetalle left join actividades a on a.sesion=c.idrecurso";			
			
			$cond = array();		
				
			if(isset($filtros["idnota_detalle"])) {
					$cond[] = "idnota_detalle = " . $this->oBD->escapar($filtros["idnota_detalle"]);
			}
			if(isset($filtros["n.idcurso"])) {
					$cond[] = "n.idcurso = " . $this->oBD->escapar($filtros["n.idcurso"]);
			}
			if(isset($filtros["idmatricula"])) {
					$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["n.idcursodetalle"])) {
					$cond[] = "n.idcursodetalle = " . $this->oBD->escapar($filtros["n.idcursodetalle"]);
			}
			if(isset($filtros["nota"])) {
					$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["observacion"])) {
					$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "n.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["metodologia"])) {
					$cond[] = "metodologia =" . $this->oBD->escapar($filtros["metodologia"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Nota_detalle").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$idmatricula,$idalumno,$iddocente,$idcursodetalle,$nota,$tipo,$observacion,$fecharegistro,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_nota_detalle_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idnota_detalle) FROM nota_detalle");
			++$id;
			
			$estados = array('idnota_detalle' => $id
							
							,'idcurso'=>$idcurso
							,'idmatricula'=>$idmatricula
							,'idalumno'=>$idalumno
							,'iddocente'=>$iddocente
							,'idcursodetalle'=>$idcursodetalle
							,'nota'=>$nota
							,'tipo'=>$tipo
							,'observacion'=>$observacion
							,'fecharegistro'=>$fecharegistro
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('nota_detalle', $estados);			
			$this->terminarTransaccion('dat_nota_detalle_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_nota_detalle_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Nota_detalle").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$idmatricula,$idalumno,$iddocente,$idcursodetalle,$nota,$tipo,$observacion,$fecharegistro,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_nota_detalle_update');
			$estados = array('idcurso'=>$idcurso
							,'idmatricula'=>$idmatricula
							,'idalumno'=>$idalumno
							,'iddocente'=>$iddocente
							,'idcursodetalle'=>$idcursodetalle
							,'nota'=>$nota
							,'tipo'=>$tipo
							,'observacion'=>$observacion
							,'fecharegistro'=>$fecharegistro
							,'estado'=>$estado								
							);
			
			$this->oBD->update('nota_detalle ', $estados, array('idnota_detalle' => $id));
		    $this->terminarTransaccion('dat_nota_detalle_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Nota_detalle").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM nota_detalle  "
					. " WHERE idnota_detalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Nota_detalle").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('nota_detalle', array('idnota_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Nota_detalle").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('nota_detalle', array($propiedad => $valor), array('idnota_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Nota_detalle").": " . $e->getMessage());
		}
	}
   
		
}