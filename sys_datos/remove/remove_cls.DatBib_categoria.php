<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_categoria extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_categoria";
			
			$cond = array();		
			
			if(!empty($filtros["id_categoria"])) {
					$cond[] = "id_categoria = " . $this->oBD->escapar($filtros["id_categoria"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_categoria";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_categoria"])) {
					$cond[] = "id_categoria = " . $this->oBD->escapar($filtros["id_categoria"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_categoria order by descripcion ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}
	
	public function insertar($descripcion)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_categoria_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_categoria) FROM bib_categoria");
			++$id;
			
			$estados = array('id_categoria' => $id
							
							,'descripcion'=>$descripcion							
							);
			
			$this->oBD->insert('bib_categoria', $estados);			
			$this->terminarTransaccion('dat_bib_categoria_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_categoria_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $descripcion)
	{
		try {
			$this->iniciarTransaccion('dat_bib_categoria_update');
			$estados = array('descripcion'=>$descripcion								
							);
			
			$this->oBD->update('bib_categoria ', $estados, array('id_categoria' => $id));
		    $this->terminarTransaccion('dat_bib_categoria_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_categoria  "
					. " WHERE id_categoria = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_categoria', array('id_categoria' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_categoria', array($propiedad => $valor), array('id_categoria' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_categoria").": " . $e->getMessage());
		}
	}
   
		
}