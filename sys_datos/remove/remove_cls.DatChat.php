<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		14-11-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatChat extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM chat";
			
			$cond = array();		
			/*if(!empty($idioma)) {
				$cond[] = "idioma " . $this->oBD->like($idioma);
			}*/		
			if(!empty($filtros["mostrar"])) {
				$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM chat";			
			
			$cond = array();		
					
			if(!empty($filtros["mostrar"])) {
				$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM chat  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}
	
	public function insertar($usuario)
	{
		try {
			
			$this->iniciarTransaccion('dat_chat_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idchat) FROM chat");
			++$id;
			
			$estados = array('idchat' => $id
							
							,'usuario'=>$usuario							
							);
			
			$this->oBD->insert('chat', $estados);			
			$this->terminarTransaccion('dat_chat_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_chat_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $usuario)
	{
		try {
			$this->iniciarTransaccion('dat_chat_update');
			$estados = array('usuario'=>$usuario								
							);
			
			$this->oBD->update('chat ', $estados, array('idchat' => $id));
		    $this->terminarTransaccion('dat_chat_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM chat  "
					. " WHERE idchat = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('chat', array('idchat' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('chat', array($propiedad => $valor), array('idchat' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Chat").": " . $e->getMessage());
		}
	}
   
		
}