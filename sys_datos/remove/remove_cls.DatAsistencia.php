<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-12-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAsistencia extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Asistencia").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM asistencia";
			
			$cond = array();		
			
			if(isset($filtros["idasistencia"])) {
					$cond[] = "idasistencia = " . $this->oBD->escapar($filtros["idasistencia"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["observacion"])) {
					$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}
			if(isset($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Asistencia").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM asistencia";			
			/*$sql = "SELECT g.fecha_inicio as fechaInicioGrupo,g.fecha_final as fechaFinalGrupo,h.fecha_inicio as fechaInicio,h.fecha_final as fechaFinal,a.fecha,a.idalumno,a.estado FROM `asistencia` a left join `acad_grupoauladetalle` g on g.`idgrupoauladetalle`=a.`idgrupoauladetalle` left join acad_horariogrupodetalle h on h.idgrupoauladetalle=g.idgrupoauladetalle";	*/		
			
			$cond = array();		
					
			
			if(isset($filtros["idasistencia"])) {
					$cond[] = "idasistencia = " . $this->oBD->escapar($filtros["idasistencia"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["observacion"])) {
					$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}
			if(isset($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Asistencia").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idgrupoauladetalle,$idalumno,$fecha,$estado,$observacion,$regusuario,$regfecha)
	{
		try {
			
			$this->iniciarTransaccion('dat_asistencia_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idasistencia) FROM asistencia");
			++$id;
			
			$estados = array('idasistencia' => $id
							
							,'idgrupoauladetalle'=>$idgrupoauladetalle
							,'idalumno'=>$idalumno
							,'fecha'=>$fecha
							,'estado'=>$estado
							,'observacion'=>$observacion
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha							
							);
			
			$this->oBD->insert('asistencia', $estados);			
			$this->terminarTransaccion('dat_asistencia_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_asistencia_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Asistencia").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupoauladetalle,$idalumno,$fecha,$estado,$observacion,$regusuario,$regfecha)
	{
		try {
			$this->iniciarTransaccion('dat_asistencia_update');
			$estados = array('idgrupoauladetalle'=>$idgrupoauladetalle
							,'idalumno'=>$idalumno
							,'fecha'=>$fecha
							,'estado'=>$estado
							,'observacion'=>$observacion
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha								
							);
			
			$this->oBD->update('asistencia ', $estados, array('idasistencia' => $id));
		    $this->terminarTransaccion('dat_asistencia_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Asistencia").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM asistencia  "
					. " WHERE idasistencia = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Asistencia").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('asistencia', array('idasistencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Asistencia").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('asistencia', array($propiedad => $valor), array('idasistencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Asistencia").": " . $e->getMessage());
		}
	}
   
		
}