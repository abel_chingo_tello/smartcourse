<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatRub_tipo_letra extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rub_tipo_letra").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM rub_tipo_letra";			
			
			$cond = array();		
					
			
			if(isset($filtros["id_tipoletra"])) {
					$cond[] = "id_tipoletra = " . $this->oBD->escapar($filtros["id_tipoletra"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["activo"])) {
					$cond[] = "activo = " . $this->oBD->escapar($filtros["activo"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rub_tipo_letra").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($id_tipoletra,$descripcion,$activo)
	{
		try {
			
			$this->iniciarTransaccion('dat_rub_tipo_letra_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(config) FROM rub_tipo_letra");
			++$id;
			
			$estados = array('config' => $id
							
							,'id_tipoletra'=>$id_tipoletra
							,'descripcion'=>$descripcion
							,'activo'=>$activo							
							);
			
			$this->oBD->insert('rub_tipo_letra', $estados);			
			$this->terminarTransaccion('dat_rub_tipo_letra_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rub_tipo_letra_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rub_tipo_letra").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_tipoletra,$descripcion,$activo)
	{
		try {
			$this->iniciarTransaccion('dat_rub_tipo_letra_update');
			$estados = array('id_tipoletra'=>$id_tipoletra
							,'descripcion'=>$descripcion
							,'activo'=>$activo								
							);
			
			$this->oBD->update('rub_tipo_letra ', $estados, array('config' => $id));
		    $this->terminarTransaccion('dat_rub_tipo_letra_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_tipo_letra").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM rub_tipo_letra  "
					. " WHERE config = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rub_tipo_letra").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rub_tipo_letra', array('config' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rub_tipo_letra").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rub_tipo_letra', array($propiedad => $valor), array('config' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_tipo_letra").": " . $e->getMessage());
		}
	}
   
		
}