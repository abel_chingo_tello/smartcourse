<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAporte_Respuesta extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}
	public function listarPreguntas($idcurso)
	{
		try {
			$sql = "SELECT * FROM aporte_respuesta WHERE idcurso = '" . $idcurso . "' order by codigo desc;";
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM acad_curso";
			
			$cond = array();		
			
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}
	public function buscarXproyecto($filtros=null)
	{
		try {
			$sql = "SELECT * from acad_curso ac INNER JOIN proyecto_cursos pc on pc.idcurso=ac.idcurso";	
			$cond = array();
			if(isset($filtros["idcurso"])) {
					$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "pc.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["noidproyecto"])) {
				$cond[] = "pc.idproyecto <> " . $this->oBD->escapar($filtros["noidproyecto"]);
			}
					
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			if(!empty($filtros["orderby"])){
				if($filtros["orderby"]!='no'){
					$sql.=" ORDER BY ".$filtros["orderby"]." ASC";
				}
			}
			//$sql .= " ORDER BY nombre ASC";
			//echo $sql;
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	



	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT *, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
					(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
					(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
					(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes from acad_curso c LEFT JOIN proyecto_cursos pc ON c.idcurso=pc.idcurso";
			
			$cond = array();
			if(isset($filtros["sql2"])) {
				$sql = "SELECT *, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
				(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
				(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
				(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes from acad_curso c";
			}
			
			if(isset($filtros["idcurso"])) {
					$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["idproyecto"])) {	$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);	}
					
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			if(!empty($filtros["orderby"])){
				if($filtros["orderby"]!='no'){
					$sql.=" ORDER BY ".$filtros["orderby"]." ASC";
				}
			}else{
				$sql.=" ORDER BY nombre ASC";
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			//exit($sql);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function getCursos_Not_In_GrupoAulaDetalle($filtros=null, $iddocente)
	{
		try {
			$sql = "SELECT C.*, PC.idproyecto FROM acad_curso C JOIN proyecto_cursos PC ON C.idcurso=PC.idcurso";			
			
			$cond = array();	
			
			if(isset($filtros["idcurso"])) {
				$cond[] = "C.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "C.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["imagen"])) {
				$cond[] = "C.imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["descripcion"])) {
				$cond[] = "C.descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "C.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["fecharegistro"])) {
				$cond[] = "C.fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["idusuario"])) {
				$cond[] = "C.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}	
			if(isset($filtros["idproyecto"])) {
				$cond[] = "PC.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			

			$cond[] = "C.idcurso NOT IN (
				SELECT idcurso FROM acad_grupoauladetalle WHERE iddocente=". $this->oBD->escapar($iddocente)."
			)";
			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			if(!empty($filtros["orderby"])){
				if($filtros["orderby"]!='no'){
					$sql.=" ORDER BY ".$filtros["orderby"]." ASC";
				}

			}
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo '<p><b>' . $sql . '</b></p>';
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$imagen,$descripcion,$estado,$fecharegistro,$idusuario,$vinculosaprendizajes,$materialesyrecursos,$color)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_curso_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcurso) FROM acad_curso");
			++$id;
			
			$estados = array('idcurso' => $id
							
							,'nombre'=>$nombre
							,'imagen'=>$imagen
							,'descripcion'=>$descripcion
							,'estado'=>$estado
							,'fecharegistro'=>!empty($fecharegistro)?$fecharegistro:date('Y-m-d H:i:s')
							,'idusuario'=>$idusuario
							,'vinculosaprendizajes'=>!empty($vinculosaprendizajes)?$vinculosaprendizajes:''
							,'materialesyrecursos'=>!empty($materialesyrecursos)?$materialesyrecursos:''
							,'color'=>!empty($color)?$color:''
							);
			
			$this->oBD->insert('acad_curso', $estados);			
			$this->terminarTransaccion('dat_acad_curso_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_curso_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function actualizar($id, $nombre,$imagen,$descripcion,$estado,$fecharegistro,$idusuario,$vinculosaprendizajes,$materialesyrecursos,$color)
	{
		try {
			$this->iniciarTransaccion('dat_acad_curso_update');
			$estados = array('nombre'=>$nombre
							,'imagen'=>$imagen
							,'descripcion'=>$descripcion
							,'estado'=>$estado
							,'fecharegistro'=>$fecharegistro
							,'idusuario'=>$idusuario
							,'vinculosaprendizajes'=>$vinculosaprendizajes
							,'materialesyrecursos'=>$materialesyrecursos
							,'color'=>$color								
							);
			
			$this->oBD->update('acad_curso ', $estados, array('idcurso' => $id));
		    $this->terminarTransaccion('dat_acad_curso_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT  *, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
					(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
					(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
					(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes  FROM acad_curso  c "
					. " WHERE c.idcurso = " . $this->oBD->escapar($id);
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			$idremove=$this->oBD->delete('acad_curso', array('idcurso' => $id));
			$this->oBD->delete('acad_cursodetalle', array('idcurso' => $id));
			return $idremove;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_curso', array($propiedad => $valor), array('idcurso' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}   		
}