<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_subcategoria extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_subcategoria";
			
			$cond = array();		
			
			if(!empty($filtros["id_subcategoria"])) {
					$cond[] = "id_subcategoria = " . $this->oBD->escapar($filtros["id_subcategoria"]);
			}
			if(!empty($filtros["id_categoria"])) {
					$cond[] = "id_categoria = " . $this->oBD->escapar($filtros["id_categoria"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_subcategoria";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_subcategoria"])) {
					$cond[] = "id_subcategoria = " . $this->oBD->escapar($filtros["id_subcategoria"]);
			}
			if(!empty($filtros["id_categoria"])) {
					$cond[] = "id_categoria = " . $this->oBD->escapar($filtros["id_categoria"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_subcategoria  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}
	
	public function insertar($id_categoria,$descripcion)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_subcategoria_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_subcategoria) FROM bib_subcategoria");
			++$id;
			
			$estados = array('id_subcategoria' => $id
							
							,'id_categoria'=>$id_categoria
							,'descripcion'=>$descripcion							
							);
			
			$this->oBD->insert('bib_subcategoria', $estados);			
			$this->terminarTransaccion('dat_bib_subcategoria_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_subcategoria_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_categoria,$descripcion)
	{
		try {
			$this->iniciarTransaccion('dat_bib_subcategoria_update');
			$estados = array('id_categoria'=>$id_categoria
							,'descripcion'=>$descripcion								
							);
			
			$this->oBD->update('bib_subcategoria ', $estados, array('id_subcategoria' => $id));
		    $this->terminarTransaccion('dat_bib_subcategoria_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_subcategoria  "
					. " WHERE id_subcategoria = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_subcategoria', array('id_subcategoria' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_subcategoria', array($propiedad => $valor), array('id_subcategoria' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_subcategoria").": " . $e->getMessage());
		}
	}
   
		
}