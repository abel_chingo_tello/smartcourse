<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatMaterial_ayuda extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Material_ayuda").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM material_ayuda";
			
			$cond = array();		
			
			if(isset($filtros["idmaterial"])) {
					$cond[] = "idmaterial = " . $this->oBD->escapar($filtros["idmaterial"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Material_ayuda").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM material_ayuda";			
			
			$cond = array();		
					
			
			if(isset($filtros["idmaterial"])) {
					$cond[] = "idmaterial = " . $this->oBD->escapar($filtros["idmaterial"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Material_ayuda").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$archivo,$tipo,$descripcion,$mostrar)
	{
		try {
			
			$this->iniciarTransaccion('dat_material_ayuda_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmaterial) FROM material_ayuda");
			++$id;
			
			$estados = array('idmaterial' => $id
							
							,'nombre'=>$nombre
							,'archivo'=>$archivo
							,'tipo'=>$tipo
							,'descripcion'=>$descripcion
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('material_ayuda', $estados);			
			$this->terminarTransaccion('dat_material_ayuda_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_material_ayuda_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Material_ayuda").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$archivo,$tipo,$descripcion,$mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_material_ayuda_update');
			$estados = array('nombre'=>$nombre
							,'archivo'=>$archivo
							,'tipo'=>$tipo
							,'descripcion'=>$descripcion
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('material_ayuda ', $estados, array('idmaterial' => $id));
		    $this->terminarTransaccion('dat_material_ayuda_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Material_ayuda").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM material_ayuda  "
					. " WHERE idmaterial = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Material_ayuda").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('material_ayuda', array('idmaterial' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Material_ayuda").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('material_ayuda', array($propiedad => $valor), array('idmaterial' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Material_ayuda").": " . $e->getMessage());
		}
	}
   
		
}