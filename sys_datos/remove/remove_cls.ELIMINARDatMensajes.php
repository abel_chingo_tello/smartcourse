<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatMensajes extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Mensajes").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM mensajes";			
			
			$cond = array();		
					
						
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Mensajes").": " . $e->getMessage());
		}
	}
	
	
	public function insertar()
	{
		try {
			
			$this->iniciarTransaccion('dat_mensajes_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmensaje) FROM mensajes");
			++$id;
			
			$estados = array('idmensaje' => $id
														
							);
			
			$this->oBD->insert('mensajes', $estados);			
			$this->terminarTransaccion('dat_mensajes_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_mensajes_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Mensajes").": " . $e->getMessage());
		}
	}
	public function actualizar($id, )
	{
		try {
			$this->iniciarTransaccion('dat_mensajes_update');
			$estados = array(								
							);
			
			$this->oBD->update('mensajes ', $estados, array('idmensaje' => $id));
		    $this->terminarTransaccion('dat_mensajes_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Mensajes").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM mensajes  "
					. " WHERE idmensaje = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Mensajes").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('mensajes', array('idmensaje' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Mensajes").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('mensajes', array($propiedad => $valor), array('idmensaje' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Mensajes").": " . $e->getMessage());
		}
	}
   
		
}