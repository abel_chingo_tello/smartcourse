<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_det_estudio_autor extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_det_estudio_autor";
			
			$cond = array();		
			
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($filtros["id_autor"])) {
					$cond[] = "id_autor = " . $this->oBD->escapar($filtros["id_autor"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_det_estudio_autor";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($filtros["id_autor"])) {
					$cond[] = "id_autor = " . $this->oBD->escapar($filtros["id_autor"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_det_estudio_autor  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}
	
	public function insertar($id_estudio,$id_autor)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_det_estudio_autor_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_detalle) FROM bib_det_estudio_autor");
			++$id;
			
			$estados = array('id_detalle' => $id
							
							,'id_estudio'=>$id_estudio
							,'id_autor'=>$id_autor							
							);
			
			$this->oBD->insert('bib_det_estudio_autor', $estados);			
			$this->terminarTransaccion('dat_bib_det_estudio_autor_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_det_estudio_autor_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_estudio,$id_autor)
	{
		try {
			$this->iniciarTransaccion('dat_bib_det_estudio_autor_update');
			$estados = array('id_estudio'=>$id_estudio
							,'id_autor'=>$id_autor								
							);
			
			$this->oBD->update('bib_det_estudio_autor ', $estados, array('id_detalle' => $id));
		    $this->terminarTransaccion('dat_bib_det_estudio_autor_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_det_estudio_autor  "
					. " WHERE id_detalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_det_estudio_autor', array('id_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_det_estudio_autor', array($propiedad => $valor), array('id_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_det_estudio_autor").": " . $e->getMessage());
		}
	}
   
		
}