<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		01-03-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatExamen_ubicacion_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Examen_ubicacion_alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM examen_ubicacion_alumno";
			
			$cond = array();		
			
			if(isset($filtros["idexam_alumno"])) {
					$cond[] = "idexam_alumno = " . $this->oBD->escapar($filtros["idexam_alumno"]);
			}
			if(isset($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["resultado"])) {
					$cond[] = "resultado = " . $this->oBD->escapar($filtros["resultado"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Examen_ubicacion_alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM examen_ubicacion_alumno";			
			
			$cond = array();		
					
			
			if(isset($filtros["idexam_alumno"])) {
					$cond[] = "idexam_alumno = " . $this->oBD->escapar($filtros["idexam_alumno"]);
			}
			if(isset($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["resultado"])) {
					$cond[] = "resultado = " . $this->oBD->escapar($filtros["resultado"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Examen_ubicacion_alumno").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idexamen,$idalumno,$estado,$resultado,$fecha)
	{
		try {
			
			$this->iniciarTransaccion('dat_examen_ubicacion_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idexam_alumno) FROM examen_ubicacion_alumno");
			++$id;
			
			$estados = array('idexam_alumno' => $id
							
							,'idexamen'=>$idexamen
							,'idalumno'=>$idalumno
							,'estado'=>$estado
							,'fecha'=>$fecha							
							);

			if(!empty($resultado)) {
				$estados['resultado'] = $resultado;
			}
			
			$this->oBD->insert('examen_ubicacion_alumno', $estados);			
			$this->terminarTransaccion('dat_examen_ubicacion_alumno_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_examen_ubicacion_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Examen_ubicacion_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idexamen,$idalumno,$estado,$resultado,$fecha)
	{
		try {
			$this->iniciarTransaccion('dat_examen_ubicacion_alumno_update');
			$estados = array('idexamen'=>$idexamen
							,'idalumno'=>$idalumno
							,'estado'=>$estado
							,'fecha'=>$fecha								
							);
			
			if(!empty($resultado)) {
				$estados['resultado'] = $resultado;
			}
			
			$this->oBD->update('examen_ubicacion_alumno ', $estados, array('idexam_alumno' => $id));
		    $this->terminarTransaccion('dat_examen_ubicacion_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examen_ubicacion_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM examen_ubicacion_alumno  "
					. " WHERE idexam_alumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Examen_ubicacion_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('examen_ubicacion_alumno', array('idexam_alumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Examen_ubicacion_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('examen_ubicacion_alumno', array($propiedad => $valor), array('idexam_alumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examen_ubicacion_alumno").": " . $e->getMessage());
		}
	}
   
		
}