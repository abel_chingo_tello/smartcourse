<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatManuales extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Manuales").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM manuales";
			
			$cond = array();		
			
			if(isset($filtros["idmanual"])) {
					$cond[] = "idmanual = " . $this->oBD->escapar($filtros["idmanual"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["abreviado"])) {
					$cond[] = "abreviado = " . $this->oBD->escapar($filtros["abreviado"]);
			}
			if(isset($filtros["stock"])) {
					$cond[] = "stock = " . $this->oBD->escapar($filtros["stock"]);
			}
			if(isset($filtros["total"])) {
					$cond[] = "total = " . $this->oBD->escapar($filtros["total"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Manuales").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM manuales";			
			
			$cond = array();		
					
			
			if(isset($filtros["idmanual"])) {
					$cond[] = "idmanual = " . $this->oBD->escapar($filtros["idmanual"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["abreviado"])) {
					$cond[] = "abreviado = " . $this->oBD->escapar($filtros["abreviado"]);
			}
			if(isset($filtros["stock"])) {
					$cond[] = "stock = " . $this->oBD->escapar($filtros["stock"]);
			}
			if(isset($filtros["total"])) {
					$cond[] = "total = " . $this->oBD->escapar($filtros["total"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Manuales").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($titulo,$abreviado,$stock,$total)
	{
		try {
			
			$this->iniciarTransaccion('dat_manuales_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmanual) FROM manuales");
			++$id;
			
			$estados = array('idmanual' => $id
							
							,'titulo'=>$titulo
							,'abreviado'=>$abreviado
							,'stock'=>$stock
							,'total'=>$total							
							);
			
			$this->oBD->insert('manuales', $estados);			
			$this->terminarTransaccion('dat_manuales_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_manuales_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Manuales").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $titulo,$abreviado,$stock,$total)
	{
		try {
			$this->iniciarTransaccion('dat_manuales_update');
			$estados = array('titulo'=>$titulo
							,'abreviado'=>$abreviado
							,'stock'=>$stock
							,'total'=>$total								
							);
			
			$this->oBD->update('manuales ', $estados, array('idmanual' => $id));
		    $this->terminarTransaccion('dat_manuales_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Manuales").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM manuales  "
					. " WHERE idmanual = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Manuales").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('manuales', array('idmanual' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Manuales").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('manuales', array($propiedad => $valor), array('idmanual' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Manuales").": " . $e->getMessage());
		}
	}
   
		
}