<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatRub_detalle_respuestas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rub_detalle_respuestas").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM rub_detalle_respuestas";			
			
			$cond = array();		
					
			
			if(isset($filtros["id_respuestas"])) {
					$cond[] = "id_respuestas = " . $this->oBD->escapar($filtros["id_respuestas"]);
			}
			if(isset($filtros["id_pregunta"])) {
					$cond[] = "id_pregunta = " . $this->oBD->escapar($filtros["id_pregunta"]);
			}
			if(isset($filtros["respuesta"])) {
					$cond[] = "respuesta = " . $this->oBD->escapar($filtros["respuesta"]);
			}
			if(isset($filtros["puntaje_obtenido"])) {
					$cond[] = "puntaje_obtenido = " . $this->oBD->escapar($filtros["puntaje_obtenido"]);
			}
			if(isset($filtros["id_dimension"])) {
					$cond[] = "id_dimension = " . $this->oBD->escapar($filtros["id_dimension"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rub_detalle_respuestas").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($id_respuestas,$id_pregunta,$respuesta,$puntaje_obtenido,$id_dimension)
	{
		try {
			
			$this->iniciarTransaccion('dat_rub_detalle_respuestas_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_det_rptas) FROM rub_detalle_respuestas");
			++$id;
			
			$estados = array('id_det_rptas' => $id
							
							,'id_respuestas'=>$id_respuestas
							,'id_pregunta'=>$id_pregunta
							,'respuesta'=>$respuesta
							,'puntaje_obtenido'=>$puntaje_obtenido
							,'id_dimension'=>$id_dimension							
							);
			
			$this->oBD->insert('rub_detalle_respuestas', $estados);			
			$this->terminarTransaccion('dat_rub_detalle_respuestas_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rub_detalle_respuestas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rub_detalle_respuestas").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_respuestas,$id_pregunta,$respuesta,$puntaje_obtenido,$id_dimension)
	{
		try {
			$this->iniciarTransaccion('dat_rub_detalle_respuestas_update');
			$estados = array('id_respuestas'=>$id_respuestas
							,'id_pregunta'=>$id_pregunta
							,'respuesta'=>$respuesta
							,'puntaje_obtenido'=>$puntaje_obtenido
							,'id_dimension'=>$id_dimension								
							);
			
			$this->oBD->update('rub_detalle_respuestas ', $estados, array('id_det_rptas' => $id));
		    $this->terminarTransaccion('dat_rub_detalle_respuestas_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_detalle_respuestas").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM rub_detalle_respuestas  "
					. " WHERE id_det_rptas = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rub_detalle_respuestas").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rub_detalle_respuestas', array('id_det_rptas' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rub_detalle_respuestas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rub_detalle_respuestas', array($propiedad => $valor), array('id_det_rptas' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_detalle_respuestas").": " . $e->getMessage());
		}
	}
   
		
}