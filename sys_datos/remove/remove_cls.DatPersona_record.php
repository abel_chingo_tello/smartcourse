<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		01-02-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatPersona_record extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Persona_record") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(idrecord) FROM persona_record";

			$cond = array();

			if (isset($filtros["idrecord"])) {
				$cond[] = "idrecord = " . $this->oBD->escapar($filtros["idrecord"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if (isset($filtros["tiporecord"])) {
				$cond[] = "tiporecord = " . $this->oBD->escapar($filtros["tiporecord"]);
			}

			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}




			if (isset($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (isset($filtros["mostrar"])) {
				$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Persona_record") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT pe.*, (SELECT nombre from general g WHERE pe.tiporecord=g.codigo AND tipo_tabla='tiporecord') as strtiporecord FROM persona_record pe LEFT JOIN personal pl on pe.idpersona=pl.idpersona";

			$cond = array();


			if (isset($filtros["idrecord"])) {
				$cond[] = "idrecord = " . $this->oBD->escapar($filtros["idrecord"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "(pe.idpersona = " . $this->oBD->escapar($filtros["idpersona"]) . " OR md5(pe.idpersona)=" . $this->oBD->escapar($filtros["idpersona"]) . ")";
			}
			if (isset($filtros["tiporecord"])) {
				$cond[] = "tiporecord = " . $this->oBD->escapar($filtros["tiporecord"]);
			}

			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}


			if (isset($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (isset($filtros["mostrar"])) {
				$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Persona_record") . ": " . $e->getMessage());
		}
	}


	public function insertar($idpersona, $tiporecord, $titulo, $descripcion, $archivo, $fecharegistro, $mostrar, $idusuario)
	{
		try {

			$this->iniciarTransaccion('dat_persona_record_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrecord) FROM persona_record");
			++$id;

			$estados = array(
				'idrecord' => $id, 'idpersona' => $idpersona, 'tiporecord' => $tiporecord, 'titulo' => $titulo, 'descripcion' => $descripcion, 'archivo' => $archivo, 'fecharegistro' => !empty($fecharegistro) ? $fecharegistro : date('Y-m-d H:i:s'), 'mostrar' => $mostrar, 'idusuario' => $idusuario
			);

			$this->oBD->insert('persona_record', $estados);
			$this->terminarTransaccion('dat_persona_record_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_persona_record_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Persona_record") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona, $tiporecord, $titulo, $descripcion, $archivo, $fecharegistro, $mostrar, $idusuario)
	{
		try {
			$this->iniciarTransaccion('dat_persona_record_update');
			$estados = array(
				'idpersona' => $idpersona, 'tiporecord' => $tiporecord, 'titulo' => $titulo, 'descripcion' => $descripcion, 'archivo' => $archivo, 'fecharegistro' => $fecharegistro, 'mostrar' => $mostrar, 'idusuario' => $idusuario
			);

			$this->oBD->update('persona_record ', $estados, array('idrecord' => $id));
			$this->terminarTransaccion('dat_persona_record_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Persona_record") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT pe.* ,(SELECT nombre from general g WHERE pe.tiporecord=g.codigo AND tipo_tabla='tiporecord') as strtiporecord FROM persona_record  pe LEFT JOIN personal pl on pe.idpersona=pl.idpersona WHERE idrecord = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Persona_record") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_record', array('idrecord' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Persona_record") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('persona_record', array($propiedad => $valor), array('idrecord' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Persona_record") . ": " . $e->getMessage());
		}
	}
}
