<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatRub_pregunta extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rub_pregunta").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM rub_pregunta";			
			
			$cond = array();		
					
			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["otros_datos"])) {
					$cond[] = "otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if(isset($filtros["id_estandar"])) {
					$cond[] = "id_estandar = " . $this->oBD->escapar($filtros["id_estandar"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["activo"])) {
					$cond[] = "activo = " . $this->oBD->escapar($filtros["activo"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rub_pregunta").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$otros_datos,$id_estandar,$tipo,$activo)
	{
		try {
			
			$this->iniciarTransaccion('dat_rub_pregunta_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_pregunta) FROM rub_pregunta");
			++$id;
			
			$estados = array('id_pregunta' => $id
							
							,'nombre'=>$nombre
							,'otros_datos'=>$otros_datos
							,'id_estandar'=>$id_estandar
							,'tipo'=>$tipo
							,'activo'=>$activo							
							);
			
			$this->oBD->insert('rub_pregunta', $estados);			
			$this->terminarTransaccion('dat_rub_pregunta_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rub_pregunta_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rub_pregunta").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$otros_datos,$id_estandar,$tipo,$activo)
	{
		try {
			$this->iniciarTransaccion('dat_rub_pregunta_update');
			$estados = array('nombre'=>$nombre
							,'otros_datos'=>$otros_datos
							,'id_estandar'=>$id_estandar
							,'tipo'=>$tipo
							,'activo'=>$activo								
							);
			
			$this->oBD->update('rub_pregunta ', $estados, array('id_pregunta' => $id));
		    $this->terminarTransaccion('dat_rub_pregunta_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_pregunta").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM rub_pregunta  "
					. " WHERE id_pregunta = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rub_pregunta").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rub_pregunta', array('id_pregunta' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rub_pregunta").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rub_pregunta', array($propiedad => $valor), array('id_pregunta' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_pregunta").": " . $e->getMessage());
		}
	}
   
		
}