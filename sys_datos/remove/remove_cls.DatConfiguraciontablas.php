<?php
/**
 * @autor		Sistec Peru
 */

class DatConfiguraciontablas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Setting").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM configuracion";
			
			$cond = array();		
			
			if(!empty($filtros["nivel"])) {
					$cond[] = "nivel = " . $this->oBD->escapar($filtros["nivel"]);
			}
			if(!empty($filtros["ventana"])) {
					$cond[] = "ventana " . $this->oBD->like($filtros["ventana"]);
			}
			if(!empty($filtros["atributo"])) {
					$cond[] = "atributo  " . $this->oBD->like($filtros["atributo"]);
			}					
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}		
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Setting").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM configuracion";			
			
			$cond = array();				
			
			if(!empty($filtros["nivel"])) {
					$cond[] = "nivel = " . $this->oBD->escapar($filtros["nivel"]);
			}
			if(!empty($filtros["ventana"])) {
					$cond[] = "ventana " . $this->oBD->like($filtros["ventana"]);
			}
			if(!empty($filtros["atributo"])) {
					$cond[] = "atributo  " . $this->oBD->like($filtros["atributo"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
}