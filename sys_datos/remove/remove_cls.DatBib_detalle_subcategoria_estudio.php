<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_detalle_subcategoria_estudio extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_detalle_subcategoria_estudio";
			
			$cond = array();		
			
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_subcategoria"])) {
					$cond[] = "id_subcategoria = " . $this->oBD->escapar($filtros["id_subcategoria"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_detalle_subcategoria_estudio";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_subcategoria"])) {
					$cond[] = "id_subcategoria = " . $this->oBD->escapar($filtros["id_subcategoria"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_detalle_subcategoria_estudio  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}
	
	public function insertar($id_subcategoria,$id_estudio,$id_categoria)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_detalle_subcategoria_estudio_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_detalle) FROM bib_detalle_subcategoria_estudio");
			++$id;
			
			$estados = array('id_detalle' => $id
							
							,'id_subcategoria'=>$id_subcategoria
							,'id_estudio'=>$id_estudio							
							,'id_categoria'=>$id_categoria							
							);
			
			$this->oBD->insert('bib_detalle_subcategoria_estudio', $estados);			
			$this->terminarTransaccion('dat_bib_detalle_subcategoria_estudio_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_detalle_subcategoria_estudio_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_subcategoria,$id_estudio)
	{
		try {
			$this->iniciarTransaccion('dat_bib_detalle_subcategoria_estudio_update');
			$estados = array('id_subcategoria'=>$id_subcategoria
							,'id_estudio'=>$id_estudio								
							,'id_categoria'=>$id_categoria								
							);
			
			$this->oBD->update('bib_detalle_subcategoria_estudio ', $estados, array('id_detalle' => $id));
		    $this->terminarTransaccion('dat_bib_detalle_subcategoria_estudio_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_detalle_subcategoria_estudio  "
					. " WHERE id_detalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_detalle_subcategoria_estudio', array('id_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_detalle_subcategoria_estudio', array($propiedad => $valor), array('id_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_detalle_subcategoria_estudio").": " . $e->getMessage());
		}
	}
   
		
}