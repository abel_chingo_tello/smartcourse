<?php

class DatVideosconferencia extends DatBase{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Videoconferencia").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM videoconferencia";			
			
			$cond = array();		
					
						
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Videoconferencia").": " . $e->getMessage());
		}
	}
	
	
	public function insertar()
	{
		try {
			
			$this->iniciarTransaccion('dat_videoconferencia_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmensaje) FROM mensajes");
			++$id;
			
			$estados = array('idmensaje' => $id
														
							);
			
			$this->oBD->insert('videoconferencia', $estados);			
			$this->terminarTransaccion('dat_videoconferencia_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_videoconferencia_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Videoconferencia").": " . $e->getMessage());
		}
	}
	public function actualizar($id, )
	{
		try {
			$this->iniciarTransaccion('dat_videoconferencia_update');
			$estados = array(								
							);
			
			$this->oBD->update('videoconferencia ', $estados, array('idmensaje' => $id));
		    $this->terminarTransaccion('dat_videoconferencia_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Videoconferencia").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM videoconferencia  "
					. " WHERE idmensaje = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Videoconferencia").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('videoconferencia', array('idmensaje' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Videoconferencia").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('videoconferencia', array($propiedad => $valor), array('idmensaje' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Videoconferencia").": " . $e->getMessage());
		}
	}
}