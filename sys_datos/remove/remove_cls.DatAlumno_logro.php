<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAlumno_logro extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Alumno_logro").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM alumno_logro";
			
			$cond = array();		
			
			if(isset($filtros["id_alumno_logro"])) {
					$cond[] = "id_alumno_logro = " . $this->oBD->escapar($filtros["id_alumno_logro"]);
			}
			if(isset($filtros["id_alumno"])) {
					$cond[] = "id_alumno = " . $this->oBD->escapar($filtros["id_alumno"]);
			}
			if(isset($filtros["id_logro"])) {
					$cond[] = "id_logro = " . $this->oBD->escapar($filtros["id_logro"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["tiporecurso"])) {
					$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(isset($filtros["bandera"])) {
					$cond[] = "bandera = " . $this->oBD->escapar($filtros["bandera"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Alumno_logro").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM alumno_logro AL JOIN logro L ON L.id_logro=AL.id_logro";			
			
			$cond = array();		
					
			
			if(isset($filtros["id_alumno_logro"])) {
				$cond[] = "AL.id_alumno_logro = " . $this->oBD->escapar($filtros["id_alumno_logro"]);
			}
			if(isset($filtros["id_alumno"])) {
				$cond[] = "AL.id_alumno = " . $this->oBD->escapar($filtros["id_alumno"]);
			}
			if(isset($filtros["id_logro"])) {
				$cond[] = "AL.id_logro = " . $this->oBD->escapar($filtros["id_logro"]);
			}
			if(isset($filtros["idrecurso"])) {
				$cond[] = "AL.idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["tiporecurso"])) {
				$cond[] = "AL.tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if(isset($filtros["fecha"])) {
				$cond[] = "AL.fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}if(isset($filtros["bandera"])) {
				$cond[] = "AL.bandera = " . $this->oBD->escapar($filtros["bandera"]);
			}
			$cond[] = "L.estado = " . $this->oBD->escapar(1);
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Alumno_logro").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($id_alumno,$id_logro,$idrecurso,$tiporecurso,$fecha,$bandera)
	{
		try {
			
			$this->iniciarTransaccion('dat_alumno_logro_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_alumno_logro) FROM alumno_logro");
			++$id;
			
			$estados = array('id_alumno_logro' => $id
							
							,'id_alumno'=>$id_alumno
							,'id_logro'=>$id_logro
							,'idrecurso'=>$idrecurso
							,'tiporecurso'=>$tiporecurso
							#,'fecha'=>$fecha							
							,'bandera'=>$bandera							
							);
			$this->oBD->insert('alumno_logro', $estados);			
			$this->terminarTransaccion('dat_alumno_logro_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_alumno_logro_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Alumno_logro").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_alumno,$id_logro,$idrecurso,$tiporecurso,$fecha,$bandera)
	{
		try {
			$this->iniciarTransaccion('dat_alumno_logro_update');
			$estados = array('id_alumno'=>$id_alumno
							,'id_logro'=>$id_logro
							,'idrecurso'=>$idrecurso
							,'tiporecurso'=>$tiporecurso
							#,'fecha'=>$fecha								
							,'bandera'=>$bandera								
							);
			
			$this->oBD->update('alumno_logro ', $estados, array('id_alumno_logro' => $id));
		    $this->terminarTransaccion('dat_alumno_logro_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Alumno_logro").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM alumno_logro  "
					. " WHERE id_alumno_logro = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Alumno_logro").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('alumno_logro', array('id_alumno_logro' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Alumno_logro").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('alumno_logro', array($propiedad => $valor), array('id_alumno_logro' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Alumno_logro").": " . $e->getMessage());
		}
	}
   
		
}