<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatRub_estandar extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rub_estandar").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM rub_estandar";			
			
			$cond = array();		
					
			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["id_rubrica"])) {
					$cond[] = "id_rubrica = " . $this->oBD->escapar($filtros["id_rubrica"]);
			}
			if(isset($filtros["puntuacion"])) {
					$cond[] = "puntuacion = " . $this->oBD->escapar($filtros["puntuacion"]);
			}
			if(isset($filtros["id_dimension"])) {
					$cond[] = "id_dimension = " . $this->oBD->escapar($filtros["id_dimension"]);
			}
			if(isset($filtros["activo"])) {
					$cond[] = "activo = " . $this->oBD->escapar($filtros["activo"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["duplicado_id_estandar"])) {
					$cond[] = "duplicado_id_estandar = " . $this->oBD->escapar($filtros["duplicado_id_estandar"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rub_estandar").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$id_rubrica,$puntuacion,$id_dimension,$activo,$tipo,$duplicado_id_estandar)
	{
		try {
			
			$this->iniciarTransaccion('dat_rub_estandar_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_estandar) FROM rub_estandar");
			++$id;
			
			$estados = array('id_estandar' => $id
							
							,'nombre'=>$nombre
							,'id_rubrica'=>$id_rubrica
							,'puntuacion'=>$puntuacion
							,'id_dimension'=>$id_dimension
							,'activo'=>$activo
							,'tipo'=>$tipo
							,'duplicado_id_estandar'=>$duplicado_id_estandar							
							);
			
			$this->oBD->insert('rub_estandar', $estados);			
			$this->terminarTransaccion('dat_rub_estandar_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rub_estandar_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rub_estandar").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$id_rubrica,$puntuacion,$id_dimension,$activo,$tipo,$duplicado_id_estandar)
	{
		try {
			$this->iniciarTransaccion('dat_rub_estandar_update');
			$estados = array('nombre'=>$nombre
							,'id_rubrica'=>$id_rubrica
							,'puntuacion'=>$puntuacion
							,'id_dimension'=>$id_dimension
							,'activo'=>$activo
							,'tipo'=>$tipo
							,'duplicado_id_estandar'=>$duplicado_id_estandar								
							);
			
			$this->oBD->update('rub_estandar ', $estados, array('id_estandar' => $id));
		    $this->terminarTransaccion('dat_rub_estandar_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_estandar").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM rub_estandar  "
					. " WHERE id_estandar = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rub_estandar").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rub_estandar', array('id_estandar' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rub_estandar").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rub_estandar', array($propiedad => $valor), array('id_estandar' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rub_estandar").": " . $e->getMessage());
		}
	}
   
		
}