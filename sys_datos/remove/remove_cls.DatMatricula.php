<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatMatricula extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Matricula").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM matricula";			
			
			$cond = array();		
					
			
			if(isset($filtros["idmatricula"])) {
					$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(isset($filtros["fechamatricula"])) {
					$cond[] = "fechamatricula = " . $this->oBD->escapar($filtros["fechamatricula"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Matricula").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idmatricula,$idalumno,$idactividad,$fechamatricula,$estado,$regusuario,$regfecha)
	{
		try {
			
			$this->iniciarTransaccion('dat_matricula_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatriculaalumno) FROM matricula");
			++$id;
			
			$estados = array('idmatriculaalumno' => $id
							
							,'idmatricula'=>$idmatricula
							,'idalumno'=>$idalumno
							,'idactividad'=>$idactividad
							,'fechamatricula'=>$fechamatricula
							,'estado'=>$estado
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha							
							);
			
			$this->oBD->insert('matricula', $estados);			
			$this->terminarTransaccion('dat_matricula_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_matricula_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Matricula").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idmatricula,$idalumno,$idactividad,$fechamatricula,$estado,$regusuario,$regfecha)
	{
		try {
			$this->iniciarTransaccion('dat_matricula_update');
			$estados = array('idmatricula'=>$idmatricula
							,'idalumno'=>$idalumno
							,'idactividad'=>$idactividad
							,'fechamatricula'=>$fechamatricula
							,'estado'=>$estado
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha								
							);
			
			$this->oBD->update('matricula ', $estados, array('idmatriculaalumno' => $id));
		    $this->terminarTransaccion('dat_matricula_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Matricula").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM matricula  "
					. " WHERE idmatriculaalumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Matricula").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('matricula', array('idmatriculaalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Matricula").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('matricula', array($propiedad => $valor), array('idmatriculaalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Matricula").": " . $e->getMessage());
		}
	}
   
		
}