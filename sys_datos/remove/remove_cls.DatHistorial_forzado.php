<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatHistorial_forzado extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Historial_forzado").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM historial_forzado";			
			
			$cond = array();		
					
			
			if(isset($filtros["idhistorialsin"])) {
					$cond[] = "idhistorialsin = " . $this->oBD->escapar($filtros["idhistorialsin"]);
			}
			if(isset($filtros["nombre_archivo"])) {
					$cond[] = "nombre_archivo = " . $this->oBD->escapar($filtros["nombre_archivo"]);
			}
			if(isset($filtros["fechaentrada"])) {
					$cond[] = "fechaentrada = " . $this->oBD->escapar($filtros["fechaentrada"]);
			}
			if(isset($filtros["estadoexp"])) {
					$cond[] = "estadoexp = " . $this->oBD->escapar($filtros["estadoexp"]);
			}
			if(isset($filtros["estadoimp"])) {
					$cond[] = "estadoimp = " . $this->oBD->escapar($filtros["estadoimp"]);
			}
			if(isset($filtros["tabla"])) {
					$cond[] = "tabla = " . $this->oBD->escapar($filtros["tabla"]);
			}
			if(isset($filtros["fechamodificacion"])) {
					$cond[] = "fechamodificacion = " . $this->oBD->escapar($filtros["fechamodificacion"]);
			}
			if(isset($filtros["completo"])) {
					$cond[] = "completo = " . $this->oBD->escapar($filtros["completo"]);
			}
			if(isset($filtros["contactualizador"])) {
					$cond[] = "contactualizador = " . $this->oBD->escapar($filtros["contactualizador"]);
			}
			if(isset($filtros["estadoimpac"])) {
					$cond[] = "estadoimpac = " . $this->oBD->escapar($filtros["estadoimpac"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Historial_forzado").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idhistorialsin,$nombre_archivo,$fechaentrada,$estadoexp,$estadoimp,$tabla,$fechamodificacion,$completo,$contactualizador,$estadoimpac)
	{
		try {
			
			$this->iniciarTransaccion('dat_historial_forzado_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idhisesion) FROM historial_forzado");
			++$id;
			
			$estados = array('idhisesion' => $id
							
							,'idhistorialsin'=>$idhistorialsin
							,'nombre_archivo'=>$nombre_archivo
							,'fechaentrada'=>$fechaentrada
							,'estadoexp'=>$estadoexp
							,'estadoimp'=>$estadoimp
							,'tabla'=>$tabla
							,'fechamodificacion'=>$fechamodificacion
							,'completo'=>$completo
							,'contactualizador'=>$contactualizador
							,'estadoimpac'=>$estadoimpac							
							);
			
			$this->oBD->insert('historial_forzado', $estados);			
			$this->terminarTransaccion('dat_historial_forzado_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_historial_forzado_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Historial_forzado").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idhistorialsin,$nombre_archivo,$fechaentrada,$estadoexp,$estadoimp,$tabla,$fechamodificacion,$completo,$contactualizador,$estadoimpac)
	{
		try {
			$this->iniciarTransaccion('dat_historial_forzado_update');
			$estados = array('idhistorialsin'=>$idhistorialsin
							,'nombre_archivo'=>$nombre_archivo
							,'fechaentrada'=>$fechaentrada
							,'estadoexp'=>$estadoexp
							,'estadoimp'=>$estadoimp
							,'tabla'=>$tabla
							,'fechamodificacion'=>$fechamodificacion
							,'completo'=>$completo
							,'contactualizador'=>$contactualizador
							,'estadoimpac'=>$estadoimpac								
							);
			
			$this->oBD->update('historial_forzado ', $estados, array('idhisesion' => $id));
		    $this->terminarTransaccion('dat_historial_forzado_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Historial_forzado").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM historial_forzado  "
					. " WHERE idhisesion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Historial_forzado").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('historial_forzado', array('idhisesion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Historial_forzado").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('historial_forzado', array($propiedad => $valor), array('idhisesion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Historial_forzado").": " . $e->getMessage());
		}
	}
   
		
}