<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatRubrica_competencia extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rubrica_competencia").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idrubrica_competencia,idrubrica,idcompetencia FROM rubrica_competencia";			
			
			$cond = array();		
					
			
			if(isset($filtros["idrubrica_competencia"])) {
					$cond[] = "idrubrica_competencia = " . $this->oBD->escapar($filtros["idrubrica_competencia"]);
			}
			if(isset($filtros["idrubrica"])) {
					$cond[] = "idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if(isset($filtros["idcompetencia"])) {
					$cond[] = "idcompetencia = " . $this->oBD->escapar($filtros["idcompetencia"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rubrica_competencia").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idrubrica,$idcompetencia)
	{
		try {
			
			$this->iniciarTransaccion('dat_rubrica_competencia_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrubrica_competencia) FROM rubrica_competencia");
			++$id;
			
			$estados = array('idrubrica_competencia' => $id
							,'idrubrica'=>$idrubrica
							,'idcompetencia'=>$idcompetencia							
							);
			
			$this->oBD->insert('rubrica_competencia', $estados);			
			$this->terminarTransaccion('dat_rubrica_competencia_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rubrica_competencia_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rubrica_competencia").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idrubrica,$idcompetencia)
	{
		try {
			$this->iniciarTransaccion('dat_rubrica_competencia_update');
			$estados = array('idrubrica'=>$idrubrica
							,'idcompetencia'=>$idcompetencia								
							);
			
			$this->oBD->update('rubrica_competencia ', $estados, array('idrubrica_competencia' => $id));
		    $this->terminarTransaccion('dat_rubrica_competencia_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rubrica_competencia").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT idrubrica_competencia,idrubrica,idcompetencia FROM rubrica_competencia  "
					. " WHERE idrubrica_competencia = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rubrica_competencia").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rubrica_competencia', array('idrubrica_competencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rubrica_competencia").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rubrica_competencia', array($propiedad => $valor), array('idrubrica_competencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rubrica_competencia").": " . $e->getMessage());
		}
	}
   
		
}