<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-03-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatForoComentario extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			
			$sql = "SELECT fc.idcomentario, fc.contenido, fc.idpublicacion, fc.idcomentariopadre, fc.estado, fc.idpersona, fc.fecha_hora, fc.nota, concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as autor FROM 
			foro_comentario fc
					inner join personal p on fc.idpersona=p.idpersona
			";

			// $cond = array();
			if (isset($filtros["idpublicacion"])) {
				$cond[] = "fc.idpublicacion = " . $this->oBD->escapar($filtros["idpublicacion"]);
			}
			if (isset($filtros["idcomentariopadre"])) {
				$cond[] = "fc.idcomentariopadre = " . $this->oBD->escapar($filtros["idcomentariopadre"]);
			}else{
				$cond[] = "fc.idcomentariopadre = " . $this->oBD->escapar('0');
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// echo $sql;
			// echo "\n";
			$sql .= " ORDER BY fecha_hora DESC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}
	public function listar($filtros = null)
	{
		try {
			// $this->usuarioAct = NegSesion::getUsuario();
			// $this->usuarioAct['idpersona']
			// $this->usuarioAct['idrol']
			$sql = "SELECT fc.idcomentario, fc.contenido, fc.idpublicacion, fc.idcomentariopadre, fc.estado, fc.idpersona, fc.fecha_hora, fc.nota, concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as autor FROM foro_comentario fc
					inner join personal p on fc.idpersona=p.idpersona
			";

			// $cond = array();
			if (isset($filtros["idpublicacion"])) {
				$cond[] = "fc.idpublicacion = " . $this->oBD->escapar($filtros["idpublicacion"]);
			}
			if (isset($filtros["idcomentariopadre"])) {
				$cond[] = "fc.idcomentariopadre = " . $this->oBD->escapar('0');
			}
			// if (isset($filtros["titulo"])) {
			// 	$cond[] = "titulo  " . $this->oBD->like($filtros["titulo"]);
			// }
			// if (isset($filtros["texto"])) {
			// 	$cond[] = "texto " . $this->oBD->like($filtros["texto"]);
			// }
			// if (isset($filtros["estado"])) {
			// 	$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			// }
			// if (isset($filtros["idusuario"])) {
			// 	$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			// }
			// if (isset($filtros["rol"])) {
			// 	$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			// }
			// if (isset($filtros["idcurso"])) {
			// 	$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			// }
			// if (isset($filtros["idgrupoaula"])) {
			// 	$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			// }
			// if (isset($filtros["idgrupoauladetalle"])) {
			// 	$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			// }
			// if (isset($filtros["idforopadre"])) {
			// 	$cond[] = "idforopadre = " . $this->oBD->escapar($filtros["idforopadre"]);
			// }
			// if (isset($filtros["tipo"])) {
			// 	$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			// }
			// if (isset($filtros["regfecha"])) {
			// 	$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			// }
			// /*if(isset($filtros["otrosdatos"])) {
			// 		$cond[] = "otrosdatos = " . $this->oBD->escapar($filtros["otrosdatos"]);
			// }	*/
			// if (isset($filtros["texto"])) {
			// 	$cond[] = "concat(texto,titulo) " . $this->oBD->like($filtros["texto"]);
			// }

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// echo $sql;
			// echo "\n";
			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}
	public function insertar($contenido, $idpublicacion, $idcomentariopadre, $idpersona)
	{
		try {

			$this->iniciarTransaccion('dat_foros_insert');

			// $id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpublicacion) FROM foro_publicacion");
			// ++$id;

			$estados = array(
				'contenido' => $contenido, 'idpublicacion' => $idpublicacion, 'idcomentariopadre' => $idcomentariopadre, 'estado' => '1', 'idpersona' => $idpersona
			);

			$this->oBD->insert('foro_comentario', $estados);
			$this->terminarTransaccion('dat_foro_comentario_insert');
			// return $id;
			return "ok";
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_foros_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function setNota($filtros = null)
	{
		try {

			$this->iniciarTransaccion('dat_forosComentNota_update');

			// $id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpublicacion) FROM foro_publicacion");
			// ++$id;

			$estados = array(
				'nota' => $filtros['nota']
			);

			$this->oBD->update('foro_comentario', $estados, array('idcomentario' => $filtros['idComent']));
			$this->terminarTransaccion('dat_forosComentNota_update');
			return "ok";
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_foros_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function actualizar($id, $titulo, $texto, $estado, $idusuario, $rol, $idcurso, $idgrupoaula, $idgrupoauladetalle, $idforopadre, $tipo, $otrosdatos)
	{
		try {
			$this->iniciarTransaccion('dat_foros_update');
			$estados = array(
				'titulo' => $titulo, 'texto' => $texto, 'estado' => $estado, 'idusuario' => $idusuario, 'rol' => $rol, 'idcurso' => $idcurso, 'idgrupoaula' => $idgrupoaula, 'idgrupoauladetalle' => $idgrupoauladetalle, 'idforopadre' => $idforopadre, 'tipo' => $tipo, 'otrosdatos' => $otrosdatos
			);

			$this->oBD->update('foros ', $estados, array('idforo' => $id));
			$this->terminarTransaccion('dat_foros_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT f.idpublicacion, f.titulo, f.subtitulo, f.estado, f.contenido, f.fecha_hora, f.idgrupoauladetalle, f.idpersona, f.idcategoria, concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as autor FROM foro_publicacion f
					inner join personal p on f.idpersona=p.idpersona"
				. " WHERE f.idpublicacion = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('foros', array('idforo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('foros', array($propiedad => $valor), array('idforo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Foros") . ": " . $e->getMessage());
		}
	}
}
