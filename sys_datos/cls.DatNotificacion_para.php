<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-02-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatNotificacion_para extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Notificacion_para").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idnotipersona ,idnotificacion,idpersona,estado FROM notificacion_para";			
			
			$cond = array();

			if(isset($filtros["idnotipersona"])) {
				$cond[] = "idnotipersona = " . $this->oBD->escapar($filtros["idnotipersona"]);
			}
			
			if(isset($filtros["idnotificacion"])) {
				$cond[] = "idnotificacion = " . $this->oBD->escapar($filtros["idnotificacion"]);
			}
			if(isset($filtros["idpersona"])) {
				$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM notificacion_para";
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if(!empty($filtros["orderby"])){
				$sql.=" ORDER BY ".$filtros["orderby"];
			}

			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;

			//$sql .= " ORDER BY fecha_creado ASC";
			
			//return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Notificacion_para").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idnotificacion,$idpersona,$estado)
	{
		try {
			
			//$this->iniciarTransaccion('dat_notificacion_para_insert');
			
			//$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idnotipersona) FROM notificacion_para");
			//++$id;
			
			$estados = array(//'idnotipersona' => $id	,						
							'idnotificacion'=>$idnotificacion
							,'idpersona'=>$idpersona
							,'estado'=>$estado							
							);
			
			//$this->oBD->insert('notificacion_para', $estados);		
			return $this->oBD->insertAI($tabla,$estados);	
			//$this->terminarTransaccion('dat_notificacion_para_insert');			
			//return $id;

		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_notificacion_para_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Notificacion_para").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnotificacion,$idpersona,$estado)
	{
		try {
			//$this->iniciarTransaccion('dat_notificacion_para_update');
			$estados = array('idnotificacion'=>$idnotificacion
							,'idpersona'=>$idpersona
							,'estado'=>$estado								
							);
			
			$this->oBD->update('notificacion_para ', $estados, array('idnotipersona' => $id));
		   // $this->terminarTransaccion('dat_notificacion_para_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notificacion_para").": " . $e->getMessage());
		}
	}
	public function actualizar2($estados,$condicion)
	{
		try {		
			
			return $this->oBD->update('notificacion_para ', $estados,$condicion);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.texto AS _texto ,tb3.nombre AS _nombre  FROM notificacion_para tb1 LEFT JOIN notificacion_de tb2 ON tb1.idnotificacion=tb2.idnotificacion  LEFT JOIN personal tb3 ON tb1.idpersona=tb3.idpersona  "
					. " WHERE tb1.idnotipersona = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Notificacion_para").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('notificacion_para', array('idnotipersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Notificacion_para").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('notificacion_para', array($propiedad => $valor), array('idnotipersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notificacion_para").": " . $e->getMessage());
		}
	}
   
		
}