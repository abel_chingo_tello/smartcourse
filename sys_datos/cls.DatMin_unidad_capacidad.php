<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatMin_unidad_capacidad extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Min_unidad_capacidad").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(idcurso) FROM min_unidad_capacidad";
			
			$cond = array();		
			
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["unidad"])) {
					$cond[] = "unidad = " . $this->oBD->escapar($filtros["unidad"]);
			}
			if(isset($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(isset($filtros["idcapacidad"])) {
					$cond[] = "idcapacidad = " . $this->oBD->escapar($filtros["idcapacidad"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Min_competencias").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT uc.id, uc.idcurso, uc.unidad, uc.orden, uc.idcapacidad, uc.idcursodetalle, mc.titulo,mc.idcompetencia,n.nombre as nombre_unidad FROM min_unidad_capacidad uc INNER JOIN acad_curso c ON uc.idcurso = c.idcurso JOIN min_capacidades mc ON mc.idcapacidad = uc.idcapacidad INNER JOIN acad_cursodetalle acc ON acc.idcursodetalle = uc.idcursodetalle INNER JOIN niveles n ON acc.idrecurso =	n.idnivel";			
			
			$cond = array();		
					
			
			if(isset($filtros["estado"])) {
				$cond[] = "c.estado = " . $this->oBD->escapar($filtros["estado"]);
            }
            if(isset($filtros["idcurso"])) {
                    $cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
            }
            if(isset($filtros["multi_idcurso"]) && is_array($filtros["multi_idcurso"]) && !empty($filtros["multi_idcurso"])) {
                $cond[] = "c.idcurso IN (" . implode(',',$filtros["multi_idcurso"]).")";
        }
            if(isset($filtros["orden"])) {
                    $cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
            }
            if(isset($filtros["idcapacidad"])) {
                    $cond[] = "idcapacidad = " . $this->oBD->escapar($filtros["idcapacidad"]);
            }						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY uc.id, uc.idcapacidad ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Min_unidad_capacidad").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$unidad,$orden,$idcapacidad)
	{
		try {
			
			$this->iniciarTransaccion('dat_min_unidad_capacidad_insert');
			
			// $id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcompetencia) FROM min_competencias");
			// ++$id;
			
			$estados = array(
							'idcurso'=>$idcurso
							,'unidad'=>$unidad
                            ,'orden'=>$orden
                            ,'idcapacidad' => $idcapacidad					
							);
			
			$this->oBD->insert('min_unidad_capacidad', $estados);			
			$this->terminarTransaccion('dat_min_unidad_capacidad_insert');			
			return $id;

		}catch(Exception $e) {
			$this->cancelarTransaccion('dat_min_unidad_capacidad_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("min_unidad_capacidad").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$unidad,$orden,$idcapacidad)
	{
		try {
			$this->iniciarTransaccion('dat_min_unidad_capacidad_update');
			$estados = array(
                            'idcurso'=>$idcurso
                            ,'unidad'=>$unidad
                            ,'orden'=>$orden
                            ,'idcapacidad' => $idcapacidad								
							);
			
			$this->oBD->update('min_unidad_capacidad ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_min_unidad_capacidad_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("min_unidad_capacidad").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  min_unidad_capacidad.id, min_unidad_capacidad.idcurso, min_unidad_capacidad.unidad, min_unidad_capacidad.orden, min_unidad_capacidad.idcapacidad, min_unidad_capacidad.idcursodetalle  FROM min_unidad_capacidad  "
					. " WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("min_unidad_capacidad").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('min_unidad_capacidad', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("min_unidad_capacidad").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('min_unidad_capacidad', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("min_unidad_capacidad").": " . $e->getMessage());
		}
	}
   
		
}