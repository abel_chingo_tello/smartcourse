<?php
class DatReportes extends DatBase
{
    public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("reportes").": " . $e->getMessage());
		}
    }
    
    public function examenes($filtros){
        try{
            if(!isset($filtros['idcurso']) || !isset($filtros['idgrupoaula']) || !isset($filtros['idproyecto']) ){
                return array();
            }
            $sql = "SELECT * ,(SELECT CONCAT(p.nombre, ' ', p.ape_paterno) FROM personal p WHERE p.idpersona = nq.idalumno LIMIT 1) AS strnombre FROM notas_quiz nq INNER JOIN 
            ( SELECT m.idalumno FROM acad_matricula m INNER JOIN acad_grupoauladetalle gud ON gud.idgrupoauladetalle = m.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON ga.idgrupoaula = gud.idgrupoaula
            WHERE gud.idgrupoaula = ".$filtros['idgrupoaula']." AND gud.idcurso = ".$filtros['idcurso']." AND ga.idproyecto = ".$filtros['idproyecto']." group by m.idalumno )
            AS tmp_matricula ON tmp_matricula.idalumno = nq.idalumno INNER JOIN (SELECT CONVERT(SUBSTRING(cd.txtjson,POSITION('idexamen=' IN cd.txtjson) + 9,POSITION('&' IN SUBSTRING(cd.txtjson,125 + 9)) - 1), UNSIGNED INTEGER) AS idExamen , cd.orden FROM acad_cursodetalle cd
            WHERE cd.idcurso = ".$filtros['idcurso']." AND ( cd.txtjson like '%\"typelink\":\"smartquiz\"%' OR cd.txtjson like '%/quiz/ver/?idexamen=%' OR cd.txtjson like '%idexamen=%') AND cd.txtjson NOT LIKE '%\"options\":%'
            ) AS tmp_idexamenes ON tmp_idexamenes.idExamen = nq.idrecurso";

            $cond = array();
            
            if(isset($filtros["fechaDesde"]) && !empty($filtros["fechaDesde"])) {
				$cond[] = "nq.regfecha >= " . $this->oBD->escapar($filtros["fechaDesde"]);
			}
			if(isset($filtros["fechaHasta"]) && !empty($filtros["fechaHasta"])) {
				$cond[] = "nq.regfecha <= " . $this->oBD->escapar($filtros["fechaHasta"]);
            }
            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("reportes").": " . $e->getMessage());
        }
    }
    public function examenAutoevaluacion($filtros){
        try{
            $sql = "SELECT * ,(SELECT CONCAT(p.nombre, ' ', p.ape_paterno) FROM personal p WHERE p.idpersona = nq.idalumno LIMIT 1) AS strnombre FROM notas_quiz nq INNER JOIN 
            ( 
                SELECT m.idalumno FROM acad_matricula m INNER JOIN acad_grupoauladetalle gud ON gud.idgrupoauladetalle = m.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON ga.idgrupoaula = gud.idgrupoaula
               WHERE gud.idgrupoaula = ".$filtros['idgrupoaula']." AND gud.idcurso = ".$filtros['idcurso']." AND ga.idproyecto = ".$filtros['idproyecto']." group by m.idalumno 
            ) AS tmp_matricula ON tmp_matricula.idalumno = nq.idalumno INNER JOIN 
            (
            SELECT CONVERT(SUBSTRING(cd.txtjson,POSITION('idexamen=' IN cd.txtjson) + 9,POSITION('&' IN SUBSTRING(cd.txtjson,125 + 9)) - 1), UNSIGNED INTEGER) AS idExamen , cd.orden FROM acad_cursodetalle cd
            WHERE cd.idcurso = ".$filtros['idcurso']." AND ( cd.txtjson like '%\"nombre\":\"Autoevaluación\"%' OR cd.txtjson like '%Autoevaluación%' OR cd.txtjson like '%Autoevaluacion%'OR cd.txtjson like '%autoevaluacion%') AND cd.txtjson LIKE '%\"options\":%'
            ) AS tmp_idexamenes ON tmp_idexamenes.idExamen = nq.idrecurso";
            $cond = array();
            if(isset($filtros["fechaDesde"]) && !empty($filtros["fechaDesde"])) {
				$cond[] = "nq.regfecha >= " . $this->oBD->escapar($filtros["fechaDesde"]);
			}
			if(isset($filtros["fechaHasta"]) && !empty($filtros["fechaHasta"])) {
				$cond[] = "nq.regfecha <= " . $this->oBD->escapar($filtros["fechaHasta"]);
            }
            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("reportes").": " . $e->getMessage());
        }
    }
    public function matricula($filtros){
        try{
            $sql = "SELECT m.idalumno,(SELECT CONCAT(p.nombre, ' ', p.ape_paterno) FROM personal p WHERE p.idpersona = m.idalumno LIMIT 1) as nombre FROM acad_matricula m INNER JOIN acad_grupoauladetalle gud ON gud.idgrupoauladetalle = m.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON ga.idgrupoaula = gud.idgrupoaula
            WHERE gud.idgrupoaula = ".$filtros['idgrupoaula']." AND gud.idcurso = ".$filtros['idcurso']." AND ga.idproyecto = ".$filtros['idproyecto']." group by m.idalumno";
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("reportes").": " . $e->getMessage());
        }
    }
}

?>