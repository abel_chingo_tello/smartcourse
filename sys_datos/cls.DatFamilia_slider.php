<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-03-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatFamilia_slider extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Familia_slider").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT familia_slider.codigo, familia_slider.idpersona, familia_slider.idproyecto, familia_slider.idempresa, familia_slider.link, familia_slider.fecha_hora FROM familia_slider";			
			
			$cond = array();		
					
			
			if(isset($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(isset($filtros["fecha_hora"])) {
					$cond[] = "fecha_hora = " . $this->oBD->escapar($filtros["fecha_hora"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Familia_slider").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($estados)
	{
		try {
			
			$this->iniciarTransaccion('dat_familia_slider_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(codigo) FROM familia_slider");
			++$id;
			$estados['codigo'] = $id;
			$this->oBD->insert('familia_slider', $estados);			
			$this->terminarTransaccion('dat_familia_slider_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_familia_slider_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Familia_slider").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona,$idproyecto,$idempresa,$link,$fecha_hora)
	{
		try {
			$this->iniciarTransaccion('dat_familia_slider_update');
			$estados = array('idpersona'=>$idpersona
							,'idproyecto'=>$idproyecto
							,'idempresa'=>$idempresa
							,'link'=>$link
							,'fecha_hora'=>$fecha_hora								
							);
			
			$this->oBD->update('familia_slider ', $estados, array('codigo' => $id));
		    $this->terminarTransaccion('dat_familia_slider_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Familia_slider").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  familia_slider.codigo, familia_slider.idpersona, familia_slider.idproyecto, familia_slider.idempresa, familia_slider.link, familia_slider.fecha_hora  FROM familia_slider  "
					. " WHERE codigo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Familia_slider").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			$link = $this->get($id)["link"];
			$this->oBD->delete('familia_slider', array('codigo' => $id));
			return $link;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Familia_slider").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('familia_slider', array($propiedad => $valor), array('codigo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Familia_slider").": " . $e->getMessage());
		}
	}
   
		
}