<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-05-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatPortafolio extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Portafolio") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT portafolio.idportafolio, portafolio.idalumno, portafolio.idproyecto, portafolio.estilo FROM portafolio";

			$cond = array();


			if (isset($filtros["idportafolio"])) {
				$cond[] = "idportafolio = " . $this->oBD->escapar($filtros["idportafolio"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Portafolio") . ": " . $e->getMessage());
		}
	}

	public function insertar($idalumno, $idproyecto)
	{
		try {

			$this->iniciarTransaccion('dat_portafolio_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idportafolio) FROM portafolio");
			++$id;

			$estados = array(
				'idportafolio' => $id, 'idalumno' => $idalumno, 'idproyecto' => $idproyecto
			);

			$this->oBD->insert('portafolio', $estados);
			$this->terminarTransaccion('dat_portafolio_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_portafolio_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Portafolio") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idalumno, $idproyecto)
	{
		try {
			$this->iniciarTransaccion('dat_portafolio_update');
			$estados = array(
				'idalumno' => $idalumno, 'idproyecto' => $idproyecto
			);

			$this->oBD->update('portafolio ', $estados, array('idportafolio' => $id));
			$this->terminarTransaccion('dat_portafolio_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Portafolio") . ": " . $e->getMessage());
		}
	}

	public function guardarEstilo($filtros)
	{
		try {
			$this->iniciarTransaccion('dat_portafolio_update');

			$estados = array('estilo' => $filtros["fondo"]);

			$this->oBD->update('portafolio ', $estados, array('idportafolio' => $filtros["idportafolio"]));
			$this->terminarTransaccion('dat_portafolio_update');
			return $filtros["idportafolio"];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Portafolio") . ": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT  portafolio.idportafolio, portafolio.idalumno, portafolio.idproyecto, portafolio.estilo  FROM portafolio  "
				. " WHERE idportafolio = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Portafolio") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('portafolio', array('idportafolio' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Portafolio") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('portafolio', array($propiedad => $valor), array('idportafolio' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Portafolio") . ": " . $e->getMessage());
		}
	}
}
