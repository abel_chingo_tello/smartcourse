<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-09-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatNotasenquiz extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}
	
	public function notas($filtros = null){
		try{
			$this->oBD->setLimite(0, 999999950);
			if(isset($filtros["sql"])){
				$sql=$filtros["sql"];
		    }else $sql="SELECT * FROM `aa_sesion`";
			$personas=$this->oBD->consultarSQL($sql);  			
			//$sql="SELECT p.identificador,p.usuario, ea.* FROM `personal` p INNER JOIN examen_alumno ea ON p.dni=ea.idalumno WHERE date(fecha)='2020-05-26'";
			//$personas=$this->oBD->consultarSQL($sql);			
			return $personas;
		}catch(Exception $e){
			throw new Exception($e);
		}
	}
}