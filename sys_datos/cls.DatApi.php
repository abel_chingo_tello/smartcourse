<?php
class DatApi extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Api").": " . $e->getMessage());
		}
	}
	private function setEstados($table){
		$result = array();
		foreach($table as $key => $value){
			if(is_null($value)){ continue; }
			$result[$key] = $value;
		}
		return $result;
	}
	private function getmatricula($filtros){
		try{
			$sql = "SELECT m.*,agd.idcurso FROM acad_matricula m INNER JOIN acad_grupoauladetalle agd ON m.idgrupoauladetalle = agd.idgrupoauladetalle";
			$cond = array();
			// if(isset($arr['idcurso'])){
			// 	$cond[] = "agd.idcurso = ".$this->oBD->escapar($arr['idcurso']) ;	
			// }
			if(isset($filtros["idalumno"])) {
				$cond[] = "m.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "m.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros['fecha_matricula'])){
				$cond[] = "m.fecha_matricula = ".$this->oBD->escapar($filtros['fecha_matricula']);
			} 
			if(isset($filtros['fecha_vencimiento'])){
				$cond[] = "m.fecha_vencimiento = ".$this->oBD->escapar($filtros['fecha_vencimiento']);
			}
			if(isset($filtros['idcomplementario'])){
				$cond[] = "m.idcomplementario = ".$this->oBD->escapar($filtros['idcomplementario']);
			}
			if(isset($filtros['idcategoria'])){
				$cond[] = "m.idcategoria = ".$this->oBD->escapar($filtros['idcategoria']);
			}
			if(isset($filtros['fechaactiva'])){
				$fecha_actual = date('Y-m-d');
				$cond[] = "m.fecha_matricula <= ".$this->oBD->escapar($fecha_actual);
				$cond[] = "m.fecha_vencimiento >= ".$this->oBD->escapar($fecha_actual);
			}
			if(!empty($cond)){
				$sql .= " WHERE " . implode(' AND ', $cond);				
			}
			$sql .= " GROUP BY m.idgrupoauladetalle";

			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e){
			return array();
		}
	}
	public function buscarmatriculados($filtros = array()){
		try {
			$sql = "SELECT p.* FROM personal p INNER JOIN acad_matricula am ON p.idpersona = am.idalumno INNER JOIN acad_grupoauladetalle agd ON am.idgrupoauladetalle = agd.idgrupoauladetalle";
			$cond = array();
			
			if(isset($filtros["idpersona"])) {
				if(is_array($filtros['idpersona'])){
					$cond[] = "p.idpersona IN (".implode(',', $filtros["idpersona"]).")";
				}else{
					$cond[] = "p.idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
				}
			}
			if(isset($filtros["dni"])) {
				if(is_array($filtros['dni'])){
					$cond[] = "p.dni IN (" .implode(',', $filtros["dni"]).")";
				}else{
					$cond[] = "p.dni = " . $this->oBD->escapar($filtros["dni"]);
				}
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function detallecursoxgrupo($filtros = array()){
		try {
			$sql = "SELECT acd.*,ac.nombre, n.nombre AS nombre_pestana FROM acad_cursodetalle acd INNER JOIN niveles n ON acd.idrecurso = n.idnivel INNER JOIN acad_curso ac ON acd.idcurso = ac.idcurso";
			$cond = array();
			
			
			if(isset($filtros["idgrupoauladetalle"])) {
				$sql .= " INNER JOIN acad_grupoauladetalle agd ON acd.idcurso = agd.idcurso";

				if(is_array($filtros['idgrupoauladetalle'])){
					$cond[] = "agd.idgrupoauladetalle IN (" .implode(',', $filtros["idgrupoauladetalle"]).")";
				}else{
					$cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
				}
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "acd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "agd.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " GROUP BY acd.idcursodetalle ORDER BY acd.orden ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function InsertMultiple($arr,$_key_,$is_prefijo,$prefijo){
		try{
			//do
			$this->iniciarTransaccion('dat_multiple_insert');
			//Logica
			foreach ($arr as $key => $value) {
				//insertar la tabla personal
				//Antes de ingresar consultar si existe el usuario, de lo contrario resolver la info
				if($value['personal']["idpersona"] != "-1"){
					$value['personal']['nuevo'] = 0;
					$idpersona = $value['personal']["idpersona"];
					if(!empty($value['personal_rol'])){
						foreach ($value['personal_rol'] as $irol => $vpersonarol) {
							$estados = $this->setEstados($vpersonarol);
							$estados['idpersonal'] = $idpersona;
							
							$sql = "SELECT * FROM persona_rol";
							$cond = array();
							$cond[] = "idrol = " . $this->oBD->escapar($estados["idrol"]);
							$cond[] = "idpersonal = " . $this->oBD->escapar($estados["idpersonal"]);
							$cond[] = "idempresa = " . $this->oBD->escapar($estados["idempresa"]);
							$cond[] = "idproyecto = " . $this->oBD->escapar($estados["idproyecto"]);
							if(!empty($cond)) {
								$sql .= " WHERE " . implode(' AND ', $cond);
							}
							$roles = $this->oBD->consultarEscalarSQL($sql);
							$estados["nuevo"] = 0;
							if(count($roles) == 0){
								$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
								++$idrol;
								$estados['iddetalle'] = $idrol;
								unset($estados["nuevo"]);
								$this->oBD->insert('persona_rol', $estados);
								$estados["nuevo"] = 1;
							}
							$value['personal_rol'][$irol] = $estados;
						}//end foreach personal_rol
						
					}
				} else {
					// var_dump($_key_);exit();
					if((int)$_key_ == 0){
						$estados = $this->setEstados($value['personal']);
						$idpersona = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
						++$idpersona;
						$estados['idpersona'] = $idpersona;
						if(isset($estados['clavesinmd5'])){
							unset($estados['clavesinmd5']);
						}
						if(isset($estados['idpersonasks'])){
							unset($estados['idpersonasks']);
						}
						if(isset($estados['instruccion'])){
							unset($estados['instruccion']);
						}
						if(isset($estados['edad_registro'])){
							unset($estados['edad_registro']);
						}

						if($is_prefijo == 1){
							$correlativo = $this->oBD->consultarEscalarSQL("SELECT COUNT(*) FROM personal WHERE usuario like '" . $prefijo . "%'");
							++$correlativo;
							$usu = $prefijo . str_pad($correlativo,12-strlen($prefijo),'0',STR_PAD_LEFT);
							$estados["usuario"] = $usu;
							$estados["clave"] = md5($usu);
						}
						$this->oBD->insert('personal', $estados);
						
						$value['personal']['idpersona'] = $idpersona;
						$value['personal']['nuevo'] = 1;
						if($is_prefijo == 1){
							$value['personal']['usuario'] = $usu;
							$value['personal']['clavesinmd5'] = $usu;
							$value['personal']['clave'] = md5($usu);
						}
						//insertar la tabla personal_rol, se necesita el idperona
						foreach ($value['personal_rol'] as $irol => $vpersonarol) {
							$estados = $this->setEstados($vpersonarol);
							$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
							++$idrol;
							$estados['iddetalle'] = $idrol;
							$estados['idpersonal'] = $idpersona;
							
							$this->oBD->insert('persona_rol', $estados);
							$estados["nuevo"] = 1;
							$value['personal_rol'][$irol] = $estados;
						}//end foreach personal_rol
					} else {
						$estados_ = $this->setEstados($value['personal']);
						$estados = array(
							"tipodoc"=>$estados_['tipodoc'],
							"dni"=>$estados_['dni']
						);
						$cond = array();
						foreach($estados as $col => $val) {
							$cond[] = $col . " = '" . $val . "'";
						}
						$sql = "SELECT * FROM personal WHERE " . implode(' AND ', $cond);
						$idpersona = $this->oBD->consultarSQL($sql)[0]["idpersona"];
						$value['personal']['idpersona'] = $idpersona;
					}
				}//endif $value['personal']["idpersona"]
				if(!isset($idpersona) || empty($idpersona)){
					throw new Exception("idpersona esta vacio");
				}
				
				//insertar la tabla acad_matricula, se necesita el idperona
				if(!empty($value['matricula'])){
					foreach ($value['matricula'] as $kmatri => $vmatricula) {
						$estados = $this->setEstados($vmatricula);
						if(isset($estados["idgrupoaula"])){
							$cond = array();
							$cond[] = "ga.idgrupoaula = '" . $estados["idgrupoaula"] . "'";
							$cond[] = "gad.idgrupoauladetalle = '" . $estados["idgrupoauladetalle"] . "'";
							$sql = "SELECT ga.*, gad.idcurso FROM acad_grupoaula ga INNER JOIN acad_grupoauladetalle gad ON (gad.idgrupoaula=ga.idgrupoaula) WHERE " . implode(' AND ', $cond);
							// echo $sql;exit();
							$ga_ = $this->oBD->consultarSQL($sql);
							if(count($ga_) > 0){
								$ga_ = $ga_[0];
								$estados["idcategoria"] = $ga_["idcategoria"];
								$cond = array();
								$cond[] = "idgrupoaula = '" . $ga_["idgrupoaula"] . "'";
								$cond[] = "idcategoria = '" . $ga_["idcategoria"] . "'";
								$cond[] = "idcursoprincipal = '" . $ga_["idcurso"] . "'";
								$sql = "SELECT * FROM acad_curso_complementario WHERE " . implode(' AND ', $cond);
								$cc = $this->oBD->consultarSQL($sql);
								if(count($cc) > 0){
									$estados["idcomplementario"] = $cc[0]["idcurso"];
								}
							}
							unset($estados["idgrupoaula"]);
						}
						/*Validar si tiene la matricula*/
						// var_dump($estados);
						
						$aBuscar = array(
							'idgrupoauladetalle'=>$estados['idgrupoauladetalle'],
							'idalumno'=>$idpersona,
							// 'fecha_matricula'=>$estados['fecha_matricula'],
							// 'fecha_vencimiento'=>$estados['fecha_vencimiento'],
							'idcomplementario'=>$estados["idcomplementario"],
							'idcategoria'=>$estados["idcategoria"],
							'fechaactiva'=>true
						);
						$buscarmat = $this->getmatricula($aBuscar);
						// var_dump($buscarmat);
						if(empty($buscarmat)){
							$sql = "SELECT gad.* FROM acad_grupoauladetalle gad WHERE gad.idgrupoauladetalle = " . $estados['idgrupoauladetalle'];
							$gad_ = $this->oBD->consultarSQL($sql)[0];
							$idmatricula = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatricula) FROM acad_matricula");
							++$idmatricula;
							$estados['idmatricula'] = $idmatricula;
							$estados['idalumno'] = $idpersona;
							$estados['idusuario'] = $idpersona;
							$fecha_inicio = new DateTime($gad_["fecha_inicio"]);
							$estados['fecha_matricula'] = $fecha_inicio->format("Y-m-d");
							// $estados['fecha_matricula'] = ($estados['tipomatricula'] == '1' ? $fecha_inicio->format("Y-m-d") : $estados['fecha_matricula']);
							$fecha_final = new DateTime($gad_["fecha_final"]);
							$estados['fecha_vencimiento'] = $fecha_final->format("Y-m-d");
							// $estados['fecha_vencimiento'] = ($estados['tipomatricula'] == '1' ? $fecha_final->format("Y-m-d") : $estados['fecha_vencimiento']);
							
							$this->oBD->insert('acad_matricula', $estados);
							$value['matricula'][$kmatri]["idmatriculaproducto"] = $idmatricula;
						}else{ 
							//actualizar los valores con el nuevo idgrupoauladetalle
							// foreach ($buscarmat as $kbmat => $vbmat) {
							// 	$this->oBD->update('acad_matricula', array('idgrupoauladetalle'=>$estados['idgrupoauladetalle']), array('idmatricula' => $vbmat['idmatricula']));
							// 	$value['matricula'][$kmatri]["idmatriculaproducto"] = $vbmat['idmatricula'];
							// }
							
							$value['matricula'][$kmatri]['signalMatriculaExist'] = true;
						}
					}
				}
				$arr[$key] = $value;
			}//end foreach $arr
			$this->terminarTransaccion('dat_multiple_insert');
			return $arr;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("InsertMultiple")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function _matricular_($datos){
		try{
			$this->iniciarTransaccion('dat_multiple_insert');

			// var_dump($datos);exit();
			$resultado = array();

			$personas = isset($datos['personas']) ? json_decode($datos['personas'], true) : null;
            $esdocente = isset($datos['esdocente']) ? $datos['esdocente'] : null;
            $fecha_inicio = isset($datos['fecha_inicio']) ? $datos['fecha_inicio'] : null;
            $fecha_vencimiento = isset($datos['fecha_vencimiento']) ? $datos['fecha_vencimiento'] : null;
            $idgrupoaula = isset($datos['idgrupoaula']) ? $datos['idgrupoaula'] : null;
            $idgrupoauladetalle = isset($datos['idgrupoauladetalle']) ? json_decode($datos['idgrupoauladetalle'], true) : null;
            $roles = isset($datos['roles']) ? json_decode($datos['roles'], true) : null;
            $empresaproyecto = isset($datos['empresaproyecto']) ? json_decode($datos['empresaproyecto'], true) : null;
            $tipousuario = isset($datos['tipousuario']) ? $datos['tipousuario'] : null;
            $idcurso = isset($datos['idcurso']) ? json_decode($datos['idcurso'], true) : null;
            $tipomatricula = isset($datos['tipomatricula']) ? $datos['tipomatricula'] : null;
			$pagorecurso = isset($datos['pagorecurso']) ? $datos['pagorecurso'] : null;
			$pagos = isset($datos['pagos']) ? json_decode($datos['pagos'], true) : null;
			$prefijo = isset($datos['prefijo']) ? $datos['prefijo'] : null;
			$is_prefijo = isset($datos['isprefijo']) ? $datos['isprefijo'] : false;

			// var_dump($idgrupoauladetalle);exit();

			if(is_array($idgrupoauladetalle)){
				// var_dump($idgrupoauladetalle);
				foreach ($idgrupoauladetalle as $_key_ => $idgad) {
					$info = array();
					$info['esdocente'] = $esdocente;
					$info['fecha_inicio'] = $fecha_inicio;
					$info['fecha_vencimiento'] = $fecha_vencimiento;
					$info['idgrupoaula'] = $idgrupoaula;
					$info['idgrupoauladetalle'] = $idgad;
					$info['roles'] = $roles;
					$info['empresaproyecto'] = $empresaproyecto;
					$info['tipousuario'] = $tipousuario;
					$info['idcurso'] = (!empty($idcurso[$_key_]) ? $idcurso[$_key_] : 0);
					$info['tipomatricula'] = $tipomatricula;
					$info['pagorecurso'] = $pagorecurso;
					$data = NegTools::SmartFormatoMultiple($info,$personas,$_key_,$prefijo,$is_prefijo);
					$arr = $data["usuario"];
					// var_dump($data, $arr);exit();
					//Logica
					foreach ($arr as $key => $value) {
						//insertar la tabla personal
						//Antes de ingresar consultar si existe el usuario, de lo contrario resolver la info
						if($value['personal']["idpersona"] != "-1"){
							$value['personal']['nuevo'] = 0;
							$idpersona = $value['personal']["idpersona"];
							if(!empty($value['personal_rol'])){
								foreach ($value['personal_rol'] as $irol => $vpersonarol) {
									$estados = $this->setEstados($vpersonarol);
									$estados['idpersonal'] = $idpersona;
									
									$sql = "SELECT * FROM persona_rol";
									$cond = array();
									$cond[] = "idrol = " . $this->oBD->escapar($estados["idrol"]);
									$cond[] = "idpersonal = " . $this->oBD->escapar($estados["idpersonal"]);
									$cond[] = "idempresa = " . $this->oBD->escapar($estados["idempresa"]);
									$cond[] = "idproyecto = " . $this->oBD->escapar($estados["idproyecto"]);
									if(!empty($cond)) {
										$sql .= " WHERE " . implode(' AND ', $cond);
									}
									$roles = $this->oBD->consultarSQL($sql);
									$estados["nuevo"] = 0;
									if(count($roles) == 0){
										$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
										++$idrol;
										$estados['iddetalle'] = $idrol;
										unset($estados["nuevo"]);
										$this->oBD->insert('persona_rol', $estados);
										$estados["nuevo"] = 1;
									}
									$value['personal_rol'][$irol] = $estados;
								}//end foreach personal_rol
								
							}
						} else {
							// var_dump($_key_);exit();
							if((int)$_key_ == 0){
								$estados = $this->setEstados($value['personal']);
								$idpersona = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
								++$idpersona;
								$estados['idpersona'] = $idpersona;
								if(isset($estados['clavesinmd5'])){
									unset($estados['clavesinmd5']);
								}
								if(isset($estados['idpersonasks'])){
									unset($estados['idpersonasks']);
								}
								if(isset($estados['instruccion'])){
									unset($estados['instruccion']);
								}
								if(isset($estados['edad_registro'])){
									unset($estados['edad_registro']);
								}

								if($is_prefijo == 1){
									$correlativo = $this->oBD->consultarEscalarSQL("SELECT COUNT(*) FROM personal WHERE usuario like '" . $prefijo . "%'");
									++$correlativo;
									$usu = $prefijo . str_pad($correlativo,12-strlen($prefijo),'0',STR_PAD_LEFT);
									$estados["usuario"] = $usu;
									$estados["clave"] = md5($usu);
								}
								
								$this->oBD->insert('personal', $estados);
								
								$value['personal']['idpersona'] = $idpersona;
								$value['personal']['nuevo'] = 1;
								if($is_prefijo == 1){
									$value['personal']['usuario'] = $usu;
									$value['personal']['clavesinmd5'] = $usu;
									$value['personal']['clave'] = md5($usu);
								}
								//insertar la tabla personal_rol, se necesita el idperona
								foreach ($value['personal_rol'] as $irol => $vpersonarol) {
									$estados = $this->setEstados($vpersonarol);
									$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
									++$idrol;
									$estados['iddetalle'] = $idrol;
									$estados['idpersonal'] = $idpersona;
									
									$this->oBD->insert('persona_rol', $estados);
									$estados["nuevo"] = 1;
									$value['personal_rol'][$irol] = $estados;
								}//end foreach personal_rol
							} else {
								$estados_ = $this->setEstados($value['personal']);
								$estados = array(
									"tipodoc"=>$estados_['tipodoc'],
									"dni"=>$estados_['dni']
								);
								$cond = array();
								foreach($estados as $col => $val) {
									$cond[] = $col . " = '" .
									 $val . "'";
								}
								$sql = "SELECT * FROM personal WHERE " . implode(' AND ', $cond);
								$idpersona = $this->oBD->consultarSQL($sql)[0]["idpersona"];
								$value['personal']['idpersona'] = $idpersona;
							}
						}//endif $value['personal']["idpersona"]
						if(!isset($idpersona) || empty($idpersona)){
							throw new Exception("idpersona esta vacio");
						}
						
						//insertar la tabla acad_matricula, se necesita el idperona
						if(!empty($value['matricula'])){
							foreach ($value['matricula'] as $kmatri => $vmatricula) {
								$estados = $this->setEstados($vmatricula);
								if(isset($estados["idgrupoaula"])){
									$cond = array();
									$cond[] = "ga.idgrupoaula = '" . $estados["idgrupoaula"] . "'";
									$cond[] = "gad.idgrupoauladetalle = '" . $estados["idgrupoauladetalle"] . "'";
									$sql = "SELECT ga.*, gad.idcurso FROM acad_grupoaula ga INNER JOIN acad_grupoauladetalle gad ON (gad.idgrupoaula=ga.idgrupoaula) WHERE " . implode(' AND ', $cond);
									// echo $sql;exit();
									$ga_ = $this->oBD->consultarSQL($sql);
									if(count($ga_) > 0){
										$ga_ = $ga_[0];
										$estados["idcategoria"] = $ga_["idcategoria"];
										$cond = array();
										$cond[] = "idgrupoaula = '" . $ga_["idgrupoaula"] . "'";
										$cond[] = "idcategoria = '" . $ga_["idcategoria"] . "'";
										$cond[] = "idcursoprincipal = '" . $ga_["idcurso"] . "'";
										$sql = "SELECT * FROM acad_curso_complementario WHERE " . implode(' AND ', $cond);
										$cc = $this->oBD->consultarSQL($sql);
										if(count($cc) > 0){
											$estados["idcomplementario"] = $cc[0]["idcurso"];
										}
									}
									unset($estados["idgrupoaula"]);
								}
								/*Validar si tiene la matricula*/
								// var_dump($estados);
								
								$aBuscar = array(
									'idgrupoauladetalle'=>$estados['idgrupoauladetalle'],
									'idalumno'=>$idpersona,
									// 'fecha_matricula'=>$estados['fecha_matricula'],
									// 'fecha_vencimiento'=>$estados['fecha_vencimiento'],
									'idcomplementario'=>$estados["idcomplementario"],
									'idcategoria'=>$estados["idcategoria"],
									'fechaactiva'=>true
								);
								$buscarmat = $this->getmatricula($aBuscar);
								// var_dump($buscarmat);
								if(empty($buscarmat)){
									$sql = "SELECT gad.* FROM acad_grupoauladetalle gad WHERE gad.idgrupoauladetalle = " . $estados['idgrupoauladetalle'];
									$gad_ = $this->oBD->consultarSQL($sql)[0];
									$idmatricula = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatricula) FROM acad_matricula");
									++$idmatricula;
									$estados['idmatricula'] = $idmatricula;
									$estados['idalumno'] = $idpersona;
									$estados['idusuario'] = $idpersona;
									$fecha_inicio = new DateTime($gad_["fecha_inicio"]);
									$estados['fecha_matricula'] = $fecha_inicio->format("Y-m-d");
									// $estados['fecha_matricula'] = ($estados['tipomatricula'] == '1' ? $fecha_inicio->format("Y-m-d") : $estados['fecha_matricula']);
									$fecha_final = new DateTime($gad_["fecha_final"]);
									$estados['fecha_vencimiento'] = $fecha_final->format("Y-m-d");
									// $estados['fecha_vencimiento'] = ($estados['tipomatricula'] == '1' ? $fecha_final->format("Y-m-d") : $estados['fecha_vencimiento']);
									
									$this->oBD->insert('acad_matricula', $estados);
									$value['matricula'][$kmatri]["idmatriculaproducto"] = $idmatricula;
								}else{
									$value['matricula'][$kmatri]['signalMatriculaExist'] = true;
								}
							}
						}
						$arr[$key] = $value;
					}
					//end Logica
					$resultado[] = $arr;
				}
			}
			
			$this->terminarTransaccion('dat_multiple_insert');
			return $resultado;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("InsertMultiple")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}

	public function _buscarCategorias($filtros = array()){
		try {
			$sql = "SELECT * FROM acad_categorias";			
			

			$cond = array();	
			if(isset($filtros["idcategoria"])) {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}		

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY orden ASC";
			
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("_buscarCategorias")." ".JrTexto::_("api").": " . $e->getMessage());
		}
	}

	public function _buscarPersonal($filtros = array()){
		try {
			$sql = "SELECT pe.* FROM personal pe";		
			
			$cond = array();		
					
			if(isset($filtros["idpersona"])) {
					$cond[] = "pe.idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["tipodoc"])) {
					$cond[] = "pe.tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if(isset($filtros["dni"])) {
					$cond[] = "pe.dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "pe.estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_externa").": " . $e->getMessage());
		}
	}

	public function _buscarRol($filtros = array()){
		try {
			$sql = "SELECT * FROM persona_rol";		
			
			$cond = array();		
					
			if(isset($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_externa").": " . $e->getMessage());
		}
	}

	public function _buscargrupos($filtros = null)
	{
		try {
			$sql = "SELECT ag.idgrupoaula,ag.nombre as strgrupoaula, comentario,nvacantes, idgrupoauladetalle, agd.nombre ,agd.fecha_inicio,agd.fecha_final, ag.tipo, (SELECT cu.tipo FROM acad_curso cu WHERE cu.idcurso = agd.idcurso) as tipocurso, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, ag.estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, agd.idcurso, (SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , agd.iddocente, agd.idlocal, idambiente,ag.tipogrupo,ag.idproducto, ag.idcategoria,agd.idcomplementario FROM acad_grupoaula ag LEFT JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula ";
			$cond = array();
			if (isset($filtros["idgrupoaula"])) {
				if(is_array($filtros["idgrupoaula"])){
					$cond[] = "ag.idgrupoaula IN (".implode(",",$filtros["idgrupoaula"]).")";
				}else{
					$cond[] = "ag.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
				}
			}
			if (isset($filtros['idgrupoauladetalle'])) {
				if (is_array($filtros["idgrupoauladetalle"])) {
					$cond[] = "agd.idgrupoauladetalle IN (" . implode(",", $filtros["idgrupoauladetalle"]) . ")";
				} else {
					$cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
				}
			}
			if (isset($filtros["texto"])) {
				$cond[] = "ag.nombre " . $this->oBD->Like($filtros["texto"]);
				$cond[] = "ag.comentario " . $this->oBD->Like($filtros["texto"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "ag.nombre " . $this->oBD->Like($filtros["nombre"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "ag.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["idlocal"])) {
				$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if (isset($filtros["idlocal_grupo"])) {
				$cond[] = "ag.idlocal = " . $this->oBD->escapar($filtros["idlocal_grupo"]);
			}
			if (isset($filtros["idambiente"])) {
				$cond[] = "agd.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "ag.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idcurso"])) {
				if (is_array($filtros["idcurso"])) {
					$cond[] = "agd.idcurso IN (" . implode(",", $filtros["idcurso"]) . ")";
				} else {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
			}
			if (isset($filtros["idcategoria"])) {
				if (is_array($filtros["idcategoria"])) {
					$cond[] = "ag.idcategoria IN (" . implode(",", $filtros["idcategoria"]) . ")";
				} else {
					$cond[] = "ag.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
				}
			}
			
			if (isset($filtros["idproyecto"])&&!isset($filtros["mysql1"])) {
				if(is_array($filtros["idproyecto"])){
					$cond[] = "ag.idproyecto IN (".implode(",",$filtros["idproyecto"]).")";
				}else{
					$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
				}
			}
			if (isset($filtros["idproducto"])) {
				$cond[] = "ag.idproducto = " . $this->oBD->escapar($filtros["idproducto"]);
			}
			if (isset($filtros['fechaactiva'])) {
				$fecha_actual = date('Y-m-d');
				// $cond[] = "ag.fecha_inicio <= ".$this->oBD->escapar($fecha_actual);
				$cond[] = "ag.fecha_fin >= " . $this->oBD->escapar($fecha_actual);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (isset($filtros["mysql1"])){
				$sql .= " ORDER BY strgrupoaula ASC";
			}
			
			// echo $sql;exit();

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function _buscargrupos2($filtros = null)
	{
		try {
			$sql = "SELECT ag.idgrupoaula, ag.nombre, agd.idgrupoauladetalle, agd.fecha_inicio, agd.fecha_final FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON (ag.idgrupoaula = agd.idgrupoaula) ";
			$cond = array();
			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "ag.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if (isset($filtros["idlocal"])) {
				$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if (isset($filtros["idlocal_grupo"])) {
				$cond[] = "ag.idlocal = " . $this->oBD->escapar($filtros["idlocal_grupo"]);
			}
			if (isset($filtros["idcurso"])) {
				if (is_array($filtros["idcurso"])) {
					$cond[] = "agd.idcurso IN (" . implode(",", $filtros["idcurso"]) . ")";
				} else {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
			}
			if (isset($filtros["idproyecto"])&&!isset($filtros["mysql1"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idproducto"])) {
				$cond[] = "ag.idproducto = " . $this->oBD->escapar($filtros["idproducto"]);
			}
			if (isset($filtros['fechaactiva'])) {
				$fecha_actual = date('Y-m-d');
				// $cond[] = "ag.fecha_inicio <= ".$this->oBD->escapar($fecha_actual);
				$cond[] = "ag.fecha_fin >= " . $this->oBD->escapar($fecha_actual);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			// echo $sql;exit();

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("_buscargrupos2") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function _transferir($datos = array())
	{
		try {
			$retorno = true;
			$this->iniciarTransaccion('dat_transfrerir');

			$idmatricula = explode(",", $datos['idmatricula']);
            $idgrupoauladetalle = $datos['idgrupoauladetalle'];
            $fecha_inicio = $datos['fecha_inicio'];
			$fecha_final = $datos['fecha_final'];
			
			// unset($datos["idmatricula"]);
			$data = array(
				"idgrupoauladetalle" => $idgrupoauladetalle,
				"fecha_matricula" => $fecha_inicio,
				"fecha_vencimiento" => $fecha_final
			);

			foreach ($idmatricula as $key => $value) {
				$this->oBD->update('acad_matricula', $data, array('idmatricula' => $value));
			}
			
			//Buscar info del grupo
			$sql = "SELECT agd.idgrupoauladetalle,agd.idgrupoaula,ag.nombre AS nombregrupo, ac.idcategoria AS idnivel, ac.nombre AS nivel, ac.idpadre AS idcarrera,(SELECT nombre FROM acad_categorias WHERE idcategoria = ac.idpadre) AS carrera FROM acad_grupoauladetalle agd INNER JOIN acad_grupoaula ag ON agd.idgrupoaula = ag.idgrupoaula LEFT JOIN acad_categorias ac ON ag.idcategoria = ac.idcategoria";
			$sql .= " AND agd.idgrupoauladetalle = ".$this->oBD->escapar($idgrupoauladetalle);
			$rs = $this->oBD->consultarSQL($sql);
			if(!empty($rs)){
				$retorno = end($rs);
			}
			$this->terminarTransaccion('dat_transfrerir');
			return $retorno;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_transfrerir');
			throw new Exception("ERROR\n" . JrTexto::_("_transferir") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function _smtp($datos = array())
	{
		try {
			// var_dump($datos);
			$sql = "SELECT p.idproyecto 
			FROM proyecto p 
			INNER JOIN bolsa_empresas e ON (p.idempresa = e.idempresa)
			INNER JOIN personal pe ON (pe.idpersona = e.idpersona)
			WHERE pe.usuario = " . $this->oBD->escapar($datos["usuario"]);
			$idproyecto = $this->oBD->consultarSQL($sql)[0]["idproyecto"];
			$this->iniciarTransaccion('dat_empresa_update');
			$this->oBD->update('proyecto', $datos["data"], array('idproyecto' => $idproyecto));
		    $this->terminarTransaccion('dat_empresa_update');
		    return true;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa").": " . $e->getMessage());
		}
	}

	public function setUsuario($usuario,$roles,$params =[]){
		try{
			$tmp_usuario = $usuario;
			if(empty($usuario)){ throw new Exception("usuario vacio"); }
			$this->iniciarTransaccion('dat_setUsuario_insert');
			if(isset($usuario["roles"])){ unset($usuario["roles"]); }
			if(isset($usuario["edad_registro"])){ unset($usuario["edad_registro"]); }
			if(isset($usuario["instruccion"])){ unset($usuario["instruccion"]); }
			$idpersona = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
			++$idpersona;

			if($params["isprefijo"] == 1){
				$correlativo = $this->oBD->consultarEscalarSQL("SELECT COUNT(*) FROM personal WHERE usuario like '" . $params["prefijo"] . "%'");
				++$correlativo;
				$usuario["usuario"] = $params["prefijo"] . $correlativo;
				$usuario["clave"] = md5($usuario["usuario"]);
			}else{
				$parte1 = substr($usuario["nombre"], 0, 1) . substr($usuario["ape_paterno"], 0, 1);
				$parte2 = $usuario["dni"];
				$usuario["usuario"] = $parte1 . $parte2;
				$usuario["clave"] = md5($usuario["usuario"]);
			}

			$usuario["idpersona"] = $idpersona;

			$this->oBD->insert('personal', $usuario);
			$usuario = array_replace_recursive($tmp_usuario,$usuario);

			if(!empty($roles)){
				foreach($roles as $rol){
					$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
					++$idrol;
					$estados = [
						"iddetalle" => $idrol,
						"idpersonal" => $idpersona,
						"idrol" => $rol,
						"idempresa" => $params["idempresa"],
						"idproyecto" => $params["idproyecto"]
					];

					$this->oBD->insert('persona_rol', $estados);
				}
			}

			$this->terminarTransaccion('dat_setUsuario_insert');
			return $usuario;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_setUsuario_insert');
			throw new Exception("ERROR\n".JrTexto::_("setUsuario")." ".JrTexto::_("api").": " . $e->getMessage());
		}
	}
	public function buscarNombreCarreraNivel(&$prepare, $idcategoria,$arreglo){
		try{
			$index = array_search($idcategoria,array_column($arreglo,"idcategoria"));
			if($index !== false){
				if(!empty($arreglo[$index]["idpadre"])){
					$prepare["idnivel"] = $arreglo[$index]["idcategoria"];
					$prepare["nombrenivel"] = $arreglo[$index]["nombre"];
					$prepare["idcarrera"] = $arreglo[$index]["idpadre"];
					$prepare["nombrecarrera"] = $arreglo[$index]["carrera"];
				}
			}
		}catch(Exception $e){
			throw new Exception("ERROR\n".JrTexto::_("buscarNombreCarreraNivel")." ".JrTexto::_("api").": " . $e->getMessage());
		}
	}

	public function matricularapi($datos){
		try{
			$datos["matriculados"] = [];
			//registrar usuarios
			foreach($datos["usuarios"] as $key => $usuario){
				if(empty($usuario["idpersona"])){
					$usuario = $this->setUsuario($usuario,$datos["roles"],$datos);
				}else{
					//check roles
					foreach($datos["roles"] as $rol){
						$search = !empty($usuario["roles"]) ? array_search($rol,array_column($usuario["roles"],"idrol")) : false;
						//VERIFICAR SI TIENE ROL CON LA EMPRESA
						if($search !== false && $usuario["roles"][$search]["idproyecto"] != $datos["idproyecto"]){ $search = false; }
						if($search === false){
							$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
							++$idrol;
							$estados = [
								"iddetalle" => $idrol,
								"idpersonal" => $usuario["idpersona"],
								"idrol" => $rol,
								"idempresa" => $datos["idempresa"],
								"idproyecto" => $datos["idproyecto"]
							];

							$this->oBD->insert('persona_rol', $estados);
						}
					}
				}
				$datos["usuarios"][$key] = $usuario;
			}
			//matricular
			$this->iniciarTransaccion('dat_multiple_insert');

			if(!empty($datos["matricula"]) && !empty($datos["usuarios"])){
				if(empty($datos["grupoauladetalle"])){ throw new Exception("grupoauladetalle no esta definido"); }
				$resetmatricula = $datos["matricula"]; //para resetear en caso tal al formato principal
				$infocarrera = [];
				foreach($datos["usuarios"] as $k => $usuario){
					$usuario["matriculas"] = [];
					foreach($datos["grupoauladetalle"] as $auladetalle){
						//buscar matricula existente
						$buscarmat = $this->getmatricula(["idgrupoauladetalle"=>$auladetalle["idgrupoauladetalle"],"idalumno"=>$usuario["idpersona"],"fechaactiva"=>true]);
						if(empty($buscarmat)){
							$prepare = [
								"idcurso" => $auladetalle["idcurso"],
								"idcarrera" => NULL,
								"idnivel" => NULL,
								"idgrupoauladetalle" => $auladetalle["idgrupoauladetalle"],
								"nombregrupo" => $auladetalle["strgrupoaula"],
								"nombrecarrera" => NULL,
								"nombrenivel" => NULL
							];
							$idmatricula = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatricula) FROM acad_matricula");
							++$idmatricula;
							$datos["matricula"]["idmatricula"] = $idmatricula;
							$datos["matricula"]["idgrupoauladetalle"] = $auladetalle["idgrupoauladetalle"];
							$datos["matricula"]["idalumno"] = $usuario["idpersona"];
							$datos["matricula"]["idusuario"] = $usuario["idpersona"];
							if(!empty($auladetalle["idcomplementario"])){ $datos["matricula"]["idcomplementario"] = $auladetalle["idcomplementario"]; }
							if(!empty($auladetalle["idcategoria"])){ 
								$datos["matricula"]["idcategoria"] = $auladetalle["idcategoria"];
								if(!empty($datos["categorias"])){
									//buscar nombre de carrera y nivel
									$this->buscarNombreCarreraNivel($prepare,$auladetalle["idcategoria"],$datos["categorias"]);
								}
							}
							$this->oBD->insert('acad_matricula', $datos["matricula"]);

							$usuario["matriculas"][] = $datos["matricula"] + $prepare;

						}
					}
					$datos["usuarios"][$k] = $usuario;
				}//end foreach usuario
			}
			$this->terminarTransaccion('dat_multiple_insert');
			return $datos;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("InsertMultiple")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}

	public function _matricular($datos){
		try{
			$this->iniciarTransaccion('dat_multiple_insert');

			// var_dump($datos);exit();
			$resultado = array();

			$personas = isset($datos['personas']) ? json_decode($datos['personas'], true) : null;
            $esdocente = isset($datos['esdocente']) ? $datos['esdocente'] : null;
            $fecha_inicio = isset($datos['fecha_inicio']) ? $datos['fecha_inicio'] : null;
            $fecha_vencimiento = isset($datos['fecha_vencimiento']) ? $datos['fecha_vencimiento'] : null;
            $idgrupoaula = isset($datos['idgrupoaula']) ? $datos['idgrupoaula'] : null;
            $idgrupoauladetalle = isset($datos['idgrupoauladetalle']) ? json_decode($datos['idgrupoauladetalle'], true) : null;
            $roles = isset($datos['roles']) ? json_decode($datos['roles'], true) : null;
            $empresaproyecto = isset($datos['empresaproyecto']) ? json_decode($datos['empresaproyecto'], true) : null;
            $tipousuario = isset($datos['tipousuario']) ? $datos['tipousuario'] : null;
            $idcurso = isset($datos['idcurso']) ? json_decode($datos['idcurso'], true) : null;
            $tipomatricula = isset($datos['tipomatricula']) ? $datos['tipomatricula'] : null;
			$pagorecurso = isset($datos['pagorecurso']) ? $datos['pagorecurso'] : null;
			$pagos = isset($datos['pagos']) ? json_decode($datos['pagos'], true) : null;
			$prefijo = isset($datos['prefijo']) ? $datos['prefijo'] : null;
			$is_prefijo = isset($datos['isprefijo']) ? $datos['isprefijo'] : false;
			$fecha_custom = !empty($datos["fecha_custom"]) ? true : false;
			$notificar = !empty($datos["notificar"]) ? 1 : 0;
			// var_dump($idgrupoauladetalle);exit();

			if(is_array($idgrupoauladetalle)){
				// var_dump($idgrupoauladetalle);
				foreach ($idgrupoauladetalle as $_key_ => $idgad) {
					$info = array();
					$info['esdocente'] = $esdocente;
					$info['fecha_inicio'] = $fecha_inicio;
					$info['fecha_vencimiento'] = $fecha_vencimiento;
					$info['idgrupoaula'] = $idgrupoaula;
					$info['idgrupoauladetalle'] = $idgad;
					$info['roles'] = $roles;
					$info['empresaproyecto'] = $empresaproyecto;
					$info['tipousuario'] = $tipousuario;
					$info['idcurso'] = (!empty($idcurso[$_key_]) ? $idcurso[$_key_] : 0);
					$info['tipomatricula'] = $tipomatricula;
					$info['pagorecurso'] = $pagorecurso;
					$info["estado_matricula"] = !empty($datos["estado_matricula"]) ? $datos["estado_matricula"] : 1;
					$data = NegTools::SmartFormatoMultiple($info,$personas,$_key_,$prefijo,$is_prefijo);
					$arr = $data["usuario"];
					// var_dump($data, $arr);exit();
					//Logica
					foreach ($arr as $key => $value) {
						//insertar la tabla personal
						//Antes de ingresar consultar si existe el usuario, de lo contrario resolver la info
						if((int)$_key_ == 0){
							$estados = $this->setEstados($value['personal']);
							$persona = $this->_buscarPersonal(array("dni" => $estados["dni"], "tipodoc" => $estados["tipodoc"]));
							if(count($persona) > 0) {
								$clavesinmd5 = $estados['clavesinmd5'];
								$idpersonasks = $estados['idpersonasks'];
								$instruccion = $estados['instruccion'];
								$edad_registro = $estados['edad_registro'];
								$value['personal'] = $persona[0];
								$value['personal']['clavesinmd5'] = $clavesinmd5;
								$value['personal']['idpersonasks'] = $idpersonasks;
								$value['personal']['instruccion'] = $instruccion;
								$value['personal']['edad_registro'] = $edad_registro;
								$value['personal']['nuevo'] = 0;
								$idpersona = $value['personal']["idpersona"];
								if(!empty($value['personal_rol'])){
									foreach ($value['personal_rol'] as $irol => $vpersonarol) {
										$estados = $this->setEstados($vpersonarol);
										$estados['idpersonal'] = $idpersona;
										
										$roles = $this->_buscarRol($estados);
										$estados["nuevo"] = 0;
										if(count($roles) == 0){
											$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
											++$idrol;
											$estados['iddetalle'] = $idrol;
											unset($estados["nuevo"]);
											$this->oBD->insert('persona_rol', $estados);
											$estados["nuevo"] = 1;
										}
										$value['personal_rol'][$irol] = $estados;
									}//end foreach personal_rol
									
								}
							} else {
								$idpersona = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
								++$idpersona;
								$estados['idpersona'] = $idpersona;
								if(isset($estados['clavesinmd5'])){
									unset($estados['clavesinmd5']);
								}
								if(isset($estados['idpersonasks'])){
									unset($estados['idpersonasks']);
								}
								if(isset($estados['instruccion'])){
									unset($estados['instruccion']);
								}
								if(isset($estados['edad_registro'])){
									unset($estados['edad_registro']);
								}

								if($is_prefijo == 1){
									$correlativo = $this->oBD->consultarEscalarSQL("SELECT COUNT(*) FROM personal WHERE usuario like '" . $prefijo . "%'");
									++$correlativo;
									// $usu = $prefijo . str_pad($correlativo,12-strlen($prefijo),'0',STR_PAD_LEFT);
									$usu = $prefijo . $correlativo;
									$clave = $usu;
									$estados["usuario"] = $usu;
									$estados["clave"] = md5($clave);
								} else {
									$parte1 = substr($estados["nombre"], 0, 1) . substr($estados["ape_paterno"], 0, 1);
									$parte2 = $estados["dni"];
									$usu = $parte1 . $parte2;
									// $clave = $parte2;
									$clave = $usu;
									$estados["usuario"] = $usu;
									$estados["clave"] = md5($clave);
								}
								
								if(empty($estados['fechanac'])){ $estados['fechanac'] = date("Y-m-d"); }
								if(empty($estados['regfecha'])){ $estados['regfecha'] = date("Y-m-d"); }

								$this->oBD->insert('personal', $estados);
								
								$value['personal']['idpersona'] = $idpersona;
								$value['personal']['nuevo'] = 1;
								// if($is_prefijo == 1){
								// 	$value['personal']['usuario'] = $usu;
								// 	$value['personal']['clavesinmd5'] = $usu;
								// 	$value['personal']['clave'] = md5($usu);
								// }
								$value['personal']['usuario'] = $usu;
								$value['personal']['clavesinmd5'] = $clave;
								$value['personal']['clave'] = md5($clave);
								//insertar la tabla personal_rol, se necesita el idperona
								foreach ($value['personal_rol'] as $irol => $vpersonarol) {
									$estados = $this->setEstados($vpersonarol);
									$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
									++$idrol;
									$estados['iddetalle'] = $idrol;
									$estados['idpersonal'] = $idpersona;
									
									$this->oBD->insert('persona_rol', $estados);
									$estados["nuevo"] = 1;
									$value['personal_rol'][$irol] = $estados;
								}//end foreach personal_rol
							}
						} else {
							$estados_ = $this->setEstados($value['personal']);
							$filtros = array(
								"tipodoc"=>$estados_['tipodoc'],
								"dni"=>$estados_['dni']
							);
							$idpersona = $this->_buscarPersonal($filtros)[0]["idpersona"];
							$value['personal']['idpersona'] = $idpersona;
						}
						
						if(!isset($idpersona) || empty($idpersona)){
							throw new Exception("idpersona esta vacio");
						}
						
						//insertar la tabla acad_matricula, se necesita el idperona
						if(!empty($value['matricula'])){
							foreach ($value['matricula'] as $kmatri => $vmatricula) {
								$estados = $this->setEstados($vmatricula);
								if(isset($estados["idgrupoaula"])){
									$cond = array();
									$cond[] = "ga.idgrupoaula = '" . $estados["idgrupoaula"] . "'";
									$cond[] = "gad.idgrupoauladetalle = '" . $estados["idgrupoauladetalle"] . "'";
									$sql = "SELECT ga.*, gad.idcurso,gad.idcomplementario FROM acad_grupoaula ga INNER JOIN acad_grupoauladetalle gad ON (gad.idgrupoaula=ga.idgrupoaula) WHERE " . implode(' AND ', $cond);
									// echo $sql;exit();
									$ga_ = $this->oBD->consultarSQL($sql);
									if(count($ga_) > 0){
										$ga_ = $ga_[0];
										$estados["idcategoria"] = $ga_["idcategoria"];
										// $cond = array();
										// $cond[] = "idgrupoaula = '" . $ga_["idgrupoaula"] . "'";
										// if(is_null($ga_["idcategoria"])){
										// 	$cond[] = "(idcategoria IS NULL OR idcategoria = 0)";
										// }else{
										// 	$cond[] = "idcategoria = '" . $ga_["idcategoria"] . "'";
										// }
										// $cond[] = "idcursoprincipal = '" . $ga_["idcurso"] . "'";
										// $sql = "SELECT * FROM acad_curso_complementario WHERE " . implode(' AND ', $cond);
										// $cc = $this->oBD->consultarSQL($sql);
										if(!empty($ga_['idcomplementario'])){
											$estados["idcomplementario"] = $ga_['idcomplementario'];
										}
										// if(count($cc) > 0){
										// 	$estados["idcomplementario"] = $cc[0]["idcurso"];
										// }
									}
									unset($estados["idgrupoaula"]);
								}
								/*Validar si tiene la matricula*/
								// var_dump($estados);
								
								$aBuscar = array(
									'idgrupoauladetalle'=>$estados['idgrupoauladetalle'],
									'idalumno'=>$idpersona,
									// 'fecha_matricula'=>$estados['fecha_matricula'],
									// 'fecha_vencimiento'=>$estados['fecha_vencimiento'],
									'idcomplementario'=>$estados["idcomplementario"],
									'idcategoria'=>(!empty($estados["idcategoria"]) ? $estados["idcategoria"] : 0),
									'fechaactiva'=>true
								);
								$buscarmat = $this->getmatricula($aBuscar);
								// var_dump($buscarmat);
								if(empty($buscarmat)){
									$sql = "SELECT gad.*,ag.idcategoria AS idnivel,ac.nombre AS nivel,ac.idpadre AS idcarrera,IF(ag.idcategoria IS NOT NULL,(SELECT nombre FROM acad_categorias WHERE idcategoria = ac.idpadre),NULL) AS carrera FROM acad_grupoauladetalle gad LEFT JOIN acad_grupoaula ag ON gad.idgrupoaula = ag.idgrupoaula LEFT JOIN acad_categorias ac ON ag.idcategoria = ac.idcategoria WHERE gad.idgrupoauladetalle = " . $estados['idgrupoauladetalle'];
									$gad_ = $this->oBD->consultarSQL($sql)[0];
									$idmatricula = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatricula) FROM acad_matricula");
									++$idmatricula;
									$estados['idmatricula'] = $idmatricula;
									$estados['idalumno'] = $idpersona;
									$estados['idusuario'] = $idpersona;
									$fecha_inicio = $fecha_custom === true ? $fecha_inicio : new DateTime($gad_["fecha_inicio"]);
									$estados['fecha_matricula'] = $fecha_custom === true ? $fecha_inicio : $fecha_inicio->format("Y-m-d");
									// $estados['fecha_matricula'] = ($estados['tipomatricula'] == '1' ? $fecha_inicio->format("Y-m-d") : $estados['fecha_matricula']);
									$fecha_final = new DateTime($gad_["fecha_final"]);
									$estados['fecha_vencimiento'] = $fecha_custom === true ? $fecha_vencimiento : $fecha_final->format("Y-m-d");
									// $estados['fecha_vencimiento'] = ($estados['tipomatricula'] == '1' ? $fecha_final->format("Y-m-d") : $estados['fecha_vencimiento']);
									if(empty($estados['idcategoria'])){
										unset($estados['idcategoria']);
									}
									$estados["notificar"] = $notificar;
									$this->oBD->insert('acad_matricula', $estados);
									$value['matricula'][$kmatri]['carrera'] = $gad_['carrera'];
									$value['matricula'][$kmatri]['idcarrera'] = $gad_['idcarrera'];
									$value['matricula'][$kmatri]['nivel'] = $gad_['nivel'];
									$value['matricula'][$kmatri]['idnivel'] = $gad_['idnivel'];
									$value['matricula'][$kmatri]["idmatriculaproducto"] = $idmatricula;
								}else{
									$value['matricula'][$kmatri]['signalMatriculaExist'] = true;
								}
							}
						}
						$arr[$key] = $value;
					}
					//end Logica
					$resultado[] = $arr;
				}
			}
			
			$this->terminarTransaccion('dat_multiple_insert');
			return $resultado;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("InsertMultiple")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}

	public function _crearusuariov2($persona,$persona_rol){
		try{
			$resultado = array();
			$this->iniciarTransaccion('dat_multiple_insert');
			//insertar la persona
			//verificar si existe el idpersona
			//check
			

			$rs = $this->oBD->consultarSQL("SELECT * FROM personal pe WHERE pe.dni = {$persona['dni']} AND pe.tipodoc = {$persona['tipodoc']}");
			if(!empty($rs) || !empty($persona['idpersona'])){
				$idpersona = empty($persona['idpersona']) ? end($rs)['idpersona'] : $persona['idpersona'];
			}else{
				$idpersona = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
				++$idpersona;
				$persona['idpersona'] = $idpersona;
				$this->oBD->insert('personal', $persona);

			}

			if(!empty($persona_rol)){
				//insertar los roles
				if(is_array($persona_rol) && !isset($persona_rol[0])){ 
					$persona_rol = array($persona_rol);
				}
				foreach ($persona_rol as $key => $value) {
					//verificar si tiene el rol
					$rs = $this->oBD->consultarSQL("SELECT * FROM persona_rol pr WHERE pr.idpersonal = {$idpersona} AND pr.idempresa = {$value['idempresa']} AND pr.idproyecto = {$value['idproyecto']} AND pr.idrol = {$value['idrol']} ");

					if(empty($rs)){
						$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
						++$idrol;
						$value['iddetalle'] = $idrol;
						$value['idpersonal'] = $idpersona;
						$this->oBD->insert('persona_rol', $value);

					}//end empty rs
				}
			}
			$this->terminarTransaccion('dat_multiple_insert');
			return $resultado;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("_crearusuariov2")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());	
		}
	}

	public function editarusuariov1($persona){
		try{
			$arrPersona = array();
			$this->iniciarTransaccion('dat_multiple_insert');
			//verificar si se necesita buscar el idpersona
			if(empty($persona)){
				throw new Exception("Persona vacio");
			}
			if(!isset($persona[0])){
				$arrPersona[] = $persona;
			}
			foreach ($arrPersona as $key => $value) {
				if(empty($value['idpersona'])){
					if(empty($value['olddni']) && empty($value['oldtipodni'])){
						throw new Exception("Falta olddni y oldtipodni");
					}
					$rs = $this->oBD->consultarSQL("SELECT p.idpersona FROM personal p WHERE p.dni = ".$this->oBD->escapar($value['olddni'])." AND p.tipodoc = ".$this->oBD->escapar($value['oldtipodni']));
					if(!empty($rs)){
						$value['idpersona'] = end($rs)['idpersona'];
					}
					
				}
				if(!empty($value['idpersona'])){
					$idpersona = $value['idpersona'];
					unset($value['idpersona']);
					unset($value['olddni']);
					unset($value['oldtipodni']);
					
					$this->oBD->update('personal', $value, array('idpersona' => $idpersona));
				}
			}
			//editar persona
			$this->terminarTransaccion('dat_multiple_insert');
			return true;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("editarusuariov1")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());	
		}
	}
	public function agregarrolv1($persona_rol){
		try{
			$arrPersonaRol = array();
			$this->iniciarTransaccion('dat_multiple_insert');
			//verificar si se necesita buscar el idpersona
			if(empty($persona_rol)){
				throw new Exception("Persona vacio");
			}
			if(!isset($persona_rol[0])){
				$arrPersonaRol[] = $persona_rol;
			}
			foreach ($arrPersonaRol as $key => $value) {
				if(empty($value['idpersonal'])){
					if(empty($value['dni']) && empty($value['tipodoc'])){
						throw new Exception("Falta dni y tipodoc");
					}
					$rs = $this->oBD->consultarSQL("SELECT p.idpersona FROM personal p WHERE p.dni = ".$this->oBD->escapar($value['dni'])." AND p.tipodoc = ".$this->oBD->escapar($value['tipodoc']));
					if(!empty($rs)){
						$value['idpersonal'] = end($rs)['idpersona'];
					}
				}
				if(empty($value['idpersonal'])){
					throw new Exception("la key: {$key} no tiene idpersonal");
				}
				unset($value['dni']);
				unset($value['tipodoc']);
				$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
				++$idrol;
				$value['iddetalle'] = $idrol;
				//verificar que no existe para agregar
				$rs = $this->oBD->consultarSQL(
					"SELECT * FROM persona_rol p WHERE p.idpersonal = ".$this->oBD->escapar($value['idpersonal'])." AND p.idrol = ".$this->oBD->escapar($value['idrol'])." AND p.idempresa = ".$this->oBD->escapar($value['idempresa'])." AND p.idproyecto = ".$this->oBD->escapar($value['idproyecto'])
				);
				if(empty($rs)){
					$this->oBD->insert('persona_rol', $value);
				}
			}
			//editar persona
			$this->terminarTransaccion('dat_multiple_insert');
			return true;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("agregarrolv1")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());	
		}
	}
	public function eliminarrolv1($persona_rol){
		try{
			$arrPersonaRol = array();
			$this->iniciarTransaccion('dat_multiple_insert');
			//verificar si se necesita buscar el idpersona
			if(empty($persona_rol)){
				throw new Exception("Persona vacio");
			}
			if(!isset($persona_rol[0])){
				$arrPersonaRol[] = $persona_rol;
			}
			foreach ($arrPersonaRol as $key => $value) {
				if(empty($value['iddetalle'])){
					//verificar si tenemos idpersonal
					if(!empty($value['idpersonal']) && !empty($value['idrol'])){
						//buscar iddetalle con el idpersonal
						$rs = $this->oBD->consultarSQL(
							"SELECT * FROM persona_rol p WHERE p.idpersonal = ".$this->oBD->escapar($value['idpersonal'])." AND p.idrol = ".$this->oBD->escapar($value['idrol'])." AND p.idproyecto = ".$this->oBD->escapar($value['idproyecto'])." AND p.idempresa = ".$this->oBD->escapar($value['idempresa'])
						);
						if(!empty($rs)){
							$value['iddetalle'] = end($rs)['iddetalle'];
						}
					}else{
						//buscar idpersonal y luego el id de persona rol
						if(!empty($value['dni']) && !empty($value['tipodoc'])){
							$rs = $this->oBD->consultarSQL("SELECT * FROM personal p WHERE p.dni = ".$this->oBD->escapar($value['dni'])." AND p.tipodoc = ".$this->oBD->escapar($value['tipodoc']));
							if(!empty($rs) && !empty($value['idrol'])){
								//buscar el iddetalle
								$value['idpersonal'] = end($rs)['idpersona'];

								$rs2 = $this->oBD->consultarSQL(
									"SELECT * FROM persona_rol p WHERE p.idpersonal = ".$this->oBD->escapar($value['idpersonal'])." AND p.idrol = ".$this->oBD->escapar($value['idrol'])." AND p.idproyecto = ".$this->oBD->escapar($value['idproyecto'])." AND p.idempresa = ".$this->oBD->escapar($value['idempresa'])
								);
								if(!empty($rs2)){
									$value['iddetalle'] = end($rs2)['iddetalle'];
								}
							}
						}//endif empty dni empty tipodoc
					}
				}
				if(empty($value['iddetalle'])){
					throw new Exception("la key: {$key} no tiene idpersonal");
				}
				
				$this->oBD->delete('persona_rol', array('iddetalle' => $value['iddetalle']));
			}
			//editar persona
			$this->terminarTransaccion('dat_multiple_insert');
			return true;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("editarusuariov1")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());	
		}
	}

	public function _registrarDocente($datos){
		try{
			$this->iniciarTransaccion('dat_multiple_insert');

			// var_dump($datos);exit();
			$resultado = array();

			$personas = isset($datos['personas']) ? json_decode($datos['personas'], true) : null;
            $esdocente = isset($datos['esdocente']) ? $datos['esdocente'] : null;
            $fecha_inicio = isset($datos['fecha_inicio']) ? $datos['fecha_inicio'] : null;
            $fecha_vencimiento = isset($datos['fecha_vencimiento']) ? $datos['fecha_vencimiento'] : null;
            $roles = isset($datos['roles']) ? json_decode($datos['roles'], true) : null;
            $empresaproyecto = isset($datos['empresaproyecto']) ? json_decode($datos['empresaproyecto'], true) : null;
            $tipousuario = isset($datos['tipousuario']) ? $datos['tipousuario'] : null;
			$prefijo = isset($datos['prefijo']) ? $datos['prefijo'] : null;
			$is_prefijo = isset($datos['isprefijo']) ? $datos['isprefijo'] : false;

			$_key_ = 0;

			$info = array();
			$info['esdocente'] = $esdocente;
			$info['fecha_inicio'] = $fecha_inicio;
			$info['fecha_vencimiento'] = $fecha_vencimiento;
			$info['roles'] = $roles;
			$info['empresaproyecto'] = $empresaproyecto;
			$info['tipousuario'] = $tipousuario;
			$data = NegTools::SmartFormatoMultipleDocente($info,$personas,$_key_,$prefijo,$is_prefijo);
			$arr = $data["usuario"];
			// var_dump($arr);exit();
			//Logica
			foreach ($arr as $key => $value) {
				//insertar la tabla personal
				//Antes de ingresar consultar si existe el usuario, de lo contrario resolver la info
				if((int)$_key_ == 0){
					$estados = $this->setEstados($value['personal']);
					$persona = $this->_buscarPersonal(array("dni" => $estados["dni"], "tipodoc" => $estados["tipodoc"]));
					if(count($persona) > 0) {
						$clavesinmd5 = $estados['clavesinmd5'];
						$idpersonasks = $estados['idpersonasks'];
						$instruccion = $estados['instruccion'];
						$value['personal'] = $persona[0];
						$value['personal']['clavesinmd5'] = $clavesinmd5;
						$value['personal']['idpersonasks'] = $idpersonasks;
						$value['personal']['instruccion'] = $instruccion;
						$value['personal']['nuevo'] = 0;
						$idpersona = $value['personal']["idpersona"];
						if(!empty($value['personal_rol'])){
							foreach ($value['personal_rol'] as $irol => $vpersonarol) {
								$estados = $this->setEstados($vpersonarol);
								$estados['idpersonal'] = $idpersona;
								
								$roles = $this->_buscarRol($estados);
								$estados["nuevo"] = 0;
								if(count($roles) == 0){
									$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
									++$idrol;
									$estados['iddetalle'] = $idrol;
									unset($estados["nuevo"]);
									$this->oBD->insert('persona_rol', $estados);
									$estados["nuevo"] = 1;
								}
								$value['personal_rol'][$irol] = $estados;
							}//end foreach personal_rol
							
						}
					} else {
						$idpersona = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
						++$idpersona;
						$estados['idpersona'] = $idpersona;
						if(isset($estados['clavesinmd5'])){
							unset($estados['clavesinmd5']);
						}
						if(isset($estados['idpersonasks'])){
							unset($estados['idpersonasks']);
						}
						if(isset($estados['instruccion'])){
							unset($estados['instruccion']);
						}
						if(isset($estados['edad_registro'])){
							unset($estados['edad_registro']);
						}

						if($is_prefijo == 1){
							$correlativo = $this->oBD->consultarEscalarSQL("SELECT COUNT(*) FROM personal WHERE usuario like '" . $prefijo . "%'");
							++$correlativo;
							// $usu = $prefijo . str_pad($correlativo,12-strlen($prefijo),'0',STR_PAD_LEFT);
							$usu = $prefijo . $correlativo;
							$estados["usuario"] = $usu;
							$estados["clave"] = md5($usu);
						}
						
						$this->oBD->insert('personal', $estados);
						
						$value['personal']['idpersona'] = $idpersona;
						$value['personal']['nuevo'] = 1;
						if($is_prefijo == 1){
							$value['personal']['usuario'] = $usu;
							$value['personal']['clavesinmd5'] = $usu;
							$value['personal']['clave'] = md5($usu);
						}
						//insertar la tabla personal_rol, se necesita el idperona
						foreach ($value['personal_rol'] as $irol => $vpersonarol) {
							$estados = $this->setEstados($vpersonarol);
							$idrol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
							++$idrol;
							$estados['iddetalle'] = $idrol;
							$estados['idpersonal'] = $idpersona;
							
							$this->oBD->insert('persona_rol', $estados);
							$estados["nuevo"] = 1;
							$value['personal_rol'][$irol] = $estados;
						}//end foreach personal_rol
					}
				} else {
					$estados_ = $this->setEstados($value['personal']);
					$filtros = array(
						"tipodoc"=>$estados_['tipodoc'],
						"dni"=>$estados_['dni']
					);
					$idpersona = $this->_buscarPersonal($filtros)[0]["idpersona"];
					$value['personal']['idpersona'] = $idpersona;
				}
				if(!isset($idpersona) || empty($idpersona)){
					throw new Exception("idpersona esta vacio");
				}
				
				$arr[$key] = $value;
			}
			//end Logica
			$resultado[] = $arr;
			
			$this->terminarTransaccion('dat_multiple_insert');
			return $resultado;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');

			throw new Exception("ERROR\n".JrTexto::_("InsertMultiple")." ".JrTexto::_("Api").": " . $e->getMessage()." - ".$e->getLine() . " - " . $e->getFile());
		}
	}
	public function insertgrupoaula($arr){
		try{
			$this->iniciarTransaccion('dat_multiple_grupoaula');
			$ids = array();
			foreach ($arr as $key => $value) {
				$estados = $this->setEstados($value);
				$idgrupoaula = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoaula) FROM acad_grupoaula");
				++$idgrupoaula;
				$estados['idgrupoaula'] = $idgrupoaula;
				$this->oBD->insert('acad_grupoaula', $estados);
				$ids[] = $idgrupoaula;
			}
			
			$this->terminarTransaccion('dat_multiple_grupoaula');
			return $ids;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_insert');
			throw new Exception("ERROR\n".JrTexto::_("insertgrupoaula")." ".JrTexto::_("Api").": " . $e->getMessage()." ".$e->getLine());
		}
	}
	public function insertgrupoauladetalle($arr,$c = null){
		try{
			$this->iniciarTransaccion('dat_multiple_grupoauladetalle');
			$ids = array();
			if(!empty($c)){
				foreach ($arr as $key => $value) {
					$estados = $this->setEstados($value);
					foreach ($c as $kcurso => $vcurso) {
						$idgrupoauladetalle = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoauladetalle) FROM acad_grupoauladetalle");
						++$idgrupoauladetalle;
						$estados['idgrupoauladetalle'] = $idgrupoauladetalle;
						$estados['idcurso'] = $vcurso;
						$this->oBD->insert('acad_grupoauladetalle', $estados);
						$ids[] = $idgrupoauladetalle;
					}//end foreach cursos
				}//end foreach arr
			}else{
				foreach ($arr as $key => $value) {
					$estados = $this->setEstados($value);
					$idgrupoauladetalle = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoauladetalle) FROM acad_grupoauladetalle");
					++$idgrupoauladetalle;
					$estados['idgrupoauladetalle'] = $idgrupoauladetalle;
					$this->oBD->insert('acad_grupoauladetalle', $estados);
					$ids[] = $idgrupoauladetalle;
				}
			}
			$this->terminarTransaccion('dat_multiple_grupoauladetalle');
			return $ids;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_grupoauladetalle');
			throw new Exception("ERROR\n".JrTexto::_("insertgrupoauladetalle")." ".JrTexto::_("Api").": " . $e->getMessage()." ".$e->getLine());
		}
	}
	public function grupoauladetalle($filtros = null){
		try {
			$sql = "SELECT gad.*, ag.nombre AS nombregrupo, ac.nombre AS nombrecurso FROM acad_grupoauladetalle gad INNER JOIN acad_curso ac ON gad.idcurso = ac.idcurso LEFT JOIN acad_grupoaula ag ON gad.idgrupoaula = ag.idgrupoaula";
			
			$cond = array();
			
			if(isset($filtros["idcurso"])) {
				if(is_array($filtros['idcurso'])){
					$cond[] = "gad.idcurso IN (".implode(',', $filtros["idcurso"]).")";
				}else{
					$cond[] = "gad.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
			}
			if(isset($filtros["iddocente"])) {
				if(is_array($filtros['iddocente'])){
					$cond[] = "gad.iddocente IN (" .implode(',', $filtros["iddocente"]).")";
				}else{
					$cond[] = "gad.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
				}
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idgrupoaula"])) {
				$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function locales($filtros = null){
		try {
			$sql = "SELECT l.*, u.descripcion as ugel, (SELECT nombre from general g WHERE l.tipo=g.codigo AND tipo_tabla='tipolocal') as strtipolocal FROM local l LEFT JOIN ugel u on u.idugel=l.idugel";
			
			$cond = array();
			
			if(isset($filtros["idlocal"])) {
				$cond[] = "l.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "l.nombre " . $this->oBD->like($filtros["nombre"]);
			}
			if(isset($filtros["direccion"])) {
				$cond[] = "l.direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["id_ubigeo"])) {
				$cond[] = "l.id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(isset($filtros["tipo"])) {
				$cond[] = "l.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = "l.nombre  " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["vacantes"])) {
				$cond[] = "l.vacantes = " . $this->oBD->escapar($filtros["vacantes"]);
			}
			if(isset($filtros["idugel"])) {
				$cond[] = "l.idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["idproyecto"])) {
				if(is_array($filtros["idproyecto"])){
					$cond[] = "l.idproyecto IN (" . implode(',',$filtros["idproyecto"]).")";
				}else{
					$cond[] = "l.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
				}
			}
				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function crearrepresentante($datos){
		$ruta = [];
		try{
			//*crear el proyecto y empresa
            //*crear el usuario 
            //*crear local con dre y ugel
            //*amarrar cursos al proyecto
			$this->iniciarTransaccion('dat_multiple_crearrepresentante');
			$usuario = $datos['usuario'];
            $idproyecto = (!empty($datos['idproyecto'])) ? $datos['idproyecto'] : 0;
            $idempresa = (!empty($datos['idempresa'])) ? $datos['idempresa'] : 0;
            $cursos = (!empty($datos['cursos'])) ? $datos['cursos'] : array();
            $empresa = $datos['empresa'];

            $idpersona = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
			++$idpersona;

            //crear usuario
            $dat = array(
            	'idpersona' => $idpersona
				,'tipodoc'=>$usuario['tipodocumento']
				,'dni'=>$usuario['dni']
				,'ape_paterno'=>$usuario['primer_ape']
				,'ape_materno'=>$usuario['segundo_ape']
				,'nombre'=>@$usuario['nombres']
				,'fechanac'=>date('Y-m-d')
				,'sexo'=>$usuario['sexo']
				,'estado_civil'=>'S'
				,'ubigeo'=>''
				,'urbanizacion'=>''
				,'direccion'=>@$empresa['direccion']
				,'telefono'=>@$usuario['telefono']
				,'celular'=>@$usuario['telefono']
				,'email'=>@$usuario['email']
				,'regusuario'=>1
				,'regfecha'=>date('Y-m-d')
				,'usuario'=>$usuario['usuario']
				,'clave'=>md5($usuario['clave'])
				,'token'=>md5($usuario['clave'])
				,'rol'=>1
				,'foto'=>''
				,'estado'=>1
				,'situacion'=>'1'
				,'idioma'=>'ES'							
				,'esdemo'=>0
			);
			if(!empty($usuario['idugel'])){ $dat['idugel']=$usuario['idugel']; }
			if(!empty($usuario['idlocal'])){ $dat['idlocal']=$usuario['idlocal']; }

			$this->oBD->insert('personal', $dat);			
			$ruta[]="inserto personal";

            //crear la empresa
            if(empty($idempresa)){
				$idempresa = $this->oBD->consultarEscalarSQL("SELECT MAX(idempresa) FROM bolsa_empresas");
				++$idempresa;
				$dat = array(
					'idempresa' => $idempresa							
					,'nombre'=>@$empresa['nombre']
					,'rason_social'=>@$empresa['razonsocial']
					,'ruc'=>@$empresa['ruc']
					,'logo'=>''
					,'direccion'=>@$empresa['direccion']
					,'telefono'=>@$usuario['telefono']
					,'representante'=>@$usuario['nombres']
					,'usuario'=>@$usuario['usuario']
					,'clave'=>md5($usuario['clave'])
					,'correo'=>@$empresa['email']
					,'estado'=>1
					,'tipo_matricula'=>1
					,'idpersona'=>$idpersona
				);
				$this->oBD->insert('bolsa_empresas', $dat);
				$ruta[]="inserto bolsaempresa";

            }
			//crear el proyecto
			if(empty($idproyecto)){

				$idproyecto = $this->oBD->consultarEscalarSQL("SELECT MAX(idproyecto) FROM proyecto");
				++$idproyecto;

				// $jsonlogin = '{"tipologin":1,"tipofondo":"video","colorfondo":"rgba(20, 92, 186, 0.93)","imagenfondo":"","videofondo":"/static/media/empresa/2019022106044052.mp4","logo1":"static/media/Emp26_Pr24_logo.jpg?id=20200429131400","logo2":"/static/media/empresa/logo-.jpg","colorfondoadmin":"rgb(53, 134, 240)","menus":{"0":{"id":"8","nombre":"Smartcourse","link":"proyecto/cursos/","icono":"fa-book"},"1":{"id":"7","nombre":"Smartacademic","link":"academico/","icono":"fa-graduation-cap"},"2":{"id":"14","nombre":"Projects","link":"proyectos_2","icono":"fa-folder"},"3":{"id":"13","nombre":"Tasks","link":"tareas","icono":"fa-tasks"},"4":{"id":"1","nombre":"Smartquiz","link":"smartquiz/?type=<TYPE>&id=<ID_ALUMNO>&pr=<SLUG_PROYECTO>&u=<USERNAME>&p=<PASSWORD>&idi=<IDIOMA>","icono":"fa-list"},"5":{"id":"10","nombre":"SmartReportes","link":"reportes","icono":"fa-chart-line"},"6":{"id":"20","nombre":"Currículo","link":"#","icono":"fa fa-handshake-o"},"7":{"id":"19","nombre":"Comunidad","link":"","icono":"fa-comments-o"},"17":{"id":"24","nombre":"Familia","link":"","icono":"fa fa-paint-brush"}},"paris":{"navbar":"navbar-dark navbar-info","texto":"accent-navy","menu":"sidebar-light-lightblue","logo":"navbar-lightblue"},"msjbienvenida":"","msjbienvenidatipo":"texto","bloqueodeexamenes":"no","notabloqueoexamenes":""}';
				$jsonlogin = '{"tipologin":1,"tipofondo":"video","colorfondo":"rgba(20, 92, 186, 0.93)","imagenfondo":"","videofondo":"/static/media/empresa/2019022106044052.mp4","logo1":"static/media/Emp26_Pr24_logo.jpg?id=20200429131400","logo2":"/static/media/empresa/logo-.jpg","colorfondoadmin":"rgb(53, 134, 240)","menus":{"0":{"id":"8","nombre":"Smartcourse","link":"proyecto/cursos/","icono":"fa-book"},"1":{"id":"7","nombre":"Smartacademic","link":"academico/","icono":"fa-graduation-cap"},"2":{"id":"14","nombre":"Projects","link":"proyectos_2","icono":"fa-folder"},"3":{"id":"13","nombre":"Tasks","link":"tareas","icono":"fa-tasks"},"4":{"id":"10","nombre":"SmartReportes","link":"reportes","icono":"fa-chart-line"},"5":{"id":"20","nombre":"Currículo","link":"#","icono":"fa fa-handshake-o"},"6":{"id":"19","nombre":"Comunidad","link":"","icono":"fa-comments-o"},"7":{"id":"24","nombre":"Familia","link":"","icono":"fa fa-paint-brush"},"8":{"id":"1","nombre":"Smartquiz","link":"quiz","icono":"fa-list"}},"paris":{"navbar":"navbar-dark navbar-primary","texto":"accent-navy","menu":"sidebar-light-navy","logo":"navbar-light"},"msjbienvenida":"","msjbienvenidatipo":"imagen","bloqueodeexamenes":"no","notabloqueoexamenes":""}';
				$_nombreurl = strtolower(explode(' ', $empresa['nombre'])[0]);
				
				$rs = $this->oBD->consultarSQL("SELECT COUNT(1) AS total FROM proyecto WHERE nombreurl = ".$this->oBD->escapar($_nombreurl));
				if(!empty($rs[0]['total'])){ $_nombreurl = 'EMP000'.$idproyecto; }

				$_tipoempre = (intval($empresa['tipoempresa']) == 1) ? 0 : 1;
				
				$proyecto = array(
					'idproyecto' => $idproyecto							
					,'idempresa'=> $idempresa
					,'jsonlogin'=>@$jsonlogin
					,'fecha'=>date('Y-m-d')
					,'idioma'=>'ES'
					,'nombre'=>@$empresa['nombre']
					,'tipo_empresa'=>$_tipoempre
					,'nombreurl'=>!empty($_nombreurl)?$_nombreurl:'EMP000'.$idproyecto
					,'tipo_portal'=>!empty($empresa['tipo_portal']) ? $empresa['tipo_portal']: 2
				);
				$this->oBD->insert('proyecto', $proyecto);
				$ruta[]="inserto proyecto";
			}//endif

			//crear persona_rol
			$idpersona_rol = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
			++$idpersona_rol;
			$estados = array(
				'iddetalle' => $idpersona_rol
				,'idrol'=>1
				,'idpersonal'=>$idpersona
				,'idproyecto'=>$idproyecto
				,'idempresa'=>$idempresa
			);
			$this->oBD->insert('persona_rol', $estados);
			$ruta[]="inserto el rol";
			// amarrar cursos al proyecto
			if(!empty($datos['cursos'])){
				foreach ($datos['cursos'] as $key => $value) {
					$idproycurso = $this->oBD->consultarEscalarSQL("SELECT MAX(idproycurso) FROM proyecto_cursos");
					++$idproycurso;
					
					$estados = array(
						'idproycurso' => $idproycurso							
						,'idcurso'=>$value['idexterno']
						,'idproyecto'=>$idproyecto
					);
					
					$this->oBD->insert('proyecto_cursos', $estados);
					$ruta[]="inserto el proyecto curso";
					$datos['cursos'][$key]['idproyecto'] = $idproyecto;
					$datos['cursos'][$key]['idempresa'] = $idempresa;
				}//endforeach
			}
			// dre
			$rs = $this->oBD->consultarSQL("SELECT * FROM min_dre WHERE descripcion = ".$this->oBD->escapar($empresa['departamento'])." AND idproyecto = ".$this->oBD->escapar($idproyecto));
			if(!empty($rs)){ 
				$obj = end($rs);
				$iddre = (int)$obj['iddre'];
			}else{
				$iddre = $this->oBD->consultarEscalarSQL("SELECT MAX(iddre) FROM min_dre");
				++$iddre;
				$estados = array(
					'iddre' => $iddre
					,'descripcion'=>$empresa['departamento']
					,'ubigeo'=>'0'
					,'opcional'=>'0'
					,'idproyecto'=>$idproyecto							
				);
				
				$this->oBD->insert('min_dre', $estados);
				$ruta[]="inserto el proyecto dre";
			}
			
			//ugel
			$rs = $this->oBD->consultarSQL("SELECT * FROM ugel WHERE descripcion = ".$this->oBD->escapar($empresa['provincia'])." AND idproyecto = ".$this->oBD->escapar($idproyecto)." AND iddre = ".$this->oBD->escapar($iddre));
			if(!empty($rs)){ 
				$obj = end($rs);
				$idugel = (int)$obj['idugel'];
			}else{
	            $idugel = $this->oBD->consultarEscalarSQL("SELECT MAX(idugel) FROM ugel");
				++$idugel;
				$estados = array(
					'idugel' => $idugel
					,'descripcion'=>$empresa['provincia']
					,'idproyecto'=>$idproyecto
					,'iddre'=>$iddre
				);			
				$this->oBD->insert('ugel', $estados);
				$ruta[]="inserto el proyecto ugel";
			}
			//crear el local central
			$idlocal = $this->oBD->consultarEscalarSQL("SELECT MAX(idlocal) FROM local");
			++$idlocal;

			$_nombre = strtolower(explode(' ', $empresa['nombre'])[0]);
			
			$dat = array(
				'idlocal' => $idlocal							
				,'nombre'=>@$_nombre
				,'direccion'=>@$empresa['direccion']
				,'tipo'=>'C'
				,'vacantes'=>5000
				,'idugel'=>$idugel
				,'idproyecto'=>$idproyecto							
			);			
			$this->oBD->insert('local', $dat);
			$ruta[]="inserto el proyecto local";

			$datos['idproyectoplataforma'] = $idproyecto;
			$datos['idempresaplataforma'] = $idempresa;
			$this->terminarTransaccion('dat_multiple_crearrepresentante');
			return $datos;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_crearrepresentante');
			// var_dump($ruta);
			throw new Exception("ERROR\n".JrTexto::_("crearrepresentante")." ".JrTexto::_("Api").": " . $e->getMessage()." ".$e->getLine());
		}
	}
	public function borrarrepresentante($datos){
		try{
			$this->iniciarTransaccion('dat_multiple_borrarrepresentante');
			foreach ($datos as $key => $value) {
				$this->oBD->delete('personal', array('idpersona' => $value['idpersona']));
				if(!empty($value['persona_rol'])){
					foreach ($value['persona_rol'] as $prkey => $prvalue) {
						$this->oBD->delete('persona_rol', array('iddetalle' => $prvalue['iddetalle']));
					}
				}
				//desvincular en bolsaempresa el idpersona
				$sql = "SELECT idempresa, idpersona FROM bolsa_empresas WHERE idpersona = ".$this->oBD->escapar($value['idpersona']);
				$rs_empresa = $this->oBD->consultarSQL($sql);
				if(!empty($rs_empresa)){
					$rowempresa = end($rs_empresa);
					$this->oBD->update('bolsa_empresas', array('idpersona'=> 1 ), array('idempresa' => $rowempresa['idempresa']));
				}
			}
			$this->terminarTransaccion('dat_multiple_borrarrepresentante');
			return $ids;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_multiple_borrarrepresentante');
			throw new Exception("ERROR\n".JrTexto::_("borrarrepresentante")." ".JrTexto::_("Api").": " . $e->getMessage()." ".$e->getLine());
		}
	}
}