<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-09-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatZoomalumnos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Zoomalumnos") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT  za.idzoomalumno,za.idzoom,za.idalumno,za.fecha,concat(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) as stralumno,pe.email 
					FROM zoomalumnos za INNER JOIN personal pe ON pe.idpersona=za.idalumno";

			$cond = array();

			if (isset($filtros['mysql'])) {
				if ($filtros['mysql'] == 2) { //PARA LISTAR EN EVENTOS (alumno)
					$sql = "SELECT  '2' tipo_evento, z.idpersona, date(z.fecha) fecha, z.urlparticipantes zoom_url,
									z.fecha as start, date_add(z.fecha,interval z.duracion minute) end,
							 		time(z.fecha) hora_comienzo,
									date_add(time(z.fecha),interval z.duracion minute) hora_fin, 
									z.titulo as title, z.agenda as detalle,
									concat(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) autor
							FROM zoom z 
								inner join personal pe on pe.idpersona=z.idpersona
								INNER JOIN zoomalumnos za ON z.idzoom = za.idzoom
							";
					if (isset($filtros["end"]) && isset($filtros["start"])) {
						$cond[] = "z.fecha between " . $this->oBD->escapar($filtros["start"]) . " and " . $this->oBD->escapar($filtros["end"]);
					}
				}
			}

			if (isset($filtros["idzoom"])) {
				$cond[] = "idzoom = " . $this->oBD->escapar($filtros["idzoom"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "za.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql;
			// exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Zoomalumnos") . ": " . $e->getMessage());
		}
	}


	public function insertar($idzoom, $idalumno)
	{
		try {
			
			$this->iniciarTransaccion('dat_zoomalumnos_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idzoomalumno) FROM zoomalumnos");
			++$id;

			$estados = array(
				'idzoomalumno' => $id, 'idzoom' => $idzoom, 'idalumno' => $idalumno
			);

			$this->oBD->insert('zoomalumnos', $estados);
			$this->terminarTransaccion('dat_zoomalumnos_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_zoomalumnos_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Zoomalumnos") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idzoom, $idalumno)
	{
		try {
			$this->iniciarTransaccion('dat_zoomalumnos_update');
			$estados = array(
				'idzoom' => $idzoom, 'idalumno' => $idalumno
			);

			$this->oBD->update('zoomalumnos ', $estados, array('idzoomalumno' => $id));
			$this->terminarTransaccion('dat_zoomalumnos_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Zoomalumnos") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idzoomalumno,idzoom,idalumno,fecha  FROM zoomalumnos  "
				. " WHERE idzoomalumno = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Zoomalumnos") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('zoomalumnos', array('idzoomalumno' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Zoomalumnos") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('zoomalumnos', array($propiedad => $valor), array('idzoomalumno' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Zoomalumnos") . ": " . $e->getMessage());
		}
	}
}
