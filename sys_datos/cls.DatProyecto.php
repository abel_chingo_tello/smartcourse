<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-04-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatProyecto extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}

	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM proyecto";
			$cond = array();
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idempresa"])) {
				$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if (isset($filtros["jsonlogin"])) {
				$cond[] = "jsonlogin = " . $this->oBD->escapar($filtros["jsonlogin"]);
			}
			if (isset($filtros["fecha"])) {
				$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if (isset($filtros["idioma"])) {
				$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["tipo_empresa"])) {
				$cond[] = "tipo_empresa = " . $this->oBD->escapar($filtros["tipo_empresa"]);
			}
			if (isset($filtros["nombreurl"])) {
				$cond[] = "nombreurl = " . $this->oBD->escapar($filtros["nombreurl"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT p.idproyecto, p.idempresa, p.jsonlogin, p.fecha, p.idioma, p.nombre, p.tipo_empresa, p.logo, p.nombreurl, p.tipo_portal,p.solocursosprincipales,p.correo_servidor,p.correo_puerto,p.correo_email,p.correo_clave ,  rason_social,ruc,e.nombre as empresa,e.logo as logoempresa,e.direccion,e.telefono,representante,e.correo as correoempresa,e.idpersona as idrepresentante FROM proyecto p INNER JOIN bolsa_empresas e ON p.idempresa=e.idempresa";

			$cond = array();
			if (isset($filtros["idproyecto"])) {
				if (is_array($filtros["idproyecto"])) {
					$cond[] = "p.idproyecto IN (" . implode(',', $filtros["idproyecto"]) . ")";
				} else {
					$cond[] = "p.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
				}
			}

			if (isset($filtros["idempresa"])) {
				if (is_array($filtros['idempresa'])) {
					$cond[] = "p.idempresa IN (" . implode(',', $filtros["idempresa"]) . ")";
				} else {
					$cond[] = "p.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
				}
			}
			if (isset($filtros["jsonlogin"])) {
				$cond[] = "jsonlogin = " . $this->oBD->escapar($filtros["jsonlogin"]);
			}
			if (isset($filtros["fecha"])) {
				$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if (isset($filtros["idioma"])) {
				$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "p.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["tipo_empresa"])) {
				$cond[] = "tipo_empresa = " . $this->oBD->escapar($filtros["tipo_empresa"]);
			}
			if (isset($filtros["tipo_portal"])) {
				$cond[] = "tipo_portal = " . $this->oBD->escapar($filtros["tipo_portal"]);
			}
			if (isset($filtros["logo"])) {
				$cond[] = "logo = " . $this->oBD->escapar($filtros["logo"]);
			}
			if (isset($filtros["nombreurl"])) {
				$cond[] = "nombreurl = " . $this->oBD->escapar($filtros["nombreurl"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "p.nombre " . $this->oBD->like($filtros["texto"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "p.estado =" . $this->oBD->escape($filtros["estado"]);
			} else $cond[] = "estado =1 ";
			if (isset($filtros["sesionlogin"])) {
				if(is_numeric($filtros["sesionlogin"])){
					$cond[] = " ( nombreurl =" . $this->oBD->escapar($filtros["sesionlogin"]) . " OR idproyecto= " . $this->oBD->escapar($filtros["sesionlogin"]) . ") ";	
				}else{
					$cond[] = " ( nombreurl =" . $this->oBD->escapar($filtros["sesionlogin"]) .")";	
				}	
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY p.nombre ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}

	public function identificar($filtros = null)
	{
		try {
			$sql = "SELECT idproyecto, nombreurl, logo
					FROM proyecto";

			$cond = array();
			if (isset($filtros["idproyecto"])) {
				if (is_array($filtros["idproyecto"])) {
					$cond[] = "idproyecto IN (" . implode(',', $filtros["idproyecto"]) . ")";
				} else {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
				}
			}

			if (isset($filtros["idempresa"])) {
				if (is_array($filtros['idempresa'])) {
					$cond[] = "idempresa IN (" . implode(',', $filtros["idempresa"]) . ")";
				} else {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
				}
			}
					if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY idproyecto ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}
	public function insertar($idempresa, $jsonlogin, $fecha, $idioma, $nombre, $tipo_empresa, $nombreurl, $logo, $tipo_portal = 2)
	{
		try {

			$this->iniciarTransaccion('dat_proyecto_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idproyecto) FROM proyecto");
			++$id;

			$estados = array(
				'idproyecto' => $id, 'idempresa' => $idempresa, 'jsonlogin' => $jsonlogin, 'fecha' => !empty($fecha) ? $fecha : date('Y-m-d'), 'idioma' => $idioma, 'nombre' => $nombre, 'tipo_empresa' => !empty($tipo_empresa) ? $tipo_empresa : 0, 'nombreurl' => !empty($nombreurl) ? $nombreurl : 'EMP000' . $id, 'logo' => $logo, 'tipo_portal' => $tipo_portal
			);

			$this->oBD->insert('proyecto', $estados);
			$this->terminarTransaccion('dat_proyecto_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_proyecto_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idempresa, $jsonlogin, $fecha, $idioma, $nombre, $tipo_empresa, $nombreurl, $logo, $tipo_portal = 2)
	{
		try {
			$this->iniciarTransaccion('dat_proyecto_update');
			$estados = array(
				'idempresa' => $idempresa, 'jsonlogin' => $jsonlogin
				//,'fecha'=>$fecha
				, 'idioma' => $idioma, 'nombre' => $nombre, 'tipo_empresa' => !empty($tipo_empresa) ? $tipo_empresa : 0, 'nombreurl' => !empty($nombreurl) ? $nombreurl : 'EMP000' . $id, 'logo' => $logo, 'tipo_portal' => $tipo_portal
			);

			$this->oBD->update('proyecto ', $estados, array('idproyecto' => $id));
			$this->terminarTransaccion('dat_proyecto_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}

	public function actulizarcampos($id,$newcampos=array()){
		try {
			$campos=array('idproyecto','idempresa','jsonlogin','fecha','idioma','nombre','tipo_empresa','logo','nombreurl','tipo_portal','solocursosprincipales','correo_servidor','correo_puerto','correo_email','correo_clave');
			$estados=array();
			if(!empty($campos))
			foreach ($campos as $k => $v){
				if(in_array($k,$campos))
					$estados[$k]=$v;
				else 
					throw new Exception("ERROR\n".JrTexto::_("Update")." campo ".$k." no existe ");
			}
			if(!empty($estados))
				$this->oBD->update('proyecto ', $estados, array('idproyecto' => $id));
			return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  proyecto.idproyecto, proyecto.idempresa, proyecto.jsonlogin, proyecto.fecha, proyecto.idioma, proyecto.nombre, proyecto.tipo_empresa, proyecto.logo, proyecto.nombreurl, proyecto.tipo_portal,solocursosprincipales,correo_servidor,correo_puerto,correo_email,correo_clave  FROM proyecto  "
				. " WHERE idproyecto = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('proyecto', array('idproyecto' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('proyecto', array($propiedad => $valor), array('idproyecto' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Proyecto") . ": " . $e->getMessage());
		}
	}
}
