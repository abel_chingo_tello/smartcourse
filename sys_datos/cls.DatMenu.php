<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-11-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatMenu extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Menu") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idmenu,nombre,descripcion,estado,url,icono,usuario_registro,fecha_registro FROM menu";
			$cond = array();
			
			if(isset($filtros["idmenu"])) {
					$cond[] = "idmenu = " . $this->oBD->escapar($filtros["idmenu"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "estado >=0 ";}
			
			if(isset($filtros["url"])) {
					$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if(isset($filtros["icono"])) {
					$cond[] = "icono = " . $this->oBD->escapar($filtros["icono"]);
			}
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}

			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM menu";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}

	public function insertar($nombre,$descripcion,$estado,$url,$icono,$usuario_registro)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idmodulo+1 FROM modulos ORDER BY idmodulo DESC limit 0,1 ");			
			$estados = array('nombre'=>$nombre							
							,'estado'=>!empty($estado)?$estado:1
							,'url'=>$url
							,'icono'=>$icono
							,'usuario_registro'=>$usuario_registro							
							);	
			if(!empty($descripcion)) $estados["descripcion"]=$descripcion;		
			return $this->oBD->insert('menu', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_modulos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$descripcion,$estado,$url,$icono,$usuario_registro)
	{
		try {			
			$estados = array('nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'estado'=>$estado
							,'url'=>$url
							,'icono'=>$icono
							,'usuario_registro'=>$usuario_registro								
							);
			$this->oBD->update('menu ', $estados, array('idmenu' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}

	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('menu', array('idmenu' => $id));
			else 
				return $this->oBD->update('menu', array('estado' => -1), array('idmenu' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('menu', array($propiedad => $valor), array('idmenu' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Menu") . ": " . $e->getMessage());
		}
	}
}
