<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-12-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatRecursos_colaborativos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Recursos_colaborativos").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idrecurso,idproyecto,titulo,descripcion,file,tipo,estado,idusuario,fecha_creacion,ngrupos,temasxgrupo FROM recursos_colaborativos re ";
			$cond = array();
			if(isset($filtros["sqlconuser"])){
				$sql="SELECT idrecurso,idproyecto,titulo,descripcion,file,tipo,re.estado,idusuario,fecha_creacion,ngrupos,temasxgrupo, concat(pe.ape_paterno,' ',pe.ape_materno,', ', pe.nombre) as struser FROM recursos_colaborativos re LEFT join personal pe ON re.idusuario=pe.idpersona ";
			}
			
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["file"])) {
					$cond[] = "file = " . $this->oBD->escapar($filtros["file"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "re.estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "re.estado >=0 ";}
			
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["fecha_creacion"])) {
					$cond[] = "fecha_creacion = " . $this->oBD->escapar($filtros["fecha_creacion"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM recursos_colaborativos";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Recursos_colaborativos").": " . $e->getMessage());
		}
	}
		
	public function insertar($idproyecto,$titulo,$descripcion,$file,$tipo,$estado,$idusuario)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idrecurso+1 FROM recursos_colaborativos ORDER BY idrecurso DESC limit 0,1 ");			
			$estados = array('idproyecto'=>$idproyecto
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'file'=>$file
							,'tipo'=>$tipo
							,'estado'=>$estado
							,'idusuario'=>$idusuario							
							);			
			return $this->oBD->insertAI('recursos_colaborativos', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_recursos_colaborativos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Recursos_colaborativos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idproyecto,$titulo,$descripcion,$file,$tipo,$estado,$idusuario)
	{
		try {			
			$estados = array('idproyecto'=>$idproyecto
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'file'=>$file
							,'tipo'=>$tipo
							,'estado'=>$estado
							,'idusuario'=>$idusuario
							);
			$this->oBD->update('recursos_colaborativos ', $estados, array('idrecurso' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colaborativos").": " . $e->getMessage());
		}
	}

	public function actualizar2($estados,$condicion){
		try {
			return $this->oBD->update('recursos_colaborativos ', $estados, $condicion);		    
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('recursos_colaborativos', array('idrecurso' => $id));
			else 
				return $this->oBD->update('recursos_colaborativos', array('estado' => -1), array('idrecurso' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Recursos_colaborativos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('recursos_colaborativos', array($propiedad => $valor), array('idrecurso' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colaborativos").": " . $e->getMessage());
		}
	}  	 
		
}