<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-05-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatPortafolio_archivo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Portafolio_archivo") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT portafolio_archivo.idarchivo, portafolio_archivo.idfolder, portafolio_archivo.nombre, portafolio_archivo.ubicacion, portafolio_archivo.tipo, portafolio_archivo.extension, portafolio_archivo.idportafolio, 'file' as kind FROM portafolio_archivo";

			$cond = array();


			if (isset($filtros["idarchivo"])) {
				$cond[] = "idarchivo = " . $this->oBD->escapar($filtros["idarchivo"]);
			}
			if (isset($filtros["idfolder"])) {
				$cond[] = "idfolder = " . $this->oBD->escapar($filtros["idfolder"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["ubicacion"])) {
				$cond[] = "ubicacion = " . $this->oBD->escapar($filtros["ubicacion"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["extension"])) {
				$cond[] = "extension = " . $this->oBD->escapar($filtros["extension"]);
			}
			if (isset($filtros["idportafolio"])) {
				$cond[] = "idportafolio = " . $this->oBD->escapar($filtros["idportafolio"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";
			// echo "$sql";exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Portafolio_archivo") . ": " . $e->getMessage());
		}
	}


	public function insertar($idfolder, $nombre, $ubicacion, $tipo, $extension, $idportafolio)
	{
		try {

			$this->iniciarTransaccion('dat_portafolio_archivo_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idarchivo) FROM portafolio_archivo");
			++$id;

			$estados = array(
				'idarchivo' => $id, 'idfolder' => $idfolder, 'nombre' => $nombre, 'ubicacion' => $ubicacion, 'tipo' => $tipo, 'extension' => $extension, 'idportafolio' => $idportafolio
			);

			$this->oBD->insert('portafolio_archivo', $estados);
			$this->terminarTransaccion('dat_portafolio_archivo_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_portafolio_archivo_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Portafolio_archivo") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idfolder, $nombre, $ubicacion, $tipo, $extension, $idportafolio)
	{
		try {
			$this->iniciarTransaccion('dat_portafolio_archivo_update');
			$estados = array(
				'idfolder' => $idfolder, 'nombre' => $nombre, 'ubicacion' => $ubicacion, 'tipo' => $tipo, 'extension' => $extension, 'idportafolio' => $idportafolio
			);

			$this->oBD->update('portafolio_archivo ', $estados, array('idarchivo' => $id));
			$this->terminarTransaccion('dat_portafolio_archivo_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Portafolio_archivo") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  portafolio_archivo.idarchivo, portafolio_archivo.idfolder, portafolio_archivo.nombre, portafolio_archivo.ubicacion, portafolio_archivo.tipo, portafolio_archivo.extension, portafolio_archivo.idportafolio  FROM portafolio_archivo  "
				. " WHERE idarchivo = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Portafolio_archivo") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('portafolio_archivo', array('idarchivo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Portafolio_archivo") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('portafolio_archivo', array($propiedad => $valor), array('idarchivo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Portafolio_archivo") . ": " . $e->getMessage());
		}
	}
}
