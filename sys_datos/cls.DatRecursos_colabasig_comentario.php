<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-01-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatRecursos_colabasig_comentario extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Recursos_colabasig_comentario").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idrecursocomentario,rc.idasignacion,rc.idpadre,rc.idpersona as idalumno ,CONCAT(ape_paterno,' ',ape_materno,', ',nombre) AS stralumno,rc.comentario,rc.fecha,rc.file,
				(select count(idrecursocomentario) from  recursos_colabasig_comentario rr WHERE rr.idpadre=rc.idrecursocomentario) as numero_mensaje
			  FROM recursos_colabasig_comentario rc INNER JOIN personal al on rc.idpersona=al.idpersona";			
			
			$cond = array();		
			if(isset($filtros["idrecursocomentario"])) {
					$cond[] = "rc.idrecursocomentario = " . $this->oBD->escapar($filtros["idrecursocomentario"]);
			}		
			
			if(isset($filtros["idasignacion"])) {
					$cond[] = "rc.idasignacion = " . $this->oBD->escapar($filtros["idasignacion"]);
			}
			if(!empty($filtros["idpadre"])) {
				if($filtros["idpadre"]=='null') $cond[] = "rc.idpadre IS NULL";
				else $cond[] = "rc.idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "rc.idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "rc.comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "rc.fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(isset($filtros["file"])) {
					$cond[] = "rc.file = " . $this->oBD->escapar($filtros["file"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			//var_dump($filtros);

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//var_dump($sql);
			
			//$sql .= " ORDER BY fecha_creado ASC";
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;

		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Recursos_colabasig_comentario").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idasignacion,$idpadre,$idpersona,$comentario,$fecha,$file)
	{
		try {
			
			//$this->iniciarTransaccion('dat_recursos_colabasig_comentario_insert');
					
			
			$estados = array(//'idrecursocomentario' => $id
							'idpadre' => !empty($idpadre)?$idpadre:'null'
							,'idasignacion'=>$idasignacion
							,'idpersona'=>$idpersona
							,'comentario'=>$comentario
							,'fecha'=>$fecha
							,'file'=>!empty($file)?$file:'null'							
							);
			
			return $this->oBD->insertAI('recursos_colabasig_comentario', $estados,true);			
			//$this->terminarTransaccion('dat_recursos_colabasig_comentario_insert');			
			//return $id;

		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_recursos_colabasig_comentario_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Recursos_colabasig_comentario").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idasignacion,$idpadre,$idpersona,$comentario,$fecha,$file)
	{
		try {
			//$this->iniciarTransaccion('dat_recursos_colabasig_comentario_update');
			$estados = array('idasignacion'=>$idasignacion
							,'idpadre' => !empty($idpadre)?$idpadre:'null'
							,'idpersona'=>$idpersona
							,'comentario'=>$comentario
							,'fecha'=>$fecha
							,'file'=>!empty($file)?$file:'null'							
							);
			
			$this->oBD->update('recursos_colabasig_comentario ', $estados, array('idrecursocomentario' => $id));
		   // $this->terminarTransaccion('dat_recursos_colabasig_comentario_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colabasig_comentario").": " . $e->getMessage());
		}
	}

	public function actualizar2($estados,$condicion){
		try {
			return $this->oBD->update('recursos_colabasig_comentario ', $estados, $condicion);		    
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}


	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM recursos_colabasig_comentario  "
					. " WHERE idrecursocomentario = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Recursos_colabasig_comentario").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('recursos_colabasig_comentario', array('idrecursocomentario' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Recursos_colabasig_comentario").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('recursos_colabasig_comentario', array($propiedad => $valor), array('idrecursocomentario' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colabasig_comentario").": " . $e->getMessage());
		}
	}
   
		
}