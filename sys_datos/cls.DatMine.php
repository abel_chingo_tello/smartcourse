<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatMine extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function set_($id, $propiedad, $valor, $tabla)
	{
		try {
			return $this->oBD->update($tabla, array($propiedad => $valor), array('idcurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}
	// public function identificarCurso($params)
	// {
	// 	try {
	// 		$sql = "
	// 				select acad_grupoauladetalle.idgrupoauladetalle, ifnull(acad_curso_complementario.idcurso,0) idcomplementario, ifnull(acad_curso.idcurso,0) idcursoprincipal
	// 				from acad_grupoauladetalle
	// 					left join acad_curso on acad_grupoauladetalle.idcurso=acad_curso.idcurso
	// 					left join acad_curso_complementario on acad_grupoauladetalle.idcomplementario=acad_curso_complementario.idcurso
	// 			";
	// 		$cond = array();
	// 		if (isset($params["idcursodetalle"])) {
	// 			$cond[] = "acad_grupoauladetalle.idcursodetalle = " . $this->oBD->escapar($params["idcursodetalle"]);
	// 		}
	// 		if (isset($params["idcurso"])) {
	// 			$cond[] = "idcurso = " . $this->oBD->escapar($params["idcurso"]);
	// 		}

	// 		if (!empty($cond)) {
	// 			$sql .= " WHERE " . implode(' AND ', $cond);
	// 		}
	// 		//echo $sql;		
	// 		if (!empty($params["neworden"])) {
	// 			$sql .= " ORDER BY cd.idpadre ASC, cd.orden ASC ";
	// 		} else if (!empty($params["orderBy"]))
	// 			$sql .= " ORDER BY cd.orden ASC , " . $params["orderBy"];
	// 		else
	// 			$sql .= " ORDER BY cd.orden ASC ";
	// 		return $this->oBD->consultarSQL($sql);
	// 	} catch (Exception $e) {
	// 		throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Mine") . ": " . $e->getMessage());
	// 	}
	// }
	public function getData($params = array("tabla" => null, "arrCampos" => null, "arrFiltros" => null))
	{
		try {
			$sql = "SELECT "; //from acad_curso c";
			// echo json_encode($params);exit();
			if (isset($params["arrCampos"])) {
				$sql .= " " . implode(' , ', $params["arrCampos"]) . " ";
			} else {
				$sql .= " * ";
			}
			if (isset($params["tabla"])) {
				$sql .= " FROM " . $params["tabla"] . " ";
			}
			if (isset($params["arrFiltros"])) {
				$sql .= " WHERE ";
				foreach ($params["arrFiltros"] as $key => $value) {
					end($params["arrFiltros"]);
					if ($key === key($params["arrFiltros"])) {
						$sql .= " $key = " . $this->oBD->escapar($value) . "  ";
					} else {
						$sql .= " $key = " . $this->oBD->escapar($value) . " AND ";
					}
				}
			}
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Mine") . ": " . $e->getMessage());
		}
	}
	public function updateData($params = array("tabla" => null, "arrUpdate" => null, "arrFiltros" => null))
	{
		try {
			$this->iniciarTransaccion('dat_mine_update');
			$sql = "UPDATE "; //from acad_curso c";
			if (isset($params["tabla"])) {
				$sql .= $params["tabla"] . " ";
			}
			$sql .= " SET ";
			foreach ($params["arrUpdate"] as $key => $value) {
				end($params["arrUpdate"]);
				if ($key === key($params["arrUpdate"])) {
					$sql .= " $key = " . $this->oBD->escapar($value) . "  ";
				} else {
					$sql .= " $key = " . $this->oBD->escapar($value) . " , ";
				}
			}
			$sql .= " WHERE ";
			foreach ($params["arrFiltros"] as $key => $value) {
				end($params["arrFiltros"]);
				if ($key === key($params["arrFiltros"])) {
					$sql .= " $key = " . $this->oBD->escapar($value) . "  ";
				} else {
					$sql .= " $key = " . $this->oBD->escapar($value) . " AND ";
				}
			}
			$this->oBD->ejecutarSQL($sql);
			$this->terminarTransaccion('dat_mine_update');
			return "ok";
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_mine_update');
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Mine") . ": " . $e->getMessage());
		}
	}
}
