<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-12-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatResources extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Resources").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM recursos";
			
			$cond = array();		
			
			if(!empty($filtros["id"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["id"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}	
			
			if(!empty($filtros["tool"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tool"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Herramientas").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idrecurso,idnivel,idunidad,idactividad,idpersonal,titulo,texto,caratula,orden,tipo,publicado,compartido  FROM recursos";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["id"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY orden ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Herramientas").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT idrecurso,idnivel,idunidad,idactividad,idpersonal,titulo,texto,caratula,orden,tipo,publicado,compartido   FROM recursos  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Herramientas").": " . $e->getMessage());
		}
	}
	
	public function insertar($idnivel,$idunidad,$idactividad,$idpersonal,$titulo,$texto,$caratula,$orden,$tipo,$publicado=0,$compartido=0)
	{
		try {
			
			$this->iniciarTransaccion('dat_recursos_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrecurso) FROM recursos");
			++$id;			
			$estados = array('idrecurso' => $id							
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'idpersonal'=>$idpersonal
							,'titulo'=>$titulo
							,'texto'=>$texto
							,'caratula'=>$caratula
							,'orden'=>$orden
							,'tipo'=>$tipo
							,'publicado'=>$publicado
							,'compartido'=>$compartido						
							);
			$this->oBD->insert('recursos', $estados);			
			$this->terminarTransaccion('dat_recursos_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_recursos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Recursos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel,$idunidad,$idactividad,$idpersonal,$titulo,$texto,$caratula,$orden,$tipo,$publicado=0,$compartido=0)
	{
		try {			
			$this->iniciarTransaccion('dat_recurso_update');
			$estados = array('idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'idpersonal'=>$idpersonal
							,'titulo'=>$titulo
							,'texto'=>$texto
							,'caratula'=>$caratula
							,'orden'=>$orden
							,'tipo'=>$tipo
							,'publicado'=>$publicado
							,'compartido'=>$compartido
							);
			
			$this->oBD->update('recursos', $estados, array('idrecurso' => $id));
		    $this->terminarTransaccion('dat_recurso_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("recursos").": " . $e->getMessage());
		}
	}
	
	public function get($id)
	{
		try {
			$sql = "SELECT idrecurso,idnivel,idunidad,idactividad,idpersonal,titulo,texto,caratula,orden,tipo,publicado,compartido  FROM recursos  "
					. " WHERE idrecurso = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Resources").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('recursos', array('idrecurso' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Resources").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('recursos', array($propiedad => $valor), array('idrecurso' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Resources").": " . $e->getMessage());
		}
	}

	public function setCampo($filtro, $propiedad, $valor)
	{
		try {
			if(!empty($filtros["id"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["id"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			

			$this->oBD->update('recursos', array($propiedad => $valor), $filtro);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Resources").": " . $e->getMessage());
		}
	}
   
		
}