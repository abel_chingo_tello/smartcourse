<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-01-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatPersona_metas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Persona_metas") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(idmeta) FROM persona_metas";

			$cond = array();

			if (isset($filtros["idmeta"])) {
				$cond[] = "idmeta = " . $this->oBD->escapar($filtros["idmeta"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if (isset($filtros["meta"])) {
				$cond[] = "meta = " . $this->oBD->escapar($filtros["meta"]);
			}
			if (isset($filtros["objetivo"])) {
				$cond[] = "objetivo = " . $this->oBD->escapar($filtros["objetivo"]);
			}
			if (isset($filtros["mostrar"])) {
				$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Persona_metas") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT pe.idmeta, pe.idpersona, pe.meta, pe.objetivo, pe.mostrar FROM persona_metas pe LEFT JOIN personal pl on pe.idpersona=pl.idpersona";

			$cond = array();
			if (isset($filtros["idmeta"])) {
				$cond[] = "idmeta = " . $this->oBD->escapar($filtros["idmeta"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "(pe.idpersona = " . $this->oBD->escapar($filtros["idpersona"]) . " OR md5(pe.idpersona)=" . $this->oBD->escapar($filtros["idpersona"]) . ")";
			}
			if (isset($filtros["meta"])) {
				$cond[] = "meta = " . $this->oBD->escapar($filtros["meta"]);
			}
			if (isset($filtros["objetivo"])) {
				$cond[] = "objetivo = " . $this->oBD->escapar($filtros["objetivo"]);
			}
			if (isset($filtros["mostrar"])) {
				$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Persona_metas") . ": " . $e->getMessage());
		}
	}


	public function insertar($idpersona, $meta, $objetivo, $mostrar)
	{
		try {

			$this->iniciarTransaccion('dat_persona_metas_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmeta) FROM persona_metas");
			++$id;

			$estados = array(
				'idmeta' => $id, 'idpersona' => $idpersona, 'meta' => $meta, 'objetivo' => $objetivo, 'mostrar' => $mostrar
			);

			$this->oBD->insert('persona_metas', $estados);
			$this->terminarTransaccion('dat_persona_metas_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_persona_metas_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Persona_metas") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona, $meta, $objetivo, $mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_persona_metas_update');
			$estados = array(
				'idpersona' => $idpersona, 'meta' => $meta, 'objetivo' => $objetivo, 'mostrar' => $mostrar
			);

			$this->oBD->update('persona_metas ', $estados, array('idmeta' => $id));
			$this->terminarTransaccion('dat_persona_metas_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Persona_metas") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT pe.idmeta, pe.idpersona, pe.meta, pe.objetivo, pe.mostrar FROM persona_metas pe LEFT JOIN personal pl on pe.idpersona=pl.idpersona WHERE idmeta = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Persona_metas") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_metas', array('idmeta' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Persona_metas") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('persona_metas', array($propiedad => $valor), array('idmeta' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Persona_metas") . ": " . $e->getMessage());
		}
	}
}
