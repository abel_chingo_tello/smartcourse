<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-11-2016  
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
class DatReportealumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Level") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(idnivel) FROM niveles";

			$cond = array();
			if (!empty($filtros["idnivel"])) {
				$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if (!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (!empty($filtros["idpadre"])) {
				$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {

			if ($filtros["tipo"] == 'tiempo_pv') {

				$sql = "SELECT sum(TIME_TO_SEC(TIMEDIFF(fechasalida, fechaentrada))) as tiempo
				FROM historial_sesion";

				$cond = array();

				if (isset($filtros['cond']['idproyecto'])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros['cond']['idproyecto']);
				}
				if (isset($filtros['cond']['lugar'])) {
					$cond[] = "lugar = " . $this->oBD->escapar($filtros['cond']['lugar']);
				}
				if (isset($filtros['cond']['idusuario'])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros['cond']['idusuario']);
				}
				if (isset($filtros['cond']['idcurso'])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros['cond']['idcurso']);
				}
				if (isset($filtros['cond']['hastafecha'])) {
					$cond[] = "fechasalida < " . $this->oBD->escapar($filtros['cond']['hastafecha']);
				}
				if (!empty($cond)) {
					$sql .= " WHERE fechaentrada is not null and fechasalida is not null AND " . implode(' AND ', $cond);
				}
				//revisar ejercito
				$sqladd = !empty($filtros['idproyecto']) ? (" AND idproyecto='" . $filtros['idproyecto'] . "'") : '';
				$sql = $sql . $sqladd;
				// echo $sql;

			} elseif ($filtros["tipo"] == 'tiempo_curso') {

				$sql = "SELECT c.nombre,sum(TIME_TO_SEC(TIMEDIFF(h.fechasalida, h.fechaentrada))) as tiempo
				FROM historial_sesion h inner join acad_curso c on c.idcurso=h.idcurso
				WHERE h.idusuario='$filtros[idusuario]' and h.lugar='A' and h.fechaentrada is not null and h.fechasalida is not null
				group by c.nombre";
			} elseif ($filtros["tipo"] == 'examen_e') {

				$sql = "SELECT c.nombre,n.nota, n.idrecurso
				FROM acad_curso c INNER JOIN acad_cursodetalle d ON c.idcurso = d.idcurso INNER JOIN notas_quiz n ON n.idrecurso = d.idrecurso INNER JOIN recursos r ON r.idrecurso = n.idrecurso";

				$cond = array();

				if (isset($filtros['cond']['idusuario'])) {
					$cond[] = "n.idalumno = " . $this->oBD->escapar($filtros['cond']['idusuario']);
				}
				if (isset($filtros['cond']['tiporecurso'])) {
					$cond[] = "d.tiporecurso = " . $this->oBD->escapar($filtros['cond']['tiporecurso']);
				}
				if (isset($filtros['cond']['idcurso'])) {
					$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros['cond']['idcurso']);
				}
				if (isset($filtros['cond']['idunidad'])) {
					$cond[] = "r.idunidad = " . $this->oBD->escapar($filtros['cond']['idunidad']);
				}

				if (!empty($cond)) {
					$sql .= " WHERE " . implode(' AND ', $cond);
				}

				//WHERE n.idalumno='$filtros[idusuario]' and n.tipo='E'";
				// echo $sql;

			} elseif ($filtros["tipo"] == 'examen_s') {

				$sql = "SELECT c.nombre,n.nota
				FROM notas_quiz n inner join acad_cursodetalle d on d.idrecurso=n.idrecurso
				inner join acad_curso c on c.idcurso=d.idcurso
				WHERE n.idalumno='$filtros[idusuario]' and n.tipo='S'";
			} elseif ($filtros["tipo"] == 'productividad_curso') {

				$sql = "SELECT c.idcurso,c.nombre
				FROM notas_quiz n inner join acad_cursodetalle d on d.idrecurso=n.idrecurso
				inner join acad_curso c on c.idcurso=d.idcurso WHERE n.idalumno='$filtros[idusuario]'
				group by c.idcurso,c.nombre";
			} elseif ($filtros["tipo"] == 'productividad_nota') {

				$sql = "SELECT n.*
				FROM notas_quiz n inner join acad_cursodetalle d on d.idrecurso=n.idrecurso
				inner join acad_curso c on c.idcurso=d.idcurso
				WHERE n.idalumno='$filtros[idusuario]' and c.idcurso=$filtros[idcurso] and n.tipo='$filtros[tipo2]'";
			} elseif ($filtros["tipo"] == 'progreso_niveles') {

				$sql = "SELECT a.sesion,n.nombre,sum(aa.porcentajeprogreso) as porcentaje,count(ad.iddetalle) as cantidad
				FROM ((actividad_alumno aa inner join actividad_detalle ad on ad.iddetalle=aa.iddetalleactividad)
				inner join actividades a on a.idactividad=ad.idactividad)
				inner join niveles n on n.idnivel=a.nivel
				WHERE aa.idalumno='$filtros[idusuario]'
				group by a.sesion,n.nombre";
			} elseif ($filtros["tipo"] == 'progreso_unidades') {

				$sql = "SELECT a.sesion,n.nombre,sum(aa.porcentajeprogreso) as porcentaje,count(ad.iddetalle) as cantidad
				FROM ((actividad_alumno aa inner join actividad_detalle ad on ad.iddetalle=aa.iddetalleactividad)
				inner join actividades a on a.idactividad=ad.idactividad)
				inner join niveles n on n.idnivel=a.unidad
				WHERE aa.idalumno='$filtros[idusuario]' and a.nivel=$filtros[idnivel]
				group by a.sesion,n.nombre";
			} elseif ($filtros["tipo"] == 'progreso_actividades') {

				$sql = "SELECT a.sesion,n.nombre,sum(aa.porcentajeprogreso) as porcentaje,count(ad.iddetalle) as cantidad
				FROM ((actividad_alumno aa inner join actividad_detalle ad on ad.iddetalle=aa.iddetalleactividad)
				inner join actividades a on a.idactividad=ad.idactividad)
				inner join niveles n on n.idnivel=a.sesion
				WHERE aa.idalumno='$filtros[idusuario]' and a.unidad=$filtros[idunidad]
				group by a.sesion,n.nombre";
			} elseif ($filtros["tipo"] == 'lista_niveles') {

				$sql = "SELECT niveles.idnivel, niveles.nombre, niveles.tipo, niveles.idpadre, niveles.idpersonal, niveles.estado, niveles.orden, niveles.imagen, niveles.descripcion from niveles
				where idpadre=$filtros[idpadre] and tipo='N' and estado = 1 order by orden";
			} elseif ($filtros["tipo"] == 'lista_unidades') {

				$sql = "SELECT niveles.idnivel, niveles.nombre, niveles.tipo, niveles.idpadre, niveles.idpersonal, niveles.estado, niveles.orden, niveles.imagen, niveles.descripcion from niveles
				where idpadre=$filtros[idpadre] and tipo='U' order by orden";
			} elseif ($filtros["tipo"] == 'lista_habilidades') {

				$sql = "SELECT idmetodologia, nombre from metodologia_habilidad
				where tipo='H'";
			} elseif ($filtros["tipo"] == 'progreso_actividadeshab') {

				$sql = "SELECT aa.porcentajeprogreso
				FROM ((actividad_alumno aa inner join actividad_detalle ad on ad.iddetalle=aa.iddetalleactividad)
				inner join actividades a on a.idactividad=ad.idactividad)
				inner join niveles n on n.idnivel=a.sesion 
				WHERE aa.idalumno='$filtros[idusuario]' and aa.habilidades like '%$filtros[hab]%'";
			} elseif ($filtros["tipo"] == 'progreso_actividadeshab_nivel') {

				$sql = "SELECT aa.porcentajeprogreso
				FROM ((actividad_alumno aa inner join actividad_detalle ad on ad.iddetalle=aa.iddetalleactividad)
				inner join actividades a on a.idactividad=ad.idactividad)
				inner join niveles n on n.idnivel=a.sesion 
				WHERE aa.idalumno='$filtros[idusuario]' and a.nivel=$filtros[idnivel] and aa.habilidades like '%$filtros[hab]%'";
			} elseif ($filtros["tipo"] == 'progreso_actividadeshab_unidad') {

				$sql = "SELECT aa.porcentajeprogreso
				FROM ((actividad_alumno aa inner join actividad_detalle ad on ad.iddetalle=aa.iddetalleactividad)
				inner join actividades a on a.idactividad=ad.idactividad)
				inner join niveles n on n.idnivel=a.sesion 
				WHERE aa.idalumno='$filtros[idusuario]' and a.unidad=$filtros[idunidad] and aa.habilidades like '%$filtros[hab]%'";
			}

			//echo $sql;
			// $cond = array();
			/*if(!empty($filtros["idnivel"])) {
				$cond[] = "n.idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}	
			if(!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["tipo"])) {
				$cond[] = "n.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["idpadre"])) {
				$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["estado"])){	
				$cond[] ="estado = ". $this->oBD->escapar($filtros["estado"]);
			}*/

			// if(!empty($cond)) {
			// 	$sql .= " WHERE " . implode(' AND ', $cond);
			// }
			/*if(!empty($filtros['sysordenar']))
				$sql.=' ORDER BY '.$filtros['sysordenar'];
			else*/
			//$sql.=' ORDER BY 4 ASC';

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}

	public function buscar2($filtros = null)
	{
		try {
			$sql = "SELECT niveles.idnivel, niveles.nombre, niveles.tipo, niveles.idpadre, niveles.idpersonal, niveles.estado, niveles.orden, niveles.imagen, niveles.descripcion FROM niveles";

			$cond = array();
			if (!empty($filtros["idnivel"])) {
				$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if (!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (!empty($filtros["idpadre"])) {
				$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			/*if(!empty($filtros['sysordenar']))
				$sql.=' ORDER BY '.$filtros['sysordenar'];
			else*/
			$sql .= ' ORDER BY orden asc';

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  niveles.idnivel, niveles.nombre, niveles.tipo, niveles.idpadre, niveles.idpersonal, niveles.estado, niveles.orden, niveles.imagen, niveles.descripcion  FROM niveles  ";

			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("List all") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}

	//public function insertar($nombre,$tipo,$idpadre,$idpersonal,$estado,$orden=0,$imagen=null)	
	public function insertar($id1, $titulo, $orden = 0, $estado, $idusuario, $imagen = null, $pestanas)
	{
		try {

			$this->iniciarTransaccion('dat_niveles_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idnivel) FROM niveles");
			++$id;
			//if (!$estado) $estado=0;

			$idpadre = 0;

			$estados = array(
				'idnivel' => $id, 'nombre' => $titulo, 'tipo' => 'C', 'idpadre' => $idpadre, 'idpersonal' => $idusuario, 'estado' => $estado, 'orden' => $orden, 'imagen' => $imagen, 'id' => $id1, 'pestanas' => $pestanas
			);


			$this->oBD->insert('niveles', $estados);
			$this->terminarTransaccion('dat_niveles_insert');

			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_niveles_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id1, $titulo, $orden = 0, $estado, $idusuario, $imagen = null, $pestanas)
	{
		try {
			$this->iniciarTransaccion('dat_niveles_update');

			$estados = array(
				'nombre' => $titulo, 'idpersonal' => $idusuario, 'estado' => $estado, 'orden' => $orden, 'imagen' => $imagen, 'id' => $id1, 'pestanas' => $pestanas
			);

			$this->oBD->update('niveles ', $estados, array('idnivel' => $id));
			$this->terminarTransaccion('dat_niveles_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}

	public function insertar_unidad($idpadre, $titulo, $orden = 0, $estado, $idusuario, $pestanas, $nivel, $imagen = null)
	{
		try {

			$this->iniciarTransaccion('dat_niveles_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idnivel) FROM niveles");
			++$id;
			//if (!$estado) $estado=0;
			if (!$pestanas) $pestanas = $idpadre . ",";
			$pestanas = $pestanas . $id . ",";


			$estados = array(
				'idnivel' => $id, 'nombre' => $titulo, 'tipo' => 'C', 'idpadre' => $idpadre, 'idpersonal' => $idusuario, 'estado' => $estado, 'orden' => $orden, 'pestanas' => $pestanas, 'nivel' => $nivel, 'imagen' => $imagen
			);


			$this->oBD->insert('niveles', $estados);
			$this->terminarTransaccion('dat_niveles_insert');

			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_niveles_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}

	public function actualizar_unidad($id, $id1, $titulo, $orden = 0, $estado, $idusuario, $imagen = null)
	{
		try {
			$this->iniciarTransaccion('dat_niveles_update');

			$estados = array(
				'nombre' => $titulo, 'idpersonal' => $idusuario, 'estado' => $estado, 'orden' => $orden, 'imagen' => $imagen
			);

			$this->oBD->update('niveles ', $estados, array('idnivel' => $id));
			$this->terminarTransaccion('dat_niveles_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT  niveles.idnivel, niveles.nombre, niveles.tipo, niveles.idpadre, niveles.idpersonal, niveles.estado, niveles.orden, niveles.imagen, niveles.descripcion  FROM niveles  "
				. " WHERE idnivel = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('niveles', array('idnivel' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('niveles', array($propiedad => $valor), array('idnivel' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}

	public function setCampo($filtros, $propiedad, $valor)
	{ //02.01.13
		try {
			$cond = array();
			if (!empty($filtros["idnivel"])) {
				$cond['idnivel'] = $this->oBD->escapar($filtros["idnivel"]);
			}

			if (!empty($filtros["tipo"])) {
				$cond["tipo"] = $filtros["tipo"];
			}
			if (!empty($filtros["idpadre"])) {
				$cond['idpadre'] = $this->oBD->escapar($filtros["idpadre"]);
			}
			if (!empty($filtros["orden"])) {
				$cond['orden'] = $this->oBD->escapar($filtros["orden"]);
			}
			if (!empty($cond))
				return $this->oBD->update('niveles', array($propiedad => $valor), $cond);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Niveles") . ": " . $e->getMessage());
		}
	}
}
