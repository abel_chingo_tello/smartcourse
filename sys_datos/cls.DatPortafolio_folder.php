<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-05-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatPortafolio_folder extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Portafolio_folder") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT portafolio_folder.idfolder, portafolio_folder.idfolder_padre, portafolio_folder.nombre, portafolio_folder.idportafolio, 'folder' as kind FROM portafolio_folder";

			$cond = array();


			if (isset($filtros["idfolder"])) {
				$cond[] = "idfolder = " . $this->oBD->escapar($filtros["idfolder"]);
			}
			if (isset($filtros["idfolder_padre"])) {
				$cond[] = "idfolder_padre = " . $this->oBD->escapar($filtros["idfolder_padre"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["idportafolio"])) {
				$cond[] = "idportafolio = " . $this->oBD->escapar($filtros["idportafolio"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Portafolio_folder") . ": " . $e->getMessage());
		}
	}


	public function insertar($idfolder_padre, $nombre, $idportafolio)
	{
		try {

			$this->iniciarTransaccion('dat_portafolio_folder_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idfolder) FROM portafolio_folder");
			++$id;

			$estados = array(
				'idfolder' => $id, 'idfolder_padre' => $idfolder_padre, 'nombre' => $nombre, 'idportafolio' => $idportafolio
			);
			// echo json_encode($estados);exit(0);
			$this->oBD->insert('portafolio_folder', $estados);
			$this->terminarTransaccion('dat_portafolio_folder_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_portafolio_folder_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Portafolio_folder") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idfolder_padre, $nombre, $idportafolio)
	{
		try {
			$this->iniciarTransaccion('dat_portafolio_folder_update');
			$estados = array(
				'idfolder_padre' => $idfolder_padre, 'nombre' => $nombre, 'idportafolio' => $idportafolio
			);

			$this->oBD->update('portafolio_folder ', $estados, array('idfolder' => $id));
			$this->terminarTransaccion('dat_portafolio_folder_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Portafolio_folder") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  portafolio_folder.idfolder, portafolio_folder.idfolder_padre, portafolio_folder.nombre, portafolio_folder.idportafolio  FROM portafolio_folder  "
				. " WHERE idfolder = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Portafolio_folder") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('portafolio_folder', array('idfolder' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Portafolio_folder") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('portafolio_folder', array($propiedad => $valor), array('idfolder' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Portafolio_folder") . ": " . $e->getMessage());
		}
	}
}
