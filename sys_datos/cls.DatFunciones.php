<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatFunciones extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Cargos").": " . $e->getMessage());
		}
    }
    public function ejercutarSQL($sql){
        try{
            return $this->oBD->ejecutarSQL($sql);
        } catch(Exception $e) {
			return -1;
        }
	}
	public function allSQL($sql){
		try{
            return $this->oBD->allsql($sql);
        } catch(Exception $e) {
           return null;
        }
	}
    

	public function getNumRegistros($filtros=null)
	{
		/*try {
			$sql = "SELECT COUNT(*) FROM cargos";
			
			$cond = array();		
			
			if(isset($filtros["idcargo"])) {
					$cond[] = "idcargo = " . $this->oBD->escapar($filtros["idcargo"]);
			}
			if(isset($filtros["cargo"])) {
					$cond[] = "cargo = " . $this->oBD->escapar($filtros["cargo"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Cargos").": " . $e->getMessage());
		}*/
	}
	public function buscar($filtros=null)
	{
		/*try {
			$sql = "SELECT * FROM cargos";			
			
			$cond = array();		
					
			
			if(isset($filtros["idcargo"])) {
					$cond[] = "idcargo = " . $this->oBD->escapar($filtros["idcargo"]);
			}
			if(isset($filtros["cargo"])) {
					$cond[] = "cargo = " . $this->oBD->escapar($filtros["cargo"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Cargos").": " . $e->getMessage());
		}*/
	}
	
	
	public function insertar($cargo,$mostrar)
	{
		/*try {
			
			$this->iniciarTransaccion('dat_cargos_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcargo) FROM cargos");
			++$id;
			
			$estados = array('idcargo' => $id
							
							,'cargo'=>$cargo
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('cargos', $estados);			
			$this->terminarTransaccion('dat_cargos_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_cargos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Cargos").": " . $e->getMessage());
		}*/
	}
	public function actualizar($id, $cargo,$mostrar)
	{
		/*try {
			$this->iniciarTransaccion('dat_cargos_update');
			$estados = array('cargo'=>$cargo
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('cargos ', $estados, array('idcargo' => $id));
		    $this->terminarTransaccion('dat_cargos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Cargos").": " . $e->getMessage());
		}*/
	}
	public function get($id)
	{
		/*try {
			$sql = "SELECT  *  FROM cargos  "
					. " WHERE idcargo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Cargos").": " . $e->getMessage());
		}*/
	}

	public function eliminar($id)
	{
		/*try {
			return $this->oBD->delete('cargos', array('idcargo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Cargos").": " . $e->getMessage());
		}*/
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		/*try {
			$this->oBD->update('cargos', array($propiedad => $valor), array('idcargo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Cargos").": " . $e->getMessage());
		}*/
	}
}