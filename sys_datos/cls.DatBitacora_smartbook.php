<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-11-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatBitacora_smartbook extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM bitacora_smartbook BS JOIN bitacora_alumno_smartbook BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";

			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idsesionB"])) {
				$cond[] = "BS.idsesionB = " . $this->oBD->escapar($filtros["idsesionB"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT BAS.idbitacora_smartbook, BAS.idcurso, BAS.idsesion, BAS.idusuario, BAS.estado, BAS.regfecha, BAS.idcomplementario, BS.idbitacora, BS.idbitacora_alum_smartbook, BS.idcurso, BS.idsesion, BS.idsesionB, BS.idusuario, BS.pestania, BS.total_pestanias, BS.fechahora, BS.progreso, BS.otros_datos, BS.idcomplementario,BS.idgrupoauladetalle FROM bitacora_smartbook BS RIGHT JOIN bitacora_alumno_smartbook BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";

			$cond = array();


			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idsesionB"])) {
				$cond[] = "BS.idsesionB = " . $this->oBD->escapar($filtros["idsesionB"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "BS.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY fechahora DESC";
			//echo $sql .'<br>';
			//exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function getProgresoPromedio($filtros = null)
	{
		try {
			$sql = "SELECT AVG(progreso) FROM bitacora_smartbook";

			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idsesionB"])) {
				$cond[] = "idsesionB = " . $this->oBD->escapar($filtros["idsesionB"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function getSumaProgresos($filtros = null)
	{
		try {
			$sql = "SELECT SUM(progreso) FROM bitacora_smartbook BS JOIN bitacora_alumno_smartbook BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";

			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idusuario"])) {
				//$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
				$cond[] = "BAS.idusuario=" . $this->oBD->escapar($filtros["idusuario"]);
			}

			if (isset($filtros["idsesionB"])) {
				$cond[] = "BS.idsesionB = " . $this->oBD->escapar($filtros["idsesionB"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}


			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql.'<br>';
			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function getSumaProgresos2($filtros = null)
	{ //array('idcomplementario' => $idCurso, 'idusuario' => $idpersona, 'idsesion' => $sesion['idrecurso'])
		try {

			$sql = "SELECT SUM(progreso) as sumprogreso ,total_pestanias FROM bitacora_smartbook BS JOIN bitacora_alumno_smartbook BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";
			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["sql2"])) {
				if ($filtros["sql2"]) {
					$sql = " SELECT BAS.idsesion, ifnull(SUM(progreso) / BS.total_pestanias,if(BAS.estado='T',100,100)) as progreso,BAS.estado
							FROM bitacora_smartbook BS 
								right JOIN bitacora_alumno_smartbook BAS 
									ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";
				}
			}


			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				// echo ("[principal]");
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcomplementario"])) {
				// echo ("[complementarioooo]");
				$cond[] = "BAS.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idsesionB"])) {
				$cond[] = "BS.idsesionB = " . $this->oBD->escapar($filtros["idsesionB"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (isset($filtros["idusuario"])) {
				//$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
				$cond[] = "BAS.idusuario=" . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}


			if (isset($filtros["sql2"]) && $filtros["sql2"]) {

				$sql .= " GROUP BY BAS.idsesion ";
			} else {
				$sql .= " GROUP BY total_pestanias ";
			}
			// echo '<hr>'.$sql.'<hr>';
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function getProgresoBitacoraSesiones($filtros = null)
	{ //array('idcomplementario' => $idCurso, 'idusuario' => $idpersona, 'idsesion' => $sesion['idrecurso'])
		try {

			$sql = "SELECT  BAS.idsesion,BS.idsesionB,ifnull(BS.progreso,if(BAS.estado='T',100,100)) as progreso,BAS.estado
					FROM bitacora_smartbook BS 
						right JOIN bitacora_alumno_smartbook BAS 
							ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook";
			#WHERE BAS.idsesion = "1632" AND BAS.idcurso =196 AND BAS.idusuario=6258
			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				// echo ("[principal]");
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcomplementario"])) {
				// echo ("[complementarioooo]");
				$cond[] = "BAS.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idsesionB"])) {
				$cond[] = "BS.idsesionB = " . $this->oBD->escapar($filtros["idsesionB"]);
			}

			#mine-descomentar
			if (isset($filtros["idgrupoauladetalle"])) {
				$sql .= " AND BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
				$cond[] = "BAS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (isset($filtros["idusuario"])) {
				$cond[] = "BAS.idusuario=" . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function insertar($idcurso, $idsesion, $idsesionB, $idusuario, $idbitacora_alum_smartbook, $pestania, $total_pestanias, $fechahora, $progreso, $otros_datos = '', $idcomplementario, $idgrupoauladetalle = null)
	{
		try {
			if (empty($idusuario)) return 0;
			$estados = array((!empty($idcurso) ? $idcurso : 0),
				$idsesion,
				$idsesionB,
				$idusuario,
				!empty($idcomplementario) ? $idcomplementario : 0,
				$idbitacora_alum_smartbook,
				$this->oBD->escapar($pestania),
				!empty($total_pestanias) ? $total_pestanias : 0,
				$this->oBD->escapar($fechahora),
				$progreso,
				$this->oBD->escapar(!empty($otros_datos) ? $otros_datos : ''),
				!empty($idgrupoauladetalle) ? $idgrupoauladetalle : 'null'
			);
			$sql = "CALL insertarbitacora (" . (implode(",", $estados)) . ")";
			return $this->oBD->consultarEscalarSQL($sql);

			//$this->iniciarTransaccion('dat_bitacora_smartbook_insert');
			/*$id = $this->oBD->consultarEscalarSQL("SELECT idbitacora FROM bitacora_smartbook order by idbitacora desc limit 0,1");
			++$id;
			$estados = array('idbitacora'=>$id,
				 'idcurso' => $idcurso, 'idsesion' => $idsesion, 'idsesionB' => $idsesionB, 'idusuario' => $idusuario, 'idcomplementario' => $idcomplementario, 'idbitacora_alum_smartbook' => $idbitacora_alum_smartbook, 'pestania' => $pestania, 'total_pestanias' => $total_pestanias, 'fechahora' => $fechahora, 'progreso' => $progreso
			);
			if (!empty($otros_datos)) {
				$estados["otros_datos"] = $otros_datos;
			}

			$this->oBD->insert('bitacora_smartbook', $estados);*/
			//$id = $this->oBD->consultarEscalarSQL("SELECT idbitacora FROM bitacora_smartbook order by idbitacora desc limit 0,1");

			//$this->terminarTransaccion('dat_bitacora_smartbook_insert');
			//return $id;
		} catch (Exception $e) {
			//$this->cancelarTransaccion('dat_bitacora_smartbook_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso, $idsesion, $idsesionB, $idusuario, $idbitacora_alum_smartbook, $pestania, $total_pestanias, $fechahora, $progreso, $otros_datos = '', $idcomplementario, $idgrupoauladetalle = null)
	{
		try {
			if (empty($pestania) || empty($idusuario)) return $id;
			//$this->iniciarTransaccion('dat_bitacora_smartbook_update');
			$estados = array(
				'idcurso' => $idcurso, 'idsesion' => $idsesion, 'idsesion' => $idsesion, 'idusuario' => $idusuario, 'idcomplementario' => !empty($idcomplementario) ? $idcomplementario : 0, 'idbitacora_alum_smartbook' => $idbitacora_alum_smartbook, 'pestania' => !empty($pestania) ? $pestania : 0, 'total_pestanias' => !empty($total_pestanias) ? $total_pestanias : 0, 'fechahora' => !empty($fechahora) ? $fechahora : date('Y-m-d H:i:s'), 'progreso' => !empty($progreso) ? $progreso : 0
			);
			if (!empty($otros_datos)) {
				$estados["otros_datos"] = $otros_datos;
			}
			if (!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"] = $idgrupoauladetalle;

			$this->oBD->update('bitacora_smartbook ', $estados, array('idbitacora' => $id));
			//$this->terminarTransaccion('dat_bitacora_smartbook_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  bitacora_smartbook.idbitacora, bitacora_smartbook.idbitacora_alum_smartbook, bitacora_smartbook.idcurso, bitacora_smartbook.idsesion, bitacora_smartbook.idsesionB, bitacora_smartbook.idusuario, bitacora_smartbook.pestania, bitacora_smartbook.total_pestanias, bitacora_smartbook.fechahora, bitacora_smartbook.progreso, bitacora_smartbook.otros_datos, bitacora_smartbook.idcomplementario,idgrupoauladetalle  FROM bitacora_smartbook  "
				. " WHERE idbitacora = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function multidelete($ids)
	{
		try {
			$this->iniciarTransaccion('dat_multiple_delete');
			$sql = "DELETE FROM bitacora_smartbook WHERE idbitacora_alum_smartbook IN (" . implode(',', $ids) . ")";
			$this->oBD->ejecutarSQL($sql);
			$this->terminarTransaccion('dat_multiple_delete');
			return true;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_multiple_delete');
			throw new Exception("ERROR\n" . JrTexto::_("Multidelete") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bitacora_smartbook', array('idbitacora' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('bitacora_smartbook', array($propiedad => $valor), array('idbitacora' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
}
