<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatLibre_tipo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Libre_tipo") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(idtipo) FROM libre_tipo";

			$cond = array();

			if (isset($filtros["idtipo"])) {
				$cond[] = "idtipo = " . $this->oBD->escapar($filtros["idtipo"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["tipo_contenido"])) {
				$cond[] = "tipo_contenido = " . $this->oBD->escapar($filtros["tipo_contenido"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Libre_tipo") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT libre_tipo.idtipo, libre_tipo.nombre, libre_tipo.tipo_contenido FROM libre_tipo";

			$cond = array();


			if (isset($filtros["idtipo"])) {
				$cond[] = "idtipo = " . $this->oBD->escapar($filtros["idtipo"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["tipo_contenido"])) {
				$cond[] = "tipo_contenido = " . $this->oBD->escapar($filtros["tipo_contenido"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY nombre ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Libre_tipo") . ": " . $e->getMessage());
		}
	}


	public function insertar($nombre, $tipo_contenido)
	{
		try {

			$this->iniciarTransaccion('dat_libre_tipo_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtipo) FROM libre_tipo");
			++$id;

			$estados = array(
				'idtipo' => $id, 'nombre' => $nombre, 'tipo_contenido' => $tipo_contenido
			);

			$this->oBD->insert('libre_tipo', $estados);
			$this->terminarTransaccion('dat_libre_tipo_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_libre_tipo_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Libre_tipo") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre, $tipo_contenido)
	{
		try {
			$this->iniciarTransaccion('dat_libre_tipo_update');
			$estados = array(
				'nombre' => $nombre, 'tipo_contenido' => $tipo_contenido
			);

			$this->oBD->update('libre_tipo ', $estados, array('idtipo' => $id));
			$this->terminarTransaccion('dat_libre_tipo_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Libre_tipo") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  libre_tipo.idtipo, libre_tipo.nombre, libre_tipo.tipo_contenido  FROM libre_tipo  "
				. " WHERE idtipo = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Libre_tipo") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('libre_tipo', array('idtipo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Libre_tipo") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('libre_tipo', array($propiedad => $valor), array('idtipo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Libre_tipo") . ": " . $e->getMessage());
		}
	}
}
