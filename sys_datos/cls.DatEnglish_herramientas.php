<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatEnglish_herramientas extends DatBase
{
	public function __construct($BD='')
	{
		try{			
			if(!empty($BD)) $BD=NAME_BDSEAdolecentes;
			parent::conectar($BD);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			
			$sql = "SELECT idtool,idnivel,idunidad,idactividad,titulo,idpersonal,tool,orden,descripcion,texto FROM herramientas";
			$cond = array();
			if(!empty($filtros["id"])) {
					$cond[] = "idtool = " . $this->oBD->escapar($filtros["id"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo LIKE '%" . $filtros["titulo"] . "%'";
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			
			if(!empty($filtros["tool"])) {
					$cond[] = "tool = " . $this->oBD->escapar($filtros["tool"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY orden ASC, idtool ASC";
			if(isset($filtros["BD"])) {
				parent::conectar($filtros["BD"]);
			}
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Herramientas").": " . $e->getMessage());
		}
	}

}