<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-01-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatSinlogin_quizproyecto extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Sinlogin_quizproyecto").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT sp.*,titulo FROM `".NAME_BD."`.`sinlogin_quizproyecto` sp INNER JOIN `".NAME_BDQUIZ."`.examenes ex  ON  sp.idquiz=ex.idexamen";			
			
			$cond = array();		
					
			
			if(isset($filtros["idquiz"])) {
					$cond[] = "sp.idquiz = " . $this->oBD->escapar($filtros["idquiz"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "sp.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["user_registro"])) {
					$cond[] = "sp.user_registro = " . $this->oBD->escapar($filtros["user_registro"]);
			}
			if(isset($filtros["fecha_hasta"])) {
					$cond[] = "sp.fecha_hasta = " . $this->oBD->escapar($filtros["fecha_hasta"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "sp.estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Sinlogin_quizproyecto").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idquiz,$idproyecto,$user_registro,$fecha_hasta,$estado)
	{
		try {
			
			//$this->iniciarTransaccion('dat_sinlogin_quizproyecto_insert');
			
			//$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idasignacion) FROM sinlogin_quizproyecto");
			//++$id;
			
			$estados = array('idquiz'=>$idquiz
							,'idproyecto'=>$idproyecto
							,'user_registro'=>$user_registro
							,'fecha_hasta'=>$fecha_hasta
							,'estado'=>$estado							
							);
			
			return $this->oBD->insertAI('sinlogin_quizproyecto', $estados,true);			
			//$this->terminarTransaccion('dat_sinlogin_quizproyecto_insert');			
			//return $id;

		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_sinlogin_quizproyecto_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Sinlogin_quizproyecto").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idquiz,$idproyecto,$user_registro,$fecha_hasta,$estado)
	{
		try {
			//$this->iniciarTransaccion('dat_sinlogin_quizproyecto_update');
			$estados = array('idquiz'=>$idquiz
							,'idproyecto'=>$idproyecto
							,'user_registro'=>$user_registro
							,'fecha_hasta'=>$fecha_hasta
							,'estado'=>$estado								
							);
			
			$this->oBD->update('sinlogin_quizproyecto ', $estados, array('idasignacion' => $id));
		   // $this->terminarTransaccion('dat_sinlogin_quizproyecto_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sinlogin_quizproyecto").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM sinlogin_quizproyecto  "
					. " WHERE idasignacion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Sinlogin_quizproyecto").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('sinlogin_quizproyecto', array('idasignacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Sinlogin_quizproyecto").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('sinlogin_quizproyecto', array($propiedad => $valor), array('idasignacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sinlogin_quizproyecto").": " . $e->getMessage());
		}
	}
   
		
}