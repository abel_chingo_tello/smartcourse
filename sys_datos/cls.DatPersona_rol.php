<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-12-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatPersona_rol extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(iddetalle) FROM persona_rol";

			$cond = array();

			if (isset($filtros["iddetalle"])) {
				$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if (isset($filtros["idrol"])) {
				$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if (isset($filtros["idpersonal"])) {
				$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT p.iddetalle, p.idrol, p.idpersonal, p.idempresa, p.idproyecto, r.rol FROM persona_rol p left Join roles r on  p.idrol=r.idrol";

			$cond = array();
			if (isset($filtros["sql2"])) {
				$sql = "SELECT p.iddetalle, p.idrol, p.idpersonal, p.idempresa, p.idproyecto, r.rol,pe.usuario,pe.clave FROM persona_rol p left Join roles r on  p.idrol=r.idrol INNER JOIN personal pe ON p.idpersonal = pe.idpersona";
				if (isset($filtros["estado"])) {
					$cond[] = "pe.estado = " . $this->oBD->escapar($filtros["estado"]);
				}
			}
			if (isset($filtros["iddetalle"])) {
				$cond[] = "p.iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if (isset($filtros["idrol"])) {
				$cond[] = "p.idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "p.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idempresa"])) {
				$cond[] = "p.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if (isset($filtros["noproyecto"]) && isset($filtros["noempresa"])) {
				$sql = "SELECT p.iddetalle, p.idrol, p.idpersonal, p.idempresa, p.idproyecto, rol,nombre as empresa FROM persona_rol p left Join roles r on  p.idrol=r.idrol INNER JOIN bolsa_empresas be ON p.idempresa=be.idempresa";
				$cond[] = "p.idproyecto <> " . $this->oBD->escapar($filtros["noproyecto"]);
				$cond[] = "p.idempresa <> " . $this->oBD->escapar($filtros["noempresa"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "p.idpersonal = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if (isset($filtros["idpersonal"])) {
				$cond[] = "(p.idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]) . " OR md5(p.idpersonal)=" . $this->oBD->escapar($filtros["idpersonal"]) . ")";
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// echo $sql;
			if (!empty($filtros['groupby'])) {
				if ($filtros['groupby'] == 'idrol') $sql .= " GROUP BY p.idrol";
				if ($filtros['groupby'] == 'idempresa') $sql .= " GROUP BY p.idempresa";
				if ($filtros['groupby'] == 'idproyecto') $sql .= " GROUP BY p.idproyecto";
			}
			$sql .= " ORDER BY p.idrol ASC";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}

	public function buscarcantidad($filtros = null)
	{
		try {
			$sql = "SELECT p.iddetalle, p.idrol, p.idpersonal, p.idempresa, p.idproyecto, rol FROM persona_rol p left Join roles r on  p.idrol=r.idrol";

			$cond = array();
			if (isset($filtros["iddetalle"])) {
				$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if (isset($filtros["idrol"])) {
				$cond[] = "p.idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "p.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idempresa"])) {
				$cond[] = "p.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if (isset($filtros["idpersonal"])) {
				$cond[] = "(idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]) . " OR md5(idpersonal)=" . $this->oBD->escapar($filtros["idpersonal"]) . ")";
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// echo $sql;
			$sql .= " ORDER BY p.idrol ASC";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}

	public function insertar($idrol, $idpersonal, $idproyecto, $idempresa)
	{
		try {
			$this->iniciarTransaccion('dat_persona_rol_insert');
			if ($idrol == 10 || $idrol == 1) {
				$id = $this->oBD->consultarEscalarSQL("SELECT (iddetalle) FROM persona_rol WHERE idpersonal='" . $idpersonal . "' AND idrol='" . $idrol . "'");
				if (!empty($id)) {
					return $this->actualizar($id, $idrol, $idpersonal, $idproyecto, $idempresa);
				}
			}
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
			++$id;
			$estados = array(
				'iddetalle' => $id, 'idrol' => $idrol, 'idpersonal' => $idpersonal, 'idproyecto' => $idproyecto, 'idempresa' => !empty($idempresa) ? $idempresa : 4
			);
			$this->oBD->insert('persona_rol', $estados);
			$this->terminarTransaccion('dat_persona_rol_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_persona_rol_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idrol, $idpersonal, $idproyecto, $idempresa)
	{
		try {
			$this->iniciarTransaccion('dat_persona_rol_update');
			$estados = array(
				'idrol' => $idrol, 'idpersonal' => $idpersonal, 'idproyecto' => $idproyecto, 'idempresa' => $idempresa
			);

			$this->oBD->update('persona_rol ', $estados, array('iddetalle' => $id));
			$this->terminarTransaccion('dat_persona_rol_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT p.iddetalle, p.idrol, p.idpersonal, p.idempresa, p.idproyecto, rol FROM persona_rol p inner Join roles r on  p.idrol=r.idrol"
				. " WHERE iddetalle = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_rol', array('iddetalle' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('persona_rol', array($propiedad => $valor), array('iddetalle' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Persona_rol") . ": " . $e->getMessage());
		}
	}
}
