<?php
class DatSendemail extends DatBase
{
    public function __construct()
    {
        try {
            parent::conectar();
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("sendemail") . ": " . $e->getMessage());
        }
    }
    public function buscar($filtros = null)
    {
        try {
            $sql = "SELECT config,config,config FROM sis_configuracion";

            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("sendemail") . ": " . $e->getMessage());
        }
    }
    public function emailsocios()
    {
        try {
            $sql = "SELECT pe.email FROM personal pe INNER JOIN (SELECT rol.idpersonal FROM persona_rol rol WHERE rol.idempresa=10 AND (rol.idrol = 1 or rol.idrol > 3)) AS tmp_table ON tmp_table.idpersonal = pe.idpersona";
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("sendemail") . ": " . $e->getMessage());
        }
    }
    public function emailmatricula($filtros = null)
    {
        try {
            $sql = "SELECT pe.email FROM acad_matricula ma INNER JOIN acad_grupoauladetalle gad ON gad.idgrupoauladetalle = ma.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON ga.idgrupoaula = gad.idgrupoaula INNER JOIN personal pe ON pe.idpersona = ma.idalumno";
            $cond = array();
            if (isset($filtros['idproyecto'])) {
                $cond[] = "ga.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
            }
            if (!empty($cond)) {
                $sql .= " WHERE " . implode(' AND ', $cond);
            }
            $sql .= " GROUP BY ma.idalumno ";
            return $this->oBD->consultarSQL($sql);
        } catch (Exception $e) {
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("sendemail") . ": " . $e->getMessage());
        }
    }
    public function actualizar($config, $valor, $autocargar)
    {
        try {
            $this->iniciarTransaccion('dat_sendEmail_update');

            $estados = array(
                'valor' => $valor, 'autocargar' => $autocargar
            );

            $this->oBD->update('sis_configuracion', $estados, array('config' => $config));
            $this->terminarTransaccion('dat_sendEmail_update');
            return true;
        } catch (Exception $e) {
            $this->terminarTransaccion('dat_sendEmail_update');
            throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("sendemail") . ": " . $e->getMessage());
        }
    }
}
