<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-02-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatNotificacion_de extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idnotificacion,idpersona,tipo,idrecurso,texto,idgrupoauladetalle FROM notificacion_de";			
			
			$cond = array();

			if(isset($filtros["idnotificacion"])) {
					$cond[] = "idnotificacion = " . $this->oBD->escapar($filtros["idnotificacion"]);
			}

			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}		
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM notificacion_de";
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if(!empty($filtros["orderby"])){
				$sql.=" ORDER BY ".$filtros["orderby"];
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idpersona,$tipo,$idrecurso,$texto,$idgrupoauladetalle)
	{
		try {
			
			//$this->iniciarTransaccion('dat_notificacion_de_insert');
			
			//$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idnotificacion) FROM notificacion_de");
			//++$id;
			
			$estados = array(//'idnotificacion' => $id,							
							'idpersona'=>$idpersona
							,'tipo'=>$tipo
							,'idrecurso'=>!empty($idrecurso)?$idrecurso:'null'
							,'idgrupoauladetalle'=>!empty($idgrupoauladetalle)?$idgrupoauladetalle:'null'
							,'texto'=>$texto							
							);
			
			//$this->oBD->insert('notificacion_de', $estados);	
			return $this->oBD->insertAI('notificacion_de',$estados);
			//insertAI		
			//$this->terminarTransaccion('dat_notificacion_de_insert');			
			//return $id;

		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_notificacion_de_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}


	public function insertar2($idpersona,$tipo,$idrecurso,$texto,$idgrupoauladetalle,$para=array()){
		try {
			$estados = array(//'idnotificacion' => $id,							
							'idpersona'=>$idpersona
							,'tipo'=>$tipo
							,'idrecurso'=>!empty($idrecurso)?$idrecurso:'null'
							,'idgrupoauladetalle'=>!empty($idgrupoauladetalle)?$idgrupoauladetalle:'null'
							,'texto'=>$texto							
							);
			$idnotificacion=$this->oBD->insertAI('notificacion_de',$estados);	
			foreach ($para as $i => $pa){
				$estados = array(//'idnotipersona' => $id	,						
							'idnotificacion'=>$idnotificacion
							,'idpersona'=>$pa
							,'estado'=>1
							);
				$this->oBD->insertAI('notificacion_para',$estados);
			}
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_notificacion_de_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}


	public function actualizar($id, $idpersona,$tipo,$idrecurso,$texto,$idgrupoauladetalle)
	{
		try {
			//$this->iniciarTransaccion('dat_notificacion_de_update');
			$estados = array('idpersona'=>$idpersona
							,'tipo'=>$tipo
							,'idrecurso'=>!empty($idrecurso)?$idrecurso:'null'
							,'idgrupoauladetalle'=>!empty($idgrupoauladetalle)?$idgrupoauladetalle:'null'
							,'texto'=>$texto								
							);
			
			$this->oBD->update('notificacion_de ', $estados, array('idnotificacion' => $id));
		   // $this->terminarTransaccion('dat_notificacion_de_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}

	public function actualizar2($estados,$condicion)
	{
		try {		
			
			return $this->oBD->update('notificacion_de ', $estados,$condicion);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}



	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.nombre AS _nombre  FROM notificacion_de tb1 LEFT JOIN personal tb2 ON tb1.idpersona=tb2.idpersona  "
					. " WHERE tb1.idnotificacion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('notificacion_de', array('idnotificacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('notificacion_de', array($propiedad => $valor), array('idnotificacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notificacion_de").": " . $e->getMessage());
		}
	}
   
		
}