<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-03-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatAcad_curso_consulta extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_curso_consulta").": " . $e->getMessage());
		}
	}

	public $resultado = array();

	public function ordenarDatos($rs = array(), $respuesta = 0) {
        if (!empty($rs)) {
            foreach ($rs as $element) {
                if ($element["respuesta"] == $respuesta) {
                    array_push($this->resultado, $element);
                    if ($element["hijos"] > 0) {
                        $this->ordenarDatos($rs, $element["codigo_consulta"]);
                    }
                }
            }
        }
    }
	
	public function buscar($filtros=null)
	{
		try {
			if(isset($filtros["sql"])) {
				switch ($filtros["sql"]) {
					case '1':
						$sql = "SELECT c.codigo as codigo_consulta, c.codigo,c.idcurso,c.idcomplementario,c.idcategoria,c.idgrupoaula,c.iddetalle,c.idpestania,c.tipocurso,c.idpersona,c.likes,c.like_de,c.contenido,c.fecha_hora,c.respuesta, p.*, DATE_FORMAT(c.fecha_hora, '%d/%m/%Y %h:%i %p') as fecha, (select count(1) from acad_curso_consulta where respuesta = c.codigo) as hijos, '' as padre FROM acad_curso_consulta c inner join personal p on (c.idpersona = p.idpersona)";
						$cond = array();
						if(isset($filtros["idcurso"])) {
							$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
						}
						if(isset($filtros["idcategoria"])) {
							$cond[] = "c.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
						}
						if(isset($filtros["idgrupoaula"])) {
							$cond[] = "c.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
						}
						if(isset($filtros["iddetalle"])) {
							$cond[] = "c.iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
						}
						if(isset($filtros["idpersona"])) {
							$cond[] = "c.idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
						}
						if(isset($filtros["idcomplementario"])) {
							$cond[] = "c.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
						}
						if(isset($filtros["idpestania"])) {
							$cond[] = "c.idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
						}
						if(!empty($cond)) {
							$sql .= " WHERE " . implode(' AND ', $cond);
						}
						$sql .= "ORDER BY c.respuesta, c.fecha_hora";
						// echo $sql;exit();
						$rs = $this->oBD->consultarSQL($sql);
						foreach ($rs as $element) {
							if (is_null($element["respuesta"])) {
								array_push($this->resultado, array_replace($element, array("padre" => 'p')));
								$this->ordenarDatos($rs, $element["codigo_consulta"]);
							}
						}
						return $this->resultado;
						break;
				}
			}
			$sql = "SELECT `codigo`,`idcurso`,`idcomplementario`,`idcategoria`,`idgrupoaula`,`iddetalle`,`idpestania`,`tipocurso`,`idpersona`,`likes`,`like_de`,`contenido`,`fecha_hora`,`respuesta` FROM acad_curso_consulta";			
			
			$cond = array();		
					
			
			if(isset($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idcategoria"])) {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["iddetalle"])) {
					$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(isset($filtros["tipocurso"])) {
					$cond[] = "tipocurso = " . $this->oBD->escapar($filtros["tipocurso"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["contenido"])) {
					$cond[] = "contenido = " . $this->oBD->escapar($filtros["contenido"]);
			}
			if(isset($filtros["fecha_hora"])) {
					$cond[] = "fecha_hora = " . $this->oBD->escapar($filtros["fecha_hora"]);
			}
			if(isset($filtros["respuesta"])) {
					$cond[] = "respuesta = " . $this->oBD->escapar($filtros["respuesta"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n  ".JrTexto::_("Search")." ".JrTexto::_("Acad_curso_consulta").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($estados)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_curso_consulta_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(codigo) FROM acad_curso_consulta");
			++$id;

			$estados["codigo"] = $id;
			
			// $estados = array('codigo' => $id
			// 				,'idcurso'=>$idcurso
			// 				,'idcategoria'=>$idcategoria
			// 				,'idgrupoaula'=>$idgrupoaula
			// 				,'iddetalle'=>$iddetalle
			// 				,'tipocurso'=>$tipocurso
			// 				,'idpersona'=>$idpersona
			// 				,'contenido'=>$contenido
			// 				,'fecha_hora'=>$fecha_hora
			// 				,'respuesta'=>$respuesta							
			// 				);
			
			$this->oBD->insert('acad_curso_consulta', $estados);			
			$this->terminarTransaccion('dat_acad_curso_consulta_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_curso_consulta_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_curso_consulta").": " . $e->getMessage());
		}
	}
	public function actualizar($estados)
	{
		try {
			$this->iniciarTransaccion('dat_acad_curso_consulta_update');
			// $estados = array('idcurso'=>$idcurso
			// 				,'idcategoria'=>$idcategoria
			// 				,'idgrupoaula'=>$idgrupoaula
			// 				,'iddetalle'=>$iddetalle
			// 				,'tipocurso'=>$tipocurso
			// 				,'idpersona'=>$idpersona
			// 				,'contenido'=>$contenido
			// 				,'fecha_hora'=>$fecha_hora
			// 				,'respuesta'=>$respuesta								
			// 				);
			$condicion = array('codigo' => $estados["codigo"]);
			unset($estados["codigo"]);
			$this->oBD->update('acad_curso_consulta', $estados, $condicion);
		    $this->terminarTransaccion('dat_acad_curso_consulta_update');
		    return $condicion;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso_consulta").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  `codigo`,`idcurso`,`idcomplementario`,`idcategoria`,`idgrupoaula`,`iddetalle`,`idpestania`,`tipocurso`,`idpersona`,`likes`,`like_de`,`contenido`,`fecha_hora`,`respuesta`  FROM acad_curso_consulta  "
					. " WHERE codigo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_curso_consulta").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_curso_consulta', array('codigo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_curso_consulta").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_curso_consulta', array($propiedad => $valor), array('codigo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso_consulta").": " . $e->getMessage());
		}
	}
   
		
}