<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		01-02-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatTest_asignacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Test_asignacion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM test_asignacion";
			
			$cond = array();		
			
			if(isset($filtros["idtestasigancion"])) {
					$cond[] = "idtestasigancion = " . $this->oBD->escapar($filtros["idtestasigancion"]);
			}
			if(isset($filtros["idtest"])) {
					$cond[] = "idtest = " . $this->oBD->escapar($filtros["idtest"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["situacion"])) {
					$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Test_asignacion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT ta.*,t.titulo,t.imagen,t.titulo,t.puntaje,t.mostrar FROM test_asignacion ta INNER JOIN test t ON t.idtest=ta.idtest";	
			$cond = array();
			if(isset($filtros["idtestasigancion"])) {
					$cond[] = "idtestasigancion = " . $this->oBD->escapar($filtros["idtestasigancion"]);
			}
			if(isset($filtros["idtest"])) {
					$cond[] = "ta.idtest = " . $this->oBD->escapar($filtros["idtest"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["situacion"])) {
					$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Test_asignacion").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idtest,$idcurso,$idrecurso,$situacion)
	{
		try {
			
			$this->iniciarTransaccion('dat_test_asignacion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtestasigancion) FROM test_asignacion");
			++$id;
			
			$estados = array('idtestasigancion' => $id
							
							,'idtest'=>$idtest
							,'idcurso'=>$idcurso
							,'idrecurso'=>$idrecurso
							,'situacion'=>$situacion							
							);
			
			$this->oBD->insert('test_asignacion', $estados);			
			$this->terminarTransaccion('dat_test_asignacion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_test_asignacion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Test_asignacion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idtest,$idcurso,$idrecurso,$situacion)
	{
		try {
			$this->iniciarTransaccion('dat_test_asignacion_update');
			$estados = array('idtest'=>$idtest
							,'idcurso'=>$idcurso
							,'idrecurso'=>$idrecurso
							,'situacion'=>$situacion								
							);
			
			$this->oBD->update('test_asignacion ', $estados, array('idtestasigancion' => $id));
		    $this->terminarTransaccion('dat_test_asignacion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Test_asignacion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM test_asignacion  "
					. " WHERE idtestasigancion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Test_asignacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('test_asignacion', array('idtestasigancion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Test_asignacion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('test_asignacion', array($propiedad => $valor), array('idtestasigancion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Test_asignacion").": " . $e->getMessage());
		}
	}
   
		
}