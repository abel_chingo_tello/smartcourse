<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-04-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatRubrica extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rubrica").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idrubrica,nombre,fecha_creacion,idproyecto,estado,iddocente FROM rubrica";			
			
			$cond = array();		
					
			
			if(isset($filtros["idrubrica"])) {
					$cond[] = "idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_creacion"])) {
					$cond[] = "fecha_creacion = " . $this->oBD->escapar($filtros["fecha_creacion"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rubrica").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$fecha_creacion,$idproyecto,$estado,$iddocente)
	{
		try {
			
			$this->iniciarTransaccion('dat_rubrica_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrubrica) FROM rubrica");
			++$id;
			
			$estados = array('idrubrica' => $id
							
							,'nombre'=>$nombre
							// ,'fecha_creacion'=>$fecha_creacion
							,'idproyecto'=>$idproyecto
							// ,'estado'=>1
							,'iddocente'=>$iddocente							
							);
			// echo json_encode($estados);exit();
			$this->oBD->insert('rubrica', $estados);			
			$this->terminarTransaccion('dat_rubrica_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rubrica_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rubrica").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$fecha_creacion,$idproyecto,$estado,$iddocente)
	{
		try {
			$this->iniciarTransaccion('dat_rubrica_update');
			$estados = array('nombre'=>$nombre
							// ,'fecha_creacion'=>$fecha_creacion
							,'idproyecto'=>$idproyecto
							// ,'estado'=>$estado
							,'iddocente'=>$iddocente								
							);
			
			$this->oBD->update('rubrica ', $estados, array('idrubrica' => $id));
		    $this->terminarTransaccion('dat_rubrica_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rubrica").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idrubrica,nombre,fecha_creacion,idproyecto,estado,iddocente FROM rubrica  "
					. " WHERE idrubrica = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rubrica").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rubrica', array('idrubrica' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rubrica").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rubrica', array($propiedad => $valor), array('idrubrica' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rubrica").": " . $e->getMessage());
		}
	}
   
		
}