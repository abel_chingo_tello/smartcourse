/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost:3306
 Source Schema         : smart_courses_20201115

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : 65001

 Date: 21/11/2020 14:00:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menu_proyecto
-- ----------------------------
DROP TABLE IF EXISTS `menu_proyecto`;
CREATE TABLE `menu_proyecto`  (
  `idmenuproyecto` int(11) NOT NULL AUTO_INCREMENT,
  `idmenu` int(11) NULL DEFAULT NULL,
  `idproyecto` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `idpadre` int(11) NULL DEFAULT NULL,
  `insertar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT 'sera usado para nombre de menu cuando es contenedor',
  `modificar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT 'icono de menu cuando es contenedor',
  `eliminar` tinyint(1) NOT NULL DEFAULT 1,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_registro` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`idmenuproyecto`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_proyecto
-- ----------------------------
INSERT INTO `menu_proyecto` VALUES (1, 1, 7, 0, 1, 0, '1', '1', 1, 1, '2020-11-21 09:52:23');
INSERT INTO `menu_proyecto` VALUES (2, 1, 7, 0, 1, 0, '1', '1', 1, 1, '2020-11-21 09:52:23');
INSERT INTO `menu_proyecto` VALUES (3, 1, 7, 1, 1, NULL, '1', '1', 1, 1, '2020-11-21 10:04:13');
INSERT INTO `menu_proyecto` VALUES (4, 2, 7, 1, 2, NULL, '1', '1', 1, 1, '2020-11-21 10:17:37');
INSERT INTO `menu_proyecto` VALUES (5, NULL, 7, 1, 4, NULL, 'Comunidad', 'fa-comments-o', 1, 1, '2020-11-21 10:19:31');
INSERT INTO `menu_proyecto` VALUES (6, 3, 7, 1, 1, 5, '1', '1', 1, 1, '2020-11-21 10:24:29');
INSERT INTO `menu_proyecto` VALUES (7, 4, 7, 1, 2, 5, '1', '1', 1, 1, '2020-11-21 10:24:38');
INSERT INTO `menu_proyecto` VALUES (8, 5, 7, 1, 3, 5, '1', '1', 1, 1, '2020-11-21 10:24:47');
INSERT INTO `menu_proyecto` VALUES (9, NULL, 7, 1, 3, NULL, ' Curriculum', 'fa-handshake-o', 1, 1, '2020-11-21 12:46:00');
INSERT INTO `menu_proyecto` VALUES (10, 7, 7, 1, 1, 9, '1', '1', 1, 1, '2020-11-21 12:47:02');
INSERT INTO `menu_proyecto` VALUES (11, 8, 7, 1, 2, 9, '1', '1', 1, 1, '2020-11-21 12:47:15');
INSERT INTO `menu_proyecto` VALUES (12, 9, 7, 1, 3, 9, '1', '1', 1, 1, '2020-11-21 12:47:26');
INSERT INTO `menu_proyecto` VALUES (13, 12, 7, 1, 5, NULL, '1', '1', 1, 1, '2020-11-21 12:48:00');
INSERT INTO `menu_proyecto` VALUES (14, 13, 7, 1, 6, NULL, '1', '1', 1, 1, '2020-11-21 12:48:12');
INSERT INTO `menu_proyecto` VALUES (15, NULL, 7, 1, 7, NULL, ' Academic', 'fa-dashboard', 1, 1, '2020-11-21 12:48:50');
INSERT INTO `menu_proyecto` VALUES (16, 14, 7, 1, 1, 15, '1', '1', 1, 1, '2020-11-21 12:49:05');

SET FOREIGN_KEY_CHECKS = 1;
