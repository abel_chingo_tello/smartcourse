ALTER TABLE `sence_usuario`
	ADD COLUMN `codigosence` VARCHAR(11) NULL DEFAULT NULL AFTER `codigocurso`;

ALTER TABLE `acad_matricula`
	ADD COLUMN `codigocursosence` VARCHAR(11) NULL DEFAULT NULL COMMENT 'codigo del curso o ID accion de SENCE' AFTER `idcategoria`;