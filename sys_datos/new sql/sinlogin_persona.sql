/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost:3306
 Source Schema         : smart_skscourses_31-12-2020

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : 65001

 Date: 19/01/2021 17:13:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sinlogin_persona
-- ----------------------------
DROP TABLE IF EXISTS `sinlogin_persona`;
CREATE TABLE `sinlogin_persona`  (
  `idpersona` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `telefono` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `correo` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `fecha_registro` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `compania` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pais` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ciudad` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idpersona`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sinlogin_quiz
-- ----------------------------
DROP TABLE IF EXISTS `sinlogin_quiz`;
CREATE TABLE `sinlogin_quiz`  (
  `idnota` int(11) NOT NULL AUTO_INCREMENT,
  `idexamen` int(11) NULL DEFAULT NULL,
  `idpersona` bigint(20) NOT NULL,
  `tipo` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nota` float NULL DEFAULT NULL,
  `notatexto` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `regfecha` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `idproyecto` int(11) NOT NULL DEFAULT 0,
  `calificacion_en` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'P porcentaje; N:puntaje; A. alfanumerico ',
  `calificacion_total` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calificacion_min` int(11) NULL DEFAULT NULL,
  `tiempo_total` time(0) NULL DEFAULT '00:30:00' COMMENT 'tiempo que dura el examen',
  `tiempo_realizado` time(0) NOT NULL DEFAULT '00:15:00',
  `calificacion` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'U ultimanota; M mejor nota',
  `habilidades` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `habilidad_puntaje` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `intento` int(11) NOT NULL DEFAULT 1,
  `datos` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `preguntas` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idnota`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sinlogin_quizproyecto
-- ----------------------------
DROP TABLE IF EXISTS `sinlogin_quizproyecto`;
CREATE TABLE `sinlogin_quizproyecto`  (
  `idasignacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `idquiz` bigint(20) NOT NULL,
  `idproyecto` bigint(20) NOT NULL,
  `fecha_registro` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `user_registro` bigint(20) NOT NULL,
  `fecha_hasta` date NULL DEFAULT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idasignacion`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

