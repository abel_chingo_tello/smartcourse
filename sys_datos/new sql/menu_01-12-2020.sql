-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 01-12-2020 a las 13:58:16
-- Versión del servidor: 5.7.11
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `smart_skscourses_2020-10-30`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `idmenu` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text,
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  `url` varchar(150) DEFAULT NULL,
  `icono` varchar(100) DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`idmenu`, `nombre`, `descripcion`, `estado`, `url`, `icono`, `usuario_registro`, `fecha_registro`) VALUES
(1, 'My Briefcase', '', 1, 'portafolio', 'fa-archive', 1, '2020-11-11 15:01:46'),
(2, 'Courses', '', 1, 'proyecto/cursos', 'fa-book', 1, '2020-11-11 15:12:43'),
(3, 'Exams', '', 1, 'quiz', 'fa-at', 1, '2020-11-30 12:22:44'),
(4, 'Tasks', NULL, 1, 'tareas', 'fa-tasks', 1, '2020-12-01 08:32:53'),
(5, 'Projects', NULL, 1, 'proyectos_2', 'fa-folder', 1, '2020-12-01 08:37:04'),
(6, ' Reports', NULL, 1, 'reportes/', 'fa-line-chart', 1, '2020-12-01 08:38:19'),
(7, 'Messages', NULL, 1, 'mensajes/', 'fa-envelope', 1, '2020-12-01 08:39:22'),
(8, ' forums', NULL, 1, 'foros', 'fa-users', 1, '2020-12-01 08:40:12'),
(9, 'Video conference', NULL, 1, 'videosconferencia', 'fa-video-camera', 1, '2020-12-01 08:41:01'),
(10, 'Events', NULL, 1, 'eventos/', 'fa-calendar-check-o', 1, '2020-12-01 08:42:10'),
(11, ' Categories', NULL, 1, 'acad_categorias/', 'fa-cc', 1, '2020-12-01 08:42:49');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idmenu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `idmenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
