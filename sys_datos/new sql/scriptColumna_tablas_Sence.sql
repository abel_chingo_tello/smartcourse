ALTER TABLE `acad_grupoauladetalle`
	ADD COLUMN `idsence_assigncursos` INT(11) NULL DEFAULT NULL AFTER `idambiente`;

ALTER TABLE `acad_grupoaula`
	ADD COLUMN `tipogrupo` TINYINT(4) NULL DEFAULT '1' AFTER `tipodecurso`;

ALTER TABLE `acad_grupoaula`
	ADD COLUMN `codaccionsence` VARCHAR(11) NULL DEFAULT NULL AFTER `tipogrupo`;

CREATE TABLE IF NOT EXISTS `sence_cursovalor` (
  `id` int(11) NOT NULL,
  `idusuariosence` int(11) NOT NULL,
  `CodSence` varchar(11) DEFAULT NULL,
  `CodigoCurso` varchar(51) DEFAULT NULL,
  `IdSesionAlumno` varchar(150) DEFAULT NULL,
  `IdSesionSence` varchar(150) DEFAULT NULL,
  `RunAlumno` varchar(11) DEFAULT NULL,
  `FechaHora` datetime DEFAULT NULL,
  `ZonaHoraria` varchar(101) DEFAULT NULL,
  `LineaCapacitacion` int(11) DEFAULT NULL COMMENT 'Identificador de la línea de capacitación: 1 = Sistema Integrado de Capacitación 3 = Impulsa Personas.',
  `GlosaError` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT current_timestamp(),
  `fecha_actualizacion` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `sence_usuario` (
  `id` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `idrol` int(11) DEFAULT NULL,
  `idcurso` int(11) NOT NULL,
  `codigocurso` varchar(51) DEFAULT NULL,
  `codusuario` varchar(255) DEFAULT NULL,
  `tipo` tinyint(10) DEFAULT 1 COMMENT '1= Comenzar sesion 2= cierresesion',
  `url` varchar(255) DEFAULT NULL COMMENT 'URL del retorno a la hora de realizar los procesos',
  `url_cierre` varchar(255) DEFAULT NULL,
  `fecha_iniciosesion` datetime DEFAULT NULL,
  `fecha_cierresesion` datetime DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT current_timestamp(),
  `fecha_actualizacion` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `sence_cursovalor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idusuariosence` (`idusuariosence`);

ALTER TABLE `sence_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpersona` (`idpersona`,`idempresa`,`idproyecto`,`idrol`,`idcurso`),
  ADD KEY `idpersona_2` (`idpersona`);
  
--
-- AUTO_INCREMENT de la tabla `sence_usuario`
--
ALTER TABLE `sence_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sence_cursovalor`
--
ALTER TABLE `sence_cursovalor`
  ADD CONSTRAINT `sence_cursovalor_ibfk_1` FOREIGN KEY (`idusuariosence`) REFERENCES `sence_usuario` (`id`) ON DELETE NO ACTION;


CREATE TABLE `sence_assigncursos` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`idempresa` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'id de bolsa_empresa de smartcourse',
	`idproyecto` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'id proyecto de smartcourse',
	`idcurso` BIGINT(20) NULL DEFAULT '0' COMMENT 'id curso principal de smartcourse',
	`idcomplementario` BIGINT(20) NULL DEFAULT '0',
	`codsence` VARCHAR(51) NULL DEFAULT '-1' COMMENT 'Identificador del curso, mínimo 7 caracteres.',
	`estado` TINYINT(10) NULL DEFAULT '1' COMMENT '1= activo, 0 = inactivo',
	`fecha_creacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	`fecha_actualizacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COMMENT='Asignaciones de los cursos sence a empresas y proyectos'
COLLATE=utf8mb4_unicode_ci
ENGINE=InnoDB
;