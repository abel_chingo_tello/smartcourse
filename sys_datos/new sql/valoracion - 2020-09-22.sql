ALTER TABLE `valoracion` DROP FOREIGN KEY `fk_valoracion_acad_curso_complementario`;
ALTER TABLE `valoracion` ADD `idgrupoauladetalle` bigint(20) NULL AFTER `comentario`;

#ELIMINACION DE REGISTROS DE USUARIOS DE PRUEBAS
DELETE FROM  `valoracion` WHERE idalumno IN(2189,2186);
DELETE FROM  `historial_sesion` WHERE idusuario IN(2189,2186);

#este script faltaba
ALTER TABLE `historial_sesion` ADD `idgrupoauladetalle` bigint(20) NULL AFTER `idcc`;


#24-09-2020
ALTER TABLE `notas_quiz` ADD `idgrupoauladetalle` bigint(20) NULL AFTER `idcomplementario`

#25-09-2020
ALTER TABLE `tareas` ADD `idgrupoauladetalle` bigint(20) NULL AFTER `vencido`

#28-09-2020
ALTER TABLE `bitacora_alumno_smartbook` ADD  `idgrupoauladetalle` bigint(20) NULL AFTER `idcomplementario`

#29-09-2020
ALTER TABLE `bitacora_alumno_smartbook_se` ADD  `idgrupoauladetalle` bigint(20) NULL AFTER `idproyecto`

#30-09-2020
ALTER TABLE `bitacora_smartbook` ADD  `idgrupoauladetalle` bigint(20) NULL AFTER `idcomplementario`

#01-10-2020
ALTER TABLE `bitacora_smartbook_se` ADD `idgrupoauladetalle` bigint(20) NULL AFTER `idproyecto`

#02-10-2020
ALTER TABLE `actividad_alumno` ADD `idgrupoauladetalle` bigint(20) NULL AFTER `idcc`


#30-09-2020
#Agregar Nuevos campos a Smarquiz
ALTER TABLE `examenes` ADD `tiempoalinicio` TINYINT(1) NULL DEFAULT '0' AFTER `idioma`, ADD `retroceder` TINYINT(1) NULL DEFAULT '1' AFTER `tiempoalinicio`;
