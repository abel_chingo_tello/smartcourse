-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-04-2021 a las 21:55:58
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `smart_skscourses`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descargas_asignadas`
--

CREATE TABLE `descargas_asignadas` (
  `id` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `total_asignado` int(11) NOT NULL DEFAULT 1,
  `fecha_registro` datetime NOT NULL DEFAULT current_timestamp(),
  `codigo_suscripcion` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descargas_consumidas`
--

CREATE TABLE `descargas_consumidas` (
  `iddescargaconsumida` bigint(20) NOT NULL,
  `idasignado` bigint(20) NOT NULL,
  `archivo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idcurso` bigint(20) NOT NULL,
  `strcurso` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT current_timestamp(),
  `idcc` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `descargas_asignadas`
--
ALTER TABLE `descargas_asignadas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_asignado_persona` (`idpersona`);

--
-- Indices de la tabla `descargas_consumidas`
--
ALTER TABLE `descargas_consumidas`
  ADD PRIMARY KEY (`iddescargaconsumida`),
  ADD KEY `fk_consumido_asignado` (`idasignado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `descargas_asignadas`
--
ALTER TABLE `descargas_asignadas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `descargas_consumidas`
--
ALTER TABLE `descargas_consumidas`
  MODIFY `iddescargaconsumida` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `descargas_asignadas`
--
ALTER TABLE `descargas_asignadas`
  ADD CONSTRAINT `fk_asignado_persona` FOREIGN KEY (`idpersona`) REFERENCES `personal` (`idpersona`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `descargas_consumidas`
--
ALTER TABLE `descargas_consumidas`
  ADD CONSTRAINT `fk_consumido_asignado` FOREIGN KEY (`idasignado`) REFERENCES `descargas_asignadas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE `descargas_asignadas` ADD `idempresa` BIGINT NOT NULL AFTER `codigo_suscripcion`, ADD `idproyecto` BIGINT NOT NULL AFTER `idempresa`;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
