/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost:3306
 Source Schema         : smart_courses_20201115

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : 65001

 Date: 21/11/2020 13:59:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `idmenu` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1,
  `url` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icono` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_registro` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`idmenu`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'Courses', NULL, 1, 'proyecto/cursos', 'fa-book', 1, '2020-11-21 09:41:44');
INSERT INTO `menu` VALUES (2, 'My Briefcase', '', 1, 'portafolio/', 'fa-archive', 1, '2020-11-21 09:43:25');
INSERT INTO `menu` VALUES (3, 'Messages', NULL, 1, 'mensajes/', 'fa-comment', 1, '2020-11-21 10:21:21');
INSERT INTO `menu` VALUES (4, '\r\nForums', '', 1, 'foros/', 'fa-users', 1, '2020-11-21 10:21:47');
INSERT INTO `menu` VALUES (5, 'Video conference', NULL, 1, 'videosconferencia', 'fa-video-camera', 1, '2020-11-21 10:23:02');
INSERT INTO `menu` VALUES (6, 'Reports', NULL, 1, 'reportes/', 'fa-area-chart', 1, '2020-11-21 12:02:09');
INSERT INTO `menu` VALUES (7, 'Competencies', NULL, 1, 'acad_competencias/', 'fa-bank', 1, '2020-11-21 12:04:43');
INSERT INTO `menu` VALUES (8, 'Capabilities', NULL, 1, 'acad_capacidades/', 'fa-bank', 1, '2020-11-21 12:05:39');
INSERT INTO `menu` VALUES (9, '\r\nCriteria or performances', NULL, 1, 'acad_criterios/', 'fa-bank', 1, '2020-11-21 12:06:56');
INSERT INTO `menu` VALUES (10, 'Slider', 'Familia slider', 1, 'familia_slider/', 'fa-image', 1, '2020-11-21 12:08:20');
INSERT INTO `menu` VALUES (11, 'Forums', 'foros para  modulo de familia', 1, 'familia_foros/', 'fa-users', 1, '2020-11-21 12:12:08');
INSERT INTO `menu` VALUES (12, 'Events', NULL, 1, 'eventos/', 'fa-calendar-check-o', 1, '2020-11-21 12:13:07');
INSERT INTO `menu` VALUES (13, 'Exams', NULL, 1, 'quiz/', 'fa-at', 1, '2020-11-21 12:15:01');
INSERT INTO `menu` VALUES (14, 'Categorías', NULL, 1, 'acad_categorias/', 'fa-suitcase', 1, '2020-11-21 12:16:11');

SET FOREIGN_KEY_CHECKS = 1;
