-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 18-01-2021 a las 20:55:41
-- Versión del servidor: 5.7.11
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `smart_skscourses_31-12-2020`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sinlogin_persona`
--

CREATE TABLE `sinlogin_persona` (
  `idpersona` bigint(20) NOT NULL,
  `nombres` varchar(250) NOT NULL,
  `telefono` varchar(150) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sinlogin_persona`
--

INSERT INTO `sinlogin_persona` (`idpersona`, `nombres`, `telefono`, `correo`, `idproyecto`, `fecha_registro`) VALUES
(1, 'asdasd', 'asdasd', 'abelchingo@gmail.com', 7, '2021-01-15 18:20:16'),
(2, 'asdasd', 'asdads', 'abelchingo@gmail.com', 7, '2021-01-15 18:25:14'),
(3, 'aasdasd', 'asdasd', 'abelchingo@gmail.com', 7, '2021-01-15 18:27:19'),
(4, 'asdasd', 'asdasd', 'abelchingo@gmail.com', 7, '2021-01-15 18:27:42'),
(5, 'asdasd', 'asdasd', 'abelchingo@gmail.com', 7, '2021-01-15 18:59:49'),
(6, 'abel Chingo Tello', '979709503', 'abelchingo@gmail.com', 7, '2021-01-15 20:57:46'),
(7, 'abel Chingo Tello', '979709503', 'abelchingo@gmail.com', 27, '2021-01-15 21:13:09'),
(8, 'abel Chingo Tello', '979709503', 'abelchingo@gmail.com', 27, '2021-01-18 11:53:19'),
(9, 'abel Chingo Tello', '979709503', 'abelchingo@gmail.com', 27, '2021-01-18 12:36:19'),
(10, 'abel Chingo Tello', '979709503', 'abelchingo@gmail.com', 27, '2021-01-18 12:37:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sinlogin_quiz`
--

CREATE TABLE `sinlogin_quiz` (
  `idnota` int(11) NOT NULL,
  `idexamen` int(11) DEFAULT NULL,
  `idpersona` bigint(20) NOT NULL,
  `tipo` char(1) CHARACTER SET utf8 NOT NULL,
  `nota` float DEFAULT NULL,
  `notatexto` text CHARACTER SET utf8,
  `regfecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idproyecto` int(11) NOT NULL DEFAULT '0',
  `calificacion_en` char(1) CHARACTER SET utf8 DEFAULT NULL COMMENT 'P porcentaje; N:puntaje; A. alfanumerico ',
  `calificacion_total` text CHARACTER SET utf8 NOT NULL,
  `calificacion_min` int(11) DEFAULT NULL,
  `tiempo_total` time DEFAULT '00:30:00' COMMENT 'tiempo que dura el examen',
  `tiempo_realizado` time NOT NULL DEFAULT '00:15:00',
  `calificacion` char(1) CHARACTER SET utf8 DEFAULT NULL COMMENT 'U ultimanota; M mejor nota',
  `habilidades` longtext CHARACTER SET utf8,
  `habilidad_puntaje` text CHARACTER SET utf8,
  `intento` int(11) NOT NULL DEFAULT '1',
  `datos` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `preguntas` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `estado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sinlogin_quiz`
--

INSERT INTO `sinlogin_quiz` (`idnota`, `idexamen`, `idpersona`, `tipo`, `nota`, `notatexto`, `regfecha`, `idproyecto`, `calificacion_en`, `calificacion_total`, `calificacion_min`, `tiempo_total`, `tiempo_realizado`, `calificacion`, `habilidades`, `habilidad_puntaje`, `intento`, `datos`, `preguntas`, `estado`) VALUES
(1, 2182, 5, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 20:54:23', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:53', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":5,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A5_I1.php\"}', -1),
(2, 2182, 5, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 20:55:15', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:53', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":5,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A5_I1.php\"}', -1),
(3, 2182, 5, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 20:56:22', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:53', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":5,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A5_I1.php\"}', -1),
(4, 2182, 5, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 20:56:34', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:53', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":5,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A5_I1.php\"}', -1),
(5, 2182, 6, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 20:58:11', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:16', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":6,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A6_I1.php\"}', 1),
(6, 2182, 6, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 21:00:15', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:44', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":6,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A6_I1.php\"}', -1),
(7, 2182, 6, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 21:00:52', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:44', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":6,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A6_I1.php\"}', -1),
(8, 2182, 6, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 21:03:09', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:44', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":6,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A6_I1.php\"}', -1),
(9, 2182, 6, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 21:03:50', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:44', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":6,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A6_I1.php\"}', -1),
(10, 2182, 6, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 21:04:28', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:44', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":6,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A6_I1.php\"}', -1),
(11, 2182, 6, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 21:05:33', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:44', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":6,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A6_I1.php\"}', -1),
(12, 2182, 6, 'U', 0, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>0.00pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 21:08:38', 7, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:44', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"0\",\"5\":\"0\",\"6\":\"0\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":6,\"idproyecto\":\"7\",\"idempresa\":\"10\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E10_P7\\\\EX_2182_A6_I1.php\"}', -1),
(13, 2182, 7, 'U', 3.65, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>3.65pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-15 21:14:08', 27, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:00:54', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"10\",\"5\":\"3\",\"6\":\"1\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":7,\"idproyecto\":\"27\",\"idempresa\":\"27\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E27_P27\\\\EX_2182_A7_I1.php\"}', -1),
(14, 2182, 8, 'U', 3.23, '<div><h2></h2><h3> Your result is located on the scale: \"<b>A1</b>\". </h3><h4>3.23pts out of 100 </h4><br><div class=\"alert alert-warning\" style=\"padding:1em;\" id=\"infopreguntasoffline\"><b>Remember</b> The percentage shown is not your final grade. Some writing questions and all the speaking ones still have to be reviewed by your teacher. After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report.</div><br></div>', '2021-01-18 11:54:44', 27, 'A', '[{\"max\":\"100\",\"min\":\"81\",\"nombre\":\"C1\"},{\"max\":\"80\",\"min\":\"61\",\"nombre\":\"B2\"},{\"max\":\"60\",\"min\":\"41\",\"nombre\":\"B1\"},{\"max\":\"40\",\"min\":\"21\",\"nombre\":\"A2\"},{\"max\":\"20\",\"min\":\"0\",\"nombre\":\"A1\"}]', NULL, '01:00:00', '00:01:20', 'U', '[{\"skill_id\":\"4\",\"skill_name\":\"Listening\"},{\"skill_id\":\"5\",\"skill_name\":\"Reading\"},{\"skill_id\":\"6\",\"skill_name\":\"Writing\"},{\"skill_id\":\"7\",\"skill_name\":\"Speaking\"}]', '{\"4\":\"3\",\"5\":\"9\",\"6\":\"1\",\"7\":\"0\"}', 1, '{\"tipo\":\"U\",\"num_examen\":0}', '{\"idpersona\":8,\"idproyecto\":\"27\",\"idempresa\":\"27\",\"archivo\":\"static\\\\media\\\\examenes_sinlogin\\\\E27_P27\\\\EX_2182_A8_I1.php\"}', -1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sinlogin_persona`
--
ALTER TABLE `sinlogin_persona`
  ADD PRIMARY KEY (`idpersona`);

--
-- Indices de la tabla `sinlogin_quiz`
--
ALTER TABLE `sinlogin_quiz`
  ADD PRIMARY KEY (`idnota`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sinlogin_persona`
--
ALTER TABLE `sinlogin_persona`
  MODIFY `idpersona` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `sinlogin_quiz`
--
ALTER TABLE `sinlogin_quiz`
  MODIFY `idnota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
