<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-11-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatBitacora_alumno_smartbook extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM bitacora_alumno_smartbook";
			
			$cond = array();		
			
			if(isset($filtros["idbitacora_smartbook"])) {
					$cond[] = "idbitacora_smartbook = " . $this->oBD->escapar($filtros["idbitacora_smartbook"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			/*if(!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}*/
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idbitacora_smartbook, idcurso, idsesion, idusuario, estado, regfecha, idcomplementario,idgrupoauladetalle FROM bitacora_alumno_smartbook";			
			
			$cond = array();		
					
			
			if(isset($filtros["idbitacora_smartbook"])) {
					$cond[] = "idbitacora_smartbook = " . $this->oBD->escapar($filtros["idbitacora_smartbook"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			/*if(!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}*/
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["idcomplementario"])) {
					$cond[] = "idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["fechaDesde"]) && !empty($filtros["fechaDesde"])) {
				$cond[] = "regfecha >= " . $this->oBD->escapar($filtros["fechaDesde"]);
			}
			if(isset($filtros["fechaHasta"]) && !empty($filtros["fechaHasta"])) {
				$cond[] = "regfecha <= " . $this->oBD->escapar($filtros["fechaHasta"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$idsesion,$idusuario,$estado,$regfecha,$idcomplementario=0, $idgrupoauladetalle=null)
	{
		try {
			if(empty($idusuario)) return 0;
			//$this->iniciarTransaccion('dat_bitacora_alumno_smartbook_insert');			
			$hay=$this->buscar(array('idcurso'=>$idcurso,'idsesion'=>$idsesion,'idusuario'=>$idusuario,'idgrupoauladetalle'=>$idgrupoauladetalle));
			if(!empty($hay[0])) return $hay[0]["idbitacora_smartbook"];

			/*$estados=array((!empty($idcurso)?$idcurso:0),
				!empty($idsesion)?$idsesion:0, 
				$idusuario, 
				!empty($idcomplementario)?$idcomplementario:0,				
				$this->oBD->escapar($estado), 				
				$this->oBD->escapar(!empty($regfecha)?$regfecha:date('Y-m-d'))				
				
			);
			$sql="CALL insertarbitacoraalumnosmartbook (".(implode(",",$estados)).")";
			if($idusuario==1) { 
				$id= $this->oBD->consultarEscalarSQL($sql);
				return $id;
			}*/			

			$id = $this->oBD->consultarEscalarSQL("SELECT idbitacora_smartbook FROM bitacora_alumno_smartbook ORDER BY idbitacora_smartbook DESC LIMIT 0,1");
			++$id;
			$estados = array('idbitacora_smartbook' => $id							
							,'idcurso'=>$idcurso
							,'idsesion'=>$idsesion
							,'idusuario'=>$idusuario
							,'idcomplementario'=>$idcomplementario
							,'estado'=>$estado
							,'regfecha'=>$regfecha							
							);
			if(!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"]=$idgrupoauladetalle;
			$this->oBD->insert('bitacora_alumno_smartbook', $estados);		
			//$this->terminarTransaccion('dat_bitacora_alumno_smartbook_insert');
			return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_bitacora_alumno_smartbook_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$idsesion,$idusuario,$estado,$regfecha,$idcomplementario=0, $idgrupoauladetalle=null)
	{
		try {
			if(empty($idusuario)) return $id;
			//$this->iniciarTransaccion('dat_bitacora_alumno_smartbook_update');
			$estados = array('idcurso'=>!empty($idcurso)?$idcurso:0
							,'idsesion'=>!empty($idsesion)?$idsesion:0
							,'idusuario'=>$idusuario
							,'idcomplementario'=>!empty($idcomplementario)?$idcomplementario:0
							,'estado'=>!empty($estado)?$estado:0
							,'regfecha'=>!empty($regfecha)?$regfecha:date('Y-m-d')				
							);
			if(!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"]=$idgrupoauladetalle;
			
			$this->oBD->update('bitacora_alumno_smartbook ', $estados, array('idbitacora_smartbook' => $id));
		    //$this->terminarTransaccion('dat_bitacora_alumno_smartbook_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idbitacora_smartbook, idcurso, idsesion, idusuario, estado, regfecha, idcomplementario  FROM bitacora_alumno_smartbook , idgrupoauladetalle  WHERE idbitacora_smartbook = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function multidelete($ids){
		try{
			$this->iniciarTransaccion('dat_multiple_delete');
			$sql = "DELETE FROM bitacora_alumno_smartbook WHERE idbitacora_smartbook IN (".implode(',', $ids).")";
			$this->oBD->ejecutarSQL($sql);
			$this->terminarTransaccion('dat_multiple_delete');
			return true;
		}catch(Exception $e) {
			$this->cancelarTransaccion('dat_multiple_delete');
			throw new Exception("ERROR\n".JrTexto::_("Multidelete")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}
	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bitacora_alumno_smartbook', array('idbitacora_smartbook' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bitacora_alumno_smartbook', array($propiedad => $valor), array('idbitacora_smartbook' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
   
		
}