<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatSis_configuracion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Sis_configuracion").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM sis_configuracion";			
			
			$cond = array();		
					
			
			if(isset($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}
			if(isset($filtros["autocargar"])) {
					$cond[] = "autocargar = " . $this->oBD->escapar($filtros["autocargar"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Sis_configuracion").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($valor,$autocargar)
	{
		try {
			
			$this->iniciarTransaccion('dat_sis_configuracion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(config) FROM sis_configuracion");
			++$id;
			
			$estados = array('config' => $id
							
							,'valor'=>$valor
							,'autocargar'=>$autocargar							
							);
			
			$this->oBD->insert('sis_configuracion', $estados);			
			$this->terminarTransaccion('dat_sis_configuracion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_sis_configuracion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Sis_configuracion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $valor,$autocargar)
	{
		try {
			$this->iniciarTransaccion('dat_sis_configuracion_update');
			$estados = array('valor'=>$valor
							,'autocargar'=>$autocargar								
							);
			
			$this->oBD->update('sis_configuracion ', $estados, array('config' => $id));
		    $this->terminarTransaccion('dat_sis_configuracion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sis_configuracion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM sis_configuracion  "
					. " WHERE config = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Sis_configuracion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('sis_configuracion', array('config' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Sis_configuracion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('sis_configuracion', array($propiedad => $valor), array('config' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sis_configuracion").": " . $e->getMessage());
		}
	}
   
		
}