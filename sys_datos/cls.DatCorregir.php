<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-04-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
ini_set('memory_limit', '-1');
class DatCorregir extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}

	public function historial_sesion($desde=0){
		try{
			$this->oBD->conlimit=false;
			$sql="SELECT idhistorialsesion,idusuario as idalumno,idcurso,idcc,h.idproyecto , concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as alumno,p.usuario,p.dni,p.rol FROM `historial_sesion` h INNER JOIN personal p ON h.idusuario=p.idpersona WHERE idcurso!=0 AND idcurso IS NOT NULL AND idcc IS NULL AND idhistorialsesion>".$desde." ORDER BY idhistorialsesion ASC LIMIT 0,10";
			$porcorregir=$this->oBD->consultarSQL($sql);
			$ncorregido=0;
			$nregistros=0;
			$sql2tmp=array();
			if(!empty($porcorregir)){
				$unosolo=array('xsrr');
				foreach ($porcorregir as $p => $his){
					$sql2="SELECT m.idgrupoauladetalle as idgrupoauladetalle ,gd.idcomplementario  as idcc,gd.idcurso,ga.idproyecto FROM `acad_matricula` m INNER JOIN acad_grupoauladetalle gd on m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula WHERE idalumno=".$his["idalumno"]." AND (idcurso=".$his["idcurso"]." OR gd.idcomplementario=".$his["idcurso"].") LIMIT 0,1";
					$unosoloindex=$his["idcurso"].'-'.(!empty($his["idcc"])?$his["idcc"]:0).'-'.$his["idalumno"];
					if(!in_array($unosoloindex, $unosolo)){
						array_push($unosolo, $unosoloindex);
						$datos=$this->oBD->consultarSQL($sql2);
						if(!empty($datos[0])){
							$estados=array('idgrupoauladetalle'=>$datos[0]["idgrupoauladetalle"],'idcc'=>$datos[0]["idcc"],'idcurso'=>$datos[0]["idcurso"]);
							$estados['idproyecto'] = $datos[0]["idproyecto"];
							$nregistros=$this->oBD->update('historial_sesion', $estados, array('idusuario'=>$his["idalumno"],'idcurso'=>$his["idcurso"]));
							$ncorregido=$ncorregido+$nregistros;
						}
						$desde=$his["idhistorialsesion"];
						$sql2tmp[]=$sql2;
					}
				}
				echo json_encode(array('code'=>200,'ncorregidos'=>$ncorregido,'desde'=>$desde,'sql1'=>$sql,'sql2'=>$sql2tmp,'termino'=>0));
			}else{
				echo json_encode(array('code'=>200,'ncorregidos'=>$ncorregido,'desde'=>$desde,'sql1'=>'termino','sql2'=>'termino','termino'=>1));
			}
			exit(0);
		}catch(Exception $ex){
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);
		}
	}

	public function valoracion($desde){
		try{
			$this->oBD->conlimit=false;
			$sql="SELECT idvaloracion,idalumno as idalumno,idcurso,idcomplementario,v.idgrupoauladetalle ,ga.idproyecto , concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as alumno,p.usuario,p.dni,p.rol FROM `valoracion` v INNER JOIN personal p ON v.idalumno=p.idpersona INNER JOIN acad_grupoaula ga on v.idgrupoaula=ga.idgrupoaula WHERE  v.idgrupoauladetalle IS NULL AND idvaloracion>".$desde." ORDER BY idvaloracion ASC LIMIT 0,10";
			$porcorregir=$this->oBD->consultarSQL($sql);
			if(empty($porcorregir)){
				echo json_encode(array('code'=>200,'ncorregidos'=>0,'desde'=>$desde,'sql1'=>'Ningun Registro a corregir','sql2'=>'Ningun Registro a corregir','termino'=>1));
				exit();
			}
			$ncorregido=0;
			$nregistros=0;
			$sql2tmp=array();
			foreach ($porcorregir as $p => $corregir){
				$sql2="SELECT m.idgrupoauladetalle as idgrupoauladetalle ,gd.idcomplementario  as idcomplementario,gd.idcurso,ga.idproyecto FROM `acad_matricula` m INNER JOIN acad_grupoauladetalle gd on m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula WHERE idalumno=".$corregir["idalumno"]." AND idcurso=".$corregir["idcurso"]." AND gd.idcomplementario=".$corregir["idcomplementario"]."  limit 0,1";
				  	$datos=$this->oBD->consultarSQL($sql2);
				  	if(!empty($datos[0])){
						$estados=array('idgrupoauladetalle'=>$datos[0]["idgrupoauladetalle"]);
						$nregistros=$this->oBD->update('valoracion', $estados, array('idvaloracion' => $corregir["idvaloracion"],'idalumno'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"]));
						$ncorregido=$ncorregido+$nregistros;
					}
					$desde=$corregir["idvaloracion"];
					$sql2tmp[]=$sql2;
			}
			echo json_encode(array('code'=>200,'ncorregidos'=>$ncorregido,'desde'=>$desde,'sql1'=>$sql,'sql2'=>$sql2tmp,'termino'=>0));
			exit(0);
		}catch(Exception $ex){
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);
		}
	}

	public function notas_quiz($desde){
		try{

			$this->oBD->conlimit=false;
			$sql="SELECT idnota as id,idalumno as idalumno,idexamenproyecto ,preguntas,idcursodetalle, idrecurso,idcurso,idcomplementario,v.idproyecto,idempresa  , concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as alumno,p.usuario,p.dni,p.rol FROM notas_quiz v INNER JOIN personal p ON v.idalumno=p.idpersona  INNER JOIN proyecto pr ON v.idproyecto=pr.idproyecto  WHERE  v.idgrupoauladetalle IS NULL AND idnota>".$desde." ORDER BY idnota ASC LIMIT 0,5";
			$porcorregir=$this->oBD->consultarSQL($sql);
			if(empty($porcorregir)){
				echo json_encode(array('code'=>200,'ncorregidos'=>0,'desde'=>$desde,'sql1'=>'Ningun Registro a corregir','sql2'=>'Ningun Registro a corregir','termino'=>1));
				exit();
			}
			
			$ncorregido=0;
			$nregistros=0;
			$sql2tmp=array();
			foreach ($porcorregir as $p => $corregir){
				$sql2="SELECT m.idgrupoauladetalle as idgrupoauladetalle ,gd.idcomplementario  as idcomplementario,gd.idcurso,ga.idproyecto FROM `acad_matricula` m INNER JOIN acad_grupoauladetalle gd on m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula WHERE idalumno=".$corregir["idalumno"]." AND idcurso=".$corregir["idcurso"]." AND gd.idcomplementario=".$corregir["idcomplementario"]." AND ga.idproyecto=".$corregir["idproyecto"]."  limit 0,1";
					$datos=$this->oBD->consultarSQL($sql2);

				  	if(!empty($datos[0])){
						$estados=array('idgrupoauladetalle'=>$datos[0]["idgrupoauladetalle"]);

						if($corregir["idexamenproyecto"]>-100){
							$archivo=RUTA_BASE.'static'.SD.'media'.SD.'examenes';
							$archivo=$archivo.SD.'E'.$corregir["idempresa"]."_P".$corregir["idproyecto"];
							@mkdir($archivo,0777,true);
							@chmod($archivo, 0777);
							$archivo=$archivo.SD.'curso_'.$corregir["idcurso"]."_".$corregir["idcomplementario"];
							@mkdir($archivo,0777,true);
							@chmod($archivo, 0777);
							$archivo=$archivo.SD.'ICD_'.$corregir["idcursodetalle"].'EX_'.$corregir["idrecurso"]."_A".$corregir["idalumno"]."_PE00_ID".$corregir["id"].".php";
							if(file_exists($archivo)) {
								@rename($archivo,$archivo."_".date('YMDHis'));
								@chmod($archivo, 0777);
							}
							$fp = fopen($archivo, "a");
							$txtpreguntas=base64_encode(preg_replace('/\s+/', ' ', $corregir["preguntas"]));
							$textexamen = <<<STREXAMEN
<?php 
\$preguntas=<<<STRCHINGO
	$txtpreguntas	
STRCHINGO;
\$preguntas=base64_decode(trim(\$preguntas));
?>
STREXAMEN;
$write = fputs($fp,$textexamen);
			fclose($fp);
			@chmod($archivo,0777);

						$datosuser=array('_user_'=>'SYSTEM','idpersona'=>$corregir["idalumno"],"idproyecto"=>$corregir["idproyecto"]);
						$datosuser["idempresa"]=$corregir["idempresa"];
						$datosuser["idrol"]=3;
						$datosuser["idcurso"]=$corregir["idcurso"];
						$datosuser["idcc"]=$corregir["idcomplementario"];
						$datosuser["idcursodeatlle"]=$corregir["idcursodetalle"];
						$datosuser["idHistSesion"]=-1;
						$pos1=stripos($archivo,'static'.SD.'media'.SD.'examenes');
						if ($pos1 !== false) {
							$archivo=substr($archivo,$pos1);
						}
						$datosuser["archivo"]=$archivo;
						$estados['preguntas']=json_encode($datosuser);
						$estados['idexamenproyecto']=-100;
						}
						$nregistros=$this->oBD->update('notas_quiz', $estados, array('idnota' => $corregir["id"],'idalumno'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"]));
						$ncorregido=$ncorregido+$nregistros;
					}
					$desde=$corregir["id"];
					$sql2tmp[]=$sql2;
			}
			echo json_encode(array('code'=>200,'ncorregidos'=>$ncorregido,'desde'=>$desde,'sql1'=>$sql,'sql2'=>$sql2tmp,'termino'=>0));
			exit(0);
			}catch(Exception $ex){				
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);
		}

	}

	public function tareas($desde){
		try{

			$this->oBD->conlimit=false;
			$sql="SELECT idtarea as id,idalumno as idalumno,recurso,idcurso,idcomplementario,idproyecto , concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as alumno,p.usuario,p.dni,p.rol FROM tareas v INNER JOIN personal p ON v.idalumno=p.idpersona  WHERE  v.idgrupoauladetalle IS NULL AND idtarea>".$desde." ORDER BY idtarea ASC LIMIT 0,10";
			$porcorregir=$this->oBD->consultarSQL($sql);
			if(empty($porcorregir)){
				echo json_encode(array('code'=>200,'ncorregidos'=>0,'desde'=>$desde,'sql1'=>'Ningun Registro a corregir','sql2'=>'Ningun Registro a corregir','termino'=>1));
				exit();
			}
			
			$ncorregido=0;
			$nregistros=0;
			$sql2tmp=array();
			foreach ($porcorregir as $p => $corregir){
				$sql2="SELECT m.idgrupoauladetalle as idgrupoauladetalle ,gd.idcomplementario  as idcomplementario,gd.idcurso,ga.idproyecto FROM `acad_matricula` m INNER JOIN acad_grupoauladetalle gd on m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula WHERE idalumno=".$corregir["idalumno"]." AND idcurso=".$corregir["idcurso"]." AND gd.idcomplementario=".$corregir["idcomplementario"]." AND ga.idproyecto=".$corregir["idproyecto"]."  limit 0,1";
					$datos=$this->oBD->consultarSQL($sql2);

				  	if(!empty($datos[0])){
						$estados=array('idgrupoauladetalle'=>$datos[0]["idgrupoauladetalle"]);
						$recurso=$corregir["recurso"];
						if(!empty($recurso)){
							$pos1=stripos($recurso,'static');
							if ($pos1 !== false) {
								$recurso=substr($recurso,$pos1);
							}
							$estados['recurso']=$recurso;
						}
						$nregistros=$this->oBD->update('tareas', $estados, array('idtarea' => $corregir["id"],'idalumno'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"]));
						$ncorregido=$ncorregido+$nregistros;
					}
					$desde=$corregir["id"];
					$sql2tmp[]=$sql2;
			}
			echo json_encode(array('code'=>200,'ncorregidos'=>$ncorregido,'desde'=>$desde,'sql1'=>$sql,'sql2'=>$sql2tmp,'termino'=>0));
			exit(0);
		}catch(Exception $ex){				
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);
		}
	}

	public function bitacora_alumno_smartbook($desde){
		try{
			$this->oBD->conlimit=false;
			$sql="SELECT idbitacora_smartbook as id,idusuario as idalumno,idcurso,idcomplementario , concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as alumno,p.usuario,p.dni FROM bitacora_alumno_smartbook v INNER JOIN personal p ON v.idusuario=p.idpersona  WHERE  v.idgrupoauladetalle IS NULL AND idbitacora_smartbook>".$desde." ORDER BY idbitacora_smartbook ASC LIMIT 0,10";
			$porcorregir=$this->oBD->consultarSQL($sql);
			if(empty($porcorregir)){
				echo json_encode(array('code'=>200,'ncorregidos'=>0,'desde'=>$desde,'sql1'=>'Ningun Registro a corregir','sql2'=>'Ningun Registro a corregir','termino'=>1));
				exit();
			}
			$ncorregido=0;
			$nregistros=0;
			$sql2tmp=array();
			foreach ($porcorregir as $p => $corregir){
				$sql2="SELECT m.idgrupoauladetalle as idgrupoauladetalle ,gd.idcomplementario  as idcomplementario,gd.idcurso,ga.idproyecto FROM `acad_matricula` m INNER JOIN acad_grupoauladetalle gd on m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula WHERE idalumno=".$corregir["idalumno"]." AND idcurso=".$corregir["idcurso"]." AND gd.idcomplementario=".$corregir["idcomplementario"]."  limit 0,1";
					$datos=$this->oBD->consultarSQL($sql2);
				  	if(!empty($datos[0])){
						$estados=array('idgrupoauladetalle'=>$datos[0]["idgrupoauladetalle"]);						
						$estados['idcomplementario']=$datos[0]["idcomplementario"];

						$nregistros=$this->oBD->update('bitacora_alumno_smartbook', $estados, array('idbitacora_smartbook' => $corregir["id"],'idusuario'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"],'idcomplementario'=>$corregir["idcomplementario"]));
						$ncorregido=$ncorregido+$nregistros;

						$nregistros=$this->oBD->update('bitacora_smartbook', $estados, array('idbitacora_alum_smartbook' => $corregir["id"],'idusuario'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"],'idcomplementario'=>$corregir["idcomplementario"]));
						$ncorregido=$ncorregido+$nregistros;
					}
					$desde=$corregir["id"];
					$sql2tmp[]=$sql2;
			}
			echo json_encode(array('code'=>200,'ncorregidos'=>$ncorregido,'desde'=>$desde,'sql1'=>$sql,'sql2'=>$sql2tmp,'termino'=>0));
			exit(0);
		}catch(Exception $ex){				
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);
		}
	}



	public function bitacora_alumno_smartbook_se($desde){
		try{
			$this->oBD->conlimit=false;
			$sql="SELECT idbitacora_smartbook as id,idusuario as idalumno, idcurso_sc as idcurso,idcc as idcomplementario , concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as alumno,p.usuario,p.dni FROM bitacora_alumno_smartbook_se v INNER JOIN personal p ON v.idusuario=p.idpersona  WHERE  v.idgrupoauladetalle IS NULL AND idbitacora_smartbook>".$desde." ORDER BY idbitacora_smartbook ASC LIMIT 0,10";
			$porcorregir=$this->oBD->consultarSQL($sql);
			if(empty($porcorregir)){
				echo json_encode(array('code'=>200,'ncorregidos'=>0,'desde'=>$desde,'sql1'=>'Ningun Registro a corregir','sql2'=>'Ningun Registro a corregir','termino'=>1));
				exit();
			}
			$ncorregido=0;
			$nregistros=0;
			$sql2tmp=array();
			foreach ($porcorregir as $p => $corregir){
				$sql2="SELECT m.idgrupoauladetalle as idgrupoauladetalle ,gd.idcomplementario  as idcomplementario,gd.idcurso,ga.idproyecto FROM `acad_matricula` m INNER JOIN acad_grupoauladetalle gd on m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula WHERE idalumno=".$corregir["idalumno"]." AND idcurso=".$corregir["idcurso"]." AND gd.idcomplementario=".$corregir["idcomplementario"]."  limit 0,1";
					$datos=$this->oBD->consultarSQL($sql2);

				  	if(!empty($datos[0])){
						$estados=array('idgrupoauladetalle'=>$datos[0]["idgrupoauladetalle"]);						
						//$estados['idcc']=$datos[0]["idcomplementario"]);
						$nregistros=$this->oBD->update('bitacora_alumno_smartbook_se', $estados, array('idbitacora_smartbook' => $corregir["id"],'idusuario'=>$corregir["idalumno"],'idcurso_sc'=>$corregir["idcurso"],'idcc'=>$corregir["idcomplementario"]));
						$ncorregido=$ncorregido+$nregistros;
						$nregistros=$this->oBD->update('bitacora_smartbook_se', $estados, array('idbitacora_alum_smartbook' => $corregir["id"],'idusuario'=>$corregir["idalumno"],'idcurso_sc'=>$corregir["idcurso"],'idcc'=>$corregir["idcomplementario"]));
						$ncorregido=$ncorregido+$nregistros;
					}
					$desde=$corregir["id"];
					$sql2tmp[]=$sql2;
			}
			echo json_encode(array('code'=>200,'ncorregidos'=>$ncorregido,'desde'=>$desde,'sql1'=>$sql,'sql2'=>$sql2tmp,'termino'=>0));
			exit(0);
		}catch(Exception $ex){				
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);
		}
	}


	public function actividad_alumno($desde){
		try{
			$this->oBD->conlimit=false;
			$sql="SELECT idactalumno  as id, idalumno as idalumno, idcurso as idcurso,idcc as idcomplementario, concat(p.ape_paterno,' ',p.ape_materno,', ',p.nombre) as alumno,p.usuario,p.dni FROM actividad_alumno v INNER JOIN personal p ON v.idalumno=p.idpersona  WHERE  v.idgrupoauladetalle IS NULL AND idactalumno>".$desde." ORDER BY idactalumno ASC LIMIT 0,50";
			$porcorregir=$this->oBD->consultarSQL($sql);
			if(empty($porcorregir)){
				echo json_encode(array('code'=>200,'ncorregidos'=>0,'desde'=>$desde,'sql1'=>'Ningun Registro a corregir','sql2'=>'Ningun Registro a corregir','termino'=>1));
				exit();
			}
			$ncorregido=0;
			$nregistros=0;
			$sql2tmp=array();			
			foreach ($porcorregir as $p => $corregir){
				$desde=$corregir["id"];
				if(empty($corregir["idcurso"]) && empty($corregir["idcomplementario"])) continue;
				else if(empty($corregir["idcomplementario"])){
					$sql2="SELECT m.idgrupoauladetalle as idgrupoauladetalle ,gd.idcomplementario  as idcomplementario,gd.idcurso,ga.idproyecto FROM `acad_matricula` m INNER JOIN acad_grupoauladetalle gd on m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula WHERE idalumno=".$corregir["idalumno"]." AND idcurso=".$corregir["idcurso"]."  limit 0,1";					
				}else{
					$sql2="SELECT m.idgrupoauladetalle as idgrupoauladetalle ,gd.idcomplementario  as idcomplementario,gd.idcurso,ga.idproyecto FROM `acad_matricula` m INNER JOIN acad_grupoauladetalle gd on m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula WHERE idalumno=".$corregir["idalumno"]." AND idcurso=".$corregir["idcurso"]." AND gd.idcomplementario=".$corregir["idcomplementario"]."  limit 0,1";
				
				}
				$datos=$this->oBD->consultarSQL($sql2);
			  	if(!empty($datos[0])){
					$estados=array('idgrupoauladetalle'=>$datos[0]["idgrupoauladetalle"]);						
					//$estados['idcc']=$datos[0]["idcomplementario"]);
					$nregistros=$this->oBD->update('actividad_alumno', $estados, array('idactalumno' => $corregir["id"],'idalumno'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"],'idcc'=>$corregir["idcomplementario"]));
					$ncorregido=$ncorregido+$nregistros;
					
				}				
				$sql2tmp[]=$sql2;
			}
			echo json_encode(array('code'=>200,'ncorregidos'=>$ncorregido,'desde'=>$desde,'sql1'=>$sql,'sql2'=>$sql2tmp,'termino'=>0));
			exit(0);
		}catch(Exception $ex){				
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);
		}
	}



	public function transferencias($desde){
		try{
			$this->oBD->conlimit=false;
			$sql="SELECT iddocente,pe.usuario as usuariodocente,idalumno,pa.usuario as usuarioalumno ,gd.idgrupoaula,idproyecto,idmatricula,gd.idgrupoauladetalle,idcurso,gd.idcomplementario as idccok ,ma.idcomplementario as idcc from acad_grupoauladetalle gd inner join acad_matricula ma on gd.idgrupoauladetalle=ma.idgrupoauladetalle inner join acad_grupoaula ga on gd.idgrupoaula=ga.idgrupoaula inner join personal pe on iddocente=pe.idpersona inner join personal pa on idalumno=pa.idpersona WHERE gd.idcomplementario!=ma.idcomplementario AND gd.idgrupoauladetalle>".$desde." ORDER BY gd.idgrupoauladetalle ASC LIMIT 0,2 ";
			$porcorregir=$this->oBD->consultarSQL($sql);
			if(empty($porcorregir)){
				echo json_encode(array('code'=>200,'ncorregidos'=>0,'desde'=>$desde,'sql1'=>'Ningun Registro a corregir','sql2'=>'Ningun Registro a corregir','termino'=>1));
				exit();
			}
			$corregidos=array();
			$totalcorregidos=0;
			foreach($porcorregir as $p => $corregir){
				//corregir historial sesion. 
				$estados1=array('idcc'=>$corregir["idccok"]);
				$nregistros=$this->oBD->update('historial_sesion', $estados1, array('idusuario'=>$corregir["idalumno"],'idcurso'=>$corregir['idcurso'],'idcc'=>$corregir["idcc"]));
				$corregidos["historial_sesion"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

				$this->oBD->delete('historial_sesion',array('idusuario'=>$corregir["idalumno"]));
				//corregir valoracion
				$estados2=array('idcomplementario'=>$corregir["idccok"]); // 'idgrupoauladetalle'=>$datos[0]["idgrupoauladetalle"]
				$nregistros=$this->oBD->update('valoracion', $estados2, array('idalumno'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"],'idcomplementario'=>$corregir["idcc"]));
				$corregidos["valoracion"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

				//corregir  notas_quiz
				$estados3=array('idcomplementario'=>$corregir["idccok"]);
				$nregistros=$this->oBD->update('notas_quiz', $estados3, array('idalumno'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"],'idcomplementario'=>$corregir["idcc"]));
				$corregidos["notas_quiz"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

				$estados4=array('idcomplementario'=>$corregir["idccok"]);
				$nregistros=$this->oBD->update('tareas', $estados4, array('idalumno'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"],'idcomplementario'=>$corregir["idcc"]));
				$corregidos["tareas"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

				$estados5=array('idcomplementario'=>$corregir["idccok"]);
				$nregistros=$this->oBD->update('bitacora_alumno_smartbook', $estados5, array('idusuario'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"],'idcomplementario'=>$corregir["idcc"]));
				$corregidos["bitacora_alumno_smartbook"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

				$estados6=array('idcomplementario'=>$corregir["idccok"]);
				$nregistros=$this->oBD->update('bitacora_smartbook', $estados6, array('idusuario'=>$corregir["idalumno"],'idcurso'=>$corregir["idcurso"],'idcomplementario'=>$corregir["idcc"]));
				$corregidos["bitacora_smartbook"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

				$estados7=array('idcc'=>$corregir["idccok"]);
				$nregistros=$this->oBD->update('bitacora_alumno_smartbook_se', $estados7, array('idusuario'=>$corregir["idalumno"],'idcurso_sc'=>$corregir["idcurso"],'idcc'=>$corregir["idcc"]));
				$corregidos["bitacora_alumno_smartbook_se"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

				$estados8=array('idcc'=>$corregir["idccok"]);
				$nregistros=$this->oBD->update('bitacora_smartbook_se', $estados8, array('idusuario'=>$corregir["idalumno"],'idcurso_sc'=>$corregir["idcurso"],'idcc'=>$corregir["idcc"]));
				$corregidos["bitacora_smartbook_se"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

				$estados9=array('idcomplementario'=>$corregir["idccok"]);
				$nregistros=$this->oBD->update('acad_matricula', $estados9, array('idalumno'=>$corregir["idalumno"],'idgrupoauladetalle'=>$corregir["idgrupoauladetalle"],'idcomplementario'=>$corregir["idcc"]));
				$corregidos["acad_matricula"]=$nregistros;
				$totalcorregidos=$totalcorregidos+$nregistros;

			}
						
			echo json_encode(array('code'=>200,'ncorregidos'=>$totalcorregidos,'desde'=>$desde,'sql1'=>$sql,'sql2'=>$corregidos,'termino'=>0));
			exit(0);
		}catch(Exception $ex){
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);	
		}
	}

public function eliminarinfouser($idalumno){
	try{
		$totalcorregidos=0;
		$t1=0;
		$corregidos=array();
		//1,5,18,19
			$nregistros=$this->oBD->delete('historial_sesion',array('idusuario'=>$idalumno)); // abelchingo
			$corregidos["acad_matricula"]=$nregistros;
			$t1=$t1+$nregistros;			

			$nregistros=$this->oBD->delete('notas_quiz',array('idalumno'=>$idalumno)); // abelchingo
			$corregidos["notas_quiz"]=$nregistros;
			$t1=$t1+$nregistros;			

			$nregistros=$this->oBD->delete('tareas',array('idalumno'=>$idalumno)); // abelchingo
			$corregidos["tareas"]=$nregistros;
			$t1=$t1+$nregistros;		

			$nregistros=$this->oBD->delete('bitacora_smartbook',array('idusuario'=>$idalumno)); // abelchingo
			$corregidos["bitacora_smartbook"]=$nregistros;
			$t1=$t1+$nregistros;

			$nregistros=$this->oBD->delete('bitacora_alumno_smartbook',array('idusuario'=>$idalumno)); // abelchingo
			$corregidos["bitacora_alumno_smartbook"]=$nregistros;
			$t1=$t1+$nregistros;

			$nregistros=$this->oBD->delete('bitacora_smartbook_se',array('idusuario'=>$idalumno)); // abelchingo
			$corregidos["bitacora_smartbook_se"]=$nregistros;
			$t1=$t1+$nregistros;			

			$nregistros=$this->oBD->delete('bitacora_alumno_smartbook_se',array('idusuario'=>$idalumno)); // abelchingo
			$corregidos["bitacora_alumno_smartbook_se"]=$nregistros;
			$t1=$t1+$nregistros;
			
			$nregistros=$this->oBD->delete('actividad_alumno',array('idalumno'=>$idalumno)); // abelchingo
			$corregidos["actividad_alumno"]=$nregistros;
			$t1=$t1+$nregistros;

			$nregistros=$this->oBD->delete('acad_matricula',array('idalumno'=>$idalumno)); // abelchingo
			$corregidos["acad_matricula"]=$nregistros;
			$t1=$t1+$nregistros;


			echo json_encode(array('code'=>200,'ncorregidos'=>$t1,'desde'=>0,'sql1'=>'usuarios :'.$idalumno. " Registros borradots ".$t1,'sql2'=>$corregidos,'termino'=>1));
			exit(0);
		}catch(Exception $ex){
			echo json_encode(array('code'=>'error','ncorregidos'=>0,'msj'=>$ex->getMessage()));
			exit(0);	
		}
	}

}