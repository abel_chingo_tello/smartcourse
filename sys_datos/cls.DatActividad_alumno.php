<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-04-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatActividad_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM actividad_alumno";

			$cond = array();

			if (!empty($filtros["idactalumno"])) {
				$cond[] = "idactalumno = " . $this->oBD->escapar($filtros["idactalumno"]);
			}


			if (!empty($filtros["idrecurso"])) {
				$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if (!empty($filtros["iddetalleactividad"])) {
				$cond[] = "iddetalleactividad = " . $this->oBD->escapar($filtros["iddetalleactividad"]);
			}
			if (!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (!empty($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (!empty($filtros["fecha"])) {
				$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if (!empty($filtros["porcentajeprogreso"])) {
				$cond[] = "porcentajeprogreso = " . $this->oBD->escapar($filtros["porcentajeprogreso"]);
			}
			if (!empty($filtros["habilidades"])) {
				$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}
			if (!empty($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (!empty($filtros["html_solucion"])) {
				$cond[] = "html_solucion = " . $this->oBD->escapar($filtros["html_solucion"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null, $bdname = null)
	{
		try {
			if (empty($bdname)) {
				$sql = "SELECT AA.tipofile,AA.iddetalleactividad,AA.idactalumno,AA.idalumno,AA.fecha,AA.porcentajeprogreso,AA.habilidades,AA.estado,AD.idactividad ,AA.html_solucion,AD.titulo ,AD.tipo, AD.idactividad, A.sesion, AD.idactividad, A.sesion, AA.idgrupoauladetalle FROM actividad_alumno AA JOIN actividad_detalle AD ON AA.iddetalleactividad = AD.iddetalle JOIN actividades A ON A.idactividad = AD.idactividad ";
			} else {
				$sql = "SELECT AA.tipofile,AA.iddetalleactividad,AA.idactalumno,AA.idalumno,AA.fecha,AA.porcentajeprogreso,AA.habilidades,AA.estado,AD.idactividad ,AA.html_solucion,AD.titulo ,AD.tipo, AD.idactividad, A.sesion, AD.idactividad, A.sesion, AA.idgrupoauladetalle FROM actividad_alumno AA JOIN `{$bdname}`.actividad_detalle AD ON AA.iddetalleactividad = AD.iddetalle JOIN `{$bdname}`.actividades A ON A.idactividad = AD.idactividad ";
			}
			//echo $sql;
			$cond = array();


			if (!empty($filtros["idactalumno"])) {
				$cond[] = "AA.idactalumno = " . $this->oBD->escapar($filtros["idactalumno"]);
			}
			if (isset($filtros["iddetalleactividad"])) {
				if (is_array($filtros["iddetalleactividad"])) {
					$cond[] = "AA.iddetalleactividad IN ('" . implode("','", $filtros["iddetalleactividad"]) . "')";
				} else
					$cond[] = "AA.iddetalleactividad = " . $this->oBD->escapar($filtros["iddetalleactividad"]);
			}

			if (!empty($filtros["idproyecto"])) {
				$cond[] = "AA.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($filtros["idalumno"])) {
				$cond[] = "AA.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "AA.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (!empty($filtros["idcurso"])) {
				$cond[] = "AA.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}

			if (!empty($filtros["idcc"])) {
				$cond[] = "AA.idcc = " . $this->oBD->escapar($filtros["idcc"]);
			}

			if (!empty($filtros["idrecurso"])) {
				$cond[] = "AA.idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if (!empty($filtros["fecha"])) {
				$cond[] = "AA.fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if (!empty($filtros["porcentajeprogreso"])) {
				$cond[] = "AA.porcentajeprogreso = " . $this->oBD->escapar($filtros["porcentajeprogreso"]);
			}
			if (!empty($filtros["habilidades"])) {
				$cond[] = "AA.habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}
			if (!empty($filtros["estado"])) {
				$cond[] = "AA.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (!empty($filtros["html_solucion"])) {
				$cond[] = "AA.html_solucion = " . $this->oBD->escapar($filtros["html_solucion"]);
			}
			if (!empty($filtros["idactividad"])) {
				$cond[] = "AD.idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if (!empty($filtros["sesion"])) {
				$cond[] = "A.sesion = " . $this->oBD->escapar($filtros["sesion"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//var_dump($filtros);
			//echo $sql;
			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql."\n";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}
	public function getProgresoHabilidadesSB($params)
	{ #$params = ["idalumno", "idrecurso", "bd_se", "concatenado"]
		try {
			$sql = "
					SELECT  #actividades.sesion, 
							actividad_detalle.idhabilidad skill_id,
							metodologia_habilidad.nombre skill_name,
							ifnull(count(actividad_detalle.iddetalle),0) cantidad,
							ifnull(round(SUM(t_actividad_alumno.porcentajeprogreso)/count(actividad_detalle.iddetalle),2),0) progreso
					FROM `{$params["bd_se"]}`.actividades
						INNER JOIN `{$params["bd_se"]}`.actividad_detalle 
							ON actividad_detalle.idactividad=actividades.idactividad
						
						LEFT JOIN `{$params["bd_se"]}`.metodologia_habilidad
							ON metodologia_habilidad.idmetodologia=actividad_detalle.idhabilidad

						LEFT JOIN  actividad_alumno t_actividad_alumno 
							ON t_actividad_alumno.iddetalleactividad=actividad_detalle.iddetalle
								AND t_actividad_alumno.idalumno={$this->oBD->escapar($params["idalumno"])}";
			$cond = [];
			if (!empty($params["idcurso"])) {
				$cond[] = " t_actividad_alumno.idcurso = " . $this->oBD->escapar($params["idcurso"]);
			}
			if (!empty($params["idcomplementario"])) {
				$cond[] = " t_actividad_alumno.idcc = " . $this->oBD->escapar($params["idcomplementario"]);
			}
			if (!empty($params["idproyecto"])) {
				$cond[] = " t_actividad_alumno.idproyecto = " . $this->oBD->escapar($params["idproyecto"]);
			}
			#mine-descomentar
			if (!empty($params["idgrupoauladetalle"])) {
				$cond[] = " t_actividad_alumno.idgrupoauladetalle = " . $this->oBD->escapar($params["idgrupoauladetalle"]);
			}
			if (!empty($cond)) {
				$sql .= " AND " . implode(' AND ', $cond) . " ";
			}

			$sql .= " WHERE ";
			if (is_array($params["idrecurso"])) {
				$sql .= " actividades.sesion in (" . implode(',', $params["idrecurso"]) . ") ";
			} else {
				$sql .= " actividades.sesion={$this->oBD->escapar($params["idrecurso"])} ";
			}

			$sql .= "
						AND actividad_detalle.idhabilidad " . (isset($params["concatenado"]) == true ? "not" : "") . " in ('4','5','6','7')
						AND actividades.metodologia in (2,3)
					GROUP BY actividad_detalle.idhabilidad
					ORDER BY actividad_detalle.tipo ASC";
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}
	public function getPorcentajeSmartbookAlumno($params)
	{ //devuelve el porcentaje de avance de un alumno en un smartbook
		try {
			$sql = "
			SELECT vista.sesion, ROUND(avg(vista.progreso),2) progreso_smartbook FROM (
				SELECT  actividades.sesion,actividad_detalle.iddetalle, actividad_detalle.tipo,
						ifnull(SUM(actividad_alumno.porcentajeprogreso)/count(actividad_detalle.iddetalle),0) progreso
				FROM `{$params["bd_se"]}`.actividades
					INNER JOIN `{$params["bd_se"]}`.actividad_detalle 
						ON actividad_detalle.idactividad=actividades.idactividad
					LEFT JOIN  actividad_alumno actividad_alumno 
						ON actividad_alumno.iddetalleactividad=actividad_detalle.iddetalle
						AND actividad_alumno.idalumno={$this->oBD->escapar($params["idalumno"])}#dinámico
						AND actividad_alumno.idcurso={$this->oBD->escapar($params["idcurso"])}#dinámico
						AND actividad_alumno.idcc={$this->oBD->escapar($params["idcomplementario"])}#dinámico
						AND actividad_alumno.idproyecto={$this->oBD->escapar($params["idproyecto"])}#dinámico							
						#mine-descomentar
						AND actividad_alumno.idgrupoauladetalle={$this->oBD->escapar($params["idgrupoauladetalle"])}#dinámico	
				WHERE actividades.sesion in ({$this->oBD->escapar($params["idrecurso"])})#dinámico para el idrecurso smartbook
					AND actividades.metodologia in (2,3)#fijo para DBY & PRACTICE
				GROUP BY actividades.metodologia
			) vista
		";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("getPorcentajeSmartbookAlumno") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  `idactalumno`,`iddetalleactividad`,`idalumno`,`fecha`,`porcentajeprogreso`,`habilidades`,`estado`,`html_solucion`,`tipofile`,`file`,`fechamodificacion`,`idrecurso`,`idproyecto`,`idcurso`,`idcc`,idgrupoauladetalle  FROM actividad_alumno  ";

			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("List all") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}

	public function insertar($iddetalleactividad, $idalumno, $idrecurso, $fecha, $porcentajeprogreso, $habilidades, $estado, $html_solucion, $idcurso = 0, $idcc = 0, $idproyecto = 0, $idgrupoauladetalle = null)
	{
		try {
			$this->iniciarTransaccion('dat_actividad_alumno_insert');

			// $id = $this->oBD->consultarEscalarSQL("SELECT MAX(idactalumno) FROM actividad_alumno");
			// ++$id;

			$estados = array(
				// 'idactalumno'=>$id,
				'iddetalleactividad' => $iddetalleactividad, 'idalumno' => $idalumno, 'idrecurso' => $idrecurso
				/*,'fecha'=>$fecha*/, 'porcentajeprogreso' => $porcentajeprogreso, 'habilidades' => $habilidades, 'estado' => $estado, 'html_solucion' => $html_solucion, 'idcurso' => !empty($idcurso) ? $idcurso : 0, 'idcc' => !empty($idcc) ? $idcc : 0, 'idproyecto' => !empty($idproyecto) ? $idproyecto : 0
			);
			if (!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"] = $idgrupoauladetalle;
			$this->oBD->insert('actividad_alumno', $estados);
			$this->terminarTransaccion('dat_actividad_alumno_insert');
			return 0;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_actividad_alumno_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $iddetalleactividad, $idalumno, $idrecurso, $fecha, $porcentajeprogreso, $habilidades, $estado, $html_solucion, $idcurso = 0, $idcc = 0, $idproyecto = 0, $idgrupoauladetalle = null)
	{
		try {
			$this->iniciarTransaccion('dat_actividad_alumno_update');
			$estados = array(
				'iddetalleactividad' => $iddetalleactividad, 'idalumno' => $idalumno, 'idrecurso' => $idrecurso
				/*,'fecha'=>$fecha*/, 'porcentajeprogreso' => $porcentajeprogreso, 'habilidades' => $habilidades, 'estado' => $estado, 'html_solucion' => $html_solucion, 'idcurso' => !empty($idcurso) ? $idcurso : 0, 'idcc' => !empty($idcc) ? $idcc : 0, 'idproyecto' => !empty($idproyecto) ? $idproyecto : 0
			);
			if (!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"] = $idgrupoauladetalle;
			$this->oBD->update('actividad_alumno ', $estados, array('idactalumno' => $id));
			$this->terminarTransaccion('dat_actividad_alumno_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT `idactalumno`,`iddetalleactividad`,`idalumno`,`fecha`,`porcentajeprogreso`,`habilidades`,`estado`,`html_solucion`,`tipofile`,`file`,`fechamodificacion`,`idrecurso`,`idproyecto`,`idcurso`,`idcc`,idgrupoauladetalle  FROM actividad_alumno WHERE idactalumno = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('actividad_alumno', array('idactalumno' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('actividad_alumno', array($propiedad => $valor), array('' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}

	public function ultimaActividad($idalumno)
	{
		try {
			$sql = "SELECT `idactalumno`,`iddetalleactividad`,`idalumno`,`fecha`,`porcentajeprogreso`,`habilidades`,`estado`,`html_solucion`,`tipofile`,`file`,`fechamodificacion`,`idrecurso`,`idproyecto`,`idcurso`,`idcc`,idgrupoauladetalle  FROM actividad_alumno WHERE idactalumno = (SELECT MAX(idactalumno) FROM actividad_alumno WHERE idalumno=" . $this->oBD->escapar($idalumno) . "  AND iddetalleactividad<>0 )";

			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Actividad_alumno") . ": " . $e->getMessage());
		}
	}
	public function countprogreso($filtros = null, $bdname = null)
	{
		try {
			if (empty($bdname)) {
				$sql = "SELECT pe.idpersona, 
				(SELECT cd.idcurso FROM actividad_alumno aa inner join actividad_detalle ad on ad.iddetalle = aa.iddetalleactividad inner join actividades a on a.idactividad = ad.idactividad inner join acad_cursodetalle cd on cd.idrecurso = a.unidad inner join acad_curso c on c.idcurso = cd.idcurso
				WHERE (aa.habilidades LIKE '%4%' OR aa.habilidades LIKE '%5%' OR aa.habilidades LIKE '%6%' OR aa.habilidades LIKE '%7%') AND aa.idalumno = pe.idpersona AND c.estado = 1 limit 1) AS idcurso,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%4%' AND aa.idalumno = pe.idpersona _fecha) AS total_L,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%5%' AND aa.idalumno = pe.idpersona _fecha) AS total_R,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%6%' AND aa.idalumno = pe.idpersona _fecha) AS total_W,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%7%' AND aa.idalumno = pe.idpersona _fecha) AS total_S, 
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%4%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_L_T,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%5%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_R_T,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%6%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_W_T,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%7%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_S_T
				FROM personal pe";
			} else {

				$sql = "SELECT pe.idpersona, 
				(SELECT cd.idcurso FROM actividad_alumno aa inner join `{$bdname}`.actividad_detalle ad on ad.iddetalle = aa.iddetalleactividad inner join `{$bdname}`.actividades a on a.idactividad = ad.idactividad inner join `{$bdname}`.acad_cursodetalle cd on cd.idrecurso = a.unidad inner join `{$bdname}`.acad_curso c on c.idcurso = cd.idcurso
				WHERE (aa.habilidades LIKE '%4%' OR aa.habilidades LIKE '%5%' OR aa.habilidades LIKE '%6%' OR aa.habilidades LIKE '%7%') AND aa.idalumno = pe.idpersona AND c.estado = 1 limit 1) AS idcurso,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%4%' AND aa.idalumno = pe.idpersona _fecha) AS total_L,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%5%' AND aa.idalumno = pe.idpersona _fecha) AS total_R,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%6%' AND aa.idalumno = pe.idpersona _fecha) AS total_W,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%7%' AND aa.idalumno = pe.idpersona _fecha) AS total_S, 
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%4%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_L_T,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%5%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_R_T,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%6%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_W_T,
				(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%7%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_S_T
				FROM personal pe";
			}

			$cond = array();

			//AND aa.fecha <= '2018-10-20 18:28:44'

			if (isset($filtros['fecha'])) {
				$sql = str_replace('_fecha', "AND aa.fecha <= " . $this->oBD->escapar($filtros['fecha']), $sql);
			} else {
				$sql = str_replace('_fecha', '', $sql);
			}

			if (isset($filtros['idpersona'])) {
				$cond[] = "pe.idpersona = " . $this->oBD->escapar($filtros['idpersona']);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			// echo $sql."<br><br>";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}
	public function actividadestotal($filtros = null, $bdname = null)
	{
		try {
			if (empty($bdname)) {
				$sql = "SELECT count(1) AS total FROM actividad_detalle ad INNER JOIN (SELECT a.idactividad FROM actividades a INNER JOIN acad_cursodetalle cd ON cd.idrecurso = a.sesion WHERE @curso a.metodologia >= 2  and cd.tiporecurso = 'L' _xunidad ) t ON ad.idactividad = t.idactividad";
			} else {
				$sql = "SELECT count(1) AS total FROM `{$bdname}`.actividad_detalle ad INNER JOIN (SELECT a.idactividad FROM `{$bdname}`.actividades a INNER JOIN `{$bdname}`.acad_cursodetalle cd ON cd.idrecurso = a.sesion WHERE @curso a.metodologia >= 2  and cd.tiporecurso = 'L' _xunidad ) t ON ad.idactividad = t.idactividad";
			}

			$cond = array();

			if (isset($filtros['idcurso'])) {
				$sql = str_replace('@curso', "cd.idcurso = " . $this->oBD->escapar($filtros['idcurso']) . " AND", $sql);
			} else {
				$sql = str_replace('@curso', '', $sql);
			}
			if (isset($filtros['xunidad'])) {
				if (isset($filtros['idrecurso']) && isset($filtros['idcurso'])) {
					$sql = str_replace('_xunidad', "AND cd.idpadre = (SELECT acd.idcursodetalle FROM acad_cursodetalle acd WHERE acd.idrecurso = " . $this->oBD->escapar($filtros['idrecurso']) . " and acd.idcurso = " . $this->oBD->escapar($filtros['idcurso']) . " and acd.tiporecurso = 'U' LIMIT 1)", $sql);
				} else {
					$sql = str_replace('_xunidad', ' ', $sql);
				} //end if idrecurso and idcurso
			} else {
				$sql = str_replace('_xunidad', ' ', $sql);
			}

			if (isset($filtros['listen'])) {
				$cond[] = "ad.idhabilidad LIKE '%4%'";
			}
			if (isset($filtros['read'])) {
				$cond[] = "ad.idhabilidad LIKE '%5%'";
			}
			if (isset($filtros['write'])) {
				$cond[] = "ad.idhabilidad LIKE '%6%'";
			}
			if (isset($filtros['speak'])) {
				$cond[] = "ad.idhabilidad LIKE '%7%'";
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}


			//echo $sql."<br><br>";


			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}
}
