<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-09-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatZoom extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Zoom") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT z.idzoom,z.idpersona,z.fecha,z.urlmoderador,z.urlparticipantes,z.titulo,z.agenda,z.estado,z.uuid,z.duracion,z.email,z.imagen,z.idcursodetalle,z.idpestania,z.idcurso,z.idcomplementario FROM zoom z";

			$cond = array();

			if (isset($filtros['sql'])) {
				if ($filtros['sql'] == 2) {
					$sql = "SELECT z.idzoom,z.idpersona,z.fecha,z.urlmoderador,z.urlparticipantes,z.titulo,z.agenda,z.estado,z.uuid,z.duracion,z.email,z.imagen,z.idcursodetalle,z.idpestania,z.idcurso,z.idcomplementario,za.idzoomalumno,za.idalumno,za.fecha FROM zoom z INNER JOIN zoomalumnos za ON z.idzoom = za.idzoom";
					if (isset($filtros["idalumno"])) {
						$cond[] = "za.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
					}
				}
			}
			if (isset($filtros['mysql'])) {
				if ($filtros['mysql'] == 2) { //PARA LISTAR EN EVENTOS (docente)
					$sql = "SELECT '2' tipo_evento,z.idpersona, date(z.fecha) fecha, z.urlmoderador zoom_url, z.fecha as start, 
								z.fecha as start, date_add(z.fecha,interval z.duracion minute) end,
								time(z.fecha) hora_comienzo,
								date_add(time(z.fecha),interval z.duracion minute) hora_fin, 
								z.titulo as title, z.agenda as detalle 
							FROM zoom z";
					if (isset($filtros["end"]) && isset($filtros["start"])) {
						$cond[] = "z.fecha between " . $this->oBD->escapar($filtros["start"]) . " and " . $this->oBD->escapar($filtros["end"]);
					}
				}
			}
			if (isset($filtros["idzoom"])) {
				$cond[] = "z.idzoom = " . $this->oBD->escapar($filtros["idzoom"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "z.idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if (isset($filtros["fecha"])) {
				$cond[] = "z.fecha >= " . $this->oBD->escapar($filtros["fecha"]);
			}
			if (isset($filtros["urlmoderador"])) {
				$cond[] = "z.urlmoderador = " . $this->oBD->escapar($filtros["urlmoderador"]);
			}
			if (isset($filtros["urlparticipantes"])) {
				$cond[] = "z.urlparticipantes = " . $this->oBD->escapar($filtros["urlparticipantes"]);
			}
			if (isset($filtros["titulo"])) {
				$cond[] = "z.titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if (isset($filtros["agenda"])) {
				$cond[] = "z.agenda = " . $this->oBD->escapar($filtros["agenda"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "z.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["uuid"])) {
				$cond[] = "z.uuid = " . $this->oBD->escapar($filtros["uuid"]);
			}
			if (isset($filtros["duracion"])) {
				$cond[] = "z.duracion = " . $this->oBD->escapar($filtros["duracion"]);
			}
			if (isset($filtros["email"])) {
				$cond[] = "z.email = " . $this->oBD->escapar($filtros["email"]);
			}
			if (isset($filtros["idcursodetalle"])) {
				$cond[] = "z.idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if (isset($filtros["idpestania"])) {
				$cond[] = "z.idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "z.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "z.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY z.fecha ASC";
			//exit($sql);

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Zoom") . ": " . $e->getMessage());
		}
	}


	public function insertar($idpersona, $fecha, $urlmoderador, $urlparticipantes, $titulo, $agenda, $estado, $uuid, $duracion,$imagen, $idcursodetalle, $idpestania, $idcurso, $idcomplementario,$email)
	{
		try {

			$this->iniciarTransaccion('dat_zoom_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idzoom) FROM zoom");
			++$id;

			$estados = array(
				'idzoom' => $id, 'idpersona' => $idpersona, 'fecha' => $fecha, 'urlmoderador' => $urlmoderador, 'urlparticipantes' => $urlparticipantes, 'titulo' => $titulo, 'agenda' => $agenda, 'estado' => $estado, 'uuid' => $uuid, 'duracion' => $duracion,'imagen' => $imagen, 'idcursodetalle' => $idcursodetalle, 'idpestania' => $idpestania, 'idcurso' => $idcurso, 'idcomplementario' => $idcomplementario,'email'=>$email
			);

			$this->oBD->insert('zoom', $estados);
			$this->terminarTransaccion('dat_zoom_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_zoom_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Zoom") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona, $fecha, $urlmoderador, $urlparticipantes, $titulo, $agenda, $estado, $uuid, $duracion,$imagen, $idcursodetalle, $idpestania, $idcurso, $idcomplementario,$email)
	{
		try {
			$this->iniciarTransaccion('dat_zoom_update');
			$estados = array(
				'idpersona' => $idpersona, 'fecha' => $fecha, 'urlmoderador' => $urlmoderador, 'urlparticipantes' => $urlparticipantes, 'titulo' => $titulo, 'agenda' => $agenda, 'estado' => $estado, 'uuid' => $uuid, 'duracion' => $duracion,'imagen' => $imagen, 'idcursodetalle' => $idcursodetalle, 'idpestania' => $idpestania, 'idcurso' => $idcurso, 'idcomplementario' => $idcomplementario,'email'=>$email
			);

			$this->oBD->update('zoom ', $estados, array('idzoom' => $id));
			$this->terminarTransaccion('dat_zoom_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Zoom") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idzoom,idpersona,fecha,urlmoderador,urlparticipantes,titulo,agenda,estado,uuid,duracion,email,imagen,idcursodetalle,idpestania,idcurso,idcomplementario  FROM zoom  WHERE idzoom = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Zoom") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('zoom', array('idzoom' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Zoom") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('zoom', array($propiedad => $valor), array('idzoom' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Zoom") . ": " . $e->getMessage());
		}
	}
}
