<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-10-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatAcad_matricula extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM acad_matricula";

			$cond = array();

			if (isset($filtros["idmatricula"])) {
				$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["tipomatricula"])) {
				$cond[] = "tipomatricula = " . $this->oBD->escapar($filtros["tipomatricula"]);
			}
			if (isset($filtros["fecha_registro"])) {
				$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["fecha_matricula"])) {
				$cond[] = "fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if (isset($filtros["fecha_vencimiento"])) {
				$cond[] = "m.fecha_vencimiento >= " . $this->oBD->escapar($filtros["fecha_vencimiento"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			//var_dump($filtros);
			$sql = "gd.idcurso , gd.idcomplementario , ac.tipo as tipocurso,IFNULL(acad_curso_complementario.nombre,ac.`nombre`) as nombrecurso, m.idmatricula,m.idgrupoauladetalle,m.idalumno,m.fecha_registro,m.idusuario,m.estado,m.fecha_matricula,m.fecha_vencimiento,m.notafinal,m.formulajson,m.fecha_termino,m.pagorecurso,m.tipomatricula,m.idcategoria,gd.idsence_assigncursos,gd.idgrupoaula,gd.iddocente,ga.idproyecto,ac.`nombre` as strcurso,
			IFNULL(acad_curso_complementario.imagen,ac.`imagen`) as imagen, ac.`descripcion`,ac.`estado` AS estado_curso,ac.`fecharegistro`, ac.`idusuario`, 
			ac.`vinculosaprendizajes`, ac.`materialesyrecursos`, ac.`color`, ac.`objetivos`, ac.`certificacion`, ac.`costo`,
			 (select configuracion_nota from proyecto_cursos pyc WHERE pyc.idcurso=ac.idcurso and pyc.idproyecto=ga.idproyecto LIMIT 0,1 ) as configuracion_nota ,
			 (select orden from proyecto_cursos WHERE idproyecto=ga.idproyecto AND idcurso=ac.idcurso limit 0,1) as ordencursoproyecto ,
			 ac.`silabo`, ac.`e_ini`, ac.`pdf`, ac.`abreviado`, ac.`e_fin`, ac.`aniopublicacion`, ac.`autor`, ac.`txtjson`,
			 ga.nombre AS strgrupoaula, (SELECT CONCAT(do.ape_paterno,' ',do.ape_materno,', ',do.nombre) FROM personal do WHERE do.dni=gd.iddocente OR do.idpersona=gd.iddocente limit 1) 
			 AS strdocente, (SELECT nombre FROM local l WHERE l.idlocal=gd.idlocal ) AS strlocal,
			 CONCAT(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) as stralumno,pe.dni,pe.esdemo, pe.email as alumno_email,
			 pe.usuario,pe.fechanac, TIMESTAMPDIFF(YEAR, pe.fechanac, CURDATE()) AS edad, pe.foto, pe.telefono, 
			 pe.tipodoc,pe.sexo, pe.estado_civil, pe.email, pe.celular, 
			 (SELECT CONCAT(numero,' ', (SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE gd.idambiente=am.idambiente) AS strambiente 
			FROM acad_matricula m 
			LEFT JOIN acad_grupoauladetalle gd ON m.idgrupoauladetalle=gd.idgrupoauladetalle 			
			left join acad_curso_complementario on gd.idcomplementario=acad_curso_complementario.idcurso
			INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula 
			INNER JOIN acad_curso ac ON ac.idcurso=gd.idcurso 
			INNER JOIN personal pe ON m.idalumno=pe.idpersona";
			if (!empty($filtros["sql"])) {
				if ($filtros["sql"] == 'sql1') {
					$sql = " m.tipomatricula, m.idmatricula,m.idgrupoauladetalle,m.estado,m.fecha_registro,m.fecha_matricula,m.fecha_vencimiento,m.fecha_termino,m.notafinal,m.formulajson,m.idalumno,m.pagorecurso, gd.idgrupoaula,gd.iddocente,ga.idproyecto,gd.idsence_assigncursos,ac.`idcurso`,ac.`nombre` as strcurso, ac.`imagen`, ac.`descripcion`, ac.`estado` AS estado_curso, ac.`color`,  ac.`costo`, (select configuracion_nota from proyecto_cursos pyc WHERE pyc.idcurso=ac.idcurso and pyc.idproyecto=ga.idproyecto LIMIT 0,1 ) as configuracion_nota , (select orden from proyecto_cursos WHERE idproyecto=ga.idproyecto AND idcurso=ac.idcurso limit 0,1) as ordencursoproyecto ,ac.`e_ini`,ac.`e_fin`, ga.nombre AS strgrupoaula, (SELECT CONCAT(do.ape_paterno,' ',do.ape_materno,', ',do.nombre) FROM personal do WHERE do.dni=gd.iddocente OR do.idpersona=gd.iddocente limit 1) AS strdocente, (SELECT nombre FROM local l WHERE l.idlocal=gd.idlocal ) AS strlocal, CONCAT(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) as stralumno,pe.dni,pe.esdemo,pe.email as alumno_email, pe.usuario,pe.foto,pe.tipodoc,pe.email, (SELECT CONCAT(numero,' ', (SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE gd.idambiente=am.idambiente) AS strambiente FROM acad_matricula m LEFT JOIN acad_grupoauladetalle gd ON m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula INNER JOIN acad_curso ac ON ac.idcurso=gd.idcurso INNER JOIN personal pe ON m.idalumno=pe.idpersona";
				} elseif ($filtros["sql"] == 'sql-soloalumnos'){
					$sql = " p.dni, m.idalumno, concat(p.ape_paterno, ' ', p.ape_materno, ' ', p.nombre) as stralumno
					from acad_matricula m inner join personal p on p.idpersona = m.idalumno";
					$filtros["orderby"] = 'nombre';
					if (isset($filtros["idproyecto"])) unset($filtros["idproyecto"]);
				} elseif ($filtros["sql"] == 'sql-alumnos'){
					$sql = "idmatricula,idgrupoauladetalle, p.dni, m.idalumno, concat(p.ape_paterno, ' ', p.ape_materno, ' ', p.nombre) as stralumno	from acad_matricula m inner join personal p on p.idpersona = m.idalumno";
					$filtros["orderby"] = 'nombre';
					if (isset($filtros["idproyecto"])) unset($filtros["idproyecto"]);
				}
			}
			$cond = array();
			if (!empty($filtros["idcategoria"])) {
				$sql = 'SELECT acc.idcategoria, ' . $sql . " LEFT JOIN acad_cursocategoria acc ON  ac.idcurso=acc.idcurso ";
				$cond[] = "acc.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			} else {
				$sql = 'SELECT ' . $sql;
			}
			if (isset($filtros["idmatricula"])) {
				$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if (!empty($filtros["idgrupoauladetalle"])) {
				if (is_array($filtros["idgrupoauladetalle"])) {
					$cond[] = "m.idgrupoauladetalle IN ('" . implode("','", $filtros["idgrupoauladetalle"]) . "')";
				} else
					$cond[] = "m.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (!empty($filtros["idalumno"])) {
				if (is_array($filtros["idalumno"])) {
					$cond[] = "idalumno IN ('" . implode("','", $filtros["idalumno"]) . "')";
				} else
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}

			if (isset($filtros["fecha_registro"])) {
				$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}

			if (isset($filtros["tipomatricula"])) {
				$cond[] = "tipomatricula = " . $this->oBD->escapar($filtros["tipomatricula"]);
			}


			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "gd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "m.estado = " . $this->oBD->escapar($filtros["estado"]);
			} else $cond[] = "m.estado = 1";

			if (isset($filtros["idusuario"])) {
				$cond[] = "m.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (!empty($filtros["idcurso"])) {
				if (is_array($filtros["idcurso"])) {
					$cond[] = "gd.idcurso IN ('" . implode("','", $filtros["idcurso"]) . "')";
				} else
					$cond[] = "gd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}


			if (isset($filtros["idproyecto"])) {
				$cond[] = "ga.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "gd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}

			if (isset($filtros["texto"])) {
				$cond[] = " (pe.dni=" . $this->oBD->escapar($filtros["texto"]) . " OR idalumno " . $this->oBD->like($filtros["texto"]) . " OR CONCAT(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) " . $this->oBD->like($filtros["texto"]) . ") ";
			}

			if (isset($filtros["fecha_matricula"])) {
				$cond[] = "m.fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "m.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["fecha_vencimiento"])) {
				$cond[] = "m.fecha_vencimiento >= " . $this->oBD->escapar($filtros["fecha_vencimiento"]);
			}
			if (isset($filtros["hasta_fecha_vencimiento"])) {
				$cond[] = "m.fecha_vencimiento <= " . $this->oBD->escapar($filtros["hasta_fecha_vencimiento"]);
			}
			if (isset($filtros['fechaactiva'])) {
				$fecha_actual = date('Y-m-d');
				$cond[] = "m.fecha_matricula <= " . $this->oBD->escapar($fecha_actual);
				$cond[] = "m.fecha_vencimiento >= " . $this->oBD->escapar($fecha_actual);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (isset($filtros["orderby"])) {
				if ($filtros["orderby"] == 'orden') $sql .= " ORDER BY ordencursoproyecto ";
				elseif ($filtros["orderby"] == 'nombre') $sql .= " ORDER BY stralumno ASC ";
				else $sql .= " ORDER BY " . $filtros["orderby"];
			} else {
				$sql .= " ORDER BY ordencursoproyecto,idmatricula Desc";
			}
			if (!empty($filtros["sql"])) {
				switch ($filtros["sql"]) {
					case '1':
						return $this->getmatricula($filtros);
						break;
				}
			}
			//echo $sql;
			//echo $sql."<hr>"; exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("buscar") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}

	public function listarAlumnos($filtros = null)
	{
		try {
			$sql = "select DISTINCT pr.idproyecto, p.dni, m.idalumno, concat(p.ape_paterno, ' ', p.ape_materno, ' ', p.nombre) as stralumno
					from acad_matricula m
					inner join personal p on p.idpersona = m.idalumno
					inner join persona_rol pr on pr.idpersonal = p.idpersona ";
			$cond = array();
			if (isset($filtros["idproyecto"])) {
				$cond[] = "pr.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "m.estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (isset($filtros["orderby"])) {
				if ($filtros["orderby"]) {
					$sql .= " order by stralumno asc";
				}
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("buscar") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}

	public function listaProfes($filtros = null)
	{
		try {
			$sql = "select distinct iddocente, strdocente, idproyecto,fecha_matricula,fecha_vencimiento from (
							SELECT  gd.iddocente,ac.tipo as tipocurso, m.idmatricula,m.idgrupoauladetalle,m.idalumno,m.fecha_registro,m.idusuario,m.estado,m.fecha_matricula,m.fecha_vencimiento,m.notafinal,m.formulajson,m.fecha_termino,m.pagorecurso,m.tipomatricula,m.idcomplementario,m.idcategoria,gd.idgrupoaula,ga.idproyecto,ac.`idcurso`,
								ac.`nombre` as strcurso,ac.`imagen`,ac.`descripcion`,ac.`estado` AS estado_curso,ac.`fecharegistro`,
								ac.`vinculosaprendizajes`, ac.`materialesyrecursos`, ac.`color`, ac.`objetivos`, 
								ac.`certificacion`, ac.`costo`, 
								(select configuracion_nota from proyecto_cursos pyc WHERE pyc.idcurso=ac.idcurso and pyc.idproyecto=ga.idproyecto LIMIT 0,1 ) as configuracion_nota , 
								(select orden from proyecto_cursos WHERE idproyecto=ga.idproyecto AND idcurso=ac.idcurso limit 0,1) as ordencursoproyecto , 
								ac.`silabo`, ac.`e_ini`, ac.`pdf`, ac.`abreviado`, ac.`e_fin`, ac.`aniopublicacion`, ac.`autor`, ac.`txtjson`, ga.nombre AS strgrupoaula, 
								(SELECT CONCAT(do.ape_paterno,' ',do.ape_materno,', ',do.nombre) FROM personal do WHERE do.dni=gd.iddocente OR do.idpersona=gd.iddocente limit 1) AS strdocente, 
								(SELECT nombre FROM local l WHERE l.idlocal=gd.idlocal ) AS strlocal, CONCAT(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) as stralumno,pe.dni,
								pe.esdemo, pe.email as alumno_email, pe.usuario, pe.foto, pe.telefono, pe.tipodoc,pe.sexo, pe.estado_civil, pe.email, pe.celular, 
								(SELECT CONCAT(numero,' ', (SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE gd.idambiente=am.idambiente) AS strambiente 
							FROM acad_matricula m 
								LEFT JOIN acad_grupoauladetalle gd ON m.idgrupoauladetalle=gd.idgrupoauladetalle 
								INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula 
								INNER JOIN acad_curso ac ON ac.idcurso=gd.idcurso 
								INNER JOIN personal pe ON m.idalumno=pe.idpersona
					) as view ";

			$cond = array();

			if (!empty($filtros["idalumno"])) {
				if (is_array($filtros["idalumno"])) {
					$cond[] = "view.idalumno IN ('" . implode("','", $filtros["idalumno"]) . "')";
				} else
					$cond[] = "view.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}

			if (isset($filtros["idproyecto"])) {
				$cond[] = "view.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if (isset($filtros['fechaactiva'])) {
				$fecha_actual = date('Y-m-d');
				$cond[] = "view.fecha_matricula <= " . $this->oBD->escapar($fecha_actual);
				$cond[] = "view.fecha_vencimiento >= " . $this->oBD->escapar($fecha_actual);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= "GROUP BY view.iddocente";

			// echo $sql;
			// exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("buscar") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function getmatricula($filtros)
	{
		try {
			$sql = "SELECT m.idmatricula,m.idgrupoauladetalle,m.idalumno,m.fecha_registro,m.idusuario,m.estado,m.fecha_matricula,m.fecha_vencimiento,m.notafinal,m.formulajson,m.fecha_termino,m.pagorecurso,m.tipomatricula,m.idcomplementario,m.idcategoria,agd.idcurso FROM acad_matricula m INNER JOIN acad_grupoauladetalle agd ON m.idgrupoauladetalle = agd.idgrupoauladetalle";
			$cond = array();
			// if(isset($arr['idcurso'])){
			// 	$cond[] = "agd.idcurso = ".$this->oBD->escapar($arr['idcurso']) ;	
			// }
			if (isset($filtros["idmatricula"])) {
				$cond[] = "m.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "m.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "m.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros['fecha_matricula'])) {
				$cond[] = "m.fecha_matricula = " . $this->oBD->escapar($filtros['fecha_matricula']);
			}
			if (isset($filtros['fecha_vencimiento'])) {
				$cond[] = "m.fecha_vencimiento = " . $this->oBD->escapar($filtros['fecha_vencimiento']);
			}
			if (isset($filtros['idcomplementario'])) {
				$cond[] = "m.idcomplementario = " . $this->oBD->escapar($filtros['idcomplementario']);
			}
			if (isset($filtros['idcategoria'])) {
				$cond[] = "m.idcomplementario = " . $this->oBD->escapar($filtros['idcomplementario']);
			}
			if (isset($filtros['fechaactiva'])) {
				$fecha_actual = date('Y-m-d');
				$cond[] = "m.fecha_matricula <= " . $this->oBD->escapar($fecha_actual);
				$cond[] = "m.fecha_vencimiento >= " . $this->oBD->escapar($fecha_actual);
			}
			if (!empty($filtros["sql"])) {
				switch ($filtros["sql"]) {
					case '1':
						$idpersonas = array();
						$cond = array();
						if (isset($filtros['idgrupoaula'])) {
							$cond[] = "agd.idgrupoaula IN (" . $filtros['idgrupoaula'] . ")";
						}
						if (isset($filtros['fechaactiva'])) {
							$fecha_actual = date('Y-m-d');
							$cond[] = "m.fecha_matricula <= " . $this->oBD->escapar($fecha_actual);
							$cond[] = "m.fecha_vencimiento >= " . $this->oBD->escapar($fecha_actual);
						}
						$sql = "SELECT m.idalumno FROM acad_matricula m INNER JOIN acad_grupoauladetalle agd ON (m.idgrupoauladetalle = agd.idgrupoauladetalle)";
						if (!empty($cond)) {
							$sql .= " WHERE " . implode(' AND ', $cond);
						}
						$rs = $this->oBD->consultarSQL($sql);
						foreach ($rs as $key => $value) {
							$idpersonas[] = $value["idalumno"];
						}
						$sql = "SELECT agd.iddocente FROM acad_matricula m INNER JOIN acad_grupoauladetalle agd ON (m.idgrupoauladetalle = agd.idgrupoauladetalle)";
						if (!empty($cond)) {
							$sql .= " WHERE " . implode(' AND ', $cond);
						}
						$rs = $this->oBD->consultarSQL($sql);
						foreach ($rs as $key => $value) {
							$idpersonas[] = $value["iddocente"];
						}
						$sql = "SELECT DISTINCT p.* FROM personal p";
						$cond = array();
						$cond[] = "p.idpersona IN (" . implode(",", $idpersonas) . ")";
						break;
				}
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// $sql .= " GROUP BY m.idgrupoauladetalle";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			return array();
		}
	}

	public function matriculadosGrupo($filtros = null)
	{
		try {
			$sql = "SELECT m.idmatricula,m.idgrupoauladetalle,m.idalumno,m.fecha_registro,m.idusuario,m.estado,m.fecha_matricula,m.fecha_vencimiento,m.notafinal,m.formulajson,m.fecha_termino,m.pagorecurso,m.tipomatricula,m.idcomplementario,m.idcategoria,gd.idgrupoaula,ga.idproyecto,ac.`idcurso`,ac.`nombre` as strcurso,ac.`imagen`,ac.`descripcion`,ac.`estado` AS estado_curso,ac.`fecharegistro`, ac.`idusuario`, ac.`vinculosaprendizajes`, ac.`materialesyrecursos`, ac.`color`, ac.`objetivos`, ac.`certificacion`, ac.`costo`, ac.`silabo`,ac.`e_ini`,ac.`pdf`,ac.`abreviado`,ac.`e_fin`,ac.`aniopublicacion`,ac.`autor`,ac.`txtjson`, ga.nombre AS strgruponombre, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal do WHERE do.dni=gd.iddocente OR do.idpersona=gd.iddocente limit 1) AS strdocente, (SELECT nombre FROM local l WHERE l.idlocal=gd.idlocal ) AS strlocal, CONCAT(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) as stralumno, dni, email, celular as alumno_email,pe.esdemo, pe.usuario,pe.foto,pe.telefono,pe.tipodoc,pe.sexo,pe.estado_civil, (SELECT CONCAT(numero,' ', (SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE gd.idambiente=am.idambiente) AS strambiente FROM acad_matricula m LEFT JOIN acad_grupoauladetalle gd ON m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula INNER JOIN acad_curso ac ON ac.idcurso=gd.idcurso INNER JOIN personal pe ON m.idalumno=pe.idpersona";

			$cond = array();
			if (isset($filtros["idmatricula"])) $cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			if (isset($filtros["idgrupoauladetalle"])) {
				if (is_array($filtros["idgrupoauladetalle"])) {
					$cond[] = "m.idgrupoauladetalle IN ('" . implode("','", $filtros["idgrupoauladetalle"]) . "')";
				} else
					$cond[] = "m.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			//if(isset($filtros["idgr"]))

			if (isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}

			if (isset($filtros["fecha_registro"])) {
				$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "gd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "m.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "m.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["tipomatricula"])) {
				$cond[] = "tipomatricula = " . $this->oBD->escapar($filtros["tipomatricula"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "gd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "ga.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "gd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}

			if (isset($filtros["texto"])) {
				$cond[] = " (idalumno " . $this->oBD->like($filtros["texto"]) . " OR CONCAT(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) " . $this->oBD->like($filtros["texto"]) . ") ";
			}

			if (isset($filtros["fecha_matricula"])) {
				$cond[] = "fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (isset($filtro["orderby"])) {
				if ($filtro["orderby"] == 'nombre')
					$sql .= " ORDER By stralumno ASC ";
			} else
				$sql .= " ORDER BY idmatricula Desc";

			// echo $sql;
			// var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("matriculadosGrupo") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}

	public function buscar2($filtros = null)
	{
		try {
			$sql = "SELECT m.idmatricula,m.idgrupoauladetalle,m.idalumno,m.fecha_registro,m.idusuario,m.estado,m.fecha_matricula,m.fecha_vencimiento,m.notafinal,m.formulajson,m.fecha_termino,m.pagorecurso,m.tipomatricula,m.idcomplementario,m.idcategoria ,p.nombre AS nombre, CONCAT(p.ape_paterno,' ',p.ape_materno) AS apellidos,p.usuario, agd.idcurso, ac.nombre AS nombrecurso FROM acad_matricula m LEFT JOIN personal p ON p.idpersona = m.idalumno LEFT JOIN acad_grupoauladetalle agd ON m.idgrupoauladetalle = agd.idgrupoauladetalle LEFT JOIN acad_curso ac ON agd.idcurso = ac.idcurso";

			$cond = array();

			if (!empty($filtros["idalumno"])) {
				if (is_array($filtros["idalumno"])) {
					$cond[] = "m.idalumno IN (" . implode(',', $filtros["idalumno"]) . ")";
				} else {
					$cond[] = "m.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
				}
			}
			if (isset($filtros["tipomatricula"])) {
				$cond[] = "m.tipomatricula = " . $this->oBD->escapar($filtros["tipomatricula"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "m.estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if (isset($filtros["fecha_registro"])) {
				$cond[] = "m.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}

			if (isset($filtros["fecha_matricula"])) {
				$cond[] = "m.fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}

			if (isset($filtros["fecha_vencimiento"])) {
				$cond[] = "m.fecha_vencimiento >= " . $this->oBD->escapar($filtros["fecha_vencimiento"]);
			}


			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {

			throw new Exception("ERROR\n" . JrTexto::_("buscar2") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}

	public function cursosAlumno($filtros = null) // ya no se usa
	{
		try {
			$sql = "SELECT 
				M.idmatricula, 
				M.idalumno,
				M.fecha_registro, 
				M.fecha_matricula,
				M.tipomatricula, 
				GAD.idgrupoauladetalle, 
				GAD.iddocente, 
				GAD.fecha_inicio, 
				GAD.fecha_final, 
				C.* , (SELECT CONCAT(P.ape_paterno,' ',P.ape_materno,' ',P.nombre) FROM personal P WHERE P.idpersona=GAD.iddocente  ) AS docente_nombre
			FROM acad_matricula M 
			JOIN acad_grupoauladetalle GAD ON M.idgrupoauladetalle=GAD.idgrupoauladetalle 
			JOIN acad_curso C ON GAD.idcurso=C.idcurso
			JOIN proyecto_cursos PC ON C.idcurso=PC.idcurso ";

			$cond = array();
			if (isset($filtros["idmatricula"])) {
				$cond[] = "M.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "M.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "M.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["fecha_registro"])) {
				$cond[] = "M.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "M.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "M.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["fecha_matricula"])) {
				$cond[] = "M.fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if (isset($filtros["tipomatricula"])) {
				$cond[] = "tipomatricula = " . $this->oBD->escapar($filtros["tipomatricula"]);
			}
			if (isset($filtros["fecha_vencimiento"])) {
				$cond[] = "M.fecha_vencimiento >= " . $this->oBD->escapar($filtros["fecha_vencimiento"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "GAD.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "PC.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			$cond[] = " C.estado = 1 ";
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY C.nombre ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("cursosAlumno") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function horarioAlumno($filtros = null) // ya nos e usa
	{
		try {
			$sql = "SELECT a.fecha_inicio as fecha_inicioG, a.fecha_final as fecha_finalG,h.*, mat.idmatricula,mat.idgrupoauladetalle,mat.idalumno,mat.fecha_registro,mat.idusuario,mat.estado,mat.fecha_matricula,mat.fecha_vencimiento,mat.notafinal,mat.formulajson,mat.fecha_termino,mat.pagorecurso,mat.tipomatricula,mat.idcomplementario,mat.idcategoria,c.nombre FROM acad_matricula mat left join acad_grupoauladetalle a on a.idgrupoauladetalle=mat.idgrupoauladetalle left join acad_horariogrupodetalle h on h.idgrupoauladetalle=a.idgrupoauladetalle left join acad_curso c on c.idcurso=a.idcurso";

			$cond = array();

			if (isset($filtros["idmatricula"])) {
				$cond[] = "mat.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "mat.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "mat.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["fecha_registro"])) {
				$cond[] = "mat.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "mat.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "mat.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["tipomatricula"])) {
				$cond[] = "tipomatricula = " . $this->oBD->escapar($filtros["tipomatricula"]);
			}
			if (isset($filtros["fecha_matricula"])) {
				$cond[] = "mat.fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "a.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["fecha_vencimiento"])) {
				$cond[] = "mat.fecha_vencimiento >= " . $this->oBD->escapar($filtros["fecha_vencimiento"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("horarioAlumno") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}

	public function horarioDocente($filtros = null) // ya nose usa 
	{
		try {
			$sql = "SELECT DISTINCT a.fecha_inicio as fecha_inicioG, a.fecha_final as fecha_finalG,h.*,c.nombre FROM acad_matricula mat left join acad_grupoauladetalle a on a.idgrupoauladetalle=mat.idgrupoauladetalle left join acad_horariogrupodetalle h on h.idgrupoauladetalle=a.idgrupoauladetalle left join acad_curso c on c.idcurso=a.idcurso";

			$cond = array();

			if (isset($filtros["idmatricula"])) {
				$cond[] = "mat.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "mat.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "mat.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["fecha_registro"])) {
				$cond[] = "mat.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "mat.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "mat.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["tipomatricula"])) {
				$cond[] = "tipomatricula = " . $this->oBD->escapar($filtros["tipomatricula"]);
			}
			if (isset($filtros["fecha_matricula"])) {
				$cond[] = "mat.fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "a.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "a.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["fecha_vencimiento"])) {
				$cond[] = "mat.fecha_vencimiento >= " . $this->oBD->escapar($filtros["fecha_vencimiento"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			#echo $sql; exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("horarioDocente") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}

	public function obtenergrupoauladetalle($idalumno, $idgrupo)
	{
		try {
			$sql = "SELECT idgrupoauladetalle FROM acad_grupoauladetalle WHERE idgrupoaula = " . $this->oBD->escapar($idgrupo);

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}


	public function insertar($idgrupoauladetalle, $idalumno, $fecha_registro, $estado, $idusuario, $fecha_matricula, $fecha_vencimiento = '', $tipomatricula = 1)
	{
		try {

			$this->iniciarTransaccion('dat_acad_matricula_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatricula) FROM acad_matricula");
			++$id;

			$estados = array(
				'idmatricula' => $id, 'idgrupoauladetalle' => $idgrupoauladetalle, 'idalumno' => $idalumno, 'fecha_registro' => !empty($fecha_registro) ? $fecha_registro : date('Y-m-d'), 'estado' => $estado, 'idusuario' => $idusuario, 'fecha_matricula' => !empty($fecha_matricula) ? $fecha_matricula : date('Y-m-d'), 'fecha_vencimiento' => !empty($fecha_vencimiento) ? $fecha_vencimiento : date("Y-m-d H:i:s", strtotime(date('Y-m-d') . "+ 1 month")), 'tipomatricula' => $tipomatricula
			);

			$this->oBD->insert('acad_matricula', $estados);
			$this->terminarTransaccion('dat_acad_matricula_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_acad_matricula_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupoauladetalle, $idalumno, $fecha_registro, $estado, $idusuario, $fecha_matricula, $fecha_vencimiento = '', $tipomatricula = 1)
	{
		try {
			$this->iniciarTransaccion('dat_acad_matricula_update');
			$estados = array(
				'idgrupoauladetalle' => $idgrupoauladetalle, 'idalumno' => $idalumno
				//,'fecha_registro'=>$fecha_registro
				, 'estado' => $estado, 'idusuario' => $idusuario
				//,'fecha_matricula'=>$fecha_matricula
				, 'fecha_vencimiento' => !empty($fecha_vencimiento) ? $fecha_vencimiento : date("Y-m-d H:i:s", strtotime(date('Y-m-d') . "+ 1 month")), 'tipomatricula' => $tipomatricula
			);

			$this->oBD->update('acad_matricula', $estados, array('idmatricula' => $id));
			$this->terminarTransaccion('dat_acad_matricula_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	/**
	 * Funcion que se encarga de actualizar registro de manera multiple en una tabla de manera generica
	 * Se requiere que los datos que se envien por parametro tengan el campo clave del identificador
	 * ATENCION: Abstener de hacer una logica especial en esta función, ya que se reutilizara en varias funcionalidades, por el simple hecho que es una funcion que opera de forma generica en el sistema
	 * @param Array Datos que seran actualizado de manera multiple
	 */
	public function multipleupdate($data, $estricto = false)
	{
		try {
			$this->iniciarTransaccion('dat_acad_matricula_update');
			if (empty($data)) {
				throw new Exception("Data esta vacia");
			}
			foreach ($data as $estados) {
				if (empty($estados['idmatricula']) && $estricto == false) {
					continue;
				} else if (empty($estados['idmatricula']) && $estricto == true) {
					throw new Exception("un registro no tiene el idmatricula definido");
				}
				$id = $estados['idmatricula'];
				unset($estados['idmatricula']);
				$this->oBD->update('acad_matricula', $estados, array('idmatricula' => $id));
			}
			$this->terminarTransaccion('dat_acad_matricula_update');
			return true;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_acad_matricula_update');
			throw new Exception("ERROR\n" . JrTexto::_("multipleupdate") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  m.idmatricula,m.idgrupoauladetalle,m.idalumno,m.fecha_registro,m.idusuario,m.estado,m.fecha_matricula,m.fecha_vencimiento,m.notafinal,m.formulajson,m.fecha_termino,m.pagorecurso,m.tipomatricula,m.idcomplementario,m.idcategoria  FROM acad_matricula m  WHERE idmatricula = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_matricula', array('idmatricula' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function eliminar2($idalumno, $idgrupo)
	{
		try {
			$respuesta;
			$sql = "SELECT idgrupoauladetalle FROM acad_grupoauladetalle WHERE idgrupoaula = " . $this->oBD->escapar($idgrupo);

			$res = $this->oBD->consultarSQL($sql);
			foreach ($res as $value) {
				$respuesta = $this->oBD->delete('acad_matricula', array('idalumno' => $idalumno, "idgrupoauladetalle" => $value["idgrupoauladetalle"]));
			}
			return $respuesta;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function eliminarfiltro($filtro)
	{
		if (empty($filtro)) return;

		$cond = array();

		if (isset($filtros["idmatricula"])) {
			$cond["idmatricula"] = $this->oBD->escapar($filtros["idmatricula"]);
		}
		if (isset($filtros["idgrupoauladetalle"])) {
			$cond["idgrupoauladetalle"] = $this->oBD->escapar($filtros["idgrupoauladetalle"]);
		}
		if (isset($filtros["idalumno"])) {
			$cond["idalumno"] = $this->oBD->escapar($filtros["idalumno"]);
		}
		if (isset($filtros["fecha_registro"])) {
			$cond["fecha_registro"] =  $this->oBD->escapar($filtros["fecha_registro"]);
		}
		if (isset($filtros["estado"])) {
			$cond["estado"] =  $this->oBD->escapar($filtros["estado"]);
		}
		if (isset($filtros["idusuario"])) {
			$cond["idusuario"] =  $this->oBD->escapar($filtros["idusuario"]);
		}
		if (isset($filtros["fecha_matricula"])) {
			$cond["fecha_matricula"] =  $this->oBD->escapar($filtros["fecha_matricula"]);
		}
		if (isset($filtros["idcurso"])) {
			$cond["idcurso"] =  $this->oBD->escapar($filtros["idcurso"]);
		}
		if (isset($filtros["iddocente"])) {
			$cond["iddocente"] =  $this->oBD->escapar($filtros["iddocente"]);
		}
		if (empty($cond)) {
			return;
		}
		return $this->oBD->delete('acad_matricula', $cond);
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {

			$this->oBD->update('acad_matricula', array($propiedad => $valor), array('idmatricula' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function setAll($campoid, $valorid, $propiedad, $valor)
	{
		try {
			$this->oBD->update('acad_matricula', array($propiedad => $valor), array($campoid => $valorid));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}

	public function updatecategoriasxcurso($idcurso, $idcategoria)
	{
		try {
			$estados = array('idcategoria' => $idcategoria);
			$this->oBD->update('acad_cursocategoria', $estados, array('idcurso' => $idcurso));
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}



	public function cursos($filtros)
	{
		try {

			$sql = "SELECT m.idmatricula,m.idalumno,m.fecha_registro,m.estado,m.fecha_matricula,m.fecha_vencimiento,m.pagorecurso,m.idusuario,(SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE  pe.idpersona=m.idusuario Limit 1) AS strusuario, cu.nombre as strcurso ,cu.tipo, cu.imagen  as imagen, ag.idproyecto, sac.codsence,agd.*,ag.nombre as strgrupoaula,m.codigocursosence, (SELECT dni FROM personal pe WHERE pe.dni=agd.iddocente OR pe.idpersona=agd.iddocente Limit 1) AS dnidocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE  pe.idpersona=agd.iddocente Limit 1) AS strdocente,m.tipomatricula,m.notificar FROM acad_grupoauladetalle agd INNER JOIN acad_grupoaula ag ON ag.idgrupoaula=agd.idgrupoaula LEFT JOIN sence_assigncursos sac ON sac.id = agd.idsence_assigncursos Right JOIN acad_curso cu ON cu.idcurso=agd.idcurso INNER JOIN acad_matricula m ON m.idgrupoauladetalle=agd.idgrupoauladetalle";

			if (isset($filtros["idgrupoauladetalle"])) {
				if (is_array($filtros["idgrupoauladetalle"]))
					$cond[] = "agd.idgrupoauladetalle IN ('" . implode("','", $filtros["idgrupoauladetalle"]) . "')";
				else $cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (isset($filtros["idproyecto"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}

			if (isset($filtros["idalumno"])) {
				if (is_array($filtros["idalumno"]))
					$cond[] = "m.idalumno IN ('" . implode("','", $filtros["idalumno"]) . "')";
				else $cond[] = "m.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}

			if (isset($filtros["idmatricula"])) {
				$cond[] = "m.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}

			if (isset($filtros["idmatricula"])) {
				$cond[] = "m.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}

			if (isset($filtros["estado"])) {
				$cond[] = "m.estado >=" . $this->oBD->escapar($filtros["estado"]);
			} else $cond[] = "m.estado >=0";


			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " ASC";
				}
			} else {
				$sql .= " ORDER BY idmatricula ASC";
			}
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("buscar") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
	public function ingresados($filtros = null){
		try{
			$cond = [];
			$sql = "SELECT 
				p.usuario,
				p.email,
				p.nombre,
				m.idmatricula, 
				m.idalumno,
				ag.idproyecto,
				IF(CURRENT_DATE >= m.fecha_registro,DATEDIFF(CURRENT_DATE,m.fecha_registro),0) AS diff_fecha,
				IF(h.idhistorialsesion IS NOT NULL,(SELECT idhistorialsesion FROM historial_sesion WHERE idhistorialsesion = h.idhistorialsesion AND lugar = 'P' LIMIT 1), NULL) AS idhistorialsesion
			FROM acad_matricula m INNER JOIN personal p ON p.idpersona = m.idalumno INNER JOIN acad_grupoauladetalle agd ON agd.idgrupoauladetalle = m.idgrupoauladetalle INNER JOIN acad_grupoaula ag ON ag.idgrupoaula = agd.idgrupoaula LEFT JOIN historial_sesion h ON m.idalumno = h.idusuario";
			
			if (isset($filtros["idmatricula"])) {
				$cond[] = "m.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "m.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["fecha_registro"])) {
				$cond[] = "m.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (isset($filtros["desde_fecha_registro"])) {
				$cond[] = "m.fecha_registro >= " . $this->oBD->escapar($filtros["desde_fecha_registro"]);
			}
			if (isset($filtros["hasta_fecha_registro"])) {
				$cond[] = "m.fecha_registro <= " . $this->oBD->escapar($filtros["hasta_fecha_registro"]);
			}
			if (isset($filtros['estado'])) {
				$cond[] = "m.estado = " . $this->oBD->escapar($filtros['estado']);
			}
			if (isset($filtros['tipomatricula'])) {
				$cond[] = "m.tipomatricula = " . $this->oBD->escapar($filtros['tipomatricula']);
			}
			if (isset($filtros['fecha_matricula'])) {
				$cond[] = "m.fecha_matricula = " . $this->oBD->escapar($filtros['fecha_matricula']);
			}
			if (isset($filtros['fecha_vencimiento'])) {
				$cond[] = "m.fecha_vencimiento = " . $this->oBD->escapar($filtros['fecha_vencimiento']);
			}
			if (isset($filtros['idcomplementario'])) {
				$cond[] = "m.idcomplementario = " . $this->oBD->escapar($filtros['idcomplementario']);
			}
			if (isset($filtros['idcategoria'])) {
				$cond[] = "m.idcategoria = " . $this->oBD->escapar($filtros['idcategoria']);
			}
			if (isset($filtros['idproyecto'])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros['idproyecto']);
			}

			$cond[] = "(h.idhistorialsesion IS NULL OR h.lugar != 'P')";

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (isset($filtros['groupby'])) {
				if($filtros['groupby'] == "idalumno"){
					$sql .= " GROUP BY m.idalumno";
				}
			}
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e){
			throw new Exception("ERROR\n" . JrTexto::_("ingresados") . " " . JrTexto::_("Acad_matricula") . ": " . $e->getMessage());
		}
	}
}
/*notafinal
formulajson
fecha_termino
tipomatricula*/