<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		29-01-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatTest_criterios extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Test_criterios").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM test_criterios";
			
			$cond = array();		
			
			if(isset($filtros["idtestcriterio"])) {
					$cond[] = "idtestcriterio = " . $this->oBD->escapar($filtros["idtestcriterio"]);
			}
			if(isset($filtros["idtest"])) {
					$cond[] = "idtest = " . $this->oBD->escapar($filtros["idtest"]);
			}
			if(isset($filtros["criterio"])) {
					$cond[] = "criterio = " . $this->oBD->escapar($filtros["criterio"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Test_criterios").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM test_criterios";			
			
			$cond = array();		
					
			
			if(isset($filtros["idtestcriterio"])) {
					$cond[] = "idtestcriterio = " . $this->oBD->escapar($filtros["idtestcriterio"]);
			}
			if(isset($filtros["idtest"])) {
					$cond[] = "idtest = " . $this->oBD->escapar($filtros["idtest"]);
			}
			if(isset($filtros["criterio"])) {
					$cond[] = "criterio = " . $this->oBD->escapar($filtros["criterio"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Test_criterios").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idtest,$criterio,$mostrar)
	{
		try {
			
			$this->iniciarTransaccion('dat_test_criterios_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtestcriterio) FROM test_criterios");
			++$id;
			
			$estados = array('idtestcriterio' => $id
							
							,'idtest'=>$idtest
							,'criterio'=>$criterio
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('test_criterios', $estados);			
			$this->terminarTransaccion('dat_test_criterios_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_test_criterios_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Test_criterios").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idtest,$criterio,$mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_test_criterios_update');
			$estados = array('idtest'=>$idtest
							,'criterio'=>$criterio
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('test_criterios ', $estados, array('idtestcriterio' => $id));
		    $this->terminarTransaccion('dat_test_criterios_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Test_criterios").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM test_criterios  "
					. " WHERE idtestcriterio = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Test_criterios").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('test_criterios', array('idtestcriterio' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Test_criterios").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('test_criterios', array($propiedad => $valor), array('idtestcriterio' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Test_criterios").": " . $e->getMessage());
		}
	}
   
		
}