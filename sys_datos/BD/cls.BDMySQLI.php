<?php
/**
 * Ultima modificacion	:	14/10/2009
 */

class BDMySQLI
{
	private $host;
	private $baseDatos;
	private $usuario;
	private $clave;
	private $puerto = null;
	protected $mysqli = null;
	private $autocommit = true;
	private $ultimoError;
	private $codificacion = 'utf8_decode';
	protected static $instancia = null;
	private $id_transaccion = '';

	//LIMIT
	private $desde = 0;
	private $desplazar = 9999999;
	public $conlimit=true;
	public $mostrarError=false;

	public function __construct($host, $baseDatos, $usuario, $clave, $puerto = null)
	{
		$this->host = $host;
		$this->baseDatos = $baseDatos;
		$this->usuario = $usuario;
		$this->clave = $clave;
		$this->puerto = is_null($puerto) ? null : 3306;
		$this->mostrarError=!empty(MODODEVELOP)?MODODEVELOP:false;
	}

	public static function &getInstancia($host, $baseDatos, $usuario, $clave, $puerto = null)
	{
		if (is_object(self::$instancia)){
			if(self::$instancia->baseDatos!=$baseDatos)
			self::$instancia = new self($host, $baseDatos, $usuario, $clave, $puerto);
		}else self::$instancia = new self($host, $baseDatos, $usuario, $clave, $puerto);
		return self::$instancia;
	}

	public function conectar()
	{
		if (!empty($this->mysqli)){			
			@$this->mysqli->next_result();
			return;
		}
		if (!extension_loaded('mysqli')) {
			throw new Exception('Extensión mysqli no instalada');
		}

		if(true === $this->autocommit) {
			@$this->mysqli = new mysqli(
				$this->host,
				$this->usuario,
				$this->clave,
				$this->baseDatos,
				$this->puerto
			);
			
			if($this->mysqli === false || mysqli_connect_errno()) {
				throw new Exception('Error de conexión');
			}
			
			$this->mysqli->set_charset("utf8");
		}
	}

	public function getUser(){
		return $this->usuario;
	}
	public function getClave(){
		return $this->clave;
	}
	public function getHost(){
		return $this->host;
	}

	public function iniciarTransaccion($id_transaccion = '')
	{
		$this->conectar();
		if (false == $this->autocommit) return;
		if ($this->mysqli->autocommit(false)) {
			$this->autocommit = false;
			$this->id_transaccion = $id_transaccion;
		} else {
			throw new Exception('Imposible iniciar transacci&oacute;n');
		}
	}

	public function terminarTransaccion($id_transaccion = '')
	{
		if ($id_transaccion != $this->id_transaccion) return;
		if (false == $this->autocommit) {
			$this->autocommit = true;
			$this->mysqli->commit();
			$this->mysqli->autocommit(true);
			$this->cerrarConexion();
			$this->id_transaccion = '';
		}
	}

	public function truncarTransaccion($id_transaccion = '')
	{
		if ($id_transaccion != $this->id_transaccion) return;
		if (false == $this->autocommit) {
			$this->autocommit = true;
			$this->mysqli->rollback();
			$this->mysqli->autocommit(true);
			$this->cerrarConexion();
			$this->id_transaccion = '';
		}
	}

	public function cerrarConexion()
	{		
		if (true === $this->autocommit && !empty($this->mysqli) ){	
			$this->mysqli->close();
			$this->mysqli = null;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		// echo "me llamaron con los params $desde, $desplazamiento<hr>";
		$this->desde		= (0 <= $desde) ? $desde : 0;
		$this->desplazar	= (1 <= $desplazamiento) ? $desplazamiento : 100;
	}

	public function resetLimite()
	{
		$this->setLimite(0, 999999);
	}

	/**
	 * Agrega un LIKE simple de doble comodin
	 * @cadena
	 */
	public function like($valor)
	{
		try {
			return "LIKE '%" . $this->escapar($valor, false) . "%'";
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function consultarRutina($sql, $metodo = 'ARRAY', $tipo = 'ASOC')
	{ //v 1.2
		try {
			if ('' == trim($sql)) {
				throw new Exception('COM_SQL_NULL');
			}

			$consultarSQL = array();
			$this->conectar();	
			if ($resultado = $this->mysqli->query($sql)) {
				if ('ARRAY' == $metodo) {
					if ('ASOC' == $tipo) {
						while ($fila = $resultado->fetch_array(MYSQLI_ASSOC)) {
							$consultarSQL[] = $fila;
						}
					} elseif ('NUM' == $tipo) {
						while ($fila = $resultado->fetch_array(MYSQLI_NUM)) {
							$consultarSQL[] = $fila;
						}
					} else {
						while ($fila = $resultado->fetch_array(MYSQLI_BOTH)) {
							$consultarSQL[] = $fila;
						}
					}
				} elseif ('OBJETO' == $metodo) {
					while ($objeto = $resultado->fetch_object()) {
						$consultarSQL[] = $objeto;
					}
				}
				$resultado->close();
			} else {
				
				if($this->mostrarError==true){
					echo "Error N°: ".@$this->mysqli->errno."<br>";
					echo "Error   : ".@$this->mysqli->error."<br>";
					echo "ErrorSQL: ".$sql."<br>";
				}				
				throw new Exception($this->getError($this->mysqli->errno));
			}
			$this->cerrarConexion();
			$this->resetLimite();
			return $consultarSQL;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function insert($tabla, $estados, $ignore = false, $returnsql = false)
	{
		try {
			if (!is_string($tabla) || !is_array($estados)) {
				throw new Exception('Argumentos no compatibles');
			}

			$cols = array();
			$vals = array();

			foreach ($estados as $col => $val) {
				$cols[] = $col;
				$vals[] = $this->escapar($val);
			}

			$sql = "INSERT " . (true == $ignore ? 'IGNORE ' : '') . "INTO "
				. $tabla
				. ' (' . implode(', ', $cols) . ')'
				. ' VALUES(' . implode(', ', $vals) . ')';

			if ($returnsql) return $sql;
			return $this->ejecutarSQL($sql);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function insertAI($tabla,$estados,$newid=true,$ignore = false){ // para tablas con autoincrementable
		try{
			if (!is_string($tabla) || !is_array($estados)) {
				throw new Exception('Argumentos no compatibles');
			}
			$cols = array();
			$vals = array();
			
			$this->conectar();
			foreach ($estados as $col => $val){				
				if(!empty($val)&&$val!='null'){
					$cols[] = $col;					
					$vals[] = $this->escapar($val);
				}
			}
			$sql = "INSERT " . (true == $ignore ? 'IGNORE ' : '') . "INTO "
				. $tabla
				. ' (' . implode(', ', $cols) . ')'
				. ' VALUES(' . implode(', ', $vals) . ');';

			if ($this->mysqli->query($sql)){
				$ejecutarSQL = $this->mysqli->affected_rows;
				$newidinsert=$this->mysqli->insert_id;
			} else {									
				if($this->mostrarError==true){
					echo "Error N°: ".$this->mysqli->errno."<br>";
					echo "Error   : ".$this->mysqli->error."<br>";
					echo "ErrorSQL: ".$sql."<br>";
				}
				throw new Exception($this->getError($this->mysqli->errno));				
			}
			$this->cerrarConexion();		
			if($newid) return $newidinsert;
			return $ejecutarSQL;
		}catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}



	public function update($tabla, $estados, $condicion = null, $returnsql = false)
	{
		try {
			if (!is_string($tabla) || !is_array($estados)) {
				throw new Exception('Argumentos no compatibles');
			}

			$sets = array();
			foreach ($estados as $col => $val) {
				$sets[] = $col . " = " . $this->escapar($val);
			}

			$where = $this->where($condicion);

			$sql = "UPDATE "
				. $tabla
				. ' SET ' . implode(', ', $sets)
				. ($where ? " WHERE " . $where : '');
			if ($returnsql) return $sql;
			return $this->ejecutarSQL($sql);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function delete($tabla, $condicion = null, $returnsql = false)
	{
		try {
			if (!is_string($tabla) || !is_array($condicion)) {
				throw new Exception('Argumentos no compatibles');
			}

			$where = $this->where($condicion);
			$sql = "DELETE FROM "
				. $tabla
				. ($where ? " WHERE " . $where : '');
			if ($returnsql) return $sql;
			return $this->ejecutarSQL($sql);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function insertOnDuplicate($tabla, $estados_i, $estados_a)
	{
		try {
			if (!is_string($tabla) || !is_array($estados_i) || !is_array($estados_a)) {
				throw new Exception('Argumentos no compatibles');
			}

			$cols_i = array();
			$vals_i = array();

			$vals_a = array();

			foreach ($estados_i as $col => $val) {
				$cols_i[] = $col;
				$vals_i[] = $this->escapar($val);
			}

			foreach ($estados_a as $col => $val) {
				$vals_a[] = $col . " = " . $this->escapar($val);
			}

			$sql = "INSERT INTO " . $tabla
				. ' (' . implode(', ', $cols_i) . ')'
				. ' VALUES(' . implode(', ', $vals_i) . ')'
				. ' ON DUPLICATE KEY UPDATE ' . implode(', ', $vals_a);
			return $this->ejecutarSQL($sql);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function replace($tabla, $estados)
	{
		try {
			if (!is_string($tabla) || !is_array($estados)) {
				throw new Exception('Argumentos no compatibles');
			}

			$cols = array();
			$vals = array();

			foreach ($estados as $col => $val) {
				$cols[] = $col;
				$vals[] = $this->escapar($val);
			}

			$sql = "REPLACE INTO "
				. $tabla
				. ' (' . implode(', ', $cols) . ')'
				. ' VALUES(' . implode(', ', $vals) . ')';
			return $this->ejecutarSQL($sql);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function where($condicion)
	{
		if (empty($condicion) || !is_array($condicion)) {
			return false;
		}

		$conds = array();
		foreach ($condicion as $col => $val) {
			$conds[] = $col . " = " . $this->escapar($val);
		}

		$conds = implode(' AND ', $conds);
		return $conds;
	}

	public function consultarSQL($sql, $metodo = 'ARRAY', $tipo = 'ASOC', $campo_id = null)
	{ //v 1.2
		try {
			if ('' == trim($sql)) {
				throw new Exception('COM_SQL_NULL');
			}

			if($this->conlimit==true)
			$sql .= " LIMIT " . $this->desde . ", " . $this->desplazar;
			$consultarSQL = array();

			$this->conectar();
			if ($resultado = $this->mysqli->query($sql)) {
				if ('ARRAY' == $metodo) {
					if ('ASOC' == $tipo) {
						while ($fila = $resultado->fetch_array(MYSQLI_ASSOC)) {
							//modificado
							if (!empty($campo_id)) {
								$consultarSQL[$fila[$campo_id]] = $fila;
							} else {
								$consultarSQL[] = $fila;
							}
						}
					} elseif ('NUM' == $tipo) {
						while ($fila = $resultado->fetch_array(MYSQLI_NUM)) {
							$consultarSQL[] = $fila;
						}
					} else {
						while ($fila = $resultado->fetch_array(MYSQLI_BOTH)) {
							$consultarSQL[] = $fila;
						}
					}
				} elseif ('OBJETO' == $metodo) {
					while ($objeto = $resultado->fetch_object()) {
						$consultarSQL[] = $objeto;
					}
				}
				$resultado->close();
			} else {
				
				if($this->mostrarError==true){
					echo "Error N°: ".@$this->mysqli->errno."<br>";
					echo "Error   : ".@$this->mysqli->error."<br>";
					echo "ErrorSQL: ".$sql."<br>";
				}
				throw new Exception($this->getError(@$this->mysqli->errno));
			}
			$this->cerrarConexion();
			$this->resetLimite();
			return $consultarSQL;
		} catch (Exception $e) {		
			throw new Exception($e->getMessage());
		}
	}

	public function consultarEscalarSQL($sql)
	{
		try {
			if ('' == trim($sql)) {
				throw new Exception('COM_SQL_NULL');
			}

			$this->conectar();

			if ($resultado = $this->mysqli->query($sql)) {
				$consultarSQL = $resultado->fetch_array(MYSQLI_NUM);
				$resultado->close();
				$consultarSQL = $consultarSQL[0];
			} else {				
				if($this->mostrarError==true){
					echo "Error N°: ".@$this->mysqli->errno."<br>";
					echo "Error   : ".@$this->mysqli->error."<br>";
					echo "ErrorSQL: ".$sql."<br>";
				}
				throw new Exception($this->getError(@$this->mysqli->errno));
			}
			$this->cerrarConexion();
			$this->resetLimite();
			return $consultarSQL;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function ejecutarSQL($sql)
	{
		try {
			if ('' == trim($sql)) {
				throw new Exception('COM_SQL_NULL');
			}

			$ejecutarSQL = null;
			$this->conectar();
			if ($this->mysqli->query($sql)) {
				$ejecutarSQL = $this->mysqli->affected_rows;
			} else {				
				if($this->mostrarError==true){
					echo "Error N°: ".@$this->mysqli->errno."<br>";
					echo "Error   : ".@$this->mysqli->error."<br>";
					echo "ErrorSQL: ".$sql."<br>";
				}
				$error =$this->getError(@$this->mysqli->errno);				
				throw new Exception($error);
				
			}
			$this->cerrarConexion();
			return $ejecutarSQL;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setBaseDatos($baseDatos)
	{
		if (!empty($baseDatos)) {
			$this->baseDatos = $baseDatos;
		}
	}

	// Obtiene el valor del autocommit
	private function getAutocommit()
	{
		$this->conectar();
		$resultado = $this->mysqli->query('SELECT @@autocommit');
		$col = $resultado->fetch_array();
		$this->cerrarConexion();
		return $col[0];
	}

	private function setErrores()
	{
		if (empty($this->error)) {
			$this->error[1048] = 'SC - El campo esta vacío.';
			$this->error[1054] = 'SC - Columna desconocida.';
			$this->error[1052] = 'SC - Columna ambigua.';
			$this->error[1062] = 'SC - El registro ya existe.';
			$this->error[1064] = 'SC - Error SQL.';
			$this->error[1092] = 'Sc - Claves primarias duplicadas.';
			$this->error[1136] = 'SC - Número de columnas incorrecto.';
			$this->error[1142] = 'SC - Sin privilegios para continuar.';
			$this->error[1146] = 'SC - La tabla no existe.';
			$this->error[1264] = 'SC - Valor incorrecto.';
			$this->error[1292] = 'SC - Valor incorrecto.';
			$this->error[1364] = 'SC - Campo sin valor por defecto.';
			$this->error[1366] = 'SC - Valor incorrecto para el campo.';
			$this->error[1406] = 'SC - Longitud de los datos es muy extensa para el campo.';
			$this->error[1451] = 'SC - Este registro tiene otros registros dependientes.';
			$this->error[1452] = 'SC - Error de claves foráneas.';			
		}		
	}

	public function getError($idError)
	{
		$this->setErrores();
		$this->cerrarConexion();
		if (isset($this->error[$idError])) {
			return $this->error[$idError];
		} else {
			return 'Error no encontrado [' . $idError . ']';
		}

	}

	public function escapar($valor, $comillar = true)
	{
		try {
			if (is_array($valor)) {
				throw new Exception('Valor incorrecto (Array)');
			}

			if (is_int($valor) || is_float($valor)) {
				return $valor;
			}

			if (is_null($valor)) {
				return "''";
			}
			if ($valor === 'null') {
				return "NULL";
			}
			$this->conectar();
			$escape_ = $this->mysqli->real_escape_string($valor);
			if (true === $comillar) {
				return "'" . $escape_ . "'";
			} else {
				return $escape_;
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function escaparArray($array)
	{
		try {
			if (empty($array) || !is_array($array)) {
				throw new Exception('Valor incorrecto para array');
			}

			$i = 0;
			foreach ($array as $val) {
				$array[$i] = $this->escapar($val);
				++$i;
			}

			$array = implode(', ', $array);
			return $array;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getbasedatos()
	{
		return $this->baseDatos;
	}

	public function allsql($sql)
	{
		try {
			$this->conectar();
			$resultado = $this->mysqli->query($sql);
			$obj = null;
			$i = 0;
			if (!empty($resultado))
				while ($res = $resultado->fetch_array()) {
					$obj[] = $res[$i];
				}
			$this->cerrarConexion();
			return $obj;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
