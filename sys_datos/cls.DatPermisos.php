<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016  
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
class DatPermisos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(idpermiso) FROM permisos";

			$cond = array();

			if (!empty($filtros["idpermiso"])) {
				$cond[] = "idpermiso = " . $this->oBD->escapar($filtros["idpermiso"]);
			}
			if (!empty($filtros["rol"])) {
				$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			}
			if (!empty($filtros["menu"])) {
				$cond[] = "menu = " . $this->oBD->escapar($filtros["menu"]);
			}
			if (!empty($filtros["_list"])) {
				$cond[] = "_list = " . $this->oBD->escapar($filtros["_list"]);
			}
			if (!empty($filtros["_add"])) {
				$cond[] = "_add = " . $this->oBD->escapar($filtros["_add"]);
			}
			if (!empty($filtros["_edit"])) {
				$cond[] = "_edit = " . $this->oBD->escapar($filtros["_edit"]);
			}
			if (!empty($filtros["_delete"])) {
				$cond[] = "_delete = " . $this->oBD->escapar($filtros["_delete"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT permisos.idpermiso, permisos.rol, permisos.menu, permisos._list, permisos._add, permisos._edit, permisos._delete, menu.idmenu, menu.nombre 
					from menu left JOIN permisos on idmenu=menu ";

			$cond = array();


			if (!empty($filtros["idpermiso"])) {
				$cond[] = "idpermiso = " . $this->oBD->escapar($filtros["idpermiso"]);
			}
			if (!empty($filtros["rol"])) {
				$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			}
			if (!empty($filtros["menu"])) {
				$cond[] = "menu = " . $this->oBD->escapar($filtros["menu"]);
			}
			if (!empty($filtros["list"])) {
				$cond[] = "_list = " . $this->oBD->escapar($filtros["_list"]);
			}
			if (!empty($filtros["add"])) {
				$cond[] = "_add = " . $this->oBD->escapar($filtros["_add"]);
			}
			if (!empty($filtros["edit"])) {
				$cond[] = "_edit = " . $this->oBD->escapar($filtros["_edit"]);
			}
			if (!empty($filtros["delete"])) {
				$cond[] = "_delete = " . $this->oBD->escapar($filtros["_delete"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  permisos.idpermiso, permisos.rol, permisos.menu, permisos._list, permisos._add, permisos._edit, permisos._delete  FROM permisos  ";

			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("List all") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}

	public function insertar($rol, $menu, $_list, $_add, $_edit, $_delete)
	{
		try {

			$this->iniciarTransaccion('dat_permisos_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpermiso) FROM permisos");
			++$id;

			$estados = array(
				'idpermiso' => $id, 'rol' => $rol, 'menu' => $menu, '_list' => $_list, '_add' => $_add, '_edit' => $_edit, '_delete' => $_delete
			);

			$this->oBD->insert('permisos', $estados);
			$this->terminarTransaccion('dat_permisos_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_permisos_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $rol, $menu, $_list, $_add, $_edit, $_delete)
	{
		try {
			$this->iniciarTransaccion('dat_permisos_update');
			$estados = array(
				'rol' => $rol, 'menu' => $menu, '_list' => $_list, '_add' => $_add, '_edit' => $_edit, '_delete' => $_delete
			);

			$this->oBD->update('permisos ', $estados, array('idpermiso' => $id));
			$this->terminarTransaccion('dat_permisos_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  permisos.idpermiso, permisos.rol, permisos.menu, permisos._list, permisos._add, permisos._edit, permisos._delete  FROM permisos  "
				. " WHERE idpermiso = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			$this->oBD->delete('permisos', array('rol' => $id));
			return $this->oBD->delete('permisos', array('idpermiso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			return $this->oBD->update('permisos', array($propiedad => $valor), array('idpermiso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Permisos") . ": " . $e->getMessage());
		}
	}
}
