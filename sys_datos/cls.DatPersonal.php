<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-10-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatPersonal extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT count(1) FROM personal pe left join persona_rol pr on idpersona=idpersonal";
			$cond = array();
			if (isset($filtros["idpersona"])) {
				$cond[] = "(idpersona = " . $this->oBD->escapar($filtros["idpersona"]) . " OR md5(idpersona)=" . $this->oBD->escapar($filtros["idpersona"]) . ")";
			}
			if (isset($filtros["tipodoc"])) {
				$cond[] = "tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if (isset($filtros["dni"])) {
				$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}

			if (isset($filtros["texto"])) {
				$cond[] = " ( ape_paterno " . $this->oBD->like($filtros["texto"]) . " OR ape_materno " . $this->oBD->like($filtros["texto"]) . " OR nombre " . $this->oBD->like($filtros["texto"]) . " ) ";
			}

			if (isset($filtros["ape_paterno"])) {
				$cond[] = "ape_paterno " . $this->oBD->like($filtros["ape_paterno"]);
			}
			if (isset($filtros["ape_materno"])) {
				$cond[] = "ape_materno " . $this->oBD->like($filtros["ape_materno"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["nombre"]);
			}
			if (isset($filtros["fechanac"])) {
				$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if (isset($filtros["sexo"])) {
				$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if (isset($filtros["estado_civil"])) {
				$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if (isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if (isset($filtros["urbanizacion"])) {
				$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if (isset($filtros["direccion"])) {
				$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if (isset($filtros["telefono"])) {
				$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if (isset($filtros["celular"])) {
				$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if (isset($filtros["email"])) {
				$cond[] = "email " . $this->oBD->like($filtros["email"]);
			}
			if (isset($filtros["idugel"])) {
				$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if (isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if (isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if (isset($filtros["usuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if (isset($filtros["clave"])) {
				$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if (isset($filtros["rol"])) {
				$cond[] = " ( idrol = " . $this->oBD->escapar($filtros["rol"]) . " OR pe.rol=" . $this->oBD->escapar($filtros["rol"]) . ") ";
			}
			if (isset($filtros["token"])) {
				$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["situacion"])) {
				$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if (isset($filtros["idioma"])) {
				$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if (isset($filtros["tipousuario"])) {
				$cond[] = "tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}
			if (isset($filtros["idlocal"])) {
				$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT pe.idpersona, pe.tipodoc, pe.dni, pe.ape_paterno, pe.ape_materno, pe.nombre, pe.fechanac, pe.sexo, pe.estado_civil, pe.ubigeo, pe.urbanizacion, pe.direccion, pe.telefono, pe.celular, pe.email, pe.idugel, pe.regusuario, pe.regfecha, pe.usuario, pe.clave, pe.token, pe.rol, pe.foto, pe.estado, pe.situacion, pe.idioma, pe.tipousuario, pe.idlocal, pe.cod_union, pe.esdemo, pe.emailpadre, idrol, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil,pe.rut FROM personal pe LEFT JOIN persona_rol pr ON idpersona=idpersonal";
			$cond = array();


			$idrol = -1;
			if (!empty($filtros["sqlsolopersona"])) $sql = "SELECT pe.idpersona, pe.tipodoc, pe.dni, pe.ape_paterno, pe.ape_materno, pe.nombre, pe.fechanac, pe.sexo, pe.estado_civil, pe.ubigeo, pe.urbanizacion, pe.direccion, pe.telefono, pe.celular, pe.email, pe.idugel, pe.regusuario, pe.regfecha, pe.usuario, pe.clave, pe.token, pe.rol, pe.foto, pe.estado, pe.situacion, pe.idioma, pe.tipousuario, pe.idlocal, pe.cod_union, pe.esdemo, pe.emailpadre,(SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, (SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil,pe.rut FROM personal pe ";

			else if (isset($filtros["rol"])) {
				$cond[] = " ( idrol = " . $this->oBD->escapar($filtros["rol"]) . " OR pe.rol=" . $this->oBD->escapar($filtros["rol"]) . ") ";
				$idrol = $filtros["rol"];
				if ($idrol == 8) {
					if (!empty($filtros["iddre"])) {
						$ugeles = $this->oBD->consultarSQL("SELECT idugel FROM  ugel WHERE iddepartamento=" . $this->oBD->escapar($filtros["iddre"]));
						$hayugeles = array();
						if (!empty($ugeles))
							foreach ($ugeles as $u) {
								$hayugeles[] = $u["idugel"];
							}
						$ugeles = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " idugel IN(" . $ugeles . ")";
						}
					}
				} else if ($idrol == 7) {
					if (!empty($filtros["idugel"])) {
						$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
					} else if (!empty($filtros["iddre"])) {
						$ugeles = $this->oBD->consultarSQL("SELECT idugel FROM  ugel WHERE iddepartamento=" . $this->oBD->escapar($filtros["iddre"]));
						$hayugeles = array();
						if (!empty($ugeles))
							foreach ($ugeles as $u) {
								$hayugeles[] = $u["idugel"];
							}
						$ugeles = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " idugel IN(" . $ugeles . ")";
						}
					}
				} else if ($idrol == 6 || $idrol == 5 || $idrol == 4) {
					if (!empty($filtros["idiiee"])) {
						$cond[] = " pe.idlocal = " . $this->oBD->escapar($filtros["idiiee"]);
					} else if (!empty($filtros["idugel"])) {
						$locales = $this->oBD->consultarSQL("SELECT idlocal FROM  local WHERE idugel=" . $this->oBD->escapar($filtros["idugel"]));
						$hayugeles = array();
						if (!empty($locales))
							foreach ($locales as $u) {
								$hayugeles[] = $u["idlocal"];
							}
						$locales = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " pe.idlocal IN(" . $locales . ")";
						}
					} else if (!empty($filtros["iddre"])) {
						$ugeles = $this->oBD->consultarSQL("SELECT idugel FROM  ugel WHERE iddepartamento=" . $this->oBD->escapar($filtros["iddre"]));
						$hayugeles = array();
						if (!empty($ugeles))
							foreach ($ugeles as $u) {
								$hayugeles[] = $u["idugel"];
							}
						$ugeles = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " idugel IN(" . $ugeles . ")";
						}
					}
				} else if ($idrol == 2) {
					$sql .= " LEFT JOIN acad_grupoauladetalle ag ON idpersona=ag.iddocente";
					if (isset($filtros["idcurso"])) {
						$cond[] = " idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
					}

					if (!empty($filtros["idiiee"])) {
						$cond[] = " (ag.idlocal = " . $this->oBD->escapar($filtros["idiiee"]) . " OR pe.idlocal = " . $this->oBD->escapar($filtros["idiiee"]) . ")";
					} else if (!empty($filtros["idugel"])) {
						$locales = $this->oBD->consultarSQL("SELECT idlocal FROM  local WHERE idugel=" . $this->oBD->escapar($filtros["idugel"]));
						$hayugeles = array();
						if (!empty($locales))
							foreach ($locales as $u) {
								$hayugeles[] = $u["idlocal"];
							}
						$locales = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " (ag.idlocal IN(" . $locales . ") OR pe.idlocal IN(" . $locales . "))";
						}
					} else if (!empty($filtros["iddre"])) {
						$ugeles = $this->oBD->consultarSQL("SELECT idugel FROM  ugel WHERE iddepartamento=" . $this->oBD->escapar($filtros["iddre"]));
						$hayugeles = array();
						if (!empty($ugeles))
							foreach ($ugeles as $u) {
								$hayugeles[] = $u["idugel"];
							}
						$ugeles = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " idugel IN(" . $ugeles . ")";
						}
					}
				}
			}
			if (isset($filtros["validarusuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]) . " AND idpersona!=" . $this->oBD->escapar($filtros["idpersona"]);
			} else if (isset($filtros["idpersona"])) {
				if (is_array($filtros["idpersona"])) {
					$cond[] = "idpersona IN (" . implode(',', $filtros["idpersona"]) . ")";
				} else {
					$cond[] = "(idpersona = " . $this->oBD->escapar($filtros["idpersona"]) . " OR md5(idpersona)=" . $this->oBD->escapar($filtros["idpersona"]) . " OR dni=" . $this->oBD->escapar($filtros["idpersona"]) . " )";
				}
			}
			if (isset($filtros["_user_"])) {
				$cond[] = " concat(usuario,clave,idpersona) = " . $this->oBD->escapar($filtros["_user_"]);
			}
			if (isset($filtros["tipodoc"])) {
				$cond[] = "tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if (isset($filtros["dni"])) {
				$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = " (dni=" . $this->oBD->escapar($filtros["texto"]) . " OR concat (ape_paterno,' ',ape_materno,' ', pe.nombre) " . $this->oBD->like($filtros["texto"]) . ")";
			}
			if (isset($filtros["ape_paterno"])) {
				$cond[] = "ape_paterno = " . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if (isset($filtros["ape_materno"])) {
				$cond[] = "ape_materno = " . $this->oBD->escapar($filtros["ape_materno"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "pe.nombre  " . $this->oBD->like($filtros["nombre"]);
			}
			if (isset($filtros["fechanac"])) {
				$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if (isset($filtros["sexo"])) {
				$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if (isset($filtros["estado_civil"])) {
				$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if (isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if (isset($filtros["urbanizacion"])) {
				$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if (isset($filtros["direccion"])) {
				$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if (isset($filtros["telefono"])) {
				$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if (isset($filtros["celular"])) {
				$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if (isset($filtros["email"])) {
				$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}

			if (isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if (isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if (isset($filtros["usuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if (isset($filtros["clave"])) {
				$_md5 = preg_match('/^[a-f0-9]{32}$/', $filtros["clave"]) == true ? $filtros["clave"] : md5($filtros["clave"]);
				$cond[] = "clave = " . $this->oBD->escapar($_md5);
			}
			if (isset($filtros["token"])) {
				$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}

			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["situacion"])) {
				$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if (isset($filtros["idioma"])) {
				$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if (isset($filtros["idproyecto"]) &&  empty($filtros["sqlsolopersona"])) {
				$cond[] = "pr.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idempresa"])) {
				$cond[] = "pr.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if (isset($filtros["idrol"])) {
				$cond[] = "pr.idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}


			/*if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}*/
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (empty($filtros["sqlsolopersona"])) {
				$sql .= " group by idpersona ORDER BY ape_paterno ASC , regfecha DESC";
			}

			if (isset($filtros['sql4'])) {
				$sql = "SELECT COUNT(idpersona) as numero FROM personal p WHERE p.clave = " . $this->oBD->escapar(md5($filtros["clave"])) . " AND p.idpersona = " . $this->oBD->escapar($filtros["sql4"]);
			}
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}
	public function buscar_paracertificado($filtros = null)
	{
		try {
			$sql = "SELECT pe.idpersona, pe.tipodoc, pe.dni, pe.ape_paterno, pe.ape_materno, pe.nombre, pe.fechanac, pe.sexo, pe.estado_civil, pe.ubigeo, pe.urbanizacion, pe.direccion, pe.telefono, pe.celular, pe.email, pe.idugel, pe.regusuario, pe.regfecha, pe.usuario, pe.clave, pe.token, pe.rol, pe.foto, pe.estado, pe.situacion, pe.idioma, pe.tipousuario, pe.idlocal, pe.cod_union, pe.esdemo, pe.emailpadre, idrol, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe LEFT JOIN persona_rol pr ON idpersona=idpersonal";
			$cond = array();
			$idrol = -1;
			if (isset($filtros["rol"])) {
				$cond[] = " ( idrol = " . $this->oBD->escapar($filtros["rol"]) . " OR pe.rol=" . $this->oBD->escapar($filtros["rol"]) . ") ";
				$idrol = $filtros["rol"];
				if ($idrol == 8) {
					if (!empty($filtros["iddre"])) {
						$ugeles = $this->oBD->consultarSQL("SELECT idugel FROM  ugel WHERE iddepartamento=" . $this->oBD->escapar($filtros["iddre"]));
						$hayugeles = array();
						if (!empty($ugeles))
							foreach ($ugeles as $u) {
								$hayugeles[] = $u["idugel"];
							}
						$ugeles = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " idugel IN(" . $ugeles . ")";
						}
					}
				} else if ($idrol == 7) {
					if (!empty($filtros["idugel"])) {
						$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
					} else if (!empty($filtros["iddre"])) {
						$ugeles = $this->oBD->consultarSQL("SELECT idugel FROM  ugel WHERE iddepartamento=" . $this->oBD->escapar($filtros["iddre"]));
						$hayugeles = array();
						if (!empty($ugeles))
							foreach ($ugeles as $u) {
								$hayugeles[] = $u["idugel"];
							}
						$ugeles = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " idugel IN(" . $ugeles . ")";
						}
					}
				} else if ($idrol == 6 || $idrol == 5 || $idrol == 4) {
					if (!empty($filtros["idiiee"])) {
						$cond[] = " pe.idlocal = " . $this->oBD->escapar($filtros["idiiee"]);
					} else if (!empty($filtros["idugel"])) {
						$locales = $this->oBD->consultarSQL("SELECT idlocal FROM  local WHERE idugel=" . $this->oBD->escapar($filtros["idugel"]));
						$hayugeles = array();
						if (!empty($locales))
							foreach ($locales as $u) {
								$hayugeles[] = $u["idlocal"];
							}
						$locales = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " pe.idlocal IN(" . $locales . ")";
						}
					} else if (!empty($filtros["iddre"])) {
						$ugeles = $this->oBD->consultarSQL("SELECT idugel FROM  ugel WHERE iddepartamento=" . $this->oBD->escapar($filtros["iddre"]));
						$hayugeles = array();
						if (!empty($ugeles))
							foreach ($ugeles as $u) {
								$hayugeles[] = $u["idugel"];
							}
						$ugeles = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " idugel IN(" . $ugeles . ")";
						}
					}
				} else if ($idrol == 2) {
					$sql .= " LEFT JOIN acad_grupoauladetalle ag ON idpersona=ag.iddocente";
					if (isset($filtros["idcurso"])) {
						$cond[] = " idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
					}

					if (!empty($filtros["idiiee"])) {
						$cond[] = " (ag.idlocal = " . $this->oBD->escapar($filtros["idiiee"]) . " OR pe.idlocal = " . $this->oBD->escapar($filtros["idiiee"]) . ")";
					} else if (!empty($filtros["idugel"])) {
						$locales = $this->oBD->consultarSQL("SELECT idlocal FROM  local WHERE idugel=" . $this->oBD->escapar($filtros["idugel"]));
						$hayugeles = array();
						if (!empty($locales))
							foreach ($locales as $u) {
								$hayugeles[] = $u["idlocal"];
							}
						$locales = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " (ag.idlocal IN(" . $locales . ") OR pe.idlocal IN(" . $locales . "))";
						}
					} else if (!empty($filtros["iddre"])) {
						$ugeles = $this->oBD->consultarSQL("SELECT idugel FROM  ugel WHERE iddepartamento=" . $this->oBD->escapar($filtros["iddre"]));
						$hayugeles = array();
						if (!empty($ugeles))
							foreach ($ugeles as $u) {
								$hayugeles[] = $u["idugel"];
							}
						$ugeles = join(",", $hayugeles);
						if (!empty($hayugeles)) {
							$cond[] = " idugel IN(" . $ugeles . ")";
						}
					}
				}
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "((idpersona = " . $this->oBD->escapar($filtros["idpersona"]) . " OR md5(idpersona)=" . $this->oBD->escapar($filtros["idpersona"]) . ") OR dni=" . $this->oBD->escapar($filtros["idpersona"]) . ")";
			}
			if (isset($filtros["tipodoc"])) {
				$cond[] = "tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if (isset($filtros["dni"])) {
				$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = " (dni=" . $this->oBD->escapar($filtros["texto"]) . " OR concat (ape_paterno,' ',ape_materno,' ', pe.nombre) " . $this->oBD->like($filtros["texto"]) . ")";
			}
			if (isset($filtros["ape_paterno"])) {
				$cond[] = "ape_paterno = " . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if (isset($filtros["ape_materno"])) {
				$cond[] = "ape_materno = " . $this->oBD->escapar($filtros["ape_materno"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "pe.nombre  " . $this->oBD->like($filtros["nombre"]);
			}
			if (isset($filtros["fechanac"])) {
				$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if (isset($filtros["sexo"])) {
				$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if (isset($filtros["estado_civil"])) {
				$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if (isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if (isset($filtros["urbanizacion"])) {
				$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if (isset($filtros["direccion"])) {
				$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if (isset($filtros["telefono"])) {
				$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if (isset($filtros["celular"])) {
				$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if (isset($filtros["email"])) {
				$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}

			if (isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if (isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if (isset($filtros["usuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if (isset($filtros["clave"])) {
				$cond[] = "clave = " . $this->oBD->escapar(md5($filtros["clave"]));
			}
			if (isset($filtros["token"])) {
				$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}

			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["situacion"])) {
				$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if (isset($filtros["idioma"])) {
				$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "pr.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idempresa"])) {
				$cond[] = "pr.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if (isset($filtros["idrol"])) {
				$cond[] = "pr.idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}

			/*if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}*/
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= "  ORDER BY ape_paterno ASC , regfecha DESC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}
	public function webService($filtros)
	{
		try {

			$sql = "SELECT p.idpersona, p.tipodoc, p.dni, p.ape_paterno, p.ape_materno, p.nombre, p.fechanac, p.sexo, p.estado_civil, p.ubigeo, p.urbanizacion, p.direccion, p.telefono, p.celular, p.email, p.idugel, p.regusuario, p.regfecha, p.usuario, p.clave, p.token, p.rol, p.foto, p.estado, p.situacion, p.idioma, p.tipousuario, p.idlocal, p.cod_union, p.esdemo, p.emailpadre FROM personal p INNER JOIN persona_rol pr ON (pr.idpersonal = p.idpersona)";

			$cond = array();
			if (isset($filtros["tipodoc"])) {
				$cond[] = "p.tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if (isset($filtros["dni"])) {
				$cond[] = "p.dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if (isset($filtros["ape_paterno"])) {
				$cond[] = "UPPER(p.ape_paterno) = " . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if (isset($filtros["idrol"])) {
				$cond[] = "pr.idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "pr.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idempresa"])) {
				$cond[] = "pr.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception(JrTexto::_('Error') . "\n " . JrTexto::_('Validate') . ' ' . JrTexto::_('enter') . $e->getMessage());
		}
	}
	public function getxCredencial($usuario, $clave)
	{
		try {

			$sql = "SELECT pe.idpersona, pe.tipodoc, pe.dni, pe.ape_paterno, pe.ape_materno, pe.nombre, pe.fechanac, pe.sexo, pe.estado_civil, pe.ubigeo, pe.urbanizacion, pe.direccion, pe.telefono, pe.celular, pe.email, pe.idugel, pe.regusuario, pe.regfecha, pe.usuario, pe.clave, pe.token, pe.rol, pe.foto, pe.estado, pe.situacion, pe.idioma, pe.tipousuario, pe.idlocal, pe.cod_union, pe.esdemo, pe.emailpadre, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe "
				. " WHERE (usuario = " . $this->oBD->escapar($usuario) . " OR email=" . $this->oBD->escapar($usuario) . ") "
				. " AND clave = '" . md5($clave) . "' AND estado=1";
			$res = $this->oBD->consultarSQL($sql);

			return !empty($res) ? $res[0] : null;
		} catch (Exception $e) {
			throw new Exception(JrTexto::_('Error') . "\n " . JrTexto::_('Validate') . ' ' . JrTexto::_('enter') . $e->getMessage());
		}
	}
	public function getxusuarioemail($usuema)
	{
		try {
			$sql = "SELECT pe.idpersona, pe.tipodoc, pe.dni, pe.ape_paterno, pe.ape_materno, pe.nombre, pe.fechanac, pe.sexo, pe.estado_civil, pe.ubigeo, pe.urbanizacion, pe.direccion, pe.telefono, pe.celular, pe.email, pe.idugel, pe.regusuario, pe.regfecha, pe.usuario, pe.clave, pe.token, pe.rol, pe.foto, pe.estado, pe.situacion, pe.idioma, pe.tipousuario, pe.idlocal, pe.cod_union, pe.esdemo, pe.emailpadre, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe "
				. " WHERE usuario = " . $this->oBD->escapar($usuema) . " OR email = " . $this->oBD->escapar($usuema);

			$res = $this->oBD->consultarSQL($sql);

			return !empty($res) ? $res[0] : null;
		} catch (Exception $e) {
			throw new Exception(JrTexto::_('Error') . "\n " . JrTexto::_('Get') . ' ' . JrTexto::_('user') . $e->getMessage());
		}
	}

	public function getxusuario($usuario)
	{
		try {
			$sql = "SELECT pe.idpersona, pe.tipodoc, pe.dni, pe.ape_paterno, pe.ape_materno, pe.nombre, pe.fechanac, pe.sexo, pe.estado_civil, pe.ubigeo, pe.urbanizacion, pe.direccion, pe.telefono, pe.celular, pe.email, pe.idugel, pe.regusuario, pe.regfecha, pe.usuario, pe.clave, pe.token, pe.rol, pe.foto, pe.estado, pe.situacion, pe.idioma, pe.tipousuario, pe.idlocal, pe.cod_union, pe.esdemo, pe.emailpadre, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe WHERE usuario = " . $this->oBD->escapar($usuario);

			$res = $this->oBD->consultarSQL($sql);

			return !empty($res) ? $res[0] : null;
		} catch (Exception $e) {
			throw new Exception(JrTexto::_('Error') . "\n " . JrTexto::_('Get') . ' ' . JrTexto::_('users') . $e->getMessage());
		}
	}

	public function getxtoken($token)
	{
		try {
			$sql = "SELECT pe.idpersona, pe.tipodoc, pe.dni, pe.ape_paterno, pe.ape_materno, pe.nombre, pe.fechanac, pe.sexo, pe.estado_civil, pe.ubigeo, pe.urbanizacion, pe.direccion, pe.telefono, pe.celular, pe.email, pe.idugel, pe.regusuario, pe.regfecha, pe.usuario, pe.clave, pe.token, pe.rol, pe.foto, pe.estado, pe.situacion, pe.idioma, pe.tipousuario, pe.idlocal, pe.cod_union, pe.esdemo, pe.emailpadre, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe  WHERE token = " . $this->oBD->escapar($token);

			$res = $this->oBD->consultarSQL($sql);

			return !empty($res) ? $res[0] : null;
		} catch (Exception $e) {
			throw new Exception(JrTexto::_('Error') . "\n " . JrTexto::_('Get') . ' ' . JrTexto::_('user') . $e->getMessage());
		}
	}


	public function insertar($tipodoc, $dni, $ape_paterno, $ape_materno, $nombre, $fechanac, $sexo, $estado_civil, $ubigeo, $urbanizacion, $direccion, $telefono, $celular, $email, $idugel, $regusuario, $regfecha, $usuario, $clave, $token, $rol, $foto, $estado, $situacion, $idioma, $tipousuario, $idlocal, $esdemo)
	{
		try {

			$this->iniciarTransaccion('dat_personal_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
			++$id;

			if (empty($usuario)) {
				$n1 = substr(str_replace(' ', '', trim($nombre)), 0, 5);
				$n2 = substr($ape_paterno, 0, 1);
				$usuario = ucfirst($n1 . $n2 . $dni);
				$clave = !empty($clave) ? $clave : $usuario;
				$token = $clave;
			}

			$estados = array(
				'idpersona' => $id, 'tipodoc' => !empty($tipodoc) ? $tipodoc : 1, 'dni' => $dni, 'ape_paterno' => $ape_paterno, 'ape_materno' => $ape_materno, 'nombre' => $nombre, 'fechanac' => $fechanac, 'sexo' => $sexo, 'estado_civil' => $estado_civil, 'ubigeo' => $ubigeo, 'urbanizacion' => $urbanizacion, 'direccion' => $direccion, 'telefono' => $telefono, 'celular' => $celular, 'email' => $email, 'idugel' => $idugel, 'regusuario' => $regusuario, 'regfecha' => !empty($regfecha) ? $regfecha : date('Y-m-d'), 'usuario' => $usuario, 'clave' => md5($clave), 'token' => !empty($token) ? $token : $clave, 'rol' => !empty($rol) ? $rol : 0, 'foto' => $foto, 'estado' => !empty($estado) ? $estado : 1, 'situacion' => !empty($situacion) ? $situacion : 1, 'idioma' => !empty($idioma) ? $idioma : 'ES', 'idlocal' => !empty($idlocal) ? $idlocal : 1, 'esdemo' => !empty($esdemo) ? $esdemo : 0
			);

			$this->oBD->insert('personal', $estados);
			$this->terminarTransaccion('dat_personal_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_personal_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}

	public function importar($id, $tipodoc, $dni, $ape_paterno, $ape_materno, $nombre, $fechanac, $sexo, $estado_civil, $ubigeo, $urbanizacion, $direccion, $telefono, $celular, $email, $idugel, $regusuario, $regfecha, $usuario, $clave, $token, $rol, $foto, $estado, $situacion, $idioma, $tipousuario, $idlocal)
	{
		try {
			if (empty($id)) return $this->insertar($tipodoc, $dni, $ape_paterno, $ape_materno, $nombre, $fechanac, $sexo, $estado_civil, $ubigeo, $urbanizacion, $direccion, $telefono, $celular, $email, $idugel, $regusuario, $regfecha, $usuario, $clave, $token, $rol, $foto, $estado, $situacion, $idioma, $tipousuario, $idlocal, 0);
			else {
				$this->iniciarTransaccion('dat_persona_insert2');
				$estados = array(
					'idpersona' => $id, 'tipodoc' => !empty($tipodoc) ? $tipodoc : 1, 'dni' => $dni, 'ape_paterno' => $ape_paterno, 'ape_materno' => $ape_materno, 'nombre' => $nombre, 'fechanac' => $fechanac, 'sexo' => $sexo, 'estado_civil' => $estado_civil, 'ubigeo' => $ubigeo, 'urbanizacion' => $urbanizacion, 'direccion' => $direccion, 'telefono' => $telefono, 'celular' => $celular, 'email' => $email, 'idugel' => $idugel, 'regusuario' => $regusuario, 'regfecha' => !empty($regfecha) ? $regfecha : date('Y-m-d'), 'usuario' => $usuario
					//,'clave'=>md5($clave)
					//,'token'=>!empty($token)?$token:$clave
					, 'rol' => !empty($rol) ? $rol : 0, 'foto' => $foto, 'estado' => !empty($estado) ? $estado : 1, 'situacion' => !empty($situacion) ? $situacion : 1, 'idioma' => !empty($idioma) ? $idioma : 'ES', 'idlocal' => $idlocal
				);
				if (!empty($clave)) {
					$estados["clave"] = md5($clave);
					$estados["token"] = !empty($token) ? $token : $clave;
				}
				//var_dump($estados);
				$this->oBD->insert('personal', $estados);
				$this->terminarTransaccion('dat_persona_insert2');
				return $id;
			}
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_persona_insert2');
			return -1;
		}
	}




	public function actualizar($id, $tipodoc, $dni, $ape_paterno, $ape_materno, $nombre, $fechanac, $sexo, $estado_civil, $ubigeo, $urbanizacion, $direccion, $telefono, $celular, $email, $idugel, $regusuario, $regfecha, $usuario, $clave, $token, $rol, $foto, $estado, $situacion, $idioma, $tipousuario, $idlocal, $esdemo)
	{
		try {
			$this->iniciarTransaccion('dat_personal_update');
			$estados = array(
				'tipodoc' => $tipodoc, 'dni' => $dni, 'ape_paterno' => $ape_paterno, 'ape_materno' => $ape_materno, 'nombre' => $nombre, 'fechanac' => $fechanac, 'sexo' => $sexo, 'estado_civil' => $estado_civil, 'ubigeo' => $ubigeo, 'urbanizacion' => $urbanizacion, 'direccion' => $direccion, 'telefono' => $telefono, 'celular' => $celular, 'email' => $email, 'idugel' => $idugel, 'regusuario' => $regusuario, 'regfecha' => $regfecha, 'usuario' => $usuario
				//,'clave'=>md5($clave)
				//,'token'=>$token
				, 'rol' => $rol, 'foto' => $foto, 'estado' => $estado, 'situacion' => $situacion, 'idioma' => $idioma, 'tipousuario' => $tipousuario, 'idlocal' => !empty($idlocal) ? $idlocal : 0, 'esdemo' => !empty($esdemo) ? $esdemo : 0
			);
			if (!empty($token))
				if ($token == 'cambiarclave' && !empty($clave)) {
					$estados["clave"] = md5($clave);
					$estados["token"] = !empty($token) ? $token : $clave;
				}

			$this->oBD->update('personal ', $estados, array('idpersona' => $id));
			$this->terminarTransaccion('dat_personal_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Personal") . ":" . $e->getMessage());
		}
	}
	public function actualizar_($id, $estados = array())
	{
		try {
			$this->iniciarTransaccion('dat_personal_update');
			$this->oBD->update('personal', $estados, array('idpersona' => $id));
			$this->terminarTransaccion('dat_personal_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Personal") . ":" . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT pe.idpersona, pe.tipodoc, pe.dni, pe.ape_paterno, pe.ape_materno, pe.nombre, pe.fechanac, pe.sexo, pe.estado_civil, pe.ubigeo, pe.urbanizacion, pe.direccion, pe.telefono, pe.celular, pe.email, pe.idugel, pe.regusuario, pe.regfecha, pe.usuario, pe.clave, pe.token, pe.rol, pe.foto, pe.estado, pe.situacion, pe.idioma, pe.tipousuario, pe.idlocal, pe.cod_union, pe.esdemo, pe.emailpadre, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, (SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil , (SELECT nombre from local g WHERE pe.idlocal=g.idlocal limit 0,1) AS strlocal, (SELECT descripcion from ugel g WHERE pe.idugel=g.idugel limit 0,1) AS strugel FROM personal pe WHERE idpersona = " . $this->oBD->escapar($id) . " OR md5(idpersona)=" . $this->oBD->escapar($id) . " OR dni= " . $this->oBD->escapar($id);
			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}
	public function borrarhistorial($idpersona)
	{
		try {
			$this->oBD->delete('actividad_alumno', array('idalumno' => $idpersona));
			$this->oBD->delete('bitacora_alumno_smartbook', array('idusuario' => $idpersona));
			$this->oBD->delete('bitacora_smartbook', array('idusuario' => $idpersona));
			$this->oBD->delete('bib_historial', array('id_personal' => $idpersona));
			return $idpersona;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			$this->borrarhistorial($id);
			return $this->oBD->delete('personal', array('idpersona' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}
	public function eliminarusuario($data){
		try{
			$this->iniciarTransaccion('dat_personal_delete');
			if(empty($data)){ throw new Exception("Data vacio al eliminar"); }
			foreach($data as $id){
				$this->borrarhistorial($id);
				$this->oBD->delete('persona_rol', array('idpersonal' => $id));
				$this->oBD->delete('personal', array('idpersona' => $id));
			}
			$this->terminarTransaccion('dat_personal_delete');
			return true;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_personal_delete');
			throw new Exception("ERROR\n" . JrTexto::_("eliminarusuario") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('personal', array($propiedad => $valor), array('idpersona' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Personal") . ": " . $e->getMessage());
		}
	}

	public function actualizartabla($tabla, $campo)
	{
		$dt = $this->oBD->consultarSQL("SELECT personal.idpersona, personal.tipodoc, personal.dni, personal.ape_paterno, personal.ape_materno, personal.nombre, personal.fechanac, personal.sexo, personal.estado_civil, personal.ubigeo, personal.urbanizacion, personal.direccion, personal.telefono, personal.celular, personal.email, personal.idugel, personal.regusuario, personal.regfecha, personal.usuario, personal.clave, personal.token, personal.rol, personal.foto, personal.estado, personal.situacion, personal.idioma, personal.tipousuario, personal.idlocal, personal.cod_union, personal.esdemo, personal.emailpadre FROM personal");
		foreach ($dt as $per) {
			$x = $this->oBD->update($tabla, array($campo => $per["idpersona"]), array($campo => $per["dni"]));
			//echo $x."- ".$per["dni"]." - ".$campo;
		}
	}
}
