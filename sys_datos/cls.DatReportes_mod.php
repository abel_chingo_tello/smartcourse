<?php
class DatReportes_mod extends DatBase
{
    public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("reportes").": " . $e->getMessage());
		}
    }
    public function findAlumnosxgrupo($filtros = array()){
        try{
            $cond = [];
            $sql = "SELECT 
                mat.idalumno AS idalumno,
                mat.idmatricula AS idmatricula,
                grupodet.idgrupoauladetalle AS idgrupoauladetalle,
                per.tipodoc AS tipodocumento,
                per.dni AS documento,
                per.nombre AS nombre,
                per.ape_paterno AS apellidopaterno,
                per.ape_materno AS apellidomaterno,
                grupoaula.nombre AS nombre_grupo,
                grupodet.nombre AS nombre_grupodetalle,
                UPPER(CONCAT(per.nombre, ' ', per.ape_paterno,' ',per.ape_materno)) AS nombrecompleto
            FROM acad_matricula mat INNER JOIN acad_grupoauladetalle grupodet ON mat.idgrupoauladetalle = grupodet.idgrupoauladetalle
            INNER JOIN personal per ON mat.idalumno = per.idpersona
            LEFT JOIN acad_grupoaula grupoaula ON grupodet.idgrupoaula = grupoaula.idgrupoaula";


            if(!empty($filtros['idcurso'])){
                $cond[] = "grupodet.idcurso = ".$this->oBD->escapar($filtros['idcurso']);
            }
            if(!empty($filtros['idgrupoauladetalle'])){
                $cond[] = "grupodet.idgrupoauladetalle = ".$this->oBD->escapar($filtros['idgrupoauladetalle']);
            }
            if(!empty($filtros['idalumno'])){
                if(is_array($filtros['idalumno'])){
                    $cond[] = "mat.idalumno IN (".implode(',',$filtros['idalumno']).")";
                }else{
                    $cond[] = "mat.idalumno = ".$this->oBD->escapar($filtros['idalumno']);
                }
            }

            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
            throw new Exception("Error en datos- findAlumnosxgrupo ". $e->getMessage());
        }
    }

}