<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatRecursivo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Recursivo") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT recursivo.idrecursivo, recursivo.comprobar, recursivo.fechaejecucion, recursivo.fechafinalizacion FROM recursivo";

			$cond = array();


			if (isset($filtros["comprobar"])) {
				$cond[] = "comprobar = " . $this->oBD->escapar($filtros["comprobar"]);
			}
			if (isset($filtros["fechaejecucion"])) {
				$cond[] = "fechaejecucion = " . $this->oBD->escapar($filtros["fechaejecucion"]);
			}
			if (isset($filtros["fechafinalizacion"])) {
				$cond[] = "fechafinalizacion = " . $this->oBD->escapar($filtros["fechafinalizacion"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Recursivo") . ": " . $e->getMessage());
		}
	}


	public function insertar($comprobar, $fechaejecucion, $fechafinalizacion)
	{
		try {

			$this->iniciarTransaccion('dat_recursivo_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrecursivo) FROM recursivo");
			++$id;

			$estados = array(
				'idrecursivo' => $id, 'comprobar' => $comprobar, 'fechaejecucion' => $fechaejecucion, 'fechafinalizacion' => $fechafinalizacion
			);

			$this->oBD->insert('recursivo', $estados);
			$this->terminarTransaccion('dat_recursivo_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_recursivo_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Recursivo") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $comprobar, $fechaejecucion, $fechafinalizacion)
	{
		try {
			$this->iniciarTransaccion('dat_recursivo_update');
			$estados = array(
				'comprobar' => $comprobar, 'fechaejecucion' => $fechaejecucion, 'fechafinalizacion' => $fechafinalizacion
			);

			$this->oBD->update('recursivo ', $estados, array('idrecursivo' => $id));
			$this->terminarTransaccion('dat_recursivo_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Recursivo") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  recursivo.idrecursivo, recursivo.comprobar, recursivo.fechaejecucion, recursivo.fechafinalizacion  FROM recursivo  "
				. " WHERE idrecursivo = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Recursivo") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('recursivo', array('idrecursivo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Recursivo") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('recursivo', array($propiedad => $valor), array('idrecursivo' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Recursivo") . ": " . $e->getMessage());
		}
	}
}
