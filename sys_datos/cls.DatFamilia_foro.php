<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-03-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatFamilia_foro extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Familia_foro").": " . $e->getMessage());
		}
	}

	public $resultado = array();

	public function ordenarDatos($rs = array(), $respuesta = 0) {
        if (!empty($rs)) {
            foreach ($rs as $element) {
                if ($element["respuesta"] == $respuesta) {
                    array_push($this->resultado, $element);
                    if ($element["hijos"] > 0) {
                        $this->ordenarDatos($rs, $element["codigo_consulta"]);
                    }
                }
            }
        }
    }
	
	public function buscar($filtros=null)
	{
		try {
			if(isset($filtros["sql"])) {
				switch ($filtros["sql"]) {
					case '1':
						$sql = "SELECT ff.codigo, ff.persona, ff.idproyecto, ff.idempresa, ff.likes, ff.like_de, ff.contenido, ff.fecha_hora, ff.respuesta, 
						DATE_FORMAT(ff.fecha_hora, '%d/%m/%Y %h:%i %p') as fecha, (select count(*) from familia_foro where respuesta = ff.codigo) as hijos, '' as padre FROM familia_foro ff";
						$cond = array();
						if(isset($filtros["idproyecto"])) {
							$cond[] = "ff.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
						}
						if(isset($filtros["idempresa"])) {
								$cond[] = "ff.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
						}
						if(!empty($cond)) {
							$sql .= " WHERE " . implode(' AND ', $cond);
						}
						$sql .= "ORDER BY ff.respuesta, ff.fecha_hora";
						// echo $sql;exit();
						$rs = $this->oBD->consultarSQL($sql);
						foreach ($rs as $element) {
							if (is_null($element["respuesta"])) {
								array_push($this->resultado, array_replace($element, array("padre" => 'p')));
								$this->ordenarDatos($rs, $element["codigo"]);
							}
						}
						return $this->resultado;
						break;
				}
			}
			$sql = "SELECT * FROM familia_foro";			
			
			$cond = array();		
					
			if(isset($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(isset($filtros["persona"])) {
					$cond[] = "persona = " . $this->oBD->escapar($filtros["persona"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["likes"])) {
					$cond[] = "likes = " . $this->oBD->escapar($filtros["likes"]);
			}
			if(isset($filtros["like_de"])) {
					$cond[] = "like_de = " . $this->oBD->escapar($filtros["like_de"]);
			}
			if(isset($filtros["contenido"])) {
					$cond[] = "contenido = " . $this->oBD->escapar($filtros["contenido"]);
			}
			if(isset($filtros["fecha_hora"])) {
					$cond[] = "fecha_hora = " . $this->oBD->escapar($filtros["fecha_hora"]);
			}
			if(isset($filtros["respuesta"])) {
					$cond[] = "respuesta = " . $this->oBD->escapar($filtros["respuesta"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Familia_foro").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($estados)
	{
		try {
			
			$this->iniciarTransaccion('dat_familia_foro_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(codigo) FROM familia_foro");
			++$id;
			
			$estados['codigo'] = $id;
			
			$this->oBD->insert('familia_foro', $estados);			
			$this->terminarTransaccion('dat_familia_foro_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_familia_foro_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Familia_foro").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $persona,$idproyecto,$idempresa,$likes,$like_de,$contenido,$fecha_hora,$respuesta)
	{
		try {
			$this->iniciarTransaccion('dat_familia_foro_update');
			$estados = array('persona'=>$persona
							,'idproyecto'=>$idproyecto
							,'idempresa'=>$idempresa
							,'likes'=>$likes
							,'like_de'=>$like_de
							,'contenido'=>$contenido
							,'fecha_hora'=>$fecha_hora
							,'respuesta'=>$respuesta								
							);
			
			$this->oBD->update('familia_foro ', $estados, array('codigo' => $id));
		    $this->terminarTransaccion('dat_familia_foro_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Familia_foro").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT familia_foro.codigo, familia_foro.persona, familia_foro.idproyecto, familia_foro.idempresa, familia_foro.likes, familia_foro.like_de, familia_foro.contenido, familia_foro.fecha_hora, familia_foro.respuesta  FROM familia_foro  "
					. " WHERE codigo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Familia_foro").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('familia_foro', array('codigo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Familia_foro").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('familia_foro', array($propiedad => $valor), array('codigo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Familia_foro").": " . $e->getMessage());
		}
	}
   
		
}