<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-12-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatDescargas_asignadas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Recursos_colaborativos").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT re.id,re.idpersona,total_asignado,re.fecha_registro,codigo_suscripcion,re.idempresa,idproyecto,(SELECT count(1) FROM descargas_consumidas WHERE idasignado=id) as total_consumido,concat(pe.ape_paterno,' ',pe.ape_materno,', ', pe.nombre) as strpersona FROM descargas_asignadas re LEFT join personal pe ON re.idpersona=pe.idpersona";
			$cond = array();
			if(isset($filtros["sqlporconsumir"])){
				$sql = "SELECT re.id,re.idpersona,total_asignado,re.fecha_registro,codigo_suscripcion,re.idempresa,idproyecto,(SELECT count(1) FROM descargas_consumidas WHERE idasignado=id) as total_consumido,concat(pe.ape_paterno,' ',pe.ape_materno,', ', pe.nombre) as strpersona FROM descargas_asignadas re LEFT join personal pe ON re.idpersona=pe.idpersona";				
				$filtros['_condicion_']='total_asignado>(SELECT count(1) FROM descargas_consumidas WHERE idasignado=id)';
				$filtros["orderby"]=' id ASC';
			}

			
			if(isset($filtros["id"])) {
					$cond[] = "re.id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "re.idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["total_asignado"])) {
					$cond[] = "re.total_asignado = " . $this->oBD->escapar($filtros["total_asignado"]);
			}
			
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "re.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["codigo_suscripcion"])) {
					$cond[] = "re.codigo_suscripcion = " . $this->oBD->escapar($filtros["codigo_suscripcion"]);
			}

			if(isset($filtros["idempresa"])) {
					$cond[] = "re.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "re.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(isset($filtros["texto"])) {
				$cond[] = "strpersona " . $this->oBD->like($filtros["texto"]);
			}

			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM descargas_asignadas";
			}
			$where='';
			if(!empty($cond)) {
				$where= " WHERE " . implode(' AND ', $cond);
				if(!empty($filtros['_condicion_'])){
					$where.=' AND ('. $filtros['_condicion_'].')';
				}
			}else{
				if(!empty($filtros['_condicion_'])){
					$where.=' WHERE ('. $filtros['_condicion_'].')';
				}
			}

			$sql.=$where;

			if(!empty($filtros["orderby"])){
				$sql.=" ORDER BY ".$filtros["orderby"];
			}

			if(isset($filtros["contarconsumidas"])){
				$sql="SELECT SUM(total_asignado) AS total_descargas, (SELECT count(1) from descargas_consumidas WHERE idasignado IN(SELECT id FROM descargas_asignadas re ".$where.")) AS total_consumidas  FROM descargas_asignadas re ".$where;
			}	
			//echo $sql;		
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("descargas_asignadas").": " . $e->getMessage());
		}
	}
		
	public function insertar($idpersona,$total_asignado,$codigo_suscripcion,$idempresa,$idproyecto)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT id+1 FROM descargas_asignadas ORDER BY id DESC limit 0,1 ");			
			$estados = array('idpersona'=>$idpersona
							,'total_asignado'=>$total_asignado							
							,'codigo_suscripcion'=>$codigo_suscripcion
							,'idempresa'=>$idempresa
							,'idproyecto'=>$idproyecto
							);			
			return $this->oBD->insertAI('descargas_asignadas', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_descargas_asignadas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("descargas_asignadas").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona,$total_asignado,$codigo_suscripcion,$idempresa,$idproyecto)
	{
		try {			
			$estados = array('idpersona'=>$idpersona
							,'total_asignado'=>$total_asignado							
							,'codigo_suscripcion'=>$codigo_suscripcion
							,'idempresa'=>$idempresa
							,'idproyecto'=>$idproyecto
							);
			$this->oBD->update('descargas_asignadas ', $estados, array('id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("descargas_asignadas").": " . $e->getMessage());
		}
	}

	public function actualizar2($estados,$condicion){
		try {
			return $this->oBD->update('descargas_asignadas ', $estados, $condicion);		    
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("descargas_asignadasasignacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('descargas_asignadas', array('id' => $id));
			else 
				return $this->oBD->update('descargas_asignadas', array('estado' => -1), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("descargas_asignadas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('descargas_asignadas', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("descargas_asignadas").": " . $e->getMessage());
		}
	}  	 
		
}