<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-09-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatRecursos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Recursos") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT recursos.idrecurso, recursos.idnivel, recursos.idunidad, recursos.idactividad, recursos.idpersonal, recursos.titulo, recursos.texto, recursos.caratula, recursos.orden, recursos.tipo, recursos.publicado, recursos.compartido FROM recursos";

			$cond = array();


			if (isset($filtros["idnivel"])) {
				$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if (isset($filtros["idunidad"])) {
				$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if (isset($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if (isset($filtros["idpersonal"])) {
				$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if (isset($filtros["titulo"])) {
				$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if (isset($filtros["caratula"])) {
				$cond[] = "caratula = " . $this->oBD->escapar($filtros["caratula"]);
			}
			if (isset($filtros["orden"])) {
				$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["publicado"])) {
				$cond[] = "publicado = " . $this->oBD->escapar($filtros["publicado"]);
			}
			if (isset($filtros["compartido"])) {
				$cond[] = "compartido = " . $this->oBD->escapar($filtros["compartido"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Recursos") . ": " . $e->getMessage());
		}
	}


	public function insertar($idnivel, $idunidad, $idactividad, $idpersonal, $titulo, $texto, $caratula, $orden, $tipo, $publicado, $compartido)
	{
		try {

			$this->iniciarTransaccion('dat_recursos_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrecurso) FROM recursos");
			++$id;

			$estados = array(
				'idrecurso' => $id, 'idnivel' => $idnivel, 'idunidad' => $idunidad, 'idactividad' => $idactividad, 'idpersonal' => $idpersonal, 'titulo' => $titulo, 'texto' => $texto, 'caratula' => $caratula, 'orden' => $orden, 'tipo' => $tipo, 'publicado' => $publicado, 'compartido' => $compartido
			);

			$this->oBD->insert('recursos', $estados);
			$this->terminarTransaccion('dat_recursos_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_recursos_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Recursos") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel, $idunidad, $idactividad, $idpersonal, $titulo, $texto, $caratula, $orden, $tipo, $publicado, $compartido)
	{
		try {
			$this->iniciarTransaccion('dat_recursos_update');
			$estados = array(
				'idnivel' => $idnivel, 'idunidad' => $idunidad, 'idactividad' => $idactividad, 'idpersonal' => $idpersonal, 'titulo' => $titulo, 'texto' => $texto, 'caratula' => $caratula, 'orden' => $orden, 'tipo' => $tipo, 'publicado' => $publicado, 'compartido' => $compartido
			);

			$this->oBD->update('recursos ', $estados, array('idrecurso' => $id));
			$this->terminarTransaccion('dat_recursos_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Recursos") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  recursos.idrecurso, recursos.idnivel, recursos.idunidad, recursos.idactividad, recursos.idpersonal, recursos.titulo, recursos.texto, recursos.caratula, recursos.orden, recursos.tipo, recursos.publicado, recursos.compartido  FROM recursos  "
				. " WHERE idrecurso = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Recursos") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('recursos', array('idrecurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Recursos") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('recursos', array($propiedad => $valor), array('idrecurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Recursos") . ": " . $e->getMessage());
		}
	}
}
