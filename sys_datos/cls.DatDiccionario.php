<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		13-02-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatDiccionario extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(id) FROM diccionario";

			$cond = array();

			if (!empty($filtros["id"])) {
				$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if (!empty($filtros["palabra"])) {
				$cond[] = "palabra LIKE '%" . $filtros["palabra"] . "'";
			}
			if (!empty($filtros["definicion"])) {
				$cond[] = "definicion = " . $this->oBD->escapar($filtros["definicion"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT diccionario.id, diccionario.palabra, diccionario.tipo, diccionario.definicion FROM diccionario";

			$cond = array();

			if (!empty($filtros["id"])) {
				$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if (!empty($filtros["palabra"])) {
				$cond[] = "palabra LIKE '" . $filtros["palabra"] . "%'";
			}
			if (!empty($filtros["definicion"])) {
				$cond[] = "definicion = " . $this->oBD->escapar($filtros["definicion"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY palabra ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  diccionario.id, diccionario.palabra, diccionario.tipo, diccionario.definicion  FROM diccionario  ";

			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("List all") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}

	public function insertar($palabra, $definicion)
	{
		try {

			$this->iniciarTransaccion('dat_diccionario_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM diccionario");
			++$id;

			$estados = array(
				'id' => $id, 'palabra' => $palabra, 'definicion' => $definicion
			);

			$this->oBD->insert('diccionario', $estados);
			$this->terminarTransaccion('dat_diccionario_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_diccionario_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $palabra, $definicion)
	{
		try {
			$this->iniciarTransaccion('dat_diccionario_update');
			$estados = array(
				'palabra' => $palabra, 'definicion' => $definicion
			);

			$this->oBD->update('diccionario ', $estados, array('id' => $id));
			$this->terminarTransaccion('dat_diccionario_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  diccionario.id, diccionario.palabra, diccionario.tipo, diccionario.definicion  FROM diccionario  "
				. " WHERE id = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('diccionario', array('id' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('diccionario', array($propiedad => $valor), array('id' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Diccionario") . ": " . $e->getMessage());
		}
	}
}
