<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-03-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatValoracion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Valoracion").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT v.puntuacion, v.comentario, CONCAT(p.nombre, ' ', p.ape_paterno, ' ', p.ape_materno) as nombre_alumno FROM valoracion v INNER JOIN personal p ON (p.idpersona = v.idalumno)";			
			
			$cond = array();		
			
			if(isset($filtros["idvaloracion"])) {
					$cond[] = "v.idvaloracion = " . $this->oBD->escapar($filtros["idvaloracion"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "v.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "v.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcomplementario"])) {
					$cond[] = "v.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "v.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
				if (is_array($filtros["idalumno"])) {
					$cond[] = "v.idalumno IN ('" . implode("','", $filtros["idalumno"]) . "')";
				} else
					$cond[] = "v.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}

			if(isset($filtros["puntuacion"])) {
					$cond[] = "v.puntuacion = " . $this->oBD->escapar($filtros["puntuacion"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "v.comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}			
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Valoracion").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($estados)
	{
		try {
			
			//$this->iniciarTransaccion('dat_valoracion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idvaloracion) FROM valoracion");
			++$id;
			$estados['idvaloracion'] = $id;
			$this->oBD->insert('valoracion', $estados);			
			//$this->terminarTransaccion('dat_valoracion_insert');			
			return $id;

		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_valoracion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Valoracion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$idrupoaula,$idcomplementario,$idalumno,$puntuacion,$comentario,$idgrupoauladetalle=null)
	{
		try {
			$this->iniciarTransaccion('dat_valoracion_update');
			$estados = array('idcurso'=>$idcurso
							,'idgrupoaula'=>$idrupoaula
							,'idcomplementario'=>$idcomplementario
							,'idalumno'=>$idalumno
							,'puntuacion'=>$puntuacion
							,'comentario'=>$comentario
							);
			if(!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"]=$idgrupoauladetalle;
			
			$this->oBD->update('valoracion ', $estados, array('idvaloracion' => $id));
		    $this->terminarTransaccion('dat_valoracion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Valoracion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idcurso,idgrupoaula,idcomplementario,idalumno,puntuacion,comentario,idgrupoauladetalle  FROM valoracion  "
					. " WHERE idvaloracion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Valoracion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('valoracion', array('idvaloracion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Valoracion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('valoracion', array($propiedad => $valor), array('idvaloracion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Valoracion").": " . $e->getMessage());
		}
	}
   
		
}