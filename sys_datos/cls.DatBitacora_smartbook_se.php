<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-11-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatBitacora_smartbook_se extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM bitacora_smartbook_se BS JOIN bitacora_alumno_smartbook_se BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";

			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT BS.idbitacora,BS.idbitacora_alum_smartbook,BS.idcurso,BS.idcursodetalle,BS.idsesionB,BS.idusuario,BS.pestania,BS.total_pestanias,BS.fechahora,BS.progreso,BS.otros_datos,BS.idcurso_sc,BAS.idbitacora_smartbook,BAS.idcurso,BAS.idsesion,BAS.idusuario,BAS.estado,BAS.regfecha,BAS.idcursodetalle_sc,BAS.idcc,BAS.idproyecto,BS.idgrupoauladetalle FROM bitacora_smartbook_se BS RIGHT JOIN bitacora_alumno_smartbook_se BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";

			$cond = array();


			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcurso_sc"])) {
				$cond[] = "BAS.idcurso_sc = " . $this->oBD->escapar($filtros["idcurso_sc"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "BAS.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idcc"])) {
				$cond[] = "BAS.idcc = " . $this->oBD->escapar($filtros["idcc"]);
			}
			if (isset($filtros["idcursodetalle_sc"])) {
				$cond[] = "BAS.idcursodetalle_sc = " . $this->oBD->escapar($filtros["idcursodetalle_sc"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY fechahora DESC";
			//echo $sql .'<br>';
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function getProgresoPromedio($filtros = null)
	{
		try {
			$sql = "SELECT AVG(progreso) FROM bitacora_smartbook_se";

			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function getSumaProgresos($filtros = null)
	{
		try {
			$sql = "SELECT SUM(progreso) FROM bitacora_smartbook_se BS JOIN bitacora_alumno_smartbook_se BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";

			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idusuario"])) {
				//$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
				$cond[] = "BAS.idusuario=" . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql.'<br>';
			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function getSumaProgresos2($filtros = null)
	{
		try {
			$sql = "SELECT SUM(progreso) as sumprogreso ,total_pestanias FROM bitacora_smartbook_se BS JOIN bitacora_alumno_smartbook_se BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";

			$cond = array();

			if (isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idusuario"])) {
				//$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
				$cond[] = "BAS.idusuario=" . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "BS.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if (isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if (isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if (isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if (isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " GROUP BY total_pestanias ";
			//echo $sql.'<br>';
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}


	public function insertar($idcurso, $idcursodetalle, $idsesionB, $idusuario, $idbitacora_alum_smartbook, $pestania, $total_pestanias, $fechahora, $progreso, $otros_datos, $idcurso_sc, $idproyecto, $idcc = 0, $idcursodetalle_sc, $idgrupoauladetalle = null)
	{
		try {

			//$this->iniciarTransaccion('dat_bitacora_smartbook_insert');
			$id = $this->oBD->consultarEscalarSQL("SELECT idbitacora FROM bitacora_smartbook_se ORDER BY idbitacora DESC LIMIT 0,1");
			++$id;

			$estados = array(
				'idbitacora' => $id, 'idcurso' => $idcurso, 'idcursodetalle' => $idcursodetalle, 'idusuario' => $idusuario, 'idbitacora_alum_smartbook' => $idbitacora_alum_smartbook, 'pestania' => $pestania, 'total_pestanias' => $total_pestanias, 'fechahora' => $fechahora, 'progreso' => $progreso, 'idcurso_sc' => $idcurso_sc, 'idproyecto' => $idproyecto, 'idcc' => $idcc, 'idcursodetalle_sc' => $idcursodetalle_sc
			);
			if (!empty($otros_datos)) {
				$estados["otros_datos"] = $otros_datos;
			}
			if (!empty($idsesionB)) {
				$estados['idsesionB'] = $idsesionB;
			}
			if (!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"] = $idgrupoauladetalle;
			$this->oBD->insert('bitacora_smartbook_se', $estados);
			//$this->terminarTransaccion('dat_bitacora_smartbook_insert');			
			return $id;
		} catch (Exception $e) {
			//$this->cancelarTransaccion('dat_bitacora_smartbook_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("bitacora_smartbook_se") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso, $idcursodetalle, $idsesionB, $idusuario, $idbitacora_alum_smartbook, $pestania, $total_pestanias, $fechahora, $progreso, $otros_datos, $idcurso_sc, $idproyecto, $idcc = 0, $idcursodetalle_sc, $idgrupoauladetalle = null)
	{
		try {
			if (empty($pestania)) return $id;
			//$this->iniciarTransaccion('dat_bitacora_smartbook_update');
			$estados = array(
				'idcurso' => $idcurso, 'idcursodetalle' => $idcursodetalle, 'idusuario' => $idusuario, 'idbitacora_alum_smartbook' => $idbitacora_alum_smartbook, 'pestania' => $pestania, 'total_pestanias' => $total_pestanias, 'fechahora' => $fechahora, 'progreso' => $progreso, 'idcurso_sc' => $idcurso_sc, 'idproyecto' => $idproyecto, 'idcc' => $idcc, 'idcursodetalle_sc' => $idcursodetalle_sc
			);
			if (!empty($otros_datos)) {
				$estados["otros_datos"] = $otros_datos;
			}
			if (!empty($idsesionB)) {
				$estados['idsesionB'] = $idsesionB;
			}
			if (!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"] = $idgrupoauladetalle;

			$this->oBD->update('bitacora_smartbook_se', $estados, array('idbitacora' => $id));
			// $this->terminarTransaccion('dat_bitacora_smartbook_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  bitacora_smartbook_se.idbitacora, bitacora_smartbook_se.idbitacora_alum_smartbook, bitacora_smartbook_se.idcurso, bitacora_smartbook_se.idcursodetalle, bitacora_smartbook_se.idsesionB, bitacora_smartbook_se.idusuario, bitacora_smartbook_se.pestania, bitacora_smartbook_se.total_pestanias, bitacora_smartbook_se.fechahora, bitacora_smartbook_se.progreso, bitacora_smartbook_se.otros_datos, bitacora_smartbook_se.fechamodificacion, bitacora_smartbook_se.previamodificacion, bitacora_smartbook_se.idcurso_sc, bitacora_smartbook_se.idcc, bitacora_smartbook_se.idcursodetalle_sc, bitacora_smartbook_se.idproyecto,bitacora_smartbook_se.idgrupoauladetalle  FROM bitacora_smartbook_se"
				. " WHERE idbitacora = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bitacora_smartbook_se', array('idbitacora' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('bitacora_smartbook_se', array($propiedad => $valor), array('idbitacora' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Bitacora_smartbook") . ": " . $e->getMessage());
		}
	}
}
