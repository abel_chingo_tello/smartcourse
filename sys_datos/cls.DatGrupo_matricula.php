<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-04-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatGrupo_matricula extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(idgrupo_matricula) FROM grupo_matricula";
			
			$cond = array();		
			
			if(!empty($filtros["idgrupo_matricula"])) {
					$cond[] = "idgrupo_matricula = " . $this->oBD->escapar($filtros["idgrupo_matricula"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["idgrupo"])) {
					$cond[] = "idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(!empty($filtros["fechamatricula"])) {
					$cond[] = "fechamatricula = " . $this->oBD->escapar($filtros["fechamatricula"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(!empty($filtros["subgrupos"])) {
					$cond[] = "subgrupos = " . $this->oBD->escapar($filtros["subgrupos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT tb1.idgrupo_matricula, tb1.idalumno, tb1.idgrupo, tb1.fechamatricula, tb1.estado, tb1.regusuario, tb1.regfecha, tb1.subgrupos, tb2.nombre, tb2.ape_paterno, tb2.ape_materno, tb2.email FROM grupo_matricula tb1 LEFT JOIN alumno tb2 ON tb1.idalumno=tb2.dni";
			
			$cond = array();		
			
			if(!empty($filtros["idgrupo_matricula"])) {
					$cond[] = "tb1.idgrupo_matricula = " . $this->oBD->escapar($filtros["idgrupo_matricula"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "tb1.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["idgrupo"])) {
					$cond[] = "tb1.idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(!empty($filtros["fechamatricula"])) {
					$cond[] = "tb1.fechamatricula = " . $this->oBD->escapar($filtros["fechamatricula"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "tb1.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "tb1.regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["regfecha"])) {
					$cond[] = "tb1.regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(!empty($filtros["subgrupos"])) {
					$cond[] = "tb1.subgrupos = " . $this->oBD->escapar($filtros["subgrupos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}
	public function buscarAlumGrup($filtros=null)
	{
		try {
			$sql = "SELECT 
			tb1.idgrupo_matricula, tb1.idalumno, tb1.idgrupo, tb1.fechamatricula, tb1.estado, tb1.regusuario, tb1.regfecha, tb1.subgrupos,
			tb2.dni, tb2.ape_paterno, tb2.ape_materno, tb2.nombre, tb2.fechanac, tb2.sexo, tb2.estado_civil, tb2.ubigeo, tb2.urbanizacion, tb2.direccion, tb2.telefono, tb2.celular, tb2.email, tb2.idugel, tb2.regusuario, tb2.regfecha, tb2.usuario, tb2.clave, tb2.token, tb2.foto, tb2.estado, tb2.situacion ,
			tb3.idgrupo, tb3.iddocente, tb3.idlocal, tb3.idambiente, tb3.idasistente, tb3.fechainicio, tb3.fechafin, tb3.dias, tb3.horas, tb3.horainicio, tb3.horafin, tb3.valor, tb3.valor_asi, tb3.regusuario, tb3.regfecha, tb3.matriculados, tb3.nivel, tb3.codigo  
			FROM grupo_matricula tb1 LEFT JOIN alumno tb2 ON tb1.idalumno=tb2.dni  RIGHT JOIN grupos tb3 ON tb1.idgrupo=tb3.idgrupo";			
			
			$cond = array();
			if(!empty($filtros["idgrupo_matricula"])) {
					$cond[] = "tb1.dgrupo_matricula = " . $this->oBD->escapar($filtros["idgrupo_matricula"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "tb1.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["idgrupo"])) {
					$cond[] = "tb1.idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(!empty($filtros["fechamatricula"])) {
					$cond[] = "tb1.fechamatricula = " . $this->oBD->escapar($filtros["fechamatricula"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "tb1.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "tb1.regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["regfecha"])) {
					$cond[] = "tb1.regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(!empty($filtros["subgrupos"])) {
					$cond[] = "tb1.subgrupos = " . $this->oBD->escapar($filtros["subgrupos"]);
			}		
			/****** .::otras tabalas::. ******/	
			if(!empty($filtros["dni"])) {
					$cond[] = "tb2.dni = " . $this->oBD->escapar($filtros["dni"]);
			}			
			if(!empty($filtros["idambiente"])) {
					$cond[] = "tb3.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}				
			if(!empty($filtros["idlocal"])) {
					$cond[] = "tb3.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT tb1.idgrupo_matricula, tb1.idalumno, tb1.idgrupo, tb1.fechamatricula, tb1.estado, tb1.regusuario, tb1.regfecha, tb1.subgrupos,
			tb2.dni, tb2.ape_paterno, tb2.ape_materno, tb2.nombre, tb2.fechanac, tb2.sexo, tb2.estado_civil, tb2.ubigeo, tb2.urbanizacion, tb2.direccion, tb2.telefono, tb2.celular, tb2.email, tb2.idugel, tb2.regusuario, tb2.regfecha, tb2.usuario, tb2.clave, tb2.token, tb2.foto, tb2.estado, tb2.situacion ,
			tb3.idgrupo, tb3.iddocente, tb3.idlocal, tb3.idambiente, tb3.idasistente, tb3.fechainicio, tb3.fechafin, tb3.dias, tb3.horas, tb3.horainicio, tb3.horafin, tb3.valor, tb3.valor_asi, tb3.regusuario, tb3.regfecha, tb3.matriculados, tb3.nivel, tb3.codigo    FROM grupo_matricula tb1 LEFT JOIN alumno tb2 ON tb1.idalumno=tb2.dni  LEFT JOIN grupos tb3 ON tb1.idgrupo=tb3.idgrupo  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}
	
	public function insertar($idalumno,$idgrupo,$fechamatricula,$estado,$regusuario,$regfecha,$subgrupos)
	{
		try {
			
			$this->iniciarTransaccion('dat_grupo_matricula_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupo_matricula) FROM grupo_matricula");
			++$id;
			
			$estados = array('idgrupo_matricula' => $id							
							,'idalumno'=>$idalumno
							,'idgrupo'=>$idgrupo
							,'fechamatricula'=>$fechamatricula
							,'estado'=>$estado
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'subgrupos'=>$subgrupos							
							);
			
			$this->oBD->insert('grupo_matricula', $estados);			
			$this->terminarTransaccion('dat_grupo_matricula_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_grupo_matricula_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idalumno,$idgrupo,$fechamatricula,$estado,$regusuario,$regfecha,$subgrupos)
	{
		try {
			$this->iniciarTransaccion('dat_grupo_matricula_update');
			$estados = array('idalumno'=>$idalumno
							,'idgrupo'=>$idgrupo
							,'fechamatricula'=>$fechamatricula
							,'estado'=>$estado
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'subgrupos'=>$subgrupos								
							);
			
			$this->oBD->update('grupo_matricula ', $estados, array('idgrupo_matricula' => $id));
		    $this->terminarTransaccion('dat_grupo_matricula_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.idgrupo_matricula, tb1.idalumno, tb1.idgrupo, tb1.fechamatricula, tb1.estado, tb1.regusuario, tb1.regfecha, tb1.subgrupos,
			tb2.dni, tb2.ape_paterno, tb2.ape_materno, tb2.nombre, tb2.fechanac, tb2.sexo, tb2.estado_civil, tb2.ubigeo, tb2.urbanizacion, tb2.direccion, tb2.telefono, tb2.celular, tb2.email, tb2.idugel, tb2.regusuario, tb2.regfecha, tb2.usuario, tb2.clave, tb2.token, tb2.foto, tb2.estado, tb2.situacion ,
			tb3.idgrupo, tb3.iddocente, tb3.idlocal, tb3.idambiente, tb3.idasistente, tb3.fechainicio, tb3.fechafin, tb3.dias, tb3.horas, tb3.horainicio, tb3.horafin, tb3.valor, tb3.valor_asi, tb3.regusuario, tb3.regfecha, tb3.matriculados, tb3.nivel, tb3.codigo    FROM grupo_matricula tb1 LEFT JOIN alumno tb2 ON tb1.idalumno=tb2.dni  LEFT JOIN grupos tb3 ON tb1.idgrupo=tb3.idgrupo  "
					. " WHERE tb1.idgrupo_matricula = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('grupo_matricula', array('idgrupo_matricula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('grupo_matricula', array($propiedad => $valor), array('idgrupo_matricula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Grupo_matricula").": " . $e->getMessage());
		}
	}
   
	

	/****************************** FK alumno ***************************/
	/*public function listarno(){
		try {
			$sql = "SELECT * FROM alumno";
			
			$res = $this->oMySQL->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("get")." ".JrTexto::_("").": " . $e->getMessage());
		}
	}

	public function listarXidalumno($dni){
		try {
			$sql = "SELECT * FROM grupo_matricula WHERE idalumno=".$dni;
			
			$res = $this->oMySQL->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("get")." ".JrTexto::_("").": " . $e->getMessage());
		}
	}*/
	

	/****************************** FK grupos ***************************/
	/*public function listaros(){
		try {
			$sql = "SELECT * FROM grupos";
			
			$res = $this->oMySQL->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("get")." ".JrTexto::_("").": " . $e->getMessage());
		}
	}

	public function listarXidgrupo($idgrupo){
		try {
			$sql = "SELECT * FROM grupo_matricula WHERE idgrupo=".$idgrupo;
			
			$res = $this->oMySQL->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("get")." ".JrTexto::_("").": " . $e->getMessage());
		}
	}*/
		
}