<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-01-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatSinlogin_persona extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Sinlogin_persona").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT  `idpersona`,`nombres`,`telefono`,correo,idproyecto,ciudad,pais,compania FROM sinlogin_persona";
			$cond = array();
			
			if(isset($filtros["idpersona"])) {
				$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}

			if(isset($filtros["ciudad"])) {
					$cond[] = "ciudad = " . $this->oBD->escapar($filtros["ciudad"]);
			}
			if(isset($filtros["pais"])) {
					$cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);
			}
			if(isset($filtros["nombres"])) {
					$cond[] = "nombres = " . $this->oBD->escapar($filtros["nombres"]);
			}
			if(isset($filtros["compania"])) {
					$cond[] = "compania = " . $this->oBD->escapar($filtros["compania"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(isset($filtros["texto"])) {
					$cond[] = "nombres " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Sinlogin_persona").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombres,$telefono,$correo,$idproyecto,$pais,$compania,$ciudad)
	{
		try {
			
			//$this->iniciarTransaccion('dat_sinlogin_persona_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM sinlogin_persona");
			++$id;			
			$estados = array('nombres'=>$nombres
							,'telefono'=>$telefono
							,'correo'=>$correo	
							,'idproyecto'=>$idproyecto							
							,'pais'=>!empty($pais)?$pais:'null'
							,'compania'=>!empty($compania)?$compania:'null'
							,'ciudad'=>!empty($ciudad)?$ciudad:'null'
							);
			
			return $this->oBD->insertAI('sinlogin_persona', $estados,true);			
			//$this->terminarTransaccion('dat_sinlogin_persona_insert');
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_sinlogin_persona_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Sinlogin_persona").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombres,$telefono,$correo,$idproyecto,$pais,$compania,$ciudad)
	{
		try {
			//$this->iniciarTransaccion('dat_sinlogin_persona_update');
			$estados = array('nombres'=>$nombres
							,'telefono'=>$telefono
							,'correo'=>$correo		
							,'idproyecto'=>$idproyecto	
							,'pais'=>!empty($pais)?$pais:'null'
							,'compania'=>!empty($compania)?$compania:'null'
							,'ciudad'=>!empty($ciudad)?$ciudad:'null'						
							);
			
			$this->oBD->update('sinlogin_persona ', $estados, array('idpersona' => $id));
		   // $this->terminarTransaccion('dat_sinlogin_persona_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sinlogin_persona").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  `idpersona`,`nombres`,`telefono`,correo,idproyecto  FROM sinlogin_persona  "
					. " WHERE idpersona = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Sinlogin_persona").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('sinlogin_persona', array('idpersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Sinlogin_persona").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('sinlogin_persona', array($propiedad => $valor), array('idpersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sinlogin_persona").": " . $e->getMessage());
		}
	}   
		
}