<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-04-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatRubrica_nivel extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Rubrica_nivel") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT (@i := @i + 1) as orden, rubrica_nivel.idrubrica_nivel, rubrica_nivel.cualitativo, rubrica_nivel.cuantitativo, rubrica_nivel.idrubrica FROM rubrica_nivel cross join (select @i := 0) rubrica_nivel";

			$cond = array();


			if (isset($filtros["idrubrica_nivel"])) {
				$cond[] = "idrubrica_nivel = " . $this->oBD->escapar($filtros["idrubrica_nivel"]);
			}
			if (isset($filtros["cualitativo"])) {
				$cond[] = "cualitativo = " . $this->oBD->escapar($filtros["cualitativo"]);
			}
			if (isset($filtros["cuantitativo"])) {
				$cond[] = "cuantitativo = " . $this->oBD->escapar($filtros["cuantitativo"]);
			}
			if (isset($filtros["idrubrica"])) {
				$cond[] = "idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Rubrica_nivel") . ": " . $e->getMessage());
		}
	}


	public function insertar($cualitativo, $cuantitativo, $idrubrica)
	{
		try {

			$this->iniciarTransaccion('dat_rubrica_nivel_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrubrica_nivel) FROM rubrica_nivel");
			++$id;

			$estados = array(
				'idrubrica_nivel' => $id, 'cualitativo' => $cualitativo, 'cuantitativo' => $cuantitativo, 'idrubrica' => $idrubrica
			);

			$this->oBD->insert('rubrica_nivel', $estados);
			$this->terminarTransaccion('dat_rubrica_nivel_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_rubrica_nivel_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Rubrica_nivel") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $cualitativo, $cuantitativo, $idrubrica)
	{
		try {
			$this->iniciarTransaccion('dat_rubrica_nivel_update');
			$estados = array(
				'cualitativo' => $cualitativo, 'cuantitativo' => $cuantitativo, 'idrubrica' => $idrubrica
			);

			$this->oBD->update('rubrica_nivel ', $estados, array('idrubrica_nivel' => $id));
			$this->terminarTransaccion('dat_rubrica_nivel_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_nivel") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idrubrica_nivel,cualitativo,cuantitativo,idrubrica  FROM rubrica_nivel  "
				. " WHERE idrubrica_nivel = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Rubrica_nivel") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rubrica_nivel', array('idrubrica_nivel' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Rubrica_nivel") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('rubrica_nivel', array($propiedad => $valor), array('idrubrica_nivel' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_nivel") . ": " . $e->getMessage());
		}
	}
}
