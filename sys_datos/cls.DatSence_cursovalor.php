<?php 
class DatSence_cursovalor extends DatBase {
    public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Sence_cursovalor").": " . $e->getMessage());
		}
    }
    /**
     * Funcion generica para buscar en la tabla especifica sin datos relacionados
     * Por favor no modificar tanto la logica de la función y si es necesaria, delimitar consulta mediante un parametro especifico
     * @param Array Son los filtros del a consulta donde se condiciona la busqueda
     * @return Array El resultado de la consulta
     */
    public function buscar($filtros = null){
        try{
            $sql = "SELECT scv.id,scv.idusuariosence,scv.CodSence,scv.CodigoCurso,scv.IdSesionAlumno,scv.IdSesionSence,scv.RunAlumno,scv.FechaHora,scv.ZonaHoraria,scv.LineaCapacitacion,scv.GlosaError,scv.fecha_creacion,scv.fecha_actualizacion FROM sence_cursovalor scv";
            
            $cond = array();
            
            if (isset($filtros["id"])) {
                if(is_array($filtros["id"])){
                    $cond[] = "scv.id IN (" . implode(",",$filtros["id"]).")";
                }else{
                    $cond[] = "scv.id = " . $this->oBD->escapar($filtros["id"]);
                }
            }
            if (isset($filtros["idusuariosence"])){ $cond[] = "scv.idusuariosence = " . $this->oBD->escapar($filtros["idusuariosence"]); }
            if (isset($filtros["CodSence"])){ $cond[] = "scv.CodSence = " . $this->oBD->escapar($filtros["CodSence"]); }
            if (isset($filtros["CodigoCurso"])){ $cond[] = "scv.CodigoCurso = " . $this->oBD->escapar($filtros["CodigoCurso"]); }
            if (isset($filtros["IdSesionAlumno"])){ $cond[] = "scv.IdSesionAlumno = " . $this->oBD->escapar($filtros["IdSesionAlumno"]); }
            if (isset($filtros["IdSesionSence"])){ $cond[] = "scv.IdSesionSence = " . $this->oBD->escapar($filtros["IdSesionSence"]); }
            if (isset($filtros["RunAlumno"])){ $cond[] = "scv.RunAlumno = " . $this->oBD->escapar($filtros["RunAlumno"]); }
            if (isset($filtros["FechaHora"])){ $cond[] = "scv.FechaHora = " . $this->oBD->escapar($filtros["FechaHora"]); }
            if (isset($filtros["ZonaHoraria"])){ $cond[] = "scv.ZonaHoraria = " . $this->oBD->escapar($filtros["ZonaHoraria"]); }
            if (isset($filtros["LineaCapacitacion"])){ $cond[] = "scv.LineaCapacitacion = " . $this->oBD->escapar($filtros["LineaCapacitacion"]); }
            if (isset($filtros["GlosaError"])){ $cond[] = "scv.GlosaError = " . $this->oBD->escapar($filtros["GlosaError"]); }
            if (isset($filtros["fecha_creacion"])){ $cond[] = "scv.fecha_creacion = " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["desdeFecha_creacion"])){ $cond[] = "scv.fecha_creacion >= " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["hastaFecha_creacion"])){ $cond[] = "scv.fecha_creacion <= " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["fecha_actualizacion"])){ $cond[] = "scv.fecha_actualizacion = " . $this->oBD->escapar($filtros["fecha_actualizacion"]); } 
            if (isset($filtros["desdeFecha_actualizacion"])){ $cond[] = "scv.fecha_actualizacion >= " . $this->oBD->escapar($filtros["fecha_actualizacion"]); } 
            if (isset($filtros["hastaFecha_actualizacion"])){ $cond[] = "scv.fecha_actualizacion <= " . $this->oBD->escapar($filtros["fecha_actualizacion"]); } 
            
            if (!empty($cond)) { $sql .= " WHERE " . implode(' AND ', $cond); }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
			throw new Exception("ERROR\n" . JrTexto::_("buscar") . " " . JrTexto::_("Sence_usuario") . ": " . $e->getMessage());
        }
    }
    public function buscarcompleto($filtros = null){}

    /**
     * Funcion generica para insertar registro en la tabla, se puede registrar 1 a N cantidad de registro ya que actua de manera masiva
     * La función esta destinada a insertar como tabla unica, si se requiere hacer un insertado mas complejo, por favor hacer otra función
     * ya que esta función es generica y se puede reutilizar en diferentes modulos del sistema
     * @param Array los registro a procesar
     * @return Integer Retorna el ultimo id insertado
     */
    public function insert($data){
        try{
            $id = false;
            $this->iniciarTransaccion('dat_sence_cursovalor_insert');
            if(empty($data)){ throw new Exception("Data esta vacio"); }
            foreach($data as $row){
                $id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM sence_cursovalor");
                ++$id;
                $row['id']=$id;
				$this->oBD->insert('sence_cursovalor', $row);
            }
			$this->terminarTransaccion('dat_sence_cursovalor_insert');            
            return $id;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_sence_cursovalor_insert');
            throw new Exception("ERROR\n" . JrTexto::_("insert") . " " . JrTexto::_("sence_cursovalor") . ": " . $e->getMessage());
        }
    }
    /**
     * Funcion generica para actualizar registro en la tabla, se puede actualizar un unico registro como varios, ya que actua de manera masiva el proceso.
     * La funcion esta destinada solo a actualizar registros sin logica especial, si se requiere un proceso mas complejo, por favor crea otra funcion preferiblemente
     * Este proceso es reutilizado por varios modulos del sistema, y un cambio drastico puede causar fallas
     * @param Array los registro a procesar
     * @param Boolean verifica si es estricto las validaciones, si esta estricto y no posee el identificador del registro, lanzara un throw Exception
     * @return Boolean Resultado en TRUE si se realizo el proceso completamente, FALSE algo ocurrio
     */
    public function update($data,$esEstricto = false){
        try{
            $this->iniciarTransaccion('dat_sence_cursovalor_update');
            if(empty($data)){ throw new Exception("Data esta vacio"); }
            foreach($data as $row){
                if(empty($row['id'])){ if($esEstricto === false){ continue; }else{ throw new Exception("Un registro no tiene definido el id para actualizar"); }  }
                $id = $row['id'];
                unset($row['id']);
                $this->oBD->update('sence_cursovalor', $row, array('id' => $id));
            }
			$this->terminarTransaccion('dat_sence_cursovalor_update');            
            return true;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_sence_cursovalor_update');
            throw new Exception("ERROR\n" . JrTexto::_("update") . " " . JrTexto::_("sence_cursovalor") . ": " . $e->getMessage());
        }
    }
    public function delete($data){}
} 
?>