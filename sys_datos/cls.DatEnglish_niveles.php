<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatEnglish_niveles extends DatBase
{
	public function __construct($BD='')
	{
		try{			
			if(!empty($BD)) $BD=NAME_BDSEAdolecentes;
			parent::conectar($BD);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT `idnivel`,`nombre`,`tipo`,`idpadre`,`idpersonal`,`estado`,`orden`,`imagen`,`descripcion` FROM niveles";		
			$cond = array();		
			
			if(isset($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre " . $this->oBD->like($filtros["nombre"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}

						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if(!empty($filtros['sysordenar']))
				$sql.=' ORDER BY '.$filtros['sysordenar'];
			else
				$sql.=' ORDER BY orden asc ,idnivel ASC';
			
			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql;
			if(isset($filtros["BD"])) {
				parent::conectar($filtros["BD"]);
			}
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}

}