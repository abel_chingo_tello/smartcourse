<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-04-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
ini_set('memory_limit', '-1');
class DatHistorial_sesion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM historial_sesion";

			$cond = array();

			if (!empty($filtros["idhistorialsesion"])) {
				$cond[] = "idhistorialsesion = " . $this->oBD->escapar($filtros["idhistorialsesion"]);
			}
			if (!empty($filtros["tipousuario"])) {
				$cond[] = "tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}
			if (!empty($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (!empty($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($filtros["lugar"])) {
				$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if (!empty($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (!empty($filtros["idcc"])) {
				$cond[] = "hs.idcc = " . $this->oBD->escapar($filtros["idcc"]);
			}
			if (!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "hs.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (!empty($filtros["fechaentrada"])) {
				$cond[] = "fechaentrada = " . $this->oBD->escapar($filtros["fechaentrada"]);
			}
			if (!empty($filtros["fechasalida"])) {
				$cond[] = "fechasalida = " . $this->oBD->escapar($filtros["fechasalida"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT historial_sesion.idhistorialsesion, historial_sesion.tipousuario, historial_sesion.idusuario, historial_sesion.lugar, historial_sesion.idcurso, historial_sesion.fechaentrada, historial_sesion.fechasalida, historial_sesion.idproyecto, historial_sesion.ip, historial_sesion.navegador, historial_sesion.idcc,idgrupoauladetalle FROM historial_sesion";

			$cond = array();
			$opciones = array();

			if (!empty($filtros["idhistorialsesion"])) {
				$cond[] = "idhistorialsesion = " . $this->oBD->escapar($filtros["idhistorialsesion"]);
			}
			if (!empty($filtros["tipousuario"])) {
				$cond[] = "tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}
			if (!empty($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (!empty($filtros["lugar"])) {
				$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if (!empty($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (!empty($filtros["idcc"])) {
				$cond[] = "hs.idcc = " . $this->oBD->escapar($filtros["idcc"]);
			}
			if (!empty($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($filtros["fechaentrada"])) {
				$cond[] = "fechaentrada = " . $this->oBD->escapar($filtros["fechaentrada"]);
			}
			if (!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (!empty($filtros["fechasalida"])) {
				if (!empty($filtros["fechasalida"]['isnull'])) {
					$cond[] = "fechasalida IS NULL";
				} else if (!empty($filtros["fechasalida"]['isnotnull'])) {
					$cond[] = "fechasalida IS NOT NULL";
				} else {
					$cond[] = "fechasalida = " . $this->oBD->escapar($filtros["fechasalida"]);
				}
			}
			if (!empty($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (!empty($filtros['OrderByDesc'])) {
				$opciones[] = "ORDER BY {$filtros['OrderByDesc']} DESC";
			}
			if (!empty($filtros['Limit'])) {
				$opciones[] = "LIMIT {$filtros['Limit']}";
			}

			if (!empty($opciones)) {
				$sql .= " " . implode(' ', $opciones);
			}

			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}

	public function buscartiempos($filtros)
	{
		try {
			$sql = "SELECT hs.tipousuario,hs.idusuario,hs.lugar,hs.idcurso,hs.idproyecto,TIMESTAMPDIFF(SECOND,fechaentrada,fechasalida) as tiempoensegundos,hs.idcurso, hs.idcc ,fechaentrada,idgrupoauldetalle FROM `historial_sesion` hs";

			if (isset($filtros["sql1"])) {
				$sql = "SELECT idusuario,CONCAT(ape_paterno,' ',ape_materno,', ',nombre) as stralumno ,lugar,idcurso,idproyecto,idcc, sum(TIMESTAMPDIFF(SECOND,fechaentrada,fechasalida)) as tiempoensegundos,idgrupoauladetalle FROM `historial_sesion` hs INNER JOIN personal on idusuario=idpersona ";
				$filtros["groupby"] = ' idusuario, lugar,idcurso,idcc ';
				$filtros["orderby"] = ' stralumno ASC ';
			}

			$cond = array();
			$opciones = array();
			if (isset($filtros["idusuario"])) {
				if (is_array($filtros["idusuario"])) {
					$cond[] = "hs.idusuario IN ('" . implode("','", $filtros["idusuario"]) . "')";
				} else
					$cond[] = "hs.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			//$cond[] = "tiempoensegundos > 0 ";
			if (isset($filtros["idcurso"])) {
				if (is_array($filtros["idcurso"])) {
					$cond[] = "hs.idcurso IN ('" . implode("','", $filtros["idcurso"]) . "')";
				} else
					$cond[] = "hs.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (!empty($filtros["idcc"])) {
				$cond[] = "hs.idcc = " . $this->oBD->escapar($filtros["idcc"]);
			}
			if (!empty($filtros["idhistorialsesion"])) {
				$cond[] = "idhistorialsesion = " . $this->oBD->escapar($filtros["idhistorialsesion"]);
			}
			if (!empty($filtros["tipousuario"])) {
				$cond[] = "tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}
			if (!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (!empty($filtros["idproyecto"])) {
				$cond[] = "hs.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($filtros["lugar"])) {
				$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (!empty($filtros["groupby"])) $sql .= " GROUP BY " . $filtros["groupby"];
			if (!empty($filtros["orderby"])) $sql .= "  ORDER BY " . $filtros["orderby"];
			else $sql .= " ORDER BY fechaentrada DESC, idhistorialsesion DESC ";
			//echo $sql; 
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Historial_sesion tiempo") . ": " . $e->getMessage());
		}
	}

	public function ultimavisita($filtros)
	{
		try {
			$sql = "SELECT hs.idusuario,hs.idcurso,hs.idproyecto,MAX(hs.fechaentrada) AS ultimavisita, hs.idcc, idgrupoauladetalle FROM historial_sesion hs";
			$cond = array();
			$opciones = array();

			if (!empty($filtros["idhistorialsesion"])) {
				$cond[] = "hs.idhistorialsesion = " . $this->oBD->escapar($filtros["idhistorialsesion"]);
			}
			if (!empty($filtros["tipousuario"])) {
				$cond[] = "hs.tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}
			if (!empty($filtros["idusuario"])) {
				$cond[] = "hs.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (!empty($filtros["lugar"])) {
				$cond[] = "hs.lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if (!empty($filtros["idcurso"])) {
				$cond[] = "hs.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "hs.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (!empty($filtros["idcc"])) {
				$cond[] = "hs.idcc = " . $this->oBD->escapar($filtros["idcc"]);
			}
			if (!empty($filtros["idproyecto"])) {
				$cond[] = "hs.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (!empty($filtros['OrderByDesc'])) {
				$opciones[] = "ORDER BY {$filtros['OrderByDesc']} DESC";
			}
			if (!empty($opciones)) {
				$sql .= " " . implode(' ', $opciones);
			}

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  historial_sesion.idhistorialsesion, historial_sesion.tipousuario, historial_sesion.idusuario, historial_sesion.lugar, historial_sesion.idcurso, historial_sesion.fechaentrada, historial_sesion.fechasalida, historial_sesion.idproyecto, historial_sesion.ip, historial_sesion.navegador, historial_sesion.idcc,idgrupoauladetalle  FROM historial_sesion  ";

			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("List all") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}

	public function insertar($tipousuario, $idusuario, $lugar, $idcurso, $fechaentrada, $fechasalida, $idproyecto, $idcc = 0, $idgrupoauladetalle = null)
	{
		try {
			//$this->iniciarTransaccion('dat_historial_sesion_insert');
			$ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);
			$navegador =	@$_SERVER['HTTP_USER_AGENT'];
			$estados = array(
				$this->oBD->escapar($tipousuario), $idusuario, $this->oBD->escapar($lugar), (!empty($idcurso) ? $idcurso : 0), $this->oBD->escapar($fechaentrada), $this->oBD->escapar($fechasalida), $idproyecto, $this->oBD->escapar($ip), $this->oBD->escapar($navegador), (!empty($idcc) ? $idcc : 0), (!empty($idgrupoauladetalle) ? $idgrupoauladetalle : 0)
			);


			$sql = "CALL insertarhistorial (" . (implode(",", $estados)) . ")";
			$id = $this->oBD->consultarEscalarSQL($sql);
			/*$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idhistorialsesion) FROM historial_sesion");
			++$id;			
			$estados = array('idhistorialsesion' => $id
							,'tipousuario'=>$tipousuario
							,'idusuario'=>$idusuario
							,'lugar'=>$lugar
							,'fechaentrada'=>$fechaentrada
							,'fechasalida'=>$fechasalida
							,'idproyecto' =>$idproyecto
							,'ip'=>isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
							,'navegador'=>@$_SERVER['HTTP_USER_AGENT']
							);
			if(!empty($idcurso)){
				$estados["idcurso"] = $idcurso;
			}
			$this->oBD->insert('historial_sesion', $estados);*/
			//$this->terminarTransaccion('dat_historial_sesion_insert');		
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_historial_sesion_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}

	public function actualizar($id, $tipousuario, $idusuario, $lugar, $idcurso, $fechaentrada, $fechasalida, $idproyecto, $idgrupoauladetalle = null)
	{
		try {
			$this->iniciarTransaccion('dat_historial_sesion_update');
			$estados = array(
				'tipousuario' => $tipousuario, 'idusuario' => $idusuario, 'lugar' => $lugar
				//,'fechaentrada'=>$fechaentrada
				, 'fechasalida' => $fechasalida, 'idgrupoauladetalle' => $idgrupoauladetalle
			);

			if (!empty($idcurso)) {
				$estados["idcurso"] = $idcurso;
			}
			$this->oBD->update('historial_sesion ', $estados, array('idhistorialsesion' => $id));
			$this->terminarTransaccion('dat_historial_sesion_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  historial_sesion.idhistorialsesion, historial_sesion.tipousuario, historial_sesion.idusuario, historial_sesion.lugar, historial_sesion.idcurso, historial_sesion.fechaentrada, historial_sesion.fechasalida, historial_sesion.idproyecto, historial_sesion.ip, historial_sesion.navegador, historial_sesion.idcc,idgrupoauladetalle  FROM historial_sesion  "
				. " WHERE idhistorialsesion = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('historial_sesion', array('idhistorialsesion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			return $this->oBD->update('historial_sesion', array($propiedad => $valor), array('idhistorialsesion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}

	public function frecuenciaAlumno($filtros, $calcular = false)
	{
		try {
			$sql = ($calcular == false) ? "SELECT DISTINCT DATE(fechaentrada) AS fecha FROM historial_sesion" : "SELECT ABS(SUM(TIME_TO_SEC(TIMEDIFF(fechaentrada, fechasalida)))) AS tiempo FROM historial_sesion";
			$cond = array();

			if (!empty($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (!empty($filtros["lugar"])) {
				$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if (!empty($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($filtros["fechaentrada"])) {
				$cond[] = "date(fechaentrada) like '{$filtros["fechaentrada"]}%'";
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY fechaentrada DESC";

			//var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Historial_sesion") . ": " . $e->getMessage());
		}
	}
}
