CREATE TABLE `reportesgeneral` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Nombre del reporte',
	`descripcion` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Descripcion del reporte',
	`icono` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Puede ser fontawesome o cualquier otro',
	`ruta` VARCHAR(50) NULL DEFAULT NULL COMMENT 'ejemplo: controlador/funcion',
	`estado` TINYINT(4) NULL DEFAULT '1' COMMENT 'Estado logico',
	`fecha_creacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	`fecha_actualizacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);


CREATE TABLE `reportesasign_proyecto` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`idreportegeneral` INT(11) NULL DEFAULT NULL,
	`idbolsa_empresa` INT(11) NULL DEFAULT NULL,
	`idproyecto` INT(11) NULL DEFAULT NULL,
	`estado` TINYINT(4) NULL DEFAULT '1' COMMENT '1 es visible, 0 es invisible',
	`fecha_creacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	`fecha_actualizacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);