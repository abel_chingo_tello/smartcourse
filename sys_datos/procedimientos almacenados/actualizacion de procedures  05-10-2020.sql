DROP PROCEDURE IF EXISTS `insertarhistorial`;

CREATE PROCEDURE `insertarhistorial`(
	IN _tipousuario char(2),
	IN _idusuario bigint(20),
	IN _lugar char(2),
	IN _idcurso bigint,
	IN _fechaentrada datetime,
	IN _fechasalida datetime,
	IN _idproyecto int,
	IN _ip varchar(25),
	IN _navegador varchar(250),
	IN _idcc int,
	IN _idgrupoauladetalle bigint
)
BEGIN
IF _idcc=0 THEN 
	set _idcc=NULL; 
END IF;
IF _idcurso=0 THEN 
	set _idcurso=NULL; 
END IF;
IF _idgrupoauladetalle=0 THEN 
	set _idgrupoauladetalle=NULL; 
END IF;

INSERT INTO historial_sesion (`tipousuario`,`idusuario`,`lugar`,`idcurso`,`fechaentrada`,`fechasalida`,`idproyecto`,`ip`,`navegador`,idcc,idgrupoauladetalle)
VALUES(_tipousuario,_idusuario,_lugar,_idcurso,_fechaentrada,_fechasalida,_idproyecto,_ip,_navegador,_idcc,_idgrupoauladetalle);
SELECT last_insert_id() AS id;
END




DROP PROCEDURE IF EXISTS `insertarbitacoraalumnosmartbook`;
CREATE PROCEDURE `insertarbitacoraalumnosmartbook`(
	IN `_idcurso` bigint(20),
	IN `_idsesion` int,
	IN `_idusuario` bigint(20),
	IN `_idcomplementario` bigint(20),
	IN `_estado` varchar(4),
	IN `_regfecha` timestamp,
	IN _idgrupoauladetalle bigint
)
BEGIN
SELECT idbitacora_smartbook+1 INTO @ID FROM  bitacora_alumno_smartbook ORDER BY idbitacora_smartbook DESC LIMIT 0,1;
INSERT INTO bitacora_alumno_smartbook (idbitacora_smartbook,idcurso,idsesion,idusuario,idcomplementario,estado,regfecha,idgrupoauladetalle)
VALUES(@ID,_idcurso,_idsesion,_idusuario,_idcomplementario,_estado,_regfecha,_idgrupoauladetalle);
SELECT @ID;
END

DROP PROCEDURE IF EXISTS `insertarbitacora`;
CREATE PROCEDURE `insertarbitacora`(
	IN `_idcurso` bigint(20),
	IN `_idsesion` int,
	IN `_idsesionB` bigint(20),
	IN `_idusuario` bigint(20),
	IN `_idcomplementario` bigint(20),
	IN `_idbitacora_alum_smartbook` bigint(20),
	IN `_pestania` VARCHAR(128),
	IN `_total_pestanias` int,
	IN `_fechahora` timestamp,
	IN `_progreso` float(5,2),
	IN `_otros_datos` text,
	IN _idgrupoauladetalle bigint

)
BEGIN
INSERT INTO bitacora_smartbook (idcurso,idsesion,idsesionB,idusuario,idcomplementario,idbitacora_alum_smartbook,pestania,total_pestanias,fechahora,progreso,otros_datos,idgrupoauladetalle)
VALUES(_idcurso,_idsesion,_idsesionB,_idusuario,_idcomplementario,_idbitacora_alum_smartbook,_pestania,_total_pestanias,_fechahora,_progreso,_otros_datos,_idgrupoauladetalle);
SELECT last_insert_id() AS id;
END