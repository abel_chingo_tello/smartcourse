<?php
class DatSence_assigncursos extends DatBase {
    public function __construct(){
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Sence_assigncursos").": " . $e->getMessage());
		}
    }
    /**
     * Funcion generica para buscar en la tabla especifica sin datos relacionados
     * Por favor no modificar tanto la logica de la función y si es necesaria, delimitar consulta mediante un parametro especifico
     * ACTUALIZADO: 10/11/2020
     * @param Array Son los filtros del a consulta donde se condiciona la busqueda
     * @return Array El resultado de la consulta
     */
    public function buscar($filtros = null){
        try{
            $sql = "SELECT sac.id,sac.idempresa,sac.codsence,sac.idproyecto,sac.idcurso,c.nombre AS nombrecurso,sac.idcomplementario,sac.estado,sac.fecha_creacion,sac.fecha_actualizacion FROM sence_assigncursos sac LEFT JOIN acad_curso c ON sac.idcurso = c.idcurso";
            
            $cond = array();
            
            if (isset($filtros["id"])) {
                if(is_array($filtros["id"])){
                    $cond[] = "sac.id IN (" . implode(",",$filtros["id"]).")";
                }else{
                    $cond[] = "sac.id = " . $this->oBD->escapar($filtros["id"]);
                }
            }
            if (isset($filtros["idempresa"])){ $cond[] = "sac.idempresa = " . $this->oBD->escapar($filtros["idempresa"]); }
            if (isset($filtros["codsence"])){ $cond[] = "sac.codsence = " . $this->oBD->escapar($filtros["codsence"]); }
            if (isset($filtros["idproyecto"])){ $cond[] = "sac.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]); }
            if (isset($filtros["idcurso"])){ $cond[] = "sac.idcurso = " . $this->oBD->escapar($filtros["idcurso"]); }
            if (isset($filtros["idcomplementario"])){ $cond[] = "sac.idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]); }
            if (isset($filtros["estado"])){ $cond[] = "sac.estado = " . $this->oBD->escapar($filtros["estado"]); }
            if (isset($filtros["fecha_creacion"])){ $cond[] = "sac.fecha_creacion = " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["desdeFecha_creacion"])){ $cond[] = "sac.fecha_creacion >= " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["hastaFecha_creacion"])){ $cond[] = "sac.fecha_creacion <= " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["fecha_actualizacion"])){ $cond[] = "sac.fecha_actualizacion = " . $this->oBD->escapar($filtros["fecha_actualizacion"]); } 
            if (isset($filtros["desdeFecha_actualizacion"])){ $cond[] = "sac.fecha_actualizacion >= " . $this->oBD->escapar($filtros["fecha_actualizacion"]); } 
            if (isset($filtros["hastaFecha_actualizacion"])){ $cond[] = "sac.fecha_actualizacion <= " . $this->oBD->escapar($filtros["fecha_actualizacion"]); } 
            
            if (!empty($cond)) { $sql .= " WHERE " . implode(' AND ', $cond); }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
			throw new Exception("ERROR\n" . JrTexto::_("buscar") . " " . JrTexto::_("Sence_assigncursos") . ": " . $e->getMessage());
        }
    }
    /**
     * Funcion generica para insertar registro en la tabla, se puede registrar 1 a N cantidad de registro ya que actua de manera masiva
     * La función esta destinada a insertar como tabla unica, si se requiere hacer un insertado mas complejo, por favor hacer otra función
     * ya que esta función es generica y se puede reutilizar en diferentes modulos del sistema
     * @param Array los registro a procesar
     * @return Integer Retorna el ultimo id insertado
     */
    public function insert($data){
        try{
            $id = false;
            $this->iniciarTransaccion('dat_sence_assigncursos_insert');
            if(empty($data)){ throw new Exception("Data esta vacio"); }
            foreach($data as $row){
                $id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM sence_assigncursos");
                ++$id;
                $row['id']=$id;
				$this->oBD->insert('sence_assigncursos', $row);
            }
			$this->terminarTransaccion('dat_sence_assigncursos_insert');            
            return $id;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_sence_assigncursos_insert');
            throw new Exception("ERROR\n" . JrTexto::_("insert") . " " . JrTexto::_("Sence_assigncursos") . ": " . $e->getMessage());
        }
    }
    /**
     * Funcion generica para actualizar registro en la tabla, se puede actualizar un unico registro como varios, ya que actua de manera masiva el proceso.
     * La funcion esta destinada solo a actualizar registros sin logica especial, si se requiere un proceso mas complejo, por favor crea otra funcion preferiblemente
     * Este proceso es reutilizado por varios modulos del sistema, y un cambio drastico puede causar fallas
     * @param Array los registro a procesar
     * @param Boolean verifica si es estricto las validaciones, si esta estricto y no posee el identificador del registro, lanzara un throw Exception
     * @return Boolean Resultado en TRUE si se realizo el proceso completamente, FALSE algo ocurrio
     */
    public function update($data,$esEstricto = false){
        try{
            $this->iniciarTransaccion('dat_sence_assigncursos_update');
            if(empty($data)){ throw new Exception("Data esta vacio"); }
            foreach($data as $row){
                if(empty($row['id'])){ if($esEstricto === false){ continue; }else{ throw new Exception("Un registro no tiene definido el id para actualizar"); }  }
                $id = $row['id'];
                unset($row['id']);
                $this->oBD->update('sence_assigncursos', $row, array('id' => $id));
            }
			$this->terminarTransaccion('dat_sence_assigncursos_update');            
            return true;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_sence_assigncursos_update');
            throw new Exception("ERROR\n" . JrTexto::_("update") . " " . JrTexto::_("sence_assigncursos") . ": " . $e->getMessage());
        }
    }
}
?>