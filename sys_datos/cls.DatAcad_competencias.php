<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatAcad_competencias extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_competencias").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			
			$sql = "SELECT co.idcompetencia,co.idcategoria,co.nombre,co.estado,co.idproyecto,(SELECT nombre FROM acad_categorias ca WHERE ca.idcategoria = co.idcategoria) as nombre_categoria ,(SELECT idpadre FROM acad_categorias ca WHERE ca.idcategoria = co.idcategoria) as idpadre ,(SELECT nombre FROM acad_categorias ca WHERE ca.idcategoria = (SELECT idpadre FROM acad_categorias ca WHERE ca.idcategoria = co.idcategoria)) as categoriapadre FROM acad_competencias co";			
			
			$cond = array();
			if(isset($filtros["idcategoria"])) {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}

			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(isset($filtros["texto"])) {
					$cond[] = "co.nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "co.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_competencias").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcategoria,$nombre,$estado,$idproyecto=0)
	{
		try {			
			$this->iniciarTransaccion('dat_acad_competencias_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT idcompetencia FROM acad_competencias ORDER BY idcompetencia DESC LIMIT 0,1");
			++$id;
			
			$estados = array('idcompetencia' => $id							
							,'idcategoria'=>$idcategoria
							,'nombre'=>$nombre
							,'estado'=>$estado	
							,'idproyecto'=>$idproyecto
							);
			
			$this->oBD->insert('acad_competencias', $estados);			
			$this->terminarTransaccion('dat_acad_competencias_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_competencias_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_competencias").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcategoria,$nombre,$estado,$idproyecto=0)
	{
		try {
			$this->iniciarTransaccion('dat_acad_competencias_update');
			$estados = array('idcategoria'=>$idcategoria
							,'nombre'=>$nombre
							,'estado'=>$estado
							,'idproyecto'=>$idproyecto							
							);
			
			$this->oBD->update('acad_competencias ', $estados, array('idcompetencia' => $id));
		    $this->terminarTransaccion('dat_acad_competencias_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_competencias").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT co.idcompetencia,co.idcategoria,co.nombre,co.estado,co.idproyecto,(SELECT nombre FROM acad_categorias ca WHERE ca.idcategoria = co.idcategoria) as nombre_categoria ,(SELECT idpadre FROM acad_categorias ca WHERE ca.idcategoria = co.idcategoria) as idpadre ,(SELECT nombre FROM acad_categorias ca WHERE ca.idcategoria = (SELECT idpadre FROM acad_categorias ca WHERE ca.idcategoria = co.idcategoria)) as categoriapadre FROM acad_competencias co  "
					. " WHERE idcompetencia = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_competencias").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_competencias', array('idcompetencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_competencias").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_competencias', array($propiedad => $valor), array('idcompetencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_competencias").": " . $e->getMessage());
		}
	}
   
		
}