<?php
class DatSence_usuario extends DatBase{
    public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Sence_usuario").": " . $e->getMessage());
		}
    }
    /**
     * Funcion generica para buscar en la tabla especifica sin datos relacionados
     * Por favor no modificar tanto la logica de la función y si es necesaria, delimitar consulta mediante un parametro especifico
     * @param Array Son los filtros del a consulta donde se condiciona la busqueda
     * @return Array El resultado de la consulta
     */
    public function buscar($filtros = null){
        try{
            $sql = "SELECT su.id,su.idpersona,su.idempresa,su.idproyecto,su.idrol, su.idcurso,su.codusuario,su.codigocurso,su.codigosence,su.tipo,su.url,su.url_cierre,su.fecha_iniciosesion,su.fecha_iniciosesion,su.fecha_cierresesion,su.fecha_creacion,su.fecha_actualizacion FROM sence_usuario su";
            
            $cond = array();
            
            if (isset($filtros["id"])) {
                if(is_array($filtros["id"])){
                    $cond[] = "su.id IN (" . implode(",",$filtros["id"]).")";
                }else{
                    $cond[] = "su.id = " . $this->oBD->escapar($filtros["id"]);
                }
            }
            if (isset($filtros["idpersona"])){ $cond[] = "su.idpersona = " . $this->oBD->escapar($filtros["idpersona"]); }
            if (isset($filtros["idempresa"])){ $cond[] = "su.idempresa = " . $this->oBD->escapar($filtros["idempresa"]); }
            if (isset($filtros["idproyecto"])){ $cond[] = "su.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]); }
            if (isset($filtros["idrol"])){ $cond[] = "su.idrol = " . $this->oBD->escapar($filtros["idrol"]); }
            if (isset($filtros["idcurso"])){ $cond[] = "su.idcurso = " . $this->oBD->escapar($filtros["idcurso"]); }
            if (isset($filtros["codusuario"])){ $cond[] = "su.codusuario = " . $this->oBD->escapar($filtros["codusuario"]); }
            if (isset($filtros["codigocurso"])){ $cond[] = "su.codigocurso = " . $this->oBD->escapar($filtros["codigocurso"]); }
            if (isset($filtros["codigosence"])){ $cond[] = "su.codigosence = " . $this->oBD->escapar($filtros["codigosence"]); }
            if (isset($filtros["tipo"])){ $cond[] = "su.tipo = " . $this->oBD->escapar($filtros["tipo"]); }
            if (isset($filtros["url"])){ $cond[] = "su.url = " . $this->oBD->escapar($filtros["url"]); }
            if (isset($filtros["url_cierre"])){ $cond[] = "su.url_cierre = " . $this->oBD->escapar($filtros["url_cierre"]); }
            if (isset($filtros["fecha_iniciosesion"])){ $cond[] = "su.fecha_iniciosesion = " . $this->oBD->escapar($filtros["fecha_iniciosesion"]); }
            if (isset($filtros["desdeFecha_iniciosesion"])){ $cond[] = "su.fecha_iniciosesion >= " . $this->oBD->escapar($filtros["fecha_iniciosesion"]); }
            if (isset($filtros["hastaFecha_iniciosesion"])){ $cond[] = "su.fecha_iniciosesion <= " . $this->oBD->escapar($filtros["fecha_iniciosesion"]); }
            if (isset($filtros["fecha_cierresesion"])){ $cond[] = "su.fecha_cierresesion = " . $this->oBD->escapar($filtros["fecha_cierresesion"]); }
            if (isset($filtros["desdeFecha_cierresesion"])){ $cond[] = "su.fecha_cierresesion >= " . $this->oBD->escapar($filtros["fecha_cierresesion"]); }
            if (isset($filtros["hastaFecha_cierresesion"])){ $cond[] = "su.fecha_cierresesion <= " . $this->oBD->escapar($filtros["fecha_cierresesion"]); }
            if (isset($filtros["fecha_creacion"])){ $cond[] = "su.fecha_creacion = " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["desdeFecha_creacion"])){ $cond[] = "su.fecha_creacion >= " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["hastaFecha_creacion"])){ $cond[] = "su.fecha_creacion <= " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["fecha_actualizacion"])){ $cond[] = "su.fecha_actualizacion = " . $this->oBD->escapar($filtros["fecha_actualizacion"]); }
            if (isset($filtros["desdeFecha_actualizacion"])){ $cond[] = "su.fecha_actualizacion >= " . $this->oBD->escapar($filtros["fecha_actualizacion"]); }
            if (isset($filtros["hastaFecha_actualizacion"])){ $cond[] = "su.fecha_actualizacion <= " . $this->oBD->escapar($filtros["fecha_actualizacion"]); }

            if (!empty($cond)) { $sql .= " WHERE " . implode(' AND ', $cond); }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
			throw new Exception("ERROR\n" . JrTexto::_("buscar") . " " . JrTexto::_("Sence_usuario") . ": " . $e->getMessage());
        }
    }
    /**
     * Funcion que retorna toda la información relacionada a un usuario/curso sence ingresado en el sistema, en el cual se obtiene por relaciones de tablas
     * la información devuelta posee los datos del usuario sence y ademas otros datos importante de la empresa.
     * Esta función es de uso especial, no se reutiliza mucho sin embargo tener precaución a la hora de hacer un cambio
     * @param Array Son los filtros del a consulta donde se condiciona la busqueda
     * @return Array El resultado de la consulta
     */
    public function busquedacompleta($filtros = null){
        try{
            $sql="SELECT su.*,sc.id AS idsence_cursovalor,sc.CodSence,sc.CodigoCurso,sc.IdSesionAlumno,sc.IdSesionSence,sc.RunAlumno,sc.FechaHora,sc.ZonaHoraria,sc.LineaCapacitacion, sc.GlosaError,be.Token,be.RutOtec FROM `sence_usuario` su INNER JOIN sence_cursovalor sc ON su.id = sc.idusuariosence INNER JOIN bolsa_empresas be ON be.idempresa = su.idempresa";
            
            if (isset($filtros["id"])) {
                if(is_array($filtros["id"])){
                    $cond[] = "su.id IN (" . implode(",",$filtros["id"]).")";
                }else{
                    $cond[] = "su.id = " . $this->oBD->escapar($filtros["id"]);
                }
            }
            if (isset($filtros["idpersona"])){ $cond[] = "su.idpersona = " . $this->oBD->escapar($filtros["idpersona"]); }
            if (isset($filtros["idempresa"])){ $cond[] = "su.idempresa = " . $this->oBD->escapar($filtros["idempresa"]); }
            if (isset($filtros["idproyecto"])){ $cond[] = "su.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]); }
            if (isset($filtros["idrol"])){ $cond[] = "su.idrol = " . $this->oBD->escapar($filtros["idrol"]); }
            if (isset($filtros["idcurso"])){ $cond[] = "su.idcurso = " . $this->oBD->escapar($filtros["idcurso"]); }
            if (isset($filtros["codusuario"])){ $cond[] = "su.codusuario = " . $this->oBD->escapar($filtros["codusuario"]); }
            if (isset($filtros["codigocurso"])){ $cond[] = "su.codigocurso = " . $this->oBD->escapar($filtros["codigocurso"]); }
            if (isset($filtros["tipo"])){ $cond[] = "su.tipo = " . $this->oBD->escapar($filtros["tipo"]); }
            if (isset($filtros["url"])){ $cond[] = "su.url = " . $this->oBD->escapar($filtros["url"]); }
            if (isset($filtros["url_cierre"])){ $cond[] = "su.url_cierre = " . $this->oBD->escapar($filtros["url_cierre"]); }
            if (isset($filtros["fecha_iniciosesion"])){ $cond[] = "su.fecha_iniciosesion = " . $this->oBD->escapar($filtros["fecha_iniciosesion"]); }
            if (isset($filtros["desdeFecha_iniciosesion"])){ $cond[] = "su.fecha_iniciosesion >= " . $this->oBD->escapar($filtros["fecha_iniciosesion"]); }
            if (isset($filtros["hastaFecha_iniciosesion"])){ $cond[] = "su.fecha_iniciosesion <= " . $this->oBD->escapar($filtros["fecha_iniciosesion"]); }
            if (isset($filtros["fecha_cierresesion"])){ $cond[] = "su.fecha_cierresesion = " . $this->oBD->escapar($filtros["fecha_cierresesion"]); }
            if (isset($filtros["desdeFecha_cierresesion"])){ $cond[] = "su.fecha_cierresesion >= " . $this->oBD->escapar($filtros["fecha_cierresesion"]); }
            if (isset($filtros["hastaFecha_cierresesion"])){ $cond[] = "su.fecha_cierresesion <= " . $this->oBD->escapar($filtros["fecha_cierresesion"]); }
            if (isset($filtros["fecha_creacion"])){ $cond[] = "su.fecha_creacion = " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["desdeFecha_creacion"])){ $cond[] = "su.fecha_creacion >= " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["hastaFecha_creacion"])){ $cond[] = "su.fecha_creacion <= " . $this->oBD->escapar($filtros["fecha_creacion"]); }
            if (isset($filtros["fecha_actualizacion"])){ $cond[] = "su.fecha_actualizacion = " . $this->oBD->escapar($filtros["fecha_actualizacion"]); }
            if (isset($filtros["desdeFecha_actualizacion"])){ $cond[] = "su.fecha_actualizacion >= " . $this->oBD->escapar($filtros["fecha_actualizacion"]); }
            if (isset($filtros["hastaFecha_actualizacion"])){ $cond[] = "su.fecha_actualizacion <= " . $this->oBD->escapar($filtros["fecha_actualizacion"]); }

            if (!empty($cond)) { $sql .= " WHERE " . implode(' AND ', $cond); }

            return $this->oBD->consultarSQL($sql);
        }catch(Exception $e){
			throw new Exception("ERROR\n" . JrTexto::_("busquedacompleta") . " " . JrTexto::_("Sence_usuario") . ": " . $e->getMessage());
        }
    }
    /**
     * Funcion generica para insertar registro en la tabla, se puede registrar 1 a N cantidad de registro ya que actua de manera masiva
     * La función esta destinada a insertar como tabla unica, si se requiere hacer un insertado mas complejo, por favor hacer otra función
     * ya que esta función es generica y se puede reutilizar en diferentes modulos del sistema
     * @param Array los registro a procesar
     * @return Integer Retorna el ultimo id insertado
     */
    public function insert($data){
        try{
            $id = false;
            $this->iniciarTransaccion('dat_sence_usuario_insert');
            if(empty($data)){ throw new Exception("Data esta vacio"); }
            foreach($data as $row){
                $id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM sence_usuario");
                ++$id;
                $row['id']=$id;
				$this->oBD->insert('sence_usuario', $row);
            }
			$this->terminarTransaccion('dat_sence_usuario_insert');            
            return $id;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_sence_usuario_insert');
            throw new Exception("ERROR\n" . JrTexto::_("insert") . " " . JrTexto::_("Sence_usuario") . ": " . $e->getMessage());
        }
    }
    /**
     * Funcion generica para actualizar registro en la tabla, se puede actualizar un unico registro como varios, ya que actua de manera masiva el proceso.
     * La funcion esta destinada solo a actualizar registros sin logica especial, si se requiere un proceso mas complejo, por favor crea otra funcion preferiblemente
     * Este proceso es reutilizado por varios modulos del sistema, y un cambio drastico puede causar fallas
     * @param Array los registro a procesar
     * @param Boolean verifica si es estricto las validaciones, si esta estricto y no posee el identificador del registro, lanzara un throw Exception
     * @return Boolean Resultado en TRUE si se realizo el proceso completamente, FALSE algo ocurrio
     */
    public function update($data,$esEstricto = false){
        try{
            $this->iniciarTransaccion('dat_sence_usuario_update');
            if(empty($data)){ throw new Exception("Data esta vacio"); }
            foreach($data as $row){
                if(empty($row['id'])){ if($esEstricto === false){ continue; }else{ throw new Exception("Un registro no tiene definido el id para actualizar"); }  }
                $id = $row['id'];
                unset($row['id']);
                $this->oBD->update('sence_usuario', $row, array('id' => $id));
            }
			$this->terminarTransaccion('dat_sence_usuario_update');            
            return true;
        }catch(Exception $e){
			$this->cancelarTransaccion('dat_sence_usuario_update');
            throw new Exception("ERROR\n" . JrTexto::_("update") . " " . JrTexto::_("Sence_usuario") . ": " . $e->getMessage());
        }
    }
    public function delete($data){}
}
?>