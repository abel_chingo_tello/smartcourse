<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */

class DatCron_tarea_proy extends DatBase
{
	public function __construct()
	{

		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Cron_tarea_proy") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT cron_tarea_proy.idcron_tapr, cron_tarea_proy.idproyecto, cron_tarea_proy.fecha_hora, cron_tarea_proy.estado, cron_tarea_proy.idsesion, cron_tarea_proy.idpestania, cron_tarea_proy.idcurso, cron_tarea_proy.idcomplementario, cron_tarea_proy.iddocente, cron_tarea_proy.nombre, cron_tarea_proy.tipo, cron_tarea_proy.recurso, cron_tarea_proy.tiporecurso, cron_tarea_proy.idgrupoauladetalle
					FROM cron_tarea_proy ";

			$cond = array();


			if (isset($filtros["idcron_tapr"])) {
				$cond[] = "idcron_tapr = " . $this->oBD->escapar($filtros["idcron_tapr"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["fecha_hora"])) {
				$cond[] = "fecha_hora = " . $this->oBD->escapar($filtros["fecha_hora"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idpestania"])) {
				$cond[] = "idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["recurso"])) {
				$cond[] = "recurso = " . $this->oBD->escapar($filtros["recurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Cron_tarea_proy") . ": " . $e->getMessage());
		}
	}


	public function insertar($idproyecto, $fecha_hora, $estado, $idsesion, $idpestania, $idcurso, $idcomplementario, $iddocente, $nombre, $tipo, $recurso, $tiporecurso, $idgrupoauladetalle)
	{
		try {

			$this->iniciarTransaccion('dat_cron_tarea_proy_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcron_tapr) FROM cron_tarea_proy");
			++$id;

			$estados = array(
				'idcron_tapr' => $id,
				'idproyecto' => $idproyecto,
				'fecha_hora' => $fecha_hora,
				'estado' => 0,
				'idsesion' => $idsesion,
				'idpestania' => $idpestania,
				'idcurso' => $idcurso,
				'idcomplementario' => $idcomplementario,
				'iddocente' => $iddocente,
				'nombre' => $nombre,
				'tipo' => $tipo,
				'recurso' => $recurso,
				'tiporecurso' => $tiporecurso,
				'idgrupoauladetalle' => $idgrupoauladetalle
			);

			$this->oBD->insert('cron_tarea_proy', $estados);
			$this->terminarTransaccion('dat_cron_tarea_proy_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_cron_tarea_proy_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Cron_tarea_proy") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idproyecto, $fecha_hora, $estado, $idsesion, $idpestania, $idcurso, $idcomplementario, $iddocente, $nombre, $tipo, $recurso, $tiporecurso)
	{
		try {
			$this->iniciarTransaccion('dat_cron_tarea_proy_update');
			$estados = array(
				'idproyecto' => $idproyecto, 'fecha_hora' => $fecha_hora, 'estado' => $estado, 'idsesion' => $idsesion, 'idpestania' => $idpestania, 'idcurso' => $idcurso, 'idcomplementario' => $idcomplementario, 'iddocente' => $iddocente, 'nombre' => $nombre, 'tipo' => $tipo, 'recurso' => $recurso, 'tiporecurso' => $tiporecurso
			);
			$this->oBD->update('cron_tarea_proy ', $estados, array('idcron_tapr' => $id));
			$this->terminarTransaccion('dat_cron_tarea_proy_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Cron_tarea_proy") . ": " . $e->getMessage());
		}
	}
	public function customUpdate($filtros, $update)
	{
		try {
			$this->iniciarTransaccion('dat_cron_tarea_proy_update');

			$sql = "UPDATE cron_tarea_proy ";
			$cond = array();
			if (isset($update["idcron_tapr"])) {
				$cond[] = "idcron_tapr = " . $this->oBD->escapar($update["idcron_tapr"]);
			}
			if (isset($update["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($update["idproyecto"]);
			}
			if (isset($update["fecha_hora"])) {
				$cond[] = "fecha_hora = " . $this->oBD->escapar($update["fecha_hora"]);
			}
			if (isset($update["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($update["estado"]);
			}
			if (isset($update["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($update["idsesion"]);
			}
			if (isset($update["idpestania"])) {
				$cond[] = "idpestania = " . $this->oBD->escapar($update["idpestania"]);
			}
			if (isset($update["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($update["idcurso"]);
			}
			if (isset($update["idcomplementario"])) {
				$cond[] = "idcomplementario = " . $this->oBD->escapar($update["idcomplementario"]);
			}
			if (isset($update["iddocente"])) {
				$cond[] = "iddocente = " . $this->oBD->escapar($update["iddocente"]);
			}
			if (isset($update["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($update["nombre"]);
			}
			if (isset($update["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($update["tipo"]);
			}
			if (isset($update["recurso"])) {
				$cond[] = "recurso = " . $this->oBD->escapar($update["recurso"]);
			}
			if (isset($update["tiporecurso"])) {
				$cond[] = "tiporecurso = " . $this->oBD->escapar($update["tiporecurso"]);
			}
			if (!empty($cond)) {
				$sql .= " SET " . implode(' , ', $cond);
			}
			$cond = array();
			//
			if (isset($filtros["idcron_tapr"])) {
				$cond[] = "idcron_tapr = " . $this->oBD->escapar($filtros["idcron_tapr"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["fecha_hora"])) {
				$cond[] = "fecha_hora = " . $this->oBD->escapar($filtros["fecha_hora"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idpestania"])) {
				$cond[] = "idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["recurso"])) {
				$cond[] = "recurso = " . $this->oBD->escapar($filtros["recurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// echo "$sql";
			// exit();
			$this->oBD->ejecutarSQL($sql);
			return $this->terminarTransaccion('dat_cron_tarea_proy_update');
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Cron_tarea_proy") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  cron_tarea_proy.idcron_tapr, cron_tarea_proy.idproyecto, cron_tarea_proy.fecha_hora, cron_tarea_proy.estado, cron_tarea_proy.idsesion, cron_tarea_proy.idpestania, cron_tarea_proy.idcurso, cron_tarea_proy.idcomplementario, cron_tarea_proy.iddocente, cron_tarea_proy.nombre, cron_tarea_proy.tipo, cron_tarea_proy.recurso, cron_tarea_proy.tiporecurso  FROM cron_tarea_proy  "
				. " WHERE idcron_tapr = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Cron_tarea_proy") . ": " . $e->getMessage());
		}
	}

	public function eliminar($filtros)
	{
		try {
			$sql = "DELETE FROM cron_tarea_proy";
			if (isset($filtros["idcron_tapr"])) {
				$cond[] = "idcron_tapr = " . $this->oBD->escapar($filtros["idcron_tapr"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["fecha_hora"])) {
				$cond[] = "fecha_hora = " . $this->oBD->escapar($filtros["fecha_hora"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idpestania"])) {
				$cond[] = "idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			// echo "$sql"; //exit();
			return $this->oBD->ejecutarSQL($sql);
			// return $this->oBD->delete('cron_tarea_proy', array('idcron_tapr' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Cron_tarea_proy") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('cron_tarea_proy', array($propiedad => $valor), array('idcron_tapr' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Cron_tarea_proy") . ": " . $e->getMessage());
		}
	}
	public function buscarTarea($filtros = null)
	{
		try {
			//$sql = "SELECT *,(select p.nombre FROM personal p where p.idpersona = ta.idalumno) as nombre_alumno FROM tareas ta";			
			$sql = "SELECT ta.criterios, ta.criterios_puntaje, ta.descripcion, ta.fecha_calificacion, ta.fecha_creacion, ta.idalumno, ta.idcomplementario, ta.idcurso, ta.iddocente, ta.idpestania, ta.idproyecto, ta.idsesion, ta.idtarea, ta.nombre, ta.nota, ta.notabaseen, ta.recurso, ta.tipo, ta.tiporecurso, ta.vencido,
			(select concat(d.ape_paterno,' ',d.ape_materno,', ',d.nombre) FROM personal d where d.idpersona = ta.iddocente) as nombre_docente,(select concat(p.ape_paterno,' ',ape_materno,', ',p.nombre) FROM personal p where p.idpersona = ta.idalumno) as nombre_alumno,(select tar.media from tareas_archivosalumno tar where tar.idtarea = ta.idtarea order by tar.idtarearecurso desc limit 1) as media,(select tar.tipo from tareas_archivosalumno tar where tar.idtarea = ta.idtarea order by tar.idtarearecurso desc limit 1) as tipo_media FROM tareas ta";

			if (isset($filtros["sql"])) {
				switch ($filtros["sql"]) {
					case '1':
						$sql = "SELECT tareas.criterios, tareas.criterios_puntaje, tareas.descripcion, tareas.fecha_calificacion, tareas.fecha_creacion, tareas.idalumno, tareas.idcomplementario, tareas.idcurso, tareas.iddocente, tareas.idpestania, tareas.idproyecto, tareas.idsesion, tareas.idtarea, tareas.nombre, tareas.nota, tareas.notabaseen, tareas.recurso, tareas.tipo, tareas.tiporecurso, tareas.vencido, concat(personal.ape_paterno,' ', personal.ape_materno,', ', personal.nombre ) as nombre_docente 
								FROM tareas 
								left join personal on tareas.iddocente=personal.idpersona";
						break;
				}
			}
			$cond = array();
			if (isset($filtros["idtarea"])) {
				$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}

			if (isset($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idpestania"])) {
				$cond[] = "idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["recurso"])) {
				$cond[] = "recurso = " . $this->oBD->escapar($filtros["recurso"]);
			}
			if (isset($filtros["tiporecurso"])) {
				$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if (isset($filtros["nota"])) {
				$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if (isset($filtros["notabaseen"])) {
				$cond[] = "notabaseen = " . $this->oBD->escapar($filtros["notabaseen"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["fecha_calificacion"])) {
				$cond[] = "fecha_calificacion = " . $this->oBD->escapar($filtros["fecha_calificacion"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql;exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}


	public function insertarTarea($params)
	{
		try {
			extract($params);
			$this->iniciarTransaccion('dat_tareas_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtarea) FROM tareas");
			++$id;

			$estados = array(
				'idtarea' => $id, 'vencido' => !empty($vencido) ? $vencido : 0, 'fecha_calificacion' => $fecha_calificacion, 'tipo' => !empty($tipo) ? $tipo : 'T', 'idcurso' => $idcurso, 'idsesion' => $idsesion, 'idpestania' => $idpestania, 'idalumno' => $idalumno, 'recurso' => $recurso, 'tiporecurso' => $tiporecurso, 'nota' => !empty($nota) ? $nota : 0, 'notabaseen' => !empty($notabaseen) ? $notabaseen : 0, 'iddocente' => $iddocente, 'idproyecto' => $idproyecto, 'nombre' => !empty($nombre) ? $nombre : '', 'descripcion' => !empty($descripcion) ? $descripcion : '', 'criterios' => "",
				'idcomplementario' => $idcomplementario
			);
			if (isset($idgrupoauladetalle) && !empty($idgrupoauladetalle)) {
				$estados["idgrupoauladetalle"] = $idgrupoauladetalle;
			}
			$this->oBD->insert('tareas', $estados);
			$this->terminarTransaccion('dat_tareas_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_tareas_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Tareas") . ": " . $e->getMessage());
		}
	}
}
