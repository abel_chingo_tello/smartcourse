<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-12-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatRecursos_colaborativosasignacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idrecursoasignacion,idrecurso,idalumno,CONCAT(ape_paterno,' ',ape_materno,', ',nombre) AS stralumno, grupo,idgrupoauladetalle,nota,nota_base,iddocente,fecha_calificacion,titulo,respuestas,fecha_respuesta,idcursodetalle,idpestania,fecha_creacion,tema,
			(select count(idrecursocomentario) from recursos_colabasig_comentario rr  where idasignacion=idrecursoasignacion) AS numero_mensaje
			 FROM recursos_colaborativosasignacion rc INNER JOIN personal al on rc.idalumno=al.idpersona";
			$cond = array();

			if(isset($filtros["solonota"])) {
				$sql = "SELECT idrecursoasignacion,idrecurso,idalumno,nota,nota_base,iddocente,fecha_calificacion FROM recursos_colaborativosasignacion";
			}
			
			if(isset($filtros["idrecursoasignacion"])) {
					$cond[] = "idrecursoasignacion = " . $this->oBD->escapar($filtros["idrecursoasignacion"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["noidalumno"])){
				$cond[] = "idalumno != ".$this->oBD->escapar($filtros["noidalumno"]);
			}
			if(isset($filtros["grupo"])) {
					$cond[] = "grupo = " . $this->oBD->escapar($filtros["grupo"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["nota"])) {
					$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if(isset($filtros["nota_base"])) {
					$cond[] = "nota_base = " . $this->oBD->escapar($filtros["nota_base"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["fecha_calificacion"])) {
					$cond[] = "fecha_calificacion = " . $this->oBD->escapar($filtros["fecha_calificacion"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["respuestas"])) {
				$cond[] = "respuestas = " . $this->oBD->escapar($filtros["respuestas"]);
			}

			if(isset($filtros["conrespuestas"])) {
				$cond[] = "respuestas IS NOT NULL  ";
			}

			if(isset($filtros["fecha_respuesta"])) {
					$cond[] = "fecha_respuesta = " . $this->oBD->escapar($filtros["fecha_respuesta"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idpestania"])){
				if($filtros["idpestania"]=='null')
					$cond[] = "idpestania IS NULL ";
				else $cond[] = "idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if(isset($filtros["fecha_creacion"])) {
					$cond[] = "fecha_creacion = " . $this->oBD->escapar($filtros["fecha_creacion"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM recursos_colaborativosasignacion";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	
			if(isset($filtros["orderby"])){
				 $sql .= " ORDER BY ".$filtros["orderby"];	
			}else 		
			 $sql .= " ORDER BY idrecursoasignacion ASC";		
			//echo $sql;	

			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}


		
	public function insertar($idrecurso,$idalumno,$grupo,$idgrupoauladetalle,$nota,$nota_base,$iddocente,$fecha_calificacion,$titulo,$respuestas,$fecha_respuesta,$idcursodetalle,$idpestania)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idrecursoasignacion+1 FROM recursos_colaborativosasignacion ORDER BY idrecursoasignacion DESC limit 0,1 ");			
			$estados = array('idrecurso'=>$idrecurso
							,'idalumno'=>$idalumno
							,'grupo'=>$grupo
							,'idgrupoauladetalle'=>$idgrupoauladetalle
							,'nota'=>!empty($nota)?$nota:'null'
							,'nota_base'=>!empty($nota_base)?$nota_base:'null'
							,'iddocente'=>!empty($iddocente)?$iddocente:'null'
							,'fecha_calificacion'=>!empty($fecha_calificacion)?$fecha_calificacion:'null'
							,'titulo'=>!empty($titulo)?$titulo:'null'
							,'respuestas'=>!empty($respuestas)?$respuestas:'null'
							,'fecha_respuesta'=>!empty($fecha_respuesta)?$fecha_respuesta:'null'
							,'idcursodetalle'=>$idcursodetalle
							,'idpestania'=>!empty($idpestania)?$idpestania:'null'
							);			
			return $this->oBD->insertAI('recursos_colaborativosasignacion', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_recursos_colaborativosasignacion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idrecurso,$idalumno,$grupo,$idgrupoauladetalle,$nota,$nota_base,$iddocente,$fecha_calificacion,$titulo,$respuestas,$fecha_respuesta,$idcursodetalle,$idpestania)
	{
		try {			
			$estados = array('idrecurso'=>$idrecurso
							,'idalumno'=>$idalumno
							,'grupo'=>$grupo
							,'idgrupoauladetalle'=>$idgrupoauladetalle
							,'nota'=>!empty($nota)?$nota:'null'
							,'nota_base'=>!empty($nota_base)?$nota_base:'null'
							,'iddocente'=>!empty($iddocente)?$iddocente:'null'
							,'fecha_calificacion'=>!empty($fecha_calificacion)?$fecha_calificacion:'null'
							,'titulo'=>!empty($titulo)?$titulo:'null'
							,'respuestas'=>!empty($respuestas)?$respuestas:'null'
							,'fecha_respuesta'=>!empty($fecha_respuesta)?$fecha_respuesta:'null'
							,'idcursodetalle'=>$idcursodetalle
							,'idpestania'=>!empty($idpestania)?$idpestania:'null'
							);
			$this->oBD->update('recursos_colaborativosasignacion ', $estados, array('idrecursoasignacion' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}

	public function actualizar2($estados,$condicion){
		try {
			return $this->oBD->update('recursos_colaborativosasignacion ', $estados, $condicion);		    
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}



	public function agregarasignacion($datos){
		try{

			$estados = array('idproyecto'=>$datos["idproyecto"]
							,'titulo'=>$datos["titulo"]
							,'descripcion'=>$datos["descripcion"]
							,'file'=>$datos["file"]
							,'tipo'=>$datos["tipo"]
							,'estado'=>$datos["estado"]
							,'idusuario'=>$datos["idusuario"]
							,'temasxgrupo'=>strtoupper(substr(!empty($datos["definirTema"])?$datos["definirTema"]:'N',0,1))
							,'ngrupos'=>$datos["ngrupos"]
							);
			if(!empty($datos["idrecurso"])) {
				$this->oBD->update('recursos_colaborativos ', $estados, array('idrecurso' => $datos["idrecurso"]));
				$idrecurso=$datos["idrecurso"];
			}else{
				$idrecurso=$this->oBD->insertAI('recursos_colaborativos', $estados,true);
			}

			$asignaciones=array();
			
			if(!empty($datos["idgrupoauladetalle"])){
			if(!empty($datos["grupos"]))
				foreach ($datos["grupos"] as $kg => $gru){
					if(!empty($gru["alumnos"]))
						foreach ($gru["alumnos"] as $ka => $alu){
							$estados = array('idrecurso'=>$idrecurso								
								,'idcursodetalle'=>$datos["idcursodetalle"]
								,'idgrupoauladetalle'=>$datos["idgrupoauladetalle"]
								,'idpestania'=>!empty($datos["idpestania"])?$datos["idpestania"]:'null'
								,'idalumno'=>$alu["idalumno"]
								,'grupo'=>$gru["nombre"]
								,'tema'=>!empty($gru["tema"])?$gru["tema"]:'null'								
								);
								if(empty($alu["idrecursoasignacion"])){
									$filtro00=array('idrecurso'=>$idrecurso,'idcursodetalle'=>$datos["idcursodetalle"],'idgrupoauladetalle'=>$datos["idgrupoauladetalle"],'idalumno'=>$alu["idalumno"]);
									if(!empty($datos["idpestania"])) $filtro00["idpestania"]=$datos["idpestania"];
									$hay=$this->buscar($filtro00);
									if(!empty($hay[0])) $alu["idrecursoasignacion"]=$hay[0]["idrecursoasignacion"];
								}
								
								if(!empty($alu["idrecursoasignacion"])){
									$this->oBD->update('recursos_colaborativosasignacion ', $estados, array('idrecursoasignacion' => $alu["idrecursoasignacion"]));
									$idrecursoasignacion=$alu["idrecursoasignacion"];								
								}else{
									$idrecursoasignacion=$this->oBD->insertAI('recursos_colaborativosasignacion ', $estados);								
								}
								$asignaciones[]=array('dni'=>$alu["idalumno"],'idalumno'=>$alu["idalumno"],'id'=>$idrecursoasignacion,'idrecursoasignacion'=>$idrecursoasignacion);

						}
				}
			}
			return array('idrecurso'=>$idrecurso,'asignaciones'=>$asignaciones);
		}catch(Exception $ex){
			throw new Exception("ERROR\n".JrTexto::_("Asignar")." ".JrTexto::_("Recursos").": " . $ex->getMessage());
		}
	}

	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('recursos_colaborativosasignacion', array('idrecursoasignacion' => $id));
			else 
				return $this->oBD->update('recursos_colaborativosasignacion', array('estado' => -1), array('idrecursoasignacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('recursos_colaborativosasignacion', array($propiedad => $valor), array('idrecursoasignacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Recursos_colaborativosasignacion").": " . $e->getMessage());
		}
	}  

		 
		
}