<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-11-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatBitacora_alumno_smartbook_se extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bitacora_alumno_smartbook_se").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM bitacora_alumno_smartbook_se";
			
			$cond = array();		
			
			if(isset($filtros["idbitacora_smartbook"])) {
					$cond[] = "idbitacora_smartbook = " . $this->oBD->escapar($filtros["idbitacora_smartbook"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			/*if(!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}*/
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bitacora_alumno_smartbook_se").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT BAS.idbitacora_smartbook,BAS.idcurso,BAS.idsesion,BAS.idusuario,BAS.estado,BAS.regfecha,BAS.idcursodetalle_sc,BAS.idcc,BAS.idproyecto,idgrupoauladetalle FROM bitacora_alumno_smartbook_se BAS";			
			
			$cond = array();		
					
			
			if(isset($filtros["idbitacora_smartbook"])) {
					$cond[] = "idbitacora_smartbook = " . $this->oBD->escapar($filtros["idbitacora_smartbook"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			/*if(!empty($filtros["idgrupoauladetalle"])) {
				$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}*/
			if(isset($filtros["idcurso_sc"])) {
				$cond[] = "BAS.idcurso_sc = " . $this->oBD->escapar($filtros["idcurso_sc"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "BAS.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idcc"])) {
				$cond[] = "BAS.idcc = " . $this->oBD->escapar($filtros["idcc"]);
			}
			if(isset($filtros["idcursodetalle_sc"])) {
				$cond[] = "BAS.idcursodetalle_sc = " . $this->oBD->escapar($filtros["idcursodetalle_sc"]);
			}

			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["idcomplementario"])) {
					$cond[] = "idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if(isset($filtros["texto"])) {
					//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["fechaDesde"]) && !empty($filtros["fechaDesde"])) {
				$cond[] = "regfecha >= " . $this->oBD->escapar($filtros["fechaDesde"]);
			}
			if(isset($filtros["fechaHasta"]) && !empty($filtros["fechaHasta"])) {
				$cond[] = "regfecha <= " . $this->oBD->escapar($filtros["fechaHasta"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$idsesion,$idusuario,$estado,$regfecha,$idcurso_sc,$idproyecto,$idcc=0,$idcursodetalle_sc,$idgrupoauladetalle=null)
	{
		try {
			
			//$this->iniciarTransaccion('dat_bitacora_alumno_smartbook_insert');			
			$hay=$this->buscar(array('idcurso_sc'=>$idcurso_sc,'idsesion'=>$idsesion,'idusuario'=>$idusuario,'idgrupoauladetalle'=>$idgrupoauladetalle));
			if(!empty($hay[0])) return $hay[0]["idbitacora_smartbook"];

			$id = $this->oBD->consultarEscalarSQL("SELECT idbitacora_smartbook FROM bitacora_alumno_smartbook_se ORDER BY idbitacora_smartbook DESC LIMIT 0,1");
			++$id;
			$estados = array('idbitacora_smartbook' => $id							
							,'idcurso'=>$idcurso
							,'idsesion'=>$idsesion
							,'idusuario'=>$idusuario
							,'estado'=>$estado
							,'regfecha'=>$regfecha
							,'idcurso_sc'=>$idcurso_sc
							,'idproyecto'=>$idproyecto
							,'idcc'=>$idcc
							,'idcursodetalle_sc'=>$idcursodetalle_sc						
							);
			if(!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"]=$idgrupoauladetalle;
			$this->oBD->insert('bitacora_alumno_smartbook_se', $estados);			
			//$this->terminarTransaccion('dat_bitacora_alumno_smartbook_insert');			
			return $id;

		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_bitacora_alumno_smartbook_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bitacora_alumno_smartbook_se").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$idsesion,$idusuario,$estado,$regfecha,$idcurso_sc,$idproyecto,$idcc=0,$idcursodetalle_sc, $idgrupoauladetalle=null)
	{
		try {
			//$this->iniciarTransaccion('dat_bitacora_alumno_smartbook_update');
			$estados = array('idcurso'=>$idcurso
							,'idsesion'=>$idsesion
							,'idusuario'=>$idusuario
							,'estado'=>$estado
							,'regfecha'=>$regfecha	
							,'idcurso_sc'=>$idcurso_sc
							,'idproyecto'=>$idproyecto
							,'idcc'=>$idcc
							,'idcursodetalle_sc'=>$idcursodetalle_sc							
							);
			if(!empty($idgrupoauladetalle)) $estados["idgrupoauladetalle"]=$idgrupoauladetalle;
			$this->oBD->update('bitacora_alumno_smartbook_se ', $estados, array('idbitacora_smartbook' => $id));
		   // $this->terminarTransaccion('dat_bitacora_alumno_smartbook_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bitacora_alumno_smartbook_se").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT BAS.idbitacora_smartbook,BAS.idcurso,BAS.idsesion,BAS.idusuario,BAS.estado,BAS.regfecha,BAS.idcursodetalle_sc,BAS.idcc,BAS.idproyecto,idgrupoauladetalle FROM bitacora_alumno_smartbook_se BAS "
					. " WHERE idbitacora_smartbook = " . $this->oBD->escapar($id);
			//echo $sql;
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function multidelete($ids){
		try{
			$this->iniciarTransaccion('dat_multiple_delete');
			$sql = "DELETE FROM bitacora_alumno_smartbook_se WHERE idbitacora_smartbook IN (".implode(',', $ids).")";
			$this->oBD->ejecutarSQL($sql);
			$this->terminarTransaccion('dat_multiple_delete');
			return true;
		}catch(Exception $e) {
			$this->cancelarTransaccion('dat_multiple_delete');
			throw new Exception("ERROR\n".JrTexto::_("Multidelete")." ".JrTexto::_("Bitacora_smartbook_se").": " . $e->getMessage());
		}
	}
	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bitacora_alumno_smartbook_se', array('idbitacora_smartbook' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bitacora_alumno_smartbook_se").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bitacora_alumno_smartbook_se', array($propiedad => $valor), array('idbitacora_smartbook' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bitacora_alumno_smartbook_se").": " . $e->getMessage());
		}
	}
   
		
}