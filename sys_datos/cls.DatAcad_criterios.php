<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-02-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatAcad_criterios extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_criterios").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT cr.idcriterio,cr.idcapacidad,cr.idcurso,cr.nombre,cr.estado,cr.idproyecto,(select ca.nombre FROM acad_capacidades ca where ca.idcapacidad = cr.idcapacidad) as capacidad,(select cu.nombre FROM acad_curso cu where cu.idcurso = cr.idcurso) as curso FROM acad_criterios cr";			
			
			$cond = array();

			if(!empty($filtros["idcriterio"])){
				if(is_array($filtros["idcriterio"])){
					$cond[]="cr.idcriterio IN ('".implode("','",$filtros["idcriterio"])."')";
				}else	
				$cond[] = "cr.idcriterio = " . $this->oBD->escapar($filtros["idcriterio"]);
			}

			if(!empty($filtros["idcurso"])){
				if(is_array($filtros["idcurso"])){
					$cond[]="cr.idcurso IN ('".implode("','",$filtros["idcurso"])."')";
				}else	
				$cond[] = "cr.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			
			if(isset($filtros["idcapacidad"])) {
					$cond[] = "cr.idcapacidad = " . $this->oBD->escapar($filtros["idcapacidad"]);
			}
			/*if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}*/
			if(isset($filtros["nombre"])) {
					$cond[] = "cr.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "cr.estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(isset($filtros["texto"])) {
					$cond[] = "cr.nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "cr.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_criterios").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcapacidad,$idcurso,$nombre,$estado,$idproyecto)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_criterios_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT idcriterio FROM acad_criterios ORDER BY idcriterio DESC limit 0,1");
			++$id;
			
			$estados = array('idcriterio' => $id
							
							,'idcapacidad'=>$idcapacidad
							,'idcurso'=>$idcurso
							,'nombre'=>$nombre
							,'estado'=>$estado	
							,'idproyecto'=>$idproyecto						
							);
			
			$this->oBD->insert('acad_criterios', $estados);			
			$this->terminarTransaccion('dat_acad_criterios_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_criterios_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_criterios").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcapacidad,$idcurso,$nombre,$estado,$idproyecto)
	{
		try {
			$this->iniciarTransaccion('dat_acad_criterios_update');
			$estados = array('idcapacidad'=>$idcapacidad
							,'idcurso'=>$idcurso
							,'nombre'=>$nombre
							,'estado'=>$estado		
							,'idproyecto'=>$idproyecto						
							);
			
			$this->oBD->update('acad_criterios ', $estados, array('idcriterio' => $id));
		    $this->terminarTransaccion('dat_acad_criterios_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_criterios").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idcriterio,idcapacidad,idcurso,nombre,estado,idproyecto FROM acad_criterios"
					. " WHERE idcriterio = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_criterios").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_criterios', array('idcriterio' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_criterios").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_criterios', array($propiedad => $valor), array('idcriterio' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_criterios").": " . $e->getMessage());
		}
	}
   
		
}