<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-12-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatProyecto_cursos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Proyecto_cursos") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM proyecto_cursos";

			$cond = array();

			if (isset($filtros["idproycurso"])) {
				$cond[] = "idproycurso = " . $this->oBD->escapar($filtros["idproycurso"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Proyecto_cursos") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT py.idproycurso,py.idproyecto,py.configuracion_nota,py.orden, ac.idcurso, ac.nombre, ac.imagen, ac.descripcion, ac.estado, ac.fecharegistro, ac.idusuario, ac.vinculosaprendizajes, ac.materialesyrecursos, ac.color, ac.objetivos, ac.certificacion, ac.costo, ac.silabo, ac.e_ini, ac.pdf, ac.abreviado, ac.e_fin, ac.aniopublicacion, ac.autor, ac.txtjson, ac.formula_evaluacion, ac.tipo, ac.sks, ac.idioma, ac.avance_secuencial
					FROM proyecto_cursos py INNER JOIN acad_curso ac ON py.idcurso=ac.idcurso";
			$cond = array();
			if (isset($filtros["idproycurso"])) {
				$cond[] = "idproycurso = " . $this->oBD->escapar($filtros["idproycurso"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "py.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY py.idcurso ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Proyecto_cursos") . ": " . $e->getMessage());
		}
	}


	public function insertar($idcurso, $idproyecto)
	{
		try {

			$this->iniciarTransaccion('dat_proyecto_cursos_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idproycurso) FROM proyecto_cursos");
			++$id;

			$estados = array(
				'idproycurso' => $id, 'idcurso' => $idcurso, 'idproyecto' => $idproyecto
			);

			$this->oBD->insert('proyecto_cursos', $estados);
			$this->terminarTransaccion('dat_proyecto_cursos_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_proyecto_cursos_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Proyecto_cursos") . ": " . $e->getMessage());
		}
	}
	/**
	 * Funcion generica que inserta 1 o mas registro en la tabla acceso, esta funcion se utiliza en varias funciones para insertar en la table, por favor tener precaucion al editar la funcion
	 * @param Array Registros a guardar en la tabla
	 * @return Integer Ultimo id guardado en la tabla
	 */
	public function insert($data){
		try{
			$this->iniciarTransaccion('dat_proyecto_cursos_insert');

			if(empty($data)){ throw new Exception("Data esta vacio"); }
			foreach($data as $row){
				$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idproycurso) FROM proyecto_cursos");
				++$id;
				$row['idproycurso'] = $id;
				$this->oBD->insert('proyecto_cursos', $row);
			}
			$this->terminarTransaccion('dat_proyecto_cursos_insert');			
			return $id;
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_proyecto_cursos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("proyecto_cursos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso, $idproyecto)
	{
		try {
			$this->iniciarTransaccion('dat_proyecto_cursos_update');
			$estados = array(
				'idcurso' => $idcurso, 'idproyecto' => $idproyecto
			);

			$this->oBD->update('proyecto_cursos ', $estados, array('idproycurso' => $id));
			$this->terminarTransaccion('dat_proyecto_cursos_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Proyecto_cursos") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  proyecto_cursos.idproycurso, proyecto_cursos.idcurso, proyecto_cursos.idproyecto, proyecto_cursos.configuracion_nota, proyecto_cursos.orden  FROM proyecto_cursos  "
				. " WHERE idproycurso = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Proyecto_cursos") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('proyecto_cursos', array('idproycurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Proyecto_cursos") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('proyecto_cursos', array($propiedad => $valor), array('idproycurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Proyecto_cursos") . ": " . $e->getMessage());
		}
	}
}
