<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		06-02-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatTest_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Test_alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM test_alumno";
			
			$cond = array();		
			
			if(isset($filtros["idtestalumno"])) {
					$cond[] = "idtestalumno = " . $this->oBD->escapar($filtros["idtestalumno"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["idtest"])) {
					$cond[] = "idtest = " . $this->oBD->escapar($filtros["idtest"]);
			}
			if(isset($filtros["idtestcriterio"])) {
					$cond[] = "idtestcriterio = " . $this->oBD->escapar($filtros["idtestcriterio"]);
			}
			if(isset($filtros["idtestasigancion"])) {
					$cond[] = "idtestasigancion = " . $this->oBD->escapar($filtros["idtestasigancion"]);
			}
			if(isset($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Test_alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM test_alumno";			
			
			$cond = array();		
					
			
			if(isset($filtros["idtestalumno"])) {
					$cond[] = "idtestalumno = " . $this->oBD->escapar($filtros["idtestalumno"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["idtest"])) {
					$cond[] = "idtest = " . $this->oBD->escapar($filtros["idtest"]);
			}
			if(isset($filtros["idtestcriterio"])) {
					$cond[] = "idtestcriterio = " . $this->oBD->escapar($filtros["idtestcriterio"]);
			}
			if(isset($filtros["idtestasigancion"])) {
					$cond[] = "idtestasigancion = " . $this->oBD->escapar($filtros["idtestasigancion"]);
			}
			if(isset($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Test_alumno").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idalumno,$idtest,$idtestcriterio,$idtestasigancion,$puntaje)
	{
		try {
			
			$this->iniciarTransaccion('dat_test_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtestalumno) FROM test_alumno");
			++$id;
			
			$estados = array('idtestalumno' => $id
							
							,'idalumno'=>$idalumno
							,'idtest'=>$idtest
							,'idtestcriterio'=>$idtestcriterio
							,'idtestasigancion'=>$idtestasigancion
							,'puntaje'=>$puntaje							
							);
			
			$this->oBD->insert('test_alumno', $estados);			
			$this->terminarTransaccion('dat_test_alumno_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_test_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Test_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idalumno,$idtest,$idtestcriterio,$idtestasigancion,$puntaje)
	{
		try {
			$this->iniciarTransaccion('dat_test_alumno_update');
			$estados = array('idalumno'=>$idalumno
							,'idtest'=>$idtest
							,'idtestcriterio'=>$idtestcriterio
							,'idtestasigancion'=>$idtestasigancion
							,'puntaje'=>$puntaje								
							);
			
			$this->oBD->update('test_alumno ', $estados, array('idtestalumno' => $id));
		    $this->terminarTransaccion('dat_test_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Test_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM test_alumno  "
					. " WHERE idtestalumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Test_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('test_alumno', array('idtestalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Test_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('test_alumno', array($propiedad => $valor), array('idtestalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Test_alumno").": " . $e->getMessage());
		}
	}
   
		
}