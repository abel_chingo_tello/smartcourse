<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		29-01-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatTest extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Test").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM test";
			
			$cond = array();		
			
			if(isset($filtros["idtest"])) {
					$cond[] = "idtest = " . $this->oBD->escapar($filtros["idtest"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Test").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM test";			
			
			$cond = array();		
					
			
			if(isset($filtros["idtest"])) {
					$cond[] = "idtest = " . $this->oBD->escapar($filtros["idtest"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Test").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($titulo,$puntaje,$mostrar,$imagen)
	{
		try {
			
			$this->iniciarTransaccion('dat_test_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtest) FROM test");
			++$id;
			
			$estados = array('idtest' => $id
							
							,'titulo'=>$titulo
							,'puntaje'=>$puntaje
							,'mostrar'=>$mostrar
							,'imagen'=>$imagen							
							);
			
			$this->oBD->insert('test', $estados);			
			$this->terminarTransaccion('dat_test_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_test_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Test").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $titulo,$puntaje,$mostrar,$imagen)
	{
		try {
			$this->iniciarTransaccion('dat_test_update');
			$estados = array('titulo'=>$titulo
							,'puntaje'=>$puntaje
							,'mostrar'=>$mostrar
							,'imagen'=>$imagen								
							);
			
			$this->oBD->update('test ', $estados, array('idtest' => $id));
		    $this->terminarTransaccion('dat_test_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Test").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM test  "
					. " WHERE idtest = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Test").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('test', array('idtest' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Test").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('test', array($propiedad => $valor), array('idtest' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Test").": " . $e->getMessage());
		}
	}
   
		
}