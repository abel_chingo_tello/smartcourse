<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */

class DatVimeo extends DatBase
{
	public function __construct()
	{

		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("tmp_vimeo_log") . ": " . $e->getMessage());
		}
	}
	public function listarCursosInglesConStaticPath()
	{
		try {
			$sql = "
			select CURSOS_ELIMINAR.*, tmp_vimeo_log.idlog, tmp_vimeo_log.static_path, tmp_vimeo_log.estado, tmp_vimeo_log.fecha_registro
			from tmp_vimeo_log
				inner join (
					SELECT * FROM (
						select 	tmp_curso_procesado.idcurso, tmp_curso_procesado.idcomplementario, 
								acad_curso.nombre, tmp_curso_procesado.fecha_registro,
								tmp_curso_procesado.videos_procesados 
						from tmp_curso_procesado
							inner join acad_curso 
							on tmp_curso_procesado.idcurso=acad_curso.idcurso 
							and tmp_curso_procesado.idcomplementario=0
						UNION
						select 	tmp_curso_procesado.idcurso, tmp_curso_procesado.idcomplementario,
								acad_curso_complementario.nombre, tmp_curso_procesado.fecha_registro,
								tmp_curso_procesado.videos_procesados 
						from tmp_curso_procesado
							inner join acad_curso_complementario 
								on tmp_curso_procesado.idcomplementario=acad_curso_complementario.idcurso 
								and tmp_curso_procesado.idcurso=acad_curso_complementario.idcursoprincipal
					) CURSOS_ELIMINAR
					WHERE 	nombre LIKE '%A1%' OR 
							nombre LIKE '%A2%' OR
							nombre LIKE '%B1%' OR
							nombre LIKE '%B2%' OR
							nombre LIKE '%C1%'
					ORDER BY nombre ASC
				) CURSOS_ELIMINAR
					on tmp_vimeo_log.idcurso=CURSOS_ELIMINAR.idcurso 
					and tmp_vimeo_log.idcomplementario=CURSOS_ELIMINAR.idcomplementario
			order by idcurso desc";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("tmp_vimeo_log") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT idlog, static_path,vimeo_uri,fecha_registro, estado
					FROM tmp_vimeo_log ";

			$cond = array();


			if (isset($filtros["idlog"])) {
				$cond[] = "idlog = " . $this->oBD->escapar($filtros["idlog"]);
			}
			if (isset($filtros["static_path"])) {
				$cond[] = "static_path = " . $this->oBD->escapar($filtros["static_path"]);
			}
			if (isset($filtros["vimeo_uri"])) {
				$cond[] = "vimeo_uri = " . $this->oBD->escapar($filtros["vimeo_uri"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["fecha_registro"])) {
				$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("tmp_vimeo_log") . ": " . $e->getMessage());
		}
	}
	public function listarCursosEmpresa($filtros)
	{
		try {
			$sql = "
				(select proyecto_cursos.idproyecto, curso.idcursoprincipal idcurso, curso.idcurso idcomplementario,curso.nombre
				from proyecto_cursos
					inner join acad_curso on proyecto_cursos.idcurso=acad_curso.idcurso
					inner join acad_curso_complementario curso on curso.idcursoprincipal=acad_curso.idcurso
						and curso.idcurso!=0
				where proyecto_cursos.idproyecto=" . $this->oBD->escapar($filtros["idproyecto"]) . ")
				union
				(select proyecto_cursos.idproyecto, curso.idcurso,0 idcomplementario,curso.nombre
				from proyecto_cursos
					inner join acad_curso curso on proyecto_cursos.idcurso=curso.idcurso
				where proyecto_cursos.idproyecto=" . $this->oBD->escapar($filtros["idproyecto"]) . ")
				";

			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("dat") . " " . JrTexto::_("listarCursosEmpresa") . ": " . $e->getMessage());
		}
	}
	public function listarCursosEmpresaIngles($filtros)
	{
		try {
			$sql = "
			SELECT * FROM (
				(select proyecto_cursos.idproyecto, curso.idcursoprincipal idcurso, curso.idcurso idcomplementario,curso.nombre
				from proyecto_cursos
					inner join acad_curso on proyecto_cursos.idcurso=acad_curso.idcurso
					inner join acad_curso_complementario curso on curso.idcursoprincipal=acad_curso.idcurso
						and curso.idcurso!=0
				where proyecto_cursos.idproyecto=" . $this->oBD->escapar($filtros["idproyecto"]) . ")
				union
				(select proyecto_cursos.idproyecto, curso.idcurso,0 idcomplementario,curso.nombre
				from proyecto_cursos
					inner join acad_curso curso on proyecto_cursos.idcurso=curso.idcurso
				where proyecto_cursos.idproyecto=" . $this->oBD->escapar($filtros["idproyecto"]) . ") 
			) V
			WHERE 
				nombre LIKE '%A1%' OR 
				nombre LIKE '%A2%' OR
				nombre LIKE '%B1%' OR
				nombre LIKE '%B2%' OR
				nombre LIKE '%C1%'
				";

			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("dat") . " " . JrTexto::_("listarCursosEmpresaIngles") . ": " . $e->getMessage());
		}
	}
	public function getNombreCurso($filtros)
	{
		try {
			$sql = "";
			if (isset($filtros["idcomplementario"]) && !empty($filtros["idcomplementario"])) {
				#es complementario
				$sql = "SELECT 	curso.nombre, curso.idcurso idcomplementario, curso.idcursoprincipal idcurso, curso.idcategoria 
						FROM acad_curso_complementario curso ";
			} else {
				$sql = "SELECT 	curso.nombre, curso.idcurso, 0 idcomplementario 
						FROM acad_curso curso ";
			}
			if (isset($filtros["idcomplementario"]) && !empty($filtros["idcomplementario"])) {
				#es complementario
				if (isset($filtros["idcurso"])) {
					$cond[] = "idcursoprincipal = " . $this->oBD->escapar($filtros["idcurso"]);
				} else {
					throw new Exception("Datos incompletos");
				}
				if (isset($filtros["idcomplementario"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcomplementario"]);
				}
			} else {
				if (isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("dat") . " " . JrTexto::_("getNombreCurso") . ": " . $e->getMessage());
		}
	}
	public function insertar($params)
	{
		try {

			$this->iniciarTransaccion('tmp_vimeo_log');
			$estados = array(
				'static_path' => $params["static_path"],
				'vimeo_uri' => $params["vimeo_uri"],
				'estado' => $params["estado"],
				'idcurso' => $params["idcurso"],
				'idcomplementario' => $params["idcomplementario"],
			);

			$this->oBD->insert('tmp_vimeo_log', $estados);
			$this->terminarTransaccion('tmp_vimeo_log');
			return "ok";
		} catch (Exception $e) {
			$this->cancelarTransaccion('tmp_vimeo_log');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("tmp_vimeo_log") . ": " . $e->getMessage());
		}
	}
	public function insertarCursoProcesado($params)
	{
		try {

			$this->iniciarTransaccion('tmp_curso_procesado');
			$estados = array(
				'idcurso' => $params["idcurso"],
				'idcomplementario' => $params["idcomplementario"],
				'estado' => $params["estado"],
				'videos_procesados' => $params["videos_procesados"],
			);
			$this->oBD->insert('tmp_curso_procesado', $estados);
			$this->terminarTransaccion('tmp_curso_procesado');
			return "ok";
		} catch (Exception $e) {
			$this->cancelarTransaccion('tmp_curso_procesado');
			if (strpos($e->getMessage(), "El registro ya existe") !== false) {
				return "El registro ya existe";
			} else {
				throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("tmp_curso_procesado") . ": " . $e->getMessage());
			}
		}
	}
	public function listarCursoProcesado($filtros = null)
	{
		try {
			$sql = "SELECT idcurso_procesado, idcurso,idcomplementario,estado, fecha_registro,videos_procesados
					FROM tmp_curso_procesado ";

			$cond = array();


			if (isset($filtros["idcurso_procesado"])) {
				$cond[] = "idcurso_procesado = " . $this->oBD->escapar($filtros["idcurso_procesado"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcomplementario"])) {
				$cond[] = "idcomplementario = " . $this->oBD->escapar($filtros["idcomplementario"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["fecha_registro"])) {
				$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("tmp_curso_procesado") . ": " . $e->getMessage());
		}
	}
	public function set_tmp_vimeo_log($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('tmp_vimeo_log', array($propiedad => $valor), array('idlog' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("tmp_vimeo_log") . ": " . $e->getMessage());
		}
	}
}
