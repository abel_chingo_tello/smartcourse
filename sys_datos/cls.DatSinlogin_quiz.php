<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-01-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatSinlogin_quiz extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Sinlogin_quiz").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT `idnota`,`idexamen`,q.idpersona,p.nombres,p.telefono,p.correo,p.compania,p.ciudad,p.pais,`tipo`,`nota`,`notatexto`,`regfecha`,q.idproyecto,`calificacion_en`,`calificacion_total`,`calificacion_min`,`tiempo_total`,`tiempo_realizado`,`calificacion`,`habilidades`,`habilidad_puntaje`,`intento`,`datos`,`preguntas` FROM sinlogin_quiz q INNER JOIN sinlogin_persona p  ON q.idpersona=p.idpersona ";			
			
			$cond = array();		
					
			
			if(isset($filtros["idnota"])) {
					$cond[] = "idnota = " . $this->oBD->escapar($filtros["idnota"]);
			}
			if(isset($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "q.idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["nota"])) {
					$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if(isset($filtros["notatexto"])) {
					$cond[] = "notatexto = " . $this->oBD->escapar($filtros["notatexto"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "q.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["calificacion_en"])) {
					$cond[] = "calificacion_en = " . $this->oBD->escapar($filtros["calificacion_en"]);
			}
			if(isset($filtros["calificacion_total"])) {
					$cond[] = "calificacion_total = " . $this->oBD->escapar($filtros["calificacion_total"]);
			}
			if(isset($filtros["calificacion_min"])) {
					$cond[] = "calificacion_min = " . $this->oBD->escapar($filtros["calificacion_min"]);
			}
			if(isset($filtros["tiempo_total"])) {
					$cond[] = "tiempo_total = " . $this->oBD->escapar($filtros["tiempo_total"]);
			}
			if(isset($filtros["tiempo_realizado"])) {
					$cond[] = "tiempo_realizado = " . $this->oBD->escapar($filtros["tiempo_realizado"]);
			}
			if(isset($filtros["calificacion"])) {
					$cond[] = "calificacion = " . $this->oBD->escapar($filtros["calificacion"]);
			}
			if(isset($filtros["habilidades"])) {
					$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}
			if(isset($filtros["habilidad_puntaje"])) {
					$cond[] = "habilidad_puntaje = " . $this->oBD->escapar($filtros["habilidad_puntaje"]);
			}
			if(isset($filtros["intento"])) {
					$cond[] = "intento = " . $this->oBD->escapar($filtros["intento"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else $cond[] = "estado >= 0" ;
			if(isset($filtros["datos"])) {
					$cond[] = "datos = " . $this->oBD->escapar($filtros["datos"]);
			}
			if(isset($filtros["fechamodificacion"])) {
					$cond[] = "fechamodificacion = " . $this->oBD->escapar($filtros["fechamodificacion"]);
			}
			if(isset($filtros["preguntas"])) {
					$cond[] = "preguntas = " . $this->oBD->escapar($filtros["preguntas"]);
			}			
			if(isset($filtros["texto"])) {
					$cond[] = "nombres " . $this->oBD->like($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Sinlogin_quiz").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idpersona,$idexamen,$tipo,$nota,$notatexto,$idproyecto,$calificacion_en,$calificacion_total,$calificacion_min,$tiempo_total,$tiempo_realizado,$calificacion,$habilidades,$habilidad_puntaje,$intento,$datos,$preguntas)
	{
		try {			
			//$this->iniciarTransaccion('dat_sinlogin_quiz_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM sinlogin_quiz");
			++$id;
			
			$estados = array('idpersona'=>$idpersona
							,'idexamen'=>$idexamen
							,'tipo'=>$tipo
							,'nota'=>$nota
							,'notatexto'=>$notatexto							
							,'idproyecto'=>$idproyecto
							,'calificacion_en'=>$calificacion_en
							,'calificacion_total'=>$calificacion_total
							,'calificacion_min'=>$calificacion_min
							,'tiempo_total'=>$tiempo_total
							,'tiempo_realizado'=>$tiempo_realizado
							,'calificacion'=>$calificacion
							,'habilidades'=>$habilidades
							,'habilidad_puntaje'=>$habilidad_puntaje
							,'intento'=>$intento
							,'datos'=>$datos
							,'preguntas'=>$preguntas							
							);
			
			return $this->oBD->insertAI('sinlogin_quiz', $estados,true);			
			//$this->terminarTransaccion('dat_sinlogin_quiz_insert');			
			

		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_sinlogin_quiz_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Sinlogin_quiz").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idexamen,$tipo,$nota,$notatexto,$idproyecto,$calificacion_en,$calificacion_total,$calificacion_min,$tiempo_total,$tiempo_realizado,$calificacion,$habilidades,$habilidad_puntaje,$intento,$datos,$preguntas)
	{
		try {
			//$this->iniciarTransaccion('dat_sinlogin_quiz_update');
			$estados = array(
							'idexamen'=>$idexamen
							,'tipo'=>$tipo
							,'nota'=>$nota
							,'notatexto'=>$notatexto							
							,'idproyecto'=>$idproyecto
							,'calificacion_en'=>$calificacion_en
							,'calificacion_total'=>$calificacion_total
							,'calificacion_min'=>$calificacion_min
							,'tiempo_total'=>$tiempo_total
							,'tiempo_realizado'=>$tiempo_realizado
							,'calificacion'=>$calificacion
							,'habilidades'=>$habilidades
							,'habilidad_puntaje'=>$habilidad_puntaje
							,'intento'=>$intento
							,'datos'=>$datos
							,'preguntas'=>$preguntas								
							);
			
			$this->oBD->update('sinlogin_quiz ', $estados, array('idnota' => $id));
		   // $this->terminarTransaccion('dat_sinlogin_quiz_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sinlogin_quiz").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT `idnota`,`idexamen`,q.idpersona,p.nombres,p.telefono,p.correo,`tipo`,`nota`,`notatexto`,`regfecha`,q.idproyecto,`calificacion_en`,`calificacion_total`,`calificacion_min`,`tiempo_total`,`tiempo_realizado`,`calificacion`,`habilidades`,`habilidad_puntaje`,`intento`,`datos`,`preguntas` FROM sinlogin_quiz q INNER JOIN sinlogin_persona p  ON q.idpersona=p.idpersona  WHERE q.idnota = " . $this->oBD->escapar($id);			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Sin login_quiz").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('sinlogin_quiz', array('idnota' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Sinlogin_quiz").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('sinlogin_quiz', array($propiedad => $valor), array('idnota' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sinlogin_quiz").": " . $e->getMessage());
		}
	}	
}