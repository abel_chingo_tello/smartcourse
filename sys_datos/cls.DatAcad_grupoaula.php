<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		14-10-2019  
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */
class DatAcad_grupoaula extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM acad_grupoaula";

			$cond = array();

			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["comentario"])) {
				$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if (isset($filtros["nvacantes"])) {
				$cond[] = "nvacantes = " . $this->oBD->escapar($filtros["nvacantes"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if (isset($filtros["idgrado"])) {
				$cond[] = "ag.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "ag.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT ag.idgrupoaula,ag.nombre,ag.tipo,ag.comentario,ag.nvacantes,ag.estado,ag.idproyecto,ag.oldid,ag.idgrado,ag.idsesion,ag.idcarrera,ag.idcategoria,ag.idlocal,ag.fecha_inicio,ag.fecha_fin,ag.idproducto,ag.tipodecurso,ag.tipogrupo,ag.codaccionsence,(SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, (SELECT nombre from general g WHERE ag.tipo=g.codigo AND tipo_tabla='tipogrupo') as strtipo FROM acad_grupoaula ag";

			$cond = array();

			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "ag.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}

			if (isset($filtros["nombre"])) {
				$cond[] = "ag.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "ag.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["comentario"])) {
				$cond[] = "ag.comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if (isset($filtros["nvacantes"])) {
				$cond[] = "ag.nvacantes = " . $this->oBD->escapar($filtros["nvacantes"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "ag.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["oldid"])) {
				$cond[] = "ag.oldid = " . $this->oBD->escapar($filtros["oldid"]);
			}
			if (isset($filtros["texto"])) {
				$cond[] = "ag.nombre " . $this->oBD->like($filtros["texto"]);
			}
			if (isset($filtros["idgrado"])) {
				$cond[] = "ag.idgrado = " . $this->oBD->escapar($filtros["idgrado"]);
			}
			if (isset($filtros["idsesion"])) {
				$cond[] = "ag.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if (isset($filtros["idcategoria"])) {
				$cond[] = "ag.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if (isset($filtros["idlocal"])) {
				$cond[] = "ag.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if (isset($filtros["idproducto"])) {
				$cond[] = "ag.idproducto = " . $this->oBD->escapar($filtros["idproducto"]);
			}
			if (isset($filtros['fechavencida'])) {
				$fecha_actual = date('Y-m-d');
				$cond[] = "ag.fecha_fin <= " . $this->oBD->escapar($fecha_actual);
			}
			if (isset($filtros['fechaactiva'])) {
				$fecha_actual = date('Y-m-d');
				// $cond[] = "ag.fecha_inicio <= ".$this->oBD->escapar($fecha_actual);
				$cond[] = "ag.fecha_fin >= " . $this->oBD->escapar($fecha_actual);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY idgrupoaula DESC, nombre ASC";
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}
	public function getGrupoAulaRol($filtros = null)
	{
		try {
			$sql = "SELECT 	acad_grupoaula.idgrupoaula, acad_grupoaula.nombre nombre_grupo
					FROM acad_grupoaula
						LEFT JOIN acad_grupoauladetalle ON acad_grupoaula.idgrupoaula=acad_grupoauladetalle.idgrupoaula
				";
			$cond = array();
			if (isset($filtros["iddocente"])) {
				$cond[] = "acad_grupoauladetalle.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "acad_grupoaula.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			$cond[] = "acad_grupoaula.estado = 1";
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= "	GROUP BY acad_grupoaula.idgrupoaula
						ORDER BY acad_grupoaula.nombre  ASC";
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("getGrupoAulaRol") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}
	public function getAlumnos($filtros = null)
	{
		try {
			$sql = "SELECT 	personal.idpersona,
							CONCAT(personal.ape_paterno ,' ', personal.ape_materno,', ', personal.nombre) nombre_full,personal.foto
					FROM acad_grupoaula
						INNER JOIN acad_grupoauladetalle ON acad_grupoaula.idgrupoaula=acad_grupoauladetalle.idgrupoaula
						INNER JOIN acad_matricula ON acad_matricula.idgrupoauladetalle=acad_grupoauladetalle.idgrupoauladetalle
						INNER JOIN personal ON personal.idpersona=acad_matricula.idalumno
				";
			$cond = array();
			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "acad_grupoaula.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "acad_grupoauladetalle.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "acad_grupoaula.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			$cond[] = "acad_grupoaula.estado = 1";
			$cond[] = "acad_matricula.estado = 1";
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= "	GROUP BY personal.idpersona
						ORDER BY personal.ape_paterno  ASC";
			// echo $sql;exit();
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("getGrupoAulaAlumno") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}
	public function vacantes($idgrupoaula)
	{
		try {
			$sql = "SELECT DISTINCT(ga.idgrupoaula), (SELECT count(DISTINCT(m.idalumno)) FROM acad_matricula m inner join acad_grupoauladetalle gd on (gd.idgrupoauladetalle=m.idgrupoauladetalle) where gd.idgrupoaula = " . $idgrupoaula . ") as ocupadas from acad_grupoaula ga inner join acad_grupoauladetalle gad on (ga.idgrupoaula=gad.idgrupoaula) where ga.idgrupoaula = " . $idgrupoaula;
			// echo $sql;
			$res1 = $this->oBD->consultarSQL($sql);
			$sql = "SELECT nvacantes as vacantes from acad_grupoaula where idgrupoaula = " . $idgrupoaula;
			// echo $sql;
			$res2 = $this->oBD->consultarSQL($sql);
			$vacantes = $res2[0]["vacantes"];
			if (count($res1) === 0) {
				$ocupadas = 0;
			} else {
				$ocupadas = $res1[0]["ocupadas"];
			}
			return array("vacantes" => $vacantes, "ocupadas" => $ocupadas);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function buscargrupos($filtros = null)
	{
		try {
			$sql = "SELECT ag.idgrupoaula,ag.nombre as strgrupoaula, comentario,nvacantes, idgrupoauladetalle, agd.nombre ,agd.fecha_inicio,agd.fecha_final, ag.tipo, (SELECT cu.tipo FROM acad_curso cu WHERE cu.idcurso = agd.idcurso) as tipocurso, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, ag.estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, agd.idcurso, (SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , agd.iddocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE agd.iddocente=dni limit 1) AS strdocente, agd.idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente, ag.fecha_inicio AS fecha_inicio_grupoaula, ag.fecha_fin AS fecha_fin_grupoaula,ag.idcategoria AS categoria_grupoaula,(SELECT idpadre FROM acad_categorias WHERE idcategoria = ag.idcategoria LIMIT 1) AS categoriapadre_grupoaula FROM acad_grupoaula ag LEFT JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula ";
			$cond = array();
			if (isset($filtros["mysql1"])) {
				$sql = "select distinct idgrupoaula,strgrupoaula, iddocente, idproyecto from (
								SELECT  ag.idproyecto, ag.idgrupoaula,ag.nombre as strgrupoaula, comentario,nvacantes, idgrupoauladetalle, agd.nombre ,agd.fecha_inicio,agd.fecha_final, ag.tipo, 
									(SELECT cu.tipo FROM acad_curso cu WHERE cu.idcurso = agd.idcurso) as tipocurso, 
									(SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, ag.estado, 
									(SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, agd.idcurso, 
									(SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , agd.iddocente, 
									(SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE agd.iddocente=dni limit 1) AS strdocente, 
									agd.idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, 
									(SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente, 
									ag.fecha_inicio AS fecha_inicio_grupoaula, ag.fecha_fin AS fecha_fin_grupoaula,ag.idcategoria AS categoria_grupoaula,
									(SELECT idpadre FROM acad_categorias WHERE idcategoria = ag.idcategoria LIMIT 1) AS categoriapadre_grupoaula 
								FROM acad_grupoaula ag 
									LEFT JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula 
						) view";
				if (isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
				}
				if (isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
				}
			}
			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "ag.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if (isset($filtros['idgrupoauladetalle'])) {
				if (is_array($filtros["idgrupoauladetalle"])) {
					$cond[] = "agd.idgrupoauladetalle IN (" . implode(",", $filtros["idgrupoauladetalle"]) . ")";
				} else {
					$cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
				}
			}
			if (isset($filtros["texto"])) {
				$cond[] = "ag.nombre " . $this->oBD->Like($filtros["texto"]);
				$cond[] = "ag.comentario " . $this->oBD->Like($filtros["texto"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "ag.nombre " . $this->oBD->Like($filtros["nombre"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "ag.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["idlocal"])) {
				$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if (isset($filtros["idlocal_grupo"])) {
				$cond[] = "ag.idlocal = " . $this->oBD->escapar($filtros["idlocal_grupo"]);
			}
			if (isset($filtros["idambiente"])) {
				$cond[] = "agd.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "ag.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["idcurso"])) {
				if (is_array($filtros["idcurso"])) {
					$cond[] = "agd.idcurso IN (" . implode(",", $filtros["idcurso"]) . ")";
				} else {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
			}
			if (isset($filtros["idcategoria"])) {
				if (is_array($filtros["idcategoria"])) {
					$cond[] = "ag.idcategoria IN (" . implode(",", $filtros["idcategoria"]) . ")";
				} else {
					$cond[] = "ag.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
				}
			}

			if (isset($filtros["idproyecto"]) && !isset($filtros["mysql1"])) {
				$cond[] = "ag.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["idproducto"])) {
				$cond[] = "ag.idproducto = " . $this->oBD->escapar($filtros["idproducto"]);
			}
			if (isset($filtros['fechaactiva'])) {
				$fecha_actual = date('Y-m-d');
				// $cond[] = "ag.fecha_inicio <= ".$this->oBD->escapar($fecha_actual);
				$cond[] = "ag.fecha_fin >= " . $this->oBD->escapar($fecha_actual);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (isset($filtros["groupby"])) {
				$sql .= " GROUP BY ag." . $filtros["groupby"];
			}
			if (isset($filtros["mysql1"])) {
				$sql .= " ORDER BY strgrupoaula ASC";
			}
			// echo $sql;exit();

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function gruposxmatricula($filtros = [])
	{
		try {
			$cond = array();

			$sql = "SELECT ag.idgrupoaula,ag.nombre, agd.idgrupoauladetalle,agd.idcomplementario, agd.nombre AS nombre_detalle,m.idmatricula,m.idalumno,m.idcategoria AS idnivel,categoria.idpadre AS idcarrera, categoria.nombre AS nivel, (SELECT nombre FROM acad_categorias WHERE idcategoria = categoria.idpadre) AS carrera FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula = agd.idgrupoaula INNER JOIN acad_matricula m ON agd.idgrupoauladetalle = m.idgrupoauladetalle LEFT JOIN acad_categorias categoria ON categoria.idcategoria = ag.idcategoria";

			if (isset($filtros["idmatricula"])) {
				if (is_array($filtros["idmatricula"])) {
					$cond[] = "m.idmatricula IN (" . implode(',', $filtros["idmatricula"]) . ")";
				} else {
					$cond[] = "m.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
				}
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "m.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "m.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if (isset($filtros["idgrupoaula"])) {
				$cond[] = "ag.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if (isset($filtros["idnivel"])) {
				$cond[] = "categoria.idcategoria = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if (isset($filtros["idcarrera"])) {
				$cond[] = "categoria.idpadre = " . $this->oBD->escapar($filtros["idcarrera"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception();
		}
	}


	public function insertar($nombre, $tipo, $comentario, $nvacantes, $estado, $idproyecto, $idgrado = 0, $idsesion = 0, $idcategoria = 0, $idlocal = 0, $fecha_inicio, $complementario, $idproducto, $tipodecurso,$tipogrupo,$codaccionsence,$idcarrera)
	{
		try {

			$this->iniciarTransaccion('dat_acad_grupoaula_insert');

			$idgrupoaula = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoaula) FROM acad_grupoaula");
			++$idgrupoaula;

			$idproducto = !empty($idproducto) ? $idproducto : 1;
			$tipodecurso = !empty($tipodecurso) ? $tipodecurso : 1;

			$grupoaula = array(
				'idgrupoaula' => $idgrupoaula, 'nombre' => $nombre, 'tipo' => $tipo, 'comentario' => $comentario, 'nvacantes' => $nvacantes, 'estado' => $estado, 'idproyecto' => $idproyecto, 'idgrado' => $idgrado, 'idsesion' => $idsesion, 'idcategoria' => $idcategoria, 'idlocal' => $idlocal, 'idproducto' => $idproducto, 'tipodecurso' => $tipodecurso, 'fecha_inicio' => $fecha_inicio, 'fecha_fin' => $fecha_inicio,'tipogrupo'=>$tipogrupo
			);
			if(!empty($codaccionsence)){ $grupoaula['codaccionsence'] = $codaccionsence; }
			if(!empty($idcarrera)){ $grupoaula['idcarrera'] = $idcarrera; }

			$grupoaula = NegTools::_empty($grupoaula);
			$this->oBD->insert('acad_grupoaula', $grupoaula);
			if ($complementario == "true") {
				if ($idcategoria != 0) {
					// $sql = "SELECT * FROM acad_curso WHERE tipo = 2 AND idcurso = 181";
					$sql = "SELECT `idcurso`,`nombre`,`imagen`,`descripcion`,`estado`,`fecharegistro`,`idusuario`,`vinculosaprendizajes`,`materialesyrecursos`,`color`,`objetivos`,`certificacion`,`costo`,`silabo`,`e_ini`,`pdf`,`abreviado`,`e_fin`,`aniopublicacion`,`autor`,`txtjson`,`formula_evaluacion`,`tipo`,`sks`,`idioma`,`avance_secuencial` FROM acad_curso c INNER JOIN acad_cursocategoria ca ON (ca.idcurso = c.idcurso)";

					$cond = array();
					$cond[] = "c.tipo = " . $this->oBD->escapar(2);
					$cond[] = "ca.idcategoria = " . $this->oBD->escapar($idcategoria);
					if (!empty($cond)) {
						$sql .= " WHERE " . implode(' AND ', $cond);
					}
					$complementarios = $this->oBD->consultarSQL($sql);
					if (count($complementarios) > 0) {
						foreach ($complementarios as $key => $complementario) {
							$idcurso = $this->oBD->consultarEscalarSQL("SELECT MAX(idcurso) FROM acad_curso_complementario");
							++$idcurso;

							$idcurso_ = $complementario["idcurso"];
							$complementario["idcurso"] = $idcurso;
							$complementario["idcursoprincipal"] = $idcurso_;
							$complementario["idgrupoaula"] = $idgrupoaula;
							$complementario["idcategoria"] = $idcategoria;
							$complementario["nombre"] .= " " . $nombre;
							$complementario["e_fin"] = (empty($complementario["e_fin"]) ? 0 : $complementario["e_fin"]);
							$complementario["fecharegistro"] = date("Y-m-d H:i:s");
							unset($complementario["sks"]);
							// $txtjson = $complementario["txtjson"];
							// $origen = RUTA_BASE . "static" . SD . "media" . SD . "cursos" . SD . "curso_" . $idcurso_;
							// $destino = RUTA_BASE . "static" . SD . "media" . SD . "cursos_complementarios" . SD . "curso_" . $idcurso;
							// NegTools::full_copy($origen, $destino);

							$this->oBD->insert('acad_curso_complementario', $complementario);

							$sql = "SELECT idcursodetalle,idcurso,orden,idrecurso,tiporecurso,idlogro,url,idpadre,color,esfinal,txtjson,espadre FROM acad_cursodetalle WHERE idcurso = " . $this->oBD->escapar($idcurso_);
							$cursodetalle = $this->oBD->consultarSQL($sql);

							foreach ($cursodetalle as $key => $value) {
								$idcursodetalle = $this->oBD->consultarEscalarSQL("SELECT MAX(idcursodetalle) FROM acad_cursodetalle_complementario");
								++$idcursodetalle;
								$value["idcursodetalle"] = $idcursodetalle;
								$value["idcurso"] = $idcurso;
								$nivel = $this->oBD->consultarSQL("SELECT `idnivel`,`nombre`,`tipo`,`idpadre`,`idpersonal`,`estado`,`orden`,`imagen`,`descripcion` FROM niveles WHERE idnivel = " . $value["idrecurso"])[0];
								$idnivel = $this->oBD->consultarEscalarSQL("SELECT MAX(idnivel) FROM niveles_complementario");
								++$idnivel;
								$nivel["idnivel"] = $idnivel;
								$value["idrecurso"] = $idnivel;
								$this->oBD->insert('niveles_complementario', $nivel);
								$this->oBD->insert('acad_cursodetalle_complementario', $value);
							}

							$idgrupoauladetalle = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoauladetalle) FROM acad_grupoauladetalle");
							++$idgrupoauladetalle;
							$fecha_inicio = date("Y-m-d");
							$fecha_final = date("Y-m-d", strtotime($fecha_inicio . "+ 4 month"));
							$detalle = array(
								'idgrupoauladetalle' => $idgrupoauladetalle, 'idgrupoaula' => $idgrupoaula, 'idcurso' => $idcurso_, 'iddocente' => (empty(@$iddocente) ? 0 : @$iddocente), 'idlocal' => (empty(@$idlocal) ? 0 : @$idlocal), 'idambiente' => (empty(@$idambiente) ? 0 : @$idambiente), 'nombre' => @$nombre, 'fecha_inicio' => @$fecha_inicio, 'fecha_final' => @$fecha_final, 'idgrado' => (empty(@$idgrado) ? 0 : @$idgrado), 'idsesion' => (empty(@$idseccion) ? 0 : @$idseccion)
							);
							$this->oBD->insert('acad_grupoauladetalle', $detalle);
						}
					}
				}
			}


			$this->terminarTransaccion('dat_acad_grupoaula_insert');
			return $idgrupoaula;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_acad_grupoaula_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre, $tipo, $comentario, $nvacantes, $estado, $idproyecto, $idgrado = 0, $idsesion = 0, $idcategoria = 0, $idlocal = 0, $fecha_inicio, $complementario,$codaccionsence,$idcarrera)
	{
		try {
			$this->iniciarTransaccion('dat_acad_grupoaula_update');
			$estados = array(
				'nombre' => $nombre, 'tipo' => $tipo, 'comentario' => $comentario, 'nvacantes' => $nvacantes, 'estado' => $estado, 'idproyecto' => $idproyecto, 'idgrado' => $idgrado, 'idsesion' => $idsesion
				// ,'idcategoria'=>$idcategoria
				// ,'idlocal'=>$idlocal
				, 'fecha_inicio' => $fecha_inicio
				// ,'fecha_fin'=>$fecha_inicio
			);
			if(!empty($codaccionsence)){ $estados['codaccionsence']=$codaccionsence; }
			if(!empty($idcarrera)){ $estados['idcarrera']=$idcarrera; }
			
			$sql = "SELECT fecha_inicio,fecha_fin FROM acad_grupoaula WHERE idgrupoaula = " . $this->oBD->escapar($id);
			$grupo = $this->oBD->consultarSQL($sql)[0];
			$date1 = new DateTime($fecha_inicio);
			$date2 = new DateTime($grupo["fecha_inicio"]);
			$accion = "+";
			if ($date1 < $date2) {
				$accion = "-";
			}
			$diff = $date1->diff($date2);
			$accion .= $diff->days . ' days';
			$fecha_fin_ = new DateTime($grupo["fecha_fin"]);
			$fecha_fin_->modify($accion);
			$estados["fecha_fin"] = $fecha_fin_->format("Y-m-d");
			$sql = "SELECT idgrupoauladetalle,fecha_inicio,fecha_final FROM acad_grupoauladetalle WHERE idgrupoaula = " . $this->oBD->escapar($id);
			$detalles = $this->oBD->consultarSQL($sql);
			foreach ($detalles as $key => $detalle) {
				$fecha_inicio_ = new DateTime($detalle["fecha_inicio"]);
				$fecha_final_ = new DateTime($detalle["fecha_final"]);
				$fecha_inicio_->modify($accion);
				$fecha_final_->modify($accion);
				$update_detalle = array(
					'fecha_inicio' => $fecha_inicio_->format("Y-m-d"), 'fecha_final' => $fecha_final_->format("Y-m-d")
				);
				$this->oBD->update('acad_grupoauladetalle', $update_detalle, array('idgrupoauladetalle' => $detalle["idgrupoauladetalle"]));
			}
			$this->oBD->update('acad_grupoaula', $estados, array('idgrupoaula' => $id));
			$this->terminarTransaccion('dat_acad_grupoaula_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT ag.idgrupoaula,ag.nombre,ag.tipo,ag.comentario,ag.nvacantes,ag.estado,ag.idproyecto,ag.oldid,ag.idgrado,ag.idsesion,ag.idcategoria,ag.idlocal,ag.fecha_inicio,ag.fecha_fin,ag.idproducto,ag.tipodecurso, (SELECT nombre from general g WHERE ag.tipo=g.codigo AND tipo_tabla='tipogrupo') as strtipo FROM acad_grupoaula ag WHERE idgrupoaula = " . $this->oBD->escapar($id);
			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_grupoaula', array('idgrupoaula' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function eliminar_($id)
	{
		try {
			$validar = true;

			$sql = "SELECT idgrupoauladetalle FROM acad_grupoauladetalle WHERE idgrupoaula = " . $this->oBD->escapar($id);
			$res_ = $this->oBD->consultarSQL($sql);
			foreach ($res_ as $key => $gad) {
				$sql = "SELECT idmatricula FROM acad_matricula WHERE idgrupoauladetalle = " . $this->oBD->escapar($gad["idgrupoauladetalle"]);
				$res = $this->oBD->consultarSQL($sql);
				if (count($res) == 0) {
					$validar = ($validar && true) ? true : false;
				} else {
					$validar = false;
				}
			}

			if ($validar) {
				foreach ($res_ as $key => $gad) {
					$sql = "SELECT c.idcurso FROM acad_grupoauladetalle agd INNER JOIN acad_curso_complementario c ON (c.idcursoprincipal = agd.idcurso AND agd.idgrupoaula = c.idgrupoaula) WHERE agd.idgrupoauladetalle = " . $this->oBD->escapar($gad["idgrupoauladetalle"]);
					$res = $this->oBD->consultarSQL($sql);
					if (count($res) > 0) {
						$this->oBD->delete('acad_curso_complementario', array('idcurso' => $res[0]["idcurso"]));
					}
					$this->oBD->delete('acad_grupoauladetalle', array('idgrupoauladetalle' => $gad["idgrupoauladetalle"]));
				}
				return $this->oBD->delete('acad_grupoaula', array('idgrupoaula' => $id));
			}
			return false;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('acad_grupoaula', array($propiedad => $valor), array('idgrupoaula' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_grupoaula") . ": " . $e->getMessage());
		}
	}
}
