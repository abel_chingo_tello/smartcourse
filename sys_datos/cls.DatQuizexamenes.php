<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatQuizexamenes extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar(NAME_BDQUIZ);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Examenes") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT `idexamen`,`titulo`,`descripcion`,`portada`,`fuente`,`fuentesize`,`aleatorio`,`calificacion_por`,`calificacion_en`,`calificacion_total`,`calificacion_min`,`tiempo_por`,`tiempo_total`,`tipo`,`estado`,`idproyecto`,`idpersonal`,`fecharegistro`,`nintento`,`calificacion`,`origen_habilidades`,`habilidades_todas`,`fuente_externa`,`dificultad_promedio`,`tiene_certificado`,`nombre_certificado`,retroceder,tiempoalinicio FROM examenes";
			$cond = array();
			if (!empty($filtros["idexamen"])) {
				if (is_array($filtros["idexamen"])) {
					$cond[] = "idexamen IN ('" . implode("','", $filtros["idexamen"]) . "')";
				} else
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if (!empty($filtros["titulo"])) {
				$cond[] = "titulo LIKE '%" . $filtros["titulo"] . "%'";
			}
			if (!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (!empty($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (!empty($filtros["idpersonal"])) {
				$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if (!empty($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (!empty($filtros["nintento"])) {
				$cond[] = "nintento = " . $this->oBD->escapar($filtros["nintento"]);
			}
			if (!empty($filtros["calificacion"])) {
				$cond[] = "calificacion = " . $this->oBD->escapar($filtros["calificacion"]);
			}
			if (!empty($filtros["habilidades_todas"])) {
				$cond[] = "habilidades_todas = " . $this->oBD->escapar($filtros["habilidades_todas"]);
			}
			if (!empty($filtros["origen_habilidades"])) {
				$cond[] = "origen_habilidades = " . $this->oBD->escapar($filtros["origen_habilidades"]);
			}
			if (!empty($filtros["fuente_externa"])) {
				$cond[] = "fuente_externa = " . $this->oBD->escapar($filtros["fuente_externa"]);
			}
			if (!empty($filtros["dificultad_promedio"])) {
				$cond[] = "dificultad_promedio = " . $this->oBD->escapar($filtros["dificultad_promedio"]);
			}
			if (!empty($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (!empty($filtros["idcc_smartcourse"])) {
				$cond[] = "idcc_smartcourse = " . $this->oBD->escapar($filtros["idcc_smartcourse"]);
			}
			if (!empty($filtros["idpersona_smartcourse"])) {
				$cond[] = "idpersona_smartcourse = " . $this->oBD->escapar($filtros["idpersona_smartcourse"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			parent::conectar(NAME_BDQUIZ);
			parent::setLimite(0, 10000);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Examenes") . ": " . $e->getMessage());
		}
	}

	public function buscarpreguntas($filtros = null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT `idpregunta`,`idexamen`,`pregunta`,`descripcion`,`ejercicio`,`idpadre`,`tiempo`,`puntaje`,`idpersonal`,`fecharegistro`,`template`,`habilidades`,`idcontenedor`,`dificultad`,`idpregunta_origen`,`orden` FROM examenes_preguntas";

			$cond = array();
			if (!empty($filtros["idpregunta"])) {
				$cond[] = "idpregunta = " . $this->oBD->escapar($filtros["idpregunta"]);
			}
			if (!empty($filtros["idexamen"])) {
				$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if (!empty($filtros["idpadre"])) {
				$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}

			if (!empty($filtros["idpersonal"])) {
				$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if (!empty($filtros["habilidad"])) {
				$cond[] = "habilidades LIKE " . $this->oBD->escapar('%' . $filtros["habilidad"] . '%');
			}
			if (!empty($filtros["template"])) {
				$cond[] = "template = " . $this->oBD->escapar($filtros["template"]);
			}
			if (!empty($filtros["idcontenedor"])) {
				$cond[] = "idcontenedor = " . $this->oBD->escapar($filtros["idcontenedor"]);
			}
			if (!empty($filtros["dificultad"])) {
				$cond[] = "dificultad = " . $this->oBD->escapar($filtros["dificultad"]);
			}
			if (!empty($filtros["idpregunta_origen"])) {
				$cond[] = "idpregunta_origen = " . $this->oBD->escapar($filtros["idpregunta_origen"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY idpregunta ASC";
			parent::conectar(NAME_BDQUIZ);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Examenes_preguntas") . ": " . $e->getMessage());
		}
	}



	public function get($id)
	{
		try {
			#parent::conectar();
			$sql = "SELECT  `idexamen`,`titulo`,`descripcion`,`portada`,`fuente`,`fuentesize`,`aleatorio`,`calificacion_por`,`calificacion_en`,`calificacion_total`,`calificacion_min`,`tiempo_por`,`tiempo_total`,`tipo`,`estado`,`idproyecto`,`idpersonal`,`fecharegistro`,`nintento`,`calificacion`,`origen_habilidades`,`habilidades_todas`,`fuente_externa`,`dificultad_promedio`,`tiene_certificado`,`nombre_certificado`,retroceder,tiempoalinicio  FROM examenes  "

				. " WHERE idexamen = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Examenes") . ": " . $e->getMessage());
		}
	}


	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			#parent::conectar();
			$this->oBD->update('examenes', array($propiedad => $valor), array('idexamen' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Examenes") . ": " . $e->getMessage());
		}
	}


	public function persona($user,$slug){
		try{
			parent::conectar(NAME_BDQUIZ);
			$sql="SELECT idproyecto  FROM proyecto WHERE slug='".$slug."' LIMIT 0, 1";
			$idproyectoquiz=$this->oBD->consultarEscalarSQL($sql);
			if(empty($idproyectoquiz)){
				$idproyectoquiz=$this->oBD->consultarEscalarSQL("SELECT idproyecto+1 FROM proyecto ORDER BY idproyecto DESC limit 0,1");
				$estado_proyectoquiz=array(
					'idproyecto'=>$idproyectoquiz,
					'nombre'=>$slug,
					'comentario'=>'Generado automaticamente',
					'slug'=>$slug,
					'fecha_inicio'=>date('Y-m-d'),
					'fecha_fin'=>'2022-12-31',
					'estado'=>1,
					'idioma'=>'ES'
				);
				$this->oBD->insert('proyecto', $estado_proyectoquiz);
			}
			$sql="SELECT dni FROM personal WHERE identificador='".$user["idpersona"]."' AND usuario='".$user["usuario"]."' LIMIT  0, 1";
			$dnipersona=$this->oBD->consultarEscalarSQL($sql);
			if(empty($dnipersona)){
				$dnipersona=$this->oBD->consultarEscalarSQL("SELECT dni+1 FROM personal ORDER BY dni DESC limit 0,1");
				$estado_personas=array('dni'=>$dnipersona,
					'ape_paterno'=>'',
					'ape_materno'=>'',
					'nombre'=>$user["nombre_full"],					
					'idproyecto'=>$idproyectoquiz,
					'identificador'=>$user["idpersona"],
					//'regusuario'=>$,
					'regfecha'=>date('Y-m-d'),
					'usuario'=>$user["usuario"],
					'clave'=>md5($user["usuario"]),
					'token'=>$user["clave"],
					'estado'=>1,
					'idioma'=>'ES'
					);
				$this->oBD->insert('personal', $estado_personas);
			}
			$sql="SELECT iddetalle FROM rol_personal WHERE idpersonal ='".$dnipersona."' AND idrol='".$user["idrol"]."' LIMIT  0, 1";
			$iddetalle=$this->oBD->consultarEscalarSQL($sql);
			if(empty($iddetalle)){
				$iddetalle=$this->oBD->consultarEscalarSQL("SELECT iddetalle+1 FROM rol_personal ORDER BY iddetalle DESC limit 0,1");
				$estado_rol=array('iddetalle'=>$iddetalle,
					'idrol'=>$user["idrol"],
					'idpersonal'=>$dnipersona);
				$this->oBD->insert('rol_personal', $estado_rol);
			}
			return array('quiz_idpersona'=>$dnipersona,'quiz_idproyecto'=>$idproyectoquiz);
		}catch(Exception $ex){
			$this->cancelarTransaccion('duplicarexamen');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("copiar Exámen") . ": " . $ex->getMessage());
		}
	}

	public function duplicar($idexamen,$slug,$user=array(),$idioma='ES'){
		try{
			parent::conectar(NAME_BDQUIZ);
			$this->iniciarTransaccion('duplicarexamen');
			//obtneniedo proyecto de smarttquiz
			$sql="SELECT idproyecto  FROM proyecto WHERE slug='".$slug."' LIMIT 0, 1";
			$idproyectoquiz=$this->oBD->consultarEscalarSQL($sql);
			if(empty($idproyectoquiz)){
				$idproyectoquiz=$this->oBD->consultarEscalarSQL("SELECT idproyecto+1 FROM proyecto ORDER BY idproyecto DESC limit 0,1");
				$estado_proyectoquiz=array(
					'idproyecto'=>$idproyectoquiz,
					'nombre'=>$slug,
					'comentario'=>'Copiado desde '.$idexamen,
					'slug'=>$slug,
					'fecha_inicio'=>date('Y-m-d'),
					'fecha_fin'=>'2022-12-31',
					'estado'=>1,
					'idioma'=>$idioma
				);
				$this->oBD->insert('proyecto', $estado_proyectoquiz);
			}
			//obtneniendo dni de persona smartquiz
			$sql="SELECT dni FROM personal WHERE identificador='".$user["idpersona"]."' AND usuario='".$user["usuario"]."' LIMIT  0, 1";
			$dnipersona=$this->oBD->consultarEscalarSQL($sql);
			if(empty($dnipersona)){
				$dnipersona=$this->oBD->consultarEscalarSQL("SELECT dni+1 FROM personal ORDER BY dni DESC limit 0,1");
				$estado_personas=array('dni'=>$dnipersona,
					'ape_paterno'=>'',
					'ape_materno'=>'',
					'nombre'=>$user["nombre_full"],					
					'idproyecto'=>$idproyectoquiz,
					'identificador'=>$user["idpersona"],
					//'regusuario'=>$,
					'regfecha'=>date('Y-m-d'),
					'usuario'=>$user["usuario"],
					'clave'=>md5($user["usuario"]),
					'token'=>$user["clave"],
					'estado'=>1,
					'idioma'=>$idioma
					);
				$this->oBD->insert('personal', $estado_personas);
			}
			//asignar rol a persona;
			$sql="SELECT iddetalle FROM rol_personal WHERE idpersonal ='".$dnipersona."' AND idrol='".$user["idrol"]."' LIMIT  0, 1";
			$iddetalle=$this->oBD->consultarEscalarSQL($sql);
			if(empty($iddetalle)){
				$iddetalle=$this->oBD->consultarEscalarSQL("SELECT iddetalle+1 FROM rol_personal ORDER BY iddetalle DESC limit 0,1");
				$estado_rol=array('iddetalle'=>$iddetalle,
					'idrol'=>$user["idrol"],
					'idpersonal'=>$dnipersona);
				$this->oBD->insert('rol_personal', $estado_rol);
			}


			//$dnipersona=$persona[0]["dni"];
			//obteniendo examen a copiar
			$sql="SELECT * FROM examenes WHERE idexamen=".$idexamen;
			$examen=$this->oBD->consultarSQL($sql);
			if(empty($examen[0])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Exámen no registrado')));
				$this->terminarTransaccion('duplicarexamen');
				exit();
			}
			$examen=$examen[0];
			//var_dump($examen);
			$estados=array();
			$estados["descripcion"]='';		
			$estados["origen_habilidades"]='';
			$estados["retroceder"]=0;
			$estados["tiempoalinicio"]=0;
			foreach ($examen as $key => $v){
				if(!empty($v))$estados[$key]=$v;
			}
			$estados["estado"]=1;
			$estados["idproyecto"]=$idproyectoquiz;
			$estados["idpersonal"]=$dnipersona;
			$estados["fecharegistro"]=date('Y-m-d');
			$estados["idpersona_smartcourse"]=$user["idpersona"];
			//$estados["idcc_smartcourse"]=0;
			$idnewexamen=$this->oBD->consultarEscalarSQL("SELECT idexamen+1 FROM examenes ORDER BY idexamen DESC limit 0,1");
			$estados["idexamen"]=$idnewexamen;
			$this->oBD->insert('examenes', $estados);

			//copiar preguntas
			$sql="SELECT * FROM examenes_preguntas WHERE idexamen=".$idexamen;
			$preguntas=$this->oBD->consultarSQL($sql);
			if(empty($preguntas[0])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Examen no tiene preguntas a ser copiados, solo se copio la configuración')));
				$this->terminarTransaccion('duplicarexamen');
				exit();
			}
			foreach ($preguntas as $key => $preg){
				$idpregunta=$this->oBD->consultarEscalarSQL("SELECT idpregunta+1 FROM examenes_preguntas ORDER BY idpregunta DESC limit 0,1");
				$estados_preguntas=array();
				$estados_preguntas["descripcion"]='';
				$estados_preguntas["habilidades"]='';
				$estados_preguntas["puntaje"]=0;
				$estados_preguntas["pregunta"]=0;
				foreach ($preg as $key => $v){if(!empty($v))$estados_preguntas[$key]=$v;}
				$estados_preguntas["idexamen"]=$idnewexamen;
				$estados_preguntas["idpersonal"]=$dnipersona;
				$estados_preguntas["fecharegistro"]=date('Y-m-d');
				$estados_preguntas["idpregunta"]=$idpregunta;
				$this->oBD->insert('examenes_preguntas', $estados_preguntas);
			}
			$this->terminarTransaccion('duplicarexamen');
			return array('idexamen'=>$idnewexamen,'idproyecto'=>$idproyectoquiz);
		}catch(Exception $ex){
			$this->cancelarTransaccion('duplicarexamen');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("copiar Exámen") . ": " . $ex->getMessage());
		}
	}

	public function guardar($datos){
		try{
			parent::conectar(NAME_BDQUIZ);
			$this->iniciarTransaccion('guardarexamen');
			$estados=array();
			$estados["descripcion"]='';		
			$estados["origen_habilidades"]='';
			$estados["retroceder"]=0;
			$estados["tiempoalinicio"]=0;		
			foreach ($datos as $key => $v){
				if(!empty($v))$estados[$key]=$v;
			}			
			$acc='nuevo';
			if(!empty($datos["idexamen"])){
				$sql="SELECT * FROM examenes WHERE idexamen=".$datos["idexamen"];
				$examen=$this->oBD->consultarSQL($sql);
				if(!empty($examen)) $acc="edit";
			}
			$estados["tipo"]='N';
			if($acc=="nuevo"){
				$idnewexamen=$this->oBD->consultarEscalarSQL("SELECT idexamen+1 FROM examenes ORDER BY idexamen DESC limit 0,1");
				$estados["idexamen"]=$idnewexamen;
				$this->oBD->insert('examenes', $estados);
			}else{
				$this->oBD->update('examenes', $estados,array('idexamen' => $estados["idexamen"]));
			}
			$this->terminarTransaccion('guardarexamen');
			return $estados["idexamen"];
		}catch(Exception $ex){
			$this->cancelarTransaccion('guardarexamen');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("copiar Exámen") . ": " . $ex->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			#parent::conectar();
			parent::conectar(NAME_BDQUIZ);
			$this->iniciarTransaccion('dat_examenes_eliminar');
			$this->oBD->delete('examenes_preguntas', array('idexamen' => $id));
			$this->oBD->delete('examenes', array('idexamen' => $id));
			$this->terminarTransaccion('dat_examenes_eliminar');
			return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}
}
