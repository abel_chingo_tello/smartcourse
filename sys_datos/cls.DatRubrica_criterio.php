<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-04-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatRubrica_criterio extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Rubrica_criterio") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT (@i := @i + 1) as orden, 
			rubrica_criterio.idrubrica_criterio, rubrica_criterio.detalle, rubrica_criterio.peso, rubrica_criterio.idrubrica, rubrica_criterio.idcapacidad 
		FROM rubrica_criterio
			cross join (select @i := 0) rubrica_criterio";

			$cond = array();


			if (isset($filtros["idrubrica_criterio"])) {
				$cond[] = "idrubrica_criterio = " . $this->oBD->escapar($filtros["idrubrica_criterio"]);
			}
			if (isset($filtros["detalle"])) {
				$cond[] = "detalle = " . $this->oBD->escapar($filtros["detalle"]);
			}
			if (isset($filtros["peso"])) {
				$cond[] = "peso = " . $this->oBD->escapar($filtros["peso"]);
			}
			if (isset($filtros["idrubrica"])) {
				$cond[] = "idrubrica = " . $this->oBD->escapar($filtros["idrubrica"]);
			}
			if (isset($filtros["idcapacidad"])) {
				$cond[] = "idcapacidad = " . $this->oBD->escapar($filtros["idcapacidad"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Rubrica_criterio") . ": " . $e->getMessage());
		}
	}


	public function insertar($detalle, $peso, $idrubrica, $idcapacidad)
	{
		try {

			$this->iniciarTransaccion('dat_rubrica_criterio_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrubrica_criterio) FROM rubrica_criterio");
			++$id;

			$estados = array(
				'idrubrica_criterio' => $id, 'detalle' => $detalle, 'peso' => $peso, 'idrubrica' => $idrubrica
				// , 'idcapacidad' => $idcapacidad
			);
			// echo json_encode($estados);exit();
			$this->oBD->insert('rubrica_criterio', $estados);
			$this->terminarTransaccion('dat_rubrica_criterio_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_rubrica_criterio_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Rubrica_criterio") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $detalle, $peso, $idrubrica, $idcapacidad)
	{
		try {
			$this->iniciarTransaccion('dat_rubrica_criterio_update');
			$estados = array(
				'detalle' => $detalle, 'peso' => $peso, 'idrubrica' => $idrubrica
				// , 'idcapacidad' => $idcapacidad
			);

			$this->oBD->update('rubrica_criterio ', $estados, array('idrubrica_criterio' => $id));
			$this->terminarTransaccion('dat_rubrica_criterio_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_criterio") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idrubrica_criterio,detalle,peso,idrubrica,idcapacidad FROM rubrica_criterio  "
				. " WHERE idrubrica_criterio = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Rubrica_criterio") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('rubrica_criterio', array('idrubrica_criterio' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Rubrica_criterio") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('rubrica_criterio', array($propiedad => $valor), array('idrubrica_criterio' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Rubrica_criterio") . ": " . $e->getMessage());
		}
	}
}
