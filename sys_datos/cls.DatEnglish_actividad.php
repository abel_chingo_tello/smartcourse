<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatEnglish_actividad extends DatBase
{

	public function __construct($BD = '')
	{
		try {
			if (empty($BD)) $BD = NAME_BDSEAdolecentes;
			parent::conectar($BD);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Acad_cursodetalle") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT `idactividad`,`nivel`,`unidad`,`sesion`,`metodologia`,`habilidad`,`titulo`,`descripcion`,`estado`,`url`,`idioma`,`idpersonal` FROM actividades";

			$cond = array();
			if (!empty($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if (!empty($filtros["nivel"])) {
				$cond[] = "nivel = " . $this->oBD->escapar($filtros["nivel"]);
			}
			if (!empty($filtros["unidad"])) {
				$cond[] = "unidad = " . $this->oBD->escapar($filtros["unidad"]);
			}
			if (!empty($filtros["sesion"])) {
				$cond[] = "sesion = " . $this->oBD->escapar($filtros["sesion"]);
			}
			if (!empty($filtros["metodologia"])) {
				$cond[] = "metodologia = " . $this->oBD->escapar($filtros["metodologia"]);
			}

			if (!empty($filtros["titulo"])) {
				$cond[] = "titulo = " . $this->oBD->like($filtros["titulo"]);
			}
			if (!empty($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";
			if (isset($filtros["BD"])) {
				parent::conectar($filtros["BD"]);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Actividades") . ": " . $e->getMessage());
		}
	}

	public function buscarmetodologia($filtros = null)
	{
		try {
			if (isset($filtros["BD"])) {
				parent::conectar($filtros["BD"]);
			}

			$sql = "SELECT metodologia_habilidad.idmetodologia, metodologia_habilidad.nombre, metodologia_habilidad.tipo, metodologia_habilidad.estado FROM metodologia_habilidad";

			$cond = array();
			if (!empty($filtros["idmetodologia"])) {
				$cond[] = "idmetodologia = " . $this->oBD->escapar($filtros["idmetodologia"]);
			}
			if (!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (isset($filtros["BD"])) {
				parent::conectar($filtros["BD"]);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Metodologia_habilidad") . ": " . $e->getMessage());
		}
	}

	public function buscardetalle($filtros = null)
	{
		try {
			$sql = "SELECT `iddetalle`,`idactividad`,`pregunta`,`orden`,`url`,`tipo`,`tipo_desarrollo`,`tipo_actividad`,`idhabilidad`,`texto_edit`,`titulo`,`descripcion`,`idpersonal` FROM actividad_detalle";
			$cond = array();
			if (!empty($filtros["iddetalle"])) {
				$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if (!empty($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if (!empty($filtros["titulo"])) {
				$cond[] = "titulo LIKE '" . $this->oBD->escapar($filtros["titulo"], false) . "%'";
			}
			if (!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (!empty($filtros["tipo_desarrollo"])) {
				$cond[] = "tipo_desarrollo = " . $this->oBD->escapar($filtros["tipo_desarrollo"]);
			}
			if (!empty($filtros["tipo_actividad"])) {
				$cond[] = "tipo_actividad = " . $this->oBD->escapar($filtros["tipo_actividad"]);
			}
			if (!empty($filtros["idhabilidad"])) {
				$condOr = array();
				foreach ($filtros["idhabilidad"] as $idH) {
					$condOr[] = "idhabilidad LIKE '%" . $idH . "%' ";
				}

				if (!empty($condOr)) {
					$cond[] = " ( " . implode(' OR ', $condOr) . " ) ";
				}
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (!empty($filtros["ordenarpor"])) {
				$sql .= ' ORDER BY ' . $filtros["ordenarpor"];
			} else
				$sql .= " ORDER BY orden ASC";
			if (isset($filtros["BD"])) {
				parent::conectar($filtros["BD"]);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Actividad_detalle") . ": " . $e->getMessage());
		}
	}
}
