<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-09-2019  
  * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */ 
class DatUgel extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idugel,descripcion,abrev,iddepartamento,idprovincia,idproyecto,iddre FROM ugel";
			
			$cond = array();		
					
			if(isset($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["iddre"])) {
					$cond[] = "iddre = " . $this->oBD->escapar($filtros["iddre"]);
			}			
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["abrev"])) {
					$cond[] = "abrev = " . $this->oBD->escapar($filtros["abrev"]);
			}
			if(isset($filtros["iddepartamento"])) {
					$cond[] = "iddepartamento = " . $this->oBD->escapar($filtros["iddepartamento"]);
			}
			if(isset($filtros["idprovincia"])) {
					$cond[] = "idprovincia = " . $this->oBD->escapar($filtros["idprovincia"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if(isset($filtros["texto"])) {
					$cond[] = "(descripcion " . $this->oBD->like($filtros["texto"])." OR abrev " . $this->oBD->like($filtros["texto"]).")";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY descripcion ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}

	public function ubigeo($filtros=null)
	{
		try {
			$sql = "SELECT id_ubigeo,pais,departamento,provincia,distrito,ciudad FROM ubigeo";			
			$cond = array();			
			if(!empty($filtros["iddep"])){
				$cond1 = " WHERE provincia ='00' AND distrito='00'";
				//$cond1 = " where iddepartamento= ".$this->oBD->escapar($filtros["iddep"])."provincia ='00' and distrito='00'";
			}

			if(!empty($filtros["idpadre"])) {
				$_iddep=substr($this->oBD->escapar($filtros["idpadre"]),1,2);
				$cond1 = " WHERE departamento= " .$_iddep." AND provincia<>'00' AND distrito='00'";	
			}

			if(!empty($filtros["idpadre1"])) {
				$_iddep=substr($this->oBD->escapar($filtros["idpadre1"]),1,2);
				$_idpro=substr($this->oBD->escapar($filtros["idpadre1"]),3,2);
				$cond1 = " WHERE departamento='" .$_iddep."' AND provincia='".$_idpro."' AND distrito<>'00'";	
			}

			$sql .=$cond1;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($descripcion,$abrev,$iddepartamento,$idprovincia,$idproyecto=0,$iddre=0)
	{
		try {
			
			$this->iniciarTransaccion('dat_ugel_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idugel) FROM ugel");
			++$id;			
			$estados = array('idugel' => $id							
							,'descripcion'=>$descripcion
							,'abrev'=>$abrev
							,'iddepartamento'=>$iddepartamento
							,'idprovincia'=>$idprovincia
							,'idproyecto'=>$idproyecto
							,'iddre'=>$iddre							
							);			
			$this->oBD->insert('ugel', $estados);			
			$this->terminarTransaccion('dat_ugel_insert');			
			return $id;
		} catch(Exception $e){
			$this->cancelarTransaccion('dat_ugel_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}


	public function actualizar($id, $descripcion,$abrev,$iddepartamento,$idprovincia,$idproyecto=0,$iddre=0)
	{
		try {
			$this->iniciarTransaccion('dat_ugel_update');
			$estados = array('descripcion'=>$descripcion
							,'abrev'=>$abrev
							,'iddepartamento'=>$iddepartamento
							,'idprovincia'=>$idprovincia
							,'idproyecto'=>$idproyecto
							,'iddre'=>$iddre							
							);
			
			$this->oBD->update('ugel ', $estados, array('idugel' => $id));
		    $this->terminarTransaccion('dat_ugel_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}

	public function importar($id,$descripcion,$abrev,$iddepartamento,$idprovincia,$idproyecto=0,$iddre=0){
		try{
			if(empty($id)) return $this->insertar($descripcion,$abrev,$iddepartamento,$idprovincia);
			else{
				$this->iniciarTransaccion('dat_ugel_insert2');
				$estados = array('idugel' => $id
							,'descripcion'=>$descripcion
							,'abrev'=>$abrev
							,'iddepartamento'=>$iddepartamento
							,'idprovincia'=>$idprovincia
							,'idproyecto'=>$idproyecto
							,'iddre'=>$iddre
							);
				$this->oBD->insert('ugel', $estados);
				$this->terminarTransaccion('dat_ugel_insert2');
				return $id;
			}
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_ugel_insert2');
			return -1;
		}
	}

	public function get($id)
	{
		try {
			//$sql = "SELECT tb1.*,tb2.ciudad AS _ciudad ,tb3.ciudad AS _ciudad  FROM ugel tb1 LEFT JOIN ubigeo tb2 ON tb1.iddepartamento=tb2.departamento  LEFT JOIN ubigeo tb3 ON tb1.idprovincia=tb3.provincia  WHERE tb1.idugel = " . $this->oBD->escapar($id);
			$sql = "SELECT idugel,descripcion,abrev,iddepartamento,idprovincia,idproyecto,iddre FROM ugel WHERE idugel = " . $this->oBD->escapar($id);			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('ugel', array('idugel' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('ugel', array($propiedad => $valor), array('idugel' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}
   
		
}