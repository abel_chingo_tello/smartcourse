<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-01-2018  
 * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */
class DatPersona_educacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Persona_educacion") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(ideducacion) FROM persona_educacion";

			$cond = array();

			if (isset($filtros["ideducacion"])) {
				$cond[] = "ideducacion = " . $this->oBD->escapar($filtros["ideducacion"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if (isset($filtros["institucion"])) {
				$cond[] = "institucion = " . $this->oBD->escapar($filtros["institucion"]);
			}
			if (isset($filtros["tipodeestudio"])) {
				$cond[] = "tipodeestudio = " . $this->oBD->escapar($filtros["tipodeestudio"]);
			}
			if (isset($filtros["titulo"])) {
				$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if (isset($filtros["areaestudio"])) {
				$cond[] = "areaestudio = " . $this->oBD->escapar($filtros["areaestudio"]);
			}
			if (isset($filtros["situacion"])) {
				$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if (isset($filtros["fechade"])) {
				$cond[] = "fechade = " . $this->oBD->escapar($filtros["fechade"]);
			}
			if (isset($filtros["fechahasta"])) {
				$cond[] = "fechahasta = " . $this->oBD->escapar($filtros["fechahasta"]);
			}
			if (isset($filtros["actualmente"])) {
				$cond[] = "actualmente = " . $this->oBD->escapar($filtros["actualmente"]);
			}
			if (isset($filtros["mostrar"])) {
				$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Persona_educacion") . ": " . $e->getMessage());
		}
	}
	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT pe.ideducacion, pe.idpersona, pe.institucion, pe.tipodeestudio, pe.titulo, pe.areaestudio, pe.situacion, pe.fechade, pe.fechahasta, pe.actualmente, pe.mostrar, (SELECT nombre from general g WHERE pe.tipodeestudio=g.codigo AND tipo_tabla='tipoestudio') as strtipoestudio,  (SELECT nombre from general g1 WHERE pe.situacion=g1.codigo AND tipo_tabla='situacionestudio') as strsituacion,  (SELECT nombre from general g2 WHERE pe.areaestudio=g2.codigo AND tipo_tabla='areaestudio') as strareaestudio FROM persona_educacion pe left join personal pl on pe.idpersona=pl.idpersona";

			$cond = array();


			if (isset($filtros["ideducacion"])) {
				$cond[] = "ideducacion = " . $this->oBD->escapar($filtros["ideducacion"]);
			}
			if (isset($filtros["idpersona"])) {
				$cond[] = "(pe.idpersona = " . $this->oBD->escapar($filtros["idpersona"]) . " OR md5(pe.idpersona)=" . $this->oBD->escapar($filtros["idpersona"]) . ")";
			}
			if (isset($filtros["institucion"])) {
				$cond[] = "institucion = " . $this->oBD->escapar($filtros["institucion"]);
			}
			if (isset($filtros["tipodeestudio"])) {
				$cond[] = "tipodeestudio = " . $this->oBD->escapar($filtros["tipodeestudio"]);
			}
			if (isset($filtros["titulo"])) {
				$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if (isset($filtros["areaestudio"])) {
				$cond[] = "areaestudio = " . $this->oBD->escapar($filtros["areaestudio"]);
			}
			if (isset($filtros["situacion"])) {
				$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if (isset($filtros["fechade"])) {
				$cond[] = "fechade = " . $this->oBD->escapar($filtros["fechade"]);
			}
			if (isset($filtros["fechahasta"])) {
				$cond[] = "fechahasta = " . $this->oBD->escapar($filtros["fechahasta"]);
			}
			if (isset($filtros["actualmente"])) {
				$cond[] = "actualmente = " . $this->oBD->escapar($filtros["actualmente"]);
			}
			if (isset($filtros["mostrar"])) {
				$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Persona_educacion") . ": " . $e->getMessage());
		}
	}


	public function insertar($idpersona, $institucion, $tipodeestudio, $titulo, $areaestudio, $situacion, $fechade, $fechahasta, $actualmente, $mostrar)
	{
		try {

			$this->iniciarTransaccion('dat_persona_educacion_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(ideducacion) FROM persona_educacion");
			++$id;

			$estados = array(
				'ideducacion' => $id, 'idpersona' => $idpersona, 'institucion' => $institucion, 'tipodeestudio' => $tipodeestudio, 'titulo' => $titulo, 'areaestudio' => $areaestudio, 'situacion' => $situacion, 'fechade' => $fechade, 'fechahasta' => $fechahasta, 'actualmente' => $actualmente, 'mostrar' => $mostrar
			);

			$this->oBD->insert('persona_educacion', $estados);
			$this->terminarTransaccion('dat_persona_educacion_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_persona_educacion_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Persona_educacion") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona, $institucion, $tipodeestudio, $titulo, $areaestudio, $situacion, $fechade, $fechahasta, $actualmente, $mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_persona_educacion_update');
			$estados = array(
				'idpersona' => $idpersona, 'institucion' => $institucion, 'tipodeestudio' => $tipodeestudio, 'titulo' => $titulo, 'areaestudio' => $areaestudio, 'situacion' => $situacion, 'fechade' => $fechade, 'fechahasta' => $fechahasta, 'actualmente' => $actualmente, 'mostrar' => $mostrar
			);

			$this->oBD->update('persona_educacion ', $estados, array('ideducacion' => $id));
			$this->terminarTransaccion('dat_persona_educacion_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Persona_educacion") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT pe.ideducacion, pe.idpersona, pe.institucion, pe.tipodeestudio, pe.titulo, pe.areaestudio, pe.situacion, pe.fechade, pe.fechahasta, pe.actualmente, pe.mostrar, (SELECT nombre from general g WHERE pe.tipodeestudio=g.codigo AND tipo_tabla='tipoestudio') as strtipoestudio,  (SELECT nombre from general g1 WHERE pe.situacion=g1.codigo AND tipo_tabla='situacionestudio') as strsituacion,  (SELECT nombre from general g2 WHERE pe.areaestudio=g2.codigo AND tipo_tabla='areaestudio') as strareaestudio FROM persona_educacion pe left join personal pl on pe.idpersona=pl.idpersona  WHERE pe.ideducacion = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Persona_educacion") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_educacion', array('ideducacion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Persona_educacion") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('persona_educacion', array($propiedad => $valor), array('ideducacion' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Persona_educacion") . ": " . $e->getMessage());
		}
	}
}
