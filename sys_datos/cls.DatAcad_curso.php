<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
 * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */
class DatAcad_curso extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros = null)
	{
		try {
			$sql = "SELECT COUNT(1) FROM acad_curso";

			$cond = array();

			if (isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["aniopublicacion"])) {
				$cond[] = "aniopublicacion = " . $this->oBD->escapar($filtros["aniopublicacion"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["imagen"])) {
				$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			/*if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}*/
			if (isset($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " ASC";
				}
			} else
				$sql .= " ORDER BY nombre ASC";

			return $this->oBD->consultarEscalarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("count") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}


	public function buscarXproyecto($filtros = null)
	{
		try {
			$sql = "SELECT ac.idcurso,nombre,imagen,descripcion,estado,fecharegistro,idusuario,color,abreviado,autor,aniopublicacion,txtjson,tipo,sks from acad_curso ac INNER JOIN proyecto_cursos pc on pc.idcurso=ac.idcurso";
			$cond = array();
			if (isset($filtros["idcurso"])) {
				$cond[] = "ac.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["imagen"])) {
				$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if (isset($filtros["aniopublicacion"])) {
				$cond[] = "aniopublicacion = " . $this->oBD->escapar($filtros["aniopublicacion"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			/*if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}*/
			if (isset($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "pc.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if (isset($filtros["noidproyecto"])) {
				$cond[] = "pc.idproyecto <> " . $this->oBD->escapar($filtros["noidproyecto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " ASC";
				}
			} else $sql .= " ORDER BY nombre ASC";
			//echo $sql;

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function buscarxcategoria($filtros = null)
	{
		try {
			$sql = "SELECT ac.idcurso,ac.nombre,ac.imagen,ac.descripcion,ac.estado,ac.fecharegistro,ac.idusuario,ac.color,ac.abreviado,ac.autor,ac.aniopublicacion,ac.txtjson,ac.tipo,ac.sks,id as idcursocategoria,act.nombre as categoria, acc.idcategoria, act.descripcion as descripcioncategoria from acad_curso ac INNER JOIN acad_cursocategoria  acc ON ac.idcurso=acc.idcurso INNER JOIN acad_categorias  act ON acc.idcategoria=act.idcategoria";
			if (isset($filtros["solocategoria"])) {
				$sql = "SELECT id as idcursocategoria,act.nombre as categoria, acc.idcategoria from acad_curso ac INNER JOIN acad_cursocategoria  acc ON ac.idcurso=acc.idcurso INNER JOIN acad_categorias  act ON acc.idcategoria=act.idcategoria";
			}
			$cond = array();
			if (!empty($filtros['cursosall']) && !empty($filtros['idproyecto'])) {
				$sql = "SELECT pc.idproyecto, ac.idcurso,ac.nombre,ac.imagen,ac.descripcion,ac.estado,ac.fecharegistro,ac.idusuario,ac.color,ac.abreviado,ac.autor,ac.aniopublicacion,ac.txtjson,ac.tipo,ac.sks,id as idcursocategoria,act.nombre as categoria ,acc.idcategoria, act.descripcion as descripcioncategoria FROM acad_curso ac INNER JOIN proyecto_cursos pc ON ac.idcurso=pc.idcurso LEFT JOIN acad_categorias act ON pc.idproyecto=act.idproyecto LEFT JOIN acad_cursocategoria acc ON act.idcategoria=acc.idcategoria ";
				$cond[] = 'pc.idproyecto=' . $filtros["idproyecto"];
			} else if (isset($filtros["idproyecto"])) {
				$sql .= " INNER JOIN proyecto_cursos pc on ac.idcurso=pc.idcurso";
				$cond[] = 'pc.idproyecto=' . $filtros["idproyecto"];
				$filtros["orderby"] = 'acc.idcategoria ASC , pc.orden ';
			}

			if (isset($filtros["aniopublicacion"])) {
				$cond[] = "aniopublicacion = " . $this->oBD->escapar($filtros["aniopublicacion"]);
			}
			if (isset($filtros["autor"])) {
				$cond[] = "autor " . $this->oBD->like($filtros["autor"]);
			}

			if (isset($filtros["idcategoria"])) {
				$cond[] = "acc.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if (isset($filtros["idcurso"])) {
				$cond[] = "ac.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}

			if (isset($filtros["texto"])) {
				$cond[] = "concat(ac.nombre,' ',ac.descripcion,' ',act.nombre,' ',act.descripcion, autor) " . $this->oBD->Like($filtros["texto"]);
			}

			/*if(isset($filtros["estado"])) {
					$cond[] = "ac.estado = " . $this->oBD->escapar($filtros["estado"]);
			}*/

			if (isset($filtros["idpadre"])) {
				$cond[] = "act.idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " ASC";
				}
			} else $sql .= " ORDER BY ac.nombre ASC";
			// echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT c.idcurso,c.idioma,c.avance_secuencial,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks,pc.idproycurso,pc.idproyecto,pc.configuracion_nota, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel, 
					(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
					(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
					(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes from acad_curso c LEFT JOIN proyecto_cursos pc ON c.idcurso=pc.idcurso";

			$cond = array();
			if (isset($filtros["sql2"])) {
				$sql = "SELECT c.idcurso,c.idioma,c.avance_secuencial,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks,pc.idproycurso,pc.idproyecto,pc.configuracion_nota, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
				(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
				(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
				(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes from acad_curso c";
			}elseif(isset($filtros["sql3"])) {
					$sql = "SELECT c.idcurso,c.idioma,c.avance_secuencial,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks,pc.idproycurso,pc.idproyecto,pc.configuracion_nota from acad_curso c INNER JOIN proyecto_cursos pc on pc.idcurso=c.idcurso";
			}elseif(!empty($filtros["sqlasignados"])) { // cursos tipo 01  que fueron asignados y / no  asignados tipo=1				
				$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks,pc.idproycurso from acad_curso c INNER JOIN proyecto_cursos pc ON c.idcurso=pc.idcurso";
				if (!empty($filtros["noasignados"])) { // si solo quiere curso no asignados 
					$sql1 = "SELECT c.idcurso from acad_curso c INNER JOIN proyecto_cursos pc ON c.idcurso=pc.idcurso ";
					$sql1 .= "WHERE tipo=1 AND idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
					$idasignados = $this->oBD->consultarSQL($sql1);
					$hayid = array();
					if (!empty($idasignados)) {
						foreach ($idasignados as $k => $v) {
							$hayid[] = $v["idcurso"];
						}
						$cond[] = " c.idcurso not in(" . (implode(",", $hayid)) . ")";
					}
					$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks from acad_curso c";
					unset($filtros["idproyecto"]);
				}
				$cond[] = "c.tipo = 1 ";	 // solo para cursos tipo 1 
				// los tipo 02 tendrian que  copiarse, no se deberian asignar.			
			}

			if (isset($filtros["idcurso"])) {
				if (is_array($filtros["idcurso"]))
					$cond[] = "c.idcurso IN ('" . implode("','", $filtros["idcurso"]) . "')";
				else $cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}

			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["imagen"])) {
				$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			/*if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}*/
			if (isset($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			if (isset($filtros["complementario"])) {
				$sql = "SELECT cc.idcurso,cc.nombre,cc.imagen,cc.descripcion,cc.estado,cc.fecharegistro,cc.idusuario,cc.color,cc.objetivos,cc.certificacion,cc.costo,cc.silabo,cc.e_ini,cc.pdf,cc.abreviado,cc.e_fin,cc.aniopublicacion,cc.autor,cc.txtjson,cc.formula_evaluacion,cc.idcursoprincipal,cc.idgrupoaula,cc.idcategoria,cc.tipo,cc.idioma,cc.avance_secuencial,pc.idproycurso,pc.idproyecto,pc.configuracion_nota, (select count(idcurso) from acad_cursodetalle_complementario n WHERE n.idcurso=cc.idcurso AND tiporecurso='N') as nnivel,
					(select count(idcurso) from acad_cursodetalle_complementario u WHERE u.idcurso=cc.idcurso AND tiporecurso='U') as nunidad,
					(select count(idcurso) from acad_cursodetalle_complementario l WHERE l.idcurso=cc.idcurso AND tiporecurso='L') as nactividad,
					(select count(idcurso) from acad_cursodetalle_complementario e WHERE e.idcurso=cc.idcurso AND tiporecurso='E') as nexamenes from acad_curso_complementario cc LEFT JOIN proyecto_cursos pc ON pc.idcurso=" . $this->oBD->escapar($filtros["idcurso"]);
				$cond = array();
				if (isset($filtros["idcomplementario"])) {
					$cond[] = "cc.idcurso = " . $this->oBD->escapar($filtros["idcomplementario"]);
				}
				// if(isset($filtros["idcomplementario"])) {
				// 	$cond[] = "cc.idcurso = " . $this->oBD->escapar($filtros["idcomplementario"]);
				// }
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " ASC";
				}
			} else {
				$sql .= " ORDER BY nombre ASC";
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql;exit();
			//exit($sql);
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function cursos($filtros)
	{
		try {
			$cond = array();

			$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks FROM acad_curso c";
			if (!empty($filtros["complementarios"])) {
				$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.objetivos,c.certificacion,c.costo,c.silabo,c.e_ini,c.pdf,c.abreviado,c.e_fin,c.aniopublicacion,c.autor,c.txtjson,c.formula_evaluacion,c.idcursoprincipal,c.idgrupoaula,c.idcategoria,c.tipo,c.idioma,c.avance_secuencial FROM acad_curso_complementario c";
				if (isset($filtros["idcursoprincipal"])) {
					if (is_array($filtros["idcursoprincipal"]))
						$cond[] = "c.idcursoprincipal IN ('" . implode("','", $filtros["idcursoprincipal"]) . "')";
					else $cond[] = "c.idcursoprincipal = " . $this->oBD->escapar($filtros["idcursoprincipal"]);
				}

				if (!empty($filtros['tipo'])) unset($filtros['tipo']);
				if (!empty($filtros['sks'])) unset($filtros['sks']);
			} else if (isset($filtros["idproyecto"])) {
				$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks,pc.idproycurso,pc.idproyecto,pc.configuracion_nota from acad_curso c INNER JOIN proyecto_cursos pc on pc.idcurso=c.idcurso ";
				$cond[] = 'pc.idproyecto=' . $filtros['idproyecto'];
				$filtros["orderby"] = 'pc.orden';
			}

			if (isset($filtros["idcurso"])) {
				if (is_array($filtros["idcurso"]))
					$cond[] = "c.idcurso IN ('" . implode("','", $filtros["idcurso"]) . "')";
				else $cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}

			if (isset($filtros["noidcurso"])) {
				if (is_array($filtros["noidcurso"]))
					$cond[] = "c.idcurso NOT IN ('" . implode("','", $filtros["noidcurso"]) . "')";
				else $cond[] = "c.idcurso != " . $this->oBD->escapar($filtros["noidcurso"]);
			}

			if (isset($filtros['nombre'])) {
				$cond[] = 'c.nombre ' . $this->oDB->like($filtros['nombre']);
			}

			if (isset($filtros['descripcion'])) {
				$cond[] = 'c.descripcion=' . $this->oDB->escapar($filtros['descripcion']);
			}
			/*if(isset($filtros['estado'])){
				$cond[]='c.estado	>= '.$filtros['estado'];
			}else $cond[]='c.estado	>= 1';*/

			if (isset($filtros['fecharegistro'])) {
				$cond[] = 'c.fecharegistro	=' . $this->oDB->escapar($filtros['fecharegistro']);
			}
			if (isset($filtros['idusuario'])) {
				$cond[] = 'c.idusuario	=' . $this->oDB->escapar($filtros['idusuario']);
			}
			if (isset($filtros['certificacion'])) {
				$cond[] = 'c.certificacion	=' . $this->oDB->escapar($filtros['certificacion']);
			}
			if (isset($filtros['costo'])) {
				$cond[] = 'c.costo=' . $this->oDB->escapar($filtros['costo']);
			}
			if (isset($filtros['e_ini'])) {
				$cond[] = 'c.e_ini=' . $this->oDB->escapar($filtros['e_ini']);
			}
			if (isset($filtros['abreviado'])) {
				$cond[] = 'c.abreviado=' . $this->oDB->escapar($filtros['abreviado']);
			}
			if (isset($filtros['e_fin'])) {
				$cond[] = 'c.e_fin=' . $this->oDB->escapar($filtros['e_fin']);
			}
			if (isset($filtros['aniopublicacion'])) {
				$cond[] = 'c.aniopublicacion=' . $this->oDB->escapar($filtros['aniopublicacion']);
			}
			if (isset($filtros['autor'])) {
				$cond[] = 'c.autor=' . $this->oDB->like($filtros['autor']);
			}
			if (isset($filtros['tipo'])) {
				$cond[] = 'c.tipo=' . $filtros['tipo'];
			}
			if (isset($filtros['sks'])) {
				$cond[] = 'c.sks=' . $this->oDB->escapar($filtros['sks']);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " ASC";
				}
			} else {
				$sql .= " ORDER BY nombre ASC";
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("curso") . ": " . $e->getMessage());
		}
	}

	public function duplicarcurso($idcurso, $idcc, $tipo, $idproyecto, $idusuario, $temas, $nombrecurso = 'curso sin nombre')
	{
		try {
			if ($idcc == 0) {
				$sql = "SELECT nombre,imagen,descripcion,vinculosaprendizajes,materialesyrecursos,color,objetivos,certificacion,costo,silabo,e_ini,pdf,abreviado,e_fin,autor,
				txtjson,formula_evaluacion,idioma,avance_secuencial FROM acad_curso WHERE idcurso=" . $idcurso . "";
			} else {
				$sql = "SELECT nombre,imagen,descripcion,vinculosaprendizajes,materialesyrecursos,color,objetivos,certificacion,costo,silabo,e_ini,pdf,abreviado,e_fin,autor,
				txtjson,formula_evaluacion,idioma,avance_secuencial FROM acad_curso_complementario WHERE idcurso=" . $idcc . " AND idcursoprincipal=" . $idcurso . "";
			}
			$curso = $this->oBD->consultarSQL($sql);
			if (empty($curso[0])) {
				echo json_encode(array('code' => 'Error', 'msj' => JrTexto::_('Curso no encontrado ' . $sql)));
				exit();
			}
			$curso = $curso[0];
			$estados = array();
			//$nombrecurso='';
			$descripcioncurso = '';
			foreach ($curso as $key => $v) {
				$estados[$key] = $v;
				if ($key == 'descripcion') $descripcioncurso = $v;
			}
			$estados['nombre'] = $nombrecurso;
			$estados["estado"] = 1;
			$estados["idusuario"] = $idusuario;
			$estados["estado"] = 1;
			$estados["e_fin"] = intval($estados["e_fin"]);
			$estados["e_ini"] = intval($estados["e_ini"]);
			$estados["tipo"] = $tipo;
			$estados["fecharegistro"] = date('Y-m-d');
			$estados["aniopublicacion"] = date('Y-m-d');
			$idnewcurso = $this->oBD->consultarEscalarSQL("SELECT idcurso+1 FROM acad_curso ORDER BY idcurso DESC limit 0,1");
			$estados["idcurso"] = $idnewcurso;
			$this->oBD->insert('acad_curso', $estados);
			$estados2 = array();
			$estados2["configuracion_nota"] = '';
			$estados2["orden"] = 1;
			if ($idcc == 0) {
				$sql = "SELECT configuracion_nota, orden FROM proyecto_cursos WHERE idcurso=" . $idcurso . " ";
				$cursoproy = $this->oBD->consultarSQL($sql);
				if (!empty($cursoproy[0])) {
					$estados2["configuracion_nota"] = $cursoproy[0]["configuracion_nota"];
					$estados2["orden"] = $cursoproy[0]["orden"];
				}
			}
			$idnewcursoproy = $this->oBD->consultarEscalarSQL("SELECT idproycurso+1 FROM proyecto_cursos ORDER BY idproycurso DESC limit 0,1");
			$estados2["idproycurso"] = $idnewcursoproy;
			$estados2["idproyecto"] = $idproyecto;
			$estados2["idcurso"] = $idnewcurso;
			$this->oBD->insert('proyecto_cursos', $estados2);
			$this->duplicartemas($temas, $idnewcurso, $idusuario, 0, 0);
			return array('idcurso' => $idnewcurso, 'idproycurso' => $idnewcursoproy, 'nombrecurso' => $nombrecurso, 'descripcioncurso' => $descripcioncurso);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("copiar curso") . ": " . $e->getMessage());
		}
	}


	public function reordenar($idproyecto, $datos)
	{
		try {
			foreach ($datos as $k => $d) {
				$tipo = $d["tipo"];
				$id = intval($d["id"]);
				$orden = intval($d["orden"]);
				if ($tipo == 'categoria' || $tipo == 'modulo') {
					$this->oBD->update('acad_categorias', array('orden' => $orden), array('idcategoria' => $id, 'idproyecto' => $idproyecto));
				} else if ($tipo == 'curso') {
					$this->oBD->update('proyecto_cursos', array('orden' => $orden), array('idcurso' => $id, 'idproyecto' => $idproyecto));
				}
			}
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("copiar curso") . ": " . $e->getMessage());
		}
	}

	private function duplicartemas($temas, $idcurso, $idusuario, $idnivelpadre = 0, $idcursodetallepadre = 0)
	{
		foreach ($temas as $key => $tem) {
			$estadoNivel = array(
				'nombre' => $tem["nombre"],
				'tipo' => $tem['tiporecurso'],
				'idpadre' => $idnivelpadre,
				'idpersonal' => $idusuario,
				'estado' => 1,
				'orden' => intval($tem['orden'])
			);
			$idnivel = $this->oBD->consultarEscalarSQL("SELECT idnivel+1 FROM niveles ORDER BY idnivel DESC limit 0,1");
			//var_dump($idnivel);
			$estadoNivel["idnivel"] = $idnivel;
			$this->oBD->insert('niveles', $estadoNivel);
			$cursodetalle = array(
				'idcurso' => $idcurso,
				'orden' => $tem["orden"],
				'idrecurso' => $idnivel,
				'tiporecurso' => $tem["tiporecurso"],
				'idlogro' => $tem["idlogro"],
				'url' => $tem["url"],
				'idpadre' => $idcursodetallepadre,
				'color' => $tem["color"],
				'esfinal' => $tem["esfinal"],
				'txtjson' => $tem["txtjson"],
				'espadre' => $tem["espadre"]
			);
			$idcursodetalle = $this->oBD->consultarEscalarSQL("SELECT idcursodetalle+1 FROM acad_cursodetalle ORDER BY idcursodetalle DESC limit 0,1");
			$cursodetalle["idcursodetalle"] = $idcursodetalle;
			$this->oBD->insert('acad_cursodetalle', $cursodetalle);
			if (!empty($tem['hijos']))
				$this->duplicartemas($tem['hijos'], $idcurso, $idusuario, $idnivel, $idcursodetalle);
		}
	}

	public function getCursos_Not_In_GrupoAulaDetalle($filtros = null, $iddocente)
	{
		try {
			$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks, PC.idproyecto FROM acad_curso c JOIN proyecto_cursos PC ON c.idcurso=PC.idcurso";

			$cond = array();

			if (isset($filtros["idcurso"])) {
				$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "c.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["imagen"])) {
				$cond[] = "c.imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "c.descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			/*if(isset($filtros["estado"])) {
				$cond[] = "c.estado = " . $this->oBD->escapar($filtros["estado"]);
			}*/
			if (isset($filtros["fecharegistro"])) {
				$cond[] = "c.fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "c.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["idproyecto"])) {
				$cond[] = "PC.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}

			$cond[] = "c.idcurso NOT IN (
				SELECT idcurso FROM acad_grupoauladetalle WHERE iddocente=" . $this->oBD->escapar($iddocente) . "
			)";

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " ASC";
				}
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo '<p><b>' . $sql . '</b></p>';

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}


	public function insertar($nombre, $imagen, $descripcion, $estado, $fecharegistro, $idusuario, $vinculosaprendizajes, $materialesyrecursos, $color, $abreviado, $autor, $aniopublicacion, $tipo, $sks)
	{
		try {

			$this->iniciarTransaccion('dat_acad_curso_insert');
			$id = $this->oBD->consultarEscalarSQL("SELECT idcurso FROM acad_curso ORDER BY idcurso DESC limit 0,1");
			++$id;
			$estados = array(
				'idcurso' => $id, 'nombre' => $nombre, 'imagen' => $imagen, 'descripcion' => $descripcion, 'estado' => !empty($estado) ? $estado : 0, 'fecharegistro' => !empty($fecharegistro) ? $fecharegistro : date('Y-m-d H:i:s'), 'idusuario' => $idusuario, 'vinculosaprendizajes' => !empty($vinculosaprendizajes) ? $vinculosaprendizajes : '', 'materialesyrecursos' => !empty($materialesyrecursos) ? $materialesyrecursos : '', 'color' => !empty($color) ? $color : '', 'abreviado' => !empty($abreviado) ? $abreviado : '', 'autor' => !empty($autor) ? $autor : '', 'aniopublicacion' => !empty($aniopublicacion) ? $aniopublicacion : date('Y-m-d'), 'txtjson' => '', 'tipo' => $tipo, 'sks' => $sks
			);

			$this->oBD->insert('acad_curso', $estados);
			$this->terminarTransaccion('dat_acad_curso_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_acad_curso_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function actualizar($id, $nombre, $imagen, $descripcion, $estado, $fecharegistro, $idusuario, $vinculosaprendizajes, $materialesyrecursos, $color, $abreviado, $autor, $aniopublicacion)
	{
		try {
			$this->iniciarTransaccion('dat_acad_curso_update');
			$estados = array(
				'nombre' => $nombre, 'imagen' => $imagen, 'descripcion' => $descripcion, 'estado' => !empty($estado) ? $estado : 0, 'fecharegistro' => $fecharegistro, 'idusuario' => $idusuario, 'vinculosaprendizajes' => $vinculosaprendizajes, 'materialesyrecursos' => $materialesyrecursos, 'color' => $color, 'abreviado' => !empty($abreviado) ? $abreviado : '', 'autor' => !empty($autor) ? $autor : '', 'aniopublicacion' => !empty($aniopublicacion) ? $aniopublicacion : date('Y-m-d')
				//,'txtjson'=>$txtjson
			);

			$this->oBD->update('acad_curso ', $estados, array('idcurso' => $id));
			$this->terminarTransaccion('dat_acad_curso_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT  c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
					(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
					(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
					(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes  FROM acad_curso  c "
				. " WHERE c.idcurso = " . $this->oBD->escapar($id);
			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}
	public function getCurso($id)
	{
		try {
			$sql = "SELECT  idcurso,nombre,imagen,descripcion,estado,fecharegistro,idusuario,color,abreviado,autor,aniopublicacion,txtjson,tipo,sks 
					FROM acad_curso "
				. " WHERE idcurso = " . $this->oBD->escapar($id);
			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}
	public function eliminar($id, $tipo)
	{
		try {
			$idremove = $id;
			if ($tipo == "1") {
				$this->oBD->delete('acad_cursocategoria', array('idcurso' => $id));
				$detalle = $this->oBD->consultarSQL("SELECT idrecurso FROM acad_cursodetalle WHERE idcurso='" . $id . "'");
				if (!empty($detalle)) {
					foreach ($detalle as $det) {
						$this->oBD->delete('niveles', array('idnivel' => $det["idrecurso"]));
					}
				}
				$this->oBD->delete('acad_cursodetalle', array('idcurso' => $id));
				$this->oBD->delete('acad_cursohabilidad', array('idcurso' => $id));
				$this->oBD->delete('acad_cursoaprendizaje', array('idcurso' => $id));
				$this->oBD->delete('acad_cursosesion', array('idcurso' => $id));
				//$this->oBD->delete('acad_sesionhtml', array('idcurso' => $id));			
				$grupos = $this->oBD->consultarSQL("SELECT idgrupoauladetalle FROM acad_grupoauladetalle WHERE idcurso='" . $id . "'");
				if (!empty($grupos))
					foreach ($grupos as $g) {
						$this->oBD->delete('acad_horariogrupodetalle', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
						$this->oBD->delete('acad_marcacion', array('idgrupodetalle' => $g["idgrupoauladetalle"]));
						$this->oBD->delete('acad_matricula', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
					}
				$this->oBD->delete('acad_grupoauladetalle', array('idcurso' => $id));
				$this->oBD->delete('bitacora_smartbook', array('idcurso' => $id));
				$this->oBD->delete('bitacora_alumno_smartbook', array('idcurso' => $id));
				$idremove = $this->oBD->delete('acad_curso', array('idcurso' => $id));
			} else {


				$this->oBD->delete('acad_cursocategoria', array('idcurso' => $id));
				$detalle = $this->oBD->consultarSQL("SELECT idrecurso FROM acad_cursodetalle WHERE idcurso='" . $id . "'");



				if (!empty($detalle)) {
					foreach ($detalle as $det) {
						$this->oBD->delete('niveles', array('idnivel' => $det["idrecurso"]));
					}
				}
				$this->oBD->delete('acad_cursodetalle', array('idcurso' => $id));
				$this->oBD->delete('acad_cursohabilidad', array('idcurso' => $id));
				$this->oBD->delete('acad_cursoaprendizaje', array('idcurso' => $id));
				$this->oBD->delete('acad_cursosesion', array('idcurso' => $id));
				//$this->oBD->delete('acad_sesionhtml', array('idcurso' => $id));			
				$grupos = $this->oBD->consultarSQL("SELECT idgrupoauladetalle FROM acad_grupoauladetalle WHERE idcurso='" . $id . "'");
				if (!empty($grupos)) {
					foreach ($grupos as $g) {
						$this->oBD->delete('acad_horariogrupodetalle', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
						$this->oBD->delete('acad_marcacion', array('idgrupodetalle' => $g["idgrupoauladetalle"]));
						$this->oBD->delete('acad_matricula', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
					}
				}
				$this->oBD->delete('acad_grupoauladetalle', array('idcurso' => $id));
				$this->oBD->delete('bitacora_smartbook', array('idcurso' => $id));
				$this->oBD->delete('bitacora_alumno_smartbook', array('idcurso' => $id));
				$idremove = $this->oBD->delete('acad_curso', array('idcurso' => $id));
				$curso = $this->oBD->consultarSQL("SELECT idcurso FROM acad_curso_complementario WHERE idcursoprincipal='" . $id . "'");
				if (!empty($curso)) {
					foreach ($curso as $cur) {
						$idcurso = $cur['idcurso'];
						$detalle = $this->oBD->consultarSQL("SELECT idrecurso FROM acad_cursodetalle_complementario WHERE idcurso='" . $idcurso . "'");
						if (!empty($detalle)) {
							foreach ($detalle as $det) {
								$this->oBD->delete('niveles_complementario', array('idnivel' => $det["idrecurso"]));
							}
						}
						$this->oBD->delete('acad_cursodetalle_complementario', array('idcurso' => $idcurso));
					}
				}
				$this->oBD->delete('acad_curso_complementario', array('idcursoprincipal' => $id));
			}
			return $idremove;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			return $this->oBD->update('acad_curso', array($propiedad => $valor), array('idcurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}
	public function set_($id, $propiedad, $valor, $tabla)
	{ //02.01.13
		try {
			return $this->oBD->update($tabla, array($propiedad => $valor), array('idcurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function insertcategoriasxcurso($idcategoria, $idcurso)
	{
		try {
			$id = $this->oBD->consultarEscalarSQL("SELECT id FROM acad_cursocategoria ORDER BY id DESC limit 0,1");
			++$id;
			$user = NegSesion::getUsuario();
			$idproy = $user['idproyecto'];
			$estados = array(
				'id' => $id, 'idcategoria' => $idcategoria, 'idcurso' => $idcurso, 'idproy' => $idproy
			);
			if (!empty($idcategoria)) $this->oBD->insert('acad_cursocategoria', $estados);

			$sql = "SELECT idgrupoauladetalle FROM `acad_grupoauladetalle` agd INNER JOIN acad_grupoaula ag on agd.idgrupoaula=ag.idgrupoaula WHERE idcurso=$idcurso AND idproyecto=$idproy";
			$grupos = $this->oBD->consultarSQL($sql);
			if (!empty($grupos))
				foreach ($grupos as $key => $gru) {
					$this->oBD->update('acad_matricula', array('idcategoria' => $idcategoria), array('idgrupoauladetalle' => $gru["idgrupoauladetalle"]));
				}
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}
	public function updatecategoriasxcurso($id, $idcategoria, $idcurso)
	{
		try {
			$user = NegSesion::getUsuario();
			$idproy = $user['idproyecto'];
			$estados = array('idcategoria' => $idcategoria, 'idcurso' => $idcurso, 'idproy' => $idproy);
			$idafectado = $this->oBD->update('acad_cursocategoria', $estados, array('id' => $id));
			$sql = "SELECT idgrupoauladetalle FROM `acad_grupoauladetalle` agd INNER JOIN acad_grupoaula ag on agd.idgrupoaula=ag.idgrupoaula WHERE idcurso=$idcurso AND idproyecto=$idproy";
			$grupos = $this->oBD->consultarSQL($sql);
			if (!empty($grupos))
				foreach ($grupos as $key => $gru) {
					$this->oBD->update('acad_matricula', array('idcategoria' => $idcategoria), array('idgrupoauladetalle' => $gru["idgrupoauladetalle"]));
				}
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function updatecategoriaextension($idcategoria, $idcurso, $idcc = 0)
	{
		try {
			$estados = array('idcategoria' => !empty($idcategoria) ? $idcategoria : 'null');
			$cond = array();
			//if(!empty($idcategoria)) $cond["idcurso"]=$idcc;
			if (!empty($idcurso)) {
				$cond["idcursoprincipal"] = $idcurso;
				$this->oBD->update('acad_curso_complementario', $estados, $cond);
			}
			return $idcurso;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function eliminarcategoriasxcurso($id)
	{
		try {
			$this->oBD->delete('acad_cursocategoria', array('id' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}
	public function eliminarcategoriaxcursoxproyecto($idcurso, $idproyecto)
	{
		try {
			$this->oBD->delete('acad_cursocategoria', array('idcurso' => $idcurso, 'idproy' => $idproyecto));
			$sql = "SELECT idgrupoauladetalle FROM `acad_grupoauladetalle` agd INNER JOIN acad_grupoaula ag on agd.idgrupoaula=ag.idgrupoaula WHERE idcurso=$idcurso AND idproyecto=$idproyecto";
			$grupos = $this->oBD->consultarSQL($sql);
			if (!empty($grupos))
				foreach ($grupos as $key => $gru) {
					$this->oBD->update('acad_matricula', array('idcategoria' => 0), array('idgrupoauladetalle' => $gru["idgrupoauladetalle"]));
				}
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function todos($filtros = null)
	{
		try {
			$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks from acad_curso c";
			$cond = array();
			if (isset($filtros["idcurso"])) {
				$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			/*if(isset($filtros["estado"])){
					$cond[] = "c.estado = " . $this->oBD->escapar($filtros["estado"]);
			}*/
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function buscarcategoriascursos($filtros)
	{
		try {
			$sql = "SELECT c.id,c.idcategoria,c.idcurso,c.idproy from acad_cursocategoria c";
			$cond = array();
			if (isset($filtros["idcurso"])) {
				$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["idcategoria"])) {
				$cond[] = "c.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if (isset($filtros["idproy"])) {
				$cond[] = "c.idproy = " . $this->oBD->escapar($filtros["idproy"]);
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function buscarcursos($filtros=null){

		try {
			$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks from acad_curso c";
			$cond = array();
			if (isset($filtros["idcurso"])) {
				$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if (isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if (isset($filtros["imagen"])) {
				$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if (isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			$haycategoria = false;
			if (isset($filtros["idcategoria"])) {
				$haycategoria = true;
				$sql = "SELECT DISTINCT(c.idcurso), c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks, cc.id,cc.idcategoria,cc.idproy from acad_curso c INNER JOIN acad_cursocategoria cc ON c.idcurso=cc.idcurso";
				$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}

			if (isset($filtros["idproyecto"])) {
				if (empty($filtros["noidproyecto"])) { //Cursos Asignados.
					$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks,pc.idproycurso,acc.idcategoria from acad_curso c INNER JOIN proyecto_cursos pc ON c.idcurso=pc.idcurso LEFT JOIN acad_cursocategoria acc ON acc.idcurso = c.idcurso";
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
					if (!isset($filtros["orderby"])) {
						$filtros["orderby"] = 'pc.orden';
					}
				} else { // listar cursos no asignados		
					if ($filtros["noidproyecto"] == 'todos') {
						$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks, NULL as idproyecto, NULL as idproycurso from acad_curso c";
						if (!empty($filtros["idmiscursos"]))	$cond[] = " c.idcurso not in(" . (implode(",", $filtros["idmiscursos"])) . ")";
					}
				}
				if ($haycategoria == true) {
					if (empty($filtros["noidproyecto"])) {
						$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks,cc.id,cc.idcategoria,cc.idproy,pc.idproycurso from acad_curso  c INNER JOIN acad_cursocategoria cc ON c.idcurso=cc.idcurso INNER JOIN proyecto_cursos pc ON c.idcurso=pc.idcurso ";
						if (!isset($filtros["orderby"])) {
							$filtros["orderby"] = 'pc.orden';
						}
						//$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
					} else {
						$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.abreviado,c.autor,c.aniopublicacion,c.txtjson,c.tipo,c.sks,cc.*, NULL as idproyecto, NULL as idproycurso from acad_curso  c INNER JOIN acad_cursocategoria cc ON c.idcurso=cc.idcurso";
						if (!empty($filtros["idmiscursos"])) $cond[] = " c.idcurso not in(" . (implode(",", $filtros["idmiscursos"])) . ")";
					}
				}
			}

			if (isset($filtros["complementario"])) {
				$sql = "SELECT c.idcurso,c.nombre,c.imagen,c.descripcion,c.estado,c.fecharegistro,c.idusuario,c.color,c.objetivos,c.certificacion,c.costo,c.silabo,c.e_ini,c.pdf,c.abreviado,c.e_fin,c.aniopublicacion,c.autor,c.txtjson,c.formula_evaluacion,c.idcursoprincipal,c.idgrupoaula,c.idcategoria,c.tipo,c.idioma,c.avance_secuencial FROM acad_curso_complementario c";
				$cond = array();
				if (isset($filtros["idcurso"])) {
					$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
				}
				if (isset($filtros["idgrupoaula"])) {
					$cond[] = "c.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
				}
				if (isset($filtros["idcategoria"])) {
					$cond[] = "c.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
				}
				if (isset($filtros["idcursoprincipal"])) {
					$cond[] = "c.idcursoprincipal = " . $this->oBD->escapar($filtros["idcursoprincipal"]);
				}
			}

			if (isset($filtros["texto"])) {
				$cond[] = "c.nombre " .  $this->oBD->like($filtros["texto"]);
			}
			if (isset($filtros["edukt"])) {
				$cond[] = "c.tipo = 1 OR (c.tipo = 2 AND c.sks = 0)";
			}
			if (isset($filtros["eduktmaestro"])) {
				$cond[] = "c.tipo = 2 AND c.sks = 1";
			}
			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$groupby = true;
			if (isset($filtros["groupby"])) {
				$groupby = $filtros["groupby"];
			}
			if ($groupby) {
				$sql .= ' GROUP BY c.idcurso';
			}
			$orderby_ = "ASC";
			if (isset($filtros["orderby_"])) {
				$orderby_ = $filtros["orderby_"];
			}
			if (!empty($filtros["orderby"])) {
				if ($filtros["orderby"] != 'no') {
					$sql .= " ORDER BY " . $filtros["orderby"] . " " . $orderby_;
				}
			} else $sql .= " ORDER BY estado DESC,nombre ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Acad_curso") . ": " . $e->getMessage());
		}
	}

	public function eliminarcurso($filtro = null)
	{
		try {
			$idcurso = $filtro["idcurso"];
			$idcc = $filtro["idcc"];
			$tipo = $filtro["tipo"];
			$idproyecto = $filtro["idproyecto"];
			$borrarbitacoras = array('idcurso' => $idcurso);
			$borrarbitacorasSE = array('idcurso_sc' => $idcurso);
			$grupos = " idcurso='" . $idcurso . "' ";
			$cursocomplementarios = " idcursoprincipal='" . $idcurso . "' ";
			$cursocomplementarios_ = array('idcursoprincipal' => $idcurso);
			$this->iniciarTransaccion('dat_acad_curso_eliminar');
			if (empty($idcc)) {
				$this->oBD->delete('acad_cursocategoria', array('idcurso' => $idcurso, 'idproy' => $idproyecto));
				$this->oBD->delete('proyecto_cursos', array('idcurso' => $idcurso, 'idproyecto' => $idproyecto));
				if (!empty($filtro["quitardelaempresa"])) {
					$this->terminarTransaccion('dat_acad_curso_eliminar');
					return $idcurso;
				}
				$this->oBD->delete('acad_cursocategoria', array('idcurso' => $idcurso));
				$this->oBD->delete('acad_criterios', array('idcurso' => $idcurso));
				$this->oBD->delete('acad_capacidades', array('idcurso' => $idcurso));
				//$this->oBD->delete('notas_quiz', array('idcurso' => $idcurso));
				$this->oBD->delete('proyecto_cursos', array('idcurso' => $idcurso));
				$this->oBD->delete('acad_cursodetalle', array('idcurso' => $idcurso));
				$this->oBD->delete('cron_tarea_proy', array('idcurso' => $idcurso));
				$detalle = $this->oBD->consultarSQL("SELECT idrecurso FROM acad_cursodetalle  WHERE idcurso='" . $idcurso . "'");

				if (!empty($detalle)) {
					foreach ($detalle as $det) {
						$this->oBD->delete('niveles', array('idnivel' => $det["idrecurso"]));
					}
				}
			} else {
				$borrarbitacoras["idcomplementario"] = $idcc;
				$borrarbitacorasSE["idcc"] = $idcc;
				$cursocomplementarios .= " AND idcurso='" . $idcc . "' ";
				$cursocomplementarios_["idcurso"] = $idcc;
				$grupos = " AND idcomplementario=" . $idcc;
			}

			$this->oBD->delete('bitacora_smartbook', $borrarbitacoras);
			$this->oBD->delete('bitacora_alumno_smartbook', $borrarbitacoras);
			$this->oBD->delete('bitacora_smartbook_se', $borrarbitacorasSE);
			$this->oBD->delete('bitacora_alumno_smartbook_se', $borrarbitacorasSE);
			$this->oBD->delete('notas_quiz', $borrarbitacoras); // borrado de examenes

			$cursocomplementarios = $this->oBD->consultarSQL("SELECT idcurso FROM acad_curso_complementario  WHERE " . $cursocomplementarios);
			if (!empty($cursocomplementarios)) {
				foreach ($cursocomplementarios as $cur) {
					$idcc2 = $cur['idcurso'];
					$detalle = $this->oBD->consultarSQL("SELECT idrecurso FROM acad_cursodetalle_complementario WHERE idcurso='" . $idcc2 . "'");
					if (!empty($detalle)) {
						foreach ($detalle as $det) {
							$this->oBD->delete('niveles_complementario', array('idnivel' => $det["idrecurso"]));
						}
					}
					$this->oBD->delete('acad_cursodetalle_complementario', array('idcurso' => $idcc2));
				}
			}

			$grupos = $this->oBD->consultarSQL("SELECT idgrupoauladetalle FROM acad_grupoauladetalle WHERE " . $grupos);
			if (!empty($grupos)) {
				foreach ($grupos as $g) {
					//$this->oBD->delete('bitacora_alumno_smartbook', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
					//$this->oBD->delete('bitacora_smartbook', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
					//$this->oBD->delete('bitacora_alumno_smartbook_se', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
					$this->oBD->delete('notas_quiz', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
					$this->oBD->delete('acad_matricula', array('idgrupoauladetalle' => $g["idgrupoauladetalle"]));
				}
			}
			$this->oBD->delete('acad_grupoauladetalle', $borrarbitacoras);
			//var_dump($borrarbitacoras);
			//	exit();	
			if (!empty($idcc))
				$idremove = $this->oBD->delete('acad_curso_complementario', $cursocomplementarios_);
			else
				$idremove = $this->oBD->delete('acad_curso', array('idcurso' => $idcurso));
			$this->terminarTransaccion('dat_acad_curso_eliminar');
			return $idremove;
		} catch (Exception $ex) {
			$this->cancelarTransaccion('dat_acad_curso_eliminar');
			throw new Exception("ERROR\n" . JrTexto::_("Eliminar") . " " . JrTexto::_("Curso") . ": " . $ex->getMessage());
		}
	}

	//lista todos los grupos a los cuales ha sido asignado un curso
	public function grupos_de_curso($filtros)
	{
		try {
			if (empty($filtros["idcurso"])) {
				echo json_encode(array('code' => 'Error', 'msj' => 'Necesita el codigo del curso a verificar'));
				exit();
			}
			$sql = "SELECT be.nombre AS strempresa, ag.nombre AS strgrupoaula, count(ma.idgrupoauladetalle) AS totalmatriculados ,idcurso,agd.idcomplementario,agd.idgrupoauladetalle FROM acad_grupoauladetalle agd  INNER JOIN acad_grupoaula ag ON agd.idgrupoaula=ag.idgrupoaula INNER JOIN proyecto py ON ag.idproyecto= py.idproyecto INNER JOIN bolsa_empresas be ON py.idempresa=be.idempresa INNER JOIN acad_matricula ma ON ma.idgrupoauladetalle=agd.idgrupoauladetalle  WHERE agd.idcurso= " . $filtros["idcurso"];
			if (!empty($filtros["idcc"]))
				$sql .= " AND agd.idcomplementario=" . $filtros["idcc"];

			if (!empty($filtros["idproyecto"]))
				$sql .= " AND ag.idproyecto=" . $filtros["idproyecto"];
			$sql .= " GROUP BY agd.idgrupoauladetalle ";
			$datos = $this->oBD->consultarSQL($sql);
			return $datos;
		} catch (Exception $ex) {
			throw new Exception("ERROR\n" . JrTexto::_("Eliminar") . " " . JrTexto::_("Curso") . ": " . $ex->getMessage());
		}
	}
}
