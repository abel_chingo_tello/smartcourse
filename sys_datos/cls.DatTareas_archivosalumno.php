<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-02-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatTareas_archivosalumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Tareas_archivosalumno") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT  idtarearecurso,idtarea,media,tipo,fecha_registro,idalumno  FROM tareas_archivosalumno";

			$cond = array();
			if (isset($filtros["idtarearecurso"])) {
				$cond[] = "idtarearecurso = " . $this->oBD->escapar($filtros["idtarearecurso"]);
			}
			if (isset($filtros["idtarea"])) {
				$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if (isset($filtros["media"])) {
				$cond[] = "media = " . $this->oBD->escapar($filtros["media"]);
			}
			if (isset($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if (isset($filtros["fecha_registro"])) {
				$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if (isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if (isset($filtros["iddocente"])) {
				$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Tareas_archivosalumno") . ": " . $e->getMessage());
		}
	}


	public function insertar($idtarea, $media, $tipo, $idalumno)
	{
		try {

			$this->iniciarTransaccion('dat_tareas_archivosalumno_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT idtarearecurso FROM tareas_archivosalumno ORDER BY idtarearecurso DESC limit 0,1");
			++$id;

			$estados = array(
				'idtarearecurso' => $id, 'idtarea' => $idtarea, 'media' => $media, 'tipo' => $tipo
				//,'fecha_registro'=>$fecha_registro
				, 'idalumno' => $idalumno
			);

			$this->oBD->insert('tareas_archivosalumno', $estados);
			$this->terminarTransaccion('dat_tareas_archivosalumno_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_tareas_archivosalumno_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Tareas_archivosalumno") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idtarea, $media, $tipo, $idalumno)
	{
		try {
			$this->iniciarTransaccion('dat_tareas_archivosalumno_update');
			$estados = array(
				'idtarea' => $idtarea, 'media' => $media, 'tipo' => $tipo
				//,'fecha_registro'=>$fecha_registro
				, 'idalumno' => $idalumno
			);

			$this->oBD->update('tareas_archivosalumno ', $estados, array('idtarearecurso' => $id));
			$this->terminarTransaccion('dat_tareas_archivosalumno_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Tareas_archivosalumno") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  idtarearecurso,idtarea,media,tipo,fecha_registro,idalumno  FROM tareas_archivosalumno  "
				. " WHERE idtarearecurso = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Tareas_archivosalumno") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tareas_archivosalumno', array('idtarearecurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Tareas_archivosalumno") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('tareas_archivosalumno', array($propiedad => $valor), array('idtarearecurso' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Tareas_archivosalumno") . ": " . $e->getMessage());
		}
	}
}
