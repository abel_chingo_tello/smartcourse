<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-02-2020  
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class DatTareas_mensajes extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("connect") . " " . JrTexto::_("Tareas_mensajes") . ": " . $e->getMessage());
		}
	}

	public function buscar($filtros = null)
	{
		try {
			$sql = "SELECT t.idtareamensaje,t.idtarea,t.idusuario,t.rol,t.mensaje,t.fecha_registro,t.estado ,concat(ape_paterno,' ',ape_materno,', ',p.nombre)as fullnombre,usuario,foto FROM tareas_mensajes t INNER JOIN personal p on idusuario=idpersona";

			$cond = array();


			if (isset($filtros["idtareamensaje"])) {
				$cond[] = "idtareamensaje = " . $this->oBD->escapar($filtros["idtareamensaje"]);
			}
			if (isset($filtros["idtarea"])) {
				$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if (isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if (isset($filtros["rol"])) {
				$cond[] = "t.rol = " . $this->oBD->escapar($filtros["rol"]);
			}
			if (isset($filtros["mensaje"])) {
				$cond[] = "mensaje = " . $this->oBD->escapar($filtros["mensaje"]);
			}
			if (isset($filtros["estado"])) {
				$cond[] = "t.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if (isset($filtros["texto"])) {
				//$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}

			if (!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Search") . " " . JrTexto::_("Tareas_mensajes") . ": " . $e->getMessage());
		}
	}


	public function insertar($idtarea, $idusuario, $rol, $mensaje, $estado = 0)
	{
		try {

			$this->iniciarTransaccion('dat_tareas_mensajes_insert');

			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtareamensaje) FROM tareas_mensajes");
			++$id;

			$estados = array(
				'idtareamensaje' => $id, 'idtarea' => $idtarea, 'idusuario' => $idusuario, 'rol' => $rol, 'mensaje' => $mensaje, 'estado' => !empty($estado) ? $estado : 0
			);

			$this->oBD->insert('tareas_mensajes', $estados);
			$this->terminarTransaccion('dat_tareas_mensajes_insert');
			return $id;
		} catch (Exception $e) {
			$this->cancelarTransaccion('dat_tareas_mensajes_insert');
			throw new Exception("ERROR\n" . JrTexto::_("Insert") . " " . JrTexto::_("Tareas_mensajes") . ": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idtarea, $idusuario, $rol, $mensaje, $estado)
	{
		try {
			$this->iniciarTransaccion('dat_tareas_mensajes_update');
			$estados = array(
				'idtarea' => $idtarea, 'idusuario' => $idusuario, 'rol' => $rol, 'mensaje' => $mensaje, 'estado' => !empty($estado) ? $estado : 0
			);

			$this->oBD->update('tareas_mensajes ', $estados, array('idtareamensaje' => $id));
			$this->terminarTransaccion('dat_tareas_mensajes_update');
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Tareas_mensajes") . ": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT idtareamensaje,idtarea,idusuario,rol,mensaje,fecha_registro,estado  FROM tareas_mensajes  "
				. " WHERE idtareamensaje = " . $this->oBD->escapar($id);

			$res = $this->oBD->consultarSQL($sql);

			return empty($res) ? null : $res[0];
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Get") . " " . JrTexto::_("Tareas_mensajes") . ": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tareas_mensajes', array('idtareamensaje' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Delete") . " " . JrTexto::_("Tareas_mensajes") . ": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{ //02.01.13
		try {
			$this->oBD->update('tareas_mensajes', array($propiedad => $valor), array('idtareamensaje' => $id));
		} catch (Exception $e) {
			throw new Exception("ERROR\n" . JrTexto::_("Update") . " " . JrTexto::_("Tareas_mensajes") . ": " . $e->getMessage());
		}
	}
}
