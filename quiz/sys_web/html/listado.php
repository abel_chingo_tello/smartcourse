<style type="text/css">
	a p.botones{
		white-space: normal;
		min-height: 40px;
	}
	.exa-item {
	    border: 1px solid #337ab7;
	    border-radius: 0.5ex;
	    position: relative;
	    min-height: 152px;
	    margin-bottom: 1ex;
	}
	.exa-item .titulo {
		background-color: #fffffe94;
	    min-height: 50px;
	    position: absolute;	   
	    font-weight: bolder;
	    min-height: 1.5em;
	    overflow: hidden;
	    text-align: center;
	    text-overflow: ellipsis;	   
	    width: 100%;
	}
	.exa-item .portada img{
		max-width: 100%;
		max-height: 150px;
	}
	.pnlexamen .btnaccion{ display: none; }
	.pnlexamen:hover .btnaccion{ display: block; }
</style>
<div class="container">
	<div class="row" id="btncreacion" style="display: none;">
		<div><br></div>
		<div class="col-xs-12 col-sm-4">			
			<a href="<?php echo $this->documento->getUrlBase() ?>quiz/examenes/editar" class="btn btn-block btn-success verexamen">			
				<div><i class="fa fa-plus fa-2x"></i></div>
				<h3 class="bolder"><?php echo ucfirst(JrTexto::_("New")) ?></h3>
				<p  class="botones"><?php echo ucfirst(JrTexto::_("Create a new assessment")) ?>.</p>
			</a>
		</div>
		<!--div class="col-xs-12 col-sm-4">
			<a href="<?php echo $this->documento->getUrlBase() ?>/examenes/ver/?acc=generar" class="btn btn-block btn-blue verexamen" style="font-size: 1em;">
				<div>
					<span class="fa-stack fa-lg fa-1x" >
						<i class="fa fa-file-o fa-stack-1x"></i>
						<i class="fa fa-cog fa-stack-1x"></i>
					</span>
				</div>
				<h3 class="bolder"><?php echo ucfirst(JrTexto::_("Generated")) ?></h3>
				<p class="botones"><?php echo ucfirst(JrTexto::_("You can select questions from other assessments")) ?>.</p>
			</a>
		</div>
		<div class="col-xs-12 col-sm-4">
			<a href="<?php echo $this->documento->getUrlBase() ?>/examenes/ver/?acc=ubicacion" class="btn btn-block btn-yellow verexamen">
				<div><i class="fa fa-compass fa-2x"></i></div>
				<h3 class="bolder"><?php echo ucfirst(JrTexto::_("Placement")) ?></h3>
				<p class="botones"><?php echo ucfirst(JrTexto::_("It gives you results of the student's initial knowledge")) ?>.</p>
			</a>
		</div-->
	</div>
	<div class="row">
		<div class="col-md-12" style="padding-top: 1em;">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="col-md-6 col-sm-6">
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="input-group">
							<input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Buscar...">
							<span role="button" class="input-group-addon btn buscar-exam"><i class="fa fa-search"></i></span>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-body table-reponsive">
					<table class="table table-striped" id="pnllisexamenes">
						<thead><tr><th></th></tr></thead>
						<body></body>
					</table>
					<div id="verexamenclone" style="display: none;">
					<div class="col-xs-6 col-sm-4 col-md-2 pnlexamen">
						<a class="_link" href="#" title="" idexamen="" iduser="">
							<div class="exa-item">
								<div class="titulo">Examen Inicial - Excel Financiero</div>
								<div class="portada"><img class="img-responsive center-block" src=""></div>
							</div>
						</a>
						<div class="btn-group btn-group-sm btnaccion" role="group" aria-label="" style="position: absolute; bottom: 0px">
						  <button type="button" class="btn btn-primary btnedit"><i class="fa fa-pencil" title="Editar" ></i></button>
						  <!--button type="button" class="btn btn-danger btnremove"><i class="fa fa-trash" title="Eliminar" ></i></button-->
						  <button type="button" class="btn btn-primary btncopy"><i class="fa fa-cc" title="Copiar" ></i></button>
						  <button type="button" class="btn btn-primary btnver"><i class="fa fa-eye" title="Ver" ></i></button>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	const idpersona_quiz='<?php echo $this->quizidpersona; ?>';
	const idrol=<?php echo $this->idrol; ?>;
	const tuser='<?php echo $this->tuser; ?>';
	$(document).ready(function(ev){
		var tablaexamenes=$('table#pnllisexamenes').DataTable(
	    { "searching": false,
	      "processing": false,      
	      "ajax":{
	        url:_sysUrlBase_+'json/quizexamenes',
	        type: "post",                
	        data:function(d){
	            d.json=true 
	            d.titulo=$('input#txtTitulo').val();
	            draw5eaba8b672583=d.draw;	           
	        },
	        dataSrc:function(json){
	          var data=json.datos;             
	          json.draw = draw5eaba8b672583;
	          //json.recordsTotal = data.length;
	          //json.recordsFiltered = data.length;	          
//https://eiger.localhost.com/smartquiz/examenes/ver/?xxestiloespecialxxx=1&amp;idpersona_smartcourse=&amp;idexamen=169//
	          var datainfo1 = new Array();	          
	          var j=0;
	          datainfo1[0]='';
	          for(var i=0;i<data.length; i++){ 
	          	$verexamen=$('#verexamenclone').clone();	          		          	
	          	$verexamen.find('a._link').attr('title',data[i].titulo||'sin titulo').attr('iduser',data[i].idpersonal).attr('idexamen',data[i].idexamen);
	          	$verexamen.find('a._link').attr('href',_sysUrlBase_+'quiz/examenes/ver/?idexamen='+data[i].idexamen);
	          	$verexamen.find('.titulo').text(data[i].titulo||'sin titulo');	          	
	          		$verexamen.find('img').attr('src',data[i].portada.replace('__xRUTABASEx__',_sysUrlBase_+'smartquiz/'));
	          	if(islogin==false)$verexamen.find('.btnedit').hide();
	          	
	          	if(tuser=='n'){
	          		if(idrol!=1){
	          			if(data[i].idpersonal!=idpersona_quiz){
	          				$verexamen.find('.btnremove').remove();
	          				if(islogin==false) $verexamen.find('.btnedit').removeAttr('style');
	          			}
	          		}else {
	          			if(data[i].idpersonal!=idpersona_quiz){
	          				$verexamen.find('.btnremove').remove();	          				
	          			}
	          		}
	          	}
	          	if(i%6==0 && i>5){
	          		j++;
	          		datainfo1[j]='';	          		
	          	}
	          	datainfo1[j]=datainfo1[j]+$verexamen.html();
	          }
	          var datainfo = new Array();	        
	          for(var i=0;i<datainfo1.length; i++){ 
	             datainfo.push([
	             	datainfo1[i]
	             ]);
	          }
	          return datainfo ;
	      }, error: function(d){console.log(d)}
	      },"language": { "url": _sysIdioma_=='es'?_sysUrlBase_+"/libs/datatable1.10/idiomas/es.json":''}
	    });

		$('input#txtTitulo').keypress(function(ev){
			var code = (ev.keyCode ? ev.keyCode : ev.which);
	        if(code==13){
	            tablaexamenes.ajax.reload();
	        }			
		})
		$('input#txtTitulo').blur(function(ev){
			tablaexamenes.ajax.reload();;
		})
		$('button.buscar-exam').click(function(ev){
			tablaexamenes.ajax.reload();;
		})
		$('#pnllisexamenes').on('click','.btnedit',function(ev){
			let idexamen=$(this).closest('.pnlexamen').children('a').attr('idexamen');
			window.location.href=_sysUrlBase_+'quiz/examenes/editar/?idexamen='+idexamen;
		}).on('click','.btnremove',function(ev){
			let idexamen=$(this).closest('.pnlexamen').children('a').attr('idexamen');
		}).on('click','.btncopy',function(ev){
			var idexamen=$(this).closest('.pnlexamen').children('a').attr('idexamen');
			Swal.fire({
			  title: 'Esta seguro de copiar este Examen?',
			  text: "Se redirigido a la edicion del examen copiado",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si copiar'
			}).then((result) => {
				console.log(result);
			  if(result.value==true) {
			    var formData = new FormData();
			    formData.append("idexamen", idexamen);
			    $.ajax({
					url: _sysUrlBase_+'json/quizexamenes/duplicar',
					type: "POST",
					data:  formData,
					contentType: false,
					dataType :'json',
					cache: false,
					processData:false,				
					success: function(rs)
					{
						if(rs.code==200){
							window.location.href=_sysUrlBase_+'quiz/examenes/editar/?idexamen='+rs.datos.idexamen;							
						}else{
							Swal.fire({
							  title: 'Error',
							  text: rs.msj,
							  icon: 'error',
							  						 
							})		
						}
					},
					error: function(xhr,status,error){		

					}               
				});
			  }
			})
		}).on('click','.btnver',function(ev){
			let idexamen=$(this).closest('.pnlexamen').children('a').attr('idexamen');
			window.location.href=_sysUrlBase_+'quiz/examenes/ver/?idexamen='+idexamen;
		}).on('click','.paginate_button',function(ev){
			
		})
		$('table#pnllisexamenes').on('page.dt',function(ev){
			if(islogin==true){
				setTimeout(function(){$('#pnllisexamenes').find('.btnedit').show()},2000);
			}
		})
		var islogin=false;
		
		var formData = new FormData();
			formData.append("idpersonal", '<?php echo $this->idusuario; ?>');
			formData.append("idpersonalquiz", idpersona_quiz);
			formData.append("idproyectoquiz", '<?php echo $this->quizidproyecto; ?>');
			formData.append("usuario", '<?php echo $this->usuario["usuario"]; ?>');
			formData.append("idproyecto", '<?php echo $this->idproyecto; ?>');
			formData.append("idrol", idrol);
			formData.append("accesodirecto", true);
			$.ajax({
				url: _sysUrlBase_+'smartquiz/sesion/',
				type: "POST",
				data:  formData,
				contentType: false,
				dataType :'json',
				cache: false,
				processData:false,				
				success: function(rs)
				{
					if(rs.code==200){
						islogin=true;
						$('#pnllisexamenes').find('.btnedit').show();
						$('#btncreacion').show();
					}
				},
				error: function(xhr,status,error){
					
				}               
			});
	})
</script>