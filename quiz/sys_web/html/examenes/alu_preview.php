<?php
$examen = $this->examen;
$preguntas=@$this->preguntas;
$rutabase = URL_SMARTQUIZ;
$arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
$srcPortada = (!empty($examen['portada']!=''))? str_replace('__xRUTABASEx__/', URL_SMARTQUIZ, $examen['portada']) : '';
$tipoEvaluacion = '';
if($examen['calificacion_en']=='N') {
    $tipoEvaluacion = ' pts.';
    $calificacion_en = ucfirst(JrTexto::_("numeric"));
} else if($examen['calificacion_en']=='P') {
    $tipoEvaluacion = '%';
    $calificacion_en = ucfirst(JrTexto::_("percentage"));
} else {
    $calificacion_en = ucfirst(JrTexto::_("alfabetic"));
}
$calif_minima = $examen["calificacion_min"];
$calif_total = $examen["calificacion_total"];
if($examen['calificacion_en']=='A'){
    $calif_minima = 51;
    $jsonEscalas = json_decode($examen["calificacion_total"], true);
    $calif_total = '<ul>';
    if(!empty($jsonEscalas) && is_array($jsonEscalas)){
        foreach ($jsonEscalas as $index=>$escala) {
            $calif_total .= '<li>'.ucfirst($escala["nombre"]).' ('.$escala["max"].' - '.$escala["min"].' pts)</li>';
        }
    }
    $calif_total .= '</ul>';
}
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_fichas.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_imagen_puntos.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_completar.css">

<input type="hidden" name="hIdExamen" id="hIdExamen" value="<?php echo @$examen['idexamen']; ?>">
<?php /*Datos de Tarea_Archivo, si es que hubiera*/ ?>
<input type="hidden" name="hIdTareaArchivo" id="hIdTareaArchivo" value="<?php echo @$this->idTareaArchivo; ?>">
<input type="hidden" name="hIdArchivoRespuesta" id="hIdArchivoRespuesta" value="<?php echo (@$this->tarea_archivo['tablapadre']=='R')?@$this->tarea_archivo['idtarea_archivos']:''; ?>">
<input type="hidden" name="hIdTareaRespuesta" id="hIdTareaRespuesta" value="<?php echo @$this->tarea_respuesta['idtarea_respuesta']; ?>">
<input type="hidden" name="datosoffline" id="datosoffline" value="<?php echo @$this->datosoffline; ?>">
<input type="hidden" name="hNroIntento" id="hNroIntento" value="<?php echo @$this->nro_intento; ?>">

<style type="text/css">
    #examen_preview{
        margin:  0;
    }
    #examen_preview .encabezado{
        border-radius: 7px;
        box-shadow: 0px 7px 6px -5px #aaa;
        margin-bottom: 3ex;
    }
    #examen_preview .pnlpresentacion {
        padding: 0 20px;
    }
    #examen_preview #portada{
        border-radius: 50%;
        max-height: 150px;
        max-width: 150px;
    }
    
    #examen_preview .title_desc-zone{
        padding: 6% 0;
    }

    #examen_preview .portada-zone,.title_desc-zone{
        display: inline-block;
        padding: 1% 0;
    }

    #examen_preview .panel{
        margin-bottom: 0;
    }

    .pnlitem , .pnlitem .tmpl-ejerc{
        display: none;
    }
    .pnlitem.active,.pnlitem .tmpl-media, .pnlitem .tmpl-ejerc.active{
        display: block;        
    }   
    .panel-body{
    	min-height: 390px;
        position: relative;
    }
    #pregunta-timer{
        position: absolute;
        right: 12px;
        z-index: 100;
        top: -25px;
        right: 0px;
    }
    #pregunta-timer .timer{
        background: #7ac1a1;
        border-radius: 50%;
        box-shadow: 0px 1px 10px -2px #444;
        display: inline-block;
        height: 80px;
        width: 80px;
    }
    #pregunta-timer .time-elapsed{
        color:  #fff;
        font-weight: bolder;
        margin-top: -0.7em;
        position: absolute;
        text-align: center;
        top: 50%;
        width: 100%;
    }

    #examen_preview .pnlpregunta .tmpl-ejerc.tiempo-acabo:before {
        background: rgba(255,255,255,0);
        content: ' ';
        height: 100%;
        position: absolute;
        width: 100%;
        z-index: 10;
    }
    .table-key-value>.table-kv-row>.key {
        width: 150px;
    }
    .table-key-value>.table-kv-row>.key:after{
        content :" : ";
        font-weight: bold;

    }
    .tmpl-media img{
        max-width: 450px !important;
        max-height: 400px !important; 
    }
    .pnlpregunta{
        min-height: 400px !important;
    }
    .pnl100{
        width: 100%;
    }
</style>
<!-- mine -->
<style type="text/css">
    body {
        height: 100vh;
    }

    body>div:first-child {
        height: 100% !important;
    }

    .panel {}

    #examen_preview .encabezado {
        display: flex;
        align-items: center;
    }

    #examen_preview .title_desc-zone {
        padding: 0 !important;

    }

    .fix-h {
        height: 100%;
    }

    .fx-col-center {
        min-height: 100%;
        display: grid;
        grid-template-columns: 1fr;
        /* flex-direction: column; */
        align-items: center;
        justify-content: center;

    }

    div.col-xs-12.col-sm-3.portada-zone>img.img-responsive#portada {
        /*min-height: auto !important;*/
        /*max-height: none !important;*/
    }

    #controlfooter {
        /* min-height: auto !important; */
        /* height: 20px !important; */
    }

    .progress {
        /* height: 12px; */
        margin-bottom: 6px;
    }

    .panel-body {
        padding-top: 6px;
    }

    .fx-tb-mx-h {
        max-height: 50vh;
        overflow-y: auto;
    }


    /* // Small devices (landscape phones, 576px and up) */
    /*@media (max-width: 576px) {
        .table-key-value>.table-kv-row>.value {

            
        }
    }*/

    /* // Medium devices (tablets, 768px and up) */
    @media (max-width: 768px) {
        .table-key-value>.table-kv-row>.value {           
           display:table-cell;
        }
        .table-key-value>.table-kv-row>.key {
            width: auto;
        }

        #examen_preview{
            padding:0px;
        }
        #examen_preview .title_desc-zone h2{
            font-size:20px;
        }
        #examen_preview #portada{         
            max-height: 100px;
            max-width: 100px;
            margin: 1px 8px;
        }

        #examen_preview .pnlpresentacion , #examen_preview .pnlpresentacion > div {
            padding: 0 0px;
            max-height: fit-content;
        }
        
        #examen_preview > div.fx-col-center{
            padding:2px;
        }

        .panel-body{
            padding-top: 6px;
            padding: 4px 0px;
        }
        .tmpl-ejerc{
            padding:2px;
        }
        .pnlpregunta {
            padding:8px 4px;
        }
        .plantilla {
            padding:6px 10px;
        }
        .plantilla.plantilla-completar{
            padding:4px 4px;
        }
        .tmpl-media img {
            max-width: 100% !important;           
        }
        
    }

    /* // Large devices (desktops, 992px and up) */
    @media (min-width: 992px) {
        img.img-responsive#portada {
            /* max-height: auto !important; */
            height: 11vh !important;
        }

    }

    /* // Extra large devices (large desktops, 1200px and up) */
    @media (min-width: 1100px) {
        img.img-responsive#portada {
            /* max-height: auto !important; */
            /* height: 20vh !important; */
        }


    }
    @media (min-width: 1200px) {
        img.img-responsive#portada {
            /* max-height: auto !important; */
            height: 22vh !important;
        }
    }
    @media (min-width: 1300px) {
        img.img-responsive#portada {
            /* max-height: auto !important; */
            height: 26vh !important;           
        }
    }
    @media (min-width: 1500px) {
        img.img-responsive#portada {
            /* max-height: auto !important; */
            height: 35vh !important;           
        }
        .col-xl-6 {
            width: 50%;
        }
    }

    @media (min-width: 1700px) {
        img.img-responsive#portada {
            /* max-height: auto !important; */
            height: 43vh !important;           
        }
    }

</style>
<!-- mine -->
<div class="row fix-h" id="examen_preview">
    <!--div class="col-xs-12">
        <a href="javascript:history.back();" class="btn btn-default atras"><i class="fa fa-arrow-left"></i> <?php echo ucfirst(JrTexto::_("Go back")) ?></a>
    </div-->
    <div class="col-md-12 fx-col-center">
        <div class="panel">
            <div class="panel-heading bg-blue">
                <div class="col-md-6">
                <div><ol class="breadcrumb" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
                      <li><a href="#" style="color:#fff"><?php echo JrTexto::_("Assessment"); ?></a></li>
                    </ol>
                </div>
                </div>
                <div class="col-md-6">
                    <span class="" id="curtimerexamen"></span>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <div id="barra-progreso" style="width: calc(100% - 74px);">
                    <div class="progress" id="progresobar2020" style="background: #ffddaf; display: none;">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            <span>0</span>%
                        </div>
                    </div>
                </div>
                <div id="pregunta-timer" style="display: none;">
                    <div class="timer"></div>
                    <div class="time-elapsed"><?php echo ($examen['tiempo_por']=='E')? $examen['tiempo_total']:'00:00:00' ?></div>
                </div>
                <div class="pnlpresentacion  pnlitem">
                    <?php if(!empty($examen)){?>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center bg-blue encabezado">
                        <?php $col = 12;
                        if($srcPortada!='') { $col = 8; ?>
                        <div class="col-xs-12 col-sm-3 portada-zone">
                            <img src="<?php echo $srcPortada; ?>" alt="img-portada" id="portada" class="img-responsive" >
                        </div>
                        <?php } ?>
                        <div class="col-xs-12 col-sm-<?php echo $col; ?> title_desc-zone">
                            <h2><?php echo ucfirst($examen["titulo"]); ?></h2>
                            <small><?php echo ucfirst($examen["descripcion"]); ?></small>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 fx-tb-mx-h">
                        <div class="table-key-value">
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Scored by")); ?></div>
                                <div class="value"><?php echo ($examen["calificacion_por"]=='Q'?ucfirst(JrTexto::_('Question')):ucfirst(JrTexto::_('Assessment'))); ?></div>
                            </div>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("type of score")); ?></div>
                                <div class="value"><?php echo $calificacion_en; ?></div>
                            </div>
                            <?php if(@$examen["calificacion_por"]!='Q'){?>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("max. score")); ?></div>
                                <div class="value"><?php echo $calif_total.$tipoEvaluacion; ?></div>
                            </div>
                            <?php } if(@$examen['calificacion_en']!='A'){?>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("min. score to pass")); ?></div>
                                <div class="value"><?php echo $calif_minima.$tipoEvaluacion; ?></div>
                            </div>
                            <?php } ?>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("number of attempts")); ?></div>
                                <div class="value"><?php echo ($examen['nintento']==$this->max_intento)?JrTexto::_("Unlimited"):($this->nro_intento." / ".$examen['nintento']); ?></div>
                            </div>
                            <?php if($examen['nintento']!=1){?>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Choose")); ?></div>
                                <div class="value"><?php echo ucfirst($examen["calificacion"]=='M'?JrTexto::_("Best try"):JrTexto::_("Last try")); ?></div>
                            </div>
                             <?php } ?>
                        </div>
                    </div>
                    <?php 
                    $habilidades_todas = json_decode(@$examen['habilidades_todas'], true);
                    if(!empty(@$habilidades_todas)){ ?>
                    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 col-xl-6 fx-tb-mx-h">
                        <div class="table-key-value">
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("objective").'s').' / '. ucfirst(JrTexto::_("skills")); ?></div>
                                <div class="value">
                                    <ul class="list-unstyled_">
                                    <?php 
                                    foreach (@$habilidades_todas as $hab) {
                                        echo '<li>'.ucfirst(JrTexto::_(  $hab['skill_name'] )).'</li>';
                                    } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="cols-xs-12 col-sm-6 col-md-6 text-center">
                        <br>
                       <a href="#" class="btn btn-lg btn-primary startexam"><?php echo JrTexto::_("Start Assessment") ?> <i class="fa fa-arrow-right"></i></a>
                   </div>                    	
               </div>
            <?php if(!empty($preguntas))
            foreach ($preguntas as $pregunta){ $codeidgui=uniqid(); ?>
            <div class="col-xs-12 col-sm-12 pnlpregunta pnlitem" style="font-family: '<?php echo $examen['fuente']; ?>'; font-size: <?php echo $examen['fuentesize'].'px'; ?>;">
            <?php 
            $nomedia=12;
            if($pregunta["F"]){
                $preg=$pregunta["F"][0];
                $tpltipo=$preg["template"];
                $tpl=($tpltipo=='image'||$tpltipo=='audio'||$tpltipo=='video')?'media':'ejerc'; 
                $nomedia=6;
                ?>
                <div class="col-xs-12 col-sm-6 tmpl tmpl-<?php echo $tpl; ?>" data-tipo="<?php echo $tpltipo; ?>" data-cls="<?php echo $tpltipo; ?>_question" data-idpregunta="<?php echo $preg["idpregunta"]; ?>" data-habilidades="<?php echo $preg["habilidades"]; ?>">
                    <?php if($tpltipo=='image'){ ?>
                    <div class="btns-img">
                        <button class="btn btn-info full-size" title="<?php echo ucfirst(JrTexto::_('Full size')); ?>"><i class="fa fa-external-link-square fa-2x"></i></button>
                    </div>
                    <?php } 
                    echo @str_replace('__xRUTABASEx__/',URL_SMARTQUIZ,@$preg["ejercicio"]); ?>
                </div>
                <?php } if($pregunta["P"]){
                    foreach ($pregunta["P"] as $preg) {
                        $tpltipo=$preg["template"];
                        $tpl=($tpltipo=='image'||$tpltipo=='audio'||$tpltipo=='video')?'media':'ejerc';?>
                        <div class="col-xs-12 col-sm-<?php echo $nomedia; ?> col-md-<?php echo $nomedia; ?> tmpl tmpl-<?php echo $tpl; ?>" data-tmpl="<?php echo $tpltipo; ?>" data-cls="<?php echo $tpltipo; ?>_question" data-idpregunta="<?php echo $preg["idpregunta"]; ?>" data-habilidades="<?php echo $preg["habilidades"]; ?>" data-tiempo="<?php echo $preg["tiempo"]; ?>" data-puntaje="<?php echo $preg["puntaje"]; ?>">
                            <?php echo @str_replace('__xRUTABASEx__/',URL_SMARTQUIZ,@$preg["ejercicio"]); ?>
                        </div>
                        <?php } } ?>
                    </div>
                    <?php } ?>                     
                    <div class="pnlresultado pnlitem">
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                          <h2><strong><?php echo ucfirst(JrTexto::_("Result")) ?></strong></h2>
                        </div> 
                        <div class="text-center">
                        <h2><?php echo @$this->usuarioAct["nombre_full"]; ?><br>
                            <small><?php echo 'P'.$this->usuarioAct["idpersona"].'C'.$this->idcurso.'CC'.$this->idcc.'E'.$this->idexamen.'I'.$this->nro_intento; ?></small><br><small><small><?php echo date('Y-m-d H:i:s') ?></small></small>
                        </h2>                       

                        </div>               	
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center" id="infototalcalificacion">

                        </div>
                        <div class="btnnoguardo text-center" style="display: none">
                            <br>
                            <hr>
                            <div class="alert alert-danger" role="alert" style="padding: 1em;">
                                <h4 class="alert-heading" style="font-size: 2em;"><?php echo ucfirst(JrTexto::_('Lo sentimos')) ?><br><small><?php echo ucfirst(JrTexto::_('surgió un error inesperado')) ?></small></h4>
                            <p class="mb-0" style="font-size: 1.54em; color: #2c3e50;">Le brindamos 3 opciones, que puede realizar</p><br>
                            <a href="#" class="btn btn-lg btn-primary btnvolveraintentar "> <i class="fa fa-refresh"></i> <?php echo JrTexto::_("Resolverlo otra vez");?></a>
                            <a href="#" class="btn btn-lg btn-danger btnintentarguardar "> <i class="fa fa-save"></i> <?php echo JrTexto::_("Intentar Guardar otra vez") ?></a>
                            <a href="#" class="btn btn-lg btn-success btnenviarpararegistro "> <i class="fa fa-envelope"></i> <?php echo JrTexto::_("Enviar al administrador") ?></a>
                            </div>
                            <hr>

                        </div>
                        <div class="text-center" id="porcesandoguardado" style="display: none">
                            <div class="alert alert-info" role="alert">
                              <h4 class="alert-heading text-center"><?php echo ucfirst(JrTexto::_('Processing data')) ?><br><small><?php echo ucfirst(JrTexto::_('please wait')) ?></small></h4>                             
                              <hr>
                              <p class="mb-0" style="font-size: 2em; color: #2c3e50;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></p>
                            </div>
                        </div>

                      <div class="col-xs-12 col-sm-12 col-md-12 text-center _resultadoporhabilidad_">
                        <hr>
                        <h3><strong><?php echo ucfirst(JrTexto::_("Results by skill")); ?></strong></h3>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12" id="resultskill">
                        <?php if(!empty($this->habilidades)){ $i=-1; 
                        foreach ($this->habilidades as $hab) {  $i++; 
                            if($i>count($arrColoresHab)){ $i=0; } ?>
                            <div class="col-xs-12 col-sm-6 padding-0 skill sk_<?php echo @$hab["skill_id"] ?>">
                                <div class="col-xs-3 col-sm-3 col-md-4 circulo" data-idskill="<?php echo @$hab["skill_id"] ?>" data-value="0" data-texto=" " data-ccolorout="<?php echo @$arrColoresHab[$i]; ?>"></div>
                                <div class="col-xs-9 col-sm-9 col-md-8 text-left bolder nombre-skill"><br><?php echo JrTexto::_($hab["skill_name"]);?></div>
                            </div>
                            <?php } 
                        }else{
                            echo "<h4 class='text-center'>No se han registrado Habilidades/Objetivos</h4>";
                        } ?>
                        </div>
                    </div>
                    

                </div>
            </div>
            <div class="panel-footer bg-blue text-center" id="controlfooter" style="min-height: 57px;">
                <div class="btn-group btn-group-left" style="display: none;">
                    <?php if(!empty($examen["retroceder"])){?>
                    <a href="#" class="btn btn-primary irprev"><i class="fa fa-arrow-left"></i></a>
                    <?php } ?>
                    <a href="#" class="btn btn-primary irinfo">-</a>
                    <a href="#" class="btn btn-primary irnext"><i class="fa fa-arrow-right"></i></a>
                </div>
                <input type="hidden" id="calificacion_por" value="<?php echo @$examen["calificacion_por"]; ?>" >
                <input type="hidden" id="calificacion_en" value="<?php echo @$examen["calificacion_en"]; ?>" >
                <textarea class="hidden" type="hidden" id="calificacion_total"><?php echo @$examen["calificacion_total"]; ?></textarea>
                <input type="hidden" id="calificacion_min" value="<?php echo @$examen["calificacion_min"]; ?>">
                <input type="hidden" id="calificacion_pregunta" value="0">
                <input type="hidden" id="calificacion_acumulada" value="0">
            </div>
        </div>
    </div>
</div>

<section id="msjes_idioma_php" class="hidden" style="display: none !important; ">
    <input type="hidden" id="you_failed_exam" value='<?php echo ucfirst(JrTexto::_('You failed the assessment')); ?>'>
    <input type="hidden" id="you_passed_exam" value='<?php echo ucfirst(JrTexto::_('You passed the assessment')); ?>'>
    <input type="hidden" id="your_score_below" value='<?php echo ucfirst(JrTexto::_('Your score is below the preset scales')); ?>'>
    <input type="hidden" id="out_of" value='<?php echo JrTexto::_('out of'); ?>'>
    <input type="hidden" id="record" value='<?php echo JrTexto::_('Record'); ?>'>
    <input type="hidden" id="guardar_examen" value='<?php echo JrTexto::_('End and save exam'); ?>'>
    <input type="hidden" id="your_result_in_scale" value='<?php echo ucfirst(JrTexto::_('your result is located on the scale')); ?>'>
    <input type="hidden" id="preguntas_por_revisar" value='<b><?php echo ucfirst(JrTexto::_('Remember'))."</b> ".ucfirst(JrTexto::_('the percentage reflected in this email is not your final grade')).". ".ucfirst(JrTexto::_('some writing questions and all the speaking ones still have to be reviewed by your teacher')).". ".ucfirst(Jrtexto::_('After having qualified them, your teacher will inform you that you can now verify your final grade in the platform’s grade report')); ?>.'>
</section>

<script type="text/javascript">
    var _con_ = {horas: 1, minutos: 30, segundos: 0};
    localStorage.setItem("conteo", JSON.stringify(_con_));
    localStorage.setItem("conteoInicial", JSON.stringify(_con_));
</script>

<script type="text/javascript">
var _TIEMPO_POR= '<?php echo $examen['tiempo_por'] ?>';
var _IDHistorialSesion = 0;
const CALLBACK = '<?php echo !empty($this->usuarioAct['callback']) ?$this->usuarioAct['callback'] :''?>';
var habtodas='<?php echo $examen["habilidades_todas"] ?>';
var hayhab=<?php echo $examen["habilidades_todas"]=="[]"||$examen["habilidades_todas"]=="{}"||empty($examen["habilidades_todas"])?'false':'true';?>;
var ttotal='<?php echo $examen["tiempo_total"] ?>';
var ccalif='<?php echo $examen["calificacion"] ?>';


var infopregunta=function(){
    var total= $('.panel-body .tmpl-ejerc').length;
    var indice=$('.panel-body .tmpl-ejerc.active .titulo_descripcion .titulo').attr('data-orden')||0;
    $('#controlfooter a.irnext').html('<i class="fa fa-arrow-right"></i>');
    if(indice>0){
        $('#controlfooter a.irinfo').html(indice+' / '+total);
        $('#controlfooter a').show();
        if(indice==1){
            $('#controlfooter a.irprev').hide();
        }else if(indice==total){
            $('#controlfooter a.irnext').html(MSJES_PHP.guardar_examen + ' <i class="fa fa-arrow-right"></i>');
        }
    }else{ 
        $('#controlfooter a').hide();
        $('#progresobar2020').hide();
    }
};

var intentodeguardado=0;
var urlsave=_sysUrlBase_+'notas_quiz/guardarNotas_quiz';
var procesandoguardando=false;
var guardarResultado = function(){
    var guardar=<?php echo !empty($this->guardar)?1:0;?>;
    if(guardar==0){
            $('#infototalcalificacion').show();
            $('#infototalcalificacion').append('<br><strong>En vista previa de Exámen los resultados no seran guardados</strong>');
            $('._resultadoporhabilidad_').show();
        __notificar({type:'info',title:'Desde la vista previa no se guardara su resultado'});
     return false;
    }
    var _con_ = {horas: 0, minutos: 30, segundos: 0};    
    localStorage.setItem("conteo", JSON.stringify(_con_));
    localStorage.setItem("conteoInicial", JSON.stringify(_con_));
    $('#infototalcalificacion').hide();
    $('._resultadoporhabilidad_').hide();
    $('.btnnoguardo').hide();
    $('#porcesandoguardado').show();
    
    var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
    var jsonPreguntas = {};
    var jsonpreguntasoffline={};
    procesandoguardando=true;
    $('.panel-body .pnlpregunta .tmpl').each(function(index, elem) {       
        var html = $(this).wrap('<div id="wrapper-tmpl"></div>');
        var htmloffline=$(elem).children('.plantilla');
        html = $('#wrapper-tmpl').html();
        $(this).unwrap();       
        if(htmloffline.hasClass('plantilla-nlsw')){
            jsonpreguntasoffline[index]=index;
        }                
        jsonPreguntas[index]=html;
        // jsonPreguntas[index]=window.btoa(unescape(encodeURIComponent(html)));
    });
    var preguntas = JSON.stringify(jsonPreguntas);
    var preguntasoffline = JSON.stringify(jsonpreguntasoffline);
    var resultado = $('.panel-body .pnlresultado #infototalcalificacion').html();
    var jsonPtjeHab = {};
    $('.panel-body .pnlresultado #resultskill .skill .circulo').each(function(){
        var idskill = $(this).attr('data-idskill');
        var value = $(this).attr('data-value');
        jsonPtjeHab[idskill] = value;
    });
    var puntajeXHab = JSON.stringify(jsonPtjeHab);
    var puntaje = $('.panel-body .pnlresultado #infototalcalificacion').attr('data-puntaje');
    var resultadoJson = '{}';
    var tiempoDuracion = $('#curtimerexamen').text();
    var formData = new FormData();
   
    formData.append("idexamen", $('#hIdExamen').val());

    formData.append("idrecurso", $('#hIdExamen').val());
    formData.append("datosoffline", $('#datosoffline').val());
    formData.append('preguntas', preguntas);
    formData.append('notatexto', resultado);
    formData.append('puntajehabilidad', puntajeXHab);
    formData.append('nota', puntaje);
    formData.append('resultadojson', resultadoJson);
    formData.append('tiempoduracion', tiempoDuracion);
    formData.append('intento', $('#hNroIntento').val());
    formData.append('preguntasoffline', preguntasoffline);
    formData.append('tipo', '<?php echo !empty($this->tipo)?$this->tipo:'E' ?>');
    formData.append('tipoexamen', '<?php echo !empty($this->tipo)?$this->tipo:'E' ?>');
    formData.append('idcurso', '<?php echo !empty($this->idcurso)?$this->idcurso:'0' ?>');
    formData.append('idcc', '<?php echo !empty($this->idcc)?$this->idcc:'0' ?>');
    formData.append('idgrupoauladetalle', '<?php echo !empty($this->idgrupoauladetalle)?$this->idgrupoauladetalle:'0' ?>');
    formData.append("idcursodetalle", '<?php echo !empty($this->idcursodetalle)?$this->idcursodetalle:0 ?>');
    // formData.append('preguntas', preguntas); 
    formData.append("idrecurso", $('#hIdExamen').val());   
    formData.append('nota', puntaje);
    formData.append('notatexto', resultado);
    formData.append('calificacion_en', $('#calificacion_en').val());
    formData.append("calificacion_total", $('#calificacion_total').val());            
    formData.append('calificacion_min', $('#calificacion_min').val());
    formData.append("tiempo_total", ttotal); 
    formData.append('tiempo_realizado', tiempoDuracion);
    formData.append('calificacion', ccalif);
    formData.append("habilidades", habtodas); 
    formData.append('habilidad_puntaje', puntajeXHab);   
    $.ajax({
        url: urlsave,
        type: "POST",
        data:  formData,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,
        beforeSend: function(XMLHttpRequest){
          __notificar({
                    type:'info',
                    title:'Guardando Examen',
                    html:'Estamos procesando' ,
                    timer:1000,
                    onBeforeOpen: () => {
                    Swal.showLoading();
                    Swal.stopTimer();
                    timerInterval = setInterval(() => {
                      if(procesandoguardando==false){
                        Swal.increaseTimer(1);
                        Swal.resumeTimer();
                      }
                    }, 100)
                  },
                  onClose: () => {
                    clearInterval(timerInterval)
                  }
                });
          // $('#procesando').show('fast'); 
        },
        success: function(data)
        {            
            $('#porcesandoguardado').hide();
            procesandoguardando=false;           
            if(data.code==='ok'){
                $('#infototalcalificacion').show();
                $('._resultadoporhabilidad_').show();
                __notificar({
                    type:'success',
                    title:msjeAttention,
                    html:data.msj                     
                });

               //mostrar_notificacion(msjeAttention, data.msj, 'success');
                $('.btnnoguardo').hide();
                var urlback='<?php echo  !empty($this->urlback)?$this->urlback:''; ?>';
                if(urlback!='')  setTimeout(function(){ window.location=window.atob(urlback); }, 4000);   
                return false;
            }else if(data.code==201){
                __notificar({
                    type:'danger',
                    title:'Lo Sentimos <br> Examen no guardado',
                    html:data.msj , 
                    callback:function(r){
                        window.top.location.reload();
                    }
                });
            }else{
                intentodeguardado++;
                __notificar({
                    type:'warning',
                    title:'Lo Sentimos <br> Examen no guardado',
                    html:'<b>Tu puedes volver a resolverlo o enviar la nota para que nosotros lo registremos por ti.' , 
                    callback:function(r){
                        $('.btnnoguardo').find('.btnintentarguardar').show();
                        $('.btnnoguardo').find('.btnenviarpararegistro').show();
                        $('.btnnoguardo').show(); //  $.ajax({url: _sysUrlBase_+'notas_quiz/noguardaNotas_quiz', type: "POST", data:  formData});         
                    }
                });
            }
        },
        error: function(xhr,status,error){
            procesandoguardando=false;
            __notificar({
                    type:'Danger',
                    title:msjeAttention,
                    html:'<b>Tu puedes volver a resolverlo' , 
                    callback:function(r){
                        $('.btnnoguardo').find('.btnintentarguardar').hide();
                        $('.btnnoguardo').find('.btnenviarpararegistro').hide();
                        $('.btnnoguardo').show(); //  $.ajax({url: _sysUrlBase_+'notas_quiz/noguardaNotas_quiz', type: "POST", data:  formData});         
                    }
                });
             // mostrar_notificacion(msjeAttention, status, 'warning');
            $('#procesando').hide('fast');
            return false;
        }
    });
}

var initCircleTimer = function(selector, seconds, $ejercActivo){
    selector = selector || '';
    $ejercActivo = $ejercActivo || '';
    var timeMilisegundos = seconds*1000;
    $ejercAddClass = ($ejercActivo!='')? $ejercActivo : $('.tmpl.tmpl-ejerc');
    if(selector!=''){
        $(selector).circletimer({
            onComplete: function() {
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('Time is over');?>', 'warning');
                $ejercAddClass.addClass('tiempo-acabo').removeClass('tiempo-inicio');
            },
            onUpdate: function(elapsed) {
                var seg_transcurrido = Math.round(elapsed/1000);
                var new_time = seconds-seg_transcurrido;
                if(new_time==0) miliseconds = new_time;
                var h = Math.floor(new_time / 3600);
                var m = Math.floor(new_time % 3600 / 60);
                var s = Math.floor(new_time % 3600 % 60);

                var hh = (h<10?"0":"")+h;
                var mm = (m<10?"0":"")+m;
                var ss = (s<10?"0":"")+s;
                $('#pregunta-timer .time-elapsed').html(hh+':'+mm+':'+ss);
                $ejercAddClass.attr('data-tiemporestante', hh+':'+mm+':'+ss);
            },
            timeout: timeMilisegundos
        });
    } else {
        alert('Impossible initialize time!!');
    }
};

var setTiempo = function(){
    var tiempo = '';
    var $ejercActivo = $('.panel-body div.pnlitem.active div.tmpl-ejerc.active');
    if(_TIEMPO_POR=='E'){
        tiempo = $('#pregunta-timer .time-elapsed').text();
        $ejercActivo = $('.panel-body .tmpl.tmpl-ejerc');
    }
    if(_TIEMPO_POR=='Q'){
        if($('#pregunta-timer .timer svg').length>0) $('#pregunta-timer .timer').circletimer("pause");
        $ejercActivo.prev().addClass('tiempo-pausa').removeClass('tiempo-inicio').removeClass('tiempo-acabo');
        tiempo = ($ejercActivo.attr('data-tiemporestante')!=undefined)? $ejercActivo.attr('data-tiemporestante') : $ejercActivo.attr('data-tiempo');
    }

    if($ejercActivo.length>0 && tiempo!=''){
        var arrTime = tiempo.split(':');
        var seconds = (+arrTime[0])*60*60 + (+arrTime[1])*60 + (+arrTime[2]);
        var miliseconds = seconds*1000;
        $('#pregunta-timer .time-elapsed').html(tiempo);
        $('#pregunta-timer').show();
        if(_TIEMPO_POR=='E' && $('#pregunta-timer .timer svg').length==0) initCircleTimer('#pregunta-timer .timer', seconds);
        if(_TIEMPO_POR=='Q') initCircleTimer('#pregunta-timer .timer', seconds, $ejercActivo);                
    } else {
        $('#pregunta-timer').hide();
        $('#pregunta-timer .time-elapsed').html('00:00:00');
    }
};

var actualizarBarraProgreso = function(){
    var $listPregs= $('.panel-body .tmpl-ejerc');
    var $ejercActivo = $('.panel-body .tmpl-ejerc.active');
    var npreguntas= $listPregs.length;
    var indice = $listPregs.index($ejercActivo);
    if(indice>=0){
        var avance = (indice*100)/npreguntas;
        avance = Math.floor(avance);
    } else {
        var avance = 100;
    }
    $('#barra-progreso .progress-bar').css('width', avance+'%');
    $('#barra-progreso .progress-bar').attr('aria-valuenow', avance);
    $('#barra-progreso .progress-bar span').text(avance);
};

var numerarpreguntas=function(){
    var i=0;
    $('b.npregunta').remove().siblings('b').remove();
    $('.panel-body').find('.tmpl-ejerc').each(function(){
        var obj= $(this).find('.titulo_descripcion .titulo');
        var tmpl = $(this).data('tmpl');
        var instruccion = getInstruccionTmpl(tmpl);
        $(this).find('.titulo_descripcion>b').remove();
        obj.find('.plantilla').removeClass('editando');
        i++;
        //if(obj.text()!=''&&obj!=undefined)
        obj.before('<b class="npregunta">'+(i)+'. '+instruccion+'</b>');
        obj.attr('data-orden',i)
    });
};

var iniciar=function(){
    $('.panel-body div.pnlitem:first').addClass('active').nextAll().removeClass('active');
    $('.panel-body div.pnlitem div.tmpl-ejerc:first').addClass('active').nextAll().removeClass('active');
    $('.plantilla-completar').examMultiplantilla();
    $('.plantilla-verdad_falso').examTrueFalse();
    $('.plantilla-ordenar.ord_simple').examOrdenSimple();
    $('.plantilla-ordenar.ord_parrafo').examOrderParagraph();
    $('.plantilla-fichas').examJoin();
    $('.plantilla-img_puntos').examTagImage();
    infopregunta();
    numerarpreguntas();
};  

var calcularpuntaje=function(){
    var  cpor=$('#calificacion_por').val()||'E';
    var  cen=$('#calificacion_en').val()||'P';
    var  ctotal=$('#calificacion_total').val()||100;
    var  cmin=$('#calificacion_min').val()||51;
    var npreguntas= $('.panel-body .tmpl-ejerc').length;
    var pxpregunta=0;
    var pacumuladatotal=0;
    var habilidades={};
    var datah={};
    var arrEscala = [];
    if(cpor=='E' && cen=='A'){
        var escalaAlfab = $.parseJSON(ctotal);
        ctotal = 100;
        cmin = 51; /* el minimo apra aprobar Alfanumerico */
    }

    if(cpor=='Q' && cen=='N'){
        ctotal = 0;
        $('.panel-body').find('.tmpl-ejerc').each(function(){
            var plantilla= $(this);
            var puntaje = plantilla.attr('data-puntaje')||0;
            ctotal = parseFloat(ctotal) + parseFloat(puntaje); 
        });
        $('#calificacion_total').val(ctotal);
    }else{
        pxpregunta=ctotal/npreguntas;
    }

    var npreguntasoffline=0;

    $('.panel-body').find('.tmpl-ejerc').each(function(){
        var plantilla= $(this);                
        if(cpor=='Q' && cen=='N'){
            var puntaje = plantilla.attr('data-puntaje')||0;
            pxpregunta = puntaje;
        }
        var tmpl=plantilla.attr('data-tmpl')||'';
        if(tmpl!=''){
            var totalitems=0;
            var itemsgood=0;
            var itemsbad=0;
            var tpl_skill=$(this).attr('data-habilidades')||'';
            if(tmpl=='tag_image'){
                totalitems=plantilla.find('.playing .dot-container').length;
                itemsbad=plantilla.find('.dot-tag[data-corregido="bad"]').length;
                itemsgood=plantilla.find('.dot-tag[data-corregido="good"]').length;
            }else if(tmpl=='options'||tmpl=='click_drag'||tmpl=='select_box'||tmpl=='gap_fill'){
                totalitems=plantilla.find('.panelEjercicio input').length;
                itemsbad=plantilla.find('.panelEjercicio .inp-corregido[data-corregido="bad"]').length;
                itemsgood=plantilla.find('.panelEjercicio .inp-corregido[data-corregido="good"]').length;
            }else if(tmpl=='true_false'){
                totalitems=plantilla.find('.ejercicio .premise').length;
                itemsbad=plantilla.find('.ejercicio .premise.bad').length;
                itemsgood=plantilla.find('.ejercicio .premise.good').length;
            }else if(tmpl=='order_paragraph'){
                totalitems=plantilla.find('.ejercicio .drag-parr div').length;
                itemsbad=plantilla.find('.drop-parr .filled[data-corregido="bad"]').length;
                itemsgood=plantilla.find('.drop-parr .filled[data-corregido="good"]').length;
            }else if(tmpl=='order_simple'){
                totalitems=plantilla.find('.element .texto .drag>div').length;
                itemsbad=plantilla.find('.drop .parte[data-corregido="bad"]').length;
                itemsgood=plantilla.find('.drop .parte[data-corregido="good"]').length;
            }else if(tmpl=='join'){
                totalitems=plantilla.find('.ejercicio .partes-2 .ficha').length;
                itemsbad=plantilla.find('.ejercicio .partes-2 .ficha.bad').length;
                itemsgood=plantilla.find('.ejercicio .partes-2 .ficha.good').length;
            }else if(tmpl=='speach'){               
                totalitems=parseInt(plantilla.find('.speachtexttotal').attr('total-palabras')||0);
                plantilla.attr('data-items',totalitems);
                itemsgood=parseInt(plantilla.find('.speachtexttotal').attr('total-ok')||0);
                itemsbad=totalitems-itemsgood;                
            }else if(tmpl=='nwrite'){            
                totalitems=parseInt(plantilla.find('.respuestaAlumno').attr('total-palabras')||0);
                plantilla.attr('data-items',totalitems);
                itemsgood=parseInt(plantilla.find('.respuestaAlumno').attr('total-acertado')||0);
                itemsbad=totalitems-itemsgood;
                npreguntasoffline++;
            }else if(tmpl=='nspeach'){               
                totalitems=1;
                plantilla.attr('data-items',totalitems);
                itemsgood=0;
                itemsbad=1;
                npreguntasoffline++;
            }
            var puntajeacumulado=0;                 
            if(plantilla.attr('data-items')==undefined){
                plantilla.attr('data-items',totalitems);                        
            }else{
                totalitems=plantilla.attr('data-items')||0;
            }
            if(pxpregunta>0&&totalitems>0){
                puntajeacumulado=(pxpregunta*itemsgood)/totalitems;
                pacumuladatotal=pacumuladatotal+puntajeacumulado;
            }
            var skills=tpl_skill.split('|');
            $.each(skills,function(key,val){
                if(val!=''){
                    if(datah[val]==''||datah[val]==undefined){
                        datah[val]={'n':1,'p':parseFloat(puntajeacumulado),'pt':parseFloat(pxpregunta)};
                    }else{
                        datah[val].n++;
                        datah[val].p=parseFloat(datah[val].p)+parseFloat(puntajeacumulado);
                        datah[val].pt=parseFloat(datah[val].pt)+parseFloat(pxpregunta);
                    }
                    habilidades[val]=datah[val];
                }                       
            });

            plantilla.attr('data-itemsbad',itemsbad);
            plantilla.attr('data-itemsgood',parseInt(itemsgood));
            plantilla.attr('data-score',puntajeacumulado);
        }
    });

    $('#calificacion_pregunta').val(pxpregunta);
    $('#calificacion_acumulada').val(pacumuladatotal);
    var sb='';
    var html='';
    if(npreguntasoffline==0){
        if(pacumuladatotal>=cmin) {
            var resultado_text = 'pass';
            html='<div class="color-green2"><h2>'+MSJES_PHP.you_passed_exam+'</h2>';
        } else {
            var resultado_text = 'fail';
            html= '<div class="color-red"><h2>'+MSJES_PHP.you_failed_exam+'</h2>';
        }
    }else html='<div class="alert-info" style="padding:1em;">';

    if(cen=='P'){
        html+='<h3> '+(pacumuladatotal.toFixed(2))+'% de '+ctotal+'% </h3>';
    }else if(cen=='A'){
        var resultado_text = 'fail';
        html= '<div><h2></h2>';
        var ptosAcum = pacumuladatotal.toFixed(2);
        if(ptosAcum>100) ptosAcum = 100;
        var obj = {};

        $.each(escalaAlfab, function(i, escala) {
             var tmpptosAcum=Math.round(ptosAcum);               
            if( tmpptosAcum>=parseFloat(escala.min) && tmpptosAcum<=parseFloat(escala.max) ){
                obj = {
                    'nombre' : escala.nombre,
                    'puntaje_min' : escala.min,
                    'ptosAcum' : ptosAcum,
                };
            }
        });
        if($.isEmptyObject(obj)){
            html += '<h3> '+MSJES_PHP.your_score_below+'. <br>'+ptosAcum+'pts '+MSJES_PHP.out_of+' '+ctotal+' </h3>';
        } else {
            html += '<h3> '+MSJES_PHP.your_result_in_scale+': "<b>'+obj.nombre+'</b>". </h3><h4>'+obj.ptosAcum+'pts '+MSJES_PHP.out_of+' '+ctotal+' </h4>';
        }
    }
    else html+='<h3> '+(pacumuladatotal.toFixed(2))+' '+MSJES_PHP.out_of+' '+ctotal+'pts </h3>';
    if(npreguntasoffline>0){
        var msjpreguntas=MSJES_PHP.preguntas_por_revisar;
        msjpreguntas=msjpreguntas.replace(/_npreguntasoffline_/gi,npreguntasoffline);
        html+='<br><div class="alert alert-warning" style="padding:1em;" id="infopreguntasoffline" >'+msjpreguntas+'</div><br>';
    }
    html+='</div>';
    $('#infototalcalificacion').html(html).attr('data-resultado', resultado_text).attr('data-puntaje', pacumuladatotal.toFixed(2));
    $('#resultskill .skill').find('.circulo').html('');
    $('#resultskill .skill').hide();
    if(hayhab==false){
        $('._resultadoporhabilidad_').hide();
    }else
    $.each(habilidades,function(key,val){
        var $skillobj=$('#resultskill .skill.sk_'+key);
        var $circulo_skill = $skillobj.find('.circulo');
        if($skillobj.length>0){
            $skillobj.show();
            var p=val.p||0;
            var pt=parseFloat(val.pt)||0;
            $circulo_skill.attr('data-acumulado',p);
            $circulo_skill.attr('data-total',pt);
            if(p>0&&pt>0){
                var val=(p*100)/pt;
                $circulo_skill.attr('data-value',val.toFixed(0));
            }else{
                $circulo_skill.attr('data-value',0);
            }
        } 
        $circulo_skill.graficocircle();                 
    });
};

$(document).ready(function(){
    cargarMensajesPHP('#msjes_idioma_php');

    $('.btnvolveraintentar').on('click',function(){
        window.location.reload()
    })
    $('.btnintentarguardar').on('click',function(){
        urlsave=_sysUrlBase_+'notas_quiz/guardarNotas_quiz';
        guardarResultado();
    })
    $('.btnenviarpararegistro').on('click',function(){
        urlsave=_sysUrlBase_+'notas_quiz/noguardarNotas_quiz';
        guardarResultado();
    })
 
    $('.startexam').click(function(){
    	$('#controlfooter .irnext').trigger('click');
        <?php if(!empty($examen['tiempoalinicio'])){?>
            setTimeout(function(){ $('#pregunta-timer .timer').circletimer("start"); },1000);
        <?php } ?>
        //iniciar tiempo.
    });

    $('body').on('click','.btns-img .full-size',function(e) {
        e.preventDefault();
        var imgSrc = $(this).closest('.tmpl').find('img').attr('src');
        var idPreg = $(this).closest('.tmpl').data('idpregunta');
        var idModal = 'imagen_'+idPreg;
        if( $('#'+idModal).length==0 ){
            var $modal = $('#modalclone').clone();
            $modal.attr('id',idModal);
            $modal.find('#modaltitle').text('<?php echo ucfirst(JrTexto::_('Image')); ?>');
            $modal.find('#modalcontent').html('<img src="'+imgSrc+'" class="center-block">');
            $('body').append($modal);
        }
        $('#'+idModal).modal('show').addClass('in show');
    });

    $('.plantilla-nlsw').on('click','.btnenformatohtml',function(ev){
        var plt=$(this).closest('.respuestaAlumno');
        edithtml_all({id:plt.children('textarea').attr('id')});
        //$(this).hide();
    })

    var addnewbuttonhtml=function(plt){
        var htmlbtn='<a class="btn btnenformatohtml btn-info btn-xs"><i class="fa fa-code"></i> Dar Formato</a>';
        var btnenformato=plt.find('label').children('.btnenformatohtml');      
        if(btnenformato.length==0 && plt.find('textarea').length>0) plt.find('label').append(htmlbtn);     
        if(plt.find('.btnadd_recordAlumno').length) plt.find('.btnadd_recordAlumno').html('<i class="fa fa-microphone-slash"></i> '+MSJES_PHP.record);
    }

    $('#controlfooter').on('click','.irprev',function(ev){
        ev.preventDefault();
        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });
        var divactive=$('.panel-body div.pnlitem.active');
        if(!divactive.hasClass('pnlpresentacion')){
           var subitems=divactive.find('.tmpl-ejerc');
           if(subitems.length>1){
                var subitemsactive=divactive.find('.tmpl-ejerc.active');
                var subi=subitemsactive.removeClass('active').prev('.tmpl-ejerc');           
                if(subi.length>0&&subi.index()>=0){
                    subi.addClass('active'); 
                }else 
                divactive.removeClass('active').prev().addClass('active').find('.tmpl-ejerc:last').addClass('active').prevAll().removeClass('active');             
           }else{
            divactive.removeClass('active').prev().addClass('active').find('.tmpl-ejerc:last').addClass('active').prevAll().removeClass('active');
           }
        }
        infopregunta();
        actualizarBarraProgreso();
        setTiempo();
    }).on('click','.irnext',function(ev){
        ev.preventDefault();
        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });
        $('#progresobar2020').show();
        var divactive=$('.panel-body div.pnlitem.active');
        var objtimer=$('#curtimerexamen');
        try{
            tinyMCE.triggerSave();
            tinyMCE.remove();        
        }catch(ex){console.log(ex)}
        if(!divactive.hasClass('pnlresultado')){
            var subitems=divactive.find('.tmpl-ejerc');
            if(!objtimer.hasClass('active')){objtimer.addClass('active'); objtimer.mitimer('oncroiniciar')}
            if(subitems.length>0){
                var subitemsactive=divactive.find('.tmpl-ejerc.active');
                if(subitemsactive.length>0){
                	var subi=subitemsactive.removeClass('active').next();
                	if(subi.length==0){
                        var tpl=divactive.removeClass('active').next().addClass('active').find('.tmpl-ejerc:first');
                        tpl.addClass('active').nextAll().removeClass('active');                        
                         var plt=tpl.children('.plantilla');
                        if(plt.hasClass('plantilla-speach')){ mostrarondaSpeach(plt.find('.pnl-speach.docente')); }
                        else if(plt.hasClass('plantilla-nlsw')) addnewbuttonhtml(plt.find('.respuestaAlumno')); 
                        else if(plt.hasClass('plantilla-verdad_falso')){
                            plt.find('span.texto-opcion').each(function(i,v){
                                if(_sysIdioma_=='en') $(v).text($(v).attr('data-en'))
                                else $(v).text($(v).attr('data-es'))
                            })
                        }
                    }else {
                        subi.addClass('active');                      
                        var plt=subi.children('.plantilla');
                        if(plt.hasClass('plantilla-speach')){ mostrarondaSpeach(plt.find('.pnl-speach.docente')); }
                        else if(plt.hasClass('plantilla-nlsw')) addnewbuttonhtml(plt.find('.respuestaAlumno'));
                        else if(plt.hasClass('plantilla-verdad_falso')){
                            plt.find('span.texto-opcion').each(function(i,v){
                                if(_sysIdioma_=='en') $(v).text($(v).attr('data-en'))
                                else $(v).text($(v).attr('data-es'))
                            })
                        }
                    }
                }else{
                    var tpl=divactive.removeClass('active').next().addClass('active').find('.tmpl-ejerc:first');
                    tpl.addClass('active');
                    var plt=tpl.children('.plantilla');
                    if(plt.hasClass('plantilla-speach')){ mostrarondaSpeach(plt.find('.pnl-speach.docente')); }
                    else if(plt.hasClass('plantilla-nlsw')) addnewbuttonhtml(plt.find('.respuestaAlumno'));
                    else if(plt.hasClass('plantilla-verdad_falso')){
                        plt.find('span.texto-opcion').each(function(i,v){
                            if(_sysIdioma_=='en') $(v).text($(v).attr('data-en'))
                            else $(v).text($(v).attr('data-es'))
                        })
                    }
                }
            }else{
                var tpl=divactive.removeClass('active').next().addClass('active').find('.tmpl-ejerc:first');      
                tpl.addClass('active');
                var plt=tpl.children('.plantilla');
                if(plt.hasClass('plantilla-speach')){ mostrarondaSpeach(plt.find('.pnl-speach.docente')); }
                else if(plt.hasClass('plantilla-nlsw')) addnewbuttonhtml(plt.find('.respuestaAlumno'));
                else if(plt.hasClass('plantilla-verdad_falso')){
                    plt.find('span.texto-opcion').each(function(i,v){
                        if(_sysIdioma_=='en') $(v).text($(v).attr('data-en'))
                        else $(v).text($(v).attr('data-es'))
                    })
                }
            } 
            calcularpuntaje();
            if($('.panel-body div.pnlitem.active').hasClass('pnlresultado')) {
                objtimer.removeClass('active');
                $('#curtimerexamen').trigger('oncropausar');
                $('#pregunta-timer .timer').circletimer("pause");               
                $('#infototalcalificacion').hide();
                $('._resultadoporhabilidad_').hide();
                guardarResultado();
            }
        }
        if($('.pnlpregunta').hasClass('active')) $('.panel-footer .btn-group').show();
        else $('.panel-footer .btn-group').hide();
        infopregunta();
        actualizarBarraProgreso();
        setTiempo();
    });

    showexamen('preview');
   
    iniciar();

    $('.panel .btn.print').click(function(e){
        var new_modal =  $('#modalclone').clone();
        var now = Date.now();
        var idexamen = $(this).data('idexamen');
        var ruta_plantilla = _sysUrlBase_ +'/examenes/imprimir/?idexamen='+idexamen;
        new_modal.removeAttr('id').attr('id', 'print_'+now).addClass('imprimir');
        new_modal.find('#modaltitle').text('<?php echo JrTexto::_("Print view"); ?>');
        $.get(ruta_plantilla, function(data) {
            new_modal.find('#modalcontent').html(data);
            new_modal.find('#modalfooter').find('.btn.cerrarmodal').addClass('pull-left');
            new_modal.find('#modalfooter').append('<a href="#" class="btn btn-primary pull-right btn-imprimir"><i class="fa fa-print"></i> <?php echo JrTexto::_("Print"); ?></a>');
        });
        $('body').append(new_modal);
        $('#print_'+now).modal({backdrop: 'static', keyboard: false});
    });

    $('body').on('hidden.bs.modal', '.modal.imprimir', function (e) {        
        $('.modal.imprimir').remove();
    }).on('click', '.btn-imprimir', function(e) {
        e.preventDefault();
        
       $('#body-content').addClass('hidden-print');
       $('div.container').hide();
       $('.modal.in').hide();
       var contenido_impresion = $('body .modal.in .modal-body').clone();
       $('body').append('<div id="contenido_impresion">'+contenido_impresion.html()+'</div>');
        window.print();
        $('#contenido_impresion').remove();
        $('div.container').show();
        $('.modal.in').show();
    });

    $(".panel-body").on("mousedown", ".pnlitem.active .tmpl-ejerc.active",function(e) {
        if( !$(this).hasClass('tiempo-acabo') && !$(this).hasClass('tiempo-inicio') ){
            $('#pregunta-timer .timer').circletimer("start");
            if(_TIEMPO_POR=='E') $('.panel-body .tmpl.tmpl-ejerc').addClass('tiempo-inicio');
            else if(_TIEMPO_POR=='Q') $(this).addClass('tiempo-inicio').removeClass('tiempo-pausa');
        }
    }).on("mouse");
    
    if(_TIEMPO_POR=='E') setTiempo();


    /******************************************************/
    var registrarHistorialSesion = function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/agregar',
            type: 'POST',
            dataType: 'json',
            data: {'lugar': 'E', 'fechaentrada': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){               
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
               
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
        });
    };

    $('.npregunta').each(function(i,v){
        var n=$(v).text().substring(0,10).match(/\d+/gi)[0];
        $(v).next('h3').prepend('<strong style="font-size: 1.2em;">'+n+'.</strong> ').css({'width':'auto'});
        $(v).hide();
        //esto falta en offline
    });
});
</script>