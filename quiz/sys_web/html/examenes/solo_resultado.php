<?php
$examen = $this->examen;
$rutabase = URL_SMARTQUIZ;
$arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
$srcPortada = (!empty($examen['portada']!=''))? str_replace('__xRUTABASEx__/', URL_SMARTQUIZ, $examen['portada']) : '';
$this->resultado['puntaje']=$this->resultado['nota'];
$this->resultado['resultado']=$this->resultado['notatexto'];
$this->resultado['puntajehabilidad']=$this->resultado['habilidad_puntaje'];
$tipoEvaluacion = '';
$calificacion_en = '';
if($examen['calificacion_en']=='N') {
	$tipoEvaluacion = ' pts.';
	$calificacion_en = ucfirst(JrTexto::_("numeric"));
} else if($examen['calificacion_en']=='P') {
	$tipoEvaluacion = '%';
	$calificacion_en = ucfirst(JrTexto::_("percentage"));
} else {
	$calificacion_en = ucfirst(JrTexto::_("alfabetic"));
}
$calif_minima = (float)$examen["calificacion_min"];
$calif_total = (float)$examen["calificacion_total"];
$nomescala='';
if($examen['calificacion_en']=='A'){
    $calif_minima = 51;
    $jsonEscalas = json_decode($examen["calificacion_total"], true);
    $calif_total = '<ul>';
    if(!empty($jsonEscalas) && is_array($jsonEscalas)){
        foreach ($jsonEscalas as $index=>$escala) {           
            if(intval($this->resultado['puntaje'])>=$escala["min"] &&  intval($this->resultado['puntaje'])<=$escala["max"])
            $nomescala=ucfirst($escala["nombre"]);
            $calif_total .= '<li>'.ucfirst($escala["nombre"]).' ('.$escala["max"].' - '.$escala["min"].' pts)</li>';
        }
    }
    $calif_total .= '</ul>';
}
#var_dump($this->resultado);
#var_dump($examen);
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_fichas.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_imagen_puntos.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_completar.css">

<input type="hidden" name="hIdExamen" id="hIdExamen" value="<?php echo @$examen['idexamen']; ?>">
<?php /*Datos de Tarea_Archivo, si es que hubiera*/ ?>
<input type="hidden" name="hIdTareaArchivo" id="hIdTareaArchivo" value="<?php echo @$this->idTareaArchivo; ?>">
<input type="hidden" name="hIdArchivoRespuesta" id="hIdArchivoRespuesta" value="<?php echo (@$this->tarea_archivo['tablapadre']=='R')?@$this->tarea_archivo['idtarea_archivos']:''; ?>">
<input type="hidden" name="hIdTareaRespuesta" id="hIdTareaRespuesta" value="<?php echo @$this->tarea_respuesta['idtarea_respuesta']; ?>">
<input type="hidden" name="hNroIntento" id="hNroIntento" value="<?php echo @$this->nro_intento; ?>">

<style type="text/css">
    #examen_preview{
        margin:  0;
    }
    #examen_preview .encabezado{
        border-radius: 7px;
        box-shadow: 0px 7px 6px -5px #aaa;
        margin-bottom: 3ex;
    }
    #examen_preview .pnlpresentacion {
        padding: 0 20px;
    }
    #examen_preview #portada{
        border-radius: 50%;
        max-width: 150px;
    }
    
    #examen_preview .title_desc-zone{
        padding: 6% 0;
    }

    #examen_preview .portada-zone,.title_desc-zone{
        display: inline-block;
        padding: 1% 0;
    }

    #examen_preview .panel{
        margin-bottom: 0;
    }

    .pnlitem , .pnlitem .tmpl-ejerc{
        /*display: none;*/
    }
    .pnlitem.active,.pnlitem .tmpl-media, .pnlitem .tmpl-ejerc.active{
        display: block;        
    }   
    .panel-body{
    	min-height: 390px;
        position: relative;
    }
    #pregunta-timer{
        position: absolute;
        right: 12px;
        z-index: 100;
    }
    #pregunta-timer .timer{
        background: #7ac1a1;
        border-radius: 50%;
        box-shadow: 0px 1px 10px -2px #444;
        display: inline-block;
        height: 80px;
        width: 80px;
    }
    #pregunta-timer .time-elapsed{
        color:  #fff;
        font-weight: bolder;
        margin-top: -0.7em;
        position: absolute;
        text-align: center;
        top: 50%;
        width: 100%;
    }

    #examen_preview .pnlpregunta .tmpl-ejerc.tiempo-acabo:before {
        background: rgba(255,255,255,0);
        content: ' ';
        height: 100%;
        position: absolute;
        width: 100%;
        z-index: 10;
    }
    #ifrRespuestas {
		height: 100vh;
    	width: 100%;
    }
    @media (max-width: 768px) {
        .table-key-value>.table-kv-row>.value {           
           display:table-cell;
        }
        .table-key-value>.table-kv-row>.key {
            width: auto;
            max-width: 200px;
        }
    }
</style>
<style>
    .contenedor-preguntas{
        overflow: hidden;
        border-bottom: 2px solid #bbb;
    }
    .contenedor-preguntas:last-child{border: none;}
    .tmpl-media img,
    .tmpl-media audio{
        max-height: 250px;
    }
    .tmpl-media .embed-responsive{
        height: 250px;
        padding: 0;
        margin: 1ex;
    }
    
    *[data-corregido='good']:before, .good:before{
        background-color: #2ed353;
        border-radius: 50%;
        content: "\f118";
        display: inline-block;
        color: white;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: 1.7em;
        height: 23px;
        position: absolute;
        right: 0;
        text-align: center;
        width: 23px;
    }
    *[data-corregido='bad']:before, .bad:before{
        background: #fc5c5c;
        border-radius: 50%;
        color: white;
        content: "\f119";
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        height: 23px;
        font-size: 1.7em;
        position: absolute;
        right: 0;
        text-align: center;
        width: 23px;
    }
    .tpl_plantilla:before{
        content: '';
        background: transparent;
    }

    ul#list-intentos li {
    	display: inline-block;
    }

    ul#list-intentos li a {
    	background: #fff;
    	border-radius: 50%;
    	height: 22px;
    	padding: 1px 5px;
    	text-align: center;
    	width: 22px;
    }
    ul#list-intentos li.active a {
    	background: #6fe3ff;
    	box-shadow: 0px 0px 3px 1px #444;
    }

    .panel .panel-heading {
    	overflow: hidden !important;
    }
    
    .panel .panel-heading .list-unstyled {
    	margin: 0;
    }

    @media (min-width: 768px){
        .table-key-value .key{
            width: 50% !important;
        }
    }

    /*estilos de impresion*/
    @media print{
        .contenedor-preguntas{
            page-break-after: always;
            break-after: always;
        }
        /* **** Join (Fichas) **** */
        .plantilla.plantilla-fichas .ejerc-fichas .ficha {
            width: 23%;
        }
    
        /* **** Verdadero o Falso **** */
        .plantilla.plantilla-verdad_falso .premise>div:first-child{
            width: 60%;
        }
        .plantilla.plantilla-verdad_falso .premise>.options{
            width: 40%;
            float: right;
        }

        /* **** Ordenar Parrafo **** */
        .plantilla.plantilla-ordenar.ord_parrafo .drag-parr,
        .plantilla.plantilla-ordenar.ord_parrafo .drop-parr{
            width: 50%;
        }

        /* **** Ordenar Parrafo **** */
        .plantilla.plantilla-img_puntos .contenedor-img{
            width: 75%;
        }
        .plantilla.plantilla-img_puntos .tag-alts{
            width: 25%;
        }
        .plantilla.plantilla-img_puntos .dot-container>.dot{
            background: #fff;
            border: 5px solid #4c5e9b;
        }
        .plantilla.plantilla-img_puntos .dot-container>.dot:after{
            border:  3px solid #efefef;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }
    }
</style>

<div class="row" id="examen_preview">
	<div class="container">
		<div class="col-xs-12"><br>
			<div class="alert alert-info alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="bolder">Intentos agotados</h4>
				<p>Has agotado los intentos permitidos para este examen. A continuación se muestran tus resultados.</p>
			</div>
		</div>

		<div class="col-xs-12" id="panel-info">
			<div class="panel">
				<div class="panel-heading bg-blue">
					<div class="col-md-6">
						<div>
							<ol class="breadcrumb" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
								<li><a href="#" style="color:#fff"><?php echo JrTexto::_("Assessment"); ?></a></li>
							</ol>
						</div>
					</div>
					<div class="col-md-6">
						<span class="" id="curtimerexamen"></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-body">
					<div class="pnlpresentacion  pnlitem">
						<?php if(!empty($examen)){?>
						<div class="col-xs-12 col-sm-12 col-md-12 text-center bg-blue encabezado">
							<?php $col = 12;
							if($srcPortada!='') { $col = 8; ?>
								<div class="col-xs-12 col-sm-3 portada-zone">
									<img src="<?php echo $srcPortada; ?>" alt="img-portada" id="portada" class="img-responsive">
								</div>
							<?php } ?>
							<div class="col-xs-12 col-sm-<?php echo $col; ?> title_desc-zone">
								<h2><?php echo ucfirst($examen["titulo"]); ?></h2>
								<small><?php echo ucfirst($examen["descripcion"]); ?></small>
							</div>
						</div>
						<?php }                         
                        ?>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="table-key-value">
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("Scored by")); ?></div>
									<div class="value"><?php echo ($examen["calificacion_por"]=='Q'?ucfirst(JrTexto::_('Question')):ucfirst(JrTexto::_('Assessment'))); ?></div>
								</div>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("type of score")); ?></div>
									<div class="value"><?php echo $calificacion_en; ?></div>
								</div>
                                <?php if(@$examen["calificacion_por"]!='Q'){?>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("max. score")); ?></div>
									<div class="value"><?php echo $calif_total.$tipoEvaluacion; ?></div>
								</div>
                                <?php } if(@$examen['calificacion_en']!='A'){?>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("min. score to pass")); ?></div>
									<div class="value"><?php echo $calif_minima.$tipoEvaluacion; ?></div>
								</div>
                                <?php } ?>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("number of attempts")); ?></div>
									<div class="value"><?php echo $examen['nintento']; ?></div>
								</div>
                                <?php if($examen['nintento']!=1){?>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("Choose")); ?></div>
									<div class="value"><?php echo ucfirst($examen["calificacion"]=='M'?JrTexto::_("Best try"):JrTexto::_("Last try")); ?></div>
								</div>
                                <?php } ?>
							</div>
						</div>
                        <?php 
                        $habilidades_todas = json_decode($examen['habilidades_todas'], true);
                        if(!empty($habilidades_todas)){ ?>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="table-key-value">
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("objective").'s').' / '. ucfirst(JrTexto::_("skills")); ?></div>
									<div class="value">
										<ul class="list-unstyled_1">
											<?php 
												foreach ($habilidades_todas as $hab) {
													echo '<li>'.ucfirst(JrTexto::_(  $hab['skill_name'] )).'</li>';
												} 
                                            ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
                      <?php }?>

						<div class="col-xs-12 col-sm-12 col-md-12 text-center">
							<h2><strong><?php echo ucfirst(JrTexto::_("Result")) ?></strong></h2>
						</div>                	
						<div class="col-xs-12 col-sm-12 col-md-12 text-center" id="infototalcalificacion">
                            <?php if(@$examen['calificacion_en']!='A'){ ?>
							<div class="alert <?php echo (float)$this->resultado['puntaje']>=$calif_minima?'alert-success':'alert-danger'; ?> " role="alert">
                                <h2><?php 
                                    if( (float)$this->resultado['puntaje']>=$calif_minima )
                                        echo ucfirst(JrTexto::_('you passed the assessment'));
                                    else echo ucfirst(JrTexto::_('you failed the assessment'));
                                ?></h2>
                                <h3><?php echo $this->resultado['puntaje'].$tipoEvaluacion." ".JrTexto::_('out of')." ".$calif_total.$tipoEvaluacion;?>  </h3>   
                            </div>
                        <?php }else{ ?>
                            <div class="alert alert-info" role="alert">
                                <h3><?php echo ucfirst(JrTexto::_('your result is located on the scale')); ?></h3>
                                <b style="font-size: 2.5em;"><?php echo $nomescala;?></b> </h3><h4>  <?php echo $this->resultado['puntaje']."  ".JrTexto::_('out of')." ".$this->maxformulacurso;?> </h4>
                                <?php if(!empty($this->resultado['preguntasoffline'])){ ?>
                                    <br><div class="alert alert-warning" style="padding:1em;" id="infopreguntasoffline" ><b><?php echo ucfirst(JrTexto::_('Remember'))."</b> ".ucfirst(JrTexto::_('The percentage shown is not your final grade')).". ".ucfirst(JrTexto::_('Some writing questions and all the speaking ones still have to be reviewed by your teacher')).". ".ucfirst(Jrtexto::_('After having reviewed them, your teacher will let you know that you can check your final grade in the notes’ report')); ?>.</div><br>
                                <?php } ?>
                            </div>   
                        <?php } ?>
                                 
						</div>
                        <?php if(!empty($habilidades_todas)) { ?>
						<div class="col-xs-12 col-sm-12 col-md-12 text-center">
							<a name="inirespuestas2" href="#inirespuestas2" id="inirespuestas2"></a>
							<h3><strong><?php echo ucfirst(JrTexto::_("Results by skill")); ?></strong></h3>
							<div class="col-xs-12 col-sm-6 col-md-6">
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12" id="resultskill">
								<?php $i=-1; 								
									foreach ($this->habilidades as $hab) {  $i++; 
										if($i>count($arrColoresHab)){ $i=0; } ?>
								<div class="col-xs-12 col-sm-6 padding-0 skill sk_<?php echo $hab["skill_id"] ?>">
									<div class="col-xs-3 col-sm-3 col-md-4 circulo" data-idskill="<?php echo $hab["skill_id"] ?>" data-value="0" data-texto=" " data-ccolorout="<?php echo $arrColoresHab[$i]; ?>"></div>
									<div class="col-xs-9 col-sm-9 col-md-8 text-left bolder nombre-skill"><br><?php echo JrTexto::_($hab["skill_name"]);?></div>
								</div>
								<?php } if($i==-1){ ?>
								<div class="text-center"><?php echo JrTexto::_("Skills").' '.JrTexto::_("not found") ?></div>
                            <?php } ?>
							</div>
						</div>
                        <?php } 
                        if(!empty($this->resultado["preguntas"])){
                        ?>						
						<div class="col-xs-12 hidden" id="pnlPreguntas" style="background-color: #fff">
                            <hr>
                            <h3><strong>Respuestas del examen</strong></h3>
							 <a name="inirespuestas" href="#inirespuestas" id="inirespuestas"></a>
							<hr>
		                    <?php                
		                     $preguntas=str_replace('__xRUTABASEx__smartquiz/', URL_SMARTQUIZ, $this->resultado["preguntas"]);
                             $preguntas=str_replace('__xRUTABASEx__/', URL_BASE, $preguntas);
		                     $preguntas=json_decode($preguntas,true);
		                     if(!empty($preguntas))
		                     foreach ($preguntas as $k => $v) {
		                         echo $v;
		                     }
		                     ?>
		                </div>
		                <div class="col-xs-12 text-center">
							<button class="btn btn-lg btn-primary ver-respuestas active"><?php echo JrTexto::_("Mostrar Respuestas") ?></button>
						</div>
                        <?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
var PUNTAJE_HABILIDAD = JSON.parse(<?php echo json_encode($this->resultado['puntajehabilidad'], true); ?>);
$(document).ready(function() {
	$('#resultskill .skill').find('.circulo').html('');
	$('#resultskill .skill').hide();
    $.each(PUNTAJE_HABILIDAD, function(key, value){
        var $skillobj=$('#resultskill .skill.sk_'+key);
        if($skillobj.length>0){
            $skillobj.show();
            var pocentaje = parseFloat(value);
            $skillobj.find('.circulo').attr('data-value', pocentaje.toFixed(0));
        } 
        $skillobj.find('.circulo').graficocircle();           		
    });

    $('.ver-respuestas').click(function(e) {
    	if($(this).hasClass('active')) { $('#pnlPreguntas').removeClass('hidden');
    	    $(this).removeClass('active').text('Ocular Respuestas');
	    	    $('body,html').stop(true,true).animate({				
				scrollTop: $('#inirespuestas').offset().top
			},1000);
    	}else{
    		$('#pnlPreguntas').addClass('hidden');
    		$(this).addClass('active').text('Mostrar Respuestas');
    		$('body,html').stop(true,true).animate({				
				scrollTop: $('#inirespuestas2').offset().top
			},1000);
    	}	
    });  

    $('.btnenformatohtml').hide();
    $('.btnadd_recordAlumno').hide();
    $('.respuestaAlumno').each(function(i,el){
        var txtarea=$(el).find('textarea');
        if(txtarea.length>0){
            $(el).find('.msjRespuesta').html('<hr><br>'+txtarea.val()+'<br><hr>');
            txtarea.hide();
        }
    })
    $('[data-cls="audio_question"]').children('audio').hide()

});
</script>