<?php $venid='ven'.date('YmdHis'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE; ?>static/libs/abelchingo/abelchingo.js"></script> 
<style type="text/css">
	#aquitable table td .alert{display: none;}
</style>
<style type="text/css">
	.estiloreporte{
		background: #fff;
    padding: 1ex;
    margin: 1ex;
    border: 1px solid #e0dddd;
    border-radius: 1ex;
    -webkit-box-shadow: 10px 10px 5px -8px rgba(133,155,199,1);
-moz-box-shadow: 10px 10px 5px -8px rgba(133,155,199,1);
box-shadow: 10px 10px 5px -8px rgba(133,155,199,1);
	}
</style>

<div class="container-fluid" id="<?php echo $venid ?>">
	<div class="vistatabla">	  
	    <div class="row-fluid">
	      <div class="span12">
	        <div class="grid simple">
	            <div class="grid-body estiloreporte">
	            <!--form id="frmbuscar" name="frmbuscar"-->
	              <div class="row">               
	                <div class="col-md-4 col-sm-6 col-xs-12">
	                    <div class="form-group">
	                      <label class="form-label">Examen</label>                      
	                      <select name="busidexamen" id="busidexamen" class="select2 form-control"  >
	                      	<?php if(!empty($this->exam))
	                      	foreach ($this->exam as $k => $exa) {    ?>
	                        <option value="<?php echo $exa["idexamen"];?>"><?php echo $exa["titulo"];?></option>
	                    	<?php }?>
	                      </select>                        
	                    </div>
	                </div>
	                <div class="col-md-4 col-sm-6 col-xs-12">
	                    <div class="form-group">
	                      <label class="form-label">Estudiante</label>
	                      <input type="text" name="bustexto" id="bustexto" class="form-control" placeholder="Ej. Abel ">
	                    </div>
	                </div>
	                <div class="col-md-4 col-sm-12 col-xs-12 text-center"><br>
	                	<button class="btn btn-primary btnbuscar" ><i class="fa fa-search"></i> Buscar</button>
	                </div>
	              </div>
	            <!--/form-->
	          </div>
	          <div class="grid-body estiloreporte" id="aquitable" >
	          </div>          
	        </div>    
	      </div>
	    </div>
	 </div>
 </div>
 <!--script type="text/javascript" src="<?php echo URL_BASE; ?>static/libs/abelchingo/abelchingo.js"></script--> 
  
 <script type="text/javascript">
 	$(document).ready(function(){
 		$fun=new _acht_();
 		var ventanaid='<?php echo $venid; ?>'
 		var ventana=$('#'+ventanaid);
 		$('.btnbuscar').click(function(ev){ vista_tabla(); })
 		$('.busidexamen').change(function(ev){ vista_tabla(); })
 		var paises={
                    "AF":"Afghanistan","AL":"Albania","DZ":"Algeria","AS":"American Samoa","AD":"Andorra","AO":"Angola","AI":"Anguilla","AQ":"Antarctica","AG":"Antigua and Barbuda","AR":"Argentina","AM":"Armenia","AW":"Aruba","AU":"Australia","AT":"Austria","AZ":"Azerbaijan","BS":"Bahamas","BH":"Bahrain","BD":"Bangladesh","BB":"Barbados","BY":"Belarus","BE":"Belgium","BZ":"Belize","BJ":"Benin","BM":"Bermuda","BT":"Bhutan","BO":"Bolivia","BA":"Bosnia and Herzegovina","BW":"Botswana","BR":"Brazil","IO":"British Indian Ocean Territory","VG":"British Virgin Islands","BN":"Brunei","BG":"Bulgaria","BF":"Burkina Faso","BI":"Burundi","KH":"Cambodia","CM":"Cameroon","CA":"Canada","CV":"Cape Verde","KY":"Cayman Islands","CF":"Central African Republic","TD":"Chad","CL":"Chile","CN":"China","CX":"Christmas Island","CC":"Cocos Islands","CO":"Colombia","KM":"Comoros","CK":"Cook Islands","CR":"Costa Rica","HR":"Croatia","CU":"Cuba","CW":"Curacao","CY":"Cyprus","CZ":"Czech Republic","CD":"Democratic Republic of the Congo","DK":"Denmark","DJ":"Djibouti","DM":"Dominica","DO":"Dominican Republic","TL":"East Timor","EC":"Ecuador","EG":"Egypt","SV":"El Salvador","GQ":"Equatorial Guinea","ER":"Eritrea","EE":"Estonia","ET":"Ethiopia","FK":"Falkland Islands","FO":"Faroe Islands","FJ":"Fiji","FI":"Finland","FR":"France","PF":"French Polynesia","GA":"Gabon","GM":"Gambia","GE":"Georgia","DE":"Germany","GH":"Ghana","GI":"Gibraltar","GR":"Greece","GL":"Greenland","GD":"Grenada","GU":"Guam","GT":"Guatemala","GG":"Guernsey","GN":"Guinea","GW":"Guinea-Bissau","GY":"Guyana","HT":"Haiti","HN":"Honduras","HK":"Hong Kong","HU":"Hungary","IS":"Iceland","IN":"India","ID":"Indonesia","IR":"Iran","IQ":"Iraq","IE":"Ireland","IM":"Isle of Man","IL":"Israel","IT":"Italy","CI":"Ivory Coast","JM":"Jamaica","JP":"Japan","JE":"Jersey","JO":"Jordan","KZ":"Kazakhstan","KE":"Kenya","KI":"Kiribati","XK":"Kosovo","KW":"Kuwait","KG":"Kyrgyzstan","LA":"Laos","LV":"Latvia","LB":"Lebanon","LS":"Lesotho","LR":"Liberia","LY":"Libya","LI":"Liechtenstein","LT":"Lithuania","LU":"Luxembourg","MO":"Macau","MK":"Macedonia","MG":"Madagascar","MW":"Malawi","MY":"Malaysia","MV":"Maldives","ML":"Mali","MT":"Malta","MH":"Marshall Islands","MR":"Mauritania","MU":"Mauritius","YT":"Mayotte","MX":"Mexico","FM":"Micronesia","MD":"Moldova","MC":"Monaco","MN":"Mongolia","ME":"Montenegro","MS":"Montserrat","MA":"Morocco","MZ":"Mozambique","MM":"Myanmar","NA":"Namibia","NR":"Nauru","NP":"Nepal","NL":"Netherlands","AN":"Netherlands Antilles","NC":"New Caledonia","NZ":"New Zealand","NI":"Nicaragua","NE":"Niger","NG":"Nigeria","NU":"Niue","KP":"North Korea","MP":"Northern Mariana Islands","NO":"Norway","OM":"Oman","PK":"Pakistan","PW":"Palau","PS":"Palestine","PA":"Panama","PG":"Papua New Guinea","PY":"Paraguay","PE":"Peru","PH":"Philippines","PN":"Pitcairn","PL":"Poland","PT":"Portugal","PR":"Puerto Rico","QA":"Qatar","CG":"Republic of the Congo","RE":"Reunion","RO":"Romania","RU":"Russia","RW":"Rwanda","BL":"Saint Barthelemy","SH":"Saint Helena","KN":"Saint Kitts and Nevis","LC":"Saint Lucia","MF":"Saint Martin","PM":"Saint Pierre and Miquelon","VC":"Saint Vincent and the Grenadines","WS":"Samoa","SM":"San Marino","ST":"Sao Tome and Principe","SA":"Saudi Arabia","SN":"Senegal","RS":"Serbia","SC":"Seychelles","SL":"Sierra Leone","SG":"Singapore","SX":"Sint Maarten","SK":"Slovakia","SI":"Slovenia","SB":"Solomon Islands","SO":"Somalia","ZA":"South Africa","KR":"South Korea","SS":"South Sudan","ES":"Spain","LK":"Sri Lanka","SD":"Sudan","SR":"Suriname","SJ":"Svalbard and Jan Mayen","SZ":"Swaziland","SE":"Sweden","CH":"Switzerland","SY":"Syria","TW":"Taiwan","TJ":"Tajikistan","TZ":"Tanzania","TH":"Thailand","TG":"Togo","TK":"Tokelau","TO":"Tonga","TT":"Trinidad and Tobago","TN":"Tunisia","TR":"Turkey","TM":"Turkmenistan","TC":"Turks and Caicos Islands","TV":"Tuvalu","VI":"U.S. Virgin Islands","UG":"Uganda","UA":"Ukraine","AE":"United Arab Emirates","GB":"United Kingdom","US":"United States","UY":"Uruguay","UZ":"Uzbekistan","VU":"Vanuatu","VA":"Vatican","VE":"Venezuela","VN":"Vietnam","WF":"Wallis and Futuna","EH":"Western Sahara","YE":"Yemen","ZM":"Zambia","ZW":"Zimbabwe"}
        var  vista_tabla=function(){	
			var data=new FormData();
				data.append('texto', $('input#bustexto').val()||'');
				data.append('estado', 1);
				data.append('idexamen',$('select#busidexamen').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${ventanaid}>">
		            <thead>
		              <tr>
		              	<th>#</th>	             
		                <th>Persona</th>
		            	<th>Pais/Ciudad/Compañia</th>		            	
		            	<th>Nota</th>		       
		            	<th></th>
		            	</tr>
		            </thead>
		            <tbody></tbody></table>
				`;
				ventana.find('#aquitable').html(html);
				$fun.postData({url:_sysUrlBase_+'quiz/examensinlogin/listado/',data:data}).then(rs => {
					html='';
					let j=0;
					forEach(rs,function(v,i){
						let nota=$(v.notatexto);
						if(nota.find('h3').children('b').length>0)
							nota=nota.find('h3').text();
						else
							nota=nota.find('h3').text();
						j++;
						html+=`<tr id="${v.idnota}" idpk="idnota">							
							<td>${j}</td>
							<td>${v.nombres+'<br><i class="fa fa-envelope"></i> '+v.correo+'<br><i class="fa fa-phone"></i> '+v.telefono}</td>	
							<td>${paises[v.pais]+'<br>'+v.ciudad+'<br>'+(v.conpania||'')}</td>							
							<td>${nota}</td>													
							<td>
								<!--span class="btn btn-sm btn-xs btn-mini btn-primary btnver"><i class="fa fa-eye " style=""></i></span-->
								<span class="btn btn-sm btn-xs btn-mini btn-warning btneliminar"><i class="fa fa-trash " style="color:red"></i></span>
							</td>
						</tr>`;
					})				
					ventana.find('table tbody').html(html);
					let $tabla=ventana.find('table tbody');
					$tabla.on('click','.btnver',function(ev){
						ev.preventDefault();
						//_this.idanimacion=$(this).closest('tr').attr('id');
						//_this.vista_formulario();
					});			
					$tabla.on('click','.btneliminar',function(ev){
						ev.preventDefault();
						let tr=$(this).closest('tr');
						let data={idnota:tr.attr('id')}
						Swal.fire({
						  title: 'Está seguro de eliminar este registro?',
						  text: "Esto no se podrá revertir!",
						  icon: 'warning',
						  type:'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: 'Si',
						  cancelButtonText: 'No',
						}).then((result) => {
						  if (result.value==true) {
						    $fun.postData({url:_sysUrlBase_+'quiz/examensinlogin/eliminar','data':data}).then(rs => {
					          	vista_tabla();
					        }).catch(e =>{
					        })
						  }
						})
					})
					let tb=ventana.find('table');
					let nomtable='Reporte de examenes '
					tb.DataTable({
						"searching":   false,
					});									//$fun.dtTable('#table'+ventanaid,{'bustexto':'',ordenar:true});
		        }).catch(e => {
		        	console.log(e);
		        })		
		}
		vista_tabla();
 	});
 </script>

