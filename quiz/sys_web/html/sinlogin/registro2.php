<?php 
$idgui=uniqid();
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";

if(!empty($this->proyecto)) $frm=$this->proyecto;
$ruta="static/media/web/nofoto.jpg";
$imgdefecto=$ruta;
$logodefecto=URL_MEDIA.'static/media/sinfoto.jpg';
$logo='';
if(!empty($frm["logoempresa"])) $logo=$frm["logoempresa"];
if(!empty($frm["logo"])) $logo=$frm["logo"];
$ifoto=strripos($logo, "static/");
if($ifoto>-1) $logo=$this->documento->getUrlBase().substr($logo,$ifoto);
else $logo=$logodefecto;
$jsonlogin=!empty($frm["jsonlogin"])?$frm["jsonlogin"]:"";
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.css">
<script type="text/javascript" src="<?php  echo $this->documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.min.js"></script>

<style>
    body {
        padding: 0px;
        margin: 0px;
        background-image: url('');
        background-color: #FFF;
        font-family: 'Lato', sans-serif;
    }
    .bold, .strong {
        font-weight: 700;
    }
    body, html, p {
        font-family: 'Lato', sans-serif;
        font-size: 16px;
        line-height: 24px;
    }

    .titulo h1, .titulo .h1 {
        font-family: 'Roboto Slab', sans-serif;
        /*font: normal 25px Helvetica, Arial, sans-serif;*/
        font-weight: 300;
        font-size: 24px;
        line-height: 20px;
        letter-spacing: -0.05em;
        margin: 10px 0 30px;
    }


    .titulo h1:before {
        background: rgb(126,126,126);
        background: -moz-linear-gradient(right, rgba(126,126,126,1) 0%, rgba(255,255,255,1) 100%);
        background: -webkit-linear-gradient(right, rgba(126,126,126,1) 0%,rgba(255,255,255,1) 100%);
        background: -o-linear-gradient(right, rgba(126,126,126,1) 0%,rgba(255,255,255,1) 100%);
        background: -ms-linear-gradient(right, rgba(126,126,126,1) 0%,rgba(255,255,255,1) 100%);
        background: linear-gradient(right, rgba(126,126,126,1) 0%,rgba(255,255,255,1) 100%);
        left: 0;
    }

    .titulo h1:after {
        background: rgb(126,126,126);
        background: -moz-linear-gradient(left, rgba(126,126,126,1) 0%, rgba(255,255,255,1) 100%);
        background: -webkit-linear-gradient(left, rgba(126,126,126,1) 0%,rgba(255,255,255,1) 100%);
        background: -o-linear-gradient(left, rgba(126,126,126,1) 0%,rgba(255,255,255,1) 100%);
        background: -ms-linear-gradient(left, rgba(126,126,126,1) 0%,rgba(255,255,255,1) 100%);
        background: linear-gradient(left, rgba(126,126,126,1) 0%,rgba(255,255,255,1) 100%);
        right: 0;
    }


    .titulo h1:before, .titulo h1:after {
        content: "";
        height: 1px;
        position: absolute;
        top: 40%;
        width: 15%;
    }
    button.btnsubmit{
        background-color: #34b6a2;
        border: none;
        border-radius: 5px;
        padding: 7px 15px;
        color: #fff;
        font-family: 'Lato', sans-serif;
        font-size: 20px;
        display: block;
        width: 100%;
    }
    label span {
        color:red;
    }

    .footer{
        position: fixed;
        bottom: 0px;
    }

    @media screen and (max-width: 700px){
        .footer{
            position: relative;
            bottom: 0px;
        }
    }

  
   /* .fondologin{   
        height:99.9vh;
        width:100%;
        position:relative;
        overflow:hidden;
    }
    .contentmedia {
        position: absolute;
        height: 100%;
        width: 100%;
        /*opacity: 0.95;*/
   /* }
    .contentmedia img, .contentmedia video{
        margin: auto;
        position: absolute;
        z-index: 1;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        min-width: 100%;
        min-height: 100%;
        opacity:0.85;
    }*/
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 text-center">
            <img src="<?php echo $logo; ?>" style="max-height: 75px;" class="img-fluid img-responsive" alt="<?php echo $frm["empresa"]; ?>" id="logo">
        </div>
        <div class="col-md-6 text-center"> <h3 style="padding-top: 1ex;"><?php echo $frm["empresa"]; ?></h3></div>
    </div>
</div>
<div class="container" style="max-width: 800px;">
    <div class="row">
        <div class="col-md-12 titulo text-center"><h1  style="" ><?php echo strtoupper(JrTexto::_('Register on the website')); ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="strong">
                <?php echo JrTexto::_('At the end of this test, you will receive an email with the results of your placement within the English levels of the Common European Framework of Reference for Languages (CEFR). Your identification is required through the following form');?>.</p>
        </div>
    </div>
</div>

<div class="container" style="max-width: 800px;">
    <form method="post" id="frmlogin">
        <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
        <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
        <div class="row">
            <div class="col-md-12 form-group has-feedback" style="position: relative;">
                <label><?php echo JrTexto::_('Last name and names'); ?> (<span>*</span>)</label><br>
                <input type="text" class="form-control" required="required" name="nombres" id="nombres" value=""  autocomplete="off" />
                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
            </div>
            <div class="col-md-6 form-group has-feedback" style="position: relative;">
                <label><?php echo JrTexto::_('Company'); ?></label><br>
                <input type="text" class="form-control" name="compania" id="compania" value="" autocomplete="off" />
                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
            </div>
            <div class="col-md-6 form-group has-feedback" style="position: relative;">
                <label><?php echo JrTexto::_('City') ?></label><br>
                <input type="text" class="form-control" name="ciudad" id="ciudad" value="" autocomplete="off" />
                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
            </div>
            <div class="col-md-6 form-group has-feedback" style="position: relative;">
                <label><?php echo JrTexto::_('Country') ?></label><br>
                <select name="pais" tabindex="8" class="form-control" >
                    <option selected="" value=""> </option>
                    <option value="AF">Afghanistan</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AS">American Samoa</option>
                    <option value="AD">Andorra</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AQ">Antarctica</option>
                    <option value="AG">Antigua and Barbuda</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AU">Australia</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan</option>
                    <option value="BS">Bahamas</option>
                    <option value="BH">Bahrain</option>
                    <option value="BD">Bangladesh</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BT">Bhutan</option>
                    <option value="BO">Bolivia</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BR">Brazil</option>
                    <option value="IO">British Indian Ocean Territory</option>
                    <option value="VG">British Virgin Islands</option>
                    <option value="BN">Brunei</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CA">Canada</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="CF">Central African Republic</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="CN">China</option>
                    <option value="CX">Christmas Island</option>
                    <option value="CC">Cocos Islands</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CK">Cook Islands</option>
                    <option value="CR">Costa Rica</option>
                    <option value="HR">Croatia</option>
                    <option value="CU">Cuba</option>
                    <option value="CW">Curacao</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="CD">Democratic Republic of the Congo</option>
                    <option value="DK">Denmark</option>
                    <option value="DJ">Djibouti</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="TL">East Timor</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="GQ">Equatorial Guinea</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="FK">Falkland Islands</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FR">France</option>
                    <option value="PF">French Polynesia</option>
                    <option value="GA">Gabon</option>
                    <option value="GM">Gambia</option>
                    <option value="GE">Georgia</option>
                    <option value="DE">Germany</option>
                    <option value="GH">Ghana</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GU">Guam</option>
                    <option value="GT">Guatemala</option>
                    <option value="GG">Guernsey</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea-Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HT">Haiti</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IR">Iran</option>
                    <option value="IQ">Iraq</option>
                    <option value="IE">Ireland</option>
                    <option value="IM">Isle of Man</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="CI">Ivory Coast</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JE">Jersey</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="XK">Kosovo</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Laos</option>
                    <option value="LV">Latvia</option>
                    <option value="LB">Lebanon</option>
                    <option value="LS">Lesotho</option>
                    <option value="LR">Liberia</option>
                    <option value="LY">Libya</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MO">Macau</option>
                    <option value="MK">Macedonia</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia</option>
                    <option value="MD">Moldova</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="ME">Montenegro</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanmar</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="NL">Netherlands</option>
                    <option value="AN">Netherlands Antilles</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NZ">New Zealand</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="KP">North Korea</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PK">Pakistan</option>
                    <option value="PW">Palau</option>
                    <option value="PS">Palestine</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="QA">Qatar</option>
                    <option value="CG">Republic of the Congo</option>
                    <option value="RE">Reunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russia</option>
                    <option value="RW">Rwanda</option>
                    <option value="BL">Saint Barthelemy</option>
                    <option value="SH">Saint Helena</option>
                    <option value="KN">Saint Kitts and Nevis</option>
                    <option value="LC">Saint Lucia</option>
                    <option value="MF">Saint Martin</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and the Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="ST">Sao Tome and Principe</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="RS">Serbia</option>
                    <option value="SC">Seychelles</option>
                    <option value="SL">Sierra Leone</option>
                    <option value="SG">Singapore</option>
                    <option value="SX">Sint Maarten</option>
                    <option value="SK">Slovakia</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="KR">South Korea</option>
                    <option value="SS">South Sudan</option>
                    <option value="ES">Spain</option>
                    <option value="LK">Sri Lanka</option>
                    <option value="SD">Sudan</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbard and Jan Mayen</option>
                    <option value="SZ">Swaziland</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="SY">Syria</option>
                    <option value="TW">Taiwan</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania</option>
                    <option value="TH">Thailand</option>
                    <option value="TG">Togo</option>
                    <option value="TK">Tokelau</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="VI">U.S. Virgin Islands</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="GB">United Kingdom</option>
                    <option value="US">United States</option>
                    <option value="UY">Uruguay</option>
                    <option value="UZ">Uzbekistan</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VA">Vatican</option>
                    <option value="VE">Venezuela</option>
                    <option value="VN">Vietnam</option>
                    <option value="WF">Wallis and Futuna</option>
                    <option value="EH">Western Sahara</option>
                    <option value="YE">Yemen</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option></select>
                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
            </div>
            <div class="col-md-6 form-group has-feedback" style="position: relative;">
                <label><?php echo JrTexto::_('Telephone'); ?> (<span>*</span>)</label><br>
                <input type="text" class="form-control" required="required" name="telefono" id="telefono" value="" autocomplete="off" />
                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
            </div>
            <div class="col-md-12 form-group has-feedback" style="position: relative;">
                <label><?php echo JrTexto::_('E-mail'); ?>(<span>*</span>)</label><br>
                <input type="email" class="form-control" required="required" name="correo" id="correo" value="" autocomplete="off" />
                <!--span class="fa fa-user form-control-feedback" style="position: absolute; bottom: 1ex; left:1ex !important;"></span-->
            </div>
            <div class="col-md-12" style="margin-top: -2ex; margin-bottom: 1em; color: #c35d5d;">
                <span class="help">(<span>*</span>) <?php echo JrTexto::_('Required fields'); ?></span><br>
            </div>

            <div class="col-md-12">
                <?php if(@$frm["idproyecto"]!=45){?>
                <div class="flex-column"><div class="checkbox-container required"><label class="flex-row align-center" ><input type="checkbox" name="acceptance" value="yes" tabindex="14"><span style="color:#272626">  <?php echo JrTexto::_('I declare that I agree with the internal use of the data entered in the form. Furthermore, I have been informed that I can cancel this authorization at any time'); ?>.</span></label></div></div>
                <?php } ?>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" id="#btn-enviar-formInfoLogin" class="btn btnsubmit" >
                    <?php echo JrTexto::_('Take exam')?></button>
            </div>
        </div>
    </form>
    <br>
</div>


<div class="container-fluid footer" style="    background: #7b8b8c;    ">
    <div class="row">
        <div class="col-md-12 text-center" >
            <p style=" margin: auto;     color: #c7c2c2; padding: 0.75em; ">© <?php echo date('Y'); ?> Smart Knowledge Solutions.</p>
        </div>
    </div>
</div>


<script type="text/javascript">
 var url_media=_sysUrlBase_;
 var imgdefecto='<?php echo $imgdefecto; ?>';
 var jsonlogin={tipologin:1,tipofondo:'imagen',colorfondo:'rgba(155, 146, 178, 0.41)',imagenfondo:imgdefecto,videofondo:'',logo1:''};
 var idempresa='<?php echo !empty($frm["idempresa"])?$frm["idempresa"]:0; ?>';
 var jsonproyecto=JSON.parse(`<?php echo !empty($jsonlogin)?str_replace('\n','<br>',$jsonlogin):'{}';?>`);
 //console.log(jsonproyecto);

 //var _sysIdioma_='ES';
$(document).ready(function(){
    $.extend(jsonlogin,jsonproyecto);  
    $('.contentmedia').css('background',jsonlogin.colorfondo);
    htmlfondo='';
    if(jsonlogin.tipofondo=='imagen'){
        htmlfondo='<img src="'+url_media+jsonlogin.imagenfondo+'">'
    }else if(jsonlogin.tipofondo=='video'){
        htmlfondo='<video autoplay="true" loop="" muted="" src="'+url_media+jsonlogin.videofondo+'"></video>'
    }
    
    $('.contentmedia').html(htmlfondo);
   
    $('#frmlogin').bind({
         submit: function(ev){
            ev.preventDefault();
            procesandoguardando=true;
            var btn=$(this).find('#btn-enviar-formInforecuperarclave');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            let frmdata=document.getElementById('frmlogin');
            var data=new FormData(frmdata);              
            data.append('showmsjok',true);
            __sysAyax({ 
                fromdata:data,
                url:_sysUrlBase_+'quiz/registro/guardar',                  
                callback:function(rs){  
                    Swal.close();                                     
                    if(rs.code==200){
                        Swal.fire({
                            type: 'success',//warning (!)//success
                            icon: 'success',//warning (!)//success
                            // title: rs.msj,
                            text: rs.msj,
                            allowOutsideClick: false,
                            closeOnClickOutside: false
                        }).then(rs=>{
                            window.location.href=_sysUrlBase_+'quiz/examensinlogin/?idioma=EN';
                        })                       
                    }
                }
            });            
        }
    });
});
</script>