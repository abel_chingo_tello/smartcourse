<?php 
$idgui=uniqid();
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";

if(!empty($this->proyecto)) $frm=$this->proyecto;
$ruta="static/media/web/nofoto.jpg";
$imgdefecto=$ruta;
$logodefecto=URL_MEDIA.'static/media/sinfoto.jpg';
$logo='';
if(!empty($frm["logoempresa"])) $logo=$frm["logoempresa"];
if(!empty($frm["logo"])) $logo=$frm["logo"];
$ifoto=strripos($logo, "static/");
if($ifoto>-1) $logo=$this->documento->getUrlBase().substr($logo,$ifoto);
else $logo=$logodefecto;
$jsonlogin=!empty($frm["jsonlogin"])?$frm["jsonlogin"]:"";
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.css">
<script type="text/javascript" src="<?php  echo $this->documento->getUrlStatic() ?>/libs/minicolors/jquery.minicolors.min.js"></script>

<style>
    .fondologin{   
        height:99.9vh;
        width:100%;
        position:relative;
        overflow:hidden;
    }
    .contentmedia {
        position: absolute;
        height: 100%;
        width: 100%;
        /*opacity: 0.95;*/
    }
    .contentmedia img, .contentmedia video{
        margin: auto;
        position: absolute;
        z-index: 1;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        min-width: 100%;
        min-height: 100%;
        opacity:0.85;
    }
</style>

<div class="fondologin">
    <div class="fondo contentmedia" style=""></div>
    <div class="">     
        <div id="wrapper">
            <div id="login" class="animate form ">
                <section class="login_content text-center">
                    <img src="<?php echo $logo; ?>" class="img-fluid img-responsive" alt="_logo_" id="logo">   
                    <div><br></div>  

                    <form method="post" id="frmlogin">
                        <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
                        <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo @$frm["idproyecto"];?>">

                        <h1><?php echo JrTexto::_('Regístrate Aqui');?>!</h1>
                            <div><br></div>

                        <div class="form-group has-feedback" style="position: relative;">
                            <input type="text" class="form-control" placeholder="Nombres y Apellidos" required="required" name="nombres" id="nombres" value="" maxlength="80" autocomplete="off" />
                            <span class="fa fa-user form-control-feedback" style="position: absolute; top: 1ex; left:1ex !important;"></span>
                        </div>

                        <div class="form-group has-feedback" style="position: relative;">
                            <input type="email" class="form-control" placeholder="Correo Electronico" required="required" name="correo" id="correo" value="" maxlength="80" autocomplete="off" />
                            <span class="fa fa-envelope form-control-feedback" style="position: absolute; top: 1ex; left:1ex !important;"></span>
                        </div>
                        
                        <div class="form-group has-feedback" style="position: relative;">
                            <input type="text" class="form-control" placeholder="Numero de Telefono" required="required" name="telefono" id="telefono" value="" maxlength="80" autocomplete="off"/>
                            <span class="fa fa-phone form-control-feedback" style="position: absolute; top: 1ex; left:1ex !important;"></span>
                        </div>

                        <!--div class="form-group has-feedback" style="position: relative;">
                            <input type="password" autocomplete="off" class="form-control" placeholder="<?php echo JrTexto::_('Password')?>" required="required" name="clave" id="clave"/>
                            <span class="fa fa-lock form-control-feedback" style="position: absolute; top: 1ex; left:1ex !important;"></span>
                        </div-->

                        <div>
                            <button type="submit" id="#btn-enviar-formInfoLogin" class="btn btn-blue" style=" border-radius: 5px !important;">
                            <i class="fa fa-user-plus"></i> <?php echo JrTexto::_('Registrar Persona')?></button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <!--p class="change_link"><?php echo JrTexto::_('have you forgotten your password?')?>
                                <a href="#torecuperar" class="to_recuperar"> <?php echo JrTexto::_('Restore password')?></a>
                            </p-->
                            <div class="clearfix"></div>
                            <br />
                            <!--div>
                                <h1></h1>
                                <p>© 2016 <?php //echo JrTexto::_('All rights reserved').' <br>'.$this->pasarHtml($this->oNegConfig->get('nombre_sitio'));?> </p>
                            </div-->
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
           
        </div>
    </div>
</div>

    
    <script type="text/javascript">
 var url_media=_sysUrlBase_;
 var imgdefecto='<?php echo $imgdefecto; ?>';
 var jsonlogin={tipologin:1,tipofondo:'imagen',colorfondo:'rgba(155, 146, 178, 0.41)',imagenfondo:imgdefecto,videofondo:'',logo1:''};
 var idempresa='<?php echo !empty($frm["idempresa"])?$frm["idempresa"]:0; ?>';
 var jsonproyecto=JSON.parse(`<?php echo !empty($jsonlogin)?str_replace('\n','<br>',$jsonlogin):'{}';?>`);
 //console.log(jsonproyecto);

 //var _sysIdioma_='ES';
$(document).ready(function(){
    $.extend(jsonlogin,jsonproyecto);  
    $('.contentmedia').css('background',jsonlogin.colorfondo);
    htmlfondo='';
    if(jsonlogin.tipofondo=='imagen'){
        htmlfondo='<img src="'+url_media+jsonlogin.imagenfondo+'">'
    }else if(jsonlogin.tipofondo=='video'){
        htmlfondo='<video autoplay="true" loop="" muted="" src="'+url_media+jsonlogin.videofondo+'"></video>'
    }
    
    $('.contentmedia').html(htmlfondo);
   
    $('#frmlogin').bind({
         submit: function(ev){
            ev.preventDefault();
            procesandoguardando=true;
            var btn=$(this).find('#btn-enviar-formInforecuperarclave');
            if(btn.hasClass('disabled')) return false;
            btn.addClass('disabled').attr('disabled','disabled');
            Swal.showLoading({type:'info', html:'Procesando'});
            let frmdata=document.getElementById('frmlogin');
            var data=new FormData(frmdata);              
            data.append('showmsjok',true);
            __sysAyax({ 
                fromdata:data,
                url:_sysUrlBase_+'quiz/registro/guardar',                  
                callback:function(rs){  
                    Swal.close();                                     
                    if(rs.code==200){
                        Swal.fire({
                            type: 'success',//warning (!)//success
                            icon: 'success',//warning (!)//success
                            // title: rs.msj,
                            text: rs.msj,
                            allowOutsideClick: false,
                            closeOnClickOutside: false
                        }).then(rs=>{
                            window.location.href=_sysUrlBase_+'quiz/examensinlogin/?idioma=EN';
                        })                       
                    }
                }
            });            
        }
    });
});
</script>