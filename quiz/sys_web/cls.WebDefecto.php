 <?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017 
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegProyecto_config', RUTA_BASE, 'sys_negocio');*/

class WebDefecto extends JrWeb
{
	protected $oNegExamenes;
    protected $userquiz=array();
    //private $oNegCursoDetalle;
	//protected $oNegpreguntas;
    //protected $oNegMetodologia;
    //protected $oNegExamen_alumno;
    //protected $oNegAlumno;
    //protected $oNegNegPersonal;
    //protected $oNegProyecto_config;

	public function __construct()
	{
		global $aplicacion;
        if(false === NegSesion::existeSesion()){
            header('Location: '.URL_BASE);
            exit();
        } 
        $user=NegSesion::getUsuario();
        parent::__construct(); 
		$this->oNegExamenes = new NegQuizexamenes;
        $this->userquiz=$this->oNegExamenes->persona();
	}
	public function defecto(){
        return $this->examenes();
	}

    public function examenes(){
        try{
            global $aplicacion;
            if(NegSesion::existeSesion()==false){
                return $aplicacion->redir();
            }
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

            $user=NegSesion::getUsuario();
            $this->idrol=$user["idrol"];
            $this->tuser=$user["tuser"];
            $this->idusuario=$user["idpersona"];
            $this->usuario=$user;
            $this->idproyecto=$user["idproyecto"];
            $this->quizidpersona=$this->userquiz["quiz_idpersona"];
            $this->quizidproyecto=$this->userquiz["quiz_idproyecto"];
            //var_dump($this->userquiz);
            $this->documento->plantilla = 'general';
            $this->esquema = 'listado';
            return parent::getEsquema();
        }catch(Exception $ex){
             throw new Exception(JrTexto::_('Error, al mostrar examenes').'!');
        }
    }
}