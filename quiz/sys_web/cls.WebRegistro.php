 <?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017 
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
//JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
//JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');*/
//JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegProyecto_config', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegSinlogin_persona', RUTA_BASE);
class WebRegistro extends JrWeb
{
	protected $oNegExamenes;
    protected $userquiz=array();
    private $oNegSinlogin_persona;
    
    //private $oNegCursoDetalle;
	//protected $oNegpreguntas;
    //protected $oNegMetodologia;
    //protected $oNegExamen_alumno;
    //protected $oNegAlumno;
    //protected $oNegNegPersonal;
    //protected $oNegProyecto_config;

	public function __construct()
	{

        global $aplicacion;  

        $this->oNegSinlogin_persona = new NegSinlogin_persona;
        //$user=NegSesion::getUsuario();
        parent::__construct(); 
		//$this->oNegExamenes = new NegQuizexamenes;
        //$this->userquiz=$this->oNegExamenes->persona();
	}
	public function defecto(){
        try{
            global $aplicacion;
            $this->documento->plantilla = 'registro';            
            $_url=$_SERVER["HTTP_HOST"];
            $_url=str_replace("www.", '', $_url);
            $_haysubdomain=stripos($_url,".");
            $_urlsubdomain='abacolima';
            if($_haysubdomain==true){
                $validar=substr($_url, 0,$_haysubdomain);
                $_bussubdomain=stripos($validar,"/");
                if($_bussubdomain===false){
                    $_urlsubdomain=substr($_url,0,$_haysubdomain);
                }
            }
            $sesion = JrSession::getInstancia();
            //$this->proyecto = $sesion->getAll('_infoempresa_');
            //if(empty($this->proyecto)){
            JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
            $oNegProyecto = new NegProyecto;
            $hayproyecto=$oNegProyecto->buscar(array('sesionlogin'=>$_urlsubdomain));
            if(empty($hayproyecto[0])) $hayproyecto=$oNegProyecto->buscar(array('sesionlogin'=>'abacolima'));
            foreach ($hayproyecto[0] as $key => $value) {
                NegSesion::setEmpresa($key,$value);
            }
            $this->proyecto = $sesion->getAll('_infoempresa_');
            $idproyecto=$this->proyecto["idproyecto"];
            JrCargador::clase('sys_negocio::NegSinlogin_quizproyecto', RUTA_BASE);
            $NegProyectoquiz=new NegSinlogin_quizproyecto;
            $idexamen=!empty($_REQUEST["idexamen"])?$_REQUEST["idexamen"]:0;            
            if(empty($idexamen)){
                $this->msj=JrTexto::_("The Exam indicated, it is not correct, check your URL address please");
                $this->esquema = 'error/general';
                return parent::getEsquema();                
            }

            $proyectoquiz=$NegProyectoquiz->buscar(array('idquiz'=>$idexamen,'idproyecto'=>$idproyecto));
            if(empty($proyectoquiz[0])){
                $this->msj=JrTexto::_("This company has not yet assigned the indicated exam");
                $this->esquema = 'error/general';
                return parent::getEsquema();
                
            }
            $this->esquema = 'sinlogin/registro2';
            NegSesion::setEmpresa('Examensinlogin',$idexamen); 
            return parent::getEsquema();
        }catch(Exception $ex){
            throw new Exception(JrTexto::_('Error, al mostrar examenes').'!');
        }
	}



    //funciones json
    public function listado(){
        $this->documento->plantilla = 'blanco';
        try{
            global $aplicacion;         
            $filtros=array();
            if(isset($_REQUEST["nombres"])&&@$_REQUEST["nombres"]!='')$filtros["nombres"]=$_REQUEST["nombres"];
            if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
            if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
            if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
            if(isset($_REQUEST["compania"])&&@$_REQUEST["compania"]!='')$filtros["compania"]=$_REQUEST["compania"];
            if(isset($_REQUEST["pais"])&&@$_REQUEST["pais"]!='')$filtros["pais"]=$_REQUEST["pais"];
            if(isset($_REQUEST["ciudad"])&&@$_REQUEST["ciudad"]!='')$filtros["ciudad"]=$_REQUEST["ciudad"];
                        
            if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];         
            $this->datos=$this->oNegSinlogin_persona->buscar($filtros);
            echo json_encode(array('code'=>200,'data'=>$this->datos));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
    }

    public function guardar(){
        $this->documento->plantilla = 'blanco';
        try {
            global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idpersona)) {
                $this->oNegSinlogin_persona->idpersona = $idpersona;
                $accion='_edit';
            }
            //$usuarioAct = NegSesion::getUsuario();         
            
            $this->oNegSinlogin_persona->nombres=@$nombres;
            $this->oNegSinlogin_persona->telefono=@$telefono;
            $this->oNegSinlogin_persona->correo=@$correo;
            $this->oNegSinlogin_persona->idproyecto=@$idproyecto;
            $this->oNegSinlogin_persona->compania=@$compania;
            $this->oNegSinlogin_persona->pais=@$pais;
            $this->oNegSinlogin_persona->ciudad=@$ciudad;  

            if($accion=='_add'){
                $res=$this->oNegSinlogin_persona->agregar();
                 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
                $res=$this->oNegSinlogin_persona->editar();
                echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            NegSesion::setEmpresa('idpersona',$res);
                        
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
    }

    public function eliminar(){
        try {
            if(empty($_REQUEST)){ 
                echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
                exit(0);
            }
            $this->oNegSinlogin_persona->__set('idpersona', $_REQUEST['idpersona']);
            $res=$this->oNegSinlogin_persona->eliminar();           
            echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
            exit(0);
        }
    }

    public function setCampo(){
        try {
            if(empty($_REQUEST)){ 
                echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
                exit(0);
            }
            $this->oNegSinlogin_persona->setCampo($_REQUEST['idpersona'],$_REQUEST['campo'],$_REQUEST['valor']);
            echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
            exit(0);
        }
    }
}