 <?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017 
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegSinlogin_quiz', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegSinlogin_persona', RUTA_BASE);
//JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegProyecto_config', RUTA_BASE, 'sys_negocio');*/

class WebExamensinlogin extends JrWeb
{
	protected $oNegExamenes;
    //private $oNegCursoDetalle;
    protected $oNegPersonal;
    protected $oNegNotas_quiz;

	/*protected $oNegpreguntas;
    protected $oNegExamen_alumno;
    protected $oNegAlumno;
    protected $oNegProyecto_config;*/

	public function __construct()
	{		
        parent::__construct();   

		$this->oNegExamenes = new NegQuizexamenes;
		$this->oNegNotas_quiz = new NegSinlogin_quiz;
		$this->oNegPersonal = new NegSinlogin_persona;
        //$this->oNegCursoDetalle = new NegAcad_cursodetalle;
       // exit("aaaaaaaaaaa");
		/*$this->oNegpreguntas = new NegExamenes_preguntas;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegExamen_alumno = new NegExamen_alumno;
        $this->oNegAlumno = new NegAlumno;        
        $this->oNegProyecto_config = new NegProyecto_config;*/
        //$this->resolver();
	}
	public function defecto(){
        try {
            global $aplicacion;
            $this->librerias();
            $sesion = JrSession::getInstancia();
            $empresa=$this->proyecto = $sesion->getAll('_infoempresa_');
            $this->idexamen=$this->proyecto["Examensinlogin"];
            $filtros["idexamen"]=$this->idexamen;
            // var_dump($filtros["idexamen"]);
            $this->nro_intento = 0;
            $this->cant_intentos = 5;
            $this->max_intento = 9999;
            if(empty($empresa)){
                throw new Exception(JrTexto::_('You have to register first').'!');
            }

            $user=array('idpersona'=>$empresa["idpersona"],'idproyecto'=>$empresa["idproyecto"],'idempresa'=>$empresa["idempresa"]);

            $this->datosoffline=base64_encode(json_encode($user));
           
            $this->tipo = 'U'; 
            $exam=$this->oNegExamenes->buscar(array('idexamen'=>$this->idexamen));
            $calificacion='M';
            $oderpor="notafinal";
            if(!empty($exam)){
                $this->examen=$exam[0];
                $calificacion=$this->examen["calificacion"];
                $idioma=!empty($_REQUEST["idioma"])?$_REQUEST["idioma"]:@$this->examen["idioma"];
                if($calificacion=="M") $oderpor="nota";
            }else{
                throw new Exception(JrTexto::_('Exam does not exist').'!');
            }

            $this->nro_intento=1;         
            $this->habilidades=json_decode($this->examen['habilidades_todas'], true);
            $filtros_preg=array();
            $filtros_preg["idexamen"]=$this->idexamen;
            $filtros_preg["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
            $this->preguntas=$this->oNegExamenes->mostrarPreguntas($filtros_preg);
            if(empty($this->preguntas)){
                throw new Exception(JrTexto::_("There are no question in this exam").".");
            }

            $this->documento->plantilla = 'verblanco'; //'examenes/general';
            $this->esquema = 'sinlogin/examen';
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }

	}
    public function librerias(){
        $this->documento->script('tinymce.min', '/libs/tinymce/');
        $this->documento->script('encode', '/libs/chingo/');
        //$this->documento->script('', URL_TEMA.'js/chingo/encode.js');
        $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
        $this->documento->script('jquery.maskedinput.min', '/tema/js/');
        $this->documento->script('', URL_TEMA.'js/chingo/cronometro.js');
        $this->documento->script('', URL_TEMA.'js/chingo/mitimer.js');
        $this->documento->script('jquery-ui.min', '/tema/js/');
        $this->documento->script('jquery.ui.touch-punch.min','/libs/jquery-ui-touch/');

        $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery-confirm.min', '/libs/alert/');
        $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery.md5', '/tema/js/');
        $this->documento->stylesheet('',URL_TEMA.'js/circletimer/circletimer.css');
        $this->documento->script('',URL_TEMA.'js/circletimer/circletimer.min.js');

        $this->documento->script('', URL_TEMA.'js/examen/multiplantilla.js?vs'._version_);
        $this->documento->script('', URL_TEMA.'js/examen/true_false.js');
        $this->documento->script('', URL_TEMA.'js/examen/order_simple.js');
        $this->documento->script('', URL_TEMA.'js/examen/order_paragraph.js');
        $this->documento->script('', URL_TEMA.'js/examen/join.js');
        $this->documento->script('', URL_TEMA.'js/examen/tag_image.js');
        $this->documento->script('', URL_TEMA.'js/examen/editactividad.js');
        $this->documento->script('', URL_TEMA.'js/examen/actividad_completar.js');
         //$this->documento->script('speach', URL_TEMA.'js/new/');
        $this->documento->script('', URL_TEMA.'js/funciones.js');
        $this->documento->script('', URL_TEMA.'js/graficos/progressbar/progressbar.min.js');
        $this->documento->script('', URL_TEMA.'js/graficos/progressbar/circle.js');
         
        $this->documento->script('', URL_TEMA.'js/speach/audioRecorder.js');
        $this->documento->script('', URL_TEMA.'js/audiorecord/wavesurfer.min.js');
        $this->documento->script('', URL_TEMA.'js/speach/callbackManager.js');
        $this->documento->stylesheet('',  URL_TEMA.'js/examen/speach.css');
        $this->documento->script('',  URL_TEMA.'js/examen/speach.js');
        //$this->documento->script('nspeach',  URL_TEMA.'js/new/');
        $this->documento->script('', URL_TEMA.'js/examen/alu_preview.js');
        $this->documento->stylesheet('',  URL_TEMA.'js/examen/actividad_nlsw.css');
        $this->documento->script('', URL_TEMA.'js/examen/actividad_nlsw.js');
    }


    public function listado(){
        $this->documento->plantilla = 'blanco';
        try{
            global $aplicacion;         
            //if(!NegSesion::tiene_acceso('Sinlogin_quiz', 'list')) {
            //  echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
            //  exit(0);
            //}
            $filtros=array();
            if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
            if(isset($_REQUEST["idexamen"])&&@$_REQUEST["idexamen"]!='')$filtros["idexamen"]=$_REQUEST["idexamen"];
            if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
            if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
            if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
            if(isset($_REQUEST["notatexto"])&&@$_REQUEST["notatexto"]!='')$filtros["notatexto"]=$_REQUEST["notatexto"];
            if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
            if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
            if(isset($_REQUEST["calificacion_en"])&&@$_REQUEST["calificacion_en"]!='')$filtros["calificacion_en"]=$_REQUEST["calificacion_en"];
            if(isset($_REQUEST["calificacion_total"])&&@$_REQUEST["calificacion_total"]!='')$filtros["calificacion_total"]=$_REQUEST["calificacion_total"];
            if(isset($_REQUEST["calificacion_min"])&&@$_REQUEST["calificacion_min"]!='')$filtros["calificacion_min"]=$_REQUEST["calificacion_min"];
            if(isset($_REQUEST["tiempo_total"])&&@$_REQUEST["tiempo_total"]!='')$filtros["tiempo_total"]=$_REQUEST["tiempo_total"];
            if(isset($_REQUEST["tiempo_realizado"])&&@$_REQUEST["tiempo_realizado"]!='')$filtros["tiempo_realizado"]=$_REQUEST["tiempo_realizado"];
            if(isset($_REQUEST["calificacion"])&&@$_REQUEST["calificacion"]!='')$filtros["calificacion"]=$_REQUEST["calificacion"];
            if(isset($_REQUEST["habilidades"])&&@$_REQUEST["habilidades"]!='')$filtros["habilidades"]=$_REQUEST["habilidades"];
            if(isset($_REQUEST["habilidad_puntaje"])&&@$_REQUEST["habilidad_puntaje"]!='')$filtros["habilidad_puntaje"]=$_REQUEST["habilidad_puntaje"];
            if(isset($_REQUEST["intento"])&&@$_REQUEST["intento"]!='')$filtros["intento"]=$_REQUEST["intento"];
            if(isset($_REQUEST["datos"])&&@$_REQUEST["datos"]!='')$filtros["datos"]=$_REQUEST["datos"];
            if(isset($_REQUEST["fechamodificacion"])&&@$_REQUEST["fechamodificacion"]!='')$filtros["fechamodificacion"]=$_REQUEST["fechamodificacion"];
            if(isset($_REQUEST["preguntas"])&&@$_REQUEST["preguntas"]!='')$filtros["preguntas"]=$_REQUEST["preguntas"];
            if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];           
            if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];  
            if(empty($filtros["idproyecto"])){
                $user=NegSesion::getUsuario();
                $filtros["idproyecto"]=$user["idproyecto"];
            }
            $this->datos=$this->oNegNotas_quiz->buscar($filtros);
            echo json_encode(array('code'=>200,'data'=>$this->datos));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
    }

    public function guardar(){
        $this->documento->plantilla = 'blanco';
        try {
            global $aplicacion;
            if(empty($_REQUEST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Data incomplete')));
                exit(0);            
            }
            $sesion = JrSession::getInstancia();
            $empresa=$this->proyecto = $sesion->getAll('_infoempresa_');

            $dtoff=json_decode(base64_decode($_REQUEST["datosoffline"]),true);   
            if(empty($empresa)){
                JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
                $oNegProyecto = new NegProyecto;
                $hayproyecto=$oNegProyecto->buscar(array('idproyecto'=>$dtoff['idproyecto']));               
                foreach ($hayproyecto[0] as $key => $value) {
                    NegSesion::setEmpresa($key,$value);
                }
                NegSesion::setEmpresa('idpersona',$dtoff['idpersona']);
                $empresa = $sesion->getAll('_infoempresa_');
            }

            if(empty($empresa["idpersona"])){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Please re-register an unexpected error occurred')));
                exit(0);            
            }

            $res=0;
            @extract($_REQUEST);
            $accion='_add';
            

            $idpersona=$empresa["idpersona"];
          
            $strJson = array();
            $strJson['tipo'] = (isset($_REQUEST['tipexamen'])) ? $_REQUEST['tipexamen'] : @$tipo; 
            $strJson['num_examen'] = (isset($_REQUEST['numexamen'])) ? $_REQUEST['numexamen'] : 0; 
            $preguntas = str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',$preguntas);
            $idproyecto=$empresa["idproyecto"];
            $idempresa=$empresa["idempresa"];

            $this->oNegNotas_quiz->idpersona = $idpersona;
            //$this->oNegNotas_quiz->idnota=@$idnota;
            $this->oNegNotas_quiz->idexamen=@$idexamen;
            $this->oNegNotas_quiz->tipo=@$tipo;
            $this->oNegNotas_quiz->nota=@$nota;
            $this->oNegNotas_quiz->notatexto=@$notatexto;

            $this->oNegNotas_quiz->idproyecto=@$idproyecto;
            $this->oNegNotas_quiz->calificacion_en=@$calificacion_en;
            $this->oNegNotas_quiz->calificacion_total=@$calificacion_total;
            $this->oNegNotas_quiz->calificacion_min=@$calificacion_min;
            $this->oNegNotas_quiz->tiempo_total=@$tiempo_total;
            $this->oNegNotas_quiz->tiempo_realizado=@$tiempo_realizado;
            $this->oNegNotas_quiz->calificacion=@$calificacion;
            $this->oNegNotas_quiz->habilidades=@$habilidades;
            $this->oNegNotas_quiz->habilidad_puntaje=@$habilidad_puntaje;
            $this->oNegNotas_quiz->intento=!empty($intento)?$intento:1;;
            $this->oNegNotas_quiz->datos=@$datos;
            $this->oNegNotas_quiz->fechamodificacion=@$fechamodificacion;
            //$this->oNegNotas_quiz->preguntas=@$preguntas;           
           
            $this->oNegNotas_quiz->datos=json_encode($strJson);;
            $this->oNegNotas_quiz->preguntasoffline=@$preguntasoffline;
            $_COOKIE['idexamen']='terminado';
            //Falta Actualizar el tiempo del examen. 

            $archivo=RUTA_BASE.'static'.SD.'media'.SD.'examenes_sinlogin';
            @mkdir($archivo,0777,true);
            @chmod($archivo, 0777);
            $archivo=$archivo.SD.'E'.$idempresa."_P".$idproyecto;
            @mkdir($archivo,0777,true);
            @chmod($archivo, 0777);
            
            $archivo=$archivo.SD.'EX_'.$idexamen."_A".$idpersona."_I".(!empty($intento)?$intento:1).".php";
            if(file_exists($archivo)) {
                @rename($archivo,$archivo."_".date('YMDHis'));
                @chmod($archivo, 0777);
            }
            $fp = fopen($archivo, "a"); 

            $txtpreguntas=base64_encode(preg_replace('/\s+/', ' ', $preguntas));
            $textexamen = <<<STREXAMEN
<?php 
\$preguntas=<<<STRCHINGO
    $txtpreguntas   
STRCHINGO;
\$preguntas=base64_decode(trim(\$preguntas));
?>
STREXAMEN;
$write = fputs($fp,$textexamen);
            fclose($fp);
            @chmod($archivo,0777);
            $mensajefinal='';
            ////////////////////
            //si se quiere  guardar solo el static
            $pos1=stripos($archivo,'static'.SD.'media'.SD.'examenes_sinlogin');
            if ($pos1 !== false) {
                $archivo=substr($archivo,$pos1);
            }

            $dtoff['archivo']=$archivo;

            $this->oNegNotas_quiz->preguntas = json_encode($dtoff);
            $mensajefinal='';   
            if($accion=='_add'){
                $res=$this->oNegNotas_quiz->agregar();              
                $mensajefinal=array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Assessment')).' '.JrTexto::_('saved successfully'),'newid'=>$res);
            }  

            $infoUsuario = $this->oNegPersonal->buscar(array('idpersona'=>$idpersona)); 
            if(!empty($infoUsuario[0])) $infoUsuario=$infoUsuario[0];
            $this->empresa=$empresa;
            $this->empresa["nombre"]=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:@$this->empresa["empresa"];
            $proyecto=$empresa;
            JrCargador::clase('jrAdwen::JrCorreo');
            $oCorreo = new JrCorreo;
            if(!empty($proyecto['correo_servidor']) && !empty($proyecto['correo_puerto']) && !empty($proyecto['correo_email']) && !empty($proyecto['correo_clave'])){
                
                /*$oCorreo->Host=$proyecto['correo_servidor'];
                $oCorreo->Port=intval($proyecto['correo_puerto']);
                $oCorreo->Username=$proyecto['correo_email'];
                $oCorreo->Password=$proyecto['correo_clave']; */              
            }
            $asunto=JrTexto::_('Result Exam');
            $titulo='Examen de '.$infoUsuario["nombres"];
            $deemail=!empty($this->empresa["correoempresa"])?$this->empresa["correoempresa"]:$infoUsuario["email"];
            $denombre=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:$infoUsuario["nombre"];
            $oCorreo->setRemitente($deemail,$denombre);
            $this->titulo=$titulo;
            $this->Asunto=$asunto." - ".$titulo;
            $this->mensaje='<h2 style="text-align:center"><strong>Estudiante : '.$infoUsuario["nombres"].'</strong></h2><br>';
            $this->mensaje.='<div style="text-align:center">Nota Obtenida '.$nota."<br>";
            $this->mensaje.=@$notatexto;
            $this->mensaje.="<div>";
            $oCorreo->setAsunto($this->Asunto);
            //$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com','Abel');
            //$oCorreo->setRemitente($usuarioAct["email"],$usuarioAct["nombre_full"]);
            if(!empty($infoUsuario["correo"]))
                $oCorreo->addDestinarioPhpmailer($infoUsuario["correo"],$infoUsuario["nombres"]);
            if(!empty($this->empresa["correoempresa"])){
                $oCorreo->setRemitente($this->empresa["correoempresa"],$this->empresa["nombre"]);
                $oCorreo->addDestinarioPhpmailer($this->empresa["correoempresa"],$this->empresa["nombre"]);
            }elseif(!empty($infoUsuario["correo"])) {
                $oCorreo->setRemitente($infoUsuario["correo"],$infoUsuario["nombres"]);
            }

            $filelogo=!empty($this->empresa["logo"])?$this->empresa["logo"]:$this->empresa["logoempresa"];

            if(!empty($filelogo)){
                $ipost=stripos($filelogo,'?');
                if($ipost===false) $filelogo=$filelogo;
                else $filelogo=substr($filelogo,0,$ipost);
            }

            if(!empty($filelogo) && is_file(RUTA_BASE.$filelogo)){
                $ipost=stripos($filelogo,'?');
                if($ipost===false) $this->logoempresa=$filelogo;
                else $this->logoempresa=substr($filelogo,0,$ipost);
                $img=str_replace('\\', '/', RUTA_BASE.$this->logoempresa);
                $oCorreo->addadjuntos(RUTA_BASE.$this->logoempresa,'logoempresa');
            }
           
            $this->para='';
            $this->esquema = 'sinlogin/correo';
            $oCorreo->setMensaje(parent::getEsquema());
            try{
                $envio=$oCorreo->sendPhpmailer();
            }catch(Exception $ex){
                $mensajefinal=array('code'=>'ok','msj'=>JrTexto::_('Error sending mail')." ".$ex->getMessage()."--".$ex->getLine());
                //if(!empty($mensajefinal)) $mensajefinal['msj'] .=", ". JrTexto::_('No hemos podido enviar el correo');    
                //else $mensajefinal=array('code'=>'Error','msj'=>ucfirst(JrTexto::_('No hemos podido enviar el correo')));                         
            }           
            if(!empty($mensajefinal)) echo json_encode($mensajefinal);
            else echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('Exam successfully saved')));
            exit();
        }catch(Exception $e){
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()."--".$e->getLine())));
            exit();
        }
    } 

    public function reporte(){
        try{
            global $aplicacion;
            $sesion = JrSession::getInstancia();
            $empresa=$this->proyecto = $sesion->getAll('_infoempresa_');
            if(empty($empresa)){
                $this->documento->plantilla = 'registro';            
                $_url=$_SERVER["HTTP_HOST"];
                $_url=str_replace("www.", '', $_url);
                $_haysubdomain=stripos($_url,".");
                $_urlsubdomain='abacolima';
                if($_haysubdomain==true){
                    $validar=substr($_url, 0,$_haysubdomain);
                    $_bussubdomain=stripos($validar,"/");
                    if($_bussubdomain===false){
                        $_urlsubdomain=substr($_url,0,$_haysubdomain);
                    }
                }
                $sesion = JrSession::getInstancia();
                //$this->proyecto = $sesion->getAll('_infoempresa_');
                //if(empty($this->proyecto)){
                JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
                $oNegProyecto = new NegProyecto;
                $hayproyecto=$oNegProyecto->buscar(array('sesionlogin'=>$_urlsubdomain));
                if(empty($hayproyecto[0])) $hayproyecto=$oNegProyecto->buscar(array('sesionlogin'=>'abacolima'));
                foreach ($hayproyecto[0] as $key => $value) {
                    NegSesion::setEmpresa($key,$value);
                }
                $empresa=$this->proyecto = $sesion->getAll('_infoempresa_');
            }
            $this->idexamen=!empty($this->proyecto["Examensinlogin"])?$this->proyecto["Examensinlogin"]:0;
            /*if(empty($this->idexamen)){
                $this->msj="El Examen indicado, no es correcto revise su  direccion URL porfavor";
                $this->esquema = 'error/general';
                 return parent::getEsquema();
            }*/

            JrCargador::clase('sys_negocio::NegSinlogin_quizproyecto', RUTA_BASE);
            $NegProyectoquiz=new NegSinlogin_quizproyecto;
            $this->examenes=$NegProyectoquiz->buscar(array('idproyecto'=>$this->proyecto["idproyecto"]));
            if(empty($this->examenes[0])){
                $this->msj="Esta empresa no tiene asignado el examen indicado";
                $this->esquema = 'error/general';
                 return parent::getEsquema();
            }
            $this->idexamenes=array();           
            foreach ($this->examenes as $k => $v){
                $this->idexamenes[]=$v["idquiz"];
            }

            $this->documento->plantilla = 'verblanco'; //'examenes/general';
            $this->esquema = 'sinlogin/reporte';
            $this->exam=$this->oNegExamenes->buscar(array('idexamen'=>$this->idexamenes));
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function eliminar(){
        try {
            if(empty($_REQUEST)){ 
                echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
                exit(0);
            }
            $this->oNegNotas_quiz->setCampo($_REQUEST['idnota'],'estado',-1);
           //$res=$this->oNegNotas_quiz->eliminar();          
            echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
            exit(0);
        }
    }
}