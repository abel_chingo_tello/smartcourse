 <?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017 
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegQuizexamenes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegProyecto_config', RUTA_BASE, 'sys_negocio');*/

class WebExamenes extends JrWeb
{
	protected $oNegExamenes;
    private $oNegCursoDetalle;
	/*protected $oNegpreguntas;
    protected $oNegMetodologia;
    protected $oNegExamen_alumno;
    protected $oNegAlumno;
    protected $oNegNegPersonal;
    protected $oNegProyecto_config;*/

	public function __construct()
	{
		if(false === NegSesion::existeSesion()){
            header('Location: '.URL_BASE);
            exit();
        }
        parent::__construct();   

		$this->oNegExamenes = new NegQuizexamenes;
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegPersonal = new NegPersonal;
        $this->oNegCursoDetalle = new NegAcad_cursodetalle;
		/*$this->oNegpreguntas = new NegExamenes_preguntas;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegExamen_alumno = new NegExamen_alumno;
        $this->oNegAlumno = new NegAlumno;        
        $this->oNegProyecto_config = new NegProyecto_config;*/
        $this->defecto();
	}
	public function defecto(){

	}
    public function librerias(){
        $this->documento->script('tinymce.min', '/libs/tinymce/');
        $this->documento->script('encode', '/libs/chingo/');
        //$this->documento->script('', URL_TEMA.'js/chingo/encode.js');
        $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
        $this->documento->script('jquery.maskedinput.min', '/tema/js/');
        $this->documento->script('', URL_TEMA.'js/chingo/cronometro.js');
        $this->documento->script('', URL_TEMA.'js/chingo/mitimer.js');
        $this->documento->script('jquery-ui.min', '/tema/js/');
        $this->documento->script('jquery.ui.touch-punch.min','/libs/jquery-ui-touch/');

        $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery-confirm.min', '/libs/alert/');
        $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery.md5', '/tema/js/');
        $this->documento->stylesheet('',URL_TEMA.'js/circletimer/circletimer.css');
        $this->documento->script('',URL_TEMA.'js/circletimer/circletimer.min.js');

        $this->documento->script('', URL_TEMA.'js/examen/multiplantilla.js?vs'._version_);
        $this->documento->script('', URL_TEMA.'js/examen/true_false.js');
        $this->documento->script('', URL_TEMA.'js/examen/order_simple.js');
        $this->documento->script('', URL_TEMA.'js/examen/order_paragraph.js');
        $this->documento->script('', URL_TEMA.'js/examen/join.js');
        $this->documento->script('', URL_TEMA.'js/examen/tag_image.js');
        $this->documento->script('', URL_TEMA.'js/examen/editactividad.js');
        $this->documento->script('', URL_TEMA.'js/examen/actividad_completar.js');
         //$this->documento->script('speach', URL_TEMA.'js/new/');
        $this->documento->script('', URL_TEMA.'js/funciones.js');
        $this->documento->script('', URL_TEMA.'js/graficos/progressbar/progressbar.min.js');
        $this->documento->script('', URL_TEMA.'js/graficos/progressbar/circle.js');
         
        $this->documento->script('', URL_TEMA.'js/speach/audioRecorder.js');
        $this->documento->script('', URL_TEMA.'js/audiorecord/wavesurfer.min.js');
        $this->documento->script('', URL_TEMA.'js/speach/callbackManager.js');
        $this->documento->stylesheet('',  URL_TEMA.'js/examen/speach.css');
        $this->documento->script('',  URL_TEMA.'js/examen/speach.js?vs'._version_);
        //$this->documento->script('nspeach',  URL_TEMA.'js/new/');
        $this->documento->script('', URL_TEMA.'js/examen/alu_preview.js');
        $this->documento->stylesheet('',  URL_TEMA.'js/examen/actividad_nlsw.css');
        $this->documento->script('', URL_TEMA.'js/examen/actividad_nlsw.js');
    }

    public function resolver() //antes teacherresrc_view()
    {
        try {
            global $aplicacion;
            $vistaMobile = JrTools::ismobile();
            $this->librerias();
            if($vistaMobile == true){
                $this->documento->stylesheet("",URL_BASE."quiz/plantillas/quiz/css/mobile/style.css");
            }
            $this->idexamen=!empty($_REQUEST["idexamen"])?$_REQUEST["idexamen"]:-1;
            $filtros["idexamen"]=$this->idexamen;
           // var_dump($filtros["idexamen"]);
            $this->nro_intento = 0;
            $this->cant_intentos = 5;
			$this->max_intento = 9999;
			$this->usuarioAct = NegSesion::getUsuario();
			$user=$this->usuarioAct;
            $this->idcurso = !empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:-1;
            $this->idcc = !empty($_REQUEST["idcc"])?$_REQUEST["idcc"]:0;
            $this->idcursodetalle = !empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:-1;
            $this->idpestania = !empty($_REQUEST["idpestania"])?$_REQUEST["idpestania"]:-1;
            $this->idgrupoauladetalle = !empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:0;           
            JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE);
            $oNegHistorial_sesion = new NegHistorial_sesion;
            $oNegHistorial_sesion->tipousuario = ($user['rol']=='Alumno')?'A':'P';
            $oNegHistorial_sesion->idusuario = $user['idpersona'];
            $oNegHistorial_sesion->lugar = 'E';
            $oNegHistorial_sesion->idcurso = $this->idcurso;
            $fechaentrada=date('Y-m-d H:i:s');
            $oNegHistorial_sesion->fechaentrada = $fechaentrada;
            $oNegHistorial_sesion->fechasalida = date('Y-m-d H:i:s',strtotime('+2 second',strtotime($fechaentrada)));
            $oNegHistorial_sesion->idcc = $this->idcc;
            if(!empty( $this->idgrupoauladetalle))
                $oNegHistorial_sesion->idgrupoauladetalle =$this->idgrupoauladetalle;
            $idHistSesion = $oNegHistorial_sesion->agregar();


            $formulacurso=$this->oNegCursoDetalle->geformula($this->idcurso, $this->idcc);
            $this->datosoffline=base64_encode(json_encode(array('_user_'=>($user["usuario"].$user["clave"].$user["idpersona"]),'idpersona'=>$user["idpersona"],'idproyecto'=>$user["idproyecto"],'idempresa'=>$user["idempresa"],'idrol'=>$user["idrol"],'idcurso'=>$this->idcurso,'idcc'=>$this->idcc,'idcursodeatlle'=>$this->idcursodetalle,'idpestania'=>$this->idpestania,'idHistSesion'=>$idHistSesion)));
			$this->tipo = (isset($_REQUEST['tip'])) ? $_REQUEST['tip'] : (!empty($_REQUEST["tipoexamen"])?$_REQUEST["tipoexamen"]:(!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:'E')); 
			$this->numexamen = !empty($_REQUEST["numexamen"])?$_REQUEST["numexamen"]:1;
			$this->callback = !empty($_REQUEST["callback"])?$_REQUEST["callback"]:'';
			$this->urlback = !empty($_REQUEST["urlback"])?$_REQUEST["urlback"]:'';
            $rol=$this->usuarioAct["rol"];
			$filtros['idproyecto'] = $this->usuarioAct['idproyecto'];
            $exam=$this->oNegExamenes->buscar(array('idexamen'=>$this->idexamen));
            $calificacion='M';
            $oderpor="notafinal";
            if(!empty($exam)){
                $this->examen=$exam[0];
                $calificacion=$this->examen["calificacion"];
                $idioma=!empty($_REQUEST["idioma"])?$_REQUEST["idioma"]:@$this->examen["idioma"];
                if($calificacion=="M") $oderpor="nota";
            }else{
                throw new Exception(JrTexto::_('Exam does not exist').'!');
            }
            if($this->usuarioAct['idrol']==1 || $this->usuarioAct['idrol']==3){
                $filtros["estado"]=1;
                $buscarexamen=array('idalumno'=>$user["idpersona"],'idrecurso'=>$this->idexamen,'idproyecto'=>$user["idproyecto"],'idcursodetalle'=>$this->idcursodetalle,'idcurso'=>$this->idcurso,'tipo'=>$this->tipo,'idcomplementario'=>$this->idcc,'order'=>$oderpor);
                if($this->usuarioAct['idrol']!=1) $buscarexamen["idgrupoauladetalle"]=$this->idgrupoauladetalle;
                $notaquiz=$this->oNegNotas_quiz->buscar($buscarexamen);
                if(!empty($notaquiz[0])){
                	$this->nro_intento  = count($notaquiz);
                    $this->resultado=$notaquiz[0];   
                    if($this->resultado["idexamenproyecto"]==-100){
                        $preguntas=json_decode($this->resultado["preguntas"],true);
                        $ipost=stripos($preguntas["archivo"],'static');
                        $archivo=RUTA_BASE.substr($preguntas["archivo"],$ipost);                       
                        if(file_exists($archivo)) {
                            @chmod($archivo, 0777);
                            require_once($archivo);  
                            $this->resultado["preguntas"]=$preguntas;
                        }else{
                            $this->resultado["preguntas"]=null;
                        }
                        
                    }           
                }       
                $this->nro_intento++;  
            }
            $this->documento->plantilla = $vistaMobile == true ? "verblanco_mobile" : 'verblanco'; //'examenes/general';
            $this->esquema = $vistaMobile == true ? "examenes_mobile/alu_preview" : 'examenes/alu_preview';  
			$this->habilidades=json_decode($this->examen['habilidades_todas'], true);
            if(!empty($formulacurso)){
                if($formulacurso["tipocalificacion"]!='A' && $this->examen['calificacion_por']='E'){ //no aplica formula para alfanumerico
                    $this->examen['calificacion_en']=$formulacurso["tipocalificacion"];
                    $this->examen['calificacion_min']=$formulacurso["mincal"];
                    $this->examen['calificacion_total']=$formulacurso["maxcal"];
                    if(!empty($this->resultado) && $this->resultado['calificacion_en']!= $this->examen['calificacion_en']){ // se necesita hacer una conversion de nota
                        $this->resultado['nota']=$this->resultado['nota']*$this->examen['calificacion_total']/$this->resultado["calificacion_total"];
                    }
                }elseif($formulacurso["tipocalificacion"]=='A'  && $this->examen['calificacion_por']='E'){
                    $this->examen['calificacion_en']='A';
                    $newcalificacion_total=array();
                    $maxformulacurso=0;
                    if(!empty($formulacurso['puntuacion']))
                        foreach ($formulacurso['puntuacion'] as $k => $v) {
                            $newcalificacion_total[]=array('max'=>$v["maxcal"],'min'=>$v["mincal"],'nombre'=>$v["nombre"]);
                            if($maxformulacurso<$v["maxcal"]) $maxformulacurso=$v["maxcal"];
                        }
                    else{
                        $maxformulacurso=$formulacurso["maxcal"];
                        $newcalificacion_total[]=array('max'=>$formulacurso["maxcal"],'min'=>$formulacurso["mincal"],'nombre'=>' -- ');
                    }
                    $newcalificacion_total=json_encode($newcalificacion_total,true);
                    $this->examen["calificacion_total"]=$newcalificacion_total;
                    if(!empty($this->resultado)){
                        $maxexamen=0;
                        if($this->resultado['calificacion_en']=='A'){
                            if(!empty($this->resultado['calificacion_total'])){
                                $exacalificacion_total=json_decode($this->resultado['calificacion_total'],true);
                                foreach ($exacalificacion_total as $key => $value) {
                                    if($maxexamen<$value["max"]) $maxexamen= $value["max"];
                                }
                            }
                        }else{
                            $maxexamen=$this->resultado['calificacion_total'];
                        }
                        $this->resultado['nota']=$this->resultado['nota']*$maxformulacurso/$maxexamen;
                        $this->maxformulacurso=$maxformulacurso;
                    }                    
                }
            }

            if($this->nro_intento<=(int)$this->examen['nintento']){
               	/**
            	* si el numero de intentos del ultimo registro  es <= a la cant de intentos, podrá resolver su examen
            	**/                
                $filtros_preg=array();
	            $filtros_preg["idexamen"]=$this->idexamen;
	            $filtros_preg["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
                $this->preguntas=$this->oNegExamenes->mostrarPreguntas($filtros_preg);
	            if(empty($this->preguntas)){
	                throw new Exception(JrTexto::_("There are no question in this exam").".");
	            }
            }else{
            	/**
            	* cargar plantilla de resultado del examen
            	**/	
            	//$this->resultado = $notaquiz;//$this->examen, $notaquiz);
                $this->documento->plantilla = $vistaMobile == true ? "verblanco_mobile" : 'verblanco';
                $this->esquema = $vistaMobile == true ? "examenes_mobile/solo_resultado" : 'examenes/solo_resultado';
            }
            $this->guardar=true;
          //  var_dump($this->documento->plantilla, $this->esquema);
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function ver(){
        try{   
            global $aplicacion;
            $this->librerias();
            $this->idexamen=!empty($_REQUEST["idexamen"])?$_REQUEST["idexamen"]:-1;
            $filtros["idexamen"]=$this->idexamen;
            $exam=$this->oNegExamenes->buscar(array('idexamen'=>$this->idexamen));
            $calificacion='M';
            $oderpor="notafinal";
            if(!empty($exam)){
                $this->examen=$exam[0];
                $calificacion=$this->examen["calificacion"];
                $idioma=!empty($_REQUEST["idioma"])?$_REQUEST["idioma"]:@$this->examen["idioma"];
                if($calificacion=="M") $oderpor="nota";
            }else{
                throw new Exception(JrTexto::_('Exam does not exist').'!');
            }
            $filtros_preg=array();
            $filtros_preg["idexamen"]=$this->idexamen;
            $filtros_preg["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
            $this->preguntas=$this->oNegExamenes->mostrarPreguntas($filtros_preg);
            if(empty($this->preguntas)){
                throw new Exception(JrTexto::_("There are no question in this exam").".");
            }
            $this->documento->plantilla = 'verblanco';
            $this->esquema = 'examenes/alu_preview';
            $this->guardar=false;
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function editar(){
        try{
            global $aplicacion;
            if(NegSesion::existeSesion()==false){
                return $aplicacion->redir();
            }
            $usuarioAct = NegSesion::getUsuario();
            $this->userquiz=$this->oNegExamenes->persona();
            $user=NegSesion::getUsuario();
            $this->idrol=$user["idrol"];
            $this->tuser=$user["tuser"];
            $this->idusuario=$user["idpersona"];
            $this->usuario=$user;
            $this->idproyecto=$user["idproyecto"];
            $this->quizidpersona=$this->userquiz["quiz_idpersona"];
            $this->quizidproyecto=$this->userquiz["quiz_idproyecto"];

            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->accion = 'N';

            $this->idexamen=!empty($_REQUEST["idexamen"])?$_REQUEST["idexamen"]:-1;            
            $examen=$this->oNegExamenes->buscar(array('idexamen'=>$this->idexamen));
            $this->idpersona_smartcourse=$user["idpersona"];

            /*$usuarioAct = NegSesion::getUsuario();
            $rol=$usuarioAct["rol"];  */         
           
            if(!empty($examen[0])){
                $this->examen = $examen[0];                
                $this->accion = 'Editar';
            }
            $filtros = array();
            $filtros["allcategorias"] = true;
            $cursos=array('miscursos'=>array());
            if ($this->idrol == 1) {                
                JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE);
                $oNegAcad_curso = new NegAcad_curso;
                $cursos = $oNegAcad_curso->cursos($filtros);
                $cursos2=array();
                if(!empty($cursos["miscursos"]))
                foreach ($cursos["miscursos"] as $key => $value) {
                    $dt=array('idcurso'=>$value["idcurso"],'nombre'=>$value["nombre"]);
                    if($this->solocursosprincipales=='2') {
                        if(empty($value["idcomplementario"])) $cursos2[]=$dt;
                    }                    
                    else $cursos2[]=$dt;
                }
                $cursos["miscursos"]=$cursos2;
            }else if($this->idrol==2){
                JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE);
                $oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
                $filtros["iddocente"] = $usuarioAct["idpersona"];
                $cursos = $oNegAcad_grupoauladetalle->cursos($filtros);
                $cursos2=array();
                foreach ($cursos["miscursos"] as $key => $value) {
                    $dt=array('idcurso'=>$value["idcurso"],'nombre'=>$value["strcurso"]);
                    $cursos2[]=$dt;
                }
                $cursos["miscursos"]=$cursos2;
            }
            $this->miscursos=$cursos["miscursos"];
            //if(!empty($this->idcurso)){ $this->examen["origen_habilidades"] = 'JSON'; }
            /* buscar si hay Config de UrlSkills */
            //$this->config_proyecto = $this->oNegProyecto_config->buscar($filtros);

            $this->cant_intentos = 10; //se listará en el <select id="nintento">
            $this->max_intento = 9999; //se listará en el <select id="nintento"> al final
            $this->documento->setTitulo(JrTexto::_('Exams').' /'.JrTexto::_('Edit'), true);
             $this->esquema = 'examenes/setting';           
            $this->documento->plantilla = 'inicio';
            return parent::getEsquema();
        }catch(Exception $ex){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
}