<?php defined('_BOOT_') or die(''); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo $documento->getUrlStatic()?>/libs/bootstrap3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_inicio.css" rel="stylesheet">
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/sweetalert/sweetalert2.all.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/pnotify.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/inicio.js"></script>
    <script> 
        var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
        var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
        var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';
        var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
    </script>
    <jrdoc:incluir tipo="cabecera" />
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
</head>
<body >
<div class="">
    <jrdoc:incluir tipo="recurso" />    
</div>
<jrdoc:incluir tipo="modulo" nombre="footer" posicion="man"/></jrdoc:incluir>
<jrdoc:incluir tipo="docsJs" />
<div class="modal fade" id="modalclone" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="modaltitle"></h4>
      </div>
      <div class="modal-body" id="modalcontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $documento->getUrlStatic() ?>/img/sistema/loading.gif"><br><span style="font-size: 1.2em; font-weight: bolder; color: #006E84;">Getting ready...</span></div>
        </div>
      <div class="modal-footer" id="modalfooter">        
        <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>
  </div>
</div>
</body>
</html>