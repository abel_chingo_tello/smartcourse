/**
* Ordenar Parrafo plugin
**/
var ANS_P= {};
var AYUDA_P = {};

var initOrdenarParrafos = function(IDGUI, LANG, ayuda){
    LANG=LANG||'';
    ayuda=ayuda||true;
    ANS_P[IDGUI] = {};
    if(AYUDA_P[IDGUI]==undefined && ayuda!=undefined) { AYUDA_P[IDGUI] = ayuda; }
    /*var now = Date.now();
    $('#parrafo_0').attr('id', 'parrafo_'+now);
    $('#txt_parrafo_0').attr('id', 'txt_parrafo_'+now);
    if( $('#lista-parrafo-edit'+IDGUI).is(':visible') ){
        $contenedorEdit = $('#tmp_'+IDGUI+' #lista-parrafo-edit'+IDGUI);
        if( $contenedorEdit.find('textarea').length>0 ) {
            iniciarTodosEditores($contenedorEdit);
        }
    }*/
    if( !$('#lista-parrafo-edit'+IDGUI).is(':visible') || $('#lista-parrafo-edit'+IDGUI).length==0 ){
        $('#ejerc-ordenar'+IDGUI+' .drag-parr>div').each(function() {
            var idParr = $(this).attr('id');
            var orden = $(this).attr('data-orden');
            ANS_P[IDGUI][idParr] = orden;
            if( !$('.mask.elegirtpl').is(':visible') || $('.mask.elegirtpl').length==0 ) $(this).removeAttr('data-orden');
        });
    }
};

var evaluarUno = function($parrafo, IDGUI){
    var idParafo = $parrafo.attr('id');
    var $ejercOrdenar = $parrafo.closest('.ejerc-ordenar');
    var index = $ejercOrdenar.find('.drop-parr>div').index($parrafo);
    if( $parrafo.hasClass('filled') ){
        if( ANS_P[IDGUI][idParafo] == (index+1) ){
            $parrafo.attr('data-corregido', 'good');
            /*if( esDBYself($parrafo) ) restarPuntaje();*/
        } else {
            $parrafo.attr('data-corregido', 'bad');
        }
    } else {
        $parrafo.removeAttr('data-corregido');
    }
};

var evaluarFinal = function($parrafo, IDGUI){
    var $ejercOrdenar = $parrafo.closest('.ejerc-ordenar');
    $ejercOrdenar.find('.drop-parr>div').each(function() {
        evaluarUno( $(this), IDGUI );
        $(this).addClass('finish');
    });
};

var addDataCorregido = function($parrafo, cantVacios){
    var cantLlenados = $parrafo.closest('.drop-parr').find('.filled').length;
    var cantCorregidoGood = $parrafo.closest('.drop-parr').find('*[data-corregido="good"]').length;
    if(cantVacios==0){
        if(cantLlenados==cantCorregidoGood){
            var valor = 'good';
        }else{
            var valor = 'bad';
        }
        $parrafo.closest('.ejerc-ordenar').attr('data-corregido', valor);
    } else {
        $parrafo.closest('.ejerc-ordenar').removeAttr('data-corregido');
    }
}

var evaluarOrdenParrafo = function( $parrafo, IDGUI ){
    var cantVacios = $parrafo.closest('.ejerc-ordenar').find('.drop-parr>div:empty').length;
    var ayuda = AYUDA_P[IDGUI];
    if(!ayuda && cantVacios==0){
        evaluarFinal($parrafo, IDGUI);
    } 
    if(ayuda){
        evaluarUno($parrafo, IDGUI);
    }

    addDataCorregido($parrafo, cantVacios);
};

(function($){
    $.fn.examOrderParagraph = function(opciones){
        var opts = $.extend({}, $.fn.examOrderParagraph.defaults, opciones);
        return this.each(function() {
            var that = $(this);
            var _idgui = that.find('#idgui').val();
            var _isEditando = that.hasClass('editando');

            that.on('click', '.ejerc-ordenar .drag-parr>div', function(e) {
                e.preventDefault();
                var $this= $(this);
                var value= $this.html();
                var id = $this.attr('id');
                var $ejercOrdenar = $this.closest('.ejerc-ordenar');
                var $primerVacio = $ejercOrdenar.find('.drop-parr div:empty').first();
                $primerVacio.html(value);
                $primerVacio.addClass('filled').attr('id', id);
                $this.remove();
                evaluarOrdenParrafo( $primerVacio, _idgui);
            }).on('click', '.ejerc-ordenar .drop-parr>div.filled', function(e) {
                e.preventDefault();
                var $this= $(this);
                //if(!$this.hasClass('finish')){
                    var value= $this.html();
                    var id = $this.attr('id');
                    var $ejercOrdenar = $this.closest('.ejerc-ordenar');
                    $ejercOrdenar.find('.drag-parr').append('<div id="'+id+'">'+value+'</div>');
                    $this.html('').removeClass('filled').removeAttr('id');
                    evaluarOrdenParrafo( $this, _idgui);
                //}
            });

            initOrdenarParrafos(_idgui);
        });
    };

    $.fn.examOrderParagraph.defaults = {};
}(jQuery));