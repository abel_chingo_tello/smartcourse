'use strict';

var dataResultado = null;
var obtenerResultado = function(dataSend) {
	$.ajax({
		async: false,
		url: _sysUrlBase_ +'/examen_alumno/getResultadoXAlumno',
		type: 'GET',
		dataType: 'json',
		data: dataSend,
	}).done(function(resp) {
		if(resp.code=='ok') {
			var data = resp.data;
			dataResultado = {
				"idexamen" : dataSend.idexamen,
				"idalumno" : data.alumno.identificador,
				"nota" : data.resultado.puntaje,
				"fecha" : data.resultado.fecha,
			};
			$.each(data.otros, function(key, val) {
				dataResultado[key] = val;
			});
		} else {
			console.log('Error : ', resp.msj);
		}
	}).fail(function(err, txtErr, other) {
		console.log("error : ", txtErr);
	}).always(function() { });
};

var sendToCallback = function(){
	$.ajax({
		url: CALLBACK,
		type: 'POST',
		dataType: 'json',
		data: dataResultado,
	}).done(function(resp) {
		console.log(resp);
	}).fail(function(err, txtErr, other) {
		console.log("error : ", txtErr);
	}).always(function() {});
};

var ejecutarCallback = function(idExamen) {
	idExamen = idExamen || 0;
	if(!CALLBACK.length || idExamen===0) { return false; }
	obtenerResultado({"idexamen" : idExamen});

	sendToCallback();
};