<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
require_once(dirname(dirname(__FILE__)).'/config.php');
require_once(dirname(__FILE__).SD.'ini_app.php');
$aplicacion->iniciar();

if(!empty($_REQUEST["idioma"])){	
	$documento = &JrInstancia::getDocumento();	
	$idioma=$_REQUEST["idioma"];
	$documento->setIdioma($idioma);
}
$aplicacion->enrutar(JrPeticion::getPeticion(0), JrPeticion::getPeticion(1));
echo $aplicacion->presentar();
function mostar__($rec, $ax){
	global $aplicacion;	
	$aplicacion->iniciar();
	$aplicacion->enrutar($rec, $ax);
	echo $aplicacion->presentar();
}