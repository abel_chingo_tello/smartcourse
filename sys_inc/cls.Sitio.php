<?php
/**
 * @autor		Abel Chingo Tello : ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016 Todos los derechos reservados.
 */

defined('RUTA_RAIZ') or die();
class Sitio extends JrAplicacion{
	public static $msjs;
	private $BD = null;
	public function __construct()
	{		
		parent::__construct();
	}
	
	public function iniciar($compat = array())
	{	
		
		if(true === $this->inicio){
			return;
		}	
		JrCargador::clase('jrAdwen::JrModulo');
		JrCargador::clase('sys_inc::ConfigSitio', RUTA_RAIZ);
		JrCargador::clase('sys_datos::DatBase', RUTA_RAIZ);
		JrCargador::clase('sys_negocio::NegSesion', RUTA_RAIZ);
		JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_RAIZ);
		$oConfigSitio = ConfigSitio::getInstancia();
		parent::iniciar($oConfigSitio->get_());
	}	
	public function enrutar($rec = null, $ax = null)
 	{
		$usuarioAct = null;
		if(NegSesion::existeSesion()){
			$usuarioAct = NegSesion::getUsuario();
		}

		$forzarlogin=false;
		$permitenologueado=array('sesion','persona','generador','api','correos','bolsa_empresas','notas_quiz','registro','examensinlogin');
		$permitemetodo=array('subirblob','login');
		if(!empty($_REQUEST["empresalogin"])){
			@session_start();
			if(@$_SESSION["empresalogin"]!=$_REQUEST["empresalogin"]){
				$forzarlogin=true;
				NegSesion::salir();
			}

		}
		

		if((IS_LOGIN===true&&!NegSesion::existeSesion()&&!in_array($rec,$permitenologueado)&&!in_array($ax, $permitemetodo))||$forzarlogin==true){
			JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE);
			$oNegProyecto = new NegProyecto;
			$_url=$_SERVER["HTTP_HOST"];
			$_url=str_replace("www.", '', $_url);
			$_pointsubdomain=stripos($_url,".");
			$hayproyecto=array();
			$subdominio='';
			if($_pointsubdomain===false){
				if($forzarlogin){
					$_pointsubdomain=!empty($_REQUEST["empresalogin"])?$_REQUEST["empresalogin"]:'smartknowledgeusa';
				}else
				$_pointsubdomain=!empty($_REQUEST["empresa"])?$_REQUEST["empresa"]:'smartknowledgeusa';			
				$hayproyecto=$oNegProyecto->buscar(array('sesionlogin'=>$_pointsubdomain)); // empresa por defecto , si noha subdominio en url			
				$subdominio=$_pointsubdomain;	
			}else{
				$validar=substr($_url, 0,$_pointsubdomain);
				$_bussubdomain=stripos($validar,"/");

				if($_bussubdomain===false){
					$subdominio=substr($_url,0,$_pointsubdomain);
					$hayproyecto=$oNegProyecto->buscar(array('sesionlogin'=>$subdominio));
					//var_dump($hayproyecto);
				}
			}
			if(empty($hayproyecto) && !empty($rec)){
				$hayproyecto=$oNegProyecto->buscar(array('sesionlogin'=>$rec));
				$subdominio=$rec;									
			}
			//var_dump($subdominio);
			//	exit();
			if(!empty($hayproyecto[0])){				
				setcookie("subdominio",$subdominio, time()+60*60*24*180,URL_BASE);
				$_COOKIE['subdominio']=$subdominio;
				setcookie("SC_URL_idproyecto_","_".$subdominio."_", time()+60*60*24*180,URL_BASE);
				$_COOKIE['SC_URL_idproyecto_']="_".$subdominio."_";
				setcookie("SC_idproyecto",$hayproyecto[0]["idproyecto"], time()+60*60*24*180,URL_BASE);
				$_COOKIE['SC_idproyecto']=$hayproyecto[0]["idproyecto"];
				$idioma=strtoupper(!empty($hayproyecto[0]["idioma"])?$hayproyecto[0]["idioma"]:NegConfiguracion::get_('idioma_defecto'));
				$nombre=!empty($hayproyecto[0]["nombre"])?$hayproyecto[0]["nombre"]:'Campus virtual';
				$idioma=!empty($_REQUEST["idioma"])?$_REQUEST["idioma"]:$idioma;	
							
				$documento =&JrInstancia::getDocumento();
				$documento->setIdioma($idioma);
				NegSesion::set('idioma',$idioma,'idioma__');
				NegSesion::set('nombre',$nombre);
				$documento->setTitulo($nombre, false);
				$this->idioma=$idioma;		
			}
			if(!empty($_REQUEST["idioma"])){
				$idioma=$_REQUEST["idioma"];
				$documento =&JrInstancia::getDocumento();
				$documento->setIdioma($idioma);
				NegSesion::set('idioma',$idioma,'idioma__');
				$this->idioma=$idioma;
			}			
			parent::enrutar('sesion');	
		}else{					
			$documento =&JrInstancia::getDocumento();
			$documento->setTitulo(JrTexto::_('Session start'), true);
			if(!empty($_REQUEST["idioma"])){
				$idioma=$_REQUEST["idioma"];
				$documento =&JrInstancia::getDocumento();
				$documento->setIdioma($idioma);
				NegSesion::set('idioma',$idioma,'idioma__');
				$this->idioma=$idioma;
			}
			if(!empty($_COOKIE['SC_URL_idproyecto_'])){
				if($_COOKIE['SC_URL_idproyecto_']=='_'.$rec."_") parent::enrutar($ax,null);
				else parent::enrutar($rec, $ax);
			}else
			parent::enrutar($rec, $ax);
		}
	}
	public static function &getInstancia()
	{
		if(empty(self::$instancia)) {
			self::$instancia = new self;
		}
		return self::$instancia;
	}

	public function error($msj, $plantilla = null)
	{
		echo "error sitio";
		JrCargador::clase('sys_web::WebExcepcion', RUTA_SITIO, 'sys_web');
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->error($msj, $plantilla);
	}
	public function noencontrado()
	{

		JrCargador::clase('sys_web::WebExcepcion', RUTA_SITIO, 'sys_web');
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->noencontrado();
	}
}