<?php
/**
 * @autor		Chingo Tello Abel, basado en Joomla
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();

class JrAplicacion extends JrObjeto
{
	protected static $instancia = null;
	protected $horaSol;
	protected $colaMsjs = array();
	
	//valores de la aplicacion por defecto
	public $rec = 'Defecto';
	protected $ax = 'defecto';
	
	public static $url_base = null;
	
	protected static $htaccess = false;
	public static $idioma = '';
	
	/**
	 * Plantilla, solo el nombre, por defecto es defecto
	 * @cadena
	 */
	protected $plantilla = 'defecto';
	
	protected $inicio = false;
	
	public function __construct()
	{
		$this->horaSol = gmdate('Y-m-d H:i');
	}
	
	public static function &getInstancia()
	{
		if(!is_object(self::$instancia)) {
			self::$instancia = new self;
		}
		
		return self::$instancia;
	}
	
	public function iniciar($config)
	{
		if(true === $this->inicio) {
			return;
		}
		
		$documento =&JrInstancia::getDocumento();
		if(array_key_exists('tema', $config))$documento->tema = $config['tema'];//temas	
		if(!empty($config['plantilla']))$documento->plantilla = $config['plantilla'];//plantilla defecto		
		if(array_key_exists('nombre_sitio', $config))$documento->setTitulo($config['nombre_sitio']);//nombre del sitio por defecto	
		if(array_key_exists('idioma', $config))$documento->idioma = $config['idioma'];//idioma por defecto	
		if(array_key_exists('url_base', $config))self::$url_base = $config['url_base'];	//url primario		
		if(array_key_exists('url_corta', $config))self::$htaccess = $config['url_corta'];//url corta		
		JrInstancia::getURI();		
		$this->inicio = true;
	}
	
	protected function enrutar($rec = null, $ax = null)
 	{
		try {
			
			$documento =&JrInstancia::getDocumento();			
			$rec = empty($rec) ? JrPeticion::getVar('rec') : $rec;
			$param_1 = $rec;
			$rec = str_replace(' ', '', ucwords(str_replace('-', ' ', $rec)));
			
			$ax = empty($ax) ? JrPeticion::getVar('ax') : $ax;
			$ax = str_replace('-', '_', $ax);
			if(empty($rec)) $rec = 'Defecto';			
			if(empty($ax))$ax = 'defecto';			
			$this->rec = $rec;
			$clase = 'Web'.ucfirst($rec);
			$dir = 'sys_web::' . $clase;			
			try {
				JrCargador::clase($dir, RUTA_SITIO, 'sys_web::', 'cls.', false);
			} catch(Exception $e) {
				JrPeticion::setVar('permalink', strtolower($param_1));				
				$ax = 'ver';
				$clase = 'WebDefecto';				
				try {
					$dir = 'sys_web::' . $clase;
					JrCargador::clase($dir, RUTA_SITIO, 'sys_web::', 'cls.', false);
				} catch(Exception $e) {
					throw new Exception($this->noencontrado());
				}
			}
			
			if(is_callable(array($clase, $ax)) == false) {
				throw new Exception($this->noencontrado());
			}
			
			$oWeb = new $clase();
			$documento->setBuffer($oWeb->$ax(), 'recurso');
		} catch(Exception $e) {
			JrCargador::clase('jrAdwen::JrWebExcepcion');
			$documento->setBuffer($e->getMessage(), 'recurso');
		}
 	}
	
	public static function instanciarClase($clase, $metodo)
	{
		$clase = ucfirst($clase);
		$metodo = strtolower($metodo);
		
		if(is_callable(array($clase, $metodo)) == false) {
			JrCargador::clase('jrAdwen::JrExcepcion');
			new JrExcepcion(utf8_encode(JrTexto::_('Resource and action not accessible')));
		}
		
		if(class_exists($clase)) {
			return new $clase;
			//return $oClase->$metodo();
		} else {
			JrCargador::clase('jrAdwen::JrExcepcion');
			new JrExcepcion(utf8_encode(JrTexto::_('The resource does not exist')));
		}
	}
	
	public function despachar()
	{
	}
	
	public function presentar()
	{			
		$params = array('ruta' => RUTA_PLANTILLAS);
		$documento =& JrInstancia::getDocumento();
		return $documento->getContenido($params);
	}
	
	/**
	 * Devuelve la plantilla
	 * @return cadena
	 */
	public function getPlantilla()
	{
		return $this->plantilla;
	}
	
	/**
	 * Asigna la plantilla
	 * @cadena
	 */
	public function setPlantilla($tema)
	{
		if (is_dir(RUTA_PLANTILLAS . $tema)) {
			$this->plantilla = $tema;
		}
	}
	
	public function redir($url = '', $root = true,$params = array())
	{
		if(true === $root) {
			$url = JrURI::root() . $url;
		}
		
		if(!preg_match('#^http#i', $url)) {
			$uri =& JrURI::getInstancia();
			
			$prefix = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			
			if($url[0] == '/') {
				$url = $prefix . $url;
			} else {
				$parts = explode('/', $uri->toString(array('path')));
				array_pop($parts);
				$path = implode('/',$parts);
				$url = $prefix . $path . $url;
			}
		}
		if(!empty($params)){
			$cond = array();
			foreach ($params as $key => $value) {
				$cond[] = "{$key}={$value}";
			}
			if(!empty($cond)){
				$url = $url."?".implode('&', $cond);
			}
		}
		
		if (headers_sent()) {
			echo "<script>document.location.href='$url';</script>\n";
		} else {
			//@ob_end_clean(); // limpiar
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $url);
		}
		
		$this->salir();
	}
	
	public function encolarMsj($msj, $sesion = false, $tipo = 'info', $titulo = '')
	{
		if(true === $sesion) {
			JrCargador::clase('jrAdwen::JrSession');
			$sesion = JrSession::getInstancia();
			$sesion->set('msj', array('tipo' => $tipo, 'msj' => $msj, 'tit' => $titulo), 'MSJS');
		}
		
		$this->colaMsjs[] = array('tipo' => $tipo, 'msj' => $msj, 'tit' => $titulo);
	}
	
	public static function existeSesion()
	{
		return null;
	}
	
	public function getColaMsjs()
	{
		if(!count($this->colaMsjs)) {
			JrCargador::clase('jrAdwen::JrSession');
			$sesion = JrSession::getInstancia();
			$msjs = $sesion->get('msj', null, 'MSJS');
			$sesion->limpiar('msj','MSJS' );			
			$this->colaMsjs[] = $msjs;
		}
		
		return $this->colaMsjs;
	}
	
	public function salir($codigo = 0)
	{
		exit($codigo);
	}
	
	public static function getUrlBase()
	{

		return self::$url_base;
	}
	
	public static function pasarParamURL($params = array(), $archivo = '', $url = null)
	{
		$configs = JrConfiguracion::get_();
		$url = empty($url) ? JrURI::base() : $url;
		
		$ult_par = @$params[2];
		unset($params[2]);
		
		if(!empty($archivo)) {
			$url .= $archivo . '.php';
			
			$params[0] = empty($params[0]) ? '' : 'rec='.$params[0];
			$params[1] = empty($params[1]) ? '' : 'ax='.$params[1];
			$params = array_filter($params);
			
			$params_ = implode('&', $params);
			if(!empty($params_) || !empty($ult_par)) {
				$url .= '?';
				$url .= $params_;
				$url .= !empty($params_) ? '&' : '';
				$url .= $ult_par;
			}
		} else {
			$params = array_filter($params);
			$sep = (true == self::$htaccess) ? '/' : '&';
			$params_ = implode($sep, $params);
			$url .= $params_;
			echo $params_;
			if(!empty($ult_par)) {
				$url .= !empty($params_) ? '/?' : '?';
				$url .= $ult_par;
			}
		}
		
		return $url;
	}
	
	public static function getJrUrl($params = array(), $archivo = '', $url = null)
	{
		//$qhu_ = empty($params[0]) ? '' : $params[0];
		$rec_ = empty($params[0]) ? '' : $params[0];
		$ax_ = empty($params[1]) ? '' : $params[1];
		$otro_param = empty($params[2]) ? '' : $params[2];
		
		$configs = JrConfiguracion::get_();
		$url = empty($url) ? JrURI::base() : $url;
		
		$param = array();		
		if(false == self::$htaccess || !empty($archivo)){
			if(!empty($rec_))
				 $param[] = 'rec=' . $rec_;
			
			if(!empty($ax_))
				 $param[] = 'ax=' . $ax_;
			
			$url .= $archivo;
			$url .= !empty($param) ? '?' . implode('&', $param) : '';
			
			if(!empty($otro_param))
				$url .= !empty($param) ? '&' . $otro_param : '?' . $otro_param;
		} else {
			if(!empty($rec_))
				 $param[] = $rec_;
			
			if(!empty($ax_))
				 $param[] = $ax_;
			
			$url .= !empty($param) ? implode('/', $param) : '';
			$url .= '/';
			
			if(!empty($otro_param))
				$url .= '?' . $otro_param;
		}
		
		return $url;
	}
	
	public static function getSDURL($subdominio)
	{
		$configs = JrConfiguracion::get_();
		return 'http://'.$subdominio.'.'.$configs['url_base'];
	}
	
	public function noencontrado()
	{
		exit(utf8_encode('P�gina no encontrada!!'));
	}

	public function setIdioma($idioma)
	{
		self::$idioma = strtolower($idioma);
	}

	public function typereturn(){
		@session_start();			
		if(!empty($_SESSION["typereturn"])){
			echo @$_SESSION["respuestajson"];
			/*try{
				unset($_SESSION['respuestajson']);
				unset($_SESSION['typereturn']);
			}catch(Exception $e){}*/
			exit(0);
		}
		else return;
	}
}