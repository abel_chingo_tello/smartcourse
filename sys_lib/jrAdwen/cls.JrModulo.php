<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class JrModulo
{
	protected $idi = 'ES';
	protected $modulo;
	protected $esquema;
	
	public function __construct()
	{
		$this->documento = JrInstancia::getDocumento();
	}
	
	public function getEsquema()
	{
		
		$ruta = RUTA_TEMA.SD.'modulos' . SD;
		$ruta .= $this->modulo . SD . 'html' . SD . $this->esquema . '.php';
		if(is_file($ruta)) {
			ob_start();
			require($ruta);
			$datos = ob_get_contents();
			ob_end_clean();
			
			return $datos;
		}
	}
	
	public function pasarHtml($cadena)
	{
		$cadena = htmlspecialchars($cadena);
		$cadena = utf8_encode($cadena);
		
		return $cadena;
	}
	
	/*public function pasarHtml($text)
	{//ENT_COMPAT, UTF-8
		return htmlentities(self::utf8($text), ENT_QUOTES, "UTF-8");
	}*/
	
	public function __get($propiedad)
	{
		if(property_exists($this, $propiedad)) {
			return $this->$propiedad;
		}
	}
	
	public function __toString()
	{
		return get_class($this);
	}
}