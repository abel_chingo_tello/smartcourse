<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();

global $jrPeticion;
$jrPeticion = array();

class JrPeticion
{
	protected static $instancia;
	protected static $peticion;
	
	public static function procesar()
	{
		if(true === self::$instancia) {
			return;
		}
		
		$uri = JrURI::getInstancia();
		
		$url__ = $uri->_scheme . '://' . $uri->_host . '/';
		$url__ = str_replace($url__, '', JrURI::base());
		$url__ = str_replace('/'.$url__, '/', $uri->_path);
	//	var_dump($url__);
		$peticion_ = explode('/', $url__);
		$peticion = array();
		for($i = 0; $i < count($peticion_); ++$i) {
			if(!empty($peticion_[$i])) {
				$peticion[] = $peticion_[$i];
			}
		}
		
		self::$peticion = $peticion;
		
		$vars = $uri->_vars;
		foreach($vars as $id => $valor) {
			self::setVar($id, $valor);
		}
		
		self::$instancia = true;
	}
	
	public static function getPeticion($param, $extraer = false)
	{
		if(true === self::$instancia) {
			if(isset(self::$peticion[$param])) {
				$val = self::$peticion[$param];
				
				if($extraer) {
					unset(self::$peticion[$param]);
					self::$peticion = array_values(self::$peticion);
				}
				
				return $val;
			}
			
			return null;
		}
		
		JrPeticion::procesar();
		
		if(isset(self::$peticion[$param])) {
			$val = self::$peticion[$param];
			
			if($extraer) {
				unset(self::$peticion[$param]);
				array_values(self::$peticion);
			}
			
			return $val;
		}
		
		return null;
	}
	
	public static function getAll()
	{
		if(true === self::$instancia) {
			return @self::$peticion;
		}
		
		JrPeticion::procesar();
		return @self::$peticion;
	}
	
	public static function getMetodo()
	{
		$metodo = strtoupper($_SERVER['REQUEST_METHOD']);
		return $metodo;
	}
	
	public static function getVar($nombre, $defecto = null, $hash = 'defecto', $tipo = 'none', $mask = 0)
	{
		global $jrPeticion;
		
		$hash = strtoupper($hash);
		if($hash === 'METHOD') {
			$hash = strtoupper($_SERVER['REQUEST_METHOD']);
		}
		$tipo	= strtoupper($tipo);
		$sig	= $hash.$tipo.$mask;
		
		switch ($hash) {
			case 'GET' :
				$input = &$_GET;
				break;
			case 'POST' :
				$input = &$_POST;
				break;
			case 'FILES' :
				$input = &$_FILES;
				break;
			case 'COOKIE' :
				$input = &$_COOKIE;
				break;
			case 'ENV'    :
				$input = &$_ENV;
				break;
			case 'SERVER'    :
				$input = &$_SERVER;
				break;
			default:
				$input = &$_REQUEST;
				$hash = 'REQUEST';
				break;
		}
		
		if (isset($jrPeticion[$nombre]['SET.'.$hash]) && ($jrPeticion[$nombre]['SET.'.$hash] === true)) {
			$var = (isset($input[$nombre]) && $input[$nombre] !== null) ? $input[$nombre] : $defecto;
			//$var = JrPeticion::limpiarValor($var, $mask, $tipo);
		} elseif (!isset($jrPeticion[$nombre][$sig])) {
			if (isset($input[$nombre]) && $input[$nombre] !== null) {
				$var = $input[$nombre];
				
				if (get_magic_quotes_gpc() && ($var != $defecto) && ($hash != 'FILES')) {
					$var = JrPeticion::quitarSlashes($var);
				}
				
				$jrPeticion[$nombre][$sig] = $var;
			} elseif($defecto !== null) {
				$var = JrPeticion::limpiarValor($defecto, $mask, $tipo);
			} else {
				$var = $defecto;
			}
		} else {
			$var = $jrPeticion[$nombre][$sig];
		}
		
		return $var;
	}
	
	public static function getInt($nombre, $defecto = 0, $hash = 'defecto')
	{
		return JrPeticion::getVar($nombre, $defecto, $hash, 'int');
	}
	
	public static function getFloat($nombre, $defecto = 0.0, $hash = 'defecto')
	{
		return JrPeticion::getVar($nombre, $defecto, $hash, 'float');
	}
	
	public static function getBool($nombre, $defecto = false, $hash = 'defecto')
	{
		return JrPeticion::getVar($nombre, $defecto, $hash, 'bool');
	}
	
	public static function getPalabra($nombre, $defecto = '', $hash = 'defecto')
	{
		return JrPeticion::getVar($nombre, $defecto, $hash, 'word');
	}
	
	public static function getCmd($nombre, $defecto = '', $hash = 'defecto')
	{
		return JrPeticion::getVar($nombre, $defecto, $hash, 'cmd');
	}
	
	public static function getCadena($nombre, $defecto = '', $hash = 'defecto', $mask = 0)
	{
		return (string) JrPeticion::getVar($nombre, $defecto, $hash, 'string', $mask);
	}
	
	public static function setVar($nombre, $valor = null, $hash = 'method', $sobreEscribir = true)
	{
		if(!$sobreEscribir && array_key_exists($nombre, $_REQUEST)) {
			return $_REQUEST[$nombre];
		}
		
		global $jrPeticion;
		$jrPeticion[$nombre] = array();
		
		$hash = strtoupper($hash);
		if($hash === 'METHOD') {
			$hash = strtoupper($_SERVER['REQUEST_METHOD']);
		}
		
		$anterior	= array_key_exists($nombre, $_REQUEST) ? $_REQUEST[$nombre] : null;
		
		switch ($hash) {
			case 'GET' :
				$_GET[$nombre] = $valor;
				$_REQUEST[$nombre] = $valor;
				break;
			case 'POST' :
				$_POST[$nombre] = $valor;
				$_REQUEST[$nombre] = $valor;
				break;
			case 'COOKIE' :
				$_COOKIE[$nombre] = $valor;
				$_REQUEST[$nombre] = $valor;
				break;
			case 'FILES' :
				$_FILES[$nombre] = $valor;
				break;
			case 'ENV'    :
				$_ENV['nombre'] = $valor;
				break;
			case 'SERVER'    :
				$_SERVER['nombre'] = $valor;
				break;
		}
		
		$jrPeticion[$nombre]['SET.'.$hash] = true;
		$jrPeticion[$nombre]['SET.REQUEST'] = true;

		return $anterior;
	}
	
	public static function get($hash = 'defecto', $mask = 0)
	{
		$hash = strtoupper($hash);
		
		if($hash === 'METHOD') {
			$hash = strtoupper( $_SERVER['REQUEST_METHOD'] );
		}
		
		switch($hash) {
			case 'GET' :
				$input = $_GET;
				break;

			case 'POST' :
				$input = $_POST;
				break;

			case 'FILES' :
				$input = $_FILES;
				break;

			case 'COOKIE' :
				$input = $_COOKIE;
				break;

			case 'ENV'    :
				$input = &$_ENV;
				break;

			case 'SERVER'    :
				$input = &$_SERVER;
				break;

			default:
				$input = $_REQUEST;
				break;
		}
		
		$result = JrPeticion::limpiarValor($input, $mask);
		
		if(get_magic_quotes_gpc() && ($hash != 'FILES')) {
			$result = JrPeticion::quitarSlashes( $result );
		}
		
		return $result;
	}
	
	public function set($array, $hash = 'defecto', $sobreEscribir = true)
	{
		foreach ($array as $key => $valor) {
			JrPeticion::setVar($key, $valor, $hash, $sobreEscribir);
		}
	}
	
	public static function limpiar()
	{
		$_FILES = array();
		$_ENV = array();
		$_GET = array();
		$_POST = array();
		$_COOKIE = array();
		$_SERVER = array();
		
		global $jrPeticion;
		$jrPeticion = array();
	}
	
	protected static function quitarSlashes( $valor )
	{
		$valor = is_array($valor) ? array_map(array('JrPeticion', 'quitarSlashes'), $valor) : stripslashes($valor);
		
		return $valor;
	}
	
	public static function limpiarValor($var, $mask = 0, $type = null)
	{
		return null;
	}
}