<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class JrDocumento extends JrObjeto
{
	protected static $instancia = null;
	
	//metadatos
	protected $titulo = '';
	protected $description = '';
	protected $keywords = '';
	protected $generator = '';
	protected $idioma = 'ES';
	protected $fechaModif = '';
	protected $charset = 'utf-8';
	protected $metaTags = array();
	protected $metaTagsFacebook = '';
	
	protected $urlEnlace = '';
	protected $urlStatic = null;
	protected $urlBase = '';
	protected $urlSitio = '';
	protected $urlTema = '';
	protected $mime = '';
	protected $scripts = '';
	protected $estilos = '';
	protected $css = array();
	protected $tipo = '';
	protected $icon = '';
	protected $buffer = array();
	
	//tema
	protected $tema = 'static::temas::tinku';
	
	public function __construct($atributos = array())
	{
		parent::__construct();
		
		if(array_key_exists('charset', $atributos)) {
			$this->charset = $atributos['charset'];
		}
		
		if(array_key_exists('idioma', $atributos)) {
			$this->idioma = $atributos['idioma'];
		}
	}
	
	/**
	 * Agrega un script
	 * @param string Ruta del script
	 * @return bool
	 */
	protected function addScript($ruta)
	{
		$this->scripts .= '<script type="text/javascript" src="'.$this->getCadenaSegura($ruta).'"></script>'."\n";		
		return true;
	}
	
	/**
	 * Agrega un estilo a importar
	 * @param string Ruta del script
	 * @return void
	 */
	protected function addEstilo($ruta)
	{
		$this->estilos .= '<link href="'.$this->getCadenaSegura($ruta).'" rel="stylesheet" type="text/css" />'."\n";
	}
	
	public function setMetaData($nombre, $valor, $http_equiv = false)
	{
		$nombre = strtolower($nombre);
		
		if($nombre == 'generator') { 
			$this->generator = $valor;
		} elseif($nombre == 'description') {
			$this->description = $valor;
		} elseif($nombre == 'keywords') {
			$this->keywords = strtolower($valor);
		} else {
			if ($http_equiv == true) {
				$this->metaTags['http-equiv'][$nombre] = $valor;
			} else {
				$this->metaTags['standard'][$nombre] = $valor;
			}
		}
	}
	
	public function setMetaFacebook($nom_sitio, $descripcion, $titulo, $tipo, $imagen, $url, $admin)
	{//og:title, og:type, og:image, og:url, og:site_name
		$this->metaTagsFacebook = '<meta property="og:title" content="'.utf8_encode($titulo).'"/>' . "\n";
		$this->metaTagsFacebook .= '<meta property="og:type" content="'.utf8_encode($tipo).'"/>' . "\n";
		$this->metaTagsFacebook .= '<meta property="og:url" content="'.utf8_encode($url).'"/>' . "\n";
		$this->metaTagsFacebook .= '<meta property="og:image" content="'.utf8_encode($imagen).'"/>' . "\n";
		$this->metaTagsFacebook .= '<meta property="og:site_name" content="'.utf8_encode($nom_sitio).'"/>' . "\n";
		$this->metaTagsFacebook .= '<meta property="og:description" content="'.utf8_encode($descripcion).'"/>' . "\n";
		$this->metaTagsFacebook .= '<meta property="fb:admins" content="'.$admin.'"/>' . "\n";
	}
	
	public function getMetaData($nombre, $http_equiv = false)
	{
		$result = '';
		$nombre = strtolower($nombre);
		if($nombre == 'description') {
			$result = $this->description;
		} elseif($nombre == 'keywords') {
			$result = $this->keywords;
		} else {
			if ($http_equiv == true) {
				$result = @$this->metaTags['http-equiv'][$nombre];
			} else {
				$result = @$this->metaTags['standard'][$nombre];
			}
		}
		return $result;
	}
	
	public function getTagsMetaData()
	{
		$tags = '';
		
		foreach($this->metaTags['http-equiv'] as $nombre => $valor) {
			$tags .= '<meta http-equiv="'.$nombre.'" content="'.$valor.'">' . "\n";
		}
		
		foreach($this->metaTags['standard'] as $nombre => $valor) {
			$tags .= '<meta name="'.$nombre.'" content="'.$valor.'">' . "\n";
		}
				
		$tags .= '<meta name="description" content="'.htmlentities($this->description, ENT_COMPAT, "UTF-8").'">' . "\n";
		//$tags .= '<meta name="keywords" content="'.utf8_encode($this->keywords).'">' . "\n";
		$tags .= "<title>".JrWeb::pasarHtml_(ucfirst($this->titulo))."</title>\n";
		$tags .=!empty($this->icon)?'<link rel="icon" type="image/png" href="'.$this->icon.'">':'';		
		$tags .= $this->metaTagsFacebook;
		return $tags;
	}
	
	/**
	 * Procesa la cadena enviada para veridicar caracteres no permitidos
	 * @param string
	 * @return string
	 */
	protected function getCadenaSegura($cadena)
	{
		$cadena = str_replace('"', '&quot;', $cadena);
		$cadena = str_replace('<', '&lt;',$cadena);
		$cadena = str_replace('>', '&gt;',$cadena);
		$cadena = preg_replace('/eval\((.*)\)/', '', $cadena);
		$cadena = preg_replace('/[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']/', '""', $cadena);
		
		return $cadena;
	}
	
	/**
	 * Asigna el titulo
	 */
	public function setTitulo($titulo, $concatena = false)
	{
		$this->titulo = (true === $concatena) ? $titulo . ' - ' . $this->titulo : $titulo;
	}

	public function setIcon($icon)
	{
		$this->icon = $icon;
	}
	
	/**
	 * Obtiene el titulo
	 */
	public function getTitulo()
	{
		return $this->titulo;
	}
	

	public function setIdioma($idioma)
	{
		$this->idioma = strtolower($idioma);		
		global $aplicacion;
		$aplicacion->setIdioma($idioma);
	}
	
	/**
	 * Obtiene el titulo
	 */
	public function getIdioma()
	{		
		return !empty($this->idioma)?$this->idioma:'ES';
	}
	/**
	 * Obtiene un objeto de clase
	 * @return objeto
	 */
	public static function &getInstancia($tipo = 'html', $atributos = array())
	{
		if(!is_object(self::$instancia)) {
			$tipo	= preg_replace('/[^A-Z0-9_\.-]/i', '', $tipo);
			$clase = 'JrDocument'.$tipo;
			$ruta	= dirname(__FILE__) . SD . $clase . '.php';
			
			if(!file_exists($ruta)) {
				$clase = 'JrDocumentoHTML';
			}
			
			if(!class_exists($clase)) {
				JrCargador::clase('jrAdwen::documento::'.$clase);
			}
			
			self::$instancia = new $clase($atributos);
		}
		
		return self::$instancia;
	}
	
	/**
	 * Obtiene la url de los estilos
	 * @return objeto
	 */
	public function setUrlStatic()
	{
		if(empty($this->urlStatic)) {
			$configs = JrConfiguracion::get_();
			$uri = JrURI::getInstancia();	
			$this->urlStatic = !empty($configs['url_static']) ? $configs['url_static'] : $uri->base() . 'static/';
		}
	}

	public function getUrlStatic()
	{
		if(empty($this->urlStatic)) {
			$this->setUrlStatic();
		}		
		return $this->urlStatic;
	}

	public function setUrlSitio()
	{
		if(empty($this->urlSitio)) {
			$configs = JrConfiguracion::get_();
			$uri = JrURI::getInstancia();			
			$this->urlSitio= $this->getUrlBase() . $configs['sitio'];
		}
	}
	
	public function getUrlSitio()
	{
		if(empty($this->urlSitio)) {
			$this->setUrlSitio();
		}		
		return $this->urlSitio;
	}


	public function setUrlTema()
	{
		if(empty($this->urlTema)) {
			$configs = JrConfiguracion::get_();
			$uri = JrURI::getInstancia();			
			$this->urlTema= $this->getUrlSitio() .'/plantillas/'. $configs['tema'];
		}
	}
	
	public function getUrlTema()
	{
		if(empty($this->urlTema)) {
			$this->setUrlTema();
		}		
		return $this->urlTema;
	}
	
	public function getUrlBase()
	{
		if(empty($this->urlBase)) {
			$this->setUrlBase();
		}
		
		return $this->urlBase;
	}

	
	
	public function setUrlBase()
	{
		if(empty($this->urlBase)) {
			$configs = JrConfiguracion::get_();
			$this->urlBase .= $configs['url_base'];
		}
	}
	/**
	 * Asigna el buffer
	 */
	public function setBuffer($contenido, $tipo, $nombre = null)
	{
		$this->buffer = $contenido;
	}
	
	/**
	 * Obtiene el buffer
	 */
	public function getBuffer()
	{
		return $this->buffer;
	}
	
	/**
	 * Cargar proceso
	 */
	public function cargarProceso($tipo, $nombre, $atributos)
	{
		try {
			$datos = null;
			$dir = RUTA_SITIO;
			
			//instancia la aplicacion para obtener el subsistema
			$aplicacion = JrAplicacion::getInstancia();
			$subsistema = $aplicacion->subsistema;			
			$dir .= empty($subsistema) ? '' : $subsistema . SD;			
			if('modulo' == $tipo){
				$dir .= 'plantillas'.SD._mitema_.SD.'modulos' . SD . $nombre . SD . $nombre;
			} else {
				return '';
			}
			
			if(is_file($dir . '.php')) {			
				ob_start();				
				if('modulo' == $tipo) {
					require($dir . '.php');
				} else {
					require_once($dir . '.php');
				}
				
				$datos = ob_get_contents();
				ob_end_clean();
			}
			
			return $datos;
		} catch(Exception $e) {
			return '';
		}
	}
	
	public function getContenido()
	{
		return null;
	}
	
	public function getMensaje()
	{
		$aplicacion = JrAplicacion::getInstancia();
		$msjs = $aplicacion->getColaMsjs();
		
		if(!is_array($msjs)) {
			return;
		}
		
		$conten = '<div id="msj-sistema">';
		foreach($msjs as $msj) {
			if(!empty($msj['msj'])) {
				
				$conten .= '<div class="alert alert-' . @$msj['tipo'] . '" role="alert">'
								. '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> '
								. htmlentities($msj['msj'], ENT_COMPAT, "UTF-8")
							. '</div>';
			}
		}
		$conten .= '</div>';
		
		return $conten;
	}
	
	/**
	 * Procesa formato de fechas
	 */
	public function getFechaDeAMDaDMA($fecha, $sepF = '-', $sepR = '/', $desc = false)
	{
		@list($anio, $mes, $dia) = @explode($sepF, $fecha);
		
		$meses = array('01' => 'Ene',
					   '02' => 'Feb',
					   '03' => 'Mar',
					   '04' => 'Abr',
					   '05' => 'May',
					   '06' => 'Jun',
					   '07' => 'Jul',
					   '08' => 'Ago',
					   '09' => 'Sep',
					   '10' => 'Oct',
					   '11' => 'Nov',
					   '12' => 'Dic');
		
		if(true === $desc) {
			return $dia.' '.@$meses[$mes].' de '.$anio;
		}
		
		return $dia.$sepR.$mes.$sepR.$anio;
	}
}