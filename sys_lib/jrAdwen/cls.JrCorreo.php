<?php

/**
 * @autor		Abel chingo Tello
 * @fecha		11/04/2019
 * @copyright	Copyright (C) 2019. Todos los derechos reservados.
 */

class JrCorreo
{
	private $remiteEmail = null;
	private $remiteNom = null;
	private $destinatarios = array();
	private $destinatarioscc = array();
	private $destinatariosbcc = array();
	private $adjuntos = array();
	private $asunto = null;
	private $mensaje = null;
	private $tipo = 'HTML';
	private  $SMTPAuth=true;
	private  $SMTPDebug=0; //0 no log; 1 mostrar log
	private  $SMTPSecure   = "ssl";
	/*	public	 $Host = 'smtp.office365.com'; 
	public  $Port = 25; 
	public  $Username = 'aulavirtual@abacovirtual.onmicrosoft.com';
    public  $Password = 'Abaco2020$@!';*/

	/* public  $SMTPSecure   = "ssl";
	public	 $Host = 'smtp.gmail.com'; 
	public  $Port = 465; 
	public  $Username = 'aulavirtual@eiger.edu.pe';
    public  $Password = 'eigerperu20';*/
	/*public	 $Host = 'smtp.gmail.com';
	public  $Port = 465;
	public  $Username = 'aulavirtual@smartknowledge-solutions.page';
	public  $Password = 'Abaco2020@!';*/
	/*public 	$Host = 'mail.abacoeducacion.org'; 
	public  $Port = 465; 
	public  $Username = 'info@abacoeducacion.org';
	public  $Password = 'Infoabaco2019@';*/

	public	$Host = 'smtp.gmail.com'; 
	public  $Port = 465; 
	public  $Username = 'aulavirtual@alitic.edu.co';
    public  $Password = '2020Alitic@';
    public  $emailempresa=false;




	public function __construct($config = array())
	{
		$this->limpiarDestinatarios();
		$this->setRemitente('info@abacoeducacion.org', 'Información de Servidor');
		$this->setAsunto('Información de servidor');
	}


	public function limpiarDestinatarios()
	{
		$this->destinatarios = array();
		$this->destinatarioscc = array();
		$this->destinatariosbcc = array();
	}

	public function setRemitente($email, $nombre = null)
	{
		$email = urldecode($email);
		if (preg_match("/\r/", $email) || preg_match("/\n/", $email)) {
			return;
		}

		$nombre = urldecode($nombre);
		if (preg_match("/\r/", $nombre) || preg_match("/\n/", $nombre)) {
			return;
		}
		//strtr($_POST['from'],"\r\n\t",'???');
		$this->remiteEmail = $email;
		$this->remiteNom = $nombre;
	}

	public function setAsunto($asunto)
	{
		$this->asunto = (string) $asunto;
	}

	public function setMensaje($mensaje)
	{
		$this->mensaje = $mensaje;
	}

	public function enviar()
	{
		if (0 == count($this->destinatarios)) {
			throw new Exception('Correo sin destinatario');
		}

		$destinatarios_ = implode(", ", $this->destinatarios);

		$cabeceras_ = "From: " . $this->remiteNom . " <" . $this->remiteEmail . ">\r\n";
		//$cabeceras_ = "To: <".$destinatarios_.">\r\n";
		//$cabeceras_ .= "Return-Path: <".$this->remiteEmail.">\r\n";
		$cabeceras_ .= "Reply-To: " . $this->remiteEmail . "\r\n";
		//$cabeceras_ .= "Cc: ".$this->remiteEmail."\r\n";
		//$cabeceras_ .= "Bcc: ".$this->remiteEmail."\r\n";
		$cabeceras_ .= "X-Sender: " . $this->remiteEmail . "\r\n";
		$cabeceras_ .= "X-Mailer: [" . $this->remiteNom . "]\r\n";
		$cabeceras_ .= "X-Priority: 3 \r\n";
		$cabeceras_ .= "MIME-Version: 1.0 \r\n";
		$cabeceras_ .= "Content-Transfer-Encoding: 7bit \r\n";
		$cabeceras_ .= 'Disposition-Notification-To: "' . $this->remiteNom . '" <' . $this->remiteEmail . "> \r\n";
		//$cabeceras_ .= "\nX-Mailer: PHP/" . phpversion();

		if ('HTML' == $this->tipo) {
			$cabeceras_ .= "Content-type: text/html; charset=iso-8859-1 \r\n";
			$mensaje_ = $this->mensaje;
		} else {
			$cabeceras_ .= "Content-Type: text/plain; charset=iso-8859-1 \r\n";
			$mensaje_ = wordwrap($this->mensaje, 80);
		}
		//echo($cabeceras_);
		//exit();
		return @mail($destinatarios_, $this->asunto, $mensaje_, $cabeceras_);
	}


	public function sendPhpmailer()
	{
		if (0 == count($this->destinatarios)) {
			throw new Exception('Correo sin destinatario');
		}

		JrCargador::clase('PHPMailer::PHPMailerAutoload');
		$mail = new PHPMailer(true);
		$mail->SMTPDebug = $this->SMTPDebug;
		$mail->IsSMTP();
		$mail->SMTPAuth = $this->SMTPAuth;
		$mail->SMTPSecure   = $this->SMTPSecure;
		$mail->Host = $this->Host;
		$mail->Port = $this->Port;
		$mail->Username = $this->Username;
		$mail->Password = $this->Password;
		$mail->From = !empty($this->remiteEmail) ? $this->remiteEmail : $mail->Username;
		$mail->FromName = !empty($this->remiteNom) ? utf8_decode($this->remiteNom) : 'SoporteInfo';

		if($this->emailempresa==true || !empty(MODODEVELOP_emailempresa)){
			$mail->SMTPSecure   = "ssl";
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 465;
			$mail->Username = 'abelchingodeveloper@gmail.com';
			$mail->Password = 'developer2021';	

			//$mail->Username = 'contato.findout@gmail.com';
			//$mail->Password = 'contact2020';	


		}

		if(!empty(email_simulardestinatarios) && !empty(email_destinatarios_dev)){
			$this->limpiarDestinatarios();
			$emailstesting=json_decode(email_destinatarios_dev,true);
			foreach ($emailstesting as $key => $emails){
				$mail->addAddress($emails['email'], !empty($emails['nombre'])?utf8_decode($emails['nombre']):'');
			}
		}else{		
			foreach ($this->destinatarios as $destinatario) {
				$mail->addAddress($destinatario['email'], !empty($destinatario['nombre'])?utf8_decode($destinatario['nombre']):'');
			}

			if (!empty($this->destinatarioscc))
				foreach ($this->destinatarioscc as $destinatario) {
					$mail->addCC($destinatario['email'], !empty($destinatario['nombre'])?utf8_decode($destinatario['nombre']):'');
			}

			if (!empty($this->destinatariosbcc))
				foreach ($this->destinatariosbcc as $destinatario) {
					$mail->AddBCC($destinatario['email'], !empty($destinatario['nombre'])?utf8_decode($destinatario['nombre']):'');
			}
		}
		
		

		$mail->Subject = utf8_decode($this->asunto);

		//adjuntos
		if (!empty($this->adjuntos)) {
			foreach ($this->adjuntos as $nombre => $ruta) {
				$mail->addAttachment($ruta, $nombre);
				$arrRuta = explode('.', $ruta);
				$extension = end($arrRuta);
				if ($extension == 'png' || $extension == 'jpg') {
					$mail->addEmbeddedImage($ruta, $nombre);
				}
			}
		}
		$mail->Body = $this->mensaje;
		$mail->IsHTML(true);
		if (!$mail->Send()) {
			return false;
		}

		return true;
	}

	public function addadjuntos($ruta, $nombre_mostrar)
	{
		if (!empty($nombre_mostrar)) {
			$this->adjuntos[$nombre_mostrar] = $ruta;
		} else {
			$this->adjuntos[] = $ruta;
		}
	}


	public function addDestinatario($direccion, $nombre = null, $adonde = 'Des')
	{ //preg_match
		$direccion = urldecode($direccion);
		if (preg_match("/\r/", $direccion) || preg_match("/\n/", $direccion)) {
			return;
		}

		$nombre = urldecode($nombre);
		if (preg_match("/\r/", $nombre) || preg_match("/\n/", $nombre)) {
			return;
		}
		if ($adonde == 'Des')
			$this->destinatarios[] = array('email' => $direccion, 'nombre' => @$nombre);
		elseif ($adonde == 'Cc')
			$this->destinatarioscc[] = array('email' => $direccion, 'nombre' => @$nombre);
		elseif ($adonde == 'Bcc')
			$this->destinatariosbcc[] = array('email' => $direccion, 'nombre' => @$nombre);
	}

	public function addDestinarioPhpmailer($direccion, $nombre = null)
	{ //preg_match
		$this->addDestinatario($direccion, $nombre);
	}

	public function sendMail($tipo = 'HTML')
	{
		if (0 == count($this->destinatarios)) {
			throw new Exception('Correo sin destinatario');
		}

		$strdestinatarios = [];
		foreach ($this->destinatarios as $destinatario) {
			if (!empty($destinatario['nombre'])) {
				$strdestinatarios[] = $destinatario['nombre'] . ' <' . $destinatario['email'] . '>';
			} else {
				$strdestinatarios[] = $destinatario['email'];
			}
		}
		$destinatarios = implode(", ", $strdestinatarios);

		$destinatarioscc = '';
		if (!empty($this->destinatarioscc)) {
			$strdestinatarioscc = [];
			foreach ($this->destinatarioscc as $destinatario) {
				if (!empty($destinatario['nombre'])) {
					$strdestinatarioscc[] = $destinatario['nombre'] . ' <' . $destinatario['email'] . '>';
				} else {
					$strdestinatarioscc[] = $destinatario['email'];
				}
			}
			$destinatarioscc = 'Cc: ' . (implode(", ", $strdestinatarioscc)) . "\r\n";
		}

		$destinatariosbcc = '';
		if (!empty($this->destinatariosbcc)) {
			$strdestinatariosbcc = [];
			foreach ($this->destinatariosbcc as $destinatario) {
				if (!empty($destinatario['nombre'])) {
					$strdestinatariosbcc[] = $destinatario['nombre'] . ' <' . $destinatario['email'] . '>';
				} else {
					$strdestinatariosbcc[] = $destinatario['email'];
				}
			}
			$destinatariosbcc = 'Bcc: ' . (implode(", ", $strdestinatariosbcc)) . "\r\n";
		}


		$cabeceras = "From: " . $this->remiteNom . " <" . $this->remiteEmail . ">\r\n";
		$cabeceras .= "Reply-To: " . $this->remiteEmail . "\r\n";
		$cabeceras .= "MIME-Version: 1.0\r\n" . $destinatarioscc . $destinatariosbcc . "\r\n";
		$cabeceras .= "X-Priority: 1 (Highest)\n";
		/*$cabeceras .= "X-MSMail-Priority: High\n";
	        $cabeceras .= "Importance: High\n";*/

		if ('HTML' == $tipo) {
			$cabeceras .= "Content-type: text/html; charset=iso-8859-1 \r\n";
			$mensaje = $this->mensaje;
		} else {
			$cabeceras .= "Content-Type: text/plain; charset=iso-8859-1 \r\n";
			$mensaje = wordwrap($this->mensaje, 80);
		}
		/*echo $cabeceras;
			echo $mensaje;
			exit();*/
		$enviado = @mail($destinatarios, $this->asunto, $mensaje, $cabeceras);
		if ($enviado) return true;
		else return false;
	}
}
