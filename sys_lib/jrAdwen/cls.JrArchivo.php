<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		04/02/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class JrArchivo
{
	public $extension;
	public $permitidos;
	
	public function __construct()
	{
		$this->permitidos = array('jpeg', 'png', 'gif', 'bmp');
	}
	
	public static function getExtensionBase64($data)
	{
		$ext = substr($data, 5, strpos($data, ';') - 5);
		$ext_ = explode('/', $ext);
		$ext = array('ext' => $ext_[1], 'mime_type' => $ext);
		if(in_array($ext_[0], array('image'))) {
			$ext['tipo_gen'] = 'imagen';
		}		
		if('jpeg' == $ext['ext']) {
			$ext['ext'] = 'jpg';
		}
		
		return $ext;
	}
	
	public static function mimeAExt($mime_type)
	{
		$ext = explode('/', $mime_type);
		$ext = array('ext' => $ext_[1], 'mime_type' => $ext);
		if('jpeg' == $ext['ext']){$ext['ext'] = 'jpg';}		
		if(in_array($ext_[0], array('image'))) {$ext['tipo_gen'] = 'imagen';}		
		return $ext;
	}
	
	public static function getExtension($nombre)
	{
		$nombre_ = explode('.', $nombre);
		$ext = strtolower($nombre_[count($nombre_)-1]);
		
		if('jpeg' == @$ext['ext']) {
			$ext['ext'] = 'jpg';
		}
		
		return $ext;
	}
	
	public static function cargar($nom_archivo, $ruta_destino, $re_nombre)
	{//id_file
		//[name] => clases.png, [type] => image/x-png, [tmp_name] => C:\WINDOWS\Temp\php7B.tmp
		//[error] => 0, [size] => 61115
		
		if(is_uploaded_file(@$_FILES[$nom_archivo]['tmp_name'])) {
			if(($_FILES[$nom_archivo]['size']/1024) > 2048) {
				throw new Exception('Solo puede subir fotos de hasta 2 MB');
			}
			
			$ext = JrArchivo::getExtension($_FILES[$nom_archivo]['name']);
			if(!in_array($ext, array('jpeg', 'png', 'gif', 'bmp', 'jpg'))){
				throw new Exception('Tipo de archivo no v�lido');
			}
			
			$nom_tmp = @explode('.', $_FILES[$nom_archivo]['name']);
			$re_nombre = empty($re_nombre) ? JrArchivo::limpiar_nombre($nom_tmp[0]) : $re_nombre;
			
			if(file_exists($ruta_destino . $re_nombre . '.' . $ext)) {
				$re_nombre .= '-1';
			}
			
			if(!move_uploaded_file($_FILES[$nom_archivo]['tmp_name'], $ruta_destino . $re_nombre . '.' . $ext)) {
				throw new Exception('El archivo ha sido movido');
			}
			
			@unlink($_FILES[$nom_archivo]['tmp_name']);
			
			$data = array('nombre_full' => $re_nombre . '.' . $ext, 'nombre' => $re_nombre, 'ext' => $ext, 'mime_type' => $_FILES[$nom_archivo]['type']);
						
			if(in_array($ext, array('jpeg', 'png', 'gif', 'bmp', 'jpg'))) {
				$data['tipo_gen'] = 'imagen';
			} elseif(in_array($ext, array('wmv'))) {
				$data['tipo_gen'] = 'video';
			}
			
			return $data;
		} else {
			throw new Exception(JrTexto::_('The file size should not exceed 2MB'));
		}
	}
	
	public static function cargar_base64($data64, $ruta_destino, $re_nombre)
	{
		try {
			if(empty($data64)) {
				throw new Exception(JrTexto::_('Select a file'));
			}			
			JrCargador::clase('jrAdwen::JrSession');			
			$ext_full = JrArchivo::getExtensionBase64($data64);
			$re_nombre = empty($re_nombre) ? uniqid() . '_' . substr(JrSession::crearRandom(), 0, 5) : $re_nombre;
			
			$base64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data64));			
			if(file_exists($ruta_destino . $re_nombre . '.' . $ext_full['ext'])) {
				$re_nombre .= '-1';
			}
			
			file_put_contents($ruta_destino . $re_nombre . '.' . $ext_full['ext'], $base64);
			
			return array('nombre_full' => $re_nombre . '.' . $ext_full['ext'], 'nombre' => $re_nombre, 'ext' => $ext_full['ext']
						, 'mime_type' => $ext_full['mime_type']);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public static function limpiar_nombre($cadena)
	{
		//$cadena = strtolower(htmlentities(self::utf8($cadena), ENT_QUOTES, "UTF-8"));
		$cadena = utf8_decode(strip_tags($cadena));
		$cadena = str_replace(' ', '-', $cadena);
		
		$b = array("�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","'", "�", " ",",",".",";",":","�","!","�","?",'"','+','(',')');
		$c = array("a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","n","","","","","","","","","","","",'','-','','');
		$cadena = str_replace($b, $c, $cadena);
		
		return $cadena;
	}
}