<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class JrInstancia
{
	/**
	 * Crea un objeto de la clase JrDocumento
	 *
	 * @access privado
	 * @return object JrDocumento
	 */
	private static function &crearDocumento()
	{
		JrCargador::clase('jrAdwen::documento::JrDocumento');
		$raw	= JrPeticion::getBool('no_html');
		$tipo	= JrPeticion::getPalabra('formato', $raw ? 'raw' : 'html');		
		$atributos = array ('charset'	=> 'utf-8',	'lineend'	=> 'unix','tab'		=> '  ');		
		$doc =& JrDocumento::getInstancia($tipo, $atributos);
		return $doc;
	}
	
	/**
	 * Obtiene un objeto de la clase JrDocumento
	 *
	 * @return object JrDocumento
	 */
	public static function &getDocumento()
	{
		static $instancia;		
		if(!is_object($instancia)) {
			$instancia = JrInstancia::crearDocumento();
		}		
		return $instancia;
	}
	
	/**
	 * Obtiene una session
	 */
	public function &getSession()
	{
		static $instancia;

		if(!is_object($instancia)) {
			$instancia = JrInstancia::crearSession();
		}		
		return $instancia;
	}
	
	/**
	 * Crea una session
	 */
	private function &crearSession()
	{
		JrCargador::clase('jrAdwen::JrSession');		
		$session = JrSession::getInstancia();		
		return $session;
	}
	
	/**
	 * Referencia al objeto JrURI
	 */
	public static function &getURI($uri = 'SERVER')
	{
		JrCargador::clase('jrAdwen::JrURI');	
		$instancia = JrURI::getInstance($uri);		
		return $instancia;
	}
}