<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		04/02/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class JrPaypal
{
	public function __construct()
	{
		
	}
	
	public static function pdt_request($tx, $pdt_identity_token, $modo = '.sandbox')
	{
		$request = curl_init();
		
		// Set request options
		curl_setopt_array($request, array
			(
				CURLOPT_URL => 'https://www' . $modo . '.paypal.com/cgi-bin/webscr',
				CURLOPT_POST => TRUE,
				CURLOPT_POSTFIELDS => http_build_query(array
					(
					'cmd' => '_notify-synch',
					'tx' => $tx,
					'at' => $pdt_identity_token,
					)
				),
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_HEADER => FALSE,
				// CURLOPT_SSL_VERIFYPEER => TRUE,
				// CURLOPT_CAINFO => 'cacert.pem',
    		)
		);
		
		// Realizar la solicitud y obtener la respuesta
		// y el c�digo de status
		$response = curl_exec($request);
		$status   = curl_getinfo($request, CURLINFO_HTTP_CODE);
		
		// Cerrar la conexi�n
		curl_close($request);
		return array('status' => $status, 'response' => $response);
	}
}