<?php

// Facebook PHP SDK v4.0.8
 
// path of these files have changes
require_once( 'src/Facebook/HttpClients/FacebookHttpable.php' );
require_once( 'src/Facebook/HttpClients/FacebookCurl.php' );
require_once( 'src/Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( 'src/Facebook/HttpClients/FacebookGuzzleHttpClient.php' );
require_once( 'src/Facebook/HttpClients/FacebookStream.php' );
require_once( 'src/Facebook/HttpClients/FacebookStreamHttpClient.php' );
 
// other files remain the same
require_once( 'src/Facebook/FacebookSDKException.php' );
require_once( 'src/Facebook/FacebookRequestException.php' );
require_once( 'src/Facebook/FacebookAuthorizationException.php' );
require_once( 'src/Facebook/FacebookSession.php' );
require_once( 'src/Facebook/FacebookCanvasLoginHelper.php' );
require_once( 'src/Facebook/FacebookClientException.php' );
require_once( 'src/Facebook/FacebookJavaScriptLoginHelper.php' );
require_once( 'src/Facebook/FacebookOtherException.php' );
require_once( 'src/Facebook/FacebookPageTabHelper.php' );
require_once( 'src/Facebook/FacebookPermissionException.php' );
require_once( 'src/Facebook/FacebookRedirectLoginHelper.php' );
require_once( 'src/Facebook/FacebookRequest.php' );
require_once( 'src/Facebook/FacebookResponse.php' );
require_once( 'src/Facebook/FacebookServerException.php' );
require_once( 'src/Facebook/FacebookThrottleException.php' );
require_once( 'src/Facebook/GraphObject.php' );
require_once( 'src/Facebook/GraphAlbum.php' );
require_once( 'src/Facebook/GraphLocation.php' );
require_once( 'src/Facebook/GraphSessionInfo.php' );
require_once( 'src/Facebook/GraphUser.php' );

/*
// path of these files have changes
use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookGuzzleHttpClient;
use Facebook\HttpClients\FacebookStream;
use Facebook\HttpClients\FacebookStreamHttpClient;

use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookSession;
use Facebook\FacebookCanvasLoginHelper;
use Facebook\FacebookClientException;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookOtherException;
use Facebook\FacebookPageTabHelper;
use Facebook\FacebookPermissionException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookServerException;
use Facebook\FacebookThrottleException;
use Facebook\GraphObject;
use Facebook\GraphAlbum;
use Facebook\GraphLocation;
use Facebook\GraphSessionInfo;
use Facebook\GraphUser;*/