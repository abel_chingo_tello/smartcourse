<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

class JrDocumentoHTML extends JrDocumento
{
	public $plantilla = 'general';
	
	public function __construct($atributos = array())
	{
		parent::__construct($atributos = array());
		
		$this->tipo = 'html';
		$this->mime = 'text/html';
		
		$this->setMetaData('Content-Type', $this->mime . '; charset=' . $this->charset,	true);
		$this->setMetaData('robots', 'index, follow' );
	}
	
	public function script($archivo, $ruta = 'js/')
	{
		$documento = &JrInstancia::getDocumento();
		$script = empty($archivo) ? $ruta : $documento->getUrlStatic() . $ruta . $archivo . '.js';
		return $documento->addScript($script);
	}
	
	/**
	 * Agrega un estilo
	 * @param string Ruta del style
	 * @return bool
	 */
	public function stylesheet($archivo, $ruta = 'static/css/')	{
		$documento = &JrInstancia::getDocumento();		
		$stylesheet = empty($archivo) ? $ruta : $documento->getUrlStatic() . $ruta . $archivo;
		
		return $documento->addEstilo($stylesheet);
	}
	
	/**
	 * Agrega al buffer
	 */
	public function setBuffer($contents, $tipo, $nombre = null)
	{
		$this->buffer[$tipo][$nombre] = $contents;
	}
	
	protected function getAgregado($tipo = null, $nombre = null, $atributos = array())
	{
		try {
			$result = null;
			
			if($tipo === null) {
				return $this->buffer;
			}
			
			if(isset($this->buffer[$tipo][$nombre])) {
				return $this->buffer[$tipo][''];
			}
			
			if($result === false) {
				return null;
			}
			
			if('modulo' == $tipo) {
				$result = $this->cargarProceso($tipo, $nombre, $atributos);
			} elseif('modulos' == $tipo) {
				$aplicacion = JrAplicacion::getInstancia();
				
				JrCargador::clase('jrAdwen::documento::JrModulos');
				$proceso = JrModulos::getInstancia($this, $aplicacion->subsistema);
				
				//nombre = posicion
				$result = $proceso->hacer($nombre);
			} else {
				$metodo = 'get' . ucfirst($tipo);
				
				if(method_exists($this, $metodo)) {
					$result = $this->$metodo();
				}
			}
			
			return $result;
		} catch(Exception $e) {
			exit($e->getMessage());
		}
	}
	
	protected function remplazarPlantilla($html)
	{
		$coinc = array();
		if(preg_match_all('#<jrdoc:incluir\ tipo="([^"]+)" (.*)\/>#iU', $html, $coinc)) {
			$nC = count($coinc[1]);
			for($i = 0; $i < $nC; $i++) {
				$tipo = $coinc[1][$i];
				$attribs = $this->pasarAtributos($coinc[2][$i]);
				$nombre  = isset($attribs['nombre']) ? $attribs['nombre'] : null;				
				$html = str_replace($coinc[0][$i], $this->getAgregado($tipo, $nombre, $attribs), $html);
			}
		}		
		return $html;
	}
	
	protected function leerPlantilla($tema, $ruta)
	{
		$documento =& JrInstancia::getDocumento();
		
		$plantilla = empty($this->plantilla) ? 'general' : $this->plantilla;
		
		$ruta_ = $ruta . $tema . SD . $plantilla . '.php';
		
		if(!is_file($ruta_)) {
			if(!is_file($ruta . $tema . SD . 'general.php')) {//agregado
				return null;
			}
			$ruta_ = $ruta . $tema . SD . 'general.php';
		}
		
		ob_start();
		require_once($ruta_);
		$datos = ob_get_contents();
		ob_end_clean();
		
		return $datos;
	}
	
	public function getContenido($args = array())
	{
		if(empty($args['ruta'])) {
			return null;
		}
		
		$datos = $this->leerPlantilla($this->tema, $args['ruta']);
		$datos = $this->remplazarPlantilla($datos);
		
		return $datos;
	}
	
	public function pasarAtributos($cadena)
	{
		$attr		= array();
		$retarray	= array();
		
		preg_match_all('/([\w:-]+)[\s]?=[\s]?"([^"]*)"/i', $cadena, $attr);
		
		if(is_array($attr)) {
			$numPairs = count($attr[1]);
			for($i = 0; $i < $numPairs; $i++) {
				$retarray[$attr[1][$i]] = $attr[2][$i];
			}
		}
		
		return $retarray;
	}
	
	public function getCabecera()
	{
		$cab = $this->getTagsMetaData();
		//$cab .= $this->scripts;
		$cab .= $this->estilos;		
		return $cab;
	}
	
	public function getCabeceraOpt()
	{
		$cab = $this->getTagsMetaData();
		$cab .= $this->estilos;
		
		return $cab;
	}
	
	public function getDocsJs()
	{
		$cab = $this->scripts;		
		return $cab;
	}
	
	public static function pasarAInput($text)
	{//ENT_COMPAT, UTF-8
		return htmlentities(self::utf8($text), ENT_QUOTES, "UTF-8");
	}
	
	public static function pasarHTML($text)
	{//ENT_COMPAT, UTF-8
		return htmlentities(self::utf8($text), ENT_QUOTES, "UTF-8");
	}
	
	public static function codificacion($texto)
	{
		return mb_detect_encoding($texto, 'ASCII,UTF-8,ISO-8859-15', true);
	}
	
	public static function utf8($texto)
	{
		 return (self::codificacion($texto) == 'ISO-8859-15') ? utf8_encode($texto) : $texto;
	}
}