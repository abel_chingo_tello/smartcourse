<?php

/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();

class JrTexto extends JrObjeto
{
	private static $instancia;
	private static $texts = array();
	private $idioma;

	public function __construct($idioma, $tipo = null)
	{
		if (!empty($idioma)) {
			$this->idioma = $idioma;
		}

		if (empty($this->idioma)) {
			$doc = JrDocumento::getInstancia();
			$this->idioma = $doc->idioma;
		}

		$this->idioma = empty($this->idioma) ? 'EN' : $this->idioma;

		if ('EN' != $this->idioma) {
			$this->cargarIdioma($tipo);
		}
	}

	private function cargarIdioma($tipo)
	{
		if (isset(self::$texts[$tipo])) {
			return;
		}

		$ruta_ = RUTA_BASE . 'sys_idiomas' . SD . strtoupper($this->idioma);
		/*echo $ruta_;*/
		if (empty($tipo)) {
			$tipo = 'gen';
		}
		$ruta2 = $ruta_ . '.php';

		if (is_file($ruta2)) {
			require_once($ruta2);
			self::$texts[$tipo] = @$texts;
		} else {

			self::$texts[$tipo] = array();
		}
	}

	private function traducir($texto, $tipo = 'gen')
	{ //($texto, $tipo {null:componente, otro:modulo}


		if ('EN' == $this->idioma) {
			return $texto;
		}

		$textob = strtolower($texto);

		if (array_key_exists($textob, @self::$texts[$tipo])) {
			return self::$texts[$tipo][$textob];
		} else {
			return $texto;
		}
	}


	public static function &getInstancia($idioma)
	{
		if (!is_object(self::$instancia)) {
			self::$instancia = new JrTexto($idioma);
		}

		return self::$instancia;
	}

	public static function _($texto, $tipo = 'gen')
	{
		$oJrTexto = JrTexto::getInstancia(null);
		return $oJrTexto->traducir($texto, $tipo);
	}
}
