<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

class JrWebExcepcion extends JrWeb
{
	private $msj;
	
	public function __construct($msj = '', $tipo = 'INFO')
	{
		parent::__construct();
		
		$this->msj = '<h1>'.$msj.'</h1>';
	}
	
	public function __toString()
	{
		return $this->msj;
	}
}
