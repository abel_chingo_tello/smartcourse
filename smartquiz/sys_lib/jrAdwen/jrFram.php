<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();

JrCargador::clase('jrAdwen::JrObjeto');
JrCargador::clase('jrAdwen::JrInstancia');
JrCargador::clase('jrAdwen::JrAplicacion');
JrCargador::clase('jrAdwen::JrPeticion');
JrCargador::clase('jrAdwen::JrWeb');
JrCargador::clase('jrAdwen::JrConfiguracion');
JrCargador::clase('jrAdwen::JrTexto');
JrCargador::clase('jrAdwen::JrString');
JrCargador::clase('jrAdwen::JrTools');