<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(__FILE__)).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
?>
<div class="row" id="diccionario">

    <div class="col-xs-12">
        <h1>
            <?php echo JrTexto::_('Dictionary');?> 
            <!--<small> 
                <i class="fa fa-angle-double-right"></i> Subtext for header
            </small>-->
        </h1>
    </div>

    <div class="col-xs-12 form-inline frmDiccionario">
        <div class="input-group col-xs-12">
            <input type="text" class="form-control palabra" placeholder="<?php echo ucfirst(JrTexto::_('search'));?>...">
            <span class="input-group-btn">
                <a href="#" class="btn btn-default search-word"><i class="fa fa-search"></i></a>
            </span>
        </div>
    </div>

    <ol class="col-xs-12 result-busqueda">
        <div style="margin-top: 5em; margin-bottom: 5em; color: #888; text-align: center;"><?php echo ucfirst(JrTexto::_('The result of your search will be shown here'));?>.</div>
    </ol>
    

</div>
<script>
$(document).ready(function() {
    $('.btn.search-word').click(function(e) {
        e.preventDefault();
        var value = $('input.palabra').val().toUpperCase();
        if(value.trim().length>0){
            $('.result-busqueda').html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');

            $.ajax({
                url: _sysUrlBase_+'/diccionario/ver',
                type: 'GET',
                dataType: 'json',
                data: {palabra: value},
            })
            .done(function(res) {
                if(res.data.length>0){
                    $.each(res.data, function(key, val) {
                        var palabra = val.palabra;
                        var tipo = val.tipo.trim();
                        var def = val.definicion;
                        var _html = '<li><h3>'+palabra ;
                        if(tipo.length!=0) _html += ' <small>['+ tipo +']</small>';
                        _html += '</h3> <p>'+def+'</p></li>';
                        if(key==0) { $('.result-busqueda').html(_html); }
                        else { $('.result-busqueda').append(_html); }
                    });
                } else {
                    var _html = '<div style="margin-top: 5em; margin-bottom: 5em; color: #888; text-align: center;"><?php echo ucfirst(JrTexto::_('we could not find your word'));?>.</div>';
                    $('.result-busqueda').html(_html);
                }
            })
            .fail(function(err) {
                console.log(err);
                var _html = '<div style="margin-top: 5em; margin-bottom: 5em; color: #888; text-align: center;"><?php echo ucfirst(JrTexto::_('Sorry, something went wrong'));?>.</div>';
                    $('.result-busqueda').html(_html);
            });
        } else {
            var _html = '<div style="margin-top: 5em; margin-bottom: 5em; color: #888; text-align: center;"><?php echo ucfirst(JrTexto::_('The result of your search will be shown here'));?>.</div>';
            $('.result-busqueda').html(_html);
        }
        
    });
    $('.frmDiccionario input.palabra').keypress(function(e) {
        if(e.which==13){
            $('.btn.search-word').trigger('click');
        }
    });
});
</script>