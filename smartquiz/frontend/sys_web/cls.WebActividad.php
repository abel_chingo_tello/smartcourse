<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHerramientas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
class WebActividad extends JrWeb
{
    protected $oNegActividades;
    protected $oNegActividad;
    protected $oNegNiveles;
    protected $oNegMetodologia;
    protected $oNegHerramientas;
    protected $oNegResources;
    private $oNegActividad_detalle;
    public function __construct()
    {
        parent::__construct();      
        $this->oNegActividades = new NegActividad;
        $this->oNegActividad = new NegActividad;
        $this->oNegNiveles = new NegNiveles;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegHerramientas = new NegHerramientas;
        $this->oNegResources = new NegResources;
        $this->oNegActividad_detalle = new NegActividad_detalle;
    }

    public function defecto(){
        return $this->listado();
    }


    public function listado()
    {
        try{
            global $aplicacion;
            $idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
            $idunidad_=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $idsesion_=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(4):0;
            if(empty($idnivel_)||empty($idunidad_)||empty($idsesion_)){ throw new Exception(JrTexto::_('Activity does not exist').'!!'); }
            $this->oNegNiveles->idnivel=$idnivel_;
            $this->nivel=$this->oNegNiveles->getXid();
            $this->oNegNiveles->idnivel=$idunidad_;
            $this->unidad=$this->oNegNiveles->getXid();
            $this->oNegNiveles->idnivel=$idsesion_;
            $this->sesion=$this->oNegNiveles->getXid(); 
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            $filtro=array('nivel'=>$idnivel_ ,'unidad'=>$idunidad_ ,'sesion'=>$idsesion_);
            $this->actividades=$this->oNegActividad->fullActividades($filtro);            
            $this->intentos=$this->oNegActividad->nintentos($idnivel_);
            $this->datostools=$this->oNegHerramientas->buscar(Array('idnivel'=>$idnivel_,'idunidad'=>$idunidad_,'idactividad'=>$idsesion_)); 
            $this->documento->script('editactividad','/js/new/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
            $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('datetimepicker.min', '/libs/datetimepicker/js/');
            $this->documento->script('actividad_completar', '/js/new/');
            $this->documento->script('completar', '/js/new/');
            $this->documento->script('actividad_ordenar', '/js/new/');
            $this->documento->script('actividad_imgpuntos', '/js/new/');
            $this->documento->script('actividad_verdad_falso', '/js/new/');
            $this->documento->script('actividad_fichas', '/js/new/');
            $this->documento->script('actividad_dialogo', '/js/new/');
            $this->documento->script('manejadores_dby', '/js/new/');
            $this->documento->script('manejadores_practice', '/js/new/');           
            $this->documento->stylesheet('estilo', '/libs/crusigrama/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->actividades=$this->oNegActividad->fullActividades($filtro);
            $this->documento->setTitulo(JrTexto::_('alumno/Actividad'), true);
            $this->esquema = 'alumno/actividad';            
            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function buscarjson(){
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            $filtros=array();
            $filtros["idmetodologia"]=!empty($_REQUEST["met"])?$_REQUEST["met"]:'';
            $filtros["titulodetalle"]=!empty($_REQUEST["titulo"])?$_REQUEST["titulo"]:'';
            
            $filtros["nivel"]=(!empty($_REQUEST["nivel"]) && $_REQUEST["nivel"]>0)?$_REQUEST["nivel"]:'';
            $filtros["unidad"]=(!empty($_REQUEST["unidad"]) && $_REQUEST["unidad"]>0)?$_REQUEST["unidad"]:'';
            $filtros["sesion"]=(!empty($_REQUEST["actividad"]) && $_REQUEST["actividad"]>0)?$_REQUEST["actividad"]:'';
           // $filtros["ejercicios"]=true;
            $actividades=$this->oNegActividades->fullActividades($filtros);
            $data=array('code'=>'ok','data'=>$actividades);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function jsonGetXId(){
        try{
            global $aplicacion;
            $this->documento->plantilla = 'returnjson';
            $filtros=array();
            $this->oNegActividad_detalle->iddetalle=!empty($_GET["idAct"])?$_GET["idAct"]:'';
            $actividad_detalles=$this->oNegActividad_detalle->getXid();
            $data=array('code'=>'ok','data'=>$actividad_detalles);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function agregar2()
    {
        try{
            global $aplicacion; 
            $this->accion=empty($_GET["acc"])?'new':@$_GET["acc"];
            /*if(!NegSesion::tiene_acceso('Actividad', 'add')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }*/
            $this->documento->script('tinymce.min', '/libs/tinymce/');
           // $this->documento->script('jquery-ui.min', '/tema/js/');

            $this->documento->script('chi_inputadd', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chi_saveedit', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chi_imageadd', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chi_videoadd', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chi_audioadd', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('editactividad','/js/new/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;
            $this->nivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->idnivel));
            if(!empty($this->nivel[0]))$this->nivel=$this->nivel[0];

            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["txtUnidad"])?$_GET["txtUnidad"]:($_idunidad);  
            $this->unidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->idunidad));
             if(!empty($this->unidad[0]))$this->unidad=$this->unidad[0];

            $this->sesiones=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idsesion=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;
            $this->idsesion=!empty($_GET["txtsesion"])?$_GET["txtsesion"]:($_idsesion);
            $this->actividad=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->idsesion));
             if(!empty($this->actividad[0]))$this->actividad=$this->actividad[0];
             
            $filtro=array('nivel'=>$this->idnivel,'unidad'=>$this->idunidad,'sesion'=>$this->idsesion);
            $this->actividades=$this->oNegActividad->fullActividades($filtro);
            $this->intentos=$this->oNegActividad->nintentos($this->idnivel);
            $this->metodologias=$this->oNegMetodologia->buscar(array('tipo'=>'M'));
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
            $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('datetimepicker.min', '/libs/datetimepicker/js/');
            $this->documento->script('actividad_completar', '/js/new/');
            $this->documento->script('completar', '/js/new/');
            $this->documento->script('actividad_ordenar', '/js/new/');
            $this->documento->script('actividad_imgpuntos', '/js/new/');
            $this->documento->script('actividad_verdad_falso', '/js/new/');
            $this->documento->script('actividad_fichas', '/js/new/');
            $this->documento->script('actividad_dialogo', '/js/new/');
            $this->documento->script('manejadores_dby', '/js/new/');
            $this->documento->script('manejadores_practice', '/js/new/');           
            $this->documento->stylesheet('estilo', '/libs/crusigrama/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('tools_games', '/js/');
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->esquema = 'docente/actividad';
            return parent::getEsquema();
        }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function xExiteactividad(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
                if(empty($args[0])) { return;}
                    $frm = $args[0];
                    if(!empty($frm["nivel"])&&!empty($frm["unidad"])&&!empty($frm["sesion"])){
                        $nivel=$frm["nivel"];
                        $unidad=$frm["unidad"];
                        $sesion=$frm["sesion"];                        
                        $res=$this->oNegActividades->buscar(array('nivel'=>$nivel,'unidad'=>$unidad,'seccion'=>$sesion));
                    }
                    if(!empty($res))
                        $oRespAjax->setReturnValue($res);
                    else
                        $oRespAjax->setReturnValue(false);
                } catch(Exception $e) {
                $oRespAjax->setReturnValue(false);
            } 
        }
    }

    public function guardar(){
        $this->documento->plantilla = 'returnjson';
        try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario();   
            @extract($_POST);
            $this->oNegActividad->nivel=@$txtNivel;
            $this->oNegActividad->unidad=@$txtunidad;
            $this->oNegActividad->sesion=@$txtSesion;
            $this->oNegActividad->metodologias=@$metodologias;
            $this->oNegActividad->titulos=@$titulo;
            $this->oNegActividad->descripcion=@$descripcion;
            $this->oNegActividad->habilidades=@$habilidades;
            $this->oNegActividad->det_tipo=@$det_tipo;
            $this->oNegActividad->det_orden=@$orden;
            $this->oNegActividad->det_tipo_desarrollo=@$tipo_desarrollo;
            $this->oNegActividad->det_tipo_actividad=@$tipo_actividad;
            $this->oNegActividad->det_url=@$url;
            $this->oNegActividad->det_texto=@$det_texto;
            $this->oNegActividad->det_texto_edit=@$det_texto_edit;           
            $this->oNegActividad->rutabase=@$this->documento->getUrlBase();
            $this->oNegActividad->iduser=@$usuarioAct["dni"];
            $this->oNegActividad->det_users=@$iduser;
            $this->oNegActividad->det_iddetalle=@$iddetalle;
            $res=$this->oNegActividad->agregar();
            echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('excercise')).' '.JrTexto::_('saved successfully'),'newides'=>$res)); //
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }
}