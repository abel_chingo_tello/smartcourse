<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-05-2017 
 * @copyright	Copyright (C) 11-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegReporte', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
class WebReporte extends JrWeb
{
	private $oNegReporte;
	private $oNegProyecto;
	private $oNegPersonal;
	private $oNegExamen_alumno;
	public function __construct()
	{
		parent::__construct();
		$this->oNegReporte = new NegReporte;
		$this->oNegProyecto = new NegProyecto;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegExamen_alumno = new NegExamen_alumno;

		$this->usuarioAct = NegSesion::getUsuario();
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;	
			if(strtolower($this->usuarioAct['rol'])=='alumno'){ throw new Exception(JrTexto::_("You must not be here")); }
			$this->listReportes = array(
				array("url"=>"/resultadosxproyecto", "name"=>"Reporte por proyecto"),
				array("url"=>"/resultadoxalumno", "name"=>"Resultados por alumno"),
			);

			$this->documento->setTitulo(JrTexto::_('List'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'reporte/listado';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	/** 
	* resultadosxproyecto: 
	* Reporte para mostrar los resultados de todos los alumnos de un Proyecto (empresa)
	* @param "pr" (opcional) : Slug del proyecto;
	*/
	public function resultadosxproyecto()
	{
		try {
			global $aplicacion;
			if(strtolower($this->usuarioAct['rol'])=='alumno'){ throw new Exception(JrTexto::_("You must not be here")); }

	        $this->documento->script('highcharts', '/libs/highchart/');
	        $this->documento->script('data', '/libs/highchart/modules/');
	        $this->documento->script('drilldown', '/libs/highchart/modules/');
	        $this->documento->script('exporting', '/libs/highchart/modules/');

			$this->documento->script('resultadosxproyecto', '/js/reporte/');
			
			$this->esSuperAdmin = strtolower($this->usuarioAct['rol'])==='superadmin';
			if($this->esSuperAdmin) {
				$this->empresas = $this->oNegProyecto->buscar();
			}

			$this->documento->setTitulo(JrTexto::_('Results by project'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'reporte/resultadosxproyecto';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	/** 
	* resultadoxalumno: 
	* Reporte para mostrar los resultados de examen de un alumno
	* @param "pr" (opcional) : Slug del proyecto;
	*/
	public function resultadoxalumno()
	{
		try {
			global $aplicacion;
			if(strtolower($this->usuarioAct['rol'])=='alumno'){ throw new Exception(JrTexto::_("You must not be here")); }

			$this->documento->script('resultadoxalumno', '/js/reporte/');

			$this->esSuperAdmin = strtolower($this->usuarioAct['rol'])==='superadmin';
			if($this->esSuperAdmin) {
				$this->empresas = $this->oNegProyecto->buscar();
			}

            #var_dump($this->usuarioAct);
            #$this->alumnos = $this->oNegPersonal->buscar(array("idproyecto"=>$this->usuarioAct["proyecto"]["idproyecto"]));

            $this->documento->setTitulo(JrTexto::_('Exams').' /'.JrTexto::_('reports'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'reporte/resultadoxalumno';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function resultado_alumno()
	{
		try {
			global $aplicacion;
			if(empty($_GET['slug']) && empty($_GET['identificador']) && empty($_GET['idexamen'])) {
				throw new Exception(JrTexto::_("Params are missing"));
			}
			extract($_GET);

			$searchEmpresa = $this->oNegProyecto->buscar(array('slug'=>$slug));
			if(empty($searchEmpresa)) { throw new Exception(JrTexto::_("Proyecto/Empresa no habilitada")); }
			$this->proyecto = $searchEmpresa[0];

			$searchAlumno = $this->oNegPersonal->buscar(array('idproyecto'=>$this->proyecto["idproyecto"], 'identificador'=>$identificador));
			if(empty($searchAlumno)) {throw new Exception(JrTexto::_("Student not found"));}
			$this->alumno = $searchAlumno[0];

			$this->examenes_alumno = $this->oNegExamen_alumno->buscarConExam(array('idalumno'=>$this->alumno["dni"], 'idexamen'=>$idexamen));

			if(empty($this->examenes_alumno)) {throw new Exception(JrTexto::_("Exam not found"));}
			$this->examen_alumno = $this->examenes_alumno[0];

			#var_dump($this->examenes_alumno);

			$this->habilidades = json_decode($this->examen_alumno["exam_habilidades_todas"], true);

            $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
            $this->documento->script('circle', '/libs/graficos/progressbar/');

			$this->documento->plantilla = 'verblanco';
            $this->esquema = 'reporte/resultado_alumno';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function xAgregar_editar($datos, $id=null)
	{
		//$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if($id!=null) { $this->oNegReporte->idreporte = $id; }				
			$this->oNegReporte->__set('idusuario',@$datos['idusuario']);
			$this->oNegReporte->__set('idrol',@$datos["idrol"]);
			$this->oNegReporte->__set('tipo',@$datos["tipo"]);
			$this->oNegReporte->__set('id_alumno_grupo',@$datos["id_alumno_grupo"]);
			$this->oNegReporte->informacion = @$datos["informacion"];
			$this->oNegReporte->__set('fechacreacion',@$datos["fechacreacion"]);

			if(@$datos["accion"]=="Nuevo"){ $res=$this->oNegReporte->agregar(); }
			else{ $res=$this->oNegReporte->editar(); }

			$data = array( 'code'=>'ok','data'=>$res );
			return $data;
		} catch (Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			return $data;
		}
	}

	public function ultima_sesion()
	{
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			if(empty($_POST['id']) && empty($_POST['tipo'])){
				throw new Exception(JrTexto::_('Error in filtering'));
			}
			$tipo='';
			$usuarioAct = NegSesion::getUsuario();
			if($_POST['tipo']=='A'){ $tipo='SEGALUM'; }
			if($_POST['tipo']=='G'){ $tipo='SEGGRUPO'; }
			$filtros = array(
				'idusuario'=>$usuarioAct["dni"],
				'idrol'=>$usuarioAct["rol"],
				'id_alumno_grupo'=>$_POST['id'],
				'tipo'=>$tipo,
				'fechacreacion'=>date('y-m-d'),
			);
			$reportePasado=$this->oNegReporte->buscarultimo($filtros);
			$reporteHoy=$this->oNegReporte->buscarDeHoy($filtros);
			
			$datos = $filtros;
			$informacion=json_decode($_POST['informacion'],true);
			$arrElems=array();
			foreach ($informacion as $idHab=>$porcentaje) {
				$elem=array( 'idhabilidad'=>$idHab, 'porcentaje'=>$porcentaje, );
				$arrElems[]=$elem;
			}
			$datos['informacion']=json_encode($arrElems);
			if(empty($reporteHoy)) { $datos['accion']='Nuevo'; } 
			else { $datos['accion']='Editar'; }

			$report = $this->xAgregar_editar($datos, $reporteHoy['idreporte']);
			if($report['code']=='Error'){ throw new Exception(JrTexto::_($report['mensaje'])); }

			$data=array( 'code'=>'ok','data'=>array('ultimo_reporte'=>$reportePasado, 'idreporte_hoy'=>$report['data']) );
            echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
	        echo json_encode($data);
		}	
	}


	// ========================== Funciones xajax ========================== //
	public function x_resultadosxproyecto()
	{
		try {
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';
			$arrResultadosXAlum = array();
			$slugProyecto = @$_REQUEST['slug'];
			if(!$this->estaAfiliado($slugProyecto)){ throw new Exception(JrTexto::_("you are not affiliated with smartQuiz")); }
			if(strtolower($this->usuarioAct['rol'])!=='superadmin' && $this->usuarioAct['proyecto']['slug']==$slugProyecto){ throw new Exception(JrTexto::_("You can not access the results of this Project")); }

			$idProyecto = $this->proyecto['idproyecto'];
			$alumnos = $this->oNegPersonal->buscarConRol(array('idproyecto'=> $idProyecto, 'rol'=>'alumno'));

			if(!empty($alumnos)){
				foreach ($alumnos as $i=>$a) {
					$exam_result = $this->oNegExamen_alumno->buscarConExam(array('idalumno'=>$a['dni'], 'order_by'=>array('EA.idexamen', 'EA.intento')));
					$nodo_alum = array(
						"dni" => $a['dni'],
						"ape_paterno" => $a['ape_paterno'],
						"ape_materno" => $a['ape_materno'],
						"nombre" => $a['nombre'],
						"identificador" => $a['identificador'],
						"foto" => $a['foto'],
						"resultados" => $exam_result,
					);
					$arrResultadosXAlum[] = $nodo_alum;
				}
			}

			echo json_encode(array("code"=>'ok', 'data'=>$arrResultadosXAlum));exit();
			return parent::getEsquema();
		} catch (Exception $e) {
			echo json_encode(array("code"=>'error', 'msje'=>JrTexto::_($e->getMessage())));
		}		
	}
	/*public function xSaveReporte(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdreporte'])) {
					$this->oNegReporte->idreporte = $frm['pkIdreporte'];
				}
				
				$this->oNegReporte->__set('idusuario',@$frm["txtIdusuario"]);
				$this->oNegReporte->__set('idrol',@$frm["txtIdrol"]);
				$this->oNegReporte->__set('tipo',@$frm["txtTipo"]);
				$this->oNegReporte->__set('id_alumno_grupo',@$frm["txtId_alumno_grupo"]);
				$this->oNegReporte->__set('informacion',@$frm["txtInformacion"]);
				$this->oNegReporte->__set('fechacreacion',@$frm["txtFechacreacion"]);
				
			    if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegReporte->agregar();
				}else{
					$res=$this->oNegReporte->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegReporte->idreporte);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDReporte(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegReporte->__set('idreporte', $pk);
				$this->datos = $this->oNegReporte->dataReporte;
				$res=$this->oNegReporte->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegReporte->__set('idreporte', $pk);
				$res=$this->oNegReporte->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}*/

	/*================= FUNCIONES PRIVADAS =================*/
	private function estaAfiliado($slugProyecto=null)
	{
		try {
			if(empty($slugProyecto)){ throw new Exception(JrTexto::_("Falta parámetro 'pr'")); }
			$this->oNegProyecto->slug = $slugProyecto;
			$this->proyecto = $this->oNegProyecto->getXSlug();
			if(empty($this->proyecto)){
	            return false;
	        }
	        return true;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	// ============== Obsoleto par este modulo ============================= //
/*
	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Reporte', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegReporte->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte'), true);
			$this->esquema = 'reporte-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Reporte', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Reporte').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Reporte', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegReporte->idreporte = @$_GET['id'];
			$this->datos = $this->oNegReporte->dataReporte;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Reporte').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegReporte->idreporte = @$_GET['id'];
			$this->datos = $this->oNegReporte->dataReporte;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte').' /'.JrTexto::_('see'), true);
			$this->esquema = 'reporte-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'reporte-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/

}