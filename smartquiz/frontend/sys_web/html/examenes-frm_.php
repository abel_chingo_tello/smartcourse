<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Examenes'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdexamen" id="pkidexamen" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdnivel">
              <?php echo JrTexto::_('Idnivel');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdnivel" name="txtIdnivel" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idnivel"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdunidad">
              <?php echo JrTexto::_('Idunidad');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdunidad" name="txtIdunidad" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idunidad"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdactividad">
              <?php echo JrTexto::_('Idactividad');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdactividad" name="txtIdactividad" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idactividad"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTitulo">
              <?php echo JrTexto::_('Titulo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTitulo" name="txtTitulo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["titulo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDescipcion">
              <?php echo JrTexto::_('Descipcion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDescipcion" name="txtDescipcion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["descipcion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtPortada">
              <?php echo JrTexto::_('Portada');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtPortada" name="txtPortada" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["portada"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFuente">
              <?php echo JrTexto::_('Fuente');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFuente" name="txtFuente" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fuente"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFuentesize">
              <?php echo JrTexto::_('Fuentesize');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFuentesize" name="txtFuentesize" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fuentesize"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Tipo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTipo" name="txtTipo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["tipo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtGrupo">
              <?php echo JrTexto::_('Grupo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtGrupo" name="txtGrupo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["grupo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtAleatroio">
              <?php echo JrTexto::_('Aleatroio');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtAleatroio" name="txtAleatroio" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["aleatroio"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCalificacion_por">
              <?php echo JrTexto::_('Calificacion por');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <select id="txtcalificacion_por" name="txtCalificacion_por" class="form-control col-md-7 col-xs-12" required>
              <option value=""><?php JrTexto::_("Seleccione");?></option>
                              <option value="1"><?php echo JrTexto::_('Activo');?></option>
              <option value="0"><?php echo JrTexto::_('Inactivo');?></option>
                                   
              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCalificacion_en">
              <?php echo JrTexto::_('Calificacion en');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <select id="txtcalificacion_en" name="txtCalificacion_en" class="form-control col-md-7 col-xs-12" required>
              <option value=""><?php JrTexto::_("Seleccione");?></option>
                              <option value="1"><?php echo JrTexto::_('Activo');?></option>
              <option value="0"><?php echo JrTexto::_('Inactivo');?></option>
                                   
              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCalificacion_total">
              <?php echo JrTexto::_('Calificacion total');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <select id="txtcalificacion_total" name="txtCalificacion_total" class="form-control col-md-7 col-xs-12" required>
              <option value=""><?php JrTexto::_("Seleccione");?></option>
                              <option value="1"><?php echo JrTexto::_('Activo');?></option>
              <option value="0"><?php echo JrTexto::_('Inactivo');?></option>
                                   
              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCalificacion_min">
              <?php echo JrTexto::_('Calificacion min');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <select id="txtcalificacion_min" name="txtCalificacion_min" class="form-control col-md-7 col-xs-12" required>
              <option value=""><?php JrTexto::_("Seleccione");?></option>
                              <option value="1"><?php echo JrTexto::_('Activo');?></option>
              <option value="0"><?php echo JrTexto::_('Inactivo');?></option>
                                   
              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTiempo_por">
              <?php echo JrTexto::_('Tiempo por');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <select id="txttiempo_por" name="txtTiempo_por" class="form-control col-md-7 col-xs-12" required>
              <option value=""><?php JrTexto::_("Seleccione");?></option>
                              <option value="1"><?php echo JrTexto::_('Activo');?></option>
              <option value="0"><?php echo JrTexto::_('Inactivo');?></option>
                                   
              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTiempo_total">
              <?php echo JrTexto::_('Tiempo total');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTiempo_total" name="txtTiempo_total" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["tiempo_total"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["estado"];?>"
                data-valueno="0" 
                data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtEstado" value="<?php echo @$frm["estado"];?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdpersonal">
              <?php echo JrTexto::_('Idpersonal');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdpersonal" name="txtIdpersonal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idpersonal"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFecharegistro">
              <?php echo JrTexto::_('Fecharegistro');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFecharegistro" name="txtFecharegistro" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fecharegistro"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveExamenes" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('examenes'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Examenes', 'saveExamenes', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Examenes"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Examenes"))?>');
        <?php endif;?>       }
     }
  });







  
  
});


</script>

