<?php defined('RUTA_BASE') or die(); $idgui = uniqid();?>
  	<style>	
	canvas { 
		display: inline-block; 
		background: #202020; 
		width: 95%;
		height: 45%;
		box-shadow: 0px 0px 10px blue;
	}
	#controls {
		display: flex;
		flex-direction: row;
		align-items: center;
		justify-content: space-around;
		height: 20%;
		width: 100%;
	}
	#record { height: 15vh; }
	#record.recording { 
		background: red;
		background: -webkit-radial-gradient(center, ellipse cover, #ff0000 0%,lightgrey 75%,lightgrey 100%,#7db9e8 100%); 
		background: -moz-radial-gradient(center, ellipse cover, #ff0000 0%,lightgrey 75%,lightgrey 100%,#7db9e8 100%); 
		background: radial-gradient(center, ellipse cover, #ff0000 0%,lightgrey 75%,lightgrey 100%,#7db9e8 100%); 
	}
	#save, #save img { height: 10vh; }
	#save { opacity: 0.25;}
	#save[download] { opacity: 1;}
	#viz {
		height: 500px;
		width: 100%;
		display: flex;
		flex-direction: column;
		justify-content: space-around;
		align-items: center;
	}
	@media (orientation: landscape) {
		body { flex-direction: row;}
		#controls { flex-direction: column; height: 250px; width: 10%;}
		#viz { height: 250px; width: 700px; float:left}
	}

	</style>
	<audio src="<?php echo $this->documento->getUrlStatic() ?>/media/audio/N1-U1-A1-20161214101834.mp3" controls></audio>



	<div id="viz">
		<canvas id="analyser" width="1024" height="300"></canvas>
		<canvas id="wavedisplay" width="1024" height="300"></canvas>
        <div id="div_audio" style="width:100%; height:50px">
        <audio width="100%" height="50px" style=" border:solid 0px #000" controls></audio>
        </div>
	</div>
	<div id="controls">
		<img id="record" src="<?php echo $this->documento->getUrlStatic() ?>/libs/record/mic128.png" onclick="toggleRecording(this);">
		<a id="save" href="#"><img src="<?php echo $this->documento->getUrlStatic() ?>/libs/record/save.svg"></a>
        
	</div>

	<div id="div_listaaudio" style="width:100%; height:250px">
		

		<div class="form-view" >
		
		  
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			   <div class="x_panel">
		        <div class="x_title">		          
		          <div class="nav navbar-right"></div>
		          <div class="clearfix"></div>
		        </div>
		        <div class="div_linea"></div>
		         <div class="x_content">
		            <table class="table table-striped table-responsive">
		              <thead>
		                <tr class="headings">
		                  <th>#</th>                  
		                    <th><?php echo JrTexto::_("Titulo") ;?></th>		                    
		                    <th><?php echo JrTexto::_("Estado") ;?></th>
		                    
		                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
		                </tr>
		              </thead>
		              <tbody>
		              <?php $i=0; 
		                if(!empty($this->datos))
		                foreach ($this->datos as $reg){ $i++; ?>
		                <tr>
		                  <td><?php echo $i;?></td>                  
		                    <td><?php echo $reg["nombre"] ;?></td>
		                    <td><?php echo $reg["estado"] ;?></td>
		                    <td><a class="btn btn-xs lis_ver " onclick="ver_audio('<?php echo $reg["link"] ;?>')"><i class="fa fa-eye"></i></a>                
		                
		                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["idrecord"]; ?>" ><i class="fa fa-trash-o"></i></a>
		              </td>                        
		            </tr>
		            <?php } ?>
		                    </tbody>
		        </table>
		        </div>
		        </div>
		      </div>
		    </div>
		</div>


	</div>
    

<form method="post" id="frm-record"  target="" enctype="multiform/form-data" class="form-horizontal form-label-left"  onsubmit="return false">
        <input type="text" name="txtAudio" id="txtAudio">
        <input type="text" name="txtTemp" id="txtTemp">
        <input type="text" name="nivel" id="nivel" value="<?=$_GET['idnivel'];?>">
        <input type="text" name="unidad" id="unidad" value="<?=$_GET['idunidad'];?>">
        <input type="text" name="actividad" id="actividad" value="<?=$_GET['idactividad'];?>">
        <input type="text" name="txtTipo" id="txtTipo" value="Record">
        <button id="btn-saveActividades" type="submit" class="btn btn-success"   ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
        
        <a onclick="blobToDataURL1()">ok</a>
       
        </form>



<script type="text/javascript">
//**dataURL to blob**

function dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}

//**blob to dataURL**
function blobToDataURL(blob, callback) {
    var a = new FileReader();
    a.onload = function(e) {callback(e.target.result);}
    a.readAsDataURL(blob);
}


var dominiostatic='<?php echo $this->documento->getUrlStatic() ?>';
</script>
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic() ?>/libs/record/recorder.js?36"></script>
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic() ?>/libs/record/main.js?35"></script>


<script type="text/javascript">
$(document).ready(function(){  
           
	
	//$('#nivel').val($('#txtNivel').val());
		   
	$('#frm-record').bind({    
     submit: function(event){
		
		
	var formData = new FormData();
	/*formData.append(fileType + '-filename', fileName);
	formData.append(fileType + '-blob', blob);*/

	var blob =($('#txtTemp').val());
	alert(blob);

	formData.append("tipo","audio");
    formData.append("filearchivo",blob);
    formData.append("nivel",1);//$("#txtNivel").val()
    formData.append("unidad",1);//$("#txtunidad").val()
    formData.append("leccion",1);//$("#txtsesion").val()
    //dataurl=file.attr('data-url');

		  
	$.ajax({
      url: '<?php echo $this->documento->getUrlBase(); ?>/biblioteca/cargarmedia',
      type: "POST",
      data:  formData,
      contentType: false,
      dataType :'json',
      cache: false,
      processData:false,
      success: function(data)
      {
         
         console.log(data);

		},
      error: function(e){
      	console.log(e);	
      }
      
		});

      /*event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Record', 'saveRecord', xajax.getFormValues('frm-record'));*/

      
	
      }
	 
  });
  
  

});

	
	initAudio();
	//defecto();


function ver_audio(audio_url){
	alert(audio_url);
	document.getElementById("div_audio").innerHTML='<audio src="'+audio_url+'" width="100%" height="50px" style=" border:solid 0px #000" controls></audio>';
}



</script>