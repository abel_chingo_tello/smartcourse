<?php defined('RUTA_BASE') or die(); $idgui = uniqid(); 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_vocabulary.css">
<?php 
$vocAdmin=null;
$vocdocente=null;
if(!empty($this->datos))
    foreach ($this->datos as $voc){
        if($voc["orden"]=='0'){
            $vocAdmin=$voc;
        }
        if($voc["idpersonal"]===$usuarioAct["dni"]&&$rolActivo!=1){
            $vocdocente=$voc;
        }
    }
$txtini='<table width="98%"><thead><tr><th>Vocabulary</th><th>English</th><th>Class</th></tr></thead><tbody><tr><td> <br> </td><td> <br> </td><td> <br> </td></tr><tr><td> <br> </td><td> <br> </td><td> <br> </td></tr></tbody></table>';
 if(empty($vocAdmin)){
    $vocAdmin["texto"]=$txtini;
    }
  if(empty($vocdocente)){
    $vocdocente["texto"]=$txtini;
    }
if($rolActivo=='Alumno'){?>

<?php }else{ ?>
<div class="container-fluid" id="voca-tool">
    <div class="row voca-container" id="admin-voca">
        <div class="to_clone" data-edit="pnl0<?php echo $idgui; ?>">
            <h2 class="col-md-12 col-sm-12 col-xs-12 title-voca"><?php echo JrTexto::_('check these out') ?>...</h2>
            <?php if($rolActivo==1){?>
            <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                <div class="btn-toolbar" role="toolbar" >
                    <div class="btn btn-xs btn-group btnedithtml<?php echo $idgui; ?>"><i class="fa fa-text-width"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnsavehtml<?php echo $idgui; ?> disabled" data-id="<?php echo @$vocAdmin["idtool"] ?>">
                        <i class="fa fa-save"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnborrarhtml<?php echo $idgui; ?>" data-id="<?php echo @$vocAdmin["idtool"] ?>">
                        <i class="fa fa-trash"></i>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-md-12 col-sm-12 col-xs-12 voca-main">
                <?php if($rolActivo==1){?>
                <textarea id="txtarea_pnl0<?php echo $idgui; ?>" style="display: none">
                   <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$vocAdmin["texto"]); ?>
                </textarea>
                <?php } ?>
                <div class="tbl-voca panel_pnl0<?php echo $idgui; ?>">
                    <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$vocAdmin["texto"]); ?>
                </div>
            </div>
        </div>
    </div>
<?php if($rolActivo!=1){?>
    <hr>
    <div class="row voca-container" id="other-voca">
        <div class="row other-addpnl1<?php echo $idgui; ?>">
            <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                <a href="#" class="btn btn-primary addMyvoca" style="display: block;" data-edit="pnl1<?php echo $idgui; ?>">
                    <i class="fa fa-plus-square"></i> 
                    <span><?php echo JrTexto::_('add my own vocabulary') ?></span>
                </a>
            </div>
        </div>
        <div class="to_clone hidden d_pnl1<?php echo $idgui; ?>" data-edit="pnl1<?php echo $idgui; ?>">
            <h2 class="col-md-12 col-sm-12 col-xs-12 title-voca text-right">...<?php echo JrTexto::_('a few voca more'); ?></h2>
            <?php  if($rolActivo!=1){?>
            <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                <div class="btn-toolbar" role="toolbar" >
                    <div class="btn btn-xs btn-group btnedithtml<?php echo $idgui; ?>">
                        <i class="fa fa-text-width"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnsavehtml<?php echo $idgui; ?> disabled" data-id="<?php echo @$vocdocente["idtool"] ?>">
                        <i class="fa fa-save"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnborrarhtml<?php echo $idgui; ?>" data-id="<?php echo @$vocdocente["idtool"] ?>">
                        <i class="fa fa-trash"></i>
                    </div>
                </div>
            </div>
            <?php  } ?>
            <div class="col-md-12 col-sm-12 col-xs-12 voca-main">
                <?php if($rolActivo!=1){?>
                <textarea id="txtarea_pnl1<?php echo $idgui; ?>" style="display: none">
                   <?php echo @@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$vocdocente["texto"]); ?>
                </textarea>
                <?php } ?>
                <div class="tbl-voca panel_pnl1<?php echo $idgui; ?>">
                    <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$vocdocente["texto"]); ?>
                </div>
            </div>
            <div class="row other-removepnl1<?php echo $idgui; ?>">
                <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                    <a href="#" class="btn btn-danger btnborrarhtml<?php echo $idgui; ?>" data-id="<?php echo @$vocdocente["idtool"] ?>" style="display: block;">
                        <i class="fa fa-trash"></i> 
                        <span><?php echo JrTexto::_('Remove just my own vocabulary') ?></span>
                    </a>
                </div>
            </div>  
        </div>        
    </div>
    <?php } ?>
</div>
<script type="text/javascript">
	//var TXTAREA_INICIAL_VOCAB = $('.vocabulary-main textarea').clone();
	$('.btnborrarhtml<?php echo $idgui; ?>').click(function(e){
        e.preventDefault();
        var obj=$(this).closest('.to_clone');
        var pnl=obj.data('edit');        
        var res = xajax__('', 'Tools', 'eliminar', $(this).data('id'));
        $(obj).find('#textarea_'+pnl).html('<?php echo $txtini;?>');
        $(obj).find('.tbl-voca.panel_'+pnl).html('<?php echo $txtini;?>');
        $(this).addClass('disabled').attr('data-id',0);
        $(this).siblings('[data-id]').addClass('disabled').attr('data-id',0);
        $('.other-add'+pnl).show();
        $('.other-add'+pnl+' .addMyvoca').show();
        $('.d_'+pnl).hide();
    });

	var iniciarVocabulario = function(idVocabul){
		$(idVocabul+'	.btn.btnedithtml<?php echo $idgui; ?>').trigger('click');
		$(idVocabul+'	.panel<?php echo $idgui; ?>').hide();
		$(idVocabul+'	.btnedithtml<?php echo $idgui; ?>').addClass('disabled');
	};

	var formatoVocabul = function(){
		$('#voca-tool table,#voca-tool th,#voca-tool tr,#voca-tool td').removeAttr('style');

        $('#voca-tool td').find('img').each(function(index, el) {
            $(this).addClass('table-img');
            $(this).parents('td').css('width', $(this).outerWidth() );
        });
	};

	
	var asignarDimensionModal = function($elem_tool){
		var height = $(document).outerHeight();
		var idModal = $elem_tool.parents('.modal').attr('id');
		$('#'+idModal+' #modalcontent').css({
			'max-height': (height-180)+'px',
			'overflow' : 'auto',
		})
	};

	var previewVocabulario = function(){
		$('#voca-tool .nopreview').hide();
		$('#admin-vocabulary .btnsavehtml<?php echo $idgui; ?>, #other-vocabulary .btnsavehtml<?php echo $idgui; ?>').trigger('click');
	};

	var guardarVocabulario = function( $textarea ,idpk,btn){
        var data = new Array();
        data.idNivel = $('#actividad-header').find('select#txtNivel').val();
        data.idUnidad = $('#actividad-header').find('select#txtunidad').val();
        data.idActividad = $('#actividad-header').find('select#txtsesion').val();
        data.texto= $textarea.val();
        data.tool = 'V';
        data.pkIdlink= idpk||0;


        var res = xajax__('', 'Tools', 'saveTools', data);
        if(res){
            $(btn).attr('data-id',res);
            $(btn).siblings('[data-id]').attr('data-id',res);
        }
    };

	var vertinymcevoc=function(id,txt=null){
		if(txt!=''&&txt!=undefined&&txt!=null){
			$(id).html(txt);
		}
		tinymce.init({
			relative_urls : false,
			convert_newlines_to_brs : true,
			menubar: false,
			statusbar: false,
			verify_html : false,
			content_css : URL_BASE+'/static/tema/css/bootstrap.min.css',
			selector: id,
			height: 400,
			plugins:["table chingoinput chingoimage chingoaudio chingovideo textcolor" ], 
			toolbar: 'undo redo | table | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor | chingoinput chingoimage chingoaudio chingovideo'
		});
	};

	 $('#voca-tool #admin-voca, #voca-tool #other-voca')
        .on('click', '.btnsavehtml<?php echo $idgui; ?>', function(e){
            if($(this).hasClass('disabled')){
                return false;
            }
            $(this).addClass('disabled');
            var obj=$(this).closest('.to_clone');
            var pnl=obj.data('edit');
            var idContainer = ' #' + $(this).parents('.voca-container').attr('id');
            $(idContainer+' .btnedithtml<?php echo $idgui; ?>').removeClass('disabled');
            tinyMCE.triggerSave();
            var txt=$(idContainer+' #txtarea_'+pnl).val();
            $(idContainer+' .panel_'+pnl).show();
            $(idContainer+' .panel_'+pnl).html(txt);
            tinymce.remove(idContainer+' textarea');   
            $(idContainer+' textarea').hide();
            $(idContainer+' .btnborrarhtml<?php echo $idgui; ?>').removeClass('disabled');            
            btn=$(this);
            $(idContainer+' .btnsavehtml<?php echo $idgui; ?>').addClass('disabled');
            formatoVocabul();
            guardarVocabulario( $(idContainer+' textarea'),$(this).data('id'),btn);
            
        })
        .on('click', '.btnedithtml<?php echo $idgui; ?>', function(e){
            var idContainer = ' #' + $(this).parents('.voca-container').attr('id');
            if($(this).hasClass('disabled')){
                return false;
            }else{
                var obj=$(this).closest('.to_clone');
                var pnl=obj.data('edit');
                $(idContainer+' .panel_'+pnl).hide();
                $(idContainer+' textarea').show();
                $(idContainer+' .btnedithtml<?php echo $idgui; ?>').addClass('disabled');
                $(idContainer+' .btnsavehtml<?php echo $idgui; ?>').removeClass('disabled');
                vertinymcevoc('#'+$(idContainer+' textarea').attr('id'));
            }
        });

    $('#voca-tool #other-voca')
        .on('click', '.btn.addMyvoca', function(e){
            e.preventDefault();
            var objid=$(this).data('edit');
            $('.d_'+objid).show().removeClass('hidden');
            $('.other-remove'+objid+' .btn').removeClass('disabled');
            $(this).hide();           
        })
        .on('click', '.btn.removevoca', function(e){
            e.preventDefault();
            $('#other-voca').find('.cloned').remove();
            $('.btn.addMyvoca').removeClass('hidden');
        });

	$('.preview').click(function(e) {
		previewVocabulario();
	});

	$('.btnsaveActividad').click(function(e){
		previewVocabulario();
	});

	/* al iniciar */
	iniciarVocabulario('#admin-vocabulary');
	asignarDimensionModal( $('#voca-tool') );
</script>
<?php } ?>