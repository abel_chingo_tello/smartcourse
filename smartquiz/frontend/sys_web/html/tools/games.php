<?php defined('RUTA_BASE') or die(); $idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$idTool = null;
if(!empty($this->datos[0])){
    $idTool = $this->datos[0]['idtool'];
    $game=@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$this->datos[0]["texto"]);
}
?>
<input type="hidden" name="hRol" id="hRol" value="<?php echo $rolActivo; ?>">
<input type="hidden" name="idNivel" id="idNivel" value="<?php echo $this->nivel; ?>">
<input type="hidden" name="idUnidad" id="idUnidad" value="<?php echo $this->unidad; ?>">
<input type="hidden" name="idActividad" id="idActividad" value="<?php echo $this->actividad; ?>">
<input type="hidden" name="idgame" id="idgame" value="<?php echo $idTool; ?>">
<style type="text/css"> #games-tool .addtext{ margin: 0; } </style>
<div class="container-fluid" id="games-tool">
    <div class="row games-container">
        <?php if($rolActivo!='Alumno'){ ?>
        <div class="col-md-12 col-sm-12 col-xs-12 games-selector">
            <div class="col-sm-6 col-md-4  btn-container">
                <a href="#" class="btn btn-blue btn-square" data-template="game_crucigrama">
                    <span class="btn-label"><?php echo JrTexto::_('crossword') ?></span>
                    <span class="btn-information"></span>
                    <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/puzzle_crossword.png" class="btn-icon">
                </a>
            </div>

            <div class="col-sm-6 col-md-4 btn-container">
                <a href="#" class="btn btn-green2 btn-square" data-template="game_rompecabezas">
                    <span class="btn-label"><?php echo JrTexto::_('puzzle') ?></span>
                    <span class="btn-information"></span>
                    <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/puzzle.png" class="btn-icon">
                </a>
            </div>

            <div class="col-sm-6 col-md-4 btn-container">
                <a href="#" class="btn btn-yellow btn-square" data-template="game_sopaletras">
                    <span class="btn-label"><?php echo JrTexto::_('word<br>search') ?></span>
                    <span class="btn-information"></span>
                    <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/puzzle_word_search.png" class="btn-icon">
                </a>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 games-btns nopreview" style="display: none;">
            <a href="#" class="btn btn-danger discard-game">
                <i class="fa fa-trash-o"></i> 
                <?php echo JrTexto::_('Discard').' '.JrTexto::_('game'); ?>
            </a>
            <a href="#" class="btn btn-info preview-game ">
                <i class="fa fa-eye"></i> 
                <?php echo JrTexto::_('Preview') ?>
            </a>
             <a href="#" class="btn btn-default backedit-game " style="display: none;">
                <i class="fa fa-edit"></i> 
                <?php echo JrTexto::_('Back') ?>
            </a>
        </div>
        <?php } ?>

        <div class="col-xs-12 games-titulo-descripcion text-center">
            <div class="col-md-12 titulo">
                <h3>
                    <p style="display: none;"></p>
                    <div class="input-group">
                        <span class="input-group-addon" id="titulo-game"><?php echo ucfirst(JrTexto::_('Title')); ?></span>
                        <input class="titulo-game form-control"  name="titulo-game" placeholder="<?php echo JrTexto::_('Add title'); ?>" value="<?php echo @$this->datos[0]["titulo"]; ?>">
                    </div>
                </h3>
            </div>
            <div class="col-md-12 descripcion">
                <p style="display: none;"></p>
                <div class="input-group">
                    <span class="input-group-addon" id="descripcion-game"><?php echo ucfirst(JrTexto::_('Description')); ?></span>
                    <input class="descripcion-game form-control"  name="descripcion-game" placeholder="<?php echo JrTexto::_('Add description'); ?>"  value="<?php echo @$this->datos[0]["descripcion"]; ?>">
                </div>
            </div>
            <hr>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 games-main">
            <?php if(!empty($game)){ echo $game; } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var IDGame = '<?php echo $idTool; ?>';
    var msjes = {
        'confirm_action' : '<?php echo JrTexto::_('Confirm action');?>',
        'are_you_sure' : '<?php echo JrTexto::_('are you sure you want to delete this').' '.JrTexto::_('game'); ?>?',
        'accept' : '<?php echo JrTexto::_('Accept');?>',
        'cancel' : '<?php echo JrTexto::_('Cancel');?>',
        'guardar' : '<?php echo JrTexto::_('Save');?>',
    };

    $('.games-selector').on('click', 'a', function(e){
        e.preventDefault();
        var tmp = $(this).data('template'); 
        var ruta = sitio_url_base+'/plantillas/actividad/'+tmp+'.php?nivel=<?php echo $this->nivel; ?>&unidad=<?php echo $this->unidad; ?>&actividad=<?php echo $this->actividad; ?>';
        $.get(ruta, function(data) {
            mostrarGame(true, data);
        });
        $('.games-titulo-descripcion').show();
    });
    var fnPreviewGame = function(){
        $('.discard-game').hide();
        $('.save-crossword').hide();
        $('.plantilla-crucigrama .nopreview').hide();
        $('.plantilla-sopaletras .nopreview').hide();
        $('.plantilla-rompecabezas .nopreview').hide();
        $(this).hide();
        $('.backedit-game').show();        
        $('.titulo-game').closest('.input-group').hide().closest('.titulo').find('p').html($('.titulo-game').val()).show();
        $('.descripcion-game').closest('.input-group').hide().siblings('p').html($('.descripcion-game').val()).show();
    };
    $('.games-btns').on('click', '.btn.discard-game',function(e){
        e.preventDefault();        
        var msje =msjes;
        $.confirm({
            title: msje.confirm_action,
            content: msje.are_you_sure,
            confirmButton: msje.accept,
            cancelButton: msje.cancel,
            confirmButtonClass: 'btn-success',
            cancelButtonClass: 'btn-danger',
            closeIcon: true,
            confirm: function(){
                var idgame=$('#idgame').val();
                if(idgame!=''){
                    var res = xajax__('', 'Game', 'eliminar', idgame);
                    if(res){
                        IDGame = '';
                        var padre = window.parent;
                        $(padre.document).find("ul.listagames li.active").remove();
                        padre.ejecutarli();                      
                    }
                }
                $('.games-titulo-descripcion').hide();
                mostrarGame(false);
            }
        });
    }).on('click', '.preview-game',function(e){
        e.preventDefault();  
        fnPreviewGame();
    }).on('click', '.backedit-game',function(e){
        e.preventDefault();  
        $('.btn.discard-game').show();
        $('.btn.save-crossword').show();
        $('.plantilla-sopaletras #findword-game .nopreview').show();
        $('.plantilla-crucigrama .nopreview').show();
        $('.plantilla-rompecabezas .nopreview').show();
        if($('.plantilla-rompecabezas .edit-puzzle').hasClass('active')){
            $('.plantilla-rompecabezas .botones-puzzle').hide();
            $('.plantilla-rompecabezas .video-loader').show();
            $('.plantilla-rompecabezas .rspta-nivel-contenedor').show();
        }else{
            $('.plantilla-rompecabezas .botones-puzzle').show();
            $('.plantilla-rompecabezas .video-loader').hide();
            $('.plantilla-rompecabezas .rspta-nivel-contenedor').hide();
        }        
        $(this).hide();
        $('.preview-game').show();
        $('.titulo-game').closest('.input-group').show().siblings('p').hide();
        $('.descripcion-game').closest('.input-group').show().siblings('p').hide();
    }).on('click', '.add-new-game', function(e) {e.preventDefault();});

    $('#games-tool')
        .on('click', '.addtext', function(e){
            $(this).css({"border": "0px"}); 
            addtext1(e,this);
        })
        .on('blur','.addtext>input',function(e){
            e.preventDefault();
            $(this).closest('.addtext').removeAttr('Style'); 
            addtext1blur(e,this);
        })
        .on('keypress','.addtext>input', function(e){
            if(e.which == 13){
                e.preventDefault();          
                $(this).trigger('blur');
            } 
        })
        .on('keyup','input', function(e){
            if(e.which == 27){
                $(this).attr('data-esc',"1");
                $(this).trigger('blur');
            }
     });
    //asignarDimensionModal( $('#games-tool') );
    $(document).ready(function(){
        <?php if(!empty($game)){?>
        if( $('#hRol').val()=='Alumno' ){ fnPreviewGame(); }
        mostrarGame(true);
        $('.games-selector').hide();
        $('.games-titulo-descripcion').show();
        $('.games-main').iniciarVisor({
            fnAlIniciar: function(){
                var $tmpl = $('.games-main').find('.plantilla');
                if($tmpl.hasClass('plantilla-sopaletras')){iniciarSopaLetras($tmpl);}
                else if($tmpl.hasClass('plantilla-crucigrama')){iniciarCrucigrama($tmpl);}
                else if($tmpl.hasClass('plantilla-rompecabezas')){iniciarRompecabezas($tmpl); console.log($tmpl);}
            },
            texto:msjes,
        });
        <?php }else{?>
            $('.games-selector').show();
            $('.games-titulo-descripcion').hide();
        <?php } ?>
    });
    

    var _IDHistorialSesion=0;
    /******************************************************/
    var registrarHistorialSesion = function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/agregar',
            type: 'POST',
            dataType: 'json',
            data: {'lugar': 'G', 'fechaentrada': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                console.log("success");
                console.log(resp);
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                console.log(resp);
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
        });
    };

    registrarHistorialSesion();
    $(window).on('beforeunload', function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _IDHistorialSesion, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                console.log("success");
                console.log(resp);
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
            return false;
        });
    });
</script>