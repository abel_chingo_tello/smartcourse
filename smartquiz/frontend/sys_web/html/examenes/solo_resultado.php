<?php
$examen = $this->examen;
$rutabase = $this->documento->getUrlBase();
$arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
$srcPortada = (!empty($examen['portada']!=''))? str_replace('__xRUTABASEx__', $rutabase, $examen['portada']) : '';

$tipoEvaluacion = '';
$calificacion_en = '';
if($examen['calificacion_en']=='N') {
	$tipoEvaluacion = ' pts.';
	$calificacion_en = ucfirst(JrTexto::_("numeric"));
} else if($examen['calificacion_en']=='P') {
	$tipoEvaluacion = '%';
	$calificacion_en = ucfirst(JrTexto::_("percentage"));
} else {
	$calificacion_en = ucfirst(JrTexto::_("alfabetic"));
}
$calif_minima = (float)$examen["calificacion_min"];
$calif_total = (float)$examen["calificacion_total"];
#var_dump($this->resultado);
#var_dump($examen);
?>
<input type="hidden" name="hIdExamen" id="hIdExamen" value="<?php echo @$examen['idexamen']; ?>">
<?php /*Datos de Tarea_Archivo, si es que hubiera*/ ?>
<input type="hidden" name="hIdTareaArchivo" id="hIdTareaArchivo" value="<?php echo @$this->idTareaArchivo; ?>">
<input type="hidden" name="hIdArchivoRespuesta" id="hIdArchivoRespuesta" value="<?php echo (@$this->tarea_archivo['tablapadre']=='R')?@$this->tarea_archivo['idtarea_archivos']:''; ?>">
<input type="hidden" name="hIdTareaRespuesta" id="hIdTareaRespuesta" value="<?php echo @$this->tarea_respuesta['idtarea_respuesta']; ?>">
<input type="hidden" name="hNroIntento" id="hNroIntento" value="<?php echo @$this->nro_intento; ?>">

<style type="text/css">
    #examen_preview{
        margin:  0;
    }
    #examen_preview .encabezado{
        border-radius: 7px;
        box-shadow: 0px 7px 6px -5px #aaa;
        margin-bottom: 3ex;
    }
    #examen_preview .pnlpresentacion {
        padding: 0 20px;
    }
    #examen_preview #portada{
        border-radius: 50%;
        min-height: 18.277em;
    }
    
    #examen_preview .title_desc-zone{
        padding: 6% 0;
    }

    #examen_preview .portada-zone,.title_desc-zone{
        display: inline-block;
        padding: 1% 0;
    }

    #examen_preview .panel{
        margin-bottom: 0;
    }

    .pnlitem , .pnlitem .tmpl-ejerc{
        /*display: none;*/
    }
    .pnlitem.active,.pnlitem .tmpl-media, .pnlitem .tmpl-ejerc.active{
        display: block;        
    }   
    .panel-body{
    	min-height: 390px;
        position: relative;
    }
    #pregunta-timer{
        position: absolute;
        right: 12px;
        z-index: 100;
    }
    #pregunta-timer .timer{
        background: #7ac1a1;
        border-radius: 50%;
        box-shadow: 0px 1px 10px -2px #444;
        display: inline-block;
        height: 80px;
        width: 80px;
    }
    #pregunta-timer .time-elapsed{
        color:  #fff;
        font-weight: bolder;
        margin-top: -0.7em;
        position: absolute;
        text-align: center;
        top: 50%;
        width: 100%;
    }

    #examen_preview .pnlpregunta .tmpl-ejerc.tiempo-acabo:before {
        background: rgba(255,255,255,0);
        content: ' ';
        height: 100%;
        position: absolute;
        width: 100%;
        z-index: 10;
    }
    #ifrRespuestas {
		height: 100vh;
    	width: 100%;
    }
</style>
<div class="row" id="examen_preview">
	<div class="container">
		<div class="col-xs-12"><br>
			<div class="alert alert-info alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="bolder">Intentos agotados</h4>
				<p>Has agotado los intentos permitidos para este examen. A continuación se muestran tus resultados.</p>
			</div>
		</div>

		<div class="col-xs-12" id="panel-info">
			<div class="panel">
				<div class="panel-heading bg-blue">
					<div class="col-md-6">
						<div>
							<ol class="breadcrumb" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
								<li><a href="#" style="color:#fff"><?php echo JrTexto::_("Assessment"); ?></a></li>
							</ol>
						</div>
					</div>
					<div class="col-md-6">
						<span class="" id="curtimerexamen"></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-body">
					<div class="pnlpresentacion  pnlitem">
						<?php if(!empty($examen)){?>
						<div class="col-xs-12 col-sm-12 col-md-12 text-center bg-blue encabezado">
							<?php $col = 12;
							if($srcPortada!='') { $col = 8; ?>
								<div class="col-xs-12 col-sm-3 portada-zone">
									<img src="<?php echo $srcPortada; ?>" alt="img-portada" id="portada" class="img-responsive">
								</div>
							<?php } ?>
							<div class="col-xs-12 col-sm-<?php echo $col; ?> title_desc-zone">
								<h2><?php echo ucfirst($examen["titulo"]); ?></h2>
								<small><?php echo ucfirst($examen["descripcion"]); ?></small>
							</div>
						</div>
						<?php } ?>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="table-key-value">
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("Scored by")); ?></div>
									<div class="value"><?php echo ($examen["calificacion_por"]=='Q'?ucfirst(JrTexto::_('Question')):ucfirst(JrTexto::_('Assessment'))); ?></div>
								</div>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("type of score")); ?></div>
									<div class="value"><?php echo $calificacion_en; ?></div>
								</div>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("max. score")); ?></div>
									<div class="value"><?php echo $calif_total.$tipoEvaluacion; ?></div>
								</div>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("min. score to pass")); ?></div>
									<div class="value"><?php echo $calif_minima.$tipoEvaluacion; ?></div>
								</div>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("number of attempts")); ?></div>
									<div class="value"><?php echo $examen['nintento']; ?></div>
								</div>
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("Choose")); ?></div>
									<div class="value"><?php echo ucfirst($examen["calificacion"]=='M'?JrTexto::_("Best try"):JrTexto::_("Last try")); ?></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="table-key-value">
								<div class="table-kv-row">
									<div class="key"><?php echo ucfirst(JrTexto::_("objective").'s').' / '. ucfirst(JrTexto::_("skills")); ?></div>
									<div class="value">
										<ul class="list-unstyled_1">
											<?php $habilidades_todas = json_decode(@$examen['habilidades_todas'], true);
											if(!empty($habilidades_todas)) {
												foreach ($habilidades_todas as $hab) {
													echo '<li>'.ucfirst(JrTexto::_(  $hab['skill_name'] )).'</li>';
												} } else { ?>
												<div class="text-center"><?php echo JrTexto::_("Skills").' '.JrTexto::_("not found") ?></div>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 text-center">
							<h2><strong><?php echo ucfirst(JrTexto::_("Result")) ?></strong></h2>
						</div>                	
						<div class="col-xs-12 col-sm-12 col-md-12 text-center" id="infototalcalificacion">
							<div class="alert <?php echo (float)$this->resultado['puntaje']>=$calif_minima?'alert-success':'alert-danger'; ?> " role="alert"><?php echo $this->resultado['resultado']; ?></div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 text-center">
							<h3><strong><?php echo ucfirst(JrTexto::_("Results by skill")); ?></strong></h3>
							<div class="col-xs-12 col-sm-6 col-md-6">
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12" id="resultskill">
								<?php $i=-1; 
								if(!empty($habilidades_todas)) {
									foreach ($this->habilidades as $hab) {  $i++; 
										if($i>count($arrColoresHab)){ $i=0; } ?>
								<div class="col-xs-12 col-sm-6 padding-0 skill sk_<?php echo $hab["skill_id"] ?>">
									<div class="col-xs-3 col-sm-3 col-md-4 circulo" data-idskill="<?php echo $hab["skill_id"] ?>" data-value="0" data-texto=" " data-ccolorout="<?php echo $arrColoresHab[$i]; ?>"></div>
									<div class="col-xs-9 col-sm-9 col-md-8 text-left bolder nombre-skill"><br><?php echo JrTexto::_($hab["skill_name"]);?></div>
								</div>
								<?php } } else { ?>
								<div class="text-center"><?php echo JrTexto::_("Skills").' '.JrTexto::_("not found") ?></div>
								<?php } ?>
							</div>
						</div>
						<div class="col-xs-12 text-center">
							<button class="btn btn-lg btn-primary ver-respuestas"><?php echo JrTexto::_("See my answers") ?></button>
						</div>
					</div>
				</div>
			</div>
		</div>

	    <div class="col-xs-12 padding-0 hidden" id="panel-respuestas">
    		<button class="btn btn-default btn-volver"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Back") ?></button>
	    	<iframe src="<?php echo $this->documento->getUrlBase().'/reporte/resultado_alumno/?slug='.@$this->usuarioAct["proyecto"]["slug"].'&identificador='.@$this->usuarioAct["identificador"].'&idexamen='.@$this->examen["idexamen"]; ?>" frameborder="0" id="ifrRespuestas"></iframe>
	    </div>
	</div>
</div>

<script type="text/javascript">
var PUNTAJE_HABILIDAD = JSON.parse(<?php echo json_encode($this->resultado['puntajehabilidad'], true); ?>);
$(document).ready(function() {
	$('#resultskill .skill').find('.circulo').html('');
	$('#resultskill .skill').hide();
    $.each(PUNTAJE_HABILIDAD, function(key, value){
        var $skillobj=$('#resultskill .skill.sk_'+key);
        if($skillobj.length>0){
            $skillobj.show();
            var pocentaje = parseFloat(value);
            $skillobj.find('.circulo').attr('data-value', pocentaje.toFixed(0));
        } 
        $skillobj.find('.circulo').graficocircle();           		
    });

    $('.ver-respuestas').click(function(e) {
    	$('#panel-respuestas').removeClass('hidden');
    	$('#panel-info').addClass('hidden');
    	return false;
    });

    $('.btn-volver').click(function(e) {
    	$('#panel-info').removeClass('hidden');
    	$('#panel-respuestas').addClass('hidden');
    	return false;
    });
});
</script>