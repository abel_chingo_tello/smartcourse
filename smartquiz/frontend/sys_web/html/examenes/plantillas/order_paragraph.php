<?php 
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$html_edicion = $this->pregunta['ejercicio'];
$rutabase = $this->documento->getUrlBase();
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic()?>/libs/tinymce/tinymce.min.js"></script>

<div class="plantilla plantilla-ordenar ord_parrafo editando" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>" data-clone="#panelEjercicio">
    <input type="hidden" value="<?php echo $this->pregunta['dificultad']; ?>"  name="hPregDificultad" id="hPreguntaDificultad">
    <div id="panelEjercicio">
        <input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
        <div class="row lista-parrafo-edit nopreview" id="lista-parrafo-edit<?php echo $idgui; ?>">
            <div class="col-xs-12 parrafo-edit" id="parrafo_0">
                <div class="col-xs-12 col-sm-10 col-lg-11 zona-edicion">
                    <div class="col-xs-12 col-sm-11 inputs-parrafo">
                        <textarea name="" id="txt_parrafo_0"></textarea>
                        <div class="row check-fixed">
                            <label class="col-xs-8 col-sm-4 col-md-3"><?php echo ucfirst(JrTexto::_('fix')).' '.JrTexto::_('this paragraph'); ?>: </label>
                            <div class="col-xs-4 col-sm-8 col-md-9">
                                <input type="checkbox" class="checkbox-ctrl" name="chkFixed">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-1 btns-parrafo text-center">
                        <a href="#" class="btn btn-danger delete-parrafo" data-tooltip="tooltip" title="<?php echo JrTexto::_('discard').' '.JrTexto::_(''); ?>"><i class="fa fa-trash-o"></i></a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-lg-1 zona-btns-mover">
                    <a href="#" class="col-xs-6 col-sm-12 btn btn-default move-parrafo up" data-tooltip="tooltip" data-placement="top" title="<?php echo ucfirst(JrTexto::_('move up')); ?>"><i class="fa fa-chevron-up"></i></a>

                    <a href="#" class="col-xs-6 col-sm-12 btn btn-default move-parrafo down" data-tooltip="tooltip" data-placement="bottom" title="<?php echo ucfirst(JrTexto::_('move down')); ?>"><i class="fa fa-chevron-down"></i></a>
                </div>
            </div>
        </div>
        <div class="row botones nopreview">
            <div class="col-xs-12 text-center botones-creacion">
                <a href="#" class="btn btn-primary add-parrafo" data-clone-from="#clone_parrafo" data-clone-to=".lista-parrafo-edit"><i class="fa fa-plus-square"></i> <?php echo JrTexto::_('add').' '.JrTexto::_('paragraph'); ?></a>
            </div>
            <div class="col-xs-12 text-center botones-editar hidden" style="display: none !important;">
                <a href="#" class="btn btn-success back-edit-parr"><i class="fa fa-pencil"></i> <?php echo JrTexto::_('back').' '.JrTexto::_('and').' '.JrTexto::_('edit'); ?></a>
            </div>

            <div class="hidden col-xs-12 parrafo-edit" data-id="parrafo_" id="clone_parrafo">
                <div class="col-xs-12 col-sm-10 col-lg-11 zona-edicion">
                    <div class="col-xs-12 col-sm-11 inputs-parrafo">
                        <textarea class="renameId" data-id="txt_parrafo_" id=""></textarea>
                        <div class="row">
                            <label class="col-xs-6 col-sm-4 col-md-3"><?php echo ucfirst(JrTexto::_('fix')).' '.JrTexto::_('this paragraph'); ?>: </label>
                            <div class="col-xs-6 col-sm-8 col-md-9">
                                <input type="checkbox" class="checkbox-ctrl" name="chkFixed">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-1 btns-parrafo text-center">
                        <a href="#" class="btn btn-danger delete-parrafo" data-tooltip="tooltip" title="<?php echo JrTexto::_('discard').' '.JrTexto::_(''); ?>"><i class="fa fa-trash-o"></i></a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-lg-1 zona-btns-mover">
                    <a href="#" class="col-xs-6 col-sm-12 btn btn-default move-parrafo up" data-tooltip="tooltip" data-placement="top" title="<?php echo ucfirst(JrTexto::_('move up')); ?>"><i class="fa fa-chevron-up"></i></a>

                    <a href="#" class="col-xs-6 col-sm-12 btn btn-default move-parrafo down" data-tooltip="tooltip" data-placement="bottom" title="<?php echo ucfirst(JrTexto::_('move down')); ?>"><i class="fa fa-chevron-down"></i></a>
                </div>
            </div>
        </div>
        <div class="row ejerc-ordenar tpl_plantilla" id="ejerc-ordenar<?php echo $idgui; ?>" style="display: none;">
            <div class="col-xs-12 col-sm-6 drop-parr">

            </div>
            <div class="col-xs-12 col-sm-6 drag-parr">

            </div>
        </div>
        
        <audio src="" class="hidden" id="audio-ordenar<?php echo $idgui; ?>" style="display: none;"></audio>
    </div>

     <a href="#" id="generarhtml" class="btn btn-success generar-parrafo hidden" data-clone-from="" data-clone-to=".ejerc-ordenar.tpl_plantilla"><i class="fa fa-rocket" style="display: none !important;"></i> <?php echo JrTexto::_('finish').' '.JrTexto::_('and').' '.JrTexto::_('generate').' '.JrTexto::_(''); ?></a>
</div>
<section id="sectionPreCarga" class="hidden" style="display: none !important;">
<?php 
if($html_edicion!=''){ 
    echo $html_edicion;
} ?>
</section>

<script type="text/javascript">
$(document).ready(function() {

    var $plantilla = $('#tmp_<?php echo $idgui; ?>');
    var initEditor=function(idTextArea, LANG=''){
        tinymce.init({
            relative_urls : false,
            convert_newlines_to_brs : true,
            menubar: false,
            statusbar: false,
            verify_html : false,
            content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
            selector: '#tmp_<?php echo $idgui; ?> #'+idTextArea,
            height: 100,
            plugins:[""], 
            toolbar: 'undo redo | styleselect | removeformat | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent '

        });
    };

    var saveEditor = function(idTextArea){
        tinyMCE.triggerSave();
        var texto=$('#tmp_<?php echo $idgui; ?>  #'+idTextArea).val();
        $('#tmp_<?php echo $idgui; ?>  #'+idTextArea).html(texto);
        $('#tmp_<?php echo $idgui; ?>  #'+idTextArea).show();
        tinymce.remove('#tmp_<?php echo $idgui; ?>  #'+idTextArea);
    };

    var agregarRandom = function(strHtml, $aDonde, index){
        var $parteTexto = $aDonde.children();
        if( $parteTexto.length == 0 ){
            $aDonde.html(strHtml);
        } else {
            var posic = Math.floor(Math.random()*$parteTexto.length);
            while( posic == index ){
                posic = Math.floor(Math.random()*$parteTexto.length);
            }
            var aleat = ($parteTexto.length == 1) ? 2 : Math.floor(Math.random()*10);
            if(aleat%2 == 0) { $parteTexto.eq(posic).before(strHtml); }
            else { $parteTexto.eq(posic).after(strHtml); }
        }
    };

    var generarParrafo = function(idCloneFrom, idCloneTo, IDGUI=''){
        var $listParrafoEdit = $('.lista-parrafo-edit', $plantilla);
        var parrafoEdit = '.parrafo-edit';

        $(idCloneTo+' .drop-parr', $plantilla).html('');
        $(idCloneTo+' .drag-parr', $plantilla).html('');

        $listParrafoEdit.find(parrafoEdit).each(function() {
            var idTextArea = $(this).find('textarea').attr('id');
            saveEditor(idTextArea);
            var estaFijo = $(this).find('input[type="checkbox"]').is(':checked');
            if(estaFijo){ $(this).find('input[type="checkbox"]').attr('checked', 'checked'); }
            var texto = $('#'+idTextArea, $plantilla).val();
            var idParr = (idTextArea!='' && idTextArea!=undefined)? idTextArea.split('_')[idTextArea.split('_').length-1] : '';
            var index = $listParrafoEdit.find(parrafoEdit).index( $(this) );

            if( estaFijo ){
                $(idCloneTo+' .drop-parr', $plantilla).append('<div class="fixed">'+texto+'</div>');
            } else {
                $(idCloneTo+' .drop-parr', $plantilla).append('<div></div>');

                agregarRandom('<div id="'+idParr+'" data-orden="'+(index+1)+'">'+texto+'</div>', $(idCloneTo+' .drag-parr', $plantilla), (index+1));
            }
        });
    };

    var iniciarTodosEditores = function($contenedor, LANG=''){
        $contenedor.find('textarea').each(function() {
            var idTextArea = $(this).attr('id');
            /*console.log($('#tmp_<?php echo $idgui; ?> #'+idTextArea));*/
            initEditor(idTextArea , LANG);
        });
    };

    $('.plantilla-ordenar')
    .on('click', '.delete-parrafo', function(e) {
        if(!$plantilla.hasClass('editando')) return false;
        e.preventDefault();
        $(this).closest('.parrafo-edit').remove();
    })
    .on('click', '.move-parrafo', function(e) {
        e.preventDefault();
        var idElemEdit = $(this).closest('.parrafo-edit').attr('id');
        var $listaElemEdit = $('.lista-parrafo-edit', $plantilla);
        var $parrafoEdit = $('#'+idElemEdit, $plantilla);
        var idTextArea = $parrafoEdit.find('textarea').attr('id');

        if( $listaElemEdit.children('.parrafo-edit').length>1 ){
            /*console.log($parrafoEdit);*/
            saveEditor( idTextArea );
            if( $(this).hasClass('up') && $parrafoEdit.prev().length!=0 ){
                var $parrafoPrev = $parrafoEdit.prev();
                $parrafoEdit.insertBefore($parrafoPrev);
            }

            if( $(this).hasClass('down') && $parrafoEdit.next().length!=0 ){
                var $parrafoNext = $parrafoEdit.next();
                $parrafoEdit.insertAfter($parrafoNext);
            }
            initEditor( idTextArea );
        }
    })
    .on('click', '.list-parrafo-part>div', function(e) {
        $(this).toggleClass('fixed');
    })
    .on('click', '.add-parrafo', function(e) {
        e.preventDefault();
        var cloneFrom = $(this).attr('data-clone-from');
        var cloneTo = $(this).attr('data-clone-to');
        var newFicha = $(cloneFrom, $plantilla).clone();
        var now = Date.now();

        newFicha.find('.renameId').each(function() {
            var id = $(this).data('id');
            $(this).attr('id', id+now);
            $(this).removeClass('renameId').removeAttr('data-id');
        });

        newFicha.find('.renameClass').each(function() {
            var clase = $(this).data('clase');
            $(this).addClass(clase+now);
            $(this).removeClass('renameClass').removeAttr('data-clase');
        });

        newFicha.find('.renameDataUrl').each(function() {
            var url = $(this).data('url');
            $(this).removeAttr('data-url').attr('data-url', url+now);
            $(this).removeClass('renameDataUrl');
        });

        var new_IdParrafo = newFicha.data('id')+now;
        newFicha.removeAttr('id');
        newFicha.attr( 'id', new_IdParrafo ).removeAttr('data-id');

        $(cloneTo, $plantilla).append(newFicha);
        $("*[data-tooltip=\"tooltip\"]").tooltip();

        initEditor( 'txt_parrafo_'+now );
        $('#'+new_IdParrafo).removeClass('hidden');
    })
    .on('click', '.generar-parrafo', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $plantilla.removeClass('editando');
        var idCloneFrom = $(this).data('clone-from');
        var idCloneTo = $(this).data('clone-to');
        var result = generarParrafo(idCloneFrom, idCloneTo);
        $('.ejerc-ordenar', $plantilla).show();
        $('.lista-parrafo-edit', $plantilla).hide().addClass('hidden');
        $plantilla.find('.botones-editar').show();
        $plantilla.find('.botones-creacion').hide().addClass('hidden');
        $plantilla.find('.nopreview').hide();
    })
    .on('click', '.back-edit-parr', function(e) {
        e.preventDefault();
        $('.ejerc-ordenar', $plantilla).hide();
        iniciarTodosEditores( $('.lista-parrafo-edit', $plantilla) );
        $('.lista-parrafo-edit', $plantilla).show().removeClass('hidden');
        $plantilla.find('.botones-editar').hide();
        $plantilla.find('.botones-creacion').show().removeClass('hidden');
        $('.lista-parrafo-edit', $plantilla).find('input[type="checkbox"]').each(function() {
            var isChecked = $(this).attr('checked');
            if(isChecked!=undefined){
                $(this).removeAttr('checked');
                $(this).prop('checked', true);
            }
        });
         $plantilla.find('.nopreview').show();
    });


    var existeHtml = <?php echo ($html_edicion!='')? 'true':'false'  ?>;
    if(existeHtml){
        var precarga = $('#sectionPreCarga').clone();
        var html = precarga.find('.ejercicio').html();

        $plantilla.find('#panelEjercicio').html(html); /*evita recargar el titulo y desrcip infinitas veces*/

        $('.btn.back-edit-parr', $plantilla).trigger('click');
        $('#sectionPreCarga').remove();
    } else {
        iniciarTodosEditores($('.lista-parrafo-edit', $plantilla));
    }
});
</script>