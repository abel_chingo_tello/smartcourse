<?php 
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic()?>/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic()?>/libs/tinymce/plugins/chingo/exa_saveedit.js"></script>
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic()?>/libs/tinymce/plugins/chingo/exa_inputadd.js"></script>
<div class="plantilla plantilla-completar editando" data-addclass="" data-idgui="<?php echo $idgui; ?>" data-clone="#panelEjercicio">
    <input type="hidden" value="<?php echo $this->pregunta['dificultad']; ?>"  name="hPregDificultad" id="hPreguntaDificultad">
    <div class="row">        
        <div class="col-md-8 col-sm-8 col-xs-12" style="padding:0.5ex" >
            <div id="panelEjercicio" style="display: none"></div>
            <label><?php echo ucfirst(JrTexto::_("Exercise")); ?>:</label>
            <textarea class="form-control gris" id="txtarea<?php echo $idgui; ?>"></textarea>             
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="panel inactive" id="setting-textbox"  >
                <div class="panel-heading bg-blue btn-primary">
                    <div style="font: bold; padding: 1ex;">
                        <i class="fa fa-edit"></i>
                        <span><?php echo ucfirst(JrTexto::_('Edit textbox')); ?></span>
                    </div>
                </div>
                <div class="panel-body">
                    <ol class="row">
                        <li class="col-xs-12 correct-ans" style="font: bold; padding: 1ex;">
                            <span class="item-title"><b><?php echo JrTexto::_('Correct answer'); ?></b></span>
                            <input type="text" class="form-control">
                        </li>
                        <li class="col-xs-12 distractions" style="font: bold; padding: 1ex;">
                            <span class="item-title" >
                                <b><?php echo JrTexto::_('Distractors'); ?></b>
                                <a href="#" class="btn btn-primary btn-xs pull-right add-distraction"> 
                                    <i class="fa fa-plus"></i>
                                    <?php echo JrTexto::_('add'); ?>
                                </a>
                            </span>
                            <input type="text" class="form-control">
                            <input type="text" class="form-control">
                            <input type="text" class="form-control">
                        </li>
                        <li class="col-xs-12 assisted" style="font: bold; padding: 1ex; display:none;">
                            <span class="item-title" ><b><?php echo JrTexto::_('Assisted exercise'); ?></b></span>
                            <input type="checkbox" class="isayuda checkbox-ctrl">
                        </li>
                    </ol>
                    <a href="#" class="btn btn-primary center-block save-setting-textbox" >
                        <i class="fa fa-floppy-o"></i> 
                        <?php echo ucfirst(JrTexto::_('save changes')); ?>
                    </a>
                    <a href="#" class="btn btn-primary " id="activaredicioninput"  style="display: none;" >
                    </a>
                </div>
            </div>
        </div> 
    </div>
    <button id="generarhtml" style="display:none"></button>
</div>
<section class="hidden" id="ejercicio-edit-precarga">
    <?php echo trim($this->pregunta["ejercicio"]);?>
</section>
<script type="text/javascript">
var jsonmultiplantilla={};
var idgui='<?php echo $idgui; ?>';
var alternativas_dataOptions = function(selectorTxt){
    $(selectorTxt+'_ifr').contents().find("img.mce-object-input").each(function(i, elem) {
        var idInput = $(elem).attr('id');
        var dataOptions = jsonmultiplantilla[idInput].al;
        $(elem).attr('data-options', JSON.stringify(dataOptions) );
    });
};

var mostrarEditorMCE  = function(obj,showtoolstiny){
  var showtools=showtoolstiny||'';
  tinymce.init({
    relative_urls : false,
    convert_newlines_to_brs : true,
    menubar: false,
    statusbar: false,
    verify_html : false,
    content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
    selector: obj,
    height: 200,
    paste_auto_cleanup_on_paste : true,
    paste_preprocess : function(pl, o) {
        var html='<div>'+o.content+'</div>';
        var txt =$(html).text(); 
        o.content = txt;
    },paste_postprocess : function(pl, o) {       
        o.node.innerHTML = o.node.innerHTML;
        o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
    },
    plugins:[showtools+" exasave textcolor paste inputadd table" ],  //chingosave chingoinput chingoimage chingoaudio chingovideo styleselect
    toolbar: 'exasave | undo redo | removeformat | table | bold italic underline | alignleft aligncenter alignright alignjustify | numlist |  forecolor backcolor | inputadd '+showtools, // chingosave chingoinput chingoimage chingoaudio chingovideo 
    setup: function (editor) {
        editor.on('init', function () {
            /*console.log('tinymce is ready');*/
            alternativas_dataOptions(obj);
        });
    }
  });
};

var mostraropcioneseditable=function(obj,distractions,asistido){
    if(distractions==false)$('#setting-textbox .distractions').hide();
    else{
        $('#setting-textbox .distractions input').remove();
             var iopt=0;
             try{
                var options=jsonmultiplantilla[obj.attr('id')];
                options=options||{al:[],ok:$(obj).attr('data-texto').toString()}; 
                var txt=options.ok||$(obj).attr('data-texto').toString();
                var text_unescaped = txt.replace(/''/gi, '"');
                $('#setting-textbox .correct-ans input').val(text_unescaped.trim());
                if(options.al.length>0){                           
                    $.each(options.al,function(i,v){
                        iopt++;
                        var vinput=document.createElement("input");
                        vinput.setAttribute('class','form-control');
                        vinput.setAttribute('type','text');
                        vinput.setAttribute('value',v);
                        vinput.value=v;
                        $('#setting-textbox .distractions').append(vinput);
                    });
                }
             }catch(ex){} 
             for(var i = iopt; i <3 ; i++) {
                $('#setting-textbox .distractions').append('<input value="" type="text" class="form-control">'); 
             }
             $('#setting-textbox .distractions').show();
    }
    if(asistido==false) $('.asistido').hide();
    else {
        $('#setting-textbox .assisted').show();
        $('#setting-textbox .assisted input.isayuda').prop("checked", obj.hasClass('isayuda')?true:false);
        $('.asistido').show();
    }
    obj.addClass('_enedicion_');
    $('#setting-textbox .correct-ans input').focus();
};

var estaRepetido = function(html, $input) {
    var repetido = false;
    var idInput = $input.attr('id');
    $(html).find('img.mce-object-input').not($input).each(function(i, el) {
        let id = $(el).attr('id');
        if(id == idInput){
            repetido = true;
            return false ;
        }
    });
    return repetido;
};

var saveTextboxEdit=function(){
    var infotemplate=$('.plantilla-completar.editando');
    var response = {
        'asistido'      : $('#setting-textbox .assisted input').is(':checked'),
        'correcta'      : $('#setting-textbox .correct-ans input').val().replace(/"/gi, "''"),
        'distraccion'   : [],
    };
    console.log(response);
    $('#setting-textbox .distractions input').each(function() {
        if( $(this).val().trim()!='' ) response['distraccion'].push( $(this).val().trim() );
    });
    tinyMCE.triggerSave();        
    var acteditor=tinyMCE.activeEditor.getBody();
    var actobj=$(acteditor).find('._enedicion_');
    if(actobj.length>0){        
        var addclassinput=infotemplate.attr('data-addclass')||'';
        actobj.attr('data-texto',response.correcta);
        console.log('dataTexto->' , actobj.attr('data-texto') );
        actobj.attr('data-mce-selected','1');
        actobj.removeClass('_enedicion_ isdrop isclicked iswrite isayuda ischoice');
        actobj.addClass(addclassinput);           
        if(response.asistido) actobj.addClass('isayuda');
        if(response.distraccion){
            var options=JSON.stringify(response.distraccion);
            /*console.log('actobj=',actobj);
            console.log('options=',options);*/
            actobj.attr('data-options',options);
            var idtmp=actobj.attr('id');
            if( typeof idtmp=='undefined' || idtmp=='' || estaRepetido(acteditor, actobj) ){
                idtmp = 'id'+Date.now();
            }
            actobj.attr('id',idtmp);
            jsonmultiplantilla[idtmp]={
                "al":response.distraccion,
                "ok":response.correcta //.replace(/"/gi, "''")
            };
        }
        var html=actobj.prop('outerHTML');
        $(actobj).replaceWith(html);
    }
    $('#setting-textbox').find('input.form-control').val('');
    $('#setting-textbox input.added').remove();
    $('#setting-textbox input[type="checkbox"]').prop( "checked", false );
};

var iniciarcontroles=function(){
    var haypregunta='<?php echo !empty($this->pregunta)?"si":"no" ?>';
    if(haypregunta==='si'){
        var txtinicio=$('#ejercicio-edit-precarga').find('.panelEjercicio');
        $(txtinicio).find('input.mce-object-input').each(function(i,v){
            var idtmp=$(v).attr('id');
            if( typeof idtmp=='undefined' || idtmp=='' || estaRepetido(txtinicio, $(v)) ) {
                idtmp = 'id'+Date.now()+i;
            }
            $(v).attr('id',idtmp);
            var optionstmp=$(v).attr('data-options')||'';
            optionstmp=_isJson(optionstmp)?JSON.parse(optionstmp):optionstmp.split(',');
            console.log( 'initPLantilla. txt->', $(v).attr('data-texto') );
            jsonmultiplantilla[idtmp]={
                "al":optionstmp,
                "ok":$(v).attr('data-texto').replace(/"/gi, "''")||''
            };
            $(v).removeAttr('data-options');
        });
        $('#txtarea'+idgui).html(txtinicio.html());
    }
};

$(document).ready(function(){
    var txtarea='#txtarea'+idgui;    
    iniciarcontroles();
    mostrarEditorMCE(txtarea);
    var editable=$('#setting-textbox').hasClass('inactive')?false:true;
    var edit=$('.plantilla-completar.editando').attr('data-addclass');
    if(edit=='iswrite'){
         $('#setting-textbox .distractions').hide();
    }

    $('#activaredicioninput').click(function(ev){
        var acteditor=tinyMCE.activeEditor.getBody();
        $('#setting-textbox').removeClass('inactive');
         var obj=$(acteditor).find('._enedicion_');//input en edicion
         var asistido=false;
         var alternativas=$(this).closest('.plantilla-completar').attr('data-addclass')=='iswrite'?false:true;
         if($(obj).hasClass('mce-object-input')){
            obj=$(obj);
            var txt=obj.attr('data-texto').toString();
            var text_unescaped = txt.replace(/''/gi, '"');
            $('#setting-textbox .correct-ans input').val(text_unescaped.trim());
            mostraropcioneseditable(obj,alternativas,asistido);
         }                
    });

    $('.save-setting-textbox').click(function(ev){
        ev.preventDefault();
         if(editable==true){
            ev.preventDefault();
            ev.isImmediatePropagationStopped();
            return false;
         }
         saveTextboxEdit();
    });

    $('.add-distraction').click(function(){
        if(editable==true){
            ev.preventDefault();
            ev.isImmediatePropagationStopped();
            return false;
        }
        $('.distractions').append('<input type="text" class="form-control">');
    });

    var mezclarOpciones = function(arrInputOptions, cantInputs){
        var array_Alts = [];
        var cont_aux = 0;
        var cantidad_alternativas = 3; /*se considera de la a)-d) descontando la correcta*/
        for (var x = 0; x < cantidad_alternativas; x++) {
            if(cont_aux==15){ console.log('Bucle interrumpido'); break; }
            var new_alternativa = nuevaAlternativa(arrInputOptions, cantInputs);
            if(array_Alts.length>0){
                if( $.inArray(new_alternativa, array_Alts)>-1 ){
                    x--;
                } else {
                    array_Alts.push(new_alternativa);
                }
            } else {
                array_Alts.push(new_alternativa);
            }
            cont_aux++;
        }
        return array_Alts;
    };

    $('.plantilla-completar[data-idgui="<?php echo $idgui; ?>"]').on('click','#generarhtml',function(ev){

        var plantilla= $(this).closest('.plantilla');
        plantilla.removeClass('editando');
        var $panelEjercicios=$('#panelEjercicio',plantilla);
        tinyMCE.triggerSave();
        var txt=$(txtarea).val();
        var div='<div class="panelEjercicio img-thumbnail panel pnl100">'+txt+'</div>';
        var alter='';
         tinyMCE.remove();
        $panelEjercicios.html(div);


        //console.log('al momento de generar JSONMULTIPLANTILLA->', jsonmultiplantilla);
        $panelEjercicios.find('input.mce-object-input').each(function(i, input) {
            var id = $(input).attr('id');
            var correcta = jsonmultiplantilla[id]['ok'];
            $(input).attr('data-texto', correcta);
        });

        var inpdrag=$panelEjercicios.find('input.isdrop[data-mce-object="input"]');
        var inpClicked=$panelEjercicios.find('input.isclicked[data-mce-object="input"]');
        if(inpdrag.length>0){
            var spanalt=[];
            inpdrag.each(function(){
                var optionstmp=jsonmultiplantilla[$(this).attr('id')];
                optionstmp=optionstmp||{al:[],ok:[]};
                var optDistrac=optionstmp.al||[];
                var optCorrect=optionstmp.ok||'';

                if(inpdrag.length>1){ //draggable="true"  ondragend="sysdragend(this,event)"
                  spanalt.push('<span class="isdragable" >'+optCorrect+'</span>');
                }else{
                  var opt=optDistrac;
                  if(opt!='' && opt!=undefined){
                    spanalt.push('<span class="isdragable">'+optCorrect+'</span>');
                    $.each(opt,function(index,value){
                      spanalt.push('<span class="isdragable">'+value+'</span>');
                    });
                  }
                }
          });
          spanalt.sort(function(){return Math.random() - 0.5});
          alter=spanalt.toString();
        }else if(inpClicked.length>0){
            var opc_corr = '';
            var arr_opc_corr = [];
            var arr_InputOptions = [];
            var arr_options2 = [];
            inpClicked.each(function() {
                var optionstmp=jsonmultiplantilla[$(this).attr('id')];
                optionstmp=optionstmp||{al:[],ok:[]};
                var optDistrac=optionstmp.al||[];
                var optCorrect=optionstmp.ok||'';

                arr_opc_corr.push(optCorrect);

                var arr_options = [];
                arr_options2 = optDistrac;     
                var options = optDistrac;
                if(options.length>0){
                    arr_InputOptions.push(options);
                }
            });
            var rspta_correcta = arr_opc_corr.join(' - ');
            if(inpClicked.length==1) {  var arr_alternativas = arr_options2; }
            else if(arr_InputOptions.length>0){var arr_alternativas = mezclarOpciones(arr_InputOptions,inpClicked.length);}
            var list=[];
            list.push('<a href="#">'+rspta_correcta+'</a>');
            $.each(arr_alternativas, function(key, val) {
              list.push('<a href="#">'+val.trim()+'</a>');
            });
            list.sort(function(){return Math.random() - 0.5});
            var ascii = 97;
            var htmlalter='';
            $.each(list, function(i,v){
              htmlalter+='<li class="alt-item"><span class="alt-letra">&#'+ascii+';) </span>'+v+'</li>';
              ascii++;
            });
            alter = '<ul class="alternativas-list">'+ htmlalter + '</ul>';
        }
        if(alter!=''){
            var divalter='<div class="panelAlternativas img-thumbnail panel pnl100">'+alter+'</div>';
            $panelEjercicios.html(div+divalter);
        }else $panelEjercicios.html(div);   
    });
});
</script>