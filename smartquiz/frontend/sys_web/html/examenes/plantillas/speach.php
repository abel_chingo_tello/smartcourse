<?php
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$html_edicion = $this->pregunta['ejercicio'];
if(!empty($html_edicion)) $html_edicion=str_replace('__xRUTABASEx__/',$this->urlBase,$html_edicion);
$rutabase = $this->documento->getUrlBase();
?>
<style type="text/css">
  .pnl-speach.docente .txtalu1edit{
      height: 31px;
      border: 1px solid #ccc;
      display: block;
  }
</style>
<div class="plantilla plantilla-speach" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>" data-tipo-tmp="speach" data-clone="#tmp_<?php echo $idgui; ?>">
  <input type="hidden" value="<?php echo $this->pregunta['dificultad']; ?>"  name="hPregDificultad" id="hPreguntaDificultad">
  <div class="row">
    <div class="col-xs-12 col-md-12 col-md-12 text-center">

      <div class="pnl-speach docente" id="pnl1<?php echo $idgui; ?>" data-idgui="<?php echo $idgui; ?>">
        <div>
          <a class="btn btn-sm btn-danger btnGrabarAudio btngrabaraudioDoc"> <i class="fa fa-microphone-slash"></i> <span></span></a> 
          <a class="btn btn-sm btn-danger btnadd_audio "> <i class="fa fa-file-audio-o"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('audio');?></a>
          <a class="btn btn-sm btn-primary btnPlaytexto hidden"> <i class="fa fa-play"></i> <span></span></a>
        </div>
        <div class="form-group" idioma="EN">
          <div class="text-center">
            <span class="edittexto_ch ingles txtalu1"></span>
            <i class="pronunciar icon1 fa fa-volume-up hidden"></i>
          </div> 
        </div>
        <div class="grafico"></div>       
      </div>
      <br>
      <hr>
      <div class="textohablado" id="txtspeach<?php echo $idgui; ?>"></div>
      <br>
      <div class="pnl-speach alumno" id="pnl2<?php echo $idgui; ?>"  data-idgui="<?php echo $idgui; ?>">
        <div class="grafico"></div>
        <div class="form-group" idioma="EN">
          <div class="text-center">
            <span class="edittexto_ch ingles txtalu2"></span>
            <i class="pronunciar icon1 fa fa-volume-up hidden"></i>
          </div> 
        </div>
        <div> 
          <a class="btn btn-sm btn-danger btnGrabarAudio hidden"> <i class="fa fa-microphone-slash"></i> <span></span></a> 
          <a class="btn btn-sm btn-primary btnPlaytexto hidden"> <i class="fa fa-play"></i> <span></span></a>
        </div>        
      </div>
    </div>
  </div>
  <button id="generarhtml" style="display: none;"></button>
</div>
<textarea style="display:none;" class="tmp_speach<?php echo $idgui ?>" name="texto"></textarea>
<section id="sectionPreCarga<?php echo $idgui; ?>" class="hidden" style="display: none !important;">
<?php if($html_edicion!=''){  echo $html_edicion;} ?>
</section>

<script>
$(document).ready(function(){
    $('#tmp_<?php echo $idgui; ?>').on('click','.btngrabaraudioDoc',function(ev){
      var btn=$(this);
      var pnl=btn.closest('.pnl-speach')
      var idgui=pnl.attr('data-idgui');
      btn.toggleClass('btn-danger');
      if(!btn.hasClass('btn-danger')){
        btn.addClass('btn-success').children('i').removeClass('fa fa-microphone-slash ').addClass('fa fa-microphone animated infinite zoomIn');
        btn.siblings('.btnPlaytexto').addClass('disabled').attr('disabled',true);
        speach_iniciargrabacion(pnl,idgui);
      }else{
        btn.addClass('btn-danger').removeClass('btn-success').children('i').removeClass('fa fa-microphone animated infinite zoomIn').addClass('fa fa-microphone-slash');
        speach_detenergrabacion(pnl,idgui);
        btn.siblings('.btnPlaytexto').removeClass('disabled').attr('disabled',false).removeClass('hidden');
      }
    }).on('click','#generarhtml',function(){
      var plantilla=$(this).closest('.plantilla');
       plantilla.find('.pnl-speach.docente .btnadd_audio').addClass('hidden');
      plantilla.find('.pnl-speach.docente .btnGrabarAudio').addClass('hidden');
      plantilla.find('.pnl-speach.docente .txtalu1').removeClass('edittexto_ch txtalu1edit');
      plantilla.find('.pnl-speach.alumno .btnGrabarAudio').removeClass('hidden');
    })

     $('#tmp_<?php echo $idgui; ?>').find('.txtalu1').addClass('edittexto_ch txtalu1edit');

    var initplantilla=function(){
      var $speachdoc=$('#sectionPreCarga<?php echo $idgui; ?>').find('.pnl-speach.docente');
       if($speachdoc.length>0){
        $speachdoc.find('.btngrabaraudioDoc').removeClass('hidden').addClass('btn-danger').removeClass('btn-success').children('i').removeClass('fa fa-microphone animated infinite zoomIn').addClass('fa fa-microphone-slash');
        $speachdoc.find('.btnPlaytexto').removeClass('hidden');
        $tpldocente=$('#tmp_<?php echo $idgui; ?>').find('.pnl-speach.docente');
        $tpldocente.html($speachdoc.html());  
         $tpldocente.find('.txtalu1').addClass('edittexto_ch txtalu1edit');   
        var url=$tpldocente.find('audio').attr('src');
        var donde=$tpldocente.find('.grafico');
        donde.html(''); 
        
        if($('#tmp_<?php echo $idgui; ?>').closest('.zona-plantilla-edicion').length>0){
            $('#tmp_<?php echo $idgui; ?>').find('.btnadd_audio').removeClass('hidden'); 
          }
        setTimeout(function(){
          crearonda(url,$tpldocente,true);
          
        },1000)        
        $speachdoc.remove();
      }
    }
    initplantilla();
})
</script>