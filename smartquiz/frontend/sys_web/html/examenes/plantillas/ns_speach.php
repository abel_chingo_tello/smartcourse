<?php
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$html_edicion = $this->pregunta['ejercicio'];
if(!empty($html_edicion)) $html_edicion=str_replace('__xRUTABASEx__',$this->urlBase,$html_edicion);
$rutabase = $this->documento->getUrlBase();
?>
<div class="plantilla plantilla-nlsw" id="tmp_nlsw<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>" data-tipo-tmp="nlsspeach" data-clone="#tmp_nlsw<?php echo $idgui; ?>">
    <input type="hidden" value="<?php echo $this->pregunta['dificultad']; ?>"  name="hPregDificultad" id="hPreguntaDificultad">
    <div class="row nopreview text-center">
        <a href="#" class="btn btn-primary btnadd_record" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-microphone-slash"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('record');?></a>
        <a href="#" class="btn btn-primary btnadd_audio" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-file-audio-o"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('audio');?></a>
        <a href="#" class="btn btn-primary btnadd_video" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-file-video-o"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('video'); ?></a>
        <a href="#" class="btn btn-primary btnadd_image" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-file-picture-o"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('image'); ?></a>
        <a href="#" class="btn btn-primary btnadd_texto" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-text-width"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('text'); ?></a>
    </div>
    <div class="row preg-docns" id="preguntasDocente<?php echo $idgui; ?>">
        
    </div>
    <hr>
    <div class="row respuestaAlumno ">
        <label class="showonpreview_nlsw hidden"><?php echo ucfirst(JrTexto::_("Student's answer")); ?></label>
        <!--span class="agregarrespuesta nopreview">
            <a href="#" class="btn btn-xs btn-warning btnadd_textorespuesta" data-idgui="<?php echo $idgui; ?>">
                <i class="fa fa-sign-in"></i> <?php //echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('Answer'); ?></a>
        </span-->
        <div class="msjRespuesta grabarfile" data-tipo="audio"></div>      
        <div class="col-md-12 text-center showonpreview_nlsw hidden">
            <a href="#" class="btn btn-danger btnadd_recordAlumno" data-idgui="<?php echo $idgui; ?>"><i class="fa fa-microphone-slash"></i> <?php echo ucfirst(JrTexto::_('Record')); ?> </a>
            <!--a href="#" class="btn btn-primary btnsend_data"><?php //echo ucfirst(JrTexto::_('Send')); ?> <i class="fa fa-envelope-o"></i></a-->
        </div>
    </div>
    <button id="generarhtml" style="display: none;"></button>
</div>
<textarea style="display:none;" class="tmp_speach<?php echo $idgui ?>" name="texto"></textarea>
<section id="sectionPreCarga<?php echo $idgui; ?>" class="hidden" style="display: none !important;">
<?php if($html_edicion!=''){  echo $html_edicion;} ?>
</section>
<script>
$(document).ready(function(){
    $('#tmp_nlsw<?php echo $idgui; ?>').on('click','.btngrabaraudioDoc',function(ev){
      var btn=$(this);
      var pnl=btn.closest('.pnl-speach')
      var idgui=pnl.attr('data-idgui');
      btn.toggleClass('btn-danger');
      if(!btn.hasClass('btn-danger')){
        btn.addClass('btn-success').children('i').removeClass('fa fa-microphone-slash ').addClass('fa fa-microphone animated infinite zoomIn');
        btn.siblings('.btnPlaytexto').addClass('disabled').attr('disabled',true);
        speach_iniciargrabacion(pnl,idgui);
      }else{
        btn.addClass('btn-danger').removeClass('btn-success').children('i').removeClass('fa fa-microphone animated infinite zoomIn').addClass('fa fa-microphone-slash');
        speach_detenergrabacion(pnl,idgui);
        btn.siblings('.btnPlaytexto').removeClass('disabled').attr('disabled',false).removeClass('hidden');
      }
    }).on('click','#generarhtml',function(){
      var plantilla=$(this).closest('.plantilla');
      plantilla.find('.nopreview').addClass('hidden');
      plantilla.find('.showonpreview_nlsw').removeClass('hidden');
    })    
    var initplantilla=function(){
      var $speachdoc=$('#sectionPreCarga<?php echo $idgui; ?>').find('.preg-docns');      
       if($speachdoc.length>0){
        $speachdoc.find('.nopreview').removeClass('hidden');
        $speachdoc.find('.showonpreview_nlsw').addClass('hidden');
        $tpldocente=$('#tmp_nlsw<?php echo $idgui; ?>').find('.preg-docns');
        $tpldocente.html($speachdoc.html());       
        //setTimeout(function(){/* crearonda(url,$tpldocente,true);*/ },1000)        
       
      }
    }
    initplantilla();
})
</script>