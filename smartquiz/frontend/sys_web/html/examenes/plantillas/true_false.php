<?php 
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$html_edicion = $this->pregunta['ejercicio'];
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<div class="plantilla plantilla-verdad_falso editando" id="tmp_<?php echo $idgui; ?>" data-clone="#ejercicioTerminado">
    <input type="hidden" value="<?php echo $this->pregunta['dificultad']; ?>" name="hPregDificultad" id="hPreguntaDificultad">
    <?php //if($html_edicion!='') {
        //echo $html_edicion;
    //} else { ?>
    <div class="row list-premises tpl_plantilla" id="pnl_premisas<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>">
        <!-- Premises container -->
    </div>
    <?php //} ?>
    <div class="row nopreview">
        <div class="col-xs-12 premise pr- hidden" id="molde">
            <div class="col-xs-12 col-sm-5">
                <span class="live-edit addtext"><?php echo ucfirst(JrTexto::_('premise')); ?></span>
                <input type="hidden" class="valPremise valin nopreview" data-nopreview=".premise.pr-">
            </div>
            <div class="col-xs-12 col-sm-6 options">
                <label class="col-xs-6"><input type="radio" class="radio-ctrl" name="opcPremise-" value="T"> <span class="texto-opcion" data-en="True" data-es="Verdadero"><?php echo ucfirst(JrTexto::_('true')); ?></span></label>
                <label class="col-xs-6"><input type="radio" class="radio-ctrl" name="opcPremise-" value="F"> <span class="texto-opcion" data-en="False" data-es="Falso"><?php echo ucfirst(JrTexto::_('false')); ?></span></label>
            </div>
            <div class="col-xs-2 col-sm-1 icon-zone">
                <a href="#" class="btn color-danger btn-xs tooltip delete nopreview" title="<?php echo JrTexto::_('remove') ?>" ><i class="fa fa-trash "></i></a>
                <span class="icon-result"></span>
            </div>
            <input type="hidden" id="opcPremise-">
        </div>
        <div class="col-xs-12">
            <div class="col-xs-offset-11 col-xs-1">
                <a href="#" class="btn btn-primary btn-xs tooltip add-premise" data-clone-from="#molde" data-clone-to=".list-premises.tpl_plantilla" title="<?php echo JrTexto::_('add') ?>" ><i class="fa fa-plus-square"></i></a>
            </div>
        </div>
    </div>

    <div id="ejercicioTerminado" class="hidden" style="display: none !important;"></div>
    <button id="generarhtml" class="hidden" style="display: none !important;"></button>
</div>
<textarea style="display:none;" class="tmp_verdad_falso<?php echo $idgui ?>" name="texto"></textarea>

<section id="sectionPreCarga" class="hidden" style="display: none !important;">
<?php 
if($html_edicion!=''){ 
    echo $html_edicion;
} ?>
</section>

<script type="text/javascript">
var idgui='<?php echo $idgui; ?>';

$(document).ready(function() {
    var asignarRsptaEdicion = function( $inpRadio,pnl ){
        var name = $inpRadio.attr('name');
        var marcada_value = $('input[name="'+name+'"].radio-ctrl:checked',pnl).val();
        var now = Date.now();
        var md5_rspta = $.md5(marcada_value, now);
        $('input#'+name,pnl).attr('data-lock', now);
        $('input#'+name,pnl).val(md5_rspta);
    };

    var vistaEdicionVoF = function(){
        var $plantilla = $('#tmp_<?php echo $idgui; ?>');
        $('.list-premises',$plantilla).find('input[type="radio"]').each(function() {
            /* volver a marcar las opciones en vista de edición*/
            /*console.log($(this));*/
            var name = $(this).attr('name');
            var rspta_crrta = $('input#'+name).val();
            var lock = $('input#'+name).attr('data-lock');
            var value = $(this).val();
            var md5_value = $.md5(value, lock);
            if(md5_value===rspta_crrta){
                $(this).prop('checked', true);
            }
        });
        $('.icon-result',$plantilla).html('');
        $('.icon-zone a.delete',$plantilla).show();
        $('.list-premises .premise.nopreview',$plantilla).removeClass('nopreview hidden');
    };

    $('.plantilla-verdad_falso')
    .on('click', '.list-premises a.btn.delete', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('.premise').remove();
    })
    .on('click','a.btn.add-premise',function(e) {
        e.preventDefault();
        e.stopPropagation();
        var id = $(this).closest('.plantilla').attr('id');
        var $plantilla = $('#'+id);
        var clone_from = $(this).attr('data-clone-from');
        var clone_to = $(this).attr('data-clone-to');
        var new_premise = $(clone_from, $plantilla).clone();
        var cant_premises = $(clone_to+' .premise', $plantilla).length;
        if(cant_premises>0) {
            var nameInput= $(clone_to+' .premise:nth-child('+cant_premises+') .options input').attr('name').split('-');
            var new_index = parseInt(nameInput[1]) + 1;
        } else {
            var new_index = 1;
        }
        new_index = Date.now();
        new_premise.find('.options input').each(function() {
            var new_name = $(this).attr('name') + new_index;
            $(this).attr('name', new_name ); /*renombrando grupo de opciones Radio*/
        });
        new_premise.find('input#opcPremise-').each(function() {
            var new_id = $(this).attr('id') + new_index;
            $(this).attr('id', new_id); /*renombrando input hidden opc-correcta*/
        });
        new_premise.find('input.valPremise.nopreview').each(function() {
            var new_data = $(this).attr('data-nopreview') + new_index;
            $(this).attr('data-nopreview', new_data); /*renombrando input hidden opc-correcta*/
        });

        $(clone_to, $plantilla).append('<div class="col-xs-12 premise pr-'+new_index+'">'+ new_premise.html() +'</div>');
        $('.istooltip').tooltip();
    })
    .on('change','.list-premises .options input[type="radio"].radio-ctrl', function(e){
        e.stopPropagation();
        var $this=$(this);
        var id = $(this).closest('.plantilla').attr('id');
        var objcont = $('#'+id);
        asignarRsptaEdicion($this,objcont);
    })
    .on('click', '#generarhtml', function(e) {
        e.preventDefault();
        var $plantilla = $(this).closest('.plantilla');
        $plantilla.removeClass('editando')
        var idClonar = $plantilla.attr('data-clone');
        var listPremises = $plantilla.find('.list-premises').clone();
        listPremises.find('.premise').each(function() {
            $(this).find('.icon-zone a.delete').hide();
        });

        $(idClonar).html(listPremises);
    });

     var existeHtml = <?php echo ($html_edicion!='')? 'true':'false'  ?>;
    if(existeHtml){
        /*evita recargar el titulo y desrcip infinitas veces*/
        var precarga = $('#sectionPreCarga').clone();
        var html = precarga.find('.ejercicio').html();
        var $plantilla = $('#tmp_<?php echo $idgui; ?>');
        $plantilla.find('.list-premises').replaceWith(html);
        vistaEdicionVoF();
       $('#sectionPreCarga').remove();
    } else {
        $('a.btn.add-premise').trigger('click');
    }
});
</script>