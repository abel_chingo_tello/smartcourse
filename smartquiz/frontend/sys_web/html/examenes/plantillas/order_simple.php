<?php 
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$html_edicion = $this->pregunta['ejercicio'];
$rutabase = $this->documento->getUrlBase();
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">

<div class="plantilla plantilla-ordenar ord_simple editando" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>" data-clone="#panelEjercicio">
    <input type="hidden" value="<?php echo $this->pregunta['dificultad']; ?>"  name="hPregDificultad" id="hPreguntaDificultad">
    <div class="" id="panelEjercicio">
        <input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
        <div class="row lista-elem-edit nopreview" id="lista-elem-edit<?php echo $idgui; ?>">
            <div class="col-xs-12 elem-edit" id="elem_0">
                <div class="col-xs-12 zona-edicion">
                    <div class="col-xs-12 col-sm-5 col-md-3 btns-media">
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-default btn-block selectmedia" data-tipo="image" data-url=".img_0" data-tooltip="tooltip" title="<?php echo JrTexto::_('select').' '.JrTexto::_('image') ?>"><i class="fa fa-picture-o"></i></a>
                            <img src="" class="hidden img_0">
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-default btn-block selectmedia"  data-tipo="audio" data-url=".audio_0" data-tooltip="tooltip" title="<?php echo JrTexto::_('select') ?> audio"><i class="fa fa-headphones"></i></a>
                            <audio src="" class="hidden audio_0"></audio>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-9 inputs-elem">
                        <input type="text" class="form-control txt-words" name="txtWord" placeholder="<?php echo ucfirst(JrTexto::_('write')).' '.JrTexto::_('something'); ?>" autocomplete="off">
                        <div class="row fix-part-elem">
                            <label class="col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('fix')).' '.JrTexto::_('a part'); ?>: </label>
                            <div class="col-xs-12 col-sm-9 list-elem-part">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row hidden" style="display: none !important;">
            <div class="col-xs-12 text-center botones-editar" style="display: none;">
                <a href="#" class="btn btn-success back-edit"><i class="fa fa-pencil"></i> <?php echo JrTexto::_('back').' '.JrTexto::_('and').' '.JrTexto::_('edit'); ?></a>
            </div>

            <div class="hidden col-xs-12 element toClone" id="toClone">
                <div class="col-xs-12 col-sm-4 multimedia hide">
                    <img src="" class="img-responsive hide">
                    <a href="#" class="btn btn-orange btn-block hide" data-tooltip="tooltip"><i class="fa fa-play"></i></a>
                </div>
                <div class="col-xs-12 col-sm-8 texto">
                    <div class="drag">
                    </div>
                    <div class="drop">
                    </div>
                </div>
            </div>
        </div>
        <div class="row ejerc-ordenar tpl_plantilla" id="ejerc-ordenar<?php echo $idgui; ?>" style="display: none;">
        </div>
        <audio src="" class="hidden" id="audio-ordenar<?php echo $idgui; ?>" style="display: none;"></audio>
    </div>
    <div class="col-xs-12 text-center botones-creacion">
        <a href="#" id="generarhtml" class="btn btn-success generar-elem hidden" data-clone-from="#toClone" data-clone-to=".ejerc-ordenar.tpl_plantilla" style="display: none !important;"><i class="fa fa-rocket"></i> <?php echo JrTexto::_('finish').' '.JrTexto::_('and').' '.JrTexto::_('generate'); ?></a>
    </div>
</div>

<section id="sectionPreCarga" class="hidden" style="display: none !important;">
<?php 
if($html_edicion!=''){ 
    echo str_replace('__xRUTABASEx__', $rutabase, $html_edicion);
} ?>
</section>

<script>
var lastBtnClicked = null;
function activarBtn(){
    var $btn = $(lastBtnClicked);
    var classElem = $btn.attr('data-url');
    if( $btn.data('tipo') == 'image' ){
        var src = $btn.siblings('img'+classElem).attr('src');
        if( src!='' && src!=undefined ){
            $btn.removeClass('btn-default').addClass('btn-success');
        } else {
            $btn.removeClass('btn-success').addClass('btn-default');
        }
    }
    if( $btn.data('tipo') == 'audio' ){
        var src = $btn.siblings('audio'+classElem).data('audio');
        if( src!='' && src!=undefined ){
            $btn.removeClass('btn-default').addClass('btn-orange');
        } else {
            $btn.removeClass('btn-orange').addClass('btn-default');
        }
    }
    lastBtnClicked = null;
}

var TIPO_DIVISION = 'letra';
var dividirCadena = function(value, $contenedor){
    var arr_value = null;
    var html = '';
    if( TIPO_DIVISION == 'letra' ){
        value = value.toUpperCase();
        var arr_value = value.split('');
    }

    if( TIPO_DIVISION == 'palabra' ){
        var arr_value = value.split(' ');
    }
    if(arr_value!=null){
        $contenedor.html('');
        $.each(arr_value, function(key, val) {
            if(val!=''){
                html = '<div>'+val+'</div>';
                $contenedor.append(html);
            }
        });
    }
};
$plantilla = $('#tmp_<?php echo $idgui; ?>');

var generarElementOrdenar = function(idCloneFrom, idCloneTo, IDGUI){
    //var $plantilla= $('.elem-edit').closest('.plantilla-ordenar');
    var $contenedor = $('.lista-elem-edit', $plantilla);
    var clsElem = '.elem-edit';
    var clsPartes = '.list-elem-part';
    $(idCloneTo, $plantilla).html('');

    $contenedor.find(clsElem).each(function() {
        var html = $(idCloneFrom).clone();
        html.removeClass('toClone');
        var texto = $(this).find('input.txt-words').val().trim();
        if(texto!='' && texto!=undefined){
            var key = $(this).attr('id').split('_')[1];
            if( texto.split(' ').length > 1 ) { var clase = 'word'; } 
            else { var clase = 'letter'; }

            $(this).find('input.txt-words').attr('value', texto);

            /* Agregando las Partes a la zona Drag y a la Drop*/
            $(this).find(clsPartes).children('div').each(function() {
                if($(this).hasClass('fixed')){
                    /* Agregando PARTES fijas en DROP*/
                    html.find('.drop').append('<div class="parte fixed '+clase+'"><div>'+$(this).text()+'</div></div>');
                } else {
                    /* Agregando PARTES en DRAG en posic al azar*/
                    var $_dragParts = html.find('.drag').children();
                    if( $_dragParts.length == 0 ){
                        html.find('.drag').html('<div>'+$(this).text()+'</div>');
                    } else {
                        var posic = Math.floor(Math.random()*$_dragParts.length),
                        aleat = Math.floor(Math.random()*10);
                        if(aleat%2 == 0) { $_dragParts.eq(posic).before('<div>'+$(this).text()+'</div>'); }
                        else { $_dragParts.eq(posic).after('<div>'+$(this).text()+'</div>'); }
                    }

                    /* Agregando PARTES en blanco en DROP*/
                    html.find('.drop').append('<div class="parte blank '+clase+'"></div>');
                }
            });

            /* Agregando los contenidos multimedia */
            var imgSrc = $(this).find('img.hidden').attr('src');
            if( imgSrc!='' && imgSrc!=undefined ){
                html.find('.multimedia').removeClass('hide');
                html.find('img').attr('src', imgSrc);
                html.find('img').removeClass('hide');
            } 
            var audSrc = $(this).find('audio.hidden').attr('data-audio');
            if( audSrc!='' && audSrc!=undefined ){
                html.find('.multimedia').removeClass('hide');
                html.find('a').attr('data-audio', audSrc);
                html.find('a').removeClass('hide');
            } 

            html.removeClass('hidden').attr('id', 'e_'+key).attr({
                'id': 'e_'+key,
                'data-resp': texto
            });
            $(idCloneTo, $plantilla).html(html);
        }
    });
};
$(document).ready(function() {
    var sysinitOrdenar=function(){
        $('.plantilla-ordenar').each(function(){
            var idgui=$(this).attr('id');
            var ayuda=$(this).closest('.method').attr('data-idmet')==3?false:true;
            initOrdenarSimple(idgui,ayuda);
        });           
    }
    var isEditando = $('.plantilla').hasClass('editando');

    $('.plantilla-ordenar')
    .on('click', '.selectmedia', function(e){
        if(isEditando==false) return false;
        e.preventDefault();
        var txt = MSJES_PHP.select_upload;
        lastBtnClicked = this;
        selectedfile(e,this,txt,'activarBtn');
    })
    .on('focusin','input.txt-words', function(e) {
        if(isEditando==false) return false;
        var string = $(this).val();
        if( string.split(' ').length > 1 ) { TIPO_DIVISION = 'palabra'; }
        else { TIPO_DIVISION = 'letra'; }
    })
    .on('keyup', 'input.txt-words', function(e) {
        if(isEditando==false) return false;
        e.preventDefault();
        var string = $(this).val();
        if( string == '' ){ TIPO_DIVISION = 'letra' }
            if(e.keyCode == 32){  TIPO_DIVISION = 'palabra'; }
        if(e.keyCode == 8){
            if( string.split(' ').length <= 1 ){ TIPO_DIVISION = 'letra'; }
        }
        $contenedor = $(this).parents('.inputs-elem').find('.list-elem-part');

        dividirCadena(string, $contenedor);
    })
    .on('click', '.list-elem-part>div', function(e) {
        $(this).toggleClass('fixed');
        return false;
    })
    .on('click', '.generar-elem',function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $plantilla=$(this).closest('.plantilla-ordenar');
        var idTmpl = $plantilla.attr('id');
        $plantilla.removeClass('editando');
        var idguiTmpl = $('.ejerc-ordenar', $plantilla).attr('id').replace('ejerc-ordenar','');
        var idCloneFrom = $(this).data('clone-from');
        var idCloneTo = $(this).data('clone-to');
        var x = generarElementOrdenar(idCloneFrom, idCloneTo, idguiTmpl);
        $('.ejerc-ordenar', $plantilla).show();
        $('.lista-elem-edit', $plantilla).hide().addClass('hidden');
        $plantilla.find('.botones-editar').show();
        $plantilla.find('.botones-creacion').hide().addClass('hidden');
        $plantilla.find('.nopreview').addClass('hidden').hide();
    });

    var existeHtml = <?php echo ($this->pregunta['ejercicio']!='')? 'true':'false'  ?>;
    if(existeHtml){
        var precarga = $('#sectionPreCarga').clone();
        var html = precarga.find('.ejercicio').html();
        $plantilla.find('#panelEjercicio').html(html); /*evita recargar el titulo y desrcip infinitas veces*/
        $plantilla.find('#panelEjercicio .nopreview').removeClass('hidden').show();
        $plantilla.find('#panelEjercicio .ejerc-ordenar.tpl_plantilla').hide();
        $plantilla.find('#panelEjercicio .botones-editar').hide();
        $plantilla.find('#panelEjercicio .botones-creacion').show().removeClass('hidden');
        $('#sectionPreCarga').remove();
    }

});
</script>