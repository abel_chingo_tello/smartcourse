<style>
    ol.dd-list {
        counter-reset: li;
    }

    ol.dd-list>li.dd-item:before {
        counter-increment: li;
        content: '·'counter(li) ' ';
        color: gray;
        position: absolute;
        left: -1.8em;
        top: 1.5em;
    }

    .dd {
        max-width: 100% !important;
        padding-right: 2rem;
    }

    .dd-handle {
        height: auto;
    }

    .boxordenlist {
        background: white;
        border: 2px solid #689dca;
        border-radius: 1em;
    }

    .spancontenedor {
        font-weight: normal;
    }

    .btnsavecontainer {
        padding: 5px 0;
    }
</style>

<input type="hidden" name="hIdExamen" id="hIdExamen" value="<?php echo @$this->examen["idexamen"]; ?>">

<?php if (!empty(@$this->examen["idexamen"])) : ?>
    <div>
        <div class="input-group" style="margin: 0;">
            <div class="input-group-addon"><?php echo ucfirst(JrTexto::_("Link to solve the exam")) ?></div>
            <input type="url" class="form-control" id="fuente_externa" name="fuente_externa" placeholder="<?php echo ucfirst(JrTexto::_("e.g.")) ?>: https://www.myweb.com/skills/list" value="<?php echo $this->documento->getUrlBase() . '/examenes/resolver/?idexamen=' . @$this->examen["idexamen"]; ?>" readonly="readonly" style="background-color: #fff;">
        </div>
        <br>
    </div>
<?php endif; ?>
<div class="container">
    <h4>Ordenar los contenedores</h4>
    <p class="alert-warning" style="padding:1.5em;">Atención ordenar los contenedores solo surgirá efecto si tiene la configuración del examen en "preguntas ordenadas" y no en "aleatorio"</p>
    <div class="boxordenlist">
        <div class="btnsavecontainer">
            <button class="btn btn-success btn-block on-save"><?= JrTexto::_("Save"); ?></button>
        </div>
        <!--START ORDEN LIST-->
        <div class="dd">
            <div class="dd-list">
                <ol class="dd-list">
                    <?php if (!empty($this->preguntas)) : ?>
                        <?php foreach ($this->preguntas as $k => $pregunta) : ?>
                            <li class="dd-item" data-idcontenedor="<?= $k ?>">
                                <div class="dd-handle">
                                    <div>Id contenedor: <span class="spancontenedor"><?= $k ?></span></div>
                                    <div>Preguntas: <?= $pregunta['nombrespreguntas'] ?></div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ol>
            </div>
        </div>
        <!--END ORDEN LIST-->
        <div class="btnsavecontainer">
            <button class="btn btn-success btn-block on-save"><?= JrTexto::_("Save"); ?></button>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    jQuery.fn.scrollParent = function() {
        var overflowRegex = /(auto|scroll)/,
            position = this.css("position"),
            excludeStaticParent = position === "absolute",
            scrollParent = this.parents().filter(function() {
                var parent = $(this);
                if (excludeStaticParent && parent.css("position") === "static") {
                    return false;
                }
                var overflowState = parent.css(["overflow", "overflowX", "overflowY"]);
                return (overflowRegex).test(overflowState.overflow + overflowState.overflowX + overflowState.overflowY);
            }).eq(0);

        return position === "fixed" || !scrollParent.length ? $(this[0].ownerDocument || document) : scrollParent;
    };

    var IDEXAMEN = <?= !empty($this->examen["idexamen"]) ? $this->examen["idexamen"] : 0 ?>;

    function initevents() {
        $('body').on('click', '.on-save', function() {
            var jsonlist = $('.dd').nestable('serialize');
            $.post(String.prototype.concat(_sysUrlBase_, "/examenes/saveorden"), {
                lista: jsonlist,
                idexamen: IDEXAMEN
            }, function(response) {
                if (response.estado == 200) {
                    Swal.fire({
                        icon: 'success',
                        title: response.msj,
                        showConfirmButton: true
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: response.msj,
                        showConfirmButton: true
                    });
                    console.error(response.msj);
                }
            }, 'json');
        });
    }
    $(document).ready(function() {
        initevents();
        $('.dd').nestable({
            scroll: true,
            maxDepth: 1,
            scrollSensitivity: 2,
            scrollSpeed: 10
        });
        showexamen('orden');
    });
</script>