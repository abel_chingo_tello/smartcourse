
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Alumno'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('alumno'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["dni"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["dni"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Dni") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["dni"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Ape paterno") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["ape_paterno"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Ape materno") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["ape_materno"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Nombre") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["nombre"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fechanac") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fechanac"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Sexo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["sexo"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Estado civil") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["estado_civil"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Ubigeo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["ubigeo"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Urbanizacion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["urbanizacion"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Direccion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["direccion"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Telefono") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["telefono"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Celular") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["celular"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Email") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["email"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idugel") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idugel"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Regusuario") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["regusuario"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Regfecha") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["regfecha"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Usuario") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["usuario"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Clave") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["clave"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Token") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["token"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Foto") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["foto"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Estado") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["estado"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Situacion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["situacion"] ;?></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('alumno'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["dni"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["dni"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkdni').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
          addFancyAjax("<?php echo JrAplicacion::getJrUrl(array('Alumno', 'frm'))?>?tpl=b&acc=Editar&id="+id, true);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Alumno', 'eliminarAlumno',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                    recargarpagina(false);
                    } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>