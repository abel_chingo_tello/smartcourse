<?php defined('RUTA_BASE') or die(); 
if(isset($_GET['type']) && @$_GET['type']=='admin'){
    $ruta= $this->documento->getUrlBase();
    $btnColor = 'btn-primary';
    $icon = 'fa-user';
    $rol = 'administrador';
}else{
    $ruta= $this->documento->getUrlBase().'/?type=admin';
    $btnColor = 'btn-danger';
    $icon = 'fa-lock';
    $rol = 'usuario';
} ?>
<div class="container-fluid" >
    <a class="hiddenanchor" id="torecuperar"></a>
    <a class="hiddenanchor" id="tologin"></a>
    
    <div class="row"><div class="col-xs-12 col-sm-3" id="" style="height: 94px;">
        <!--img src="<?php echo $this->documento->getUrlStatic() ?>/img/sistema/logo_kfactory_color.png" alt="" class="img-responsive"-->
    </div></div> 
    <div class="row"><div class="col-xs-3" id="logo_segundo">
        <!--img src="<?php echo $this->documento->getUrlStatic() ?>/img/sistema/logo_edukt_color.png" alt="" class="img-responsive center-block"-->
        <h1 class="text-center" style="font-size: 3em;"><small class="color-blue">smart</small><span class="color-green2">Quiz</span></h1>
    </div></div>
    <div id="wrapper">

        <div id="login" class="animate form ">
            <section class="login_content text-center">
            	<?php if(!empty($this->msjErrorLogin)):?>
                <div class="alert alert-warning" role="alert">
                	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only"><?php echo JrTexto::_('Attention')?>:</span> <?php echo JrTexto::_('Combination of email and password incorrect')?>
              	</div>
				<?php endif;?>
                
                <form method="post">
                    <h1><?php echo JrTexto::_('Login'). ' - ' . ucfirst($rol);?></h1>
                    
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="<?php echo JrTexto::_('User')?>" required="required" name="usuario" id="usuario" value="<?php echo $this->pasarHtml($this->usuario)?>" maxlength="80" />
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="<?php echo JrTexto::_('Password')?>" required="required" name="clave" id="clave"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div>
                        <button type="submit" id="#btn-enviar-formInfoLogin" class="btn btn-blue">
                        <i class="fa fa-key"></i> <?php echo JrTexto::_('Log in')?></button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">
                        <p class="change_link"><?php echo JrTexto::_('Did you forget your password?')?>
                            <a href="#torecuperar" class="to_recuperar"> <?php echo JrTexto::_('Recover password')?></a>
                        </p>
                        <div class="clearfix"></div>
                        <br />
                        <?php /*if($this->oNegConfig->get('multiidioma')=='SI'):?>
                        <div>
                            <a class="changeidioma" idioma="EN"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/idiomas/en_us.png"></a>
                            <a class="changeidioma" idioma="ES"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/idiomas/es.png"></a>
                            <!--a class="changeidioma" idioma="FR"><img src="http://www.stneots-ec.com/templates/images/flag-3.png"></a-->
                            
                        </div>
                        <?php endif;*/ ?>
                        <!--<div>
                            <h1></h1>

                            <p>© 2016 <?php echo JrTexto::_('All rights reserved').' <br>'.$this->pasarHtml($this->oNegConfig->get('nombre_sitio'));?> </p>
                        </div>-->
                    </div>
                </form>
                <!-- form -->
            </section>
            <!-- content -->
            <div class="btnAdministrador">
                
                <a href="<?php echo $ruta; ?>" class="btn <?php echo $btnColor; ?>"><i class="fa <?php echo $icon; ?>"></i></a>
            </div>
        </div>
        <div id="recuperar" class="animate form">
            <section class="login_content">
                <form method="post" id="formRecupClave" onsubmit="return false;">
                    <h1><?php echo JrTexto::_('Recover password')?></h1>
                    <div class="form-group has-feedback">
                        <input type="text" name="usuario" id="usuario"   class="form-control" placeholder="<?php echo JrTexto::_('User or email')?>" required="required" maxlength="80" />
                        <span class="glyphicon glyphicon-envelope form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div>
                        <button type="submit" id="#btn-enviar-formInforecuperarclave" class="btn btn-default submits"> <?php echo JrTexto::_('Recover password')?></button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">
                        <p class="change_link">
                            <a href="#tologin" class="to_recuperar"><?php echo JrTexto::_('Return to login')?></a>
                        </p>
                        <div class="clearfix"></div>
                        <br />
                        <?php if($this->oNegConfig->get('multiidioma')=='SI'):?>
                        <div>
                            <a class="changeidioma" idioma="EN"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/idiomas/en_us.png"></a>
                            <a class="changeidioma" idioma="ES"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/idiomas/es.png"></a>
                            <!--a class="changeidioma" idioma="FR"><img src="http://www.stneots-ec.com/templates/images/flag-3.png"></a>
                            <a class="changeidioma" idioma="CH"><img src="http://www.stneots-ec.com/templates/images/flag-4.png"></a-->
                        </div>
                        <?php endif;?>
                        <!--<div>
                            <h1></h1>

                            <p>© 2016 <?php echo JrTexto::_('All rights reserved').' <br>'.$this->pasarHtml($this->oNegConfig->get('nombre_sitio'));?> </p>
                        </div>-->
                    </div>
                </form>
                <!-- form -->
            </section>
            <!-- content -->
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#formRecupClave').bind({
		 submit: function() {
			xajax__('', 'sesion', 'solicitarCambioClave', xajax.getFormValues('formRecupClave'));
		 }
    });
});
</script>