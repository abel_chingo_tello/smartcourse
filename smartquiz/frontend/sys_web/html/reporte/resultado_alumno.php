<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_fichas.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_imagen_puntos.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_completar.css">
<style>
    .contenedor-preguntas{
        overflow: hidden;
        border-bottom: 2px solid #bbb;
    }
    .contenedor-preguntas:last-child{border: none;}
    .tmpl-media img,
    .tmpl-media audio{
        max-height: 250px;
    }
    .tmpl-media .embed-responsive{
        height: 250px;
        padding: 0;
        margin: 1ex;
    }
    
    *[data-corregido='good']:before, .good:before{
        background-color: #2ed353;
        border-radius: 50%;
        content: "\f118";
        display: inline-block;
        color: white;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: 1.7em;
        height: 23px;
        position: absolute;
        right: 0;
        text-align: center;
        width: 23px;
    }
    *[data-corregido='bad']:before, .bad:before{
        background: #fc5c5c;
        border-radius: 50%;
        color: white;
        content: "\f119";
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        height: 23px;
        font-size: 1.7em;
        position: absolute;
        right: 0;
        text-align: center;
        width: 23px;
    }
    .tpl_plantilla:before{
        content: '';
        background: transparent;
    }

    ul#list-intentos li {
    	display: inline-block;
    }

    ul#list-intentos li a {
    	background: #fff;
    	border-radius: 50%;
    	height: 22px;
    	padding: 1px 5px;
    	text-align: center;
    	width: 22px;
    }
    ul#list-intentos li.active a {
    	background: #6fe3ff;
    	box-shadow: 0px 0px 3px 1px #444;
    }

    .panel .panel-heading {
    	overflow: hidden !important;
    }
    
    .panel .panel-heading .list-unstyled {
    	margin: 0;
    }

    @media (min-width: 768px){
        .table-key-value .key{
            width: 50% !important;
        }
    }

    /*estilos de impresion*/
    @media print{
        .contenedor-preguntas{
            page-break-after: always;
            break-after: always;
        }
        /* **** Join (Fichas) **** */
        .plantilla.plantilla-fichas .ejerc-fichas .ficha {
            width: 23%;
        }
    
        /* **** Verdadero o Falso **** */
        .plantilla.plantilla-verdad_falso .premise>div:first-child{
            width: 60%;
        }
        .plantilla.plantilla-verdad_falso .premise>.options{
            width: 40%;
            float: right;
        }

        /* **** Ordenar Parrafo **** */
        .plantilla.plantilla-ordenar.ord_parrafo .drag-parr,
        .plantilla.plantilla-ordenar.ord_parrafo .drop-parr{
            width: 50%;
        }

        /* **** Ordenar Parrafo **** */
        .plantilla.plantilla-img_puntos .contenedor-img{
            width: 75%;
        }
        .plantilla.plantilla-img_puntos .tag-alts{
            width: 25%;
        }
        .plantilla.plantilla-img_puntos .dot-container>.dot{
            background: #fff;
            border: 5px solid #4c5e9b;
        }
        .plantilla.plantilla-img_puntos .dot-container>.dot:after{
            border:  3px solid #efefef;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title text-center col-xs-offset-3 col-xs-6">
                	<?php echo ucfirst($this->examen_alumno['exam_titulo']); ?>
                </h3>
                <div class="col-xs-3">
                	<ul class="list-unstyled pull-right" id="list-intentos">
                		<span><?php echo ucfirst(JrTexto::_("attemps")); ?>: </span>
                		<?php foreach ($this->examenes_alumno as $i => $ea) { ?>
                		<li class="<?php echo ($i==0)?'active':''; ?> intento" data-idexaalumno="<?php echo $ea["idresultado_alumno"] ?>"><a href="#"><?php echo $i+1; ?></a></li>
                		<?php } ?>
                	</ul>
                </div>
            </div>

            <div class="panel-body" id="pnl-resultalumno"  style="font-family: '<?php echo $this->examen_alumno['exam_fuente']; ?>'; font-size: <?php echo $this->examen_alumno['exam_fuentesize'].'px'; ?>;">

                <div id="blocker-exercise" style="position: absolute; height: 100%; background: rgba(255,255,255,0); width: 100%; z-index: 10; "></div>
                
                <div class="col-xs-12" id="pnlPreguntas"></div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center hidden" id="pnlhabilidades">
                    <h3><strong><?php echo JrTexto::_("Result for Skill") ?></strong></h3>
                    <div class="col-xs-12 col-sm-12 col-md-12" id="resultskill">
                    <?php $i=-1; 
                    $arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
                    foreach ($this->habilidades as $hab) {  $i++; ?>
                        <div class="col-xs-6 col-sm-3 col-md-2 imagepay skill<?php echo $hab["skill_id"] ?>" data-idskill="<?php echo $hab["skill_id"] ?>" data-value="0" data-texto="<?php echo JrTexto::_($hab['skill_name']);?>" data-ccolorout="<?php echo $arrColoresHab[$i]; ?>"></div>
                    <?php } ?>
                 </div> 
            </div>
        </div>
    </div>
    <!--div class="col-md-12 text-center" style="z-index: 100;">
        <a href="#" class="btn btn-primary enviarcorreo"><?php echo ucfirst(JrTexto::_('Send by email')); ?></a>
    </div-->
</div>

<section id="msjes_idioma" class="hidden" style="display: none !important; ">
    <input type="hidden" id="alumno_tiene_mejorar" value='<?php echo ucfirst(JrTexto::_('The student has to improve the following skills')); ?>'>
    <input type="hidden" id="habilidades_destacadas" value='<?php echo ucfirst(JrTexto::_('These are the most outstanding skills of the student')); ?>'>
    <input type="hidden" id="attention" value='<?php echo ucfirst(JrTexto::_('Attention')); ?>'>
</section>

<script type="text/javascript">
const IDALUMNO = <?php echo $this->alumno['dni']; ?>;
var getResultado = function(formData) {
	$.ajax({
        url: _sysUrlBase_+'/examen_alumno/verjson',
        type: "POST",
        data:  formData,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,
        beforeSend: function(XMLHttpRequest) {
            $('#procesando').show('fast'); 
            $('#pnl-resultalumno').hide();
            $('#pnlhabilidades').addClass('hidden');
            $('.panel-body #pnlPreguntas').html('');
            $('.panel-body #resultado').remove();
            $('.panel-body #msjes-reporte').remove();
        },
        success: function(data) {
            if(data.code==='ok'){
                var exam = data.data['0'];
                if(exam!=undefined) {
                    var jsonPreg = $.parseJSON(exam.preguntas);

                    var html = '';
                    $.each(jsonPreg, function(index, val) {
                        html += val.replace(/__xRUTABASEx__/g, _sysUrlBase_);

                    });
                    $('.panel-body #pnlPreguntas').html(html);
                    $('.panel-body .tmpl').removeClass('col-sm-6 col-md-6').addClass('col-sm-12')

                    limpiarEjercicios();
                    cargarEstadisticas(exam);
                    $('#pnl-resultalumno').show();
                }
            }else{
                mostrar_notificacion(MSJES_PHP.attention, data.msj, 'warning');
            }
            $('#procesando').hide('fast');
            return false;
        },
        error: function(xhr,status,error) {
            mostrar_notificacion(MSJES_PHP.attention, status, 'warning');
            $('#procesando').hide('fast');
            return false;
        }               
    });
};

var limpiarEjercicios = function(){
    $('.valinput.open').unwrap();
    $('.valinput.open').siblings().remove();
};

var cargarEstadisticas = function(exam){
    $('#resultskill .imagepay').html('').hide();
    $('#pnlhabilidades').removeClass('hidden');
    var ptjeXHab = $.parseJSON(exam.puntajehabilidad);
    var hab_good = '';
    var hab_bad = '';
    console.log('ptjeXHab ', ptjeXHab);
    $.each(ptjeXHab,function(i,v){
        var skillobj=$('#resultskill .imagepay.skill'+i);
        console.log(skillobj);
        var nombreHab = $('#resultskill .imagepay.skill'+i).attr('data-texto');
        if(skillobj.length>0){
        	console.log(skillobj);
            skillobj.show();
            skillobj.attr('data-value',v);
            skillobj.graficocircle();
        } 
        if(v>50){
            hab_good += '<li class="list-group-item">'+nombreHab+'</li>';
        } else {
            hab_bad += '<li class="list-group-item">'+nombreHab+'</li>';
        }
    }); 
    $('#pnlhabilidades').before('<div class="col-xs-12 text-center" id="resultado">'+exam.resultado+'</div>');
    $('#pnlhabilidades').after('<div class="col-xs-12" id="msjes-reporte"></div>')
    if(hab_good!='') $('#msjes-reporte').append('<div class="col-xs-12 col-sm-4"><h4>'+MSJES_PHP.habilidades_destacadas+'</h4><ul class="list-group">'+hab_good+'</ul></div>');
    if(hab_bad!='') $('#msjes-reporte').append('<div class="col-xs-12 col-sm-4"><h4>'+MSJES_PHP.alumno_tiene_mejorar+'</h4><ul class="list-group">'+hab_bad+'</ul></div>');
};

$(document).ready(function() {
    /*$('.enviarcorreo').click(function(e) {
        e.preventDefault();
        var valor = $('#optAlumnos').val();
        var email = $('#optAlumnos option[value="'+valor+'"]').attr('data-email');
        var nombre_alum = $('#optAlumnos option[value="'+valor+'"]').text();
        var msje = $('#resultado').html();
        msje += $('#pnlhabilidades').html();
        msje += $('#msjes-reporte').html();

        var formData = new FormData();
        formData.append("email", email);
        formData.append("nombre", nombre_alum);
        formData.append("msje", msje);
        formData.append("asunto", '<?php echo ucfirst(JrTexto::_('Your assessment result')); ?>');
        $.ajax({
            url: _sysUrlBase_+'/sendemail/enviar',
            type: "POST",
            data:  formData,
            contentType: false,
            dataType :'json',
            cache: false,
            processData:false,
            beforeSend: function(XMLHttpRequest){
                $('#procesando').show('fast'); 
            },
            success: function(data)
            {
                if(data.code==='ok'){
                    
                }else{
                    mostrar_notificacion(MSJES_PHP.attention, data.msj, 'warning');
                }
                $('#procesando').hide('fast');
                return false;
            },
            error: function(xhr,status,error){
                mostrar_notificacion(MSJES_PHP.attention, status, 'warning');
                $('#procesando').hide('fast');
                return false;
            }               
        });
    });*/

    $('#list-intentos .intento').click(function(e) {
    	var fd = new FormData();
	    var idExamAlumno = $(this).data('idexaalumno');
	    fd.append("idexaalumno", idExamAlumno);
	    fd.append("idalumno", IDALUMNO);

	    $('#list-intentos .intento').removeClass('active');
	    $(this).addClass('active');
		getResultado(fd);
    	return false;
    });

    $('#list-intentos .intento.active').trigger('click');

    cargarMensajesPHP('#msjes_idioma');
});
</script>