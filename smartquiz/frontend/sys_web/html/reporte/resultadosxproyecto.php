<div class="row" id="resultados_proyecto">
	<?php if($this->esSuperAdmin) { ?>
	<div class="col-xs-12 col-sm-9 form-group select-ctrl-wrapper select-azul" id="filtroEmpresa">
		<select name="opcEmpresa" id="opcEmpresa" class="form-control select-ctrl">
			<option value="">- <?php echo JrTexto::_("Select Project") ?> -</option>
			<?php foreach ($this->empresas as $e) { ?>
			<option value="<?php echo $e['slug'] ?>"><?php echo $e['nombre'] ?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-xs-12 col-sm-3">
		<button class="btn btn-info btn-filtrar"><?php echo JrTexto::_("Filter") ?></button>
	</div>
	<?php } ?>

	<input type="hidden" id="hSlugProyecto" name="hSlugProyecto" value="">

	<div class="col-xs-12" id="resultados"></div>
</div>