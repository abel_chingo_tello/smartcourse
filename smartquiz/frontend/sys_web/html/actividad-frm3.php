<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
$actividades=!empty($this->actividades)?$this->actividades:null; 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
if(!empty($this->datos)) $frm=$this->datos;
?>
<style type="text/css">
    .addtext{
        border-bottom: 1px solid rgba(62, 168, 239, 0.93);
        padding: 1ex 0;
        line-height: 25px;
    }
    .addtext>input{
        margin:  1em 0;
        padding: 0 7px;
    }    
    .cargarpage{
        border: 1px solid rgba(62, 168, 239, 0.93) !important;
    }
</style>
<div id="msj-interno"></div>
<form method="post" id="frm-<?php echo $idgui;?>" class="form-horizontal form-label-left" onsubmit="return false;" >
<div class="main-container" data-rol="<?php echo $rolActivo; ?>"> <div class="page-content">
    <div class="row" id="actividad"> <div class="col-xs-12">
        <div class="row" id="actividad-header">
         <input type="hidden" name="txtNivel" id="txtNivel" value="<?php echo $this->idnivel; ?>" >
         <input type="hidden" name="txtunidad" id="txtunidad" value="<?php echo $this->idunidad; ?>" >
         <input type="hidden" name="txtSesion" id="txtsesion" value="<?php echo $this->idsesion; ?>" >
        <div class="col-md-12">
            <ol class="breadcrumb">
              <li><a href="<?php echo $this->documento->getUrlSitio(); ?>"><?php echo JrTexto::_("Home")?></a></li>
              <li><a href="<?php echo $this->documento->getUrlSitio(); ?>/recursos/listar/<?php echo $this->idnivel."/".$this->idunidad."/".$this->idsesion; ?>"><?php echo JrTexto::_("Activities")?></a></li>
              <li><a href="#"><?php echo $this->actividad["nombre"] ?></a></li>
              <li class="active"><?php echo JrTexto::_("Edit");?></li>
            </ol>
        </div>            
                       
        </div>
        <div class="row" id="actividad-body">
            <div class="col-xs-12 col-sm-9">
                <ul class="nav nav-pills  metodologias">
                    <?php 
                    $imet=0;
                    $metcode=null;
                    if(!empty($this->metodologias))
                    foreach ($this->metodologias as $met) { $imet++;?>
                        <li class="<?php echo $imet==1?'active':'';?>">
                        <a href="#met-<?php echo $met["idmetodologia"] ?>" data-toggle="pill"><?php echo JrTexto::_(trim($met["nombre"])); ?></a>
                        </li>
                    <?php 
                        $metcode.=$met["idmetodologia"].'|';
                    }?>
                    <input name="metodologias" type="hidden" value="<?php echo substr($metcode,0,-1); ?>" >
                </ul>
                <div class="actividad-main-content tab-content">
                    <?php //if($met["idmetodologia"]==3){ ?>
                    <div id="contenedor-paneles">
                        <div id="panel-intentos-dby" class="panel-info " style="display: none;">
                            <!--
                                <div class="numero" title="<?php echo ucfirst(JrTexto::_('number of attempts you take')); ?>">
                                -->
                                <span class="titulo-panel"><?php echo ucfirst(JrTexto::_('attempt')); ?></span>
                                <span class="actual">1</span>
                                <span class="total"><?php echo $this->intentos; ?></span>
                                <!--
                                </div>
                                <a href="#" class="btn btn-xs btn-primary try-again">
                                    <i class="fa fa-undo" aria-hidden="true"></i>
                                    <span class="hidden-xs"><?php echo ucfirst(JrTexto::_('try again')); ?></span>
                                </a>
                            -->
                        </div>
                    </div>
                    <?php //} ?>  

                    <?php 
                    $imet=0;
                    if(!empty($this->metodologias))
                    foreach ($this->metodologias as $met) { $imet++;?>
                                
                    <div id="met-<?php echo $met["idmetodologia"] ?>" class="metod tab-pane fade <?php echo $imet==1?'active in':'';?>">
                        <div class="row" >
                        <div class="btnchoiceplantilla nopreview" data-met="<?php echo $met["idmetodologia"] ?>" data-tpl=""> <span class="btn btn-primary"><?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('template')?></span></div>
                        <?php 
                        $tpl=$this->oNegActividad->tplxMetodologia($met["idmetodologia"]);
                        if(!empty($tpl))
                            if($tpl["padre"]==false){
                                if(!empty($this->actividades[$met["idmetodologia"]]["act"]["det"][0]["texto_edit"]))
                                   $texto_edit=$this->actividades[$met["idmetodologia"]]["act"]["det"][0]["texto_edit"];
                                ?>
                            
                            <div class="Ejercicio"><br>
                            <div class="textosave"  data-texto="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>">
                                <?php if(!empty($texto_edit)) {
                                    echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$texto_edit);
                                }else{?>
                                <div class="col-xs-12">
                                    <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" > 
                                        <div class="col-md-12">
                                            <h3 class="addtext addtexttitle<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('title') ?></h3>
                                            <input type="hidden" class="valin nopreview"  name="titulo[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtexttitle<?php echo $met["idmetodologia"]; ?>">
                                        </div>
                                        <div class="space-line pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"></div>
                                    </div>
                                    <div class="tab-description pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"> 
                                        <div class="col-md-12">
                                            <p class="addtext addtextdescription<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('description') ?></p>
                                            <input type="hidden" class="valin nopreview"  name="descripcion[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtextdescription<?php echo $met["idmetodologia"]; ?>" >
                                        </div>
                                    </div>

                                    <div class="aquicargaplantilla"  id="tmppt_<?php echo $met["idmetodologia"] ?>"></div>
                                    <input type="hidden" name="habilidades[<?php echo $met["idmetodologia"] ?>][1]" class="selected-skills" id="habilidades_met-<?php echo $met["idmetodologia"] ?>_1" value="0">
                                </div>
                                <?php } ?>
                            </div>
                            <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>" class="hidden"></textarea>
                            <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_edit" class="hidden"></textarea>
                            </div>
                            <?php }else{ 
                                $texto_edit=null;
                                $ntexto_edit=0;
                                if(!empty($this->actividades[$met["idmetodologia"]]["act"]["det"])){
                                    $texto_edit=$this->actividades[$met["idmetodologia"]]["act"]["det"];
                                    $ntexto_edit=count($texto_edit);
                                }
                                ?>
                            <div class="col-xs-12">
                                <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', $met["nombre"]) ?>" data-id-metod="<?php echo $met["idmetodologia"]; ?>">
                                    <?php if(!empty($ntexto_edit))
                                    for ($im=1 ;$im<=$ntexto_edit;$im++){?>
                                         <li class="<?php echo $im==1?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]).'-'.$im; ?>" data-toggle="tab"><?php echo $im; ?></a></li>
                                     <?php }else{ ?>
                                    <li class="active"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1" data-toggle="tab">1</a></li>
                                    <?php } ?>
                                    <li class="btn-Add-Tab nopreview"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-0" class="tooltip" title="<?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('tab') ?>"><i class="fa fa-plus-circle"></i></a></li>
                                </ul>
                            </div>

                            <div class="col-xs-12">
                                <div class="tab-content" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-main-content">
                                   <?php if(!empty($texto_edit)){
                                        $im=0;
                                        foreach ($texto_edit as $tedit){ $im++?>
                                        <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"])."-".$im; ?>" class="tabhijo tab-pane fade <?php echo $im==1?'active':''; ?> in textosave" 
                                    data-texto="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im;  ?>">
                                           <?php echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$tedit["texto_edit"]);?>
                                         </div>  
                                        <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][<?php echo $im;?>]; " id="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im; ?>" class="hidden"></textarea>
                                        <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][<?php echo $im;?>]; " id="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im; ;  ?>_edit" class="hidden"></textarea>
                                       <?php }
                                       }else{ ?>
                                    <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1" class="tabhijo tab-pane fade active in textosave" 
                                    data-texto="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1">
                                        <div class="row"> 
                                          <div class="col-xs-12">
                                            <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" > 
                                                <div style="position: absolute; right: 1.5em; ">
                                                    <div class="btn-close-tab tooltip nopreview" title="<?php echo JrTexto::_('remove') ?> <?php echo JrTexto::_('tab') ?>" data-id-tab="tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1">
                                                      <button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="display:none;">
                                                    <h3 class="addtext addtexttitulo<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('title') ?></h3>
                                                    <input type="hidden" class="valin nopreview"  name="titulo[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtexttitulo<?php echo $met["idmetodologia"]; ?>">
                                                </div>
                                                <div class="space-line pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"></div>
                                            </div>
                                            <div class="tab-description pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"> 
                                                <div class="col-md-12">
                                                    <p class="addtext addtextdescription<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('description') ?></p>
                                                    <input type="hidden" class="valin nopreview"  name="descripcion[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtextdescription<?php echo $met["idmetodologia"]; ?>" >
                                                </div>
                                            </div>
                                            <div class="aquicargaplantilla" id="tmppt_<?php echo $met["idmetodologia"] ?>_1"></div>
                                            <input type="hidden" name="habilidades[<?php echo $met["idmetodologia"] ?>][1]" class="selected-skills" id="habilidades_met-<?php echo $met["idmetodologia"] ?>_1" value="0">
                                          </div>
                                        </div>
                                    </div>
                                    <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1" class="hidden"></textarea>
                                    <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1_edit" class="hidden"></textarea>
                                    <?php } ?>
                                </div>
                            </div>
                          <?php } ?>
                        </div>                      
                    </div>

                    <?php } ?>

                    <div class="text-right" id="btns-guardar-actividad" style="display: none;">
                        <a href="#" class="btn btn-info preview pull-left"> Vista Previa</a>
                        <a href="#" class="btn btn-default back pull-left" style="display: none;"> Salir de Vista previa</a>
                        <a href="#" class="btn btn-success btnsaveActividad continue"> Guardar y agregar más ejercicios</a>
                        <a href="#" class="btn btn-success btnsaveActividad"> Guardar y terminar</a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <div class="widget-box">
                    <div class="widget-header bg-red">
                        <h4 class="widget-title">
                            <i class="fa fa-paint-brush"></i>
                            <span><?php echo JrTexto::_('Skills'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row habilidades edicion">
                                <div class="col-xs-12 ">
                                <?php 
                                $ihab=0;
                                if(!empty($this->habilidades))
                                foreach ($this->habilidades as $hab) { $ihab++;?>
                                    <span class="col-xs-12 btn-skill" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></span>
                                <?php } ?>                                  
                                </div>
                            </div>
                            <div class="row" id="info-avance-alumno">
                                <div class="col-xs-12">
                                    <hr>
                                    <a href="#" class="btn btn-blue btn-square">
                                        <div class="btn-label"><?php echo JrTexto::_('advance<br>sessions');?></div>
                                        <span class="btn-information">45</span>
                                        <i class="btn-icon fa fa-play-circle-o"></i>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="#" class="btn btn-green btn-square">
                                        <span class="btn-label"><?php echo JrTexto::_('time');?></span>
                                        <span class="btn-information">04:15</span>
                                        <i class="btn-icon glyphicon glyphicon-time"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="widget-box" id="setting-textbox" style="display: none;">
                    <div class="widget-header bg-blue">
                        <h4 class="widget-title">
                            <i class="fa fa-edit"></i>
                            <span><?php echo JrTexto::_('edit').' '.JrTexto::_('blank'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <ol class="row">
                                <li class="col-xs-12 correct-ans">
                                    <span class="item-title"><?php echo JrTexto::_('correct answer'); ?></span>
                                    <input type="text" class="form-control">
                                </li>
                                <li class="col-xs-12 distractions" style="display: none;">
                                    <span class="item-title">
                                        <?php echo JrTexto::_('distractions'); ?>
                                        <a href="#" class="btn btn-primary btn-xs pull-right add-distraction"> 
                                            <i class="fa fa-plus"></i>
                                            <?php echo JrTexto::_('add'); ?>
                                        </a>
                                    </span>
                                    <input type="text" class="form-control">
                                    <input type="text" class="form-control">
                                    <input type="text" class="form-control">
                                </li>
                                <li class="col-xs-12 assisted" style="display: none;">
                                    <span class="item-title"><?php echo JrTexto::_('assisted exercise'); ?></span>
                                    <input type="checkbox" class="isayuda checkbox-ctrl">
                                </li>
                            </ol>
                            <a href="#" class="btn btn-primary center-block save-setting-textbox">
                                <i class="fa fa-floppy-o"></i> 
                                <?php echo JrTexto::_('save').' '.JrTexto::_('changes'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="actividad-footer">
            <div class="col-xs-12">
                <div class="widget-box">
                    <div class="widget-body widget-none-header">
                        <div class="widget-main">
                            <div class="row ">
                                <div class="col-xs-6 col-sm-2 btn-container ">
                                    <a href="#" class="btn btn-yellow btn-rectangle btnvermodal" data-ventana="teacherresources" >
                                        <span class="btn-label"><?php echo JrTexto::_('Teaching<br>resoruces');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-male"></i>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-2 btn-container ">
                                    <a href="#" class="btn btn-pink btn-rectangle btnvermodal" data-ventana="speakinglabs" >
                                        <span class="btn-label"><?php echo JrTexto::_('Recording');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-microphone"></i>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-2 btn-container ">
                                    <a href="#" class="btn btn-green2 btn-rectangle btnvermodal" data-ventana="workbook">
                                        <span class="btn-label"><?php echo JrTexto::_('Workbook');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-pencil-square"></i>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-2 btn-container ">
                                    <a href="#" class="btn btn-blue btn-rectangle btnvermodal" data-ventana="vocabulary">
                                        <span class="btn-label"><?php echo JrTexto::_('Vocabulary');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-file-text-o"></i>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-2 btn-container">
                                    <a href="#" class="btn btn-info btn-rectangle btnvermodal" data-ventana="games">
                                        <span class="btn-label"><?php echo JrTexto::_('game');?>s</span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-puzzle-piece"></i>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-2 btn-container ">
                                    <a href="#" class="btn btn-lilac btn-rectangle btnvermodal"  data-ventana="links">
                                        <span class="btn-label"><?php echo JrTexto::_('Interest<br>links');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-at"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div> 
    </div>
</div> 
</div>
</form>
<div class="modal fade" id="addinfotxt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="min-height: 600px;">
      <div class="modal-header text-center">
        <button type="button" class="close btncloseaddinfotxt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="addinfotitle"></h4>
      </div>
      <div class="modal-body" id="addinfocontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>
      </div>
    </div>
  </div>
</div>
<div id="tpledittoolbar" class="col-md-12" style="display:none">
    <div class="btn-toolbar" role="toolbar" >
      <div><h4 class="color-blue"><?php echo ucfirst(JrTexto::_('tool')).'s '.JrTexto::_('to edit').' '.JrTexto::_('the template'); ?>:</h4></div>
      <div class="btn btn-primary btnedithtml" id="btnedithtml<?php echo $idgui; ?>"  ><i class="fa fa-text-width"></i> <?php echo ucfirst(JrTexto::_('edit')).' '.JrTexto::_('template'); ?></div>
      <div class="btn btn-primary disabled btnsavehtmlsystem" id="btnsavehtml<?php echo $idgui; ?>"  ><i class="fa fa-save"></i>  <?php echo ucfirst(JrTexto::_('save')).' '.JrTexto::_('template'); ?></div>
      <div class="btn btn-primary btndistribucion"><i class="fa fa-th-large"></i> <?php echo ucfirst(JrTexto::_('divide into')).' 2 '.JrTexto::_('column'); ?>s</div>
      <div class="btn btn-primary vervideohelp"><i class="fa fa-film"></i> <?php echo ucfirst(JrTexto::_('show guide video')); ?> </div>
    </div>
</div>
<div id="btns_guardar_progreso" style="display: none;">
    <div class="row" id="pnl_botones">
        <div class="col-sm-12 botones-control">
          <a class="btn btn-inline btn-success btn-lg save-continue"><?php echo JrTexto::_('save').' '.JrTexto::_('and').' '.JrTexto::_('continue') ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>
<div id="footernoshow" style="display: none">
    <label style="float: left;margin-top: 0.5em;">
    <input type="checkbox" class="nomostrarmodalventana" name="chkDejarDeMostrar" value="1" id="chkDejarDeMostrar">   
     <?php echo ucfirst(JrTexto::_('no show video start')); ?>
    </label>
</div>
<div id="paneles-info" style="display: none;">
    <div id="panel-tiempo" class="panel-info" style="display: none;">
        <span class="titulo-panel"><?php echo ucfirst(JrTexto::_('time')); ?></span>
        <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50">
        <div class="info-show showonpreview" style="display: none;">00</div>
    </div>

    <div id="panel-puntaje" class="panel-info" style="display: none;">
        <span class="titulo-panel"><?php echo ucfirst(JrTexto::_('score')); ?></span>
        <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="puntaje" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 100">
        <div class="info-show showonpreview" style="display: none;">000</div>
    </div>
</div>

<button id="btnActivarinputEdicion" style="display: none"></button>
<audio id="curaudio" dsec="1" style="display: none;"></audio>

<script type="text/javascript">
$(document).ready(function(e){
   var objeto={}
    var tmpmet1=<?php echo json_encode($this->oNegActividad->tplxMetodologia2(1)) ?>;
    var tmpmet2=<?php echo json_encode($this->oNegActividad->tplxMetodologia2(2)) ?>;
    var tmpmet3=<?php echo json_encode($this->oNegActividad->tplxMetodologia2(3)) ?>;

$.fn.testearEjercicios=function(ev){ 
    var objall=$(this); 
    var curmetod;
    var curtabhijo;
    var mostrarBtnGuardarContinuar = function(){
        paneActivo =  $('.nav-pills>li.active>a').attr('href');
        if( $(paneActivo+' .nav-tabs').length>0 ){
            $('#btns-guardar-actividad .btn.continue').show();
        }else{
            $('#btns-guardar-actividad .btn.continue').hide();
        }

        if( !$(paneActivo+' .aquicargaplantilla').is(':empty') ){
            $('#btns-guardar-actividad').show();
        }
    }

    var showHabilidadesActivas = function(idPanelContenedor){
        console.log(idPanelContenedor)
      var cant_input_hidden = $(idPanelContenedor + ' input[type="hidden"].selected-skills').length;
      if( cant_input_hidden == 1){
        var input_hidden_value = $(idPanelContenedor + ' input[type="hidden"].selected-skills').val();
      } else {
        var input_hidden_value = $(idPanelContenedor + ' .tab-content>.tab-pane.active input[type="hidden"].selected-skills').val();
      }

      if( input_hidden_value!='' && input_hidden_value!=undefined ){
        var arr_values = input_hidden_value.split('|');
        $('#actividad-body .widget-box div.habilidades .btn-skill').each(function() {
          $(this).removeClass('active');
          var id_skill = $(this).attr('data-id-skill');
          $.each(arr_values, function(index, elem) {
            $('#actividad-body .widget-box div.habilidades .btn-skill[data-id-skill="'+elem+'"]').addClass('active', {duration:200});
          });
        });
      }
    };

    var preview=function(ev){        
        $('.back').show('fast');
        mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
        actualizarPanelesInfo();
        resetDialogo();  /* para dialogo */
        previewDBYself();/*desde static/js/actividad_completar.js */
        $('.cargarpage').hide();
        $('.nopreview').hide();
        $('.showonpreview').show();
        $('.btn.add-bubble').hide(); /*solo para dialogo.php*/
        $('.valin').each(function(){
            var vp=$(this).val();
            var nodpview=$(this).attr('data-nopreview');
            if(vp==''||vp==undefined){
                $(this).siblings(nodpview).hide().addClass('showreset');
                $(nodpview).hide().addClass('showreset');
            } });
        $('.valvideo').each(function(){
            var src = $(this).attr('src');
            if(src==''||src==undefined) {
                $(this).siblings('img').show();
                $(this).hide();
            } else {
                $(this).siblings('img').hide();
                $(this).show();
            } });
        var contentabsenedicion=$('.actividad-main-content .tabhijo').find('.btnsavehtmlsystem');
        contentabsenedicion.removeClass('enedicionpreview');
        $.each(contentabsenedicion,function(){
                if(!$(this).hasClass('disabled')){
                    $(this).addClass('enedicionpreview');                   
                }
                $(this).trigger('click');
        });
    }

    var iniciarshow=function(ev){medactivo(); mostrarBtnGuardarContinuar();}    
    var finalizotodo=function(ev){ console.log('finalizo'); }
   
    //Ejercicio Activo
    var ejeractivo=function(ev){ curtabhijo=curmetod.attr('id')!='met-1'?curmetod.find('.tabhijo.active'):null; 

        if(curtabhijo==null) showHabilidadesActivas('#'+curmetod.attr('id'));
        else showHabilidadesActivas('#'+curtabhijo.attr('id'));
    }
   
    //Metodologia Activa
    var medactivo=function(ev){ curmetod=objall.find('.metod.active'); ejeractivo()}

    var avanzarmet=function(ev){
        var ulcontent=objall.find('ul.metodologias');
        var index=$('li',ulcontent).index($('li.active',ulcontent));
        var cant_tab = ($('li',ulcontent).length) - 1;
        if(index==cant_tab) finalizotodo();
        else $(' li:eq('+index+') a',ulcontent).tab('show'); }
    var ejeravanzar=function(ev){
        var ulcontent=objall.find('.metod.active').find('ul.ejercicios');
        var index=$('li',ulcontent).index($('li.active',ulcontent));
        var cant_tab = ($('li',ulcontent).length) - 1;
        if(index==cant_tab) avanzarmet();
        else $(' li:eq('+index+') a',ulcontent).tab('show'); }

    objall.on('shown.bs.tab','ul.metodologias li',function(ev){
        medactivo(); 
    });

    objall.on('shown.bs.tab','ul.ejercicios li',function(ev){  
        ejeractivo(); 
    });
   

    //edicion, y manipulacion de tabs;
    var renombrarTabs = function(idulcontent, cant_tabs){
        for(i = 1; i <= cant_tabs; i++){
            $(idulcontent + ' li:nth-child('+i+') a').html(i);
        } };
    objall.on('click','.tab-pane .btn-close-tab', function(ev){
        ev.preventDefault();
        ejeractivo();
        var idhijo=$(this).closest('.tabhijo.active').attr('id');
        var ulcontent=$(this).closest('.metod.active').find('ul.ejercicios');
        var idulcontent='#'+ulcontent.attr('id');
        var index=$('li',ulcontent).index($('li.active',ulcontent));
        $('#'+idhijo).remove(); 
        $('li.active',ulcontent).remove();
        var cant_tab = ($('li',ulcontent).length) - 1;
        if(cant_tab>=1){
            renombrarTabs(idulcontent, cant_tab);
            index=index==cant_tab?index-1:index;
            $(' li:eq('+index+') a',ulcontent).tab('show');
        }
        //actualizarPanelesInfo();
    });
    
    objall.on('click','li.btn-Add-Tab a', function(ev){
        ev.preventDefault();
        medactivo();
        var metid=curmetod.find('.btnchoiceplantilla').attr('data-met');
        var ulcontent=$(this).closest('ul'); 
        var ntabs=$('li',ulcontent).length;
        var indextab=(parseInt(ulcontent.attr('data-totalli'))+1)||ntabs;
        ulcontent.attr('data-totalli',indextab); 
        var addli=ulcontent.find('li:first-child').clone(true,true);
         addli.removeAttr('class')
         addli.find('a').text(ntabs);

        var idpanel='tab-'+ulcontent.attr('data-metod')+'-'+indextab;
         addli.find('a').attr('href','#'+idpanel);        
        $(this).parent().before('<li>'+addli.html()+'</li>');
        var panelcontent=curmetod.find('.tab-content');
        var newpanel=panelcontent.find('.tabhijo:first-child').clone(true,true);
        newpanel.find('.addtexttitulo2').parent().hide();
        newpanel.find('.tab-description').hide();
        newpanel.find('.aquicargaplantilla').html('');
        newpanel.find('input.selected-skills').attr('name','habilidades['+metid+']['+indextab+']');
        newpanel.find('input.selected-skills').attr('id','habilidades_met-'+metid+'_'+indextab);
        newpanel.find('input.selected-skills').val('0');
        addtxtedit='<textarea name="det_texto['+metid+']['+indextab+']" id="tptext_'+metid+'_'+indextab+'" class="hidden"></textarea>'
            +'<textarea name="det_texto_edit['+metid+']['+indextab+']" id="tptext_'+metid+'_'+indextab+'_edit" class="hidden"></textarea>';
        var _newpanel='<div id="'+idpanel+'" class="tabhijo tab-pane fade"><div class="textosave" >'+newpanel.html()+'</div>'+addtxtedit+'</div>';
        panelcontent.append(_newpanel);
        ulcontent.find('a[href="#'+idpanel+'"]').trigger('click');
        curmetod.find('.btnchoiceplantilla').trigger('click',medactivo());
    });

    objall.on('click','.btnchoiceplantilla',function(ev){
        var met=$(this).data('met');
        var tplsel=$(this).data('tpl')||'';         
        var tmp=null;        
            tmp=met==1?tmpmet1:(met==2?tmpmet2:tmpmet3);
        var showtime=tmp.showtime||0;
        var showpuntaje=tmp.showpuntaje||0;
        var timedefualt=tmp.timedefualt||0;
        var _html='<div class="row">';               
        $.each(tmp.tpl,function(i,v){
            var img=v.img||'default';
            var bgcolor=v.color||'#fc5c5c';
            var asistido=v.showasistido||0;
            var alternativas=v.showalternativas||0;
            var tools=v.tools||'';
            var videohelp=v.video||'';
            var addclassinput=v.addclass||'';
            var items=v.items||false;
            if(items){
                _html+='<div class="col-md-12 col-sm-12 col-xs-12"><div class="row tmp-categoria" style="background:'+bgcolor+';"><strong>'+v.name+'</strong></div><div class="row">';
                $.each(items,function(ii,vv){
                    page=vv.page||v.page;
                    name=vv.name||v.name;
                    videoh=vv.video||videohelp;
                    imgi=vv.img||img;
                    addclassinput=vv.addclass||addclassinput;
                    bgcolori=vv.color||bgcolor;
                    toolsi=vv.tools||tools;
                    _html+='<div class="col-md-3 col-sm-6 col-xs-12 text-center seliplantilla" data-url="'+page+'" data-showasistido="'+asistido+'"  data-showalternativas="'+alternativas+'"'
                         +' data-tpltitulo="'+v.name+'-'+name+'"  data-showtime="'+showtime+'"  data-showpuntaje="'+showtime+'" data-puntaje="0" data-time="'+timedefualt+'" data-tools="'+toolsi+'"  data-videohelp="'+videoh+'" data-addclass="'+addclassinput+'" >'
                         +'<div class="btn btn-template" style="background:'+bgcolori+';"><div class="nametpl"><strong>'+name+'</strong></div>'
                         +'<img class="img-responsive" src="'+(_sysUrlBase_+'/static/sysplantillas/'+imgi)+'.gif"></div></div>';
                });
                _html+='</div></div>';
               
            }else{
            _html+='<div class="col-md-3 col-sm-6 col-xs-12 text-center seliplantilla" data-url="'+v.page+'" data-showasistido="'+asistido+'"  data-showalternativas="'+alternativas+'"'
                 +' data-tpltitulo="'+v.name+'" data-showtime="'+showtime+'"  data-showpuntaje="'+showtime+'" data-puntaje="0" data-time="'+timedefualt+'" data-tools="'+tools+'"  data-videohelp="'+videohelp+'" data-addclass="'+addclassinput+'" >'
                 +'<div class="btn btn-template" style="background:'+bgcolor+';"><div class="nametpl"><strong>'+v.name+'</strong></div>'
                 +'<img class="img-responsive" src="'+(_sysUrlBase_+'/static/sysplantillas/'+img)+'.gif">'
                 +'</div></div>';
             }
        });
        _html+='<div class="clearfix"></div></div>'
        var data={
          titulo:'<?php echo JrTexto::_("Select Template"); ?>',
          html:_html,
          ventanaid:'selplantilla-'+met,
          borrar:true
        }
        var modal=sysmodal(data);
        modal.on('click','.seliplantilla',function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var tmpptl=$(this).attr('data-url');
            var asistido=$(this).attr('data-showasistido');
            var alternativas=$(this).attr('data-showalternativas');
            var tools=$(this).attr('data-tools');
            var videohelp=$(this).attr('data-videohelp');
            var addclassinput=$(this).attr('data-addclass');
            var tpltitulo=$(this).attr('data-tpltitulo');
            var showtime=$(this).attr('data-showtime');
            var showpuntaje=$(this).attr('data-showpuntaje');
            var time=$(this).attr('data-time');
            var puntaje=$(this).attr('data-puntaje');
            if(tmp.padre==false){
                var content=$('#met-'+met).find('.aquicargaplantilla');
            }else{
              var content=$('#met-'+met).find('.tabhijo.active').find('.aquicargaplantilla');  
            }            

            var source='<?php echo $this->documento->getUrlSitio() ?>/plantillas/actividad/';
            var orden=1;
            var param_inten='&inten=3';
            modal.find('.cerrarmodal').trigger('click');            
            content.attr('data-tpl',tmpptl);
            content.attr('data-helpvideo',videohelp);
            content.attr('data-showasistido',asistido);
            content.attr('data-showalternativas',alternativas);
            content.attr('data-tools',tools);
            content.attr('data-tpltitulo',tpltitulo);
            content.attr('data-addclass',addclassinput);
            content.attr('data-showtime',showtime);
            content.attr('data-showpuntaje',showpuntaje);
            content.attr('data-time',time);
            content.attr('data-puntaje',puntaje);
            sysaddhtml(content, source+tmpptl+'.php?met='+met+'&ord='+orden+param_inten);
            content.siblings('.tab-title-zone').show();
            content.siblings('.tab-description').show();
            mostrarBtnGuardarContinuar();
            if( $('#met-3').hasClass('active') ){
                $('#met-3 .tab-pane.active').find('.aquicargaplantilla').attr('data-intentos', '<?php echo $this->intentos; ?>');
                mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
            }
            actualizarPanelesInfo(); /* actividad_completar.js */
        });
    });

    objall.on('click', '.btn-skill', function(e){
        medactivo();
        var id_tab_metod = curmetod.attr('id');
        var cant_input_hidden = $('#'+id_tab_metod + ' input[type="hidden"].selected-skills').length;
        if( $('.btn.preview').is(':visible') && cant_input_hidden>=1 ){ //si btn.preview es Visible ó si hay al menos 1 Input Hidden en panel activo
            var id_skill = $(this).data('id-skill');
            $(this).toggleClass('active');
            if(id_tab_metod == 'met-1') var index_tab = '1';
            else var index_tab = $('#'+id_tab_metod).find('ul.nav-tabs>li.active>a').text();
            var input_hidden =  $('#'+id_tab_metod).find('input#habilidades_'+id_tab_metod+'_'+index_tab);
            var input_hidden_val = input_hidden.val().trim(); 
            if( $(this).hasClass('active') ){
                if( input_hidden_val == '0'){ input_hidden_val = id_skill; }
                else{ input_hidden_val += '|'+id_skill; }
            } else {
                arrValues = input_hidden_val.split('|');
                arrValues = jQuery.grep(arrValues, function(value) {//encuentra elemento de arrValues , segun filtro
                    return value != id_skill; //filtra todos los values diferentes de Id_Skill
                });
                input_hidden_val = arrValues.join( "|" );
                if(input_hidden_val=='') input_hidden_val = '0';
            }
            input_hidden.val(input_hidden_val);
        }
    });

    objall.on('click','.preview',function(e){
        e.preventDefault();
        $(this).hide();
        preview();        
    });

    objall.on('click','.back',function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).hide();
        $('.preview').show();
        mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
        activarTabsEjercicios(true); /*desde static/js/actividad_completar.js */
        $('.cargarpage').show();
        $('.nopreview').show();
        $('.showonpreview').hide();
        $('.showreset').show().removeClass('showreset');
        $('.btn.add-bubble[data-info="show"]').show(); /*solo para dialogo.php*/
        $('.valvideo').each(function() {
            var src = $(this).attr('src');
            if(src==''||src==undefined) {
                $(this).siblings('img').show();
                $(this).hide();
            } else {
                $(this).siblings('img').hide();
                $(this).show();
            }
        });
        var contentabsenedicion=$('.tabhijo').find('.btnsavehtmlsystem.enedicionpreview');
        contentabsenedicion.removeClass('enedicionpreview');
        $.each(contentabsenedicion,function(){
               $(this).siblings('.btnedithtml').trigger('click');                
        });
    }); 

    iniciarshow()

   }

   $('#actividad-body').testearEjercicios();

});




</script>
