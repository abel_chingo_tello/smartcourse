<?php defined("RUTA_BASE") or die(); ?><div class="form-view" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('actividades'));?>"><?php echo JrTexto::_('Actividades'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <div class="x_title">
          <a class="btn btn-success btn-xs" href="<?php echo JrAplicacion::getJrUrl(array("Actividades", "agregar"));?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
          <div class="nav navbar-right"></div>
          <div class="clearfix"></div>
        </div>
        <div class="div_linea"></div>
         <div class="x_content">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>                  
                    <th><?php echo JrTexto::_("Titulo") ;?></th>
                    <th><?php echo JrTexto::_("Descripcion") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>                  
                    <td><?php echo $reg["titulo"] ;?></td>
                    <td><?php echo substr($reg["descripcion"],0,100)."..."; ?></td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="<?php echo $reg["idactividad"]; ?>"> <i class="fa fa<?php echo !empty($reg["estado"])?"-check":""; ?>-circle-o fa-lg"></i>  <?php echo $reg["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('actividades'))?>ver/?id=<?php echo $reg["idactividad"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("actividades", "editar", "id=" . $reg["idactividad"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["idactividad"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function () {
  $('.btn-eliminar').bind({   
    click: function() {
       var id=$(this).attr('data-id');
       $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'actividades', 'eliminar', id);
          if(res){
            return redir('<?php echo JrAplicacion::getJrUrl(array('actividades'))?>');
          }
        }
      });     
    }
  });
  
  $('.btn-chkoption').bind({
    click: function() {     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           var res = xajax__('', 'actividades', 'setCampo', id,campo,data);
           if(res) {
            return redir('<?php echo JrAplicacion::getJrUrl(array('actividades'))?>');
           }
        }
      });
    }
  });
  
  $('.table').DataTable( {
    "language": {
            "url": "<?php echo $this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma(); ?>.json"
    }
  });
});
</script>