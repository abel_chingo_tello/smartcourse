
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Examenes_preguntas'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('examenes_preguntas'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idpregunta"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idpregunta"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Idpregunta") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["idpregunta"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idexamen") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idexamen"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Pregunta") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["pregunta"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Descripcion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["descripcion"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Ejercicio") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["ejercicio"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idpadre") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idpadre"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Tiempo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["tiempo"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Puntaje") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["puntaje"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idpersonal") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idpersonal"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fecharegistro") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fecharegistro"] ;?></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('examenes_preguntas'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idpregunta"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idpregunta"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkidpregunta').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
          addFancyAjax("<?php echo JrAplicacion::getJrUrl(array('Examenes_preguntas', 'frm'))?>?tpl=b&acc=Editar&id="+id, true);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Examenes_preguntas', 'eliminarExamenes_preguntas',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                    recargarpagina(false);
                    } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>