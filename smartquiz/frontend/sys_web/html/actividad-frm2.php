<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
$actividades=!empty($this->actividades)?$this->actividades:null; 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
if(!empty($this->datos)) $frm=$this->datos;
?>
<style type="text/css">
    .addtext{
        border-bottom: 1px solid rgba(62, 168, 239, 0.93);
        padding: 1ex 0;
        line-height: 25px
    }
    .addtext>input{
        margin:  1em 0;
        padding: 0 7px;
    }
    @media (min-width: 1250px) and (max-height: 800px){  /* para laptops*/
        .widget-box{
            margin: 0px;
        }
        .widget-main{
            padding: 0px;
        }
        hr{
            margin: 5px;
        }
        .btn-container{
            padding: 5px !important;
        }
    }
</style>
<div id="msj-interno"></div>
<form method="post" id="frm-<?php echo $idgui;?>" class="form-horizontal form-label-left editable" onsubmit="return false;" data-iduser="<?php echo $usuarioAct["dni"] ?>" data-user="<?php echo $usuarioAct["nombre_full"]  ?>" >
<div class="container" data-rol="<?php echo $rolActivo; ?>"> <div class="page-content">
    <div class="row" id="actividad"> <div class="col-xs-12">
        <div class="row" id="actividad-header">
            <input type="hidden" name="txtNivel" id="txtNivel" value="<?php echo $this->idnivel; ?>" >
            <input type="hidden" name="txtunidad" id="txtunidad" value="<?php echo $this->idunidad; ?>" >
            <input type="hidden" name="txtSesion" id="txtsesion" value="<?php echo $this->idsesion; ?>" >
            <div class="col-md-12">
                <ol class="breadcrumb">
                  <li><a href="<?php echo $this->documento->getUrlSitio(); ?>"><?php echo JrTexto::_("Home")?></a></li>
                  <li><a href="<?php echo $this->documento->getUrlSitio(); ?>/recursos/listar/<?php echo $this->idnivel."/".$this->idunidad."/".$this->idsesion; ?>"><?php echo JrTexto::_("Activities")?></a></li>
                  <li><a href="#"><?php echo $this->actividad["nombre"] ?></a></li>
                  <li class="active"><?php echo JrTexto::_("Edit");?></li>
                </ol>
            </div>
        </div>

        <div class="row" id="actividad-body">
            <div class="col-xs-12 col-sm-9">
                <ul class="nav nav-pills  metodologias">
                    <?php 
                    $imet=0;
                    $metcode=null;
                    if(!empty($this->metodologias))
                    foreach ($this->metodologias as $met) { $imet++;?>
                        <li class="<?php echo $imet==1?'active':'';?>">
                        <a href="#met-<?php echo $met["idmetodologia"] ?>" data-toggle="pill"><?php echo JrTexto::_(trim($met["nombre"])); ?></a>
                        </li>
                    <?php $metcode.=$met["idmetodologia"].'|'; } ?>
                    <input name="metodologias" type="hidden" value="<?php echo substr($metcode,0,-1); ?>" >
                </ul>
                <div class="actividad-main-content tab-content">
                    <div id="contenedor-paneles">
                        <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="display: none;">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    <span>0</span>%
                                </div>
                            </div>
                        </div>
                        <div id="panel-changedplantilla" class="eje-panelinfo  text-center" >
                            <div class="btnchoiceplantilla nopreview" data-tpl=""> 
                                <span class="btn btn-primary"><?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('template')?></span>
                            </div>
                        </div>
                        <div id="panel-ejericiosinfo" class="eje-panelinfo text-center" style="display: none;">
                            <span class="btn btn-sm  btn-primary eje-btnanterior"><i class="fa fa-arrow-left"></i></span>
                            <span class="actual" style="padding: 0.5ex 1ex; margin: 0px;">1/3</span>
                            <span class="btn  btn-sm btn-primary eje-btnsiguiente"><i class="fa fa-arrow-right"></i></span>                           
                        </div>
                        <div id="panel-tiempo" class="eje-panelinfo pull-right" style="display: none;">
                            <div class="titulo btn-yellow"><?php echo ucfirst(JrTexto::_('time')); ?></div>
                            <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                            <div class="info-show showonpreview">00</div>
                        </div>
                        <div id="panel-intentos-dby" class="eje-panelinfo text-center pull-right" style="display: none;">
                            <div class="titulo btn-blue"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                            <span class="actual">1</span>
                            <span class="total"><?php echo $this->intentos; ?></span>
                        </div> 
                    </div>

                    <?php 
                    $imet=0;
                    if(!empty($this->metodologias))
                    foreach ($this->metodologias as $met) { $imet++;?>
                                
                    <div id="met-<?php echo $met["idmetodologia"] ?>" class="metod tab-pane fade <?php echo $imet==1?'active in':'';?>" data-idmet="<?php echo $met["idmetodologia"] ?>">
                        <div class="row" >
                        <div class="btnchoiceplantilla nopreview" data-met="<?php echo $met["idmetodologia"] ?>" data-tpl=""> <span class="btn btn-primary"><?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('template')?></span></div>
                        <?php 
                        $tpl=$this->oNegActividad->tplxMetodologia($met["idmetodologia"]);
                        if(!empty($tpl))
                            if($tpl["padre"]==false){
                                if(!empty($this->actividades[$met["idmetodologia"]]["act"]["det"][0]["texto_edit"]))
                                   $texto_edit=$this->actividades[$met["idmetodologia"]]["act"]["det"][0]["texto_edit"];
                                ?>
                            
                            <div class="Ejercicio"><br>
                            <div class="textosave"  data-texto="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>">
                                <?php if(!empty($texto_edit)) {
                                    echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$texto_edit);
                                }else{?>
                                <div class="col-xs-12">
                                    <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"> 
                                        <div class="col-md-12">
                                            <h3 class="addtext addtexttitle<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('title') ?></h3>
                                            <input type="hidden" class="valin nopreview"  name="titulo[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtexttitle<?php echo $met["idmetodologia"]; ?>">
                                        </div>
                                        <div class="space-line pw1_<?php echo $met["idmetodologia"] ?>"></div>
                                    </div>
                                    <div class="tab-description pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"> 
                                        <div class="col-md-12">
                                            <p class="addtext addtextdescription<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('description') ?></p>
                                            <input type="hidden" class="valin nopreview"  name="descripcion[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtextdescription<?php echo $met["idmetodologia"]; ?>" >
                                        </div>
                                    </div>
                                    <div class="aquicargaplantilla"  id="tmppt_<?php echo $met["idmetodologia"] ?>"></div>
                                    <input type="hidden" name="habilidades[<?php echo $met["idmetodologia"] ?>][1]" class="selected-skills" id="habilidades_met-<?php echo $met["idmetodologia"] ?>_1" value="0">
                                </div>
                                <?php } ?>
                            </div>
                            <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>" class="hidden"></textarea>
                            <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_edit" class="hidden"></textarea>
                            </div>
                            <?php }else{ 
                                $texto_edit=null;
                                $ntexto_edit=0;
                                if(!empty($this->actividades[$met["idmetodologia"]]["act"]["det"])){
                                    $texto_edit=$this->actividades[$met["idmetodologia"]]["act"]["det"];
                                    $ntexto_edit=count($texto_edit);
                                }
                                ?>
                            <div class="col-xs-12">
                                <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', $met["nombre"]) ?>" data-id-metod="<?php echo $met["idmetodologia"]; ?>">
                                    <?php if(!empty($ntexto_edit))
                                    for ($im=1 ;$im<=$ntexto_edit;$im++){?>
                                         <li class="<?php echo $im==1?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]).'-'.$im; ?>" data-toggle="tab"><?php echo $im; ?></a></li>
                                     <?php }else{ ?>
                                    <li class="active"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1" data-toggle="tab">1</a></li>
                                    <?php } ?>
                                    <li class="btn-Add-Tab nopreview"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-0" class="istooltip" title="<?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('tab') ?>"><i class="fa fa-plus-circle"></i></a></li>
                                </ul>
                            </div>

                            <div class="col-xs-12">
                                <div class="tab-content" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-main-content">
                                   <?php if(!empty($texto_edit)){
                                        $im=0;
                                        foreach ($texto_edit as $tedit){ $im++?>
                                        <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"])."-".$im; ?>" class="tabhijo tab-pane fade <?php echo $im==1?'active':''; ?> in" >
                                           <div class="textosave" data-texto="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im;  ?>">
                                           <?php echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$tedit["texto_edit"]);?>
                                           </div>
                                           <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][<?php echo $im;?>]; " id="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im; ?>" class="hidden"></textarea>
                                           <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][<?php echo $im;?>]; " id="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im; ;  ?>_edit" class="hidden"></textarea>
                                        </div>
                                       <?php }
                                       }else{ ?>
                                    <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1" class="tabhijo tab-pane fade active in ">
                                        <div class="textosave" data-texto="text_<?php echo $idgui."_".$met["idmetodologia"]."_1";  ?>">
                                        <div class="row"> 
                                          <div class="col-xs-12">
                                            <div class="nopreview" style="position: absolute; right: 1.5em; z-index:10">
                                                <div class="btn-close-tab istooltip" title="<?php echo JrTexto::_('remove').' '.JrTexto::_('tab') ?>" data-id-tab="tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1">
                                                    <button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                            </div>
                                            <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;">
                                                <div class="col-md-12">
                                                    <h3 class="addtext addtexttitulo<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('title') ?></h3>
                                                    <input type="hidden" class="valin nopreview"  name="titulo[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtexttitulo<?php echo $met["idmetodologia"]; ?>">
                                                </div>
                                                <div class="space-line pw1_<?php echo $met["idmetodologia"] ?>"></div>
                                            </div>
                                            <div class="tab-description pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"> 
                                                <div class="col-md-12">
                                                    <p class="addtext addtextdescription<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('description') ?></p>
                                                    <input type="hidden" class="valin nopreview"  name="descripcion[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtextdescription<?php echo $met["idmetodologia"]; ?>" >
                                                </div>
                                            </div>
                                            <div class="aquicargaplantilla" id="tmppt_<?php echo $met["idmetodologia"] ?>_1"></div>
                                            <input type="hidden" name="habilidades[<?php echo $met["idmetodologia"] ?>][1]" class="selected-skills" id="habilidades_met-<?php echo $met["idmetodologia"] ?>_1" value="0">
                                          </div>
                                        </div>
                                        </div>
                                        <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1" class="hidden"></textarea>
                                        <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1_edit" class="hidden"></textarea>
                                    </div>
                                    
                                    <?php } ?>
                                </div>
                            </div>
                          <?php } ?>
                        </div>                      
                    </div>

                    <?php } ?>

                    <div class="text-right" id="btns-guardar-actividad" style="display: none;">
                        <a href="#" class="btn btn-info preview pull-left"> <?php echo JrTexto::_('Preview');?></a>
                        <a href="#" class="btn btn-default back pull-left" style="display: none;"> <?php echo JrTexto::_('Exit preview');?></a>
                        <a href="#" class="btn btn-success btnsaveActividad continue"> <?php echo JrTexto::_('Save and add exercise');?></a>
                        <a href="#" class="btn btn-success btnsaveActividad"> <?php echo JrTexto::_('Save and finish');?></a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <div class="panel panel-info" style="width: 100%; padding: 0.5ex;">
                    <div class="panel-heading text-center autor"><b><?php echo JrTexto::_('Author'); ?> : </b> <span id="infoautor"></span></div>
                </div>
                <div class="widget-box">
                    <div class="widget-header bg-red">
                        <h4 class="widget-title">
                            <i class="fa fa-paint-brush"></i>
                            <span><?php echo JrTexto::_('Skills'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row habilidades edicion">
                                <div class="col-xs-12 ">
                                <?php 
                                $ihab=0;
                                if(!empty($this->habilidades))
                                foreach ($this->habilidades as $hab) { $ihab++;?>
                                    <div class="col-xs-12 col-md-6 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                <?php } ?>                                  
                                </div>
                            </div> 
                        </div>
                    </div>                   
                </div>

                <div class="widget-box" id="info-avance-alumno">                    
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row"  style="margin-bottom: 8px; ">
                                
                                    <div class="col-md-6 btn-container">
                                        <a href="<?php echo $this->documento->getUrlBase()?>/tools/teacherresources/?idnivel=<?php echo $this->idnivel; ?>&idunidad=<?php echo $this->idunidad; ?>&idactividad=<?php echo $this->idsesion; ?>" target="_blank" class="btn btn-yellow  btn-rectangle vertical-center " data-ventana="teacherresources"  style="margin: 0.3ex; min-height: 60px;">
                                            <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Teaching<br>resources');?></span>
                                            <i class="btn-icon fa fa-male" style="font-size: 1.8em;"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-6 btn-container">
                                        <a href="#" class="btn btn-pink btn-rectangle btn-inactive vertical-center" data-ventana="speakinglabs"  style="margin: 0.3ex; min-height: 60px;">
                                            <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Recording');?></span>
                                            <i class="btn-icon fa fa-microphone" style="font-size: 1.8em;"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-6 btn-container">
                                        <a href="#" class="btn btn-green2 btn-rectangle btn-inactive vertical-center" data-ventana="workbook" style="margin: 0.3ex; min-height: 60px;">
                                            <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Workbook');?></span>
                                            <i class="btn-icon fa fa-pencil-square" style="font-size: 1.8em;"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-6 btn-container">
                                        <a href="#" class="btn btn-info btn-rectangle btnvermodal vertical-center" data-ventana="games" style="margin: 0.3ex; min-height: 60px;">
                                            <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('game');?>s</span>
                                            <i class="btn-icon fa fa-puzzle-piece" style="font-size: 1.8em;"></i>
                                        </a>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="widget-box" id="setting-textbox" style="display: none;">
                    <div class="widget-header bg-blue">
                        <h4 class="widget-title">
                            <i class="fa fa-edit"></i>
                            <span><?php echo JrTexto::_('edit').' '.JrTexto::_('blank'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <ol class="row">
                                <li class="col-xs-12 correct-ans">
                                    <span class="item-title"><?php echo JrTexto::_('correct answer'); ?></span>
                                    <input type="text" class="form-control">
                                </li>
                                <li class="col-xs-12 distractions" style="display: none;">
                                    <span class="item-title">
                                        <?php echo JrTexto::_('distractors'); ?>
                                        <a href="#" class="btn btn-primary btn-xs pull-right add-distraction"> 
                                            <i class="fa fa-plus"></i>
                                            <?php echo JrTexto::_('add'); ?>
                                        </a>
                                    </span>
                                    <input type="text" class="form-control">
                                    <input type="text" class="form-control">
                                    <input type="text" class="form-control">
                                </li>
                                <li class="col-xs-12 assisted" style="display: none;">
                                    <span class="item-title"><?php echo JrTexto::_('assisted exercise'); ?></span>
                                    <input type="checkbox" class="isayuda checkbox-ctrl">
                                </li>
                            </ol>
                            <a href="#" class="btn btn-primary center-block save-setting-textbox" >
                                <i class="fa fa-floppy-o"></i> 
                                <?php echo JrTexto::_('save').' '.JrTexto::_('changes'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                
    </div> 
    </div>
</div> 
</div>
</form>

<div class="modal fade" id="addinfotxt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="min-height: 500px;">
      <div class="modal-header text-center">
        <button type="button" class="close btncloseaddinfotxt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="addinfotitle"></h4>
      </div>
      <div class="modal-body" id="addinfocontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>
      </div>
    </div>
  </div>
</div>
<div id="tpledittoolbar" class="col-md-12" style="display:none">
    <div class="btn-toolbar" role="toolbar" >
      <div><h4 class="color-blue"><?php echo ucfirst(JrTexto::_('tool')).'s '.JrTexto::_('to edit').' '.JrTexto::_('the template'); ?>:</h4></div>
      <div class="btn btn-primary btnedithtml" id="btnedithtml<?php echo $idgui; ?>"  ><i class="fa fa-text-width"></i> <?php echo ucfirst(JrTexto::_('edit')).' '.JrTexto::_('template'); ?></div>
      <div class="btn btn-primary disabled btnsavehtmlsystem" id="btnsavehtml<?php echo $idgui; ?>"  ><i class="fa fa-save"></i>  <?php echo ucfirst(JrTexto::_('save')).' '.JrTexto::_('template'); ?></div>
      <div class="btn btn-primary btndistribucion"><i class="fa fa-th-large"></i> <?php echo ucfirst(JrTexto::_('divide into')).' 2 '.JrTexto::_('column'); ?>s</div>
      <div class="btn btn-primary vervideohelp"><i class="fa fa-film"></i> <?php echo ucfirst(JrTexto::_('show guide video')); ?> </div>
    </div>
</div>

<div id="btns_progreso_intentos" style="display: none;">
    <div class="row" id="btns_control_alumno">
        <div class="col-sm-12 botones-control">
            <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
            <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>
<div id="footernoshow" style="display: none">
    <label style="float: left;margin-top: 0.5em;">
    <input type="checkbox" class="nomostrarmodalventana" name="chkDejarDeMostrar" value="1" id="chkDejarDeMostrar">   
     <?php echo ucfirst(JrTexto::_('Do not show this again')); ?>
    </label>
</div>

<div id="paneles-info" style="display: none;">
    <div id="panel-tiempo" class="panel-info" style="display: none;">
        <span class="titulo-panel"><?php echo ucfirst(JrTexto::_('time')); ?></span>
        <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50">
        <div class="info-show showonpreview" style="display: none;">00</div>
    </div>
    <!--
    <div id="panel-puntaje" class="panel-info hidden" style="display: none;">
        <span class="titulo-panel"><?php // echo ucfirst(JrTexto::_('score')); ?></span>
        <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="puntaje" placeholder="<?php //echo JrTexto::_('e.g.'); ?>: 100">
        <div class="info-show showonpreview" style="display: none;">000</div>
    </div>
    -->
</div>

<button id="btnActivarinputEdicion" style="display: none"></button>
<audio id="curaudio" dsec="1" style="display: none;"></audio>
<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
</section>
<script type="text/javascript">

$(document).ready(function(e){
    var gbl_INTENTOS = '<?php echo $this->intentos; ?>';
    var gbl_IDGUI = '<?php echo $idgui; ?>';
    var TMPL = [];
    TMPL[0] = <?php echo json_encode($this->oNegActividad->tplxMetodologia2(1)) ?>;
    TMPL[1] = <?php echo json_encode($this->oNegActividad->tplxMetodologia2(2)) ?>;
    TMPL[2] = <?php echo json_encode($this->oNegActividad->tplxMetodologia2(3)) ?>;
    var _curmetod=null; //Metodologia Activa
    var _curejercicio=null; //Ejercicio activo
    var _cureditable=false; //indica si estamos vista de edicion
    var _curdni=null;

    actualizarPanelesInfo(); /* actividad_completar.js */
    cargarMensajesPHP();


    $('.istooltip').tooltip();

    var mostrarbtnremovetab=function(){
        $('.tab-pane.metod').each(function(ii,vv){
            tabhijo=$(".tabhijo",this);
            if(tabhijo.length==1){
                $(".btn-close-tab",tabhijo).hide(0);
              }else{
                $(".btn-close-tab",tabhijo).show(0);
            } 
        });
    }
    var EjercicioActivo=function(){
        _curmetod=$('.actividad-main-content .metod.active');
        _curejercicio=_curmetod.attr('id')!='met-1'?_curmetod.find('.tabhijo.active'):_curmetod.find('.Ejercicio'); 
        _cureditable=$('form.editable').length;        
    }

     $('#actividad').on('click','[data-audio]',function(){
        var srcaudio=$(this).attr('data-audio');
        srcaudio=srcaudio.trim();
        if(srcaudio!=undefined||srcaudio!=''){
            srcaudio=_sysUrlBase_+'/static/media/audio/'+srcaudio;
            $('#curaudio').attr('src',srcaudio);
            $('#curaudio').trigger('play');
        }
    });

    $('.btnvermodal').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var ventana=$(this).attr('data-ventana');
        var claseid=ventana+gbl_IDGUI;
        var titulo=$(this).find('.btn-label').html();
        titulo=titulo.toString().replace('<br>',' ');
        var paramadd='&idnivel='+$('#txtNivel').val()+'&idunidad='+$('#txtunidad').val()+'&idactividad='+$('#txtsesion').val();
        var url=_sysUrlBase_ +'/tools/' +ventana+'/?plt=modal'+paramadd;
        var modaldata={
          titulo:titulo,
          url:url,
          ventanaid:'ventana-'+ventana,
          borrar:false
        }
        var modal=sysmodal(modaldata);
    });
    
    /*************** solo Edicion */

    $('.actividad-main-content').on('click',".metod.active .btnselectedfile",function(e){ 
        e.preventDefault();
        var txt=MSJES_PHP.select_upload;
        selectedfile(e,this,txt);
    });

    var alcargarplantilla=function(){
        EjercicioActivo();
        var aquicargaplantilla =_curejercicio.find('.aquicargaplantilla');
        aquicargaplantilla.attr('data-iduser',$('form-'+gbl_IDGUI).attr('data-iduser'));
        var tpl=aquicargaplantilla.attr('data-tpl');
        var idgui=$('input#idgui',aquicargaplantilla).val();
        var plantilla=$('.plantilla',aquicargaplantilla);
        plantilla.addClass('editando');
        var metodActual = plantilla.parents('.metod.active').attr('id');
        if(plantilla.hasClass('plantilla_video')){
            $('.btnselectedfile[data-tipo="video"]',plantilla).trigger('click');
        }else if(plantilla.hasClass('plantilla_audio')){
            $('.btnselectedfile[data-tipo="audio"]',plantilla).trigger('click');
        }else if(plantilla.hasClass('plantilla_imagen_audio')){
            $('.btnselectedfile.selimage',plantilla).trigger('click');
        }else if(plantilla.hasClass('plantilla-dialogo')){
            $('.btnselectedfile.selimage',plantilla).trigger('click');
        }else if(tpl=='pra_arrastre_texto'){
             iniciarCompletar_Practice(idgui);
        }else if(plantilla.hasClass('plantilla-verdad_falso')){
            $('a.btn.add-premise',plantilla).trigger('click');
            mostrarBtnsDinamicos(idgui);
        }else if(plantilla.hasClass('plantilla-fichas')){
            initFichas(idgui);
        }else if(plantilla.hasClass('plantilla-ordenar ord_simple')){
            mostrarBtnsDinamicos(idgui);
        }else if(plantilla.hasClass('plantilla-ordenar ord_parrafo')){
            mostrarBtnsDinamicos(idgui);
            if(metodActual == 'met-2') { var help_parr = true; }
            else { var help_parr = false; }
            initOrdenarParrafos(idgui, "en_US", help_parr);
        }else if(plantilla.hasClass('plantilla-img_puntos')){
            mostrarBtnsDinamicos(idgui);
        }
        mostrarBotonesguardardoc();
        addBtnsControlAlumno(true);
        $('.istooltip').tooltip();
    };

    var verautor=function(){
       EjercicioActivo();
        var iduserplantilla= $('.aquicargaplantilla',_curejercicio).find('#sysiduser').val();
        var iduserActual=$('#frm-'+gbl_IDGUI).attr('data-iduser');
        if(iduserplantilla==iduserActual||iduserplantilla==undefined){
            _curdni=iduserActual;
            var user=$('#frm-'+gbl_IDGUI).attr('data-user');
            $('.autor span#infoautor').html(user);
        }else{
            if(_curdni==iduserplantilla)return false;
            _curdni=iduserplantilla;
            try{
                var formData = new FormData();
                formData.append("dni",iduserplantilla);
                $.ajax({
                      url: '<?php echo $this->documento->getUrlBase(); ?>/usuario/buscarjson/',
                      type: "POST",
                      data:  formData,
                      contentType: false,
                      processData: false,
                      dataType :'json',
                      cache: false,
                      processData:false,
                      beforeSend: function(XMLHttpRequest){
                        $('.autor span#infoautor').html('<?php echo JrTexto::_('Loading'); ?>');
                      },
                      success: function(data){     
                        if(data.code==='ok'){
                            var datainfo=data.data;
                            var nombre_full=datainfo.ape_paterno+' '+datainfo.ape_materno+' '+datainfo.nombre;
                            $('.autor span#infoautor').html(nombre_full);
                        }else{
                            $('.autor span#infoautor').html('<?php echo JrTexto::_('Anonymous'); ?>');
                        }
                      },
                      error: function(e){
                        $('.autor span#infoautor').html('<?php echo JrTexto::_('Error'); ?>');
                      }
                });
            }catch(ex){}            
        }
    }
    var reordenarplantillas=function(){ //reordenra antes de guardar
        $('.textosave').each(function(){
            var idmet=$(this).closest('.metod').attr('id');
            var met=$('#'+idmet).attr('data-idmet');
            var tpl=$(this).find('.aquicargaplantilla');
            var atexto=$(this).attr("data-texto");
            var idgui=$(this).find('input#idgui');
            var iduser=$(this).find('input#sysiduser');
            var dettipo=$(this).find('input#sysdettipo').val();           
            var iduserActual=$('#frm-'+gbl_IDGUI).attr('data-iduser');
            var sysiduser=($(iduser).length==0)?iduserActual:iduser.val();
            var sysidgui=($(idgui).length==0)?gbl_IDGUI:idgui.val();
            var sysdettipo=(dettipo==''||dettipo==undefined)?$('ul.metodologias li a[href="#'+idmet+'"]').text():dettipo;
            var systipodesarrollo=$(tpl).attr('data-tpltitulo');
            var txtords=atexto.split("_");
            var orden=txtords.pop();
            var inputs='<input type="hidden" id="sysidorden" name="orden['+met+']['+orden+']" value="'+orden+'">'
                        +'<input type="hidden" id="sysdettipo" name="det_tipo['+met+']['+orden+']" value="'+sysdettipo+'">'
                        +'<input type="hidden" id="systipodesarrollo" name="tipo_desarrollo['+met+']['+orden+']" value="'+systipodesarrollo+'">'
                        +'<input type="hidden" id="systipoactividad" name="tipo_actividad['+met+']['+orden+']" value="A">'
                        +'<input type="hidden" id="sysiduser" name="iduser['+met+']['+orden+']" value="'+sysiduser+'">'
                        +'<input type="hidden" id="idgui" name="idgui" value="'+sysidgui+'">';
            $(tpl).children('input').remove();
            validartxt=$(tpl).text();
            $(tpl).prepend(inputs);            
            var html_=$(this).clone();
            html_.find('.btn-toolbar').remove();
            html_.find('#btns_control_alumno').remove();
            $('#'+atexto+'_edit').val(html_.html());
            html_.find('.plantilla').removeClass('editando');
            $('.valin',html_).each(function(){
                var vp=$(this).val();
                $(this).attr('value', vp);
                if(vp==''||vp==undefined) {
                    var dpview=$(this).attr('data-nopreview');
                    $(this).siblings(dpview).remove();
                }
                $(this).remove();
            });
            $('.nopreview',html_).remove();
            $('div.tooltip.fade[role="tooltip"]').remove();
            if(validartxt.trim()!='')
            $('#'+atexto).val(html_.html());
        });
    }

    $('.btnchoiceplantilla').click(function(){
        var met=$(this).attr('data-met');
        var tplsel=$(this).attr('data-tpl')||''; 
        var tmp1=TMPL[0];
        var tmp2=TMPL[1];
        var tmp3=TMPL[2];
        var tmp=null;        
            tmp=met==1?tmp1:(met==2?tmp2:tmp3);
        var showtime=tmp.showtime||0;
        var showpuntaje=tmp.showpuntaje||0;
        var timedefualt=tmp.timedefualt||0;
        var _html='<div class="row">';               
        $.each(tmp.tpl,function(i,v){
            var img=v.img||'default';
            var bgcolor=v.color||'#fc5c5c';
            var asistido=v.showasistido||0;
            var alternativas=v.showalternativas||0;
            var tools=v.tools||'';
            var videohelp=v.video||'';
            var addclassinput=v.addclass||'';
            var items=v.items||false;
            if(items){
                _html+='<div class="col-md-12 col-sm-12 col-xs-12"><div class="row tmp-categoria" style="background:'+bgcolor+';"><strong>'+v.name+'</strong></div><div class="row">';
                $.each(items,function(ii,vv){
                    page=vv.page||v.page;
                    name=vv.name||v.name;
                    videoh=vv.video||videohelp;
                    imgi=vv.img||img;
                    addclassinput=vv.addclass||addclassinput;
                    bgcolori=vv.color||bgcolor;
                    toolsi=vv.tools||tools;
                    _html+='<div class="col-md-3 col-sm-6 col-xs-12 text-center seliplantilla" data-url="'+page+'" data-showasistido="'+asistido+'"  data-showalternativas="'+alternativas+'"'
                         +' data-tpltitulo="'+v.name+'-'+name+'"  data-showtime="'+showtime+'"  data-showpuntaje="'+showtime+'" data-puntaje="0" data-time="'+timedefualt+'" data-tools="'+toolsi+'"  data-videohelp="'+videoh+'" data-addclass="'+addclassinput+'" >'
                         +'<div class="btn btn-template" style="background:'+bgcolori+';"><div class="nametpl"><strong>'+name+'</strong></div>'
                         +'<img class="img-responsive" src="'+(_sysUrlBase_+'/static/sysplantillas/'+imgi)+'.gif"></div></div>';
                });
                _html+='</div></div>';
               
            }else{
            _html+='<div class="col-md-3 col-sm-6 col-xs-12 text-center seliplantilla" data-url="'+v.page+'" data-showasistido="'+asistido+'"  data-showalternativas="'+alternativas+'"'
                 +' data-tpltitulo="'+v.name+'" data-showtime="'+showtime+'"  data-showpuntaje="'+showtime+'" data-puntaje="0" data-time="'+timedefualt+'" data-tools="'+tools+'"  data-videohelp="'+videohelp+'" data-addclass="'+addclassinput+'" >'
                 +'<div class="btn btn-template" style="background:'+bgcolor+';"><div class="nametpl"><strong>'+v.name+'</strong></div>'
                 +'<img class="img-responsive" src="'+(_sysUrlBase_+'/static/sysplantillas/'+img)+'.gif">'
                 +'</div></div>';
             }
        });
        _html+='<div class="clearfix"></div></div>'
        var data={
          titulo: MSJES_PHP.select_activity,
          html:_html,
          ventanaid:'selplantilla-'+met,
          borrar:true
        }
        var modal=sysmodal(data);
        modal.on('click','.seliplantilla',function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var tmpptl=$(this).attr('data-url');
            var asistido=$(this).attr('data-showasistido');
            var alternativas=$(this).attr('data-showalternativas');
            var tools=$(this).attr('data-tools');
            var videohelp=$(this).attr('data-videohelp');
            var addclassinput=$(this).attr('data-addclass');
            var tpltitulo=$(this).attr('data-tpltitulo');
            var showtime=$(this).attr('data-showtime');
            var showpuntaje=$(this).attr('data-showpuntaje');
            var time=$(this).attr('data-time');
            var puntaje=$(this).attr('data-puntaje');
            if(tmp.padre==false){
                var content=$('#met-'+met).find('.aquicargaplantilla');
            }else{
              var content=$('#met-'+met).find('.tabhijo.active').find('.aquicargaplantilla');  
            } 
            var source=sitio_url_base+'/plantillas/actividad/';
            var orden=1;
            var param_inten='&inten=3';
            modal.find('.cerrarmodal').trigger('click');            
            content.attr('data-tpl',tmpptl);
            content.attr('data-helpvideo',videohelp);
            content.attr('data-showasistido',asistido);
            content.attr('data-showalternativas',alternativas);
            content.attr('data-tools',tools);
            content.attr('data-tpltitulo',tpltitulo);
            content.attr('data-addclass',addclassinput);
            content.attr('data-showtime',showtime);
            content.attr('data-showpuntaje',showpuntaje);
            content.attr('data-time',time);
            content.attr('data-puntaje',puntaje);
            sysaddhtml(content, source+tmpptl+'.php?met='+met+'&ord='+orden+param_inten,'',alcargarplantilla);
            content.siblings('.tab-title-zone').show();
            content.siblings('.tab-description').show();
            if( $('#met-3').hasClass('active') ){
                $('#met-3 .tab-pane.active').find('.aquicargaplantilla').attr('data-intentos', gbl_INTENTOS);
                mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
            }
            actualizarPanelesInfo(); /* actividad_completar.js */
        });
    });
    

    $('.actividad-main-content').on('click', '.addtext', function(e){
            if(!$('.btnsaveActividad').is(':visible')){
                return false;
            }
            $(this).css({"border": "0px"}); 
            addtext1(e,this);
      }).on('blur','.addtext>input',function(e){
            e.preventDefault();
            $(this).closest('.addtext').removeAttr('Style'); 
            addtext1blur(e,this);
      }).on('keypress','.addtext>input', function(e){
            if(e.which == 13){ //Enter pressed
              e.preventDefault();          
              $(this).trigger('blur');
            } 
      }).on('keyup','input', function(e){
            if(e.which == 27){ //Esc pressed
              $(this).attr('data-esc',"1");
              $(this).trigger('blur');
            }
      });

   
    var resetImagen_Audio = function(){
        var data_audio = $('.playaudioimagen').attr('data-audio');
        if(data_audio!=undefined && data_audio!=''){
            $('.plantilla_video .playaudioimagen').removeClass('hidden');
        }
    };
    
    var resetDialogo = function(){
        $('.plantilla-dialogo .dialogue-container .tpanedialog.tab-pane ').removeClass('active in') ;
        $('.plantilla-dialogo .dialogue-container .tpanedialog.tab-pane:first').addClass('active in') ;

        $('.plantilla-dialogo .dialogue-pagination>ul>li').removeClass('active');
        $('.plantilla-dialogo .dialogue-pagination>ul>li:first').addClass('active');

               
        /* audioStop() */
        $(".plantilla-dialogo #curaudiodialogo").attr('src', '');
        $(".plantilla-dialogo #curaudiodialogo").trigger("stop");
        $('.plantilla-dialogo #playdialogo').attr('data-estado', 'stopped');
        if( $('.plantilla-dialogo #playdialogo i').hasClass('fa-stop') ) {
          $('.plantilla-dialogo #playdialogo i').removeClass('fa-stop').addClass('fa-play');
        }
        /* fin audioStop() */
    };

    var resetAlternativas = function(){
        $('.plantilla-marcar_respuesta .question-alternatives .correct').removeClass('correct');
        $('.plantilla-marcar_respuesta .question-alternatives .warning').removeClass('warning');
    };

    var resetVerdadFalso = function(){
        $('.list-premises input[type="radio"].radio-ctrl').prop('checked', false);
        $('.list-premises .valin').each(function(){ 
            var vp=$(this).val();
            if(vp==''||vp==undefined) { 
                var dpview=$(this).attr('data-nopreview');
                $(dpview).remove();
            }
        });
        reiniciarVoF( $('.list-premises') );
        $('.list-premises .icon-result').removeAttr('data-corregido');
        $('.list-premises .icon-result').html('');
    };

    var resetgenerador=function(){
        $('.btnsavehtmlsystem').trigger('click');
        $('.inner-icon').remove();
    };

    var resetOrdenar = function(){
        if( $('.plantilla.plantilla-ordenar').length>0 ){
            $('.plantilla.plantilla-ordenar').each(function() {
                var id = $(this).attr('id');
                $('#'+id+' .generar-parrafo').trigger('click');
                $('#'+id+' .generar-elem').trigger('click');
            });
        }
    };

    var resetFichas = function(){
        if( $('.plantilla.plantilla-fichas').length>0 ){
            $('.plantilla.plantilla-fichas').each(function() {
                var idTmpl = $(this).attr('id');
                $('#'+idTmpl+'  .generar-fichas').trigger('click');
            });
        }
    };

    var resetImgPuntos = function(){
        if( $('.plantilla.plantilla-img_puntos').length>0 ){
            $('.plantilla.plantilla-img_puntos').each(function() {
                var id = $(this).attr('id');
                $('#'+id+' .generate-tag').trigger('click');
            });
        }
    };

    $('.btnsaveActividad').click(function(event){
        event.preventDefault();
        event.stopPropagation();
        try{  
            $('.preview').trigger('click');
            resetDialogo();
            resetAlternativas();  
            resetgenerador();         
            previewDBYself();
            reordenarplantillas();

            var paramsPHP = {'idgui' : gbl_IDGUI,'msje' : { 'attention' : MSJES_PHP.attention,},};
            guardaractividaddetalle(paramsPHP); /* editactividad.js */
            $('.back').trigger('click');
            if( $(this).hasClass('continue') ) {
                $('.metod.active .nav-tabs.ejercicios .btn-Add-Tab>a').trigger('click');
            }
        }catch(err){
            mostrar_notificacion(MSJES_PHP.attention,err,'warning');
        }
    });

    $("form").keypress(function(e) {
        if (e.which == 13) {
            return false;
        }
    });
   
    $('.actividad-main-content').on('shown.bs.tab', 'ul.nav-tabs.ejercicios li a', function (e) {
        e.preventDefault();        
        var panelShown = '#'+$(this).attr('href').replace('#', '');
        mostrarHabilidadesActivas(panelShown);
        if($(panelShown+' .aquicargaplantilla').is(':empty')) {
            $('#btns-guardar-actividad').hide();
        } else {
            $('#btns-guardar-actividad').show();
        }
        resetinfoTextbox();
        mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
        actualizarPanelesInfo(); /* actividad_completar.js */
        verautor();
    });

    $('#actividad-body').on('shown.bs.tab', 'ul.nav-pills.metodologias li a', function (e) {
        var href=$(this).attr('href');
        var panelShown = '#'+href.replace('#', '');
        mostrarHabilidadesActivas(panelShown);
        if($(panelShown+' .aquicargaplantilla').is(':empty')) { /*si está vacío*/
            $('#btns-guardar-actividad').hide();
            $(panelShown+' .btnchoiceplantilla').trigger('click'); /*cargar selector de plantillas */
        }else{
           mostrarBotonesguardardoc();
        }
        resetinfoTextbox();
        mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
        actualizarPanelesInfo(); /* actividad_completar.js */
        verautor();
    });

    $('#actividad-body div.habilidades')
        .on('click', '.btn-skill', function(e){
            var id_TabMetodologia = '#' + $('#actividad-body ').find('div.actividad-main-content>.metod.active').attr('id');
            var cant_input_hidden = $(id_TabMetodologia + ' input[type="hidden"].selected-skills').length;
            if( $('.btn.preview').is(':visible') && cant_input_hidden>=1 ){ //si btn.preview es Visible ó si hay al menos 1 Input Hidden en panel activo
                var id_skill = $(this).data('id-skill');
                $(this).toggleClass('active');
                var id_tab_metod = $('.actividad-main-content').find('.metod.tab-pane.active').attr('id');

                if(id_tab_metod == 'met-1') var index_tab = '1';
                else var index_tab = $('#'+id_tab_metod).find('ul.nav-tabs>li.active>a').text();

                var input_hidden =  $('#'+id_tab_metod).find('input#habilidades_'+id_tab_metod+'_'+index_tab);
                var input_hidden_val = input_hidden.val().trim();
                
                if( $(this).hasClass('active') ){
                    if( input_hidden_val == '0'){ input_hidden_val = id_skill; }
                    else{ input_hidden_val += '|'+id_skill; }
                } else {
                    arrValues = input_hidden_val.split('|');
                    arrValues = jQuery.grep(arrValues, function(value) {//encuentra elemento de arrValues , segun filtro
                        return value != id_skill; //filtra todos los values diferentes de Id_Skill
                    });
                    input_hidden_val = arrValues.join( "|" );
                    if(input_hidden_val=='') input_hidden_val = '0';
                }
                input_hidden.val(input_hidden_val);
            }
        });
    
    $('.preview').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $('.preview').hide().removeClass('active');
        $('.back').show('fast');
        $('.btnsaveActividad').hide();        
        mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
        actualizarPanelesInfo();
        $tabhijo=$('.actividad-main-content .tabhijo');
        $.each($tabhijo,function(i,obj){
            var btnsave=$(obj).find('.btnsavehtmlsystem');
            if($(btnsave).length>0 && !$(btnsave).hasClass('disabled')){$(btnsave).trigger('click');}
            $(btnsave).addClass('enedicionpreview');
            var plantilla=$(obj).find('.plantilla');
            plantilla.removeClass('editando');
            $('#btns_control_alumno a',plantilla).show();
            if( $(this).closest('.metod').attr('id')=='met-3' ) $('#btns_control_alumno a.save-progreso',plantilla).hide();
            if(plantilla.hasClass('plantilla-dialogo')) resetDialogo();
            else if(plantilla.hasClass('plantilla-verdad_falso')) resetVerdadFalso();
            else if(plantilla.hasClass('plantilla-fichas')) resetFichas(); 
            else if(plantilla.hasClass('plantilla-ordenar')) resetOrdenar();
            else if(plantilla.hasClass('plantilla-img_puntos')) resetImgPuntos();
            //else if(plantilla.hasClass('plantilla-ordenar')) resetOrdenar();
        });
        previewDBYself();/*desde static/js/actividad_completar.js */
        $('.plantilla.ejerc-terminado-correcto').removeClass('ejerc-terminado-correcto ejerc-iniciado tiempo-pausado tiempo-acabo');
        $('video').stop();
        $('audio').stop();
        $('.nopreview').hide();
        $('.showonpreview').show();
        $('.btn.add-bubble').hide(); /*solo para dialogo.php*/
        $('.valin').each(function(i,obj){
            var vp=$(obj).val().toString().trim();
            var nodpview=$(obj).attr('data-nopreview');
            $(obj).hide('fast');
            if(vp==''||vp===""||vp==undefined){
                if(nodpview!='.addtext'){
                $(obj).siblings(nodpview).hide().addClass('showreset');
                $(nodpview).hide().addClass('showreset');               
                }
            }else{
                $(obj).siblings(nodpview).show('fast');
                $(nodpview).show('fast');
            }
        });
        $('.valvideo').each(function(){
            var src = $(this).attr('src');
            if(src==''||src==undefined) {
                $(this).siblings('img').show();
                $(this).hide();
            } else {
                $(this).siblings('img').hide();
                $(this).show();
            }
        });

        //plantilla imagen_audio
        var data_audio = $('.plantilla_imagen_audio .playaudioimagen').attr('data-audio');
        if(data_audio!=undefined && data_audio!=''){
            $('.plantilla_imagen_audio .playaudioimagen').removeClass('hidden');
        }else{
            $('.plantilla_imagen_audio .playaudioimagen').addClass('hidden');
        }

        $('.plantilla-completar .btn-toolbar').hide();

        /*$('.plantilla-verdad_falso').each(function() {
            var $pnlPremisas = $(this).find('.list-premises');
            reiniciarVoF($pnlPremisas);
        });*/
    });

    $('.back').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).hide();
        $('.preview').show().addClass('active');
        mostrarBotonesguardardoc();
        mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
        backDBY();
        resetOrdenar();
        $('video').stop();
        $('audio').stop();
        $('#panel-tiempo .info-show').trigger('oncropausar');
        $('.nopreview').show();
        $('.showonpreview').hide();
        $('.showreset').show().removeClass('showreset');
        $('.btn.add-bubble[data-info="show"]').show(); /*solo para dialogo.php*/
        $('.valvideo').each(function() {
            var src = $(this).attr('src');
            if(src==''||src==undefined) {
                $(this).siblings('img').show();
                $(this).hide();
            } else {
                $(this).siblings('img').hide();
                $(this).show();
            }
        });

        $tabhijo=$('.actividad-main-content .tabhijo');
        $.each($tabhijo,function(i,obj){
            var btnsave=$(obj).find('.btnsavehtmlsystem.enedicionpreview');
            if($(btnsave).length>0 && !$(btnsave).hasClass('disabled')){
                $(btnsave).removeClass('enedicionpreview');
                $(btnsave).siblings('.btnedithtml').trigger('click');
            }
            var plantilla=$(obj).find('.plantilla');
            plantilla.addClass('editando');  
            $('#btns_control_alumno a',plantilla).hide();         
        });        
        vistaEdicion();
        $('.plantilla-completar .btn-toolbar').show();
        //var data_audio = $('.plantilla_imagen_audio .playaudioimagen').attr('data-audio');
        //if(data_audio==undefined||data_audio==''){
            $('.plantilla_imagen_audio .playaudioimagen').addClass('hidden');
        //}
    });

    seleccionarmedia=function(e,tipo,donde){ //faltaria modicar
        if(tipo==''||tipo==undefined)return false;
        var rutabiblioteca=sitio_url_base+'/biblioteca/?plt=tinymce&robj=afile&donde='+donde+'&type='+tipo;
        $('#addinfotitle').html(MSJES_PHP.select_upload+tipo);
        $('#addinfocontent').load(rutabiblioteca);
        $('#addinfotxt').modal('show');
        $('#addinfotxt').show();
    };

    var nuevoPanel = function(index, id_metodologia, nomb_metod){
        var identif = id_metodologia+'_'+index;
        var intentos = (id_metodologia==3)?' data-intentos="'+gbl_INTENTOS+'" ':'';
        return '<div class="row"> '+
                  '<div class="col-xs-12">'+
                    '<div class="nopreview" style="position: absolute; right: 1.5em; z-index:10">'+
                        '<div class="btn-close-tab istooltip" title="'+MSJES_PHP.remove_tab+'" data-id-tab="tab-'+nomb_metod+'-'+index+'">'+
                              '<button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                        '</div>'+
                    '</div>'+
                    '<div class="tab-title-zone pw1_'+id_metodologia+'" style="display:none;"> '+                        
                        '<div class="col-md-12">'+
                            '<h3 class="addtext addtexttitulo2">'+
                            MSJES_PHP.click_add_title+
                            '</h3>'+
                            '<input type="hidden" class="valin nopreview"  name="titulo['+id_metodologia+']['+index+']" data-nopreview=".addtexttitulo2">'+
                        '</div>'+
                        '<div class="space-line pw1_'+id_metodologia+'"></div>'+
                    '</div>'+
                    '<div class="tab-description pw1_'+id_metodologia+'" style="display:none;"> '+
                        '<div class="col-md-12">'+
                            '<p class="addtext addtextdescription2'+id_metodologia+'">'+ 
                            MSJES_PHP.click_add_description+
                            '</p>'+
                            '<input type="hidden" class="valin nopreview"  name="descripcion['+id_metodologia+']['+index+']" data-nopreview=".addtextdescription2'+id_metodologia+'" >'+
                        '</div>'+
                    '</div>'+            
                    '<div class="aquicargaplantilla" id="tmppt_'+identif+'"></div>'+
                    '<input type="hidden" name="habilidades['+id_metodologia+']['+index+']" class="selected-skills" id="habilidades_met-'+identif+'" value="0">'+
                  '</div>'+
                '</div>';
    };

    $('li.btn-Add-Tab a').on('click', function(e){
        e.preventDefault();
        var method=$(this).closest('.metod.active');
        var id_ulContenedor= '#' + $(this).parent().parent().attr('id');
        var id_PanelesContenedor = '#' + $(id_ulContenedor).parent().next().find('.tab-content').attr('id');
        var nombre_metodologia = $(id_ulContenedor).attr('data-metod');
        var id_metodologia = $(id_ulContenedor).attr('data-id-metod');
        var ultimo_tab = $(this).parent();
        var cant_tab = $(id_ulContenedor+' li').length;
        if(cant_tab == 1) { index_ultimo_tab = 2; } else { index_ultimo_tab = cant_tab;} //Si Solo queda boton Add Tab
        var a_href_tab = $(id_ulContenedor+' li:nth-child('+(index_ultimo_tab-1)+') a').attr('href').split('-'); //#tab-Metodologia-ultimoIndex
        var index_NewTab = parseInt(a_href_tab[2]) + 1; //ultimoIndex + 1
        /* agregando nuevo tab */
        var newTab = '<li> <a href="#tab-'+nombre_metodologia+'-'+index_NewTab+'" data-toggle="tab">'+ cant_tab +'</a> </li>';
        ultimo_tab.before(newTab);

        /* agregando Tab Panel */
        var newPanel = '<div id="tab-'+nombre_metodologia+'-'+index_NewTab+'" class="tabhijo tab-pane fade " >'+
                         '<div class="textosave" data-texto="tptext_'+id_metodologia+'_'+index_NewTab+'" >'+ 
                            nuevoPanel(index_NewTab, id_metodologia, nombre_metodologia) +
                        '</div><textarea name="det_texto['+id_metodologia+']['+index_NewTab+']" id="tptext_'+id_metodologia+'_'+index_NewTab+'" class="hidden">'+
                        '</textarea>'+
                        '<textarea name="det_texto_edit['+id_metodologia+']['+index_NewTab+']" id="tptext_'+id_metodologia+'_'+index_NewTab+'_edit" class="hidden"></textarea>'+
                        '</div>';
        $(id_PanelesContenedor).append(newPanel);
        
        //actualizarplantillas();


        /* mostrar Tab agregado*/
        $(id_ulContenedor+' a[href="#tab-'+nombre_metodologia+'-'+index_NewTab+'"]').tab('show'); 
        mostrarHabilidadesActivas('#tab-'+nombre_metodologia+'-'+index_NewTab);
        mostrarbtnremovetab();
        $('#met-'+id_metodologia).find('.btnchoiceplantilla').trigger('click');
    });

    var renombrarTabs = function(id_ulContenedor, cant_tabs){
        for(i = 1; i <= cant_tabs; i++){
            $(id_ulContenedor + ' li:nth-child('+i+') a').html(i);
        }
    };

    $('.tab-content').on('click', '.tab-pane .btn-close-tab', function(e){
        e.preventDefault();
        var idhijo=$(this).closest('.tabhijo.active').attr('id');
        var method=$(this).closest('.metod.active');
        var ulcontent=method.find('ul.ejercicios');
        var idulcontent='#'+ulcontent.attr('id');
        var index=$('li',ulcontent).index($('li.active',ulcontent));
        $('#'+idhijo).remove(); 
        $('li.active',ulcontent).remove();
        var cant_tab = ($('li',ulcontent).length) - 1;      
        if(cant_tab>=1){
            renombrarTabs(idulcontent, cant_tab);
            index=index==cant_tab?index-1:index;
            $(' li:eq('+index+') a',ulcontent).tab('show');
            if(cant_tab==1){
                method.find('.tab-title-zone .btn-close-tab').hide('fast'); 
            };
        }
        mostrarbtnremovetab();
        actualizarPanelesInfo();
    });

    /**** manupulacion de inputs **iniinputs****/
    var initSettingTextbox = function(obj, hasDistraction=false, hasAssist=false){
        obj=$(obj);       
        $('#info-avance-alumno').hide();
        var txt=obj.attr('data-texto').toString();
        $('#setting-textbox .correct-ans input').val(txt.trim());
        $('#setting-textbox .correct-ans input').attr('data-objeto',obj.attr('id'));
        $('#setting-textbox .distractions ').hide();
        $('#setting-textbox .assisted ').hide();
        $('#setting-textbox .assisted input.isayuda').prop("checked",false);
        if(hasDistraction&&hasDistraction!='0') { 
             $('#setting-textbox .distractions input').remove();           
             var options=obj.attr('data-options');
             var iopt=0;
             try{
                var option=options.split(",");             
                $.each(option,function(i,v){
                    iopt++;
                    $('#setting-textbox .distractions').append('<input value="'+v+'" class="form-control">');
                });
             }catch(ex){} 
             for(var i = iopt; i <3 ; i++) {
                $('#setting-textbox .distractions').append('<input value="" class="form-control">'); 
             }
             $('#setting-textbox .distractions').show();
        }
        if( hasAssist&&hasAssist!='0' ) {
            $('#setting-textbox .assisted').show();
            $('#setting-textbox .assisted input.isayuda').prop("checked", obj.hasClass('isayuda')?true:false);
        }
        $('#setting-textbox').show('fast');
        obj.addClass('_enedicion_');
         $('#setting-textbox .correct-ans input').focus();
    };

    var resetSettingTextbox = function(){
        $('#setting-textbox input[type="text"]').removeAttr('value').val('');
        $('#setting-textbox input.added').remove();
        $('#setting-textbox input[type="checkbox"]').prop( "checked", false );
        $('#setting-textbox .distractions').hide();
        $('#setting-textbox .assisted').hide();        
    };

    var resetinfoTextbox=function(){
        resetSettingTextbox();
        $('#setting-textbox').hide('fast');
        $('#info-avance-alumno').removeClass('editandoinput').show('fast');
    }

    var saveSettingTextbox = function(){
        var response = {
            'asistido'      : $('#setting-textbox .assisted input').is(':checked'),
            'correcta'      : $('#setting-textbox .correct-ans input').val(),
            'distraccion'   : [],
        };
        $('#setting-textbox .distractions input').each(function() {
            if( $(this).val().trim()!='' ) response['distraccion'].push( $(this).val().trim() );
        });
        return response;
    };

    var saveTextboxEdit=function(){
        var infotemplate=$('.metod.active .tabhijo.active .aquicargaplantilla');
        if(!infotemplate.find('.btnedithtml').hasClass('disabled')) return false;
        var response = saveSettingTextbox();
        tinyMCE.triggerSave();        
        var acteditor=tinyMCE.activeEditor.getBody();
        var objenedit= $('#setting-textbox .correct-ans input').attr('data-objeto');
        var actobj=$(acteditor).find('._enedicion_');
        if(actobj.length>0){        
            var addclassinput=infotemplate.attr('data-addclass')||false;
            actobj.attr('data-texto',response.correcta);
            actobj.attr('data-mce-selected','1');
            actobj.removeClass('_enedicion_ isdrop isclicked iswrite isayuda ischoice');
            actobj.addClass(addclassinput);           
            if(response.asistido) actobj.addClass('isayuda');
            if(response.distraccion){
                var options=response.distraccion.join(",");
                actobj.attr('data-options',options);
            }
            var html=actobj.prop('outerHTML');
            $(actobj).replaceWith(html);
        }
       resetinfoTextbox();
    };

    $('#setting-textbox .add-distraction').click(function(e) {
        e.preventDefault();
        $('#setting-textbox .distractions').append('<input type="text" class="form-control added">');
        $('#setting-textbox .distractions').children(':last-child').focus();
    });

    $('#setting-textbox .save-setting-textbox').click(function(e) {
        e.preventDefault();
        saveTextboxEdit();       
    });

    $('#btnActivarinputEdicion').click(function(){
        $('#info-avance-alumno').addClass('editandoinput');
        var acteditor=tinyMCE.activeEditor.getBody();
        var obj=$(acteditor).find('._enedicion_');
        if($(obj).hasClass('mce-object-input')){
            var infotemplate=$('.metod.active .tabhijo.active .aquicargaplantilla');
            var asistido=infotemplate.attr('data-showasistido')||false;
            var alternativas=infotemplate.attr('data-showalternativas')||false;
            initSettingTextbox(obj, alternativas, asistido );
        }else{            
            $(obj).addClass('_mce_modalobjectenedicion_');
        }
    });

    //plantilla completar
    $('.actividad-main-content').tplcompletar();


//Plantilla de Verdadero y falso
  
   
    /** EJECUCIÓN del ejerc Fichas :
     ** (copiar este codigo para actvidad.php de alumno) 
    **/
     
//plantilla look_video_texto lectura
/*
  if($('#vid_2<?php //echo $met."_".$idgui ?>').attr('src')!=''){ //no es necesario aqui
    $('tr').removeClass('inlectura');
    $('#vid_2<?php //echo $met."_".$idgui ?>').trigger('play');
  }*/
 
   

//Plantilla Ordenar Simple
    $("*[data-tooltip=\"tooltip\"]").tooltip();
     
    var addBtnsControlAlumno = function(active){
        var active=active||false;
        var addtxt=active==false?'':'.active';   
        var html = $('#btns_progreso_intentos').clone().html();    
        $('.metod'+addtxt+' .tabhijo'+addtxt+' .plantilla #btns_control_alumno').remove();
        $('.metod'+addtxt+' .tabhijo'+addtxt+' .plantilla').each(function(i,v){            
            var met=$(v).closest('.metod').attr('data-idmet');
            var $html = $(html);
            if(met<3) $html.find('.btn.try-again').remove();
            if(met==3) $html.find('.btn.save-progreso').hide();
            $('.botones-control a',$html).hide();
            $(v).append($html);
        });     
    };

    var mostrarBotonesguardardoc=function(){
        EjercicioActivo();
        //if(_cureditable==1 && $('.btn.preview').hasClass('active')){           
            $('.actividad-main-content #btns-guardar-actividad').show();
            $('.actividad-main-content #btns-guardar-actividad .btnsaveActividad').show(); 
            if(_curmetod.attr('id')=='met-1')$('.actividad-main-content #btns-guardar-actividad .continue').hide();
            else $('.actividad-main-content #btns-guardar-actividad .continue').show();
        /*}else{
           $('.actividad-main-content #btns-guardar-actividad').hide(); 
        }*/
    }

    <?php if(!empty($actividades)){?> 
        addBtnsControlAlumno();
        mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
    <?php }?>
    EjercicioActivo();
    if(_cureditable==1){
        $('.btn.back').trigger('click');
        mostrarbtnremovetab();
        mostrarBotonesguardardoc();       
        var txttmp=$('.aquicargaplantilla',_curejercicio).text().toString().trim();               
        if(txttmp=='')$('.btnchoiceplantilla',_curmetod).trigger('click');
        verautor();
    }

});
</script>
