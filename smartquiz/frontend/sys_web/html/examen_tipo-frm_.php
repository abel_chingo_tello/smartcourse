<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Examen_tipo'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdtipo" id="pkidtipo" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Tipo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTipo" name="txtTipo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["tipo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                  <a href="javascript:;"  class="btn-chkoption" campo="estado" > <i class="fa fa<?php echo !empty($frm["estado"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo @$frm["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a>
                 <input type="hidden" name="txtEstado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveExamen_tipo" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('examen_tipo'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Examen_tipo', 'saveExamen_tipo', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Examen_tipo"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Examen_tipo"))?>');
        <?php endif;?>       }
     }
  });

 $('.btn-chkoption').bind({
    click: function(){
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      if(data===0){
         $("i",this).addClass('fa-circle-o').removeClass('fa-check-circle-o');       
      }else
        $("i",this).removeClass('fa-circle-o').addClass('fa-check-circle-o');
      $(this).siblings('input').val(data);
      
    }
  });  
});


</script>

