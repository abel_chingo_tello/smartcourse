<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
$actividades=$this->actividades; 
if(!empty($this->datos)) $frm=$this->datos;
?>
<div id="msj-interno"></div>
<form method="post" id="frm-<?php echo $idgui;?>" class="form-horizontal form-label-left" onsubmit="return false;" >
<div class="main-container"> <div class="page-content">
    <div class="row" id="actividad"> <div class="col-xs-12">
        <div class="row" id="actividad-header">
            <h1 class="col-xs-12 actividad-title">
                 <?php echo JrTexto::_('Level') ?>
                 <select id="txtNivel" name="txtNivel" class="act_select">
                <?php
                $selidpadre=$this->idnivel;
                foreach ($this->niveles as $nivel) { ?>
                <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $selidpadre===$nivel["idnivel"]?'selected="selected"':''?>><?php echo $nivel["nombre"]?> </option>
                <?php } ?>
                </select> -
                <select id="txtunidad" name="txtunidad" class="act_select">
                  <?php 
              $selidpadre=$this->idunidad;
              foreach ($this->unidades as $unit) { ?>
                <option value="<?php echo $unit["idnivel"]; ?>" <?php echo $selidpadre===$unit["idnivel"]?'selected="selected"':''?>><?php echo $unit["nombre"]?> </option>
              <?php } ?> 
                </select> - 
                <select id="txtsesion" name="txtSesion" class="act_select">
                <?php
               $selidpadre1=$this->idsesion;
                foreach ($this->sesiones as $ses) { ?>
                <option value="<?php echo $ses["idnivel"]; ?>" <?php echo $selidpadre1===$ses["idnivel"]?'selected="selected"':''?>><?php echo $ses["nombre"]?> </option>
                <?php } ?>
                </select> 
                <div class="btn-group pull-right">
                     <button class="btn btn-primary btnsaveActividad tooltip" title="<?php echo JrTexto::_('Save All') ?>"><i class="fa fa-floppy-o"></i></button>
                     <button class="btn btn-info preview tooltip" title="<?php echo JrTexto::_('preview') ?>"><i class="fa fa-eye"></i></button>
                     <button class="btn btn-default back tooltip" title="<?php echo JrTexto::_('back to edit') ?>" style="display: none;"><i class="fa fa-arrow-left" ></i></button>
                </div>
            </h1>            
        </div>
        <div class="row" id="actividad-body">
            <div class="col-xs-12 col-sm-9">
                <ul class="nav nav-pills  metodologias">
                    <?php 
                    $imet=0;
                    $metcode=null;
                    if(!empty($this->metodologias))
                    foreach ($this->metodologias as $met) { $imet++;?>
                        <li class="<?php echo $imet==1?'active':'';?>">
                        <a href="#met-<?php echo $met["idmetodologia"] ?>" data-toggle="pill"><?php echo $met["nombre"] ?></a>
                        </li>
                    <?php 
                    $metcode.=$met["idmetodologia"].'|';
                    }?>
                    <input name="metodologias" type="hidden" value="<?php echo substr($metcode,0,-1); ?>" >
                </ul>
                <div class="actividad-main-content tab-content">
                    <?php 
                    $imet=0;
                    if(!empty($this->metodologias))
                    foreach ($this->metodologias as $met) { $imet++; $texto_edit=null; ?>                    
                    <div id="met-<?php echo $met["idmetodologia"] ?>" class="metod tab-pane fade <?php echo $imet==1?'active in':'';?>">
                        <div class="row" >
                        <?php $tpl=$this->oNegActividad->tplxMetodologia($met["idmetodologia"]);
                        if(!empty($tpl))
                            if($tpl["padre"]==false){
                                if(!empty($this->actividades[$met["idmetodologia"]]["act"]["det"][0]["texto_edit"]))
                                   $texto_edit=$this->actividades[$met["idmetodologia"]]["act"]["det"][0]["texto_edit"];                   
                                ?>
                                <div class="textosave"  data-texto="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>">
                                <?php if(!empty($texto_edit)) {
                                    echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$texto_edit);
                                }else{?>
                                <div class="col-xs-12">
                                    <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" > 
                                        <div class="col-md-12">
                                            <h3 class="addtext addtexttitle<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('title') ?></h3>
                                            <input type="hidden" class="valin nopreview"  name="titulo[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtexttitle<?php echo $met["idmetodologia"]; ?>">
                                        </div>
                                    </div>
                                    <div class="space-line pw1_<?php echo $met["idmetodologia"] ?>"></div>
                                    <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" > 
                                        <div class="col-md-12">
                                            <p class="addtext addtextdescription<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('description') ?></p>
                                            <input type="hidden" class="valin nopreview"  name="descripcion[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtextdescription<?php echo $met["idmetodologia"]; ?>" >
                                        </div>
                                    </div>
                                    <br> 
                                    <select id="txtplantilla" name="txtPlantilla" class="act_select cargarpage nopreview" data-source="<?php echo $this->documento->getUrlSitio() ?>/plantillas/actividad/" data-container="<?php echo $met["idmetodologia"] ?>">
                                        <option value=""><?php echo JrTexto::_('select template') ?></option>
                                        <?php foreach($tpl['tpl'] as $key=>$val): ?>
                                            <option value="<?=$key?>"><?=$val?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <div id="tmppt_<?php echo $met["idmetodologia"] ?>">
                                        <div></div>
                                    </div>
                                    <input type="hidden" name="habilidades[<?php echo $met["idmetodologia"] ?>][1]" class="selected-skills" id="habilidades_met-<?php echo $met["idmetodologia"] ?>_1" value="0">
                                </div>
                                <?php } ?>
                            </div>
                            <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>" class="hidden"></textarea>
                            <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_edit" class="hidden"></textarea>
                            <?php }else{ 
                                     $texto_edit=$this->actividades[$met["idmetodologia"]]["act"]["det"];
                                     $ntexto_edit=count($texto_edit);
                                ?>
                            <div class="col-xs-12">
                                <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', $met["nombre"]) ?>" data-id-metod="<?php echo $met["idmetodologia"]; ?>">
                                    <?php if(!empty($ntexto_edit))
                                    for ($im=1 ;$im<=$ntexto_edit;$im++){?>
                                         <li class="<?php echo $im==1?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]).'-'.$im; ?>" data-toggle="tab"><?php echo $im; ?></a></li>
                                     <?php }else{ ?>
                                    <li class="active"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1" data-toggle="tab">1</a></li>
                                    <?php } ?>
                                    <li class="btn-Add-Tab nopreview"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-0" class="tooltip" title="<?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('tab') ?>"><i class="fa fa-plus-circle"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12">                             
                                <div class="tab-content" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-main-content">
                                    <?php 
                                    if(!empty($texto_edit)){
                                        $im=0;
                                        foreach ($texto_edit as $tedit){ $im++?>
                                        <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"])."-".$im; ?>" class="tab-pane fade <?php echo $im==1?'active':''; ?> in textosave" 
                                    data-texto="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im;  ?>">
                                           <?php echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$tedit["texto_edit"]);?>
                                         </div>  
                                        <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][<?php echo $im;?>]; " id="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im; ?>" class="hidden"></textarea>
                                        <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][<?php echo $im;?>]; " id="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im; ;  ?>_edit" class="hidden"></textarea>
                                       <?php }
                                       }else{ ?>
                                    <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1" class="tab-pane fade active in textosave" 
                                    data-texto="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1">
                                       <?php  var_dump($texto_edit[0]["texto_edit"]); ?>
                                        <div class="row"> 
                                          <div class="col-xs-12">
                                            <div style="position: absolute; right: 1.5em; ">
                                                <div class="btn-close-tab tooltip nopreview" title="<?php echo JrTexto::_('remove') ?> <?php echo JrTexto::_('tab') ?>" data-id-tab="tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1">
                                                  <button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                            </div>
                                            <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" > 
                                                <div class="col-md-12">
                                                    <h3 class="addtext addtexttitulo<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('title') ?></h3>
                                                    <input type="hidden" class="valin nopreview"  name="titulo[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtexttitulo<?php echo $met["idmetodologia"]; ?>">
                                                </div>
                                            </div>
                                            <div class="space-line pw1_<?php echo $met["idmetodologia"] ?>"></div>
                                            <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" > 
                                                <div class="col-md-12">
                                                    <p class="addtext addtextdescription<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('description') ?></p>
                                                    <input type="hidden" class="valin nopreview"  name="descripcion[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtextdescription<?php echo $met["idmetodologia"]; ?>" >
                                                </div>
                                            </div>
                                            <br>
                                            <select id="txtplantilla" name="txtPlantilla" class="act_select cargarpage nopreview" data-source="<?php echo $this->documento->getUrlSitio() ?>/plantillas/actividad/" data-container="<?php echo $met["idmetodologia"] ?>_1">
                                                <option value=""><?php echo JrTexto::_('select template') ?></option>
                                                <?php foreach($tpl['tpl'] as $key=>$val): ?>
                                                    <option value="<?=$key?>"><?=$val?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div id="tmppt_<?php echo $met["idmetodologia"] ?>_1">
                                                <div></div>
                                            </div>
                                            <input type="hidden" name="habilidades[<?php echo $met["idmetodologia"] ?>][1]" class="selected-skills" id="habilidades_met-<?php echo $met["idmetodologia"] ?>_1" value="0">                                          
                                          </div>
                                        </div>
                                    </div>
                                    <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1" class="hidden"></textarea>
                                    <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1_edit" class="hidden"></textarea>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>                      
                    </div>                    
                    <?php } ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <div class="widget-box">
                    <div class="widget-header bg-red">
                        <h4 class="widget-title">
                            <i class="fa fa-paint-brush"></i>
                            <span><?php echo JrTexto::_('Skill'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row habilidades">
                                <div class="col-xs-12 ">
                                <?php 
                                $ihab=0;
                                if(!empty($this->habilidades))
                                foreach ($this->habilidades as $hab) { $ihab++;?>
                                    <span class="col-xs-12 btn-skill" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo JrTexto::_($hab['nombre']); ?></span>
                                <?php } ?>                                  
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#" class="btn btn-blue btn-square">
                                        <div class="btn-label"><?php echo JrTexto::_('advance<br>sessions');?></div>
                                        <span class="btn-information">45</span>
                                        <i class="btn-icon fa fa-play-circle-o"></i>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="#" class="btn btn-green btn-square">
                                        <span class="btn-label"><?php echo JrTexto::_('time');?></span>
                                        <span class="btn-information">04:15</span>
                                        <i class="btn-icon glyphicon glyphicon-time"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="row" id="actividad-footer">
             <div class="col-xs-12">
                <div class="widget-box">
                    <div class="widget-body widget-none-header">
                        <div class="widget-main">
                            <div class="row ">
                                <div class="col-xs-6 col-sm-2 btn-container">
                                    <a href="#" class="btn btn-yellow btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('teaching<br>resoruces');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-male"></i>
                                    </a>
                                </div>

                                <div class="col-xs-6 col-sm-2 btn-container">
                                    <a href="#" class="btn btn-pink btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('recording');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-microphone"></i>
                                    </a>
                                </div>

                                <div class="col-xs-6 col-sm-2 btn-container">
                                    <a href="#" class="btn btn-green2 btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('workbook');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-pencil-square"></i>
                                    </a>
                                </div>

                                <div class="col-xs-6 col-sm-2 btn-container">
                                    <a href="#" class="btn btn-blue btn-rectangle" data-toggle="modal" data-target="#modalVocabulary">
                                        <span class="btn-label"><?php echo JrTexto::_('vocabulary');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-file-text-o"></i>
                                    </a>
                                </div>

                                <div class="col-xs-6 col-sm-2 btn-container">
                                    <a href="#" class="btn btn-green btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('dictionary');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-book"></i>
                                    </a>
                                </div>

                                <div class="col-xs-6 col-sm-2 btn-container">
                                    <a href="#" class="btn btn-lilac btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('interest<br>links');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-at"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> 
    </div>
</div> 
</div>
</form>

<div class="modal fade" id="addinfotxt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 id="addinfotitle"></h2>
      </div>
      <div class="modal-body" id="addinfocontent">
        <h4><i class="fa fa-cog fa-spin fa-fw"></i> <?php echo JrTexto::_('Loading') ?>...</h4>
      </div>
      <!--
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      -->
    </div>
  </div>
</div>
<audio id="curaudio" dsec="1" style="display: none;"></audio>
<script>
$('.tooltip').tooltip();
$(document).on('ready', function(e){
    $('.actividad-main-content')
      .on('click', '.addtext', function(e){
            if( $('.preview').is(':visible')){ //si btn.preview es Visible:estamos en edicion
              var in_=$(this);
              var texto_inicial = $(this).text().trim();
              $(this).attr('data-initial_val', texto_inicial);
              in_.html('<input type="text" required="required" class="_intxtadd form-control" value="'+ texto_inicial +'">');
              var cini=$('input',in_).parent().siblings('input.valin').attr('data-cini');
              if(cini==''||cini==undefined){
                $('input',in_).parent().siblings('input.valin').attr('data-cini',1);
                $('input',in_).parent().siblings('input.valin').attr('data-valueini',texto_inicial);
              }
              $('input',in_).focus().select();
            }
      })
      .on('blur','.addtext>input',function(e){
            e.preventDefault();
            var dataesc=$(this).attr('data-esc'); 
            var vini=$(this).parent().attr('data-initial_val');
            var vin=$(this).val();
            var dvaliniin=$(this).parent().siblings('input.valin').attr('data-valueini');
            if(dataesc) _intxt=vini;
            else _intxt=vin;
            if(_intxt==dvaliniin)vin='';
            if(vin)$(this).parent().siblings('data-cini',2);
            if(_intxt=='')_intxt = dvaliniin;
            $(this).removeAttr('data-esc');
            $(this).parent().attr('data-initial_val',_intxt);
            $(this).parent().siblings('input.valin').val(vin);       
            var vpara=$(this).parent().attr('data-valpara');
            if(vpara!=''&&vpara!=undefined){
                var vdonde=$(this).parent().attr('data-valen');
                if(vdonde!=''&&vdonde!=undefined) $(vpara).attr(vdonde,_intxt);
            }
             $(this).parent().text(_intxt);
      })
      .on('keypress','.addtext>input', function(e){
            if(e.which == 13){ //Enter pressed
              e.preventDefault();          
              $(this).trigger('blur');
            } 
      })
      .on('keyup','input', function(e){
            if(e.which == 27){ //Esc pressed
              $(this).attr('data-esc',"1");
              $(this).trigger('blur');
            }
      });
    
    $('.tab-content').on('click', '.addtexto', function(e){
        var info=$(this).attr('data-info');
        var donde=$(this).attr('data-adonde');
        var source=$(this).attr('data-source');
        e.preventDefault(); 
        source='<?php echo $this->documento->getUrlSitio() ?>/plantillas/actividad/'+source;
        $('#addinfotitle').html(info);
        $('#addinfocontent').load(source+'.php?adonde='+donde);
        $('#addinfotxt').modal('show');
    });
    $( ".word-space, .word-list" ).sortable({
        connectWith: ".word"
    }).disableSelection();

    $('.tab-content').on('change', '.cargarpage', function(e){
         e.preventDefault();
         e.stopPropagation();
         var source = $(this).attr("data-source");
         var container = $(this).attr("data-container");
         var tmpptl=$(this).val();
         var metod_orden = container.split('_');
         if(metod_orden[1]==undefined) metod_orden[1] = 1;
         if(source&&container&&tmpptl){
            $('#tmppt_'+container).html('<h4><i class="fa fa-cog fa-spin fa-fw"></i> <?php echo JrTexto::_('Loading') ?>...</h4>');
            $('#tmppt_'+container).load(source+tmpptl+'.php?met='+metod_orden[0]+'&ord='+metod_orden[1], function(response, status, xhr){
                $('#tmppt_'+container).html(response); /*al terminar de cargar el Resource*/
            });
         }
    });
    
    $('#txtNivel').change(function(){
        var idnivel=$(this).val();
        $('#txtunidad option').remove();
        var data={tipo:'U','idpadre':idnivel}
        var res = xajax__('', 'niveles', 'getxPadre',data);
        if(res){ 
          $.each(res,function(){
            x=this;
            $('#txtunidad').append('<option value='+x["idnivel"]+'>'+x["nombre"]+'</option>');
          });
           $('#txtunidad').trigger('change');
        }
      });
  
    $('#txtunidad').change(function(){
        var idunidad=$(this).val();
        $('#txtsesion option').remove();
        var data={tipo:'L','idpadre':idunidad}
        var res = xajax__('', 'niveles', 'getxPadre',data);
        if(res){ 
            $('#txtsesion').append('<option value="0">Selected</option>');
          $.each(res,function(){
            x=this;
            $('#txtsesion').append('<option value='+x["idnivel"]+'>'+x["nombre"]+'</option>');
          });  
        }
    });

    $('#txtsesion').change(function(){
         var idsec=$(this).val();
         if(idsec>0){
             var data={nivel:$('#txtNivel').val(),unidad:$('#txtunidad').val(),sesion:idsec};
             var resact=xajax__('', 'actividad', 'Exiteactividad',data);
             console.log(resact);
          if(resact){
            return redir('<?php echo JrAplicacion::getJrUrl(array('actividad','editar'))?>?idnivel='+$('#txtNivel').val()+"&txtUnidad="+$('#txtunidad').val()+"&txtsesion="+idsec);
          }else{
            <?php if($this->accion=='edit') {?>
            return redir('<?php echo JrAplicacion::getJrUrl(array('actividad','agregar'))?>');
            <?php } ?>
          }
         }
    });

    var resetDialogo = function(){
        $('.plantilla-dialogo .tpanedialog').removeClass('active').removeClass('in');
        $('.plantilla-dialogo .dialogue-pagination>ul>li>a[data-toggle="pill"]:first')
            .trigger('click') ;
        $('.plantilla-dialogo .tpanedialog:first').addClass('active in');

        /*$('.dialogue-boxes .valin').each(function(){
            var vp=$(this).val();
            var nodpview=$(this).attr('data-nopreview');
            if(vp==''||vp==undefined){                
                $(this).siblings(nodpview).hide().addClass('showreset');
                $(nodpview).hide().addClass('showreset');
            }

        });*/

        /* audioStop() */
        $(".plantilla-dialogo #curaudiodialogo").attr('src', '');
        $(".plantilla-dialogo #curaudiodialogo").trigger("stop");
        $('.plantilla-dialogo #playdialogo').attr('data-estado', 'stopped');
        if( $('.plantilla-dialogo #playdialogo i').hasClass('fa-stop') ) {
          $('.plantilla-dialogo #playdialogo i').removeClass('fa-stop').addClass('fa-play');
        }
        /* fin audioStop() */
    };

    var resetAlternativas = function(){
        $('.plantilla-marcar_respuesta .question-alternatives .correct').removeClass('correct');
        $('.plantilla-marcar_respuesta .question-alternatives .warning').removeClass('warning');
    };

    var resetVerdadFalso = function(){
        $('.list-premises input[type="radio"].radio-ctrl').prop('checked', false);
        $('.list-premises .valin').each(function(){ 
            var vp=$(this).val();
            if(vp==''||vp==undefined) { 
                var dpview=$(this).attr('data-nopreview');
                console.log(dpview);
                $(dpview).remove();
            }
        }); 
    };

    var resetgenerador=function(){
        $('.btnsavehtmlsystem').trigger('click');
        $('.inner-icon').remove();
    }

    $('.btnsaveActividad').click(function(event){
          event.preventDefault();
          //resetDialogo();  /* para dialogo */
          resetAlternativas();
          resetVerdadFalso();
          resetgenerador();
          $('.textosave').each(function(){
            var atexto=$(this).attr("data-texto");
            var html_=$(this).clone();
            $('#'+atexto+'_edit').val(html_.html());

            $('.valin',html_).each(function(){                 
                var vp=$(this).val();           
                if(vp==''||vp==undefined) {                
                    var dpview=$(this).attr('data-nopreview');
                    $(this).siblings(dpview).remove();
                }
                $(this).remove();
            });           
            $('.nopreview',html_).remove();
           
            $('#'+atexto).val(html_.html());
          });
          $('#frm-<?php echo $idgui;?>').submit();
    });

    $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){ 
          //reset html generador tinymice     
         event.preventDefault();
         $('.preview').trigger( "click" );
         var formData = new FormData($("#frm-<?php echo $idgui;?>")[0]);
         $.ajax({
            url: '<?php echo $this->documento->getUrlBase(); ?>/actividad/guardar',
            type: "POST",
            data:  formData,
            contentType: false,
            dataType :'json',
            cache: false,
            processData:false,
            success: function(data)
            {
               console.log(data);
               if(data.code==='ok'){
                mostrar_notificacion('<?php echo JrTexto::_('Attention 1');?>',data.msj,'success');                     
              }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention 2');?>',data.msj,'warning');
                return false;
              }
              $('.back').trigger( "click" );
            },
            error: function(xhr, status, error) 
              { 
                console.log(xhr);            
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',status,'warning');
                return false;
                 $('.back').trigger( "click" );
              }
               
          });
     }
    });

    $("form").keypress(function(e) {
        if (e.which == 13) {
            return false;
        }
    });

    var mostrarHabilidadesActivas = function(idPanelContenedor){
        var cant_input_hidden = $(idPanelContenedor + ' input[type="hidden"].selected-skills').length;
        if( cant_input_hidden == 1) {
            var input_hidden_value = $(idPanelContenedor + ' input[type="hidden"].selected-skills').val();
        } else {
            var input_hidden_value = $(idPanelContenedor + ' .tab-content>.tab-pane.active input[type="hidden"].selected-skills').val();
        }
        
        var arr_values = input_hidden_value.split('|');
        $('#actividad-body .widget-box div.habilidades .btn-skill').each(function() {
            $(this).removeClass('active');
            var id_skill = $(this).data('id-skill');
            $.each(arr_values, function(index, elem) {
                $('#actividad-body .widget-box div.habilidades .btn-skill[data-id-skill="'+elem+'"]').addClass('active', {duration:200});
            });
        });
    };

    $('.actividad-main-content').on('shown.bs.tab', 'ul.nav-tabs.ejercicios li a', function (e) {
        var panelShown = '#'+$(this).attr('href').replace('#', '');
        mostrarHabilidadesActivas(panelShown);
    });

    $('#actividad-body').on('shown.bs.tab', 'ul.nav-pills.metodologias li a', function (e) {
        var panelShown = '#'+$(this).attr('href').replace('#', '');
        mostrarHabilidadesActivas(panelShown);
    });

    $('#actividad-body div.habilidades')
        .on('click', '.btn-skill', function(e){
            var id_TabMetodologia = '#' + $('#actividad-body ').find('div.actividad-main-content>.metod.active').attr('id');
            var cant_input_hidden = $(id_TabMetodologia + ' input[type="hidden"].selected-skills').length;
            if( $('.btn.preview').is(':visible') && cant_input_hidden>=1 ){ //si btn.preview es Visible ó si hay al menos 1 Input Hidden en panel activo
                var id_skill = $(this).data('id-skill');
                $(this).toggleClass('active');
                var id_tab_metod = $('.actividad-main-content').find('.metod.tab-pane.active').attr('id');

                if(id_tab_metod == 'met-1') var index_tab = '1';
                else var index_tab = $('#'+id_tab_metod).find('ul.nav-tabs>li.active>a').text();

                var input_hidden =  $('#'+id_tab_metod).find('input#habilidades_'+id_tab_metod+'_'+index_tab);
                var input_hidden_val = input_hidden.val().trim();
                
                if( $(this).hasClass('active') ){
                    if( input_hidden_val == '0'){ input_hidden_val = id_skill; }
                    else{ input_hidden_val += '|'+id_skill; }
                } else {
                    arrValues = input_hidden_val.split('|');
                    arrValues = jQuery.grep(arrValues, function(value) {//encuentra elemento de arrValues , segun filtro
                        return value != id_skill; //filtra todos los values diferentes de Id_Skill
                    });
                    input_hidden_val = arrValues.join( "|" );
                    if(input_hidden_val=='') input_hidden_val = '0';
                }
                input_hidden.val(input_hidden_val);
            }
        });

    tinymce.init({
        relative_urls : false,
        cleanup : false,
        verify_html : false,
        selector: '#txaVocabulario',
        height: 400,       
        menubar: false,
        plugins:["chingoinput textcolor" ], 
        toolbar: 'undo redo | styleselect | table | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor | chingoinput '        
      });

    $('.preview').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).hide();
        $('.back').show('fast');
        resetDialogo();  /* para dialogo */
        $('.cargarpage').hide();
        $('.nopreview').hide();
        $('.btn.add-bubble').hide(); /*solo para dialogo.php*/
        $('.valin').each(function(){
            var vp=$(this).val();
            var nodpview=$(this).attr('data-nopreview');
            if(vp==''||vp==undefined){                
                $(this).siblings(nodpview).hide().addClass('showreset');
                $(nodpview).hide().addClass('showreset');
            }else{

            }

        });
        $('.valvideo').each(function(){
            var src = $(this).attr('src');
            console.log(src);
            if(src==''||src==undefined) {
                $(this).siblings('img').show();
                $(this).hide();
            } else {
                $(this).siblings('img').hide();
                $(this).show();
            }
        });
   });

    $('.back').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).hide();
        $('.preview').show();
        $('.cargarpage').show();
        $('.nopreview').show();
        $('.showreset').show().removeClass('showreset');
        $('.btn.add-bubble[data-info="show"]').show(); /*solo para dialogo.php*/
        $('.valvideo').each(function() {
            var src = $(this).attr('src');
            if(src==''||src==undefined) {
                $(this).siblings('img').show();
                $(this).hide();

            } else {
                $(this).siblings('img').hide();
                $(this).show();
            }
        });
    });

    var agregarAudio = function(selector, src_name){
        var audiosrc = $(selector).data('audio');
        $('audio').attr('src','<?php echo $this->documento->getUrlStatic(); ?>/media/audio/'+src_name);
    };

    subirmedia=function(tipo,file){
          var formData = new FormData();
          formData.append("tipo",tipo);
          formData.append("filearchivo",file[0].files[0]);
          formData.append("nivel",$("#txtNivel").val());
          formData.append("unidad",$("#txtunidad").val());
          formData.append("leccion",$("#txtsesion").val());
          dataurl=file.attr('data-url');
          $.ajax({
            url: '<?php echo $this->documento->getUrlBase(); ?>/biblioteca/cargarmedia',
            type: "POST",
            data:  formData,
            contentType: false,
            dataType :'json',
            cache: false,
            processData:false,
            success: function(data)
            {
               //console.log(data);
               if(data.code==='ok'){
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
                if(tipo=='image'){ 
                    $('img'+dataurl).siblings('img').hide();
                    $('img'+dataurl).attr("src","<?php echo $this->documento->getUrlStatic(); ?>/media/image/"+data.namelink); 
                    $('img'+dataurl).show('fast');
                }else if(tipo=='audio'){
                    $('audio'+dataurl).siblings('img').hide();
                    $('audio'+dataurl).attr("data-audio",data.namelink);
                    $('audio'+dataurl).show('fast');
                    agregarAudio('audio'+dataurl, data.namelink);
                }else if(tipo=='video'){ 
                    $('video'+dataurl).siblings('img').hide();
                    $('video'+dataurl).attr("src","<?php echo $this->documento->getUrlStatic(); ?>/media/video/"+data.namelink); 
                    $('video'+dataurl).show('fast');
                }          
              }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
                return false;
              }
            },
            error: function(e) 
              {
                console.log(e);
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
                return false;
              }
            
          });
    };

    $('#addinfotxt').on('hidden.bs.modal', function (e) {
        $('#addinfotxt #addinfotitle').html('');
        $('#addinfotxt .modal-body').html('<h4><i class="fa fa-cog fa-spin fa-fw"></i> <?php echo JrTexto::_('Loading') ?>...</h4>');
    });

    var nuevoPanel = function(index, id_metodologia, nomb_metod){
        var identif = id_metodologia+'_'+index;
        return '<div class="row"> '+
          '<div class="col-xs-12">'+
            '<div style="position: absolute; right: 1.5em; ">'+
               '<div class="btn-close-tab tooltip nopreview" title="<?php echo JrTexto::_('remove') ?> <?php echo JrTexto::_('tab') ?>" data-id-tab="tab-'+nomb_metod+'-'+index+'">'+
                  '<button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                '</div>'+
            '</div>'+
            '<div class="tab-title-zone pw1_'+id_metodologia+'" > '+
                '<div class="col-md-12">'+
                    '<h3 class="addtext addtexttitulo2"><?php echo JrTexto::_('Click here to add title') ?></h3>'+
                    '<input type="hidden" class="valin nopreview"  name="titulo['+id_metodologia+']['+index+']" data-nopreview=".addtexttitulo2">'+
                '</div>'+
            '</div>'+
            '<div class="space-line pw1_'+id_metodologia+'"></div>'+
            '<div class="tab-title-zone pw1_'+id_metodologia+'" > '+
                '<div class="col-md-12">'+
                    '<p class="addtext addtextdescription2"><?php echo JrTexto::_('Click here to add descripcion') ?></p>'+
                    '<input type="hidden" class="valin nopreview"  name="descripcion['+id_metodologia+']['+index+']" data-nopreview=".addtext" >'+
                '</div>'+
            '</div>'+
            '<br>'+
            '<select id="txtplantilla" name="txtPlantilla" class="act_select cargarpage nopreview" data-source="<?php echo $this->documento->getUrlSitio() ?>/plantillas/actividad/" data-container="'+identif+'">'+
                '<option value=""><?php echo JrTexto::_('select template') ?></option>'+

                <?php foreach($tpl['tpl'] as $key=>$val): ?>
                    '<option value="<?=$key?>"><?=$val?></option>'+
                <?php endforeach; ?>

            '</select>'+
            '<div id="tmppt_'+identif+'">'+
                '<div></div>'+
            '</div>'+
            '<input type="hidden" name="habilidades['+id_metodologia+']['+index+']" class="selected-skills" id="habilidades_met-'+identif+'" value="0">'+
          '</div>'+
        '</div>';
    };

    $('li.btn-Add-Tab a').on('click', function(e){
        e.preventDefault();
        var id_ulContenedor= '#' + $(this).parent().parent().attr('id');
        var id_PanelesContenedor = '#' + $(id_ulContenedor).parent().next().find('.tab-content').attr('id');
        var nombre_metodologia = $(id_ulContenedor).data('metod');
        var id_metodologia = $(id_ulContenedor).data('id-metod');
        var ultimo_tab = $(this).parent();
        var cant_tab = $(id_ulContenedor+' li').length;
        if(cant_tab == 1) { index_ultimo_tab = 2; } else { index_ultimo_tab = cant_tab;} //Si Solo queda boton Add Tab
        var a_href_tab = $(id_ulContenedor+' li:nth-child('+(index_ultimo_tab-1)+') a').attr('href').split('-'); //#tab-Metodologia-ultimoIndex
        var index_NewTab = parseInt(a_href_tab[2]) + 1; //ultimoIndex + 1
        /* agregando nuevo tab */
        var newTab = '<li> <a href="#tab-'+nombre_metodologia+'-'+index_NewTab+'" data-toggle="tab">'+ cant_tab +'</a> </li>';
        ultimo_tab.before(newTab);

        /* agregando Tab Panel */
        var newPanel = '<div id="tab-'+nombre_metodologia+'-'+index_NewTab+'" class="tab-pane fade " >'+
                         '<div class="textosave" data-texto="tptext_'+id_metodologia+'_'+index_NewTab+'" >'+ 
                            nuevoPanel(index_NewTab, id_metodologia, nombre_metodologia) +
                        '<textarea name="det_texto['+id_metodologia+']['+index_NewTab+']" id="tptext_'+id_metodologia+'_'+index_NewTab+'" class="hidden">'+
                        '</textarea>'+
                        '<textarea name="det_texto_edit['+id_metodologia+']['+index_NewTab+']" id="tptext_'+id_metodologia+'_'+index_NewTab+'_edit" class="hidden"></textarea>'+
                        '</div>';
        $(id_PanelesContenedor).append(newPanel);

        /* mostrar Tab agregado*/
        $(id_ulContenedor+' a[href="#tab-'+nombre_metodologia+'-'+index_NewTab+'"]').tab('show'); 
        mostrarHabilidadesActivas('#tab-'+nombre_metodologia+'-'+index_NewTab);
    });

    var renombrarTabs = function(id_ulContenedor, cant_tabs){
        for(i = 1; i <= cant_tabs; i++){
            $(id_ulContenedor + ' li:nth-child('+i+') a').html(i);
        }
    };

    $('.tab-content').on('click', '.tab-pane .btn-close-tab', function(e) {
        e.preventDefault();
        var id_TabPanel =  '#' + $(this).data('id-tab');
        var li_eliminar = $('ul.nav-tabs').find('a[href="'+id_TabPanel+'"]').parent();
        var id_ulContenedor = '#' + li_eliminar.parent().attr('id');
        $(id_TabPanel).remove();
        li_eliminar.remove();

        /* mostrar ultimo tab diferente de (Add Tab)*/
        var cant_tab = ($(id_ulContenedor + ' li').length) - 1;
        if(cant_tab>=1){
            renombrarTabs(id_ulContenedor, cant_tab);
            $(id_ulContenedor +' li:nth-child(1) a').tab('show');
        }
    });
});

</script>