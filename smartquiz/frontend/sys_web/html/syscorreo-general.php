<?php
defined('RUTA_BASE') or die();
?>
<style>
table {
border-collapse:collapse;
}

table[class="ecxdeviceWidth"] {
width:600px;
max-width:600px;
}

table[class="ecxbtn"] {
font-size:21px;
}

table[class="ecxWidthTableInt_I"] {
max-width:425px !important;
width:100% !important;
text-align:left !important;
}

table[class="ecxWidthTableInt_D"] {
width:100% !important;
max-width:90px !important;
text-align:right !important;
}

@media only screen and (max-width: 600px), screen and (max-device-width: 600px) {
table[class="ecxdeviceWidth"] {
width:440px !important;
padding:0 !important;
}
table[class="ecxbtn"] {
font-size:18px !important;
}
table[class="ecxWidthTableInt_I"] {
width:440px !important;
}
table[class="ecxWidthTableInt_D"] {
width:440px !important;
text-align:left !important;
}
}

@media only screen and (max-width: 479px), screen and (max-device-width: 479px) {
table[class="ecxdeviceWidth"] {
width:280px !important;
padding:0 !important;
}
table[class="ecxWidthTableInt_I"] {
width:280px !important;
text-align:left !important;
}
table[class="ecxWidthTableInt_D"] {
width:280px !important;
text-align:left !important;
}
}
</style>

<table style="width:100% !important;table-layout:fixed;" align="center" bgcolor="#ececec">
        <tbody>
            <tr>
                <td>
                    <table class="ecxdeviceWidth" style="max-width:600px;" align="center">
                        <tbody>
                        <tr>
                            <td style="padding-bottom:8px;">
                                <table style="color:#353e4a;font-family:Arial,sans-serif;font-size:15px;" align="center" bgcolor="#ffffff" border="0">
                                    <tbody>
                                        <tr>
                                        <td style="color:#1e80b6;padding-top:20px;padding-bottom:10px;padding-left:20px;padding-right:20px;">
                                            <?php echo $this->pasarHtml($this->para)?></td>
                                        </tr>
                                        <tr>
                                            <td style="line-height:24px;padding-left:20px;padding-right:20px;padding-bottom:10px;">
                                                <?php echo $this->mensaje /*$this->pasarHtml($this->mensaje)*/ ; ?>
                                          	</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color:#ececec;">
                                <table style="color:#353e4a;font-family:Arial,sans-serif;font-size:14px;padding-bottom:10px;" align="center" border="0" width="100%">
                                    <tbody>

                                        <tr>
                                            <td style="color:#717175;font-size:12px;padding-top:10px;padding-bottom:10px;">
                                               <?php echo $this->pasarHtml(JrTexto::_('Este email se ha generado automáticamente. Por favor, no contestes a este email'))?>.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
              	</table>
                </td>
            </tr>
        </tbody>
    </table>