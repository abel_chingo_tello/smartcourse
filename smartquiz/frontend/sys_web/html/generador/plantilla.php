<?php defined('RUTA_BASE') or die(); ?>
<div class="">
	<div class="page-title">   
    <div class="row">
    	<div class="col-md-12 col-xs-12">      
    	<div class="x_panel">
      <div id="msj-interno"></div>
    	<div class="x_title">
        	<h2> Generador de Mantenimientos</h2>
            <div class="clearfix"></div>
      	</div>
      	<div class="x_content">
      	<form method="post" class="form-horizontal form-label-left"  id="formInfoGenerador" onsubmit="return false;" >
        <div class="form-group col-md-12 col-xs-12">
        <div class="form-group col-md-2 col-xs-2">
        <input type="hidden" name="pk" id="pk" value="" >
        <label for="tablaActiva"> Listado de Tabla *:</label>
        <select class="form-control" id="tablaActiva" name="tablaActiva" required >
            <option value="">Selecione..</option>
            <?php 
            if(!empty($this->tablas))
            foreach ($this->tablas as $tb) {?>
            <option value="<?php echo $tb ?>"><?php echo $tb; ?></option>
            <?php } ?>
            
        </select>
        </div>
        <div class="form-group col-md-2 col-xs-2">
           <label><input type="checkbox" name="createlistado" value="1" checked > Crear Listado</label>
           <label><input type="checkbox" name="createver" value="1" > Crear ver</label>
           <label><input type="checkbox" name="createfrm" value="1" checked > Crear formulario</label>
        </div>
        <div class="form-group col-md-2 col-xs-2">
           <label><input type="checkbox" name="createclaseweb" value="1"  > Crear clase web</label>
           <label><input type="checkbox" name="createnegocio" value="1"  > Crear negocio</label>
           <label><input type="checkbox" name="createdatos" value="1"  > Crear datos</label>
        </div>
        <div class="form-group col-md-2 col-xs-2">
           <label><input type="checkbox" name="accver" value="1" checked > Accion ver</label>
           <label><input type="checkbox" name="acceditar" value="1" checked > Accion editar</label>
           <label><input type="checkbox" name="acceliminar" value="1" checked > Accion eliminar</label>
        </div>
        <div class="form-group col-md-2 col-xs-2">
           <label><input type="checkbox" name="guardarmenu" value="1" checked > Guardar en Menu</label>
           <label>Menu Padre</label><br><select name="menupadre">
             <option value="0">Ninguno</option>
             <option value="1">Dashboard</option>
             
           </select>
        </div>
        <div class="form-group col-md-2 col-xs-2">
           <label><input type="radio" name="createlistadodeafult" value="listado" checked > Ir a listado</label>
           <label><input type="radio" name="createlistadodeafult" value="ver" > Ir a ver</label>
        </div>
        </div> 
        <br> 
         <table id="allcampos" class="table table-striped responsive-utilities jambo_table bulk_action">
            <thead>
                <tr class="headings">                    
                    <th class="column-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Campo </th>
                    <th class="column-title">Tipo dato</th>
                    <th class="column-title">Tipo Control </th>
                    <th class="column-title">Detalle</th>
                    <th class="column-title">fk id y ver campo</th>
                    <th class="column-title">fk Mostrar Como</th>                                       
            </thead>
            <tbody>
            </tbody>
          </table>
            <div>
                <button  type="submit" id="btn-enviar-formInfoGenerador" class="btn btn-success"> .:. Generar .:.</button>
            </div>    
        	</form>
      	</div>
    	</div>
    	</div>
    </div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
 $('#btn-enviar-formInfoGenerador').attr('disabled',true);
  $('#formInfoGenerador').bind({
         submit: function(){
            //console.log('aa');
            $('#btn-enviar-formInfoGenerador').attr('disabled', true); 
            var res = xajax__('', 'generador', 'setGenerarPagina', xajax.getFormValues('formInfoGenerador'));
            if(res) {
                agregar_msj_interno('success', 'La Pagina se Generado Correctamente');                
            }
            $('#btn-enviar-formInfoGenerador').attr('disabled', false);
            setTimeout(function(){$('.alert').fadeOut();}, 3000);
         }
    });



   $("#tablaActiva").change(function() {
     var tbAct= $('#tablaActiva').val();
    $("#btn-enviar-formInfoGenerador").html (".:. Generar "+tbAct+".:.");
    var res = xajax__('', 'generador', 'getCampos',tbAct);
    if(res) {
      //console.info(res);
      var tb=$('#allcampos');
      tb.find("tr:gt(0)").remove();
      var ntd=$("#allcampos tr:first td").length;
      var ntr=$("#allcampos tr").length;
      str='';
      
      for(var dat in res){
              var name=res[dat]["COLUMN_NAME"];
             
              var ctype=res[dat]["COLUMN_TYPE"];
              var dtype=res[dat]["DATA_TYPE"];
              var key=res[dat]["COLUMN_KEY"];
              var descripcion=res[dat]["COLUMN_COMMENT"];
              var maxcaracter=res[dat]["CHARACTER_MAXIMUM_LENGTH"];
              var valordefecto=res[dat]["COLUMN_DEFAULT"];
              var parteentera=res[dat]["NUMERIC_PRECISION"];
              var partedecimal=res[dat]["NUMERIC_SCALE"];
              var aceptanulos=res[dat]["IS_NULLABLE"];
              var _tipocampo='';
             // alert(ctype+' '+dtype);
              var pk='PK';
              var _checked='';
              if(key!='PRI'){ 
                _checked='checked';
                _tipocampo=tipocampo(name,dtype);
                pk='';
              }else{
                $("#pk").val(name);
              } 
              strmoney='';
              if(dtype=='double'||dtype=='float',dtype=='decimal'){
                strmoney='<input data-toggle="tooltip" data-placement="left" title="Mostrar como campo de moneda $ " type="checkbox" name="ismoney'+name+'">';
              }
              var str2='';
              if(name){
              str+='<tr>';
              str2='<input type="hidden" name="comentario'+name+'" value="'+descripcion+'">';
              str2+='<input type="hidden" name="maximocaracteres'+name+'" value="'+maxcaracter+'">';
              str2+='<input type="hidden" name="valordefecto'+name+'" value="'+valordefecto+'">';
              str2+='<input type="hidden" name="parteentera'+name+'" value="'+parteentera+'">';
              str2+='<input type="hidden" name="partedecimal'+name+'" value="'+partedecimal+'">';
              str2+='<input type="hidden" name="coltype'+name+'" value="'+ctype+'">';
              str2+='<input type="hidden" name="dattype'+name+'" value="'+dtype+'">';
              str2+='<input type="hidden" name="aceptanulos'+name+'" value="'+aceptanulos+'">';
              str+='<td>'+str2+'<input data-toggle="tooltip" data-placement="left" title="Mostrar en listado" type="checkbox" name="_chk'+name+'" '+_checked+'> <input type="hidden" name="campo[]" value="'+name+'"> '+name+' '+pk+'</td>';         
              str+='<td><input type="hidden" name="dtipo_'+dtype+'" value="'+dtype+'"> '+ctype+' '+strmoney+'</td>';
              str+='<td>' +_tipocampo+'</td>';
              str+='<td id="tdop2_'+name+'"></td>';
              str+='<td id="tdop3_'+name+'"></td>';
              str+='<td id="tdop4_'+name+'"></td>';             
              str+='</tr>';  
              }           
      }
      
      tb.append(str);
      $('#btn-enviar-formInfoGenerador').attr('disabled',false);
    }    
  });

  var tipocampo=function(id,tipo){
     
   var  str='<select name="tipo_'+id+'" id="tipo_'+id+'" onchange="opcion2(\''+id+'\',this);">';
        str+='<option value="text" >text</option>'; 
        if(tipo=='date'){
          str+='<option value="date">Date</option>'; 
        }else if(tipo=='datetime'||tipo=='timestamp'){
          str+='<option value="datetime">Datetime</option>';
        }else if(tipo=='time'){
          str+='<option value="time">Time</option>';
        }else if(tipo=='text'||tipo=='varchar'){
          str+='<option value="textArea">textArea</option>';
          str+='<option value="file">File</option>';
          str+='<option value="password">password</option>';
          str+='<option value="email">email</option>';
          str+='<option value="color">color</option>';
        }else if(tipo=='int'||tipo=='char'||tipo=='tinyint'||tipo=='bigint'||tipo=='enum'){
          str+='<option value="combobox">combobox</option>';
          str+='<option value="radiobutton">radiobutton</option>';
          str+='<option value="checkbox">Chekbox</option>';
          str+='<option value="int">int</option>';
        }else if(tipo=='float'||tipo=='decimal'||tipo=='double'){
          str+='<option value="int">int</option>';
          str+='<option value="decimal">decimal</option>';
          str+='<option value="money">money</option>';     
        }          
        str+='<option value="fk">Clave Foranea</option>';        
        str+='</select>';
        /*var str2='<input type="hidden" name="comentario'+id+'">';
        str2+='<input type="hidden" name="maximocaracteres'+id+'">';
        str2+='<input type="hidden" name="valordefecto'+id+'">';
        str2+='<input type="hidden" name="parteentera'+id+'">';
        str2+='<input type="hidden" name="partedecimal'+id+'">';
        str2+='<input type="hidden" name="comentario'+id+'">';
        str2+='<input type="hidden" name="aceptanulos'+id+'">';*/
        return str;
  }
   

  });
    function opcion2(id,ele){

      var tipo_=ele.value;
      var str__='';
      var str__accion='';
      str_='<option value="">Selecione Tipo</option>';
      if(tipo_=='date'||tipo_=='datetime'){
          str__ +='<option value="user">registrar</option>'; 
          str__ +='<option value="system">Por el Sistema</option>';     
      }else if(tipo_=='password'){
          str__ +='<option value="md5">Encriptado md5</option>';
          str__ +='<option value="simple">sin encriptar</option>';          
      }else if(tipo_=='textArea'){
          str__ +='<option value="simple">simple</option>';
          str__ +='<option value="chkeditor">CHK Editor</option>';
          str__+='<option value="wysihtml5">Wysihtml5</option>';
           str__+='<option value="tinymce">Tinymce</option>';
      }else if(tipo_=='file'){
          str__ +='<option value="all">Cualquiera</option>';
          str__ +='<option value="imagen">imagen</option>';
          str__ +='<option value="video">video</option>';
          str__ +='<option value="documentos">documentos</option>';
      }else if(tipo_=='combobox'||tipo_=='radiobutton'||tipo_=='checkbox' ){
          str__ +='<option value="simple">Simple</option>';
          str__ +='<option value="2">2 valores</option>';
          str__ +='<option value="3">3 valores</option>';
          str__ +='<option value="4">4 valores</option>'; 
          str__ +='<option value="5">5 valores</option>'; 
          str__ +='<option value="10">10 valores</option>';
      }else  if(tipo_=='int'){
          str__ +='<option value="simple">Simple</option>';
          str__ +='<option value="number">Numero</option>';
      }else if(tipo_=="fk"){
           var tablas=<?php echo json_encode($this->tablas); ?>;
            str__+='<option value="">Selecione Tabla</option>';
           for(i=0;i<tablas.length;i++){
            str__+='<option value="'+tablas[i]+'">'+tablas[i]+'</option>';
           }
          str_+="fk";
          str__accion='onchange="opcion3(\''+id+'\',this);"';
      }
      if(str__!='')
          str_='<select style="width:110px" name="tipo2_'+id+'" id="tipo2_'+id+'" '+str__accion+' >'+str__+'</select>';
      else str_='--';
      $('#tdop2_'+id).html(str_);  
      $('#tdop3_'+id).html('--');  
      $('#tdop4_'+id).html('--');
    }
    function opcion3(id,tb2){
        tb2=tb2.value;         
        str__='<option value="">Selecione Tipo</option>';  
        if(tb2!=''){   
          var res = xajax__('', 'generador', 'getCampos', tb2);
         
          if(res){
            for(rd in res){
              str__+='<option value="'+res[rd]["COLUMN_NAME"]+'">'+res[rd]["COLUMN_NAME"]+'</option>';
            }
          }
        }
        var str3_='<select style="width:100px" name="tipofkid_'+id+'" id="tipofkid_'+id+'" >'+str__+'</select>';
         str3_+='<select style="width:100px" name="tipofkver_'+id+'" id="tipofkver_'+id+'" >'+str__+'</select>';
        
        var  txtstr='<option value="combobox">combobox</option>';
         txtstr+='<option value="radiobutton">radiobutton</option>';
         txtstr+='<option value="checkbox">Chekbox</option>';
         txtstr+='<option value="text">texto Auto completo</option>';
         txtstr+='<option value="listado">listado</option>';
         if(tb2=='sys_usuarios')
         txtstr+='<option value="system">usuario logeado</option>';
        var str4_='<select style="width:100px" name="tipofkcomo_'+id+'" id="tipofkcomo_'+id+'" >'+txtstr+'</select>';
        $('#tdop3_'+id).html(str3_);
       // $('#tdop4_'+id).html(str4_);
        $('#tdop4_'+id).html(str4_);
    }

</script>