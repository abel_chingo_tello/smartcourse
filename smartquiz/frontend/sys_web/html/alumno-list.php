<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="panel">
    <div class="panel-heading border-bottom">
      <h1><?php echo ucfirst(JrTexto::_("Student")); ?>
        <small id="frmaction"><i class="fa fa-angle-double-right"></i> <?php echo JrTexto::_("Listado");?></small></h1>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body"> <div class="row">
      <div class="col-xs-12">
        <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("alumno", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Alumno").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('add'))?></a>
        <br><br>
      </div>
      <div class="col-xs-12">
      <table class="table table-striped table-responsive" id="tblAlumnos">
          <thead>
            <tr class="headings">
              <th>N&deg;</th>
              <th><?php echo JrTexto::_("Nombre") ;?></th> 
              <th><?php echo JrTexto::_("Sexo"); ?></th>                                       
              <th><?php echo JrTexto::_("Telefonos") ;?></th>
              <th><?php echo JrTexto::_("Email") ;?></th> 
              <th><?php echo JrTexto::_("Usuario") ;?></th>                    
              <th><?php echo JrTexto::_("Foto") ;?></th>
              <th><?php echo JrTexto::_("Estado") ;?></th>                    
              <!--th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th-->
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div></div>
  </div>
</div>

<script type="text/javascript">
var tabledatos597627b63b43a='';
function refreshdatos597627b63b43a(){
    tabledatos597627b63b43a.ajax.reload();
}
$(document).ready(function(){  
  var estados597627b63b43a={'A':'<?php echo JrTexto::_("Active") ?>','I':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var sexos597627b63b43a=<?php echo json_encode($this->fksexo); ?>;
  var sexo597627b63b43a={};
  $.each(sexos597627b63b43a,function(i,v){
    sexo597627b63b43a["'"+v.codigo+"'"]=v.nombre;
  })
  var estadocivil597627b63b43a=<?php echo json_encode($this->fkestado_civil); ?>;
  var estadociv597627b63b43a={};
  $.each(estadocivil597627b63b43a,function(i,v){
    estadociv597627b63b43a["'"+v.codigo+"'"]=v.nombre;
  })
  var tituloedit597627b63b43a='<?php echo ucfirst(JrTexto::_("alumno"))." - ".JrTexto::_("edit"); ?>';
  var tituloficha597627b63b43a='<?php echo ucfirst(JrTexto::_("Ficha"));?> - ';
  var titulocambiaruser597627b63b43a='<?php echo ucfirst(JrTexto::_("Change"))." - ".JrTexto::_("User"); ?> ';
  var draw597627b63b43a=0;

  
  $('#datefechanac').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#fkcbsexo').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#fkcbestado_civil').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#fkcbubigeo').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#fkcbidugel').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#dateregfecha').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#cbestado').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#cbsituacion').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#texto').keyup(function(e) {
    if(e.keyCode == 13) {
        refreshdatos597627b63b43a();
    }
  });
  tabledatos597627b63b43a=$('#tblAlumnos').DataTable(
    { /*"searching": false,*/
    "processing": false,
    /*"serverSide": true,*/
    "columns" : [
    {'data': '#'},        
    {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
    {'data': '<?php echo JrTexto::_("Sexo") ;?>'},
    /*{'data': '<?php //echo JrTexto::_("Estado civil") ;?>'},*/
    {'data': '<?php echo JrTexto::_("Telefono / celular") ;?>'},
    {'data': '<?php echo JrTexto::_("Email") ;?>'}, 
    {'data': '<?php echo JrTexto::_("Usuario") ;?>'},           
    {'data': '<?php echo JrTexto::_("Foto") ;?>'},
    {'data': '<?php echo JrTexto::_("Estado") ;?>'},                
    /*{'data': '<?php echo JrTexto::_("Actions") ;?>'}*/
    ],
    "ajax":{
      url:_sysUrlBase_+'/alumno/xBuscar',
      type: "post",                
      data:function(d){
        d.json=true                   
        d.fechanac=$('#datefechanac').val(),
        d.sexo=$('#fkcbsexo').val(),
        d.estado_civil=$('#fkcbestado_civil').val(),
        d.texto=$('#texto').val(),            
        /*d.situacion=$('#cbsituacion').val(),*/
        /*d.texto=$('#texto').val(),*/

        draw597627b63b43a=d.draw;
        /*console.log(d);*/
        d.idempresa= '<?php echo $this->usuarioAct['idempresa']; ?>';
      },
      "dataSrc":function(json){
        var data=json.data;             
        json.draw = draw597627b63b43a;
        json.recordsTotal = json.data.length;
        json.recordsFiltered = json.data.length;
        var datainfo = new Array();
        for(var i=0;i< data.length; i++){
          var fullname=data[i].apellidopaterno+' '+data[i].apellidomaterno+', '+data[i].nombre;
          var actions='<a class="btn btn-xs btnvermodal" href="'+_sysUrlBase_+'/alumno/verficha/?id='+data[i].idalumno+'" data-titulo="'+tituloficha597627b63b43a+fullname+'"><i class="fa fa-eye"></i></a>'+
          '<a class="btn btn-xs btnvermodal" href="'+_sysUrlBase_+'/alumno/editar/?id='+data[i].idalumno+'" data-titulo="'+tituloedit597627b63b43a+'"><i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>'+
          '<a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idalumno+'" ><i class="fa fa-trash-o"></i></a>';
          
          var imgSrc = _sysUrlStatic_+'/img/sistema/alumno_avatar.png';
          if(data[i].img!='' && data[i].img!=null){
            imgSrc = '<?php echo $this->raiz; ?>/fotos/'+data[i].idalumno+'.JPG?<?php echo $this->uniqid; ?>';
          }
          var img='<img class="img-circle img-responsive" src="'+imgSrc+'" style="max-height:40px; max-width:40px;">';
          datainfo.push({
            '#':(i+1),
            '<?php echo JrTexto::_("Nombre") ;?>': fullname,             
            '<?php echo JrTexto::_("Sexo") ;?>': (data[i].sexo=='M'?'Masculino':'Femenino'),
            /*'<?php //echo JrTexto::_("Estado civil") ;?>': data[i].estado_civil!=''?estadociv597627b63b43a["'"+data[i].estado_civil+"'"]:'',*/
            '<?php echo JrTexto::_("Telefono / celular") ;?>': data[i].telefono+'<br>'+data[i].celular,
            '<?php echo JrTexto::_("Email") ;?>': data[i].email,            
            '<?php echo JrTexto::_("Usuario") ;?>': data[i].usuario,             
            '<?php echo JrTexto::_("Foto") ;?>': img,
            '<?php echo JrTexto::_("Estado") ;?>': (data[i].idalumno=='<?php echo $this->usuarioAct['idusuario']; ?>')?'':'<a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="'+data[i].idalumno+'"> <i class="fa fa'+(data[i].estado=='A'?'-check':'')+'-circle-o fa-lg"></i> '+(data[i].estado=='A'?'Activo':'Inactivo')+'</a>',              
            /*'<?php echo JrTexto::_("Actions") ;?>': '' actions ,*/
          });
        }
        return datainfo 
      }, 
      error: function(d){console.log(d)}
    }
    <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
  });

  $('#tblAlumnos').on('click','.btn-chkoption',function(){
    var id=$(this).attr('data-id');
    var campo=$(this).attr('campo');
    var data='I';
    if($("i",this).hasClass('fa-circle-o')) data='A';
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){
        var res = xajax__('', 'alumno', 'setCampo', id,campo,data);
        if(res) tabledatos597627b63b43a.ajax.reload();
      }
    });
  });

  $('#tblAlumnos').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos597627b63b43a';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Alumno';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });

  $('#tblAlumnos .table').on('click','.btn-eliminar',function(){
    var id=$(this).attr('data-id');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'alumno', 'eliminar', id);
        if(res) tabledatos597627b63b43a.ajax.reload();
      }
    }); 
  });
});
</script>