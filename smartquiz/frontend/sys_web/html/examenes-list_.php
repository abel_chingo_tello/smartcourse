<?php defined("RUTA_BASE") or die(); ?><div class="form-view" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('examenes'));?>"><?php echo JrTexto::_('Examenes'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <div class="x_title">
          <a class="btn btn-success btn-xs" href="<?php echo JrAplicacion::getJrUrl(array("Examenes", "agregar"));?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
          <div class="nav navbar-right"></div>
          <div class="clearfix"></div>
        </div>
        <div class="div_linea"></div>
         <div class="x_content">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idnivel") ;?></th>
                    <th><?php echo JrTexto::_("Idunidad") ;?></th>
                    <th><?php echo JrTexto::_("Idactividad") ;?></th>
                    <th><?php echo JrTexto::_("Titulo") ;?></th>
                    <th><?php echo JrTexto::_("Descipcion") ;?></th>
                    <th><?php echo JrTexto::_("Portada") ;?></th>
                    <th><?php echo JrTexto::_("Fuente") ;?></th>
                    <th><?php echo JrTexto::_("Fuentesize") ;?></th>
                    <th><?php echo JrTexto::_("Tipo") ;?></th>
                    <th><?php echo JrTexto::_("Grupo") ;?></th>
                    <th><?php echo JrTexto::_("Aleatroio") ;?></th>
                    <th><?php echo JrTexto::_("Calificacion por") ;?></th>
                    <th><?php echo JrTexto::_("Calificacion en") ;?></th>
                    <th><?php echo JrTexto::_("Calificacion total") ;?></th>
                    <th><?php echo JrTexto::_("Calificacion min") ;?></th>
                    <th><?php echo JrTexto::_("Tiempo por") ;?></th>
                    <th><?php echo JrTexto::_("Tiempo total") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th><?php echo JrTexto::_("Idpersonal") ;?></th>
                    <th><?php echo JrTexto::_("Fecharegistro") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $reg["idnivel"] ;?></td>
                    <td><?php echo $reg["idunidad"] ;?></td>
                    <td><?php echo $reg["idactividad"] ;?></td>
                    <td><?php echo substr($reg["titulo"],0,100)."..."; ?></td>
                    <td><?php echo substr($reg["descipcion"],0,100)."..."; ?></td>
                    <td><?php echo substr($reg["portada"],0,100)."..."; ?></td>
                    <td><?php echo $reg["fuente"] ;?></td>
                    <td><?php echo $reg["fuentesize"] ;?></td>
                    <td><?php echo $reg["tipo"] ;?></td>
                    <td><?php echo substr($reg["grupo"],0,100)."..."; ?></td>
                    <td><?php echo $reg["aleatroio"] ;?></td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="calificacion_por"  data-id="<?php echo $reg["idexamen"]; ?>"> <i class="fa fa<?php echo !empty($reg["calificacion_por"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["calificacion_por"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="calificacion_en"  data-id="<?php echo $reg["idexamen"]; ?>"> <i class="fa fa<?php echo !empty($reg["calificacion_en"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["calificacion_en"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="calificacion_total"  data-id="<?php echo $reg["idexamen"]; ?>"> <i class="fa fa<?php echo !empty($reg["calificacion_total"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["calificacion_total"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="calificacion_min"  data-id="<?php echo $reg["idexamen"]; ?>"> <i class="fa fa<?php echo !empty($reg["calificacion_min"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["calificacion_min"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="tiempo_por"  data-id="<?php echo $reg["idexamen"]; ?>"> <i class="fa fa<?php echo !empty($reg["tiempo_por"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["tiempo_por"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td><?php echo $reg["tiempo_total"] ;?></td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="<?php echo $reg["idexamen"]; ?>"> <i class="fa fa<?php echo !empty($reg["estado"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td><?php echo $reg["idpersonal"] ;?></td>
                    <td><?php echo $reg["fecharegistro"] ;?></td>
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('examenes'))?>ver/?id=<?php echo $reg["idexamen"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("examenes", "editar", "id=" . $reg["idexamen"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["idexamen"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function () {
  $('.btn-eliminar').bind({   
    click: function() {
       var id=$(this).attr('data-id');
       $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'examenes', 'eliminar', id);
          if(res){
            return redir('<?php echo JrAplicacion::getJrUrl(array('examenes'))?>');
          }
        }
      });     
    }
  });
  
  $('.btn-chkoption').bind({
    click: function() {     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           var res = xajax__('', 'examenes', 'setCampo', id,campo,data);
           if(res) {
            return redir('<?php echo JrAplicacion::getJrUrl(array('examenes'))?>');
           }
        }
      });
    }
  });
  
  $('.table').DataTable( {
    "language": {
            "url": "<?php echo $this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma(); ?>.json"
    }
  });
});
</script>