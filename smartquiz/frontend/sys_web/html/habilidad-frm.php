<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('habilidad'); ?>&nbsp;<small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdmetodologia" id="pkidmetodologia" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <input type="hidden" name="txtTipo" id="txttipo" value="H">
          <div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            

            <div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 " style="text-align: left;">
              <a href="javascript:;"  class="btn-chkoption" > <i class="fa fa<?php echo !empty($frm["estado"])?"-check":""; ?>-circle-o fa-lg"></i>  <span><?php echo @$frm["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></span>
               <input type="hidden" name="txtEstado" value="<?php echo @$frm["estado"];?>" >
              </a>                                
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveMetodologia_habilidad" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('metodologia'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Metodologia', 'saveMetodologia', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Habilidad"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Habilidad"))?>');
        <?php endif;?>       }
     }
  });




$('.btn-chkoption').bind({
    click: function() {     
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $("i",this).removeClass().addClass('fa fa'+(data==1?'-check-':'-')+'circle-o');
      $("span",this).html(data==1?'<?php echo JrTexto::_("Active"); ?>':'<?php echo JrTexto::_("Inactive"); ?>');
      $("input",this).val(data);

     
    }
  });



  
  
});


</script>

