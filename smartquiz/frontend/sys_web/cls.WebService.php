<?php
/**
 * @autor       Eder Figueroa Piscoya
 * @fecha       2017-11-07
 * @copyright   Copyright (C) 2017. Todos los derechos reservados.
**/
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRol_personal', RUTA_BASE, 'sys_negocio');
class WebService extends JrWeb
{
	protected $oNegExamenes;
	protected $oNegProyecto;
	protected $oNegExamen_alumno;
	protected $oNegPersonal;
	protected $oNegRol_personal;

	public function __construct()
	{
		parent::__construct();
		$this->oNegExamenes = new NegExamenes;
		$this->oNegProyecto = new NegProyecto;
		$this->oNegExamen_alumno = new NegExamen_alumno;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegRol_personal = new NegRol_personal;
	}

	public function defecto()
	{
		echo "<h1>403 - Restricted access</h1>";
	}

	/** 
	* exam_info: Retorna un solo exámen con toda su información completa
	* @param  "pr" : id del proyecto (o empresa) afiliada a SmartQuiz
	* @param  "idexam" : id del examen a buscar
	*/
	public function exam_info()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$idProyecto = @$_REQUEST['pr'];
			$idExamen = @$_REQUEST['idexam'];
			$esarrayidalumno=stripos($idExamen,",");
			if($esarrayidalumno){
				$idExamen=explode(',',$idExamen);
			}
			
			$_filtros = array();
			
			$_filtros['idexamen'] = $idExamen;
			$_filtros['estado'] = 1;

			if($idProyecto!='ALL'){ 
				if(!$this->estaAfiliado($idProyecto)){ throw new Exception(JrTexto::_("Proyecto/Empresa no habilitada")); }
				if(empty($idExamen)){ throw new Exception(JrTexto::_("Falta parámetro 'idexam'")); }
				$_filtros['idproyecto'] = $this->proyecto['idproyecto'];
			}

			$examenes = $this->oNegExamenes->buscar($_filtros);
			if(empty($examenes)){ throw new Exception("Examen no encontrado."); }

			if($esarrayidalumno){
				$response = array(
				'code'=>200,
				'message' => 'Success',
				'data'=>$examenes
				);
			}else
			$response = array(
				'code'=>200,
				'message' => 'Success',
				'data'=>$examenes[0]
			);
			echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), json_encode($response));exit();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit();
		}
	}

	/** 
	* exam_list: Retorna listado de examenes con su información
	* @param  "pr" : id del proyecto (o empresa) afiliada a SmartQuiz
	* @param  "t" (opcional) : titulo del examen a buscar
	*/
	public function exam_list()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$_filtros = array();

			$idProyecto = @$_REQUEST['pr']; 
			if(!$this->estaAfiliado($idProyecto)){ throw new Exception(JrTexto::_("Proyecto/Empresa no habilitada")); }
			$titulo = @$_REQUEST['t'];
			if(!empty($titulo)){
				$_filtros['titulo'] = $this->proyecto['idproyecto'];
			}
			$_filtros['idproyecto'] = $this->proyecto['idproyecto'];
			$_filtros['estado'] = 1;
			if(isset($_REQUEST['xxidccxx']) && !empty($_REQUEST['xxidccxx'])){
				$_filtros['idcc_smartcourse'] = $_REQUEST['xxidccxx'];
			}
			if(isset($_REQUEST['xxidpersonaxx']) && !empty($_REQUEST['xxidpersonaxx'])){
				$_filtros['idpersona_smartcourse'] = $_REQUEST['xxidpersonaxx'];
			}
			
			$examenes = $this->oNegExamenes->buscar($_filtros);

			$response = array(
				'code'=>200,
				'message' => 'Success',
				'data'=>$examenes
			);
			echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), json_encode($response));exit();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit();
		}
	}

	/** 
	* exam_result: Retorna el reporte de un solo examen de un alumno
	* @param  "pr" : id del proyecto (o empresa) afiliada a SmartQuiz
	* @param  "idexam" : id del examen a buscar
	* @param  "idstudent" : id_alumno que rindió el examen
	*/
	public function exam_result()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$idProyecto = @$_REQUEST['pr']; 
			if(!$this->estaAfiliado($idProyecto)){ throw new Exception(JrTexto::_("Proyecto/Empresa no habilitada")); }
			if( $this->faltanParams(array("pr","idexam","idstudent")) ){ exit(0);  }

			$idExamen = @$_REQUEST['idexam'];
			$identifAlum = @$_REQUEST['idstudent'];
			if(empty($idExamen) && empty($idAlumno)){ throw new Exception(JrTexto::_("Falta parámetro: 'idexamen' , 'idstudent'")); }

			$resultadoXAlumno = $this->oNegExamen_alumno->getResultadoXAlumno(array(
				"idexamen"=>$idExamen,
				"idproyecto"=>$this->proyecto['idproyecto'],
				"estado"=>1,
				"identificador"=>$identifAlum
			));

			$resultado = $resultadoXAlumno["resultado"];
			$examen = $resultadoXAlumno["examen"];
			#$alumno = $resultadoXAlumno["alumno"];

			$resultado = array(
				'idexamen' =>  $resultado['idexamen']
				,'idalumno' => $identifAlum //$resultado['idalumno']
				,'puntaje' => $resultado['puntaje']
				,'tiempoduracion' => $resultado['tiempoduracion']
				,'fecha' => $resultado['fecha']
				,'intento' => $resultado['intento']
				#,'puntajehabilidad' => $resultado['puntajehabilidad']
				,'calificacion_min' => $examen['calificacion_min']
				,'calificacion_max' => $examen['calificacion_total']
				,'calificacion_unidad' => ($examen['calificacion_en']=='N')?'pts':($examen['calificacion_en']=='P')?'%':''
			);

			$response = array(
				'code'=>200,
				'message' => 'Success',
				'data'=>$resultado
			);
			echo json_encode($response); exit();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit();
		}
	}

	/** 
	* results_student: Retorna los reusltados de de un alumno
	* @param  "pr" : id del proyecto (o empresa) afiliada a SmartQuiz
	* @param  "idstudent" : id_alumno que rindió el examen
	*/
	public function results_student()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$arrTodosResultados = array();
			$idProyecto = @$_REQUEST['pr']; 
			if(!$this->estaAfiliado($idProyecto)){ throw new Exception(JrTexto::_("Proyecto/Empresa no habilitada")); }
			$identifAlum = @$_REQUEST['idstudent'];

			$alumnos = $this->oNegPersonal->buscar(array('identificador'=>$identifAlum, 'idproyecto'=>$this->proyecto['idproyecto']));
			if(empty($alumnos)){ throw new Exception("Alumno no encontrado."); }
			$alumno = $alumnos[0];

			$all_results = $this->oNegExamen_alumno->buscar(array('idalumno'=>$alumno['dni']));
			foreach ($all_results as $i => $r) {
				$examenes = $this->oNegExamenes->buscar(array('idexamen'=>$r['idexamen'], 'idproyecto'=>$this->proyecto['idproyecto'], 'estado'=>1));
				if(!empty($examenes)){
					$examen = $examenes[0];
					if($examen['calificacion']=='M'){ $order_by = array('puntaje DESC'); }
					else{ $order_by = array('fecha DESC'); }
					$resultado = $this->oNegExamen_alumno->buscar(array('idexamen'=>$examen['idexamen'], 'idalumno'=>$alumno['dni'],'order_by'=>$order_by));
					if(empty($resultado)){ throw new Exception("Resultado no encontrado."); }
					$resultado = $resultado[0];

					$result = array(
						'idexamen' =>  $resultado['idexamen']
						,'idalumno' => $identifAlum //$resultado['idalumno']
						,'puntaje' => $resultado['puntaje']
						,'tiempoduracion' => $resultado['tiempoduracion']
						,'fecha' => $resultado['fecha']
						,'idresultado' => $resultado['idexaalumno']
						,'intento' => $resultado['intento']
						,'puntajehabilidad' => $resultado['puntajehabilidad']
						,'examen_habilidades' => $examen['habilidades_todas']
						,'calificacion_min' => $examen['calificacion_min']
						,'calificacion_max' => $examen['calificacion_total']
						,'calificacion_unidad' => ($examen['calificacion_en']=='N')?'pts':($examen['calificacion_en']=='P')?'%':''
					);
					$arrTodosResultados[] = $result;
				}
				
			}

			$response = array(
				'code'=>200,
				'message' => 'Success',
				'data'=>$arrTodosResultados
			);
			echo json_encode($response); exit();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit();
		}
	}

	/** 
	* results_student: Retorna los reusltados de de un alumno
	* @param  "pr" : id del proyecto (o empresa) afiliada a SmartQuiz
	*/
	public function result_students_project()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$arrResultadosXAlum = array();
			$slugProyecto = @$_REQUEST['pr']; 
			if(!$this->estaAfiliado($slugProyecto)){ throw new Exception(JrTexto::_("Proyecto/Empresa no habilitada")); }

			$idProyecto = $this->proyecto['idproyecto'];
			$alumnos = $this->oNegPersonal->buscarConRol(array('idproyecto'=> $idProyecto, 'estado'=>1,  'rol'=>'alumno'));
			
			if(!empty($alumnos)){
				foreach ($alumnos as $i=>$a) {
					$arrResultados = $idExamsRendidos = array();
					$exam_result = $this->oNegExamen_alumno->buscarConExam(array('idalumno'=>$a['dni'], 'order_by'=>array('EA.idexamen', 'EA.intento')));
					foreach ($exam_result as $r) {
						if( !in_array($r['idexamen'], $idExamsRendidos) ){
							if($r['calificacion']=='M'){ $order_by = array('puntaje DESC'); } else { $order_by = array('fecha DESC'); }

							$result = $this->oNegExamen_alumno->buscarConExam(array('idexamen'=>$r['idexamen'], 'idalumno'=>$a['dni'],'order_by'=>$order_by));
							if(!empty($result)){ 
								$arrResultados[] = $result[0];
								$idExamsRendidos[] = $result[0]['idexamen'];
							}
						}
					}
					$nodo_alum = array(
						"idalumno" => $a['dni'],
						"ape_paterno" => $a['ape_paterno'],
						"ape_materno" => $a['ape_materno'],
						"nombre" => $a['nombre'],
						"identificador" => $a['identificador'],
						"foto" => $a['foto'],
						"resultados" => $arrResultados,
					);
					$arrResultadosXAlum[] = $nodo_alum;
				}
			}

			$response = array(
				'code'=>200,
				'message' => 'Success',
				'data'=>$arrResultadosXAlum
			);
			echo json_encode($response); exit();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit();
		}
	}

	/** 
	* crear_proyecto: Crea un proeycto nuevo y vacio (sin exámenes).
	* @param  "pr" (obligatorio): debe tener el valor de "superadmin" para identificarlo como SuperAdmin
	* @param  "nombre" (obligatorio): contiene el nombre del proyecto.
	* @param  "comentario" (opcional): contiene el comentario del proyecto.
	* @param  "slug" (opcional): contiene el slug que identificará al proyecto.
	* @param  "fecha_fin" (obligatorio): contiene fecha_fin del proyecto.
	* @param  "idioma" (obligatorio): contiene el idioma del proyecto: 'ES','EN'.
	* @param  "user_id" (obligatorio): contiene el idusuario del usario creador de exámenes.
	*/
	public function crear_proyecto()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$data = array();
			$slugProyecto = @$_REQUEST['pr']; 
			if($slugProyecto=='superadmin'){ throw new Exception(JrTexto::_("No puede acceder a este servicio")); }
			if( $this->faltanParams(array("pr","nombre","fecha_fin","idioma","user_id")) ){ exit(0);  }

			@extract($_REQUEST);

			$this->oNegProyecto->nombre = @$nombre;
			$this->oNegProyecto->comentario = @$comentario;
			$this->oNegProyecto->fecha_fin = @$fecha_fin;
			if(!empty($slug)) {
				$this->oNegProyecto->slug = @$slug;
				$proyecto = $this->oNegProyecto->getXSlug();
				if(!empty($proyecto)){ throw new Exception("El Slug de proyecto ya existe"); }
			}
			$this->oNegProyecto->estado = 1;
			$this->oNegProyecto->idioma = @$idioma;

			$idProyecto = $this->oNegProyecto->agregar();

			$searchProy = $this->oNegProyecto->buscar(array("idproyecto"=>$idProyecto));
			if(!empty($searchProy)) {
				$searchProy = $searchProy[0];

				/***** Agregando Nuevo Usuario *****/
				$username = $searchProy["slug"].'_admin';
				$password = '1234';
				$clave_md5 = md5($password);
				$this->oNegPersonal->ape_paterno = 'creado';
				$this->oNegPersonal->ape_materno = 'desde';
				$this->oNegPersonal->nombre = 'Service::crear_proyecto';
				$this->oNegPersonal->identificador = @$user_id;
				$this->oNegPersonal->idproyecto = $searchProy["idproyecto"];
				$this->oNegPersonal->usuario = $username;
				$this->oNegPersonal->clave = $clave_md5;
				$this->oNegPersonal->token = md5($username.' '.$clave_md5);
				$this->oNegPersonal->estado = 1;
				$this->oNegPersonal->regusuario = '_self_';
				$this->oNegPersonal->idioma = @$idioma;
				$idPersonal = $this->oNegPersonal->agregar();

				/***** Agregando Roles a Usuario como Creador y Alumno *****/
				$this->oNegRol_personal->idrol = '2'; //Creador
				$this->oNegRol_personal->idpersonal = $idPersonal;
				$idRol = $this->oNegRol_personal->agregar();
				$this->oNegRol_personal->idrol = '3'; //Alumno
				$this->oNegRol_personal->idpersonal = $idPersonal;
				$idRol = $this->oNegRol_personal->agregar();

				$data = array(
					"slug" => $searchProy['slug'],
					"user_id" => @$user_id,
					"username" => $username,
					"password" => $password,
				);
			}

			$response = array(
				'code'=>200,
				'message' => 'Success',
				'data'=>$data
			);
			echo json_encode($response); exit();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit();
		}
	}

	/*================= FUNCIONES PRIVADAS =================*/
	private function estaAfiliado($idProyecto=null)
	{
		try {
			if(empty($idProyecto)){ throw new Exception(JrTexto::_("Falta parámetro 'pr'")); }
			$this->oNegProyecto->slug = $idProyecto;
			$this->proyecto = $this->oNegProyecto->getXSlug();
			if(empty($this->proyecto)){
	            return false;
	        }
	        return true;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function faltanParams($arrParams = array())
	{
		try {
			$paramsMissing = array();
			foreach ($arrParams as $param) {
				if (!array_key_exists($param, $_GET)) {
					$paramsMissing[] = " '".$param."' ";
				}
			}

			if (!empty($paramsMissing)) {
				throw new Exception( JrTexto::_("Params are missing").': '.implode(' , ', $paramsMissing) );
			}

			return false;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}