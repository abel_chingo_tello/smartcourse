<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017 
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
class WebExamenes_preguntas extends JrWeb
{
	private $oNegExamenes_preguntas;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamenes_preguntas = new NegExamenes_preguntas;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Examenes_preguntas', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegExamenes_preguntas->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Examenes_preguntas'), true);
			$this->esquema = 'examenes_preguntas-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	

	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Examenes_preguntas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Examenes_preguntas').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Examenes_preguntas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegExamenes_preguntas->idpregunta = @$_GET['id'];
			$this->datos = $this->oNegExamenes_preguntas->dataExamenes_preguntas;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Examenes_preguntas').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegExamenes_preguntas->idpregunta = @$_GET['id'];
			$this->datos = $this->oNegExamenes_preguntas->dataExamenes_preguntas;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Examenes_preguntas').' /'.JrTexto::_('see'), true);
			$this->esquema = 'examenes_preguntas-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'examenes_preguntas-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveExamenes_preguntas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdpregunta'])) {
					$this->oNegExamenes_preguntas->idpregunta = $frm['pkIdpregunta'];
				}
				
				$this->oNegExamenes_preguntas->__set('idexamen',@$frm["txtIdexamen"]);
					$this->oNegExamenes_preguntas->__set('pregunta',@$frm["txtPregunta"]);
					$this->oNegExamenes_preguntas->__set('descripcion',@$frm["txtDescripcion"]);
					$this->oNegExamenes_preguntas->__set('ejercicio',@$frm["txtEjercicio"]);
					$this->oNegExamenes_preguntas->__set('idpadre',@$frm["txtIdpadre"]);
					$this->oNegExamenes_preguntas->__set('tiempo',@$frm["txtTiempo"]);
					$this->oNegExamenes_preguntas->__set('puntaje',@$frm["txtPuntaje"]);
					$this->oNegExamenes_preguntas->__set('idpersonal',@$frm["txtIdpersonal"]);
					$this->oNegExamenes_preguntas->__set('fecharegistro',@$frm["txtFecharegistro"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegExamenes_preguntas->agregar();
					}else{
									    $res=$this->oNegExamenes_preguntas->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegExamenes_preguntas->idpregunta);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDExamenes_preguntas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegExamenes_preguntas->__set('idpregunta', $pk);
				$this->datos = $this->oNegExamenes_preguntas->dataExamenes_preguntas;
				$res=$this->oNegExamenes_preguntas->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegExamenes_preguntas->__set('idpregunta', $pk);
				$res=$this->oNegExamenes_preguntas->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}