<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017 
 * @copyright	Copyright (C) 24-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegEstado_civil', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegCargos', RUTA_BASE, 'sys_negocio');
class WebAlumno extends JrWeb
{
	private $oNegAlumno;
	private $oNegUbigeo;
	private $oNegEstado_civil;
	private $oNegCargos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAlumno = new NegAlumno;
		$this->oNegUbigeo = new NegUbigeo;
		$this->oNegEstado_civil = new NegEstado_civil;
		$this->oNegCargos = new NegCargos;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$this->usuarioAct = NegSesion::getUsuario();
			$this->raiz = _http_.$_SERVER["SERVER_NAME"];
			$this->uniqid = uniqid();
			$this->datos=$this->oNegAlumno->buscar(['idempresa'=>$this->usuarioAct['idempresa'], ]);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'inicio';
			$this->documento->setTitulo(JrTexto::_('Alumno'), true);
			$this->esquema = 'alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function verficha(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.JrTexto::_('Ficha'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'alumno/ficha';
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!=''){
				$this->oNegAlumno->idalumno=$_REQUEST["id"];
				$this->datos=$this->oNegAlumno->dataAlumno;
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cambiarclave(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.JrTexto::_('Change Password'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->user='alumno';
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!=''){
				$this->oNegAlumno->idalumno=$_REQUEST["id"];
				$this->datos=$this->oNegAlumno->dataAlumno;
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}			
			$this->esquema = 'usuario/cambiarclave';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}	
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegAlumno->idalumno = @$_GET['id'];
			$this->datos = $this->oNegAlumno->dataAlumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->usuarioAct = NegSesion::getUsuario();
			//$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->estado_civil=$this->oNegEstado_civil->buscar();
			$this->area=$this->oNegCargos->buscar(array('tipo'=>'A', 'idempresa'=>$this->usuarioAct['idempresa']));
			//$this->fkubigeo=$this->oNegUbigeo->buscar();
			//$this->fkidugel=$this->oNegUgel->buscar();
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'inicio';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function xBuscar(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["apellidopaterno"])&&@$_REQUEST["texto"]!='')$filtros["apellidopaterno"]=$_REQUEST["texto"];
			if(isset($_REQUEST["apellidomaterno"])&&@$_REQUEST["texto"]!='')$filtros["apellidomaterno"]=$_REQUEST["texto"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["texto"]!='')$filtros["nombre"]=$_REQUEST["texto"];
			if(isset($_REQUEST["fechanacimiento"])&&@$_REQUEST["fechanacimiento"]!='')$filtros["fechanacimiento"]=$_REQUEST["fechanacimiento"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["idestadocivil"])&&@$_REQUEST["idestadocivil"]!='')$filtros["idestadocivil"]=$_REQUEST["idestadocivil"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["idcargo"])&&@$_REQUEST["idcargo"]!='')$filtros["idcargo"]=$_REQUEST["idcargo"];
			if(isset($_REQUEST["clave1"])&&@$_REQUEST["clave1"]!='')$filtros["clave1"]=$_REQUEST["clave1"];
			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$this->oNegAlumno->setLimite(0,99999);
			$this->datos=$this->oNegAlumno->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function json_datosxalumno()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST["id"]) || empty($_POST["tipo"])) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $promXHab = [];
            $this->alumnos = [];
            if($_POST['tipo']=='A'){
            	$filtros["idalumno"]=$_POST["id"];
            } elseif ($_POST['tipo']=='G') {
            	$filtros['idgrupo']=$_POST["id"];
            }
            $exams_alum=$this->oNegGrupo_matricula->buscar($filtros);
            foreach ($exams_alum as $al) {
            	$this->oNegAlumno->idalumno = $al['idalumno'];
            	$alumno=$this->oNegAlumno->getXid();
            	
            	$ultimaActividad=$this->oNegActividad_alumno->ultimaActividadxAlum($al['idalumno']);
            	
            	if($alumno['estado']==1){
            		$new_alum = [];
	            	$new_alum = [
	            		'idalumno' => $alumno['idalumno'],
	            		'apellidopaterno' => $alumno['apellidopaterno'],
	            		'apellidomaterno' => $alumno['apellidomaterno'],
	            		'nombre' => $alumno['nombre'],
	            		'email' => $alumno['email'],
	            		'foto' => $alumno['foto'],
	            		'ultima_actividad' => $ultimaActividad,
	            	];
	            	$this->alumnos[] = $new_alum;
            	}
            }
            $data=array('code'=>'ok','data'=>$this->alumnos);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
	}

	public function guardarAlumno(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkidalumno)) {
				$this->oNegAlumno->idalumno = $frm['pkidalumno'];
				$accion='_edit';
			}

        	global $aplicacion;         
        	$usuarioAct = NegSesion::getUsuario();
        	@extract($_POST);
        	JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
			$this->oNegAlumno->apellidopaterno=@$txtApellidopaterno;
			$this->oNegAlumno->apellidomaterno=@$txtApellidomaterno;
			$this->oNegAlumno->nombre=@$txtNombre;
			$this->oNegAlumno->fechanacimiento=@$txtFechanacimiento;
			$this->oNegAlumno->sexo=@$txtSexo;
			$this->oNegAlumno->dni=@$txtDni;
			$this->oNegAlumno->idestadocivil=@$txtidEstadocivil;
			$this->oNegAlumno->ubigeo=@$txtUbigeo;
			$this->oNegAlumno->direccion=@$txtDireccion;
			$this->oNegAlumno->telefono=@$txtTelefono;
			$this->oNegAlumno->celular=@$txtCelular;
			$this->oNegAlumno->email=@$txtEmail;
			$this->oNegAlumno->idugel=@$txtIdugel;
			$this->oNegAlumno->regusuario=@$txtRegusuario;
			$this->oNegAlumno->regfecha=@$txtRegfecha;
			$this->oNegAlumno->usuario=@$txtUsuario;
			$this->oNegAlumno->clave=@$txtClave;
			$this->oNegAlumno->clave1=@$txtclave1;
			$this->oNegAlumno->estado=@$txtEstado;
			$this->oNegAlumno->observacion=@$txtobservacion;
					
            if($accion=='_add') {
            	$res=$this->oNegAlumno->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAlumno->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAlumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				$usuarioAct = NegSesion::getUsuario();
				
				if(!empty($frm['pkidalumno'])) {
					$this->oNegAlumno->idalumno = $frm['pkidalumno'];
				}
				JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
				$this->oNegAlumno->apellidopaterno=@$frm["txtApellidopaterno"];
				$this->oNegAlumno->apellidomaterno=@$frm["txtApellidomaterno"];
				$this->oNegAlumno->nombre=@$frm["txtNombre"];
				$this->oNegAlumno->fechanacimiento=@$frm["txtFechanacimiento"];
				$this->oNegAlumno->sexo=@$frm["txtSexo"];
				$this->oNegAlumno->dni=@$frm["txtDni"];
				$this->oNegAlumno->idestadocivil=@$frm["txtIdEstadocivil"];
				$this->oNegAlumno->ubigeo=@$frm["txtUbigeo"];
				$this->oNegAlumno->direccion=@$frm["txtDireccion"];
				$this->oNegAlumno->telefono=@$frm["txtTelefono"];
				$this->oNegAlumno->celular=@$frm["txtCelular"];
				$this->oNegAlumno->email=@$frm["txtEmail"];
				$this->oNegAlumno->idugel=@$frm["txtIdugel"];
				$this->oNegAlumno->usuario=@$frm["txtUsuario"];
				$this->oNegAlumno->clave=@$frm["txtClave"];
				$this->oNegAlumno->clave1=@$frm["txtclave1"];
				$this->oNegAlumno->estado=@$frm["txtEstado"];
				$this->oNegAlumno->idcargo=@$frm["txtIdcargo"];
				$this->oNegAlumno->observacion=@$frm["txtobservacion"];
				$this->oNegAlumno->regusuario=@$usuarioAct["idusuario"];
				$this->oNegAlumno->idempresa=@$usuarioAct["idempresa"];
				$this->oNegAlumno->regfecha=date('Y-m-d');
				
			   if(@$frm["accion"]=="Nuevo"){
				
					$txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "alumno",false,100,100 );
					$this->oNegAlumno->__set('foto',@$txtFoto);
									    $res=$this->oNegAlumno->agregar();
				}else{
				
					$archivo=basename($frm["txtFoto_old"]);
					if(!empty($frm["txtFoto"])) $txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "alumno",false,100,100 );
					$this->oNegAlumno->__set('foto',@$txtFoto);
					@unlink(RUTA_SITIO . SD ."static/media/alumno/".$archivo);
									    $res=$this->oNegAlumno->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAlumno->idalumno);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAlumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAlumno->__set('idalumno', $pk);
				$this->datos = $this->oNegAlumno->dataAlumno;
				$res=$this->oNegAlumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAlumno->__set('idalumno', $pk);
				$res=$this->oNegAlumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAlumno->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}