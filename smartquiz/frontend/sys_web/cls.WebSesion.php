
<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
class WebSesion extends JrWeb
{
	private $oNegSesion;
	protected $oNegConfig;
	public $oNegHistorialSesion;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSesion = new NegSesion;
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegHistorialSesion = new NegHistorial_sesion;
	}
	public function defecto()
	{
		return $this->login();
	}
	public function login()
	{
		try {
			global $aplicacion;
			/**
			* ============== Documentación de código fuente ============== *
			* @title ------- Ingreso a la plataforma a través de URL -------
			* @author Abel Chingo Tello. (https://twitter.com/EderNick)
			* Ejemplo de acceso por URL para Admins:
			* midominio.com/smartquiz/?type=admin&id=<ID_ALUMNO>&pr=<SLUG_PROYECTO>&u=<USERNAME>&p=<PASSWORD> 
			* Ejemplo de acceso por URL para Alumno:
			* midominio.com/smartquiz/?id=<ID_ALUMNO>&pr=<SLUG_PROYECTO>&u=<USERNAME>&p=<PASSWORD> 
			*
			* @param type (opcional): si tiene este param con el valor "admin" le dirá a Smartquiz que se intenta acceder como Administrador (creador). Smartquiz verificará si tiene los permisos y según eso le dará acceso como creador o como un simple alumno.
			* @param id (obligatorio): es el id con el que Smartquiz identificará al estudiante.
			* @param pr (obligatorio): es el id único del proyecto proporcionado por Smartquiz para diferenciarlo de otros proyectos
			* @param u (opcional): username de acceso a su sistema, este campo se actualiza en la BD de smartquiz y sirve para ingresar a smartquiz con las misma credenciales que del sistema que viene.
			* @param p (opcional): password de acceso a su sistema (encriptado, de ser el caso), este campo se actualiza en la BD de smartquiz y sirve para ingresar a smartquiz con las misma credenciales que del sistema que viene.
			* @param callback (opcional) : Es una URL sin parametros GET, a la cual Smartquiz hará un POST enviando datos del resultado del examen de un alumno. Aplicable a "./examenes/resolver".
			* @param idexamen (opcional) : Id de examen al cual se quiere acceder. Aplicable a "./examenes/resolver"; "./examenes/ver".
			* @param lang (opcional) : Idioma del proyecto, solos irve cuando es un proyecto nuevo.
			**/
			if(!empty($_REQUEST["accesodirecto"])){
				$this->acceso();
			}

			$route_self=str_replace('index.php','',$_SERVER['PHP_SELF']);
			$redirected_url = _http_ . $_SERVER['HTTP_HOST'] . $route_self;
			if(!empty(@$_SERVER['REDIRECT_URL'])){
				if(strpos($_SERVER['REDIRECT_URL'], '/ver')!==false){ $redirected_url.='examenes/ver/'; }
				else if(strpos($_SERVER['REDIRECT_URL'], '/resolver')!==false){ $redirected_url.='examenes/resolver/'; }
				else if(strpos($_SERVER['REDIRECT_URL'], '/preguntas')!==false){ $redirected_url.='examenes/preguntas/'; }
				else if(strpos($_SERVER['REDIRECT_URL'], '/preview')!==false){ $redirected_url.='examenes/preview/'; }
				else if(strpos($_SERVER['REDIRECT_URL'], '/reportes')!==false){ $redirected_url.='examenes/reportes/'; }
				$redirected_url.=(!empty(@$_GET['idexamen']))?'?idexamen='.@$_GET['idexamen']:'';
			}
			$Oparams = array("id","pr","u","p","type","callback","idexamen","lang");
			$otrosParams = $this->agruparOtrosParams($Oparams);
			/* * * * Ingreso por URL (GET) * * * */
			if(!empty(@$_GET['id']) && !empty(@$_GET['pr'])) {
				if( true === $this->oNegSesion->ingresarxIdentifUser_Proyecto( array("identifier"=>@$_GET['id'], "project_slug"=>@$_GET['pr'], "user"=>@$_GET['u'], "password"=>@$_GET['p'], "tipoAcceso"=>@$_GET['type'], "callback"=>@$_GET["callback"], "lang"=>@$_GET["lang"], "otrosParams"=>$otrosParams,"urlback"=>@$_GET["urlback"]) ) ) {
					$this->cambiarIdiomaProyecto();
					$_TMP_GET = !empty($_GET) ? $_GET : array();
					$cond = array();
					if(!empty($_TMP_GET)){
						$_TMP_GET = array_flip($_TMP_GET);
						foreach ($Oparams as $val) {
							$find = array_search($val, $_TMP_GET);
							if($find !== false){
								unset($_TMP_GET[$find]);
							}
						}
						if(!empty($_TMP_GET)){
							$_TMP_GET = array_flip($_TMP_GET);
							foreach ($_TMP_GET as $key => $value) {
								$cond[] = "{$key}={$value}";
							}
							$redirected_url = $redirected_url."?".implode('&', $cond);
						}//endif
					}//endif empty					
					header('Location: '. $redirected_url );
					exit();
				}
			} else if(!empty(@$_GET['t'])) { // ** Obsoleto **
				if(true === $this->oNegSesion->ingresarxToken(@$_GET['t'], @$_GET['type'])) {
					$this->cambiarIdiomaProyecto();
					header('Location: '. $redirected_url );
					exit();
					//return $aplicacion->redir();
					/*return $aplicacion->redir(JrAplicacion::getJrUrl(array('perfil', 'cambiar-clave')), false);*/
				}
			}
			if(true === NegSesion::existeSesion()) {
				return $aplicacion->redir();
			}


			/* * * * Ingreso por Form (POST) * * * */
			@extract($_POST, EXTR_OVERWRITE);
			if(!empty($usuario) && !empty($clave)) {
				if(true === $this->oNegSesion->ingresar(@$usuario, @$clave)) {
					$this->cambiarIdiomaProyecto();
					header('Location: '. $redirected_url );
					exit();
					/*$this->iniciarHistorialSesion('P');*/
					//return $aplicacion->redir();
				}				
				$this->msjErrorLogin = true;
			}
			return $this->form(@$usuario);
		} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			return $this->form(@$usuario);
		}
	}

	public function acceso(){
		$datos=array();
		$datos["dni"]=!empty($_REQUEST["idpersonalquiz"])?$_REQUEST["idpersonalquiz"]:-100;
		$datos["idproyecto"]=!empty($_REQUEST["idproyectoquiz"])?$_REQUEST["idproyectoquiz"]:-100;
		if(true === $this->oNegSesion->accesos($datos)){			
			$user=NegSesion::getUsuario();			
			echo json_encode(array('code' => 200,'datos'=>$user));	
		}else
			echo json_encode(array('code' => 'Error'));
        exit();
	}

	protected function form($usuario = null)
	{
		try {
			global $aplicacion;			
			if(true == NegSesion::existeSesion()) {
				$aplicacion->redir();
			}			
			$this->usuario = $usuario;			
			$this->documento->plantilla = 'login';
			$this->esquema = 'login';
			return parent::getEsquema();
		} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}
	}	
	
	private function cambiarIdiomaProyecto()
	{
		/*** IDIOMA SEGUN PROYECTO ***/
		$this->idioma=NegSesion::getUsuario()['proyecto']['idioma'];
		$documento = &JrInstancia::getDocumento();
        $documento->setIdioma($this->idioma);
        NegSesion::set('idioma', $this->idioma, 'idioma__');
	}

	public function agruparOtrosParams($arrParamNecesarios = array())
	{
		try {
			global $aplicacion;
			$arrOtrosParams = array();
			if(empty($_GET)){ return $arrOtrosParams; }

			foreach ($_GET as $paramGet=>$valueGet) {
				if (!in_array($paramGet, $arrParamNecesarios)) {
					$arrOtrosParams[$paramGet] = $valueGet;
				}
			}
			return json_encode($arrOtrosParams);
		} catch (Exception $e) {
			$aplicacion->redir();
		}
	}

	public function salir()
	{
		try {
			global $aplicacion;
			if(true === NegSesion::existeSesion()) {
				/*$this->terminarHistorialSesion('P');*/
				$this->oNegSesion->salir();
			}
			$aplicacion->redir();
		} catch(Exception $e) {
			$aplicacion->redir();
		}
	}

	public function cambiar_ambito()
	{
		try {
			global $aplicacion;			
			if(!empty($_GET['ambito'])) {
				$oNegSesion = new NegSesion;
				$oNegSesion->cambiar_ambito($_GET['ambito']);
			}			
			$aplicacion->redir();
		} catch(Exception $e) {
			$aplicacion->redir();
		}
	}
	
	/*protected function iniciarHistorialSesion($lugar)
	{
		$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->tipousuario = ($usuarioAct['rol']=='Alumno')?'A':'P';
		$this->oNegHistorialSesion->idusuario = $usuarioAct['dni'];
		$this->oNegHistorialSesion->lugar = $lugar;
		$this->oNegHistorialSesion->fechaentrada = date('Y-m-d H:i:s');
		$this->oNegHistorialSesion->fechasalida = null;
		$idHistSesion = $this->oNegHistorialSesion->agregar();
		$sesion = JrSession::getInstancia();
		$sesion->set('idHistorialSesion', $idHistSesion, '__admin_m3c');
	}
	protected function terminarHistorialSesion($lugar)
	{
		$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->idhistorialsesion = $usuarioAct['idHistorialSesion'];
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$resp = $this->oNegHistorialSesion->editar();
	}*/

	// ========================== Funciones xajax ========================== //
	public function xSolicitarCambioClave(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0]['usuario'])) { return;}
				JrCargador::clase('sys_negocio::NegUsuario', RUTA_BASE, 'sys_negocio');
				$oNegUsuario = new NegUsuario;			
				$this->admin = $oNegUsuario->procesarSolicitudCambioClave($args[0]['usuario']);
				if(!empty($this->admin)) {
					try {
						$isonline=NegTools::isonline();
						if(!empty($isonline)){						
							global $configSitio;
							JrCargador::clase('jrAdwen::JrCorreo');
							$oCorreo = new JrCorreo;
							$oCorreo->setRemitente('desarrollo@sistecapps.com', JrTexto::_('System management'));
							$oCorreo->setAsunto(JrTexto::_('Change password'));						
							$this->esquema = 'syscorreo-recuperar-clave';
							$oCorreo->setMensaje(parent::getEsquema());
							$oCorreo->addDestinarioPhpmailer($this->admin['email'], $this->admin['nombre']);						
							$oCorreo->sendPhpmailer();
							$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention'))
								, $this->pasarHtml(JrTexto::_('If the email is correct you will receive a message with a link to change your password'))
								, 'success');
						}else{
							$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention'))
								, $this->pasarHtml(JrTexto::_('This option is only active in online mode security'))
								, 'info');
						}
					} catch(Exception $e) {}
				}				
				
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'error');
			} 
		}
	}

	public function xCambiarRol(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try{
				global $aplicacion;
				$usu = NegSesion::getUsuario();
				if($usu["rol"]==$usu["rol_original"]){
					$x=NegSesion::set('rol','Alumno');
				}else{
					$y=NegSesion::set('rol',$usu["rol_original"]);
				}
				$oRespAjax->call('redir', $this->documento->getUrlBase());
			}catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'error');
			} 
		}
	}
}