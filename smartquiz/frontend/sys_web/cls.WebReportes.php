<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegCargos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMatricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegCurso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBitacora', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMatricula_tema', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegReplica', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
#JrCargador::clase('sys_negocio::Neg...', RUTA_BASE, 'sys_negocio');
class WebReportes extends JrWeb
{
	public $usuarioAct;
	private $oNegAlumno;
	private $oNegCargos;
	private $oNegMatricula;
	private $oNegCurso;
	private $oNegBitacora;
	private $oNegMatricula_tema;
	private $oNegReplica;
	private $oNegExamen;
	private $oNegTarea;
	#private $oNeg...;

	public function __construct()
	{
		parent::__construct();		
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegAlumno = new NegAlumno;
		$this->oNegCargos = new NegCargos;
		$this->oNegMatricula = new NegMatricula;
		$this->oNegCurso = new NegCurso;
		$this->oNegBitacora = new NegBitacora;
		$this->oNegMatricula_tema = new NegMatricula_tema;
		$this->oNegReplica = new NegReplica;
		$this->oNegExamen = new NegExamen;
		$this->oNegTarea = new NegTarea;
		#$this->oNeg... = new Neg...;
	}

	public function defecto(){
		return $this->inicio();
	}

	public function inicio(){
		global $aplicacion;
		//if(!NegSesion::tiene_acceso('Reportes', 'list')) { 
			/*modificar tiene_acceso('Reportes' ...
			* por las tablas que se van a usar en este sitio */
			//throw new Exception(JrTexto::_('Restricted access').'!!');
		//}
		$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
        $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
        $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
        $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
        $this->documento->script('circle', '/libs/graficos/progressbar/');
        $this->documento->script('loader', '/libs/googlecharts/');

        $this->documento->script('highcharts', '/libs/highchart/');
        $this->documento->script('data', '/libs/highchart/modules/');
        $this->documento->script('drilldown', '/libs/highchart/modules/');
        $this->documento->script('exporting', '/libs/highchart/modules/');

        $this->reportes = array(
        	array('nombre'=>'Listado de inscripciones', 'ruta'=>'lista_inscripciones'),
        	array('nombre'=>'Listado de matriculados por curso', 'ruta'=>'lista_matriculas_curso'),
        	array('nombre'=>'Listado de cursos libres', 'ruta'=>'lista_cursos_libres'),
			array('nombre'=>'Porcentaje de avances por curso', 'ruta'=>'porcentaje_avance'),
			array('nombre'=>'Permanencia en el curso', 'ruta'=>'permanencia_curso'),
			array('nombre'=>'Notas y Alumnos aprobados por curso', 'ruta'=>'alumnos_aprobados'),
			array('nombre'=>'Productividad por curso', 'ruta'=>'progreso_examenes'),
			array('nombre'=>'Cumplimiento de tareas por curso', 'ruta'=>'cumplimiento_tareas'),
		);

		$this->documento->setTitulo(ucfirst(JrTexto::_('Reports')), true);
        $this->esquema = 'reportes/inicio';
        $this->documento->plantilla = 'inicio';
        return parent::getEsquema();
	} 

	public function lista_inscripciones()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			$this->oNegAlumno->setLimite(0,99999);
			$this->alumnos = $this->oNegAlumno->buscar(array('idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A'));
			if(!empty($this->alumnos)){
				$i=0;
				foreach ($this->alumnos as $a) {
					$cargo = array();
					$cargo = $this->oNegCargos->buscar(array('idcargo'=>$a['idcargo']));
					if(!empty($cargo)) $cargo = $cargo[0];
					$this->alumnos[$i]['cargo'] = $cargo;
					$i++;
				}
			}
			$this->esquema = 'reportes/lista_inscripciones';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function lista_matriculas_curso()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			$this->cursos = $this->oNegCurso->buscar(array('estado'=>'A', 'idcurso'=>array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110')  ));
			if(!empty($this->cursos)){
				$i=0;
				foreach ($this->cursos as $c) {
					$this->oNegMatricula->setLimite(0,999999);
					$matriculas = $this->oNegMatricula->buscar(array('idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A', 'idcurso'=>$c['idcurso']));
					$this->cursos[$i]['matriculas'] = $matriculas;
					$i++;
				}
			}
			$this->esquema = 'reportes/lista_matriculas_curso';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function lista_cursos_libres()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			$this->cursos = $this->oNegCurso->reporteCursosNoObligados(array('estado'=>'A', 'idcurso'=>array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110')  ));
			$this->esquema = 'reportes/lista_cursos_libres';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function asistencia_curso()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			# . . . este reporte podra ser visualizado por las diferentes areas, validar quien esta logueado actualmente....
			$this->cursos = $this->oNegCurso->buscar(array('estado'=>'A', 'idcurso'=>array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110')  ));
			
			$alumno = $this->oNegAlumno->buscar(array( 'idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A' ));
			$bitacora = $this->oNegBitacora->buscar(array( 'tipo'=>'C',  ));
			$this->esquema = 'reportes/asistencia_curso';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function porcentaje_avance()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			$this->esquema = 'reportes/porcentaje_avance';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function permanencia_curso()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			/*$this->areas =  $this->oNegCargos->buscar(array('idempresa'=>$this->usuarioAct['idempresa'], 'tipo'=>'A'));
			//echo var_dump($this->areas);
			$this->alumno = $this->oNegAlumno->buscar(array( 'idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A' ));*/
			$this->esquema = 'reportes/permanencia_curso';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function alumnos_aprobados()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			$this->esquema = 'reportes/alumnos_aprobados';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function progreso_examenes()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			$this->esquema = 'reportes/progreso_examenes';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cumplimiento_tareas()
	{
		try {
			global $aplicacion;
			#...verificar si tiene el permiso o el nivel de acceso requerido...
			$this->esquema = 'reportes/cumplimiento_tareas';
			return $this->reporteView();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function reporteView()
	{
		try {
			global $aplicacion;	

			$this->areas =  $this->oNegCargos->buscar(array('idempresa'=>$this->usuarioAct['idempresa'], 'tipo'=>'A'));
			//echo var_dump($this->areas);
			$this->alumno = $this->oNegAlumno->buscar(array( 'idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A' ));
			$this->cursos = $this->oNegCurso->buscar(array('estado'=>'A', 'idcurso'=>array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110')  ));

			$this->documento->plantilla = 'blanco';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	/*============================= Funciones AJAX =============================*/
	public function xMatriculadosCurso()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			if(empty($_GET['idcurso'])){ 
				throw new Exception(JrTexto::_("IdCurso is missing to filter enrolled students"));
			}
			$matriculas = $this->oNegMatricula->setLimite(0,999999);
			$matriculas = $this->oNegMatricula->buscar(array( 'idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A', 'idcurso'=>$_GET['idcurso'] ));
			if(!empty($matriculas)){
				foreach ($matriculas as $i=>$mat) {
					$this->oNegAlumno->idalumno = $mat['idalumno'];
					$alumno = $this->oNegAlumno->getXid();
					$matriculas[$i] = array_merge($alumno, $mat);
				}
			}
			echo json_encode(array('code'=>'ok', 'data'=>$matriculas, 'msje'=>JrTexto::_("List of enrolled") ));
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error', 'msje'=>JrTexto::_($e->getMessage()) ));
		}
	}

	public function xPorcentajeAvance()
	{
		$this->documento->plantilla = 'returnjson';
		try {

			$idCurso = @$_POST['idcurso'];
			$idAlumno = @$_POST['idalumno'];
			$idCargo = @$_POST['idcargo'];
			$data = $cursos = array();
			$filtros = array('estado'=>'A');
			if(!empty($idCurso)){ $filtros['idcurso'] = $idCurso; }
			else{  $filtros['idcurso'] = array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110'); }
			$cursos = $this->oNegCurso->buscar($filtros);
			if(!empty($cursos)){
				foreach ($cursos as $i=>$cur) {
					$matriculas = array();
					$filtro2 = array('idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A', 'idcurso'=>$cur['idcurso']);
					if(!empty($idAlumno)){ $filtro2['idalumno'] = $idAlumno; }
					if(!empty($idCargo)){ $filtro2['idcargo'] = $idCargo; }
					$matriculas = $this->oNegMatricula->setLimite(0,999999);
					$matriculas = $this->oNegMatricula->buscar( $filtro2 );
					if(!empty($matriculas)){
						foreach ($matriculas as $j=>$m) {
							$matr_temas = array();
							$suma_porcent_avance = 0;
							$promedio_porcent_avance = 0.0;
							$matr_temas = $this->oNegMatricula_tema->buscar(array( 'idmatricula'=>$m['idmatricula'] ));
							if(!empty($matr_temas)){
								$total_pest = 4; /* Son 4 pestañas las que se consideran */
								foreach ($matr_temas as  $k=>$mt) {
									$cant_pest_abiertas = 0;
									/******** Comprobando  pestañas ya realizadas ********/
									if(!empty($mt['accdia']) && $mt['accdia']>0){
										$cant_pest_abiertas++;
									}
									/*if(!empty($mt['accses']) && $mt['accses']>0){
										$cant_pest_abiertas++;
									}*/
									if(!empty($mt['accvid']) && $mt['accvid']>0){
										$cant_pest_abiertas++;
									}
									/*if(!empty($mt['accinter']) && $mt['accinter']>0){
										$cant_pest_abiertas++;
									}*/
									if(!empty($mt['accpdf']) && $mt['accpdf']>0){
										$cant_pest_abiertas++;
									}
									if(!empty($mt['accexa']) && $mt['accexa']>0){
										$cant_pest_abiertas++;
									}
									$pocentaje = ($cant_pest_abiertas*100)/$total_pest; 
									$suma_porcent_avance+=$pocentaje;
									$matr_temas[$k]['cant_pestanias_abiertas'] = $cant_pest_abiertas;
									$matr_temas[$k]['porcentaje_avance'] = $pocentaje;
								}
								$promedio_porcent_avance = $suma_porcent_avance/count($matr_temas);
							}
							$matriculas[$j]['matricula_tema'] = $matr_temas;
							$matriculas[$j]['promedio_avance'] = $promedio_porcent_avance;
						}
					}
					$cursos[$i]['matriculas'] = $matriculas;
				}
			}

			echo json_encode(array('code'=>'ok', 'data'=>$cursos, 'msje'=>JrTexto::_("Success") ));
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error', 'msje'=>JrTexto::_($e->getMessage()) ));
		}
	}

	public function xPermanenciaCurso_2($idalum=0,$idarea=0,$idsubarea=0)
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$idAlumno = ($idalum!=0)?$idalum:'';
			$idArea = ($idarea!=0)?$idarea:$_POST['idarea'];
			$idSubarea = ($idsubarea!=0)?$idsubarea:'';
			/*$matriculas_tema = $this->oNegMatricula_tema->reporteAvanceXSesion(array('idalumno'=>$idAlumno, 'idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A'));
			var_dump($matriculas_tema);
			exit();*/
			//echo $idArea."eve";
			if (!$idAlumno){
				$alumnos = $this->oNegMatricula->buscar_alumno(array( 'idempresa'=>$this->usuarioAct['idempresa'],'idcargo'=>$idArea ));	
			}else{
				$alumnos = $this->oNegMatricula->buscar_alumno(array( 'idempresa'=>$this->usuarioAct['idempresa'],'idalumno'=>$idAlumno,'idcargo'=>$idArea ));	
			}
			

			if(!empty($alumnos)){
			foreach ($alumnos as $y=>$a) {

				$alumnos[$y]['alumnos'] = $a['idalumno'];
				

			$matriculas = $this->oNegMatricula->buscar(array( 'idalumno'=>$a['idalumno'], 'idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A','idcurso'=>array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110') ));

				$alumnos[$y]['totalcursos'] = $matriculas;

				foreach ($matriculas as $i=>$m) {
					$suma_porcent_avance = 0;
					$promedio_porcent_avance = 0.0;

					$matr_temas = $this->oNegMatricula->permamencia(array( 'idalumno'=>$a['idalumno'],'idcurso'=>$m['idcurso'],'tipo'=>'C' ));

					//echo $a['idalumno']."<br>";					
					//echo $matr_temas."<br>";
					$alumnos[$y]['totalcursos'][$i]['idcurso'] = $m['idcurso'];
					$alumnos[$y]['totalcursos'][$i]['curso'] = $m['curso_nombre'];
					$alumnos[$y]['totalcursos'][$i]['tiempoxcurso'] = $matr_temas[0]['total'];

					
					//$matriculas[$i]['promedio_avance'] = $promedio_porcent_avance;
				}
			}//for alumnos

			}//if

			if($idalum!=0){ return $alumnos; }
			echo json_encode(array('code'=>'ok', 'data'=>$alumnos, 'msje'=>JrTexto::_("Success") ));
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error', 'msje'=>JrTexto::_($e->getMessage()) ));
		}
	}

	public function xPermanenciaCurso()
	{
		$this->documento->plantilla = 'returnjson';
		try {

			$idCurso = @$_POST['idcurso'];
			$idAlumno = @$_POST['idalumno'];
			$idCargo = @$_POST['idcargo'];
			$data = $cursos = array();
			$filtros = array('estado'=>'A');
			if(!empty($idCurso)){ $filtros['idcurso'] = $idCurso; }
			else{  $filtros['idcurso'] = array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110'); }
			$cursos = $this->oNegCurso->buscar($filtros);

			if(!empty($cursos)){
				foreach ($cursos as $i=>$cur) {
					$cantAprob = $cantDesap = $cantAlums = 0;
					$filtro2 = array('idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A', 'idcurso'=>$cur['idcurso']);
					if(!empty($idAlumno)){ $filtro2['idalumno'] = $idAlumno; }
					if(!empty($idCargo)){ $filtro2['idcargo'] = $idCargo; }
					$matriculas = $this->oNegMatricula->setLimite(0,999999);
					$matriculas = $this->oNegMatricula->buscar( $filtro2 );
					if(!empty($matriculas)){
						$tiempoXtema = 2*60*60; //segundos;
						foreach ($matriculas as $j=>$mat) {
							$time_seconds = 0;
							$cant_temas_curso = $this->oNegCurso->getNumTemasxCurso(array( 'idcurso'=>$cur['idcurso'] ));
							$matr_temas = $this->oNegMatricula->permamencia(array( 'idalumno'=>$mat['idalumno'],'idcurso'=>$cur['idcurso'],'tipo'=>'C' ));
							
							$tiempoXcursoTotal = $cant_temas_curso*$tiempoXtema;

							$matriculas[$j]['idcurso'] = $cur['idcurso'];
							$matriculas[$j]['curso'] = $cur['nombre'];
							$t_curso = $matr_temas[0]['total'];
							$matriculas[$j]['tiempoxcurso'] = '00:00:00';
							if(!empty($t_curso)){
								$matriculas[$j]['tiempoxcurso'] = $t_curso;

								sscanf($t_curso, "%d:%d:%d", $hours, $minutes, $seconds);

								$time_seconds += isset($hours) ? $hours * 3600 : 0;
								$time_seconds += isset($minutes) ? $minutes * 60 : 0;
								$time_seconds += isset($seconds) ? $seconds : 0;
							}

							$matriculas[$j]['tiempoXcursoTotal'] = $tiempoXcursoTotal;
							$matriculas[$j]['cant_temas_curso'] = $cant_temas_curso;
							$matriculas[$j]['tiempoxcurso_seg'] = $time_seconds;
							$matriculas[$j]['tiempoxcurso_porcent'] = ($time_seconds/$tiempoXcursoTotal)*100;
						}
					}
					$cursos[$i]['matriculas'] = $matriculas;
				}
			}

			echo json_encode(array('code'=>'ok', 'data'=>$cursos, 'msje'=>JrTexto::_("Success") ));
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error', 'msje'=>JrTexto::_($e->getMessage()) ));
		}
	}

	public function xNotaXCurso()
	{
		$this->documento->plantilla = 'returnjson';
		try {

			$idCurso = @$_POST['idcurso'];
			$idAlumno = @$_POST['idalumno'];
			$idCargo = @$_POST['idcargo'];
			$data = $cursos = array();
			$filtros = array('estado'=>'A');
			if(!empty($idCurso)){ $filtros['idcurso'] = $idCurso; }
			else{  $filtros['idcurso'] = array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110'); }
			$cursos = $this->oNegCurso->buscar($filtros);

			if(!empty($cursos)){
				foreach ($cursos as $i=>$cur) {
					$cantAprob = $cantDesap = $cantAlums = 0;
					$filtro2 = array('idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A', 'idcurso'=>$cur['idcurso']);
					if(!empty($idAlumno)){ $filtro2['idalumno'] = $idAlumno; }
					if(!empty($idCargo)){ $filtro2['idcargo'] = $idCargo; }
					$matriculas = $this->oNegMatricula->setLimite(0,999999);
					$matriculas = $this->oNegMatricula->buscar( $filtro2 );
					if(!empty($matriculas)){
						$cantAlums = count($matriculas);
						
						foreach ($matriculas as $j=>$mat) {
							/******** Tareas ********/
							$suma_notas = $promedioTareas = $notaExamenFinal = $notaPromedio = 0.0;
							$tarea_alumnos = $this->oNegReplica->buscar(array( 'idempresa'=>$this->usuarioAct['idempresa'], 'idtema'=>$cur['idcurso'], 'usuario'=>$mat['idalumno'] ));

							if(!empty($tarea_alumnos)){
								foreach ($tarea_alumnos as $k=>$ta) {
									$suma_notas += (float)$ta['nota'];
								}
								$promedioTareas = $suma_notas/count($tarea_alumnos);
							}
							$matriculas[$j]['promedio_tareas'] = $promedioTareas;

							/******** Examen Final ********/
							$examen_final = $this->oNegExamen->buscar(array( 'idexamen'=>$mat['idmatricula'].'99' , 'tipo'=>'F' ));
							if(!empty($examen_final)){ 
								$examen_final = $examen_final[0];
								$notaExamenFinal = (float)$examen_final['puntaje'];
							}
							$matriculas[$j]['nota_examen_final'] = $notaExamenFinal;

							$notaPromedio = ($promedioTareas+$notaExamenFinal)/2;
							$matriculas[$j]['nota_promedio'] = $notaPromedio;
							if($notaPromedio>=11){
								$cantAprob++;
							} else {
								$cantDesap++;
							}
						}
					}
					$cursos[$i]['matriculas'] = $matriculas;
					$cursos[$i]['cant_aprobados'] = $cantAprob;
					$cursos[$i]['cant_desaprobados'] = $cantDesap;
					$cursos[$i]['cant_alumnos_total'] = $cantAlums;
				}
			}

			echo json_encode(array('code'=>'ok', 'data'=>$cursos, 'msje'=>JrTexto::_("Success") ));
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error', 'msje'=>JrTexto::_($e->getMessage()) ));
		}
	}

	public function xExamenesXCurso()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$idCurso = @$_POST['idcurso'];
			$idAlumno = @$_POST['idalumno'];
			$idCargo = @$_POST['idcargo'];
			$data = $cursos = array();
			$filtros = array('estado'=>'A');
			if(!empty($idCurso)){ $filtros['idcurso'] = $idCurso; }
			else{  $filtros['idcurso'] = array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110'); }
			$cursos = $this->oNegCurso->buscar($filtros);

			if(!empty($cursos)){
				foreach ($cursos as $i=>$cur) {
					$matriculas = array();
					$filtro2 = array('idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A', 'idcurso'=>$cur['idcurso']);
					if(!empty($idAlumno)){ $filtro2['idalumno'] = $idAlumno; }
					if(!empty($idCargo)){ $filtro2['idcargo'] = $idCargo; }
					$matriculas = $this->oNegMatricula->setLimite(0,999999);
					$matriculas = $this->oNegMatricula->buscar( $filtro2 );
					if(!empty($matriculas)){
						foreach ($matriculas as $j=>$mat) {
							$nota_inicial = $nota_final = 0.0;
							$examenes_ini_fin = array();
							$examenes_ini_fin = $this->oNegExamen->buscar(array( 'idexamen'=>array($mat['idmatricula'].'00', $mat['idmatricula'].'99') , 'tipo'=>'F' ));
							if(!empty($examenes_ini_fin)){
								foreach ($examenes_ini_fin as $k=>$e) {
									if(substr( $e['idexamen'] , -2)=='00'){
										$nota_inicial = (float)$e['puntaje'];
									}
									if(substr( $e['idexamen'] , -2)=='99'){
										$nota_final = (float)$e['puntaje'];
									}
								}

							}
							$matriculas[$j]['examenes']=$examenes_ini_fin;
							$matriculas[$j]['nota_exam_inicial'] = $nota_inicial;
							$matriculas[$j]['nota_exam_final'] = $nota_final;
						}
					}
					$cursos[$i]['matriculas'] = $matriculas;
				}
			}

			echo json_encode(array('code'=>'ok', 'data'=>$cursos, 'msje'=>JrTexto::_("Success") ));
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error', 'msje'=>JrTexto::_($e->getMessage()) ));
		}
	}

	public function xTareasXCurso()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$idCurso = @$_POST['idcurso'];
			$idAlumno = @$_POST['idalumno'];
			$idCargo = @$_POST['idcargo'];
			$data = $cursos = array();
			$filtros = array('estado'=>'A');
			if(!empty($idCurso)){ $filtros['idcurso'] = $idCurso; }
			else{  $filtros['idcurso'] = array('0000','0010','0043','0044','0045','0046','0048','0049','0065','0068','0069','0070','0071','0089','0090','0091','0092','0093','0094','0095','0096','0102','0103','0104','0109','0110'); }
			$cursos = $this->oNegCurso->buscar($filtros);

			if(!empty($cursos)){
				foreach ($cursos as $i=>$cur) {
					$matriculas = array();
					$filtro2 = array('idempresa'=>$this->usuarioAct['idempresa'], 'estado'=>'A', 'idcurso'=>$cur['idcurso']);
					if(!empty($idAlumno)){ $filtro2['idalumno'] = $idAlumno; }
					if(!empty($idCargo)){ $filtro2['idcargo'] = $idCargo; }
					$matriculas = $this->oNegMatricula->setLimite(0,999999);
					$matriculas = $this->oNegMatricula->buscar( $filtro2 );
					if(!empty($matriculas)){
						foreach ($matriculas as $j=>$mat) {
							$suma_notas = $promedioTareas = 0.0;
							$tarea_alumnos = $this->oNegReplica->buscar(array('usuario'=>$mat['idalumno'], 'idtema'=>$cur['idcurso'], /*'idempresa'=>$this->usuarioAct['idempresa']*/ ));
							if(!empty($tarea_alumnos)){
								foreach ($tarea_alumnos as $k=>$ta) {
									$suma_notas += (float)$ta['nota'];
								}
								$promedioTareas = $suma_notas/count($tarea_alumnos);
							}
							$matriculas[$j]['promedio_tareas'] = $promedioTareas;
						}
					}
					$cursos[$i]['matriculas'] = $matriculas;
				}
			}

			echo json_encode(array('code'=>'ok', 'data'=>$cursos, 'msje'=>JrTexto::_("Success") ));
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error', 'msje'=>JrTexto::_($e->getMessage()) ));
		}
	}
}