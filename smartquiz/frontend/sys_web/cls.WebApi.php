<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto_config', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRol', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRol_personal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBiblioteca', RUTA_BASE, 'sys_negocio');

class WebApi extends JrWeb
{	
	private $oNegNiveles;
	private $oNegResources;
    protected $oNegMetodologia;
    protected $oNegPersonal;
    protected $oNegProyecto;
    protected $oNegProyecto_config;
    protected $oNegRol;
	protected $oNegRol_personal;
	protected $oNegConfiguracion;
	protected $oNegExamen_alumno;
	protected $oNegExamenes_preguntas;
	protected $oNegExamenes;
	protected $oNegBiblioteca;

	public function __construct()
	{
		parent::__construct();
		/*$this->oNegNiveles = new NegNiveles;*/
		$this->oNegResources = new NegResources;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegPersonal = new NegPersonal;
        $this->oNegProyecto = new NegProyecto;
        $this->oNegProyecto_config = new NegProyecto_config;
        $this->oNegRol = new NegRol;
        $this->oNegRol_personal = new NegRol_personal;
        $this->oNegConfiguracion = new NegConfiguracion;
        $this->oNegExamen_alumno = new NegExamen_alumno;
        $this->oNegExamenes_preguntas = new NegExamenes_preguntas;
        $this->oNegExamenes = new NegExamenes;
        $this->oNegBiblioteca = new NegBiblioteca;
	}
	private function printJSON($code='200',$msj = '', $data=array(),$extra=array()){
		echo json_encode(array('code'=>strval($code),'msj'=>$msj,'data'=>$data,'extra'=> $extra));
			exit(0);
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$resultado = array();
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de una tabla en especifico tabla
	* @param String nombre de la tabla
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado(){
		try {
			global $aplicacion;
			$tabla = (isset($_REQUEST['tabla']) && !empty($_REQUEST['tabla'])) ? $_REQUEST['tabla'] : null;
			$tablaname = "listado_".$tabla;
			if(method_exists($this, "listado_".$tabla)){
				$this->$tablaname();
			}else{
				$this->listado_personal();	
			}
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla personal
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_personal(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['dni']) && !empty($_REQUEST['dni'])){ $filtros['dni'] = $_REQUEST['dni']; }
			if(isset($_REQUEST['ape_paterno']) && !empty($_REQUEST['ape_paterno'])){ $filtros['ape_paterno'] = $_REQUEST['ape_paterno']; }
			if(isset($_REQUEST['ape_materno']) && !empty($_REQUEST['ape_materno'])){ $filtros['ape_materno'] = $_REQUEST['ape_materno']; }
			if(isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre'])){ $filtros['nombre'] = $_REQUEST['nombre']; }
			if(isset($_REQUEST['fechanac']) && !empty($_REQUEST['fechanac'])){ $filtros['fechanac'] = $_REQUEST['fechanac']; }
			if(isset($_REQUEST['sexo']) && !empty($_REQUEST['sexo'])){ $filtros['sexo'] = $_REQUEST['sexo']; }
			if(isset($_REQUEST['estado_civil']) && !empty($_REQUEST['estado_civil'])){ $filtros['estado_civil'] = $_REQUEST['estado_civil']; }
			if(isset($_REQUEST['ubigeo']) && !empty($_REQUEST['ubigeo'])){ $filtros['ubigeo'] = $_REQUEST['ubigeo']; }
			if(isset($_REQUEST['urbanizacion']) && !empty($_REQUEST['urbanizacion'])){ $filtros['urbanizacion'] = $_REQUEST['urbanizacion']; }
			if(isset($_REQUEST['direccion']) && !empty($_REQUEST['direccion'])){ $filtros['direccion'] = $_REQUEST['direccion']; }
			if(isset($_REQUEST['telefono']) && !empty($_REQUEST['telefono'])){ $filtros['telefono'] = $_REQUEST['telefono']; }
			if(isset($_REQUEST['celular']) && !empty($_REQUEST['celular'])){ $filtros['celular'] = $_REQUEST['celular']; }
			if(isset($_REQUEST['email']) && !empty($_REQUEST['email'])){ $filtros['email'] = $_REQUEST['email']; }
			if(isset($_REQUEST['idproyecto']) && !empty($_REQUEST['idproyecto'])){ $filtros['idproyecto'] = $_REQUEST['idproyecto']; }
			if(isset($_REQUEST['identificador']) && !empty($_REQUEST['identificador'])){ $filtros['identificador'] = $_REQUEST['identificador']; }
			if(isset($_REQUEST['regusuario']) && !empty($_REQUEST['regusuario'])){ $filtros['regusuario'] = $_REQUEST['regusuario']; }
			if(isset($_REQUEST['regfecha']) && !empty($_REQUEST['regfecha'])){ $filtros['regfecha'] = $_REQUEST['regfecha']; }
			if(isset($_REQUEST['usuario']) && !empty($_REQUEST['usuario'])){ $filtros['usuario'] = $_REQUEST['usuario']; }
			if(isset($_REQUEST['clave']) && !empty($_REQUEST['clave'])){ $filtros['clave'] = $_REQUEST['clave']; }
			if(isset($_REQUEST['token']) && !empty($_REQUEST['token'])){ $filtros['token'] = $_REQUEST['token']; }
			if(isset($_REQUEST['foto']) && !empty($_REQUEST['foto'])){ $filtros['foto'] = $_REQUEST['foto']; }
			if(isset($_REQUEST['estado']) && !empty($_REQUEST['estado'])){ $filtros['estado'] = $_REQUEST['estado']; }
			if(isset($_REQUEST['idioma']) && !empty($_REQUEST['idioma'])){ $filtros['idioma'] = $_REQUEST['idioma']; }

			$resultado = $this->oNegPersonal->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla biblioteca
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_biblioteca(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['idbiblioteca']) && !empty($_REQUEST['idbiblioteca'])){ $filtros['idbiblioteca'] = $_REQUEST['idbiblioteca']; }
			if(isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre'])){ $filtros['nombre'] = $_REQUEST['nombre']; }
			if(isset($_REQUEST['link']) && !empty($_REQUEST['link'])){ $filtros['link'] = $_REQUEST['link']; }
			if(isset($_REQUEST['tipo']) && !empty($_REQUEST['tipo'])){ $filtros['tipo'] = $_REQUEST['tipo']; }
			if(isset($_REQUEST['idproyecto']) && !empty($_REQUEST['idproyecto'])){ $filtros['idproyecto'] = $_REQUEST['idproyecto']; }
			if(isset($_REQUEST['idpersonal']) && !empty($_REQUEST['idpersonal'])){ $filtros['idpersonal'] = $_REQUEST['idpersonal']; }
			if(isset($_REQUEST['fechareg']) && !empty($_REQUEST['fechareg'])){ $filtros['fechareg'] = $_REQUEST['fechareg']; }
			if(isset($_REQUEST['estado']) && !empty($_REQUEST['estado'])){ $filtros['estado'] = $_REQUEST['estado']; }
			
			$resultado = $this->oNegBiblioteca->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla examenes
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_examenes(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['idexamen']) && !empty($_REQUEST['idexamen'])){ $filtros['idexamen'] = $_REQUEST['idexamen']; }
			if(isset($_REQUEST['titulo']) && !empty($_REQUEST['titulo'])){ $filtros['titulo'] = $_REQUEST['titulo']; }
			if(isset($_REQUEST['descripcion']) && !empty($_REQUEST['descripcion'])){ $filtros['descripcion'] = $_REQUEST['descripcion']; }
			if(isset($_REQUEST['portada']) && !empty($_REQUEST['portada'])){ $filtros['portada'] = $_REQUEST['portada']; }
			if(isset($_REQUEST['fuente']) && !empty($_REQUEST['fuente'])){ $filtros['fuente'] = $_REQUEST['fuente']; }
			if(isset($_REQUEST['fuentesize']) && !empty($_REQUEST['fuentesize'])){ $filtros['fuentesize'] = $_REQUEST['fuentesize']; }
			if(isset($_REQUEST['aleatorio']) && !empty($_REQUEST['aleatorio'])){ $filtros['aleatorio'] = $_REQUEST['aleatorio']; }
			if(isset($_REQUEST['calificacion_por']) && !empty($_REQUEST['calificacion_por'])){ $filtros['calificacion_por'] = $_REQUEST['calificacion_por']; }
			if(isset($_REQUEST['calificacion_en']) && !empty($_REQUEST['calificacion_en'])){ $filtros['calificacion_en'] = $_REQUEST['calificacion_en']; }
			if(isset($_REQUEST['calificacion_total']) && !empty($_REQUEST['calificacion_total'])){ $filtros['calificacion_total'] = $_REQUEST['calificacion_total']; }
			if(isset($_REQUEST['calificacion_min']) && !empty($_REQUEST['calificacion_min'])){ $filtros['calificacion_min'] = $_REQUEST['calificacion_min']; }
			if(isset($_REQUEST['tiempo_por']) && !empty($_REQUEST['tiempo_por'])){ $filtros['tiempo_por'] = $_REQUEST['tiempo_por']; }
			if(isset($_REQUEST['tiempo_total']) && !empty($_REQUEST['tiempo_total'])){ $filtros['tiempo_total'] = $_REQUEST['tiempo_total']; }
			if(isset($_REQUEST['tipo']) && !empty($_REQUEST['tipo'])){ $filtros['tipo'] = $_REQUEST['tipo']; }
			if(isset($_REQUEST['estado']) && !empty($_REQUEST['estado'])){ $filtros['estado'] = $_REQUEST['estado']; }
			if(isset($_REQUEST['idproyecto']) && !empty($_REQUEST['idproyecto'])){ $filtros['idproyecto'] = $_REQUEST['idproyecto']; }
			if(isset($_REQUEST['idpersonal']) && !empty($_REQUEST['idpersonal'])){ $filtros['idpersonal'] = $_REQUEST['idpersonal']; }
			if(isset($_REQUEST['fecharegistro']) && !empty($_REQUEST['fecharegistro'])){ $filtros['fecharegistro'] = $_REQUEST['fecharegistro']; }
			if(isset($_REQUEST['nintento']) && !empty($_REQUEST['nintento'])){ $filtros['nintento'] = $_REQUEST['nintento']; }
			if(isset($_REQUEST['calificacion']) && !empty($_REQUEST['calificacion'])){ $filtros['calificacion'] = $_REQUEST['calificacion']; }
			if(isset($_REQUEST['origen_habilidades']) && !empty($_REQUEST['origen_habilidades'])){ $filtros['origen_habilidades'] = $_REQUEST['origen_habilidades']; }
			if(isset($_REQUEST['habilidades_todas']) && !empty($_REQUEST['habilidades_todas'])){ $filtros['habilidades_todas'] = $_REQUEST['habilidades_todas']; }
			if(isset($_REQUEST['fuente_externa']) && !empty($_REQUEST['fuente_externa'])){ $filtros['fuente_externa'] = $_REQUEST['fuente_externa']; }
			if(isset($_REQUEST['dificultad_promedio']) && !empty($_REQUEST['dificultad_promedio'])){ $filtros['dificultad_promedio'] = $_REQUEST['dificultad_promedio']; }
			if(isset($_REQUEST['tiene_certificado']) && !empty($_REQUEST['tiene_certificado'])){ $filtros['tiene_certificado'] = $_REQUEST['tiene_certificado']; }
			if(isset($_REQUEST['nombre_certificado']) && !empty($_REQUEST['nombre_certificado'])){ $filtros['nombre_certificado'] = $_REQUEST['nombre_certificado']; }

			$resultado = $this->oNegExamenes->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla examenes_preguntas
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_examenes_preguntas(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['idpregunta']) && !empty($_REQUEST['idpregunta'])){ $filtros['idpregunta'] = $_REQUEST['idpregunta']; }
			if(isset($_REQUEST['idexamen']) && !empty($_REQUEST['idexamen'])){ $filtros['idexamen'] = $_REQUEST['idexamen']; }
			if(isset($_REQUEST['pregunta']) && !empty($_REQUEST['pregunta'])){ $filtros['pregunta'] = $_REQUEST['pregunta']; }
			if(isset($_REQUEST['descripcion']) && !empty($_REQUEST['descripcion'])){ $filtros['descripcion'] = $_REQUEST['descripcion']; }
			if(isset($_REQUEST['ejercicio']) && !empty($_REQUEST['ejercicio'])){ $filtros['ejercicio'] = $_REQUEST['ejercicio']; }
			if(isset($_REQUEST['idpadre']) && !empty($_REQUEST['idpadre'])){ $filtros['idpadre'] = $_REQUEST['idpadre']; }
			if(isset($_REQUEST['tiempo']) && !empty($_REQUEST['tiempo'])){ $filtros['tiempo'] = $_REQUEST['tiempo']; }
			if(isset($_REQUEST['puntaje']) && !empty($_REQUEST['puntaje'])){ $filtros['puntaje'] = $_REQUEST['puntaje']; }
			if(isset($_REQUEST['idpersonal']) && !empty($_REQUEST['idpersonal'])){ $filtros['idpersonal'] = $_REQUEST['idpersonal']; }
			if(isset($_REQUEST['fecharegistro']) && !empty($_REQUEST['fecharegistro'])){ $filtros['fecharegistro'] = $_REQUEST['fecharegistro']; }
			if(isset($_REQUEST['template']) && !empty($_REQUEST['template'])){ $filtros['template'] = $_REQUEST['template']; }
			if(isset($_REQUEST['habilidades']) && !empty($_REQUEST['habilidades'])){ $filtros['habilidades'] = $_REQUEST['habilidades']; }
			if(isset($_REQUEST['idcontenedor']) && !empty($_REQUEST['idcontenedor'])){ $filtros['idcontenedor'] = $_REQUEST['idcontenedor']; }
			if(isset($_REQUEST['dificultad']) && !empty($_REQUEST['dificultad'])){ $filtros['dificultad'] = $_REQUEST['dificultad']; }
			if(isset($_REQUEST['idpregunta_origen']) && !empty($_REQUEST['idpregunta_origen'])){ $filtros['idpregunta_origen'] = $_REQUEST['idpregunta_origen']; }
			
			$resultado = $this->oNegExamenes_preguntas->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla examen_alumno
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_examen_alumno(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['idexaalumno']) && !empty($_REQUEST['idexaalumno'])){ $filtros['idexaalumno'] = $_REQUEST['idexaalumno']; }
			if(isset($_REQUEST['idexamen']) && !empty($_REQUEST['idexamen'])){ $filtros['idexamen'] = $_REQUEST['idexamen']; }
			if(isset($_REQUEST['idalumno']) && !empty($_REQUEST['idalumno'])){ $filtros['idalumno'] = $_REQUEST['idalumno']; }
			if(isset($_REQUEST['preguntas']) && !empty($_REQUEST['preguntas'])){ $filtros['preguntas'] = $_REQUEST['preguntas']; }
			if(isset($_REQUEST['resultado']) && !empty($_REQUEST['resultado'])){ $filtros['resultado'] = $_REQUEST['resultado']; }
			if(isset($_REQUEST['puntajehabilidad']) && !empty($_REQUEST['puntajehabilidad'])){ $filtros['puntajehabilidad'] = $_REQUEST['puntajehabilidad']; }
			if(isset($_REQUEST['puntaje']) && !empty($_REQUEST['puntaje'])){ $filtros['puntaje'] = $_REQUEST['puntaje']; }
			if(isset($_REQUEST['resultadojson']) && !empty($_REQUEST['resultadojson'])){ $filtros['resultadojson'] = $_REQUEST['resultadojson']; }
			if(isset($_REQUEST['tiempoduracion']) && !empty($_REQUEST['tiempoduracion'])){ $filtros['tiempoduracion'] = $_REQUEST['tiempoduracion']; }
			if(isset($_REQUEST['fecha']) && !empty($_REQUEST['fecha'])){ $filtros['fecha'] = $_REQUEST['fecha']; }
			if(isset($_REQUEST['intento']) && !empty($_REQUEST['intento'])){ $filtros['intento'] = $_REQUEST['intento']; }
			

			$resultado = $this->oNegExamen_alumno->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla proyecto
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_proyecto(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['idproyecto']) && !empty($_REQUEST['idproyecto'])){ $filtros['idproyecto'] = $_REQUEST['idproyecto']; }
			if(isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre'])){ $filtros['nombre'] = $_REQUEST['nombre']; }
			if(isset($_REQUEST['comentario']) && !empty($_REQUEST['comentario'])){ $filtros['comentario'] = $_REQUEST['comentario']; }
			if(isset($_REQUEST['slug']) && !empty($_REQUEST['slug'])){ $filtros['slug'] = $_REQUEST['slug']; }
			if(isset($_REQUEST['fecha_inicio']) && !empty($_REQUEST['fecha_inicio'])){ $filtros['fecha_inicio'] = $_REQUEST['fecha_inicio']; }
			if(isset($_REQUEST['fecha_fin']) && !empty($_REQUEST['fecha_fin'])){ $filtros['fecha_fin'] = $_REQUEST['fecha_fin']; }
			if(isset($_REQUEST['estado_civil']) && !empty($_REQUEST['estado_civil'])){ $filtros['estado_civil'] = $_REQUEST['estado_civil']; }
			if(isset($_REQUEST['estado']) && !empty($_REQUEST['estado'])){ $filtros['estado'] = $_REQUEST['estado']; }
			if(isset($_REQUEST['idioma']) && !empty($_REQUEST['idioma'])){ $filtros['idioma'] = $_REQUEST['idioma']; }
			

			$resultado = $this->oNegProyecto->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla proyecto_config
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_proyecto_config(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['idconfig']) && !empty($_REQUEST['idconfig'])){ $filtros['idconfig'] = $_REQUEST['idconfig']; }
			if(isset($_REQUEST['configuracion']) && !empty($_REQUEST['configuracion'])){ $filtros['configuracion'] = $_REQUEST['configuracion']; }
			if(isset($_REQUEST['valor']) && !empty($_REQUEST['valor'])){ $filtros['valor'] = $_REQUEST['valor']; }
			if(isset($_REQUEST['idproyecto']) && !empty($_REQUEST['idproyecto'])){ $filtros['idproyecto'] = $_REQUEST['idproyecto']; }

			$resultado = $this->oNegProyecto_config->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla rol
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_rol(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['idrol']) && !empty($_REQUEST['idrol'])){ $filtros['idrol'] = $_REQUEST['idrol']; }
			if(isset($_REQUEST['rol']) && !empty($_REQUEST['rol'])){ $filtros['rol'] = $_REQUEST['rol']; }

			$resultado = $this->oNegRol->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla rol_personal
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_rol_personal(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['iddetalle']) && !empty($_REQUEST['iddetalle'])){ $filtros['iddetalle'] = $_REQUEST['iddetalle']; }
			if(isset($_REQUEST['idrol']) && !empty($_REQUEST['idrol'])){ $filtros['idrol'] = $_REQUEST['idrol']; }
			if(isset($_REQUEST['idpersonal']) && !empty($_REQUEST['idpersonal'])){ $filtros['idpersonal'] = $_REQUEST['idpersonal']; }

			$resultado = $this->oNegRol_personal->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla sis_configuracion
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	public function listado_sis_configuracion(){
		try {
			global $aplicacion;
			$filtros = array();

			if(isset($_REQUEST['config']) && !empty($_REQUEST['config'])){ $filtros['config'] = $_REQUEST['config']; }
			if(isset($_REQUEST['valor']) && !empty($_REQUEST['valor'])){ $filtros['valor'] = $_REQUEST['valor']; }
			if(isset($_REQUEST['autocargar']) && !empty($_REQUEST['autocargar'])){ $filtros['autocargar'] = $_REQUEST['autocargar']; }

			$resultado = $this->oNegConfiguracion->buscar($filtros);
			$this->printJSON(200,"Request completed",$resultado);
		} catch(Exception $e) {
			$this->printJSON(400,$e->getMessage());
		}
	}
	/**
	* retorna el listado de la tabla temporal
	* @param Array filtros de la busqueda
	* @return Array de los resultados
	*/
	// public function listado_temporal(){
	// 	try {
	// 		global $aplicacion;
	// 		$filtros = array();

	// 		if(isset($_REQUEST['codigo']) && !empty($_REQUEST['codigo'])){ $filtros['codigo'] = $_REQUEST['codigo']; }

	// 		$resultado = $this->oNegConfiguracion->buscar($filtros);
	// 		$this->printJSON(200,"Request completed",$resultado);
	// 	} catch(Exception $e) {
	// 		$this->printJSON(400,$e->getMessage());
	// 	}
	// }
}