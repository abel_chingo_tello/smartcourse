<?php defined('_BOOT_') or die(''); ?>
<?php $usuarioActivo=NegSesion::getUsuario(); 
$usuariodni=@$usuarioActivo["dni"];

$ruta_completa=$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
$rutaBase = str_replace(array('http://', 'https://'), '', $documento->getUrlBase());
$ruta_sin_UrlBase = str_replace($rutaBase,'',$ruta_completa);
$arrRuta = @explode('/', $ruta_sin_UrlBase);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_inicio.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/examenes/general.css" rel="stylesheet">
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/pnotify.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/inicio.js"></script>
    <script> 
        var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
        var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
        var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';
        var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
    </script>
    <jrdoc:incluir tipo="cabecera" />
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
</head>
<body>
  
<jrdoc:incluir tipo="modulo" nombre="toolbar" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="sidebar_right" /></jrdoc:incluir>
<div class="container">
  <div class="row" >
      <div class="col-xs-12" >
          <ul class="nav nav-pills nav-justified thumbnail menus-examenes">
              <!--li class="menu-home"> 
                  <a class="exa-menu" href="<?php echo $documento->getUrlBase()?>" >
                     <h4 class="list-group-item-heading"><?php echo ucfirst(JrTexto::_("Home"));?></h4>
                     <p class="list-group-item-text">First step description</p>
                  </a> 
              </li-->
              <li class="menu-home <?php echo (@$arrRuta[2]=='')?'active':'' ?>"> 
                  <a class="exa-menu" href="<?php echo $documento->getUrlBase()?>/../quiz" >
                     <h4 class="list-group-item-heading"><?php echo ucfirst(JrTexto::_("Assessments"));?></h4>
                     <!--p class="list-group-item-text">First step description</p-->
                  </a> 
              </li>

              <li class="menu-setting <?php echo (@$arrRuta[2]=='ver')?'active':'' ?>" > 
                  <a class="exa-menu" href="<?php echo $documento->getUrlBase()?>/../quiz/examenes/editar/?idexamen=<?php echo @$_REQUEST["idexamen"]; ?>" >
                     <h4 class="list-group-item-heading"><?php echo ucfirst(JrTexto::_("Settings"));?></h4>
                     <!--p class="list-group-item-text">Descripcion</p-->
                  </a> 
              </li>
              <li class="menu-question <?php echo (@$arrRuta[2]=='preguntas')?'active':'' ?>"> 
                  <?php if (!empty($_REQUEST['xxestiloespecialxxx'])): ?>
                    <a class="exa-menu" href="<?php echo $documento->getUrlBase()?>/examenes/preguntas/?idexamen=<?php echo @$_REQUEST["idexamen"]; ?>&xxestiloespecialxxx=1<?php echo !empty($_REQUEST['idpersona_smartcourse']) ? '&idpersona_smartcourse='.$_REQUEST['idpersona_smartcourse'] : '' ?>" >

                  <?php else: ?>
                    <a class="exa-menu" href="<?php echo $documento->getUrlBase()?>/examenes/preguntas/?idexamen=<?php echo @$_REQUEST["idexamen"]; ?>" >

                  <?php endif; ?>
                     <h4 class="list-group-item-heading"><?php echo ucfirst(JrTexto::_("questions"));?></h4>
                     <!--p class="list-group-item-text">Descripcion</p-->
                  </a> 
              </li>
              <li class="menu-orden <?php echo (@$arrRuta[2]=='orden')?'active':'' ?>"> 
                  <a class="exa-menu" href="<?php echo $documento->getUrlBase()?>/examenes/orden/?idexamen=<?php echo @$_REQUEST["idexamen"]; ?>" >
                     <h4 class="list-group-item-heading"><?php echo ucfirst(JrTexto::_("sort questions"));?></h4>
                     <!--p class="list-group-item-text">Descripcion</p-->
                  </a> 
              </li>
              <li class="menu-preview <?php echo (@$arrRuta[2]=='preview')?'active':'' ?>"> 
                  <a class="exa-menu" href="<?php echo $documento->getUrlBase()?>/examenes/preview/?idexamen=<?php echo @$_REQUEST["idexamen"]; ?>" >
                     <h4 class="list-group-item-heading"><?php echo ucfirst(JrTexto::_("preview"));?></h4>
                     <!--p class="list-group-item-text">Descripcion</p-->
                  </a> 
              </li>
          </ul>
      </div>
      <div class="col-xs-12">
          <jrdoc:incluir tipo="recurso" />
      </div>
  </div>
</div>

<jrdoc:incluir tipo="docsJs" />
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<div class="modal fade" id="modalclone" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="modaltitle"></h4>
      </div>
      <div class="modal-body" id="modalcontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $documento->getUrlStatic() ?>/img/sistema/loading.gif"><br><span style="font-size: 1.2em; font-weight: bolder; color: #006E84;">Getting ready...</span></div>
        </div>
      <div class="modal-footer" id="modalfooter">        
        <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>
  </div>
</div>
</body>
</html>