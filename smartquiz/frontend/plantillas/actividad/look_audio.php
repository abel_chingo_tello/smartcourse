<?php 
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta.'ini_app.php');
    defined('RUTA_BASE') or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $met=empty($_GET["met"])?exit(0):$_GET["met"];
    $orden=empty($_GET["ord"])?1:$_GET["ord"];
    $idgui = uniqid();
    $usuarioAct = NegSesion::getUsuario();
    $rolActivo=$usuarioAct["rol"];
?>
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Look">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Audio">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<input type="hidden" id="video<?php echo $idgui ?>" name="url[<?php echo $met ?>]" value="">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_video.css">
<div class="plantilla plantilla_audio" data-idgui="<?php echo $idgui ?>">
  <div class="tmp_video v<?php echo $idgui ?>">
    <div class="row">
      <div class="col-xs-12 video-loader">
        <div class="btnselectedfile btn btn-orange nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select an audio from our multimedia library or upload your own audio')) ?>" data-tipo="audio" data-url=".aud_1<?php echo $met."_".$idgui ?>">
          <i class="fa fa-headphones" ></i> 
          <?php echo ucfirst(JrTexto::_('select')) ?> audio
        </div>
      </div>
      <div class="col-xs-12 text-center">              
        <div >
          <audio src="" controls="true" class="aud_1<?php echo $met."_".$idgui ?> valvideo" id="audioaud_1<?php echo $met."_".$idgui ?>"  style="display: none;"></audio>
          <img src="<?php echo $documento->getUrlStatic(); ?>/media/web/noaudio.png" class="img-responsive img-thumbnail embed-responsive-item nopreview">
        </div>
      </div>
    </div>
  </div>    
 </div>