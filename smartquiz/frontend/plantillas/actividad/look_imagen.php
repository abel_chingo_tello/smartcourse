<?php 
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta.'ini_app.php');
    defined('RUTA_BASE') or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $met=empty($_GET["met"])?exit(0):$_GET["met"];
    $orden=empty($_GET["ord"])?1:$_GET["ord"];
    $idgui = uniqid();
    $usuarioAct = NegSesion::getUsuario();
    $rolActivo=$usuarioAct["rol"];
?>
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Look">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Imagen_audio">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<input type="hidden" id="video<?php echo $idgui ?>" name="url[<?php echo $met ?>]" value="">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_video.css">
<div class="plantilla plantilla_imagen_audio" data-idgui="<?php echo $idgui ?>">
  <div class="tmp_video v<?php echo $idgui ?>">
    <div class="row">
      <div class="col-xs-12 video-loader">
        <div class="btnselectedfile btn btn-primary nopreview tooltip selimage" title="<?php echo ucfirst(JrTexto::_('select an image from our multimedia library or upload your own image')) ?>" data-tipo="image" data-url=".img_1<?php echo $met."_".$idgui ?>">
          <i class="fa fa-image" ></i> 
          <?php echo ucfirst(JrTexto::_('select')).' '.JrTexto::_('image'); ?> 
        </div>
        <div class="btnselectedfile btn btn-orange nopreview tooltip" title="<?php echo ucfirst(JrTexto::_('select an audio from our multimedia library or upload your own audio')) ?>" data-tipo="audio" data-url=".playaudioimagen">
          <i class="fa fa-headphones" ></i>
          <?php echo ucfirst(JrTexto::_('select')) ?> audio
        </div>
      </div>
      <div class="col-xs-12 text-center">
        <div class="btn btn-orange btn-block hidden playaudioimagen" style="display: none;"><i class="fa fa-play" aria-hidden="true"></i></div>
        <div>
          <img class="img_1<?php echo $met."_".$idgui ?> img-responsive img-thumbnail valvideo" src="" style="display: none">
          <img src="<?php echo $documento->getUrlStatic(); ?>/media/web/noimage.png" class="img-responsive img-thumbnail embed-responsive-item nopreview">
        </div>
      </div>
    </div>
  </div>
  <audio src="" controls="true" id="audioimg_1<?php echo $met."_".$idgui ?>" style="display: none !important;"></audio>
 </div>