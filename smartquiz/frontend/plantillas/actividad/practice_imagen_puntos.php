<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(dirname(__FILE__))).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
$met=empty($_GET["met"])?exit(0):$_GET["met"];
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_imagen_puntos.css">

<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Practice">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Imagen_puntos">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<div class="plantilla plantilla-img_puntos" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>">
    <div class="row">
        <div class="col-xs-12 botones-edicion nopreview">
            <a class="btnselectedfile btn btn-primary selimage" data-tipo="image" data-url=".img-dots<?php echo $idgui; ?>">
                <i class="fa fa-image" ></i> 
                <?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('image'); ?> 
            </a>
            <a class="btn btn-primary start-tag"> 
                <i class="fa fa-dot-circle-o"></i>
                <?php echo ucfirst(JrTexto::_('start tagging')); ?> 
            </a>
            <a class="btn btn-red stop-tag" style="display: none;">
                <i class="fa fa-stop"></i> 
                <?php echo ucfirst(JrTexto::_('stop tagging')); ?> 
            </a>
            <a class="btn btn-danger delete-tag pull-right">
                <i class="fa fa-trash-o"></i> 
                <?php echo ucfirst(JrTexto::_('delete tag')); ?>s 
            </a>
            <a class="btn btn-danger stop-del-tag pull-right" style="display: none;">
                <i class="fa fa-stop"></i> 
                <?php echo ucfirst(JrTexto::_('stop deleting tag')); ?>s 
            </a>
            <a class="btn btn-success generate-tag">
                <i class="fa fa-rocket"></i> 
                <?php echo ucfirst(JrTexto::_('finish').' '.JrTexto::_('and').' '.JrTexto::_('generate')); ?>
            </a>
            <a class="btn btn-success back-edit-tag" style="display: none;">
                <i class="fa fa-pencil"></i> 
                <?php echo ucfirst(JrTexto::_('back').' '.JrTexto::_('and').' '.JrTexto::_('edit')); ?>
            </a>

            <div class="hidden dot-container edition clone_punto<?php echo $idgui; ?>" data-id="clone_punto<?php echo $idgui; ?>">
                <div class="dot"></div>
                <div class="dot-tag"> </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-9 contenedor-img">
            <picture>
                <img src="" class="img-responsive valvideo img-dots<?php echo $idgui; ?>" style="display: none;">
                <img src="<?php echo $documento->getUrlStatic(); ?>/media/web/noimage.png" class="img-responsive nopreview" style="width: initial; margin:0 auto;">
            </picture>
            <div class="mask-dots">
            </div>
        </div>

        <div class="col-xs-12 col-md-3 tag-alts-edit nopreview"> </div>

        <div class="col-xs-12 col-md-3 tag-alts" style="display: none;">
            <div class="tag" id="tag_123456789">
                <div class="arrow"></div>
                Una etiqueta.
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    var HELP = true; /* set 'false' on D.B.Y. */
    initImageTagged('<?php echo $idgui; ?>', HELP);
});
</script>