<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(dirname(__FILE__))).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
$met=empty($_GET["met"])?exit(0):$_GET["met"];
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Look">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Dialogo">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<input type="hidden" id="idgui" name="idgui" value="<?php echo $idgui ?>">
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_dialogo.css">
<div class="plantilla plantilla-dialogo" data-idgui="<?php echo $idgui ?>"> 
  <div class="row">
    <div class="col-xs-12 dialogue-container tab-content" style="border:none; min-height: initial;">
      <div class="botones-add text-center">
        <div class="btn btn-orange btn-file istooltip" id="playdialogo" title="<?php echo JrTexto::_('play') ?>" data-estado="stopped">
          <i class="fa fa-play" ></i>                   
        </div>
        <div class="btn btn-primary btn-file istooltip" id="replaydialogo" title="<?php echo JrTexto::_('re-play') ?>" >
          <i class="fa fa-undo" ></i>                   
        </div>                
      </div>
      <div id="page_1" class=" tpanedialog tab-pane fade active in">
        <div class="col-xs-12 col-sm-3 dialogue-boxes left">
          <div class="d-box left box1_<?php echo $met; ?>_1" data-orden="0" >
            <div class="arrow"></div>
            <div class="botones-add btn-group nopreview">
              <div class="btn btn-orange istooltip btnselectedfile" title="<?php echo JrTexto::_('Selected audio') ?>" data-tipo="audio" data-url=".box1_<?php echo $met; ?>_1">
                <i class="fa fa-headphones"></i>                   
              </div>
              <div class=" nro-orden" > 
                <select class="opt-nro-orden form-control">
                  <option value="-1">- <?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('play order'); ?> -</option>
                  <option value="0"><?php echo ucfirst(JrTexto::_('play first')); ?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_('play second')); ?></option>
                </select>
              </div>
            </div>
            <div class="d-bubble" id="dialogo-texto_1_1"> 
              <span class="addtext"><?php echo ucfirst(JrTexto::_('click here to')).' '.JrTexto::_('add').' '.JrTexto::_('text') ?></span>
              <input type="hidden" class="valin nopreview" data-nopreview=".box1_<?php echo $met; ?>_1">
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 dialogue-image text-center">
          <div class="botones-add nopreview" style="z-index: 1;">
            <div class="btn btn-primary istooltip btnselectedfile selimage" data-tipo="image" data-url=".img_1<?php echo $met."_".$idgui ?>_1" title="<?php echo JrTexto::_('Selected Image') ?>" style="z-index: 1;" >
              <i class="fa fa-image" ></i> 
              <?php echo ucfirst(JrTexto::_('select')).' '.JrTexto::_('image'); ?> 
            </div>                
          </div>
          <div class="image">
            <img id="img_1<?php echo $met."_".$idgui ?>_1" src="<?php echo $documento->getUrlStatic(); ?>/media/web/noimage.png" class="img-responsive img-thumbnail img-thu img_1<?php echo $met."_".$idgui ?>_1" data-default="<?php echo $documento->getUrlStatic(); ?>/media/web/noimage.png" style="padding: 0;">
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 dialogue-boxes right">
          <div class="d-box right box2_<?php echo $met; ?>_1" data-orden="0">
            <div class="arrow"></div>
            <div class="botones-add nopreview btn-group">
              <div class="btn btn-orange istooltip  btnselectedfile"  title="<?php echo JrTexto::_('Selected audio') ?>" data-tipo="audio" data-url=".box2_<?php echo $met; ?>_1">
                <i class="fa fa-headphones"></i>                    
              </div>
              <div class="nro-orden" > 
                <select class="opt-nro-orden form-control">
                  <option value="-1">- <?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('play order'); ?> -</option>
                  <option value="0"><?php echo ucfirst(JrTexto::_('play first')); ?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_('play second')); ?></option>
                </select>
              </div> 

            </div>
            <div class="d-bubble" id="dialogo-texto_2_1">
              <span class="addtext"><?php echo ucfirst(JrTexto::_('click here to')).' '.JrTexto::_('add').' '.JrTexto::_('text') ?></span>
              <input type="hidden" class="valin nopreview"  data-nopreview=".box2_<?php echo $met; ?>_1">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 dialogue-pagination">
      <ul class="nav nav-pills">
        <li class="active"><a href="#page_1" data-toggle="pill">1</a></li>
        <li class="nopreview" id="btn-AddPage" toggle-tooltip="true" data-placement="bottom" title="<?php echo JrTexto::_('Add scene'); ?>"><a href="#"><i class="fa fa-plus-circle"></i></a></li>
        <div class="nopreview btn-inline" id="btn-DelPage" toggle-tooltip="true" data-placement="bottom" title="<?php echo JrTexto::_('Delete actual scene'); ?>" style="padding: 10px 15px; display: none;"><a href="#"><i class="fa fa-trash-o color-red"></i></a></div>
      </ul>
    </div>
  </div>
</div>
<audio id="curaudiodialogo" class="hidden" style="display: none;"></audio>
<script>
  $('[toggle-tooltip="true"]').tooltip();
  $('#curaudiodialogo').on('ended', function(e){
    e.preventDefault();
    e.stopPropagation();
    handlerAudio_ended( $(this) );
  });

</script>