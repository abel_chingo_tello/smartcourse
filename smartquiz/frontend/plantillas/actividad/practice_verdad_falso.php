<?php 
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta.'ini_app.php');
    defined('RUTA_BASE') or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $met=empty($_GET["met"])?exit(0):$_GET["met"];
    $orden=empty($_GET["ord"])?1:$_GET["ord"];
    $idgui = uniqid();
    $usuarioAct = NegSesion::getUsuario();
    $rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Practice">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Verdadero_falso">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<div class="plantilla plantilla-verdad_falso" id="tmp_<?php echo $idgui; ?>">
    <div class="row list-premises tpl_plantilla" id="pnl_premisas<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>">
        <!-- Premises container -->
    </div>    
    <div class="row nopreview">
        <div class="col-xs-12 premise pr- hidden" id="molde">
            <div class="col-xs-12 col-sm-5 col-md-7">
                <span class="addtext"><?php echo ucfirst(JrTexto::_('premise')); ?></span>
                <input type="hidden" class="valPremise valin nopreview" data-nopreview=".premise.pr-">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 options">
                <label class="col-xs-6"><input type="radio" class="radio-ctrl" name="opcPremise-" value="T"> <?php echo ucfirst(JrTexto::_('true')); ?></label>
                <label class="col-xs-6"><input type="radio" class="radio-ctrl" name="opcPremise-" value="F"> <?php echo ucfirst(JrTexto::_('false')); ?></label>
            </div>
            <div class="col-xs-2 col-sm-1 col-md-1 icon-zone">
                <a href="#" class="btn color-danger btn-xs tooltip delete nopreview" title="<?php echo JrTexto::_('remove') ?>" ><i class="fa fa-trash "></i></a>
                <span class="icon-result"></span>
            </div>
            <input type="hidden" id="opcPremise-">
        </div>
        <div class="col-xs-12">
            <div class="col-xs-offset-11 col-xs-1">
                <a href="#" class="btn btn-primary btn-xs tooltip add-premise" data-clone-from="#molde" data-clone-to="#pnl_premisas<?php echo $idgui; ?>" title="<?php echo JrTexto::_('add') ?>" ><i class="fa fa-plus-square"></i></a>
            </div>
        </div>
    </div> 
</div>
<textarea style="display:none;" class="tmp_verdad_falso<?php echo $idgui ?>" name="texto[<?php echo $met ?>]"></textarea>