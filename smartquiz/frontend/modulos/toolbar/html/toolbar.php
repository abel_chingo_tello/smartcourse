<aside class="toolbar" id="toolbar">
    <div class="col-xs-12 toolbar-option">
        <a href="#" class="btn btn-toolbar changeidioma " title="<?php echo JrTexto::_('Change language');?>" idioma="<?php echo $this->documento->getIdioma()=="ES"? "EN":"ES";?>" data-placement="left">
            <i class="fa fa-language hvr-float" aria-hidden="true"></i>
        </a>
    </div>
    <div class="col-xs-12 toolbar-option " >
        <a href="#" class="btn btn-toolbar mostrarvideo " title="<?php echo JrTexto::_('Show help');?>!" data-placement="left">
            <i class="fa fa-question-circle-o hvr-float"></i>
        </a>
    </div>
     <?php /*
    <div class="col-xs-12 toolbar-option " >
        <a href="#" class="btn btn-toolbar " title="" data-placement="left">
            <i class="fa fa-cube hvr-float"></i>
        </a>
    </div>
    */ ?>
    <?php /*if($this->usuario["rol"]!='Alumno'){?>
    <div class="col-xs-12 toolbar-option" >
        <a href="<?php echo $this->documento->getUrlSitio() ?>/administrador" class="btn btn-toolbar " title="<?php echo JrTexto::_('Admin panel');?>" data-placement="left">
            <i class="fa fa-th hvr-float"></i>
        </a>
    </div>
    <?php }*/ ?>
    <?php /*
    <div class="col-xs-12 toolbar-option " >
        <a href="#" class="btn btn-toolbar slide-sidebar-right" title="<?php echo JrTexto::_('Dictionary');?>" data-placement="left" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/diccionario">
            <i class="fa fa-book hvr-float"></i>
        </a>
    </div>
    */ ?>
    <div class="col-xs-12 toolbar-option " >
        <a href="<?php echo $this->documento->getUrlBase() ?>/sesion/salir" class="btn btn-toolbar" title="<?php echo JrTexto::_('log out');?>" data-placement="left">
            <i class="fa fa-power-off hvr-float"></i>
        </a>
    </div>
</aside>
<script type="text/javascript">
    $(document).ready(function(){
        $('.btn-toolbar').addClass('').tooltip();
        $('.mostrarvideo').click(function(){          
            $('#myModal').modal({
                keyboard: false, /*desactiva Cerrar Modal con ESC*/
            });
        });
    });
</script>