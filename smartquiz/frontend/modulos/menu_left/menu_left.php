<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
JrCargador::clase('modulos::menu_left::MenuLeft', RUTA_SITIO, 'modulos::menu_left');
$oMod = new MenuLeft;
echo $oMod->mostrar();