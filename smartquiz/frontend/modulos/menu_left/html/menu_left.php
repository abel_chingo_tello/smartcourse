<?php 
$menu = array(
	/**** Menús de Inicio ****/
	array('nombre' => ucfirst(JrTexto::_('my profile')), 'link'=>'/defecto/mi_perfil', 'icon'=>'fa-user', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('my courses')), 'link'=>'/defecto/mis_cursos', 'icon'=>'fa-file', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('homework').'s'), 'link'=>'/defecto/tareas', 'icon'=>'fa-files-o', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('exams')), 'link'=>'/defecto/examenes', 'icon'=>'fa-book', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Smart Class')), 'link'=>'/defecto/smart_class', 'icon'=>'fa-video-camera', 'descripcion'=>''),


	array('nombre' => ucfirst(JrTexto::_('administrative module')), 'link'=>'/', 'icon'=>'fa-bars', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('area')), 'link'=>'/cargos/?nivel=1', 'icon'=>'fa-th-large', 'descripcion'=>''),
	array('nombre' => ' Sub-'.ucfirst(JrTexto::_('area')), 'link'=>'/cargos/?nivel=2', 'icon'=>'fa-th', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Obligatory courses')), 'link'=>'/curso_obligado', 'icon'=>'fa-book"></i><i class="fa fa-exclamation ', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('administrative')), 'link'=>'/usuario', 'icon'=>'fa-lock ', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('student')), 'link'=>'/alumno', 'icon'=>'fa-graduation-cap ', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Reports')), 'link'=>'/reportes', 'icon'=>'fa-bar-chart', 'descripcion'=>''),


	array('nombre' => ucfirst(JrTexto::_('Log out')), 'link'=>'/sesion/salir', 'icon'=>'fa-power-off', 'descripcion'=>''),
);
$ruta_completa=$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
$rutaBase = str_replace(array('http://', 'https://'), '', $this->documento->getUrlBase());
$ruta_sin_UrlBase = str_replace($rutaBase,'',$ruta_completa);
$ruta_sin_UrlBase = @explode('/', $ruta_sin_UrlBase)[1];
?>
<div class="list-group" id="sidebar-nav">
	<?php foreach ($menu as $m) {
		$active = ('/'.$ruta_sin_UrlBase==$m['link'])?'active':'';
	?>
		<a class="list-group-item <?php echo $active; ?>" href="<?php echo $this->documento->getUrlBase().$m['link']; ?>">
			<h5 class="list-group-item-heading">
				<i class="fa <?php echo $m['icon']; ?> fa-fw"></i> <?php echo $m['nombre']; ?>
			</h5>
			<?php if(!empty($m['descripcion'])) { ?>
			<p class="list-group-item-text"> <?php echo $m['descripcion']; ?></p>
			<?php } ?>
		</a>
	<?php }
	?>
</div>
<script type="text/javascript">	
var calcularAncho = function () {
	var ventanaW = $(window).outerWidth();
	if(ventanaW<=768){ $('#menu-left').hide(); }
	else{ $('#menu-left').show(); }
};

$(window).resize(calcularAncho);

$(document).ready(function() {
	calcularAncho();
	$('header .navbar-header button.navbar-toggle').click(function() {
		var estaMenuVisible = $('#menu-left #sidebar-nav').is(':visible');
		if(estaMenuVisible){ $('#menu-left').hide('fast'); }
		else{ $('#menu-left').show('fast'); }
	});
	
});

</script>