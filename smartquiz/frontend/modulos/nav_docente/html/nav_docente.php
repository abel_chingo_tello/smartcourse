<div class="main clearfix" style="position: relative;">    
    <nav id="menu" class="menutop2">                 
        <ul>			
			<li>
                <a href="<?php echo $this->documento->getUrlSitio() ?>" class="hvr-underline-from-center">
                    <span class="icon hvr-sink">
                        <i aria-hidden="true" class="fa fa-home "></i>
                    </span>
                    <span><?php echo JrTexto::_('Home');?></span>
                </a>
			</li>
			
			<?php if(NegSesion::tiene_acceso('registros', 'list')): ?>
			<li>
				<a href="<?php echo $this->documento->getUrlSitio() ?>/registros" class="hvr-underline-from-center">
					<span class="icon hvr-sink"> 
						<i aria-hidden="true" class="fa fa-list-alt"></i>
					</span>
					<span><?php echo JrTexto::_('Register');?></span>
				</a>
			</li>
			<?php endif ?>
			<?php if(NegSesion::tiene_acceso('Cursos', 'list')): ?>
			<li>
				<a href="<?php echo $this->documento->getUrlSitio() ?>/cursos" class="hvr-underline-from-center">
					<span class="icon hvr-sink">
						<i aria-hidden="true" class="fa fa-book"></i>
					</span>
					<span><?php echo JrTexto::_('Courses');?></span>
				</a>
			</li>
			<?php endif ?>
			<?php if(NegSesion::tiene_acceso('reportes', 'list')): ?>
			<li >
				<a href="<?php echo $this->documento->getUrlSitio() ?>/reportes" class="hvr-underline-from-center">
					<span class="icon hvr-sink">
						<i aria-hidden="true" class="fa fa-bar-chart"></i>
					</span>
					<span><?php echo JrTexto::_('Reports');?></span>
				</a>
			</li>
			<?php endif ?>
			<?php if(NegSesion::tiene_acceso('comunidad', 'list')): ?>
			<li>
				<a href="<?php echo $this->documento->getUrlSitio() ?>/comunidad" class="hvr-underline-from-center">
					<span class="icon hvr-sink">
						<i aria-hidden="true" class="fa fa-users"></i>
					</span>
					<span><?php echo JrTexto::_('Community');?></span>
				</a>
			</li>
			<?php endif ?>
			<?php if(NegSesion::tiene_acceso('recursos', 'list')): ?>
			<li>
				<a href="<?php echo $this->documento->getUrlSitio() ?>/recursos" class="hvr-underline-from-center">
					<span class="icon hvr-sink">
						<i aria-hidden="true" class="fa fa-th-large"></i>
					</span>
					<span><?php echo JrTexto::_('Resources');?></span>
				</a>
			</li>
			<?php endif ?>
			<?php if(NegSesion::tiene_acceso('Evaluacion', 'list')): ?>
			<li>
				<a href="<?php echo $this->documento->getUrlSitio() ?>/evaluacion" class="hvr-underline-from-center">
					<span class="icon hvr-sink">
						<i aria-hidden="true" class="fa fa-list"></i>
					</span>
					<span><?php echo JrTexto::_('Evaluation');?></span>
				</a>
			</li>
			<?php endif ?>
			
			<li>
				<a href="<?php echo $this->documento->getUrlSitio() ?>/setting" class="hvr-underline-from-center">
					<span class="icon hvr-sink">
						<i aria-hidden="true" class="fa fa-wrench"></i>
					</span>
					<span><?php echo JrTexto::_('Setting');?></span>
				</a>
			</li>
			
			<?php if(NegSesion::tiene_acceso('sincronizar', 'list')): ?>
			<li>
				<a href="<?php echo $this->documento->getUrlSitio() ?>/sincronizacion" class="hvr-underline-from-center">
					<span class="icon hvr-sink">
						<i aria-hidden="true" class="fa fa-refresh"></i>
					</span>
					<span><?php echo JrTexto::_('Sync');?></span>
				</a>
			</li>
			<?php endif ?>			
		</ul>
	</nav>
	<span class="showtoolbar verttip" title="<?php echo JrTexto::_('Hide Toolbar') ?>"><i class="fa fa-angle-double-up"></i></span>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.showtoolbar').click(function(){
			var obj=$(this).children('i');
			if(obj.hasClass('fa-angle-double-down')){
				$('nav#menu.menutop2').show('fast');
				obj.removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
				$(this).attr('data-original-title','<?php echo JrTexto::_('Hide Toolbar') ?>');
				$(this).animate({left: "+=30%"});
				localStorage.setItem("_sysvertoptoolbar", 'si');
			}else{
				$('nav#menu.menutop2').hide('fast');
				obj.removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
				$(this).attr('data-original-title','<?php echo JrTexto::_('Show Toolbar') ?>');
				$(this).animate({left: "-=30%"});
				localStorage.setItem("_sysvertoptoolbar", 'no');
			}
			$(this).tooltip();
		});
		$('.verttip').tooltip();
		var _sysvertoptoolbar = localStorage.getItem("_sysvertoptoolbar");
		if(_sysvertoptoolbar=='no'){
			$('.showtoolbar').click();
		}
	});
</script>