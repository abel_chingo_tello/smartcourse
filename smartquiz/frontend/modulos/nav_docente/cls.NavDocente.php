<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_SITIO, 'sys_negocio');
class NavDocente extends JrModulo
{
	protected $oNegConfig;
	public function __construct()
	{
		parent::__construct();
		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->modulo = '';//'nav_docente';
	}
	
	public function mostrar()
	{
		try {
			$this->usuario = NegSesion::getUsuario();
			$this->esquema = 'nav_docente';
			if($this->usuario["rol"]!='Alumno'){
				return $this->getEsquema();
			}
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}