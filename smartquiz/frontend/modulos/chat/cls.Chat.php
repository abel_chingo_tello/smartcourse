<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_SITIO, 'sys_negocio');
class Chat extends JrModulo
{
	//protected $oNegConfig;
	public function __construct()
	{
		parent::__construct();
		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->modulo = 'chat';
	}
	
	public function mostrar($posicion = null)
	{
		try {
			$pos=!empty($posicion)?($posicion."_"):'';
			$this->pos=$pos;
			$this->usuario = NegSesion::getUsuario();
			$this->esquema = @$pos.'chat';
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}