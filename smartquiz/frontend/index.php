<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
// require_once(dirname(__FILE__).DIRECTORY_SEPARATOR . 'ini_app.php');
// $aplicacion->iniciar();
// //$idi = !empty(NegSesion::get('idioma','idioma__'))?NegSesion::get('idioma','idioma__'):NegConfiguracion::get_('idioma_defecto');
// //$documento = &JrInstancia::getDocumento();
// //$documento->setIdioma($idi);
// $aplicacion->enrutar(JrPeticion::getPeticion(0), JrPeticion::getPeticion(1));
// $aplicacion->despachar();
// echo $aplicacion->presentar();

// function mostar__($rec, $ax) {
// 	global $aplicacion;	
// 	$aplicacion->iniciar();
// 	$aplicacion->enrutar($rec, $ax);
// 	echo $aplicacion->presentar();
// }

error_reporting(E_ALL);
$host= !empty($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : 'localhost';
// var_dump($host);
$url= @$_SERVER["REQUEST_URI"];
define('SD', DIRECTORY_SEPARATOR);
define('_http_',!empty($_SERVER['HTTPS'])?'https://':'http://');
define('_HOST_', _http_.$host);
define('_ishttp_',false);
@session_start();
$_SESSION["urlredirok"]=_http_.$host.$url;
if(_ishttp_==true&&empty($_SERVER['HTTPS'])){header('Location:'._HOST_.$url); exit(0);}
require_once(dirname(__FILE__).SD.'ini_app.php');
$aplicacion->iniciar();

if(!empty($_REQUEST["idioma"])){	
	$documento = &JrInstancia::getDocumento();	
	$idioma=$_REQUEST["idioma"];
	$documento->setIdioma($idioma);
}

$aplicacion->enrutar(JrPeticion::getPeticion(0), JrPeticion::getPeticion(1));
$aplicacion->despachar();
echo $aplicacion->presentar();

function mostar__($rec, $ax){
	global $aplicacion;	
	$aplicacion->iniciar();
	$aplicacion->enrutar($rec, $ax);
	echo $aplicacion->presentar();
}