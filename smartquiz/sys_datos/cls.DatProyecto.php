<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-09-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatProyecto extends DatBase
{
	public function __construct()
	{
		try {
			#parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
	
	public function getNumRegistros($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT COUNT(1) FROM proyecto";
			
			$cond = array();		
			
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["slug"])) {
					$cond[] = "slug = " . $this->oBD->escapar($filtros["slug"]);
			}
			if(isset($filtros["slug_like"])) {
					$cond[] = "slug LIKE '" . $filtros["slug_like"] ."%' ";
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_fin"])) {
					$cond[] = "fecha_fin = " . $this->oBD->escapar($filtros["fecha_fin"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idioma"])) {
					$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM proyecto";			
			
			$cond = array();		
					
			
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["slug"])) {
					$cond[] = "slug = " . $this->oBD->escapar($filtros["slug"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_fin"])) {
					$cond[] = "fecha_fin = " . $this->oBD->escapar($filtros["fecha_fin"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idioma"])) {
					$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre,$comentario,$slug,$fecha_inicio,$fecha_fin,$estado,$idioma)
	{
		try {
			parent::conectar();
			
			$this->iniciarTransaccion('dat_proyecto_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idproyecto) FROM proyecto");
			++$id;
			
			$estados = array('idproyecto' => $id
							
							,'nombre'=>$nombre
							,'comentario'=>$comentario
							,'slug'=>$slug
							#,'fecha_inicio'=>$fecha_inicio
							,'fecha_fin'=>$fecha_fin
							,'estado'=>$estado							
							,'idioma'=>$idioma							
							);
			
			$this->oBD->insert('proyecto', $estados);			
			$this->terminarTransaccion('dat_proyecto_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_proyecto_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}

	public function actualizar($id, $nombre,$comentario,$slug,$fecha_inicio,$fecha_fin,$estado,$idioma)
	{
		try {
			parent::conectar();
			$this->iniciarTransaccion('dat_proyecto_update');
			$estados = array('nombre'=>$nombre
							,'comentario'=>$comentario
							,'slug'=>$slug
							#,'fecha_inicio'=>$fecha_inicio
							,'fecha_fin'=>$fecha_fin
							,'estado'=>$estado								
							,'idioma'=>$idioma								
							);
			
			$this->oBD->update('proyecto ', $estados, array('idproyecto' => $id));
		    $this->terminarTransaccion('dat_proyecto_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			parent::conectar();
			$sql = "SELECT  *  FROM proyecto  "
					. " WHERE idproyecto = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}

	public function getXSlug($slug)
	{
		try {
			parent::conectar();
			$sql = "SELECT  *  FROM proyecto  "
					. " WHERE slug = " . $this->oBD->escapar($slug);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			parent::conectar();
			return $this->oBD->delete('proyecto', array('idproyecto' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			parent::conectar();
			$this->oBD->update('proyecto', array($propiedad => $valor), array('idproyecto' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
   
		
}