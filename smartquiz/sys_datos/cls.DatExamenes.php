<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatExamenes extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function getNumRegistros($filtros=null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT COUNT(1) FROM examenes";
			
			$cond = array();		
			
			if(!empty($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["titulo"])) {
				$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(!empty($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["idpersonal"])) {
				$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["nintento"])) {
				$cond[] = "nintento = " . $this->oBD->escapar($filtros["nintento"]);
			}
			if(!empty($filtros["calificacion"])) {
				$cond[] = "calificacion = " . $this->oBD->escapar($filtros["calificacion"]);
			}
			if(!empty($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(!empty($filtros["habilidades_todas"])) {
				$cond[] = "habilidades_todas = " . $this->oBD->escapar($filtros["habilidades_todas"]);
			}
			if( !empty($filtros["origen_habilidades"]) ){
				$cond[] = "origen_habilidades = " . $this->oBD->escapar($filtros["origen_habilidades"]);
			}
			if( !empty($filtros["fuente_externa"]) ){
				$cond[] = "fuente_externa = " . $this->oBD->escapar($filtros["fuente_externa"]);
			}
			if( !empty($filtros["dificultad_promedio"]) ){
				$cond[] = "dificultad_promedio = " . $this->oBD->escapar($filtros["dificultad_promedio"]);
			}
			if( !empty($filtros["idproyecto"]) ){
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT `idexamen`,`titulo`,`descripcion`, `portada`,`fuente`,`fuentesize`, `aleatorio`,`calificacion_por`, `calificacion_en`,`calificacion_total`,`calificacion_min`, `tiempo_por`,`tiempo_total`,`tipo`,`estado`,`idproyecto`, `idpersonal`,`fecharegistro`,`nintento`,`calificacion`, `origen_habilidades`,`habilidades_todas`,`fuente_externa`, `dificultad_promedio`,`tiene_certificado`,`nombre_certificado`, `idcc_smartcourse`,`idpersona_smartcourse`,idioma FROM examenes ";			
			
			$cond = array();

			if(!empty($filtros["idexamen"])) {				
				if(is_array($filtros["idexamen"])){
					$cond[]="idexamen IN ('".implode("','",$filtros["idexamen"])."')";
				}else
				$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(!empty($filtros["titulo"])) {
				$cond[] = "titulo LIKE '%" . $filtros["titulo"]."%'";
			}
			if(!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["idpersonal"])) {
				$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["fecharegistro"])) {
				$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(!empty($filtros["nintento"])) {
				$cond[] = "nintento = " . $this->oBD->escapar($filtros["nintento"]);
			}
			if(!empty($filtros["calificacion"])) {
				$cond[] = "calificacion = " . $this->oBD->escapar($filtros["calificacion"]);
			}
			if(!empty($filtros["habilidades_todas"])) {
				$cond[] = "habilidades_todas = " . $this->oBD->escapar($filtros["habilidades_todas"]);
			}
			if( !empty($filtros["origen_habilidades"]) ){
				$cond[] = "origen_habilidades = " . $this->oBD->escapar($filtros["origen_habilidades"]);
			}
			if( !empty($filtros["fuente_externa"]) ){
				$cond[] = "fuente_externa = " . $this->oBD->escapar($filtros["fuente_externa"]);
			}
			if( !empty($filtros["dificultad_promedio"]) ){
				$cond[] = "dificultad_promedio = " . $this->oBD->escapar($filtros["dificultad_promedio"]);
			}
			if( !empty($filtros["idproyecto"])){
				if($filtros["idproyecto"]!=25 && $filtros["idproyecto"]!=26 && $filtros["idproyecto"]!=27)
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if( !empty($filtros["idcc_smartcourse"]) ){
				$cond[] = "idcc_smartcourse = " . $this->oBD->escapar($filtros["idcc_smartcourse"]);
			}
			if( !empty($filtros["idpersona_smartcourse"]) ){
				$cond[] = "idpersona_smartcourse = " . $this->oBD->escapar($filtros["idpersona_smartcourse"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			parent::setLimite(0,10000);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function buscarRendidos($filtros=null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT `idexamen`,`titulo`,`descripcion`, `portada`,`fuente`,`fuentesize`, `aleatorio`,`calificacion_por`, `calificacion_en`,`calificacion_total`,`calificacion_min`, `tiempo_por`,`tiempo_total`,`tipo`,`estado`,`idproyecto`, `idpersonal`,`fecharegistro`,`nintento`,`calificacion`, `origen_habilidades`,`habilidades_todas`,`fuente_externa`, `dificultad_promedio`,`tiene_certificado`,`nombre_certificado`, `idcc_smartcourse`,`idpersona_smartcourse`,idioma FROM examenes	WHERE idexamen IN (SELECT idexamen FROM examen_alumno EA JOIN personal P ON EA.idalumno=P.dni ";
			
			$cond = array();

			if(!empty($filtros["idexaalumno"])) {
				$cond[] = "EA.idexaalumno = " . $this->oBD->escapar($filtros["idexaalumno"]);
			}
			if(!empty($filtros["idalumno"])) {
				$cond[] = "EA.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["identificador"])) {
				$cond[] = "P.identificador = " . $this->oBD->escapar($filtros["identificador"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY fecha_creado ASC";
			$sql .= " )";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function insertar($titulo,$descripcion,$portada,$fuente,$fuentesize,$aleatorio,$calificacion_por,$calificacion_en,$calificacion_total,$calificacion_min,$tiempo_por,$tiempo_total,$tipo,$estado,$idpersonal,$nintento=1,$calificacion='U', $habilidades_todas, $origen_habilidades, $fuente_externa, $dificultad_promedio, $idproyecto, $tiene_certificado, $nombre_certificado,$idpersona_smartcourse = null,$idioma='ES')
	{
		try {
			#parent::conectar();
			$this->iniciarTransaccion('dat_examenes_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idexamen) FROM examenes");
			++$id;
			/* obligatory fields: */
			$estados = array('idexamen' => $id
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'fuente'=>$fuente
							,'fuentesize'=>$fuentesize
							,'aleatorio'=>$aleatorio
							,'calificacion_por'=>$calificacion_por
							,'calificacion_en'=>$calificacion_en
							,'calificacion_total'=>$calificacion_total
							,'tiempo_por'=>$tiempo_por
							,'tipo'=>$tipo
							,'estado'=>$estado
							,'idpersonal'=>$idpersonal
							,'nintento'=>$nintento
							,'calificacion'=>$calificacion
							,'origen_habilidades'=>$origen_habilidades
							,'idproyecto'=>$idproyecto
							,'idioma' =>$idioma
							);

			/* Fields accept NULL: */
			if (!empty($portada)) { $estados['portada'] = $portada; }
			if (!empty($calificacion_min)) { $estados['calificacion_min'] = $calificacion_min; }
			if (!empty($tiempo_total)) { $estados['tiempo_total'] = $tiempo_total; }
			if (!empty($fecharegistro)) { $estados['fecharegistro'] = $fecharegistro; }
			if (!empty($nintento)) { $estados['nintento'] = $nintento; }
			if (!empty($calificacion)) { $estados['calificacion'] = $calificacion; }
			if (!empty($habilidades_todas)) { $estados['habilidades_todas'] = $habilidades_todas; }
			if (!empty($fuente_externa)) { $estados['fuente_externa'] = $fuente_externa; }
			if (!empty($dificultad_promedio)) { $estados['dificultad_promedio'] = $dificultad_promedio; }
			if (!empty($tiene_certificado)) { $estados['tiene_certificado'] = $tiene_certificado; }
			if (!empty($nombre_certificado)) { $estados['nombre_certificado'] = $nombre_certificado; }
			if (!empty($idpersona_smartcourse)) { $estados['idpersona_smartcourse'] = $idpersona_smartcourse; }

			
			$this->oBD->insert('examenes', $estados);			
			$this->terminarTransaccion('dat_examenes_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_examenes_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function actualizar($id, $titulo,$descripcion,$portada,$fuente,$fuentesize,$aleatorio,$calificacion_por,$calificacion_en,$calificacion_total,$calificacion_min,$tiempo_por,$tiempo_total,$tipo,$estado,$idpersonal,$nintento,$calificacion,$habilidades_todas,$origen_habilidades,$fuente_externa,$dificultad_promedio,$idproyecto, $tiene_certificado, $nombre_certificado,$idpersona_smartcourse = null,$idioma='ES')
	{
		try {
			#parent::conectar();
			$this->iniciarTransaccion('dat_examenes_update');
			/* obligatory fields: */
			$estados = array('titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'fuente'=>$fuente
							,'fuentesize'=>$fuentesize
							,'aleatorio'=>$aleatorio
							,'calificacion_por'=>$calificacion_por
							,'calificacion_en'=>$calificacion_en
							,'calificacion_total'=>$calificacion_total
							,'tiempo_por'=>$tiempo_por
							,'tipo'=>$tipo
							,'estado'=>$estado
							,'idpersonal'=>$idpersonal
							,'nintento'=>$nintento
							,'calificacion'=>$calificacion
							,'idproyecto'=>$idproyecto
							,'idioma'=>$idioma
							);

			/* Fields accept NULL: */
			if (!empty($portada)) { $estados['portada'] = $portada; }
			if (!empty($calificacion_min)) { $estados['calificacion_min'] = $calificacion_min; }
			if (!empty($tiempo_total)) { $estados['tiempo_total'] = $tiempo_total; }
			if (!empty($fecharegistro)) { $estados['fecharegistro'] = $fecharegistro; }
			if (!empty($nintento)) { $estados['nintento'] = $nintento; }
			if (!empty($calificacion)) { $estados['calificacion'] = $calificacion; }
			if (!empty($habilidades_todas)) { $estados['habilidades_todas'] = $habilidades_todas; }
			if (!empty($dificultad_promedio)) { $estados['dificultad_promedio'] = $dificultad_promedio; }
			if (!empty($tiene_certificado)) { $estados['tiene_certificado'] = $tiene_certificado; }
			if (!empty($nombre_certificado)) { $estados['nombre_certificado'] = $nombre_certificado; }
			if (!empty($idpersona_smartcourse)) { $estados['idpersona_smartcourse'] = $idpersona_smartcourse; }

			$this->oBD->update('examenes ', $estados, array('idexamen' => $id));
		    $this->terminarTransaccion('dat_examenes_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			#parent::conectar();
			$sql = "SELECT `idexamen`,`titulo`,`descripcion`, `portada`,`fuente`,`fuentesize`, `aleatorio`,`calificacion_por`, `calificacion_en`,`calificacion_total`,`calificacion_min`, `tiempo_por`,`tiempo_total`,`tipo`,`estado`,`idproyecto`, `idpersonal`,`fecharegistro`,`nintento`,`calificacion`, `origen_habilidades`,`habilidades_todas`,`fuente_externa`, `dificultad_promedio`,`tiene_certificado`,`nombre_certificado`, `idcc_smartcourse`,`idpersona_smartcourse`,idioma FROM examenes  WHERE idexamen = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			#parent::conectar();
			$this->iniciarTransaccion('dat_examenes_eliminar');
				$this->oBD->delete('examenes_preguntas', array('idexamen' => $id));
				$this->oBD->delete('examenes', array('idexamen' => $id));
			$this->terminarTransaccion('dat_examenes_eliminar');
			return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			#parent::conectar();
			$this->oBD->update('examenes', array($propiedad => $valor), array('idexamen' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}
   
		
}