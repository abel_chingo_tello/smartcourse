<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAlumno extends DatBase
{
	public function __construct()
	{
		try {
			#parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT COUNT(*) FROM alumno ";
			
			$cond = array();
			
			if(isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = " ( apellidopaterno " . $this->oBD->like($filtros["texto"])." OR apellidomaterno " . $this->oBD->like($filtros["texto"])." OR nombre " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["apellidopaterno"])) {
				$cond[] = "apellidopaterno " . $this->oBD->like($filtros["apellidopaterno"]);
			}
			if(isset($filtros["apellidomaterno"])) {
				$cond[] = "apellidomaterno " . $this->oBD->like($filtros["apellidomaterno"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["nombre"]);
			}
			if(isset($filtros["fechanacimiento"])) {
				$cond[] = "fechanacimiento = " . $this->oBD->escapar($filtros["fechanacimiento"]);
			}
			if(isset($filtros["sexo"])) {
				$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["dni"])) {
				$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["idestadocivil"])) {
				$cond[] = "idestadocivil = " . $this->oBD->escapar($filtros["idestadocivil"]);
			}
			if(isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["pais"])) {
				$cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);
			}
			if(isset($filtros["direccion"])) {
				$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
				$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
				$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
				$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["usuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idempresa"])) {
				$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["idcargo"])) {
				$cond[] = "idcargo = " . $this->oBD->escapar($filtros["idcargo"]);
			}
			if(isset($filtros["idugel"])) {
				$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["clave1"])) {
				$cond[] = "clave1 = " . $this->oBD->escapar($filtros["clave1"]);
			}
			if(isset($filtros["observacion"])) {
				$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT A.*, C.descripcion AS cargo_descripcion, C.tipo AS cargo_tipo, C.nivel AS cargo_nivel, C.idarea AS cargo_idarea FROM alumno A JOIN cargo C ON C.idcargo=A.idcargo ";			
			
			$cond = array();		
					
			
			if(isset($filtros["idalumno"])) {
				$cond[] = "A.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = " ( A.apellidopaterno " . $this->oBD->like($filtros["texto"])." OR A.apellidomaterno " . $this->oBD->like($filtros["texto"])." OR A.nombre " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["apellidopaterno"])) {
				$cond[] = "A.apellidopaterno =" . $this->oBD->escapar($filtros["apellidopaterno"]);
			}
			if(isset($filtros["apellidomaterno"])) {
				$cond[] = "A.apellidomaterno =" . $this->oBD->escapar($filtros["apellidomaterno"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "A.nombre =" . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fechanacimiento"])) {
				$cond[] = "A.fechanacimiento = " . $this->oBD->escapar($filtros["fechanacimiento"]);
			}
			if(isset($filtros["sexo"])) {
				$cond[] = "A.sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["dni"])) {
				$cond[] = "A.dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["idestadocivil"])) {
				$cond[] = "A.idestadocivil = " . $this->oBD->escapar($filtros["idestadocivil"]);
			}
			if(isset($filtros["ubigeo"])) {
				$cond[] = "A.ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["pais"])) {
				$cond[] = "A.pais = " . $this->oBD->escapar($filtros["pais"]);
			}
			if(isset($filtros["direccion"])) {
				$cond[] = "A.direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
				$cond[] = "A.telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
				$cond[] = "A.celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
				$cond[] = "A.email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["idugel"])) {
				$cond[] = "A.idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "A.regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "A.regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["usuario"])) {
				$cond[] = "A.usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "A.clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["clave1"])) {
				$cond[] = "A.clave1 = " . $this->oBD->escapar($filtros["clave1"]);
			}
			if(isset($filtros["foto"])) {
				$cond[] = "A.foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "A.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idempresa"])) {
				$cond[] = "A.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["observacion"])) {
				$cond[] = "A.observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}			
			if(isset($filtros["idcargo"])) {
				$cond[] = " ( C.idcargo = " . $this->oBD->escapar($filtros["idcargo"])."OR C.idarea = " . $this->oBD->escapar($filtros["idcargo"]).") ";
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//$sql .= " ORDER BY regfecha ASC";
			//echo $sql;
			/* echo $sql; exit(); */

			$sql .= " ORDER BY cargo_nivel ASC, idugel DESC";
			
			//echo $sql; exit();

			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}

	public function getxCredencial($usuario, $clave)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM alumno "
					. " WHERE (usuario = " . $this->oBD->escapar($usuario)." OR email=". $this->oBD->escapar($usuario).") "
					. " AND clave = '" . md5($clave) . "' AND estado='A'";
			$res = $this->oBD->consultarSQL($sql);			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	public function getxToken($token)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM alumno WHERE md5(usuario || ' ' || clave) = '" . $token . "' AND estado='A' ";
			$res = $this->oBD->consultarSQL($sql);	
			//echo $sql; var_dump($res[0]);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}*/

	public function getxusuario($usuario)
	{
		try {
			parent::conectar();
			$sql = "SELECT *  FROM alumno  "
					. " WHERE usuario = " . $this->oBD->escapar($usuario);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('users'). $e->getMessage());
		}
	}


	public function getxusuarioemail($usuema)
	{
		try {
			parent::conectar();
			$sql = "SELECT *  FROM alumno "
					. " WHERE usuario = " . $this->oBD->escapar($usuema)." OR email = " . $this->oBD->escapar($usuema);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user'). $e->getMessage());
		}
	}
	
	public function getxclave1($clave1)
	{
		try {
			parent::conectar();
			$sql = "SELECT *  FROM alumno "
					. " WHERE clave1 = " . $this->oBD->escapar($clave1);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user').$e->getMessage());
		}
	}
	
	public function insertar($apellidopaterno,$apellidomaterno,$nombre,$fechanacimiento,$sexo,$dni,$idestadocivil,$pais,$direccion,$telefono,$celular,$email,$idugel,$regusuario,$regfecha,$usuario,$clave,$clave1,$img,$idcargo,$estado,$idempresa,$observacion)
	{
		try {
			parent::conectar();
			
			$this->iniciarTransaccion('dat_alumno_insert');
			
			$longitud_campo = $this->oBD->consultarEscalarSQL("SELECT LENGTH(idalumno) FROM alumno LIMIT 1;");
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idalumno) FROM alumno");
			$id = (int)$id + 1;
			$cant_digitos = strlen((string)$id);
			for($x=0; $x<$longitud_campo-$cant_digitos; $x++){
				$id='0'.$id;
			}
			
			$estados = array('idalumno' => $id
							
							,'apellidopaterno'=>$apellidopaterno
							,'apellidomaterno'=>$apellidomaterno
							,'nombre'=>$nombre
							,'fechanacimiento'=>$fechanacimiento
							,'sexo'=>$sexo
							,'dni'=>$dni
							,'idestadocivil'=>$idestadocivil
							,'pais'=>$pais
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'email'=>$email
							,'idugel'=>$idugel
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'clave1'=>$clave1
							,'img'=>$img
							,'idcargo'=>$idcargo
							,'estado'=>$estado
							,'idempresa'=>$idempresa
							,'observacion'=>$observacion							
							);
			
			$this->oBD->insert('alumno', $estados);			
			$this->terminarTransaccion('dat_alumno_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($idalumno,$apellidopaterno,$apellidomaterno,$nombre,$fechanacimiento,$sexo,$dni,$idestadocivil,$pais,$direccion,$telefono,$celular,$email,$idugel,$regusuario,$regfecha,$usuario,$clave,$clave1,$img,$idcargo,$estado,$idempresa,$observacion)
	{
		try {
			parent::conectar();
			$this->iniciarTransaccion('dat_alumno_update');
			$estados = array('apellidopaterno'=>$apellidopaterno
							,'apellidomaterno'=>$apellidomaterno
							,'nombre'=>$nombre
							,'fechanacimiento'=>$fechanacimiento
							,'sexo'=>$sexo
							,'dni'=>$dni
							,'idestadocivil'=>$idestadocivil
							,'pais'=>$pais
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'email'=>$email
							,'idugel'=>$idugel
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'clave1'=>$clave1
							,'img'=>$img
							,'idcargo'=>$idcargo
							,'estado'=>$estado
							,'idempresa'=>$idempresa
							,'observacion'=>$observacion								
							);
			
			$this->oBD->update('alumno ', $estados, array('idalumno' => $idalumno));
		    $this->terminarTransaccion('dat_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM alumno WHERE idalumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			parent::conectar();
			return $this->oBD->delete('alumno', array('idalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			parent::conectar();
			$this->oBD->update('alumno', array($propiedad => $valor), array('idalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
   
		
}