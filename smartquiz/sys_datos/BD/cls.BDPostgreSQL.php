<?php
/**
 * Ultima modificacion	:	14/10/2009
 */

class BDPostgreSQL
{
	private $host;
	private $baseDatos;
	private $usuario;
	private $clave;
	//private $puerto = null;
	private $autocommit = true;
	private $oCnxPgSQL = null;
	private $ultimoError;
	private $codificacion = 'utf8_decode';
	protected static $instancia = null;
	private $id_transaccion = '';
	
	//LIMIT
	private $desde = 0;
	private $desplazar = 100;
	
	public function __construct($host, $baseDatos, $usuario, $clave/*, $puerto = null*/)
	{
		$this->host = $host;
		$this->baseDatos = $baseDatos;
		$this->usuario = $usuario;
		$this->clave = $clave;
		//$this->puerto = is_null($puerto) ? null : 3306;
	}
	
	public static function &getInstancia($host, $baseDatos, $usuario, $clave/*, $puerto = null*/)
	{
		if(!is_object(self::$instancia)) {
			self::$instancia = new self($host, $baseDatos, $usuario, $clave/*, $puerto*/);
		}
		
		return self::$instancia;
	}
	
	public function conectar()
	{
		if(true === $this->autocommit) {
			$this->oCnxPgSQL = pg_connect('host=' . $this->host . ' dbname=' . $this->baseDatos . ' user=' . $this->usuario . ' password=' . $this->clave);
			
			if(!$this->oCnxPgSQL) {
				throw new Exception('Error de conexi�n: ' . pg_last_error());
			}
			
			//$this->mysqli->set_charset("utf8");
		}
	}
	
	public function iniciarTransaccion($id_transaccion = '')
	{
		$this->conectar();
		
		if(false === $this->autocommit) return;
		
		try {
			$this->autocommit = false;
			$this->ejecutarSQL('BEGIN;');
			$this->id_transaccion = $id_transaccion;
		} catch(Exception $e) {
			throw new Exception('Imposible iniciar transacci�n: ' . $e->getMessage());
		}
	}
	
	public function terminarTransaccion($id_transaccion = '')
	{
		if($id_transaccion != $this->id_transaccion) return;
		
		if(false == $this->autocommit) {
			$this->ejecutarSQL('COMMIT;');
			$this->autocommit = true;
			$this->cerrarConexion();
			$this->id_transaccion = '';
		}
	}
	
	public function truncarTransaccion($id_transaccion = '')
	{
		if($id_transaccion != $this->id_transaccion) return;
		
		if(false == $this->autocommit) {
			$this->ejecutarSQL('ROLLBACK;');
			$this->cerrarConexion();
			$this->autocommit = true;
			$this->id_transaccion = '';
		}
	}
	
	public function agregarBloqueTransac($id_bloque)
	{
		if(false == $this->autocommit) {
			$this->ejecutarSQL('SAVEPOINT ' . $id_bloque .';');
		}
	}
	
	public function truncarBloqueTransac($id_bloque)
	{
		if(false == $this->autocommit) {
			$this->ejecutarSQL('ROLLBACK TO ' . $id_bloque . ';');
		}
	}
	
	public function cerrarConexion()
	{
		if(true === $this->autocommit) {
			pg_close($this->oCnxPgSQL);
			$this->oCnxPgSQL = null;
		}
	}
	
	public function setLimite($desde, $desplazamiento)
	{
		$this->desde		= (0 <= $desde) ? $desde : 0;
		$this->desplazar	= (1 <= $desplazamiento) ? $desplazamiento : 100;
	}
	
	public function resetLimite()
	{
		$this->setLimite(0, 50);
	}
	
	/**
	 * Agrega un LIKE simple de doble comodin
	 * @cadena
	 */
	public function like($valor)
	{
		try {
			return "LIKE '%" . $this->escapar($valor, false) . "%'";
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function insert($tabla, $estados)
	{
		try {
			if(!is_string($tabla) || !is_array($estados)) {
				throw new Exception('Tabla o estados no definidos');
			}
			
			$cols = array();
			$vals = array();
			
			foreach($estados as $col => $val) {
				$cols[] = $col;
				$vals[] = $this->escapar($val);
			}
			
			$sql = "INSERT INTO "
				 . $tabla
				 . ' (' . implode(', ', $cols) . ')'
				 . ' VALUES(' . implode(', ', $vals) . ')';
			
			/*echo $sql;
			return '010101';*/
			
			return $this->ejecutarSQL($sql);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function update($tabla, $estados, $condicion = null)
	{
		try {
			if(!is_string($tabla) || !is_array($estados)) {
				throw new Exception('Tabla o estados no definidos');
			}
			
			$sets = array();
			foreach($estados as $col => $val) {
				$sets[] = $col . " = " . $this->escapar($val);
			}
			
			$where = $this->where($condicion);
			
			$sql = "UPDATE "
				 . $tabla
				 . ' SET ' . implode(', ', $sets)
				 . ($where ? " WHERE " . $where : '');
			
			return $this->ejecutarSQL($sql);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function delete($tabla, $condicion = null)
	{
		try {
			if(!is_string($tabla) || !is_array($condicion)) {
				throw new Exception('Tabla o estados no definidos');
			}
			
			$where = $this->where($condicion);
			
			$sql = "DELETE FROM "
				 . $tabla
				 . ($where ? " WHERE " . $where : '');
			
			return $this->ejecutarSQL($sql);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	private function where($condicion)
	{
		if(empty($condicion) || !is_array($condicion)) {
            return false;
        }
		
		$conds = array();
		foreach($condicion as $col => $val) {
			$conds[] = $col . " = " . $this->escapar($val);
		}
		
		$conds = implode(' AND ', $conds);
        return $conds;
	}
	
	public function concatenar($piezas)
	{
		return implode(' || ', $piezas);
	}
	
	public function case_($evaluar, array $condiciones)
	{
		/*
		$condiciones[] = array(0 => condicion, 1 => accion);
		*/
		
		$sql = "CASE " . $evaluar;
		
		foreach($condiciones as $condicion) {
			$sql .= !is_null($condicion[0]) ? " WHEN " . $condicion[0] : " ELSE ";
			$sql .= !is_null($condicion[0]) ? " THEN " . $condicion[1] : $condicion[1];
		}
		
		$sql .= " END";
		
		return $sql;
	}
	
	public function consultarSQL($sql, $metodo = 'ARRAY', $tipo = 'ASOC', $campo_id = null)
	{//v 1.2
		try {
			if('' == trim($sql)) {
				throw new Exception('COM_SQL_NULL');
			}
			
			$sql .= " LIMIT " . $this->desplazar . " OFFSET " . $this->desde;
			
			$consultarSQL = array();
			
			$this->conectar();
			//echo $sql;
			if($resultado = pg_query($this->oCnxPgSQL, $sql)) {
				$num_rows = pg_num_rows($resultado);
				
				if('ARRAY' == $metodo) {
					if('ASOC' == $tipo) {
						for($i = 0; $i < $num_rows; $i++) {
							$fila = pg_fetch_array($resultado, $i);
							
							if(!empty($campo_id)) {
								$consultarSQL[$fila[$campo_id]] = $fila;
							} else {
								$consultarSQL[] = $fila;
							}
						}
					} else {
						for($i = 0; $i < $num_rows; $i++) {
							$fila = pg_fetch_array($resultado, $i);
							
							$consultarSQL[] = $fila;
						}
					}
				} elseif('OBJETO' == $metodo) {
					for($i = 0; $i < $num_rows; $i++) {
						$objeto = pg_fetch_object($resultado, $i);
						
						$consultarSQL[] = $objeto;
					}
				}
				
				pg_free_result($resultado);
			} else {
				throw new Exception($this->getError( pg_last_error() ));
			}
			
			$this->cerrarConexion();
			
			$this->resetLimite();
			
			return $consultarSQL;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function consultarEscalarSQL($sql)
	{
		try {
			if('' == trim($sql)) {
				throw new Exception('COM_SQL_NULL');
			}
			
			$this->conectar();
			
			if($resultado = pg_query($this->oCnxPgSQL, $sql)) {
				$consultarSQL = pg_fetch_array($resultado, 0);
				pg_free_result($resultado);
				
				$consultarSQL = $consultarSQL[0];
			} else {
				throw new Exception($this->getError(pg_last_error()));
			}
			
			$this->cerrarConexion();
			
			$this->resetLimite();
			
			return $consultarSQL;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function ejecutarSQL($sql)
	{
		try {
			if('' == trim($sql)) {
				throw new Exception('COM_SQL_NULL');
			}
			
			$ejecutarSQL = null;
			
			$this->conectar();
			
			if($resultado = pg_query($this->oCnxPgSQL, $sql)) {
				$ejecutarSQL = pg_affected_rows($resultado);
				pg_free_result($resultado);
			} else {
				throw new Exception($this->getError(pg_last_error()));
			}
			
			$this->cerrarConexion();
			
			return $ejecutarSQL;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
		
	public function setBaseDatos($baseDatos)
	{
		if(!empty($baseDatos)) {
			$this->baseDatos = $baseDatos;
		}
	}
	
	public function getUltmimoError()
	{
		return $this->getError(@pg_last_error());
	}
	
	private function setErrores()
	{
		if(empty($this->error)) {
			$this->error[1048] = 'El campo esta vac�o.';
			$this->error[1054] = 'Columna desconocida.';
			$this->error[1052] = 'Columna ambigua.';
			$this->error[1062] = 'El registro ya existe.';
			$this->error[1064] = 'Error SQL.';
			$this->error[1092] = 'Claves primarias duplicadas.';
			$this->error[1136] = 'N�mero de columnas incorrecto.';
			$this->error[1142] = 'Sin privilegios para continuar.';
			$this->error[1146] = 'La tabla no existe.';
			$this->error[1264] = 'Valor incorrecto.';
			$this->error[1292] = 'Valor incorrecto.';
			$this->error[1364] = 'Campo sin valor por defecto.';
			$this->error[1366] = 'Valor incorrecto para el campo.';
			$this->error[1406] = 'Longitud de los datos es muy extensa para el campo.';
			$this->error[1451] = 'Este registro tiene otros registros dependientes.';
			$this->error[1452] = 'Error de claves for�neas.';
		}
	}
	
	public function getError($idError)
	{
		$this->setErrores();
		
		if(isset($this->error[$idError])) {
			return $this->error[$idError];
		} else {
			return 'Error no encontrado ['.$idError.']';
		}
	}
	
	public function escapar($valor, $comillar = true)
	{
		try {
			if(is_array($valor)) {
				throw new Exception('Valor incorrecto (Array)');
			}
			
			if(is_int($valor) || is_float($valor)) {
				return $valor;
			}
			
			if(is_null($valor)) {
				return "''";
			}
			
			$this->conectar();
			
			$escape_ = pg_escape_string($this->oCnxPgSQL, $valor);
			//$escape_ = $this->mysqli->real_escape_string(call_user_func($this->codificacion, $valor));
			
			if(true === $comillar) {
				return "'" . $escape_ ."'";
			} else {
				return $escape_;
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function escaparArray($array)
	{
		try {
			if(empty($array) || !is_array($array)) {
				throw new Exception('Valor incorrecto para array');
			}
			
			$i = 0;
			foreach($array as $val) {
				$array[$i] = $this->escapar($val);
				++$i;
			}
			
			$array = implode(', ', $array);
			return $array;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}