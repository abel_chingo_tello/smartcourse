<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		14-11-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatMetodologia_habilidad extends DatBase
{
	public function __construct()
	{
		try {
			#parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT COUNT(*) FROM metodologia_habilidad";
			
			$cond = array();
			if(!empty($filtros["idmetodologia"])) {
				$cond[] = "idmetodologia = " . $this->oBD->escapar($filtros["idmetodologia"]);
			}
			if(!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM metodologia_habilidad";			
			
			$cond = array();
			if(!empty($filtros["idmetodologia"])) {
				$cond[] = "idmetodologia = " . $this->oBD->escapar($filtros["idmetodologia"]);
			}		
			if(!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["tipo"])) {
				$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			/*print_r ($filtros["tipo"]);
			exit;*/
			return $this->oBD->consultarSQL($sql);
			
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			parent::conectar();
			$sql = "SELECT  *  FROM metodologia_habilidad  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre,$tipo,$estado)
	{
		try {
			parent::conectar();
			
			$this->iniciarTransaccion('dat_metodologia_habilidad_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmetodologia) FROM metodologia_habilidad");
			++$id;
			if (!$estado) $estado=0;
			$estados = array('idmetodologia' => $id							
							,'nombre'=>$nombre
							,'tipo'=>$tipo
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('metodologia_habilidad', $estados);			
			$this->terminarTransaccion('dat_metodologia_habilidad_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_metodologia_habilidad_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$tipo,$estado)
	{
		try {
			parent::conectar();
			$this->iniciarTransaccion('dat_metodologia_habilidad_update');
			$estados = array('nombre'=>$nombre
							,'tipo'=>$tipo
							,'estado'=>$estado								
							);
			
			$this->oBD->update('metodologia_habilidad ', $estados, array('idmetodologia' => $id));
		    $this->terminarTransaccion('dat_metodologia_habilidad_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			parent::conectar();
			$sql = "SELECT  *  FROM metodologia_habilidad  "
					. " WHERE idmetodologia = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			parent::conectar();
			return $this->oBD->delete('metodologia_habilidad', array('idmetodologia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			parent::conectar();
			$this->oBD->update('metodologia_habilidad', array($propiedad => $valor), array('idmetodologia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Metodologia_habilidad").": " . $e->getMessage());
		}
	}
   
		
}