<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatPersonal extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT COUNT(1) FROM personal";
			
			$cond = array();		
			
			if(isset($filtros["dni"])) {
				$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = " ( ape_paterno " . $this->oBD->like($filtros["texto"])." OR ape_materno " . $this->oBD->like($filtros["texto"])." OR nombre " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["ape_paterno"])) {
				$cond[] = "ape_paterno =" . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if(isset($filtros["ape_materno"])) {
				$cond[] = "ape_materno =" . $this->oBD->escapar($filtros["ape_materno"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre =" . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fechanac"])) {
				$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(isset($filtros["sexo"])) {
				$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["estado_civil"])) {
				$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["urbanizacion"])) {
				$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if(isset($filtros["direccion"])) {
				$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
				$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
				$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
				$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["usuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			
			if(isset($filtros["rol"])) {
				$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			}
			if(isset($filtros["foto"])) {
				$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["identificador"])) {
				$cond[] = "identificador = " . $this->oBD->escapar($filtros["identificador"]);
			}
			if(isset($filtros["idioma"])) {
				$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
	
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT P.`dni`,P.`ape_paterno`,P.`ape_materno`, P.`nombre`, P.`usuario`, P.`clave`,P.`identificador`,P.`estado`, PR.nombre AS proyecto_nombre, PR.slug AS proyecto_slug FROM personal P JOIN proyecto PR ON P.idproyecto = PR.idproyecto";			
			$cond = array();
			if(isset($filtros["dni"])) {
				$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = " ( ape_paterno " . $this->oBD->like($filtros["texto"])." OR ape_materno " . $this->oBD->like($filtros["texto"])." OR nombre " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["ape_paterno"])) {
				$cond[] = "ape_paterno = " . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if(isset($filtros["ape_materno"])) {
				$cond[] = "ape_materno = " . $this->oBD->escapar($filtros["ape_materno"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fechanac"])) {
				$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(isset($filtros["sexo"])) {
				$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["estado_civil"])) {
				$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["urbanizacion"])) {
				$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if(isset($filtros["direccion"])) {
				$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
				$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
				$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
				$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["usuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["token"])) {
				$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}
			if(isset($filtros["foto"])) {
				$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["identificador"])) {
				$cond[] = "identificador = " . $this->oBD->escapar($filtros["identificador"]);
			}
			if(isset($filtros["idioma"])) {
				$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "P.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["slug"])) {
				$cond[] = "slug = " . $this->oBD->escapar($filtros["slug"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY ape_paterno, ape_materno, P.nombre";
			#echo $sql; exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function buscarConRol($filtros=null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT DISTINCT P.`dni`,P.`ape_paterno`,P.`ape_materno`, P.`nombre`, P.`usuario`, P.`clave`,P.`identificador`,P.`estado`, FROM personal P JOIN  rol_personal RP ON P.dni=RP.idpersonal JOIN `rol` R ON RP.idrol=R.idrol ";
			
			$cond = array();		
					
			
			if(isset($filtros["dni"])) {
				$cond[] = "P.dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = "P. ( ape_paterno " . $this->oBD->like($filtros["texto"])." OR ape_materno " . $this->oBD->like($filtros["texto"])." OR nombre " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["ape_paterno"])) {
				$cond[] = "P.ape_paterno = " . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if(isset($filtros["ape_materno"])) {
				$cond[] = "P.ape_materno = " . $this->oBD->escapar($filtros["ape_materno"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "P.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fechanac"])) {
				$cond[] = "P.fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(isset($filtros["sexo"])) {
				$cond[] = "P.sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["estado_civil"])) {
				$cond[] = "P.estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(isset($filtros["ubigeo"])) {
				$cond[] = "P.ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["urbanizacion"])) {
				$cond[] = "P.urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if(isset($filtros["direccion"])) {
				$cond[] = "P.direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
				$cond[] = "P.telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
				$cond[] = "P.celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
				$cond[] = "P.email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "P.regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "P.regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["usuario"])) {
				$cond[] = "P.usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "P.clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["token"])) {
				$cond[] = "P.token = " . $this->oBD->escapar($filtros["token"]);
			}
			if(isset($filtros["foto"])) {
				$cond[] = "P.foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "P.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["identificador"])) {
				$cond[] = "P.identificador = " . $this->oBD->escapar($filtros["identificador"]);
			}
			if(isset($filtros["idioma"])) {
				$cond[] = "P.idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "P.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idrol"])) {
				$cond[] = "RP.idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["rol"])) {
				$cond[] = "R.rol = " . $this->oBD->escapar($filtros["rol"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function getxCredencial($usuario, $clave)
	{
		try {
			#parent::conectar();
			$sql = "SELECT R.*, P.`dni`,P.`ape_paterno`,P.`ape_materno`, P.`nombre`, P.`usuario`, P.`clave`,P.`identificador`,P.`estado`, RP.iddetalle FROM personal P "
					." LEFT JOIN rol_personal RP ON RP.idpersonal=P.dni "
					." INNER JOIN rol R ON R.idrol=RP.idrol "
					. " WHERE (P.usuario = " . $this->oBD->escapar($usuario)." OR P.identificador=". $this->oBD->escapar($usuario).") "
					. " AND P.clave = '" . md5($clave) . "' AND P.estado=1";
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	public function getxusuarioemail($usuema)
	{
		try {
			#parent::conectar();
			$sql = "SELECT R.*, P.`dni`,P.`ape_paterno`,P.`ape_materno`, P.`nombre`, P.`usuario`, P.`clave`,P.`identificador`,P.`estado`, RP.iddetalle  FROM personal P "
					." LEFT JOIN rol_personal RP ON RP.idpersonal=P.dni "
					." INNER JOIN rol R ON R.idrol=RP.idrol "
					. " WHERE usuario = " . $this->oBD->escapar($usuema)." OR email = " . $this->oBD->escapar($usuema);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user'). $e->getMessage());
		}
	}

	public function getxusuario($usuario)
	{
		try {
			#parent::conectar();
			$sql = "SELECT R.*, P.`dni`,P.`ape_paterno`,P.`ape_materno`, P.`nombre`, P.`usuario`, P.`clave`,P.`identificador`,P.`estado`, RP.iddetalle  FROM personal P "
					." LEFT JOIN rol_personal RP ON RP.idpersonal=P.dni "
					." INNER JOIN rol R ON R.idrol=RP.idrol "
					. " WHERE usuario = " . $this->oBD->escapar($usuario);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('users'). $e->getMessage());
		}
	}

	public function getxIdentificadorProyecto($identifier, $project)
	{
		try {
			#parent::conectar();
			$sql = "SELECT R.*, P.`dni`,P.`ape_paterno`,P.`ape_materno`, P.`nombre`, P.`usuario`, P.`clave`,P.`identificador`,P.`estado`, RP.iddetalle FROM personal P "
					." JOIN proyecto PR ON P.idproyecto = PR.idproyecto "
					." LEFT JOIN rol_personal RP ON RP.idpersonal=P.dni "
					." INNER JOIN rol R ON R.idrol=RP.idrol "
					." WHERE P.identificador=".$this->oBD->escapar($identifier)." AND PR.slug=".$this->oBD->escapar($project)." AND P.estado=1 AND PR.estado=1";
			//echo $sql;
			$res = $this->oBD->consultarSQL($sql);
			//var_dump($res);
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	public function getxToken($token)
	{
		try {
			#parent::conectar();
			$sql = "SELECT R.*, P.`dni`,P.`ape_paterno`,P.`ape_materno`, P.`nombre`, P.`usuario`, P.`clave`,P.`identificador`,P.`estado`, RP.iddetalle FROM personal P "
					." JOIN proyecto PR ON P.idproyecto = PR.idproyecto "
					." LEFT JOIN rol_personal RP ON RP.idpersonal=P.dni "
					." INNER JOIN rol R ON R.idrol=RP.idrol "
					." WHERE ( md5(CONCAT(P.identificador , ' ' , PR.slug)) = " . $this->oBD->escapar($token) . "  OR P.token= ". $this->oBD->escapar($token) ." ) AND P.estado=1 ";
			$res = $this->oBD->consultarSQL($sql);
			//echo $sql; var_dump($res[0]);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	public function insertar($ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idproyecto,$regusuario,$usuario,$clave,$token,$foto,$estado,$identificador,$idioma)
	{
		try {
			#parent::conectar();
			
			$this->iniciarTransaccion('dat_personal_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(dni) FROM personal");
			++$id;
			/* NOT NULL fields: */
			$estados = array('dni' => $id
							,'ape_paterno'=>$ape_paterno
							,'ape_materno'=>$ape_materno
							,'nombre'=>$nombre
							,'idproyecto'=>$idproyecto
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'token'=>$token
							,'estado'=>$estado
							,'idioma'=>$idioma							
							);
			/* NULL acepted fields: */
			if (!empty($fechanac)) { $estados['fechanac'] = $fechanac; }
			if (!empty($sexo)) { $estados['sexo'] = $sexo; }
			if (!empty($estado_civil)) { $estados['estado_civil'] = $estado_civil; }
			if (!empty($ubigeo)) { $estados['ubigeo'] = $ubigeo; }
			if (!empty($urbanizacion)) { $estados['urbanizacion'] = $urbanizacion; }
			if (!empty($direccion)) { $estados['direccion'] = $direccion; }
			if (!empty($telefono)) { $estados['telefono'] = $telefono; }
			if (!empty($celular)) { $estados['celular'] = $celular; }
			if (!empty($email)) { $estados['email'] = $email; }
			if (!empty($identificador)) { $estados['identificador'] = $identificador; }
			if (!empty($regusuario)) { $estados['regusuario'] = ($regusuario=='_self_')?$id:$regusuario; }
			if (!empty($foto)) { $estados['foto'] = $foto; }

			$this->oBD->insert('personal', $estados);			
			$this->terminarTransaccion('dat_personal_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_personal_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idproyecto,$regusuario,$usuario,$clave,$token,$foto,$estado,$identificador,$idioma)
	{
		try {
			#parent::conectar();
			$this->iniciarTransaccion('dat_personal_update');

			/* NOT NULL fields: */
			$estados = array('dni' => $id
							,'ape_paterno'=>$ape_paterno
							,'ape_materno'=>$ape_materno
							,'nombre'=>$nombre
							,'idproyecto'=>$idproyecto
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'token'=>$token
							,'estado'=>$estado
							,'idioma'=>$idioma							
							);
			/* NULL acepted fields: */
			if (!empty($fechanac)) { $estados['fechanac'] = $fechanac; }
			if (!empty($sexo)) { $estados['sexo'] = $sexo; }
			if (!empty($estado_civil)) { $estados['estado_civil'] = $estado_civil; }
			if (!empty($ubigeo)) { $estados['ubigeo'] = $ubigeo; }
			if (!empty($urbanizacion)) { $estados['urbanizacion'] = $urbanizacion; }
			if (!empty($direccion)) { $estados['direccion'] = $direccion; }
			if (!empty($telefono)) { $estados['telefono'] = $telefono; }
			if (!empty($celular)) { $estados['celular'] = $celular; }
			if (!empty($email)) { $estados['email'] = $email; }
			if (!empty($identificador)) { $estados['identificador'] = $identificador; }
			if (!empty($regusuario)) { $estados['regusuario'] = $regusuario; }
			if (!empty($foto)) { $estados['foto'] = $foto; }

			$this->oBD->update('personal ', $estados, array('dni' => $id));
		    $this->terminarTransaccion('dat_personal_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			#parent::conectar();
			$sql = "SELECT `dni`,`ape_paterno`,`ape_materno`, `nombre`, `usuario`, `clave`,`identificador`,`estado`  FROM personal "
					. " WHERE dni = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			#parent::conectar();
			return $this->oBD->delete('personal', array('dni' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			#parent::conectar();
			$this->oBD->update('personal', array($propiedad => $valor), array('dni' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
   
		
}