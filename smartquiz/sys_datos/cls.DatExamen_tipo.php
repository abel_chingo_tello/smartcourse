<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatExamen_tipo extends DatBase
{
	public function __construct()
	{
		try {
			#parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Examen_tipo").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT COUNT(*) FROM examen_tipo";
			
			$cond = array();		
			
			if(!empty($filtros["idtipo"])) {
					$cond[] = "idtipo = " . $this->oBD->escapar($filtros["idtipo"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Examen_tipo").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM examen_tipo";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idtipo"])) {
					$cond[] = "idtipo = " . $this->oBD->escapar($filtros["idtipo"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Examen_tipo").": " . $e->getMessage());
		}
	}
	
	public function insertar($tipo,$estado)
	{
		try {
			parent::conectar();
			
			$this->iniciarTransaccion('dat_examen_tipo_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtipo) FROM examen_tipo");
			++$id;
			
			$estados = array('idtipo' => $id							
							,'tipo'=>$tipo
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('examen_tipo', $estados);			
			$this->terminarTransaccion('dat_examen_tipo_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_examen_tipo_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Examen_tipo").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tipo,$estado)
	{
		try {
			parent::conectar();
			$this->iniciarTransaccion('dat_examen_tipo_update');
			$estados = array('tipo'=>$tipo
							,'estado'=>$estado								
							);
			
			$this->oBD->update('examen_tipo ', $estados, array('idtipo' => $id));
		    $this->terminarTransaccion('dat_examen_tipo_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examen_tipo").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			parent::conectar();
			$sql = "SELECT  *  FROM examen_tipo  "
					. " WHERE idtipo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Examen_tipo").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			parent::conectar();
			return $this->oBD->delete('examen_tipo', array('idtipo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Examen_tipo").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			parent::conectar();
			$this->oBD->update('examen_tipo', array($propiedad => $valor), array('idtipo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examen_tipo").": " . $e->getMessage());
		}
	}
   
		
}