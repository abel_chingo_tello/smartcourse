<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatUsuario extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Usuario").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM usuario";
			
			$cond = array();		
			
			if(isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = " ( apellidos " . $this->oBD->like($filtros["texto"])." OR nombres " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["apellidos"])) {
				$cond[] = "apellidos =" . $this->oBD->escapar($filtros["apellidos"]);
			}
			if(isset($filtros["nombres"])) {
				$cond[] = "nombres =" . $this->oBD->escapar($filtros["nombres"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["codigo"])) {
				$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(isset($filtros["idempresa"])) {
				$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
	
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Usuario").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT U.idusuario, U.apellidos, U.nombres, U.estado, U.regusuario, U.regfecha, U.codigo,U.idempresa, E.nombre AS nombre_empresa, (US.apellidos || ' ' || US.nombres) AS nombre_regusuario FROM usuario U LEFT JOIN empresa E ON U.idempresa=E.idempresa JOIN usuario US ON U.regusuario=US.idusuario ";			
			
			$cond = array();		

			if(isset($filtros["idusuario"])) {
				$cond[] = "U.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = " ( U.apellidos " . $this->oBD->like($filtros["texto"])." OR U.nombres " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["apellidos"])) {
				$cond[] = "U.apellidos =" . $this->oBD->escapar($filtros["apellidos"]);
			}
			if(isset($filtros["nombres"])) {
				$cond[] = "U.nombres =" . $this->oBD->escapar($filtros["nombres"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "U.clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "U.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "U.regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "U.regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["codigo"])) {
				$cond[] = "U.codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(isset($filtros["idempresa"])) {
				$cond[] = "U.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Usuario").": " . $e->getMessage());
		}
	}

	public function getxCredencial($usuario, $clave)
	{
		try {
			$sql = "SELECT * FROM usuario "
					. " WHERE (idusuario = " . $this->oBD->escapar($usuario).") "
					. " AND clave = '" . md5($clave) . "' AND estado='A'";
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	public function getxusuarioemail($usuema)
	{
		try {
			$sql = "SELECT *  FROM usuario "
					. " WHERE idusuario = " . $this->oBD->escapar($usuema);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('Usuario'). $e->getMessage());
		}
	}

	public function getxusuario($usuario)
	{
		try {
			$sql = "SELECT *  FROM usuario  "
					. " WHERE idusuario = " . $this->oBD->escapar($usuario);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('Usuarios'). $e->getMessage());
		}
	}

	public function getxtoken($token)
	{
		try {
			$sql = "SELECT *  FROM usuario "
					. " WHERE token = " . $this->oBD->escapar($token);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('Usuario').$e->getMessage());
		}
	}
	
	
	public function insertar($apellidos,$nombres,$clave,$estado,$regusuario,$regfecha,$codigo,$idempresa, $usuario)
	{
		try {
			
			$this->iniciarTransaccion('dat_usuario_insert');
			
			//$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idusuario) FROM usuario");
			//++$id;
			$id = $usuario;
			$estados = array('idusuario' => $id
							,'apellidos'=>$apellidos
							,'nombres'=>$nombres
							,'clave'=>$clave
							,'estado'=>$estado
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'codigo'=>$codigo
							,'idempresa'=>$idempresa
							);
			
			$this->oBD->insert('usuario', $estados);			
			$this->terminarTransaccion('dat_usuario_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_usuario_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Usuario").": " . $e->getMessage());
		}
	}
	public function actualizar($idusuario,$apellidos,$nombres,$clave,$estado,$regusuario,$regfecha,$codigo,$idempresa)
	{
		try {
			$this->iniciarTransaccion('dat_usuario_update');
			$estados = array('apellidos'=>$apellidos
							,'nombres'=>$nombres
							,'clave'=>$clave
							,'estado'=>$estado
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'codigo'=>$codigo
							,'idempresa'=>$idempresa								
							);
			
			$this->oBD->update('usuario ', $estados, array('idusuario' => $idusuario));
		    $this->terminarTransaccion('dat_usuario_update');
		    return $idusuario;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Usuario").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT *  FROM usuario "
					. " WHERE idusuario = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Usuario").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('usuario', array('idusuario' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Usuario").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('usuario', array($propiedad => $valor), array('idusuario' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Usuario").": " . $e->getMessage());
		}
	}
   
		
}