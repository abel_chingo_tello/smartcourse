<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAlumno extends DatBase
{
	public function __construct()
	{
		try {
			#parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT COUNT(*) FROM alumno";
			
			$cond = array();
			
			if(isset($filtros["dni"])) {
				$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = " ( ape_paterno " . $this->oBD->like($filtros["texto"])." OR ape_materno " . $this->oBD->like($filtros["texto"])." OR nombre " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["ape_paterno"])) {
				$cond[] = "ape_paterno " . $this->oBD->like($filtros["ape_paterno"]);
			}
			if(isset($filtros["ape_materno"])) {
				$cond[] = "ape_materno " . $this->oBD->like($filtros["ape_materno"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["nombre"]);
			}
			if(isset($filtros["fechanac"])) {
				$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(isset($filtros["sexo"])) {
				$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["estado_civil"])) {
				$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["urbanizacion"])) {
				$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if(isset($filtros["direccion"])) {
				$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
				$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
				$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
				$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["usuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["token"])) {
				$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}
			
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["situacion"])) {
				$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM alumno";			
			
			$cond = array();		
					
			
			if(isset($filtros["dni"])) {
				$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = " ( ape_paterno " . $this->oBD->like($filtros["texto"])." OR ape_materno " . $this->oBD->like($filtros["texto"])." OR nombre " . $this->oBD->like($filtros["texto"])." ) ";
			}
			if(isset($filtros["ape_paterno"])) {
				$cond[] = "ape_paterno =" . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if(isset($filtros["ape_materno"])) {
				$cond[] = "ape_materno =" . $this->oBD->escapar($filtros["ape_materno"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre =" . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fechanac"])) {
				$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(isset($filtros["sexo"])) {
				$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["estado_civil"])) {
				$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(isset($filtros["ubigeo"])) {
				$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["urbanizacion"])) {
				$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if(isset($filtros["direccion"])) {
				$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
				$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
				$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
				$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["regusuario"])) {
				$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
				$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["usuario"])) {
				$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
				$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["token"])) {
				$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}
			if(isset($filtros["foto"])) {
				$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["situacion"])) {
				$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}

	public function getxCredencial($usuario, $clave)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM alumno "
					. " WHERE (usuario = " . $this->oBD->escapar($usuario)." OR email=". $this->oBD->escapar($usuario).") "
					. " AND clave = '" . md5($clave) . "' AND estado=1";
			$res = $this->oBD->consultarSQL($sql);			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	public function getxusuario($usuario)
	{
		try {
			parent::conectar();
			$sql = "SELECT *  FROM alumno  "
					. " WHERE usuario = " . $this->oBD->escapar($usuario);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('users'). $e->getMessage());
		}
	}

	public function getxusuarioemail($usuema)
	{
		try {
			parent::conectar();
			$sql = "SELECT *  FROM alumno "
					. " WHERE usuario = " . $this->oBD->escapar($usuema)." OR email = " . $this->oBD->escapar($usuema);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user'). $e->getMessage());
		}
	}

	public function getxToken($token)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM alumno WHERE ( md5(CONCAT(usuario , ' ' , clave)) = '" . $token . "'  OR token= '". $token ."' ) AND estado=1 ";
			$res = $this->oBD->consultarSQL($sql);
			//echo $sql; var_dump($res[0]);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	public function getxUserPassProj($user, $password, $project)
	{
		try {
			parent::conectar();
			$sql = "SELECT A.* FROM alumno A LEFT JOIN proyecto P ON P.idproyecto = A.idproyecto WHERE A.usuario= ".$user." AND A.clave= ".$password." AND P.slug=".$project." AND A.estado=1 AND P.estado=1";
			$res = $this->oBD->consultarSQL($sql);
			//echo $sql; var_dump($res[0]);
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	/*public function getxtoken($token) //antigua
	{
		try {
			parent::conectar();
			$sql = "SELECT *  FROM alumno "
					. " WHERE token = " . $this->oBD->escapar($token);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user').$e->getMessage());
		}
	}*/

	public function insertar($ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idproyecto,$regusuario,$regfecha,$usuario,$clave,$token,$foto,$estado,$situacion)
	{
		try {
			parent::conectar();
			
			$this->iniciarTransaccion('dat_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(dni) FROM alumno");
			++$id;
			
			$estados = array('dni' => $id
							
							,'ape_paterno'=>$ape_paterno
							,'ape_materno'=>$ape_materno
							,'nombre'=>$nombre
							,'fechanac'=>$fechanac
							,'sexo'=>$sexo
							,'estado_civil'=>$estado_civil
							,'ubigeo'=>$ubigeo
							,'urbanizacion'=>$urbanizacion
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'email'=>$email
							,'idproyecto'=>$idproyecto
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'token'=>$token
							,'foto'=>$foto
							,'estado'=>$estado
							,'situacion'=>$situacion							
							);
			$this->oBD->insert('alumno', $estados);			
			$this->terminarTransaccion('dat_alumno_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idproyecto,$regusuario,$regfecha,$usuario,$clave,$token,$foto,$estado,$situacion)
	{
		try {
			parent::conectar();
			$this->iniciarTransaccion('dat_alumno_update');
			$estados = array('ape_paterno'=>$ape_paterno
							,'ape_materno'=>$ape_materno
							,'nombre'=>$nombre
							,'fechanac'=>$fechanac
							,'sexo'=>$sexo
							,'estado_civil'=>$estado_civil
							,'ubigeo'=>$ubigeo
							,'urbanizacion'=>$urbanizacion
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'email'=>$email
							,'idproyecto'=>$idproyecto
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'token'=>$token
							,'foto'=>$foto
							,'estado'=>$estado
							,'situacion'=>$situacion								
							);
			
			$this->oBD->update('alumno ', $estados, array('dni' => $id));
		    $this->terminarTransaccion('dat_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			parent::conectar();
			$sql = "SELECT *  FROM alumno WHERE dni = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			parent::conectar();
			return $this->oBD->delete('alumno', array('dni' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			parent::conectar();
			$this->oBD->update('alumno', array($propiedad => $valor), array('dni' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Alumno").": " . $e->getMessage());
		}
	}
   
		
}