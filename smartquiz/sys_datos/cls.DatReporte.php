<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatReporte extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM reporte";
			
			$cond = array();		
			
			if(!empty($filtros["idreporte"])) {
					$cond[] = "idreporte = " . $this->oBD->escapar($filtros["idreporte"]);
			}
			if(!empty($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["id_alumno_grupo"])) {
					$cond[] = "id_alumno_grupo = " . $this->oBD->escapar($filtros["id_alumno_grupo"]);
			}
			if(!empty($filtros["informacion"])) {
					$cond[] = "informacion = " . $this->oBD->escapar($filtros["informacion"]);
			}
			if(!empty($filtros["fechacreacion"])) {
					$cond[] = "fechacreacion = " . $this->oBD->escapar($filtros["fechacreacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM reporte";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idreporte"])) {
					$cond[] = "idreporte = " . $this->oBD->escapar($filtros["idreporte"]);
			}
			if(!empty($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["id_alumno_grupo"])) {
					$cond[] = "id_alumno_grupo = " . $this->oBD->escapar($filtros["id_alumno_grupo"]);
			}
			if(!empty($filtros["informacion"])) {
					$cond[] = "informacion = " . $this->oBD->escapar($filtros["informacion"]);
			}
			if(!empty($filtros["fechacreacion"])) {
					$cond[] = "fechacreacion = " . $this->oBD->escapar($filtros["fechacreacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY fechacreacion DESC ";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}
	public function buscarultimo($filtros=null)
	{
		try {
			$sql = "SELECT * FROM reporte";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idreporte"])) {
					$cond[] = "idreporte = " . $this->oBD->escapar($filtros["idreporte"]);
			}
			if(!empty($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["id_alumno_grupo"])) {
					$cond[] = "id_alumno_grupo = " . $this->oBD->escapar($filtros["id_alumno_grupo"]);
			}
			if(!empty($filtros["informacion"])) {
					$cond[] = "informacion = " . $this->oBD->escapar($filtros["informacion"]);
			}
			if(!empty($filtros["fechacreacion"])) {
					$cond[] = "fechacreacion <> " . $this->oBD->escapar($filtros["fechacreacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY fechacreacion DESC ";

			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search Last")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM reporte  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}
	
	public function insertar($idusuario,$idrol,$tipo,$id_alumno_grupo,$informacion,$fechacreacion)
	{
		try {
			
			$this->iniciarTransaccion('dat_reporte_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idreporte) FROM reporte");
			++$id;
			
			$estados = array('idreporte' => $id
							,'idusuario'=>$idusuario
							,'idrol'=>$idrol
							,'tipo'=>$tipo
							,'id_alumno_grupo'=>$id_alumno_grupo
							,'informacion'=>$informacion
							,'fechacreacion'=>$fechacreacion
							);
			
			$this->oBD->insert('reporte', $estados);			
			$this->terminarTransaccion('dat_reporte_insert');			
			return $id;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_reporte_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idusuario,$idrol,$tipo,$id_alumno_grupo,$informacion,$fechacreacion)
	{
		try {
			$this->iniciarTransaccion('dat_reporte_update');
			$estados = array('idusuario'=>$idusuario
							,'idrol'=>$idrol
							,'tipo'=>$tipo
							,'id_alumno_grupo'=>$id_alumno_grupo
							,'informacion'=>$informacion
							,'fechacreacion'=>$fechacreacion								
							);
			
			$this->oBD->update('reporte ', $estados, array('idreporte' => $id));
		    $this->terminarTransaccion('dat_reporte_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM reporte  "
					. " WHERE idreporte = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('reporte', array('idreporte' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('reporte', array($propiedad => $valor), array('idreporte' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Reporte").": " . $e->getMessage());
		}
	}

}