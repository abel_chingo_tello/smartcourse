<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-11-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatActividad_detalle extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM actividad_detalle";
			
			$cond = array();		
			
			if(!empty($filtros["iddetalle"])) {
					$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}

			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo LIKE '" . $this->oBD->escapar($filtros["titulo"],false)."%'";
			}	
	
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["tipo_desarrollo"])) {
					$cond[] = "tipo_desarrollo = " . $this->oBD->escapar($filtros["tipo_desarrollo"]);
			}
			if(!empty($filtros["tipo_actividad"])) {
					$cond[] = "tipo_actividad = " . $this->oBD->escapar($filtros["tipo_actividad"]);
			}
		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM actividad_detalle";			
			
			$cond = array();		
			if(!empty($filtros["iddetalle"])) {
					$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo LIKE '" . $this->oBD->escapar($filtros["titulo"],false)."%'";
			}	
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["tipo_desarrollo"])) {
					$cond[] = "tipo_desarrollo = " . $this->oBD->escapar($filtros["tipo_desarrollo"]);
			}
			if(!empty($filtros["tipo_actividad"])) {
					$cond[] = "tipo_actividad = " . $this->oBD->escapar($filtros["tipo_actividad"]);
			}
	
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql.=" ORDER BY orden ASC";
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}

	public function insertar($idactividad,$pregunta,$orden,$url,$tipo,$tipo_desarrollo,$tipo_actividad,$idhabilidad,$texto=null,$texto_edit=null,$tit=null,$des=null,$iduser=null)
	{
		try {
			
			$this->iniciarTransaccion('dat_actividad_detalle_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM actividad_detalle");
			++$id;
			
			$estados = array('iddetalle' => $id							
							,'idactividad'=>$idactividad
							,'pregunta'=>$pregunta							
							,'orden'=>$orden
							,'url'=>$url							
							,'tipo'=>$tipo
							,'tipo_desarrollo'=>$tipo_desarrollo
							,'tipo_actividad'=>$tipo_actividad
							,'idhabilidad'=>$idhabilidad
							,'texto'=>$texto
							,'texto_edit'=>$texto_edit
							,'titulo'=>$tit
							,'descripcion'=>$des
							,'idpersonal'=>$iduser
							);
			
			$this->oBD->insert('actividad_detalle', $estados);			
			$this->terminarTransaccion('dat_actividad_detalle_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_actividad_detalle_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idactividad,$pregunta,$orden,$url,$tipo,$tipo_desarrollo,$tipo_actividad,$idhabilidad=null,$texto=null,$texto_edit=null,$tit=null,$des=null,$iduser=null){
		try {
			$this->iniciarTransaccion('dat_actividad_detalle_update');
			$estados = array('idactividad'=>$idactividad
							,'pregunta'=>$pregunta							
							,'orden'=>$orden
							,'url'=>$url
							,'tipo'=>$tipo
							,'tipo_desarrollo'=>$tipo_desarrollo
							,'tipo_actividad'=>$tipo_actividad
							,'idhabilidad'=>$idhabilidad	
							,'texto'=>$texto
							,'texto_edit'=>$texto_edit
							,'titulo'=>$tit
							,'descripcion'=>$des
							,'idpersonal'=>$iduser
							);
			
			$this->oBD->update('actividad_detalle ', $estados, array('iddetalle' => $id));
		    $this->terminarTransaccion('dat_actividad_detalle_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			
			$sql = "SELECT  *  FROM actividad_detalle  "
					. " WHERE iddetalle = " . $this->oBD->escapar($id);	
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('actividad_detalle', array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}

	public function eliminarxActividad($id)
	{
		try {
			return $this->oBD->delete('actividad_detalle', array('idactividad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('actividad_detalle', array($propiedad => $valor), array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividad_detalle").": " . $e->getMessage());
		}
	}
   
		
}