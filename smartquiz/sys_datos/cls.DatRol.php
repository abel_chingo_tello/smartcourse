<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatRol extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM rol";			
			$cond = array();
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["rol"])) {
					$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM rol";
			$cond = array();
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["rol"])) {
					$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
		
	public function insertar($rol)
	{
		try {			
			$this->iniciarTransaccion('dat_rol_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrol) FROM rol");
			++$id;			
			$estados = array('idrol' => $id	,'rol'=>$rol);
			$this->oBD->insert('rol', $estados);			
			$this->terminarTransaccion('dat_rol_insert');			
			return $id;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rol_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $rol)
	{
		try {
			$this->iniciarTransaccion('dat_rol_update');
			$estados = array('rol'=>$rol);			
			$this->oBD->update('rol ', $estados, array('idrol' => $id));
		    $this->terminarTransaccion('dat_rol_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM rol WHERE idrol = " . $this->oBD->escapar($id);			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			$this->oBD->delete('permisos', array('rol' => $id));
			return $this->oBD->delete('rol', array('idrol' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('rol', array($propiedad => $valor), array('idrol' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
	public function existe_permiso($rol, $menu, $accion)
	{//02.01.13
		
		try { 
			if($rol==1)return true;
			$in_permiso=array('list','add','edit','delete');
			if(!in_array($accion,$in_permiso)){
				return false;
			}
			$sql = "SELECT COUNT(*) FROM permisos p left Join rol r on p.rol=idrol left Join menu m on p.menu=idmenu"
						. " WHERE (lower(r.rol) = " . $this->oBD->escapar(strtolower($rol))." OR p.rol=".$this->oBD->escapar($rol)." ) "
						. " AND (lower(m.nombre) = " . $this->oBD->escapar(strtolower($menu))." OR p.menu=".$this->oBD->escapar($menu)." ) "
						. " AND lower(_".$accion.")='1' ";
			$res = $this->oBD->consultarEscalarSQL($sql);
			return empty($res) ? false : true;			
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Privilege exists")." " . $e->getMessage());
		}
	}	
}