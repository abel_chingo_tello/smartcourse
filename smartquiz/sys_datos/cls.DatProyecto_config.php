<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-10-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatProyecto_config extends DatBase
{
	public function __construct()
	{
		try {
			#parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Proyecto_config").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT COUNT(*) FROM proyecto_config";
			
			$cond = array();		
			
			if(isset($filtros["idconfig"])) {
					$cond[] = "idconfig = " . $this->oBD->escapar($filtros["idconfig"]);
			}
			if(isset($filtros["configuracion"])) {
					$cond[] = "configuracion = " . $this->oBD->escapar($filtros["configuracion"]);
			}
			if(isset($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Proyecto_config").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM proyecto_config";			
			
			$cond = array();		
					
			
			if(isset($filtros["idconfig"])) {
					$cond[] = "idconfig = " . $this->oBD->escapar($filtros["idconfig"]);
			}
			if(isset($filtros["configuracion"])) {
					$cond[] = "configuracion = " . $this->oBD->escapar($filtros["configuracion"]);
			}
			if(isset($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			//return $this->oBD->consultarSQL($sql);
			return $this->oBD->consultarSQL($sql, 'ARRAY', 'ASOC', 'configuracion');
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Proyecto_config").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($configuracion,$valor,$idproyecto)
	{
		try {
			parent::conectar();
			
			$this->iniciarTransaccion('dat_proyecto_config_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idconfig) FROM proyecto_config");
			++$id;
			
			$estados = array('idconfig' => $id
							
							,'configuracion'=>$configuracion
							,'valor'=>$valor
							,'idproyecto'=>$idproyecto							
							);
			
			$this->oBD->insert('proyecto_config', $estados);			
			$this->terminarTransaccion('dat_proyecto_config_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_proyecto_config_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Proyecto_config").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $configuracion,$valor,$idproyecto)
	{
		try {
			parent::conectar();
			$this->iniciarTransaccion('dat_proyecto_config_update');
			$estados = array('configuracion'=>$configuracion
							,'valor'=>$valor
							,'idproyecto'=>$idproyecto								
							);
			
			$this->oBD->update('proyecto_config ', $estados, array('idconfig' => $id));
		    $this->terminarTransaccion('dat_proyecto_config_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto_config").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			parent::conectar();
			$sql = "SELECT  *  FROM proyecto_config  "
					. " WHERE idconfig = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Proyecto_config").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			parent::conectar();
			return $this->oBD->delete('proyecto_config', array('idconfig' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Proyecto_config").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			parent::conectar();
			$this->oBD->update('proyecto_config', array($propiedad => $valor), array('idconfig' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto_config").": " . $e->getMessage());
		}
	}
   
		
}