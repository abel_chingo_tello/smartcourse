<?php

date_default_timezone_set('America/Lima');
error_reporting(E_ALL);
// ini_set('display_errors', '1');

define('CARPETA_RAIZ', 'smart/smartcourse/smartquiz');
define('_version_', 'modalshow');
define('IDPROYECTO', 'smartcourse');
define('IDLEARN', 'loadtest');
define('_ishttp_', true);
define("_CONFIG_", 
    json_encode(
        array(
            "frontend" => array(
                "tema" => "tema1"
                ,"is_login" => true
            )
        )
    )
);

/* Para la base de datos */
#DESARROLLO
/*define("HOST_BD", '34.68.37.175');
define("USER_BD", 'desarrollo');
define("PWD_BD", 'Abaco2020$$$');
define("NAME_BD", 'SCQUIZDB');*/

define("HOST_BD", "localhost");
define("USER_BD", "root");
define("PWD_BD", "");
define("NAME_BD", "smart_skscourses_quiz");

/* No cambiar */
$host= !empty($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : 'localhost';
$url= @$_SERVER["REQUEST_URI"];
define('sysVERSION', _version_);
if(defined('_http_')==false) define('_http_', !empty($_SERVER['HTTPS']) ? 'https://' : 'http://');
if(defined('SD')==false) define('SD', DIRECTORY_SEPARATOR);
if(defined('_HOST_')==false) define('_HOST_', _http_ . $host);
define('_BOOT_', true);                     
$ruta_ini=dirname(__FILE__);
define('RUTA_BASE', $ruta_ini . SD);
define('RUTA_RAIZ', RUTA_BASE);
define('RUTA_LIBS', RUTA_RAIZ. 'sys_lib' . SD);
define('RUTA_INC',	RUTA_BASE. 'sys_inc' . SD);
define('URL_BASE', _HOST_ . '/' . CARPETA_RAIZ);
define('URL_RAIZ', URL_BASE);
define('URL_MEDIA', URL_RAIZ);
@session_start();
$_SESSION["urlredirok"]=_http_.$host.$url;
if(_ishttp_==true&&empty($_SERVER['HTTPS'])){header('Location:'._HOST_.$url); exit(0);}