<?php
/**
 * @autor		Abel Chingo Tello : ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016 Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class Sitio extends JrAplicacion{
	public static $msjs;	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function iniciar($compat = array())
	{
		if(true === $this->inicio) {
			return;
		}		
		JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
		JrCargador::clase('sys_inc::ConfigSitio', RUTA_BASE, 'sys_inc::');
		JrCargador::clase('sys_datos::DatBase', RUTA_BASE, 'sys_datos');
		JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
		JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
		$oConfigSitio = ConfigSitio::getInstancia();
		parent::iniciar($oConfigSitio->get_());
	}	
	public function enrutar($rec = null, $ax = null)
 	{
	
		
		
		$permitenologueado=array('service','api');
		if(IS_LOGIN===true&&!NegSesion::existeSesion()&&!in_array($rec,$permitenologueado)){
			parent::enrutar('sesion');
		} else {

			/* * * Editado eder.figueroa * * */ 
			if(!empty($_GET['id'])&&!empty($_GET['pr'])){
				$identificador = @$_GET['id'];
				$proyecto = @$_GET['pr'];
				$rol = (!empty(@$_GET['type'])&&@$_GET['type']=='admin')?'docente':'alumno';
				if(true !== NegSesion::existeSesion()){
					if( true !== $this->oNegSesion->ingresarxIdentifUser_Proyecto( array("identifier"=>@$_GET['id'], "project_slug"=>@$_GET['pr'], "user"=>@$_GET['u'], "password"=>@$_GET['p'], "tipoAcceso"=>@$_GET['type'], "callback"=>@$_GET["callback"], "lang"=>@$_GET["lang"], "otrosParams"=>$otrosParams,"urlback"=>@$_GET["urlback"]) ) ) {
						
						parent::enrutar('sesion');
						
					}
				}
				$usuarioAct = NegSesion::getUsuario();

				$usuario=!empty(@$_GET['usuario'])?$_GET['usuario']:$usuarioAct['usuario'];
				$type=!empty(@$_GET['type'])?$_GET['type']:'alumno';

				if($usuarioAct['proyecto']['slug']!==$proyecto || $usuarioAct['identificador']!==$identificador || $usuarioAct['rol']!==$rol|| $usuarioAct['usuario']!==$usuario|| ($usuarioAct['type']!==$type&&$type!=$usuarioAct['rol'])){
					NegSesion::set('idexamencur', !empty($_GET['type'])?$_GET['type']:0);	
					parent::enrutar('sesion');
				}
				//NegSesion::set('idexamencur', !empty($_GET['type'])?$_GET['type']:0);
				
			}
			
			/* * * fin Editado eder.figueroa * * */
			parent::enrutar($rec, $ax);
			$documento =& JrInstancia::getDocumento();
			$documento->setTitulo(NegSesion::get('nombre'), true);
		}
 	}	
	public static function &getInstancia()
	{
		if(empty(self::$instancia)) {
			self::$instancia = new self;
		}
		return self::$instancia;
	}
	public function error($msj, $plantilla = null)
	{
		JrCargador::clase('sys_web::WebExcepcion', RUTA_SITIO, 'sys_web');
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->error($msj, $plantilla);
	}
	public function noencontrado()
	{
		JrCargador::clase('sys_web::WebExcepcion', RUTA_SITIO, 'sys_web');
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->noencontrado();
	}
}