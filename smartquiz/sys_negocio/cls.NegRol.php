<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		23-12-2016
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRol', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegRol 
{
	protected $idrol;
	protected $rol;
	
	protected $dataRoles;
	protected $oDatRol;	

	public function __construct()
	{
		$this->oDatRol = new DatRol;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRol->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatRol->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRol->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRol->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRol->get($this->idrol);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('roles', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatRol->iniciarTransaccion('neg_i_Roles');
			$this->idrol = $this->oDatRol->insertar($this->rol);
			//$this->oDatRol->terminarTransaccion('neg_i_Roles');	
			return $this->idrol;
		} catch(Exception $e) {	
		   //$this->oDatRol->cancelarTransaccion('neg_i_Roles');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('roles', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatRol->actualizar($this->idrol,$this->rol);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Roles', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatRol->eliminar($this->idrol);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrol($pk){
		try {
			$this->dataRoles = $this->oDatRol->get($pk);
			if(empty($this->dataRoles)) {
				throw new Exception(JrTexto::_("Roles").' '.JrTexto::_("not registered"));
			}
			$this->idrol = $this->dataRoles["idrol"];
			$this->rol = $this->dataRoles["rol"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('roles', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataRoles = $this->oDatRol->get($pk);
			if(empty($this->dataRoles)) {
				throw new Exception(JrTexto::_("Roles").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRol->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}