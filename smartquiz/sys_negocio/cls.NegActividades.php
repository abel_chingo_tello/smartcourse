<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-11-2016
 * @copyright	Copyright (C) 22-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatActividades', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegActividades 
{
	protected $idactividad;
	protected $nivel;
	protected $unidad;
	protected $sesion;
	protected $metodologia;
	protected $habilidad;
	protected $titulo;
	protected $descripcion;
	protected $estado;
	protected $url;
	protected $idioma;
	
	protected $dataActividades;
	protected $oDatActividades;	

	public function __construct()
	{
		$this->oDatActividades = new DatActividades;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatActividades->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatActividades->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatActividades->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatActividades->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatActividades->get($this->idactividad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			
			$this->idactividad = $this->oDatActividades->insertar($this->nivel,$this->unidad,$this->sesion,$this->metodologia,$this->habilidad,$this->titulo,$this->descripcion,$this->estado,$this->url,$this->idioma);

			return $this->idactividad;
		} catch(Exception $e) {	
		   //$this->oDatActividades->cancelarTransaccion('neg_i_Actividades');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
					
			return $this->oDatActividades->actualizar($this->idactividad,$this->nivel,$this->unidad,$this->sesion,$this->metodologia,$this->habilidad,$this->titulo,$this->descripcion,$this->estado,$this->url,$this->idioma);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Actividades', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatActividades->eliminar($this->idactividad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdactividad($pk){
		try {
			$this->dataActividades = $this->oDatActividades->get($pk);
			if(empty($this->dataActividades)) {
				throw new Exception(JrTexto::_("Actividades").' '.JrTexto::_("not registered"));
			}
			$this->idactividad = $this->dataActividades["idactividad"];
			$this->nivel = $this->dataActividades["nivel"];
			$this->unidad = $this->dataActividades["unidad"];
			$this->sesion = $this->dataActividades["sesion"];
			$this->metodologia = $this->dataActividades["metodologia"];
			$this->habilidad = $this->dataActividades["habilidad"];
			$this->titulo = $this->dataActividades["titulo"];
			$this->descripcion = $this->dataActividades["descripcion"];
			$this->estado = $this->dataActividades["estado"];
			$this->url = $this->dataActividades["url"];
			$this->idioma = $this->dataActividades["idioma"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('actividades', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataActividades = $this->oDatActividades->get($pk);
			if(empty($this->dataActividades)) {
				throw new Exception(JrTexto::_("Actividades").' '.JrTexto::_("not registered"));
			}

			return $this->oDatActividades->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}