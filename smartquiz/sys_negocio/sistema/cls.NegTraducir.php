<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		07-06-2016
 * @copyright	Copyright (C) 07-06-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTraducir', RUTA_SITIO, 'sys_datos');
JrCargador::clase('sys_negocio::sistema::NegTools', RUTA_SITIO, 'sys_negocio::sistema');
class NegTraducir 
{
	protected $id_traducir;
	protected $tabla;
	protected $idioma;
	protected $id_tabla;
	protected $campo;
	protected $valor;
	
	protected $dataTraducir;
	protected $oDatTraducir;	

	public function __construct()
	{
		$this->oDatTraducir = new DatTraducir;
	}
	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function __set($prop, $valor)
	{
		$this->set__($prop, $valor);
	}

	private function set__($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}
	
	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set__($prop_, $valor);
			}
		}
		
		$this->set__($prop, $valor);
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTraducir->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($idioma)
	{
		try {
			return $this->oDatTraducir->getNumRegistros($idioma);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($idioma = null)
	{
		try {
			return $this->oDatTraducir->buscar($idioma);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTraducir->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	/*public function getXid()
	{
		try {
			return $this->oDatTraducir->get($this->id_traducir);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}*/

	 public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('traducir', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatTraducir->iniciarTransaccion('neg_i_Traducir');
			$this->id_traducir = $this->oDatTraducir->insertar($this->tabla,$this->idioma,$this->id_tabla,$this->campo,$this->valor);
			$this->oDatTraducir->terminarTransaccion('neg_i_Traducir');	
			return $this->id_traducir;
		} catch(Exception $e) {	
		   $this->oDatTraducir->cancelarTransaccion('neg_i_Traducir');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('traducir', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatTraducir->iniciarTransaccion('neg_e_Traducir');
			$this->oDatTraducir->actualizar($this->id_traducir,$this->tabla,$this->idioma,$this->id_tabla,$this->campo,$this->valor);
			$this->oDatTraducir->terminarTransaccion('neg_e_Traducir');			
		} catch(Exception $e) {
		    $this->oDatTraducir->cancelarTransaccion('neg_e_Traducir');
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($pk)
	{
		try {
			if(!NegSesion::tiene_acceso('Traducir', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatTraducir->iniciarTransaccion('neg_d_Traducir');
			$data=$this->oDatTraducir->eliminar($pk);
			$this->oDatTraducir->terminarTransaccion('neg_d_Traducir');	
			return $data;
		} catch(Exception $e) {
			 $this->oDatTraducir->cancelarTransaccion('neg_d_Traducir');
			throw new Exception($e->getMessage());
		}
	}

	public function setId_traducir($pk){
		try {
			$this->dataTraducir = $this->oDatTraducir->get($pk);
			if(empty($this->banner_data)) {
				throw new Exception(JrTexto::_("Traducir").' '.JrTexto::_("not registered"));
			}
			$this->id_traducir = $this->dataTraducir["id_traducir"];
			$this->tabla = $this->dataTraducir["tabla"];
			$this->idioma = $this->dataTraducir["idioma"];
			$this->id_tabla = $this->dataTraducir["id_tabla"];
			$this->campo = $this->dataTraducir["campo"];
			$this->valor = $this->dataTraducir["valor"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}
	
	public function settabla($tabla)
	{
		try {
			$this->tabla= NegTools::validar('todo', $tabla, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	public function setidioma($idioma)
	{
		try {
			$this->idioma= NegTools::validar('todo', $idioma, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	public function setid_tabla($id_tabla)
	{
		try {
			$this->id_tabla= NegTools::validar('todo', $id_tabla, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	public function setcampo($campo)
	{
		try {
			$this->campo= NegTools::validar('todo', $campo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	public function setvalor($valor)
	{
		try {
			$this->valor= NegTools::validar('todo', $valor, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}