<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-12-2016
 * @copyright	Copyright (C) 27-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatResources', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegResources
{
	protected $idrecurso;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $idpersonal;
	protected $texto;
	protected $titulo;
	protected $caratula;
	protected $orden;
	protected $tipo;
	protected $publicado;
	protected $compartido;	
	protected $dataResources;
	protected $oDatResources;	


	public function __construct()
	{
		$this->oDatResources = new DatResources;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatResources->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatResources->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatResources->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatResources->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatResources->get($this->idrecurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {

			$res=null;
			if($this->orden==0){
				/*$res=$this->buscar(array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>$tool));-*/

				$res=$this->buscar(array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'idpersonal'=>$this->idpersonal,'tipo'=>$this->tipo));
			}else
			   $res=$this->buscar(array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'idpersonal'=>$this->idpersonal,'tipo'=>$this->tipo));


			if(empty($res[0])){

				$this->idrecurso = $this->oDatResources->insertar($this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->titulo,$this->texto,$this->caratula,$this->orden,$this->tipo,$this->publicado,$this->compartido);
			}else{
				$this->idrecurso=$res[0]["idrecurso"];
				$this->idrecurso =$this->oDatResources->actualizar($this->idrecurso,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->titulo,$this->texto,$this->caratula,$this->orden,$this->tipo,$this->publicado,$this->compartido);
			}

			return $this->idrecurso;
		} catch(Exception $e) {	
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {					
			return $this->oDatResources->actualizar($this->idrecurso,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->titulo,$this->texto,$this->caratula,$this->orden,$this->tipo,$this->publicado,$this->compartido);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {			
			return $this->oDatResources->eliminar($this->idrecurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtool($pk){
		try {
			$this->dataResources = $this->oDatResources->get($pk);
			if(empty($this->dataResources)) {
				throw new Exception(JrTexto::_("Resourse").' '.JrTexto::_("not registered"));
			}
			$this->idrecurso = $this->dataResources["idrecurso"];
			$this->idnivel = $this->dataResources["idnivel"];
			$this->idunidad = $this->dataResources["idunidad"];
			$this->idactividad = $this->dataResources["idactividad"];
			$this->idpersonal = $this->dataResources["idpersonal"];
			$this->texto = $this->dataResources["texto"];
			$this->orden = $this->dataResources["orden"];
			$this->tool = $this->dataResources["tool"];
			$this->publicado = $this->dataResources["publicado"];
			$this->compartido = $this->dataResources["compartido"];
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {	
			$this->dataResources = $this->oDatResources->get($pk);
			if(empty($this->dataResources)){
				throw new Exception(JrTexto::_("Resourse").' '.JrTexto::_("not registered"));
			}
			return $this->oDatResources->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}