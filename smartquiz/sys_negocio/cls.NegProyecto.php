<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		28-09-2017
 * @copyright	Copyright (C) 28-09-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatProyecto', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegProyecto 
{
	protected $idproyecto;
	protected $nombre;
	protected $comentario;
	protected $slug;
	protected $fecha_inicio;
	protected $fecha_fin;
	protected $estado;
	protected $idioma;
	
	protected $dataProyecto;
	protected $oDatProyecto;	

	public function __construct()
	{
		$this->oDatProyecto = new DatProyecto;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatProyecto->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function setUsarBD($bd_usar)
	{
		try {
			$this->oDatProyecto->setUsarBD($bd_usar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getUsarBD()
	{
		try {
			return $this->oDatProyecto->getUsarBD();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatProyecto->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatProyecto->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatProyecto->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatProyecto->get($this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXSlug()
	{
		try {
			return $this->oDatProyecto->getXSlug($this->slug);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			#$this->oDatProyecto->iniciarTransaccion('neg_i_Proyecto');
			if(empty($this->slug)){ $this->slug = $this->generarSlug(); }
			$this->idproyecto = $this->oDatProyecto->insertar(
				$this->nombre,
				$this->comentario,
				$this->slug,
				$this->fecha_inicio,
				$this->fecha_fin,
				$this->estado,
				$this->idioma
			);
			#$this->oDatProyecto->terminarTransaccion('neg_i_Proyecto');	
			return $this->idproyecto;
		} catch(Exception $e) {	
		    #$this->oDatProyecto->cancelarTransaccion('neg_i_Proyecto');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatProyecto->actualizar($this->idproyecto,$this->nombre,$this->comentario,$this->slug,$this->fecha_inicio,$this->fecha_fin,$this->estado,$this->idioma);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function generarSlug()
	{
		try {
			$this->slug = substr($this->nombre, 0, 9);
			$this->slug .= substr(md5($this->nombre), 0, 9);
			$this->slug .= date('Y');
			$search = $this->getXSlug();
			if(!empty($search)) {
				$cant = $this->getNumRegistros(array("slug_like"=>$this->slug));
				$num = $cant + 1;
				$this->slug .= $num;
			}
			return $this->slug;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Proyecto', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatProyecto->eliminar($this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdproyecto($pk){
		try {
			$this->dataProyecto = $this->oDatProyecto->get($pk);
			if(empty($this->dataProyecto)) {
				throw new Exception(JrTexto::_("Proyecto").' '.JrTexto::_("not registered"));
			}
			$this->idproyecto = $this->dataProyecto["idproyecto"];
			$this->nombre = $this->dataProyecto["nombre"];
			$this->comentario = $this->dataProyecto["comentario"];
			$this->slug = $this->dataProyecto["slug"];
			$this->fecha_inicio = $this->dataProyecto["fecha_inicio"];
			$this->fecha_fin = $this->dataProyecto["fecha_fin"];
			$this->estado = $this->dataProyecto["estado"];
			$this->idioma = $this->dataProyecto["idioma"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('proyecto', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataProyecto = $this->oDatProyecto->get($pk);
			if(empty($this->dataProyecto)) {
				throw new Exception(JrTexto::_("Proyecto").' '.JrTexto::_("not registered"));
			}

			return $this->oDatProyecto->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}