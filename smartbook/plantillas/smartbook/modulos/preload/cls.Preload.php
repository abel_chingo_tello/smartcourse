<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegProyecto',RUTA_BASE);
class Preload extends JrModulo
{
	private $oNegProyecto;
	public function __construct()
	{
		parent::__construct();	
		$this->oNegProyecto = new NegProyecto;	
		$this->modulo = 'preload';
	}
	
	public function mostrar($html=null)
	{
		try {
			$this->empresa = @NegSesion::get('empresa');
			$usuarioAct = NegSesion::getUsuario();			
			if(empty($this->empresa)){
				$empresa=array();
				$infoEmpresa = $this->oNegProyecto->buscar(array('idproyecto'=>$usuarioAct["idproyecto"]));	
				if(empty($infoEmpresa[0])) exit("empresa no existe");
				$this->empresa=$infoEmpresa[0];			
				$empresa["nombre"]=!empty($this->empresa["nombre"])?$this->empresa["nombre"]:@$this->empresa["empresa"];
				$filelogo=!empty($this->empresa["logo"])?$this->empresa["logo"]:$this->empresa["logoempresa"];
				if(!empty($filelogo)){
					$ipost=stripos($filelogo,'?');
					if($ipost===false) $filelogo=$filelogo;
					else $filelogo=substr($filelogo,0,$ipost);
				}				
				if(!empty($filelogo) && is_file(RUTA_BASE.$filelogo)){
					$ipost=stripos($filelogo,'?');
					if($ipost===false) $this->logoempresa=$filelogo;
					else $this->logoempresa=substr($filelogo,0,$ipost);
				}
				$empresa["logo"]=$this->logoempresa;
				NegSesion::set('empresa',$empresa);		
				$this->empresa=	$empresa;	
			}
			if(empty($this->empresa["logo"])){
				$this->empresa["logo"]='static/img/soporte.gif';
			}
			$this->esquema = 'preload'.(!empty($html)?('-'.$html):'');
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}