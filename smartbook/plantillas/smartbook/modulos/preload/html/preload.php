<link href="<?php echo $this->documento->getUrlTema() ?>/css/preload.css" rel="stylesheet">
<div id="loader-wrapper">
  <div id="loader">
    <div class="my-loader my-color-blue"></div>
    <!-- <img src="<?php //echo URL_BASE .$this->empresa["logo"]; 
                    ?>" class="img-fluid img-responsive" style="max-width: 300px; max-height: 250px"><br>
        <br><h2><?php //echo $this->empresa["nombre"]; 
                ?></h2></span> -->
  </div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>
<style>
  .my-loader {
    position: absolute;
    margin: auto;
    top: 10%;
    right: 75%;
  }
</style>
<script type="text/javascript">
  var __cargo = false;
  $(document).ready(function() {
    $(window).on("load", function() {
      closeLoading();
    })

    setTimeout(function() {
      if (__cargo == false) {
        closeLoading();
      }
    },5000);
  });
  function closeLoading() {
    $('#smartbook-ver').removeClass('smart-hide')
    $('body').addClass('loaded');
    $('h1').css('color', '#222222');
    __cargo = true;
  }
</script>