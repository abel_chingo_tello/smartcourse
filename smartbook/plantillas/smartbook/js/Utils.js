class Utils {
    constructor(){
        this._URLBASE = null
    }
    /**
     * Metodo para escapar en la ultima posición de la cadena de texto
     * @param {String} str Cadena de texto principal
     * @param {Char} char Caracter a escapar
     * @returns 
     */
    static rtrim(str,char) {
        return (str && char) ? str.replace(new RegExp("[" + char + "]*$"), '') : str.replace(/\s+$/g, '');
    }
    /**
     * Metodo para guardar bitacora del alumno
     * @param {Object} params 
     * @returns 
     */
    static guardarBitacora(params){
        if(this._URLBASE == null && params.URLBASE != null){    
            this._URLBASE = params.URLBASE
        }
        this._URLBASE = this.rtrim(this._URLBASE,"/");
        return $.ajax({
            url: this._URLBASE+'/smartbook/bitacora_smartbook/guardarBitacora',
            type: 'POST',
            dataType: 'json',
            data: params.dataSave,
            async : false,
        }).done(function(resp) {
            if(resp.code=='ok'){
                params.tabPane.attr('data-idbitacora', resp.newid);
            }
        }).fail(function(e) {
            console.log("!Error", e);
        });
    }
    /**
     * Metodo para registrar el historial de la sesion del usuario
     * @param {Object} params 
     * @returns 
     */
    static registrarHistorialSesion(params){
        if(this._URLBASE == null && params.URLBASE != null){ this._URLBASE = params.URLBASE }
        var idTabPane = params.idTabPane != null ? params.idTabPane : null
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        var type = function(t){
            switch(t){
                case '#div_games': { return 'G'; } break;
                case '#div_practice' : { return 'A';} break;
            }
            return 'TR';
        };
        
        var lugar = idTabPane == null ? 'TR' : type(idTabPane);
        
        if(params.Attention == null){
            params.Attention = "Attention";
        }
        
        this._URLBASE = this.rtrim(this._URLBASE,"/");

        if(lugar=='TR'){
            $.ajax({
                url: this._URLBASE +'/json/historial_sesion/guardarhistorialsesion',
                type: 'POST',
                dataType: 'json',
                data: {
                    'lugar': lugar,
                    'idcurso': params.idcurso_sc,
                    'idcc': params.idcc,
                    'idgrupoauladetalle':params.idgrupoauladetalle},
            }).done(function(resp) {
                if(resp.code=='ok'||resp.code==200){
                    if(idTabPane === null){
                        _IDHistorialSesion = resp.data.idhistorialsesion;
                    }else{
                        if(lugar === 'G'){
                            oIdHistorial.games = resp.data.idhistorialsesion; 
                        }else if(lugar === 'A'){
                            oIdHistorial.practice = resp.data.idhistorialsesion;
                        }else{
                            oIdHistorial.games = resp.data.idhistorialsesion;
                        }
                    }
                }else if(resp.code==201){
                    mostrar_notificacion(params.Attention, resp.Error, 'error');
                    redir('');
                }else {
                    mostrar_notificacion(params.Attention, resp.mensaje, 'error'); 
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
            });
        }//endif
        return 0;
    }
    static editarHistoriaSesion(params,id = null,idTabPane = null){
        if(this._URLBASE == null && params.URLBASE != null){ this._URLBASE = params.URLBASE }
        var _id = id != null ? id : 0 ;
        var type = function(t){
            switch(t){
                case '#div_games': { return 'G'; } break;
                case '#div_practice' : { return 'A';} break;
            }
            return 'TR';
        };
        var lugar = idTabPane == null ? 'TR' : type(idTabPane);
        
        this._URLBASE = this.rtrim(this._URLBASE,"/");

        if(lugar=='TR' && id>0){
            $.ajax({
                url: this._URLBASE+'/json/historial_sesion/guardarhistorialsesion',
                async: false,
                type: 'POST',
                dataType: 'json',
                data: {'idhistorialsesion': _id},
            }).done(function(resp) {
                if(resp.code=='ok'||resp.code==200){
                    if(id === null){
                        _IDHistorialSesion = resp.data.idhistorialsesion;
                    }else{
                        if(lugar === 'G'){
                            oIdHistorial.games = resp.data.idhistorialsesion; 
                        }else if(lugar === 'A'){
                            oIdHistorial.practice = resp.data.idhistorialsesion;
                        }else{
                            oIdHistorial.games = resp.data.idhistorialsesion;
                        } 
                    }
                }else if(resp.code==201){
                    mostrar_notificacion(params.Attention, resp.Error, 'error');
                    redir('');
    
                } else {
                    return false;
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
        }//end if
    }
    static initBitac_Alum_Smbook(params){   
        //console.log($('#idBitacoraAlumnoSmbook').val()); 
        if(this._URLBASE == null && params.URLBASE != null){ this._URLBASE = params.URLBASE }
        this._URLBASE = this.rtrim(this._URLBASE,"/");

        if( $('#idBitacoraAlumnoSmbook').val()!=='' ) { return false; }    
        
        $.ajax({
            url: this._URLBASE + '/smartbook/bitacora_alumno_smartbook/xGuardar',
            type: 'POST',
            dataType: 'json',
            data: {
                'txtIdcurso' : $('#idCurso').val(),
                'txtIdsesion' : $('#idSesion').val(),
                'idcurso_sc': params.idcurso_sc,
                'idcc':params.idcc,
                'idcursodetalle_sc':params.idcursodetalle_sc,
                'txtEstado' : 'P',
                'idgrupoauladetalle':params.idgrupoauladetalle
            },
        }).done(function(resp) {
            if(resp.code=='ok'){
                $('#idBitacoraAlumnoSmbook').val(resp.newid);
            }
        }).fail(function(err) {
            console.log("!Error", err);
        }).always(function() {});
    }
}