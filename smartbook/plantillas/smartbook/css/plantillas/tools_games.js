var asignarDimensionModal = function($elem_tool){
    var height = $(document).outerHeight();
    var idModal = $elem_tool.parents('.modal').attr('id');
    $('#'+idModal+' #modalcontent').css({
        'max-height': (height-180)+'px',
        'overflow' : 'auto',
    });

    $('#'+idModal).css('z-index', '1049');
    $('#'+idModal).siblings('.modal-backdrop').css('z-index', '1048');
};

var saveGame = function(_data,msjes){
    var data = new Array();
    data.idTool=_data.idgame;
    data.idNivel = _data.nivel;
    data.idUnidad = _data.unidad;
    data.idActividad = _data.actividad;
    data.texto = _data.texto;
    data.titulo = _data.titulo;
    data.descripcion = _data.descripcion;
    data.tool = 'G';
    var res = xajax__('', 'Tools', 'saveTools', data);
    if(res){ 
        IDGame = res; 
        padre=$(window.parent.document);
        padre.find('ul.listagames li.active').attr('data-idgame',res);
        mostrar_notificacion(msjes.attention, msjes.guardado_correcto, 'success');
        return res;
    }
};

var mostrarGame = function(mostrar, html){
    var html=html||'';
    if( mostrar ){
        if(html!=='') $('.games-main').html(html);
        $('.games-selector').hide('fast');
        $('.games-btns').show();
        $('.games-titulo-descripcion').show();
        return;
    }
    $('.games-btns').hide();
    $('.games-selector').show('fast');
    $('.games-main').html('');
    $('.games-titulo-descripcion').hide();
    return;
};


var handlerBtnGameSelector_click = function(e){
    e.preventDefault();
    var ruta = sitio_url_base+'/plantillas/actividad/';
    var tmp = $(this).data('template');

    $.get(ruta+tmp+'.php?met=-1', function(data) {
        mostrarGame(true, data);
    });
};

var handlerDiscardGame_click = function(e){
    e.preventDefault();
    var msje = e.data.msjes;
    $.confirm({
        title: msje.confirm_action,
        content: msje.are_you_sure,
        confirmButton: msje.accept,
        cancelButton: msje.cancel,
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
            if(IDGame!=''){
                var res = xajax__('', 'Game', 'eliminar', IDGame);
                if(res){
                    IDGame = '';
                }
            }
            mostrarGame(false);
        }
    });
};


var calcularPuntajeJuego = function(PTJE_MAX, $plantillaJuego){
    var PTJE_MAX=PTJE_MAX||100;
    var $plantillaJuego=$plantillaJuego||null;  
    var puntaje = 0.0;
    var cantTotal=1 , cantCorrectos=0;
    if($plantillaJuego!=null){
        if($plantillaJuego.hasClass('plantilla-sopaletras')){ 
            cantTotal = $plantillaJuego.find('.word').length;
            cantCorrectos = $plantillaJuego.find('.word.wordFound').length;
        }

        if($plantillaJuego.hasClass('plantilla-crucigrama')){
            cantTotal = $plantillaJuego.find('.crusigramalista').length;
            cantCorrectos = $plantillaJuego.find('.crusigramalista[style~="green"]').length;
        }

        if($plantillaJuego.hasClass('plantilla-rompecabezas')){
            cantTotal = $plantillaJuego.find('#pile .snappuzzle-piece').length;
            cantCorrectos = $plantillaJuego.find('#pile .snappuzzle-piece.correct').length;
        }
        puntaje = (cantCorrectos*PTJE_MAX)/cantTotal;
    }
    return puntaje.toFixed(2);
};


var reproducirAudio = function(idgui){
    var $audio = $('.audioimg_1'+idgui);
    var data_audio = $audio.data('audio');
    if(data_audio!='' && data_audio!=undefined){
        var src_audio = $audio.attr('src', _sysUrlStatic_+'/media/audio/'+data_audio );
        $audio.trigger('play');
    }
};

var recalcularTamanioImg = function($img){
    var width = $img.width();
    var height = $img.height();
    $img.attr({
        'data-width': width,
        'data-height': height
    });
};

var start_puzzle = function(puzzle_container, pnl_finish, img_src, id_pila, grid){
    $(pnl_finish).hide();
    $(puzzle_container).addClass('puzzle-started');

    var idgui = $(puzzle_container).closest('.plantilla').attr('data-idgui');
    $(img_src).snapPuzzle({
        rows: grid, columns: grid,
        pile: id_pila,
        containment: puzzle_container,
        onComplete: function(){
            $(img_src).fadeOut(100).fadeIn();
            $(pnl_finish).find('i.fa-undo').hide();
            $(pnl_finish).show();
            $(id_pila).hide();
            reproducirAudio(idgui);
            $('.botones-puzzle').hide();
        }
    });
    $(id_pila).height( $(img_src).height() );
};

var borrarNoPreview=function($plantillaJuego, $body){
    var $plantillaJuego=$plantillaJuego||null;
    var $body=$body||null;
    if($plantillaJuego==null || $body==null){return false;}
    var valRol = $body.find('#hRol').val();
    if( valRol!='' && valRol!=undefined ){
        if(valRol != 1){
            $plantillaJuego.find('.nopreview').remove();
        }
    }
};

var iniciarSopaLetras = function($plantillaJuego){
    var $plantillaJuego=$plantillaJuego||null;
    if($plantillaJuego==null){return false;}
    var arrWords=[];
    if( $plantillaJuego.find('#words').html()!='' ){
        $plantillaJuego.find('#words').find('li.word').each(function() {
            var word = $(this).text();
            arrWords.push(word);
        });

        var gamePuzzle = wordfindgame.create(arrWords, $plantillaJuego.find('#puzzle'), $plantillaJuego.find('#words'), {'gamewrapper':$plantillaJuego});
    }
};

var iniciarRompecabezas = function($plantillaJuego){
    var $plantillaJuego=$plantillaJuego||null;
    if($plantillaJuego==null){return false;}
    var idgui = $plantillaJuego.attr('data-idgui');
    var img_src = $plantillaJuego.find('.img_1'+idgui).attr('src');
    var now = Date.now();
    var idJuego= 'juego_'+idgui;
    $plantillaJuego.attr('id', idJuego);
    if(img_src.length>0){
        var container    = $plantillaJuego.find('#puzzle-containment');
        var img          = $plantillaJuego.find('.img_1'+idgui);
        var id_pnlFinish = $plantillaJuego.find('#panel_finish');
        var id_pila      = $plantillaJuego.find('#pile');
        var cant_grid    = $plantillaJuego.find('#panel_finish a.start-puzzle').data('grid');

        recalcularTamanioImg($plantillaJuego.find('.img_1'+idgui));
        $plantillaJuego.find(id_pila).html('');
        $plantillaJuego.find(id_pila).removeClass('snappuzzle-pile');

        $plantillaJuego.find(img).siblings('.snappuzzle-slot').remove();
        $plantillaJuego.find(img).unwrap();
        $plantillaJuego.find('#puzzle-containment').removeClass('puzzle-started');
        $plantillaJuego.find(id_pila).height($plantillaJuego.find(img).data('height'));
        start_puzzle( container, id_pnlFinish, img, id_pila, cant_grid );
    }
};

var iniciarCrucigrama = function($plantillaJuego){
    var $plantillaJuego=$plantillaJuego||null;
    if($plantillaJuego==null){return false;}
    /*var valRol = $('#hRol').val();
    if( valRol!='' && valRol!=undefined ){
        if(valRol != 1){
            $('.plantilla-crucigrama').find('.nopreview').remove();
        }
    }*/
};