var CLAVE_ACTIVA = null;
var COLOR_SELECC = null;
var arrColores = ['blue','red', 'green', 'pink', 'orange', 'skyblue', 'purple', 'aqua'];

var crearFicha = function($row, idCloneFrom, key, index){
    var image = $row.find('img').attr('src'),
    audio = $row.find('audio').attr('data-audio'),
    input = $row.find('input[type="text"]').val(),
    html = null;
    $row.find('input[type="text"]').attr('value', input);

    if( image!='' && image!=undefined ){
        if(html==null) html = $(idCloneFrom).clone();
        html.find('img').attr('src', image).removeClass('hide');
    } 
    if( audio!='' && audio!=undefined ) {
        if(html==null) html = $(idCloneFrom).clone();
        html.find('a').attr('data-audio', audio).removeClass('hide');
    } 
    if( input!='' && input!=undefined ){
        if(html==null) html = $(idCloneFrom).clone();
        html.find('p').text(input).removeAttr('class');
    }

    if(html!=null){
        html.removeClass('hidden').removeAttr('id');
        html.attr('data-key', key);
        html.attr('data-color', arrColores[index]);
        html.find('.hide').remove();
    }

    return html;
};

var generarFichas = function(idCloneFrom, idCloneTo){
    var $plantilla= $(idCloneFrom).parents('.plantilla-fichas');
    var $contendorFichas = $plantilla.find('._lista-fichas');
    var sFichaEdicion = '.ficha-edit';
    var rspta = false;
    $(idCloneTo).find('.partes-1').html('');
    $(idCloneTo).find('.partes-2').html('');
    $contendorFichas.find(sFichaEdicion).each(function(i, elem) {
        /* * * * Generacion de las PARTES-1 * * * */
        var $rowPrincipal=$(this).find('.part-1');
        var key = $(this).attr('id').split('_')[1];

        var html = crearFicha($rowPrincipal, idCloneFrom, key, i);

        if(html!=null){
            $(idCloneTo).find('.partes-1').append(html);
            rspta = true;
        } else {
            mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.debes_completar_fichas, 'error');
            $(idCloneTo).find('.partes-1').html('');
            $(idCloneTo).find('.partes-2').html('');
            rspta = false;
            return false;
        }

        /* * * * Generacion de las PARTES-2 * * * */
        $(this).find('.part-2 .ficha-row').each(function() {
            var html2 = crearFicha($(this), idCloneFrom, key);

            if(html2!=null) {
                /* Agregando nuevo Html en posic al azar */
                var $fichas_Part2 = $(idCloneTo).find('.partes-2').children();
                if( $fichas_Part2.length==0 ){
                    $(idCloneTo).find('.partes-2').append(html2);
                } else {
                    var posic = Math.floor(Math.random()*$fichas_Part2.length),
                    aleat = Math.floor(Math.random()*10);
                    if(aleat%2 == 0) { $fichas_Part2.eq(posic).before(html2); }
                    else { $fichas_Part2.eq(posic).after(html2); }
                }
                rspta = true;
            } else {
                mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.debes_completar_fichas, 'error');
                $(idCloneTo).find('.partes-1').html('');
                $(idCloneTo).find('.partes-2').html('');
                rspta = false
                return false;
            }
        });
        if(!rspta) return false;
    });

    return rspta;
};

var desmarcarIncorrectas = function( esDBY ,content ){    
    $('.plantilla-fichas',content).find('.ficha.active').each(function() {
        if( !$(this).hasClass('corregido good') ){ 
            $(this).removeClass('active');
            if(!esDBY){ $(this).removeClass('corregido bad'); }
        }
    });
};

var pausarTiempoFichas = function(){
  var $tmplActiva = getTmplActiva();
  var fichasTotal = $tmplActiva.find('.tpl_plantilla .partes-2 .ficha');
  var fichasCorregidas = $tmplActiva.find('.tpl_plantilla .partes-2 .ficha.corregido');
  if( fichasTotal.length==0 ){ console.log("Ther was an error pausing time in Fichas. Due to: '"+fichasTotal+"'"); return false; }
  if( fichasTotal.length === fichasCorregidas.length ){
    $('#panel-tiempo .info-show').trigger('oncropausar');
    $tmplActiva.find('.plantilla').addClass('tiempo-pausado');
  }
};

var corregirFicha = function( $ficha ){
    if( $ficha.hasClass('active') ){
        $ficha.addClass('corregido');
        var valor = $ficha.attr('data-key');
        if(CLAVE_ACTIVA==valor){ /* Match correcto */
            $ficha.removeClass('bad').addClass('good');
            var textoFicha = $ficha.text().trim();
            if(textoFicha.length>0){ pronunciar(textoFicha,false,'EN'); } /*pronunciacion.js*/
        } else { /* Match incorrecto */
            $ficha.removeClass('good').addClass('bad');
            if( esDBYself($ficha) ) restarPuntaje();
        }
    } else {
        $ficha.removeClass('corregido').removeClass('bad').removeClass('good');
    }
    if( esDBYself($ficha) ) pausarTiempoFichas();
};

var initFichas = function(IDGUI){
    if($('#lista-fichas'+IDGUI).html()!=undefined && $('#lista-fichas'+IDGUI).html().trim()=='') { 
        $('#tmp_'+IDGUI+'   .add-ficha').trigger('click'); 
    }
    mostrarBtnsDinamicos(IDGUI);
    $("*[data-tooltip=\"tooltip\"]").tooltip();
};