var reiniciarVoF = function($pnlPremisas){
    $pnlPremisas.find('input[type="radio"].radio-ctrl').prop('checked', false);
    $pnlPremisas.find('input[type="radio"].radio-ctrl').removeAttr('checked');
    $pnlPremisas.find('.icon-result').html('');

    $pnlPremisas.find('.valPremise').each(function() {
        var actual_value = $(this).val();
        var initial_value = $(this).attr('data-valueini');
        if(initial_value==undefined && actual_value != initial_value){
            var nopreview = $(this).attr('data-nopreview');
            $(nopreview).addClass('nopreview hidden');
        }
    });
};
var vistaEdicionVoF = function(){       
    $('.list-premises input[type="radio"].radio-ctrl').each(function() {
        /* volver a marcar las opciones en vista de edición*/
        var name = $(this).attr('name');
        var rspta_crrta = $('input#'+name).val();
        var lock = $('input#'+name).attr('data-lock');
        var value = $(this).val();
        var md5_value = $.md5(value, lock);
        if(md5_value===rspta_crrta){
            $(this).prop('checked', true);
        }
    });
    $('.icon-result').html('');
    $('.list-premises .premise.nopreview').removeClass('nopreview hidden');
};

var terminoEjercicio = function($contenedor){
    var $premisas = $contenedor.find('.premise');
    var rspta = true;
    if( !$('.preview').is(':visible') ){
        $premisas.each(function() {
            var radio = $(this).find('input[type="radio"].radio-ctrl:checked');
            if( radio.length == 0 ){
                rspta = false;
                return false;
            }
        });
    }
    return rspta;
};

var pausarTiempoVoF = function(){
    var $tmplActiva = getTmplActiva();
    var inputsTabActivo = $tmplActiva.find('.tpl_plantilla .premise');
    var inputsCorregidos = $tmplActiva.find('.tpl_plantilla .premise *[data-corregido]');

    if( inputsTabActivo.length === inputsCorregidos.length ){
        $('#panel-tiempo .info-show').trigger('oncropausar');
        $tmplActiva.find('.plantilla').addClass('tiempo-pausado');
    }
};

var asignarRsptaEdicion = function( $inpRadio,pnl ){
    var name = $inpRadio.attr('name');
    var marcada_value = $('input[name="'+name+'"].radio-ctrl:checked',pnl).val();
    var now = Date.now();
    var md5_rspta = $.md5(marcada_value, now);
    $('input#'+name,pnl).attr('data-lock', now);
    $('input#'+name,pnl).val(md5_rspta);
};

var evaluarRsptaVoF = function( $inpRadio,pnl ){
    var name = $inpRadio.attr('name');
    var marcada_value = $('input[name="'+name+'"].radio-ctrl:checked',pnl).val();
    var rspta_crrta = $('input#'+name,pnl).val();
    var lock = $('input#'+name,pnl).attr('data-lock');
    var rspta_marcada = $.md5(marcada_value, lock);
    var $icon_zone = $inpRadio.parents('.options').siblings('.icon-zone').find('.icon-result');
    
    if(rspta_marcada===rspta_crrta){
        var icono = '<i class="fa fa-check color-green2"></i>';
        var corregido = 'good';

        /* Premisa */
        var textoPremisa = $inpRadio.closest('.premise').find('div:first-child').text();
        if(textoPremisa.length>0){ pronunciar(textoPremisa.trim(),false,'EN');} /*pronunciacion.js*/
    } else {
        var icono = '<i class="fa fa-times color-red"></i>';
        var corregido = 'bad';
        if( esDBYself($inpRadio) ) restarPuntaje();
    }
    $($icon_zone,pnl).html(icono);
    $($icon_zone,pnl).attr('data-corregido', corregido);

    pausarTiempoVoF();
};

var reiniciarInpRadios = function($pnlPremisas){
    $pnlPremisas.find('input[type="radio"].radio-ctrl').prop('checked', false);
    $pnlPremisas.find('input[type="radio"].radio-ctrl').removeAttr('checked');
    $pnlPremisas.find('.icon-result').html('');

    $pnlPremisas.find('.valPremise').each(function() {
        var actual_value = $(this).val();
        var initial_value = $(this).attr('data-valueini');
        if(initial_value==undefined && actual_value != initial_value){
            var nopreview = $(this).attr('data-nopreview');
            $(nopreview).addClass('nopreview hidden');
        }
    });
};

var inputsRadCompletos = function($panel){
    var $inputsRad = $panel.find('input[type="radio"]');
    var rspta = true;
    $inputsRad.each(function() {
        var opcion_name = $(this).attr('name');
        if( $('input[name="'+opcion_name+'"]:checked').val()==undefined ){
            rspta = false;
            return false;
        }
    });
    return rspta;
};

var vistaEdicionVoF = function(){
    $('.list-premises input[type="radio"].radio-ctrl').each(function() {
        /* volver a marcar las opciones en vista de edición*/
        var name = $(this).attr('name');
        var rspta_crrta = $('input#'+name).val();
        var lock = $('input#'+name).attr('data-lock');
        var value = $(this).val();
        var md5_value = $.md5(value, lock);
        if(md5_value===rspta_crrta){
            $(this).prop('checked', true);
        }
    });
    $('.icon-result').removeAttr('data-corregido');
    $('.icon-result').html('');
};

var iniciarVerdadFalso = function(IDGUI){
    if( $('#pnl_premisas'+IDGUI).text().trim().length==0 ){
        $('#tmp_'+IDGUI+'   a.btn.add-premise').trigger('click');
    }
    mostrarBtnsDinamicos(IDGUI);
};