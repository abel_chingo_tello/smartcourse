var ANS= {}; /* global variable of answers in ORDENAR template, filled in initOrdenarSimple() */
var TIPO_DIVISION = 'letra';
var AYUDA_ORD= {};

var pausarTiempoOrdenar = function(){
  var $tmplActiva = getTmplActiva();
  var $plantillaActiva = $tmplActiva.find('.plantilla');
  if( $plantillaActiva.attr('data-tipo-tmp')=='ordenar_simple' ){
    var todos = '.tpl_plantilla .element';
    var corregidos = '.tpl_plantilla .element[data-corregido]';
  }
  if( $plantillaActiva.attr('data-tipo-tmp')=='ordenar_parrafo' ){
    var todos = '.tpl_plantilla .drop-parr>div';
    var corregidos = '.tpl_plantilla .drop-parr>div.finish';
  }
  var elemsTotal = $plantillaActiva.find(todos);
  var elemsCorregidas = $plantillaActiva.find(corregidos);
  if( elemsTotal.length === elemsCorregidas.length ){
    $('#panel-tiempo .info-show').trigger('oncropausar');
    $plantillaActiva.addClass('tiempo-pausado');
  }
};

var generarElementOrdenar = function(idCloneFrom, idCloneTo, IDGUI){
    var $contenedor = $('#lista-elem-edit'+IDGUI);
    var clsElem = '.elem-edit';
    var clsPartes = '.list-elem-part';
    $(idCloneTo).html('');

    $contenedor.find(clsElem).each(function() {
        var html = $(idCloneFrom).clone();
        var texto = $(this).find('input.txt-words').val().trim();
        if(texto!='' && texto!=undefined){
            var key = $(this).attr('id').split('_')[1];
            if( texto.split(' ').length > 1 ) { var clase = 'word'; } 
            else { var clase = 'letter'; }

            $(this).find('input.txt-words').attr('value', texto);

            /* Agregando las Partes a la zona Drag y a la Drop*/
            $(this).find(clsPartes).children('div').each(function() {
                if($(this).hasClass('fixed')){
                    /* Agregando PARTES fijas en DROP*/
                    html.find('.drop').append('<div class="parte fixed '+clase+'"><div>'+$(this).text()+'</div></div>');
                } else {
                    /* Agregando PARTES en DRAG en posic al azar*/
                    var $_dragParts = html.find('.drag').children();
                    if( $_dragParts.length == 0 ){
                        html.find('.drag').html('<div>'+$(this).text()+'</div>');
                    } else {
                        var posic = Math.floor(Math.random()*$_dragParts.length),
                        aleat = Math.floor(Math.random()*10);
                        if(aleat%2 == 0) { $_dragParts.eq(posic).before('<div>'+$(this).text()+'</div>'); }
                        else { $_dragParts.eq(posic).after('<div>'+$(this).text()+'</div>'); }
                    }

                    /* Agregando PARTES en blanco en DROP*/
                    html.find('.drop').append('<div class="parte blank '+clase+'"></div>');
                }
            });

            /* Agregando los contenidos multimedia */
            var imgSrc = $(this).find('img.hidden').attr('src');
            if( imgSrc!='' && imgSrc!=undefined ){
                html.find('.multimedia').removeClass('hide');
                html.find('img').attr('src', imgSrc);
                html.find('img').removeClass('hide');
            } 
            var audSrc = $(this).find('audio.hidden').attr('data-audio');
            if( audSrc!='' && audSrc!=undefined ){
                html.find('.multimedia').removeClass('hide');
                html.find('a').attr('data-audio', audSrc);
                html.find('a').removeClass('hide');
            } 

            html.removeClass('hidden').attr('id', 'e_'+key).attr({
                'id': 'e_'+key,
                'data-resp': texto
            });
            $(idCloneTo).append(html);
        }
    });
};

var evaluarFinal_simple = function($element , IDGUI){
    var id = $element.attr('id');
    var string = '' ;
    
    if( $element.find('.drop>div.parte').hasClass('letter') ){
        $element.find('.drop>div.parte').each(function() {
            string += $(this).text();
        });
        if( ANS[IDGUI][id]!=undefined ) var isMatch = ( ANS[IDGUI][id].toUpperCase() == string.toUpperCase() );
    }
    if( $element.find('.drop>div.parte').hasClass('word') ){
        $element.find('.drop>div.parte').each(function() {
            string += $(this).text()+' ';
        });
        if( ANS[IDGUI][id]!=undefined ) var isMatch = ( ANS[IDGUI][id] == string.trim() ) ; 
    }

    if( isMatch!=undefined ){
        if( isMatch ){
            $element.attr('data-corregido', 'good');
            $element.find('.drop').removeClass('bad').addClass('good');
            $element.find('.drop .parte.blank').removeClass('bad').addClass('good'); 

            /* Pronunciar */
            var arrPartes = [];
            $element.find('.drop.good .parte div').each(function(i, elem) {
                arrPartes.push($(elem).text());
            });
            var textoOrdenado = arrPartes.join(' ');
            if(textoOrdenado.length>0){ pronunciar(textoOrdenado,false,'EN');} /*pronunciacion.js*/

        } else {
            $element.attr('data-corregido', 'bad');
            $element.find('.drop').removeClass('good').addClass('bad');
            $element.find('.drop .parte.blank').removeClass('good').addClass('bad');
            if( esDBYself($element) ) restarPuntaje();
        }
    }

};

var evaluarUno_simple = function($element, IDGUI){
    var cantVacios = $element.find('.drop>div:empty').length;
    var id = $element.attr('id');
    var i=0;
    var rspta_completa='';
    if( ANS[IDGUI][id]!=undefined ) {
        rspta_completa = ANS[IDGUI][id];
        if( $element.find('.drop>.parte').hasClass('word') ) rspta_completa = rspta_completa.split(' ');
    }
    $element.find('.drop>.parte:empty').removeClass('bad').removeClass('good');
    $element.find('.drop>.parte').each(function() {
        var $this = $(this);
        if( $this.is(':empty') ){ return false; }
        if( !$this.hasClass('fixed') ){
            var parte = $(this).text();
            if( parte.toUpperCase()==rspta_completa[i].toUpperCase() ){
                $this.removeClass('bad').addClass('good');
            }else{
                $this.removeClass('good').addClass('bad');
            }
        }

        i++;
    });

    if( cantVacios==0 ){ evaluarFinal_simple($element, IDGUI); }

};

var evaluarOrdenCorrecto = function( $element, IDGUI ){
    var cantVacios = $element.find('.drop>div:empty').length;
    var isDBY = esDBYself( $element );
    var ayuda = AYUDA_ORD[IDGUI];

    if( !ayuda && cantVacios == 0 ) {
        evaluarFinal_simple($element, IDGUI);
    } else {
        $element.removeAttr('data-corregido');
        $element.find('.drop').removeClass('bad').removeClass('good');
    }
    if(ayuda){
        evaluarUno_simple($element, IDGUI);
    }

    pausarTiempoOrdenar();
};

var initOrdenarSimple = function(IDGUI, ayuda){
    ANS[IDGUI] = {};
    if(AYUDA_ORD[IDGUI]==undefined && ayuda!=undefined) { AYUDA_ORD[IDGUI] = ayuda; }
    var now = Date.now();
    $('#elem_0').attr('id', 'elem_'+now);
    $('a[data-url=".img_0"]').attr('data-url', '.img_'+now);
    $('img.img_0').removeClass('img_0').addClass('img_'+now);
    $('a[data-url=".audio_0"]').attr('data-url', '.audio_'+now);
    $('audio.audio_0').removeClass('audio_0').addClass('audio_'+now);

    if( !$('#lista-elem-edit'+IDGUI).is(':visible') || $('#lista-elem-edit'+IDGUI).length==0 ){
        $('#ejerc-ordenar'+IDGUI+' .element').each(function() {
            var resp = $(this).attr('data-resp');
            var id = $(this).attr('id');
            ANS[IDGUI][id] = resp;
            //if( !$('.btnsaveActividad').is(':visible') ) $(this).removeAttr('data-resp');
            /* si se borra el atributo 'data-resp'; el html resuelto del alumno
            * se guarda en BD sin el atrib. 'data-resp' y no podrá volver a cargarse en memoria 
            * el valor de este atrib. y por lo tanto no podrán realizar otra vez el ejercicio 
            * ya que no habrá palabra o frase con qué comparar */
        });

        $('#tmp_'+IDGUI+' .elem-edit .selectmedia').each(function() {
            lastBtnClicked = this;
            activarBtn();
        });
    }
};

var dividirCadena = function(value, $contenedor){
    var arr_value = null;
    var html = '';
    if( TIPO_DIVISION == 'letra' ){
        /*value = value.toUpperCase();*/
        var arr_value = value.split('');
    }

    if( TIPO_DIVISION == 'palabra' ){
        var arr_value = value.split(' ');
    }
    if(arr_value!=null){
        $contenedor.html('');
        $.each(arr_value, function(key, val) {
            if(val!=''){
                html = '<div>'+val+'</div>';
                $contenedor.append(html);
            }
        });
    }
};

var resetOrdenarSimple = function($pnlEjercicio, IDGUI){
    var $parents;
    $pnlEjercicio.find('.parte.blank').each(function() {
        var value= $(this).children('div');
        $parents = $(this).parents('.element');
        $parents.find('.drag').append(value);
    });
    evaluarOrdenCorrecto( $parents , IDGUI);
};

var ANS_P= {}; /* global variable of answers in ORDENAR PARRAFO template, filled in initOrdenarSimple() */
var AYUDA_P = {};

var initEditor=function(idTextArea, LANG){
    var LANG = LANG||'';
    tinymce.init({
        relative_urls : false,
        convert_newlines_to_brs : true,
        menubar: false,
        statusbar: false,
        verify_html : false,
        content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
        selector: '#'+idTextArea,
        height: 150,
        plugins:[""], 
        toolbar: 'undo redo | styleselect  | removeformat | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent '
    });
};

var saveEditor = function(idTextArea){
    tinyMCE.triggerSave();
    var texto=$('#'+idTextArea).val();
    $('#'+idTextArea).html(texto);
    tinymce.remove('#'+idTextArea);
};

var agregarRandom = function(strHtml, $aDonde, index){
    var $parteTexto = $aDonde.children();
    if( $parteTexto.length == 0 ){
        $aDonde.html(strHtml);
    } else {
        var posic = Math.floor(Math.random()*$parteTexto.length);
        while( posic == index ){
            posic = Math.floor(Math.random()*$parteTexto.length);
        }
        var aleat = ($parteTexto.length == 1) ? 2 : Math.floor(Math.random()*10);
        if(aleat%2 == 0) { $parteTexto.eq(posic).before(strHtml); }
        else { $parteTexto.eq(posic).after(strHtml); }
    }
};

var generarParrafo = function(idCloneFrom, idCloneTo, IDGUI){
    var $listParrafoEdit = $('#lista-parrafo-edit'+IDGUI),
        parrafoEdit = '.parrafo-edit';
    
    $(idCloneTo+' .drop-parr').html('');
    $(idCloneTo+' .drag-parr').html('');

    $listParrafoEdit.find(parrafoEdit).each(function() {
        var idTextArea = $(this).find('textarea').attr('id');
        saveEditor(idTextArea);
        var estaFijo = $(this).find('input[type="checkbox"]').is(':checked');
        if(estaFijo){ $(this).find('input[type="checkbox"]').attr('checked', 'checked'); }
        var texto = $('#'+idTextArea).val();
        var idParr = (idTextArea!='' && idTextArea!=undefined)? idTextArea.split('_')[idTextArea.split('_').length-1] : '';
        var index = $listParrafoEdit.find(parrafoEdit).index( $(this) );

        if( estaFijo ){
            $(idCloneTo+' .drop-parr').append('<div class="fixed">'+texto+'</div>');
        } else {
            $(idCloneTo+' .drop-parr').append('<div></div>');

            agregarRandom('<div id="'+idParr+'" data-orden="'+(index+1)+'">'+texto+'</div>', $(idCloneTo+' .drag-parr'), (index+1));
        }


    });
};

var evaluarUno = function($parrafo, IDGUI){
    var idParafo = $parrafo.attr('id');
    var index = $('#ejerc-ordenar'+IDGUI+' .drop-parr>div').index($parrafo);
    if( $parrafo.hasClass('filled') ){
        if( ANS_P[IDGUI][idParafo] == (index+1) ){
            $parrafo.addClass('good').removeClass('bad');
            if( esDBYself($parrafo) ) restarPuntaje();
        } else {
            $parrafo.addClass('bad').removeClass('good');
        }
    } else {
        $parrafo.removeClass('bad').removeClass('good');
    }
};

var evaluarFinal = function(IDGUI){
    $('#ejerc-ordenar'+IDGUI+' .drop-parr>div').each(function() {
        evaluarUno( $(this), IDGUI );
        $(this).addClass('finish');
    });
};

var evaluarOrdenParrafo = function( $parrafo, IDGUI ){
    var cantVacios = $('#ejerc-ordenar'+IDGUI+' .drop-parr>div:empty').length;
    var ayuda = AYUDA_P[IDGUI];
    if(!ayuda && cantVacios==0){
        evaluarFinal(IDGUI);

        /* Pronunciar */
        var $completados = $('#ejerc-ordenar'+IDGUI+' .drop-parr div.filled');
        var $correctos = $('#ejerc-ordenar'+IDGUI+' .drop-parr div.filled.good');
        if($completados.length==$correctos.length){
            var arrPartes = [];
            $completados.children().each(function(i, elem) {
                arrPartes.push($(elem).text()+' ');
            });
            var textoOrdenado = arrPartes.join('. ');
            if(textoOrdenado.length>0){ pronunciar(textoOrdenado);} /*pronunciacion.js*/
        }
    } 
    if(ayuda){
        evaluarUno($parrafo, IDGUI);
    }
    pausarTiempoOrdenar();
};

var iniciarTodosEditores = function($contenedor, LANG){
    var LANG = LANG||'';
    $contenedor.find('textarea').each(function() {
        var idTextArea = $(this).attr('id');
        initEditor(idTextArea , LANG);
    });
}

var initOrdenarParrafos = function(IDGUI, LANG, ayuda){
    var LANG = LANG||'';
    ANS_P[IDGUI] = {};
    if(AYUDA_P[IDGUI]==undefined && ayuda!=undefined) { AYUDA_P[IDGUI] = ayuda; }
    var now = Date.now();
    $('#parrafo_0').attr('id', 'parrafo_'+now);
    $('#txt_parrafo_0').attr('id', 'txt_parrafo_'+now);
    if( $('#lista-parrafo-edit'+IDGUI).is(':visible') ){
        $contenedorEdit = $('#tmp_'+IDGUI+' #lista-parrafo-edit'+IDGUI);
        if( $contenedorEdit.find('textarea').length>0 ) {
            iniciarTodosEditores($contenedorEdit);
        }
    }

    if( !$('#lista-parrafo-edit'+IDGUI).is(':visible') || $('#lista-parrafo-edit'+IDGUI).length==0 ){
        $('#ejerc-ordenar'+IDGUI+' .drag-parr>div').each(function() {
            var idParr = $(this).attr('id');
            var orden = $(this).attr('data-orden');
            if(idParr!=undefined)ANS_P[IDGUI][idParr] = orden;
            //if( !$('.btnsaveActividad').is(':visible') ) $(this).removeAttr('data-orden');
        });
        $('#ejerc-ordenar'+IDGUI+' .drop-parr>div').each(function() {
            var idParr = $(this).attr('id');
            var orden = $(this).attr('data-orden');
            if(idParr!=undefined)ANS_P[IDGUI][idParr] = orden;
            //if( !$('.btnsaveActividad').is(':visible') ) $(this).removeAttr('data-orden');
        });
    }
};

var resetOrdenarParrafo = function($pnlEjercicio, IDGUI){
    $pnlEjercicio.find('.drop-parr>div.filled').each(function() {
        var $this = $(this);
        var value= $this.html();
        var id = $this.attr('id');
        var orden=$this.attr('data-orden');
        var $ejercOrdenar = $this.parents('#ejerc-ordenar'+IDGUI);
        $ejercOrdenar.find('.drag-parr').append('<div id="'+id+'" data-orden="'+orden+'">'+value+'</div>');
        $this.html('').removeAttr('class').removeAttr('id');
        evaluarOrdenParrafo( $this, IDGUI);
    });
};