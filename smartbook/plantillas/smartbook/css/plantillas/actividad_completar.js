
/*var HOSTNAME = window.location.hostname;
var PROTOCOL = window.location.protocol;
var URL_BASE = PROTOCOL+'//'+HOSTNAME;*/
var dividirPaneles = function(valor, IDGUI){
  var tpltxt=$('#tpl'+IDGUI).find('#txt'+IDGUI);
  var tplalter=$('#tpl'+IDGUI).find('#alter'+IDGUI);
  if(valor){
    tpltxt.removeClass().addClass('col-md-6 col-sm-7 col-xs-12');
    tplalter.removeClass().addClass('col-md-6 col-sm-5 col-xs-12');
  }else{
    tpltxt.removeClass().addClass('col-md-12 col-sm-12 col-xs-12');
    tplalter.removeClass().addClass('col-md-12 col-sm-12 col-xs-12');
  }
};

var edithtml=function(IDGUI){
 $('#tpl'+IDGUI).hide();
 $('#txtarea'+IDGUI).show();
 $('#btnedithtml'+IDGUI).addClass('disabled');
 $('#btnsavehtml'+IDGUI).removeClass('disabled');
 tinymce.init({
  relative_urls : false,
  convert_newlines_to_brs : true,
  menubar: false,
  statusbar: false,
  verify_html : false,
  content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
  selector: '#txtarea'+IDGUI,
  height: 400,
  plugins:["chingosave chingoinput chingoimage chingoaudio chingovideo textcolor advlist table" ], 
  toolbar: 'chingosave | undo redo chingodistribution | advlist table | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor | chingoinput chingoimage chingoaudio chingovideo '
});
};

var removerTodosFlotantes = function(idPanel){
  var $inputsMCE = $(idPanel).find('input.open');
  $(idPanel).find('.helper-zone').remove();
  $(idPanel).find('ul.options-list').remove();
  var $inputsMCE = $(idPanel).find('input[data-mce-object="input"]');
  $inputsMCE.each(function() {
    if( $(this).parents().hasClass('input-flotantes wrapper') ){
      try{
      $(this).parents('.input-flotantes.wrapper').children().unwrap();
      }catch(ex){}
    }
  });
};

var generarID = function(inputMCE){
  var now = Date.now();
  var id = 'input_'+now;
  $(inputMCE).attr('id', id);
  return id;
};

var prepararInput = function(inputMCE, id){
  if(!$(inputMCE).parents().hasClass('input-flotantes wrapper')){
    $(inputMCE).wrap('<div class="input-flotantes wrapper" id="wrapper-'+id+'"></div>')
  }
  if(isMobile.getBrowserInfo().navegador == 'Safari'){
    setTimeout(function(){$(inputMCE).focus();},100);
  }
}

var calcularPosicion = function(inputMCE, ubicacion){
  var ubicacion=ubicacion||false;
  var estilos = '';
  var posicion = $(inputMCE).position();
  var ancho = $(inputMCE).outerWidth();
  estilos += ' width:'+ancho+'px; ';
  estilos += ' min-width: 150px; ';
  estilos += ' left:'+posicion.left+'px; ';
  if( ubicacion == 'up' ){
    var top = $(inputMCE).outerHeight() * (-1);
    estilos += ' top: '+top+'px; ' ;
  }
  return estilos;
};

var desordenarRspta = function(contenedor, str_elem){
  var listado = $(contenedor).children();
  if(listado.length==0){
    $(contenedor).append(str_elem);
  } else {
    var posic = $(contenedor).siblings('input').data('num');

    if(posic==''||posic==undefined){
      posic = Math.floor(Math.random()*listado.length);
      $(contenedor).siblings('input').attr('data-num',posic);
    }

    var aleat = Math.floor(Math.random()*10);
    if(aleat%2 == 0) listado.eq(posic).before(str_elem);
    else listado.eq(posic).after(str_elem);
  }
};

var asignarLetrasAlt = function(idContenedor){
  var elem = $(idContenedor).children();
  var ascii = 97; // ascci for 'a'
  elem.each(function(){
    $(this).children().before('<span class="alt-letra">&#'+ascii+';) </span>')
    ascii++;
  });
};

var desplegarAyuda = function(inputMCE, id){
  var styles = calcularPosicion(inputMCE, 'up');
  var html = '<div class="helper-zone" id="help-'+id+'" style="'+styles+'">'+
  '<a href="#" class="btn-help" style="padding:5px 0px!important;"><i class="fa fa-question-circle"></i>&nbsp;Help!</a>'+
  '</div>';
  $(inputMCE).before(html);
};

var desplegarOpciones = function(inputMCE, id){
  var id = $(inputMCE).attr('id');
  var opc = $(inputMCE).attr('data-options');
  var opc_corr = $(inputMCE).attr('data-texto');
  var list = '';
  if(opc!='' && opc!=undefined){
    var arr_opc = _isJson(opc)?JSON.parse(opc):opc.split(',');
    $.each(arr_opc, function(key, val) {
      list += '<li class="opt-item"><a href="#">'+val.trim()+'</a></li>';
    });
  }
  var styles = calcularPosicion(inputMCE, 'down');
  var html_opt = '<ul class="options-list" id="opts-'+id+'" style="'+styles+'">'+ list + '</ul>';
  $(inputMCE).after(html_opt);
  
  var correcta = '<li class="opt-item"><a href="#">'+opc_corr+'</a></li>';
  desordenarRspta('#opts-'+id, correcta);
};

var nuevaAlternativa = function(arrInputOptions, cantInputs){
  var arr_temporal = [];
  //console.log(arrInputOptions);
  //console.log(cantInputs);
  for (var i = 0; i < cantInputs; i++) { //<--- ¡¡¡ ERROR EN MOZILLA FIREFOX !!!
    if(arrInputOptions[i]===undefined){ console.log('Bucle interrumpido'); break; }
    var rand = Math.floor(Math.random() * arrInputOptions[i].length);
    arr_temporal.push( arrInputOptions[i][rand].trim() );
    var new_alternativa = arr_temporal.join(' - ');
  }
  return new_alternativa;
};

var mezclarOpciones = function(arrInputOptions, cantInputs){
  var array_Alts = [];
  var cont_aux = 0;
  var cantidad_alternativas = 3; //se considera de la a)-d) descontando la correcta
  for (var x = 0; x < cantidad_alternativas; x++) {
    if(cont_aux==15){ console.log('interrumpido Bucle'); break; }
    var new_alternativa = nuevaAlternativa(arrInputOptions, cantInputs);
    if(array_Alts.length>0){
      if( $.inArray(new_alternativa, array_Alts)>-1 ){
        x--;
      } else {
        array_Alts.push(new_alternativa);
      }
    } else {
      array_Alts.push(new_alternativa);
    }
    cont_aux++;
  }
  return array_Alts;
};

var desplegarAlternativas = function(IDGUI){
  var opc_corr = '';
  var arr_opc_corr = [];
  var arr_InputOptions = [];
  var inpClicked = $('#pnl_edithtml'+IDGUI).find('input.isclicked[data-mce-object="input"]');
  if(inpClicked.length>0){
    var arr_options2 = [];
    inpClicked.each(function() {
      var optionstmp=jsonmultiplantilla[$(this).attr('id')];
      optionstmp=optionstmp||{al:[],ok:[]};
      var optionsal=optionstmp.al||[];
      var optionsok=optionstmp.ok||'';

      arr_opc_corr.push(optionsok);

      var arr_options = [];
      arr_options2 = optionsal;
      var options = optionsal;
      if(options.length>0){
        arr_InputOptions.push(options);
      }
    });

    var rspta_correcta = arr_opc_corr.join(' - ');
    if(inpClicked.length==1) {  var arr_alternativas = arr_options2; }
    else if(arr_InputOptions.length>0){var arr_alternativas = mezclarOpciones(arr_InputOptions,inpClicked.length);}

    var list = '';
    $.each(arr_alternativas, function(key, val) {
      list += '<li class="alt-item"><a href="#">'+val.trim()+'</a></li>';
    });
    var html_alt = '<ul class="alternativas-list">'+ list + '</ul>';
    $('#pnl_editalternatives'+IDGUI).html(html_alt);
    
    var correcta = '<li class="alt-item"><a href="#">'+rspta_correcta+'</a></li>';
    desordenarRspta('#pnl_editalternatives'+IDGUI+' .alternativas-list', correcta);
    asignarLetrasAlt('#pnl_editalternatives'+IDGUI+' .alternativas-list');
  }
};

var playRsptaCorrecta = function(IDGUI, URL_MEDIA, audioSRC){
  var IDGUI = IDGUI||'';
  var URL_MEDIA = URL_MEDIA||'';
  var audioSRC = audioSRC||'';
  if(IDGUI!='' && URL_MEDIA!='' &&  audioSRC!=''){
    var ruta= URL_MEDIA+'/media/audio/',
        $audio = $('#aud_completar'+IDGUI);
    $audio.attr('src', ruta+audioSRC);
    $audio.trigger('play');
  }
};

var agregarIcon = function(input, valor){
  var valor = valor||false;
  $(input).parents('.inp-corregido.wrapper').find('span.inner-icon').remove();
  if(valor === 'good'){
    var clsIcon = 'fa fa-check color-green2';
  } else {
    var clsIcon = 'fa fa-times color-red';
  }
  if(!$(input).parents('.wrapper').hasClass('inp-corregido')){
    if( $(input).parent('.wrapper').hasClass('input-flotantes') ){
      $(input).parent('.input-flotantes.wrapper').wrap('<div class="inp-corregido wrapper"></div>');
    } else {
      $(input).wrap('<div class="inp-corregido wrapper"></div>');
    }
  }

  $(input).parents('.inp-corregido.wrapper').attr('data-corregido',valor);
  $(input).parents('.inp-corregido.wrapper').append('<span class="inner-icon"><i class="'+clsIcon+'"></i></span>');
};

var pausarTiempoMultiplantilla = function(){
  var $tmplActiva = getTmplActiva();
  var inputsTabActivo = $tmplActiva.find('.tpl_plantilla .panelEjercicio input');
  var inputsCorregidos = $tmplActiva.find('.tpl_plantilla .panelEjercicio .inp-corregido');
  if( inputsTabActivo.length === inputsCorregidos.length ){
    $('#panel-tiempo .info-show').trigger('oncropausar');
    $tmplActiva.find('.plantilla').addClass('tiempo-pausado');
  }
};

var restarPuntaje = function(){
  var $pnlPuntaje = $('#panel-puntaje');
  var puntaje = $pnlPuntaje.find('.info-show').text();
};

var esDBYself = function($elem){
  /*var id_contenedor = $elem.closest('.actividad-main-content').children('div.tab-pane.active').attr('id');*/
  var id_contenedor = $elem.closest('.metod.active').attr('id');
  if(id_contenedor==="met-3"){ return true; }
  else{ return false; }
};

var evaluarRespuesta = function(inputMCE){
  if ( esDBYself($(inputMCE)) && $(inputMCE).closest('.wrapper').hasClass('inp-corregido') ) return false;

  var value = $(inputMCE).val().trim();
  var rspta_crrta = $(inputMCE).attr('data-texto');
  var srcaudio=$(inputMCE).attr('data-audio');
  if(value.length>0){
    var $imgs = $('.metod.active .tabhijo.active .plantilla img[data-mce-object="image"]');
    /*if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){*/
    if(rspta_crrta.trim() === value.trim()){
      var valor = 'good';
      if(srcaudio!=undefined&&srcaudio!=''){
          srcaudio=srcaudio.trim();
          srcaudio=_sysUrlBase_+'/static/media/audio/'+srcaudio;
          $('#curaudio').attr('src',srcaudio);
          $('#curaudio').trigger('play');
      }else{
        /* //tratando de extraer toda la frase para pronunciación:
        var $parrafo = $(inputMCE).closest('p');
        $parrafo.find('div.inp-corregido').replaceWith('<span>'+value+'</span>');
        console.log($parrafo);
        console.log(value);
        console.log($parrafo.text());*/
        pronunciar(value,false,'EN'); /*pronunciacion.js*/
      }
      if($imgs.length>0){ $imgs.removeClass('gray-scale'); }     
    }else{
      var valor = 'bad';
      if($imgs.length>0){ $imgs.addClass('gray-scale'); }
      if( esDBYself($(inputMCE)) ) restarPuntaje();
    }
    $(inputMCE).attr('value', value);
    agregarIcon(inputMCE, valor); /* si se desea, se puede Condicionar esta linea con la funcion esDBYself(input); , si es que no se desea mostrar icono en la metodol D.B.Y.*/
    pausarTiempoMultiplantilla();
    return valor;
  }
};

var evaluarRsptaAltern = function(inputMCE, IDGUI,cont){
  var seleccionada = $(inputMCE).text();
  var $inpClicked = $('#pnl_edithtml'+IDGUI,cont).find('input.isclicked.mce-object-input');
  var imgs = $('#pnl_edithtml'+IDGUI,cont).find('img[data-mce-object="image"]');
  var arr_opc_corr = [];
  if($inpClicked.length>0){
    $inpClicked.each(function() {
      var opc_corr = $(this).attr('data-texto');
      arr_opc_corr.push(opc_corr);
    });
  }
  var rspta_crrta = arr_opc_corr.join(' - ');
  if(rspta_crrta === seleccionada){
    var valor = 'good';
    if(imgs.length>0){ imgs.removeClass('gray-scale'); }
  } else {
    var valor = 'bad';
    if(imgs.length>0){ imgs.addClass('gray-scale'); }
    if( esDBYself($(inputMCE)) ) {
      restarPuntaje();
    }
  }
  agregarRsptaInput($('#pnl_edithtml'+IDGUI,cont), seleccionada);
  agregarIcon(inputMCE, valor); /* si se desea, se puede Condicionar esta linea con la funcion esDBYself(input); , si es que no se desea mostrar icono en la metodol D.B.Y.*/
  pausarTiempoMultiplantilla();
};

var crearTagAudio = function(IDGUI){
  var $inputsAudio = $('#pnl_edithtml'+IDGUI).find('input.mce-object-input[data-audio]');
  if( $inputsAudio.length>0 ){
    var html = '<audio src="" class="hidden" id="aud_completar'+IDGUI+'" style="display: none;"></audio>';
    $inputsAudio.parents('.plantilla').append(html);
  }
};

var reiniciarIntentos = function(){
  var $nro_intento = $('#met-3 .tab-content').find('.intentos>.nro-intento');
  $nro_intento.each(function() {
    if( !$(this).hasClass('active') ){
      $(this).addClass('active');
    }
  });
  if($('.btn.try-again').hasClass('disabled')) {
    $('.btn.try-again').removeClass('disabled');
  }
}

var habilitarElem = function(elem, valor){
  var attr = $(elem).attr('readonly');
  if( valor ){
    if (typeof attr != typeof undefined && attr != false) {
      $(elem).removeAttr('readonly');
    }
  } else {
    if (typeof attr == typeof undefined || attr == false) {
      $(elem).attr('readonly', 'readonly');
    }
  }

}

var cerrarFlotantes = function(inputMCE){
  if($(inputMCE).val() == ''){ 
    if($(inputMCE).closest(".plantilla-completar").find('.nopreview').length == 0 && $(inputMCE).closest(".panelEjercicio").find("#"+$(inputMCE).data("nube")).length == 0){
      var padre = $(inputMCE).parent();
      padre.append("<span class='nubeclick' id='"+$(inputMCE).data("nube")+"'>Click aquí</span>");
      console.log(inputMCE,$(inputMCE));
      padre.find("#"+$(inputMCE).data("nube")).css({ top:($(inputMCE).position().top + 23.5).toString() + "px", "left": $(inputMCE).position().left.toString() + "px" });
    }
    $(inputMCE).closest(".panelEjercicio").find("#"+$(inputMCE).data("nube")).show();
  }
  if($(inputMCE).parent().hasClass('input-flotantes wrapper')) {
    var target = $(inputMCE);
  } else if($(inputMCE).parent().parent().hasClass('input-flotantes wrapper')){
    var target = $(inputMCE).parent();
  }
  if(target!=undefined){
    try{
    target.siblings().remove();
    target.unwrap();
    }catch(ex){}
  }
  $(inputMCE).removeClass('open');
};

var reiniciarInputsMCE = function(plantilla){
  var plantilla=plantilla||$('.actividad-main-content');
  var inputsMCE = plantilla.find('input[data-mce-object="input"]');
  
  inputsMCE.each(function(){
    $(this).val('');
    $(this).attr('value','');
    $(this).removeClass('_success_');
    cerrarFlotantes(this);
    if($(this).parent().hasClass('wrapper')){
      $(this).siblings().remove();
      $(this).unwrap();
    }
    if( $(this).hasClass('open') ) $(this).removeClass('open');
    if( $(this).hasClass('isclicked') ) {
      var altern = $(this).parents('div.panel').parent().parent().siblings().find('.inp-corregido.wrapper>a');
      altern.each(function() {
        while( !$(this).parent().is('li.alt-item') ){
          $(this).siblings().remove();
          $(this).unwrap();
        }
      });
    }
    habilitarElem(this, true);
  });

  var $imgs = $('.tpl_plantilla .panelEjercicio').find('img[data-mce-object="image"]');
    if($imgs.length>0){
        $imgs.addClass('gray-scale');
    }
};

var activarTabsEjercicios = function(valor){
  var $contenTabs = $('#met-3 ul.nav-tabs.ejercicios');
  if(!valor){
    $contenTabs.find('li:first-child a').trigger('click');
    if( !$contenTabs.find('li').hasClass('disabled') ) { 
      $contenTabs.find('li').addClass('disabled');
    }
  }else{
    if( $contenTabs.find('li').hasClass('disabled') ) { 
      $contenTabs.find('li').removeClass('disabled');
    }
  }
};

var previewDBYself = function(){
  activarTabsEjercicios(false);
  reiniciarInputsMCE();
  reiniciarIntentos();
}

var backDBY = function(){
  activarTabsEjercicios(true);
  reiniciarInputsMCE();
};

var inputsCompletos = function( $panel ){
  var inputsMCE = $panel.find('input[data-mce-object="input"]');
  var rspta = true;
  inputsMCE.each(function() {
    if( $(this).val().length==0 ){ 
      rspta=false; 
      return false; // interrumpir loop $.each();
    }
  });
  return rspta;
};

var inputsCompletos_minimoUno = function($panel){
  var inputsMCE = $panel.find('input[data-mce-object="input"]');
  var rspta = false;
  inputsMCE.each(function() {
    if( $(this).val().length>0 ){
      rspta=true;
      return false; // interrumpir loop $.each();
    }
  });
  return rspta;
}

var inputMCE_Completo = function( $inputMCE ){
  var value = $inputMCE.val();
  if(value.length===0){ return false; } 
  else { return true; }
};

var guardarProgreso = function(){  
  var rol=$('#rolUsuario').val()||'';
  if( rol.toLowerCase() == 'administrador' || rol==1 ){ console.log('Administrador no guarda su avance'); return false; }
  var id_metodologia = $('ul.nav.metodologias li.active>a').attr('href');
  var $navEjercicios = $(id_metodologia).find('ul.nav.nav-tabs.ejercicios');
  var nro_ejercicio = '';
  if( $navEjercicios.length>0 ){
    nro_ejercicio = $navEjercicios.find('li.active>a').text();
    var id_Ejercicio = $navEjercicios.find('li.active>a').attr('href');
  }
 

  
  var iddetalle = $(id_Ejercicio).attr('data-iddetalle');
  var avance = $(id_Ejercicio).attr('data-progreso') || 0.00;
  var habilidades = $(id_Ejercicio).find('input.selected-skills').val();

  $(id_Ejercicio).find('textarea.txtareaedit').remove();
  $(id_Ejercicio).find('input').each(function(i,v){
    var value=$(this).val();
    $(this).val(value);
    $(this).attr('value',value);
  })
  savetemplate_nlsw($(id_Ejercicio));

  var html_solucion = $(id_Ejercicio).html()||''
  if(html_solucion!='') html_solucion=html_solucion.replace(/(\r\n\t|\n|\r\t)/gm,"").trim();
  html_solucion=html_solucion.trim().replace(/\s+/gi,' ').replace(/  /,' ');
  var cantTotalEjerc = $navEjercicios.find('li').length;
  var ptjexEjerc = 100/cantTotalEjerc;
  var progreso=avance*100/ptjexEjerc;
  var estado = 'P'; /* pendiente */
  var $tmplActiva = $(id_Ejercicio).find('.aquicargaplantilla .plantilla');
  if(id_metodologia=='#div_autoevaluar'){
    totalintentos=parseInt($('#panel-intentos-dby').find('.total').text());
    actualintento=parseInt($(id_Ejercicio).find('.plantilla').attr('data-intento')||1);
    if(actualintento>=totalintentos) estado='T';    
    if(!$tmplActiva.hasClass('yalointento'))$tmplActiva.addClass('yalointento');    
  }
  if(progreso>=100) {
    progreso=100.0;
    estado = 'T'; /* terminado */
  }
  if (typeof iddetalle=="undefined" || iddetalle=="") { console.log('falta iddetalle guardarprogreso'); return false; }
  var tipofile='';
  var file='';
  if($tmplActiva.hasClass('plantilla-nlsw')){
    var grabarfiletmp=$tmplActiva.find('.grabarfile');
    tipofile=grabarfiletmp.attr('data-tipo');
    if(tipofile=='texto') file=grabarfiletmp.val()||'';
    else if(tipofile=='audio'){
      var nombreaudio=grabarfiletmp.children('audio').attr('src')||'';
      file=nombreaudio.replace(_sysUrlBase_,'');
    }

  }
  //console.log(file,tipofile);

  $.ajax({
      url: _sysUrlBase_+'/actividad_alumno/ajax_agregar',
      type: 'POST',
      dataType: 'json',
      async: false,
      data: {
        'iddetalle_actividad' : iddetalle,
        'estado' : estado,
        'progreso' : progreso,
        'habilidades' : habilidades,
        'html_solucion' : html_solucion,
        'file':file,
        'tipofile':tipofile
      },
    }).done(function(resp) {// console.log("Success");
      if(resp.code=='ok'){
        //console.log(resp.data);
      }else{
        mostrar_notificacion(MSJES_PHP.attention, resp.mensaje, 'error');
      }
    }).fail(function(err) {
      console.log("Error: ",err);
    });
};

var generarHTMLIntentos = function(IDGUI, params){
  var label_attempt = params.attempt;
  var try_again = params.try_again;
  var $pnl_intentos = $('#pnl_intentos'+IDGUI);
  $pnl_intentos.html('');
  var cantIntentos = $pnl_intentos.data('intentos');
  var html_inten = '';
  for (var i = 1; i <= cantIntentos; i++) {
    html_inten += '<div class="nro-intento active">'+i+'</div>';
  }
  var html = '<div class="row" >'+
  '<div class="col-sm-offset-6 col-sm-6 col-xs-12 pull-right intentos">'+
  '<div class="descripcion capitalize">'+label_attempt+'s: </div>'+
  html_inten+
  '</div>'+
  '<div class="col-sm-offset-6 col-sm-6 col-xs-12 pull-right boton-intentar">'+
  '<a class="btn btn-inline btn-default btn-xs try-again"><i class="fa fa-undo"></i> '+try_again+'</a>'+
  '</div>'+
  '</div>';
  $pnl_intentos.append(html);
};

var descontarIntentos = function(IDGUI){
  var $pnl_intentos = $('#pnl_intentos'+IDGUI);
  var cant_intentos_activos = $pnl_intentos.find('.nro-intento.active').length;
  if(cant_intentos_activos>0){
    $pnl_intentos.find('.nro-intento.active').eq(cant_intentos_activos-1).removeClass('active', {duration:200});
    if( (cant_intentos_activos-1)===0 ) $('#pnl_botones'+IDGUI+' .btn.try-again').addClass('disabled');
  } 
  return cant_intentos_activos;
};

var aumentarIntentos = function(){
  var $panelIntentos = $('#panel-intentos-dby');
  var $tmplActiva = getTmplActiva();

  var intento_actual=parseInt($tmplActiva.find('.plantilla').attr('data-intento')||1);  
  var intentos_total = $panelIntentos.find('.total').text();
  if(intento_actual==0)intento_actual=1;
  var rspta = false;
  if(intento_actual<intentos_total ){
    $tmplActiva.find('.btn.try-again').removeClass('disabled').show();
    $tmplActiva.find('.save-progreso').hide();
    intento_actual++; 
    rspta = true;
  }

  $tmplActiva.find('.plantilla').attr('data-intento',intento_actual);
  $panelIntentos.find('.actual').text(intento_actual);

  if(intento_actual==intentos_total ) {
    $tmplActiva.find('.btn.try-again').addClass('disabled').hide();
    $tmplActiva.find('.save-progreso').show();
    if($tmplActiva.closest('#div_autoevaluar')){$tmplActiva.find('.plantilla').addClass('terminointentos') }
  }
  return rspta;
};

var existeAltMarcada = function($panel_alt){
  var rspta = false;
  var $alternativa = $panel_alt.find('ul.alternativas-list li.alt-item a');
  $alternativa.each(function() {
    if( $(this).parent().hasClass('wrapper') ){
      rspta = true;
      return false; //stop loop Each
    }
  });
  return rspta;
};

var agregarRsptaInput = function($panel, rsptaSelec){
  var $inputClicked = $panel.find('input.isclicked[data-mce-object="input"]');
  var arrRspta = rsptaSelec.split(' - ');
  var i = 0;
  $inputClicked.each(function() {
    $(this).val(arrRspta[i]);
    $(this).attr('value', arrRspta[i]);
    evaluarRespuesta(this);
    i++;
  });
};

var generarDragDrop = function(IDGUI){
  var inputDrag=$('#pnl_edithtml'+IDGUI).find('input.isdrop[data-mce-object="input"]');
  var ninputDrag=inputDrag.length;
  var spanalt=[];
  inputDrag.each(function(){
    if(ninputDrag>1){
      spanalt.push('<span class="isdragable" draggable="true" >'+$(this).data('texto')+'</span>');
    }else{
      var opt=$(this).data('options');
      if(opt!='' && opt!=undefined){
        spanalt.push('<span class="isdragable" draggable="true" >'+$(this).data('texto')+'</span>');
        opt = _isJson(opt)?JSON.parse(opt):opt.split(',');
        $.each(opt,function(index,value){
          spanalt.push('<span class="isdragable" draggable="true" >'+value+'</span>');
        });
      }
    }
  });

  spanalt.sort(function(){return Math.random() - 0.5});
  $('#pnl_editalternatives'+IDGUI).html(spanalt.toString());
};

var cargarContenidoTextarea = function(IDGUI){
  var html_edit = $('#pnl_edithtml'+IDGUI).html();
  $('#txtarea'+IDGUI).html(html_edit);
};

var isTodoCorrecto = function($panelEjercicio, buscarTodos, buscarCorrecto){
  var $tmplActiva = getTmplActiva();
  if($panelEjercicio.length==0)$panelEjercicio=$tmplActiva.find('.panelEjercicio');
  var cantElemTotal = $panelEjercicio.find(buscarTodos).length;
  var cantElemCorrectos = $panelEjercicio.find(buscarCorrecto).length;
  var rspta = false;
  if(cantElemTotal==0){console.log("There was an error counting total elements: '"+buscarTodos+"'!"); return false;}
  if( cantElemTotal == cantElemCorrectos ){
    $tmplActiva.find('.plantilla').removeClass('ejerc-iniciado').addClass('ejerc-terminado-correcto');
    rspta = true;
  }
  return rspta;
};

var existeSgteEjercicio = function(){
  var $metodActivo = $('.metod.active');
  var rspta= true;
  if( $metodActivo.find('ul.nav.ejercicios').length==0 ){
    rspta=false;
  }
  if( $metodActivo.find('ul.nav.ejercicios>li.active').next('li').length==0 ){
    rspta=false;
  }
  return rspta;
};

var mostrarBtnsDinamicos = function(IDGUI, plantilla){
  var plantilla=plantilla||'';
  var ROL = $('.main-container').data('rol');
  if(ROL=='Alumno'){
    var sysalt=$('#pnl_editalternatives'+IDGUI).html();
    if(sysalt!=undefined && sysalt.trim()==''){
      $('#pnl_editalternatives'+IDGUI).hide();
      $('#btns_control_alumno .save-progreso').show();
    }
  }else{
    iniciarPlantilla(plantilla); /* editactividad.js */
  }
};

var setDataTipoTmp = function(IDGUI){
  var $tmpActual = $('#tmp_'+IDGUI);
  var dataAddClass = $tmpActual.parents('.aquicargaplantilla').attr('data-addclass');
  if(dataAddClass=='isclicked'){
    var tipoTmp = 'tipo_2'; //alternativas
  } else{
    var tipoTmp = 'tipo_1'; //completar, arrastre, desplegable
  }
  $tmpActual.attr('data-tipo-tmp', tipoTmp);
};

var MSJES_PHP = {};
var cargarMensajesPHP = function(){
  var $sectionMensajes = $('#msjes_idioma');
  $sectionMensajes.find('input').each(function() {
    var id = $(this).attr('id');
    var valor = $(this).val();
    MSJES_PHP[id] = valor;
  });
};

var veces = 0;
var fnTiempoIntentosPuntaje = function(condicionTmpl, fn, buscarTodos, buscarCorrecto){
  var $tmplActiva = getTmplActiva();
  if($tmplActiva.data('addclass')=='iswrite' && veces==1){ veces=0; return false; }

  var $plantActiva = $tmplActiva.find('.plantilla');
  if( condicionTmpl ){
    if( !$plantActiva.hasClass('tiempo-acabo') ){
      fn();
    } else {
      var mensaje = MSJES_PHP.tiempo_acabo;
      if($plantActiva.find('.btn.save-progreso').is(':visible')) mensaje += ' '+MSJES_PHP.puedes_continuar;
      if(!$plantActiva.find('.btn.try-again').hasClass('disabled')) mensaje += ' '+MSJES_PHP.intentalo_otra_vez;
      mostrar_notificacion(MSJES_PHP.attention, mensaje, 'warning');
    }

    if( isTodoCorrecto($plantActiva.find('.tpl_plantilla'),buscarTodos,buscarCorrecto) ){
      $plantActiva.find('.btn.save-progreso').show();
      $plantActiva.find('.btn.try-again').addClass('disabled');
      var mensaje = MSJES_PHP.ejerc_exito;
      if( existeSgteEjercicio() ){ mensaje += ' '+ MSJES_PHP.ir_sgte_ejerc; }
      else { mensaje += ' '+ MSJES_PHP.finalizar; }
      mostrar_notificacion(MSJES_PHP.bien_hecho, mensaje, 'success');
    
      //@edu se agrego bloque else if - inicio
    } else if(isTodoCorrecto($plantActiva.find('.contenedor-img'),buscarTodos,buscarCorrecto)){
      $plantActiva.find('.btn.save-progreso').show();
      $plantActiva.find('.btn.try-again').addClass('disabled');
      var mensaje = MSJES_PHP.ejerc_exito;
      if( existeSgteEjercicio() ){ mensaje += ' '+ MSJES_PHP.ir_sgte_ejerc; }
      else { mensaje += ' '+ MSJES_PHP.finalizar; }
      mostrar_notificacion(MSJES_PHP.bien_hecho, mensaje, 'success');
      // fin
    
    } else if( ($plantActiva.hasClass('tiempo-acabo') || $plantActiva.hasClass('tiempo-pausado')) &&  $plantActiva.find('.btn.try-again').hasClass('disabled') ){
      $plantActiva.find('.btn.save-progreso').show();
    }
  } else {
    if( $plantActiva.hasClass('ejerc-terminado-correcto') ){
      mostrar_notificacion(MSJES_PHP.bien_hecho, MSJES_PHP.puedes_continuar,'success');
    } else {
      mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar,'warning');
    }
  }
  
  if($tmplActiva.data('addclass')=='iswrite') veces++;
};

var revisarejericio=function($plantilla){  
  var intento=$plantilla.attr('data-intento')||1;
  if(intento==3){
    $plantilla.find('.btn.try-again').remove();
    $plantilla.find('.btn.save-progreso').show('fast');
  }else{
    var completo=true;
    if($plantilla.hasClass('plantilla-completar')){      
      $plantilla.find('input.mce-object-input').each(function(i,v){        
        if(completo==false) return;
        $this=$(this);
        var dt=$this.attr('data-texto')||'_lleno_';
        var val=$this.val()||'_vacio_';
        if(dt!=val&&val!='')completo=false;
      })
      if( $plantilla.find('.panelAlternativas').length>0){
        if($plantilla.find('.panelAlternativas').text().trim()=='') $plantilla.find('.panelAlternativas').parent('.tpl_alternatives').parent().remove();
      }
    }else if($plantilla.hasClass('plantilla-ordenar')){
      
      if($plantilla.hasClass('ord_parrafo')){
        if($plantilla.find('.drop-parr > div').length==0) completo=false;
        $plantilla.find('.drop-parr > div').each(function(i,v){        
          if(completo==false) return;
          $this=$(this);
          if(!$this.hasClass('good'))completo=false;
        })

      }else if($plantilla.hasClass('ord_simple')){
        $plantilla.find('.drop > .parte').each(function(i,v){        
          if(completo==false) return;
          $this=$(this);
          if(!$this.hasClass('good'))completo=false;
        })
      }
    }else if($plantilla.hasClass('plantilla-speach')){
      var compl=$plantilla.find('.textohablado .speachtexttotal');
      if(compl.length==0) completo=false;
      if(compl!='100%') completo=false;     
    }else if($plantilla.hasClass('plantilla-img_puntos')){
      $plantilla.find('.tag-alts > .tag').each(function(i,v){        
        if(completo==false) return;
        $this=$(this);
        if(!$this.hasClass('_success_'))completo=false;
      })      
    }else if($plantilla.hasClass('plantilla-verdad_falso')){
      $plantilla.find('.icon-result').each(function(i,v){        
        if(completo==false) return;
        $corr=$(this).attr('data-corregido')||'no';
        if($corr!='good')completo=false;
      })      
    }else if($plantilla.hasClass('plantilla-fichas')){
      $plantilla.find('.partes-2 > .ficha').each(function(i,v){        
        $this=$(this);
        if(!$this.hasClass('good'))completo=false;
      })      
    }
    if(completo==true){
      $plantilla.find('.btn.try-again').remove();
      $plantilla.find('.btn.save-progreso').show('fast');
    }else{
      $plantilla.find('.btn.try-again').show('fast');
      $plantilla.find('.btn.save-progreso').hide(0);
    }
    return completo;
  }
  
}

/*var iniciarCompletar_Practice = function(IDGUI){
  mostrarBtnsDinamicos(IDGUI, 'multi');
  desplegarAlternativas(IDGUI);
  cargarContenidoTextarea(IDGUI);
};*/

/* ** Controlando Paneles Tiempo y Puntaje ** */
var getTmplActiva = function(){
  var $tmplActiva = $('.metod.active .tabhijo.active .aquicargaplantilla');
  if( $tmplActiva.length==0 ) $tmplActiva = $('.metod.active .aquicargaplantilla'); 
  return $tmplActiva;
};

var iniciarTiempo = function(fnCallback){
  var fnCallback=fnCallback||'';
  var $tmplActiva = getTmplActiva();
  var $plantActiva = $tmplActiva.find('.plantilla'); 
  var intento_actual=parseInt($plantActiva.attr('data-intento')||1);  
  var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
  if(intento_actual<intentos_total) $plantActiva.removeClass('terminadoentiempo'); 
  if($plantActiva.hasClass('terminadoentiempo')) return;
  var $time = $('#panel-tiempo .info-show');
  if( $time.is(':visible') && !$plantActiva.hasClass('tiempo-pausado') ){
    $time.cronometroa0('oncropausar');
    $time.cronometroa0('oncroiniciar', fnCallback);
    $plantActiva.addClass('ejerc-iniciado');    
  }
  if(intento_actual==intentos_total) $plantActiva.addClass('terminadoentiempo'); 
};


$('#contenedor-paneles')
.on('change', 'input.setpanelvalue', function(e) {
  var $tmplActiva = getTmplActiva();
  var actualiza = $(this).data('actualiza');
  var value = $(this).val();

  $(this).attr('value', value);
  $(this).parents('.panel-info').find('.info-show').text(value);
  $tmplActiva.attr('data-'+actualiza, value);
});

var finTiempo= function(){
  var $tmplActiva = getTmplActiva();
  var $plantActiva =  $tmplActiva.find('.plantilla');
  $plantActiva.addClass('tiempo-acabo');
  if( $plantActiva.hasClass('plantilla-completar') ){
    var buscarTodos = 'input.mce-object-input';
    var buscarCorrecto = '.inp-corregido[data-corregido="good"]';
  } else if( $plantActiva.hasClass('plantilla-fichas') ){
    var buscarTodos = '.partes-2 .ficha';
    var buscarCorrecto = '.partes-2 .ficha.corregido.good';
  } else if( $plantActiva.attr('data-tipo-tmp')=='ordenar_simple' ){
    var buscarTodos = '.element';
    var buscarCorrecto = '.element[data-corregido="good"]';
  } else if( $plantActiva.attr('data-tipo-tmp')=='ordenar_parrafo' ){
    var buscarTodos = '.drop-parr>div';
    var buscarCorrecto = '.drop-parr>div.good';
  }else if( $plantActiva.hasClass('plantilla-verdad_falso') ){
    var buscarTodos = '.premise';
    var buscarCorrecto = '*[data-corregido="good"]';
  }else if($plantActiva.hasClass('plantilla-speach')){
    btnaudiograbando=$plantActiva.find('.pnl-speach.alumno .btnGrabarAudio');
    if(!btnaudiograbando.hasClass('disabled')) btnaudiograbando.trigger('click');
    var buscarTodos = '.textohablado span';
    var buscarCorrecto = '.textohablado span.speachtextook';
  };

  if( !isTodoCorrecto($plantActiva.find('.tpl_plantilla'),buscarTodos,buscarCorrecto) && ($plantActiva.hasClass('tiempo-acabo') || $plantActiva.hasClass('tiempo-pausado')) &&  $plantActiva.find('.btn.try-again').hasClass('disabled') ){
    $plantActiva.find('.btn.save-progreso').show();
  }
};


/* Volver a intentar */



//funciones en plantilla;

  var iniciarCompletar_DBY = function(IDGUI, msjes){

      /*mostrarBtnsDinamicos(IDGUI, 'multi');
      desplegarAlternativas(IDGUI);
      cargarContenidoTextarea(IDGUI);
      setDataTipoTmp(IDGUI);
      $('.plantilla').removeClass('ejerc-terminado-correcto').removeClass('tiempo-pausado').removeClass('tiempo-acabo').removeClass('ejerc-iniciado');*/
    };
