 var jsonmultiplantilla={};
 (function($){
    $.fn.tplcompletar=function(opciones){
        var config = $.extend({'editando': false,'tpls':null,idgui:null}, opciones);
        cargarMensajesPHP();
        return this.each(function(){
            var objcont=$(this);
            var _cureditable=config.editando;
            var _curmetod=null; //Metodologia Activa
            var _curejercicio=null; //Ejercicio activo
            var _curdni=null;
            var _curaquicargaplantilla=null;
            var TMPL=config.tpls;
            var gbl_IDGUI=config.idgui;
            var gbl_INTENTOS=config.nintentos;
            var enpreview=false;
            var mobilhacklife = false;

            objcont.addClass('smartic-initialized');

            var EjercicioActivo=function(){
               
                var metodo=$('.metod.active',objcont);               
                if(metodo.length==0){                    
                    objcont.children('.metod').addClass('active');
                    metodo=objcont.children('.metod');
                    /*let tmpmet=objcont.attr('id')=='div_look'?1:2; //solo afecta al smartbook;
                    if(objcont.attr('id')=='div_look')
                    objcont.addClass('metod active').attr('data-idmet',tmpmet);  */
                }
                _curmetod=metodo.length>0?metodo:objcont;
                _curejercicio=_curmetod.attr('id')!='met-1'?_curmetod.find('.tabhijo.active'):_curmetod.find('.Ejercicio');
                _curaquicargaplantilla=$('div.aquicargaplantilla',_curejercicio);
            }

            var resetEdicion_variables=function(){               
                if(editableejercicio==false) return; // no como vista de alumno
                var ee=EjercicioActivo();
                var idmet=_curmetod.attr('data-idmet');
                if(idmet==1) return; // solo practice y dby   
                $textosave=_curaquicargaplantilla.closest('.textosave');
                var index=$textosave.closest('.tab-content').find('div.tabhijo.active').index();
                index++;
                $textosave.find('h3.addtexttitulo2').siblings('input').attr('name','titulo['+idmet+']['+index+']');
                $textosave.find('p.addtextdescription22').siblings('input').attr('name','descripcion['+idmet+']['+index+']');
                if(idmet==2){

                }
            }

            var verplantillas=function(forzado){
                var forzado=forzado||false;
                if(_cureditable==false) return false;
                EjercicioActivo();
                if(_curaquicargaplantilla.text()!=''&&forzado==false) return false
                var met=_curmetod.attr('data-idmet');
                var tplsel=$(this).attr('data-tpl')||'';
                var tmp=TMPL[met-1];
                var showtime=tmp.showtime||0;
                var showpuntaje=tmp.showpuntaje||0;
                var timedefualt=tmp.timedefualt||0;
                var _html='<div class="row">';
                $.each(tmp.tpl,function(i,v){
                    var img=v.img||'default';
                    var bgcolor=v.color||'#fc5c5c';
                    var asistido=v.showasistido||0;
                    var alternativas=v.showalternativas||0;
                    var tools=v.tools||'';
                    var videohelp=v.video||'';
                    var addclassinput=v.addclass||'';
                    var items=v.items||false;
                    if(items){
                        _html+='<div class="col-md-12 col-sm-12 col-xs-12"><div class="row tmp-categoria" style="background:'+bgcolor+';"><strong>'+v.name+'</strong></div><div class="row">';
                        $.each(items,function(ii,vv){
                            page=vv.page||v.page;
                            name=vv.name||v.name;
                            videoh=vv.video||videohelp;
                            imgi=vv.img||img;
                            addclassinput=vv.addclass||addclassinput;
                            bgcolori=vv.color||bgcolor;
                            toolsi=vv.tools||tools;
                            _html+='<div class="col-md-3 col-sm-6 col-xs-12 text-center seliplantilla" data-url="'+page+'" data-showasistido="'+asistido+'"  data-showalternativas="'+alternativas+'"'
                                 +' data-tpltitulo="'+v.name+'-'+name+'"  data-showtime="'+showtime+'"  data-showpuntaje="'+showtime+'" data-puntaje="0" data-time="'+timedefualt+'" data-tools="'+toolsi+'"  data-videohelp="'+videoh+'" data-addclass="'+addclassinput+'" >'
                                 +'<div class="btn btn-template" style="background:'+bgcolori+';"><div class="nametpl"><strong>'+name+'</strong></div>'
                                 +'<img class="img-responsive" src="'+(_sysUrlBase_+'/static/sysplantillas/'+imgi)+'.gif"></div></div>';
                        });
                        _html+='</div></div>';

                    }else{
                    _html+='<div class="col-md-3 col-sm-6 col-xs-12 text-center seliplantilla" data-url="'+v.page+'" data-showasistido="'+asistido+'"  data-showalternativas="'+alternativas+'"'
                         +' data-tpltitulo="'+v.name+'" data-showtime="'+showtime+'"  data-showpuntaje="'+showtime+'" data-puntaje="0" data-time="'+timedefualt+'" data-tools="'+tools+'"  data-videohelp="'+videohelp+'" data-addclass="'+addclassinput+'" >'
                         +'<div class="btn btn-template" style="background:'+bgcolor+';"><div class="nametpl"><strong>'+v.name+'</strong></div>'
                         +'<img class="img-responsive" src="'+(_sysUrlBase_+'/static/sysplantillas/'+img)+'.gif">'
                         +'</div></div>';
                     }
                });
                _html+='<div class="clearfix"></div></div>'
                var data={
                  titulo: MSJES_PHP.select_activity,
                  html:_html,
                  ventanaid:'selplantilla-'+met,
                  borrar:true
                }
                var modal=sysmodal(data);
                modal.on('click','.seliplantilla',function(ev){
                    ev.preventDefault();
                    ev.stopPropagation();
                    var tmpptl=$(this).attr('data-url');
                    var asistido=$(this).attr('data-showasistido');
                    var alternativas=$(this).attr('data-showalternativas');
                    var tools=$(this).attr('data-tools');
                    var videohelp=$(this).attr('data-videohelp');
                    var addclassinput=$(this).attr('data-addclass');
                    var tpltitulo=$(this).attr('data-tpltitulo');
                    var showtime=$(this).attr('data-showtime');
                    var showpuntaje=$(this).attr('data-showpuntaje');
                    var time=$(this).attr('data-time');
                    var puntaje=$(this).attr('data-puntaje');
                    var content=_curaquicargaplantilla;
                    var source=sitio_url_base+'/plantillas/actividad/';
                    var orden=1;
                    var param_inten='&inten=3';
                    modal.find('.cerrarmodal').trigger('click');
                    content.attr('data-tpl',tmpptl);
                    content.attr('data-helpvideo',videohelp);
                    content.attr('data-showasistido',asistido);
                    content.attr('data-showalternativas',alternativas);
                    content.attr('data-tools',tools);
                    content.attr('data-tpltitulo',tpltitulo);
                    content.attr('data-addclass',addclassinput);
                    content.attr('data-showtime',showtime);
                    content.attr('data-showpuntaje',showpuntaje);
                    content.attr('data-time',time);
                    content.attr('data-puntaje',puntaje);
                    sysaddhtml(content, source+tmpptl+'.php?met='+met+'&ord='+orden+param_inten,'',sysalcargarplantilla);
                    if(tmpptl=='practice_speach'||tmpptl=='dby_practice_speach'){
                        inp=content.siblings('.tab-title-zone').find('input').val()||'';
                        inpdes=content.siblings('.tab-title-zone').find('input').val()||'';                       
                        if(inp=='') content.siblings('.tab-title-zone').find('h3.addtext').text('Speaking Practice');
                        if(inpdes=='') content.siblings('.tab-description').find('p.addtext').text('Listen carefully and REPEAT.');
                    }
                    content.siblings('.tab-title-zone').show();
                    content.siblings('.tab-description').show();
                    
                    /*if( $('#met-3').hasClass('active') ){
                        $('#met-3 .tab-pane.active').find('.aquicargaplantilla').attr('data-intentos', gbl_INTENTOS);
                        mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
                    /*}
                    actualizarPanelesInfo(); /* actividad_completar.js */
                });
            }
            var showBotonesguardardoc=function(){
                if(_cureditable==false){
                    if(enpreview=false){
                        $('#btns-guardar-actividad',objcont).hide();
                    }
                    return false;
                }
                EjercicioActivo();
                var plantilla=$('.plantilla',_curaquicargaplantilla);
                if(plantilla.hasClass('editando')){
                    $('#btns-guardar-actividad',objcont).hide();
                    return false;
                }

                if($('.btnedithtml',_curaquicargaplantilla).hasClass('disabled')){
                     $('#btns-guardar-actividad',objcont).hide('fast');
                     return false;
                }
                $('#btns-guardar-actividad',objcont).show('fast');
                $('#btns-guardar-actividad .btnsaveActividad',objcont).show('fast');
                if(_curmetod.attr('id')=='met-1')$('#btns-guardar-actividad .continue',objcont).hide();
                else $('#btns-guardar-actividad .continue').show();
            }
            var DBY_mostrar_ocultarBtnProgreso = function(){
                EjercicioActivo();
                if(_curmetod.attr('data-idmet')==3 && _cureditable==false){
                    var avance = _curejercicio.attr('data-progreso') || 0.00;
                    var cantTotalEjerc = objcont.find('.tab-content .tab-pane.tabhijo').length;
                    var ptjexEjerc = 100/cantTotalEjerc;
                    var progreso=avance*100/ptjexEjerc;
                    //console.log(_curejercicio.closest('.smartic-initialized').attr('id'));
                    if(!_curejercicio.closest('.smartic-initialized').attr('id')=='divautoevaluar')
                    if(progreso>=100.0){                        
                        _curejercicio.find('.btn.save-progreso').show();
                    }else{
                        _curejercicio.find('.btn.save-progreso').hide();
                    }
                }
            };

            var addBtnsControlAlumno = function(pnlactive){
                var pnlactive=pnlactive||objcont;
                var html = $('#btns_progreso_intentos').clone().html();
                $('.plantilla #btns_control_alumno',pnlactive).remove();
                $('.plantilla',pnlactive).each(function(i,v){
                    var met=$(v).closest('.metod').attr('data-idmet');
                    var $html = $(html);
                    $('.botones-control a',$html).show();
                    if(met<3) $html.find('.btn.try-again').remove();
                    if(met==3) $html.find('.btn.save-progreso').hide();
                    $(v).append($html);
                });
                if(_cureditable==true) $('.plantilla #btns_control_alumno',pnlactive).hide();
            };

            var resetEjercicio = {};

            var resetActividadDBY = function(tipoPlant, idTmpl){
              var IDGUI = idTmpl.split('_').pop();
              console.log(tipoPlant,idTmpl,IDGUI);
              resetEjercicio[tipoPlant](IDGUI);
            };


            var resetImagen_Audio = function(){
                var plantillas=objcont.find('.plantilla_imagen_audio');
                if(plantillas.length==0)return false;
                plantillas.each(function(){
                    var plantilla=$(this);
                    var data_audio = $('.playaudioimagen',plantilla).attr('data-audio');
                    if(data_audio!=undefined && data_audio!=''){
                        $('.playaudioimagen',plantilla).removeClass('hidden').show('fast');
                    }else{
                        $('.playaudioimagen',plantilla).addClass('hidden');
                    }
                    /*$('.playaudioimagen',plantilla).addClass('hidden');*/

                });
            };

            var resetDialogo = function(){
                var plantillas=$(objcont).find('.plantilla-dialogo');
                if(plantillas.length==0) return false;
                plantillas.each(function(){
                    var plantilla=$(this);
                    plantilla.find("#curaudiodialogo").attr('src', '');
                    plantilla.find("#curaudiodialogo").trigger("stop");
                    plantilla.find('#playdialogo').attr('data-estado', 'stopped');
                    if( plantilla.find('#playdialogo i').hasClass('fa-stop') ) {
                      plantilla.find('#playdialogo i').removeClass('fa-stop').addClass('fa-play');
                    }
                    plantilla.find('.dialogue-pagination>ul>li').removeClass('active');
                    plantilla.find('.dialogue-pagination>ul>li:first').addClass('active');
                    plantilla.find('.tab-pane').removeClass('active in');
                    plantilla.find('.tab-pane:first').addClass('active in');
                    plantilla.find('.tab-pane.active .d-box[data-orden="1"]').css('display','none');
                });
            };

            var resetcompletar=function(){
                reiniciarInputsMCE(plantilla);
                if($('.tpl_alternatives',plantilla).text()=='')$('.tpl_alternatives',plantilla).hide();
            }

            var resetFichas = function(){
                var plantillas=objcont.find('.plantilla-fichas');
                if(plantillas.length==0)return false;
                plantillas.each(function(){
                     var pnl=$(this);
                     if(pnl.hasClass('editando')){
                        var idTmpl = $(this).attr('id');
                        $('#'+idTmpl+'  .generar-fichas').trigger('click');
                     }else
                     $('.ficha',pnl).removeClass('active corregido good bad');
                });
            };

            var resetAlternativas = function(){
                $('.plantilla-marcar_respuesta .question-alternatives .correct').removeClass('correct');
                $('.plantilla-marcar_respuesta .question-alternatives .warning').removeClass('warning');
            };

            var resetVerdadFalso = function(){
                $('.list-premises input[type="radio"].radio-ctrl').prop('checked', false);
                $('.list-premises .valin').each(function(){
                    var vp=$(this).val();
                    if(vp==''||vp==undefined) {
                        var dpview=$(this).attr('data-nopreview');
                        $(dpview).remove();
                    }
                });
                reiniciarVoF($('.list-premises'));
                $('.list-premises .icon-result').removeAttr('data-corregido');
                $('.list-premises .icon-result').html('');
            };

            var resetOrdenar = function(){
                var plantillas=objcont.find('.plantilla.plantilla-ordenar');
                if(plantillas.length==0)return false;
                plantillas.each(function(){
                    var id = $(this).attr('id');
                    $('#'+id+' .generar-parrafo').trigger('click');
                    $('#'+id+' .generar-elem').trigger('click');
                });
            };


            var resetImgPuntos = function(){
                var plantillas=objcont.find('.plantilla-img_puntos');
                if(plantillas.length==0)return false;
                plantillas.each(function(){
                    plantilla=$(this);
                    if(plantilla.hasClass('editando')){
                        var id = plantilla.attr('id');
                        $('#'+id+' .generate-tag').trigger('click');
                        plantilla.find('.dot._success_').removeClass('_success_');
                    }else{
                        plantilla.find('.dot-tag').removeClass('corregido bad good');
                        plantilla.find('.dot._success_').removeClass('_success_');
                        plantilla.find('.tag').show().removeClass('_success_');
                    }
                });
            };


            resetEjercicio['tipo_1'] = function(IDGUI){
              /* arrastre, desplegable, completar */
              var $panelEdit = $('#pnl_edithtml'+IDGUI);
              var $inputsMCE = $panelEdit.find('input.mce-object-input');
              var todoCorrecto = isTodoCorrecto($panelEdit,'input.mce-object-input','.inp-corregido[data-corregido="good"]');
              var $tmplActiva = $panelEdit.closest('.aquicargaplantilla');
              var $plantActiva = $tmplActiva.find('.plantilla');

              if( !todoCorrecto){
                //$inputsMCE.length,$plantActiva.hasClass('ejerc-iniciado');
                if($inputsMCE.length>=1 ){/*inputsCompletos($panelEdit) &&*/
                  var puedeReiniciar = aumentarIntentos();
                  if(puedeReiniciar){
                    reiniciarInputsMCE($panelEdit);
                    $('#panel-tiempo .info-show').trigger('oncropausar');
                    actualizarPanelesInfo();
                  }
                } else {                  
                  mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.you_must_finish, 'warning');
                }
              } else {
                var mensaje = MSJES_PHP.ejerc_exito
                if( existeSgteEjercicio() ){ mensaje += ' '+ MSJES_PHP.ir_sgte_ejerc; }
                else { mensaje += ' '+ MSJES_PHP.finalizar; }
                mostrar_notificacion(MSJES_PHP.bien_hecho, mensaje, 'success');
              }
            };

            resetEjercicio['tipo_2'] = function(IDGUI){
              /* alternativas */
              var $panelEdit = $('#pnl_edithtml'+IDGUI);
              console.log($panelEdit);
              var $inputsClicked = $panelEdit.find('input.isclicked');
              console.log($inputsClicked);
              var $tmplActiva = $panelEdit.closest('.aquicargaplantilla');
              var $plantActiva = $tmplActiva.find('.plantilla');
              //if( $inputsClicked.length>=1 && $plantActiva.hasClass('ejerc-iniciado') ){/*inputsCompletos($panelEdit) &&*/
                var puedeReiniciar = aumentarIntentos();
                if( puedeReiniciar ){
                  reiniciarInputsMCE($panelEdit);
                  $('#panel-tiempo .info-show').trigger('oncropausar');
                  actualizarPanelesInfo();
                }
             /* } else {
                mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.you_must_finish, 'warning');
              }*/
            };

            resetEjercicio['verdadero_falso'] = function(IDGUI){
                var $tmplActiva = _curaquicargaplantilla
                var $plantActiva = $tmplActiva.find('.plantilla');
                var $pnlPremisas = $('#tmp_'+IDGUI+' #pnl_premisas'+IDGUI);
                //if( $plantActiva.hasClass('ejerc-iniciado') ){/*inputsRadCompletos($pnlPremisas)*/
                    var puedeReiniciar = aumentarIntentos();
                    if( puedeReiniciar ){
                        reiniciarInpRadios($pnlPremisas);
                        $pnlPremisas.find('*[data-corregido]').removeAttr('data-corregido');

                        $('#panel-tiempo .info-show').trigger('oncropausar');
                        actualizarPanelesInfo();
                    }
                /*} else {
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.you_must_finish, 'warning');
                }*/
            };

            resetEjercicio['ordenar_simple'] = function(IDGUI){
                var $tmplActiva = _curaquicargaplantilla
                var $plantActiva = $tmplActiva.find('.plantilla');
                var $pnlEjercicio = $('#tmp_'+IDGUI+' #ejerc-ordenar'+IDGUI);
                //if( $plantActiva.hasClass('ejerc-iniciado') ){
                    var puedeReiniciar = aumentarIntentos();
                    if( puedeReiniciar ){
                        resetOrdenarSimple($pnlEjercicio, IDGUI);

                        $('#panel-tiempo .info-show').trigger('oncropausar');
                        actualizarPanelesInfo();
                    }
               /* } else {
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.you_must_finish, 'warning');
                }*/
            };

            resetEjercicio['ordenar_parrafo'] = function(IDGUI){
                var $tmplActiva = _curaquicargaplantilla
                var $plantActiva = $tmplActiva.find('.plantilla');
                var $pnlEjercicio = $('#tmp_'+IDGUI+' #ejerc-ordenar'+IDGUI);
                //if( $plantActiva.hasClass('ejerc-iniciado') ){
                    var puedeReiniciar = aumentarIntentos();
                    if( puedeReiniciar ){
                        resetOrdenarParrafo($pnlEjercicio, IDGUI);

                        $('#panel-tiempo .info-show').trigger('oncropausar');
                        actualizarPanelesInfo();
                    }
                /*} else {
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.you_must_finish, 'warning');
                }*/
            };

            resetEjercicio['img_puntos'] = function(IDGUI){
                var $tmplActiva = _curaquicargaplantilla;
                var $plantActiva = $tmplActiva.find('.plantilla');
                //var $pnlEjercicio = $('#tmp_'+IDGUI+' .tpl_plantilla'); //@M
                var $pnlEjercicio = $('#tmp_'+IDGUI+' .contenedor-img');    //@edu se modifico [@M]
               // if( $plantActiva.hasClass('ejerc-iniciado') ){
                    var puedeReiniciar = aumentarIntentos();
                    if( puedeReiniciar ){
                        resetImgsTagged($pnlEjercicio, IDGUI);
                        $('#panel-tiempo .info-show').trigger('oncropausar');
                        actualizarPanelesInfo();
                    }
                /*} else {
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.you_must_finish, 'warning');
                }*/
            };

            resetEjercicio['fichas'] = function(IDGUI){
                var $tmplActiva = _curaquicargaplantilla;
                var $plantActiva = $tmplActiva.find('.plantilla');
                var $pnlEjercicio = $('#tmp_'+IDGUI+' #ejerc-fichas'+IDGUI);
               // if($plantActiva.hasClass('ejerc-iniciado')){
                    var puedeReiniciar = aumentarIntentos();
                    if( puedeReiniciar ){
                        $pnlEjercicio.find('.ficha').removeClass('active').removeClass('corregido').removeClass('good').removeClass('bad');
                        $('#panel-tiempo .info-show').trigger('oncropausar');
                        actualizarPanelesInfo();
                    }
                /*} else {
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.you_must_finish, 'warning');
                }*/
            };

            resetEjercicio['speach']=function(IDGUI){
                //console.log('---reset---');
                var $tmplActiva = _curaquicargaplantilla;
                var $plantActiva = $tmplActiva.find('.plantilla');
                var $pnlEjercicio = $('#tmp_'+IDGUI);

                //if($plantActiva.hasClass('ejerc-iniciado')){
                    var puedeReiniciar = aumentarIntentos();
                    if(puedeReiniciar){
                        $pnlEjercicio.find('.textohablado').html('');                        
                        $pnlalumno=$pnlEjercicio.find('.pnl-speach.alumno');
                        $pnlalumno.find('.txtalu2').html('');
                        $pnlalumno.find('.grafico').html('');
                        $pnlalumno.find('.btnAccionspeachdby').removeClass('active');
                        if($pnlalumno.find('.btnGrabarAudio').hasClass('btn-success')){
                            $pnlalumno.find('.btnGrabarAudio').trigger('click');
                        }
                        //.log('---llego---');
                        $pnlalumno.find('.btnGrabarAudio').removeClass('disabled hide').removeAttr('disabled');
                        $('#panel-tiempo .info-show').trigger('oncropausar');
                        actualizarPanelesInfo();  
                    }
                /*} else {
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.you_must_finish, 'warning');
                }*/
            }

            var addBtnsEdicionMCE = function(pnlactive,editar){
              var pnlactive=pnlactive||objcont;
              var editar=editar||false;
              var html = $('#tpledittoolbar').clone().html();
              $('.plantilla-completar .btn-toolbar',pnlactive).remove();
              $('.plantilla-completar .tpl_plantilla',pnlactive).each(function(i,v){
                    var idgui=$(v).attr('data-idgui');
                    var tpl=$(v).parent();
                    tpl.prepend(html);
                    $(tpl).find('.btn-toolbar').attr('data-idgui',idgui).addClass('nopreview');
                    $(tpl).find('.btn-toolbar').find('.btn.btnedithtml').attr('id','btnedithtml'+idgui);
                    $(tpl).find('.btn-toolbar').find('.btn.btnsavehtml').attr('id','btnedithtml'+idgui);
                    if(editar) $('.btnedithtml',tpl).trigger('click');
              });
            };

            var mostrarPanelIntentos = function(){
                EjercicioActivo();
                var met=_curmetod.attr('data-idmet');
              if( met==3){
                var total_intentos = _curaquicargaplantilla.attr('data-intentos');
                var actual_intentos =parseInt(_curaquicargaplantilla.find('.plantilla').attr('data-intento')||1);
                $('#panel-intentos-dby .actual').text(actual_intentos);
                $('#panel-intentos-dby .total').text(total_intentos);
                $('#btns_control_alumno .btn.try-again',_curaquicargaplantilla).removeClass('disabled');
                if( !_curaquicargaplantilla.is(':empty') ){
                    if( $(window).outerWidth()<=768 ) {$('#panel-intentos-dby').show();}
                    else {$('#panel-intentos-dby').fadeTo(1, 1);}
                    $('#btns_control_alumno .btn.try-again',_curaquicargaplantilla).show();
                } else {
                  if( $(window).outerWidth()<=768 ) {$('#panel-intentos-dby').hide();}
                  else {$('#panel-intentos-dby').fadeTo(1, 0);}
                  $('#btns_control_alumno .btn.try-again',_curaquicargaplantilla).hide();
                }
              } else {
                  if( $(window).outerWidth()<=768 ) {$('#panel-intentos-dby').hide();}
                  else {$('#panel-intentos-dby').fadeTo(1, 0);}
              }
            };

            var sysalcargarplantilla =function(){
                EjercicioActivo();
                var aquicargaplantilla =_curaquicargaplantilla;
                aquicargaplantilla.attr('data-iduser',$('form-'+gbl_IDGUI).attr('data-iduser'));
                var tpl=aquicargaplantilla.attr('data-tpl');
                var idgui=$('input#idgui',aquicargaplantilla).val();
                var plantilla=$('.plantilla',aquicargaplantilla);
                plantilla.addClass('editando');
                var metodActual = _curmetod.attr('data-idmet');
                showBotonesguardardoc();
                if(metodActual==1){ //LOOK
                    if(plantilla.hasClass('plantilla_video')){
                        $('.btnselectedfile[data-tipo="video"]',plantilla).trigger('click');
                    }else if(plantilla.hasClass('plantilla_audio')){
                        $('.btnselectedfile[data-tipo="audio"]',plantilla).trigger('click');
                    }else if(plantilla.hasClass('plantilla_imagen_audio')){
                        $('.btnselectedfile.selimage',plantilla).trigger('click');
                    }else if(plantilla.hasClass('plantilla-dialogo')){
                        $('.btnselectedfile.selimage',plantilla).trigger('click');
                    }
                    plantilla.removeClass('editando');
                    showBotonesguardardoc();
                }else{
                    if(plantilla.hasClass('plantilla-completar')){
                         addBtnsEdicionMCE(aquicargaplantilla,true);
                    }else if(plantilla.hasClass('plantilla-verdad_falso')){
                        $('a.btn.add-premise',plantilla).trigger('click');
                        plantilla.removeClass('editando');
                        showBotonesguardardoc();
                    }else if(plantilla.hasClass('plantilla-fichas')){
                        initFichas(idgui);
                    }else if(plantilla.hasClass('plantilla-ordenar ord_simple')){
                        //mostrarBtnsDinamicos(idgui);
                    }else if(plantilla.hasClass('plantilla-ordenar ord_parrafo')){
                        //mostrarBtnsDinamicos(idgui);
                        if(metodActual == 'met-2') { var help_parr = true; }
                        else { var help_parr = false; }
                        initOrdenarParrafos(idgui, "en_US", help_parr);
                    }else if(plantilla.hasClass('plantilla-img_puntos')){
                        //mostrarBtnsDinamicos(idgui);
                    }else if(plantilla.hasClass('plantilla-speach')){ 
                        plantilla.removeClass('editando');
                        showBotonesguardardoc();
                    }else if(plantilla.hasClass('plantilla-nlsw')){ 
                        plantilla.removeClass('editando');
                        showBotonesguardardoc();
                    }
                    if(metodActual==3){
                         actualizarPanelesInfo();
                    }
                    acomodarvariablestpl();
                }

                showvideoayudatemplate(false);
                 $('.istooltip').tooltip();
            }
            var verautor=function(){
                EjercicioActivo();
                var iduserplantilla= _curaquicargaplantilla.find('#sysiduser').val();
                var iduserActual=$('#frm-'+gbl_IDGUI).attr('data-iduser');
                var user=$('#frm-'+gbl_IDGUI).attr('data-user');
                if(iduserplantilla==iduserActual||iduserplantilla==undefined){
                    _curdni=iduserActual;                   
                    $('.autor span#infoautor').html(user);
                }else{
                    if(_curdni==iduserplantilla)return false;
                    _curdni=iduserplantilla;
                    try{
                        var formData = new FormData();
                        formData.append("dni",iduserplantilla);
                        $.ajax({
                              url: _sysUrlBase_+'/usuario/buscarjson/',
                              type: "POST",
                              data:  formData,
                              contentType: false,
                              processData: false,
                              dataType :'json',
                              cache: false,
                              processData:false,
                              beforeSend: function(XMLHttpRequest){
                                $('.autor span#infoautor').html(MSJES_PHP.cargando+'...');
                              },
                              success: function(data){
                                if(data.code==='ok'){                                    
                                    var datainfo=data.data;                                   
                                    var nombre_full='';                              
                                    if(datainfo[0]!=undefined){                                        
                                        datainfo=datainfo[0];
                                        nombre_full=datainfo.ape_paterno+' '+datainfo.ape_materno+' '+datainfo.nombre;
                                    }else{                                       
                                        nombre_full=user;
                                        _curaquicargaplantilla.find('#sysiduser').val(iduserActual);
                                    }                                    
                                    $('.autor span#infoautor').html(nombre_full);
                                }else if(data.code===201){
                                    redir('');
                                }else{
                                    $('.autor span#infoautor').html('?');
                                }
                              },
                              error: function(e){
                                $('.autor span#infoautor').html('Error?');
                              }
                        });
                    }catch(ex){}
                }
            }

            var mostrarHabilidadesActivas = function(idPanelContenedor){
                EjercicioActivo();
                var cant_input_hidden = $(idPanelContenedor).find('input[type="hidden"].selected-skills').length;
                if( cant_input_hidden == 1) {
                    var input_hidden_value = $(idPanelContenedor).find('input[type="hidden"].selected-skills').val();
                } else {
                    var input_hidden_value = $(idPanelContenedor).find('.tab-content>.tab-pane.active input[type="hidden"].selected-skills').val();
                }
                var arr_values=[];
                if(input_hidden_value!=undefined && input_hidden_value!='') arr_values = input_hidden_value.split('|');
                $('.widget-box div.habilidades .btn-skill',objcont).each(function() {
                    $(this).removeClass('active');
                    var id_skill = $(this).data('id-skill');
                    $.each(arr_values, function(index, elem) {
                        $('.widget-box div.habilidades .btn-skill[data-id-skill="'+elem+'"]',objcont).addClass('active', {duration:200});
                    });
                });
            };

            var nuevoPanel = function(index, id_metodologia, nomb_metod){
                var identif = id_metodologia+'_'+index;
                var intentos = (id_metodologia==3)?' data-intentos="'+gbl_INTENTOS+'" ':'';
                return '<div class="row"> '+
                          '<div class="col-xs-12">'+
                            '<div class="nopreview" style="position: absolute; right: 1.5em; z-index:10">'+
                                '<div class="btn-close-tab istooltip" title="'+MSJES_PHP.remove_tab+'" data-id-tab="tab-'+nomb_metod+'-'+index+'">'+
                                      '<button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                '</div>'+
                            '</div>'+
                            '<div class="tab-title-zone pw1_'+id_metodologia+'" style="display:none;"> '+
                                '<div class="col-md-12">'+
                                    '<h3 class="addtext addtexttitulo2">'+
                                    MSJES_PHP.click_add_title+
                                    '</h3>'+
                                    '<input type="hidden" class="valin nopreview"  name="titulo['+id_metodologia+']['+index+']" data-nopreview=".addtexttitulo2">'+
                                '</div>'+
                                '<div class="space-line pw1_'+id_metodologia+'"></div>'+
                            '</div>'+
                            '<div class="tab-description pw1_'+id_metodologia+'" style="display:none;"> '+
                                '<div class="col-md-12">'+
                                    '<p class="addtext addtextdescription2'+id_metodologia+'">'+
                                    MSJES_PHP.click_add_description+
                                    '</p>'+
                                    '<input type="hidden" class="valin nopreview"  name="descripcion['+id_metodologia+']['+index+']" data-nopreview=".addtextdescription2'+id_metodologia+'" >'+
                                '</div>'+
                            '</div>'+
                            '<div class="aquicargaplantilla" id="tmppt_'+identif+'"></div>'+
                            '<input type="hidden" name="habilidades['+id_metodologia+']['+index+']" class="selected-skills" id="habilidades_met-'+identif+'" value="0">'+
                          '</div>'+
                        '</div>';
            };

            var crearPanelesInfo = function(){
              var _paneles = $('#paneles-info').clone();
              var _html = _paneles.html()
              $('#contenedor-paneles, .contenedor-paneles').append(_html);
              //$('#panel-tiempo input').mask('99:99'); //use /tema/js/jquery.maskedinput.min.js
              $('#panel-tiempo .info-show, .panel-tiempo .info-show').cronometroa0('');
            };

            var resetSettingTextbox = function(){
                $('#setting-textbox input[type="text"]').removeAttr('value').val('');
                $('#setting-textbox input.added').remove();
                $('#setting-textbox input[type="checkbox"]').prop( "checked", false );
                $('#setting-textbox .distractions').hide();
                $('#setting-textbox .assisted').hide();
            };

            var resetinfoTextbox=function(){
                resetSettingTextbox();
                $('#setting-textbox').hide('fast');
                $('#info-avance-alumno').removeClass('editandoinput').show('fast');
            }

            var saveSettingTextbox = function(){
                var response = {
                    'asistido'      : $('#setting-textbox .assisted input',objcont).is(':checked'),
                    'correcta'      : $('#setting-textbox .correct-ans input',objcont).val(),
                    'distraccion'   : [],
                };
                $('#setting-textbox .distractions input').each(function() {
                    if( $(this).val().trim()!='' ) response['distraccion'].push( $(this).val().trim() );
                });
                return response;
            };

            var estaIDRepetido = function(html, $input) {
                var repetido = false;
                var idInput = $input.attr('id');
                $(html).find('img.mce-object-input').not($input).each(function(i, el) {
                    var id = $(el).attr('id');
                    if(id == idInput){
                        repetido = true;
                        return false ;
                    }
                });
                return repetido;
            };

            var saveTextboxEdit=function(){
                var infotemplate=_curaquicargaplantilla;
                if(!infotemplate.find('.btnedithtml').hasClass('disabled')) return false;
                var response = saveSettingTextbox();
                tinyMCE.triggerSave();
                var acteditor=tinyMCE.activeEditor.getBody();
                var objenedit= $('#setting-textbox .correct-ans input').attr('data-objeto');
                var actobj=$(acteditor).find('._enedicion_');
                if(actobj.length>0){
                    var addclassinput=infotemplate.attr('data-addclass')||false;
                    actobj.attr('data-texto',response.correcta);
                    actobj.attr('data-mce-selected','1');
                    actobj.removeClass('_enedicion_ isdrop isclicked iswrite isayuda ischoice');
                    actobj.addClass(addclassinput);
                    if(response.asistido) actobj.addClass('isayuda');
                    if(response.distraccion){
                        var options=JSON.stringify(response.distraccion);
                        actobj.attr('data-options',options);                
                        var idtmp=actobj.attr('id');

                        if( typeof idtmp=='undefined' || idtmp=='' || estaIDRepetido(acteditor, actobj) ){
                            idtmp = 'id'+Date.now();
                        }

                        actobj.attr('id',idtmp);
                        jsonmultiplantilla[idtmp]={al:response.distraccion,ok:response.correcta};
                    }
                    var html=actobj.prop('outerHTML');
                    $(actobj).replaceWith(html);
                }
               resetinfoTextbox();
            };

            var actualizarPanelesInfo = function(){
                  EjercicioActivo();
                  var $tmplActiva = _curaquicargaplantilla;
                  var showTime = $tmplActiva.attr('data-showtime');
                  var showPuntaje = $tmplActiva.attr('data-showpuntaje');
                  var val_time = $tmplActiva.attr('data-time');
                  var val_puntaje = parseInt($tmplActiva.attr('data-puntaje'));

                  if( $('#contenedor-paneles #panel-tiempo, .contenedor-paneles .panel-tiempo').length==0 && $('#contenedor-paneles #panel-puntaje, .contenedor-paneles .panel-puntaje').length==0 ){
                    crearPanelesInfo();
                  }
                  $('#panel-tiempo input, .panel-tiempo input').mask('99:99');
                  $('#panel-tiempo input, .panel-tiempo input').attr('value', val_time).val(val_time);
                  $('#panel-puntaje input, .panel-puntaje input').attr('value', val_puntaje).val(val_puntaje);
                  $('#panel-tiempo .info-show, .panel-tiempo .info-show').text( $('#panel-tiempo input, .panel-tiempo input').val() );
                  $('#panel-puntaje .info-show, .panel-puntaje .info-show').text( $('#panel-puntaje input, .panel-puntaje input').val() );
                  if(showTime==0 || showTime==undefined){
                    $('#panel-tiempo, .panel-tiempo').addClass('hidden').hide();
                  } else {
                    $('#panel-tiempo, .panel-tiempo').removeClass('hidden').show().css('display','inline-block');
                  }

                  if(showPuntaje==0 || showPuntaje==undefined){
                    $('#panel-puntaje, .panel-puntaje').addClass('hidden').hide();
                  } else {
                    $('#panel-puntaje, .panel-puntaje').removeClass('hidden').show();
                  }

                  $tmplActiva.find('.plantilla').removeClass('tiempo-pausado').removeClass('tiempo-acabo').removeClass('ejerc-iniciado');

                  if($tmplActiva.find('input.mce-object-input').hasClass('isclicked')){
                    /* iniciar automat. sólo para Ejerc Alternativas */
                    iniciarTiempo(finTiempo);
                  }
            };

            var mostrarbtnremovetab=function(){
                $('.tab-pane.metod').each(function(ii,vv){
                    tabhijo=$(".tabhijo",this);
                    if(tabhijo.length==1){
                        $(".btn-close-tab",tabhijo).hide(0);
                      }else{
                        $(".btn-close-tab",tabhijo).show(0);
                    }
                });
            }

            var renombrarTabs = function(id_ulContenedor, cant_tabs){
                for(i = 1; i <= cant_tabs; i++){
                    $(id_ulContenedor + ' li:nth-child('+i+') a').html(i);
                }
                acomodarvariablestpl();
            };

            var acomodarvariablestpl=function(){
                $('.textosave',objcont).each(function(){
                    var idmet=$(this).closest('.metod').attr('id');
                    var iddetalle=$(this).attr('data-iddetalle');
                    var met=$('#'+idmet,objcont).attr('data-idmet');
                    var tpl=$(this).find('.aquicargaplantilla');
                    var atexto=$(this).attr("data-texto");
                    var idgui=$(this).find('input#idgui');
                    var iduser=$(this).find('input#sysiduser');
                    var dettipo=$(this).find('input#sysdettipo').val();
                    var iduserActual=$('#frm-'+gbl_IDGUI).attr('data-iduser');
                    var sysiduser=($(iduser).length==0)?iduserActual:iduser.val();
                    var sysidgui=($(idgui).length==0)?gbl_IDGUI:idgui.val();
                    var sysdettipo=(dettipo==''||dettipo==undefined)?$('ul.metodologias li a[href="#'+idmet+'"]'.objcont).text():dettipo;
                    var systipodesarrollo=$(tpl).attr('data-tpltitulo');
                    var txtords=atexto.split("_");
                    var orden=txtords.pop();
                    var inputs='<input type="hidden" id="sysidorden" name="orden['+met+']['+orden+']" value="'+orden+'">'
                                +'<input type="hidden" id="sysdettipo" name="det_tipo['+met+']['+orden+']" value="'+sysdettipo+'">'
                                +'<input type="hidden" id="systipodesarrollo" name="tipo_desarrollo['+met+']['+orden+']" value="'+systipodesarrollo+'">'
                                +'<input type="hidden" id="systipoactividad" name="tipo_actividad['+met+']['+orden+']" value="A">'
                                +'<input type="hidden" id="sysiduser" name="iduser['+met+']['+orden+']" value="'+sysiduser+'">'
                                +'<input type="hidden" id="idgui" name="idgui" value="'+sysidgui+'">'
                                +'<input type="hidden" id="iddetalle_'+met+'_'+orden+'" name="iddetalle['+met+']['+orden+']" value="'+iddetalle+'">';
                    $(tpl).attr('id','tmppt_'+met+'_'+orden);
                    $(tpl).siblings('.selected-skills').attr('id','habilidades_met-'+met+'_'+orden);
                    $(tpl).siblings('.selected-skills').attr('name','habilidades['+met+']['+orden+']');
                    $(tpl).children('input').remove();
                    validartxt=$(tpl).text();
                    $(tpl).find('h3.addtexttitulo2 input.valin').attr('name','titulo['+met+']['+orden+']');
                    $(tpl).find('p.addtextdescription22 input.valin',tpl).attr('name','titulo['+met+']['+orden+']');
                    if(tpl.length==0) return true;
                    $(tpl).prepend(inputs);

                 });
            }

            var reordenarplantillas=function(){ //reordenra antes de guardar
                $('.textosave',objcont).each(function(){
                    var idmet=$(this).closest('.metod').attr('id');
                    var met=$('#'+idmet,objcont).attr('data-idmet');
                    var tpl=$(this).find('.aquicargaplantilla');
                    var iddetalle=$(this).attr('data-iddetalle');
                    var atexto=$(this).attr("data-texto");
                    var idgui=$(this).find('input#idgui');
                    var iduser=$(this).find('input#sysiduser');
                    var dettipo=$(this).find('input#sysdettipo').val();
                    var iduserActual=$('#frm-'+gbl_IDGUI).attr('data-iduser');
                    var sysiduser=iduserActual;
                    var sysidgui=($(idgui).length==0)?gbl_IDGUI:idgui.val();
                    var sysdettipo=(dettipo==''||dettipo==undefined)?$('ul.metodologias li a[href="#'+idmet+'"]'.objcont).text():dettipo;
                    var systipodesarrollo=$(tpl).attr('data-tpltitulo');
                    var txtords=atexto.split("_");
                    var orden=txtords.pop();
                    var inputs='<input type="hidden" id="sysidorden" name="orden['+met+']['+orden+']" value="'+orden+'">'
                                +'<input type="hidden" id="sysdettipo" name="det_tipo['+met+']['+orden+']" value="'+sysdettipo+'">'
                                +'<input type="hidden" id="systipodesarrollo" name="tipo_desarrollo['+met+']['+orden+']" value="'+systipodesarrollo+'">'
                                +'<input type="hidden" id="systipoactividad" name="tipo_actividad['+met+']['+orden+']" value="A">'
                                +'<input type="hidden" id="sysiduser" name="iduser['+met+']['+orden+']" value="'+sysiduser+'">'
                                +'<input type="hidden" id="idgui" name="idgui" value="'+sysidgui+'">'
                                +'<input type="hidden" id="iddetalle_'+met+'_'+orden+'" name="iddetalle['+met+']['+orden+']" value="'+iddetalle+'">';
                    $(tpl).attr('id','tmppt_'+met+'_'+orden);
                    $(tpl).siblings('.selected-skills').attr('id','habilidades_met-'+met+'_'+orden);
                    $(tpl).siblings('.selected-skills').attr('name','habilidades['+met+']['+orden+']');
                    $(tpl).children('input').remove();
                    validartxt=$(tpl).text();
                    $(tpl).find('h3.addtexttitulo2 input.valin').attr('name','titulo['+met+']['+orden+']');
                    $(tpl).find('p.addtextdescription22 input.valin',tpl).attr('name','titulo['+met+']['+orden+']');
                    $(tpl).prepend(inputs);

                    var plantilla=$(this).find('.plantilla');
                    if(plantilla.hasClass('plantilla-completar')){
                        reiniciarInputsMCE(plantilla);
                        if($('.tpl_alternatives',plantilla).text()=='')$('.tpl_alternatives',plantilla).hide();
                    }else if(plantilla.hasClass('plantilla-fichas')){
                        $('.ficha',plantilla).removeClass('active corregido good bad');
                    }else if(plantilla.hasClass('plantilla-ordenar ord_simple')){
                        $('.generar-elem',plantilla).trigger('click');
                       /* var id = $(this).attr('id');
                        $('#'+id+' .generar-parrafo').trigger('click'); */
                    }
                    var html_=$(this).clone();
                    html_.find('.btn-toolbar').remove();
                    html_.find('#btns_control_alumno').remove();

                    $('#'+atexto+'_edit').val(html_.html());
                    html_.find('.btn-close-tab').remove();
                    html_.find('.plantilla').removeClass('editando');
                    $('.valin',html_).each(function(){
                        var vp=$(this).val();
                        $(this).attr('value', vp);
                        if(vp==''||vp==undefined) {
                            var dpview=$(this).attr('data-nopreview');
                            $(this).siblings(dpview).remove();
                        }
                        $(this).remove();
                    });
                     if(html_.find('.plantilla_video').length>0){
                        html_.find('.addtextvideo').removeClass('.addtextvideo');
                     }

                    if( html_.find('.plantilla_imagen_audio').length>0){
                        var data_audio = html_.find('.plantilla_imagen_audio .playaudioimagen').attr('data-audio');
                        if(data_audio!=undefined && data_audio!=''){
                            html_.find('.plantilla_imagen_audio .playaudioimagen').removeAttr('style').removeClass('hidden');
                        }else{
                            html_.find('.plantilla_imagen_audio .playaudioimagen').remove();
                        }
                    }
                    if(html_.find('.plantilla-dialogo').length>0){
                        html_.find('.plantilla-dialogo .dialogue-pagination>ul>li').removeClass('active');
                        html_.find('.plantilla-dialogo .dialogue-pagination>ul>li:first').addClass('active');
                        html_.find('.plantilla-dialogo .tab-pane').removeClass('active');
                        html_.find('.plantilla-dialogo .tab-pane:first').addClass('active in');
                        html_.find('.plantilla-dialogo .tab-pane.active .d-box[data-orden="1"]').css('display','none');
                    }
                    html_.find('.nopreview').remove();
                    $('div.tooltip.fade[role="tooltip"]').remove();
                    if(validartxt.trim()!='')
                    $('#'+atexto).val(html_.html());
                });
            }

            var jAlert = null;
            var mostrarCargando = function(){
                $('body .btnsaveActividad').attr('disabled', 'disabled');
                var date = Date.now();
                jAlert = $.alert({
                    title: '',
                    content: '<div class="text-center" id="alert'+date+'"><div><i class="fa fa-cog fa-spin fa-4x"></i></div> <span>' + MSJES_PHP.cargando + '...</span></div>',
                    confirmButtonClass: 'hidden',
                    cancelButtonClass: 'hidden'
                });
            };
            var ocultarCargando = function () {
                $('body .btnsaveActividad').removeAttr('disabled');
                jAlert.close();
                jAlert = null;
            };

           

            var guardaractividaddetalle=function(params){
              var msjeAttention = params.msje.attention;
              var IDGUI = gbl_IDGUI;
              if(editableejercicio==false) return; // no como vista de alumno
                var ee=EjercicioActivo();
                var idMet=_curmetod.attr('data-idmet');                
              var formData = new FormData();
              var iddetalle=0;
              if(idMet=="1") {
                var $form = _curmetod;
                $('#infoautor').text($("#frm-"+gbl_IDGUI).attr('data-user')); 
                iddetalle=$form.children('.textosave').attr('data-iddetalle');
              } else {
                var $form = _curmetod.find('.tabhijo.active');
                $('#infoautor').text($("#frm-"+gbl_IDGUI).attr('data-user'));
                iddetalle=$form.children('.textosave').attr('data-iddetalle');
              }
              if(iddetalle==0 || iddetalle==undefined);
              iddetalle = $form.find('input[name^="iddetalle"]').val()=='undefined'?'':$form.find('input[name^="iddetalle"]').val();
              formData.append('titulo' , $form.find('.tab-title-zone .addtext').text());
              formData.append('descripcion' , $form.find('.tab-description p.addtext').text());
              formData.append('txtNivel' , $('#txtNivel').val());
              formData.append('txtunidad' , $('#txtunidad').val());
              formData.append('txtSesion' , $('#txtsesion').val());
              formData.append('metodologias' , idMet);
              formData.append('habilidades' , $form.find('input.selected-skills').val());
              formData.append('det_tipo' , $('.nav.metodologias li.active a').text());
              formData.append('det_orden' , $form.find('#sysidorden').val());
              formData.append('det_tipo_desarrollo' , $form.find('#systipodesarrollo').val());
              formData.append('det_tipo_actividad' , $form.find('#systipoactividad').val());
              formData.append('det_users' , $form.find('#sysiduser').val());
              formData.append('iddetalle' , iddetalle);
              formData.append('det_texto' , $form.find('textarea[name^="det_texto"]').val());
              formData.append('det_texto_edit' , $form.find('textarea[name^="det_texto_edit"]').val());
              formData.append('det_url' , '');

              $.ajax({
                url: _sysUrlBase_+'/actividad/guardar',
                type: "POST",
                data:  formData,
                contentType: false,
                dataType :'json',
                cache: false,
                processData:false,
                beforeSend: function(XMLHttpRequest){
                   mostrarCargando();
                },
                success: function(data)
                {
                  if(data.code==='ok'){
                    mostrar_notificacion(msjeAttention, data.msj, 'success');
                    newides=data.newides;
                    /*$.each(newides,function(i,v){
                        $.each(v,function(ii,vv){
                           var input=$('input#iddetalle_'+i+'_'+ii);
                           $(input).val(vv);
                           $(input).closest('.textosave').attr('data-iddetalle',vv);
                        });
                    });*/
                    var input=$form.find('input[name^="iddetalle"]');
                    $(input).val(newides);
                    $(input).closest('.textosave').attr('data-iddetalle',newides);
                    if(params.callback!=undefined)params.callback();
                    ocultarCargando();
                  }else{
                    mostrar_notificacion(msjeAttention, data.msj, 'warning');
                  }
                  $('#procesando').hide('fast');
                  return false;
                },
                error: function(xhr,status,error){
                  mostrar_notificacion(msjeAttention, status, 'warning');
                  $('#procesando').hide('fast');
                  return false;
                }
              });
            };

            var calcularBarraProgreso = function(){
                EjercicioActivo() ;
                var $ul=$('ul.ejercicios',_curmetod);
                var $li = $ul.find('li');
                var nli=$li.length-1;
                if(_curmetod.find('.tabhijo').length==0){
                    $('#panel-barraprogreso, .panel-barraprogreso').hide();
                }
                _curmetod.find('.tabhijo').each(function(index, tabHijo) {

                    var porcProgresoEjerc = $(tabHijo).attr('data-progreso');
                    porcProgresoEjerc = (porcProgresoEjerc!=undefined&&porcProgresoEjerc!='')?parseFloat(porcProgresoEjerc):0.00;
                    var idmet=_curmetod.attr('data-idmet');
                    var porcXejerc=100/nli;
                    var $plantilla = $(tabHijo).find('.plantilla');
                    if($plantilla.hasClass('plantilla-completar')){
                        var cantTodos = $plantilla.find('.panelEjercicio input.mce-object-input').length;
                        var cantCorrectos = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="good"]').length;
                    } else if($plantilla.hasClass('plantilla-verdad_falso')){
                        var cantTodos = $plantilla.find('.list-premises .premise').length;
                        var cantCorrectos = $plantilla.find('.list-premises *[data-corregido="good"]').length;
                    } else if($plantilla.hasClass('plantilla-fichas')){
                        var cantTodos = $plantilla.find('.partes-2 .ficha').length;
                        var cantCorrectos = $plantilla.find('.partes-2 .ficha.corregido.good').length;
                    } else if($plantilla.hasClass('plantilla-ordenar ord_simple')){
                        var cantTodos = $plantilla.find('.drop .parte.blank').length;
                        var cantCorrectos = $plantilla.find('.drop .parte.blank.good').length;
                    } else if($plantilla.hasClass('plantilla-ordenar ord_parrafo')){
                        var cantTodos = $plantilla.find('.drop-parr div:not(.fixed)').length;
                        var cantCorrectos = $plantilla.find('.drop-parr .filled.good').length;
                    } else if($plantilla.hasClass('plantilla-img_puntos')){
                        var cantTodos = $plantilla.find('.mask-dots .dot-container .dot-tag').length;
                        var cantCorrectos = $plantilla.find('.mask-dots .dot-container .dot-tag.good').length;
                    } else if($plantilla.hasClass('plantilla-speach')){
                        var $txthablado=$plantilla.find('.speachtexttotal')
                        var cantTodos = $txthablado.attr('total-palabras')||1;
                        var cantCorrectos = $txthablado.attr('total-ok')||0;                        
                    } else if($plantilla.hasClass('plantilla-nlsw')){
                        var $txthablado=$plantilla.find('.respuestaAlumno')
                        var cantTodos = $plantilla.attr('total-palabras')||1;
                        var cantCorrectos = $txthablado.attr('total-ok')||0;                        
                    }
                    var porcAcum = (cantCorrectos*porcXejerc)/cantTodos;
                    var newPorc = (porcAcum).toFixed(5);
                    $(tabHijo).attr('data-progreso', newPorc);

                    var progresoGeneral = 0.00;
                    $li.each(function() {
                        var idTab = $(this).find('a').attr('href');
                        var progreso = $(idTab).attr('data-progreso');
                        progreso = (progreso!=undefined && progreso!='' && !isNaN(progreso))?parseFloat(progreso):0.00;
                        progresoGeneral += progreso;
                    });

                    var newProgrGnral = progresoGeneral.toFixed(1);
                    newProgrGnral = (newProgrGnral>100)? 100.0:newProgrGnral;
                    _curmetod.attr('data-progreso', newProgrGnral);
                    if(_cureditable==false && idmet>1){
                        $('#panel-barraprogreso .progress-bar, .panel-barraprogreso .progress-bar').css('width',newProgrGnral+'%');
                        $('#panel-barraprogreso .progress-bar span, .panel-barraprogreso .progress-bar span').text(newProgrGnral);
                        $('#panel-barraprogreso, .panel-barraprogreso').show();
                    }else{
                        $('#panel-barraprogreso, .panel-barraprogreso').hide();
                    }
                });
            };

            var showliminimo=function(){
                EjercicioActivo() ;
                $ul=$('ul.ejercicios',_curmetod);
                $li=$ul.find('li');
                nli=$li.length-1;
                var idmet=_curmetod.attr('data-idmet');
                index=$ul.find('li.active').text();
                var porc=(index*100)/nli;

                if(nli>5){
                    if(_cureditable==false) {
                        $('#contenedor-paneles #panel-ejericiosinfo, .contenedor-paneles .panel-ejericiosinfo').hide('fast');
                         $li.hide('fast');

                        return false;
                      }else{
                        $('#panel-barraprogreso, .panel-barraprogreso').hide();
                      }
                    $('#contenedor-paneles #panel-ejericiosinfo, .contenedor-paneles .panel-ejericiosinfo').show('fast');
                    $li.hide('fast');
                    if(_cureditable==true) $ul.find('li.btn-Add-Tab').show('fast');
                    $('#panel-ejericiosinfo .actual, .panel-ejericiosinfo .actual').text(index+"/"+nli);
                }else{
                    if(_cureditable==false){

                        return false;
                    }else{
                        $('#panel-barraprogreso').hide();
                    }
                    $('#contenedor-paneles #panel-ejericiosinfo, .contenedor-paneles .panel-ejericiosinfo').hide('fast');
                    $li.show('fast');
                }
            };

            var comprobarEjercCompleto = function(flag){
                objcont.find('.aquicargaplantilla').each(function() {
                    if(flag){ /* esta en preview */
                        var existePlantilla = $(this).find('.plantilla').length;
                        if(!existePlantilla){
                            $(this).html('<div class="template-not-found"><center>'+
                                '<h2><i class="fa fa-exclamation-circle fa-4x color-red"></i><br>'+MSJES_PHP.ejerc_no_completado+'.</h2>'+
                                '<div class="row" id="btns_control_alumno">'+
                                '<div class="col-sm-12 botones-control"><br>'+
                                '<br><a class="btn btn-inline btn-success btn-lg save-progreso" style="display: inline;">'+
                                'Continue <i class="fa fa-arrow-right"></i></a>'+
                                '</div></div></center></div>');
                        }
                    }else{ /* esta en back */
                        $(this).find('.template-not-found').remove();
                    }
                });
            };


//eventos generales de edicion;
            objcont.on('click', '.addtext', function(e){
                if(_cureditable==false)return false;
                $(this).css({"border": "0px"});
                addtext1(e,this);
            }).on('blur','.addtext>input',function(e){
                    e.preventDefault();
                    $(this).closest('.addtext').removeAttr('Style');
                    addtext1blur(e,this);
            }).on('keypress','.addtext>input', function(e){
                    if(e.which == 13){ //Enter pressed
                      e.preventDefault();
                      $(this).trigger('blur');
                    }
            }).on('keyup','input', function(e){               
                    if(e.which == 27){ //Esc pressed
                      $(this).attr('data-esc',"1");
                      $(this).trigger('blur');
                    }
            }).on('click','.btnchoiceplantilla',function(ev){
                ev.preventDefault();
                ev.stopPropagation();
                verplantillas(true);
            }).on('click','ul.nav-tabs.ejercicios li a',function(ev){
                if($(this).parent('li').hasClass('disabled')){
                    ev.preventDefault();
                    ev.stopPropagation();
                    return false;
                }
            }).on('shown.bs.tab', 'ul.nav-tabs.ejercicios li a', function (ev) {
                    ev.preventDefault();
                    EjercicioActivo();                   
                    var panelShown = $(this).attr('href');
                    mostrarHabilidadesActivas(panelShown);
                    if(_curaquicargaplantilla.is(':empty')){
                        $('#btns-guardar-actividad').hide();
                        verplantillas();
                    } else {
                        $('#btns-guardar-actividad').show();
                    }
                    DBY_mostrar_ocultarBtnProgreso();
                    showBotonesguardardoc();
                    showliminimo();
                    mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
                    actualizarPanelesInfo(); /* actividad_completar.js */
                    verautor();
                    rinittemplatealshow();
                    $('audio').each(function(){ this.pause() });
                    $('video').each(function(){ this.pause() });
                    resetEdicion_variables();
                    if($('#smartbook-ver').length==0)
                    rinittemplate_nlsw(true);
            }).on('click','ul.nav-tabs.ejercicios li.btn-Add-Tab a', function(ev){
                ev.preventDefault();
                var method=$(this).closest('.metod.active');
                var id_ulContenedor= '#' + $(this).parent().parent().attr('id');
                var id_PanelesContenedor = '#' + $(id_ulContenedor).parent().next().find('.tab-content').attr('id');
                var nombre_metodologia = $(id_ulContenedor).attr('data-metod');
                var id_metodologia = $(id_ulContenedor).attr('data-id-metod');
                var ultimo_tab = $(this).parent();
                var cant_tab = $(id_ulContenedor+' li').length;
                if(cant_tab == 1) { index_ultimo_tab = 2; } else { index_ultimo_tab = cant_tab;} //Si Solo queda boton Add Tab
                var a_href_tab = $(id_ulContenedor+' li:nth-child('+(index_ultimo_tab-1)+') a').attr('href').split('-'); //#tab-Metodologia-ultimoIndex
                var index_NewTab = parseInt(a_href_tab[2]) + 1; //ultimoIndex + 1
                /* agregando nuevo tab */
                var newTab = '<li> <a href="#tab-'+nombre_metodologia+'-'+index_NewTab+'" data-toggle="tab">'+ cant_tab +'</a> </li>';
                ultimo_tab.before(newTab);

                /* agregando Tab Panel */
                var newPanel = '<div id="tab-'+nombre_metodologia+'-'+index_NewTab+'" class="tabhijo tab-pane fade " >'+
                                 '<div class="textosave" data-texto="tptext_'+id_metodologia+'_'+index_NewTab+'" >'+
                                    nuevoPanel(index_NewTab, id_metodologia, nombre_metodologia) +
                                '</div><textarea name="det_texto['+id_metodologia+']['+index_NewTab+']" id="tptext_'+id_metodologia+'_'+index_NewTab+'" class="hidden">'+
                                '</textarea>'+
                                '<textarea name="det_texto_edit['+id_metodologia+']['+index_NewTab+']" id="tptext_'+id_metodologia+'_'+index_NewTab+'_edit" class="hidden"></textarea>'+
                                '</div>';
                $(id_PanelesContenedor).append(newPanel);
                //actualizarplantillas();
                /* mostrar Tab agregado*/
                $(id_ulContenedor+' a[href="#tab-'+nombre_metodologia+'-'+index_NewTab+'"]').tab('show');
            }).on('shown.bs.tab', 'ul.metodologias li a', function (ev) {
                ev.preventDefault();
                EjercicioActivo();
                var href=$(this).attr('href');
                var panelShown = '#'+href.replace('#', '');
                mostrarHabilidadesActivas(panelShown);                
                if(_cureditable==true){
                    if($(_curaquicargaplantilla).is(':empty')) { /*si está vacío*/
                        $('#btns-guardar-actividad').hide();
                        verplantillas(true);
                    }else{
                       showBotonesguardardoc();                     
                       $('.showonpreview_nlsw').hide().addClass('hidden');
                    }
                }

                showliminimo();
                calcularBarraProgreso();
                mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
                actualizarPanelesInfo(); /* actividad_completar.js */
                verautor();
                rinittemplatealshow();
                $('audio').each(function(){ this.pause() });
                $('video').each(function(){ this.pause() });
                resetEdicion_variables();
            }).on('click', 'div.habilidades .btn-skill', function(e){
                if(_cureditable==false) return false;
                EjercicioActivo();
                var id_TabMetodologia = '#'+_curmetod.attr('id');
                var cant_input_hidden = $('input[type="hidden"].selected-skills',_curmetod).length;
                var skillenejericio = $('input[type="hidden"].selected-skills',_curejercicio);
                var met=_curmetod.attr('data-idmet');
                var tpl=_curaquicargaplantilla;
                var atexto=_curaquicargaplantilla.closest('.textosave').attr("data-texto");
                var txtords=atexto.split("_");
                var orden=txtords.pop();
                $(tpl).attr('id','tmppt_'+met+'_'+orden);
                $(tpl).siblings('.selected-skills').attr('id','habilidades_met-'+met+'_'+orden);
                if(cant_input_hidden>=1 ){ //si btn.preview es Visible ó si hay al menos 1 Input Hidden en panel activo
                    var id_skill = $(this).data('id-skill');
                    $(this).toggleClass('active');
                    var id_tab_metod =_curmetod.attr('id');
                    if(id_tab_metod == 'met-1') var index_tab = '1';
                    else var index_tab = _curmetod.find('ul.nav-tabs>li.active>a').text();
                    var input_hidden =  _curmetod.find('input#habilidades_'+id_tab_metod+'_'+index_tab);
                    var input_hidden_val = input_hidden.val().trim()||'';

                    if( $(this).hasClass('active') ){
                        if( input_hidden_val == '0'){ input_hidden_val = id_skill; }
                        else{ input_hidden_val += '|'+id_skill; }
                    } else {
                        arrValues = input_hidden_val.split('|');
                        arrValues = jQuery.grep(arrValues, function(value) {//encuentra elemento de arrValues , segun filtro
                            return value != id_skill; //filtra todos los values diferentes de Id_Skill
                        });
                        input_hidden_val = arrValues.join( "|" );
                        if(input_hidden_val=='') input_hidden_val = '0';
                    }
                    input_hidden.val(input_hidden_val);
                }
            }).on('click',".btnselectedfile",function(ev){
                ev.preventDefault();
                var txt=MSJES_PHP.select_upload;
                selectedfile(ev,this,txt);
            }).on('click','.btnsaveActividad',function(ev){
                $('#isSaved').val("y");
                ev.preventDefault();
                ev.stopPropagation();
                $('#togglePrev').removeClass('hidden');
                $('.showonpreview_nlsw').show().removeClass('hidden');
                try{
                    resetImagen_Audio();
                    resetDialogo();
                    resetFichas();
                    resetImgPuntos();
                    resetOrdenar();
                    resetVerdadFalso(); vistaEdicionVoF();
                    //previewspeach(true);
                    rinittemplate_nlsw('saveedit');
                    
                    /*resetAlternativas();
                    previewDBYself();*/                    
                    reordenarplantillas();
                    var paramsPHP = {'idgui' : gbl_IDGUI,'msje' : { 'attention' : MSJES_PHP.attention,},callback:function(){
                        $('.showonpreview_nlsw').hide().addClass('hidden');
                    }};
                    guardaractividaddetalle(paramsPHP); /* editactividad.js */
                    //$('.back').trigger('click');
                    if( $(this).hasClass('continue') ) {
                        $('.btn-Add-Tab>a',_curmetod).trigger('click');
                    }
                    $('.showonpreview_nlsw').hide().addClass('hidden');
                }catch(err){
                    mostrar_notificacion(MSJES_PHP.attention,err,'warning');
                }
            }).on('click','.preview',function(ev){
                $('#togglePrev').removeClass('hidden');
                ev.preventDefault();
                ev.stopPropagation();
                $(this).hide().removeClass('active');
                $('.nopreview').hide();
                $('.showonpreview').show();
                $('.showonpreview_nlsw').show().removeClass('hidden');
                tabcontent=objcont.find('.tab-content');
                reiniciarInputsMCE(tabcontent);
                enpreview=true;
                _cureditable=false;
                inicio();
                $('#btns-guardar-actividad',objcont).show('fast');
                $('#btns-guardar-actividad .btnsaveActividad',objcont).hide('fast');
                $('.back',objcont).show('fast');
                resetImagen_Audio();
                resetDialogo();
                resetFichas();
                resetImgPuntos();
                resetOrdenar();
                resetVerdadFalso();
                //previewspeach(true);
                if($('#smartbook-ver').length==0)
                rinittemplate_nlsw(true);
                comprobarEjercCompleto(true);
                objcont.find('.isdragable').removeClass('hidden hiddenok');
               // mostrarPanelIntentos(); /*static/js/actividad_completar.js*/

            }).on('click','.back',function(ev){
                ev.preventDefault();
                $('#togglePrev').addClass('hidden');
                $(this).hide();
                $('.preview',objcont).addClass('active').show('fast');
                $('.nopreview').show();
                $('.showonpreview').hide();
                $('.showonpreview_nlsw').hide().addClass('hidden');
                $tabhijo=$('.aquicargaplantilla',objcont);
                objcont.find('.isdragable').removeClass('hidden hiddenok');
                resetOrdenar();
                $.each($tabhijo,function(i,obj){
                    var plantilla=$(obj).find('.plantilla');
                    if(plantilla.hasClass('plantilla-dialogo')){
                        $('.dialogue-pagination>ul>li:first>a',plantilla).trigger('click');
                        $('.d-box').show();
                    }else if(plantilla.hasClass('plantilla-fichas'))  $('.ficha',plantilla).removeClass('active corregido good bad');
                });
                backDBY();
                resetImgPuntos();
                 enpreview=false;
                _cureditable=true;
                inicio();
                $('.valvideo',objcont).each(function() {
                    var src = $(this).attr('src');
                    if(src==''||src==undefined) {
                        $(this).siblings('img').show();
                        $(this).hide();
                    } else {
                        $(this).siblings('img').hide();
                        $(this).show();
                    }
                });
                vistaEdicionVoF();
                //_previewspeach(false);
                comprobarEjercCompleto(false);
                if($('#smartbook-ver').length==0)
                rinittemplate_nlsw(false);
                 // mostrarPanelIntentos(); /*static/js/actividad_completar.js*/
            }).on('click','#panel-ejericiosinfo .eje-btnanterior',function(ev){
                $ul=$('ul.ejercicios',_curmetod);
                $ul.find('li.active').prev('li').find('a').trigger('click',true);
            }).on('click','#panel-ejericiosinfo .eje-btnsiguiente',function(ev){
                $ul=$('ul.ejercicios',_curmetod);
                $ul.find('li.active').next('li').find('a').trigger('click',true);
            }).on('click', '.tab-content .tab-pane .btn-close-tab', function(e){
                e.preventDefault();
                var idhijo=$(this).closest('.tabhijo.active').attr('id');
                var method=$(this).closest('.metod.active');
                var ulcontent=method.find('ul.ejercicios');
                var idulcontent='#'+ulcontent.attr('id');
                var index=$('li',ulcontent).index($('li.active',ulcontent));
                var idDetalle = $(this).closest('.textosave').attr('data-iddetalle') || '';
                var cant_tab = ($('li',ulcontent).not($('li.btn-Add-Tab')).length) - 1; /* -1: el eliminado*/
                $.confirm({
                    title: 'Eliminar ejercicio',
                    content: '¿Está seguro que desea eliminar este ejercicio?',
                    confirm: function(){
                        if(cant_tab>=1){
                            if(idDetalle==''){
                                $('#'+idhijo).remove();
                                $('li.active',ulcontent).remove();
                                renombrarTabs(idulcontent, cant_tab);
                                index=index==cant_tab?index-1:index;
                                $(' li:eq('+index+') a',ulcontent).tab('show');
                                if(cant_tab==1){
                                    method.find('.tab-title-zone .btn-close-tab').hide('fast');
                                };
                                mostrarbtnremovetab();
                                acomodarvariablestpl();
                            } else {
                                $.ajax({
                                    url: _sysUrlBase_+'/actividad/jxEliminar',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: {'iddetalle': idDetalle},
                                }).done(function(resp){
                                    window.location=window.location;

                                    /*if(resp.code=='ok') {                                        
                                        $('#'+idhijo).remove();
                                        $('li.active',ulcontent).remove();
                                        $.each(resp.data, function(idDetalle, orden) {
                                            var datatexto = $('.textosave[data-iddetalle="'+idDetalle+'"]').attr('data-texto');
                                            var arrDataTexto = datatexto.split('_');
                                            arrDataTexto.pop();
                                            arrDataTexto.push(orden);
                                            datatexto = arrDataTexto.join('_');
                                            $('.textosave[data-iddetalle="'+idDetalle+'"]').attr('data-texto', datatexto);
                                        });
                                        renombrarTabs(idulcontent, cant_tab);
                                        index=index==cant_tab?index-1:index;
                                        $(' li:eq('+index+') a',ulcontent).tab('show');
                                        if(cant_tab==1){
                                            method.find('.tab-title-zone .btn-close-tab').hide('fast');
                                        };
                                        mostrarbtnremovetab();
                                        acomodarvariablestpl();
                                    } else {
                                        mostrar_notificacion(MSJES_PHP.attention, resp.msj, 'error');
                                    }*/
                                }).fail(function(err) {
                                    console.log("error" , err);
                                }).always(function() { });
                            }
                        }
                    },
                });
            });

            objcont.on('click', '.btn.try-again', function(ev){
                ev.preventDefault();
                ev.stopPropagation();
                var $tmplActiva = $(this).closest('.aquicargaplantilla');
                var $plantActiva = $tmplActiva.find('.plantilla');
                //// nuevo para intentos
                //$('#panel-tiempo .info-show').trigger('oncropausar');
                if(!$plantActiva.hasClass('yalointento'))$plantActiva.addClass('yalointento');                
                 $('.btnejercicio-next_').removeClass('disabled').removeAttr('disabled');
                /// finde intentos 
                var idPlant = $plantActiva.attr('id');                
                if($plantActiva.hasClass('plantilla-completar')){
                    var idgui=$plantActiva.attr('data-idgui'); 
                    console.log(idgui);
                    if($plantActiva.find('input.isclicked').length>0)
                        resetEjercicio['tipo_2'](idgui);
                    else{
                      
                       resetEjercicio['tipo_1'](idgui);
                    }
                    $plantActiva.find('.isdragable').removeClass('hidden hiddenok');
                }else if($plantActiva.hasClass('plantilla-nlsw')){
                    var puedeReiniciar = aumentarIntentos();
                    var rptalumn=$plantActiva.find('.respuestaAlumno');
                        rptalumn.children('.msjRespuesta').html('');
                    var textarea=rptalumn.find('textarea.txtrespuestaAlumno');
                    var idtexarea=textarea.attr('id');
                    if(puedeReiniciar){                       
                        if(rptalumn.find('.mce-tinymce').length){
                            tinyMCE.get(idtexarea).setContent('');
                            textarea.val('');
                        }
                    }else {
                        rptalumn.find('.showonpreview_nlsw').addClass('hidden');
                        textarea.attr('readonly',true);
                    }
                
                //@edu se agrego bloque else if - inicio
                }else if($plantActiva.hasClass('plantilla-img_puntos')){
                    let tipoPlant = 'img_puntos';
                    resetActividadDBY(tipoPlant, idPlant); //fin

                }else{                    
                    var tipoPlant = $plantActiva.data('tipo-tmp')||'';                  
                    if(tipoPlant==''){
                        if($plantActiva.hasClass('ord_parrafo')) tipoPlant='ordenar_parrafo';
                        else if($plantActiva.hasClass('ord_simple')) tipoPlant='ordenar_simple';
                    }
                  
                    if(tipoPlant=='speach'){
                        if($plantActiva.length)
                        var btn=$plantActiva.find('.btnGrabarAudio');
                        if(btn.children('i.fa-microphone'))
                        if(!btn.hasClass('hide')){
                            btn.trigger('click');
                        }
                    }
                    resetActividadDBY(tipoPlant, idPlant);
                }
              }).on('click', '.btn.save-progreso', function(e){
                  //console.log('guardarprogreso');
                    e.preventDefault();
                    var btn=$(this);
                    e.stopPropagation();
                    //calcularBarraProgreso();                
                    guardarProgreso();                   
                    var metod= btn.closest('.metod.active');
                    var idMetod=metod.attr('id');
                    var idmet=metod.attr('data-idmet'); 
                    var haycuaderno=btn.closest('#cuaderno');               
                    if(idmet=='1')
                        if(haycuaderno.length>0){
                            $('a[href="#div_practice"]').trigger('click');
                        }else{
                            $('ul.metodologias a[href="#met-2"]').trigger('click');
                        }
                    else{
                        if(haycuaderno.length>0){
                            $('.btnejercicio-next_').trigger('click');
                        }else{
                            var actual_tab = $('#'+idMetod +' ul.ejercicios li.active');
                            var next_tab = actual_tab.next('li').find('a');
                            if(next_tab.length>0 && !next_tab.parent().hasClass('btn-Add-Tab')){
                                next_tab.trigger('click', true);
                            }else{
                                actual_tab=$('ul.metodologias li.active');
                                var next_tab = actual_tab.nextAll('li:visible').find('a');
                                if(next_tab.length>0){
                                    var href=next_tab.attr('href').replace(/#/g, "");
                                    next_tab.trigger('click', true);
                                    //$('#'+href+' ul.ejercicios li:first').trigger('click',true);
                                }
                            }
                        }
                    }
                    //rinittemplate_nlsw(true);                            
              });


            /*************Eventos de plantillas  ********************/
////////////plantilla look_video_texto edicion
            objcont.on("click",".plantilla_video #txtdialogocole .removedialog", function(){
                $(this).parents('tr').remove();
            }).on('click','.plantilla_video #txtdialogocole .addtextvideo', function(ev){
                if(_cureditable==true){
                  var in_=$(this);
                  var texto_inicial = $(this).text().trim();
                  $(this).attr('data-initial_val', texto_inicial);
                  var title=$(this).attr('title');
                  in_.html('<input type="text" required="required" class="form-control" id="inputEditTexto" placeholder="'+title+'" value="'+ texto_inicial +'">');
                  if($(this).hasClass("addtime")){
                    $('input#inputEditTexto',in_).addClass('addtime').attr('placeholder','00:00');
                    $('input#inputEditTexto',in_).mask('99:99');
                  }
                  $('input#inputEditTexto',in_).focus().select();
              }
            }).on('blur','.plantilla_video #txtdialogocole input#inputEditTexto',function(){
                var dataesc=$(this).attr('data-esc');
                var vini=$(this).parent().attr('data-initial_val');
                var vin=$(this).val();
                if(dataesc||vin=='') _intxt=vini;
                else _intxt=vin;
                $(this).removeAttr('data-esc');
                $(this).parent().attr('data-initial_val',_intxt);
                if($(this).hasClass("addtime")){
                  var time=_intxt.split(":");
                  var h=0,m=0,s=0;
                  if(time.length==3){h=parseInt(time[0]);m=parseInt(time[1]);s=parseInt(time[2])}
                  if(time.length==2){m=parseInt(time[0]);s=parseInt(time[1])}
                  if(time.length==1){s=parseInt(time[0])}
                    s=(h*3600)+(m*60)+s;
                  var parent = $(this).parent().parent().parent().parent().attr('data-time',s);
                }
                $(this).parent().text(_intxt);
            }).on('keypress','.plantilla_video #txtdialogocole input#inputEditTexto', function(e){
                if(e.which == 13){
                  e.preventDefault();
                  $(this).trigger('blur');
                }
            }).on('keyup','.plantilla_video #txtdialogocole input#inputEditTexto', function(e){
                if(e.which == 27){
                  $(this).attr('data-esc',"1");
                  $(this).trigger('blur');
                }
            }).on('click','.plantilla_video #txtdialogocole .playvideo',function(){
                if(_cureditable==true)  return false;
                var time=$(this).attr('data-time');
                var idmet=_curmetod.attr('data-idmet');
                var idgui=$('input#idgui',_curejercicio).val();
                var $video = $('#vid222',_curejercicio);
                if($video.attr('src')!=''){
                  $('#vid222').trigger('stop');
                  $('#vid222')[0].currentTime=time;
                  $('#vid222').trigger('play');
                }
            }).on('click','.plantilla_video .texto-video .addclone',function(){
                var de=$(this).attr('data-source');
                var adonde=$(this).attr('data-donde');
                var clon=$(de).clone();
                $(adonde).append('<tr data-time="0" class="playvideo">'+clon.html()+'</tr>');
            }); 

            objcont.on('click','.plantilla-dialogo #btn-AddPage a', function(e) {
                e.preventDefault();
                if( $(this).hasClass('disabled') ) return false;
                var contenedor = ".dialogue-pagination>ul.nav-pills";
                var idTab = "page_";
                var index_tab = $(contenedor+' li',objcont).length;
                var now = Date.now();
                var src_ultima_img = $('.dialogue-container #page_'+now+' .image>img',objcont).attr('src');
                var newTab= '<li><a href="#'+idTab+now+'" data-toggle="pill">'+index_tab+'</a></li>';
                $('#btn-AddPage',objcont).before(newTab);
                var met=$('.metod.active').attr('data-idmet');
                var idgui=$('.metod.active .aquicargaplantilla',objcont).find('#idgui').val();
                var htmlpanel=$('.dialogue-container .tpanedialog:last-child',objcont).clone(true,true);
                htmlpanel.find('.d-box.left').removeAttr('class').addClass('d-box left box1_'+met+'_'+now);
                htmlpanel.find('.d-box.left').removeAttr('data-audio');
                htmlpanel.find('.d-box.left .d-bubble').attr('id','dialogo-texto_1_'+now);
                htmlpanel.find('.d-box.left .d-bubble .addtext').text('Click here to Add text');
                htmlpanel.find('.d-box.left .d-bubble input.valin').attr('data-nopreview','box1_'+met+'_'+now);
                htmlpanel.find('.d-box.left .btnselectedfile[data-tipo="audio"]').attr('data-url','.box1_'+met+'_'+now);
                htmlpanel.find('.dialogue-image .btnselectedfile').attr('data-url','.img_1'+met+'_'+idgui+'_'+now);
                htmlpanel.find('.dialogue-image .image img').attr('id','img_1'+met+'_'+idgui+'_'+now);
                htmlpanel.find('.dialogue-image .image img').removeAttr('class').addClass('img-responsive img-thumbnail img-thu img_1'+met+'_'+idgui+'_'+now);

                htmlpanel.find('.d-box.right').removeAttr('class').addClass('d-box right box2_'+met+'_'+now);
                htmlpanel.find('.d-box.right').removeAttr('data-audio');
                htmlpanel.find('.d-box.right .d-bubble').attr('id','dialogo-texto_2_'+now);
                htmlpanel.find('.d-box.right .d-bubble .addtext').text('Click here to Add text');
                htmlpanel.find('.d-box.right .d-bubble input.valin').attr('data-nopreview','box2_'+met+'_'+now);
                htmlpanel.find('.d-box.right .btnselectedfile[data-tipo="audio"]').attr('data-url','.box2_'+met+'_'+now);

                var newPanel = '<div id="'+idTab+now+'" class="tpanedialog tab-pane fade">'+htmlpanel.html()+'</div>';
                $('.dialogue-container',objcont).append(newPanel);
                $(contenedor+' a[href="#'+idTab+now+'"]',objcont).tab('show');
                $('.istooltip').tooltip();
            }).on('click', '.plantilla-dialogo #btn-DelPage a', function(e) {
                e.preventDefault();
                $(this).parents('.dialogue-pagination').find('li.active').remove();
                $(this).parents('.plantilla-dialogo').find('.dialogue-container .tpanedialog.active').remove();
                renombrarPageDialogo( $(this).parents('.dialogue-pagination') );
                $('.dialogue-pagination>ul>li>a[data-toggle="pill"]:first',objcont).trigger('click');
            }).on('click', '.plantilla-dialogo .dialogue-pagination ul.nav-pills li a', function(e) {
                $('#btn-AddPage a',objcont).addClass('disabled');
            }).on('shown.bs.tab', '.plantilla-dialogo .dialogue-pagination ul.nav-pills li a', function(e){
                if(_cureditable==true){
                    $('#btn-AddPage a',objcont).removeClass('disabled');
                    if( $(this).parents('ul').find('a[data-toggle="pill"]').length>1 && !$(this).parents('ul').find('li:first-child').hasClass('active') ){
                        if(_cureditable==true){
                            $('.dialogue-pagination #btn-DelPage',objcont).show(); /*mostrar*/
                        }
                    } else {
                        $('.dialogue-pagination #btn-DelPage',objcont).hide(); /*ocultar*/
                    }
                }else{
                    var estado = $('#playdialogo',objcont).attr('data-estado')
                    if( estado=='playing' ){
                      playAudio();
                    }
                    if( estado=='stopped' ){
                      stopAudio();
                    }
                }
            }).on('click','.plantilla-dialogo #playdialogo',function(){
                if(_cureditable==true) return false;
                var estado = $(this).attr('data-estado');
                if( estado=='stopped' ){
                    playAudio();
                }
                if( estado=='playing' ){
                    stopAudio();
                }
            }).on('click','.plantilla-dialogo #replaydialogo',function(){
                if(_cureditable==true) return false;
                $('.dialogue-pagination>ul>li>a[data-toggle="pill"]:first',objcont).trigger('click');
                $('#playdialogo',objcont).trigger('click');
            }).on('change', '.plantilla-dialogo .dialogue-container .opt-nro-orden', function(e) {
                e.stopPropagation();
                var $box = $(this).parents('.dialogue-boxes');
                var value = $(this).val();
                if( value != -1 ){ $(this).parents('.d-box').attr('data-orden', value); }
                if( value == -1 ) {var otherValue = -1;}
                if( value == 0 ) {var otherValue = 1;}
                if( value == 1 ) {var otherValue = 0;}
                if( $box.hasClass('left') ){ var otherBox = 'right'; }
                if( $box.hasClass('right') ){ var otherBox = 'left'; }
                var $otherSelectTag = $box.siblings('.dialogue-boxes.'+otherBox).find('.opt-nro-orden');
                $otherSelectTag.val(otherValue);
                if( otherValue!=-1 ){ $otherSelectTag.parents('.d-box').attr('data-orden', otherValue); }
            }).on('click', '.plantilla-dialogo .opt-nro-orden, .d-box', function(e) { e.stopPropagation(); });


////////////plantilla completar
            var initcompletar=function(idgui){
                if(_cureditable==true){
                    addBtnsEdicionMCE();
                }
            }

            var mostrarEditorMCE  = function(){
              var tpl_plantilla=$('.tpl_plantilla',_curejercicio);
              tpl_plantilla.hide();
              var idgui=tpl_plantilla.attr('data-idgui');
              var html=tpl_plantilla.find('.panel.panelEjercicio');

              $(html).find('input.mce-object-input').each(function(i,v){
                var idtmp=$(v).attr('id');

                if( typeof idtmp=='undefined' || idtmp=='' || estaIDRepetido(html, $(v)) ) {
                    idtmp = 'id'+Date.now()+i;
                }

                $(v).attr('id',idtmp);
                var optionstmp=$(v).attr('data-options')||'';
                optionstmp=_isJson(optionstmp)?JSON.parse(optionstmp):optionstmp.split(',');
                jsonmultiplantilla[idtmp]={
                    al:optionstmp,
                    ok:$(v).attr('data-texto')||''
                };                   
                    //$(v).removeAttr('data-options');
              });
              col1=$(html).clone();
              var media=col1.find('img');                
                if(media.length==0){
                    media=col1.find('audio');
                    if(media.length==1)media=media.closest('div');
                }
                if(media.length==0){ 
                    media=col1.find('video');
                    if(media.length==1)media=media.closest('div');
                }
                var htmlmedia='';
                if(media.length==1){
                    htmlmedia=$('<div>').append(media.clone()).html();
                    media.remove();
                }

                var _ejerhtml=col1.html();
                if(col1.children('.row').length==1){
                    _ejerhtml=col1.children('.row').children('div').last().html();
                }
                $('body').children('[role="log"]').remove();
                $('body').children('.mce-widget').remove();
                tpl_plantilla.closest('.plantilla').find('.mce-tinymce').remove();
                //console.log(tpl_plantilla);
               // $('.mce-widget').remove();
              $('#txtarea'+idgui).html(htmlmedia+_ejerhtml);
             // $('#txtarea'+idgui).html($(html).html());
              $('#txtarea'+idgui).show();
              $('.plantilla .btnedithtml',_curejercicio).addClass('disabled');
              $('.plantilla .btnsavehtmlsystem',_curejercicio).removeClass('disabled');
              var info=_curaquicargaplantilla;
              var showtoolstiny=info.attr('data-tools')||'chingoinput';
              tinymce.init({
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                convert_newlines_to_brs : true,
                menubar: false,
                statusbar: false,
                verify_html : false,
                content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
                selector: '#txtarea'+idgui,
                height: 250,
                paste_auto_cleanup_on_paste : true,
                paste_preprocess : function(pl, o) {
                         var html='<div>'+o.content+'</div>';
                        var txt =$(html).text();
                        o.content = txt;
                    },
                paste_postprocess : function(pl, o) {
                        o.node.innerHTML = o.node.innerHTML;
                        o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
                    },
                plugins:[showtoolstiny+" chingosave textcolor paste lists advlist table" ],  // chingoinput chingoimage chingoaudio chingovideo
                toolbar: 'chingosave | undo redo chingodistribution | styleselect | removeformat table |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | '+showtoolstiny, //chingoinput chingoimage chingoaudio chingovideo,
                advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",
              });
            };

            var regrabarInputsMCE = function($panelEjerc) {
                $panelEjerc.find('input.mce-object-input').each(function(index, elem) {
                    var idInput = $(elem).attr('id');
                    var respuesta = '';
                    var distracts = [];
                    if(typeof jsonmultiplantilla[idInput]!='undefined') {
                        respuesta = jsonmultiplantilla[idInput].ok;
                        distracts = jsonmultiplantilla[idInput].al;
                    }
                    $(elem).attr({
                        'data-texto': respuesta,
                        'data-options': JSON.stringify(distracts),
                    });
                });
            };
            
            var funcionalidaddraganddrop=function(cont){
                $('.plantilla-completar .isdragable',cont).draggable({
                    start:function(event,ui){
                        sysdragstart(event,ui);
                    },
                    stop:function(event,iu){
                       sysdragend(event,iu)
                    }
                });
                $('.plantilla-completar .isdrop',cont).droppable({
                    drop:function(event,ui){
                        sysdrop(event,ui,$(this));
                        calcularBarraProgreso();
                    }
                });
            };

            funcionalidaddraganddrop(objcont);
            objcont.on('click','.btndistribucion',function(ev){  //dividir paneles sin idguipara todos
                ev.preventDefault();
                if(_cureditable==false) return false;
                var _tplplantilla=$('.tpl_plantilla',_curejercicio);
                var col1=_tplplantilla.find('.discol1').clone();
                var col2=_tplplantilla.find('.discol2').clone();
                var envista=_tplplantilla.attr('envista')||'n';
                var media=col1.find('img');                
                if(media.length==0){
                    media=col1.find('audio');
                    if(media.length==1)media=media.closest('div');
                }
                if(media.length==0){ 
                    media=col1.find('video');
                    if(media.length==1)media=media.closest('div');
                }
                var htmlmedia='';
                if(media.length==1){
                    htmlmedia=$('<div>').append(media.clone()).html();
                    media.remove();
                }
                var _ejerhtml=col1.find('.panelEjercicio');
                if(_ejerhtml.children('.row').length==1){
                    _ejerhtml=_ejerhtml.children('.row').children('div').last();
                }
                if(envista=='n'){
                    _tplplantilla.attr('envista','v1');
                    htmlall='<div class="row">';
                    htmlall+='<div class="col-md-6 col-sm-12">'+htmlmedia+'</div>';
                    htmlall+='<div class="col-md-6 col-sm-12" style="padding-top:1ex;">'+_ejerhtml.html()+'</div>';
                    htmlall+='</div>';
                     _tplplantilla.find('.panelEjercicio').html(htmlall);
                   
                }else if(envista=='v1'){
                    _tplplantilla.attr('envista','v2');
                    htmlall='<div class="row">';
                    htmlall+='<div class="col-md-12">'+htmlmedia+_ejerhtml.html()+'</div>';
                    htmlall+='</div>';
                    _tplplantilla.find('.panelEjercicio').html(htmlall);
                }else{
                    _tplplantilla.attr('envista','n');
                }


                var tpltxt=$('.tpl_plantilla .discol1',_curejercicio);
                var tplalter=$('.tpl_plantilla .discol2',_curejercicio);
                if(tpltxt.hasClass('col-md-12')){
                      tpltxt.removeClass().addClass('col-md-6 col-sm-7 col-xs-12 discol1');
                      tplalter.removeClass().addClass('col-md-6 col-sm-5 col-xs-12 discol2');
                }else{
                  tpltxt.removeClass().addClass('col-md-12 col-sm-12 col-xs-12 discol1');
                  tplalter.removeClass().addClass('col-md-12 col-sm-12 col-xs-12 discol2');
                }
            }).on('click','.btnedithtml',function(ev){
                ev.preventDefault();
                if(_cureditable==false) return false;
                if($(this).hasClass('disabled')){
                  return false;
                }else{
                  var tabcontent =$(this).closest('.plantilla');
                  tabcontent.addClass('editando');
                  reiniciarInputsMCE(tabcontent);
                  mostrarEditorMCE();
                }
                $('#btns-guardar-actividad',objcont).hide('fast');
                $(this).siblings('.btndistribucion').addClass('disabled');
            }).on('click','.btnsavehtmlsystem',function(ev){
                if($(this).hasClass('disabled')) return false;
                var tabhijo=_curaquicargaplantilla;
                $('.plantilla',tabhijo).removeClass('editando');
                var tpl_plantilla=$('.tpl_plantilla',tabhijo);
                var panelEjercicios=$('.panelEjercicio',tpl_plantilla);
                $('.plantilla .btnedithtml',tabhijo).removeClass('disabled');
                tinyMCE.triggerSave();
                var idgui=tpl_plantilla.attr('data-idgui');
                var txt=$('#txtarea'+idgui).val();
                panelEjercicios.html(txt);

                regrabarInputsMCE(panelEjercicios);

                tpl_plantilla.show();

                tinymce.remove('#txtarea'+idgui);
                $('#txtarea'+idgui).hide();

                var inpdrag=panelEjercicios.find('input.isdrop[data-mce-object="input"]');
                var ninpdrag=inpdrag.length;                
                plnaleternativas= $('.panelAlternativas',tpl_plantilla);
                if(ninpdrag>0){ //Ejericios de arrastre
                  var spanalt=[];
                  inpdrag.each(function(){
                    var optionstmp=jsonmultiplantilla[$(this).attr('id')];
                    optionstmp=optionstmp||{al:[],ok:[]};
                    var optionsal=optionstmp.al||[];
                    var optionsok=optionstmp.ok||'';
                    if(ninpdrag>1){ //draggable="true"  ondragend="sysdragend(this,event)"
                      spanalt.push('<span class="isdragable" >'+optionsok+'</span>');
                    }else{
                      var opt=optionsal;                      
                      if(opt.length>0){
                        spanalt.push('<span class="isdragable">'+optionsok+'</span>'); 
                        $.each(opt,function(index,value){
                          spanalt.push('<span class="isdragable">'+value+'</span>');
                        });
                      }
                    }
                  });
                  spanalt.sort(function(){return Math.random() - 0.5});
                  plnaleternativas.html(spanalt.toString());
                  if($('span.isdragable',plnaleternativas).length>0)
                  funcionalidaddraganddrop(tabhijo);
                }else{
                  var inpClicked=panelEjercicios.find('input.isclicked[data-mce-object="input"]');
                  if(inpClicked.length>0){
                    desplegarAlternativas(idgui);
                  }
                }

                var $imgs = panelEjercicios.find('img[data-mce-object="image"]');
                if($imgs.length>0){ $imgs.addClass('gray-scale'); }
                plnaleternativas.parent().removeClass('hide').removeAttr('style');
                if(plnaleternativas.text()=='')
                    plnaleternativas.parent().addClass('hide');
                $(this).removeClass('disabled');
                $(this).addClass('disabled');
                $(this).siblings('.btndistribucion').removeClass('disabled');
                showBotonesguardardoc();
            }).on('click','.vervideohelp',function(e){
                    showvideoayudatemplate(true);
            }).on('click','#setting-textbox .add-distraction',function(ev){
                ev.preventDefault();
                $('#setting-textbox .distractions',objcont).append('<input type="text" class="form-control added">');
                $('#setting-textbox .distractions',objcont).children(':last-child').focus();
            }).on('click','#setting-textbox .save-setting-textbox',function(ev) {
                ev.preventDefault();
                saveTextboxEdit();
            });

            $('#btnActivarinputEdicion').click(function(e){
                if(_cureditable==false) return false;
                $('#info-avance-alumno',objcont).addClass('editandoinput');
                var acteditor=tinyMCE.activeEditor.getBody();
                var obj=$(acteditor).find('._enedicion_');//input en edicion
                if($(obj).hasClass('mce-object-input')){
                    var infotemplate=_curaquicargaplantilla;
                    var asistido=infotemplate.attr('data-showasistido')||false;
                    var alternativas=infotemplate.attr('data-showalternativas')||false;
                    $('#info-avance-alumno',objcont).hide();
                    var optionstmp=jsonmultiplantilla[$(obj).attr('id')];
                    optionstmp=optionstmp||{al:[],ok:$(obj).attr('data-texto').toString()}; 
                    var txt=optionstmp.ok||$(obj).attr('data-texto').toString();
                    $('#setting-textbox .correct-ans input',objcont).val(txt.trim());
                    $('#setting-textbox .correct-ans input',objcont).attr('data-objeto',$(obj).attr('id'));
                    obj=$(obj);
                    if(alternativas&&alternativas!='0'){
                         $('#setting-textbox .distractions input',objcont).remove();
                         var options=optionstmp.al||[];                        
                         var iopt=0;
                         try{
                            if(options.length>0){                           
                            $.each(options,function(i,v){
                                iopt++;
                                var vinput=document.createElement("input");
                                vinput.setAttribute('class','form-control');
                                vinput.setAttribute('type','text');
                                vinput.setAttribute('value',v);
                                vinput.value=v;
                                $('#setting-textbox .distractions',objcont).append(vinput);
                            });
                            }
                         }catch(ex){}
                         for(var i = iopt; i <3 ; i++) {
                            $('#setting-textbox .distractions',objcont).append('<input value="" class="form-control">');
                         }
                         $('#setting-textbox .distractions',objcont).show();
                    }else $('#setting-textbox .distractions',objcont).hide();
                    if( asistido&&asistido!='0' ) {
                        $('#setting-textbox .assisted',objcont).show();
                        $('#setting-textbox .assisted input.isayuda',objcont).prop("checked", obj.hasClass('isayuda')?true:false);
                    }
                    $('#setting-textbox',objcont).show('fast');
                    obj.addClass('_enedicion_');
                    $('#setting-textbox .correct-ans input',objcont).focus();
                }
            });

            objcont.on('focusin',  '.tabhijo.active input.mce-object-input', function(e){
                var idpnl=$(this).closest('.panel.panelEjercicio').attr('id');
                var params = {
                    id : '#'+idpnl,
                    attention_text: MSJES_PHP.attention,
                    obj :this,
                };
                var isDBY = esDBYself($(this));
                $(this).closest(".panelEjercicio").find("#"+$(this).data("nube")).hide();
                if(isDBY){ dby_handlerInputMCE_focusin(e, params); }else{handlerInputMCE_focusin(e, params); }
            }).on('focusout', '.tabhijo.active input.mce-object-input', function(e){
                e.preventDefault();
                
                var isDBY = esDBYself($(this));
                var _this = this;        

                if(!$(_this).hasClass('iswrite')){
                    return false
                }
                var $this=$(_this);
                var value=$this.val().trim();
                if(value==''){
                    $(this).closest(".panelEjercicio").find("#"+$(this).data("nube")).show();
                    return;
                }  // si no lleno nada;
                if($this.closest('.wrapper').hasClass('input-flotantes') ){
                    $this.closest('.input-flotantes.wrapper').find('.helper-zone').remove();
                    $this.closest('.input-flotantes.wrapper').find('.options-list').remove();
                }
                $this.removeClass('active open');
                if($this.closest('.wrapper').hasClass('input-flotantes')){
                    try{
                    $this.closest('.input-flotantes.wrapper').children().unwrap();
                    }catch(e){}
                }
                var isCompleto = inputMCE_Completo($this);
                var condicion = ( isDBY && isCompleto );
                var buscar = 'input.mce-object-input',
                buscarCorrectos = '.inp-corregido[data-corregido="good"]';   
                var rspta_crrta = $this.attr('data-texto');    
                var fn = function(){
                    evaluarRespuesta(_this);
                    calcularBarraProgreso();
                };
                if(isDBY) fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
                /*if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){*/
                if(rspta_crrta.trim() === value){
                    var valor = 'good';
                    agregarIcon(_this, valor);
                    fn();
                }else{
                     var valor = 'bad';
                    agregarIcon(_this, valor);
                    fn();
                }
                $this.off('focusout'); 

            }).on('click', '.plantilla-completar ul.options-list>li>a', function(e){
                e.preventDefault();
                e.stopPropagation();
                var _this = this;
                var isDBY = esDBYself( $(_this));
                var fn = function(){
                    var value = $(_this).text();
                    var input = $(_this).parents('ul.options-list').siblings('input.open');
                    input.val(value);
                    input.attr('value',value);
                    cerrarFlotantes(input);
                    evaluarRespuesta(input);
                    calcularBarraProgreso();
                };

                if(isDBY){
                    var isCompleto = inputMCE_Completo( $(_this) );
                    var condicion = (isDBY && !isCompleto);
                    var buscar = 'input.mce-object-input',
                    buscarCorrectos = '.inp-corregido[data-corregido="good"]';
                    fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
                }else{
                    var value = $(this).text();
                    var input = $(this).parents('ul.options-list').siblings('input.open');
                    input.val(value);
                    input.attr('value',value);
                    cerrarFlotantes(input);
                    evaluarRespuesta(input);
                    calcularBarraProgreso();
                }
            }).on('click', '.plantilla-completar div.helper-zone>a.btn-help', function(e){
                e.preventDefault();
                e.stopPropagation();
                var $tmplActiva = getTmplActiva();
                var $plantActiva = $(this).closest('.plantilla');
                var isDBY = esDBYself( $(this));
                if( $plantActiva.hasClass('tiempo-acabo') && isDBY){
                    return true;
                }
                var $inputMCE = $(this).parent().siblings('input[data-mce-object="input"]');

                if( $(this).find('span.tip-word').length==0 ){
                    $(this).append('<span class="tip-word"> : </span>');
                }
                var rspta= $inputMCE.attr('data-texto');
                var arr_rspta= rspta.split("");

                var i = $inputMCE.siblings().find('span.tip-word').attr('data-pulsado');

                if(i==''||i==undefined){
                    i = 0;
                }
                i = parseInt(i);
                if( i < arr_rspta.length){
                    $inputMCE.siblings().find('span.tip-word').attr('data-pulsado',(i+1));
                    var texto = $inputMCE.siblings().find('span.tip-word').text();
                    texto = texto.concat(arr_rspta[i]);
                    $inputMCE.siblings().find('span.tip-word').text(texto);
                }
                $inputMCE.focus();
            }).on('keypress', '.plantilla-completar input.mce-object-input', function(e){               
                var $plantActiva = $(this).closest('.plantilla');
                var isDBY = esDBYself( $(this));
                if( $plantActiva.hasClass('tiempo-acabo') && isDBY){
                    return true;
                }  
            }).on('keyup', '.plantilla-completar input.mce-object-input', function(e){
                var _this = this;
                var buscar = 'input.mce-object-input',
                    buscarCorrectos = '.inp-corregido[data-corregido="good"]';
                var isDBY = esDBYself( $(_this) );
                var isCompleto = inputMCE_Completo( $(_this) );
                var condicion = ( isDBY && isCompleto ); 
                $(this).off('focusout');
                if(e.which != 13){
                    var value = $(this).val().trim();
                    var rspta_crrta = $(this).attr('data-texto');
                    if(value.length>0){
                        /*if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){*/
                        if(rspta_crrta.trim() === value.trim()){
                            if( $(this).closest('.wrapper').hasClass('input-flotantes') ){
                                $(this).closest('.input-flotantes.wrapper').find('.helper-zone').remove();
                                $(this).closest('.input-flotantes.wrapper').find('.options-list').remove();
                            }
                            $(this).removeClass('active open');
                            if($(this).closest('.wrapper').hasClass('input-flotantes')){
                                try{
                                    $(this).closest('.input-flotantes.wrapper').children().unwrap();
                                }catch(e){}
                            }
                            var fn = function(){
                               evaluarRespuesta(_this);
                               calcularBarraProgreso();
                            };
                            if(isDBY) fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
                            else{
                                var valor = 'good';
                                agregarIcon(this, valor);
                            }
                            $(this).addClass('esteejecuta'); // siguiente tan faltaria
                            var inputs=$(this).closest('.plantilla').find('input.mce-object-input')
                            var encontro=false;
                            $.each(inputs,function(){
                                if(encontro==true) {
                                    $(this).trigger('focusin');
                                    encontro=false;
                                }
                                if($(this).hasClass('esteejecuta')){
                                    $(this).removeClass('esteejecuta');
                                    encontro=true;
                                }
                            })
                            $(this).removeClass('esteejecuta');
                        }                        
                    }
                }
                else{                    
                    $(this).on('focusout').addClass('esteejecuta').trigger('focusout'); // siguiente tan faltaria
                    var inputs=$(this).closest('.plantilla').find('input.mce-object-input')
                    var encontro=false;                    
                    $.each(inputs,function(){
                        if(encontro==true) {
                            $(this).trigger('focusin');
                            encontro=false;
                        }
                        if($(this).hasClass('esteejecuta')){
                            $(this).removeClass('esteejecuta');
                            encontro=true;
                        }
                    })
                    $(this).removeClass('esteejecuta');

                }
                if($(_this).hasClass('iswrite')){                    
                    $(this).on('focusout');                   
                }
            }).on('click', '.plantilla-completar .panelAlternativas ul.alternativas-list li.alt-item a', function(e){
                e.preventDefault();
                e.stopPropagation();
                var _this = this;
                var isDBY = esDBYself($(_this));
                var tpl=$(this).closest('.tpl_plantilla');
                var pnlEjercicio=tpl.find('.panel.panelEjercicio');
                var idpnl=pnlEjercicio.attr('id');
                var idgui=tpl.attr('data-idgui');
                if(isDBY){
                    var condicion = (isDBY && !existeAltMarcada($('#pnl_editalternatives'+idgui,_curejercicio)));
                    var buscar = '.panelEjercicio input.mce-object-input',
                    buscarCorrectos = '.panelEjercicio .inp-corregido[data-corregido="good"]';
                    var fn = function(){
                        $('#pnl_editalternatives'+idgui+' ul.alternativas-list li.alt-item a',_curejercicio).each(function() {
                            if( $(_this).parent().hasClass('wrapper') ){
                                $(_this).siblings('span.inner-icon').remove();
                                $(_this).unwrap();
                            }
                        });
                        evaluarRsptaAltern(_this, idgui,tpl);
                        calcularBarraProgreso();
                    };
                    fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
                }else{
                        var seleccionada = $(this).text();
                        $('ul.alternativas-list li.alt-item a', _curejercicio).each(function() {
                            if( $(this).parent().hasClass('wrapper') ){
                                $(this).siblings('span.inner-icon').remove();
                                $(this).unwrap();
                            }
                        });
                        var $inpClicked = $('#'+idpnl).find('input.isclicked[data-mce-object="input"]');
                        var arr_opc_corr = [];
                        if($inpClicked.length>0){
                            $inpClicked.each(function() {
                                var opc_corr = $(this).attr('data-texto');
                                arr_opc_corr.push(opc_corr);
                            });
                        }
                        var rspta_crrta = arr_opc_corr.join(' - ');
                        if(rspta_crrta === seleccionada){
                            agregarIcon(this, 'good');
                            agregarRsptaInput($('#'+idpnl,objcont),seleccionada);
                        } else {
                            agregarIcon(this, 'bad');
                            agregarRsptaInput($('#'+idpnl,objcont),seleccionada);
                        }
                        calcularBarraProgreso();
                }
            }).on('click', '.plantilla-completar  ul.nav-tabs.ejercicios li a', function (e, esSgte) {
                var esSgte=esSgte||false;
                var $li = $(this).parent();
                if( $li.hasClass('disabled') && !esSgte){
                    e.preventDefault();
                    e.stopPropagation();
                }if(esSgte){
                    if( $li.prev('li').hasClass('disabled') ) $li.prev('li').removeClass('disabled');
                    if( $li.hasClass('disabled') ) $li.removeClass('disabled');
                }
            }).on('click', '.plantilla-completar .tpl_plantilla', function(ev) {
                if(_cureditable==true) return false;
                var panelEjercicio=$('.tabhijo.active .tpl_plantilla').find('.panel.panelEjercicio');
                var idpnl=panelEjercicio.attr('id');
                var $input = $('#'+idpnl+' input.mce-object-input.open');
                if($(window).width() <= 768){
                    if(objcont.find('.options-list').length > 0 || objcont.find('.input-flotantes').length > 0 ){
                        mobilhacklife = !mobilhacklife;
                    }else{
                        mobilhacklife = false;
                    }
                    // console.log(mobilhacklife);
                    if(!mobilhacklife){
                        cerrarFlotantes($input);
                    }
                }else{
                    cerrarFlotantes($input);
                }
            }).on('mousedown', '.aquicargaplantilla .tpl_plantilla', function(e) {
                if(_cureditable==true) return false;
                var plantilla=$(this).closest('.plantilla');
                iniciarTiempo(finTiempo);
            })


///////////Plantilla verdadero y falso
        objcont.on('click', '.plantilla-verdad_falso   .list-premises a.btn.delete', function(e){
            e.preventDefault();
            e.stopPropagation();
            $(this).parents('.premise').remove();
        }).on('click','.plantilla-verdad_falso  a.btn.add-premise',function(e) {
            e.preventDefault();
            e.stopPropagation();
            var clone_from = $(this).attr('data-clone-from');
            var clone_to = $(this).attr('data-clone-to');
            var new_premise = $(clone_from).clone();
            var cant_premises = $(clone_to+' .premise').length;
            if(cant_premises>0) {
                var nameInput= $(clone_to+' .premise:nth-child('+cant_premises+') .options input').attr('name').split('-');
                var new_index = parseInt(nameInput[1]) + 1;
            } else {
                var new_index = 1;
            }
            new_index = Date.now();
            new_premise.find('.options input').each(function() {
                var new_name = $(this).attr('name') + new_index;
                $(this).attr('name', new_name ); /*renombrando grupo de opciones Radio*/
            });
            new_premise.find('input#opcPremise-').each(function() {
                var new_id = $(this).attr('id') + new_index;
                $(this).attr('id', new_id); /*renombrando input hidden opc-correcta*/
            });
            new_premise.find('input.valPremise.nopreview').each(function() {
                var new_data = $(this).attr('data-nopreview') + new_index;
                $(this).attr('data-nopreview', new_data); /*renombrando input hidden opc-correcta*/
            });

            $(clone_to).append('<div class="col-xs-12 premise pr-'+new_index+'">'+ new_premise.html() +'</div>');
            $('.istooltip').tooltip();
        }).on('change','.plantilla-verdad_falso  .list-premises .options input[type="radio"].radio-ctrl', function(e){
            e.stopPropagation();
            var $this=$(this);
            var isDBY = esDBYself($this);
            if(!isDBY){
                var name = $this.attr('name');
                var value = $('input[name="'+name+'"].radio-ctrl:checked',objcont).val();
                var plantilla=$this.closest('.plantilla');
                var $opciones=$this.closest('.options');
                if(_cureditable==true)
                    asignarRsptaEdicion($this,objcont);
                else{
                    $opciones.find('input.radio-ctrl',objcont).not($this).removeAttr('checked');
                    $this.attr('checked','checked');
                    evaluarRsptaVoF($this,objcont);
                }
                calcularBarraProgreso();
            }else{
                var $iconoCorregido = $this.parents('.options').siblings('.icon-zone').find('.icon-result>i.fa');
                if( $iconoCorregido.length==0 ){
                    if( _cureditable==true ){ //si está en vista de edicion
                        asignarRsptaEdicion( $this,objcont);
                    } else {
                        var condicion = esDBYself($this);
                        var fn = function(){
                            $this.attr('checked','checked');
                            evaluarRsptaVoF($this,objcont);
                            calcularBarraProgreso();
                        };
                        var buscar = '.premise',
                        buscarCorrectos = '*[data-corregido="good"]';
                        fnTiempoIntentosPuntaje( condicion, fn, buscar, buscarCorrectos );
                    }
                } else {
                    var $sibling = $this.parent().siblings('label').find('input[type="radio"]');
                    $this.removeProp('checked');
                    $sibling.prop('checked', true);
                }
            }
        }).on('mouseup', '.plantilla-verdad_falso  .list-premises .options label', function(e){
            if(_cureditable==true) return false;
            e.stopPropagation();
            var isDBY = esDBYself($(this));
            if(!isDBY) return false;
                var $sibling = $(this).siblings('label').find('input[type="radio"]');
                if( $sibling.is(':checked') ){
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar, 'warning');
                }
        });

///////////Plantilla  fichas
        objcont.on('click', '.plantilla-fichas .ejerc-fichas .ficha', function(e) {
            if(_cureditable==true) return false;
            e.stopPropagation();
            var isDBY = esDBYself($(this));
            if( $(this).parents('.partes-1').length!=0 ){
                var seleccionado = $(this).attr('data-key');
                if(CLAVE_ACTIVA != seleccionado){
                    CLAVE_ACTIVA = seleccionado;
                    COLOR_SELECC = $(this).attr('data-color');
                    desmarcarIncorrectas( isDBY,objcont );
                    $('.partes-1 .ficha[data-key="'+CLAVE_ACTIVA+'"]').addClass('active');
                } else {
                    $(this).removeClass('active');
                    desmarcarIncorrectas( isDBY,objcont );
                    CLAVE_ACTIVA = null;
                    COLOR_SELECC = null;
                }
            }

            if( $(this).parents('.partes-2').length!=0 ){
                e.stopPropagation();

                if(CLAVE_ACTIVA!=null){
                    if( !$(this).hasClass('corregido') ){
                        $(this).toggleClass('active');
                        if(isDBY){
                            var $this = $(this);
                            var condicion = isDBY;
                            var buscarTodos = '.partes-2 .ficha';
                            var buscarCorrectos = '.partes-2 .ficha.corregido.good';
                            var fn = function(){
                                corregirFicha($this);
                                calcularBarraProgreso();
                            };
                            fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
                        } else {
                            corregirFicha( $(this) );
                            calcularBarraProgreso();
                        }
                        $(this).attr('data-color', COLOR_SELECC);
                        $('.partes-1 .ficha.active').removeClass('active');
                        CLAVE_ACTIVA = null;
                        COLOR_SELECC = null;
                    } else if(isDBY){
                        mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar, 'warning');
                    }
                }
            }
        }).on('click', '.plantilla-fichas .ejerc-fichas .ficha a', function(e) {
            if(_cureditable==true) return false;
            e.preventDefault();
            e.stopPropagation();
            var $plantilla=$(this).parents('.plantilla-fichas');
            var idgui=$plantilla.attr('data-idgui');
            var $audio = $('#audio-ejercicio'+idgui),
            src = $(this).attr('data-audio');
            $audio.attr('src', _sysUrlStatic_ +'/media/audio/'+src);
            $audio.trigger('play');
            $(this).closest('.ficha').trigger('click');
        });
    /**
    *** EDICION del ejerc Fichas :
    **/
        objcont.on('click','.plantilla-fichas > ._lista-fichas .selectmedia', function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            e.stopPropagation();
            var txt= MSJES_PHP.select_upload;
            lastBtnClicked = this;
            selectedfile(e,this,txt,'activarBtn');//activarBtn() en 'editactividad.js'
        }).on('click', '.plantilla-fichas > ._lista-fichas .delete-ficha', function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            e.stopPropagation();
            $(this).parents('.ficha-edit').remove();
        }).on('click', '.plantilla-fichas > ._lista-fichas .delete-ficha-row', function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            e.stopPropagation();
            $(this).parents('.ficha-row').remove();
        }).on('click', '.plantilla-fichas > ._lista-fichas .add-ficha-row', function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            e.stopPropagation();
            var cloneFrom = $(this).attr('data-clone-from');
            var cloneTo = $(this).attr('data-clone-to');
            var index = $(this).parents('.ficha-edit').attr('id').split('_')[1];
            var now = Date.now();
            var newRow = $(cloneFrom).clone();

            newRow.find('.renameClass').each(function() {
                var clase = $(this).data('clase');
                $(this).addClass(clase+index+'_'+now);
                $(this).removeClass('renameClass').removeAttr('data-clase');
            });

            newRow.find('.renameDataUrl').each(function() {
                var url = $(this).data('url');
                $(this).removeAttr('data-url').attr('data-url', url+index+'_'+now);
                $(this).removeClass('renameDataUrl');
            });

            newRow.removeClass('hidden').removeAttr('id');

            $(cloneTo).append(newRow);
            $("*[data-tooltip=\"tooltip\"]").tooltip();
        }).on('click', '.plantilla-fichas .add-ficha',function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            e.stopPropagation();
            var cloneFrom = $(this).attr('data-clone-from');
            var cloneTo = $(this).attr('data-clone-to');
            var newFicha = $(cloneFrom).clone();
            var now = Date.now();

            newFicha.find('.renameClass').each(function() {
                var clase = $(this).attr('data-clase');
                $(this).addClass(clase+now);
                $(this).removeClass('renameClass').removeAttr('data-clase');
            });

            newFicha.find('.renameDataUrl').each(function() {
                var url = $(this).attr('data-url');
                $(this).removeAttr('data-url').attr('data-url', url+now);
                $(this).removeClass('renameDataUrl');
            });

            newFicha.find('.add-ficha-row').attr('data-clone-to', '#f_'+now+' .part-2');

            newFicha.removeClass('hidden').removeAttr('id');
            newFicha.attr( 'id', newFicha.attr('data-clase')+now ).removeAttr('data-clase');

            $(cloneTo).append(newFicha);
            $("*[data-tooltip=\"tooltip\"]").tooltip();
        }).on('click','.plantilla-fichas .generar-fichas',function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            e.stopPropagation();
            var idCloneFrom = $(this).attr('data-clone-from');
            var idCloneTo = $(this).attr('data-clone-to');
            var $plantilla=$(this).parents('.plantilla-fichas');
            $plantilla.removeClass('editando');
            var idgui=$plantilla.attr('data-idgui');
            var resp = generarFichas(idCloneFrom, idCloneTo);
            if(resp){
                $(idCloneTo).show();
                $('#lista-fichas'+idgui).hide().addClass('hidden');
                $('#tmp_'+idgui+' .botones-creacion').hide().addClass('hidden');
                $('#tmp_'+idgui+' .botones-editar').show();
            } else {
                $(idCloneTo).hide();
                $('#lista-fichas'+idgui).show().removeClass('hidden');
                $('#tmp_'+idgui+' .botones-creacion').show().removeClass('hidden');
                $('#tmp_'+idgui+' .botones-editar').hide();
            }
            plantilla=$()
            showBotonesguardardoc();
        }).on('click','.plantilla-fichas .botones-editar .back-edit', function(e){ //editar fichas
            if(_cureditable==false) return false;
            e.preventDefault();
            e.stopPropagation();
            var $plantilla=$(this).parents('.plantilla-fichas');
            $plantilla.addClass('editando');
            var idgui=$plantilla.attr('data-idgui');
            $('#ejerc-fichas'+idgui).hide();
            $('#lista-fichas'+idgui).show().removeClass('hidden');
            $('#tmp_'+idgui+' .botones-creacion').show().removeClass('hidden');
            $('#tmp_'+idgui+' .botones-editar').hide();
            $('#ejerc-fichas'+idgui).find('.partes-1').html('');
            $('#ejerc-fichas'+idgui).find('.partes-2').html('');
            showBotonesguardardoc();
        });


    ////////// plantilla Speach
    objcont.on('click', '.plantilla-speach .btncalcular', function(e){
        calcularBarraProgreso();
    }).on('click', '.plantilla-speach .alumno .btnAccionspeachdby', function(e) {
        if(_cureditable==true) return false;
        if($(this).hasClass('active')){
            var plantilla=$(this).closest('.plantilla');
            iniciarTiempo(finTiempo);
        }else{
            setTimeout(pausarTiempoSpeach(),5000);
        }
    });

    objcont.on('calcularprogreso', '.plantilla-nlsw', function(e){
        calcularBarraProgreso();
    })


//////////Plantilla Ordenar Simple
        var sysinitOrdenar=function(){
            objcont.find('.plantilla-ordenar').each(function(){
                var idOrdenar=$(this).attr('id');
                var idgui=idOrdenar.split('_').pop();
                var ayuda=$(this).closest('.metod').attr('data-idmet')==3?false:true;
                if($(this).hasClass('ord_simple')){ initOrdenarSimple(idgui,ayuda); }
                else if($(this).hasClass('ord_parrafo')){ initOrdenarParrafos(idgui,'',ayuda); }
            });
        }
        //edicion Plantilla Ordenar
        objcont.on('click', '.plantilla-ordenar .selectmedia', function(e){
            if(_cureditable==false) return false;
            e.preventDefault();
            var txt = MSJES_PHP.select_upload;
            lastBtnClicked = this;
            selectedfile(e,this,txt,'activarBtn');
        }).on('focusin','.plantilla-ordenar input.txt-words', function(e) {
             if(_cureditable==false) return false;
                var string = $(this).val();
                if( string.split(' ').length > 1 ) { TIPO_DIVISION = 'palabra'; }
                else { TIPO_DIVISION = 'letra'; }
        }).on('keyup', '.plantilla-ordenar input.txt-words', function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            var string = $(this).val();
            if( string == '' ){ TIPO_DIVISION = 'letra' }
            if(e.keyCode == 32){  TIPO_DIVISION = 'palabra'; }
            if(e.keyCode == 8){
                if( string.split(' ').length <= 1 ){ TIPO_DIVISION = 'letra'; }
            }
            $contenedor = $(this).parents('.inputs-elem').find('.list-elem-part');

            dividirCadena(string, $contenedor);
        }).on('click', '.plantilla-ordenar .list-elem-part>div', function(e) {
            $(this).toggleClass('fixed');
        }).on('click', '.plantilla-ordenar  .generar-elem',function(e) {
            e.preventDefault();
            e.stopPropagation();
            var plantilla=$(this).closest('.plantilla-ordenar');
            var idTmpl = plantilla.attr('id');
            plantilla.removeClass('editando');
            var idguiTmpl = idTmpl.split('_').pop();
            var idCloneFrom = $(this).data('clone-from');
            var idCloneTo = $(this).data('clone-to');
            var xy = generarElementOrdenar(idCloneFrom, idCloneTo, idguiTmpl);
            $('#ejerc-ordenar'+idguiTmpl).show();
            $('#lista-elem-edit'+idguiTmpl).hide().addClass('hidden');
            $('#'+idTmpl+' .botones-editar').show();
            $('#'+idTmpl+' .botones-creacion').hide().addClass('hidden');
            initOrdenarSimple(idguiTmpl);
            showBotonesguardardoc();
        }).on('click', '.plantilla-ordenar .back-edit', function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            var plantilla=$(this).parents('.plantilla-ordenar');
            plantilla.addClass('editando');
            var idTmpl = plantilla.attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            $('#ejerc-ordenar'+idguiTmpl).hide();
            $('#lista-elem-edit'+idguiTmpl).show().removeClass('hidden');
            $('#'+idTmpl+'  .botones-editar').hide();
            $('#'+idTmpl+'  .botones-creacion').show().removeClass('hidden');
            $('#ejerc-ordenar'+idguiTmpl).html('');
            showBotonesguardardoc();
        }).on('click', '.plantilla-ordenar .ejerc-ordenar .drag>div', function(e) {
            if(_cureditable==true) return false;
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $this = $(this);
            var isDBY = esDBYself( $this );
            var condicion = isDBY;
            var buscarTodos = '.element';
            var buscarCorrectos = '.element[data-corregido="good"]';
            var fn = function(){
                var value= $this.text();
                var idElem = $this.parents('.element').attr('id');
                var $primerVacio = $('#'+idElem,objcont).find('.drop>div:empty').first();
                $primerVacio.html('<div>'+value+'</div>')
                $this.remove();
                evaluarOrdenCorrecto( $('#'+idElem,objcont) , idguiTmpl);
                calcularBarraProgreso();
            };
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
        }).on('click', '.plantilla-ordenar .ejerc-ordenar .drop>div.parte.blank>div', function(e) {
            if(_cureditable==true) return false;
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $this= $(this);
            var isDBY = esDBYself( $this );
            var condicion = !isDBY;
            var buscarTodos = '.element';
            var buscarCorrectos = '.element[data-corregido="good"]';
            var fn = function(){
                var value= $this;
                var $parents = $this.parents('.element');
                $parents.find('.drag').append(value);
                evaluarOrdenCorrecto( $parents , idguiTmpl);
                calcularBarraProgreso();
            };
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
        }).on('click', '.multimedia a[data-audio]', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $audio = $('#audio-ordenar'+idguiTmpl),
            src = $(this).attr('data-audio');
            $audio.attr('src', _sysUrlStatic_+'/media/audio/'+src);
            $audio.trigger('play');
        });

//Plantilla Ordenar Parrafo
        objcont.on('click', '.plantilla-ordenar  .delete-parrafo', function(e) {
            if(_cureditable==false) return false;
            e.preventDefault();
            $(this).parents('.parrafo-edit').remove();
        }).on('click', '.plantilla-ordenar  .move-parrafo', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            var idElemEdit = $(this).parents('.parrafo-edit').attr('id');
            var $listaElemEdit = $('#lista-parrafo-edit'+idguiTmpl),
                $parrafoEdit = $('#'+idElemEdit),
                idTextArea = $parrafoEdit.find('textarea').attr('id');

            if( $listaElemEdit.children('.parrafo-edit').length>1 ){
                saveEditor( idTextArea );
                if( $(this).hasClass('up') && $parrafoEdit.prev().length!=0 ){
                    var $parrafoPrev = $parrafoEdit.prev();
                    $parrafoEdit.insertBefore($parrafoPrev);
                }

                if( $(this).hasClass('down') && $parrafoEdit.next().length!=0 ){
                    var $parrafoNext = $parrafoEdit.next();
                    $parrafoEdit.insertAfter($parrafoNext);
                }
                initEditor( idTextArea );
            }
        }).on('click', '.plantilla-ordenar  .list-parrafo-part>div', function(e) {
            $(this).toggleClass('fixed');
        }).on('click', '.plantilla-ordenar  .add-parrafo', function(e) {
            e.preventDefault();
            var cloneFrom = $(this).attr('data-clone-from');
            var cloneTo = $(this).attr('data-clone-to');
            var newFicha = $(cloneFrom).clone();
            var now = Date.now();

            newFicha.find('.renameId').each(function() {
                var id = $(this).data('id');
                $(this).attr('id', id+now);
                $(this).removeClass('renameId').removeAttr('data-id');
            });

            newFicha.find('.renameClass').each(function() {
                var clase = $(this).data('clase');
                $(this).addClass(clase+now);
                $(this).removeClass('renameClass').removeAttr('data-clase');
            });

            newFicha.find('.renameDataUrl').each(function() {
                var url = $(this).data('url');
                $(this).removeAttr('data-url').attr('data-url', url+now);
                $(this).removeClass('renameDataUrl');
            });

            var new_IdParrafo = newFicha.data('id')+now;
            newFicha.removeAttr('id');
            newFicha.attr( 'id', new_IdParrafo ).removeAttr('data-id');

            $(cloneTo).append(newFicha);
            $("*[data-tooltip=\"tooltip\"]").tooltip();

            initEditor( 'txt_parrafo_'+now );
            $('#'+new_IdParrafo).removeClass('hidden');
        }).on('click', '.plantilla-ordenar  .generar-parrafo', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var plantilla=$(this).closest('.plantilla-ordenar');
            plantilla.removeClass('editando');
            var idTmpl = plantilla.attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            var idCloneFrom = $(this).data('clone-from');
            var idCloneTo = $(this).data('clone-to');
            var result = generarParrafo(idCloneFrom, idCloneTo, idguiTmpl);
            $('#ejerc-ordenar'+idguiTmpl).show();
            $('#lista-parrafo-edit'+idguiTmpl).hide().addClass('hidden');
            $('#'+idTmpl+'  .botones-editar').show();
            $('#'+idTmpl+'  .botones-creacion').hide().addClass('hidden');
            initOrdenarParrafos(idguiTmpl, "en_US");
            showBotonesguardardoc();
        }).on('click', '.plantilla-ordenar  .back-edit-parr', function(e) {
            e.preventDefault();
            var plantilla=$(this).parents('.plantilla-ordenar');
            var idTmpl = plantilla.attr('id');
             plantilla.addClass('editando');
            var idguiTmpl = idTmpl.split('_').pop();

            $('#ejerc-ordenar'+idguiTmpl).hide();
            iniciarTodosEditores( $('#lista-parrafo-edit'+idguiTmpl) );
            $('#lista-parrafo-edit'+idguiTmpl).show().removeClass('hidden');
            $('#'+idTmpl+'  .botones-editar').hide();
            $('#'+idTmpl+'  .botones-creacion').show().removeClass('hidden');
            $('#lista-parrafo-edit'+idguiTmpl+'  input[type="checkbox"]').each(function() {
                var isChecked = $(this).attr('checked');
                if(isChecked!=undefined){
                    $(this).removeAttr('checked');
                    $(this).prop('checked', true);
                }
            });
            showBotonesguardardoc();
        }).on('click', '.plantilla-ordenar .ejerc-ordenar .drag-parr>div', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $this= $(this);
            var isDBY = esDBYself( $this );
            var condicion = isDBY;
            var buscarTodos = '.drop-parr>div.filled';
            var buscarCorrectos = '.drop-parr>div.good';
            var fn = function(){
                var value= $this.html();
                var id = $this.attr('id');
                var orden=$this.attr('data-orden'); 
                var $ejercOrdenar = $this.parents('#ejerc-ordenar'+idguiTmpl);
                var primerVacio = $ejercOrdenar.find('.drop-parr div:empty').first();
                primerVacio.html(value).attr('data-orden',orden);
                primerVacio.addClass('filled').attr('id', id);
                $this.remove();
                evaluarOrdenParrafo( primerVacio, idguiTmpl);
                calcularBarraProgreso();
            };
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
        }).on('click', '.plantilla-ordenar .ejerc-ordenar .drop-parr>div.filled', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $this= $(this);
            var isDBY = esDBYself( $this );
            var condicion = !isDBY;
            var buscarTodos = '.drop-parr>div.filled';
            var buscarCorrectos = '.drop-parr>div.good';
            var fn = function(){
                if(!$this.hasClass('finish')){
                    var value= $this.html();
                    var id = $this.attr('id');
                    var orden=$this.attr('data-orden');
                    var $ejercOrdenar = $this.parents('#ejerc-ordenar'+idguiTmpl);
                    $ejercOrdenar.find('.drag-parr').append('<div id="'+id+'" data-orden="'+orden+'">'+value+'</div>');
                    $this.html('').removeClass('filled').removeAttr('id');
                    evaluarOrdenParrafo( $this, idguiTmpl);
                    calcularBarraProgreso();
                }
            };
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
        });

        var sysinitImageTag=function(){
            $('.plantilla-img_puntos').each(function(){
                var idgui=$(this).attr('data-idgui');
                var ayuda=$(this).closest('.method').attr('data-idmet')==3?false:true;
                initImageTagged(idgui,ayuda);
            });
        }

//Plantilla Imagen Etiquetada (con puntos):
        objcont.on('click', '.plantilla-img_puntos .botones-edicion .start-tag', function(e){
            e.preventDefault();
            var idTmpl = $(this).closest('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+'  .mask-dots').addClass('start-tagging');
            $(this).hide();
            $(this).siblings('.btn').addClass('disabled');
            $('#'+idTmpl+'  .stop-tag').show().removeClass('disabled');
        }).on('click', '.plantilla-img_puntos .botones-edicion  .stop-tag', function(e) {
            e.preventDefault();
            var idTmpl = $(this).closest('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+' .mask-dots').removeClass('start-tagging');
            $(this).hide();
            $(this).siblings('.btn').removeClass('disabled');
            $('#'+idTmpl+' .start-tag').show().removeClass('disabled');
        }).on('click', '.plantilla-img_puntos .botones-edicion  .delete-tag', function(e) {
            e.preventDefault();
            if(_cureditable==false) return false;
            var idTmpl = $(this).closest('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+' .mask-dots').addClass('delete-tagging');
            $(this).hide();
            $(this).siblings('.btn').addClass('disabled');
            $('#'+idTmpl+' .stop-del-tag').show().removeClass('disabled');
        }).on('click', '.plantilla-img_puntos .botones-edicion  .stop-del-tag', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+' .mask-dots').removeClass('delete-tagging');
            $(this).hide();
            $(this).siblings('.btn').removeClass('disabled');
            $('#'+idTmpl+' .delete-tag').show().removeClass('disabled');
        }).on('click', '.plantilla-img_puntos .botones-edicion  .generate-tag', function(e) {
            e.preventDefault();
            if(_cureditable==false) return false;
            var plantilla=$(this).closest('.plantilla-img_puntos');
            plantilla.removeClass('editando');
            var idTmpl = plantilla.attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            $(this).hide();
            $(this).siblings('.btn').hide();
            $('#'+idTmpl+' .back-edit-tag').show();
            generarTags($('#'+idTmpl+' .tag-alts-edit'), $('#'+idTmpl+' .tag-alts'));
            $('#'+idTmpl+' .tag-alts-edit').addClass('hidden').hide();
            $('#'+idTmpl+' .mask-dots .dot-container').each(function() {
                $(this).removeClass('edition');
                $(this).find('.dot-tag').attr('data-number', $(this).find('.dot-tag').text() );
            });
            $('#'+idTmpl+' .tag-alts').show();
            $('#'+idTmpl+' .mask-dots').addClass('playing');
            playEjercicio(idguiTmpl, $('#'+idTmpl+' .tag-alts'));
            showBotonesguardardoc();
        }).on('click', '.plantilla-img_puntos .botones-edicion  .back-edit-tag', function(e) {
            e.preventDefault();
            if(_cureditable==false) return false;
            var plantilla=$(this).closest('.plantilla-img_puntos');
            plantilla.addClass('editando');
            var idTmpl = plantilla.attr('id');
            var idgui=plantilla.attr('data-idgui');
            $(this).hide();
            $(this).siblings('.btn').show();
            $('.btn.stop-tag, .btn.stop-del-tag').hide();
            $('#'+idTmpl+' .tag-alts').hide();
            $('#'+idTmpl+' .mask-dots .dot-container').each(function() {
                $(this).addClass('edition');
                $(this).find('.dot-tag').text( $(this).find('.dot-tag').attr('data-number') );
                var numero=$(this).find('.dot-tag').attr('data-number');
                $(this).find('.dot-tag').removeAttr('data-number').removeClass('corregido good bad');
                $(this).removeClass('hover');
                //$(this).attr('id', 'dot_'+ANS_TAG[idgui][numero]);
            });
            $('#'+idTmpl+'  .tag-alts-edit').removeClass('hidden').show();
            $('#'+idTmpl+'  .mask-dots').removeClass('playing');
            showBotonesguardardoc();
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots.start-tagging', function(e) {
            var tpl=$(this).parents('.plantilla-img_puntos');
            var idTmpl = tpl.attr('id');
            var idguiTmpl = tpl.attr('data-idgui');
            $('.clone_punto'+idguiTmpl).attr('id','clone_punto'+idguiTmpl).removeClass('clone_punto'+idguiTmpl);

            var resp = crearPunto(e, '#clone_punto'+idguiTmpl, '#'+idTmpl+' .mask-dots', idguiTmpl);
            crearInput(resp.cant, resp.now, MSJES_PHP.write_tag, idguiTmpl);
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots.delete-tagging .dot', function(e) {
            var idTmpl = $(this).closest('.plantilla-img_puntos').attr('id');
            eliminarTag( $(this).parent(), $('#'+idTmpl+' .tag-alts-edit') );
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots .dot-container.edition>.dot', function(e) {
            var idTmpl = $(this).closest('.plantilla-img_puntos').attr('id');
            var indexId = $(this).parent().attr('id').split('_')[1];
            $('#'+idTmpl+'   #edit_tag_'+indexId+' input').focus();
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots.playing .dot', function(e) {
            //e.stopPropagation();
            $this=$(this);
            if($this.hasClass('_success_')) return false;

            var isDotIt = esDBYself($this); //@edu se agrego
            //@edu se agrego bloque if - inicio
            if(isDotIt){
                if($this.hasClass('_fallo_')) return false;
            }   //fin

            var idTmpl = $(this).closest('.plantilla-img_puntos').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            if( HELP[idguiTmpl] ){
                if( $(this).parent().hasClass('hover') ){
                    $(this).parent().removeClass('hover');
                    $('#'+idTmpl+' .tag-alts>.tag').removeClass('active');
                } else {
                    $('#'+idTmpl+' .mask-dots.playing .dot-container').removeClass('hover');
                    $(this).parent().addClass('hover');
                    $('#'+idTmpl+' .tag-alts>.tag').addClass('active');
                }
            }else {
                if( !$(this).siblings('.dot-tag').hasClass('corregido') ){
                    if( $(this).parent().hasClass('hover') ){
                        $(this).parent().removeClass('hover');
                        $('#'+idTmpl+' .tag-alts>.tag').removeClass('active');
                    } else {
                        $('#'+idTmpl+' .mask-dots.playing .dot-container').removeClass('hover');
                        $(this).parent().addClass('hover');
                        $('#'+idTmpl+' .tag-alts>.tag').addClass('active');
                    }
                }
            }
        }).on('focusin', '.plantilla-img_puntos .tag-alts-edit input[type="text"]', function(e) {
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            var indexId = $(this).parent().attr('id').split('_').pop();
            $('#'+idTmpl+' .mask-dots #dot_'+indexId).addClass('hover');
        }).on('focusout', '.plantilla-img_puntos .tag-alts-edit input[type="text"]', function(e) {
            e.stopPropagation();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+' .mask-dots .dot-container').removeClass('hover');
        }).on('click', '.plantilla-img_puntos .tag-alts .tag.active', function(e) {
            e.preventDefault();
            var $this = $(this);
            if($this.hasClass('_success_')) return false;
            var idTmpl = $this.parents('.plantilla-img_puntos').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            var textTag = $this.text();
            var isDBY = esDBYself( $this );
            var condicion = isDBY;
            var buscarTodos = '.dot';
            var buscarCorrectos = '.dot-tag.corregido.good';
            var fn = function(){
                $('#'+idTmpl+' .mask-dots .dot-container.hover .dot-tag').text(textTag);
                evaluarTag($this, $('#'+idTmpl+' .mask-dots .dot-container.hover'), HELP[idguiTmpl], idguiTmpl);
                calcularBarraProgreso();
            }
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
            if( $('#'+idTmpl+' .mask-dots.playing').children().hasClass('hover') ){
                $('#'+idTmpl+' .mask-dots.playing').children().removeClass('hover');
                $('#'+idTmpl+' .tag-alts>.tag').removeClass('active');
            }
        });

            var inicio=function(){
                $('.plantilla',objcont).removeClass('editando');
                EjercicioActivo();
                showBotonesguardardoc();
                addBtnsControlAlumno();
                initcompletar();
                sysinitImageTag();
                sysinitOrdenar();
                $('.addtext').removeAttr('style');              
                var id_panelInicial = $('ul.nav-pills.metodologias li.active a',objcont).attr('href');
                if (id_panelInicial!='' && id_panelInicial!=undefined){                    
                    mostrarHabilidadesActivas(id_panelInicial);
                }else{                   
                    mostrarHabilidadesActivas(objcont);
                }

                showliminimo();
                calcularBarraProgreso();
                DBY_mostrar_ocultarBtnProgreso();
                actualizarPanelesInfo();
                mostrarPanelIntentos();
                verautor();
                acomodarvariablestpl();
                resetEdicion_variables();
                if(_cureditable==true){
                    $('.plantilla-dialogo .d-box').show();
                }
            }
            inicio();
            return this;
        });
    };
}(jQuery));
 $(document).ready(function(){
    var objetos = $('input.mce-object-input');
    var objAlternativas = $('.panelAlternativas');
    objetos.each(function(e){
        var texto=($(this).data("texto").length);
        var tamanio=60;
        if(texto<10) tamanio = (($(this).data("texto").length) * 4) + 55;
        else tamanio = (($(this).data("texto").length) * 5) + 60;
        $(this).css('width',tamanio + "px");
    });
    objAlternativas.each(function(index, el) {
        if($(this).html().trim().length == 0){
            $(this).parent().hide();
        }
        $(this).css('box-shadow','initial');
    });    
 });
