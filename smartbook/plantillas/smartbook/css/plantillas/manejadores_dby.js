var dby_handlerInputMCE_focusin = function(e,params){
    var params=params||false;
    var idPanel =  params.id;
    var _this =  params.obj;
    var $tmplActiva = getTmplActiva();
    var $plantActiva = $tmplActiva.find('.plantilla');
    e.preventDefault();

    $(_this).on('focusout');
    // $(_this).on('focusout',dby_handlerInputMCE_focusout(e));
    console.log("asdasdsad");
    if(!$(_this).hasClass('active') ){
        
        $(idPanel).find('input.mce-object-input').removeClass('open').removeClass('active');
        removerTodosFlotantes(idPanel); 
    }

    var isDBY = esDBYself($(_this));
    var isCompleto = inputMCE_Completo( $(_this) );
    if( !$(_this).hasClass('open') ){
        $(_this).off('focusout');
        //$(_this).on('focusout',function(e){ e.preventDefault(); return false;});
        $(_this).addClass('open active');
        var id = generarID(_this);
        prepararInput(_this, id);

        if( !$(_this).hasClass('iswrite') ) {
            $(_this).attr('readonly', 'readonly');
        } else {
            if( isDBY && !isCompleto && !$plantActiva.hasClass('tiempo-acabo')){
                $(_this).focus();
            }else{
                if( $plantActiva.hasClass('ejerc-terminado-correcto') ){
                    mostrar_notificacion(MSJES_PHP.bien_hecho, MSJES_PHP.puedes_continuar,'success');
                } else { 
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar,'warning');
                }
                habilitarElem(_this, false);
            }
        }

        if( $(_this).hasClass('isayuda') ){
            if( isDBY && !isCompleto && !$plantActiva.hasClass('tiempo-acabo') ){
                desplegarAyuda(_this, id);
            }
        }

        if( $(_this).hasClass('ischoice') ){
            if( isDBY && !isCompleto){
                desplegarOpciones(_this, id);
            }else{
                if( $plantActiva.hasClass('ejerc-terminado-correcto') ){
                    mostrar_notificacion(MSJES_PHP.bien_hecho, MSJES_PHP.puedes_continuar,'success');
                } else {
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar,'warning');
                }
                habilitarElem(_this, false);
            }
        }
    } 
};

var dby_handlerBtnHelp_click = function(e){
    e.preventDefault();
    e.stopPropagation();
    var $tmplActiva = getTmplActiva();
    var $plantActiva = $tmplActiva.find('.plantilla');
    if( !$plantActiva.hasClass('tiempo-acabo') ){
        var $inputMCE = $(this).parent().siblings('input[data-mce-object="input"]');
        if( $(this).find('span.tip-word').length==0 ){
            $(this).append('<span class="tip-word"> : </span>');
        }
        var rspta= $(this).parent().siblings('input.open').data('texto');
        var arr_rspta= rspta.split("");
        var i = $('span.tip-word').attr('data-pulsado');
        if(i==''||i==undefined){
            i = 0;
        }
        i = parseInt(i);
        if( i < arr_rspta.length){
            $('span.tip-word').attr('data-pulsado',(i+1));
            var texto = $('span.tip-word').text();
            texto = texto.concat(arr_rspta[i]);
            $('span.tip-word').text(texto);
        }
        $inputMCE.focus();
    }
};

/*var dby_handlerListAlternativas_click = function(e){
    e.preventDefault();
    var _this = this;
    var isDBY = esDBYself( $(_this) );
    var IDGUI = e.data.idgui;
    var condicion = (isDBY && !existeAltMarcada($('#pnl_editalternatives'+IDGUI)));
    var buscar = '.panelEjercicio input.mce-object-input',
    buscarCorrectos = '.panelEjercicio .inp-corregido[data-corregido="good"]';
    var fn = function(){
        $('#pnl_editalternatives'+IDGUI+' ul.alternativas-list li.alt-item a').each(function() {
            if( $(_this).parent().hasClass('wrapper') ){
                $(_this).siblings('span.inner-icon').remove();
                $(_this).unwrap();
            }
        });
        evaluarRsptaAltern(_this, IDGUI);
    };

    fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
};*/

var dby_handlerIsDrop_drop = function(e){
    e.preventDefault();  
    e.stopPropagation();
    var _this = this;
    var isDBY = esDBYself( $(_this) );
    var isCompleto = inputMCE_Completo( $(_this) );
    var condicion = ( isDBY && !isCompleto );
    var buscar = 'input.mce-object-input',
    buscarCorrectos = '.inp-corregido[data-corregido="good"]';
    var fn = function(){
        var idinmove=e.originalEvent.dataTransfer.getData("id");
        texto=$('#'+idinmove).text().trim();
        $(_this).val(texto);
        evaluarRespuesta(_this);
    };
    fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
};

var dby_handlerInputMCE_focusout = function(e){   
    e.preventDefault();
    e.stopPropagation();
    var _this = this;
    if(!$(_this).hasClass('iswrite') ) {
        return false 
    }
    evaluarRespuesta(_this);
};

/*var dby_handlerOptionList_click = function(e){
    e.preventDefault();
    var _this = this;
    var isDBY = esDBYself( $(_this) );
    var isCompleto = inputMCE_Completo( $(_this) );
    var condicion = (isDBY && !isCompleto);
    var buscar = '.panelEjercicio input.mce-object-input',
    buscarCorrectos = '.panelEjercicio .inp-corregido[data-corregido="good"]';
    var fn = function(){
        var value = $(_this).text();
        var input = $(_this).parents('ul.options-list').siblings('input.open');
        input.val(value);
        cerrarFlotantes(input);
        evaluarRespuesta(input);
    };

    fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
};*/

var dby_handlerInputMCE_keyup = function(e){
    var _this = this;
    var buscar = 'input.mce-object-input',
        buscarCorrectos = '.inp-corregido[data-corregido="good"]';
    var isDBY = esDBYself( $(_this) );
    var isCompleto = inputMCE_Completo( $(_this) );
    var condicion = ( isDBY && isCompleto );
    
    if(e.which != 13){
        var value = $(_this).val();
        var rspta_crrta = $(_this).data('texto');
        if(value.length>0){
            if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){

                if( $(_this).parents('.wrapper').hasClass('input-flotantes') ){
                    $(_this).parents('.input-flotantes.wrapper').find('.helper-zone').remove();
                    $(_this).parents('.input-flotantes.wrapper').find('.options-list').remove();
                }
                $(_this).removeClass('active open');

                if($(_this).parents('.wrapper').hasClass('input-flotantes')){
                    $(_this).parents('.input-flotantes.wrapper').children().unwrap();
                }
                var fn = function(){
                   evaluarRespuesta(_this);
                };
                fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);

                var $next_input = $(_this).parents('p').children('input.mce-object-input');
                if( $next_input.length>0 ){
                    $next_input.trigger('focusin');
                } else {
                    $next_input = $(_this).parents('p').next('p').children('input.mce-object-input');
                    $next_input.trigger('focusin');
                }
            }
        }
    }
};

var dby_handlerInputMCE_keypress = function(e){
    if($(this).parents('.plantilla').hasClass('tiempo-acabo')) return false;   
    if(e.which == 13){
        e.preventDefault();
        if( $(this).parents('.wrapper').hasClass('input-flotantes') ){
            $(this).parents('.input-flotantes.wrapper').find('.helper-zone').remove();
            $(this).parents('.input-flotantes.wrapper').find('.options-list').remove();
        }
        $(this).removeClass('active open');

        if($(this).parents('.wrapper').hasClass('input-flotantes')){
            $(this).parents('.input-flotantes.wrapper').children().unwrap();
        }
    }
};


function resetAllplantilla($plantilla){    
    if($plantilla.hasClass('plantilla-completar')){
        var tipo=$plantilla.closest('.aquicargaplantilla').attr('data-addclass');
        var inputsMCE = $plantilla.find('input[data-mce-object="input"]');       
        inputsMCE.each(function(){
            eleinput=$(this);
            eleinput.removeClass('_success_').val('').attr('value','');
            cerrarFlotantes(this);
            if(eleinput.parent().hasClass('wrapper')){
              eleinput.siblings().remove();
              eleinput.unwrap();
            }
            eleinput.removeClass('open');
            if(eleinput.hasClass('isclicked')){
                var altern = eleinput.parents('div.panel').parent().parent().siblings().find('.inp-corregido.wrapper>a');
                    altern.each(function() {
                        while( !$(this).parent().is('li.alt-item') ){
                          $(this).siblings().remove();
                          $(this).unwrap();
                        }
                    });
            }
            if(tipo=='iswrite' && !eleinput.hasClass('iswrite'))  eleinput.addClass('iswrite');
            eleinput.removeAttr('readonly').removeClass('readonly');
        });
        var $imgs = $plantilla.find('img[data-mce-object="image"]');
        if($imgs.length>0){ $imgs.addClass('gray-scale'); }        
    }
    var intento_actual=parseInt($plantilla.attr('data-intento')||1);
    if(intento_actual==0)intento_actual=1;   
    $('#panel-intentos-dby').find('.actual').text(intento_actual);
}