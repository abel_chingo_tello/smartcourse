<?php defined('_BOOT_') or die(''); $version=!empty(_version_)?_version_:'1.0'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google" content="notranslate" />
    <link href="<?php echo $documento->getUrlStatic()?>/libs/bootstrap3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL_BASE ?>smartbook/plantillas/smartbook/css/mobile/general.css" />
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css?vs=<?php echo $version; ?>" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_inicio.css?vs=<?php echo $version; ?>" rel="stylesheet">
    <script src="<?php echo $documento->getUrlStatic()?>/libs/smartenglish/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/sweetalert/sweetalert2.all.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/pnotify.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/libs/smartenglish/funciones.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/inicio.js"></script>
    <script> 
        var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
        var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
        var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';
        var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';

    </script>
    <jrdoc:incluir tipo="cabecera" />
</head>
<body >   
<div class="container">    
    <jrdoc:incluir tipo="recurso" />    
</div>
<div class="modal fade" id="modalclone" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="modaltitle" style="width: 100%"></h5>
          <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" id="modalcontent">
          <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $documento->getUrlStatic() ?>/media/cargando.gif"><br><span style="color: #006E84;"><?php echo JrTexto::_("Loading"); ?></span></div>
        </div>
        <div class="modal-footer" id="modalfooter">
          <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
        </div>
      </div>
    </div>
  </div>
<jrdoc:incluir tipo="modulo" nombre="preload" posicion=""/></jrdoc:incluir>
<jrdoc:incluir tipo="docsJs" />
 <script src="<?php echo $documento->getUrlStatic()?>/libs/smartenglish/bootstrap.min.js"></script>
<!--script src="<?php echo URL_BASE; ?>static/tema/paris/adminLTE3/bootstrap/js/bootstrap.bundle.min.js"></script-->
    <script type="text/javascript" src="<?php echo URL_BASE; ?>static/libs/poppers/dist/umd/popper.min.js"></script>
<script type="text/javascript">
var estainteractuando = true;
var revisarInactividad = function () {
    var t;
    window.onload = reiniciarTiempo;
    document.onmousemove = reiniciarTiempo;
    document.onkeypress = reiniciarTiempo;
    document.onload = reiniciarTiempo;
    document.onmousemove = reiniciarTiempo;
    document.onmousedown = reiniciarTiempo;
    document.ontouchstart = reiniciarTiempo;
    document.onclick = reiniciarTiempo; 
    document.onscroll = reiniciarTiempo;
    document.onkeypress = reiniciarTiempo;
    function tiempoExcedido() {
        estainteractuando = false;
        revisarInactividad();
    }

    function reiniciarTiempo() {
        clearTimeout(t);
        t = setTimeout(tiempoExcedido, 3000)
        estainteractuando = true;
    }
};
revisarInactividad();

var _tooltipactual='.tooltip1';
var _asistentetooltipcontador=0;
var t1;
var asistentetooltip=function(){
	//console.log(_tooltipactual);
	//if(estainteractuando==false){
		if(t1!=null) clearTimeout(t1);
		_asistentetooltipcontador++;

		if(isMobile.any()!=null){
			let $tooltiptmp=$(_tooltipactual);
			//console.log(_asistentetooltipcontador,$tooltiptmp);
			if(_asistentetooltipcontador<10 && $tooltiptmp.length){
				$(_tooltipactual).tooltip('toggle');
			}
		}

		t1=setTimeout(function(){		
			if(_asistentetooltipcontador==20) _asistentetooltipcontador=0;
			asistentetooltip();
		},1500);
	//}
};

var inicio=false;
$(document).ready(function(){
	var _ismobiletmp=isMobile.any();
	asistentetooltip();
	$('body').on('click','.asistentetooltip',function(ev){		
		let $tooltiptmp=$(_tooltipactual);
		_tooltipactual=$(this).attr('shownexttooltip')||'';
		setTimeout(function(){$tooltiptmp.tooltip('hide')},1000);
	})
	$('body').click(function(){
		$('[data-toggle="tooltip"]').tooltip('hide');
  })
 	$('[data-toggle="tooltip"]').tooltip();
    	
});

</script>
<style>
.bs-tooltip-auto[x-placement^=bottom] .arrow::before, .bs-tooltip-bottom .arrow::before {
    border-bottom-color: #f40505;
}

.tooltip-inner {
    color: #fff;
    background-color: #f40505;
}
</body>
</html>