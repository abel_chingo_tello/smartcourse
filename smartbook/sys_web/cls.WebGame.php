<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/07/2012
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGame', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_herramientas', RUTA_BASE);

class WebGame extends JrWeb
{
	private $oNegGame;
    private $oNegHerramientas;

    protected $url_SE;
    protected $css;

	public function __construct()
	{
        if(false === NegSesion::existeSesion()){
            header('Location: '.URL_BASE);
            exit();
        }
        parent::__construct();
        $this->DB=NAME_BDSEAdolecentes;
        $this->url_SE=URL_SEAdolecentes;
        $this->css='ver';
        if(!empty($_REQUEST['PV'])){
            if($_REQUEST['PV']=='PVADU_1'){
                $this->DB=NAME_BDSEAdultos;
                $this->url_SE=URL_SEAdultos;
                $this->css='ver_adultos';
            }
        }
        $this->oNegGame = new NegGame;
        $this->NegEnglish_Herramientas = new NegEnglish_herramientas($this->DB);      
	}
	
	public function defecto()
	{		
		return $this->ver();		
	}


    public function ver(){
        try{
            global $aplicacion;         
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.ui.touch-punch.min','/libs/jquery-ui-touch/');
            $this->documento->stylesheet('estilo', '/libs/crusigrama/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
            $this->documento->script('tools_games', '/js/new/');
            $this->documento->script('plugin_visor', '/js/');
            $usuarioAct = NegSesion::getUsuario();
            $idgame=!empty($_REQUEST["idgame"])?$_REQUEST["idgame"]:9;
            $idgame=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):$idgame;
            $this->datos=$this->NegEnglish_Herramientas->buscar(Array('id'=>$idgame,'tool'=>'G','BD'=>$this->DB)); 
            $this->rolUsuarioAct = $usuarioAct['rol'];
            //$arrDatosTarea = $this->fnTareaArchivo($idgame);
            $this->idTareaArchivo = @$_GET['idtarea_archivos'];
            $this->tarea = @$arrDatosTarea['tarea'];
            $this->tarea_archivo = @$arrDatosTarea['tarea_archivo'];
            $this->tarea_asignacion_alumno = @$arrDatosTarea['tarea_asignacion_alumno'];
            $this->tarea_respuesta = @$arrDatosTarea['tarea_respuesta'];
            $this->documento->plantilla='verblanco';
            $this->esquema = 'tools/game_ver';
            return parent::getEsquema();
        }catch(Exception $e) {
            /*$aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();*/
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }


}