<style>
    * {
        font-family: 'Montserrat', sans-serif;
    }
    .icon {
        margin: auto 15px; width: 23px; height: 21px; display: block; background-size: 19px 21px; background-repeat: no-repeat;
    }
    .icon-simple-arrow { 
        background-image: url('<?=$this->rutaAssets?>/img/icon -onic-ios-arrow-forward.svg'); 
    }
    .icon-circle-sb{
        background-image: url('<?=$this->rutaAssets?>/img/icons/icon-simple-circle.svg'); 
    }
    .icon-simple-circle-blue {
        background-image: url('<?=$this->rutaAssets?>/img/icon-simple-circle-blue.svg');
    }

    .icon-simple-circle-green {
        background-image: url('<?=$this->rutaAssets?>/img/icon-simple-circle-green.svg');
    }

    .icon-simple-circle-magento {
        background-image: url('<?=$this->rutaAssets?>/img/icon-simple-circle-magento.svg');
    }
    .icon-simple-circle-yellow{
        background-image: url('<?=$this->rutaAssets?>/img/icon-simple-circle-yellow.svg');
    }

    .text-ellipsis { 
        text-overflow: ellipsis; overflow: hidden; white-space: nowrap; 
    }
    .img-smartbook-portada{ 
        position:relative; background-image: url('../img/icons/smartbookportada.png'); background-repeat: no-repeat; background-size: cover;
    }
    .img-smartbook-portada::before{ 
        content: ""; position: absolute; width: 100%; height: 100%; background: #b8d2ff52;
    }
    .txt_title_smartbook {
        font-size: 4rem; opacity: .8; font-weight: 600; padding-top: 0.5em; color: white;
    }
    .txt_subtitle_smartbook{
        font-size: 25px; color: white; padding-left: 15px; padding-bottom: 20px;
    }
    .txt_spantitle_smartbook{
        background-color: #b9b9b978; padding: 0 5px; border-radius: .2em;
    }
    .txt_title_option{
        font-size: 20px;
    }
    .txt_subtitle_option{
        font-size: 12px;
    }
    .Menu-item{ 
        margin:2em 0 
    }
    .btnpronunciacionshow{ border-radius: 1em; }
    
</style>
<div class="smartbook-portada">
    <div class="img-smartbook-portada" style="background-image: url('<?php echo @str_replace('__xRUTABASEx__/', $this->url_SE, $this->sesion["imagen"]) ;?>'); ">
        <div class="mask-smartbook-portada"></div>
        <div class="title-smartbook">
            <h3 class="txt_title_smartbook white m-auto">Smartbook</h3>
            <p class="txt_subtitle_smartbook text-ellipsis">
                <span class="txt_spantitle_smartbook"><?=@$this->sesion["nombre"];?></span>
            </p>
        </div>
    </div>
</div>
<a href="#" class="btn btn-lilac btn-pronunciar btnpronunciacionshow" data-source="<?= $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion">
    <i class="fa fa-bullhorn"></i> <?= ucfirst(JrTexto::_("Pronunciation")) ?>
</a>
<!--START PRONUNCIACION CONTENT-->
<div style="display: none;">
    <div id="ventanapronunciacion">
        <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Volume")); ?>:</label>
                <input type="range" id="voicevolume" min="0" max="1" value="0.7" step="0.1" />
            </div>
          </div>
          <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Speed")); ?>:</label>
                <input type="range" id="voiceSpeed" min="0.1" max="2" value="1" step="0.1" />
            </div>
          </div>
        <!-- <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Pitch")); ?>:</label>
                <input type="range" id="voicepitch" min="0" max="2" value="1" step="0.1" />
            </div>
          </div> -->
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Voice"))?> :</label>
              <div class="cajaselect">
                <select name="voicepronunciation" id="voicepronunciation" class="speachvoives form-control">                  
                </select>
              </div>
            </div>            
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Write text"))?> :</label>
              <textarea id="texto" placeholder="Write text here" style="resize:none;" class="form-control" rows="3"></textarea>              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button id="btn-leertexto" type="button" class="btn btn-default center-block"><?php echo JrTexto::_('Listen to the pronunciation'); ?><i class="fa fa-bullhorn"></i></button>
              <br>
              <button  type="button" class="btn btn-warning center-block cerrarmodal"><?php echo JrTexto::_('Exit');?> <i class="fa fa-close"></i> </button>
          </div>
        </div> 
      </div>
    </div>
</div>
<!--END PRONUNCIACION CONTENT-->
<!--START MENU CONTENT-->
<div>
    <?php if(!empty($this->look)): ?>
        <a href="<?= $this->rutabase."pestana/look".$this->parametros ?>">
            <div class="Menu-item">
                <div class="row m-0 p-0">
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-circle-sb"></i>
                    </div>
                    <div class="col-xs-6">
                        <p class="txt_title_option">Look</p>
                        <p class="txt_subtitle_option text-ellipsis">Presentación del curso</p>
                    </div>
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-arrow"></i>
                    </div>
                </div>
            </div>
        </a>
    <?php endif; ?>
    <?php if(!empty($this->ejercicios)): ?> 
        <a href="<?= $this->rutabase."pestana/practice".$this->parametros ?>">
            <div class="Menu-item">
                <div class="row m-0 p-0">
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-circle-yellow"></i>
                    </div>
                    <div class="col-xs-6">
                        <p class="txt_title_option">Practice</p>
                        <p class="txt_subtitle_option text-ellipsis">Ejercicios del curso</p>
                    </div>
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-arrow"></i>
                    </div>
                </div>
            </div>
        </a>
    <?php endif; ?>
    <?php if(!empty($this->games)): ?>
        <a href="<?= $this->rutabase."pestana/games".$this->parametros ?>">
            <div class="Menu-item">
                <div class="row m-0 p-0">
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-circle-blue"></i>
                    </div>
                    <div class="col-xs-6">
                        <p class="txt_title_option">Games</p>
                        <p class="txt_subtitle_option text-ellipsis">Juegos del curso</p>
                    </div>
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-arrow"></i>
                    </div>
                </div>
            </div>
        </a>
    <?php endif; ?>
    <?php if(!empty($this->vocabulario)): ?>
        <a href="<?= $this->rutabase."pestana/vocabulary".$this->parametros ?>">
            <div class="Menu-item">
                <div class="row m-0 p-0">
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-circle-green"></i>
                    </div>
                    <div class="col-xs-6">
                        <p class="txt_title_option">Vocabulary</p>
                        <p class="txt_subtitle_option text-ellipsis">Vocabularios del curso</p>
                    </div>
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-arrow"></i>
                    </div>
                </div>
            </div>
        </a>
    <?php endif; ?>
    <?php if(!empty($this->dby)): ?>
        <a href="<?= $this->rutabase."pestana/dby".$this->parametros ?>">
            <div class="Menu-item">
                <div class="row m-0 p-0">
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-circle-magento"></i>
                    </div>
                    <div class="col-xs-6">
                        <p class="txt_title_option">Do it by youself</p>
                        <p class="txt_subtitle_option text-ellipsis">Ejercicios del curso</p>
                    </div>
                    <div class="col-xs-3">
                        <i style="padding: 5px" class="icon icon-simple-arrow"></i>
                    </div>
                </div>
            </div>
        </a>
    <?php endif; ?>
</div>
<!--END MENU CONTENT-->
<script>
    try{
        const origin = String.prototype.concat(window.location.protocol,"//",window.location.hostname) 
        window.parent.postMessage({modulo:"smartbook",vista:"menu"},origin)        
    }catch(err){
        console.error("Error en postMessage ",err)
    }
    $(document).ready(function(){
        $('body').on('click', '.btnpronunciacionshow', function(e){
            e.preventDefault();
            var htmlmodal = $('#ventanapronunciacion').clone(true);
            var dt = {
                titulo:'pronunciation',  
                html: htmlmodal
            };
            var md = __sysmodal(dt);
            md.on('click','#btn-leertexto',function(ev){
                texto=md.find('#texto').val();
                pronunciar(texto);            
            }).on('change','#voicepronunciation',function(ev){
                var voice=$(this).val();
                __cambiarvoice(voice);
            });
        }); 
    })
</script>