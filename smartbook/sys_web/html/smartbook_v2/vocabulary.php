<div class="mask-smartbook-portada-detail">
    <div class="row mask-smartbook-row">
        <div class="col-xs-2">
            <a href="<?= $this->rutabase."/ver".$this->parametros ?>" >
                <i class="icon icon-arrow-left"></i>
            </a>
            <div class="separator-detail-arrow"></div>
        </div>
        <div class="col-xs-10">
            <div class="csd-section2-top">
                <span class="miga txt-type-smartbook" style="background-color: green">Vocabulary</span>
                <h3 class="txt_title_smartbook-detail">Smartbook</h3>
            </div>
            <div class="csd-section2-bottom">
                <p class="txt_subtitle_smartbook-detail text-ellipsis">
                    <?=@$this->sesion["nombre"];?>
                </p>
            </div>
        </div>
    </div>
</div>
<div style="text-align: right; padding:5px;">
    <a href="#" class="btn btn-lilac btn-pronunciar btnpronunciacionshow" data-source="<?= $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion">
        <i class="fa fa-bullhorn"></i> <?= ucfirst(JrTexto::_("Pronunciation")) ?>
    </a>
</div>
<!--START PRONUNCIACION CONTENT-->
<div style="display: none;">
    <div id="ventanapronunciacion">
        <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Volume")); ?>:</label>
                <input type="range" id="voicevolume" min="0" max="1" value="0.7" step="0.1" />
            </div>
          </div>
          <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Speed")); ?>:</label>
                <input type="range" id="voiceSpeed" min="0.1" max="2" value="1" step="0.1" />
            </div>
          </div>
        <!-- <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Pitch")); ?>:</label>
                <input type="range" id="voicepitch" min="0" max="2" value="1" step="0.1" />
            </div>
          </div> -->
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Voice"))?> :</label>
              <div class="cajaselect">
                <select name="voicepronunciation" id="voicepronunciation" class="speachvoives form-control">                  
                </select>
              </div>
            </div>            
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Write text"))?> :</label>
              <textarea id="texto" placeholder="Write text here" style="resize:none;" class="form-control" rows="3"></textarea>              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button id="btn-leertexto" type="button" class="btn btn-default center-block"><?php echo JrTexto::_('Listen to the pronunciation'); ?><i class="fa fa-bullhorn"></i></button>
              <br>
              <button  type="button" class="btn btn-warning center-block cerrarmodal"><?php echo JrTexto::_('Exit');?> <i class="fa fa-close"></i> </button>
          </div>
        </div> 
      </div>
    </div>
</div>
<!--END PRONUNCIACION CONTENT-->
<div class="tab-pane" id="div_voca" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->vocabulario['bitacora'])?'data-idbitacora='.$this->vocabulario['bitacora']['idbitacora']:'' ?>>
    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Vocabulary") ?></h1>
    <div class="col-xs-12">
        <?php if(!empty($this->vocabulario)){
            foreach ($this->vocabulario as $i => $v) {
                $cadena_resultante=str_replace('__xRUTABASEx__static/',$this->url_SE."static/",@$v['texto']);
                $cadena_resultante=str_replace('__xRUTABASEx__/static/',$this->url_SE."static/",$cadena_resultante);
                $cadena_resultante=str_replace('<video','<video oncontextmenu="return false;" controlslist="nodownload" ',$cadena_resultante);
                $cadena_resultante= preg_replace('/\$\(\'\[toggle\-tooltip\=\"true\"\]\'\)\.tooltip\(\)\;/i', '', $cadena_resultante);
                $cadena_resultante=str_replace('pnlvocabulario edit','pnlvocabulario',$cadena_resultante);
                echo $cadena_resultante;                           
            } 
        }else{ ?>
        <div class="text-center">
            <h1 style="color: rgb(148, 148, 148);"><i class="fa fa-minus-circle fa-3x"></i></h1>
            <h4><?php echo JrTexto::_("No vocabulary to display"); ?></h4>
        </div>
        <?php } ?>
    </div>
</div>

<input type="hidden" id="rolUsuario" value="<?php echo @$this->idrol; ?>">
<input type="hidden" id="idCurso" value="<?php echo @$this->idcurso; ?>">
<input type="hidden" id="idSesion" value="<?php echo @$this->idactividad; ?>">
<input type="hidden" id="idrecurso_cursodetalle" value="<?= @$this->idsesionCursodetalle ?>" />
<input type="hidden" id="idgrupoauladetalle" value="<?= @$this->idgrupoauladetalle ?>" />
<input type="hidden" id="idBitacoraAlumnoSmbook" value="<?php echo !empty($this->bitac_alum_smbook)? @$this->bitac_alum_smbook['idbitacora_smartbook']:'';?>">
<input type="hidden" id="idLogro" value="<?php echo @$this->idLogro; ?>">
<input type="hidden" id="idcurso_sc" value="<?php echo $this->idcurso_sc; ?>">
<input type="hidden" id="idcc" value="<?php echo $this->idcc; ?>">
<input type="hidden" id="idcursodetalle_sc" value="<?php echo $this->idcursodetalle_sc; ?>">
<input type="hidden" id="PV" value="<?php echo $this->PV;?>">
<input type="hidden" id="PV_SE_URL" value="<?php echo $this->url_SE;?>">
<script type="text/javascript">
    try{
        const origin = String.prototype.concat(window.location.protocol,"//",window.location.hostname) 
        window.parent.postMessage({modulo:"smartbook",vista:"vocabulary"},origin)        
    }catch(err){
        console.error("Error en postMessage ",err)
    }
    $(document).ready(function(){
        $('body').on('click', '.btnpronunciacionshow', function(e){
            e.preventDefault();
            var htmlmodal = $('#ventanapronunciacion').clone(true);
            var dt = {
                titulo:'pronunciation',  
                html: htmlmodal
            };
            var md = __sysmodal(dt);
            md.on('click','#btn-leertexto',function(ev){
                texto=md.find('#texto').val();
                pronunciar(texto);            
            }).on('change','#voicepronunciation',function(ev){
                var voice=$(this).val();
                __cambiarvoice(voice);
            });
        }); 
    });
   /* var idTabPane = "#div_look"
    var $tabPane = $(idTabPane);
    var totalPestana = <?=!empty($this->totalPestana) ? $this->totalPestana : "4" ?>;

    window.addEventListener("load",main)

    function main(){
        let progreso = 100.00;
        let idbitacora=$tabPane.attr('data-idbitacora')||null;

        if(idbitacora==null){
            Utils.guardarBitacora({
                URLBASE:_sysUrlBase_,
                dataSave: {
                    'Idbitacora' : idbitacora,
                    'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val()||null,
                    'txtIdcurso' : $('#idCurso').val(),
                    'txtIdsesion' : '<?php echo $this->idcursoDetalle;?>',
                    'txtIdsesionB': $('#idSesion').val(),
                    'txtPestania' : $tabPane.attr('id'),
                    'txtTotal_Pestanias' : totalPestana,
                    'txtProgreso' : progreso.toFixed(2),
                    'txtOtros_datos' : null,
                    'txtIdlogro' : $('#idLogro').val()
                },
                tabPane: $tabPane
            })
        }//endif
    }

    $(document).ready(function(){
        $tabPane.find('.content-smartic.metod').addClass('active');
        $plantilla=$tabPane.find('.plantilla');
        plantillalook($plantilla);
        console.log('test');
       // $('.nopreview').hide(); 
    });
*/
</script>