<div class="mask-smartbook-portada-detail">
    <div class="row mask-smartbook-row">
        <div class="col-xs-2">
            <a href="<?= $this->rutabase."/ver".$this->parametros ?>" >
                <i class="icon icon-arrow-left"></i>
            </a>
            <div class="separator-detail-arrow"></div>
        </div>
        <div class="col-xs-10">
            <div class="csd-section2-top">
                <span class="miga txt-type-smartbook" style="background-color: orange">practice</span>
                <h3 class="txt_title_smartbook-detail">Smartbook</h3>
            </div>
            <div class="csd-section2-bottom">
                <p class="txt_subtitle_smartbook-detail text-ellipsis">
                    <?=@$this->sesion["nombre"];?>
                </p>
            </div>
        </div>
    </div>
</div>
<!--START INPUTS HIDDEN-->
<div id="smartbook-ver" class="editando row smart-hide" data-idautor="<?php echo @$voc['idpersonal']; ?>">
    <input type="hidden" id="rolUsuario" value="<?php echo @$this->idrol; ?>">
    <input type="hidden" id="idCurso" value="<?php echo @$this->idcurso; ?>">
    <input type="hidden" id="idSesion" value="<?php echo @$this->idactividad; ?>">
    <input type="hidden" id="idrecurso_cursodetalle" value="<?= @$this->idsesionCursodetalle ?>" />
    <input type="hidden" id="idgrupoauladetalle" value="<?= @$this->idgrupoauladetalle ?>" />
    <input type="hidden" id="idBitacoraAlumnoSmbook" value="<?php echo !empty($this->bitac_alum_smbook)? @$this->bitac_alum_smbook['idbitacora_smartbook']:'';?>">
    <input type="hidden" id="idLogro" value="<?php echo @$this->idLogro; ?>">
</div>

<input type="hidden" id="idcurso_sc" value="<?php echo $this->idcurso_sc; ?>">
<input type="hidden" id="idcc" value="<?php echo $this->idcc; ?>">
<input type="hidden" id="idcursodetalle_sc" value="<?php echo $this->idcursodetalle_sc; ?>">
<input type="hidden" id="PV" value="<?php echo $this->PV;?>">
<input type="hidden" id="PV_SE_URL" value="<?php echo $this->url_SE;?>">
<!--START INPUTS IDIOMAS-->
<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="cargando" value='<?php echo ucfirst(JrTexto::_('Loading')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
    <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
    <input type="hidden" id="ejerc_no_completado" value='<?php echo ucfirst(JrTexto::_('you have not finished editing this exercise')); ?>'>
    <input type="hidden" id="grabar_audio" value='<?php echo ucfirst(JrTexto::_('Record')); ?>'>
    <input type="hidden" id="detener_audio" value='<?php echo ucfirst(JrTexto::_('Stop')); ?>'>
    <input type="hidden" id="escribir_texto" value='<?php echo ucfirst(JrTexto::_('Write')); ?>'>
    <input type="hidden" id="reproducir_audio" value='<?php echo ucfirst(JrTexto::_('Play')); ?>'>
    <input type="hidden" id="end" value='<?php echo ucfirst(JrTexto::_('End')); ?>'>
    <input type="hidden" id="apply_format" value='<?php echo ucfirst(JrTexto::_('Write answer and Apply format')); ?>'>
    <input type="hidden" id="escuchar_audio" value='<?php echo ucfirst(JrTexto::_('click on the audio to play it')); ?>'>
</section>
<!--END INPUTS IDIOMAS-->
<!--END INPUTS HIDDEN-->
<!--START BARRA DE PROGRESO-->
<div id="contenedor-paneles">
    <div id="panel-barraprogreso" class="text-center eje-panelinfo" >
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                <span>0</span>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!--END BARRA DE PROGRESO-->
<div style="text-align: right; padding:5px;">
    <a href="#" class="btn btn-lilac btn-pronunciar btnpronunciacionshow" data-source="<?= $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion">
        <i class="fa fa-bullhorn"></i> <?= ucfirst(JrTexto::_("Pronunciation")) ?>
    </a>
</div>
<!--START PRONUNCIACION CONTENT-->
<div style="display: none;">
    <div id="ventanapronunciacion">
        <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Volume")); ?>:</label>
                <input type="range" id="voicevolume" min="0" max="1" value="0.7" step="0.1" />
            </div>
          </div>
          <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Speed")); ?>:</label>
                <input type="range" id="voiceSpeed" min="0.1" max="2" value="1" step="0.1" />
            </div>
          </div>
        <!-- <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Pitch")); ?>:</label>
                <input type="range" id="voicepitch" min="0" max="2" value="1" step="0.1" />
            </div>
          </div> -->
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Voice"))?> :</label>
              <div class="cajaselect">
                <select name="voicepronunciation" id="voicepronunciation" class="speachvoives form-control">                  
                </select>
              </div>
            </div>            
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Write text"))?> :</label>
              <textarea id="texto" placeholder="Write text here" style="resize:none;" class="form-control" rows="3"></textarea>              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button id="btn-leertexto" type="button" class="btn btn-default center-block"><?php echo JrTexto::_('Listen to the pronunciation'); ?><i class="fa fa-bullhorn"></i></button>
              <br>
              <button  type="button" class="btn btn-warning center-block cerrarmodal"><?php echo JrTexto::_('Exit');?> <i class="fa fa-close"></i> </button>
          </div>
        </div> 
      </div>
    </div>
</div>
<!--END PRONUNCIACION CONTENT-->

<!--START CONTENIDO-->
<div class="tab-pane active" id="div_practice" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->practice['bitacora'])?'data-idbitacora='.$this->practice['bitacora']['idbitacora']:'' ?>>
    <div class="content-smartic metod" id="met-<?php echo @$this->practice["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->practice["met"]["idmetodologia"]; ?>">
        <div class="col-xs-12">
            <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->practice["met"]["idmetodologia"]; ?>">
                <?php if(!empty(@$this->practice['act']['det'])){
                $aquiSeQuedo  = false;
                $flag  = false;
                $num=0;
                $encontro=false;
                $c = !empty($this->practice['act']['det'])?count($this->practice['act']['det']):1;
                foreach ($this->practice['act']['det'] as $i=>$ejerc) { $num++;
                    // if($this->rolActivo=='Alumno'){
                    if($encontro==false){
                        if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                            $aquiSeQuedo = $flag = true;
                            $encontro=true;
                        }elseif(@$ejerc['desarrollo']['estado']=='T' && $c== $i+1){
                            $aquiSeQuedo = true;
                            $encontro=true;
                        }else{
                            $aquiSeQuedo = false;
                        }
                    }else $aquiSeQuedo = false;
                    /*}else{
                        $aquiSeQuedo = ($num==1);
                    }*/
                    ?>
                    <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                <?php } } ?>
                <li class="hidden comodin"><!-- para que cuente correctamente la barra progreso --></li>
            </ul>

            <div class="col-xs-12 text-center numero_ejercicio" style="display: none">
                <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                <span class="actual"></span>
                <span class="total"></span>
            </div>
        </div>
        <div class="col-xs-12" style="padding:0px;">
            <div class="tab-content tabcontent-main-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ) ?>-main-content">
            <?php
            if(!empty(@$this->practice['act']['det'])){
                $aquiSeQuedo  = false;
                $flag  = false;
                $encontro=false;
                $c = !empty($this->practice['act']['det'])?count($this->practice['act']['det']):1;
                foreach (@$this->practice['act']['det'] as $i=>$ejerc) { 
                    $x=$i+1; 
                    $iddetalle=$ejerc["iddetalle"]; 
                    //if($this->rolActivo=='Alumno'){
                        if($encontro==false){
                        if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                            $aquiSeQuedo = $flag = true;
                            $encontro=true;
                        }elseif(@$ejerc['desarrollo']['estado']=='T' && $c== $i+1){
                            $aquiSeQuedo = true;
                            $encontro=true;
                        }else{
                            $aquiSeQuedo = false;
                        }
                    }else $aquiSeQuedo = false;
                    /* }else{
                        $aquiSeQuedo = ($x==1);
                    } */
                    ?>
                <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>"  data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>" >
                <?php $html = $ejerc["texto_edit"];

                if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){                              
                    $html  = $ejerc['desarrollo']['html_solucion'];
                }
                $cadena_resultante= preg_replace('/<link(.*) href="(.*)\/(.*).css"(.*)>/i', '<link$1 href="'.URL_BASE.'static/libs/smartenglish/css/$3.css"$4>', $html);
                $cadena_resultante=str_replace('__xRUTABASEx__static/',$this->url_SE."static/",$cadena_resultante);
                $cadena_resultante=str_replace('__xRUTABASEx__/static/',$this->url_SE."static/",$cadena_resultante);
                $cadena_resultante=str_replace('<video','<video  oncontextmenu="return false;" controlslist="nodownload" ',$cadena_resultante);
                echo $cadena_resultante;
                ?>
                </div>
                <?php } } ?>
            </div>
        </div>
        <div class="col-xs-3 hidden">
            <div class="widget-box">
                <div class="widget-header bg-red">
                    <h4 class="widget-title">
                        <i class="fa fa-paint-brush"></i>
                        <span><?php echo JrTexto::_('Skills'); ?></span>
                    </h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row habilidades">
                            <div class="col-xs-12 ">
                            <?php 
                            $ihab=0;
                            if(!empty($this->habilidades))
                            foreach ($this->habilidades as $hab) { $ihab++;?>
                                <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                            <?php } ?>                                  
                            </div>
                        </div> 
                    </div>
                </div>                   
            </div>
        </div>
    </div> 
</div>
<div class="row botonesNextBack">
<div class="col-md-12 infouserejericiopanel" style="background: #ECEDF0;">                   
        <div class="col-md-3 col-sm-3 col-xs-4 text-left" >
            <a class="btn btn-inline btn-success btn-lg btnejercicio-back_ " style="display: inline-block;  padding-top: 0.9em;  height: 3.1em;"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-4 text-center">
            <span class="infouserejericio ejeinfo-numero numero_ejercicio">
                <div class="titulo">Exercise</div>
                <span class="actual">1</span>
                <span class="total">16</span>
            </span>
            <span id="panel-intentos-dby"  class="infouserejericio ejeinfo-intento">
                <div class="titulo"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                <span class="actual">1</span>
                <span class="total"><?php echo $this->intentos ?></span>
            </span>
            <!--span id="panel-tiempo" class="infouserejericio ejeinfo-tiempo">
                <div class="titulo">< ?php //echo ucfirst(JrTexto::_('time')); ?></div>
                <span class="actual"></span>
                <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="< ?php //echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                <span class="info-show">00:00</span>
            </span-->                         
        </div>
        <div class="col-md-3 col-sm-3 col-xs-4 text-right" >
            <a class="btn btn-inline btn-success btn-lg btnejercicio-next_" style="display: inline-block; padding-top: 0.9em;  height: 3.1em;"><?php echo JrTexto::_('Next'); ?> <i class="fa fa-arrow-right"></i></a>
        </div>
        <!-- <div class="clearfix"></div> -->
    </div>
</div>
<!--SAVE-->
<div id="btns_progreso_intentos" style="display: none;">
    <div class="row" id="btns_control_alumno">
        <div class="col-sm-12 botones-control">
            <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
            <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>
<script type="text/javascript">
    const URL_RETORNO = "<?= $this->rutabase."/ver".$this->parametros ?>";
    var idTabPane = "#div_practice"
    var _IDHistorialSesion=0;
    var oIdHistorial = {'games': 0,'practice': 0} //variables para las id's de los hitoriales
    var _tabPane = null;
    var totalPestana = <?=!empty($this->totalPestana) ? $this->totalPestana : "4" ?>;
    var editableejercicio=false;

    //PASAR A UTLS
    var activarbtnnext=function(plantilla,acc,obj){  
        if(acc==false){//bloquear        
            if(obj.hasClass('btnejercicio-next_')){ 
                obj.addClass('disabled').append('<i class="fa fa-gear fa-spin"></i>');
                $('.save-progreso').append('<i class="fa fa-gear fa-spin"></i>');
            }else obj.addClass('disabled').prepend('<i class="fa fa-gear fa-spin"></i>');
        }else{ // desbloquear
            $('.btnejercicio-back_').removeClass("disabled").removeAttr('disabled').children('i.fa-spin').remove();
            $('.btnejercicio-next_').removeClass("disabled").removeAttr('disabled').children('i.fa-spin').remove();
            $('.save-progreso').removeClass("disabled").children('i.fa-spin').remove();
        }
    }
    //PASAR A UTLS
    var contarNumeroEjercicio = function($tabPane) {
        var $pnlNroEjercicio = $tabPane.find('.numero_ejercicio');
        if($pnlNroEjercicio.length===0){ return false; }
        var total_txt = $tabPane.find('ul.nav.ejercicios li').not('.comodin').length;
        var $actualTab = $tabPane.find('ul.nav.ejercicios li.active');
        var actual_txt = $actualTab.find('a[data-toggle="tab"]').text();
        if($actualTab.length==0){ actual_txt = MSJES_PHP.end; total_txt=''; }
        $pnlNroEjercicio.find('.actual').text(actual_txt);
        $pnlNroEjercicio.find('.total').text(total_txt);
        $('.infouserejericio.ejeinfo-numero .actual').text(actual_txt);
        $('.infouserejericio.ejeinfo-numero .total').text(total_txt);
    }
    window.addEventListener("load",main)
    /**
     * Metodo principal
     */
    function main(){
        try{
            const origin = String.prototype.concat(window.location.protocol,"//",window.location.hostname) 
            window.parent.postMessage({modulo:"smartbook",vista:"look"},origin)        
        }catch(err){
            console.error("Error en postMessage ",err)
        }
        cargarMensajesPHP();
        Utils.initBitac_Alum_Smbook({
            URLBASE:_sysUrlBase_,
            idcurso_sc: '<?php echo $this->idcurso_sc;?>',
            idcc: '<?php echo $this->idcc;?>',
            idcursodetalle_sc: '<?php echo $this->idcursodetalle_sc;?>',
            txtEstado: "P",
            idgrupoauladetalle: '<?php echo $this->idgrupoauladetalle;?>'
        });
        initEvents()
        _tabPane = $(idTabPane);

        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });

        $('.tab-pane .content-smartic.metod.active').removeClass('active');
        _tabPane.find('.content-smartic.metod').addClass('active');
        contarNumeroEjercicio(_tabPane);
        addBtnsControlAlumno(_tabPane,false);
        Utils.registrarHistorialSesion({
            URLBASE:_sysUrlBase_,
            idTabPane: idTabPane,
            Attention: "<?php echo JrTexto::_('Attention'); ?>",
            idcurso_sc: '<?php echo $this->idcurso_sc;?>',
            idcc: '<?php echo $this->idcc;?>',
            idgrupoauladetalle: '<?php echo $this->idgrupoauladetalle;?>'
        });
        $(idTabPane).find('ul.nav.ejercicios').children('li.active').children('a').trigger('shown.bs.tab');
    }
    function initEvents(){
        $('.btnejercicio-next_').on('click',function(ev){
            try{
                $this=$(this);
                $this.children('i.fa-spin').remove();
                if($this.hasClass('disabled')) return false;
                $this.addClass('disabled');
                var tabPaneActivo=$('#div_practice.active');

                if(tabPaneActivo.length == 0){ throw "#div_practice.active no encontrado" }

                var plantilla=tabPaneActivo.find('.tabhijo.active').find('.plantilla');
                activarbtnnext(plantilla,false,$this);
                guardarProgreso(); 
                try{
                    if(plantilla.length){
                        var pltaulumno=plantilla.find('.pnl-speach.alumno');
                        var btn=pltalumno.find('.btnGrabarAudio');
                        if(btn.length){
                            if(btn.children('i.fa-microphone').length){
                                if(!btn.hasClass('btn-danger')){
                                    if(btn.hasClass('hide')) btn.trigger('click');
                                }
                            }
                        }
                    }
                    var btn=plantilla.find('.');
                }catch(ex){
                    // console.error("Error en el proceso de plantilla, dentro del evento btnejercicio-next_: ",error)
                }
                var progreso = parseFloat(tabPaneActivo.find('.metod').attr('data-progreso')||0);
                
                Utils.guardarBitacora({
                    URLBASE:_sysUrlBase_,
                    dataSave:{
                        'Idbitacora' : tabPaneActivo.attr('data-idbitacora')||null,
                        'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val(),
                        'txtIdcurso' : $('#idCurso').val(),
                        'txtIdsesion' : '<?php echo $this->idcursoDetalle;?>',
                        'txtIdsesionB': $('#idSesion').val(),
                        'txtPestania' : tabPaneActivo.attr('id'),
                        'txtTotal_Pestanias' : totalPestana,
                        'txtProgreso' : progreso.toFixed(2),
                        'txtOtros_datos' : null,
                        'txtIdlogro' : $('#idLogro').val(),
                    },
                    tabPane: tabPaneActivo
                }); 
            
                var tabactivo=tabPaneActivo.find('.tabhijo.active');
                var tabejercicioactivo=tabPaneActivo.find('ul.ejercicios').children('li.active');
                var indexhijo=tabejercicioactivo.index();
                var totalhijos=tabPaneActivo.find('ul.ejercicios').find('li').length-2;
                
                idtab=tabPaneActivo.attr('id');

                if(indexhijo==totalhijos){ 
                    window.location.href = URL_RETORNO
                }else{
                    if(idtab=='div_autoevaluar') tabactivo.find('.plantilla').addClass('avanzoejercicio');
                    tabejercicioactivo.next('li').children('a').trigger('click');
                } 
                setTimeout(function(){ activarbtnnext('',true,false); },1000);
            }catch(error){
                console.error("Error en el evento btnejercicio-next_: ",error)
            }
        })
        $('.btnejercicio-back_').on('click',function(ev){
            try{
                $this=$(this);
                $this.children('i.fa-spin').remove();
                if($this.hasClass('disabled')) return false;        
                var tabPaneActivo=$('#div_practice.active');
                if(tabPaneActivo.length == 0){ throw "#div_practice.active no encontrado" }
                var plantilla=tabPaneActivo.find('.tabhijo.active').find('.plantilla');
                activarbtnnext(plantilla,false,$this);
                var tabejercicioactivo=tabPaneActivo.find('ul.ejercicios').children('li.active');
                var indexhijo=tabejercicioactivo.index();
                if(indexhijo==0){
                    window.location.href = URL_RETORNO
                }else{            
                    tabejercicioactivo.prev('li').children('a').trigger('click');
                }
                setTimeout(function(){ activarbtnnext('',true,false); },1000);
            }catch(error){
                console.error("Error en el evento btnejercicio-back_: ",error)
            }
        })
        //Evento para el boton continuar (continue)
        $('#div_practice').on('click', '#btns_control_alumno .save-progreso', function(e) {
            $this=$(this);
            $this.children('i.fa-spin').remove(); 
            if($this.hasClass('disabled')) return false;
            $this.addClass('disabled');
            $('.btnejercicio-next_').trigger('click');
        });
        $('body ul.nav.ejercicios').on('shown.bs.tab', 'a[data-toggle="tab"]', function(ev) {
            ev.preventDefault();
            var $idpane=$(this).closest('.tab-pane').closest('.tab-pane').attr('id');
            var idTab = $(this).attr('href');
            var $tabPane = $(idTab).closest('div[data-tabgroup="navTabsLeft"]');
            contarNumeroEjercicio($tabPane);
            var $tabhijoActive=$tabPane.find('.tabhijo.active');        
            var totalpuntaje=100/$tabPane.find('.tabhijo').length; // total puntaje por ejericio.     
            var plantilla=$tabhijoActive.find('.plantilla');  
            
            plantilla.find('#btns_control_alumno .save-progreso').show();
            console.log(plantilla);
            plantillaejercicios(plantilla,false);
            
            if(plantilla.hasClass("plantilla-completar")){
                plantilla.find(".ischoice").attr("tabindex","-1")
            }
            
            modResponsive();
            if($(window).width() <= 500){
                plantilla.find('img.img-responsive').css({'max-width':'100%'});
            }else
            if(plantilla.hasClass('plantilla-img_puntos')){
                plantilla.find('img.img-responsive').css({'max-width':'100%'});
            }else 
            $tabhijoActive.find('img.img-responsive').css({'width':'100%','max-width':'600px'});
            setTimeout(function(){ activarbtnnext('',true,false); },3000);
        })
        //Obtener el evento cuando no se selecciona el boton nav-tab con direccion especifica
        $('body .nav-tabs').on('shown.bs.tab', 'a[href!="#div_practice"]', function (e){  
            Utils.editarHistoriaSesion({URLBASE:_sysUrlBase_,Attention: "<?php echo JrTexto::_('Attention'); ?>",},oIdHistorial.practice,"#div_practice");
            //setTimeout(function(){ activarbtnnext('',true,false); },1000);
        });
        $(window).on('beforeunload', function(){        
            Utils.editarHistoriaSesion({URLBASE:_sysUrlBase_,Attention: "<?php echo JrTexto::_('Attention'); ?>"},_IDHistorialSesion);
            Utils.editarHistoriaSesion({URLBASE:_sysUrlBase_,Attention: "<?php echo JrTexto::_('Attention'); ?>"},oIdHistorial.practice,"#div_practice");
        });
        $('body').on('click', '.btnpronunciacionshow', function(e){
            e.preventDefault();
            var htmlmodal = $('#ventanapronunciacion').clone(true);
            var dt = {
                titulo:'pronunciation',  
                html: htmlmodal
            };
            var md = __sysmodal(dt);
            md.on('click','#btn-leertexto',function(ev){
                texto=md.find('#texto').val();
                pronunciar(texto);            
            }).on('change','#voicepronunciation',function(ev){
                var voice=$(this).val();
                __cambiarvoice(voice);
            });
        }); 
    }//endif
</script>