<div class="mask-smartbook-portada-detail">
    <div class="row mask-smartbook-row">
        <div class="col-xs-2">
            <a href="<?= $this->rutabase."/ver".$this->parametros ?>" >
                <i class="icon icon-arrow-left"></i>
            </a>
            <div class="separator-detail-arrow"></div>
        </div>
        <div class="col-xs-10">
            <div class="csd-section2-top">
                <span class="miga txt-type-smartbook" style="background-color: blue">Games</span>
                <h3 class="txt_title_smartbook-detail">Smartbook</h3>
            </div>
            <div class="csd-section2-bottom">
                <p class="txt_subtitle_smartbook-detail text-ellipsis">
                    <?=@$this->sesion["nombre"];?>
                </p>
            </div>
        </div>
    </div>
</div>
<!--START INPUTS HIDDEN-->
<div id="smartbook-ver" class="editando row smart-hide" data-idautor="<?php echo @$voc['idpersonal']; ?>">
    <input type="hidden" id="rolUsuario" value="<?php echo @$this->idrol; ?>">
    <input type="hidden" id="idCurso" value="<?php echo @$this->idcurso; ?>">
    <input type="hidden" id="idSesion" value="<?php echo @$this->idactividad; ?>">
    <input type="hidden" id="idrecurso_cursodetalle" value="<?= @$this->idsesionCursodetalle ?>" />
    <input type="hidden" id="idgrupoauladetalle" value="<?= @$this->idgrupoauladetalle ?>" />
    <input type="hidden" id="idBitacoraAlumnoSmbook" value="<?php echo !empty($this->bitac_alum_smbook)? @$this->bitac_alum_smbook['idbitacora_smartbook']:'';?>">
    <input type="hidden" id="idLogro" value="<?php echo @$this->idLogro; ?>">
</div>

<input type="hidden" id="idcurso_sc" value="<?php echo $this->idcurso_sc; ?>">
<input type="hidden" id="idcc" value="<?php echo $this->idcc; ?>">
<input type="hidden" id="idcursodetalle_sc" value="<?php echo $this->idcursodetalle_sc; ?>">
<input type="hidden" id="PV" value="<?php echo $this->PV;?>">
<input type="hidden" id="PV_SE_URL" value="<?php echo $this->url_SE;?>">
<!--START INPUTS IDIOMAS-->
<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="cargando" value='<?php echo ucfirst(JrTexto::_('Loading')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
    <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
    <input type="hidden" id="ejerc_no_completado" value='<?php echo ucfirst(JrTexto::_('you have not finished editing this exercise')); ?>'>
    <input type="hidden" id="grabar_audio" value='<?php echo ucfirst(JrTexto::_('Record')); ?>'>
    <input type="hidden" id="detener_audio" value='<?php echo ucfirst(JrTexto::_('Stop')); ?>'>
    <input type="hidden" id="escribir_texto" value='<?php echo ucfirst(JrTexto::_('Write')); ?>'>
    <input type="hidden" id="reproducir_audio" value='<?php echo ucfirst(JrTexto::_('Play')); ?>'>
    <input type="hidden" id="end" value='<?php echo ucfirst(JrTexto::_('End')); ?>'>
    <input type="hidden" id="apply_format" value='<?php echo ucfirst(JrTexto::_('Write answer and Apply format')); ?>'>
    <input type="hidden" id="escuchar_audio" value='<?php echo ucfirst(JrTexto::_('click on the audio to play it')); ?>'>
</section>
<!--END INPUTS IDIOMAS-->
<!--END INPUTS HIDDEN-->
<div style="text-align: right; padding:5px;">
    <a href="#" class="btn btn-lilac btn-pronunciar btnpronunciacionshow" data-source="<?= $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion">
        <i class="fa fa-bullhorn"></i> <?= ucfirst(JrTexto::_("Pronunciation")) ?>
    </a>
</div>
<!--START PRONUNCIACION CONTENT-->
<div style="display: none;">
    <div id="ventanapronunciacion">
        <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Volume")); ?>:</label>
                <input type="range" id="voicevolume" min="0" max="1" value="0.7" step="0.1" />
            </div>
          </div>
          <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Speed")); ?>:</label>
                <input type="range" id="voiceSpeed" min="0.1" max="2" value="1" step="0.1" />
            </div>
          </div>
        <!-- <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Pitch")); ?>:</label>
                <input type="range" id="voicepitch" min="0" max="2" value="1" step="0.1" />
            </div>
          </div> -->
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Voice"))?> :</label>
              <div class="cajaselect">
                <select name="voicepronunciation" id="voicepronunciation" class="speachvoives form-control">                  
                </select>
              </div>
            </div>            
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Write text"))?> :</label>
              <textarea id="texto" placeholder="Write text here" style="resize:none;" class="form-control" rows="3"></textarea>              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button id="btn-leertexto" type="button" class="btn btn-default center-block"><?php echo JrTexto::_('Listen to the pronunciation'); ?><i class="fa fa-bullhorn"></i></button>
              <br>
              <button  type="button" class="btn btn-warning center-block cerrarmodal"><?php echo JrTexto::_('Exit');?> <i class="fa fa-close"></i> </button>
          </div>
        </div> 
      </div>
    </div>
</div>
<!--END PRONUNCIACION CONTENT-->
<!--START TAB GAMES CONTENT-->
<div class="tab-pane" id="div_games" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->games['bitacora'])?'data-idbitacora='.$this->games['bitacora']['idbitacora']:'' ?>>
    <div class="col-xs-12">
        <ul class="nav nav-tabs list-games items-select-list">
            <?php
            if(!empty($this->games)){
                foreach ($this->games as $i=>$g) {
                    echo '<li'.(($i==0)?' class="active"':'').'><a href="#game_'.$g['idtool'].'" data-idgame="'.$g['idtool'].'" data-title="'.$g['titulo'].'" class="item-select" data-toggle="tab">'.($i+1).'</a></li>';
                    $i++;
                }
            } ?>
            <li style="float:right;"><button type="button" class="btn btn-success continue-game"><?php echo ucfirst(JrTexto::_('next')) ?></button></li>
        </ul>
    </div>
    <div class="tab-content col-xs-12">
        <div class="mascara_visor">
            <!--Dynamic content-->
        </div>
    </div>
    <div class="col-xs-12" id="btns_control_alumnogame">
        <div class="col-sm-12 botones-control" style="margin: 2.3em auto;">
            <a class="btn btn-inline btn-success btn-lg continue-game" style="display: inline;"><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>
<!--END TAB GAMES CONTENT-->

<script>
    const URL_RETORNO = "<?= $this->rutabase."/ver".$this->parametros ?>";
    var idTabPane = "#div_games"
    var _IDHistorialSesion=0;
    var oIdHistorial = {'games': 0,'practice': 0} //variables para las id's de los hitoriales
    var _tabPane = null;
    var totalPestana = <?=!empty($this->totalPestana) ? $this->totalPestana : "4" ?>;
    var optSlike={
        infinite: false,
        navigation: false,
        slidesToScroll: 1,
        centerPadding: '10px',
        slidesToShow: 7,
        responsive:[
        { breakpoint: 1200, settings: {slidesToShow: 6} },
        { breakpoint: 992, settings: {slidesToShow: 5 } },
        { breakpoint: 880, settings: {slidesToShow: 4 } },
        { breakpoint: 720, settings: {slidesToShow: 3 } },
        { breakpoint: 320, settings: {slidesToShow: 2} }
        ]
    };

    var pos=-1;
    var posv=-1;
    var showdiv=[];
    window.addEventListener("load",main)
    /**
     * Metodo principal
     */
    function main(){
        try{
            const origin = String.prototype.concat(window.location.protocol,"//",window.location.hostname) 
            window.parent.postMessage({modulo:"smartbook",vista:"games"},origin)        
        }catch(err){
            console.error("Error en postMessage ",err)
        }
        cargarMensajesPHP();
        modResponsive();
        Utils.initBitac_Alum_Smbook({
            URLBASE:_sysUrlBase_,
            idcurso_sc: '<?php echo $this->idcurso_sc;?>',
            idcc: '<?php echo $this->idcc;?>',
            idcursodetalle_sc: '<?php echo $this->idcursodetalle_sc;?>',
            txtEstado: "P",
            idgrupoauladetalle: '<?php echo $this->idgrupoauladetalle;?>'
        });
        initEvents()
        _tabPane = $(idTabPane);
        
        var $slider = _tabPane.find('.myslider');
        
        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });
        
        let progreso = 100.00;
        let idbitacora=_tabPane.attr('data-idbitacora')||null;

        if($slider.length>0 && !$slider.hasClass('slick-initialized')){
            $slider.slick(optSlike);
        }

        if(idbitacora==null){
            Utils.guardarBitacora({
                URLBASE:_sysUrlBase_,
                dataSave: {
                    'Idbitacora' : idbitacora,
                    'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val()||null,
                    'txtIdcurso' : $('#idCurso').val(),
                    'txtIdsesion' : '<?php echo $this->idcursoDetalle;?>',
                    'txtIdsesionB': $('#idSesion').val(),
                    'txtPestania' : _tabPane.attr('id'),
                    'txtTotal_Pestanias' : totalPestana,
                    'txtProgreso' : progreso.toFixed(2),
                    'txtOtros_datos' : null,
                    'txtIdlogro' : $('#idLogro').val()
                },
                tabPane: _tabPane
            })
        }
        showPrimerElemento(_tabPane);
        
        Utils.registrarHistorialSesion({
            URLBASE:_sysUrlBase_,
            idTabPane: idTabPane,
            Attention: "<?php echo JrTexto::_('Attention'); ?>",
            idcurso_sc: '<?php echo $this->idcurso_sc;?>',
            idcc: '<?php echo $this->idcc;?>',
            idgrupoauladetalle: '<?php echo $this->idgrupoauladetalle;?>'
        });
    }
    function initEvents(){
        $('#div_games').on('click', '.nav-tabs a[data-toggle="tab"]', function(e) {
            e.preventDefault();
            const Url_Base = Utils.rtrim(_sysUrlBase_,"/")
            var idGame = $(this).data('idgame');
            var titulo = $(this).data('title');
            var $container = $(this).closest('.tab-pane').find('.mascara_visor');
            if($(window).width() <= 425){
                $container.html('<div style="padding:2em;"><h1>'+titulo+'</h1><div><a href="'+Url_Base+'/smartbook/game/ver/?idgame='+idGame+'&PV=<?php echo $this->PV ?>" target="_blank" class="btn btn-primary"><?php echo JrTexto::_('Play') ?></a></div></div>');
            }else{            
                $container.html('<iframe src="'+Url_Base+'/smartbook/game/ver/?idgame='+idGame+'&PV=<?php echo $this->PV ?>" frameborder="0"></iframe>');
            }
        });
        //Obtener el evento cuando no se selecciona el boton nav-tab con direccion especifica
        $('body .nav-tabs').on('shown.bs.tab', 'a[href!="#div_games"]', function (e){
            Utils.editarHistoriaSesion({URLBASE:_sysUrlBase_,Attention: "<?php echo JrTexto::_('Attention'); ?>",},oIdHistorial.games,"#div_games");
        });
        /**
         * Boton de siguiente en la seccion de juegos....
         */
        $('#div_games').on('click','.continue-game',function(){
            var listaSmartbook = $('#navTabsLeft');
            var listaSmartbookLi = listaSmartbook.find("li");
            var listaSmartbookLiActive = listaSmartbook.find("li.active");
            var listaSmartbookLiCount = listaSmartbookLi.length;
            var listaJuegos = $('.list-games');
            var listaJuegosLi = listaJuegos.find("li");
            var listaJuegosLiCount = listaJuegosLi.length - 1;
            var indice = listaJuegos.find("li.active").index() + 1;
            
            if((indice % listaJuegosLiCount) == 0){
                // var indiceSmartbook =  listaSmartbookLiActive.index() + 1;
                // if((indiceSmartbook  % listaSmartbookLiCount)){
                //     listaSmartbookLi.eq(indiceSmartbook).find("a").click();
                // }
                window.location.href = URL_RETORNO
            }else{
                listaJuegosLi.eq(indice).find("a").click();
            }
        });
        $(window).on('beforeunload', function(){        
            Utils.editarHistoriaSesion({URLBASE:_sysUrlBase_,Attention: "<?php echo JrTexto::_('Attention'); ?>"},_IDHistorialSesion);
            Utils.editarHistoriaSesion({URLBASE:_sysUrlBase_,Attention: "<?php echo JrTexto::_('Attention'); ?>"},oIdHistorial.games,"#div_games");
        });
        $('body').on('click', '.btnpronunciacionshow', function(e){
            e.preventDefault();
            var htmlmodal = $('#ventanapronunciacion').clone(true);
            var dt = {
                titulo:'pronunciation',  
                html: htmlmodal
            };
            var md = __sysmodal(dt);
            md.on('click','#btn-leertexto',function(ev){
                texto=md.find('#texto').val();
                pronunciar(texto);            
            }).on('change','#voicepronunciation',function(ev){
                var voice=$(this).val();
                __cambiarvoice(voice);
            });
        }); 
    }
    function showPrimerElemento($contenedor) {
        if($contenedor.length==0){ return false; }
        $contenedor.find('.items-select-list .item-select').first().trigger('click');
        $contenedor.find('.items-select-list').removeClass('items-select-list');
    }
</script>