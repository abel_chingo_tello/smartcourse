<?php
$game = $titulo = $descripcion = '';
if (!empty($this->datos)) {
    $rutabase = $this->documento->getUrlBase();
    $game = @str_replace('__xRUTABASEx__/', $this->url_SE, $this->datos[0]["texto"]);
    $titulo = @$this->datos[0]["titulo"];
    $descripcion = @$this->datos[0]["descripcion"];
}
if (empty($game)) { ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong><i class="fa fa-close"></i> <?php echo JrTexto::_('Error'); ?> </strong>
        <?php echo JrTexto::_('Game no exist'); ?>
    </div>
<?php } ?>
<style type="text/css">
    .partesrompecabeza {
        position: absolute;
        bottom: 0px;
        /* right: 0px;*/
    }
</style>
<!-- mine -->
<style>
    .plantilla-rompecabezas img.img-responsive.valvideo {
        height: calc(100vh - 150px) !important;
        width: 100% !important;
        max-width: unset !important
    }

    .plantilla-rompecabezas div.text-center.col-xs-6,
    .plantilla-rompecabezas div.container {
        width: 100% !important;
    }

    .plantilla-rompecabezas .partesrompecabeza {
        height: 100% !important;
        width: calc(100% - 30px) !important;
        text-align: center;
        float: left;
        padding-right: 15px;
        padding-left: 15px;
    }
</style>
<!-- mine -->
<input type="hidden" name="hRol" id="hRol" value="Alumno">
<input type="hidden" name="idNivel" id="idNivel" value="<?php echo $this->idnivel; ?>">
<input type="hidden" name="idUnidad" id="idUnidad" value="<?php echo $this->idunidad; ?>">
<input type="hidden" name="idActividad" id="idActividad" value="<?php echo $this->idactividad; ?>">
<input type="hidden" name="hIdGame" id="hIdGame" value="<?php echo @$this->datos[0]['idtool']; ?>">
<input type="hidden" name="hRolUsuarioAct" id="hRolUsuarioAct" value="<?php echo @$this->rolUsuarioAct; ?>">
<input type="hidden" name="hIdTareaArchivo" id="hIdTareaArchivo" value="<?php echo @$this->idTareaArchivo; ?>">
<input type="hidden" name="hIdArchivoRespuesta" id="hIdArchivoRespuesta" value="<?php echo (@$this->tarea_archivo['tablapadre'] == 'R') ? @$this->tarea_archivo['idtarea_archivos'] : ''; ?>">
<input type="hidden" name="hIdTareaRespuesta" id="hIdTareaRespuesta" value="<?php echo @$this->tarea_respuesta['idtarea_respuesta']; ?>">

<style type="text/css">
    #games-tool .addtext {
        margin: 0;
    }
</style>
<div class="container-fluid" id="games-tool">
    <div class="row games-container">
        <div class="col-xs-12 games-titulo-descripcion" style="display: none;">
            <div class="col-md-12 titulo text-center">
                <h3><?php echo $titulo; ?></h3>
            </div>
            <div class="col-md-12 descripcion text-center">
                <?php echo $descripcion; ?>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 games-main">
            <?php if ((empty(@$this->tarea_archivo) && !empty($game)) ||(@$this->tarea_archivo['tablapadre'] == 'T' || @$this->tarea_archivo['tablapadre'] == null && !empty($game)) ) {
                $cadena_resultante= preg_replace('/<link(.*) href="(.*)\/(.*).css"(.*)>/i', '<link$1 href="'.URL_BASE.'static/libs/smartenglish/css/$3.css"$4>', $game);
                $cadena_resultante=str_replace('__xRUTABASEx__static/',$this->url_SE."static/",$cadena_resultante);
                $cadena_resultante=str_replace('__xRUTABASEx__/static/',$this->url_SE."static/",$cadena_resultante);
                $cadena_resultante= preg_replace('/\$\(\'\[toggle\-tooltip\=\"true\"\]\'\)\.tooltip\(\)\;/i', '', $cadena_resultante);
                $cadena_resultante= preg_replace('/\$\(\"\*\[data-tooltip=\\\"tooltip\\\"\]\"\)\.tooltip\(\)\;/i', '', $cadena_resultante);
                
                echo $cadena_resultante ;
            } elseif (@$this->tarea_archivo['tablapadre'] == 'R') {
                $texto = json_decode(@$this->tarea_archivo['texto'], true);
                $cadena_resultante= preg_replace('/<link(.*) href="(.*)\/(.*).css"(.*)>/i', '<link$1 href="'.URL_BASE.'static/libs/smartenglish/css/$3.css"$4>', $texto[0]['html']);
                $cadena_resultante=str_replace('__xRUTABASEx__static/',$this->url_SE."static/",$cadena_resultante);
                $cadena_resultante=str_replace('__xRUTABASEx__/static/',$this->url_SE."static/",$cadena_resultante);
                $cadena_resultante= preg_replace('/\$\(\'\[toggle\-tooltip\=\"true\"\]\'\)\.tooltip\(\)\;/i', '', $cadena_resultante);
                $cadena_resultante= preg_replace('/\$\(\"\*\[toggle\-tooltip\=\"true\"\]\'\)\.tooltip\(\)\;/i', '', $cadena_resultante);
                $cadena_resultante= preg_replace('/\$\(\"\*\[data-tooltip=\\\"tooltip\\\"\]\"\)\.tooltip\(\)\;/i', '', $cadena_resultante);
                
                echo $cadena_resultante ;
            } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var msjes = {
        'confirm_action': '<?php echo JrTexto::_('Confirm action'); ?>',
        'are_you_sure': '<?php echo JrTexto::_('are you sure you want to delete this') . ' ' . JrTexto::_('game'); ?>?',
        'accept': '<?php echo JrTexto::_('Accept'); ?>',
        'cancel': '<?php echo JrTexto::_('Cancel'); ?>',
        'guardar': '<?php echo JrTexto::_('Save'); ?>',
    };
    var fnPreviewGame = function() {
        $('.discard-game').hide();
        $('.save-crossword').hide();
        $('.plantilla-crucigrama .nopreview').hide();
        $('.plantilla-sopaletras .nopreview').hide();
        $('.plantilla-rompecabezas .nopreview').hide();
        $('.plantilla-recursoexterno .nopreview').hide();
        $('.plantilla-recursoexterno .save-ganecontentdinamico').hide();
        $('.plantilla-recursoexterno').css('height', '95vh');
        $('.btn.preview-game').hide();
        $('.btn.backedit-game').show();
        $('.titulo-game').closest('.input-group').hide().closest('.titulo').find('p').html($('.titulo-game').val()).show();
        $('.descripcion-game').closest('.input-group').hide().siblings('p').html($('.descripcion-game').val()).show();
    };

    function imgLoaded(imgElement) {
        return imgElement.complete && imgElement.naturalHeight !== 0;
    }

    function estilosopa() {
        if ($(window).width() <= 768) {
            $('#findword-game #puzzle').css('padding', '0px');
            $('#findword-game #puzzle .puzzleSquare').css('width', '7%');
            $('#findword-game #words ul').css({
                textAlign: 'center',
                padding: '0px'
            });
            $('#findword-game #words li').css('width', '33%');
        }
    }

    function estilorompecabeza() {
        $('.plantilla-rompecabezas').css('min-width', '400px');
    }

    function checkTipoJuego() {
        $plantilla = $('body').find('.plantilla');
        if ($plantilla.hasClass('plantilla-sopaletras')) {
            console.log($plantilla);
        } else if ($plantilla.hasClass('plantilla-rompecabezas')) {
            estilorompecabeza();
        } else if ($plantilla.hasClass('plantilla-crucigrama')) {
            jugarcrusigrama($plantilla);
        }
    }
    $(document).ready(function() {
        $('.games-titulo-descripcion').show();
        //if( $('#hRol').val()=='Alumno' ){ 
        fnPreviewGame();
        //}
        $('.games-main').iniciarVisor({
            fnAlIniciar: function() {
                var $tmpl = $('.games-main').find('.plantilla');
                if ($tmpl.hasClass('plantilla-sopaletras')) {
                    iniciarSopaLetras($tmpl);
                } else if ($tmpl.hasClass('plantilla-crucigrama')) {
                    iniciarCrucigrama($tmpl);
                } else if ($tmpl.hasClass('plantilla-rompecabezas')) {
                    //$('body').children('div').removeClass('container');
                    //$tmpl.find('span.snappuzzle-wrap').css()
                    $tmpl.find('#pile').parent().removeClass('col-xs-6').addClass('partesrompecabeza');
                    var $img = $tmpl.find('#puzzle-containment img.valvideo');
                    var _img_ = document.getElementsByClassName("valvideo")[0];
                    if (imgLoaded(_img_)) {
                        iniciarRompecabezas($tmpl);
                    } else {
                        $img.load(function() {
                            iniciarRompecabezas($tmpl);
                        });
                    }
                    $tmpl.find('')
                }
            },
            texto: msjes,
            btnGuardar: ($('#hRol').val() == 'Alumno') ? '.btn_guardar_juego' : false,
        });
        setTimeout(() => {
            checkTipoJuego();
        }, 500);
        $(window).resize(function(){
            console.log('redimensionando');
            checkTipoJuego();
        });
        mostrarGame(true);
    });
</script>