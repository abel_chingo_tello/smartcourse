<style>
    html,
    body {
        height: 100%;
        width: 100%;
        background-color: dodgerblue;
        display: flex;
        justify-content: center;
        align-items: center;
        font: 200 70px/1.2em "Segoe UI Light", "Segoe UI", Arial, Helvetica,
            Sans-Serif !important;
    }

    p {
        text-align: center;
        color: white;
    }

    * {
        box-sizing: border-box;
        margin: 0;
    }

    body>div>a {
        display: none !important;
    }

    @media(max-width: 400px) {

        html,
        body {
            font: 200 30px/1.2em "Segoe UI Light", "Segoe UI", Arial, Helvetica,
                Sans-Serif !important;
        }
    }
</style>

<main>
    <p>En desarrollo...</p>
</main>
