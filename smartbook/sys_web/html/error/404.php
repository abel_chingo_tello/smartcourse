<?php
defined('RUTA_BASE') or die();
?>
<div class="container">
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron">
                <h1><strong>Opss!</strong></h1>                
                <div class="disculpa"><?php echo JrTexto::_('Sorry, we could not find the requested address')?>.</div>
                <div class=""><br>
                    <a class="btn btn-default close cerrarmodal" href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Go back')?></a>
                </div>
            </div>
        </div>
	</div>
</div>