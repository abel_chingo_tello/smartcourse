<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-11-2020 
 * @copyright	Copyright (C) 10-11-2020. Todos los derechos reservados.
 */
?>
 <style type="text/css">
 	.sombra-bootom{
 		-webkit-box-shadow: 0px 13px 9px -17px rgba(0,0,0,0.75);
		-moz-box-shadow: 0px 13px 9px -17px rgba(0,0,0,0.75);
		box-shadow: 0px 13px 9px -17px rgba(0,0,0,0.75);
 	}
 	 .thumbnail{
        /*height: auto !important;*/
        height: 200px !important;
        /* padding: 1em; */
        padding: 0 !important;
    }

    .img-curso{
        width: 100% !important;
        display: block !important;
        text-align: center;
        cursor: pointer !important;
        text-align: center;
        margin: auto;
        height: 150px;
    }
   
    .mask{
        height: 100% !important;
    }
    .tools-bottom{
        background-color: #464646 !important;
        margin: 0 !important;
    }
    .categoria_ {
        font-weight: bold;
        margin: 0px;
        background: #a1e5f0;
        padding: 1ex 1ex 1ex 2ex;
        font-size: x-large;
        font-family: initial;
    }
    .subcategoria_{
        transform: rotate(-90deg);
        -webkit-transform: rotate(-90deg);
        text-align: center;
        vertical-align: middle !important;
        font-size: 20px;
        font-weight: bold;
        padding: 0 !important;
        max-width: 120px;
    }
    .subcategoria2_{     
        font-size: 20px;
        font-weight: bold;    
        padding-left: 2.5em !important;
        display: none;
    }

    .curso-lock{
        opacity: 1 !important;
    }
    .curso-lock > i{
        color: white;
        font-size: 45px;
        padding-top: 30%;
    }   
    .titulo-p{
        float: none !important;
        font-size: 24px !important;
        font-weight: bold !important;
        text-align: center !important;
        width: 100% !important;
    }
    .tools a { cursor: pointer; } 
    ._nombrecurso{
    	font-size: 14px;
    	margin-top: 0px;
    	text-transform: capitalize;
    	padding: 1ex;
    }
    .view-first{
    	padding: 1ex;
    	margin-bottom: 1ex;
    }

 </style>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo @$this->user["idrol"]; ?>" idempresa="<?php echo $this->user["idempresa"]; ?>" >
  	<div class="paris-navbar-container"></div>
  	<script>
      addItemNavBar({
        class:'breadcrumbactive',
        text: '<?php echo JrTexto::_("Smartbooks"); ?>'
      });     
    </script>
    <div class="row">	
		<div class="col-md-12">
			<div class="row sombra-bootom" style="">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Plataforma</label>
						<select name="plataforma" id="plataforma" class="form-control select2">
							<option value="PVADU_1">Smart English Adultos</option>
							<option value="PVADO_1">Smart English Adolescentes</option>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Curso SmatEnglish</label>
						<select name="idcurso" id="idcurso" class="form-control select2">							
							<?php if(!empty($this->listado_cursos))
							foreach ($this->listado_cursos as $key => $cur){?>								
								<option value="<?php echo $cur["idcurso"];?>"><?php echo $cur["nombre"];?></option>					
							<?php }?>
							<option value="all">Todos</option>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Texto a Buscar.</label>
						<input type="text" name="texto" id="texto" class="form-control">
					</div>
				</div>	
				<div class="col-md-3 col-sm-6 col-xs-12 text-center">
					<div class="form-group"><label></label><br>
						<button  class="btn btn-primary btnbuscar"> Buscar <i class="fa fa-search"></i></button>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<div class="row">	
		<div class="col-md-12">
			<div class="vistatabla" nombre="<?php echo JrTexto::_("Smartbooks"); ?>">       
				<div class="row-fluid">
				  <div class="span12">
				    <div class="grid simple">
				      <div class="grid-body" id="listadosmartbooks" ></div>
				    </div>    
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#<?php echo $ventana;?>').closest('.container').removeClass('container').addClass('container-fluid');
	class venSmartbook{
	ventana=null;
	ventanaid=null;	
	frmid='frmmenu';
	tb='menu';
	$fun=null;
	bustexto='';
	idmenu=null;
	URL_SE=null;
	loginingles=false;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idmenu=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})						;
		}
		this.ventana.on('change','select#plataforma',function(ev){			
			ev.preventDefault();
			var data=new FormData();
			data.append('PV_INGLES',$(this).val()||'');
			let selectcursos=_this.ventana.find('select#idcurso');
			selectcursos.children('option').las().Sibligns().remove();
			_this.loginingles=false;
			_this.$fun.postData({url:_sysUrlBase_+'smartbook/ingles/cursos',data:data}).then(rs => {
				forEach(rs,function(v,i){
					selectcursos.append('<option value="'+v.idcurso+'">'+v.nombre+'</option>');
				})
				selectcursos.trigger('change');
			})
		}).on('change','select#idcurso',function(ev){
			ev.preventDefault();
			let idcurso=_this.ventana.find('select#idcurso').val()||'';
			if(idcurso!='') _this.versesiones();
			else{
				_this.ventana.find('#listadosmartbooks').hrml('');
			}
		}).on('click','.btnbuscar',function(ev){
			ev.preventDefault();
			_this.versesiones();
		}).on('click','.smartbook_ver',function(ev){
			ev.preventDefault();
			_this.smartbook_ver($(this));
		}).on('click','.smartbook_copy',function(ev){
			ev.preventDefault();
			_this.smartbook_copiar($(this));
		}).on('click','.smartbook_edit',function(ev){
			ev.preventDefault();
			_this.smartbook_editar($(this))
		}).on('click','.smartbook_eliminar',function(ev){
			ev.preventDefault();
			_this.smartbook_eliminar($(this));
		})
		this.ini_librerias();
	}
	ini_librerias(){
		$('select.select2').select2();
		$('select.select2icon').select2({
			templateSelection: function (item) {
				var $span = $('<span><i class="fa '+ item.id + '"/></i> ' + item.text + "</span>");
		  	    return $span;},
		  	templateResult:function(item){
		    	var $span = $('<span><i class="fa '+ item.id + '"/></i> ' + item.text + "</span>");
		  	    return $span;}
		 });
	}

	versesiones(){
		let _this=this;
		let PV=_this.ventana.find('select#plataforma').val()||'';
		if(PV=='PVADU_1') _this.URL_SE='<?php echo URL_SEAdultos ?>';
		else _this.URL_SE='<?php echo  URL_SEAdolecentes?>';
		let idcurso=_this.ventana.find('select#idcurso').val()||'';		
		var data=new FormData();
		data.append('PV_INGLES',PV);
		data.append('idcurso',idcurso);	
		data.append('texto',_this.ventana.find('input#texto').val()||'');	
		let listadosmartbooks=_this.ventana.find('#listadosmartbooks');			
		_this.$fun.postData({url:_sysUrlBase_+'smartbook/ingles/sesiones',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				html+=`<div class="col-md-2 col-sm-4 col-xs-6 text-center" style="margin-bottom:2ex;">                        
                            <div class="image view view-first item-curso sombra-bootom" idcursodetalle="${v.idcursodetalle}" idrecurso="${v.idrecurso}">
                                <div class="col-md-12"><img class="img-curso img-responsive img-fluid" src="${_this.URL_SE+v.imagen}" alt="${v.nombre}" /></div>
                                <div class="col-md-12"> 
                                	<div class="_nombrecurso"  id="nombrecurso">${v.nombre}</div>
                            	</div>
                            	<div class="mask">
                                    <div class="tools tools-bottom"> 
                                    	<a class="a-mask smartbook_ver" data-toggle="popover" title="Ver Smartbook"><i class="fa fa-eye"></i></a>                                      
                                        <a class="a-mask smartbook_copy" data-toggle="popover" title="Copiar Smartbook"><i class="fa fa-copy"></i></a>
                                        <a class="a-mask smartbook_edit" data-toggle="popover" title="Editar smartbook" style="display:none;"><i class="fa fa-pencil"></i></a>
                                        <a class="a-mask smartbook_eliminar" data-toggle="popover" title="Eliminar smartbook" style="display:none;"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                        	</div>
                    	</div>`;			
			})
			listadosmartbooks.html(html);
			if(_this.loginingles==false)_this.iniciar_sesion();
		})		
	}

	smartbook_ver(obj){
		let _this=this;
		var item=obj.closest('.item-curso'); 
		var nombre=''+item.find('#nombrecurso').text();
		let PV=_this.ventana.find('select#plataforma').val()||'';
		let idcurso=_this.ventana.find('select#idcurso').val()||'';	
		let idcursodetalle=item.attr('idcursodetalle')||'';	
		let idrecurso=item.attr('idrecurso')||'';
		window.location.href=_sysUrlBase_+'smartbook/ingles/solover/?PV_INGLES='+PV+'&idcursodetalle='+idcursodetalle+'&idrecurso='+idrecurso;
	}
	smartbook_copiar(obj){
		let _this=this;
		var item=obj.closest('.item-curso'); 
		var nombre=''+item.find('#nombrecurso').text();
		Swal.fire({
		  title: 'Nombre de copia de smartbook',
		  input: 'text',
		  inputValue:nombre,		  
		  showCancelButton: true,
		  confirmButtonText: 'Copiar',
		  showLoaderOnConfirm: true,		  
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
			let dismiss=result.dismiss||'';
			console.log(result);
			if(dismiss=='' || dismiss==undefined){
				let PV=_this.ventana.find('select#plataforma').val()||'';
				let idcurso=_this.ventana.find('select#idcurso').val()||'';	
				let idcursodetalle=item.attr('idcursodetalle')||'';	
				let idrecurso=item.attr('idrecurso')||'';

				var data=new FormData();
				data.append('PV_INGLES',PV);
				data.append('idcurso',idcurso);
				data.append('idcursodetalle',idcursodetalle);
				data.append('idrecurso',idrecurso);
				data.append('nombrecurso',result.value);
				let listadosmartbooks=_this.ventana.find('#listadosmartbooks');
				_this.$fun.postData({url:_sysUrlBase_+'smartbook/ingles/copiar',data:data}).then(rs => {
					//let html='';
					/*forEach(rs,function(v,i){
						html+=`<div class="col-md-2 col-sm-4 col-xs-6 text-center" style="margin-bottom:2ex;">                        
		                            <div class="image view view-first item-curso sombra-bootom" idcursodetalle="${v.idcursodetalle}" idrecurso="${v.idrecurso}">
		                                <div class="col-md-12"><img class="img-curso img-responsive img-fluid" src="${_this.URL_SE+v.imagen}" alt="${v.nombre}" /></div>
		                                <div class="col-md-12"> 
		                                	<div class="_nombrecurso"  id="nombrecurso">${v.nombre}</div>
		                            	</div>
		                            	<div class="mask">
		                                    <div class="tools tools-bottom"> 
		                                    	<a class="a-mask smartbook_ver" data-toggle="popover" title="Ver Smartbook"><i class="fa fa-eye"></i></a>                                      
		                                        <a class="a-mask smartbook_copy" data-toggle="popover" title="Copiar Smartbook"><i class="fa fa-copy"></i></a>
		                                        <a class="a-mask smartbook_edit" data-toggle="popover" title="Editar smartbook" style="display:none"><i class="fa fa-pencil"></i></a>
		                                    </div>
		                                </div>
		                        	</div>
		                    	</div>`;			
					})*/
					_this.versesiones();
					//listadosmartbooks.html(html);
				})

			}			
		})
	}
	smartbook_editar(obj){
		let _this=this;
		var item=obj.closest('.item-curso'); 
		let PV=_this.ventana.find('select#plataforma').val()||'';
		var nombre=''+item.find('#nombrecurso').text();
		if(PV=='PVADU_1') _this.URL_SE='<?php echo URL_SEAdultos ?>';
		else _this.URL_SE='<?php echo  URL_SEAdolecentes?>web/';


		let idcurso=_this.ventana.find('select#idcurso').val()||'';	
		let idcursodetalle=item.attr('idcursodetalle')||'';	
		let idrecurso=item.attr('idrecurso')||'';
		let a=document.createElement("a");
		a.href=_this.URL_SE+'frontend/actividad/agregar2/?token=<?php echo @$this->userIngles["token"] ?>&acc=edit&idnivel=&txtUnidad=&idcursodetalle='+idcursodetalle+'&txtsesion='+idrecurso;
		a.target='_blank';
		a.click();
		//window.location.href=

			//https://inglesadultos.skscourses.com/actividad/agregar2/?idnivel=&txtUnidad=&idcursodetalle=800&txtsesion=56
	}
	smartbook_eliminar(obj){
		let _this=this;
		var item=obj.closest('.item-curso'); 
		var nombre=''+item.find('#nombrecurso').text();
		let PV=_this.ventana.find('select#plataforma').val()||'';
		let idcurso=_this.ventana.find('select#idcurso').val()||'';	
		let idcursodetalle=item.attr('idcursodetalle')||'';	
		let idrecurso=item.attr('idrecurso')||'';
		Swal.fire({
		  title: 'Está seguro de eliminar este registro?',
		  text: "Esto no se podrá revertir!",
		  icon: 'warning',
		  type:'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si',
		  cancelButtonText: 'No',
		}).then((result) => {
			
		  if(result.value==true){
		  	if(PV=='PVADU_1') _this.URL_SE='<?php echo URL_SEAdultos ?>';
			else _this.URL_SE='<?php echo  URL_SEAdolecentes?>';
			let idcurso=_this.ventana.find('select#idcurso').val()||'';		
			var data=new FormData();
			data.append('PV_INGLES',PV);
			data.append('idcurso',idcurso);	
			data.append('idrecurso',idrecurso);
			data.append('idcursodetalle',idcursodetalle);
		    _this.$fun.postData({url:_sysUrlBase_+'smartbook/ingles/eliminar','data':data}).then(rs => {
	          	item.parent().remove();	          	
	        }).catch(e =>{console.log(e)})
		  }
		})
	}
	iniciar_sesion(){
		let _this=this;
		let PV=_this.ventana.find('select#plataforma').val()||'';
		if(PV=='PVADU_1') _this.URL_SE='<?php echo URL_SEAdultos ?>';
		else _this.URL_SE='<?php echo  URL_SEAdolecentes?>';

		var data=new FormData();
			data.append('PV_INGLES',PV);
			data.append('idempresa','<?php echo @$this->userIngles["idempresa"] ?>');	
			data.append('idproyecto','<?php echo @$this->userIngles["idproyecto"] ?>');
			data.append('token','<?php echo @$this->userIngles["token"] ?>');
			data.append('apiexterno',true);
		_this.$fun.postData({url:_this.URL_SE+'json/sesion/login',data:data,showloading:false}).then(rs =>{
			if(rs.code==200){
				_this.ventana.find('#listadosmartbooks').find('.smartbook_edit').show();
				_this.ventana.find('#listadosmartbooks').find('.smartbook_eliminar').show();
				_this.loginingles=true;
			}
		})

	}
}


$(function(){
	var oSmartbook=new venSmartbook('<?php echo $ventana; ?>');	
    oSmartbook.ventana.find('select#plataforma').trigger('change');
})

</script>