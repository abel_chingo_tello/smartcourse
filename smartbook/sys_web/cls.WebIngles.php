<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/07/2012
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEnglish_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_niveles', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_actividad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_herramientas', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook_se', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_smartbook_se', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE);*/
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE);

class WebIngles extends JrWeb
{
	private $NegEnglish_cursodetalle;
   /* private $oNegBitac_alum_smartbook_se;
    private $oNegBitacora_smartbook_se;
    private $oNegActividad_alumno;
    private $oNegBolsa_empresas;
    private $userEnglish;*/


    // datos de pv de ingles
    private $BD_SE;
    public $URL_SE;
    protected $css;


	public function __construct()
	{
        if(false === NegSesion::existeSesion()){
            header('Location: '.URL_BASE);
            exit();
        }
        parent::__construct();
		$this->NegEnglish_cursodetalle = new NegEnglish_cursodetalle($this->DB);
		$this->NegEnglish_Niveles = new NegEnglish_niveles($this->DB);
		$this->NegEnglish_Actividad = new NegEnglish_actividad($this->DB);
		$this->NegEnglish_Herramientas = new NegEnglish_herramientas($this->DB);		
        $this->conectar_ingles();
        /*$this->DB=NAME_BDSEAdolecentes;
        $this->url_SE=URL_SEAdolecentes;
        $this->css='ver';

        $this->PV=!empty($_REQUEST['PV'])?$_REQUEST['PV']:'PVADU_1';
        if($this->PV=='PVADU_1'){
            $this->DB=NAME_BDSEAdultos;
            $this->url_SE=URL_SEAdultos;
            $this->css='ver_adultos';
        }*/
        /*$this->oNegBitac_alum_smartbook_se = new NegBitacora_alumno_smartbook_se;
        $this->oNegBitacora_smartbook_se = new NegBitacora_smartbook_se;
        $this->oNegActividad_alumno = new NegActividad_alumno;*/
        $this->oNegBolsa_empresas = new NegBolsa_empresas;

        //$this->NegEnglish_cursodetalle = new NegEnglish_cursodetalle($this->DB);
	}
	
	public function defecto()
	{		
        return $this->listado();		
	}

    private function conectar_ingles(){
        $PV=!empty($_REQUEST['PV_INGLES'])?$_REQUEST['PV_INGLES']:'PVADU_1';
        if($PV=='PVADU_1'){
            $this->BD_SE=NAME_BDSEAdultos;
            $this->URL_SE=URL_SEAdultos;
            $this->css='ver_adultos';
        }else{
            $this->BD_SE=NAME_BDSEAdolecentes;
            $this->URL_SE=URL_SEAdolecentes;
            $this->css='ver';
        }
        $this->personaingles();
    }

    public function listado(){
        try{
           global $aplicacion;  
            $this->documento->plantilla = 'reportes';
            $this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
            $this->documento->stylesheet('rw-theme-paris', '/libs/othersLibs/mine/css/');       
           
            //$this->documento->stylesheet('jquery.dataTables.min','/libs/othersLibs/dataTables/');
            $this->documento->stylesheet('tablas', '/libs/abelchingo/');
            //$this->documento->script('jquery.dataTables.min','/libs/othersLibs/dataTables/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            /*$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');*/

            $this->documento->stylesheet('select2.min', '/libs/select2/');
            $this->documento->script('select2.min', '/libs/select2/');
            $this->documento->script('abelchingo', '/libs/abelchingo/');

                     
            $this->documento->plantilla = 'mantenimientos';          
            $this->esquema = 'smartbook/listado';
            $this->conectar_ingles();
            $this->listado_cursos=$this->cursos(false);

            $user_=NegSesion::getUsuario();
            $sesion = JrSession::getInstancia();
            $useringles=$sesion->get('iduser_'.$this->BD_SE,null,'useringles');
            $user_["getpersona"]=true;
            $user_["iduseringles"]=$useringles;
            $this->userIngles=$this->NegEnglish_cursodetalle->personaingles($user_,$this->BD_SE);
            $this->userIngles=!empty($this->userIngles[0])?$this->userIngles[0]:null;    
            $this->user=$user_;  
            //print_r($this->user);   
            //$iduser=$this->NegEnglish_cursodetalle->personaingles($user_,$this->BD_SE);
            //var_dump($iduser);

            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }


    public function solover() // solo ver el smartbook sin interaccion
    {
        try{
            global $aplicacion;
            $this->documento->stylesheet('mine', '/libs/othersLibs/mine/css/');
            # *** Librerías *** #
             $this->documento->script('tinymce.min', '/libs/tinymce54/');
             $this->documento->script('encode', '/libs/chingo/');
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->script('jquery.ui.touch-punch.min','/libs/jquery-ui-touch/');
             $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
             
             $this->documento->script('slick.min', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('cronometro', '/libs/chingo/');
             $this->documento->script('audioRecorder','/libs/speach/');
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('callbackManager', '/libs/speach/');
             $this->documento->script('jquery.md5', '/tema/js/'); 
             $this->documento->script('actividad_verdad_falso', '/libs/smartenglish/');
             $this->documento->script('actividad_dialogo', '/libs/smartenglish/');
             $this->documento->script('plantillas', '/libs/smartenglish/');

             $this->documento->stylesheet('speach','/libs/smartenglish/');
             $this->documento->script('speach','/libs/smartenglish/');
             $this->documento->stylesheet('actividad_nlsw','/libs/smartenglish/');
             $this->documento->script('actividad_nlsw','/libs/smartenglish/');
             //$this->documento->script('completar','/libs/smartenglish/'); 
             $this->documento->script('tools_games','/libs/smartenglish/');  

             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->stylesheet('estilo', '/libs/crusigrama/');
             $this->documento->script('crossword', '/libs/crusigrama/');
             $this->documento->script('micrusigrama', '/libs/crusigrama/');
             $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
             $this->documento->script('wordfind', '/libs/sopaletras/js/');
             $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
             $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
             $this->documento->script('pronunciacion', '/js/');
             $this->documento->script('scorm1', '/js/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
             
             
             $idcurso_se=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:-1;
             $idcursodetalle_se=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:-1;
             $idsesion_se=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:-1;

            $filtros['idcursodetalle']=$idcursodetalle_se;
            $PV=!empty($_REQUEST['PV_INGLES'])?$_REQUEST['PV_INGLES']:'PVADU_1';
            if($PV=='PVADU_1'){
                $BD_SE=NAME_BDSEAdultos;
                $this->URL_SE=URL_SEAdultos;
                $this->css='ver_adultos';
            }else{
                $BD_SE=NAME_BDSEAdolecentes;
                $this->URL_SE=URL_SEAdolecentes;
                $this->css='ver';
            }

            $filtros['BD']=$this->BD_SE;
            $cursodetalle=$this->NegEnglish_cursodetalle->buscar($filtros);

            if(empty($cursodetalle[0])){ throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }
            $this->idsesionCursodetalle = $cursodetalle[0]['idrecurso'];
            $this->cursodetalle=$cursodetalle[0];  
            $this->idcursoSE=$this->cursodetalle["idcurso"];
            $this->idcurso=$this->idcursoSE;
            $this->idcursoDetalle=$this->cursodetalle["idcursodetalle"];
            //$cursos=$this->oNegcurso->buscar(array('idcurso'=>$this->idcursoSE));
            $cursos=$this->NegEnglish_cursodetalle->buscarcurso(array('idcurso'=>$this->idcursoSE,'PV'=>@$PV,'BD'=>$this->BD_SE));
            $this->curso=array();
            if(!empty($cursos[0])) $this->curso=$cursos[0];                     
            $this->idrecurso=$this->cursodetalle["idrecurso"];
            $this->idlogro=$this->cursodetalle["idlogro"];
            $this->orden=$this->cursodetalle["orden"];
            $this->idactividad=$this->idrecurso;
            $this->sesiones=$this->NegEnglish_Niveles->buscar(array('tipo'=>'L','idnivel'=>$this->idactividad,'BD'=>$this->BD_SE));
            $this->sesion=array();
            if(!empty($this->sesiones[0])) $this->sesion=$this->sesiones[0];
            else { throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }
            $usuarioAct = NegSesion::getUsuario();
            $this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $usuarioAct["idempresa"]));
            $this->logo_emp = @$this->empresa["logo"];
            
            $this->smartbook=$this->sesion["nombre"];

            $intentos=5;
            $this->intentos=$intentos;//
           
            $this->habilidades=$this->NegEnglish_Actividad->buscarmetodologia(array('tipo'=>'H','BD'=>$this->BD_SE));
            $this->ejercicios = $this->NegEnglish_Actividad->fullActividades(array('sesion'=>$this->idactividad,'BD'=>$this->BD_SE));
            
            $this->look = !empty($this->ejercicios)?$this->ejercicios[1]:null;
            $this->practice = !empty($this->ejercicios)?$this->ejercicios[2]:null;
            $this->dby = !empty($this->ejercicios)?$this->ejercicios[3]:null;
            $this->workbook =array();
            $this->vocabulario=array();
            $this->games=array();

            $herr=$this->NegEnglish_Herramientas->buscar(array('idactividad'=>$this->idactividad,'BD'=>$this->BD_SE));
            if(!empty($herr)){
                foreach ($herr as $hkey => $hvalue){
                    if($hvalue['tool']=='G'){
                        $this->games[]=$hvalue;
                    }else if($hvalue['tool']=='V'){
                        $this->vocabulario[0]= $hvalue;           
                    }
                }
            }

            $this->pdfs = null;
            $this->audios = null;            
            $this->imagenes = null;            
            $this->videos = null;
            /*$this->urlBtnRetroceder = '/curso/?id='.$this->idcurso;
            $this->breadcrumb = [
                [ 'texto'=> $this->curso["nombre"], 'link'=> $this->urlBtnRetroceder ],
                //[ 'texto'=> $this->nivel['nombre'], 'link'=> '/smartbook/listar/'.$this->nivel['idnivel'] ],
                //[ 'texto'=> $this->unidad['nombre'], 'link'=> '/smartbook/listar/'.$this->nivel['idnivel'].'/'.$this->unidad['idnivel'] ],
                [ 'texto'=> $this->sesion['nombre'] ],
            ];*/

            $iddetalle=array();
            if(!empty($this->practice['act']['det'])){
                foreach ($this->practice['act']['det'] as $i=>$ejerc) {
                    $iddetalle[]=$ejerc["iddetalle"];                    
                }
            }
            /* Ejercicios - D.B.Y. */
            if(!empty($this->dby['act']['det'])){
                foreach ($this->dby['act']['det'] as $i=>$ejerc) {
                    $iddetalle[]=$ejerc["iddetalle"];                   
                }
            }
           
            $this->documento->setTitulo(JrTexto::_('Smartbook'));
            $this->documento->plantilla = 'verblanco';          
            $this->esquema = 'smartbook/solover';
            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }


    /*retorna el listado de cursos en json*/
    public function cursos($json=true){
        try{
            $this->conectar_ingles();
            $cursos=$this->NegEnglish_cursodetalle->buscarcurso(array('estado'=>1,'sql3'=>1,'BD'=>$this->BD_SE));
            if($json==true){
                echo json_encode(array('code'=>200,'data'=>$cursos));
                exit(0);
            }else 
            return $cursos;
        }catch(EXception $ex){

        }
    }

    public function sesiones($json=true){
        try{
            $this->conectar_ingles();            
            $texto=!empty($_REQUEST["texto"])?$_REQUEST["texto"]:'';
            $idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:31;
            if($idcurso=='all'){
                $temas=$this->NegEnglish_cursodetalle->allsesiones(0,$this->BD_SE,$texto);
            }else
                $temas=$this->NegEnglish_cursodetalle->solosesiones($idcurso,0,$this->BD_SE,$texto);
            if($json==true){
                echo json_encode(array('code'=>200,'data'=>$temas));
                exit(0);
            }else 
            return $temas;
        }catch(EXception $ex){

        }   
    }


    public function copiar($json=true){
        try{
            $this->conectar_ingles();            
            $filtros["idcursodetalle"]=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:'';
            $filtros["idrecurso"]=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:'';
            $filtros["idcurso"]=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:31;
            $filtros["nombrecurso"]=!empty($_REQUEST["nombrecurso"])?$_REQUEST["nombrecurso"]:31;
            $filtros["BD"]=$this->BD_SE;
            
            $sesion = JrSession::getInstancia();
            $useringles=$sesion->get('iduser_'.$this->BD_SE,null,'useringles');

            $filtros["idpersona"]=$useringles;            
            $idsesion=$this->NegEnglish_cursodetalle->copiar($filtros);
            $temas=$this->NegEnglish_cursodetalle->solosesiones($filtros["idcurso"],$idsesion,$this->BD_SE,null);
            if($json==true){
                echo json_encode(array('code'=>200,'data'=>$idsesion,'msj'=>'Smartbook copiado correctamente','tema'=>$temas));
                exit(0);
            }else 
            return $temas;
        }catch(EXception $ex){

        }
    }

    public function eliminar($json=true){
        try{
            $this->conectar_ingles();            
            $filtros["idcursodetalle"]=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:'';
            $filtros["idrecurso"]=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:'';
            $filtros["idcurso"]=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:31;           
            $filtros["BD"]=$this->BD_SE;
                        
            $temas=$this->NegEnglish_cursodetalle->eliminarsmartbook($filtros,$this->BD_SE);
            if($json==true){
                echo json_encode(array('code'=>200,'msj'=>'Smartbook eliminado correctamente'));
                exit(0);
            }else 
            return $temas;
        }catch(EXception $ex){

        }
    }

    /*regitsar usuario si no existe*/
    public function personaingles(){
        try{
            $sesion = JrSession::getInstancia();
            $useringles=$sesion->get('iduser_'.$this->BD_SE,null,'useringles');
            if(empty($useringles)){
                $user_=NegSesion::getUsuario();
                $iduser=$this->NegEnglish_cursodetalle->personaingles($user_,$this->BD_SE);
                $sesion->set('iduser_'.$this->BD_SE,$iduser,'useringles');
            }
        }catch(Exception $ex){

        }
    }
}