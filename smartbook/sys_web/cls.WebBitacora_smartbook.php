<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBitacora_smartbook_se', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook_se', RUTA_BASE);
//JrCargador::clase('sys_negocio::NegAlumno_logro', RUTA_BASE);

class WebBitacora_smartbook extends JrWeb
{
	private $oNegBitacora_alumno_smartbook_se;
	private $oNegBitacora_smartbook_se;
	//private $oNegAlumno_logro;
	private $oNegAcad_cursodetalle;		
	public function __construct()
	{
		parent::__construct();		
        $this->usuarioAct = NegSesion::getUsuario();
       
		$this->oNegBitacora_smartbook_se = new NegBitacora_smartbook_se;
		$this->oNegBitacora_alumno_smartbook_se = new NegBitacora_alumno_smartbook_se;
	}
	/** ---------------------------------------------- */
	/** ---------------------------------------------- */
	/** FUNCIONES AJAX */
	/** ---------------------------------------------- */
	/** ---------------------------------------------- */
	public function guardarBitacora(){
		try {
            global $aplicacion;
            if(NegSesion::existeSesion()==false){
				echo json_encode(array('code' => 'Error','msj'=>'Sesion ha sido cerrado'));
				exit();
			}
			$usuarioAct = NegSesion::getUsuario();
			if($usuarioAct["idrol"]!=3){
            	echo json_encode(array('code'=>'ok','msj'=>'Solo guardamos bitacoras  para el Alumno.')); 
            	exit();
            }

            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            //if(!empty(@$pkIdbitacora)) {
            if(!empty($_REQUEST['Idbitacora'])) {
				$this->oNegBitacora_smartbook_se->idbitacora = $_REQUEST['Idbitacora']; //$frm['pkIdbitacora'];
				$accion='_edit';
			}

        	global $aplicacion;         

			@extract($_POST);
			if($accion=='_add'){
				$res=$this->oNegBitacora_smartbook_se->buscar(array('idcurso'=>@$txtIdcurso,'idusuario'=>@$this->usuarioAct['idpersona'],'pestania'=>@$txtPestania,'idbitacora_alum_smartbook'=>@$Idbitacora_smartbook,'idproyecto'=>$this->usuarioAct["idproyecto"],'idcurso_sc'=>@$idcurso_sc,'idgrupoauladetalle'=>@$idgrupoauladetalle));
				if(!empty($res[0])){
					$this->oNegBitacora_smartbook_se->idbitacora = $res[0]["idbitacora"];
					$accion='_edit';
				}
			}
			$this->oNegBitacora_smartbook_se->idcurso=@$txtIdcurso;
			$this->oNegBitacora_smartbook_se->idcursodetalle=@$txtIdsesion;
			$this->oNegBitacora_smartbook_se->idusuario=@$this->usuarioAct['idpersona'];
			$this->oNegBitacora_smartbook_se->idbitacora_alum_smartbook=@$Idbitacora_smartbook;
			$this->oNegBitacora_smartbook_se->pestania=@$txtPestania;
			$this->oNegBitacora_smartbook_se->total_pestanias=@$txtTotal_Pestanias;
			$this->oNegBitacora_smartbook_se->fechahora=date('Y-m-d H:i:s');
			$this->oNegBitacora_smartbook_se->progreso=@$txtProgreso;
			$this->oNegBitacora_smartbook_se->idcurso_sc=@$idcurso_sc;
			$this->oNegBitacora_smartbook_se->idcc=@$idcc;
			$this->oNegBitacora_smartbook_se->idcursodetalle_sc=@$idcursodetalle_sc;
			$this->oNegBitacora_smartbook_se->idproyecto=@$this->usuarioAct["idproyecto"];
			$this->oNegBitacora_smartbook_se->otros_datos= @str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$txtOtros_datos);	
			$this->oNegBitacora_smartbook_se->idgrupoauladetalle=@$idgrupoauladetalle;		
					
            if($accion=='_add') {
            	$res=$this->oNegBitacora_smartbook_se->agregar();
            	$texto_msje = JrTexto::_('saved successfully');
            }else{
            	$res=$this->oNegBitacora_smartbook_se->editar();
            	$texto_msje = JrTexto::_('update successfully');
            }
            $this->oNegBitacora_alumno_smartbook_se->setCampo($Idbitacora_smartbook, 'regfecha', @date('Y-m-d H:i:s'));

            $progreso = $this->oNegBitacora_smartbook_se->getProgresoPromedio(array("idbitacora_alum_smartbook"=>$Idbitacora_smartbook ));

            // $flagMostrado = $this->setAlumnoLogro( (int)@$txtIdlogro, @$txtIdsesion, @$txtIdcurso, $progreso);
            
            echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Bitacora_smartbook')).' '.$texto_msje,'newid'=>$res, 'progreso'=>$progreso)); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage() ));
            exit(0);
        }
	}
}

?>