<?php

/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-04-2017 
 * @copyright	Copyright (C) 12-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegAcad_cursohabilidad', RUTA_BASE, 'sys_negocio');

class WebActividad_alumno extends JrWeb
{
	private $oNegActividad_alumno;
	protected $oNegMetodologia;
	private $oNegGrupo_matricula;
	private $oNegCursodetalle;

	public function __construct()
	{
		parent::__construct();
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegMetodologia = new NegMetodologia_habilidad;
		$this->oNegGrupo_matricula = new NegGrupo_matricula;
		$this->oNegCursodetalle = new NegAcad_cursodetalle;
		// $this->oNegCursohabilidad = new NegAcad_cursohabilidad;
		$this->DB = NAME_BDSEAdolecentes;
		$this->url_SE = URL_SEAdolecentes;
		$this->css = 'ver';
		$this->PV = !empty($_REQUEST['PV']) ? $_REQUEST['PV'] : 'PVADU_1';
		if ($this->PV == 'PVADU_1') {
			$this->DB = NAME_BDSEAdultos;
			$this->url_SE = URL_SEAdultos;
			$this->css = 'ver_adultos';
		}
	}
	public function defecto()
	{
		return $this->listado();
	}

	public function listado()
	{
		try {
			global $aplicacion;
			if (!NegSesion::tiene_acceso('Actividad_alumno', 'list')) {
				throw new Exception(JrTexto::_('Restricted access') . '!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');


			$this->datos = $this->oNegActividad_alumno->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno'), true);
			$this->esquema = 'actividad_alumno-list';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function guardaractividad()
	{
		try {
			global $aplicacion;
			if (NegSesion::existeSesion() == false) {
				echo json_encode(array('code' => 'Error', 'msj' => 'Sesion ha sido cerrado'));
				exit();
			}

			$usuarioAct = NegSesion::getUsuario();
			if($usuarioAct["idrol"]!=3){
            	echo json_encode(array('code'=>'ok','msj'=>'Solo guardamos bitacoras  para el Alumno.')); 
            	exit();
            } 


			$act_alum = $this->oNegActividad_alumno->buscar(array('iddetalleactividad' => @$_POST['iddetalle_actividad'], 'idproyecto' => $this->usuarioAct["idproyecto"], 'idcurso' => @$_REQUEST['idcurso'], 'idcc' => @$_REQUEST['idcc'], 'idalumno' => $this->usuarioAct["idpersona"], 'idgrupoauladetalle' => @$_REQUEST["idgrupoauladetalle"]), $this->DB);
			if (empty($act_alum)) {
				$accion = 'nuevo';
			} else {
				$accion = 'editar';
				$this->oNegActividad_alumno->idactalumno = $act_alum[0]['idactalumno'];
			}

			$this->oNegActividad_alumno->iddetalleactividad = (int) $_POST['iddetalle_actividad'];
			$this->oNegActividad_alumno->idalumno = $this->usuarioAct['idpersona'];
			$this->oNegActividad_alumno->idrecurso = !empty($_POST['idrecurso']) ? (int) $_POST['idrecurso'] : 0;
			$this->oNegActividad_alumno->fecha = date('Y-m-d');
			$this->oNegActividad_alumno->porcentajeprogreso = (float) $_POST['progreso'];
			$this->oNegActividad_alumno->habilidades = $_POST['habilidades'];
			$this->oNegActividad_alumno->estado = $_POST['estado'];
			$this->oNegActividad_alumno->html_solucion = @str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$_POST['html_solucion']);
			$this->oNegActividad_alumno->idcurso = @$_REQUEST['idcurso'];
			$this->oNegActividad_alumno->idcc = @$_REQUEST['idcc'];
			$this->oNegActividad_alumno->idcursodetalle_sc = @$_REQUEST['idcursodetalle_sc'];
			$this->oNegActividad_alumno->idproyecto = @$this->usuarioAct["idproyecto"];
			$this->oNegActividad_alumno->idgrupoauladetalle = @$_REQUEST['idgrupoauladetalle'];

			if ($accion == 'nuevo') {
				$idactalumno = $this->oNegActividad_alumno->agregar();
			} else {
				$idactalumno = $this->oNegActividad_alumno->editar();
			}
			$data = array('code' => 'ok', 'data' => ['idactalumno' => $idactalumno]);
			echo json_encode($data);
			exit(0);
		} catch (Exception $e) {
			//return $aplicacion->error(JrTexto::_($e->getMessage()));
			$data = array('code' => 'Error', 'mensaje' => JrTexto::_($e->getMessage()));
			echo json_encode($data);
			exit(0);
		}
	}

	/*	public function progresoxalumno($idalumno=null)
	{
		$this->documento->plantilla = 'returnjson';
		try{
            global $aplicacion;

            if(empty($_REQUEST["id"]) && $idalumno==null) { 
            	throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $filtros=array();
            $filtros["idalumno"]=($idalumno==null)?$_REQUEST["id"]:$idalumno;
            $idCurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:null;
            $actividad_alumno= array();
            if(empty($idCurso)){
            	$actividad_alumno=$this->oNegActividad_alumno->buscar($filtros);
            }else{
            	$arrCursoDet = $this->oNegCursodetalle->buscar(array("idcurso"=>$idCurso, "tiporecurso"=>'L'));
            	foreach ($arrCursoDet as $cd) {
            		$filtros["sesion"] = $cd["idrecurso"];
            		$acts_alum = $this->oNegActividad_alumno->buscar($filtros);
            		if(!empty($acts_alum)){
            			foreach ($acts_alum as $a) { 
            				$actividad_alumno[] = $a; 
            			}
            		}
            	}
            }
            $arrPorcentajexHab = array();
            foreach ($actividad_alumno as $act_alum) {
            	$arrHab_Act = explode ('|', $act_alum['habilidades']);
            	foreach ($arrHab_Act as $idSkill) {
            		if(!isset($arrPorcentajexHab[$idSkill])) {
            			$cursoHabilidad = $this->oNegCursohabilidad->buscar(array('idcursohabilidad'=>$idSkill));
            			if(!empty($cursoHabilidad)){ $cursoHabilidad = $cursoHabilidad[0]; }
            			$arrPorcentajexHab[$idSkill] = array(
            				'suma_porcentajes' => 0.0,
		    				'total_elem' => 0,
		    				'promedio_porcentaje' => 0.0,
		    				'nombre' => $cursoHabilidad['texto'],
            			);
            		}
	            	$arrPorcentajexHab[$idSkill]['suma_porcentajes']+=$act_alum['porcentajeprogreso'];
            		$arrPorcentajexHab[$idSkill]['total_elem']+=1 ;
            		$arrPorcentajexHab[$idSkill]['promedio_porcentaje']=$arrPorcentajexHab[$idSkill]['suma_porcentajes']/$arrPorcentajexHab[$idSkill]['total_elem'] ;
            	}
            }
            if($idalumno==null){
	            $data=array('code'=>'ok','data'=>$arrPorcentajexHab);
	            echo json_encode($data);
            } else {
            	return $arrPorcentajexHab;
            }
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
	}

	public function progresoxgrupo()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST["id"])) { 
            	throw new Exception(JrTexto::_('Error in filtering')); 
            }
			$filtros = [];
			$arrPtjesProm = [];
            $filtros['idgrupo']=$_POST['id'];
            $alum_grupo = $this->oNegGrupo_matricula->buscar($filtros);
            foreach ($alum_grupo as $al) {
            	$arrPtjes = $this->progresoxalumno($al['idalumno']);
            	foreach ($arrPtjes as $idHab => $array) {
            		if(!isset($arrPtjesProm[$idHab])){
            			$arrPtjesProm[$idHab] = [];
            			$arrPtjesProm[$idHab] = [
            				'suma_porcentajes' => 0.0,
		    				'total_elem' => 0,
		    				'promedio_porcentaje' => 0.0,
            			];
            		}
            		$arrPtjesProm[$idHab]['suma_porcentajes'] += $array['promedio_porcentaje'];
            		$arrPtjesProm[$idHab]['total_elem'] += 1;
            		$arrPtjesProm[$idHab]['promedio_porcentaje'] = $arrPtjesProm[$idHab]['suma_porcentajes']/$arrPtjesProm[$idHab]['total_elem'];
            	}
            }

            $data=array('code'=>'ok','data'=>$arrPtjesProm);
	        echo json_encode($data);
	        return parent::getEsquema();
		} catch (Exception $ex) {
			
		}
	}    */
}
