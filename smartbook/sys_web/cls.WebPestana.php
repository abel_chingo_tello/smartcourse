<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEnglish_cursodetalle', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_niveles', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_actividad', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEnglish_herramientas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook_se', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBitacora_smartbook_se', RUTA_BASE);
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE);
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE);

class WebPestana extends JrWeb{
	private $NegEnglish_cursodetalle;
    private $oNegBitac_alum_smartbook_se;
    private $oNegBitacora_smartbook_se;
    private $oNegActividad_alumno;
    private $oNegBolsa_empresas;

    protected $url_SE;
    protected $css;

    public function __construct()
    {
        $this->DB=NAME_BDSEAdolecentes;
        $this->url_SE=URL_SEAdolecentes;
        $this->css='ver';
        $this->totalPestana = 4; // Total de pestaña, por defecto 4
        $this->PV=!empty($_REQUEST['PV'])?$_REQUEST['PV']:'PVADU_1';
        if($this->PV=='PVADU_1'){
            $this->DB=NAME_BDSEAdultos;
            $this->url_SE=URL_SEAdultos;
            $this->css='ver_adultos';
        }

        $this->NegEnglish_cursodetalle = new NegEnglish_cursodetalle($this->DB);
		$this->NegEnglish_Niveles = new NegEnglish_niveles($this->DB);
		$this->NegEnglish_Actividad = new NegEnglish_actividad($this->DB);
		$this->NegEnglish_Herramientas = new NegEnglish_herramientas($this->DB);		
        $this->oNegBitac_alum_smartbook_se = new NegBitacora_alumno_smartbook_se;
        $this->oNegBitacora_smartbook_se = new NegBitacora_smartbook_se;
        $this->oNegActividad_alumno = new NegActividad_alumno;
        $this->oNegBolsa_empresas = new NegBolsa_empresas;

        $this->usuario = NegSesion::getUsuario();
        parent::__construct();
    }
    private function ImportarLibrerias(){
        $this->documento->script('tinymce.min', '/libs/tinymce54/');
        $this->documento->script('encode', '/libs/chingo/');
        $this->documento->script('jquery-ui.min', '/tema/js/');
        $this->documento->script('jquery.ui.touch-punch.min','/libs/jquery-ui-touch/');
        $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
        $this->documento->script('jquery.maskedinput.min', '/tema/js/');
        $this->documento->script('cronometro', '/libs/chingo/');
        $this->documento->script('audioRecorder','/libs/speach/');
        $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
        $this->documento->script('callbackManager', '/libs/speach/');
        $this->documento->script('jquery.md5', '/tema/js/'); 
        $this->documento->script('actividad_verdad_falso', '/libs/smartenglish/');
        $this->documento->script('actividad_dialogo', '/libs/smartenglish/');
        $this->documento->script('plantillas', '/libs/smartenglish/');
        $this->documento->stylesheet('speach','/libs/smartenglish/');
        $this->documento->script('speach','/libs/smartenglish/');
        $this->documento->stylesheet('actividad_nlsw','/libs/smartenglish/');
        $this->documento->script('actividad_nlsw','/libs/smartenglish/');
        $this->documento->script('pronunciacion', '/js/');
        $this->documento->script('jquery-confirm.min', '/libs/alert/');

        $this->documento->script("",URL_BASE."smartbook/plantillas/smartbook/js/Utils.js");
    }
    private function getParametrosRequest($arreglo){
        try{
            $parametros_request = "?";
            if(!empty($arreglo)){
                foreach($arreglo as $key=>$value){
                    $parametros_request .= "{$key}={$value}&";
                }
            }
            $parametros_request = rtrim($parametros_request,"&");
            return $parametros_request;
        }catch(Exception $e){
            return "";
        }
    }
    public function look(){
        try{
            global $aplicacion;  
            $this->documento->script('pronunciacion', '/js/');

            $this->parametros = $this->getParametrosRequest($_REQUEST);
            $this->rutabase = $this->documento->getUrlBase()."smartbook/";
            //Parametros
            $idcurso_se=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:-1;
            $idcursodetalle_se=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:-1;
            $idsesion_se=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:-1;
            $idcurso_sc=!empty($_REQUEST["idcursoexterno"])?$_REQUEST["idcursoexterno"]:-1; // curso SC
            $idcc=!empty($_REQUEST["icc"])?$_REQUEST["icc"]:0;
            $idcursodetalle_sc=!empty($_REQUEST["idcursodetalle_sc"])?$_REQUEST["idcursodetalle_sc"]:-1;
            $idpestania=!empty($_REQUEST["idpestania"])?$_REQUEST["idpestania"]:0;

            $this->idcurso_sc=$idcurso_sc;
            $this->idcc=$idcc;
            $this->idcursodetalle_sc=$idcursodetalle_sc;
            $this->idgrupoauladetalle=!empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:'';
            $filtros['idcursodetalle']=$idcursodetalle_se;
            $filtros['BD']=$this->DB;
            $cursodetalle=$this->NegEnglish_cursodetalle->buscar($filtros);

            if(empty($cursodetalle[0])){ throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            $this->idsesionCursodetalle = $cursodetalle[0]['idrecurso'];
            $this->cursodetalle=$cursodetalle[0];  
            $this->idcursoSE=$this->cursodetalle["idcurso"];
            $this->idcurso=$this->idcursoSE;
            $this->idcursoDetalle=$this->cursodetalle["idcursodetalle"];

            $cursos=$this->NegEnglish_cursodetalle->buscarcurso(array('idcurso'=>$this->idcursoSE,'PV'=>@$_REQUEST["PV"],'BD'=>$this->DB));

            $this->curso=array();

            if(!empty($cursos[0])) $this->curso=$cursos[0];                     
            $this->idrecurso=$this->cursodetalle["idrecurso"];
            $this->idlogro=$this->cursodetalle["idlogro"];
            $this->orden=$this->cursodetalle["orden"];
            $this->idactividad=$this->idrecurso;
            $this->sesiones=$this->NegEnglish_Niveles->buscar(array('tipo'=>'L','idnivel'=>$this->idactividad,'BD'=>$this->DB));
            $this->sesion=array();
            if(!empty($this->sesiones[0])) $this->sesion=$this->sesiones[0];
            else { throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            if(empty($this->usuario)){ throw new Exception("User Not Found"); }

            $this->idrol = $this->usuario["idrol"];
            $this->rolActivo=($this->idrol==2||$this->idrol==3)?'Alumno':$this->usuario["rol"];
            $this->idpersona=$this->usuario["idpersona"];
            $this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuario["idempresa"]));
            $this->logo_emp = @$this->empresa["logo"];

            $this->habilidades=$this->NegEnglish_Actividad->buscarmetodologia(array('tipo'=>'H','BD'=>$this->DB));
            $this->ejercicios = $this->NegEnglish_Actividad->fullActividades(array('sesion'=>$this->idactividad,'BD'=>$this->DB));

            $this->documento->stylesheet("",URL_BASE."smartbook/plantillas/smartbook/css/mobile/look.css");
            $this->documento->script("",URL_BASE."smartbook/plantillas/smartbook/js/Utils.js");
            $this->documento->script('actividad_dialogo', '/libs/smartenglish/');
            $this->documento->script('plantillas', '/libs/smartenglish/');
            

            $this->look = !empty($this->ejercicios)?$this->ejercicios[1]:null;

            $this->documento->setTitulo(JrTexto::_('Smartbook'));
            $this->documento->plantilla = 'verblanco_mobile';
            $this->esquema = "smartbook_v2/look";
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function practice(){
        try{
            global $aplicacion;  

            $this->parametros = $this->getParametrosRequest($_REQUEST);
            $this->rutabase = $this->documento->getUrlBase()."smartbook/";
            $this->ImportarLibrerias();

            $idcurso_se=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:-1;
            $idcursodetalle_se=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:-1;
            $idsesion_se=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:-1;
            $idcurso_sc=!empty($_REQUEST["idcursoexterno"])?$_REQUEST["idcursoexterno"]:-1; // curso SC
            $idcc=!empty($_REQUEST["icc"])?$_REQUEST["icc"]:0;
            $idcursodetalle_sc=!empty($_REQUEST["idcursodetalle_sc"])?$_REQUEST["idcursodetalle_sc"]:-1;
            $idpestania=!empty($_REQUEST["idpestania"])?$_REQUEST["idpestania"]:0;

            $this->idcurso_sc=$idcurso_sc;
            $this->idcc=$idcc;
            $this->idcursodetalle_sc=$idcursodetalle_sc;
            $this->idgrupoauladetalle=!empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:'';
            $filtros['idcursodetalle']=$idcursodetalle_se;
            $filtros['BD']=$this->DB;
            $cursodetalle=$this->NegEnglish_cursodetalle->buscar($filtros);

            if(empty($cursodetalle[0])){ throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            $this->idsesionCursodetalle = $cursodetalle[0]['idrecurso'];
            $this->cursodetalle=$cursodetalle[0];  
            $this->idcursoSE=$this->cursodetalle["idcurso"];
            $this->idcurso=$this->idcursoSE;
            $this->idcursoDetalle=$this->cursodetalle["idcursodetalle"];

            $cursos=$this->NegEnglish_cursodetalle->buscarcurso(array('idcurso'=>$this->idcursoSE,'PV'=>@$_REQUEST["PV"],'BD'=>$this->DB));

            $this->curso=array();

            if(!empty($cursos[0])) $this->curso=$cursos[0];                     
            $this->idrecurso=$this->cursodetalle["idrecurso"];
            $this->idlogro=$this->cursodetalle["idlogro"];
            $this->orden=$this->cursodetalle["orden"];
            $this->idactividad=$this->idrecurso;
            $this->sesiones=$this->NegEnglish_Niveles->buscar(array('tipo'=>'L','idnivel'=>$this->idactividad,'BD'=>$this->DB));
            $this->sesion=array();
            if(!empty($this->sesiones[0])) $this->sesion=$this->sesiones[0];
            else { throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            if(empty($this->usuario)){ throw new Exception("User Not Found"); }

            $this->idrol = $this->usuario["idrol"];
            $this->rolActivo=($this->idrol==2||$this->idrol==3)?'Alumno':$this->usuario["rol"];
            $this->idpersona=$this->usuario["idpersona"];
            $this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuario["idempresa"]));
            $this->logo_emp = @$this->empresa["logo"];

            $this->habilidades=$this->NegEnglish_Actividad->buscarmetodologia(array('tipo'=>'H','BD'=>$this->DB));
            $this->ejercicios = $this->NegEnglish_Actividad->fullActividades(array('sesion'=>$this->idactividad,'BD'=>$this->DB));

            $this->practice = !empty($this->ejercicios)?$this->ejercicios[2]:null;
            
            $this->pdfs = null;
            $this->audios = null;            
            $this->imagenes = null;            
            $this->videos = null;

            $iddetalle=array();
            if(!empty($this->practice['act']['det'])){
                foreach ($this->practice['act']['det'] as $i=>$ejerc) {
                    $iddetalle[]=$ejerc["iddetalle"];                    
                }
            }

            $filtraractividad=array('iddetalleactividad'=>$iddetalle,'idproyecto'=>$this->usuario["idproyecto"],'idcurso'=>$idcurso_sc,'idalumno'=>$this->usuario["idpersona"],'idgrupoauladetalle'=>$this->idgrupoauladetalle);

            $detalles=$this->oNegActividad_alumno->buscar($filtraractividad,$this->DB);

            if(!empty($this->practice['act']['det'])&& !empty($detalles)){
                foreach ($this->practice['act']['det'] as $i=>$ejerc) {  
                  //var_dump($ejerc,$detalles)  ;                
                    foreach ($detalles as $det => $dval){
                        if($dval['iddetalleactividad']==$ejerc["iddetalle"]){ 
                           // var_dump($dval);
                            $this->practice['act']['det'][$i]['desarrollo']=$dval;
                           unset($detalles[$det]);        
                        }
                    }
                }
            }
            $this->bitac_alum_smbook = $this->oNegBitac_alum_smartbook_se->buscar(array('idusuario'=>$this->usuario['idpersona'],'idproyecto'=>$this->usuario["idproyecto"],'idcurso_sc'=>$idcurso_sc,'idsesion'=>$this->idactividad,'idgrupoauladetalle'=>$this->idgrupoauladetalle));            
            if(!empty($this->bitac_alum_smbook)) {
                $this->bitac_alum_smbook = $this->bitac_alum_smbook[0];
            }
            $this->bitacora = $this->oNegBitacora_smartbook_se->buscar(array('idusuario'=>$this->usuario['idpersona'],'idproyecto'=>$this->usuario["idproyecto"],'idcurso_sc'=>$idcurso_sc,'idsesion'=>$this->idactividad,'idgrupoauladetalle'=>$this->idgrupoauladetalle));
            
            //ESTA PARA OPTIMIZAR
            if(!empty($this->bitacora)){
                foreach ($this->bitacora as $i => $b) {
                    $otros_datos = $b['otros_datos'];
                    $b['otros_datos'] = json_decode($otros_datos,true);
                    if($i===0){ $b['ultimo_visto'] = 'ultimo_visto'; }
                    if($b['pestania'] != "div_practice"){
                        continue;
                    }else{
                        $this->practice['bitacora'] =$b;
                        break;
                    }
                }
            }

            //Estilo para la vista
            $this->documento->stylesheet("",URL_BASE."smartbook/plantillas/smartbook/css/mobile/practice.css");
            
            $this->documento->setTitulo(JrTexto::_('Smartbook'));
            $this->documento->plantilla = 'verblanco_mobile';
            $this->esquema = "smartbook_v2/practice";
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));

        }
    }
    public function games(){
        try{
            global $aplicacion;
            
            $this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
            $this->documento->script('plantillas', '/libs/smartenglish/');
            $this->documento->script('tools_games','/libs/smartenglish/'); 
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
            $this->documento->script('scorm1', '/js/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script("pronunciacion","/js/");
            
            $this->parametros = $this->getParametrosRequest($_REQUEST);
            $this->rutabase = $this->documento->getUrlBase()."smartbook/";

            $idcurso_se=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:-1;
            $idcursodetalle_se=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:-1;
            $idsesion_se=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:-1;
            $idcurso_sc=!empty($_REQUEST["idcursoexterno"])?$_REQUEST["idcursoexterno"]:-1; // curso SC
            $idcc=!empty($_REQUEST["icc"])?$_REQUEST["icc"]:0;
            $idcursodetalle_sc=!empty($_REQUEST["idcursodetalle_sc"])?$_REQUEST["idcursodetalle_sc"]:-1;
            $idpestania=!empty($_REQUEST["idpestania"])?$_REQUEST["idpestania"]:0;

            $this->idcurso_sc=$idcurso_sc;
            $this->idcc=$idcc;
            $this->idcursodetalle_sc=$idcursodetalle_sc;
            $this->idgrupoauladetalle=!empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:'';
            $filtros['idcursodetalle']=$idcursodetalle_se;
            $filtros['BD']=$this->DB;
            $cursodetalle=$this->NegEnglish_cursodetalle->buscar($filtros);

            if(empty($cursodetalle[0])){ throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            $this->idsesionCursodetalle = $cursodetalle[0]['idrecurso'];
            $this->cursodetalle=$cursodetalle[0];  
            $this->idcursoSE=$this->cursodetalle["idcurso"];
            $this->idcurso=$this->idcursoSE;
            $this->idcursoDetalle=$this->cursodetalle["idcursodetalle"];

            $cursos=$this->NegEnglish_cursodetalle->buscarcurso(array('idcurso'=>$this->idcursoSE,'PV'=>@$_REQUEST["PV"],'BD'=>$this->DB));

            $this->curso=array();

            if(!empty($cursos[0])) $this->curso=$cursos[0];                     
            $this->idrecurso=$this->cursodetalle["idrecurso"];
            $this->idlogro=$this->cursodetalle["idlogro"];
            $this->orden=$this->cursodetalle["orden"];
            $this->idactividad=$this->idrecurso;
            $this->sesiones=$this->NegEnglish_Niveles->buscar(array('tipo'=>'L','idnivel'=>$this->idactividad,'BD'=>$this->DB));
            $this->sesion=array();
            if(!empty($this->sesiones[0])) $this->sesion=$this->sesiones[0];
            else { throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            if(empty($this->usuario)){ throw new Exception("User Not Found"); }

            $this->idrol = $this->usuario["idrol"];
            $this->rolActivo=($this->idrol==2||$this->idrol==3)?'Alumno':$this->usuario["rol"];
            $this->idpersona=$this->usuario["idpersona"];
            $this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuario["idempresa"]));
            $this->logo_emp = @$this->empresa["logo"];

            $this->games=array();
            //vocabularios y games
            $herr=$this->NegEnglish_Herramientas->buscar(array('idactividad'=>$this->idactividad,'BD'=>$this->DB));
            if(!empty($herr)){
                foreach ($herr as $hkey => $hvalue){
                    if($hvalue['tool']=='G'){
                        $this->games[]=$hvalue;
                    }
                }
            }

            $this->bitac_alum_smbook = $this->oNegBitac_alum_smartbook_se->buscar(array('idusuario'=>$this->usuario['idpersona'],'idproyecto'=>$this->usuario["idproyecto"],'idcurso_sc'=>$idcurso_sc,'idsesion'=>$this->idactividad,'idgrupoauladetalle'=>$this->idgrupoauladetalle));            
            if(!empty($this->bitac_alum_smbook)) {
                $this->bitac_alum_smbook = $this->bitac_alum_smbook[0];
            }
            $this->bitacora = $this->oNegBitacora_smartbook_se->buscar(array('idusuario'=>$this->usuario['idpersona'],'idproyecto'=>$this->usuario["idproyecto"],'idcurso_sc'=>$idcurso_sc,'idsesion'=>$this->idactividad,'idgrupoauladetalle'=>$this->idgrupoauladetalle));
            
            //Estilo para la vista
            $this->documento->stylesheet("",URL_BASE."smartbook/plantillas/smartbook/css/mobile/games.css");
            $this->documento->script("",URL_BASE."smartbook/plantillas/smartbook/js/Utils.js");

            $this->documento->setTitulo(JrTexto::_('Smartbook'));
            $this->documento->plantilla = 'verblanco_mobile';
            $this->esquema = "smartbook_v2/games";
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function vocabulary(){
        try{
            global $aplicacion;
            
            //$this->documento->script('slick.min', '/libs/sliders/slick/');
            //$this->documento->stylesheet('slick', '/libs/sliders/slick/');
            //$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
            $this->documento->script('plantillas', '/libs/smartenglish/');
            //$this->documento->script('tools_games','/libs/smartenglish/'); 
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            //$this->documento->script('crossword', '/libs/crusigrama/');
            //$this->documento->script('micrusigrama', '/libs/crusigrama/');
            //$this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            //$this->documento->script('wordfind', '/libs/sopaletras/js/');
            //$this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            //$this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
            //$this->documento->script('scorm1', '/js/');
            $this->documento->script("pronunciacion","/js/");
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
            $this->parametros = $this->getParametrosRequest($_REQUEST);
            $this->rutabase = $this->documento->getUrlBase()."smartbook/";

            $idcurso_se=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:-1;
            $idcursodetalle_se=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:-1;
            $idsesion_se=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:-1;
            $idcurso_sc=!empty($_REQUEST["idcursoexterno"])?$_REQUEST["idcursoexterno"]:-1; // curso SC
            $idcc=!empty($_REQUEST["icc"])?$_REQUEST["icc"]:0;
            $idcursodetalle_sc=!empty($_REQUEST["idcursodetalle_sc"])?$_REQUEST["idcursodetalle_sc"]:-1;
            $idpestania=!empty($_REQUEST["idpestania"])?$_REQUEST["idpestania"]:0;

            $this->idcurso_sc=$idcurso_sc;
            $this->idcc=$idcc;
            $this->idcursodetalle_sc=$idcursodetalle_sc;
            $this->idgrupoauladetalle=!empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:'';
            $filtros['idcursodetalle']=$idcursodetalle_se;
            $filtros['BD']=$this->DB;
            $cursodetalle=$this->NegEnglish_cursodetalle->buscar($filtros);

            if(empty($cursodetalle[0])){ throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            $this->idsesionCursodetalle = $cursodetalle[0]['idrecurso'];
            $this->cursodetalle=$cursodetalle[0];  
            $this->idcursoSE=$this->cursodetalle["idcurso"];
            $this->idcurso=$this->idcursoSE;
            $this->idcursoDetalle=$this->cursodetalle["idcursodetalle"];

            $cursos=$this->NegEnglish_cursodetalle->buscarcurso(array('idcurso'=>$this->idcursoSE,'PV'=>@$_REQUEST["PV"],'BD'=>$this->DB));

            $this->curso=array();

            if(!empty($cursos[0])) $this->curso=$cursos[0];                     
            $this->idrecurso=$this->cursodetalle["idrecurso"];
            $this->idlogro=$this->cursodetalle["idlogro"];
            $this->orden=$this->cursodetalle["orden"];
            $this->idactividad=$this->idrecurso;
            $this->sesiones=$this->NegEnglish_Niveles->buscar(array('tipo'=>'L','idnivel'=>$this->idactividad,'BD'=>$this->DB));
            $this->sesion=array();
            if(!empty($this->sesiones[0])) $this->sesion=$this->sesiones[0];
            else { throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            if(empty($this->usuario)){ throw new Exception("User Not Found"); }

            $this->idrol = $this->usuario["idrol"];
            $this->rolActivo=($this->idrol==2||$this->idrol==3)?'Alumno':$this->usuario["rol"];
            $this->idpersona=$this->usuario["idpersona"];
            //$this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuario["idempresa"]));
            //$this->logo_emp = @$this->empresa["logo"];

            $this->vocabulario=array();
            //vocabularios y games
            $herr=$this->NegEnglish_Herramientas->buscar(array('idactividad'=>$this->idactividad,'tool'=>'V','BD'=>$this->DB));
            //var_dump($this->idactividad);
            //var_dump($herr);
            if(!empty($herr)){
                foreach ($herr as $hkey => $hvalue){
                    if($hvalue['tool']=='V'){
                        $this->vocabulario[0]= $hvalue;           
                    }
                }
            }


            /*$this->bitac_alum_smbook = $this->oNegBitac_alum_smartbook_se->buscar(array('idusuario'=>$this->usuario['idpersona'],'idproyecto'=>$this->usuario["idproyecto"],'idcurso_sc'=>$idcurso_sc,'idsesion'=>$this->idactividad,'idgrupoauladetalle'=>$this->idgrupoauladetalle));            
            if(!empty($this->bitac_alum_smbook)) {
                $this->bitac_alum_smbook = $this->bitac_alum_smbook[0];
            }
            $this->bitacora = $this->oNegBitacora_smartbook_se->buscar(array('idusuario'=>$this->usuario['idpersona'],'idproyecto'=>$this->usuario["idproyecto"],'idcurso_sc'=>$idcurso_sc,'idsesion'=>$this->idactividad,'idgrupoauladetalle'=>$this->idgrupoauladetalle));
            */
            //Estilo para la vista
            $this->documento->stylesheet("",URL_BASE."smartbook/plantillas/smartbook/css/mobile/vocabulary.css");
            $this->documento->script("",URL_BASE."smartbook/plantillas/smartbook/js/Utils.js");

            $this->documento->setTitulo(JrTexto::_('Smartbook'));
            $this->documento->plantilla = 'verblanco_mobile';
            $this->esquema = "smartbook_v2/vocabulary";
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function dby(){
        try{
            global $aplicacion;  

            $this->parametros = $this->getParametrosRequest($_REQUEST);
            $this->rutabase = $this->documento->getUrlBase()."smartbook/";
            $this->ImportarLibrerias();

            $idcurso_se=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:-1;
            $idcursodetalle_se=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:-1;
            $idsesion_se=!empty($_REQUEST["idrecurso"])?$_REQUEST["idrecurso"]:-1;
            $idcurso_sc=!empty($_REQUEST["idcursoexterno"])?$_REQUEST["idcursoexterno"]:-1; // curso SC
            $idcc=!empty($_REQUEST["icc"])?$_REQUEST["icc"]:0;
            $idcursodetalle_sc=!empty($_REQUEST["idcursodetalle_sc"])?$_REQUEST["idcursodetalle_sc"]:-1;
            $idpestania=!empty($_REQUEST["idpestania"])?$_REQUEST["idpestania"]:0;

            $this->idcurso_sc=$idcurso_sc;
            $this->idcc=$idcc;
            $this->idcursodetalle_sc=$idcursodetalle_sc;
            $this->idgrupoauladetalle=!empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:'';
            $filtros['idcursodetalle']=$idcursodetalle_se;
            $filtros['BD']=$this->DB;
            $cursodetalle=$this->NegEnglish_cursodetalle->buscar($filtros);

            if(empty($cursodetalle[0])){ throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            $this->idsesionCursodetalle = $cursodetalle[0]['idrecurso'];
            $this->cursodetalle=$cursodetalle[0];  
            $this->idcursoSE=$this->cursodetalle["idcurso"];
            $this->idcurso=$this->idcursoSE;
            $this->idcursoDetalle=$this->cursodetalle["idcursodetalle"];

            $cursos=$this->NegEnglish_cursodetalle->buscarcurso(array('idcurso'=>$this->idcursoSE,'PV'=>@$_REQUEST["PV"],'BD'=>$this->DB));

            $this->curso=array();

            if(!empty($cursos[0])) $this->curso=$cursos[0];                     
            $this->idrecurso=$this->cursodetalle["idrecurso"];
            $this->idlogro=$this->cursodetalle["idlogro"];
            $this->orden=$this->cursodetalle["orden"];
            $this->idactividad=$this->idrecurso;
            $this->sesiones=$this->NegEnglish_Niveles->buscar(array('tipo'=>'L','idnivel'=>$this->idactividad,'BD'=>$this->DB));
            $this->sesion=array();
            if(!empty($this->sesiones[0])) $this->sesion=$this->sesiones[0];
            else { throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            if(empty($this->usuario)){ throw new Exception("User Not Found"); }

            $this->idrol = $this->usuario["idrol"];
            $this->rolActivo=($this->idrol==2||$this->idrol==3)?'Alumno':$this->usuario["rol"];
            $this->idpersona=$this->usuario["idpersona"];
            $this->empresa=$this->oNegBolsa_empresas->buscar(array("idempresa" => $this->usuario["idempresa"]));
            $this->logo_emp = @$this->empresa["logo"];

            $intentos=5;
            $this->intentos=$intentos;

            $this->habilidades=$this->NegEnglish_Actividad->buscarmetodologia(array('tipo'=>'H','BD'=>$this->DB));
            $this->ejercicios = $this->NegEnglish_Actividad->fullActividades(array('sesion'=>$this->idactividad,'BD'=>$this->DB));

            $this->dby = !empty($this->ejercicios)?$this->ejercicios[3]:null;
            
            $this->pdfs = null;
            $this->audios = null;            
            $this->imagenes = null;            
            $this->videos = null;

            $iddetalle=array();
            if(!empty($this->dby['act']['det'])){
                foreach ($this->dby['act']['det'] as $i=>$ejerc) {
                    $iddetalle[]=$ejerc["iddetalle"];                   
                }
            }

            $filtraractividad=array('iddetalleactividad'=>$iddetalle,'idproyecto'=>$this->usuario["idproyecto"],'idcurso'=>$idcurso_sc,'idalumno'=>$this->usuario["idpersona"],'idgrupoauladetalle'=>$this->idgrupoauladetalle);

            $detalles=$this->oNegActividad_alumno->buscar($filtraractividad,$this->DB);

            /* Ejercicios - D.B.Y. */
            if(!empty($this->dby['act']['det']) && !empty($detalles)){
                foreach ($this->dby['act']['det'] as $i=>$ejerc) {                    
                    foreach ($detalles as $det => $dval){
                        if($dval['iddetalleactividad']==$ejerc["iddetalle"]){                          
                            $this->dby['act']['det'][$i]['desarrollo']=$dval;
                            unset($detalles[$det]);        
                        }
                    }
                }
            }
            $this->bitac_alum_smbook = $this->oNegBitac_alum_smartbook_se->buscar(array('idusuario'=>$this->usuario['idpersona'],'idproyecto'=>$this->usuario["idproyecto"],'idcurso_sc'=>$idcurso_sc,'idsesion'=>$this->idactividad,'idgrupoauladetalle'=>$this->idgrupoauladetalle));            
            if(!empty($this->bitac_alum_smbook)) {
                $this->bitac_alum_smbook = $this->bitac_alum_smbook[0];
            }
            $this->bitacora = $this->oNegBitacora_smartbook_se->buscar(array('idusuario'=>$this->usuario['idpersona'],'idproyecto'=>$this->usuario["idproyecto"],'idcurso_sc'=>$idcurso_sc,'idsesion'=>$this->idactividad,'idgrupoauladetalle'=>$this->idgrupoauladetalle));
            
            //ESTA PARA OPTIMIZAR
            if(!empty($this->bitacora)){
                foreach ($this->bitacora as $i => $b) {
                    $otros_datos = $b['otros_datos'];
                    $b['otros_datos'] = json_decode($otros_datos,true);
                    if($i===0){ $b['ultimo_visto'] = 'ultimo_visto'; }
                    if($b['pestania'] != "div_dby"){
                        continue;
                    }else{
                        $this->practice['bitacora'] =$b;
                        break;
                    }
                }
            }

            //Estilo para la vista
            $this->documento->stylesheet("",URL_BASE."smartbook/plantillas/smartbook/css/mobile/dby.css");
            
            $this->documento->setTitulo(JrTexto::_('Smartbook'));
            $this->documento->plantilla = 'verblanco_mobile';
            $this->esquema = "smartbook_v2/dby";
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));

        }
    }
}

?>