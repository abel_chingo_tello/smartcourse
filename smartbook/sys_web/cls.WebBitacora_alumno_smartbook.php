<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/07/2012
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook_se', RUTA_BASE, 'sys_negocio');

class WebBitacora_alumno_smartbook extends JrWeb
{
	private $NegEnglish_cursodetalle;
    protected $url_SE;
    protected $oNegBitacora_alumno_smartbook_smartenglish;


	public function __construct()
	{
        $this->usuarioAct = NegSesion::getUsuario();

		if(false === $this->usuarioAct){
            header('Location: '.URL_BASE);
            exit();
        }
        parent::__construct();
        $DB=NAME_BDSEAdolecentes;
        $this->url_SE=URL_SEAdolecentes;
        if(!empty($_REQUEST['PV'])){
            if($_REQUEST['PV']=='PVADU_1'){
                $DB=NAME_BDSEAdultos;
                $this->url_SE=URL_SEAdultos;
            }
        }
        $this->oNegBitacora_alumno_smartbook_smartenglish = new NegBitacora_alumno_smartbook_se();
		// $this->NegEnglish_cursodetalle = new NegEnglish_cursodetalle($DB);
	}
	
	public function xGuardar()
    {
        try{
            global $aplicacion;
            if(NegSesion::existeSesion()==false){
                echo json_encode(array('code' => 'Error','msj'=>'Sesion ha sido cerrado'));
                exit();
            }
            $usuarioAct = NegSesion::getUsuario();
            if($usuarioAct["idrol"]!=3){
                echo json_encode(array('code'=>'ok','msj'=>'Solo guardamos bitacoras  para el Alumno.')); 
                exit();
            }

            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            if(!empty(@$pkIdbitacora_smartbook)) {
                $this->oNegBitacora_alumno_smartbook->idbitacora_smartbook = @$pkIdbitacora_smartbook;
                $accion='_edit';
            }            
            $this->oNegBitacora_alumno_smartbook_smartenglish->idcurso=@$txtIdcurso;
            $this->oNegBitacora_alumno_smartbook_smartenglish->idsesion=@$txtIdsesion;
            $this->oNegBitacora_alumno_smartbook_smartenglish->idusuario=@$this->usuarioAct['idpersona'];
            $this->oNegBitacora_alumno_smartbook_smartenglish->estado=@$txtEstado;
            $this->oNegBitacora_alumno_smartbook_smartenglish->regfecha= @date('Y-m-d H:i:s'); 
            $this->oNegBitacora_alumno_smartbook_smartenglish->idcurso_sc=@$idcurso_sc;
            $this->oNegBitacora_alumno_smartbook_smartenglish->idcc=@$idcc;
            $this->oNegBitacora_alumno_smartbook_smartenglish->idcursodetalle_sc=@$idcursodetalle_sc;
            $this->oNegBitacora_alumno_smartbook_smartenglish->idproyecto=@$this->usuarioAct["idproyecto"];
            $this->oNegBitacora_alumno_smartbook_smartenglish->idgrupoauladetalle=@$idgrupoauladetalle;  
            if($accion=='_add') {
                $res=$this->oNegBitacora_alumno_smartbook_smartenglish->agregar();
                 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Bitacora_alumno_smartbook')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
                $res=$this->oNegBitacora_alumno_smartbook_smartenglish->editar();
                echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Bitacora_alumno_smartbook')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
    }
}