var cargarcrucigrama = function(idgui,datos){
    if(datos==undefined)return 0;
    datos=datos[0];   
    if(datos['palabras']==undefined)datos['palabras']=["abel",'chingo','genio'];
    if(datos['frase']==undefined)datos['frase']=["como te llamas",'cual es tu apellido','eres un gran'];
    var words = datos['palabras'];
    var clues = datos['frase'];

    // Create crossword object with the words and clues
    var cw = new Crossword(words, clues);

    // create the crossword grid (try to make it have a 1:1 width to height ratio in 10 tries)
    var tries = 10; 
    var grid = cw.getSquareGrid(tries);

    // report a problem with the words in the crossword
    if(grid == null){
        var bad_words = cw.getBadWords();
        var str = [];
        for(var i = 0; i < bad_words.length; i++){
            str.push(bad_words[i].word);
        }
        alert("Shoot! A grid could not be created with these words:\n" + str.join("\n"));
        return;
    }

    // turn the crossword grid into HTML
    var show_answers = true;
    document.getElementById("crossword"+idgui).innerHTML = CrosswordUtils.toHtml(grid, show_answers);

    // make a nice legend for the clues
    var legend = cw.getLegend(grid);
    addLegendToPage(legend,idgui); // agrega los textos de referncias...
};

function addLegendToPage(groups,idgui){    
    for(var k in groups){
        var html = [];
        var ori=(k=="down")?'col':'row';
        for(var i = 0; i < groups[k].length; i++){
            var txt=groups[k][i]['word'];
            html.push("<li class='crusigramalista' data-valn='"+groups[k][i]['position']+"' data-orient='"+ori+"' data-val='"+txt.toUpperCase()+"' ><strong>" + groups[k][i]['position'] + ".</strong> " + groups[k][i]['clue'] + "</li>");
        }
        document.getElementById(k+idgui).innerHTML = html.join("\n");
    }
}

var jugarcrusigrama=function(plt){
    if(plt.length>0){
        plt.on('keydown','input.tvalcrusigrama',function(ev){
            if($(this).hasClass('ok')) return false;
            $(this).val('');
        }).on('keyup','input.tvalcrusigrama',function(){
            val =$(this).data('val');
            val2=$(this).val();
            var tvalrow=['--'];
            plt.find('li[data-orient="row"]').each(function(){
              var rval=$(this).data('val');
              rval
              tvalrow.push({texto:rval.toUpperCase(),val:rval.toUpperCase().replace(/\s/g,'')});
            });
            var tvalcol=new Array();
            plt.find('li[data-orient="col"]').each(function(){
              var rval=$(this).data('val');

              tvalcol.push({texto:rval.toUpperCase(),val:rval.toUpperCase().replace(/\s/g,'')});
            });
            
            if(val.trim().toUpperCase()==val2.trim().toUpperCase()){
              $(this).addClass('ok').removeClass('error').attr('readonly','true');
              var r=$(this).data('row');
              var c=$(this).data('col');
              var todosrow=plt.find('input.tvalcrusigrama.ok[data-row="'+r+'"]');

              var palabra=new Array();              
              if(todosrow.length>1){
                $.each(todosrow,function(){
                  var c=$(this).data('col');
                  var v_=$(this).data('val');
                  palabra[c]=v_;
                });
                txt=palabra.join("").toUpperCase();
                if(tvalrow.length)
                    $.each(tvalrow,function(i,v){                        
                        if(txt==v.val||txt.indexOf(v.val)>-1){                         
                           plt.find('li[data-val="'+v.texto+'"]').css({color:'green','text-decoration':'line-through'}); 
                           try{pronunciar(v.texto)}catch(error){console.log(error)}
                        }
                    })
              }
              //var palabra=[];
              var todoscol=plt.find('input.tvalcrusigrama.ok[data-col="'+c+'"]');
              var palabra=new Array();             
              if(todoscol.length>1){
                $.each(todoscol,function(){         
                  var c=$(this).data('row');
                  var v_=$(this).data('val');
                  palabra[c]=v_;
                });
                txt=palabra.join("").toUpperCase();
                if(tvalcol.length)
                    $.each(tvalcol,function(i,v){                       
                        if(txt==v.val||txt.indexOf(v.val)>-1){                           
                            plt.find('li[data-val="'+v.texto+'"]').css({color:'green','text-decoration':'line-through'});
                            try{pronunciar(v.texto)}catch(error){console.log(error)}
                        }                        
                    })
              }      
            }else
              $(this).addClass('error').removeClass('ok');
              
          });
    }
}

