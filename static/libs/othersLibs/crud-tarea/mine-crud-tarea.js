class CrudTarea {
    constructor(params) {
    }
    loadModuloTareas(params) {
        this.section = params.section;
        this.triggers = $(this.section).find(`[name="${params.name}"]`);
        this.addEvents();
    }
    launchCursoCrear(params) {
        this.li = null;
        this.section = document;
        this.mine = new Mine;
    }
    updateSesionPestania(params) {
        let oTarea = this.identify();
        oTarea = Object.assign(oTarea, params);
        this.mine.postData({
            url: _sysUrlBase_ + `json/Mine/updateData`,
            data: {
                params: {
                    arrUpdate: { nombre: oTarea.nombre },
                    tabla: `tareas`,
                    arrFiltros: { idsesion: oTarea.idsesion, idpestania: oTarea.idpestania }
                },
            },
            showloading: false
        });
    }
    identify() {
        let idpestania, idsesion;
        let arrLiParents = this.li.parents('li').get();
        // console.log('arrLiParents', arrLiParents);
        if (arrLiParents.length > 0 && $(arrLiParents[0]).attr('espadre') != "1") {  //es una pestaña
            //hay que saber si fue recien creado
            let id = this.li.attr('id');
            if (id.includes('_')) { //es nuevo
                idpestania = (id.split('_'))[1];
                idsesion = $(arrLiParents[0]).attr('idcursodetalle');
            } else {//ya existía
                idpestania = this.li.attr('data-id');
                idsesion = this.li.attr('idcursodetalle');
            }
            idpestania = `${idsesion}` + `${idpestania}`
        }
        else {
            idpestania = 0;
            idsesion = this.li.attr('idcursodetalle');
        }
        let oTarea = {
            idcomplementario: IDCOMPLEMENTARIO,
            idcurso: idcurso,
            idpestania,
            idsesion
        };
        return oTarea;
    }
    addEvents() {
        this.triggers.on('click', ev => {
            this.uiDelete(ev.target);
        })
    }
    uiDelete(target) {
        let tarea = {
            idpestania: $(target).attr('idpestania'),
            idsesion: $(target).attr('idsesion'),
            idcurso: $(target).attr('idcurso'),
            idcomplementario: $(target).attr('idcomplementario'),
            idgrupoauladetalle: $(target).attr('idgrupoauladetalle')
        }
        swalConfirm({
            action: 'eliminar',
            fx: () => {
                Swal.showLoading();
                this.crudDelete(tarea);
            }
        });
    }
    crudDelete(tarea) {
        postData(_sysUrlBase_ + 'json/tareas/eliminarTodos', tarea).then(rs => {
            Swal.close();
            location.reload();
        }).catch(e => {
            swalError();
            Swal.close();
        })
    }
    crudUpdate(tarea) {
        postData(_sysUrlBase_ + 'json/tareas/eliminarTodos', tarea).then(rs => {
            Swal.close();
            location.reload();
        }).catch(e => {
            swalError();
            Swal.close();
        })
    }
    esTareaProyecto(li) {
        return $(li).children('.btn-group').children('i.btnestarea').hasClass('bg-success');
    }
}