class CrudNotasSmartbook {
    constructor(params = null) {
        this.mine = new Mine;
        this.section = $(document);
        this.debug = false;
        this.webJson = 'json/ModificacionNotas';
        this.selectCursos = this.section.find('#selectCursos');
        this.selectAlumnos = this.section.find('#selectAlumnos');
        this.selectSmartbooks = this.section.find('#selectSmartbooks');
        this.selectTipo = this.section.find('#selectTipo');
        this.table = this.section.find('#table');
        this.md = this.section.find('#md-crudNotas');
        this.mdCrear = this.md.find('div[name="modo-crear"]');
        this.drawTarget = this.section.find('#smartbook-ver');
        this.maxCal = 20;
        this.minCal = 11;
        this.btnSave = this.section.find('#btn-save');
        this.url_SE = null;
        this.oCurrentExamen = null;
        this.url_ADULTOS = __url_ADULTOS;
        this.url_ADOLESCENTES = __url_ADOLESCENTES;
        this.confHabilidades = null;
        this.arrSmartbookAlumno = null;
        // this.currentIdCurso;
        // this.currentIdAlumno;
    }
    launch(params = null) {
        this.addEvents();
        this.loadSelectCursos();
    }
    loadSelectCursos() {
        this.getCursosUsuario().then(arrCursos => {
            if (arrCursos.length > 0) {
                let arrTmp = arrCursos.map((curso) => {
                    curso.id = curso.idgrupoauladetalle;
                    curso.text = curso.nombrecurso;
                    return curso;
                });
                arrCursos = arrTmp;
                this.selectCursos.select2({ data: arrCursos })
                this.selectCursos.on("change.select2", () => {//Añadido evento al cambiar
                    this.clearSelects();
                    let curso = this.selectCursos.select2('data')[0];//obteniendo elemento seleccionado
                    if (curso.formula.hasOwnProperty('formulaActual') && curso.formula.formulaActual.hasOwnProperty('formulatxt')) {
                        this.maxCal = parseFloat(curso.formula.formulaActual.maxcal);
                        this.minCal = parseFloat(curso.formula.formulaActual.mincal);
                    } else {
                        this.formulaNoAsignada();
                    }
                    this.loadSelectAlumnos(curso.arrAlumnos);
                })
                this.selectCursos.trigger('change.select2');
            } else {
                this.setMsg('No tiene cursos asignados!');
            }
        })
    }
    formulaNoAsignada() {
        Swal.fire({
            icon: 'error',//warning (!)//success
            title: 'El curso no tiene fórmula asignada',
            text: "La nota máxima es necesaria para validar la puntuación de cada ejercicio",
            footer: ""
        });
    }
    loadSelectAlumnos(arrAlumnos) {
        if (arrAlumnos.length > 0) {
            let arrTmp = arrAlumnos.map((alumno) => {
                return { id: alumno.idalumno, text: alumno.stralumno }
            });
            arrAlumnos = arrTmp;
            this.selectAlumnos.empty();
            this.selectAlumnos.select2({ data: arrAlumnos })
            this.selectAlumnos.on("change.select2", () => {//Añadido evento al cambiar
                let alumno = this.selectAlumnos.select2('data')[0];//obteniendo elemento seleccionado
                let curso = this.selectCursos.select2('data')[0];//obteniendo elemento seleccionado
                this.getSmartbookCurso(curso).then(arrSmartBooks => {
                    this.loadSelectSmartBooks({
                        arrSmartBooks,
                        idalumno: alumno.id,
                        idcurso: curso.idcurso,
                        idgrupoauladetalle: curso.idgrupoauladetalle
                    });
                })

            })
            this.selectAlumnos.trigger('change.select2');
        } else {
            this.selectAlumnos.select2();
            this.selectSmartbooks.select2();
            this.selectTipo.select2();
            this.setMsg('Aún no hay alumnos matriculados en el curso!');
        }
    }
    loadSelectSmartBooks(params) {
        let arrSmartBooks = params.arrSmartBooks;
        if (arrSmartBooks.length > 0) {
            let arrTmp = arrSmartBooks.map((smartbook) => {
                if (smartbook.tipoMenu == "sesion") {
                    smartbook.text = smartbook.nombre;
                } else {
                    smartbook.text = smartbook.nombrePadre + " - " + smartbook.nombre;
                }
                return smartbook
            });
            arrSmartBooks = arrTmp;
            this.selectSmartbooks.empty();
            this.selectSmartbooks.select2({ data: arrSmartBooks })
            this.selectSmartbooks.on("change.select2", () => {//Añadido evento al cambiar
                this.loadSelectTipo(params);
            })
            this.selectSmartbooks.trigger('change.select2');
        } else {
            this.setMsg('Aún no hay smartbooks en el curso!');
        }
    }
    loadSelectTipo(params) {

        let arrTipo = [
            { id: 2, text: "Practice" }, { id: 3, text: "Do it by yourself" }
        ];

        this.selectTipo.empty();
        this.selectTipo.select2({ data: arrTipo })
        this.selectTipo.on("change.select2", () => {//Añadido evento al cambiar
            let smartbook = this.selectSmartbooks.select2('data')[0];//obteniendo elemento seleccionado
            let tipo = this.selectTipo.select2('data')[0];//obteniendo elemento seleccionado
            let idrecurso = /idrecurso=([^&]+)/.exec(smartbook.link)[1];
            this.url_SE = /PV=([^&]+)/.exec(smartbook.link)[1];
            this.getSmartbookAlumno(params.idcurso, params.idgrupoauladetalle, idrecurso, params.idalumno, tipo.id, this.url_SE).then(rs => {
                this.confHabilidades = rs.confHabilidades;
                console.log('this.confHabilidades', this.confHabilidades);
                this.drawPreguntas(rs.arrPreguntasAlumno);
            });
        })
        this.selectTipo.trigger('change.select2');

    }
    getSmartbookCurso(curso) {
        return new Promise((resolve, reject) => {
            Swal.showLoading();
            this.mine.postData({
                url: _sysUrlBase_ + 'json/Mine/getSesionesFiltradas',
                data: {
                    params: {
                        'idcurso': curso.idcurso,
                        'idcomplementario': curso.idcomplementario,
                        'filter': 'smartenglish'
                    }
                }
            }).then(arrExamenAlumno => {
                Swal.close();
                resolve(arrExamenAlumno);
            })
        })
    }
    notaPorcentaje(nota) {
        return ((nota / this.maxCal) * 100);
    }
    getSmartbookAlumno(idcurso,idgrupoauladetalle, idrecurso, idalumno, tipo, pv) {
        return new Promise((resolve, reject) => {
            Swal.showLoading();
            this.mine.postData({
                url: _sysUrlBase_ + 'json/CrudNotasSmartbook/listarSmartBook',
                data: { idrecurso, idalumno, idcurso, idgrupoauladetalle, tipo, pv }
            }).then(arrSmartbookAlumno => {
                Swal.close();
                resolve(arrSmartbookAlumno);
            })
        })
    }
    drawPreguntas(arrSmartbookAlumno) {
        let $this = this;
        if (arrSmartbookAlumno.length > 0) {
            this.arrSmartbookAlumno = arrSmartbookAlumno;
            this.drawTarget.html('');
            let haypreguntas = false;
            this.arrSmartbookAlumno.forEach((smartbookAlumno, i) => {
                if (smartbookAlumno.html_solucion.includes('plantilla-nlsw')) {
                    $('#PV_SE_URL').remove();
                    let tmpHtml = `<input id="PV_SE_URL" type="hidden" value="${(this.url_SE == 'PVADU_1') ? this.url_ADULTOS : this.url_ADOLESCENTES}">`;
                    $(document).find('body').append(tmpHtml);
                    smartbookAlumno.html_solucion = smartbookAlumno.html_solucion.replace(/__xRUTABASEx__/gi, (this.url_SE == 'PVADU_1') ? this.url_ADULTOS : this.url_ADOLESCENTES);
                    let html = `<div id="card-${i}" data-index="${i}" class="card my-card my-shadow my-mg-bottom">
                                        <div class="card-header" id="head-${i}">
                                            <h5 class="mb-0">
                                                <button id="btn-collapse-${i}" class="btn btn-link" data-toggle="collapse" data-target="#collapse-${i}" aria-expanded="true" aria-controls="collapse-${i}">
                                                    <h3 class="titulo" data-orden="25" style="width: auto;"><strong style="font-size: 1.2em;">${smartbookAlumno.orden}° </strong> ${smartbookAlumno.titulo}</h3>
                                                </button>
                                                <span class="cont-nota my-float-r">
                                                    <label>Nota: </label>
                                                        <small >
                                                            <a class="preg-nota" href="#!" >
                                                                (Sin calificar)
                                                            </a>
                                                        </small>
                                                </span>
                                            </h5>
                                        </div>
                                        <div id="collapse-${i}" class="collapse show" aria-labelledby="headingOne" data-parent="#${this.drawTarget.attr('id')}">
                                            <div class="card-body" data-idactalumno=${smartbookAlumno.idactalumno}>
                                                ${smartbookAlumno.html_solucion}
                                            </div>
                                        </div>
                                    </div>`;
                    html = $(html);
                    if (!(html.find('.msjRespuesta').children().length > 0)) {
                        let textoTextArea = html.find('.respuestaAlumno').find('textarea').html();
                        if (textoTextArea != '') {
                            let tmp = $('<div />').html(textoTextArea).text();
                            html.find('.msjRespuesta').append(tmp);
                        }
                    }
                    if (html.find('.msjRespuesta').children().length > 0) {
                        haypreguntas = true;
                        this.drawTarget.append(html);
                        let card = this.drawTarget.find(`#card-${i}`);
                        let spanNota = card.find('.calificacion-docente').find('span');
                        let nota = card.find('.preg-nota');
                        if (spanNota.length > 0) {
                            nota.html(spanNota.html());
                        }
                        card.find('.cont-nota').find('a').on('click', (e) => {
                            this.showModal(card, nota.html(), i);
                        })
                    }
                }
            });
            if (!haypreguntas) {
                this.setMsg('No hay preguntas para revisar!');
            } else {
                let plantilla = $('.plantilla');
                plantilla.each(function (i, v) {

                    var msjRespuesta = $(v).find('.respuestaAlumno').children('.msjRespuesta');
                    var urlaudio = msjRespuesta.children('audio').attr('src') || '';
                    if (urlaudio != '') {
                        var index1_ = urlaudio.lastIndexOf('static/');
                        if (index1_ > -1) urlaudio = urlaudio.substring(index1_, 10000);
                        urlaudio = _sysUrlBase_ + urlaudio;
                        msjRespuesta.children('audio').attr('src', urlaudio);
                    }
                    $(v).find('.btn.try-again').hide();
                    $(v).find('.btnadd_recordAlumno').hide();
                    $(v).find('.save-progreso').hide();
                })

                $('.collapse').collapse({
                    toggle: true
                });
            }


        } else {
            this.setMsg('Este alumno no dió el smartbook...');
        }
    }
    showModal(card, nota, i) {
        console.log('card', card);
        let form = this.md.find('#form-add-crudNotas');
        let input = form.find('#input-nota');
        if (!isNaN(nota)) {
            input.val(nota);
        }
        form.off("submit");
        form.submit((e) => {
            let txtHabilidades = card.find('.selected-skills').val();
            e.preventDefault();
            let nota = parseFloat(input.val().replace(',', '.'));
            if (nota > this.maxCal) {
                Swal.fire({
                    icon: 'error',//warning (!)//success
                    title: 'Oops...',
                    text: "La nota no puede ser mayor que " + this.maxCal,
                    footer: "Vuelve a intentarlo"
                })
            } else {
                let txtHabilidades = card.find('.selected-skills').val();
                let indexArrSB = card.attr('data-index');
                let htmlHabilidadEvaludada = '';
                if (txtHabilidades != '') {

                    let progresoPrevio = 0;
                    let inputPrevio = card.find('[name="habilidadevaluada"]');
                    console.log('inputPrevio', inputPrevio);
                    if (inputPrevio.length > 0) {
                        progresoPrevio = parseFloat(inputPrevio.val());
                    }
                    console.log('#para restar en caso editen', progresoPrevio)
                    console.log('#index de la pregunta en arrSmartbookAlumno', indexArrSB);
                    // let arrHabilidades = txtHabilidades.split('|');//las habilidades que evalúa el ejercicio
                    // arrHabilidades = [...new Set(arrHabilidades)];//haciéndolos únicos (no repetibles)
                    // console.log('#arr id-habilidades (únicos) que evalúa la pregunta', arrHabilidades);
                    let porcentajeHab = this.notaPorcentaje(nota);
                    console.log('#nota de pregunta en % (maxNota de formula curso)', porcentajeHab);
                    // arrHabilidades.forEach((idhabilidad) => {
                    this.arrSmartbookAlumno[indexArrSB] = this.arrSmartbookAlumno[indexArrSB];
                    // let totalPreguntasHab = this.confHabilidades[idhabilidad];
                    // console.log('#total de preguntas que evalúan la habilidad ' + idhabilidad, totalPreguntasHab);
                    // let notaHabilidad = (porcentajeHab / totalPreguntasHab);
                    let notaHabilidad = porcentajeHab;
                    console.log('#nota que corresponde a la habilidad (%) (para sumar)', notaHabilidad);
                    console.log('#ejercicio del alumno (join actividad_alumno)', this.arrSmartbookAlumno[indexArrSB]);
                    let notaAcumuladaHab = parseFloat(this.arrSmartbookAlumno[indexArrSB].porcentajeprogreso);
                    console.log('#nota antes de sumar', notaAcumuladaHab);
                    notaAcumuladaHab = notaAcumuladaHab - progresoPrevio;
                    console.log('#nota luego de restar en caso de edición', notaAcumuladaHab);
                    this.arrSmartbookAlumno[indexArrSB].porcentajeprogreso = (notaAcumuladaHab + notaHabilidad).toFixed(2);
                    console.log('#porcentaje habilidad actualizada (sumada)', this.arrSmartbookAlumno[indexArrSB].porcentajeprogreso);
                    // });
                    //en caso que editen, recupero lo que sumé, resto y vuelvo a calcular
                    htmlHabilidadEvaludada = `<input name="habilidadevaluada" value="${this.arrSmartbookAlumno[indexArrSB].porcentajeprogreso}" type="hidden">`
                }

                card.find('a.preg-nota').html(nota);
                let h3 = card.find('h3.addtext');
                let span = h3.find('span.calificacion-docente');
                let calificacion = `<i>Tu calificación es: </i>${htmlHabilidadEvaludada}<span style="color:${(nota >= this.minCal) ? '#2ed353' : 'red'}">${nota}</span><span style="color: #2ed353">/${this.maxCal}</span>`;
                if (span.length > 0) {
                    span.html(calificacion);
                } else {
                    h3.append(`<span class="calificacion-docente" style="float:right">${calificacion}</span>`);
                }
                let cardBody = card.find('.card-body');
                let plantilla = cardBody.find('.plantilla');
                plantilla.attr('total-palabras', this.maxCal);
                let txthablado = plantilla.find('.respuestaAlumno')
                txthablado.attr('total-ok', nota);
                let htmlToSave = cardBody.html();
                let idactalumno = cardBody.attr('data-idactalumno');
                this.saveData(idactalumno, htmlToSave, this.arrSmartbookAlumno[indexArrSB]);
                this.md.modal('hide');
            }
        })
        this.mdCrear.removeClass('my-hide');
        this.md.modal('show');
    }
    saveData(idactalumno, htmlToSave, ejercicioAlumno) {
        // Swal.showLoading();
        if (!isNaN(ejercicioAlumno.porcentajeprogreso)) {
            this.mine.postData({
                url: 'json/Mine/updateData',
                data: {
                    params: {
                        tabla: "actividad_alumno",
                        arrUpdate: { html_solucion: htmlToSave, porcentajeprogreso: ejercicioAlumno.porcentajeprogreso },
                        arrFiltros: { idactalumno }
                    }
                }
            }).then(rs => {
                console.log('respuesta al guardar:', rs)
            }).catch(rs => { console.error('Error al guardar:', rs) })
        } else {
            this.formulaNoAsignada();
        }
    }
    getCursosUsuario() {
        return new Promise((resolve, reject) => {
            this.mine.postData({
                url: _sysUrlBase_ + this.webJson
            }).then(arrCursos => {
                resolve(arrCursos);
            })
        })
    }
    setMsg(msg) {
        this.drawTarget.html(`
            <div id="div-msg" class="my-card my-shadow msg my-mg-bottom">
                ${msg}
            </div>
        `);
        this.btnSave.attr('disabled', 'disabled');
    }
    addEvents() {
        let $this = this;
        let btnOpenMd = this.section.find('#open-md-crudNotas');
        btnOpenMd.on('click', () => {
            this.showModal();
        })
        // $("#form-add-submenu").submit(event => {
        //     event.preventDefault();
        //     $this.saveData();
        // });
        this.md.on('hidden.bs.modal', function (e) {
            $this.clearForm(e);
        })
        this.btnSave.on('click', () => {
            this.saveData();
        })
    }
    clearForm(e) {
        let modal = e.currentTarget;
        let arrInput = $(modal).find('input');
        for (let index = 0; index < arrInput.length; index++) {
            arrInput[index].value = '';
            $(arrInput[index]).prop('disabled', false)
        }
        let arrTextarea = $(modal).find('textarea');
        for (let index = 0; index < arrTextarea.length; index++) {
            arrTextarea[index].value = '';
            $(arrTextarea[index]).prop('disabled', false)
        }
        let arrButton = $(modal).find('button');
        for (let index = 0; index < arrButton.length; index++) {
            arrButton[index].value = '';
            $(arrButton[index]).prop('disabled', false)
        }
        $(modal).find('#autor-btn').addClass('my-hide');
        $(modal).find('#default-btn').addClass('my-hide');
        $(modal).find('#new-btn').addClass('my-hide');
    }
    clearSelects() {
        // this.selectCursos.empty();
        this.selectAlumnos.empty();
        this.selectSmartbooks.empty();
        this.selectTipo.empty();
    }
}