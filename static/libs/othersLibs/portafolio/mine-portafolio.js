
function modalVer() {
    $('#md-ver-rubrica').find("#default-btn").removeClass('my-hide');
    $('#md-ver-rubrica').modal('show')
}
function addOnModalClose() {
    let arrModals = $('.my-modal').get()
    arrModals.forEach(modal => {
        $(modal).on('hidden.bs.modal', function (e) {
            clearForm(e);
        })
    });
}
function clearForm(e) {
    console.log('limpiando forms...')
    let modal = e.currentTarget;
    let arrInput = $('.my-modal').find('input');
    for (let index = 0; index < arrInput.length; index++) {
        arrInput[index].value = '';
        $(arrInput[index]).prop('disabled', false)
    }
    let arrTextarea = $('.my-modal').find('textarea');
    for (let index = 0; index < arrTextarea.length; index++) {
        arrTextarea[index].value = '';
        $(arrTextarea[index]).prop('disabled', false)
    }
    let arrButton = $('.my-modal').find('button');
    for (let index = 0; index < arrButton.length; index++) {
        arrButton[index].value = '';
        $(arrButton[index]).prop('disabled', false)
    }
    $(modal).find('#autor-btn').addClass('my-hide');
    $(modal).find('#default-btn').addClass('my-hide');
    $(modal).find('#new-btn').addClass('my-hide');
}
function swalSaved(param = {}) {
    param = {
        title: param.hasOwnProperty('title') ? param.title : null,
        timer: param.hasOwnProperty('timer') ? param.timer : null
    }
    if (!param.timer) {
        return;
    }
    Swal.fire({
        icon: 'success',
        title: param.title,
        showConfirmButton: false,
        timer: param.timer,
    })
}
function swalWrong(param = {}) {
    param = {
        title: param.hasOwnProperty('title') ? param.title : 'Algo salio mal...',
        timer: param.hasOwnProperty('timer') ? param.timer : 600
    }
    if (!param.timer) {
        return;
    }
    Swal.fire({
        icon: 'warning',
        title: param.title,
        showConfirmButton: false,
        timer: param.timer,
    })
}
function swalEliminar() {
    Swal.fire({
        title: 'Estás seguro?',
        text: "Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar!'
    }).then((result) => {
        if (result.value) {
            postData(urlModulo + `/eliminar`, rubrica).then(rpt => {
                if (rpt.code == 200) {
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Algo salió mal...',
                        text: 'Vuelve a intentralo más tarde',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                }
            })

        }
    })
}