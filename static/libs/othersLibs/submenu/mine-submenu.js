class Submenu {
    constructor(params = null) {
        this.section = $(document);
        this.debug = false;
    }
    launchCursoCrear(params = null) {
        this.nestable = this.section.find('#nestable');
        this.md = this.section.find('#md-submenu');
        this.mdCrear = this.md.find('div[name="modo-crear"]');
        this.addEvents();
        this.btnGuardar = this.section.find('#btn-md-guardar');
    }
    addEvents() {
        let $this = this;
        let btnOpenMd = this.section.find('#open-md-submenu');
        btnOpenMd.on('click', () => {
            this.showModal();
        })
        $("#form-add-submenu").submit(event => {
            event.preventDefault();
            // this.btnGuardar=$(this).find;
            if (this.btnGuardar.attr('disabled') == 'disabled') {
                return false;
            }
            this.btnGuardar.attr('disabled', 'disabled');

            $this.saveData();
        });
        this.md.on('hidden.bs.modal', function (e) {
            $this.clearForm(e);
        })
    }
    showModal() {
        this.mdCrear.removeClass('my-hide');
        this.md.modal('show');
    }
    drawLiPadre(liclonado) {

        let liButtons = liclonado.find('span.accionesdecurso');
        let btnIcon = liButtons.find('.btn-group.btn-group-sm');
        console.log('btnIcon', btnIcon);
        let btnHide = liButtons.find('i.btnverenmenu');
        let btnEdit = liButtons.find('i.btneditmenu');
        let btnRmove = liButtons.find('i.btnremovemenu');
        liclonado.find('span.accionesdecurso').html(' ');
        liButtons.append(btnIcon);
        liButtons.append(btnHide);
        liButtons.append(btnEdit);
        liButtons.append(btnRmove);

        let fixbtneditmenu = liclonado.find('.btneditmenu');
        fixbtneditmenu.removeClass('btneditmenu');
        fixbtneditmenu.addClass('btneditmenu');

        let ultimoli = this.nestable.children('ol').find('li').last();
        liclonado.insertBefore(ultimoli);
    }
    saveData() {
        Swal.showLoading();
        let liclonado = this.nestable.find('li.liclone').clone(true);
        liclonado.removeClass('liclone').show();
        liclonado.addClass('borde-padre').show();
        liclonado.attr('orden', this.nestable.find('li').length);
        let nombre = this.md.find('#input-nombre-submenu').val();
        let objSave = {
            espadre: 1,
            idcurso,
            icc: IDCOMPLEMENTARIO,
            nombre,
            idcursodetalle: 0,
            idrecursopadre: 0,
            idrecursoorden: 0,
            orden: parseInt(liclonado.attr('orden')),
            idrecurso: 0,
            tiporecurso: 'M',
            idlogro: 0,
            url: '',
            color: '',//rgba(32, 50, 69, 0)
            esfinal: 0,
            descripcion: '',
            imagen: '',
            accimagen: 'sg',
        }
        postData(_sysUrlBase_ + 'smart/acad_cursodetalle/guardarMenu', objSave).then(rs => {
            let dt = rs.newid;
            liclonado.children('span.titulo').text(nombre);
            liclonado.attr('idcursodetalle', dt.idcursodetalle);
            liclonado.data('idcursodetalle', dt.idcursodetalle);
            // liclonado.attr('data-idcursodetalle', dt.idcursodetalle);
            liclonado.attr('idnivel', dt.idnivel);
            liclonado.attr('idpadre', dt.idpadre);
            // liclonado.attr('data-id', dt.orden);
            liclonado.data('id', dt.orden);
            liclonado.attr('ordennivel', dt.ordennivel);
            liclonado.attr('espadre', dt.espadre);
            this.drawLiPadre(liclonado);
            Swal.close();
            this.md.modal('hide');
            this.btnGuardar.removeAttr('disabled');
        }).catch(e => {
            swalError();
            console.warn(e);
        })
    }
    launchInicioVer(params = null) {
        this.drawFunction = () => { };
    }
    clearForm(e) {
        let modal = e.currentTarget;
        let arrInput = $(modal).find('input');
        for (let index = 0; index < arrInput.length; index++) {
            arrInput[index].value = '';
            $(arrInput[index]).prop('disabled', false)
        }
        let arrTextarea = $(modal).find('textarea');
        for (let index = 0; index < arrTextarea.length; index++) {
            arrTextarea[index].value = '';
            $(arrTextarea[index]).prop('disabled', false)
        }
        let arrButton = $(modal).find('button');
        for (let index = 0; index < arrButton.length; index++) {
            arrButton[index].value = '';
            $(arrButton[index]).prop('disabled', false)
        }
        $(modal).find('#autor-btn').addClass('my-hide');
        $(modal).find('#default-btn').addClass('my-hide');
        $(modal).find('#new-btn').addClass('my-hide');
    }
    log(msg) {
        if (this.debug) {
            console.log(msg)
        }
    }
}