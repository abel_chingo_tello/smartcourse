class ReporteNotasAdmin {
    constructor() {
        this.__arrCursos = [];
        // this.load();
    }
    load() {
        postData(_sysUrlBase_ + `json/reportes/getAlumnosAdmin`).then(arrAlumnosAdmin => {
            if (arrAlumnosAdmin.length > 0) {
                let arrTmp = arrAlumnosAdmin.map((alum) => {
                    return { id: alum.idalumno, text: alum.stralumno }
                });
                arrAlumnosAdmin = arrTmp; //array de alumnos

                const selectAlumnos = $('#selectAlumnos');
                selectAlumnos.select2({ data: arrAlumnosAdmin })

                selectAlumnos.on("change.select2", () => {          //Añadido evento al cambiar
                    let alumnSelect = selectAlumnos.select2('data')[0];   //obteniendo elemento seleccionado
                    console.log('id', alumnSelect.id, 'nombre', alumnSelect.text);
                    this.getCursoAlumno(alumnSelect.id);
                })
                selectAlumnos.trigger('change.select2');
            } else {
                $('#tb-msg').html('No tiene cursos asignados!');
            }
        })
        $.extend($.fn.dataTable.defaults, {
            responsive: true
        });
    }
    getCursoAlumno(idAlum) {
        if ($.fn.DataTable.isDataTable('#my-table')) {
            $('#my-table').DataTable().destroy();
        }
        postData(_sysUrlBase_ + "json/reportes/getCursosAlumno", { idAlunmo: idAlum }).then(arrCursos => {
            this.__arrCursos = arrCursos;
            //console.log('arrCursosMy:', arrCursos);
            if (arrCursos.length > 0) {
                let htmlTh = `<th class="nombre-alumno  thead-border">Curso</th>`;
                let htmlTd = `<td></td>`;
                let htmlHead = `<tr>${htmlTh}<th class="notas  thead-border my-tooltip" ><div>Promedio<br>final</div></th></tr>`;
                let htmlRow = `<tr>${htmlTd}<td></td></tr>`;

                $('#tb-head').html(htmlHead);
                $('#tbody').html(htmlRow);
                $('#my-table').DataTable({
                    responsive: true,
                    targets: [0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric',
                    searching: false,
                    ordering: false,
                    paging: false,
                    info: false,
                    bLengthChange: false,
                    language: {
                        search: "Buscar:",
                        lengthMenu: "Mostrando las &nbsp _MENU_ &nbsp primeras publicaciones",
                        info: "Mostrando _START_ a _END_ publicaciones de un total de _TOTAL_ ",
                        paginate: {
                            first: "Primera página",
                            previous: "Anterior",
                            next: "Siguiente",
                            last: "Última página"
                        }
                    }
                });
                this.drawTable(arrCursos);
            } else {
                $('#tb-msg').html('Aún no tiene cursos asignados!');
            }
        })
    }

    drawTable(arrCursos = []) {
        if ($.fn.DataTable.isDataTable('#my-table')) {
            $('#my-table').DataTable().destroy();
        }

        let tbody = $('#tbody');

        let htmlRows = `<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">Aún no tiene cursos asignados!</td></tr>`;
        if (arrCursos.length > 0) {
            htmlRows = ``;
            let htmlTd = ``;
            arrCursos.forEach(alumno => {
                htmlTd += `<td><div class="nombre-alumno">${alumno.nombrecurso.trim().toLowerCase()} "  (" ${alumno.strgrupoaula.trim().toLowerCase()}")"</div></td>`
                //let formulatmp=alumno.arrCursos_nota[0].formulaActual.notas;
                let formulaNotas=alumno.arrCursos_nota[0].formulaActual.notas;                
                let formulaNotastmp=[];
                formulaNotas.forEach(function(v,i){
                    if(v.borrado==false){
                        let formulatmp=[];
                        v.formula.forEach(function(vv,ii){
                            if(vv.borrado==false) formulatmp.push(vv);
                        })
                        v.formula=formulatmp;
                        formulaNotastmp.push(v)
                    }
                 })

                let nota = this.getNota(formulaNotastmp, alumno.arrCursos_nota[0].temasaevaluar);
                if (alumno.arrCursos_nota[0].formulaActual.notas.length > 0) {
                    htmlRows += `<tr>${htmlTd}<td class="my-text-center"><span class=" ${parseInt(nota) >= parseInt(alumno.arrCursos_nota[0].formulaActual.mincal) ? 'aprobado' : 'desaprobado'}">${nota}</span></td></tr>`;
                } else {
                    htmlRows += `<tr>${htmlTd}<td class="my-text-center"  ><span class=" ">--</span></td></tr>`;
                }

                htmlTd = ``;
            });

        }

        tbody.html(htmlRows);
        $('#my-table').DataTable({
            responsive: true,
            // colReorder: true,
            // order: [[ 0, "asc" ]],
            // ordering: true,
            dom: 'Bfrtip',
            targets: [0, 1, 2],
            className: 'mdl-data-table__cell--non-numeric',
            searching: false,
            ordering: true,
            paging: false,
            info: false,
            buttons: [
                /*'copy', 'csv',*/
                {
                    extend: 'excel',
                    className: 'btn-dt btn-excel',
                    text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>'
                }, {
                    extend: 'pdf',
                    className: 'btn-dt btn-pdf',
                    text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>'
                }, {
                    extend: 'print',
                    className: 'btn-dt btn-print',
                    text: '<i class="fa fa-print" aria-hidden="true"></i>'
                }
            ],
            bLengthChange: false,
            language: {
                search: "Buscar:",
                lengthMenu: "Mostrando las &nbsp _MENU_ &nbsp primeras publicaciones",
                info: "Mostrando _START_ a _END_ publicaciones de un total de _TOTAL_ ",
                paginate: {
                    first: "Primera página",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Última página"
                }
            }
        });
    }
    getNota(arrNotas, arrTemas) {
        //console.log('allnotas',arrNotas);
        //console.log(arrTemas);
        let promedioFinal = 0;
        let peso = 1 / arrNotas.length;
        let notaGrupo;
        let arrLabelStr = []; let arrData = []; let arrNotasStr = []; let arrNotasNum = []; let arrLabelNum = [];
        arrNotas.forEach(objNotaGrupo => { //primer nivel (array grupo de notas) (notas)
            if (objNotaGrupo.tipo == "%") {
                peso = parseInt(objNotaGrupo.valortipo) / 100;
            }
            notaGrupo = 0;
            objNotaGrupo.formula.forEach(formula_tema => {//2do nivel (formula)
                let arrTema = arrTemas.filter((tema) => {
                    if (tema.tipo == formula_tema.typerecurso && tema.id == formula_tema.id) {
                        return tema;
                    }
                })
                let tema_nota = arrTema[0];
                let _peso = 0;
                if (formula_tema.tipo == '%') {
                    _peso = parseFloat(formula_tema.valortipo) / 100;
                } else {
                    _peso = 1 / (objNotaGrupo.formula.length);
                }
                if (tema_nota == undefined) {
                    Swal.fire({
                        icon: 'warning',
                        title: '¡Hay un error en la fórmula del curso!',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        closeOnClickOutside: false
                    });
                    return 0;
                }
                if (tema_nota.hasOwnProperty('nota') && tema_nota.nota != null && !isNaN(tema_nota.nota)) {
                    let tmp_nota = tema_nota.nota * _peso;
                    notaGrupo += tmp_nota;
                    arrNotasStr.push(tmp_nota);
                    arrNotasNum.push(tema_nota.nota);
                    arrLabelNum.push(tema_nota.prefijo + ` (${tema_nota.nombreunico})`)
                } else {
                    arrNotasStr.push(tema_nota.nota)
                }
                // arrLabelStr.push(tema_nota.prefijo + ` (${tema_nota.nombreunico})`)
                arrLabelStr.push(tema_nota.prefijo)
                arrData.push(_peso * peso)//graficos chart
            });
           /* objNotaGrupo.formula.forEach(formula_tema => {
            //console.log(formula_tema);

                // switch (formula_tema.typerecurso) {
                //     case 'estarea' || 'esproyecto':
                //console.log('a1',arrTemas);
                let arrTema = arrTemas.filter((tema) => {
                    //console.log(tema,tema.tipo,formula_tema.typerecurso,tema.id,formula_tema.id);
                    if (tema.tipo == formula_tema.typerecurso && tema.id == formula_tema.id){
                        console.log('tema',tema);
                        return tema;
                    }
                })
                //console.log('a2',arrTema);
                let tema_nota = arrTema[0];
                let _peso = 0;
                //console.log('tema_nota',tema_nota);
                //if(tema_nota.borrado==false){
                if (formula_tema.tipo == '%') {
                    _peso = parseFloat(formula_tema.valortipo) / 100;
                } else {
                    _peso = 1 / (objNotaGrupo.formula.length);
                }
                if (tema_nota.hasOwnProperty('nota') && tema_nota.nota != null && !isNaN(tema_nota.nota)) {
                    let tmp_nota = tema_nota.nota * _peso;
                    notaGrupo += tmp_nota;
                    arrNotasStr.push(tmp_nota);
                    arrNotasNum.push(tema_nota.nota);
                    arrLabelNum.push(tema_nota.prefijo + ` (${tema_nota.nombreunico})`)
                } else {
                    arrNotasStr.push(tema_nota.nota)
                }

                //console.log(tema_nota.prefijo + ` (${tema_nota.nombreunico})`+tema_nota.nota,tema_nota);

                arrLabelStr.push(tema_nota.prefijo + ` (${tema_nota.nombreunico})`)
                arrData.push(_peso * peso)
                //}
                //     break;
                // case 'smartquiz':
                //     break;
                // default:
                //     break;
                // }
            });*/
            //console.log('notaGrupoMy', notaGrupo);
            promedioFinal += notaGrupo * peso;            
            //console.log('promedio final',promedioFinal);
        });
        if (__usuario.idrol == 3) {
            if (arrNotas.length > 0) {
                var myChart = new Chart(document.getElementById('myChart').getContext('2d'), {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                            label: '# of Votes',
                            backgroundColor: [
                                '#36a2eb',
                                '#4bc0c0',
                                '#ffcd56',
                                '#ff6384',
                                '#ff9f40',
                                // 'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'white'
                            ],
                            borderWidth: 1
                        }]
                    },
                });
                myChart.data.labels = arrLabelStr;
                myChart.data.datasets[0].data = arrData;
                myChart.options.tooltips.callbacks.label = function (tooltipItem, data, arrNotas = arrNotasStr) {
                    let label;
                    if (arrNotas[tooltipItem.index] != null) {
                        label = "Tu nota: " + arrNotas[tooltipItem.index];
                    } else {
                        label = 'no presentado';
                    }
                    label += ` (${data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] * 100}%)`;
                    return label;
                }
                myChart.update();
                //
                var myChart2 = new Chart(document.getElementById('myChart2').getContext('2d'), {
                    type: 'line',
                    fill: 'start',
                    data: {
                        datasets: [{
                            label: 'Mis notas',
                            backgroundColor: [
                                'rgba(255, 111, 141, 0.26)',
                            ],
                            borderColor: [
                                '#ff6384',
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        scales: {
                            yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    max: parseInt(this.__arrCursos[0].arrCursos_nota[0].formulaActual.maxcal),
                                    min: 0
                                }
                            }]
                        }
                    }
                });
                myChart2.data.labels = arrLabelNum;
                myChart2.data.datasets[0].data = arrNotasNum;
                myChart2.update();
                $(document).find('.graf-cont').show();
            } else {
                $(document).find('.graf-cont').hide();
            }

        }

        return promedioFinal.toFixed(2);
    }
}