class ReporteNota {
    chartDoughnut = null;
    chartLine = null;
    str_sincursos = 'You have no courses currently assigned';
    str_espere = 'Wait a moment please';
    str_yourscore = 'You Score';
    str_notpresented = 'Not Presented';
    str_Reportnotes = 'Report of notes';
    str_There_are_no_students_enrolled_in_the_course_yet = 'There are no students enrolled in the course yet';
    str_All = 'All';
    str_student = 'Student';
    str_Not_included_in_the_formula = 'Not included in the formula';
    str_notes = 'Notes';
    str_my_notes = 'My Notes';
    str_finalaverage = 'Final average';
    str_search = 'Search';
    str_There_is_an_error_in_the_course_formula = 'There is an error in the course formula';
    str_approved = 'Approved';
    str_disapproved = 'Disapproved';
    str_The_Alphabetic_grade_setup_in_this_course_is_not_complete = 'str_The_Alphabetic_grade_setup_in_this_course_is_not_complete';
    str_first_page = 'First page';
    str_previous = 'Previous';
    str_next = 'Next';
    str_last_page = 'Last page';
    str_Showing_the_MENU_first_posts = 'Showing the & nbsp _MENU_ & nbsp first posts';
    str_Showing_START_to_END_posts_out_of_a_total_of_TOTAL_ = 'Showing _START_ to _END_ posts out of a total of _TOTAL_';
    constructor() {
        this.__arrAlumnos = [];
        this.__arrColumnsNotas = []
        this.__arrCharts = {}
        this.idnumgrafico = 0
        this.ChartColor = Chart.helpers.color
        this.vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
        // this.load();
    }
    load() {
        postData(_sysUrlBase_ + `json/reportes/getCursosUsuario`).then(arrCursos => {
            console.log('arrCursos: ', arrCursos)
            if (arrCursos.length > 0) {
                if (__usuario.idrol == 1) {
                    let arrGrupos = arrCursos.filter(grupo => {
                        if (grupo.arrCursos.length > 0) {
                            return grupo;
                        }
                    });
                    let arrTmp = arrGrupos.map((grupo) => {
                        return { id: grupo.idgrupoaula, text: grupo.nombre, arrCursos: grupo.arrCursos }
                    });
                    arrGrupos = arrTmp;
                    let selectGrupos = $('#selectGrupos');
                    selectGrupos.select2({ data: arrGrupos })
                    selectGrupos.on("change.select2", () => { //Añadido evento al cambiar
                        let grupo = selectGrupos.select2('data')[0]; //obteniendo elemento seleccionado
                        console.log('grupo', grupo);
                        arrCursos = grupo.arrCursos;
                        arrTmp = arrCursos.map((curso) => {
                            return { id: curso.idgrupoauladetalle, text: curso.nombrecurso + ' - ' + curso.strgrupoaula, arrAlumnos: curso.arrAlumnos }
                        });
                        arrCursos = arrTmp;
                        let selectCursos = $('#selectCursos');
                        selectCursos.empty();
                        selectCursos.select2({ data: arrCursos })
                        selectCursos.on("change.select2", () => { //Añadido evento al cambiar
                            let curso = selectCursos.select2('data')[0]; //obteniendo elemento seleccionado
                            // console.log('curso: ', curso);
                            $('#selectCursos').attr('disabled', 'disabled');
                            this.getCurso(curso)
                        })
                        selectCursos.trigger('change.select2');
                    })
                    selectGrupos.trigger('change.select2');
                } else {
                    this.loadSelectCursos(arrCursos);
                }
            } else {
                this.setMsg(this.str_sincursos + '!');
                $('#selectCursos').removeAttr('disabled');
            }
        })
        $.extend($.fn.dataTable.defaults, {
            responsive: true
        });
        if ($("#btnVistasContainer").length > 0) {
            this.mostrarVista()
        }
    }
    setTitle(strCurso) {
        document.title = this.str_Reportnotes + ' - ' + strCurso;
    }
    setMsg(msg) {
        if ($.fn.DataTable.isDataTable('#my-table')) {
            $('#my-table').DataTable().destroy();
        }
        let html = `<tr class="odd">
                        <td id="tb-msg" valign="top" colspan="4" class="dataTables_empty">${msg}</td>
                    </tr>`;
        $('#tb-head').html('');
        $('#tbody').html(html);
    }
    loadSelectCursos(arrCursos) {
        let arrTmp = arrCursos.map((curso) => {
            return { id: curso.idgrupoauladetalle, text: curso.nombrecurso + ' - ' + curso.strgrupoaula, arrAlumnos: curso.arrAlumnos }
        });
        arrCursos = arrTmp;
        let selectCursos = $('#selectCursos');
        selectCursos.select2({ data: arrCursos })
        selectCursos.on("change.select2", () => { //Añadido evento al cambiar
            let curso = selectCursos.select2('data')[0]; //obteniendo elemento seleccionado
            $('#selectCursos').attr('disabled', 'disabled');
            this.getCurso(curso)
        })
        selectCursos.trigger('change.select2');
    }
    getCurso(curso) {
        this.setTitle(curso.text);
        let _this = this;
        Swal.fire({
            title: _this.str_espere,
            //  text:"",
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        })
        console.log('getCurso');
        let idalumno = '';
        if (__usuario.idrol == 3) {
            idalumno = __usuario.idpersona;
        }
        postData(_sysUrlBase_ + "json/reportes/notasFormula", { idgrupoauladetalle: curso.id, idalumno }).then((arrAlumnos) => {
            this.__arrAlumnos = arrAlumnos;
            console.log('arrAlumnos', arrAlumnos);
            if (arrAlumnos.length > 0) {
                this.loadSelectGrupoNota(arrAlumnos[0].arrCursos_nota[0].formulaActual.notas) //solito se debe disparar

            } else {
                this.setMsg(_this.str_There_are_no_students_enrolled_in_the_course_yet + '!');
                $('#selectCursos').removeAttr('disabled');
            }
            Swal.close();
        })
    }
    loadSelectGrupoNota(arrGrupoNota) {
        let arrData = arrGrupoNota.slice();
        let optTodos = { id: -1, text: str_All };
        let arrTmp = arrData.map((grupo, index) => {
            return { id: index, text: grupo.nombre }
        })
        arrData = arrTmp;
        arrData.unshift(optTodos);
        let selectGrupoNota = $('#selectGrupoNota');
        selectGrupoNota.empty();
        selectGrupoNota.select2({ data: arrData })
        selectGrupoNota.on("change.select2", () => { //Añadido evento al cambiar
            let grupoNota = selectGrupoNota.select2('data')[0]; //obteniendo elemento seleccionado
            let index = parseInt(grupoNota.id);
            this.render(this.__arrAlumnos, index == -1 ? null : index); //momentaneo
        });
        selectGrupoNota.trigger('change.select2');
    }
    render(arrAlumnos, index = null) {
        console.log('render');
        if ($.fn.DataTable.isDataTable('#my-table')) {
            $('#my-table').DataTable().destroy();
        }
        let arrTemasFormula;
        let arrTemasaevaluar = arrAlumnos[0].arrCursos_nota[0].temasaevaluar;
        if (index != null) {
            arrTemasFormula = [arrAlumnos[0].arrCursos_nota[0].formulaActual.notas[index]]; //solo un bimestre
            //recortar temas
            //se supone que es solo un bimestre
            let arrTmp = [];
            arrTemasFormula[0].formula.forEach(temaFormula => {
                let arrTema = arrTemasaevaluar.filter((tema) => {
                        if (tema.tipo == temaFormula.typerecurso && tema.id == temaFormula.id) {
                            return tema;
                        }
                    })
                    //se supone que solo me regresa 1
                arrTmp.push(arrTema[0]);
            });
            arrTemasaevaluar = arrTmp; //hasta aqui he cortado de todoslos temas a mostrar
        } else {
            arrTemasFormula = arrAlumnos[0].arrCursos_nota[0].formulaActual.notas; //todos los bimestres
        }
        let formulatxt = arrAlumnos[0].arrCursos_nota[0].formulaActual.formulatxt;
        this.__arrColumnsNotas = []
        this.drawTableHead(formulatxt, arrTemasFormula, arrTemasaevaluar);
        this.drawTableBody(arrAlumnos, arrTemasFormula, index == null ? 0 : 1);
        if (__usuario.idrol == 3) {
            this.drawChart()
        }
    }
    drawTableHead(formulatxt, arrTemasFormula = [], arrTemasaevaluar = [] /*son los mismos temas para todos los alumnos */ ) {
        console.log('drawTableHead');
        let htmlTh0 = `<th class="nombre-alumno  thead-border"></th>`;
        let htmlTh1 = `<th class="nombre-alumno  thead-border">${this.str_student}</th>`;
        let htmlTd = `<td></td>`;
        let strGrupoActual = '';
        let arrContador = [];

        arrTemasaevaluar.forEach(tema => {
            let done = false;
            for (let i = 0; i < arrTemasFormula.length; i++) {
                const nota = arrTemasFormula[i];
                let arrTmp = nota.formula.filter(itemGrupo => {
                    if (itemGrupo.typerecurso == tema.tipo && itemGrupo.id == tema.id) {
                        return itemGrupo;
                    }
                })
                if (arrTmp.length > 0) {
                    strGrupoActual = nota.nombre;
                    tema.grupo = nota.nombre;
                    if (!arrContador.hasOwnProperty(nota.nombre)) {
                        arrContador[nota.nombre] = 1;
                    } else {
                        arrContador[nota.nombre]++;
                    }
                    done = true;
                } else {
                    tema.grupo = "(" + str_Not_included_in_the_formula + ")";
                }
                if (done) {
                    break;
                }
            }
            this.__arrColumnsNotas.push({
                    id: tema.id,
                    nombre: this.defineName(tema).nombre_mostrar,
                    tipo_nombre: this.defineKind(tema.tipo),
                    tipo: tema.tipo
                })
                //INSERTAR AQUI EL TIPO DE NOTAS
                //FALTA POR REALIZAR
            htmlTh1 += ` <th class="notas  thead-border">${this.defineName(tema).nombre_mostrar}<br>
                        <small><i>(${this.defineKind(tema.tipo)})</i></small>
                    </th>`
            htmlTd += `<td></td>`
        });
        for (let key of Object.keys(arrContador)) {
            htmlTh0 += `<th class="notas nombregrupo" colspan="${arrContador[key]}"  thead-border">${key}</th>`
        }
        let htmlHead = `<tr>${htmlTh0}<th></th></tr>`;

        // htmlHead += `<tr>${htmlTh1}<th class="notas  thead-border my-tooltip" ><div>Promedio<br>final</div> <span class="my-tooltiptext my-tooltip-left f-remove">${formulatxt}</span></th></tr>`;
        htmlHead += `<tr>${htmlTh1}<th class="notas  thead-border my-tooltip" ><div>${this.str_finalaverage}</div> </th></tr>`;
        let htmlRow = `<tr>${htmlTd}<td></td></tr>`;

        $('#tb-head').html(htmlHead);
        $('#tbody').html(htmlRow);
        // console.log('respinsive:', this.vw < 500);
        let _this = this;
        $('#my-table').DataTable({
            responsive: this.vw < 500,
            targets: [0, 1, 2],
            className: 'mdl-data-table__cell--non-numeric',
            searching: false,
            ordering: false,
            paging: false,
            info: false,
            bLengthChange: false,
            language: {
                search: _this.str_search + ":",
                lengthMenu: _this.str_Showing_the_MENU_first_posts,
                info: _this.str_Showing_START_to_END_posts_out_of_a_total_of_TOTAL_,
                paginate: {
                    first: _this.str_first_page,
                    previous: _this.str_previous,
                    next: _this.str_next,
                    last: _this.str_last_page
                }
            }
        });
    }
    drawTableBody(arrAlumnos = [], arrTemasFormula = [], modo = 0) {
        //console.log('drawTableBody');
        if ($.fn.DataTable.isDataTable('#my-table')) {
            $('#my-table').DataTable().destroy();
        }
        let _this = this;
        let tbody = $('#tbody');
        let htmlRows = `<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">${_this.str_There_are_no_students_enrolled_in_the_course_yet}!</td></tr>`;
        if (arrAlumnos.length > 0) {
            htmlRows = ``;
            let htmlTd = ``;
            arrAlumnos.forEach(alumno => {
                let arrTemasaevaluar = []; //NOTA DISTINTA POR ALUMNO, todos los temas (nombre de la propiedad)
                arrTemasaevaluar = alumno.arrCursos_nota[0].temasaevaluar;
                if (modo == 1) { //se supone que es solo un bimestre
                    let arrTmp = [];
                    arrTemasFormula[0].formula.forEach(temaFormula => {
                        let arrTema = arrTemasaevaluar.filter((tema) => {
                                if (tema.tipo == temaFormula.typerecurso && tema.id == temaFormula.id) {
                                    return tema;
                                }
                            })
                            //se supone que solo me regresa 1
                        arrTmp.push(arrTema[0]);
                    });
                    arrTemasaevaluar = arrTmp; //hasta aqui he cortado de todoslos temas a mostrar
                }
                htmlTd += `<td><div class="nombre-alumno">${alumno.stralumno.trim().toLowerCase()}</div></td>`
                arrTemasaevaluar.forEach(tema => { //dibuja todos los temas con nota (2do nivel) POR ALUMNO
                    const index = this.__arrColumnsNotas.findIndex(e => e.id == tema.id)
                    this.__arrColumnsNotas[index].nota = index != -1 ? tema.nota : 0
                    htmlTd += `<td class="my-text-center">${tema.nota == null ? '<small><i>('+_this.str_notpresented+')</i></smal>' : tema.nota}</td>`;
                });
                let nota;
                nota = this.getNota(arrTemasFormula, arrTemasaevaluar, modo);
                let notaAlfabetica = null;
                if (this.__arrAlumnos[0].arrCursos_nota[0].formulaActual.tipocalificacion == "A") {
                    if (this.__arrAlumnos[0].arrCursos_nota[0].formulaActual.puntuacion.length > 0) {
                        let arrAlfabetica = this.__arrAlumnos[0].arrCursos_nota[0].formulaActual.puntuacion;
                        for (let i = 0; i < arrAlfabetica.length; i++) {
                            const objAlfabetica = arrAlfabetica[i];
                            if (parseInt(nota) <= objAlfabetica.maxcal && parseInt(nota) >= objAlfabetica.mincal) {
                                notaAlfabetica = objAlfabetica.nombre;
                                break;
                            }
                        }
                    } else {
                        Swal.fire({
                            icon: 'warning',
                            title: '¡' + _this.str_The_Alphabetic_grade_setup_in_this_course_is_not_complete + '!',
                            showConfirmButton: true,
                        })
                    }
                }
                if (arrTemasFormula.length > 0) { //sí hay formula
                    htmlRows += `<tr>
                                    ${htmlTd}
                                    <td class="my-text-center" >
                                        <span class="${(notaAlfabetica == null) ? ('') : ('my-tooltip')} ${parseInt(nota) >= parseInt(this.__arrAlumnos[0].arrCursos_nota[0].formulaActual.mincal) ? _this.str_approved : _this.str_disapproved}">
                                            ${(notaAlfabetica == null) ? (nota) : (notaAlfabetica + '<span class="my-tooltiptext my-tooltip-left f-remove">  (' + nota + ')</span>')}
                                        </span>
                                    </td>
                                </tr>`;
                } else { //no hay formula
                    htmlRows += `<tr>${htmlTd}<td class="my-text-center"  ><span class=" ">--</span></td></tr>`;
                }
                htmlTd = ``;
            });
        } else {
            // $('#tb-head').find('tr').html(`<th></th>`);
        }
        tbody.html(htmlRows);

        $('#my-table').DataTable({
            responsive: this.vw < 500,
            // colReorder: true,
            // order: [[ 0, "asc" ]],
            // ordering: true,
            dom: 'Bfrtip',
            targets: [0, 1, 2],
            className: 'mdl-data-table__cell--non-numeric',
            searching: false,
            ordering: true,
            paging: false,
            info: false,
            buttons: [
                /*'copy', 'csv',*/
                {
                    extend: 'excel',
                    className: 'btn-dt btn-excel',
                    text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>'
                }, {
                    extend: 'pdf',
                    className: 'btn-dt btn-pdf',
                    text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
                    customize: function(win) {
                        console.log('pdfDoc', win);
                    }
                }, {
                    extend: 'print',
                    className: 'btn-dt btn-print',
                    text: '<i class="fa fa-print" aria-hidden="true"></i>'
                }
            ],
            bLengthChange: false,
            language: {
                search: _this.str_search + ":",
                lengthMenu: _this.str_Showing_the_MENU_first_posts,
                info: _this.str_Showing_START_to_END_posts_out_of_a_total_of_TOTAL_,
                paginate: {
                    first: _this.str_first_page,
                    previous: _this.str_previous,
                    next: _this.str_next,
                    last: _this.str_last_page
                }
            }
        });
        $('#selectCursos').removeAttr('disabled');
    }
    defineKind(tipo) {
        let kind = '';
        switch (tipo) {
            case 'smartquiz':
                kind = "examen";
                break;
            case 'estarea':
                kind = "tarea";
                break;
            case 'esproyecto':
                kind = "proyecto";
                break;
            case 'wiki':
                kind = 'wiki';
            case 'foro':
                kind = 'foro';
            default:
                break;
        }
        return kind;
    }
    defineName(tema) {
        if (tema.hasOwnProperty('prefijo')) {
            tema.nombre_mostrar = tema.prefijo;
        } else {
            tema.nombre_mostrar = tema.nombreunico;
        }
        return tema;
    }
    getNota(arrNotas, arrTemas, modo = 0) {
        let promedioFinal = 0;
        let peso = 1 / (modo == 1 ? 1 : arrNotas.length);
        let notaGrupo;
        let arrLabelStr = [];
        let arrData = [];
        let arrNotasStr = [];
        let arrNotasNum = [];
        let arrLabelNum = [];
        let _this = this;
        arrNotas.forEach(objNotaGrupo => { //primer nivel (array grupo de notas) (notas)
            if (modo != 1 && objNotaGrupo.tipo == "%") {
                peso = parseInt(objNotaGrupo.valortipo) / 100;
            }
            notaGrupo = 0;
            objNotaGrupo.formula.forEach(formula_tema => { //2do nivel (formula)
                let arrTema = arrTemas.filter((tema) => {
                    if (tema.tipo == formula_tema.typerecurso && tema.id == formula_tema.id) {
                        return tema;
                    }
                })
                let tema_nota = arrTema[0];
                let _peso = 0;
                if (formula_tema.tipo == '%') {
                    _peso = parseFloat(formula_tema.valortipo) / 100;
                } else {
                    _peso = 1 / (objNotaGrupo.formula.length);
                }
                if (tema_nota == undefined) {
                    Swal.fire({
                        icon: 'warning',
                        title: '¡' + _this.str_There_is_an_error_in_the_course_formula + '!',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        closeOnClickOutside: false
                    });
                    return 0;
                }
                if (tema_nota.hasOwnProperty('nota') && tema_nota.nota != null && !isNaN(tema_nota.nota)) {
                    let tmp_nota = tema_nota.nota * _peso;
                    notaGrupo += tmp_nota;
                    arrNotasStr.push(tmp_nota);
                    arrNotasNum.push(tema_nota.nota);
                    arrLabelNum.push(tema_nota.prefijo + ` (${tema_nota.nombreunico})`)
                } else {
                    arrNotasStr.push(tema_nota.nota)
                }
                // arrLabelStr.push(tema_nota.prefijo + ` (${tema_nota.nombreunico})`)
                arrLabelStr.push(tema_nota.prefijo)
                arrData.push(_peso * peso) //graficos chart
            });
            promedioFinal += notaGrupo * peso;
        });
        if (__usuario.idrol == 3) {
            if (arrNotas.length > 0) {
                if (this.chartDoughnut != null) {
                    this.chartDoughnut.destroy();
                }
                if (this.chartLine != null) {
                    this.chartLine.destroy();
                }
                this.chartDoughnut = new Chart(document.getElementById('myChart').getContext('2d'), {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                            label: '# of Votes',
                            backgroundColor: [
                                '#36a2eb',
                                '#4bc0c0',
                                '#ffcd56',
                                '#ff6384',
                                '#ff9f40',
                                // 'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'white'
                            ],
                            borderWidth: 1
                        }]
                    },
                });

                this.chartDoughnut.data.labels = arrLabelStr;
                this.chartDoughnut.data.datasets[0].data = arrData;
                this.chartDoughnut.options.tooltips.callbacks.label = (tooltipItem, data, arrNotas = arrNotasStr, arrLabelStrs = arrLabelStr) => {
                    let label;
                    if (arrNotas[tooltipItem.index] != null) {
                        let notaNum = parseFloat(arrNotas[tooltipItem.index]);
                        label = `(${arrLabelStrs[tooltipItem.index]}) ` + this.str_yourscore + notaNum.toFixed(2);
                    } else {
                        label = `(${arrLabelStrs[tooltipItem.index]}) ` + this.str_notpresented;
                    }
                    let porcentajeNum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] * 100;
                    label += ` (${porcentajeNum.toFixed(2)}%)`;
                    return label;
                }
                this.chartDoughnut.update();
                //
                this.chartLine = new Chart(document.getElementById('myChart2').getContext('2d'), {
                    type: 'line',
                    fill: 'start',
                    data: {
                        datasets: [{
                            label: _this.str_my_notes,
                            backgroundColor: [
                                'rgba(255, 111, 141, 0.26)',
                            ],
                            borderColor: [
                                '#ff6384',
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        scales: {
                            yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    max: parseInt(this.__arrAlumnos[0].arrCursos_nota[0].formulaActual.maxcal),
                                    min: 0
                                }
                            }]
                        }
                    }
                });
                this.chartLine.data.labels = arrLabelNum;
                this.chartLine.data.datasets[0].data = arrNotasNum;
                this.chartLine.update();
                $(document).find('.graf-cont').show();
            } else {
                $(document).find('.graf-cont').hide();
            }

        }
        //WORK
        return promedioFinal.toFixed(2);
    }
    removeExtras() {
        $('#my-table').find('.f-remove').remove();
    }
    mostrarVista() {
        const btn = $("#btnVistasContainer .changevista.active")
        const vista = btn.attr("vista")
        $('.vista-item').hide()
        $("#" + vista).show("fade", {}, 'fast')
        if (__usuario.idrol == 3) {
            if (vista != "table")
                this.drawChart(vista)
        }
    }
    randomColor() {
            // return String.prototype.concat("#",Math.floor(Math.random()*16777215).toString(16));
            return String.prototype.concat('#', (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6))
        }
        /**
         * funcion que dibuja diferentes graficos del reporte con la libreria ChartJS
         * @param {String} tipo Tipo de vista 
         */
    drawChart(tipo = null) {
        tipo = tipo == null ? $("#btnVistasContainer .changevista.active").attr("vista") : tipo
        const tiposPermitidos = ["area", "bar", "line", "pie"]
        const columns = this.__arrColumnsNotas.map(elementos => { return elementos.nombre })
        const configuracion = {
            type: tipo == "area" ? "line" : tipo,
            data: {
                labels: [],
                datasets: []
            },
            options: {}
        }
        let datos = []
        let _datasets = []
        let _options = {
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        // steps: 10,
                        // stepValue: 5,
                        // max: 100
                    }
                }]
            }
        }
        let colores = []

        //Checar si es un tipo de vista valido
        if (tiposPermitidos.find(v => v == tipo) == null) {
            console.error("Tipo de visor no permitido para dibujar en un Chart");
            return false;
        }

        this.__arrColumnsNotas.forEach(d => {
            const colorear = this.randomColor()
                // datos.push(Math.floor(Math.random() * 20) + 1);
            datos.push((d.nota == null ? 0 : d.nota));
            colores.push(this.ChartColor(colorear).alpha(0.5).rgbString())
        })
        let _this = this;
        _datasets.push({
            label: _this.str_notes,
            backgroundColor: colores,
            borderWidth: 1,
            data: datos,
            borderColor: this.randomColor(),
            fill: tipo == "area" ? true : false,
            lineTension: tipo == "area" ? 0.4 : 0.1
        })

        if (this.__arrCharts[tipo] == null) {
            this.idnumgrafico++
                let idElement = String.prototype.concat("grafico", this.idnumgrafico)
            $('#' + tipo + ' .graficoContainer').html(`<canvas class="chart" id="${idElement}" ></canvas>`);
            let ctx = document.getElementById(idElement).getContext('2d');

            configuracion.options = _options
            configuracion.data.labels = columns
            configuracion.data.datasets = _datasets

            this.__arrCharts[tipo] = new Chart(ctx, configuracion);
        } else {
            //update
            this.__arrCharts[tipo].options = _options
            this.__arrCharts[tipo].data.labels = columns
            this.__arrCharts[tipo].data.datasets = _datasets

            this.__arrCharts[tipo].update()
        }

        //Fix display
        setTimeout(function beforePrintHandler() {
            for (var id in Chart.instances) {
                Chart.instances[id].resize();
            }
        }, 450)


    }
}