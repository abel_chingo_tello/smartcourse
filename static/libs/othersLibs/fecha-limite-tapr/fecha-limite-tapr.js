class FechaTarea {

    constructor(params) {
        this.htmlSection = $(`#${params.idsection}`);//modal
        this.flujo = params.flujo;
        this.li = null;

        this.disable = false;
        this.checkbox = this.htmlSection.find('#check-sin-fecha-limite');
        // console.log('this.checkbox ', this.checkbox);
        this.addEvents();
    }
    get getCurDatetime() {
        return moment().format("YYYY-MM-DD H:MM");
    }
    updateSesionPestania(params) {
        if (this.disable) {
            return null;
        }
        let cronTarPr = this.identify();
        cronTarPr = Object.assign(cronTarPr, params);
        postData(_sysUrlBase_ + `json/Cron_tarea_proy/updateCron`, cronTarPr);
    }
    identify() {
        let idpestania, idsesion;
        let arrLiParents = this.li.parents('li').get();
        // console.log('arrLiParents', arrLiParents);
        if (this.li.attr('tipo-li') == 'pestania') {  //es una pestaña
            idsesion = this.li.attr('idcursodetalle');
            idpestania = this.li.attr('idpestania');
        } else if (this.li.attr('tipo-li') == 'menu') {
            idpestania = 0;
            idsesion = this.li.attr('idcursodetalle');
        }
        let cronTarPr = {
            idcomplementario: IDCOMPLEMENTARIO,
            idcurso: idcurso,
            idgrupoauladetalle: __oCurso.idgrupoauladetalle,
            idpestania,
            idsesion
        };
        return cronTarPr;
    }
    showModal() {
        if (this.disable) {
            this.flujo();
            return null;
        }
        this.htmlSection.modal('show')
        this.htmlSection.find('[name="new-btn"]').removeClass('my-hide');
    }
    hideModal() {
        this.htmlSection.modal('hide')
    }
    getModal() {
        return this.htmlSection;
    }
    validateDatetime(userDateTime) {
        // console.log(userDateTime, ">", this.getCurDatetime)
        // console.log(new Date(userDateTime).getTime(), ">", new Date(this.getCurDatetime).getTime())
        // console.log(userDateTime, '>', this.getCurDatetime)
        // console.log(new Date(userDateTime).getTime(), '>', new Date(this.getCurDatetime).getTime())
        return (new Date(userDateTime).getTime() > new Date(this.getCurDatetime).getTime());
    }
    saveData() {
        if (this.disable || this.checkbox.is(':checked')) {
            let sinFecha = 'Sin fecha limite'
            this.li.attr('fecha_limite', sinFecha);
            this.li.attr('title', `${sinFecha}`);
            this.flujo(sinFecha);
            this.htmlSection.modal('hide');
            return null;
        }
        let fecha = this.htmlSection.find('#fecha-limite-tapr').val();
        let hora = this.htmlSection.find('#hora-limite-tapr').val();
        let userDateTime = `${fecha} ${hora}:00`;
        if (this.validateDatetime(userDateTime)) {
            this.htmlSection.modal('hide');
            let cronTarPr = this.identify();
            if (this.li.parents('li').get().length > 0) {//pestaña
                let nombreSesion = $(this.li.parents('li').find('.titulo')[0]).text().trim();
                cronTarPr.nombre = nombreSesion + ' - ' + ($(this.li.find('.titulo')[0]).text().trim());
                cronTarPr.recurso = this.li.attr('link');
                cronTarPr.tiporecurso = this.li.attr('type');
            } else {//menu
                cronTarPr.nombre = $(this.li.find('.titulo')[0]).text().trim();
                cronTarPr.recurso = this.li.attr('txtjson-link');
                cronTarPr.tiporecurso = this.li.attr('txtjson-typelink');
            }
            cronTarPr.tipo = this.li.attr('tipo-cron');
            cronTarPr.fecha_hora = userDateTime;
            postData(_sysUrlBase_ + `json/Cron_tarea_proy/guardar`, cronTarPr).then(rs => {
                this.li.attr('fecha_limite', userDateTime);
                this.li.attr('title', `Vence: ${userDateTime}`);
                this.flujo(cronTarPr.fecha_hora);
            }).catch(e => {
                Swal.fire({
                    icon: 'warning',
                    title: 'Ocurrió un error al guardar la fecha límite, por favor vuelve a intentarlo en un momento.',
                    showConfirmButton: true,
                })
                console.warn('fecha-limite-tapr.js>saveData():', e);
            })
        } else {
            Swal.fire({
                icon: 'warning',
                title: '¡Por favor, ingrese una fecha válida!',
                showConfirmButton: true,
            })
        }
    }
    deleteCron() {
        if (this.disable) {
            this.flujo();
            return null;
        }
        this.li.removeAttr('fecha_limite');
        this.li.removeAttr('title');
        let cronTarPr = this.identify();
        postData(_sysUrlBase_ + `json/Cron_tarea_proy/eliminar`, cronTarPr);
        this.flujo();
    }
    addEvents() {
        let modalFooter = this.htmlSection.find('[name="new-btn"]');
        modalFooter.removeClass('my-hide');
        modalFooter.find('[name="crear"]').on('click', () => {
            this.saveData();
        })
        modalFooter.find('[name="cancelar"]').on('click', () => {

        })
        let $this = this;
        $(this.htmlSection).on('hidden.bs.modal', function (e) {
            $this.clearForm(e);
        })

        this.checkbox.change(() => {
            let inputFecha = this.htmlSection.find('#fecha-limite-tapr');
            let inputHora = this.htmlSection.find('#hora-limite-tapr');
            if (this.checkbox.is(':checked')) {
                inputFecha.prop('disabled', true);
                inputHora.prop('disabled', true);
            } else {
                inputFecha.prop('disabled', false);
                inputHora.prop('disabled', false);
            }
        })
    }
    clearForm(e) {
        let modal = e.currentTarget;
        let arrInput = $('.my-modal').find('input');
        for (let index = 0; index < arrInput.length; index++) {
            arrInput[index].value = '';
            $(arrInput[index]).prop('disabled', false)
        }
        this.checkbox.prop('checked', false);
        let arrTextarea = $('.my-modal').find('textarea');
        for (let index = 0; index < arrTextarea.length; index++) {
            arrTextarea[index].value = '';
            $(arrTextarea[index]).prop('disabled', false)
        }
        let arrButton = $('.my-modal').find('button');
        for (let index = 0; index < arrButton.length; index++) {
            arrButton[index].value = '';
            $(arrButton[index]).prop('disabled', false)
        }
        $(modal).find('#autor-btn').addClass('my-hide');
        $(modal).find('#default-btn').addClass('my-hide');
        $(modal).find('#new-btn').addClass('my-hide');
    }
    bloquearTarea(cronTarPr){

        postData(_sysUrlBase_ + `json/Cron_tarea_proy/`, cronTarPr).then(rs => {
            if (rs.length > 0) {
                let cronTarPr = rs[0];
                if (cronTarPr.estado == 0 &&
                    this.validateDatetime(cronTarPr.fecha_hora)
                    // moment(this.getCurDatetime).isAfter(cronTarPr.fecha_hora)
                ) {
                    this.flujo();
                } else {
                    if(cronTarPr.idcomplementario==0) this.flujo();
                    else
                    Swal.fire({
                        icon: 'warning',
                        title: `Lo sentimos, esta tarea ya superó su fecha limite <br> (${cronTarPr.fecha_hora})`,
                        showConfirmButton: true,
                    })
                }
            } else {
                this.flujo();
            }

        })

    }
}