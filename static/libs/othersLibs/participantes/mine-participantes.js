var urlModulo = _sysUrlBase_ + `json/participantes`;
var arrGroupsSelect = [];
var arrAlumnos = []
var loading = false;
const oMine = new Mine;

$(document).ready(() => {
    postData(urlModulo + `/listado`).then(arrCursos => {
        console.log('arrCursos', arrCursos)
        let arrTmp = arrCursos.map((curso) => {
            return { id: curso.idcurso, text: curso.nombrecurso + ' - ' + curso.strgrupoaula, arrAlumnos: curso.arrAlumnos, idgrupoauladetalle: curso.idgrupoauladetalle }
        });
        arrCursos = arrTmp;
        let selectCursos = $('#selectCursos');
        selectCursos.select2({ data: arrCursos })
        selectCursos.on("change.select2", () => { //Añadido evento al cambiar
            let curso = selectCursos.select2('data')[0]; //obteniendo elemento seleccionado
            console.log('curso: ', curso);
            listGroups(curso.idgrupoauladetalle);
            drawTable(curso.arrAlumnos)
        })
        selectCursos.trigger('change.select2');
    })
    $.extend($.fn.dataTable.defaults, {
        responsive: true
    });
    initializeDatatable();

});

function initializeDatatable() {
    $('#my-table').DataTable({
        responsive: true,
        targets: [0, 1, 2],
        className: 'mdl-data-table__cell--non-numeric',
        searching: false,
        ordering: false,
        paging: false,
        info: false,
        bLengthChange: false,
        language: {
            search: "Buscar:",
            lengthMenu: "Mostrando las &nbsp _MENU_ &nbsp primeras publicaciones",
            info: "Mostrando _START_ a _END_ publicaciones de un total de _TOTAL_ ",
            paginate: {
                first: "Primera página",
                previous: "Anterior",
                next: "Siguiente",
                last: "Última página"
            }
        }
    });
}

function drawTable(arrAlumnoss = []) {
    arrAlumnos = arrAlumnoss;

    $('#my-table').DataTable().destroy();
    let tbody = $('#tbody');

    let htmlRows = `<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">Aún no hay alumnos en este curso!</td></tr>`;
    if (arrAlumnos.length > 0) {
        htmlRows = ``;
        arrAlumnos.forEach((alumno, i) => {
            htmlRows += `<tr id="tr-${i}">
                            <td><img class="foto-alumno" src="${alumno.foto != '' ? _sysUrlBase_ + "static/media/usuarios/" + alumno.foto : _sysUrlBase_ + 'static/libs/othersLibs/participantes/img/foto_usuario.png'}" alt=""></td>
                            <td>${alumno.stralumno}</td>
                            <td class="text-center">${alumno.email}</td>
                            <td class="text-center">${alumno.edad}</td>
                            <td class="text-center">
                            <label class="label-editable" title="Doble click para asignar grupo de trabajo" id_grupo_trabajo="${alumno.id_grupo_trabajo}">${alumno.nombre_grupo_trabajo}</label>
                                <div class="select-container my-hide">
                                    <select id="select-${i}">
                                    </select>
                                </div>
                            </td>
                        </tr>`;
        });
        $('#tb-head').find('tr').html(`<th></th><th>Nombre</th><th>Email</th><th>Edad</th><th>Grupo de trabajo</th>`);
    } else {
        $('#tb-head').find('tr').html(`<th></th>`);
    }
    tbody.html(htmlRows);
    initializeDatatable();
    addEvents();
}

function addEvents() {
    let arrLabel = Array.from(document.querySelectorAll('tr label'));
    let btnSave = $(document).find('#btn-save');
    btnSave.on('click', () => saveGroups(arrGroupsSelect));

    let fx = (arrLabel) => {
        arrLabel.forEach(label => {
            label.addEventListener('dblclick', (e => {
                let tr = e.target.parentElement.parentElement;
                let index = tr.id.split('-')[1];
                let label = tr.querySelector("label");
                let selectGroup = $(tr.querySelector("select"));
                console.log('inicializando select:', selectGroup, 'con ', arrGroupsSelect);
                if (selectGroup[0].hasAttribute("data-select2-id")) {
                    console.log('limpiando select...')
                    selectGroup.empty();
                    // selectGroup.select2('destroy');
                }
                selectGroup.select2({
                    data: arrGroupsSelect,
                    tags: true,
                })

                selectGroup.on("change.select2", () => {
                    let grupo = selectGroup.select2('data')[0]; //obteniendo elemento seleccionado
                    let idmatricula = arrAlumnos[index].idmatricula;
                    console.log('grupo', grupo);
                    tr.querySelector(".select-container").classList.add('my-hide');
                    let oGroup = {
                        nombre: grupo.text,
                        idgrupoauladetalle: arrAlumnos[index].idgrupoauladetalle,
                        id: grupo.text,
                        text: grupo.text,
                    }
                    console.log('oGroup', oGroup);

                    let indexSelect = arrGroupsSelect.findIndex(grupoSelect => grupoSelect.text == oGroup.text);
                    if (!(indexSelect >= 0)) {
                        oGroup.arrMatriculas = [];
                        arrGroupsSelect.push(oGroup);
                        indexSelect = arrGroupsSelect.length - 1;
                    }
                    console.log('nuevo', arrGroupsSelect, indexSelect)
                    let indexMatricula = arrGroupsSelect[indexSelect].arrMatriculas.findIndex(grupoIdmatricula => grupoIdmatricula == idmatricula);
                    if (indexMatricula >= 0) {
                        arrGroupsSelect[indexSelect].arrMatriculas[indexMatricula] = idmatricula;
                    } else {
                        arrGroupsSelect[indexSelect].arrMatriculas.push(idmatricula);
                    }
                    label.innerHTML = grupo.text;
                    label.classList.remove("my-hide");
                });
                console.log('disparando...');
                selectGroup.trigger('change');
                console.log('disparado');
                tr.querySelector(".select-container").classList.remove('my-hide');
                selectGroup.select2('open');
                label.classList.add("my-hide");
                console.log('dblclick  en', tr)
            }))
        });
    }
    fx(arrLabel);
    $('#my-table').on('column-visibility.dt', function(e, settings, column, state) {
        console.log('column-visibility.dt');
        fx(arrLabel);
    });
}

function saveGroups(arrGroupsSelect) {
    $(document).find('#btn-save').prop('disabled', true);
    return new Promise((resolve, reject) => {
        oMine.postData({
            url: _sysUrlBase_ + 'json/Tb_grupo_trabajo/guardar',
            data: {
                params: {
                    arrGroupsSelect
                }
            }
        }).then(rs => {
            console.log('rs Tb_grupo_trabajo', rs);
            clearGroupsSelect();
            $(document).find('#btn-save').prop('disabled', false);
        });
    });
}

function listGroups(idgrupoauladetalle) {
    loading = true;
    return new Promise((resolve, reject) => {
        oMine.postData({
            url: _sysUrlBase_ + 'json/Tb_grupo_trabajo',
            data: { idgrupoauladetalle }
        }).then(arrRs => {
            loading = false;
            console.log('arrRs Tb_grupo_trabajo', arrRs)
            arrRs = arrRs.map(grupo => {
                grupo.id = grupo.id_grupo_trabajo;
                grupo.arrMatriculas = [];
                grupo.text = grupo.nombre;
                return grupo;
            })
            arrRs.unshift({ id: -1, text: 'Sin grupo', selected: true, arrMatriculas: [] });
            arrGroupsSelect = arrRs;
        });
    });
}

function clearGroupsSelect() {
    arrGroupsSelect.forEach((groupSelect, i) => {
        groupSelect.arrMatriculas = []
        arrGroupsSelect[i] = groupSelect;
    });
}