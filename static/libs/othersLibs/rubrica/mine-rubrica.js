var urlModulo = _sysUrlBase_ + `json/rubrica`;
var idrubricaWorking = 0;
var idcriterioWorking = 0;
var idnivelWorking = 0;
var arrRubrica = [];
var arrRendered = []
var _htmlSection = null;
var _my_li = null;
var _currentSelect = null;
var _instancia = null;
var notaRubrica = 0;
var _porcentajeRestante = 0;
var _idalumno = null;
var _idRubricaInstance = null;
var _htmlCheck = $($.parseHTML(`<div id="check-instancia" class="check-instancia my-center"><i class="fa fa-check-circle" aria-hidden="true"></i></div>`));
var _rubricaValida = true;
var _idcurso = null;
var _configuracionNota = null;
var _maxRubricaNivel = 0;

// FRONTEND
function renderCalificarRubrica(arrRubrica, htmlSection = null, mode = 0) {
    // working
    if (htmlSection == null) {//si no recibo parametro
        if (_htmlSection != null) {//busco si ya lo tengo establecido
            htmlSection = _htmlSection;
        } else {//sino lo defino
            htmlSection = document;
            _htmlSection = htmlSection;
        }
    } else {
        _htmlSection = htmlSection;
    }
    arrRendered = arrRubrica;
    let mainCont = null;
    if (htmlSection) {
        mainCont = $(htmlSection).find('#main-cont');
    } else {
        mainCont = $('#main-cont');
    }
    mainCont.html('');
    arrRubrica.forEach((rubrica, index_rubrica) => {
        _maxRubricaNivel = 0;
        _rubricaValida = true;
        let idClone = 'row-cont-rubrica-clone' + index_rubrica;
        let cloneRubrica = $('#row-cont-rubrica').clone(true);
        cloneRubrica.attr('id', idClone);
        cloneRubrica.attr('idrubrica', rubrica.idrubrica);
        _idRubricaInstance = rubrica.idrubrica;
        cloneRubrica.find('h3').html(rubrica.nombre);
        cloneRubrica.find('h3').attr('idrubrica', rubrica.idrubrica)
        cloneRubrica.find('h3').attr('index', index_rubrica);
        cloneRubrica.find('#body-rubrica').html('');
        let rubricaCode = $('#rubrica-code').clone(true);
        rubricaCode.find('.code-num').html('00' + rubrica.idrubrica);
        rubricaCode.attr('idrubrica', rubrica.idrubrica);
        rubricaCode.attr('index', index_rubrica);
        rubrica.arrNivel.forEach((nivel, index) => {
            if (parseInt(nivel.cuantitativo) > _maxRubricaNivel) {
                // console.log('nivel.cuantitativo > _maxRubricaNivel?', (parseInt(nivel.cuantitativo) > _maxRubricaNivel))
                // console.log('nuevo valor máximo: ', nivel.cuantitativo)
                _maxRubricaNivel = parseInt(nivel.cuantitativo);
            }
            let htmlNivel = $('#row-cont-rubrica').find('#cont-btn-nivel').clone(true);
            htmlNivel.find('.nivel-text').html(nivel.cualitativo)
            htmlNivel.find('.nivel-num').html(`(${nivel.cuantitativo})`);
            htmlNivel.attr('orden', nivel.orden);
            htmlNivel.attr('index', index);
            htmlNivel.attr('idrubrica', nivel.idrubrica);
            htmlNivel.attr('index_rubrica', index_rubrica);
            cloneRubrica.find('#body-rubrica').append(htmlNivel);
        });
        let porcentajeTotal = 100;//si fuese equitativo
        let cantSinPeso = 0;
        let arrCriteriosVacios = [];
        rubrica.arrCriterio.forEach((criterio, index) => {
            // if (mode == 2) {
            if (parseInt(criterio.peso) > 0) {
                porcentajeTotal -= parseInt(criterio.peso);//resto los no equitativos
            } else {
                cantSinPeso++;
            }
            // }
            let htmlCriterio = $('#row-cont-rubrica').find('#cont-btn-crit').clone(true);
            htmlCriterio.find('.criterio-text').html(criterio.detalle)
            if (parseInt(criterio.peso) > 0) {
                htmlCriterio.find('.criterio-num').html(criterio.peso + '%');
            } else {
                arrCriteriosVacios.push(htmlCriterio.find('.criterio-num'));
            }
            htmlCriterio.attr('orden', criterio.orden);
            htmlCriterio.attr('index', index);
            htmlCriterio.attr('idrubrica', criterio.idrubrica);
            htmlCriterio.attr('index_rubrica', index_rubrica);
            cloneRubrica.find('#body-rubrica').append(htmlCriterio);
        });
        if (/*mode == 2 &&*/ cantSinPeso > 0) {//si hubo criterios sin peso..
            // console.log('porcentajeTotal', porcentajeTotal)
            _porcentajeRestante = porcentajeTotal / cantSinPeso;//repartir porcentaje restante
            arrCriteriosVacios.forEach(htmlCriterio => {
                htmlCriterio.html(Math.round(_porcentajeRestante) + '%');
            });
        }
        // MIX
        rubrica.arrNivel.forEach(nivel => {
            rubrica.arrCriterio.forEach(criterio => {
                let servIndicador = rubrica.arrIndicador.filter((indicador) => {
                    let rpt = null;
                    if (indicador.idrubrica_nivel == nivel.idrubrica_nivel &&
                        indicador.idrubrica_criterio == criterio.idrubrica_criterio) {
                        rpt = indicador;
                    }
                    return rpt;
                });
                let htmlIndicador = null;
                if (servIndicador.length > 0) {
                    htmlIndicador = $('#row-cont-rubrica').find('#cont-btn-indicador').clone(true);
                    htmlIndicador.attr('index', parseInt(servIndicador[0].orden) - 1);
                    htmlIndicador.attr('index_rubrica', index_rubrica);
                    htmlIndicador.attr('idindicador', servIndicador[0].idrubrica_indicador);
                    if (mode == 2) {
                        htmlIndicador.attr('onclick', 'checkIndicador(this)');
                    }
                    htmlIndicador.html(servIndicador[0].detalle);
                } else {
                    _rubricaValida = false;
                    htmlIndicador = $('#row-cont-rubrica').find('#btn-new-indicador').clone(true);
                }
                htmlIndicador.attr('idrubrica', rubrica.idrubrica);
                htmlIndicador.attr('criterio', criterio.idrubrica_criterio);
                htmlIndicador.attr('nivel', nivel.idrubrica_nivel);
                htmlIndicador.attr('style', `grid-column:${parseInt(nivel.orden) + 1};grid-row:${parseInt(criterio.orden) + 1}`);
                cloneRubrica.find('#body-rubrica').append(htmlIndicador);
            });
        });
        if (mode == 1) {
            cloneRubrica.addClass('ver-rubrica');
        }
        if (mode == 2) {
            cloneRubrica.addClass('calificar-rubrica');

        }
        cloneRubrica.find('#body-rubrica').append(rubricaCode);//se le asigna su codigo
        mainCont.append(cloneRubrica);
        if (mode < 1) {
            let btnNewCriterio = $('#btn-new-criterio').clone(true);
            let btnNewNivel = $('#btn-new-nivel').clone(true);
            btnNewCriterio.attr('idrubrica', rubrica.idrubrica)
            btnNewNivel.attr('idrubrica', rubrica.idrubrica)
            mainCont.find(`#${idClone}`).find('#out-cont').append(btnNewNivel);
            mainCont.find(`#${idClone}`).find('#out-cont').append(btnNewCriterio);
            let fixWidth = mainCont.find(`#${idClone}`).find('#rubrica-code').css('width');
            let fixHeight = mainCont.find(`#${idClone}`).find('#rubrica-code').css('height');
            btnNewNivel.attr('style', `min-height:${fixHeight};`);
            btnNewCriterio.attr('style', `width:${fixWidth};`);
            if (!rubrica.editable) {
                btnNewNivel.prop('disabled', true);
                btnNewCriterio.prop('disabled', true);
            } else {
                btnNewNivel.prop('disabled', false);
                btnNewCriterio.prop('disabled', false);
            }
        }
        if (mode > 0) {
            getIndicadorAlumno();
        }

    });
    if (mode == 0 || mode == 2) {//
        if ($('#compose').get().length == 0) {
            $(htmlSection).find('.modal-dialog').addClass('fix-modal-dialog');
        }

    }
}

/* [CALIFICACIÓN RUBRICA] */
function getConfiguracionNota(idcurso = _idcurso) {
    if (idcurso) {
        postData(_sysUrlBase_ + 'json/proyecto_cursos',
            { idcurso: idcurso }
        ).then(rs => {
            _configuracionNota = JSON.parse(rs[0].configuracion_nota);
        })
    }
}
function checkIndicador(el) {
    // console.log('el', el);
    let hasCheck = $(el).find('#check-instancia').get();
    let arrRow = $(_htmlSection).find(`.cont-btn-indicador[criterio=${$(el).attr('criterio')}]`).get();
    arrRow.forEach(cell => {
        let hasCheck = $(cell).find('#check-instancia').get();
        if (hasCheck.length > 0) {
            hasCheck.forEach(check => {
                check.remove();
            });
        }
    });
    if (hasCheck.length == 0) {//marca
        let htmlCheck = _htmlCheck.clone();
        htmlCheck.addClass('clone');
        $(el).append(htmlCheck);
    }
    sumIndicador();
}
function renderCheckIndicador(arrIndicadorAlumno) {
    arrIndicadorAlumno.forEach(indicadorAlumno => {
        // let htmlCheck = $('#to-clone').find('#check-instancia').clone();
        let htmlCheck = _htmlCheck.clone();
        htmlCheck.addClass('clone');
        let htmlIndicador = $(_htmlSection).find(`.cont-btn-indicador[idindicador=${indicadorAlumno.idrubrica_indicador}]`);
        htmlIndicador.append(htmlCheck);
    });
}
function sumIndicador() {
    notaRubrica = 0;
    arrHtmlCheck = $(_htmlSection).find('.check-instancia.my-center.clone').get();
    arrHtmlCheck.forEach(htmlCheck => {
        let htmlIndicador = $(htmlCheck).parent();
        let idcriterio = $(htmlIndicador).attr('criterio');
        let idnivel = $(htmlIndicador).attr('nivel');
        let criterio = arrRendered[0].arrCriterio.filter((criterio) => { return criterio.idrubrica_criterio == idcriterio })[0];
        let nivel = arrRendered[0].arrNivel.filter((nivel) => { return nivel.idrubrica_nivel == idnivel })[0];
        if (criterio.peso > 0) {
            notaRubrica += parseInt(nivel.cuantitativo) * (parseInt(criterio.peso) / 100);
        } else {
            notaRubrica += parseInt(nivel.cuantitativo) * (_porcentajeRestante / 100);
        }
    });
    notaRubrica = Math.round(notaRubrica);
    if (_configuracionNota) {
        let notaConvertida = notaRubrica;
        switch (_configuracionNota.tipocalificacion) {
            case 'N': //Numérico
                notaConvertida = Math.round((notaRubrica * _configuracionNota.maxcal) / _maxRubricaNivel);
                // console.log('notaConvertida (N)', notaConvertida);
                break;
            case 'P': //Porcentaje
                notaConvertida = Math.round((notaRubrica / _maxRubricaNivel)) * 100;
                // console.log('notaConvertida (P)', notaConvertida, '%');
                break;
            case 'A': //Alfanumérico
                notaConvertida = Math.round((notaRubrica * _configuracionNota.maxcal) / _maxRubricaNivel);
                // console.log('notaConvertida (A)', notaConvertida);
                break;
            default:
                break;
        }
        let campoRubrica = $(_htmlSection).find('#campo-nota-rubrica');
        let campoDefault = $(_htmlSection).find('#campo-default');
        $(campoRubrica).addClass('my-y-center');
        $(campoDefault).addClass('my-y-center');
        campoRubrica.removeClass('my-hide');
        $(campoRubrica).find('.span-nota-rubrica').html(_maxRubricaNivel);
        $(campoRubrica).find('.input-nota-rubrica').val(notaRubrica);
        $(_htmlSection).find('#span-my-base').html(_configuracionNota.maxcal);
        $(_htmlSection).find('input.notafinal').val(notaConvertida);
    } else {
        $(_htmlSection).find('input.notafinal').val(notaRubrica);
    }
}


//toBackend
/* [RUBRICA] */
function saveRubrica() {
    // let rpt = validate();
    let rpt = {};
    rpt.bool = true;
    if (rpt.bool) {
        Swal.fire({
            title: 'Cargando...',
            timerProgressBar: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                $('#md-nueva-rubrica').modal('hide');
                Swal.showLoading();
                postData(urlModulo + `/guardar`, {
                    nombre: $('#nombre-rubrica').val(),
                    idrubrica: $('#input-idrubrica').val()
                }).then(respuesta => {
                    if (respuesta.code === 200) {
                        Swal.close();
                        swalSaved({ timer: 1000, title: 'Rubrica guardada!' });
                        getSelectRubrica(null, null, respuesta.newid);
                    }
                })
            },
        }).then((result) => {
            /* Read more about handling dismissals below */

        })

    } else {
        Swal.fire({
            icon: 'warning',
            title: 'Hay campos por corregir...',
            text: rpt.msj,
        })
    }
    return false;
}
function getSelectRubrica(htmlSection = null, my_li = null, idpreselect = null) {

    let inModal = true;

    if (htmlSection == null) {//si no recibo parametro
        if (_htmlSection != null) {//busco si ya lo tengo establecido
            htmlSection = _htmlSection;
            if (_my_li) {
                my_li = _my_li;
            } else {
                inModal = false;
            }

        } else {//sino lo defino
            inModal = false;
            htmlSection = document;
            _htmlSection = htmlSection;
        }
    } else {
        _htmlSection = htmlSection;
        _my_li = my_li;
    }
    let htmlSelect = $(htmlSection).find('#selectRubrica');
    htmlSelect.html('<option></option>');
    let instanciaRubrica = null;
    if (inModal) {
        instanciaRubrica = {
            idcursodetalle: $(my_li).attr('idcursodetalle'),
            idcomplementario: __oCurso.idcomplementario,
            idcurso: __oCurso.idcurso
        }

        let idpestania = 0;
        if ($(my_li).attr('tipo-li') == "pestania") {
            idpestania = $(my_li).attr('idpestania');
        }
        if (idpestania > 0) {
            instanciaRubrica.idpestania = idpestania;
        }
    }
    hasInstance(instanciaRubrica).then(instancia => {
        idpreselect = instancia.idrubrica;
        if (!instancia.editable) {
            htmlSelect.prop('disabled', true);
            let mytext='Esta rúbrica no puede ser reemplazada porque ya hay alumnos evaluados con esta.';
            htmlSelect.prop('title',mytext);
            htmlSelect.parent().prop('title',mytext);
        }
        $('#input-idinstancia').val(instancia.idrubrica_instancia);
        // console.log('tiene instancia?:', idpreselect)
    }).catch((e) => {
        console.warn('No se encontraron instacias:', e)
    }).finally(() => {
        postData(urlModulo + `/listado`, {
            getEditable: true, getAditional: idpreselect
        }).then(data => {
            let arrTmp = data.map((rubrica) => {
                rubrica.text = rubrica.nombre;
                rubrica.id = rubrica.idrubrica;
                return rubrica;
            })
            arrRubrica = arrTmp;
            if (!inModal && arrRubrica.length > 0) {
                arrRubrica.push({ id: 0, text: 'Todas' });
            }
            if (inModal) {
                // arrRubrica.push({ id: -1, text: 'Sin Rubrica' });
            }
            if (idpreselect) {
                _currentSelect = idpreselect;
            } else {
                if (_currentSelect) {
                    idpreselect = _currentSelect;
                }
            }
            let htmlMensaje = ` <div id="row-cont-rubrica-x" class="col-sm-12 row-cont-rubrica">
                                    <div class="cont-rubrica my-card my-shadow">
                                        <h3 class="text-center my-full-w my-rm-link my-italic">Seleccione una rúbrica</h3>
                                    </div>
                                </div>`;
            $(htmlSection).find('#main-cont').html(htmlMensaje);
            addSelect2(htmlSelect, arrRubrica, (rubrica) => {
                if (rubrica.length > 0) {
                    _currentSelect = rubrica[0].id;
                    if (rubrica[0].id == 0) {
                        renderCalificarRubrica(arrRubrica.slice(0, arrRubrica.length - 1), htmlSection, 0);
                    } else {
                        renderCalificarRubrica(rubrica, htmlSection, 0);
                    }
                }
            }, idpreselect)
            addOnModalClose();
        })
    });

}
function eliminarRubrica() {
    let rubrica = {
        idrubrica: $('#input-idrubrica').val(),
    };
    Swal.fire({
        title: 'Estás seguro?',
        text: "Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar!'
    }).then((result) => {
        if (result.value) {
            postData(urlModulo + `/eliminar`, rubrica).then(rpt => {
                if (rpt.code == 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'La rubrica ha sido eliminada!',
                        showConfirmButton: false,
                        timer: 1000
                    })
                    getSelectRubrica();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Algo salió mal...',
                        text: 'Vuelve a intentralo más tarde',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                }
            })

        }
    })
}
/* [INSTANCIA] */
function saveInstancia(htmlSection = null, my_li = null) {
    let rpt = true;
    // console.log('en instancia:', htmlSection, my_li)

    let selectRubrica = $(htmlSection).find('#selectRubrica');
    let id = $(my_li).attr('id');
    let idpestania, idcursodetalle;
    idcursodetalle = $(my_li).attr('idcursodetalle');
    idpestania = 0;
    if ($(my_li).attr('tipo-li') == "pestania") {
        idpestania = $(my_li).attr('idpestania');
    }

    let instanciaRubrica = {
        idrubrica_instancia: $('#input-idinstancia').val(),
        idrubrica: selectRubrica.val(),
        idpestania: idpestania,
        idcursodetalle,
        idcomplementario: __oCurso.idcomplementario,
        idcurso: __oCurso.idcurso,
    }
    if (instanciaRubrica.idrubrica == 0) {
        Swal.fire({
            icon: 'warning',
            title: 'Rubrica inválida',
            text: 'Seleccione una rúbrica.',
        })
        rpt = false;
    } else {
        if (_rubricaValida) {
            // console.log('Guardando instanciaRubrica', instanciaRubrica);
            Swal.fire({
                title: 'Cargando...',
                timerProgressBar: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                onBeforeOpen: () => {
                    $('#md-nueva-rubrica').modal('hide')
                    Swal.showLoading();
                    postData(urlModulo + `_instancia/guardar`, instanciaRubrica)
                        .then(respuesta => {
                            if (respuesta.code === 200) {
                                Swal.close();
                                __cerrarmodal(htmlSection);
                                swalSaved({ timer: 1000, title: 'Rúbrica  asignada!' });
                            }
                        })
                },
            }).then((result) => { })
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Rubrica inválida',
                text: 'La rubrica seleccionada no está completa.',
            })
            rpt = false;
        }

    }
    return rpt;
}
function removeInstancia(my_li) {
    // console.log('...[REMOVIENDO INSTANCIAS] de:', my_li);
    instanciaRubrica = {
        idcursodetalle: $(my_li).attr('idcursodetalle'),
        idcomplementario: __oCurso.idcomplementario,
        idcurso: __oCurso.idcurso
    }
    let idpestania = $(my_li).attr('id');
    if (idpestania > 0) {
        instanciaRubrica.idpestania = idpestania;
    }
    postData(urlModulo + `_instancia/deleteAll`, instanciaRubrica).then(rs => {
        // console.log('instancias eliminadas: ', rs);
    }).catch(e => { console.warn('Error eliminando instancias:', e) })

}
function hasInstance(instanciaRubrica) {
    return new Promise((resolve, reject) => {
        if (instanciaRubrica) {
            instanciaRubrica.getEditable = true;
            let instancia;
            postData(urlModulo + `_instancia/listado`, instanciaRubrica).then(arrInstancia => {
                // console.log('...[hasInstance]', arrInstancia);
                if (arrInstancia.length) {
                    instancia = arrInstancia[0];
                    resolve(instancia);
                } else {
                    reject(null);
                }
            }).catch(e => { 'Error en función  hasInstance()', e })
        } else {
            reject(null);
        }
    })

}
/* [NIVEL] */
function saveNivel() {
    // let rpt = validate();
    let rpt = {};
    rpt.bool = true;
    if (rpt.bool) {
        Swal.fire({
            title: 'Cargando...',

            timerProgressBar: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                $('#md-nivel').modal('hide')
                Swal.showLoading()
                postData(urlModulo + `_nivel/guardar`, {
                    idrubrica_nivel: $('#input-idnivel').val(),
                    cualitativo: $('#cualitativo').val(),
                    cuantitativo: $('#cuantitativo').val(),
                    idrubrica: idrubricaWorking
                }).then(respuesta => {
                    if (respuesta.code === 200) {
                        Swal.close();
                        swalSaved({ title: 'Nivel guardado!' });
                        getSelectRubrica();
                    }
                })
            },
        }).then((result) => {
            /* Read more about handling dismissals below */

        })

    } else {
        Swal.fire({
            icon: 'warning',
            title: 'Hay campos por corregir...',
            text: rpt.msj,
        })
    }
    return false;
}
function eliminarNivel() {
    let nivel = {
        idrubrica_nivel: $('#input-idnivel').val(),
    };
    Swal.fire({
        title: 'Estás seguro?',
        text: "Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar!'
    }).then((result) => {
        if (result.value) {
            postData(urlModulo + `_nivel/eliminar`, nivel).then(rpt => {
                if (rpt.code == 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'El nivel ha sido eliminado.',
                        showConfirmButton: false,
                        timer: 1000
                    })
                    getSelectRubrica();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Algo salió mal...',
                        text: 'Vuelve a intentralo más tarde',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                }
            })

        }
    })
}
/* [CRITERIO] */
function saveCriterio() {
    // let rpt = validate();
    let rpt = {};
    rpt.bool = true;
    if (rpt.bool) {
        Swal.fire({
            title: 'Cargando...',

            timerProgressBar: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                $('#md-criterio').modal('hide')
                Swal.showLoading()
                postData(urlModulo + `_criterio/guardar`, {
                    idrubrica_criterio: $('#input-idcriterio').val(),
                    detalle: $('#nombre-criterio').val(),
                    peso: $('#peso-criterio').val(),
                    idrubrica: idrubricaWorking
                }).then(respuesta => {
                    if (respuesta.code === 200) {
                        Swal.close();
                        swalSaved({ title: 'Criterio guardado!' });
                        getSelectRubrica();
                    }
                })
            },
        }).then((result) => {
            /* Read more about handling dismissals below */

        })

    } else {
        Swal.fire({
            icon: 'warning',
            title: 'Hay campos por corregir...',
            text: rpt.msj,
        })
    }
    return false;
}
function eliminarCriterio() {
    let criterio = {
        idrubrica_criterio: $('#input-idcriterio').val(),
    };
    Swal.fire({
        title: 'Estás seguro?',
        text: "Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar!'
    }).then((result) => {
        if (result.value) {
            postData(urlModulo + `_criterio/eliminar`, criterio).then(rpt => {
                if (rpt.code == 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'El criterio ha sido eliminado.',
                        showConfirmButton: false,
                        timer: 1000
                    })
                    getSelectRubrica();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Algo salió mal...',
                        text: 'Vuelve a intentralo más tarde',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                }
            })

        }
    })
}
/* [INDICADOR] */
function saveIndicador(el) {
    let rpt = {};
    rpt.bool = true;
    if (rpt.bool) {
        Swal.fire({
            title: 'Cargando...',

            timerProgressBar: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                $('#md-indicador').modal('hide')
                Swal.showLoading();
                postData(urlModulo + `_indicador/guardar`, {
                    idrubrica_indicador: $('#input-idindicador').val(),
                    detalle: $('#detalle-indicador').val(),
                    idrubrica: idrubricaWorking,
                    idrubrica_criterio: idcriterioWorking,
                    idrubrica_nivel: idnivelWorking
                }).then(respuesta => {
                    if (respuesta.code === 200) {
                        Swal.close();
                        swalSaved({ title: 'Indicador guardado!' });
                        getSelectRubrica();

                    }
                })
            },
        }).then((result) => {
            /* Read more about handling dismissals below */

        })
    } else {
        Swal.fire({
            icon: 'warning',
            title: 'Hay campos por corregir...',
            text: rpt.msj,
        })
    }
    return false;
}
function eliminarIndicador() {
    let indicador = {
        idrubrica_indicador: $('#input-idindicador').val(),
    };
    Swal.fire({
        title: 'Estás seguro?',
        text: "Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar!'
    }).then((result) => {
        if (result.value) {
            postData(urlModulo + `_indicador/eliminar`, indicador).then(rpt => {
                if (rpt.code == 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'El indicador ha sido eliminado.',
                        showConfirmButton: false,
                        timer: 1000
                    })
                    getSelectRubrica();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Algo salió mal...',
                        text: 'Vuelve a intentralo más tarde',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                }
            })

        }
    })
}
/* [INDICADOR_ALUMNO] */
function saveIndicadorAlumno() {
    Swal.fire({
        // title: 'Cargando...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        onBeforeOpen: () => {
            Swal.showLoading();
        },
    })
    let arrIndicadorAlumno = [];
    arrHtmlCheck = $(_htmlSection).find('.check-instancia.my-center.clone').get();
    arrHtmlCheck.forEach(htmlCheck => {
        let htmlIndicador = $(htmlCheck).parent();
        let idindicador = $(htmlIndicador).attr('idindicador');//el indicador marcado
        let indicador = arrRendered[0].arrIndicador.filter((indicador) => { return indicador.idrubrica_indicador == idindicador })[0];
        let indicadorAlumno = {
            idalumno: _idalumno,
            idrubrica_indicador: indicador.idrubrica_indicador,
            idrubrica: indicador.idrubrica,
            idrubrica_instancia: _instancia.idrubrica_instancia,
            idgrupoauladetalle
        }
        arrIndicadorAlumno.push(indicadorAlumno);
    });
    if (arrIndicadorAlumno.length > 0) {
        postJson(urlModulo + `_indicador_alumno/guardar`, arrIndicadorAlumno).then(rs => {
            Swal.close();
            swalSaved({ timer: 400, title: 'Guardado!' });
            // swal.hideLoading()
            getIndicadorAlumno();
        })
    } else {
        postData(urlModulo + `_indicador_alumno/eliminarPorInstancia`,
            { idrubrica: _instancia.idrubrica, idrubrica_instancia: _instancia.idrubrica_instancia }
        ).then(rs => {
            Swal.close();
            swalSaved({ timer: 400, title: 'Guardado!' });
            // swal.hideLoading()
            getIndicadorAlumno();
        })
    }

}
function getIndicadorAlumno() {
    // console.log('[PIDIENDO INSTANCIA]', _instancia.idrubrica_instancia);
    postData(urlModulo + `_indicador_alumno/listado`, {
        idalumno: _idalumno,
        idrubrica_instancia: _instancia.idrubrica_instancia,
        idrubrica: _idRubricaInstance,
        idgrupoauladetalle: idgrupoauladetalle
    }).then(arrIndicadorAlumno => {
        renderCheckIndicador(arrIndicadorAlumno);
        sumIndicador();
    })
}

// tools
function addSelect2(htmlSelect, arrData, fxOnChange = () => { }, idPreselect = null) {
    $.fn.select2.defaults.set('language', 'es');
    // console.log('adding select2 on', htmlSelect);
    // console.log('idPreselect', idPreselect)
    let htmlSelect2 = $(htmlSelect);
    // AREA DE SELECT2
    if (htmlSelect2.hasClass("select2-hidden-accessible")) {
        htmlSelect2.empty();
    }
    htmlSelect2.select2({
        data: arrData,
        placeholder: 'Seleccione...',
        selectionTitleAttribute: false
    })
    htmlSelect2.on("change.select2", () => {//Añadido evento al cambiar
        let selectedItem = htmlSelect2.select2('data');
        fxOnChange(selectedItem);
    })
    if (idPreselect) {
        htmlSelect2.val(idPreselect);
        htmlSelect2.trigger('change.select2');
    }
}
/* [MODALES] */
function modalRubrica(el = null) {
    let index = $(el).attr('index');
    if (index) {
        let rubrica = arrRendered[index];
        $('#nombre-rubrica').val(rubrica.nombre);
        $('#input-idrubrica').val(rubrica.idrubrica);
        $('#md-nueva-rubrica').find('#autor-btn').removeClass('my-hide');
        $('#md-nueva-rubrica').find('#default-btn').removeClass('my-hide');
        $('#md-nueva-rubrica').find('#new-btn').addClass('my-hide');
        if (!rubrica.editable) {
            $('#md-nueva-rubrica').find('.btn-delete').prop('disabled', true)
        }
    } else {
        $('#md-nueva-rubrica').find('#autor-btn').addClass('my-hide');
        $('#md-nueva-rubrica').find("#new-btn").removeClass('my-hide');
    }
    $('#md-nueva-rubrica').modal('show');
}
function modalNivel(el) {

    idrubricaWorking = $(el).attr('idrubrica');
    let index = $(el).attr('index');
    let index_rubrica = $(el).attr('index_rubrica');
    if (index) {
        let nivel = arrRendered[index_rubrica].arrNivel[index];
        $('#cualitativo').val(nivel.cualitativo);
        $('#cuantitativo').val(nivel.cuantitativo);
        $('#input-idnivel').val(nivel.idrubrica_nivel);
        $('#md-nivel').find('#autor-btn').removeClass('my-hide');
        $('#md-nivel').find('#default-btn').removeClass('my-hide');
        $('#md-nivel').find('#new-btn').addClass('my-hide');
        let rubrica = arrRendered[index_rubrica];
        if (!rubrica.editable) {
            $('#md-nivel').find('.btn-delete').prop('disabled', true);
            $('#md-nivel').find('#cuantitativo').prop('disabled', true);
        }
    } else {
        $('#md-nivel').find('#autor-btn').addClass('my-hide');
        $('#md-nivel').find("#new-btn").removeClass('my-hide');
    }
    $('#md-nivel').modal('show');
}
function modalCriterio(el) {

    let index = $(el).attr('index');
    let index_rubrica = $(el).attr('index_rubrica');
    idrubricaWorking = $(el).attr('idrubrica');
    if (index) {
        let criterio = arrRendered[index_rubrica].arrCriterio[index];
        $('#nombre-criterio').val(criterio.detalle);
        $('#peso-criterio').val(criterio.peso);
        $('#input-idcriterio').val(criterio.idrubrica_criterio);
        $('#md-criterio').find('#autor-btn').removeClass('my-hide');
        $('#md-criterio').find('#default-btn').removeClass('my-hide');
        $('#md-criterio').find('#new-btn').addClass('my-hide');
        let rubrica = arrRendered[index_rubrica];
        if (!rubrica.editable) {
            $('#md-criterio').find('.btn-delete').prop('disabled', true);
            $('#md-criterio').find('#peso-criterio').prop('disabled', true);
        }
    } else {
        $('#md-criterio').find('#autor-btn').addClass('my-hide');
        $('#md-criterio').find("#new-btn").removeClass('my-hide');
    }
    $('#md-criterio').modal('show');
}
function modalIndicador(el) {

    idrubricaWorking = $(el).attr('idrubrica');
    idnivelWorking = $(el).attr('nivel');
    idcriterioWorking = $(el).attr('criterio');
    let index = $(el).attr('index');
    let index_rubrica = $(el).attr('index_rubrica');
    if (index) {
        let indicador = arrRendered[index_rubrica].arrIndicador[index];
        $('#detalle-indicador').val(indicador.detalle);
        $('#input-idindicador').val(indicador.idrubrica_indicador);
        $('#md-indicador').find('#autor-btn').removeClass('my-hide');
        $('#md-indicador').find('#default-btn').removeClass('my-hide');
        $('#md-indicador').find('#new-btn').addClass('my-hide');
        let rubrica = arrRendered[index_rubrica];
        if (!rubrica.editable) {
            $('#md-indicador').find('.btn-delete').prop('disabled', true);
        }
    } else {
        $('#md-indicador').find('#autor-btn').addClass('my-hide');
        $('#md-indicador').find("#new-btn").removeClass('my-hide');
    }
    $('#md-indicador').modal('show');
}
/* [VER RUBRICA COMO ALUMNO] */
function modalVer() {
    $('#md-ver-rubrica').find("#default-btn").removeClass('my-hide');
    $('#md-ver-rubrica').modal('show')
}
function addOnModalClose() {
    $('#md-nueva-rubrica').on('hidden.bs.modal', function (e) {
        clearForm(e);
    })
    $('#md-nivel').on('hidden.bs.modal', function (e) {
        clearForm(e);
    })
    $('#md-criterio').on('hidden.bs.modal', function (e) {
        clearForm(e);
    })
    $('#md-indicador').on('hidden.bs.modal', function (e) {
        clearForm(e);
    })
}
function clearForm(e) {
    let modal = e.currentTarget;
    let arrInput = $('.my-modal').find('input');
    for (let index = 0; index < arrInput.length; index++) {
        arrInput[index].value = '';
        $(arrInput[index]).prop('disabled', false)
    }
    let arrTextarea = $('.my-modal').find('textarea');
    for (let index = 0; index < arrTextarea.length; index++) {
        arrTextarea[index].value = '';
        $(arrTextarea[index]).prop('disabled', false)
    }
    let arrButton = $('.my-modal').find('button');
    for (let index = 0; index < arrButton.length; index++) {
        arrButton[index].value = '';
        $(arrButton[index]).prop('disabled', false)
    }
    $(modal).find('#autor-btn').addClass('my-hide');
    $(modal).find('#default-btn').addClass('my-hide');
    $(modal).find('#new-btn').addClass('my-hide');
}
function swalSaved(param = {}) {
    param = {
        title: param.hasOwnProperty('title') ? param.title : null,
        timer: param.hasOwnProperty('timer') ? param.timer : null
    }
    if (!param.timer) {
        return;
    }
    Swal.fire({
        icon: 'success',
        title: param.title,
        showConfirmButton: false,
        timer: param.timer,
    })
}