class CC_Vimeo {

    constructor({ idproyecto, oCurso }) {
        this.oMine = new Mine;
        this.idproyecto = idproyecto;
        this.oCurso = oCurso;
    }
    getEstructuraCurso() {
        return new Promise((resolve, reject) => {
            this.oMine.postData({
                url: _sysUrlBase_ + 'json/Vimeo/estructurar',
                data: {
                    params: {
                        idproyecto: this.idproyecto,
                        oCurso: this.oCurso
                    },
                },
                showloading: false
            }).then(rs => {
                resolve(rs);
            });
        });
    }
}