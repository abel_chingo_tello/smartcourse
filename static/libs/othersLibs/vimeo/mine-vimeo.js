class VimeoInterface {
    url = 'https://api.vimeo.com';
    constructor(params) {
        this.headers = {
            'Authorization': 'Bearer ' + params.token,
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.vimeo.*+json;version=3.4'
        };
        this.mine = new Mine;
    }
    interact(params) {
        return new Promise((resolve, reject) => {
            let config = {
                method: 'GET',
                endpoint: '',
                url: null,
                data: {},
                headers: this.headers,
                showloading: false
            };
            Object.assign(config, params);
            if (config.url != null) {
                config.url = config.url
            } else {
                config.url = this.url + config.endpoint;
            }
            this.mine.postData(config).then(rs => {
                resolve(rs);
            }).catch(err => {
                reject(err);
            });
        });
    }
    listFolders(params = {}) {
        return new Promise((resolve, reject) => {
            this.interact({
                endpoint: '/me/projects',
                method: 'GET',
                rqType: 'json',
                rsType: 'metadata',
            }).then((rs) => {
                resolve(rs.body.data);
            });
        });
    }
    createFolder({ name }) {
        return new Promise((resolve, reject) => {
            if (!Mine.hasValue(name)) {
                reject("Especifique el nombre.")
            }
            this.interact({
                endpoint: '/me/projects',
                method: 'POST',
                rqType: 'json',
                rsType: 'metadata',
                data: {
                    name: name,
                }
            }).then((rs) => {
                resolve(rs.body.uri);
            });
        });
    }
    uploadVideo({ video, name }) {
        return new Promise((resolve, reject) => {
            if (!Mine.hasValue(name)) {
                name = video.name;
            }
            if (!Mine.hasValue(video)) {
                reject("Video incorrecto.")
            }
            oVimeoInterface.interact({
                endpoint: '/me/videos?filter=upload',
                method: 'POST',
                rqType: 'json',
                rsType: 'metadata',
                data: {
                    upload: {
                        approach: "tus",
                        size: video.size
                    },
                    name: name,
                }
            }).then(rs => {
                let usefulData = {
                    upload: rs.body.upload,
                    uri: rs.body.uri
                }
                oVimeoInterface.interact({
                    headers: {
                        'Tus-Resumable': '1.0.0',
                        'Upload-Offset': '0',
                        'Content-Type': 'application/offset+octet-stream',
                        'Accept': 'application/vnd.vimeo.*+json;version=3.4'
                    },
                    rqType: '',
                    rsType: 'metadata',
                    url: usefulData.upload.upload_link,
                    method: 'PATCH',
                    data: video
                }).then(rs => {
                    if (rs.headers.hasOwnProperty("upload-offset") &&
                        rs.headers['upload-offset'] == video.size
                    ) {
                        resolve(usefulData.uri);
                    } else {
                        reject("El video no subió completamente, porfavor vuelva a intentarlo.");
                    }
                })
            }).catch(e => {
                console.error(e);
                reject(e);
            });
        });
    }
    getIdFromUri({ uri }) {
        let arrTmp = explode("/", uri);
        let size = arrTmp.length;
        let rs = null;
        if (size > 0) {
            rs = arrTmp[size - 1];
        } else {
            throw "Uri inválida.";
        }
        return rs;
    }
}