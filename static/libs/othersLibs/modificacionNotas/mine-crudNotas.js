class CrudNotas {
    constructor(params = null) {
        this.mine = new Mine;
        this.section = $(document);
        this.debug = false;
        this.webJson = 'json/ModificacionNotas';
        this.selectCursos = this.section.find('#selectCursos');
        this.selectAlumnos = this.section.find('#selectAlumnos');
        this.selectExamenes = this.section.find('#selectExamenes');
        this.table = this.section.find('#table');
        this.md = this.section.find('#md-crudNotas');
        this.mdCrear = this.md.find('div[name="modo-crear"]');
        this.drawTarget = this.section.find('#drawQuiz');
        this.btnSave = this.section.find('#btn-save');
        this.oCurrentPreguntasoffline = [];
        this.oCurrentExamen = null;
        this.msgConf = $('#conf_examen');
        this.totalPreguntas = null;
        this.calificacionPregunta = null;
        this.maxCalExamen = null;
        this.arrConfHabilidadesEx = [];
        this.arrExamenAlumno = null;
        this.oHabilidadPuntajeEx = null;
        this.habilidadesEx = null;
    }
    launch(params = null) {
        this.addEvents();
        this.loadSelectCursos();
    }
    loadSelectCursos() {
        this.getCursosUsuario().then(arrCursos => {
            if (arrCursos.length > 0) {
                let arrTmp = arrCursos.map((curso) => {
                    curso.id = curso.idgrupoauladetalle;
                    curso.text = curso.nombrecurso;
                    return curso;
                });
                arrCursos = arrTmp;
                this.selectCursos.select2({ data: arrCursos })
                this.selectCursos.on("change.select2", () => {//Añadido evento al cambiar
                    let curso = this.selectCursos.select2('data')[0];//obteniendo elemento seleccionado
                    // this.selectCursos.attr('disabled', 'disabled');
                    // this.currentIdCurso=curso.id;
                    this.loadSelectAlumnos(curso.arrAlumnos);
                })
                this.selectCursos.trigger('change.select2');
            } else {
                this.setMsg('No tiene cursos asignados!');
            }
        })
    }
    loadSelectAlumnos(arrAlumnos) {
        if (arrAlumnos.length > 0) {
            let arrTmp = arrAlumnos.map((alumno) => {
                return { id: alumno.idalumno, text: alumno.stralumno }
            });
            arrAlumnos = arrTmp;
            // console.log('arrAlumnos', arrAlumnos);
            this.selectAlumnos.empty();
            this.selectAlumnos.select2({ data: arrAlumnos })
            this.selectAlumnos.on("change.select2", () => {//Añadido evento al cambiar
                let alumno = this.selectAlumnos.select2('data')[0];//obteniendo elemento seleccionado
                let curso = this.selectCursos.select2('data')[0];//obteniendo elemento seleccionado
                this.loadSelectExamenes({
                    curso,
                    idalumno: alumno.id
                });
            })
            this.selectAlumnos.trigger('change.select2');
        } else {
            this.setMsg('Aún no hay alumnos matriculados en el curso!');
        }
    }
    loadSelectExamenes(params) {
        let arrExamenes = params.curso.formula.temasaevaluar.filter(tema => {
            if (tema.tipo == "smartquiz") {
                return tema;
            }
        })

        if (arrExamenes.length > 0) {
            let arrTmp = arrExamenes.map((examen) => {
                examen.text = examen.nombreunico;
                return examen
            });
            arrExamenes = arrTmp;

            this.selectExamenes.empty();
            this.selectExamenes.select2({ data: arrExamenes })
            this.selectExamenes.on("change.select2", () => {//Añadido evento al cambiar
                let examen = this.selectExamenes.select2('data')[0];//obteniendo elemento seleccionado
                this.getExamenesCursoAlumno(params.curso, examen.idexamen, params.idalumno).then(arrExamenAlumno => {
                    this.drawPreguntas(arrExamenAlumno);
                });
                // console.log('examen seleccionado', examen);
            })
            this.selectExamenes.trigger('change.select2');
        } else {
            this.setMsg('Aún no hay examenes en el curso!');
        }
    }
    getExamenesCursoAlumno(curso, idexamen, idalumno) {
        return new Promise((resolve, reject) => {
            Swal.showLoading();
            postData(_sysUrlBase_ + 'json/Mine/getExamenesCursoAlumno',
                {
                    'idcurso': curso.idcurso,
                    'idcomplementario': curso.idcomplementario,
                    'idgrupoauladetalle': curso.idgrupoauladetalle,
                    idexamen, idalumno
                }

                // { idexamen: '499', idalumno: 1 }
            ).then(arrExamenAlumno => {
                Swal.close();
                resolve(arrExamenAlumno);
            })
        })
    }
    drawPreguntas(arrExamenAlumno) {
        this.msgConf.text('');
        console.log('arrExamenAlumno', arrExamenAlumno);
        if (arrExamenAlumno.length > 0) {

            let examen = arrExamenAlumno[0]
            this.oCurrentExamen = examen;

            if (examen.preguntasoffline != null && examen.preguntasoffline != '{}') {
                this.arrExamenAlumno = arrExamenAlumno;
                this.habilidadesEx = JSON.parse(this.arrExamenAlumno[0].habilidades);
                this.oHabilidadPuntajeEx = JSON.parse(this.arrExamenAlumno[0].habilidad_puntaje);
                let oPreguntasoffline = JSON.parse(examen.preguntasoffline);
                this.oCurrentPreguntasoffline = oPreguntasoffline;
                let oPreguntas = JSON.parse(examen.preguntas);


                if (examen.calificacion_total.includes('{')) {
                    this.maxCalExamen = JSON.parse(examen.calificacion_total);
                    // console.log('this.maxCalExamen', this.maxCalExamen);
                    this.maxCalExamen = this.maxCalExamen[0].max;
                } else {
                    this.maxCalExamen = examen.calificacion_total;
                }
                this.maxCalExamen = this.convertirNumero(this.maxCalExamen);

                let arrTmp = Object.entries(oPreguntas);
                let contador = 0;
                let preguntastmp = [];
                let habilidades = [];
                let $item_ = '', $hab = '', $habtem = '';
                arrTmp.forEach(item => {
                    if (!item[1].includes('tmpl-media')) {
                        contador++;
                        $item_ = $(item[1]);
                        $hab = $item_.attr('data-habilidades') || '';
                        $habtem = $hab.split('|');
                        if ($habtem.length)
                            $habtem.forEach(_hab => {
                                if (_hab != '') {
                                    if (habilidades[_hab] == undefined) habilidades[_hab] = 0;
                                    habilidades[_hab]++;
                                }
                            })
                        preguntastmp.push(item[1]);
                    }
                });
                this.arrConfHabilidadesEx = habilidades;
                // console.log('this.arrConfHabilidadesEx=', this.arrConfHabilidadesEx);
                this.totalPreguntas = contador;
                this.calificacionPregunta = (this.maxCalExamen / this.totalPreguntas).toFixed(2);
                this.msgConf.text(`(Máxima calificación por pregunta: ${this.calificacionPregunta}, examen en base ${this.maxCalExamen}, total preguntas: ${this.totalPreguntas})`);

                this.drawTarget.html('');
                for (let i of Object.keys(oPreguntasoffline)) {
                    let pregunta = oPreguntas[i];
                    // let pregunta = decodeURIComponent(escape(window.atob(oPreguntas[i])));
                    pregunta = pregunta.replace(/__xRUTABASEx__/gi, _sysUrlBase_);
                    let $pregunta = $('<div>' + pregunta + '</div>');
                    $pregunta.find('.nopreview').remove();
                    $pregunta.find('.btnadd_recordAlumno').hide();
                    $pregunta.find('.msjRespuesta').removeClass('hidden').show();
                    $pregunta.find('.msjRespuesta').siblings('textarea').hide();
                    $pregunta.find('.btnenformatohtml').hide();
                    pregunta = $pregunta.html();
                    let html = `<div id="card-${i}" class="card my-card my-shadow my-mg-bottom">
                                    <div class="card-header" id="head-${i}">
                                        <h5 class="mb-0">
                                            <button id="btn-collapse-${i}" class="btn btn-link" data-toggle="collapse" data-target="#collapse-${i}" aria-expanded="true" aria-controls="collapse-${i}">
                                                Collapsible Group Item #1
                                            </button>
                                            <span class="cont-nota my-float-r">
                                                <label>Nota: </label>
                                                    <small >
                                                        <a class="preg-nota" href="#!" >
                                                            (Sin calificar)
                                                        </a>
                                                    </small>
                                            </span>
                                        </h5>
                                    </div>
                                    <div id="collapse-${i}" class="collapse show" aria-labelledby="headingOne" data-parent="#drawQuiz">
                                        <div class="card-body">
                                            ${pregunta}
                                        </div>
                                    </div>
                                </div>`;
                    this.drawTarget.append(html);
                    let card = this.drawTarget.find(`#card-${i}`);
                    card.find('.cont-nota').find('a').on('click', (e) => {
                        let nota = card.find('.preg-nota');
                        this.showModal(card, nota.html(), i);
                    })
                    let preg = $(`#collapse-${i}`);
                    let btn = $(`#btn-collapse-${i}`);
                    let titulo = preg.find('h3.titulo');
                    btn.html(titulo);
                    preg.find('h3.titulo').remove();
                }
                this.btnSave.removeAttr('disabled');
                this.btnSave.removeClass('my-hide');
                $('.collapse').collapse({
                    toggle: true
                });

            } else {
                this.setMsg('El alumno no tiene preguntas por revisar en este examen...');
            }
        } else {
            this.setMsg('Este alumno no dió el examen...');
        }
    }
    showModal(card, nota, i) {
        let form = this.md.find('#form-add-crudNotas');
        let input = form.find('#input-nota');
        if (!isNaN(nota)) {
            input.val(nota);
        } else {
            input.val(this.calificacionPregunta);
        }
        form.off("submit");
        form.submit((e) => {
            e.preventDefault();
            let notaPregunta = this.convertirNumero(input.val());

            if (this.validarNota(notaPregunta)) {
                console.log('this.arrConfHabilidadesEx', this.arrConfHabilidadesEx);
                console.log('habilidadesEx', this.habilidadesEx);
                console.log('oHabilidadPuntajeEx', this.oHabilidadPuntajeEx);
                let porcentajeHab = this.notaPorcentaje(notaPregunta);
                console.log('#porcentaje habilidad que corresponde en esta pregunta:', porcentajeHab);
                let strHabPreg = card.find('.card-body').children().attr('data-habilidades');
                if (strHabPreg.substring(strHabPreg.length - 1) == '|') {
                    strHabPreg = strHabPreg.substring(0, strHabPreg.length - 1);
                }
                let valorPrevio = card.attr('data-habilidadevaluada');
                if (typeof valorPrevio !== 'undefined' && valorPrevio != '') {
                    valorPrevio = parseFloat(valorPrevio);
                } else {
                    valorPrevio = 0;
                }
                let arrHabPreg = strHabPreg.split('|')
                arrHabPreg = [...new Set(arrHabPreg)];
                console.log('#arr habilidades que evalúa la pregunta', arrHabPreg);
                let nuevo = 0;
                arrHabPreg.forEach(habilidadPreg => {
                    let cantPreg = this.arrConfHabilidadesEx[habilidadPreg];
                    let oldSuma = parseFloat(this.oHabilidadPuntajeEx[habilidadPreg]);
                    console.log('oldSuma', oldSuma);
                    nuevo = porcentajeHab / cantPreg;
                    console.log('añadiendo', nuevo);
                    let newSuma = (oldSuma - valorPrevio) + nuevo;
                    if (newSuma > 100) {
                        console.warn('SUMA DE % DE HABILIDADES SUPERÓ EL 100%', newSuma);
                        newSuma = 100;//por si falle algo por temas de redondeo
                    }
                    console.log('porcentaje preg hab', newSuma);
                    this.oHabilidadPuntajeEx[habilidadPreg] = newSuma.toFixed(2);
                });
                card.attr('data-habilidadevaluada', nuevo);
                card.find('a.preg-nota').html(notaPregunta);
                this.md.modal('hide');
                delete this.oCurrentPreguntasoffline[i];
            } else {
                swalError({
                    text: 'La nota máxima por pregunta es ' + this.calificacionPregunta
                });
            }
        })
        this.mdCrear.removeClass('my-hide');
        this.md.modal('show');
    }
    notaPorcentaje(nota) {
        return ((parseFloat(nota) / this.calificacionPregunta) * 100);
    }
    convertirNumero(strNota) {
        console.log('convertirNumero', strNota)
        let nota = strNota;
        if (isNaN(strNota)) {//si no es numero
            strNota = strNota.replace(',', '.');
            nota = parseFloat(strNota);
        }
        console.log('retornando', nota);
        return nota;
    }
    validarNota(strNota) {
        let $return = false;
        let nota = this.convertirNumero(strNota);
        if (nota <= this.calificacionPregunta) {
            $return = true;
        }
        return $return;
    }
    calcNota() {
        let sumaPreguntas = 0;
        let nota = -1;
        let notaExamen = parseFloat(this.oCurrentExamen.nota);
        let maxCal = parseFloat(this.oCurrentExamen.calificacion_total);
        let arrPregNotas = this.section.find('.preg-nota');
        console.log('arrPregNotas', arrPregNotas)
        for (let i = 0; i < arrPregNotas.length; i++) {
            const htmlNota = $(arrPregNotas[i]);
            let notaPreg = htmlNota.html()
            notaPreg = this.convertirNumero(notaPreg);
            if (!isNaN(notaPreg)) {
                sumaPreguntas += parseFloat(notaPreg);
            }
        }
        // if ((notaExamen + sumaPreguntas) <= maxCal) {
        nota = notaExamen + sumaPreguntas;
        // }
        return nota;
    }
    saveData() {
        Swal.showLoading();

        let nota = this.calcNota();
        console.log('nota antes:', nota);
        nota = nota.toFixed(1);
        nota = parseFloat(nota);
        console.log('nota ahora:', nota);
        if (nota <= this.maxCalExamen) {
            // let objSave = {
            //     idnota: this.oCurrentExamen.idnota,
            //     campo: 'preguntasoffline',
            //     valor: JSON.stringify(this.oCurrentPreguntasoffline),
            // }
            this.mine.postData({
                url: 'json/Mine/updateData',
                data: {
                    params: {
                        tabla: "notas_quiz",
                        arrUpdate: {
                            nota,
                            preguntasoffline: JSON.stringify(this.oCurrentPreguntasoffline),
                            habilidad_puntaje: JSON.stringify(this.oHabilidadPuntajeEx)
                        },
                        arrFiltros: { idnota: this.oCurrentExamen.idnota }
                    }
                }
            }).then(rs => {
                this.selectExamenes.trigger('change.select2');
                this.oHabilidadPuntajeEx = null;
                Swal.close();
            }).catch(e => {
                swalError();
                console.warn(e);
            })
            // postData(_sysUrlBase_ + 'json/Notas_quiz/setCampo', objSave).then(rs => {
            //     objSave.campo = 'nota';
            //     objSave.valor = nota;
            //     postData(_sysUrlBase_ + 'json/Notas_quiz/setCampo', objSave).then((rs) => {
            //         this.selectExamenes.trigger('change.select2');
            //         Swal.close();
            //     }).catch(e => {
            //         swalError();
            //         console.warn(e);
            //     })
            // }).catch(e => {
            //     swalError();
            //     console.warn(e);
            // })
        } else {
            swalError({
                text: `La suma de las preguntas (${nota}) superan la calificación máxima establecida en el examen (${this.maxCalExamen})`
            });
        }
    }
    getCursosUsuario() {
        return new Promise((resolve, reject) => {
            postData(_sysUrlBase_ + this.webJson).then(arrCursos => {
                // console.log('arrCursos', arrCursos);
                resolve(arrCursos);
            })
        })
    }
    setMsg(msg) {
        this.msgConf.text('');
        this.section.find('#drawQuiz').html(`
            <div id="div-msg" class="my-card my-shadow msg my-mg-bottom">
                ${msg}
            </div>
        `);
        this.btnSave.attr('disabled', 'disabled');
        this.btnSave.addClass('my-hide');
    }
    addEvents() {
        let $this = this;
        let btnOpenMd = this.section.find('#open-md-crudNotas');
        btnOpenMd.on('click', () => {
            this.showModal();
        })
        // $("#form-add-submenu").submit(event => {
        //     event.preventDefault();
        //     $this.saveData();
        // });
        this.md.on('hidden.bs.modal', function (e) {
            $this.clearForm(e);
        })
        this.btnSave.on('click', () => {
            this.saveData();
        })
    }
    clearForm(e) {
        let modal = e.currentTarget;
        let arrInput = $(modal).find('input');
        for (let index = 0; index < arrInput.length; index++) {
            arrInput[index].value = '';
            $(arrInput[index]).prop('disabled', false)
        }
        let arrTextarea = $(modal).find('textarea');
        for (let index = 0; index < arrTextarea.length; index++) {
            arrTextarea[index].value = '';
            $(arrTextarea[index]).prop('disabled', false)
        }
        let arrButton = $(modal).find('button');
        for (let index = 0; index < arrButton.length; index++) {
            arrButton[index].value = '';
            $(arrButton[index]).prop('disabled', false)
        }
        $(modal).find('#autor-btn').addClass('my-hide');
        $(modal).find('#default-btn').addClass('my-hide');
        $(modal).find('#new-btn').addClass('my-hide');
    }

}