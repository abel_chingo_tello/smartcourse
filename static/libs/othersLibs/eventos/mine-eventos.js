var urlModulo = _sysUrlBase_ + `json/evento_programacion`;
var inputFecha = null;//para bug en chrome
var tzoffset = (new Date()).getTimezoneOffset() * 60000;
const today = new Date().toISOString().slice(0, 10)
const currentHour = new Date(Date.now() - tzoffset).toISOString().slice(11, 19)
var launchR1 = null;
var launchR2 = null;
$(document).ready(() => {
    inputFecha = document.getElementById('fecha');//para bug en chrome
    console.log('inputFecha', inputFecha);
    iniciarCalendario();
    postData(urlModulo + `/loadSelects`).then(data => {
        console.log(data);
        let arrTmp = data.arrGrupos.map((grupo) => {
            return { id: grupo.idgrupoaula, text: grupo.strgrupoaula }
        });
        data.arrGrupos = arrTmp;
        arrTmp = data.arrCursos.map((curso) => {
            return { id: curso.idgrupoauladetalle, text: curso.strcurso }
        });
        data.arrCursos = arrTmp;
        // $('#selectList').select2({ data: data.arrGrupos })
        launchR1 = () => {
            console.log($('input:radio[name=radio]:checked').val())
            $('#selectList').empty();
            $('#selectList').select2({ data: data.arrGrupos })
        };
        launchR2 = () => {
            console.log($('input:radio[name=radio]:checked').val())
            $('#selectList').empty();
            $('#selectList').select2({ data: data.arrCursos })
        };
        $('input:radio[name=radio]').change(() => {
            if ($('input:radio[name=radio]:checked').val() === 'idgrupoaula') {
                launchR1();
            } else {
                launchR2();
            }
        })

    })
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        clearForm();
    })
})
function saveEvent() {
    let rpt = validate();
    if (rpt.bool) {
        Swal.fire({
            title: 'Cargando...',
            // html: 'I will close in <b></b> milliseconds.',
            timer: 2000,
            timerProgressBar: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                Swal.showLoading()
                postData(urlModulo + `/guardar`, {
                    id_programacion: $('#id_programacion').val(),
                    fecha: $('#fecha').val(),
                    titulo: $('#titulo').val(),
                    detalle: $('#detalle').val(),
                    hora_comienzo: $('#hora_comienzo').val(),
                    hora_fin: $('#hora_fin').val(),
                    idgrupoaula: $('input:radio[name=radio]:checked').val() === 'idgrupoaula' ? $('#selectList').val() : null,
                    idgrupoauladetalle: $('input:radio[name=radio]:checked').val() === 'idgrupoauladetalle' ? $('#selectList').val() : null,
                }).then(respuesta => {

                    console.log(respuesta);
                    if (respuesta.code === 200) {
                        Swal.hideLoading();
                        console.log(respuesta);
                        $('#exampleModal').modal('hide')
                        Swal.fire({
                            icon: 'success',
                            title: 'Evento guardado!',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        iniciarCalendario();
                    }
                })

            },
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
                console.log('I was closed by the timer')
            }
        })

    } else {
        Swal.fire({
            icon: 'warning',
            title: 'Hay campos por corregir...',
            text: rpt.msj,
        })
    }
    return false;
}
function validate() {

    let fecha = $('#fecha').val();
    let rpt = {
        bool: true, msj: ''
    };
    let hora_comienzo = $('#hora_comienzo').val();
    let hora_fin = $('#hora_fin').val();
    if ((new Date(fecha).getTime() < new Date(today).getTime())) {//está en el pasado
        rpt.bool = false;
        rpt.msj = 'La fecha está en el pasado.';
    }

    if ((new Date(fecha).getTime() == new Date(today).getTime())) {//fecha de hoy
        //la hora ya pasó:
        console.log('hora_comienzo', hora_comienzo);
        console.log('currentHour', currentHour);
        if ((new Date(fecha + ' ' + hora_comienzo).getTime() <= new Date(fecha + ' ' + currentHour).getTime())) {
            rpt.bool = false;
            rpt.msj = 'La hora de comienzo ya pasó.';//Fecha de hoy: 
        }
    }

    if ((new Date(fecha + ' ' + hora_comienzo).getTime() >= new Date(fecha + ' ' + hora_fin).getTime())) {
        rpt.bool = false;
        rpt.msj = 'La hora de comienzo es mayor o igual que la hora de fin.';
    }
    return rpt;
}
function iniciarCalendario() {
    $('#calendar').fullCalendar('destroy');
    $('#calendar').fullCalendar({
        header: {
            // left: 'prev,next today',
            left: '',
            center: 'title',
            // right: 'agendaDay,agendaWeek,month,listMonth'
            // right: 'agendaDay,agendaWeek,month'
        },
        buttonText: {
            today: 'Hoy',
            month: 'Mes',
            week: 'Semana',
            day: 'Día'
            // list: 'Lista'
        },
        events: urlModulo,//"aqui la url con los datos que regresan",
        editable: false,
        selectHelper: true,
        selectable: true,
        droppable: false,
        // weekends: false,
        locale: 'es',
        eventAfterAllRender: function (e) {
            // cuando ya se dibujó en la interfaz
        },
        eventRender: function (event, element) {
            let icon;
            if (event.tipo_evento == 2 || event.tipo_evento == 3) {
                icon = 'fa-video-camera';
            }
            if (event.tipo_evento == 1 && event.idpersona != idpersona) {
                icon = 'fa-graduation-cap';

            }

            //CLASE:

            if (event.idpersona == idpersona) {
                $(element).addClass('my-fc-autor');
            } else {
                $(element).addClass('my-fc-default');
            }

            if ((new Date(event.fecha).getTime() < new Date(today).getTime())) {
                $(element).addClass('my-fc-pasado');
            }

            element.find(".fc-title").prepend(` <i class="fa ${icon}"></i> `);
        },
        dayClick: function (date, jsEvent, view) {
            if ((new Date(date.format()).getTime() >= new Date(today).getTime())) {
                $('#fecha').val(date.format());
                modalNuevo();
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            modalEditar(calEvent);
        }
    });

}
function clearSelect() {
    $('#selectList').empty();
    $('#selectList').select2({ data: [{ id: -1, text: "Seleccione a donde publicar..." }] })
}
function modalNuevo() {
    clearSelect();
    $('#new-btn').removeClass('my-hide');
    $('#cont-select').removeClass('my-hide');
    $('#exampleModal').modal('show');

}
function modalEditar(calEvent) {
    $('#id_programacion').val(calEvent.id_programacion);
    $('#fecha').val(calEvent.fecha);
    $('#titulo').val(calEvent.title);
    $('#autor').val(calEvent.autor)
    $('#detalle').val(calEvent.detalle);
    $('#hora_comienzo').val(calEvent.hora_comienzo);
    $('#hora_fin').val(calEvent.hora_fin);
    if (calEvent.idgrupoaula != null) {
        launchR1();
        $("#r1").prop("checked", true);
        $('#selectList').val(calEvent.idgrupoaula);
        $('#selectList').trigger('change.select2');
    }
    if (calEvent.idgrupoauladetalle != null) {
        launchR2();
        $("#r2").prop("checked", true);
        $('#selectList').val(calEvent.idgrupoauladetalle);
        $('#selectList').trigger('change.select2');
    }

    if (calEvent.idpersona != idpersona) {// NO es AUTOR
        disableModal(calEvent);
        if (calEvent.tipo_evento == 2) {
            disableModal(calEvent, zoom = true);
        }

    } else {//SI ES AUTOR   
        $('#default-btn').removeClass('my-hide');//btn cerrar

        if (calEvent.tipo_evento == 2) {
            disableModal(calEvent, zoom = true);
        } else {
            $('#autor-btn').removeClass('my-hide');
            $('#cont-select').removeClass('my-hide');//muestro select
        }
        if ((new Date(calEvent.fecha).getTime() < new Date(today).getTime())) {
            $('#btn-save').addClass('my-hide');
        }
    }
    $('#exampleModal').modal('show');
}
function disableModal(calEvent, zoom = false) {
    $('#id_programacion').val('');
    $('#default-btn').removeClass('my-hide');
    if (!zoom) {
        $('#cont-autor').removeClass('my-hide');//si no es autor, muestro quién es   
        if (calEvent.idgrupoaula != null) {
            $("#r1").prop("checked", true);
            $("#r1").prop("disabled", true);
            $('#selectList').val(calEvent.idgrupoaula);
        }
        if (calEvent.idgrupoauladetalle != null) {
            $("#r2").prop("checked", true);
            $("#r2").prop("disabled", true);
            $('#selectList').prop("disabled", true);
        }
    } else {
        $('#zoom-url').removeClass('my-hide')
        $('#zoom-url').prop('href', calEvent.zoom_url)
        // $('#zoom-url').prop('href',calEvent.zoom_url)
    }
    if (calEvent.tipo_evento == 3) {
        $('#zoom-url').removeClass('my-hide')
        $('#tipo-conf').html('Conferencia')
        $('#zoom-url').prop('href', calEvent.link_visitor)
    }
    $('#id_programacion').prop("disabled", true);
    $('#fecha').prop("disabled", true);
    $('#titulo').prop("disabled", true);
    $('#detalle').prop("disabled", true);
    $('#hora_comienzo').prop("disabled", true);
    $('#hora_fin').prop("disabled", true);
}
function eliminar() {
    let evento = {
        id_programacion: $('#id_programacion').val(),
    };
    Swal.fire({
        title: 'Estás seguro?',
        text: "Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar!'
    }).then((result) => {
        if (result.value) {
            postData(urlModulo + `/eliminar`, evento).then(rpt => {
                if (rpt.code == 200) {
                    Swal.fire(
                        'Eliminado!',
                        'El evento ha sido eliminado.',
                        'success'
                    );
                    iniciarCalendario();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Algo salió mal...',
                        text: 'Vuelve a intentralo más tarde',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                }
            })

        }
    })
}
function clearForm() {
    $('#btn-save').removeClass('my-hide');
    $('#zoom-url').addClass('my-hide')
    $('#cont-select').addClass('my-hide');
    $('#cont-autor').addClass('my-hide');
    $('#autor-btn').addClass('my-hide');
    $('#new-btn').addClass('my-hide');
    $('#default-btn').addClass('my-hide');
    $('#id_programacion').val('');
    inputFecha.value = '';//para bug en chrome
    $('#titulo').val('');
    $('#detalle').val('');
    $('#hora_comienzo').val('');
    $('#hora_fin').val('');
    $('#selectList').val('');
    $("#r1").prop("checked", false);
    $("#r2").prop("checked", false);

    $('#id_programacion').prop("disabled", false);
    $('#fecha').prop("disabled", false);
    $('#titulo').prop("disabled", false);
    $('#detalle').prop("disabled", false);
    $('#hora_comienzo').prop("disabled", false);
    $('#hora_fin').prop("disabled", false);
    $("#r1").prop("disabled", false);
    $("#r2").prop("disabled", false);
    $('#selectList').prop("disabled", false);
}
