//Object.fromEntries(formData)
class Mine {

    constructor() { }
    postData(params = {}) {
        let config = {
            url: '',
            data: {},
            headers: {},
            method: 'POST',
            requestType: 'EDUMAX',
            rqType: null,
            responseType: '',
            rsType: null,
            mode: 'cors',
            showloading: true,
            silent: false
        };
        
        Object.assign(config, params);
        console.log('configgg',config);
        if (!config.silent && config.showloading) {
            this.showLoading();
        }
        if (config.rqType != null) {
            config.requestType = config.rqType
        }
        if (config.rsType != null) {
            config.responseType = config.rsType
        }
        config.requestType = config.requestType.toUpperCase();
        config.responseType = config.responseType.toUpperCase();
        return new Promise((resolve, reject) => {
            if (!config.headers.hasOwnProperty("Content-Type")) {
                let contentType = this.evaluateTypeData(config.requestType);

                if (!(contentType == '' || contentType == 'EDUMAX')) {
                    config.headers["Content-Type"] = contentType;
                }
            }
            let fetchSetting = {
                method: config.method,
                headers: config.headers,
                mode: config.mode,
            };
            if (config.method.toUpperCase() != 'GET') {
                let body;
                // console.log('es instancia??', config.data instanceof FormData);
                switch (config.requestType) {
                    case "JSON":
                        { body = JSON.stringify(config.data); }
                        break;
                    case "FORM": case "FORMDATA":
                        {
                            if (!(config.data instanceof FormData)) {
                                body = new FormData
                                for (let key of Object.keys(config.data)) {
                                    let value = config.data[key];
                                    if (typeof value === 'object' && value !== null) {
                                        body.append(key, JSON.stringify(config.data[key]));
                                    } else {
                                        body.append(key, config.data[key]);
                                    }
                                }
                            } else {
                                body = config.data;
                            }
                        }
                        break;
                    case "EDUMAX":
                        {
                            if (!(config.data instanceof FormData)) {
                                body = new FormData
                                for (let key of Object.keys(config.data)) {
                                    let value = config.data[key];
                                    if (typeof value === 'object' && value !== null) {
                                        body.append(key, JSON.stringify(config.data[key]));
                                    } else {
                                        body.append(key, config.data[key]);
                                    }
                                }
                            } else {
                                body = config.data;
                            }
                        }
                        break;
                    default:
                        body = config.data;
                        break;
                }
                fetchSetting.body = body;
            }
            console.log('parámetros mandados a fetch:', fetchSetting);
            fetch(config.url, fetchSetting).then(rsp => {
                console.log('--rsp', rsp);
                let headers = this.parseJson(rsp.headers);
                let fetchResponse = {
                    status: rsp.status,
                    headers,
                    body: null
                };
                switch (rsp.status) {
                    case 204: {//sin body de respuesta
                        resolve(fetchResponse);
                        break;
                    }
                    case 200: case 201:
                        {
                            return rsp.json().then(jsonRsp => {
                                switch (config.responseType) {
                                    case 'METADATA': case 'ALL':
                                        fetchResponse.body = jsonRsp;
                                        break;
                                    default:
                                        if (jsonRsp.hasOwnProperty('code')) {
                                            if (jsonRsp.code == "200" || jsonRsp.code == "ok") {
                                                if (jsonRsp.hasOwnProperty("data")) {
                                                    fetchResponse = jsonRsp.data;
                                                    //resolve(jsonRsp.data);
                                                } else if (jsonRsp.hasOwnProperty("msj")) {
                                                    //What the hell is it???
                                                    fetchResponse = jsonRsp;
                                                    //resolve(jsonRsp);
                                                } else {
                                                    fetchResponse = jsonRsp;
                                                    //resolve(jsonRsp); 
                                                }
                                                if (!config.silent && config.showloading) {
                                                    this.hideLoading();
                                                }
                                            } else {
                                                if (!config.silent && config.showloading) {
                                                    this.hideLoading();
                                                    this.swalError();
                                                }
                                                reject(jsonRsp.code);
                                            }
                                        } else {
                                            fetchResponse = jsonRsp;
                                            // resolve(jsonRsp);
                                        }
                                        break;
                                }
                                resolve(fetchResponse);
                            });
                        }
                        break;
                    default:
                        reject(rsp);
                        break;
                }
            }).catch(e => {
                if (!config.silent && config.showloading) {
                    this.hideLoading();
                    this.swalError();
                }
                console.error(e);
                reject(e);
            })
        })
    }
    evaluateTypeData(requestType) {
        let rs;
        switch (requestType) {
            case 'JSON':
                rs = 'application/json';
                break;
            case 'FORM': case 'FORMDATA':
                rs = 'multipart/form-data';
                break;
            case 'EDUMAX':
                rs = 'EDUMAX';
                break;
            case '': case null:
                rs = '';
                break;
            default:
                break;
        }
        return rs;
    }
    parseJson(obj) {
        let json = {};
        obj.forEach(function (val, key) {
            json[key] = val;
        });
        return json;
    }
    showLoading() {
        Swal.fire({
            // title: 'Cargando...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        })
    }
    hideLoading() {
        Swal.close();
    }
    postJson(url, data) {
        return new Promise((resolve, reject) => {
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }).then(rsp => rsp.json()).then(jsonRsp => {
                if (jsonRsp.code == "200" || jsonRsp.code == "ok") {
                    if (jsonRsp.hasOwnProperty("data")) {
                        resolve(jsonRsp.data);
                    } else if (jsonRsp.hasOwnProperty("msj")) {
                        resolve(jsonRsp);
                    } else { resolve(jsonRsp); }
                } else {
                    reject(jsonRsp.code);
                }
            })
        })
    }
    swalSaved() {

    }
    swalError(params = {}) {
        let config = { text: 'Algo saló mal!', footer: 'Porfavor vualva a intentarlo en un momento' };
        Object.assign(config, params);
        Swal.fire({
            icon: 'error',//warning (!)//success
            title: 'Oops...',
            text: config.text,
            footer: config.footer
        })
    }
    static swalConfirm(params = {}) {
        let config = {
            action: 'eliminar',
            fx: () => { }
        }
        Object.assign(config, params)
        Swal.fire({
            title: 'Está seguro?',
            text: "Esta acción no se puede revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Sí, ${config.action}`
        }).then((result) => {
            if (result.value) {
                config.fx();
                // Swal.fire(
                //     'Deleted!',
                //     'Your file has been deleted.',
                //     'success'
                // )
            }
        })
    }
    validUrl(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    }
    static random(min = 10, max = 999) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    static esTareaProyecto(li) {
        return $(li).children('.btn-group').children('i.btnestarea').hasClass('bg-success');
    }
    static getUrlParameters(params = { url, filter }) {//working
        /params.filter=([^&]+)/.exec(params.url)[1];
    }
    insertSelectCursos(params = { target, rol }) {
        let html = /*html*/`
                    <div class="row my-x-center my-hide">
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="">Grupos</label>
                                <select class="form-control" name="" id="selectGrupos">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="selectCursos">Cursos</label>
                                <select class="form-control" name="" id="selectCursos">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="selectGrupoNota">Alumno</label>
                                <select class="form-control" name="" id="selectGrupoNota">
                                </select>
                            </div>
                        </div>
                    </div>`;

        let target = $(`${params.target}`);
        target.html(html);

    }
    static htmlDecode(input) {
        input = input.replace(/&amp;/g, '&');
        input = input.replace(/&lt;/g, '<');
        input = input.replace(/&gt;/g, '>');
        return input;
    }
    static getColors(numeroColores = 1, conf = {
        tipo: 1
    }) {
        let arrColors = [];
        let arrColors1 = [

            '#f06292',
            '#ba68c8',
            '#e57373',//1
            '#9575cd',
            '#7986cb',
            '#64b5f6',
            '#4fc3f7',
            '#4dd0e1',
            '#4db6ac',
            '#81c784',
            '#aed581',
            '#dce775',
            '#fff176',
            '#ffd54f',
            '#ffb74d',
            '#ff8a65',
            '#a1887f',
            '#e0e0e0',
            '#90a4ae',
        ];
        let arrColors2 = [
            '#f06292',//rosado
            '#ff8a65',//anaranjado
            '#dce775',//amarillo claro
            '#4dd0e1',//celeste



            '#64b5f6',//azul dodger claro

            '#e0e0e0',//crema
            '#ffd54f',//amarillo oscuro



            '#ba68c8',//morado
            '#9575cd',//morado azul





            //
            '#e57373',
            '#81c784',


            '#7986cb',

            '#4fc3f7',

            '#4db6ac',

            '#aed581',

            '#fff176',

            '#ffb74d',

            '#a1887f',

            '#90a4ae',
        ];
        if (conf.tipo == 1) {
            arrColors = arrColors1
        } else {
            arrColors = arrColors2
        }
        let arrColorsRs = [];
        for (let i = 0; i < numeroColores; i++) {
            if (i < arrColors.length) {
                arrColorsRs.push(arrColors[i]);
            } else {
                arrColorsRs.push('#4dd0e1');
            }
        }
        return arrColorsRs;
    }
    static romanize(num) {
        var lookup = { M: 1000, CM: 900, D: 500, CD: 400, C: 100, XC: 90, L: 50, XL: 40, X: 10, IX: 9, V: 5, IV: 4, I: 1 }, roman = '', i;
        for (i in lookup) {
            while (num >= lookup[i]) {
                roman += i;
                num -= lookup[i];
            }
        }
        return roman;
    }
    static hasValue(param) {
        let rs = false;
        if (typeof param !== 'undefined' &&
            param != null && param != "") {
            rs = true;
        }
        return rs;
    }
}
/**
if (['afshin', 'saeed', 'larry'].includes(varName)) {
   alert('Hey');
} else {
   alert('Default case');
}
 */