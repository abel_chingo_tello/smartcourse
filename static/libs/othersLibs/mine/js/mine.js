function postData(url, parameters = {}) {
    return new Promise((resolve, reject) => {
        let formData = new FormData;
        for (let key of Object.keys(parameters)) {
            formData.append(key, parameters[key]);
        }
        fetch(url, {
            method: 'POST',
            body: formData
        }).then(rsp => rsp.json()).then(jsonRsp => {
            if (jsonRsp.code == "200" || jsonRsp.code == "ok") {
                if (jsonRsp.hasOwnProperty("data")) {
                    resolve(jsonRsp.data);
                } else if (jsonRsp.hasOwnProperty("msj")) {
                    resolve(jsonRsp);
                } else { resolve(jsonRsp); }
            } else {
                reject(jsonRsp.code);
            }
        }).catch(e => console.error(e))
    })
}
function postJson(url, data) {
    return new Promise((resolve, reject) => {
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(rsp => rsp.json()).then(jsonRsp => {
            if (jsonRsp.code == "200" || jsonRsp.code == "ok") {
                if (jsonRsp.hasOwnProperty("data")) {
                    resolve(jsonRsp.data);
                } else if (jsonRsp.hasOwnProperty("msj")) {
                    resolve(jsonRsp);
                } else { resolve(jsonRsp); }
            } else {
                reject(jsonRsp.code);
            }
        })
    })
}
function swalSaved() {

}
function swalError(params={}) {
    let config= { text: 'Algo saló mal!', footer: 'Porfavor vualva a intentarlo' };
    Object.assign(config,params);
    Swal.fire({
        icon: 'error',//warning (!)//success
        title: 'Oops...',
        text: config.text,
        footer: config.footer
    })
}
function swalConfirm(params = { action: 'eliminar', fx: () => { } }) {
    Swal.fire({
        title: 'Está seguro?',
        text: "Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: `Sí, ${params.action}`
    }).then((result) => {
        if (result.value) {
            params.fx();
            // Swal.fire(
            //     'Deleted!',
            //     'Your file has been deleted.',
            //     'success'
            // )
        }
    })
}
function validUrl(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
}