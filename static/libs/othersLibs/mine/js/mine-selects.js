//Object.fromEntries(formData)
class mySelects {

    constructor() { }

    insertSelectCursos(params = { target, rol, data, onFire }) {
        let html = `<div class="row my-x-center ">`;
        if (params.rol == 1) {
            html += `
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label for="">Grupos</label>
                            <select class="form-control" name="" id="selectGrupos">
                            </select>
                        </div>
                    </div>
                    `;
        }
        if (params.rol <= 3) {
            html += `
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label for="selectCursos">Cursos</label>
                            <select class="form-control" name="" id="selectCursos">
                            </select>
                        </div>
                    </div>
                    `;
        }
        if (params.rol <= 2) {
            html += `
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label for="selectGrupoNota">Alumno</label>
                            <select class="form-control" name="" id="selectGrupoNota">
                            </select>
                        </div>
                    </div>
                    `;
        }
        html += `</div>`;

        let target = $(`${params.target}`);
        console.log('target', target);
        target.html(html);
        if (params.rol <= 3) {
            this.selectCursos = $('#selectCursos');
            this.loadSelectCursos(params.data, params.onFire);
        }
    }
    loadSelectCursos(arrCursos, onFire) {
        if (arrCursos.length > 0) {
            let arrTmp = arrCursos.map((curso) => {
                curso.id = curso.idgrupoauladetalle;
                curso.text = curso.nombrecurso;
                return curso;
            });
            arrCursos = arrTmp;
            this.selectCursos.select2({ data: arrCursos })
            this.selectCursos.on("change.select2", () => {//Añadido evento al cambiar
                let curso = this.selectCursos.select2('data')[0];//obteniendo elemento seleccionado
                onFire(curso);

                // this.clearSelects();
               // let curso = this.selectCursos.select2('data')[0];//obteniendo elemento seleccionado
                // this.loadSelectAlumnos(curso.arrAlumnos);
            })
            this.selectCursos.trigger('change.select2');
        } else {
            this.setMsg('No tiene cursos asignados!');
        }
    }
    loadSelectAlumnos(arrAlumnos) {
        if (arrAlumnos.length > 0) {
            let arrTmp = arrAlumnos.map((alumno) => {
                return { id: alumno.idalumno, text: alumno.stralumno }
            });
            arrAlumnos = arrTmp;
            this.selectAlumnos.empty();
            this.selectAlumnos.select2({ data: arrAlumnos })
            this.selectAlumnos.on("change.select2", () => {//Añadido evento al cambiar
                let alumno = this.selectAlumnos.select2('data')[0];//obteniendo elemento seleccionado
                let curso = this.selectCursos.select2('data')[0];//obteniendo elemento seleccionado
                this.getSmartbookCurso(curso).then(arrSmartBooks => {
                    this.loadSelectSmartBooks({
                        arrSmartBooks,
                        idalumno: alumno.id,
                        idcurso: curso.idcurso
                    });
                })

            })
            this.selectAlumnos.trigger('change.select2');
        } else {
            this.selectAlumnos.select2();
            this.selectSmartbooks.select2();
            this.selectTipo.select2();
            this.setMsg('Aún no hay alumnos matriculados en el curso!');
        }
    }
    clearSelects() {
        this.selectCursos.empty();
        // this.selectAlumnos.empty();
        // this.selectSmartbooks.empty();
        // this.selectTipo.empty();
    }
}
