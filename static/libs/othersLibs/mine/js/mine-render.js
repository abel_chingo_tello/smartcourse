class Render {
    defaults = {
        selector: null,
        model: {},
        instance: { onRendered: () => { } },
        render: true
    };
    options;

    constructor(params = {}) {
        this.options = Object.assign(this.defaults, params)
        this._selector = this.options.selector;
        this._model = this.options.model;
        //
        this._template = null;
        this.contador = 0;
        this.setTemplate();
        if (this.options.render) {
            this.render();
        }


    }
    setTemplate() {
        return new Promise((resolve, reject) => {
            if (this._selector && !this._template) {
                this._template = this._$(this._selector).cloneNode(true);
            }
            resolve();
        })
    }
    get selector() {
        return this._selector;
    }
    set selector(_selector) {
        this._selector = _selector;
        this.setTemplate();
        // this.render();
    }
    get model() {
        return this._model;
    }
    set model(data) {
        this._model = data;
        this.render();
    }
    get template() {
        return this._template;
    }
    _$(selector) {
        return document.querySelector(selector);//[0].find=fx;
    }
    replaceMustache(str) {
        return str.replace(/{{|}}/gi, (s) => {
            return s == '{{' ? '\`+' : '+\`';
        });
    }
    htmlDecode(input) {
        // let strFixed='';
        input = input.replace(/&amp;/g, '&');
        input = input.replace(/&lt;/g, '<');
        input = input.replace(/&gt;/g, '>');
        return input;
    }
    getSafe(fn, defaultVal) {
        try {
            return fn();
        } catch (e) {
            return defaultVal;
        }
    }
    mFor(params) {
        let arrMfor = params.elmTemplate.getAttribute('m-for').split(',');
        let strArray, item = "item", index = "index";

        if (arrMfor.length > 0) {
            strArray = arrMfor[0];
        } else {
            console.error('m-render: m-for sin parámetros')
            return null;
        }
        if (arrMfor.length > 1) {
            item = arrMfor[1];
        }
        if (arrMfor.length > 2) {
            index = arrMfor[2];
        }


        let strLogic = `
        let arrFor=this.getSafe(()=>${strArray},[]);
            let strRender='';
            for (let ${index} = 0; ${index} < arrFor.length; ${index}++) {
                const ${item} = arrFor[${index}];   
                strRender+=\`${params.strContent}\`
            }
            strRender;
        `;

        return strLogic;
    }
    render(params = { norender: 'qwerty-' }) {
        this.setTemplate().then(() => {
            let huboCambios = false;
            let currNode = this._$(this._selector);
            let template = this._template.cloneNode(true);
            let arrElm_Template = this._template.querySelectorAll('[m-render]');
            let arrElmTemplate = template.querySelectorAll('[m-render]');
            let arrElmCurr = currNode.querySelectorAll('[m-render]');
            let idEvent;

            arrElmTemplate.forEach((elmTemplate, i) => {
                if (elmTemplate.id != params.norender) {
                    let strLogic = '';
                    let strContent = this.replaceMustache(this.htmlDecode(elmTemplate.innerHTML));

                    let strEvent;
                    let $ = this._model;
                    let ifRender = true;
                    if (elmTemplate.hasAttribute('m-for')) {
                        strLogic = this.mFor({ elmTemplate, strContent });
                    } else if (elmTemplate.hasAttribute('m-model')) {
                        this.contador++;
                        if (arrElm_Template.item(i).hasAttribute('data-event', idEvent)) {
                            idEvent = arrElm_Template.item(i).getAttribute('data-event');
                        } else {
                            idEvent = this.contador;
                            arrElm_Template.item(i).setAttribute('data-event', idEvent);
                            elmTemplate.setAttribute('data-event', idEvent)
                        }
                        strEvent = `
                            ${elmTemplate.getAttribute('m-model')}=e.target.value; 
                            this.render();
                        `;
                    } else if (elmTemplate.hasAttribute('nomodule')) {
                        strLogic = strContent;
                        elmTemplate = document.createElement('div');
                        elmTemplate.setAttribute('m-render', '');
                    } else {
                        strLogic = `\`${strContent}\`;`;

                    }
                    if (elmTemplate.hasAttribute('m-show')) {
                        let varShow = elmTemplate.getAttribute('m-show');
                        if (!eval(varShow)) {
                            elmTemplate.setAttribute('style', 'display:none')
                        }
                    }
                    if (elmTemplate.hasAttribute('m-attr')) {
                        let attr = elmTemplate.getAttribute('m-attr');
                        let value = elmTemplate.getAttribute('attr-v');
                        let arrAttr = attr.split(',');
                        let arrValue = value.split(',');
                        arrAttr.forEach((attr, i) => {
                            let value = arrValue[i];
                            elmTemplate.setAttribute(attr, eval(value));
                        });
                    }
                    if (elmTemplate.hasAttribute('m-if')) {
                        ifRender = eval(elmTemplate.getAttribute('m-if'))
                        console.log('m-if', ifRender);
                    }
                    if (elmTemplate.hasAttribute('m-else')) {
                        ifRender = !eval(arrElmTemplate.item(i - 1).getAttribute('m-if'))
                        console.log('m-else', ifRender);
                    }
                    // console.log('strLogic', strLogic)
                    let rendered = eval(strLogic);
                    // console.log('rendered',rendered)
                    elmTemplate.innerHTML = `${rendered}`;
                    if (!ifRender) {
                        elmTemplate.outerHTML = '';
                        elmTemplate.innerHTML = '';
                    }
                    if ((arrElmCurr.item(i).outerHTML != elmTemplate.outerHTML)) {
                        arrElmCurr.item(i).outerHTML = elmTemplate.outerHTML;
                        huboCambios = true;
                        if (elmTemplate.hasAttribute('data-event')) {
                            this._$(`[data-event="${idEvent}"]`).addEventListener('keyup', (e) => {
                                eval(strEvent);
                            });
                        }
                    }
                }
            });
            this._$(this._selector).setAttribute('style', 'visibility: visible;')
            this.options.instance.onRendered(huboCambios);
        });
    }
}