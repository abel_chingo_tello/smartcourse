//Object.fromEntries(formData)
class myModal {
    config = {
        name: Mine.random(),
        title: '',
        section: $(document).find('body'),
        content: '',
        onSubmit: () => { },
        onDelete: () => { }
    }
    loading = false;
    constructor(params = {}) {
        Object.assign(this.config, params);
        this.loadModal();
    }
    loadModal() {
        let html = /*html*/`
        <div class="modal fade my-modal soft-rw my-font-all" id="md-${this.config.name}" tabindex="-1" role="dialog" aria-labelledby="md-nivelLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            ${this.config.title}
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body modal-no-scroll">
                        <form id="form-add-${this.config.name}">
                            ${this.config.content != '' ? this.config.content : ''}
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-sm-12  col-md-6">
                                <div name="modo-editar" class="my-hide">
                                    <button name="btn-eliminar" type="button"
                                        class="btn-delete btn btn-danger fa fa-trash my-color-white  my-float-l" data-dismiss="modal"><i
                                            aria-hidden="true"></i> Eliminar</button>
                                    <button name="btn-save" type="submit" form="form-add-${this.config.name}"
                                        class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                            <div   class="col-sm-12 col-md-6">
                                <div name="modo-ver" class="my-hide">
                                    <button name="close-md" type="button" class="btn btn-secondary my-float-r"
                                        data-dismiss="modal">Cerrar
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row my-block">
                                    <div  name="modo-crear" class="my-float-r my-hide">
                                        <button name="close-md"  type="button" class="btn  btn-secondary" data-dismiss="modal">Cancelar</button>
                                        <button name="btn-save" type="submit" form="form-add-${this.config.name}"
                                            class="btn btn-primary">
                                            Crear
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
        this.config.section.append($(html));
        this.md = this.config.section.find(`#md-${this.config.name}`);
        this.addEvents();
    }
    addEvents() {
        let $this = this;
        //Evento al cerrar (limpiar form)
        this.md.on('hidden.bs.modal', function (e) {
            $this.loading = false;
            $this.clearForm(e);
        })
        //evento submit
        this.md.find('form').submit((event) => {
            event.preventDefault();
            if (!$this.loading) {
                $this.loading = true;
                let formData = new FormData(this.md.find('form').get()[0]);
                $this.config.onSubmit(formData);
            }
            $this.hide();
        })
        //evento a botones cerrar
        this.md.find('button[name="btn-eliminar"]').on('click', (event) => {
            Mine.swalConfirm({
                fx: this.config.onDelete
            })

        })
        // this.md.find('button[name="close-md"]').on('click', (event) => {
        //     // let button=$(event.currentTarget);
        //     // $this.hide();
        // })
    }
    clearForm(event) {
        let arrInput = this.md.find('input');
        for (let index = 0; index < arrInput.length; index++) {
            arrInput[index].value = '';
            $(arrInput[index]).prop('disabled', false)
        }
        let arrTextarea = this.md.find('textarea');
        for (let index = 0; index < arrTextarea.length; index++) {
            arrTextarea[index].value = '';
            $(arrTextarea[index]).prop('disabled', false)
        }
        let arrButton = this.md.find('button');
        for (let index = 0; index < arrButton.length; index++) {
            arrButton[index].value = '';
            $(arrButton[index]).prop('disabled', false)
        }
        this.md.find('div[name="modo-editar"]').addClass('my-hide');
        this.md.find('div[name="modo-ver"]').addClass('my-hide');
        this.md.find('div[name="modo-crear"]').addClass('my-hide');
    }
    show(mode = 'crear') {
        this.md.find(`div[name="modo-${mode}"]`).removeClass('my-hide');
        this.md.modal('show');
    }
    hide() {
        this.md.modal('hide');
    }
}