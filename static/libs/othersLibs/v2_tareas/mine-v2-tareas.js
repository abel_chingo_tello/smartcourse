var url = `${_sysUrlBase_}json/reportes/test`; var idproyecto = 7;
//?idalumno=237&tipo=T&

$(document).ready(() => {
    var selectCursos = $('#selectCursos');
    getCursos(idalumno, tipo).then(arrCursos => {
        let arrTmp = arrCursos.map(curso => {
            return { id: curso.idcurso, text: curso.strcurso, arrayTareas: curso.array }
        })
        arrCursos = arrTmp;
        arrCursos[0].selected = true;
        console.log(arrCursos);

        selectCursos.select2({ data: arrCursos })
        selectCursos.on("change.select2", () => {
            let cursoSeleccionado = selectCursos.select2('data');
            console.log('cursoSeleccionado: ', cursoSeleccionado[0]);
            drawTareas(cursoSeleccionado[0]);
        })
        selectCursos.trigger('change.select2');

    })
});

function drawTareas(curso) {
    let htmlTarea = ``;
    curso.arrayTareas.forEach(tarea => {
        console.log('tarea', tarea)
        htmlTarea += `
            <div class="col-sm-12 my-card my-shadow my-mg-center">
                <h3 class="col-sm-12">${tarea.nombre}</h3>
                <div id="row-1" class="row ${tarea.respondido ? `` : `my-hide`}">
                    <div class="col-sm-12 "><span class="second-text">Asignada por </span> ${tarea.nombre_docente} <span class="second-text">el ${tarea.fecha}</span> </div>
                </div>
                <div id="row-2" class="row ">
                    <div class="col-sm-12 ${tarea.nota != "Sin Calificar" ? `my-hide` : ``}">
                        <span class="second-text"> ${tarea.nota} </span>
                    </div>
                    <div class="col-sm-8  ${tarea.nota == "Sin Calificar" ? `my-hide` : ``}">
                        <span class="cont-nota">
                            Calificación: <span class="second-text">Obtuviste un</span><span class="nota"> ${tarea.nota} </span><span class="second-text"> de ${tarea.notabaseen}</span>
                        </span>
                    </div>
                    <div class="col-sm-4  ${tarea.nota == "Sin Calificar" ? `my-hide` : ``}">
                        <span class="cont-fecha-rev second-text my-y-center">
                            Revisada el : ${tarea.fecha_calificacion}
                        </span>
                    </div>
                </div>
                <div id="row-3" class="row">
                    <div class="col-sm-12 "> 
                        ${tarea.respondido ?
                `<span><i class="fa fa-check-circle entregada" aria-hidden="true"></i> Entregada</span>` :
                `<span><i class="fa fa-exclamation-circle pendiente" aria-hidden="true"></i> Pendiente</span>`}
                    </div>
                </div>
            </div>
        `;
    });
    if (htmlTarea == ``) {
        htmlTarea = `
        <div class="col-sm-12 my-card my-shadow my-mg-center">
            <h3 class="col-sm-12 second-text">
            No tienes ${tipo=='T'?'tareas':'proyectos'} en este curso.
            </h3>
        </div>`;
    }
    $('#cont-tareas').html(htmlTarea);
}

function getCursos(idalumno, tipo) {
    let frmData = new FormData;
    frmData.append("idalumno", idalumno);
    frmData.append("tipo", tipo);
    frmData.append("idproyecto", idproyecto);
    return new Promise((resolve, reject) => {
        fetch(url, {
            method: 'POST',
            body: frmData
        }).then(rsp => rsp.json()).then(jsonRsp => {
            if (jsonRsp.code == "200") {
                resolve(jsonRsp.data);
            } else {
                reject(jsonRsp.code);
            }
        })
    })
}

function initSelects() {

}