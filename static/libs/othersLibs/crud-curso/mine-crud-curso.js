class CrudCurso {
    idcurso;
    idcomplementario;
    curso;
    constructor(params = null) {
        this.section = $(document);
        this.identify();
    }
    identify() {
        if (__oCurso.hasOwnProperty('idcurso')) {
            this.curso = __oCurso;
        } else {
            this.curso = {
                avance_secuencial: 0,
                idioma: "ES"
            }
        }
    }
    launchCursoCrear(params = null) {
        console.log('this.curso', this.curso);
        this.idcomplementario = this.curso.idcomplementario;
        this.idcurso = this.curso.idcurso;
        this.checkbox = this.section.find('#check-bloqueo-contenido');
        this.selectIdioma = this.section.find('#selectIdioma');
        this.loadSelectIdioma()
        this.addEventsCC();
        this.getCheckbox().then(isChecked => this.checkbox.prop('checked', isChecked));
    }
    loadSelectIdioma() {
        this.selectIdioma.val(this.curso.idioma);
        this.selectIdioma.select2();
        this.selectIdioma.on("change.select2", () => {//Añadido evento al cambiar
            let idioma = this.selectIdioma.select2('data')[0].id;//obteniendo elemento seleccionado
            this.saveIdioma(idioma);
        })
    }
    addEventsCC() {
        this.checkbox.change((e) => {
            let checked = this.checkbox.is(":checked");
            this.saveCheckboxCC(checked);
        })
    }
    saveCheckboxCC(checked) {
        Swal.showLoading();
        let objSave = {
            idcurso: idcurso,
            icc: IDCOMPLEMENTARIO,
            campo: 'avance_secuencial',
            valor: checked * 1
        }
        postData(_sysUrlBase_ + 'json/acad_curso/setCampo', objSave).then(rs => {
            Swal.close();
        }).catch(e => {
            swalError();
            console.warn(e);
        })
    }
    saveIdioma(idioma) {
        Swal.showLoading();
        let objSave = {
            idcurso: idcurso,
            icc: IDCOMPLEMENTARIO,
            campo: 'idioma',
            valor: idioma
        }
        postData(_sysUrlBase_ + 'json/acad_curso/setCampo', objSave).then(rs => {
            Swal.close();
        }).catch(e => {
            swalError();
            console.warn(e);
        })
    }
    getCheckbox() {
        return new Promise((resolve, reject) => {
            resolve(this.curso.avance_secuencial == "1" ? true : false)
        })
    }
    launchInicioVer(params = null) {
        this.launched = false;
        this.idcomplementario = __idcomplementario;
        this.idcurso = __idcurso;
        this.bloquear = __oCurso.avance_secuencial != 0;
        // this.getCheckbox().then(isChecked => this.bloquear = isChecked);
    }
    isBlocked() {
        return new Promise((resolve, reject) => {
            if (this.bloquear == undefined && !this.launched) {
                console.log('this.bloquear', this.bloquear);
                this.getCheckbox().then(isChecked => {
                    this.bloquear = isChecked
                    this.launched = true;
                    resolve(this.bloquear);
                });
            } else {
                resolve(this.bloquear);
            }
        })
    }
};