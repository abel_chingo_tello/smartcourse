/**
 * GLOBAL VARS
 */

const habilidadxsesion = new HabilidadSession();

function show(id){
    let container = document.querySelector(id);
    if(container.classList.contains('d-none')){
        container.classList.remove('d-none');
        container.style.opacity = 0;
        setTimeout(()=>{
            container.style.opacity = 1;
        },200);
    }
}

/**
 * Events functions
 */

function stepClick(){
    let id = this.dataset.step;
    let selects = this.closest('div.my-shadow').querySelectorAll('.select2-hidden-accessible');
    let continuar = true;
    for(let val of selects){
        if(val.value == '' || val.value == 0){
            continuar = false;
            Swal.fire({ icon: 'warning', title: 'Todos los campos deben ser seleccionados', showConfirmButton: true, });
            break;
        }

    }
    if(!continuar){ return false; }
    show(id);
}
function findClick(){
    show('#preport');
    habilidadxsesion.findhabilidades();
}

/**
 * Initialize events on DOM
 */
function initevents(){
    document.querySelector('.on-continuar').addEventListener('click',stepClick);
    document.querySelector('.on-find').addEventListener('click',findClick);
}
/**
 * MAIN FUNCTION
 */
function main(){
    initevents();
    $('#pstep1').hide();
    show('#pstep2');
    habilidadxsesion.loadSelectGrupos(__usuario.idproyecto);
}
window.addEventListener('load',main);