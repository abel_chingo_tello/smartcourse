class HabilidadSession {
    tablas = new Object;
    configCalculo = new Object;
    _mine = null;
    draw5d7a9d14d8a18;
    _PV;
    _recursos;
    isExamen;

    constructor(){
        this._mine = new Mine;
        this.configCalculo.porcentaje_maximo = 100;
        this.configCalculo.puntaje_maximo = 20;
    }
    drawDataTable(id,key,obj = {}){
        if(this.tablas[key] == null && typeof this.tablas[key] == 'undefined'){
            this.tablas[key] = $(id).DataTable(obj);
        }
        return this.tablas[key];
    }
    findempresas(){
        try{
            let selectElement = $('#slc_empresa');
            let params = new Object;

            params.idpersona = __usuario.idpersona;
            params.estado = 1;

            if((__usuario.idrol == 1 && __usuario.tuser != 's') || (__usuario.idrol != 1)){
                params.idempresa = __usuario.idempresa;
            }

            if(selectElement.hasClass('.select2-hidden-accessible')){
                selectElement.select2('destroy');
            }
            selectElement.empty();
            this._mine.postData({
                url: String.prototype.concat(_sysUrlBase_,"json/bolsa_empresas/buscarempresaxpersona"),
                // url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
                data: params
            }).then(response => {
                if(Object.keys(response).length == 0){
                    Swal.fire({ icon: 'warning', title: 'El usuario no tiene empresas asignadas!', showConfirmButton: true, });
                }else{
                    response = response.map((empresa) => {
                        return { id: empresa.idempresa, text: empresa.nombre,arrProyectos:empresa.proyectos }
                    });
                    selectElement.select2({ data: response });
                    selectElement.on("change.select2", () => {//Añadido evento al cambiar
                        let empresa = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                        this.loadSelectProyectos(empresa.id,empresa.arrProyectos);
                    })
                    selectElement.trigger('change.select2');
                }
            });
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
    loadSelectProyectos(idempresa, arrproy){
        try{
            let selectElement = $('#slc_proyecto');
            
            if(selectElement.hasClass('.select2-hidden-accessible')){
                selectElement.select2('destroy');
            }
            selectElement.empty();

            if(arrproy != null && Object.keys(arrproy).length > 0){
                if(__usuario.idrol != 1){
                    arrproy = arrproy.filter((el)=>el.idproyecto == __usuario.idproyecto);
                }
                let data = arrproy.map((proyecto)=>{
                    let _nom = proyecto.nombre != '' ? proyecto.nombre : proyecto.empresa;
                    return {id:proyecto.idproyecto,text:_nom,idempresa: idempresa};
                });
                selectElement.select2({ data: data });
                selectElement.on("change.select2", () => {//Añadido evento al cambiar
                    let proyecto = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                    this.loadSelectGrupos(proyecto.id);
                });
                selectElement.trigger('change.select2');
            }else{
                Swal.fire({ icon: 'warning', title: 'El usuario no tiene proyectos asignadas!', showConfirmButton: true, });
            }
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
    loadSelectGrupos(idproyecto = 0){
        try{
            let selectElement = $('#slc_grupos');

            if(selectElement.hasClass('.select2-hidden-accessible')){
                selectElement.select2('destroy');
            }
            selectElement.empty();

            this._mine.postData({
                url: String.prototype.concat(_sysUrlBase_,"json/reportes/getCursosUsuario"),
                // url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
                data: { idproyecto: idproyecto }
            }).then(response => {
                if(__usuario.idrol != 1){
                    let arrc = [];
                    let _grupo = [];
                    response.forEach(element => {
                        if(_grupo.find(idgrup=>element.idgrupoaula == idgrup) == null){
                            let cursos = [];

                            for(let val of response){
                                if(val.idgrupoaula == element.idgrupoaula){
                                    cursos.push({
                                        idcurso:val.idcurso,
                                        strcurso:val.strcurso, 
                                        idcomplementario: val.idcomplementario, 
                                        idgrupoauladetalle: val.idgrupoauladetalle
                                    });
                                }
                            }//end for
                            arrc.push({
                                arrCursos: cursos,
                                idgrupoaula: element.idgrupoaula,
                                nombre: element.strgrupoaula,
                                idproyecto: element.idproyecto
                            });
                            _grupo.push(element.idgrupoaula);
                        }
                    });   
                    response = arrc;
                }
                response = response.map((grupo) => {
                    let _cursos = [];
                    if(grupo.arrCursos != null){
                        _cursos = grupo.arrCursos;
                    }else{
                        _cursos.push({
                            idcurso:grupo.idcurso,
                            strcurso:grupo.strcurso, 
                            idcomplementario: grupo.idcomplementario, 
                            idgrupoauladetalle: grupo.idgrupoauladetalle
                        });
                    }
                    return { id: grupo.idgrupoaula, text: (grupo.nombre !=null ? grupo.nombre : grupo.strgrupoaula), arrCursos: _cursos, idproyecto:idproyecto}
                });
                selectElement.select2({ data: response });
                selectElement.on("change.select2", () => {//Añadido evento al cambiar
                    let grupo = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                    this.loadSelectCursos(grupo.arrCursos);
                })
                selectElement.trigger('change.select2');
            });
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
    loadSelectCursos(arr){
        try{
            let selectElement = $('#slc_cursos');
            
            if(selectElement.hasClass('.select2-hidden-accessible')){
                selectElement.select2('destroy');
            }
            selectElement.empty();

            if(arr != null && arr.length > 0){
                let data = arr.map((curso)=>{
                    return {id:curso.idcurso,text:curso.strcurso, idcomplementario: curso.idcomplementario, idgrupoauladetalle: curso.idgrupoauladetalle};
                });
                selectElement.select2({ data: data });
                selectElement.on("change.select2", () => {//Añadido evento al cambiar
                    let curso = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                    this.loadSelectUnidad(curso.id,curso.idcomplementario);
                });
                selectElement.trigger('change.select2');
            }
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
    loadSelectUnidad(idcurso = null, idcomplementario = null ){
        try{
            let selectElement = $('#slc_unidades');
            let selectCurso = $('#slc_cursos').select2('data')[0]

            let params = new Object;

            //parametros
            params.idalumno = __usuario.idpersona;
            if(idcurso != null) params.idcurso = idcurso;
            if(idcomplementario != null) params.idcomplementario = idcomplementario;

            if(selectElement.hasClass('.select2-hidden-accessible')){
                selectElement.select2('destroy');
            }
            if(selectCurso != null && selectCurso.idgrupoauladetalle != null){ params.idgrupoauladetalle = selectCurso.idgrupoauladetalle; }
            else{ console.error("Curso sin idgrupoauladetalle"); }

            selectElement.empty();
            this._mine.postData({
                url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
                // url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
                data: { params: params }
            }).then(response => {
                let datos = response.arrUnidades.map((unidad,index) => {
                    return { id: index, text: unidad.nombre, arrSesiones: unidad.arrSesiones}
                });
                selectElement.select2({ data: datos });
                selectElement.on("change.select2", () => {//Añadido evento al cambiar
                    let unidad = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                    this.loadSelectSession(unidad.arrSesiones);
                })
                selectElement.trigger('change.select2');
            });
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
    loadSelectSession(sesiones){
        try{
            let selectElement = $('#slc_sesiones');
            if(selectElement.hasClass('.select2-hidden-accessible')){
                selectElement.select2('destroy');
            }
            selectElement.empty();
            if(sesiones != null && sesiones.length > 0){
                let arr = sesiones.map((sesion) => {
                    return { id: sesion.idnivel, text: sesion.nombre, arrSmartbook: sesion.arrSmartbook, arrExamenes : sesion.arrExamenes}
                });
                selectElement.select2({ data: arr });
            }
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
    calcularHabilidad(value){
        return parseFloat((value * (this.configCalculo.puntaje_maximo / 100)).toFixed(2));
    }
    findhabilidades(){
        try{
            let selectSesiones = $('#slc_sesiones').select2('data')[0];
            let _reload = (this.tablas.tablereport != null && typeof this.tablas.tablereport != 'undefined');
            this.isExamen = (selectSesiones.arrExamenes != null) ? 1 : 0;
            this._PV = this.isExamen == 1 ? false : (selectSesiones.arrSmartbook.adultos != null ? 'adultos' : 'adolescentes');
            this._recursos = this.isExamen == 1 ? selectSesiones.arrExamenes : (selectSesiones.arrSmartbook.adultos != null ? selectSesiones.arrSmartbook.adultos : selectSesiones.arrSmartbook.adolescentes);
            Swal.fire({
                title: 'Porfavor, espere un momento...',
                allowOutsideClick: false,
                allowEscapeKey: false,
                onBeforeOpen: () => {
                    Swal.showLoading();
                },
            });
            //pintar datatable
            let tabla = this.drawDataTable('#tablereport','tablereport',{
                columns : [
                    {'data': 'Documento'},        
                    {'data': 'Nombre del estudiante'},
                    {'data': 'Listening'},
                    {'data': 'Reading'},
                    {'data': 'Writing'},
                    {'data': 'Speaking'},
                    {'data': 'Total'}
                  ],
                  ajax:{
                    url:_sysUrlBase_+'json/reportes/habilidadxsesiones',
                    type: "post",                
                    data:function(d){
                        d.json=true
                        //filtros
                        d.params ={
                            grupo : $('#slc_cursos').select2('data')[0].idgrupoauladetalle,
                            curso : $('#slc_cursos').select2('data')[0].id,
                            idcomplementario : $('#slc_cursos').select2('data')[0].idcomplementario,
                            idproyecto : (__usuario.idrol == 1) ? $('#slc_proyecto').select2('data')[0].id  : __usuario.idproyecto,
                            pv_smartenglish : habilidadxsesion._PV,
                            idrecurso : habilidadxsesion._recursos,
                            esexamen: habilidadxsesion.isExamen
                        }
                        if(__usuario.idrol == 3){
                            d.params.alumnos = __usuario.idpersona; 
                        }

                        habilidadxsesion.draw5d7a9d14d8a18=d.draw;
                    },
                    dataSrc:function(json){
                      var data=json.data;             
                      var datainfo = new Array();
                      
                      json.draw = habilidadxsesion.draw5d7a9d14d8a18;
                      json.recordsTotal = json.data.length;
                      json.recordsFiltered = json.data.length;

                      for(var i=0;i< data.length; i++){
                        let listeningPuntaje = habilidadxsesion.calcularHabilidad(data[i].Listening);
                        let readingPuntaje = habilidadxsesion.calcularHabilidad(data[i].Reading);
                        let writingPuntaje = habilidadxsesion.calcularHabilidad(data[i].Writing);
                        let speakingPuntaje = habilidadxsesion.calcularHabilidad(data[i].Speaking);

                        let total = (listeningPuntaje + readingPuntaje + writingPuntaje + speakingPuntaje) / 4;

                        datainfo.push({             
                            'Documento':`<div>${data[i].nombredocumento}</div><div>${data[i].documento}</div>`,
                            'Nombre del estudiante' : data[i].nombrecompleto,
                            'Listening' : `<div>${data[i].Listening}% / ${habilidadxsesion.configCalculo.porcentaje_maximo} %</div><div>${listeningPuntaje} pts / ${habilidadxsesion.configCalculo.puntaje_maximo} pts</div>`,
                            'Reading' : `<div>${data[i].Reading}% / ${habilidadxsesion.configCalculo.porcentaje_maximo} %</div><div>${readingPuntaje} pts / ${habilidadxsesion.configCalculo.puntaje_maximo} pts</div>`,
                            'Writing' : `<div>${data[i].Writing}% / ${habilidadxsesion.configCalculo.porcentaje_maximo} %</div><div>${writingPuntaje} pts / ${habilidadxsesion.configCalculo.puntaje_maximo} pts</div>`,
                            'Speaking' : `<div>${data[i].Speaking}% / ${habilidadxsesion.configCalculo.porcentaje_maximo} %</div><div>${speakingPuntaje} pts / ${habilidadxsesion.configCalculo.puntaje_maximo} pts</div>`,
                            'Total' : total.toFixed(2),
                            });
                        }
                      Swal.close();
                      return datainfo }, error: function(d){console.log(d)}
                },
                language: {
                    search: "Buscar:",
                    lengthMenu: "Mostrando las &nbsp _MENU_ &nbsp primeras publicaciones",
                    info: "Mostrando _START_ a _END_ publicaciones de un total de _TOTAL_ ",
                    paginate: {
                        first: "Primera página",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Última página"
                    }
                },
                buttons: [
                    /*'copy', 'csv',*/
                    {
                        extend: 'excel',
                        className: 'btn-dt btn-excel',
                        text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>'
                    }, {
                        extend: 'pdf',
                        className: 'btn-dt btn-pdf',
                        text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>'
                    }, {
                        extend: 'print',
                        className: 'btn-dt btn-print',
                        text: '<i class="fa fa-print" aria-hidden="true"></i>'
                    }
                ]
            });
            if(_reload){
                tabla.ajax.reload();
            }
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
}