class ReporteProgresoHabilidad{
    arrUnidadesCurrent = [];
    constructor() {
        //do it
    }
    findEmpresas(){
        try{
            let selectElement = $('#slc_empresas');

            if(selectElement.hasClass('.select2-hidden-accessible')){
                selectElement.select2('destroy');
            }
            selectElement.empty();

            MineObject.postData({
                url: String.prototype.concat(_sysUrlBase_,"json/bolsa_empresas/buscarempresaxpersona"),
                // url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
                data: { idpersona: __usuario.idpersona,estado:1 }
            }).then(response => {
                response = response.map((empresa) => {
                    return { id: empresa.idempresa, text: empresa.nombre,arrProyectos:empresa.proyectos }
                });
                selectElement.select2({ data: response });
                selectElement.on("change.select2", () => {//Añadido evento al cambiar
                    let empresa = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                    this.loadSelectProyectos(empresa.id,empresa.arrProyectos);
                })
                selectElement.trigger('change.select2');
            });
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
    findCoursesDocentes(idrol = 0, idpersona =0, idproyecto = 0){
        let selectElement = $('#slc_cursos');
        let selectTipohabilidad = document.querySelector('#slc_tipohabilidad');

        if(selectElement.hasClass('.select2-hidden-accessible')){
            selectElement.select2('destroy');
        }
        selectElement.empty();
        $('#container_msjnocurso').hide();
        $('#container_reporte').show();
        MineObject.postData({
            url: String.prototype.concat(_sysUrlBase_,"json/reportes/getCursosUsuario"),
            // url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
            data: { idrol: idrol, idpersona:idpersona, idproyecto:idproyecto }
        }).then(response => {
            if(response.length > 0){
                response = response.map((curso) => {
                    return { id: curso.idgrupoauladetalle, text: curso.nombrecurso,idcurso:curso.idcurso,idcomplementario:curso.idcomplementario, idgrupoauladetalle: curso.idgrupoauladetalle }
                });
                
                selectElement.select2({ data: response });
                selectElement.on("change.select2", () => {//Añadido evento al cambiar
                    let curso = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                    selectTipohabilidad.removeAttribute('disabled');
                    this.loadSelectAlumnos(curso.idcurso,curso.idcomplementario,curso.id);
                })
                selectElement.trigger('change.select2');
            }else{
                $('#container_msjnocurso').show();
                $('#container_reporte').hide();
                Swal.fire({
                    icon: 'warning',
                    title: 'El docente no tiene cursos asignados!',
                    showConfirmButton: true,
                });

            }
        });

    }
    findCourses(idalumno = null, idrol=null,idproyecto=null){
        let _idalumno = __usuario.idpersona,
            selectElement = $('#slc_cursos'),
            selectTipohabilidad = document.querySelector('#slc_tipohabilidad')
        ;
        if(selectElement.hasClass('.select2-hidden-accessible')){
            selectElement.select2('destroy');
        }
        selectElement.empty();
        MineObject.postData({
            url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
            data: { params: { idalumno : _idalumno} }
        }).then(response => {
            response = response.map((curso) => {
                return { id: curso.idgrupoauladetalle, text: curso.nombrecurso,idcurso:curso.idcurso,idcategoria:curso.idcategoria,idcomplementario: curso.idcomplementario, arrExamenes: curso.arrExamenes,arrSmartbook: curso.arrSmartbook, arrUnidades: curso.arrUnidades,idgrupoauladetalle:curso.idgrupoauladetalle }
            });
            selectElement.select2({ data: response });
            selectElement.on("change.select2", () => {//Añadido evento al cambiar
                let curso = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                let arr = selectTipohabilidad.value==1 ? curso.arrExamenes : curso.arrSmartbook;
                selectTipohabilidad.removeAttribute('disabled');
                if(curso.arrSmartbook == null){
                    selectTipohabilidad.value = 1;
                    selectTipohabilidad.setAttribute('disabled',true);
                    arr = selectTipohabilidad.value==1 ? curso.arrExamenes : curso.arrSmartbook;
                }
                selectElement.attr('disabled', 'disabled');
                this.getProgreso(arr,_idalumno,curso.idcurso,curso.idcomplementario);
                this.loadSelectUnit(curso.arrUnidades);
                this.getCompetencias(_idalumno,__usuario.idproyecto,curso.idcurso,curso.idcomplementario, arr,curso.idcategoria);
            })
            selectElement.trigger('change.select2');
        });
    }
    getCompetencias(idalumno = null,idproyecto = null,idcurso = null,idcomplementario = null, arreglo = null,idcategoria = 0){
        try{
            let selectTipohabilidad = document.querySelector('#slc_tipohabilidad');
            let selectCurso = $('#slc_cursos').select2('data')[0]

            let params = new Object;
            //parametros
            params.idalumno = idalumno;
            params.idcurso = idcurso;
            params.idproyecto = idproyecto;
            params.idcomplementario = idcomplementario;
            params.idcategoria = idcategoria;
            if(selectTipohabilidad.value == 1){
                params.arrExamenes = arreglo;
            }else{
                params.arrSmartbook = arreglo;
            }
            if(selectCurso != null && selectCurso.idgrupoauladetalle != null){ params.idgrupoauladetalle = selectCurso.idgrupoauladetalle; }
            else{ console.error("Curso sin idgrupoauladetalle"); }
            if(arreglo != null && Object.keys(arreglo).length > 0){
                MineObject.postData({
                    url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCompetencias"),
                    data: { params: params }
                }).then(response => {
                    let containerElement = document.querySelector('#container-competencias');
                    let row = '';
                    let _capacidades='';
                    if(response.length > 0){
                        response.forEach((el)=>{
                            let _progreso = el.progreso != null ? el.progreso : 0;
                            _capacidades = '';
                            if(el.capacidades.length > 0){
                                el.capacidades.forEach((cri)=>_capacidades = String.prototype.concat(_capacidades,`<li>${cri.nombre}</li>`));
                            }
                            row = String.prototype.concat(row,
                                `
                                <div class="row">
                                    <div class="col-auto">
                                        <h2 style="font-size:2rem;">${el.nombre}</h2>
                                        <h5>Capacidades</h5>
                                        <ul>
                                            ${_capacidades}
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: ${_progreso}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">${_progreso}%</div>
                                        </div>
                                    </div>
                                </div>
                                `
                                );
                        });
                    }//endif response.lenght
                    containerElement.innerHTML = row;
                });
            }
            return true;
        }catch(error){
            console.error("Error in syntax: ",error);
            return false;
        }
    }
    setGraficasRadar(response){
        let objListening;
        let objReading;
        let objWriting;
        let objSpeaking;
        let listeningVal = 0;
        let readingVal = 0;
        let writingVal = 0;
        let speakingVal = 0;

        if(response.length > 0){
            objListening = response.find((el)=> el.skill_name == 'Listening');
            objReading = response.find((el)=> el.skill_name == 'Reading');
            objWriting = response.find((el)=> el.skill_name == 'Writing');
            objSpeaking = response.find((el)=> el.skill_name == 'Speaking');
            
            listeningVal = objListening.progreso >= 100 ? 100: objListening.progreso ;
            readingVal = objReading.progreso >= 100 ? 100: objReading.progreso ;
            writingVal = objWriting.progreso >= 100 ? 100: objWriting.progreso ;
            speakingVal = objSpeaking.progreso >= 100 ? 100: objSpeaking.progreso ;
        }
        
        CircleProgress.listeningCompare.setValue((listeningVal/100));
        CircleProgress.readingCompare.setValue((readingVal/100));
        CircleProgress.writingCompare.setValue((writingVal/100));
        CircleProgress.speakingCompare.setValue((speakingVal/100));

        charts.chartRadarCompare.data.datasets[0].data = [listeningVal,readingVal,writingVal,speakingVal];
        charts.chartRadarCompare.update();
    }
    setGraficas(response){
        let objListening = null;
        let objReading = null;
        let objWriting = null;
        let objSpeaking = null;

        if(response != null && Object.keys(response).length > 0){
            objListening = response.find((el)=> el.skill_name == 'Listening');
            objReading = response.find((el)=> el.skill_name == 'Reading');
            objWriting = response.find((el)=> el.skill_name == 'Writing');
            objSpeaking = response.find((el)=> el.skill_name == 'Speaking');
        }
        
        let listeningVal = objListening != null ? (objListening.progreso >= 100 ? 100: objListening.progreso):0 ;
        let readingVal = objReading != null ? (objReading.progreso >= 100 ? 100: objReading.progreso): 0 ;
        let writingVal = objWriting != null ? (objWriting.progreso >= 100 ? 100: objWriting.progreso): 0 ;
        let speakingVal = objSpeaking != null ? (objSpeaking.progreso >= 100 ? 100: objSpeaking.progreso): 0 ;
        
        ProgresoHabilidad.setProgressBarValue("barListening",listeningVal);
        ProgresoHabilidad.setProgressBarValue("barReading",readingVal);
        ProgresoHabilidad.setProgressBarValue("barWriting",writingVal);
        ProgresoHabilidad.setProgressBarValue("barSpeaking",speakingVal);

        CircleProgress.listeningCurrent.setValue((listeningVal/100));
        CircleProgress.readingCurrent.setValue((readingVal/100));
        CircleProgress.writingCurrent.setValue((writingVal/100));
        CircleProgress.speakingCurrent.setValue((speakingVal/100));

        charts.chartSkillGeneral.data.datasets[0].data = [listeningVal,readingVal,writingVal,speakingVal];
        charts.chartRadarCurrent.data.datasets[0].data = [listeningVal,readingVal,writingVal,speakingVal];

        charts.chartSkillGeneral.update();
        charts.chartRadarCurrent.update();
    }
    initGraficos(){
        charts.chartSkillGeneral = new Chart(document.getElementById('chart_general').getContext('2d'),{
            type: 'doughnut',
            data:{
                labels: ['Listening','Reading','Writing','Speaking'],
                datasets:[
                    {
                        label: 'General Skill',
                        data:[0,0,0,0],
                        backgroundColor:[
                            '#ffbb04',
                            '#c42d1b',
                            '#d9b679',
                            '#2196f3',
                        ],
                        borderColor: [
                            'white'
                        ],
                        borderWidth: 1
                    }
                ]
            }
        });
        charts.chartRadarCompare = new Chart(document.getElementById('radar1'),{
            type:'radar',
            data: {
                labels: ["Listening", "Reading", "Writing", "Speaking"],
                datasets: [{
                    label: "Habilidades obtenidas mediante el progreso",
                    data: [0,0,0,0],
                    backgroundColor: String.prototype.concat('#2196f3',"7a"),
                    pointBackgroundColor: '#2196f3',
                    borderColor: '#2196f3',
                    label: 'Unidad'
                  }],
                  options: {
                    title: {
                      display: false
                    },
                    responsive: true,
                            legend: {
                                position: 'top',
                            },
                            scale: {
                                ticks: {
                                    beginAtZero: true
                                }
                            },
                            animation: {
                                animateRotate: false,
                                animateScale: true
                            }
                  }
            }
        });
    
        charts.chartRadarCurrent = new Chart(document.getElementById('radar2'),{
            type:'radar',
            data: {
                labels: ["Listening", "Reading", "Writing", "Speaking"],
                datasets: [{
                    label: "Habilidades obtenidas mediante el progreso",
                    data: [0,0,0,0],
                    backgroundColor: String.prototype.concat('#c42d1b',"7a"),
                    pointBackgroundColor: '#c42d1b',
                    borderColor: '#c42d1b',
                    label: 'Actual'
                  }],
                  options: {
                    title: {
                      display: false
                    },
                    responsive: true,
                            legend: {
                                position: 'top',
                            },
                            scale: {
                                ticks: {
                                    beginAtZero: true
                                }
                            },
                            animation: {
                                animateRotate: false,
                                animateScale: true
                            }
                  }
            }
        });
    
        CircleProgress.listeningCompare = new RadialProgress(document.getElementById('barListeningC1'), { colorText:"#000000",thick:10,animationSpeed:0.2, fixedTextSize:0.25,colorFg:"#ffbb04" });
        CircleProgress.readingCompare = new RadialProgress(document.getElementById('barReadingC1'), {colorText:"#000000",thick:10,animationSpeed:0.2, fixedTextSize:0.25,colorFg:"#c42d1b" });
        CircleProgress.writingCompare = new RadialProgress(document.getElementById('barWritingC1'), {colorText:"#000000",thick:10,animationSpeed:0.2, fixedTextSize:0.25,colorFg:"#d9b679" });
        CircleProgress.speakingCompare = new RadialProgress(document.getElementById('barSpeakingC1'), {colorText:"#000000",thick:10,animationSpeed:0.2, fixedTextSize:0.25,colorFg:"#2196f3" });
        
        CircleProgress.listeningCurrent = new RadialProgress(document.getElementById('barListeningC2'), { colorText:"#000000",thick:10,animationSpeed:0.2, fixedTextSize:0.25,colorFg:"#ffbb04" });
        CircleProgress.readingCurrent = new RadialProgress(document.getElementById('barReadingC2'), {colorText:"#000000",thick:10,animationSpeed:0.2, fixedTextSize:0.25,colorFg:"#c42d1b" });
        CircleProgress.writingCurrent = new RadialProgress(document.getElementById('barWritingC2'), {colorText:"#000000",thick:10,animationSpeed:0.2, fixedTextSize:0.25,colorFg:"#d9b679" });
        CircleProgress.speakingCurrent = new RadialProgress(document.getElementById('barSpeakingC2'), {colorText:"#000000",thick:10,animationSpeed:0.2, fixedTextSize:0.25,colorFg:"#2196f3" });
    
        ProgresoHabilidad.setProgressBarValue("barListening");
        ProgresoHabilidad.setProgressBarValue("barReading");
        ProgresoHabilidad.setProgressBarValue("barWriting");
        ProgresoHabilidad.setProgressBarValue("barSpeaking");
    
        CircleProgress.listeningCompare.setValue(0);
        CircleProgress.readingCompare.setValue(0);
        CircleProgress.writingCompare.setValue(0);
        CircleProgress.speakingCompare.setValue(0);
    
        CircleProgress.listeningCurrent.setValue(0);
        CircleProgress.readingCurrent.setValue(0);
        CircleProgress.writingCurrent.setValue(0);
        CircleProgress.speakingCurrent.setValue(0);
        
        CircleProgress.listeningCompare.draw(true);
        CircleProgress.readingCompare.draw(true);
        CircleProgress.writingCompare.draw(true);
        CircleProgress.speakingCompare.draw(true);
    
        CircleProgress.listeningCurrent.draw(true);
        CircleProgress.readingCurrent.draw(true);
        CircleProgress.writingCurrent.draw(true);
        CircleProgress.speakingCurrent.draw(true);
    }
    getProgreso(arr,_idalumno = __usuario.idpersona,_idcurso = null,_idcomplementario = null){
        Swal.fire({
            title: 'Porfavor, espere un momento...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
        let selectTipohabilidad = document.querySelector('#slc_tipohabilidad');
        let selectCurso = $('#slc_cursos').select2('data')[0]
        let params = new Object;
        //parametros
        params.idalumno = _idalumno;
        params.idcurso = _idcurso;
        if(_idcomplementario != null) params.idcomplementario = _idcomplementario;
        if(selectTipohabilidad.value == 1){
            params.arrExamenes = arr;
        }else{
            params.arrSmartbook = arr;
        }
        if(selectCurso != null && selectCurso.idgrupoauladetalle != null){ params.idgrupoauladetalle = selectCurso.idgrupoauladetalle; }
        else{ console.error("Curso sin idgrupoauladetalle"); }

        if(arr != null && Object.keys(arr).length > 0){
            MineObject.postData({
                url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getProgresoHabilidad"),
                data: { params: params }
            }).then(response => {
                this.setGraficas(response);
                $('#slc_cursos').removeAttr('disabled');
                Swal.close();
            });
        }else{
            //no hay data setear 0
            $('#slc_cursos').removeAttr('disabled');
            Swal.close();
        }
    }
    getReportexAlumno(idalumno = null,idcurso=null,idcomplementario=null){
        let selectTipohabilidad = document.querySelector('#slc_tipohabilidad');

        MineObject.postData({
            url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
            data: { params: { idalumno : idalumno,idcurso:idcurso,idcomplementario: idcomplementario} }
        }).then(response => {
            let arr = selectTipohabilidad.value == 1 ? response.arrExamenes : response.arrSmartbook;
            if(response.arrSmartbook == null){
                selectTipohabilidad.value = 1;
                selectTipohabilidad.setAttribute('disabled',true);
                arr = response.arrExamenes;
            }
            this.getProgreso(arr,idalumno,idcurso, idcomplementario);
            this.loadSelectUnit(response.arrUnidades);
            this.getCompetencias(idalumno,__usuario.idproyecto,idcurso,idcomplementario, arr);
        });
    }
    loadSelectProyectos(idempresa, arrproy){
        try{
            let selectElement = $('#slc_proyectos');
            
            if(selectElement.hasClass('.select2-hidden-accessible')){
                selectElement.select2('destroy');
            }
            selectElement.empty();

            if(arrproy != null && Object.keys(arrproy).length > 0){
                let data = arrproy.map((proyecto)=>{
                    let _nom = proyecto.nombre != '' ? proyecto.nombre : proyecto.empresa;
                    return {id:proyecto.idproyecto,text:_nom,idempresa: idempresa};
                });
                selectElement.select2({ data: data });
                selectElement.on("change.select2", () => {//Añadido evento al cambiar
                    let proyecto = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                    this.loadSelectDocentes(proyecto.id);
                });
                selectElement.trigger('change.select2');
            }
            return true;
        }catch(error){
            console.error("Error on syntax: ",error);
            return false;
        }
    }
    loadSelectDocentes(idproyecto){
        let selectElement = $('#slc_docentes');
            
        if(selectElement.hasClass('.select2-hidden-accessible')){
            selectElement.select2('destroy');
        }
        selectElement.empty();

        MineObject.postData({
            url: String.prototype.concat(_sysUrlBase_,"json/persona/listado"),
            // url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getCursoProgreso"),
            data: { idproyecto: idproyecto,rol:2 }
        }).then(response => {
            response = response.map((persona) => {
                return { id: persona.idpersona, text: String.prototype.concat(persona.nombre," ",persona.ape_paterno), idproyecto:idproyecto}
            });
            selectElement.select2({ data: response });
            selectElement.on("change.select2", () => {//Añadido evento al cambiar
                let docente = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                this.findCoursesDocentes(2,docente.id,docente.idproyecto);
            })
            selectElement.trigger('change.select2');
        });
    }
    loadSelectAlumnos(idcurso =null,idcomplementario =null, idgrupoauladetalle = null,idproyecto = __usuario.idproyecto){
        let selectElement = $('#slc_estudiante');
        if(selectElement.hasClass('.select2-hidden-accessible')){
            selectElement.select2('destroy');
        }
        selectElement.empty();
        MineObject.postData({
            url: String.prototype.concat(_sysUrlBase_,"json/acad_matricula/listado"),
            data: { idgrupoauladetalle: idgrupoauladetalle,estado:1, idproyecto:idproyecto }
        }).then(response => {
            let arrSelect = response.map((matricula) => {
                return { id: matricula.idalumno, text: matricula.stralumno, idmatricula:matricula.idmatricula, curso: matricula.strcurso,grupoestudio:matricula.strgrupoaula }
            });
            selectElement.select2({ data: arrSelect });
            selectElement.on("change.select2", () => {//Añadido evento al cambiar
                let matricula = selectElement.select2('data')[0];//obteniendo elemento seleccionado
                document.querySelector('#lbl_nomestudiante').textContent = matricula.text;
                document.querySelector('#lbl_cursoestudiante').textContent = matricula.curso;
                document.querySelector('#lbl_grupoestudio').textContent = matricula.grupoestudio;
                
                this.getReportexAlumno(matricula.id,idcurso,idcomplementario);
            })
            selectElement.trigger('change.select2');
        });
    }
    loadSelectUnit(unidades){
        let selectElement = document.querySelector('#slc_unidad');
        let event = document.createEvent("Event");

        while (selectElement.firstChild) {
            selectElement.removeChild(selectElement.firstChild);
        }
        if(unidades != null && Object.keys(unidades).length > 0){
            unidades.forEach((el,i)=>{
                let option = document.createElement('option');
                option.value = i;
                option.textContent = el.nombre;
                selectElement.appendChild(option);
            });
            this.arrUnidadesCurrent = unidades;

            event.initEvent("change", false, true); 
            selectElement.dispatchEvent(event);
        }
    }
    setProgressBarValue(id,val=0){
        try{
            let element = document.getElementById(id);
            element.style.width = String.prototype.concat(val,"%");
            element.textContent = String.prototype.concat(val,"%");
            return true
        }catch(error){
            console.error("Error at syntax setProgressBarValue: ",error);
            return false;
        }
    }
}