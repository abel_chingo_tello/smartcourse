/**
 * MAIN VARS
 */
const charts = new Object;
const CircleProgress  = new Object;
const MineObject = new Mine;
const ProgresoHabilidad = new ReporteProgresoHabilidad();

/**
 * Event Functions
 */
function changeunidad(){
    let selectTipohabilidad = document.querySelector('#slc_tipohabilidad');
    let selectCurso = $('#slc_cursos').select2('data')[0]
    let _idalumno = $('#slc_estudiante').select2('data')[0].id;
    let _idcurso = selectCurso.idcurso;
    let _idcomplementario = selectCurso.idcomplementario;
    let arreglo = (selectTipohabilidad.value == 1) ? ProgresoHabilidad.arrUnidadesCurrent[this.value].arrExamenes : ProgresoHabilidad.arrUnidadesCurrent[this.value].arrSmartbook;
    let params = new Object;
    //parametros
    params.idalumno = _idalumno;
    params.idcurso = _idcurso;
    params.idcomplementario = _idcomplementario;
    if(selectTipohabilidad.value == 1){
        params.arrExamenes = arreglo;
    }else{
        params.arrSmartbook = arreglo;
    }
    if(selectCurso != null && selectCurso.idgrupoauladetalle != null){ params.idgrupoauladetalle = selectCurso.idgrupoauladetalle; }
    else{ console.error("Curso sin idgrupoauladetalle"); }
    if(arreglo != null && Object.keys(arreglo).length > 0){
        MineObject.postData({
            url: String.prototype.concat(_sysUrlBase_,"json/reporteModular/getProgresoHabilidad"),
            data: { params: params }
        }).then(response => {
            ProgresoHabilidad.setGraficasRadar(response);
        });
    }
}
function changeTipohabilidad(){
    $('#slc_estudiante').trigger('change');
}
/**
 * Initialize events on DOM
 */
function initEvents(){
    document.querySelector('#slc_unidad').addEventListener('change',changeunidad);
    document.querySelector('#slc_tipohabilidad').addEventListener('change',changeTipohabilidad);
}
function main(){
    initEvents();
    ProgresoHabilidad.findCoursesDocentes();
    ProgresoHabilidad.initGraficos();
}
window.addEventListener('load',main);