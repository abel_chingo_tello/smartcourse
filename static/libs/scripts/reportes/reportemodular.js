/**
 * Requiere mine2.js de /libs/othersLibs/mine/js/
 */
class reportemodular{
    constructor(session = null){
        this._mine = new Mine; // mine js
        this.reportes = [];
        this.usuario = session == null ? JSON.parse(window.localStorage.getItem('usersesion')) : session;
    }
    /**
    * Funcion que se dedica a buscar los reportes asignados en la base de datos
    * @param Array opciones que definiran algunos filtros en la busqueda
    * @return Array El resultado de la busqueda
    */
    findAssign(options = {}){
        try{
            let params = new Object;
            let setting = {
                general: false,
                skip:[]
            }
            
            Object.assign(setting,options);

            params.estado = 1;
            params.idbolsa_empresa = this.usuario.idempresa;
            
            if(!setting.general){ params.idproyecto = this.usuario.idproyecto }
            
            //Definir los parametros del usuario
            if(options.params != null){ Object.assign(params, options.params); }

            if(params.idproyecto == null){ params.nullidproyecto = 1; }

            return this._mine.postData({
                url: String.prototype.concat(_sysUrlBase_,"json/reportesasign/asignaciones"),
                data: params
            }).then(response => {
                let result = [];
                if(response != null && Object.keys(response).length > 0 ){
                    response = response.filter(el=> (setting.skip.find(f=> f== el.idreportegeneral) == null ? true : false));
                    this.reportes = response;
                    return response;
                }
                return result;
            });
        }catch(error){
            console.error("Error on syntax: ", error);
            return null;
        }
    }
    
}