
tinymce.PluginManager.add('chingoimage', function(editor, url){ 
    function showDialog(){
        var rutabase=_sysUrlBase_;
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        data = getData(editor.selection.getNode());
        var obj=data["data-mce-object"];
        if(obj!='image'&&obj!=undefined) {            
           alert('Selected image');
            return false;
        }
        var txt="444";
        var data={
            titulo:' - Image',
            url: rutabase+'/biblioteca/?plt=tinymce&type=image&robj'+obj,
            ventanaid:'biblioteca-image',
            borrar:true
        }
        var modal=sysmodal(data);
    }

    function htmlToData(html) {
        var data = {};        
        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {                             
                data = tinymce.extend(attrs.map, data);              
            }
        }).parse(html);
        return data;
    }

    function getData(element) {
        if (element.getAttribute('data-mce-object')){            
            var $=tinymce.dom.DomQuery;
            var jedit=$(tinymce.activeEditor.getBody());
            var el = $(element);
            var tipo=el.attr('data-mce-object');
            var img=$(jedit).find('img');
            $(img).removeClass('mce-imageinchanged'+editor.id+' mce-inputinchanged'+editor.id+' mce-audioinchanged'+editor.id+' mce-videoinchanged'+editor.id);
            el.addClass('mce-'+tipo+'inchanged'+editor.id);
            return htmlToData(editor.serializer.serialize(element, {selection: true}));
        }
        return {};
    }

    editor.addButton('chingoimage', {
        tooltip: 'Insert/edit image',
        icon: 'fa fa-photo',
        onclick: showDialog,
        stateSelector: ['img[data-mce-object=image]']
    });
    editor.addMenuItem('chingoimage', {
        icon: 'fa fa-photo',
        text: 'Insert/edit image',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceImage', showDialog);
    this.showDialog = showDialog;
});