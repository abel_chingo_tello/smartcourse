tinymce.PluginManager.add('chingoinput', function(editor, url) { 

    var embedChange = (tinymce.Env.ie && tinymce.Env.ie <= 8) ? 'onChange' : 'onInput';
    function showDialog(){
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var txt=element.getContent(),audiourl='';
        data = getData(editor.selection.getNode());
        var obj=data["data-mce-object"];
        if(obj==undefined){
            if(txt==''||txt==undefined){
                alert('Selected text or object input');
                return false;
            }
        }else if(obj!='input') {            
           alert('Selected text or object input');
            return false;
        }
        if(data['data-texto'])txt=data['data-texto'];
        var _css=[],_option;  
        //inicilizando por defecto;
        var _ayuda=true; 
        var _write=true;
        var _visible=false;  
        if(data["class"]){var _css=data["class"].split(" ");}
        if(obj=='input'){
           _ayuda=_css.indexOf("isayuda")>=0?true:false; 
           _write=_css.indexOf("iswrite")>=0?true:false;
           _visible=_css.indexOf("ishidden")>=0?true:false;
        }

        var _action=_css.indexOf("isdrop")>=0?"isdrop":(_css.indexOf("ischoice")>=0?"ischoice":(_css.indexOf("isclicked")>=0?"isclicked":''));
       
        if(data['data-options'])_option=data['data-options']; 
        var list=[
                    {text: 'No action', value: ''},
                    {text: 'Drag and drop', value: 'isdrop'},
                    {text: 'Choices', value: 'ischoice'},
                    {text: 'Alternatives', value: 'isclicked'}
                ];
         
        var generalFormItems = [
            {name: 'texto',  type: 'textbox', size: 40, autofocus: true, label: 'texto', value:txt.trim(), placeholder:'correct text'},
            {type: "container",layout: "flex", direction: "row", pack:"justify", 
             items: [                    
                    {name: 'visible', type: 'checkbox', checked: _visible, text: 'Is hidden'},
                    {name: 'ayuda', type: 'checkbox', checked: _ayuda, text: 'Show Help'},
                    {name: 'write', type: 'checkbox', checked: _write, text: 'Is write'}]
            },
            {name: 'action', label:'Action',type: 'listbox', values: list,value:_action},           
            {name: 'options', type: 'textbox', label: 'Alternatives' , placeholder:'Ej: option1,option2' ,value:_option},
            /*{type: "container",layout: "flex", direction: "row", pack:"justify", label:'Audio' ,
             items: [                    
                    {name: 'audio', type: 'textbox',label:'Audio',disabled:true,classes:'audio_'+editor.id, value:audiourl},
                    {name: 'fileaaudio', type: 'button', icon:'fa fa-search', text:'Search', title: 'Audio Search',onclick:verdialogo}
                    //{name: 'playaudio', type: 'button', icon: 'fa fa-play', title: 'Audio Play',onclick:playaudio} ]
            }*/
        ];
       
        win = editor.windowManager.open({
            title: 'Insert/edit Input',
            data: data,
            body: [
                {
                    title: 'General',
                    type: "form",                    
                    items: generalFormItems
                }
            ],
            onSubmit: function() {
                var beforeObjects, afterObjects, i, y;
                beforeObjects = editor.dom.select('input[data-mce-object]');
                editor.insertContent(dataToHtml(this.toJSON()));
                afterObjects = editor.dom.select('input[data-mce-object]');

                // Find new image placeholder so we can select it
                for (i = 0; i < beforeObjects.length; i++) {
                    for (y = afterObjects.length - 1; y >= 0; y--) {
                        if (beforeObjects[i] == afterObjects[y]) {
                            afterObjects.splice(y, 1);
                        }
                    }
                }                
                editor.selection.select(afterObjects[0]);
                editor.nodeChanged();
            }
        });
    }

    function verdialogo(){  
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var frmItems=[{name: 'url',  type: 'textbox', size: 40, autofocus: true, label: 'Url'}];
        var rutabase=_sysUrlBase_;
        rutabase=tinymce.baseURL+'/biblioteca/?plt=tinymce&type=audio&ideditor='+editor.id;
        win = editor.windowManager.open({
            title: 'Selected Audio',
            url: rutabase,
            width: 400,
            height: 600
        },{
            editor:editor,
            rtype:'namefile',
            editoractivo:tinymce.activeEditor,
        });
    }


    function getSource() {
        var elm = editor.selection.getNode();
        if (elm.getAttribute('data-mce-object')) {
            return editor.selection.getContent();
        }
    }

    function dataToHtml(data) {
        var html = '',_addcss='';
        if (!data.texto) {
            data.texto = '';
        }
        var audio='';
        if (data.audio) {
            audio = 'data-audio="'+data.audio+'"';
        }
        if (data.visible==true) _addcss += ' ishidden';
        if (data.ayuda==true) _addcss += ' isayuda';
        if (data.write==true) _addcss += ' iswrite';
        //if (data.action) 
        _addcss += ' '+data.action;    
        if (!data.options) {
            data.options = '';
        }
        html='<input type="text" '+audio+' data-texto="'+data.texto+'" data-options="'+data.options+'" class="mce-object-input valinput '+_addcss+'" />'; 
        return html;
    }

    function htmlToData(html) {
        var data = {};        
        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {                             
                data = tinymce.extend(attrs.map, data);              
            }
        }).parse(html);
        return data;
    }

    function getData(element) {
        if (element.getAttribute('data-mce-object')) {
            return htmlToData(editor.serializer.serialize(element, {selection: true}));
        }

        return {};
    }

    function sanitize(html) {
        if (editor.settings.media_filter_html === false) {
            return html;
        }

        var writer = new tinymce.html.Writer(), blocked;

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: false,
            special: 'script,noscript',

            comment: function(text) {
                writer.comment(text);
            },

            cdata: function(text) {
                writer.cdata(text);
            },

            text: function(text, raw) {
                writer.text(text, raw);
            },

            start: function(name, attrs, empty) {
                blocked = true;

                for (var i = 0; i < attrs.length; i++) {
                    if (attrs[i].name.indexOf('on') === 0) {
                        return;
                    }

                    if (attrs[i].name == 'style') {
                        attrs[i].value = editor.dom.serializeStyle(editor.dom.parseStyle(attrs[i].value), name);
                    }
                }

                writer.start(name, attrs, empty);
                blocked = false;
            },

            end: function(name) {
                if (blocked) {
                    return;
                }

                writer.end(name);
            }
        }, new tinymce.html.Schema({})).parse(html);

        return writer.getContent();
    }

    function updateHtml(html, data, updateAll) {
        var writer = new tinymce.html.Writer();
        var sourceCount = 0, hasImage;

        function setAttributes(attrs, updatedAttrs) {
            var name, i, value, attr;

            for (name in updatedAttrs) {
                value = "" + updatedAttrs[name];

                if (attrs.map[name]) {
                    i = attrs.length;
                    while (i--) {
                        attr = attrs[i];

                        if (attr.name == name) {
                            if (value) {
                                attrs.map[name] = value;
                                attr.value = value;
                            } else {
                                delete attrs.map[name];
                                attrs.splice(i, 1);
                            }
                        }
                    }
                } else if (value) {
                    attrs.push({
                        name: name,
                        value: value
                    });

                    attrs.map[name] = value;
                }
            }
        }

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',

            comment: function(text) {
                writer.comment(text);
            },

            cdata: function(text) {
                writer.cdata(text);
            },

            text: function(text, raw) {
                writer.text(text, raw);
            },

            start: function(name, attrs, empty){

                if (updateAll){
                    setAttributes(attrs, {
                        texto: data.texto,
                        ayuda: data.ayuda,
                        visible: data.visible,
                        write: data.write,
                        action: data.action,
                        audio:data.audio
                    });
                }
                writer.start(name, attrs, empty);
            },

            end: function(name) {
                if (name == "input" && updateAll && !hasImage) {
                    var imgAttrs = [];
                    imgAttrs.map = {};
                    writer.start("img", imgAttrs, true);
                }

                writer.end(name);
            }
        }, new tinymce.html.Schema({})).parse(html);

        return writer.getContent();
    }

    editor.on('ResolveName', function(e) {
        var name;
        if (e.target.nodeType == 1 && (name = e.target.getAttribute("data-mce-object"))) {
            e.name = name;
        }
    });

    editor.on('preInit', function() {
         // Converts iframe, video etc into placeholder images
        editor.parser.addNodeFilter('input', function(nodes, name) {
            var i = nodes.length, ai, node, placeHolder, attrName, attrValue, attribs, innerHtml; 
            while (i--) {
                node = nodes[i];
                if (!node.parent) {
                    continue;
                }                
                placeHolder = new tinymce.html.Node('img', 1);
                placeHolder.shortEnded = true;
                attribs = node.attributes;
                ai = attribs.length;
                while (ai--) {
                    attrName = attribs[ai].name;
                    attrValue = attribs[ai].value;
                    placeHolder.attr(attrName, attrValue);
                    
                }

                placeHolder.attr({
                    width: "100",
                    height:  "25" ,
                    style: node.attr('style'),
                    "data-mce-object": name,
                });

                node.replace(placeHolder);
            }
        });

        // Replaces placeholder images with real elements for video, object, iframe etc
        editor.serializer.addAttributeFilter('data-mce-object', function(nodes, name) {
            var i = nodes.length, node, realElm, ai, attribs, innerHtml, innerNode, realElmName;

            while (i--) {
                node = nodes[i];
                if (!node.parent) {
                    continue;
                }
                attribs = node.attributes;
                if(attribs==undefined)continue;
                var obj=attribs.map['data-mce-object'];
                var audio=null;
                if(obj==='audio'||obj==='video'){                    
                    realElmName='div';
                    audio=new tinymce.html.Node(obj,1);
                }
                else realElmName = node.attr(name);

                realElm = new tinymce.html.Node(realElmName, 1);
                ai = attribs.length;
                while (ai--) {
                    var attrName = attribs[ai].name;                    
                    if(obj==='audio'||obj==='video'){
                        if(attrName=='data-src') audio.attr('src',attribs[ai].value);
                        if(attrName=='alt') audio.attr('alt',attribs[ai].value);
                    }
                    realElm.attr(attrName, attribs[ai].value);                                  
                }
                if(obj==='audio'||obj==='video'){
                   audio.attr('controls','true');                   
                   if(obj=='video'){
                    audio.attr('class','col-md-12 col-sm-12 col-xs-12 data-mce-video embed-responsive-item');
                    realElm.attr('class','col-md-12 col-sm-12 col-xs-12 embed-responsive embed-responsive-16by9 mce-object-video');
                   }
                   if(obj=='audio'){
                    audio.attr('class','data-mce-audio');
                    realElm.attr('class','mce-object-audio');
                   }
                   realElm.attr('alt',obj+' Player');
                   realElm.attr('style',null);
                   realElm.attr('data-src',null);
                   realElm.append(audio); 
                }          
                node.replace(realElm);                
            }
        });
    });

    
    editor.on('ObjectSelected', function(e) {
        var objectType = e.target.getAttribute('data-mce-object');
    });    
    editor.addButton('chingoinput', {
        tooltip: 'Insert/edit textbox',
        icon: 'fa fa-edit',
        onclick: showDialog,
        stateSelector: ['img[data-mce-object=input]']
    });
    editor.addMenuItem('chingoinput', {
        icon: 'fa fa-edit',
        text: 'Insert/edit textbox',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceInput', showDialog);
    this.showDialog = showDialog;
});