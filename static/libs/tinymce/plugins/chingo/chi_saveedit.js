tinymce.PluginManager.add('chingosave', function(editor, url) { 
    function showDialog(){
        $('.tabhijo.active .aquicargaplantilla .btnsavehtmlsystem').trigger('click');        
    }
  
    editor.addButton('chingosave', {
        tooltip: 'Save edit',
        icon: 'fa fa-save',
        onclick: showDialog,
    });
    editor.addMenuItem('chingosave', {
        icon: 'fa fa-save',
        text: 'Save',
        onclick: showDialog,
        context: 'Save',
        prependToContext: true
    });
    editor.addCommand('mceSave', showDialog);
    this.showDialog = showDialog;
});