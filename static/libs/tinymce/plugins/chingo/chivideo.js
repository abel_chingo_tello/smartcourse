tinymce.PluginManager.add('chingovideo', function(editor, url){ 
    function showDialog(){
        var rutabase=_sysUrlBase_;
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var txt=element.getContent(),audiourl='';
        data = getData(editor.selection.getNode());
        var obj=data["data-mce-object"];
        if(obj=='input'||obj=='image') {
            alert('Selected video');
            return false;
        }
        win = editor.windowManager.open({
            title: 'Selected Video',
            url: rutabase+'/biblioteca/?plt=tinymce&type=video',
            width: 400,
            height: 600
           // onclose:cerrarpanel
        },{
            editor:editor,
            obj:obj
        });
    }
    function htmlToData(html) {
        var data = {};        
        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {                             
                data = tinymce.extend(attrs.map, data);              
            }
        }).parse(html);
        return data;
    }
    function getData(element) {
        if (element.getAttribute('data-mce-object')){            
            var $=tinymce.dom.DomQuery;
            var jedit=$(tinymce.activeEditor.getBody());
            var el = $(element);
            var tipo=el.attr('data-mce-object');
            var img=$(jedit).find('img');
            $(img).removeClass('mce-imageinchanged'+editor.id+' mce-inputinchanged'+editor.id+' mce-audioinchanged'+editor.id+' mce-videoinchanged'+editor.id);            
            el.addClass('mce-'+tipo+'inchanged'+editor.id);
            return htmlToData(editor.serializer.serialize(element, {selection: true}));
        }
        return {};
    }
    editor.addButton('chingovideo', {
        tooltip: 'Insert/edit video',
        icon: 'fa fa-video-camera',
        onclick: showDialog,
        stateSelector: ['img[data-mce-object=video]']
    });
    editor.addMenuItem('chingovideo', {
        icon: 'fa fa-video-camera',
        text: 'Insert/edit video',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceVideo', showDialog);
    this.showDialog = showDialog;
});