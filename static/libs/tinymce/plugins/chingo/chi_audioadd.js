tinymce.PluginManager.add('chingoaudio', function(editor, url){ 
    function showDialog(){
        var rutabase=_sysUrlBase_;
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var txt=element.getContent(),audiourl='';
        data = getData(editor.selection.getNode());
        var obj=data["data-mce-object"];  

        var txt="444";
        var data={
            titulo:' - Audio',
            url: rutabase+'/biblioteca/?plt=tinymce&type=audio&robj'+obj,
            ventanaid:'biblioteca-audio',
            borrar:true
        }
        var modal=sysmodal(data);

    }


    function htmlToData(html) {
        var data = {};        
        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {                             
                data = tinymce.extend(attrs.map, data);              
            }
        }).parse(html);
        return data;
    }
    function getData(element) {
        if (element.getAttribute('data-mce-object')){            
            var $=tinymce.dom.DomQuery;
            var jedit=$(tinymce.activeEditor.getBody());
            var el = $(element);
            var tipo=el.attr('data-mce-object');
            var img=$(jedit).find('img');
            $(img).removeClass('mce-imageinchanged'+editor.id+' mce-inputinchanged'+editor.id+' mce-audioinchanged'+editor.id+' mce-videoinchanged'+editor.id);            
            el.addClass('mce-'+tipo+'inchanged'+editor.id);
            return htmlToData(editor.serializer.serialize(element, {selection: true}));
        }
        return {};
    }

    editor.addButton('chingoaudio', {
        tooltip: 'Insert/edit Audio',
        icon: 'fa fa-music',
        onclick: showDialog,
        stateSelector: ['img[data-mce-object=audio]']
    });
    editor.addMenuItem('chingoaudio', {
        icon: 'fa fa-music',
        text: 'Insert/edit Audio',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceAudio', showDialog);
    this.showDialog = showDialog;
});