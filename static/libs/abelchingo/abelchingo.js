//(function(w,d){
function _typeof(obj){
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol"){
    _typeof=function(obj){return typeof obj;};
  }else{
    _typeof = function(obj){
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }
  return _typeof(obj);
}
function isNumber(value){ return typeof value === 'number' && !isNaN(value); }
function isObject(value){ return _typeof(value) === 'object' && value !== null; }
function isFunction(value){ return typeof value === 'function'; }
function toArray(value){ return Array.from ? Array.from(value) : slice.call(value); }
function forEach(data, callback){
  if(data && isFunction(callback)){
    if(Array.isArray(data) || isNumber(data.length) /* array-like */){
        toArray(data).forEach(function(value, key){callback.call(data, value, key, data); });
    }else if(isObject(data)){
      Object.keys(data).forEach(function(key){callback.call(data, data[key], key, data); });
    }
  }
  return data;
}

String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

class _acht_{
	version=1;
	elementos=null;
	constructor(ele){
		this.selector(ele)
	}
	selector(ele){
		if(ele==''||ele==undefined)	return ;
		this.elementos=document.querySelectorAll(ele);
		if(this.elementos.length){				
			this.elementos.forEach((el)=>{
			})
		}
		return this.elementos;
	}
	assign(opt){
		return assign(this.config,opt);
	}
	addmetodos(el,opt){ // metodos generales
		var el=el;
		el.htmlAdd=this.htmlAdd();			
	}
	htmlAdd(el,html=''){
		console.log(9);
	}
	addEventos(elems,event,fn){
        elems.forEach((el, i) => {
            el.addEventListener(event, (ev)=>{fn(ev)}, false);
        })
    }
	    
    newEvento(el,eventadd,fun){	    	
		var event = document.createEvent('Event');// Creamos el evento.			
		event.initEvent(eventadd, true, true);/* Definimos el nombre del evento que es 'eventadd'.*/			
		el.addEventListener(eventadd, function (e){ fun(e);}, false);// Asignamos el evento.			
		el.dispatchEvent(event); // target can be any Element or other EventTarget.
    }

	static random(min = 10, max = 999) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

	postData(params = {}) {
    	let config = {
        	url: '',
        	data: {},
        	method: 'POST',
        	showloading: true,
        	ele : false
    	};
        Object.assign(config, params);
        if (config.showloading) {
            this.showLoading();
        }
        if(config.ele!=false){
        	ele.setAttribute('disabled','disabled');
        	ele.classList.add('disabled');
        }

        return new Promise((resolve, reject) => {
            let formData;            
            if (!(config.data instanceof FormData)) {// console.log('es instancia??', config.data instanceof FormData);
                formData = new FormData
                for (let key of Object.keys(config.data)) {
                    let value = config.data[key];
                    if (typeof value === 'object' && value !== null) {
                        formData.append(key, JSON.stringify(config.data[key]));
                    } else {
                        formData.append(key, config.data[key]);
                    }
                }
            } else {// console.log('sí es form', config.data);                
                formData = config.data;
            }
            fetch(config.url, {
                method: config.method,
                body: formData
            }).then(rsp => rsp.json()).then(jsonRsp => { 
            	if(config.ele!=false){
            		config.ele.setAttribute('disabled','disabled');
        			config.ele.classList.add('disabled');           	
        		}
                if (jsonRsp.code == 200) {
                    if (jsonRsp.hasOwnProperty("data")) {
                        resolve(jsonRsp.data);
                    } else if (jsonRsp.hasOwnProperty("msj")) {
                        resolve(jsonRsp);
                    } else { resolve(jsonRsp); }
                    if (config.showloading) {
                        this.hideLoading();
                    }
                } else {
                    this.hideLoading();
                    if(jsonRsp.code=='error'||jsonRsp.code=='Error')
                        this.swalError({footer:'',text:jsonRsp.msj});
                    else this.swalError();
                    reject(jsonRsp);
                }
            }).catch(e => {
            	if(config.ele!=false){
            		config.ele.removeAttribute('disabled');
        			config.ele.classList.remove('disabled');
        		}
                this.hideLoading();
                this.swalError();
                console.log(e);
                reject(e);
            })
        })
    }
    showLoading() {
        Swal.fire({
            // title: 'Cargando...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        })
    }
    hideLoading() {
        Swal.close();
    }
    swalError(params = {}) {
        let config = { text: 'Algo salió mal!', footer: 'Porfavor vuelva a intentarlo en un momento' };
        Object.assign(config, params);
        Swal.fire({
        	type: 'error',//warning (!)//success
            icon: 'error',//warning (!)//success
            title: 'Oops...',
            text: config.text,
            footer: config.footer,
            allowOutsideClick: false,
            closeOnClickOutside: false
        })
    }
    dtTable(tabla,params){
    	let tb=$('table'+tabla);
        let nomtable=tb.closest('.vistatabla').attr('nombre')||'sin nombre';
    	let gritable=tb.parent('.grid-body');    	
    	gritable.addClass('table-responsive');
    	let datatb=tb.DataTable({
            ordering: true,
            searching: false,
            pageLength: 50,
            info: false,
            oLanguage: {
                "sLengthMenu": "mostrar _MENU_ Registros ",
                "oPaginate": {
                    "sFirst": "Siguiente Página", // This is the link to the first page
                    "sPrevious": "Anterior", // This is the link to the previous page
                    "sNext": "Siguiente", // This is the link to the next page
                    "sLast": "Página Anterior" // This is the link to the last page
                }
            },
            dom: 'Bfrtip',
            buttons: [{
                extend: "excelHtml5",
                className: 'btn-dt btn-excel',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
                messageTop: '',
                title: nomtable,
                customize: function(xlsx) {
                   /* var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                    $tr = $('#aquitablacursos '+idtable+' tbody').find('tr');
                    $.each($tr, function(ir, row) {
                        $(this).find('td').each(function(ic, td) {
                            var xcol = col[ic];
                            if (ir == 0) $('row c[r="' + xcol + '1"]', sheet).attr('s', 32);
                            else {
                                var s = ir % 2 == 0 ? 8 : 20;
                                $('row c[r="' + xcol + (ir + 1) + '"]', sheet).attr('s', s);
                            }
                        })
                    })*/
                }
            }, {
                extend: "pdfHtml5",
                messageTop: '',
                className: 'btn-dt btn-pdf',
                text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
                title: nomtable,//'<?php echo JrTexto::_('Report') . " " . JrTexto::_('Time') . " - " . date('d-m-Y'); ?>',
                customize: function(doc) {
                    /*doc.pageMargins = [12, 12, 12, 12];
                    $tr = $('#aquitablacursos '+idtable+' tbody').find('tr');
                    var ntr = $tr.length;
                    var ntd = $tr.eq(0).find('th').length;
                    var ncolA = [30, "*", "*", "*"];
                    /*for(i=0;i<ntd-2;i++){
                        ncolA.push("*");
                    }*/
                   /* doc.content[1].table.widths = ncolA;
                    var tblBody = doc.content[1].table.body;
                    $tr.each(function(ir, row) {
                        var color = ((ir % 2 == 0) ? '' : '#e4e1e1');
                        if ($(this).hasClass('nomcursos')) {
                            $(this).find('td').each(function(ic, td) {
                                tblBody[ir][ic].fillColor = '#5c5858';
                                tblBody[ir][ic].color = "#FFFFFF";
                            })
                        }
                    })
                    tblBody[ntr][0].alignment = 'left';
                    if (_logobase64 != '') {
                        doc.content.splice(0, 0, {
                            margin: [0, 0, 0, 12],
                            alignment: 'center',
                            image: _logobase64
                        });
                    }*/
                }
            }, {
                extend: "print",
                className: 'btn-dt btn-print',
                text: '<i class="fa fa-print" aria-hidden="true"></i>',
                title: nomtable ,//'<?php echo JrTexto::_('Report') . " " . JrTexto::_('Time') . " - " . date('d-m-Y'); ?>',
                customize: function(win) {
                    /*var body = $(win.document.body);
                    var title = body.find('h1').first().text();
                    if (_logobase64 != '') {
                        body.find('h1').first().html('<div style="text-align:center; width:100%;"><img src="' + _logobase64 + '"/><br>' + title + '</div>');
                    } else {
                        body.find('h1').first().html('<div style="text-align:center; width:100%;">' + title + '</div>');
                    }
                    table = body.find('table');
                    table.find('th').each(function(index) {
                        table.find('th').css('background-color', '#cccccc')
                    });*/
                }
            }]
        });
		gritable.children('div').addClass('row');
		gritable.find('.dt-buttons').addClass('col-md-4 col-sm-6 col-xs-12');
		let txtbuscar=`<div class="input-group">
				  <input type="text" id="bustexto" placeholder="Texto a buscar" nombre="bustexto" value="${(params.bustexto||'')}" class="form-control">
				   <span class="input-group-addon success ">		
				   <span class="arrow"></span>
					<i class="fa fa-search"></i>		
				   </span>
				</div>`;
        let ordenar=params.ordenar==undefined?false:params.ordenar;
		let btnnuevo=`<button class="btn btn-warning btnnuevo" style="margin:0.25ex;">Nuevo <i class="fa fa-file"></i> </button>`;
        let btnordenar=`<button class="btn btn-danger btnordenartb" style="margin:0.25ex;"> <i class="fa fa-list-ol"></i> </button>`;
		gritable.find('.dt-buttons').after('<div class="col-md-4 col-sm-6 col-xs-12 text-right">'+btnnuevo+(ordenar?btnordenar:'')+'</div>');
		gritable.find('.dt-buttons').after('<div class="col-md-4 col-sm-6 col-xs-12">'+txtbuscar+'</div>');


    }
    tablacambiarestado(ele,tabla){
    	let _this=this;
    	let el=$(ele);
    	let tr=$(ele).closest('tr');
    	return new Promise((resolve, reject) => {
    	 	let input=el.children('input');
    	 	var data=new FormData()		
			data.append('campo',el.attr('cp'));
			data.append('valor',input.val()==1?1:0);
			data.append(tr.attr('idpk'),tr.attr('id'));
			_this.postData({url:_sysUrlBase_+'json/'+tabla+'/setcampo','data':data}).then(rs => { 
	          	_this.cambiarestado(el);
	          	//if(tabla==true) tr.remove();
	          	resolve(true);
	        }).catch(e =>{
	        	resolve(false);	            
	        })
    	})
    }
    guardar(ventana,recargar){
    	let _this=this;
    	let recarga=recargar||true;
    	return new Promise((resolve, reject) => {
    		let _frmtmp=document.getElementById(ventana.frmid);
    		var data=new FormData(_frmtmp);


		   	_this.postData({url:_sysUrlBase_+'json/'+ventana.tb+'/guardar','data':data,'headers':{'Content-Type': 'multipart/form-data'}}).then(rs => {                
                if(recarga=='return'){
                    resolve(rs);
                }else if(recarga==true)window.location.reload();
	          	else{                    
	          		Swal.fire({
                        type: 'success',//warning (!)//success
                        icon: 'success',//warning (!)//success
                        // title: rs.msj,
                        text: rs.msj,
                        allowOutsideClick: false,
                        closeOnClickOutside: false
                    }).then(rs=>{
                        ventana.ventana.children('.vistatablaformulario').hide(0);
                        ventana.ventana.children('.vistatabla').fadeIn(500);
                        resolve(true);
                    })	          		
	          	}	        	
	        }).catch(e =>{
	        	resolve(false);	            
	        })
	    })
    }
    cambiarestado(ele){
    	let input=$(ele).children('input');
    	if(input.val()==1){
    		input.val(0);
    		input.removeAttr('checked');
            $(ele).children('label').children('i').addClass('fa-square-o').removeClass('fa-check-square');            
    		$(ele).children('label>span').text(input.attr('v2'));
    	}else{
    		input.val(1);
    		input.attr('checked',true);
            $(ele).children('label').children('i').removeClass('fa-square-o').addClass('fa-check-square');
    		$(ele).children('label>span').text(input.attr('v1'));
    	} 
    }
    eliminar(datos){
    	let _this=this;    
    	return new Promise((resolve, reject) => {
    		Swal.fire({
			  title: 'Está seguro de eliminar este registro?',
			  text: "Esto no se podrá revertir!",
			  icon: 'warning',
			  type:'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si',
			  cancelButtonText: 'No',
			}).then((result) => {
			  if (result.value==true) {
			    _this.postData({url:_sysUrlBase_+'json/'+datos.url+'/eliminar','data':datos.data}).then(rs => {
		          	if(datos.remove==true) datos.ele.remove();
		          	resolve(true);
		        }).catch(e =>{
		        	resolve(false);
		        })
			  }
			})
			resolve();
    	})
    }
    crop(opt){
        var container = opt.container; //document.querySelector('.img-container');
        let parent=container.parentNode;        
        if(!$(parent).hasClass('inicializado')){
            var image = opt.image;//container.getElementsByTagName('img').item(0); 
            var imagedefault=opt.imagedefault||_sysUrlBase_ +'static/media/defecto/logoempresa.png';
            var options={
                dragMode: 'move',
                rotate:true,
                zoom:true,
                scalable:false,
                aspectRatio: 1/1,
                autoCropArea: 1,
                restore: true,
                guides: false,
                center: true,
                highlight: true,
                cropBoxMovable: false,
                cropBoxResizable: false,
                toggleDragModeOnDblclick: false,
              }            
            var options=assign(options,opt.options);
            var cropper = new Cropper(image, options);
            $(parent).on('click','.btnrotate0',function(ev){
                ev.preventDefault();
                cropper.rotate(parseInt(-15));
                $(parent).find('.btndownload').addClass('nodescarga').trigger('click');
            })
            $(parent).on('click','.btnrotate1',function(ev){
                ev.preventDefault();
                cropper.rotate(parseInt(15));
                $(parent).find('.btndownload').addClass('nodescarga').trigger('click');
            })
            $(parent).on('click','.btnzoom0',function(ev){
                ev.preventDefault();
                cropper.zoom(-0.25);
                $(parent).find('.btndownload').addClass('nodescarga').trigger('click');
            })
            $(parent).on('click','.btnzoom1',function(ev){
                ev.preventDefault();
                cropper.zoom(0.25);
                $(parent).find('.btndownload').addClass('nodescarga').trigger('click');
            })
            $(parent).on('click','.btnrefresh',function(ev){
                ev.preventDefault();
                cropper.reset();
                $(parent).find('.btndownload').addClass('nodescarga').trigger('click');
            })
            $(parent).on('click','.btndefault',function(ev){
                ev.preventDefault();
                cropper.replace(imagedefault);
                $(parent).find('.btndownload').addClass('nodescarga').trigger('click');
            })
            $(parent).on('click','.btnupload',function(ev){
                ev.preventDefault();
                const oModal=new mymodal('',{titulo:'Biblioteca',tamanio:'modal-xl'});
                oModal.loadModal();
                const oBiblioteca=new mybilioteca(oModal.getIdModal()+'-body',{image:true,funcionretorno:function(datos){                
                    cropper.replace(datos.link);
                    oModal.closemodal();
                    $(parent).find('.btndownload').addClass('nodescarga').trigger('click');
                }});
            })
            $(parent).on('click','.btndownload',function(ev){
                ev.preventDefault();
                let dw=$(this);
                setTimeout(function(ev){
                    var imgurl =  cropper.getCroppedCanvas().toDataURL();
                    $(parent).find('input').val(imgurl);
                    if(dw.hasClass('nodescarga')){
                        dw.removeClass('nodescarga');
                        return;
                    }
                    let a=document.createElement("a");
                    a.href=imgurl;
                    a.download='imagen.png';
                    a.click();
                },100);
            }).on('changeimage',function(ev){
                ev.preventDefault();
                let image__=$(image).attr('link')||imagedefault;
                cropper.replace(_sysUrlBase_+image__);
            })
            $(parent).addClass('inicializado');
            return cropper;            
        }else{
            $(parent).trigger('changeimage');
        }
    }

    normalizetourl(str){
        let txt=str.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
        return  txt.replace( /[^-A-Za-z0-9]+/g, '-' ).toLowerCase();
    }

    _seltipomodena(num){
        let monedas={
            1:{nombre:'Soles',prefijo:'S/.'},
            2:{nombre:'Dollares US',prefijo:'$.'}
        }
        if(num!=''||num!=null)
            return monedas[num];
        return monedas;
    }
    _idgui(){
        return 'ui'+Math.floor(Math.random() * (1 - 1000)) + 1;
    }
}