class BDMenu_proyecto{
	ventana=null;
	ventanaid=null;
	idempresa=null;
	idrol=null;
	frmid='frmmenu_proyecto';
	tb='menu_proyecto';
	$fun=null;
	bustexto='';
	idmenuproyecto=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		$('#aquiformulario').hide();
		this.ventana.on('change','select#busidrol',function(ev){ev.preventDefault(); _this.vista_ordenar(this)})
		.on('click','.btnAgregar',function(ev){
			ev.preventDefault();
			let $frm=$('form#frmmodulo');			
			//$frm.children('input#idrol').val($('select#busidrol').val());
			//_this.cambiarmenu();
			//$('#aquiformulario').show();
			//$('#aquiordenar').hide();
			_this.idmenuproyecto=false;
			_this.vista_formulario();
			$(this).hide();

		}).on('click','.btncancelar',function(ev){
			ev.preventDefault();
			$('#aquiformulario').hide();
			$('#aquiordenar').show();
			$('.btnAgregar').show();
		}).on('change','select#idmenu',function(ev){
			ev.preventDefault();
			_this.cambiarmenu();
		}).on('submit','#frmmenu_proyecto',function(ev){
			ev.preventDefault();
			let idmenu=$('#frmmenu_proyecto').find('select#idmenu').val()||'';
			if(idmenu==''){	
				$('#frmmenu_proyecto').find('input#insertar').val($('#frmmenu_proyecto').find('input#nomtemporal').val());
				$('#frmmenu_proyecto').find('input#modificar').val($('#frmmenu_proyecto').find('select#icono').val());	
			}else{
				let ol=$("#aquiordenar .nestable ol");				
				if(ol.find("li[menuid='menu"+idmenu+"']").length){
					 Swal.fire({
			                	type:'warning',
			                    icon: 'warning',
			                    title: 'Oops...',
			                    text: 'Éste menú ya se encuentra asignado al Perfil ',			                    
			                    }) 
					return ;
				}else{
					$('#frmmenu_proyecto').find('input#insertar').val(1);
					$('#frmmenu_proyecto').find('input#modificar').val(1);
				}
			}
			$('#frmmenu_proyecto').find('input#idrol').val(_this.ventana.find('#busidrol').val());
			_this.$fun.guardar({frmid:'frmmenu_proyecto',tb:'menu_proyecto'},'return').then(rs=>{
				//window.location.reload();
				$('.btnAgregar').show();
				_this.vista_ordenar()
			});
			$('#aquiformulario').hide();
			$('#aquiordenar').show();
			return ;
		}).on('click', '#aquiordenar .btneditar',function(ev){
			ev.preventDefault();
			let li_=$(this).closest('li');			
			_this.idmenuproyecto=li_.attr('id')||'';
			$('.btnAgregar').hide();
			_this.vista_formulario();			
		}).on('click', '#aquiordenar .btneliminar',function(ev){
			ev.preventDefault();
			let li_=$(this).closest('li');
			let tienehijos=li_.find('li').length;
			if(tienehijos>0){
				_this.$fun.swalError({text:'Este Menú tiene submenus, elimine los submenus primero',footer:''});
				return 0;
			}
			_this.$fun.eliminar({
				url:'menu_proyecto',
				remove:true,
				ele:li_,
				data:{'idmenuproyecto':li_.attr('id')}
			})
		}).on('click', '#aquiordenar .btnguardar',function(ev){ev.preventDefault(); _this.guardarorden();})

		this.ventana.find('#busidrol').trigger('change');
		this.ini_librerias();
					
		let $frm=$('#'+this.frmid);	
		//$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		/*$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});*/		
	}
	ini_librerias(){
		$('select.select2').select2();
		$('select.select2icon').select2({
			templateSelection: function (item) {
				var $span = $('<span><i class="fa '+ item.id + '"/></i> ' + item.text + "</span>");
		  	    return $span;},
		  	templateResult:function(item){
		    	var $span = $('<span><i class="fa '+ item.id + '"/></i> ' + item.text + "</span>");
		  	    return $span;}
		 });   	   

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';		
		var data=new FormData();
				data.append('idmenu',$('select#busidmenu').val()||'');
				data.append('idproyecto',$('select#busidproyecto').val()||'');
				data.append('idrol',$('select#busidrol').val()||'');
				data.append('eliminar',$('select#buseliminar').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Orden</th>
            	<th>Idpadre</th>
            	<th>Insertar</th>
            	<th>Modificar</th>
            	<th>Eliminar</th>
            	<th>Fecha_registro</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idmenuproyecto}" idpk="idmenuproyecto">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idmenuproyecto}" type="checkbox" value="0">
                      <label for="checkbox{$v.idmenuproyecto}"></label>
                    </div></div></td>
					<td>${v.orden}</td>	
					<td>${v.idpadre}</td>	
					<td>${v.insertar}</td>	
					<td>${v.modificar}</td>	
					<td><div class="row-fluid"><div cp="eliminar" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.eliminar}" ${v.eliminar==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.eliminar==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td>${v.fecha_registro}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idmenuproyecto=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idmenuproyecto:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto,ordenar:true});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('#aquiformulario').show();
		this.ventana.find('#aquiordenar').hide();
		var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idmenuproyecto').val(rs.idmenuproyecto||'');
	    	let idmenu=rs.idmenu||'';
			$frm.find('select#idmenu').val(idmenu).trigger('change');
			_this.cambiarmenu();
			if((idmenu==''||idmenu==null) && rs.insertar!=''){
  				$frm.find('input#nomtemporal').val(rs.insertar||'');
  				$frm.find('input#nomtemporal').val(rs.insertar||'');
  				$frm.find('select#icono').val(rs.modificar).trigger('change');
			}
  			$frm.find('input#idproyecto').val(rs.idproyecto||'');
  			$frm.find('input#idrol').val(rs.idrol||'');
  			$frm.find('input#orden').val(rs.orden||'');
  			$frm.find('input#idpadre').val(rs.idpadre||'');
  			$frm.find('input#modificar').val(rs.modificar||'');
  			let eliminar=rs.eliminar||1;
			$frm.find('input#eliminar').val(eliminar);
  		}

	    if(this.idmenuproyecto==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('idmenuproyecto',this.idmenuproyecto);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{				
				llenardatos(rs);				
	        }).catch(e =>{console.log(e);});
	}

	vista_ordenar(){
		let _this=this;
		this.idrol=$('#busidrol').val();
		var data=new FormData();
			data.append('enorden',true);
			data.append('idrol',this.idrol);
		let htmlhijos_=function(rs){
				let htmlorder=``;
				$.each(rs,function(i,v){
					let nombre=v.strmenu||'';
					let strmenu=v.strmenu||'Sin nombre';
					let icono=(v.icono==undefined||v.icono==null||v.icono=='')?'':('<i class="fa '+(v.icono)+'"></i>');
					if(v.idmenu==null && v.strmenu==null && v.url==null){
						strmenu=v.insertar;
						icono='<i class="fa '+v.modificar+'"></i>';
					}
					let imagen=(v.imagen==undefined||v.imagen==null||v.imagen=='')?'':('<img src="'+_sysUrlBase_+v.imagen+'" width="25px" height="25px" >');
					let htmlhijo=(v.hijos==undefined||v.hijos==null)?'':('<ol class="dd-list">'+htmlhijos_(v.hijos)+'</ol>');
					let iscontainer=((v.idmenu==null)?true:false);
					htmlorder+=`
					<li  class="dd-item ${iscontainer==true?'escontenedor':''}" id="${v.idmenuproyecto}" menuid="menu${v.idmenu}" >
						<div class="dd-li">
							<div class="dd-border">						
								<span class="dd-handle">
									<span class="icono">${imagen+icono} </span> 
									<span class="title">${(strmenu||'Sin nombre')}</span>
								</span>
								<span class="btnacciones" style="position: absolute;    right: 1ex;    top: 1ex;">
									<i class="fa fa-pencil btneditar"></i> 
									<i class="fa fa-trash btneliminar"></i>
								</span>
							</div>
							<div>${htmlhijo}</div>
						</div>					
					</li>`;
				})
				return htmlorder;
			}

			this.$fun.postData({url:_sysUrlBase_+'json/menu_proyecto/','data':data}).then(rs =>{				
				let htmlorder=`
				<div class="row">
					<div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                        <div class="dd nestable" style="max-width: 100%;">
                        	<ol class="dd-list">${htmlhijos_(rs)}</ol>
                        </div>
                    </div>
                </div>                
                <div class="row"><div class="col-md-12 col-sm-12 col-12"><hr></div></div>
                <div class="row">
                	<div class="col-md-12 col-sm-12 col-12 text-center">
                		<button class="btn btn-primary btnguardar"> <i class="fa fa-save"></i> Guardar Orden</button>                		
                	</div>
                </div>
                `;				
				this.ventana.find('#aquiordenar').html(htmlorder);
				$('.dd').nestable({
			        onDragStart: function (l, e){},
			        beforeDragStop: function(l,e, p){
			        	let li=p.parent('li');		        	
			            if((li.length>0 && li.hasClass('escontenedor'))||li.length==0){
			                return true;
			            }
			            else {
			                Swal.fire({
			                	type:'warning',
			                    icon: 'warning',
			                    title: 'Oops...',
			                    text: 'No se puede mover En este lugar, no es un contenedor',			                    
			                    }) 
			                return false;
			            }			        	
			        }
			    })
	        }).catch(e => {console.log(e); });
	}
	guardarorden(){
		var datos=[];
		let _this=this;
		return new Promise((resolve, reject) => {
	        $('.dd').children('ol').find('li').each(function(i,li){        	
	            let _li=$(li);	           
	            let _lipadre=_li.closest('ol').closest('li');
	            let idpadre=_lipadre.length==0?'':_lipadre.attr('id');
	            datos.push({	            	
	            	'idmenuproyecto':_li.attr('id'),
	            	'orden':(_li.index()+1),
	            	'idpadre':idpadre
	            });
	        })
        	var formData = new FormData();        
        		formData.append('datos', JSON.stringify(datos));
        		_this.$fun.postData({url:_sysUrlBase_+'json/menu_proyecto/guardarorden','data':formData}).then(rs => {
        			//window.location.reload();
        			_this.vista_ordenar();
        			resolve();
        		}) 
        })
	}

	cambiarmenu(){
		let idmenu=this.ventana.find('select#idmenu').val()||'';	
		if(idmenu!=''){
			this.ventana.find('.sinmenu').hide();
		}else{
			this.ventana.find('.sinmenu').show();
		}
	}
	
}