var edithtml_all = function(obj) {
    // $('#'+obj.id).show();
    try {
        if (tinymce.get('#' + obj.id) != null)
            tinymce.get('#' + obj.id).remove();
    } catch (error) { console.log(error) };

    let toolbar = 'undo redo |  advlist | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor';
    let height = 200;
    let _ismobile = false;
    if ($(window).width() <= 768) {
        toolbar = 'alignleft aligncenter alignright alignjustify numlist bullist | bold italic underline forecolor backcolor ';
        height = 240;
        _ismobile = true;
    }


    tinymce.init({
        relative_urls: false,
        convert_newlines_to_brs: true,
        menubar: false,
        statusbar: false,
        verify_html: true,
        content_css: _sysUrlBase_ + '/static/tema/css/bootstrap.min.css',
        selector: '#' + obj.id,
        height: height,
        min_height: 50,
        setup: function(editor) {
            editor.on('blur', function(e) {
                tinyMCE.triggerSave();
                $('#' + obj.id).trigger('blur');
                $('#' + obj.id).html($('#' + obj.id).val());
            });
            editor.on('keyup', function(e) {
                tinyMCE.triggerSave();
                $('#' + obj.id).html($('#' + obj.id).val());
                $('#' + obj.id).trigger('validarrespuesta');
            });
        },
        plugins: ["advlist"],
        toolbar: toolbar,
        mobile: {
            //mobile: _ismobile,
            //theme: 'mobile',
        }
    });
    // tinyMCE.DOM.setStyle(tinyMCE.DOM.get("elm1" + '_ifr'), 'height', resizeHeight + 'px');
}


var crearcontenido_nlsw = function(datos) {
    var tmphtml = '';
    var idtmp = 'pnl_' + datos.pnlidgui + '_' + Date.now();

    var iedit = '<i class="fa fa-pencil"></i>';
    if (datos.tipo == 'record') {
        iedit = '<i class="fa fa-microphone-slash"></i>';
        tmphtml += '<div class="col-md-12 contentpregunta_nlsw" id="' + idtmp + '"><audio src="' + datos.ruta + '" class="" controls="true"></audio>';
    } else if (datos.tipo == 'audio') {
        tmphtml += '<div class="col-md-12 contentpregunta_nlsw" id="' + idtmp + '"><audio src="' + datos.ruta + '" class="" controls="true"></audio>';
    } else if (datos.tipo == 'video') {
        tmphtml += '<div class="col-md-12 contentpregunta_nlsw embed-responsive embed-responsive-16by9" id="' + idtmp + '"><video src="' + datos.ruta + '" class=" embed-responsive-item" controls="true"></video>';
    } else if (datos.tipo == 'image') {
        tmphtml += '<div class="col-md-12 text-center contentpregunta_nlsw" id="' + idtmp + '"><img src="' + datos.ruta + '" class="img-responsive img-thumbnail" style="max-width:400px; max-height:400px;" />';
    } else if (datos.tipo == 'texto') {
        tmphtml += '<div class="col-md-12 text-text-justify contentpregunta_nlsw" id="' + idtmp + '"><span class="texto">' + datos.ruta + '</span>';
    }
    tmphtml += '<span class="btnacciones nopreview"><a href="#" class="btn btn-xs btn-warning btneditarpnl_nlsw" data-tipo="' + datos.tipo + '" data-pnlidgui="' + idtmp + '" >';
    tmphtml += iedit + '</a>';
    tmphtml += '<a href="#" class="btn btn-xs btn-danger btneliminarpnl_nlsw" data-tipo="' + datos.tipo + '" data-pnlidgui="' + idtmp + '"><i class="fa fa-trash"></i></a></span></div>';
    $('#preguntasDocente' + datos.pnlidgui).append(tmphtml);
}

var editarcontenido_nlsw = function(datos) {
    var tmphtml = '';
    var iedit = '<i class="fa fa-pencil"></i>';
    if (datos.tipo == 'record') {
        var tmpid = Date.now();
        iedit = '<i class="fa fa-microphone-slash"></i>';
        tmphtml += '<audio src="' + datos.ruta + '?tmp=dt' + tmpid + '" class="" controls="true"></audio>';
    } else if (datos.tipo == 'audio') {
        datos.medio = $('.' + datos.pnlidgui);
        if (datos.medio.length) { datos.medio.removeClass(datos.pnlidgui).attr('id', datos.pnlidgui); }
        tmphtml += '<audio src="' + datos.ruta + '" class="" controls="true"></audio>';
    } else if (datos.tipo == 'video') {
        datos.medio = $('.' + datos.pnlidgui);
        if (datos.medio.length) { datos.medio.removeClass(datos.pnlidgui).attr('id', datos.pnlidgui); }
        tmphtml += '<video src="' + datos.ruta + '" class=" embed-responsive-item" controls="true"></video>';
    } else if (datos.tipo == 'image') {
        datos.medio = $('.' + datos.pnlidgui);
        if (datos.medio.length) { datos.medio.removeClass(datos.pnlidgui).attr('id', datos.pnlidgui); }
        tmphtml += '<img src="' + datos.ruta + '" class="img-responsive img-thumbnail" style="max-width:400px; max-height:400px;" />';
    } else if (datos.tipo == 'texto') {
        tmphtml += '<span class="texto">' + datos.ruta + '</span>';
    }
    tmphtml += '<span class="btnacciones nopreview"><a href="#" class="btn btn-xs btn-warning btneditarpnl_nlsw" data-tipo="' + datos.tipo + '" data-pnlidgui="' + datos.pnlidgui + '" >' + iedit + '</a>';
    tmphtml += '<a href="#" class="btn btn-xs btn-danger btneliminarpnl_nlsw"  data-tipo="' + datos.tipo + '" data-pnlidgui="' + datos.pnlidgui + '"><i class="fa fa-trash"></i></a></span></div>';
    if (datos.medio != undefined)
        datos.medio.html(tmphtml);
    else
        $('#' + datos.pnlidgui).html(tmphtml);
}


var grabarAudio_nlsw = function(datos) {
    chunks = [];
    navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
        grabando_streamnlsw = stream;
        mediaRecordernlsw = new MediaRecorder(stream);
        mediaRecordernlsw.onstop = function(e) {
            blob = new Blob(chunks, { 'type': 'audio/wav' });
            speachuploadblob_nlsw({ blob: blob, idgui: datos.idgui, acc: datos.acc, medio: datos.medio, donde: datos.donde || '' });
        }
        mediaRecordernlsw.ondataavailable = function(e) { chunks.push(e.data); }
        mediaRecordernlsw.start();
        //datos.grabando.addClass('fa-microphone fa-spinner animated zoomIn infinite').removeClass('fa-microphone-slash');
    }).catch(function(err) { console.log(err.name + ": " + err.message); });
}
var stopAudio_nlsw = function(datos) {
    try {
        if (grabando_streamnlsw) grabando_streamnlsw.getTracks()[0].stop();
        if (mediaRecordernlsw != undefined)
            if (mediaRecordernlsw.state != 'inactive') mediaRecordernlsw.stop();
        datos.grabando.addClass('fa-microphone-slash').removeClass('fa-microphone fa-spinner color-green animated zoomIn infinite');
    } catch (ex) { console.log(ex) };
}

var speachuploadblob_nlsw = function(datos) {
    var donde = datos.donde || false;
    var data = new FormData();
    data.append('filearchivo', datos.blob);
    data.append('type', datos.blob.type);
    data.append('name', datos.idgui);
    $.ajax({
        url: _sysUrlBase_ + "/biblioteca/subirblob",
        type: 'POST',
        data: data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(res) {
            if (res["code"] == 'ok') {
                if (datos.acc == 'new') crearcontenido_nlsw({ tipo: 'record', ruta: _sysUrlBase_ + '/' + res.namelink, pnlidgui: datos.idgui });
                else if (datos.acc == 'recordAlumno') {
                    var tmphtml = '<br><h4><strong style="color: #309a35;">' + (MSJES_PHP.escuchar_audio || 'Click on the audio to play it') + '.</strong></h4><audio src="' + _sysUrlBase_ + '/' + res.namelink + '?tmp=' + Date.now() + '" class="" controls="true" preload="auto"></audio>';
                    if (donde != false) {
                        if (donde.length) {
                            donde.children('.msjRespuesta').html(tmphtml);
                            console.log(donde.find('audio'));
                            donde.find('audio').get(0).load();
                        } else {
                            $('#tmp_nlsw' + datos.idgui).children('.respuestaAlumno').children('.msjRespuesta').html(tmphtml);
                            console.log('ggg2');
                            $('#tmp_nlsw' + datos.idgui).children('.respuestaAlumno').children('.msjRespuesta').find('audio').get(0).load()
                        }
                    } else {
                        $('#tmp_nlsw' + datos.idgui).children('.respuestaAlumno').children('.msjRespuesta').html(tmphtml);
                        console.log('ggg3');
                        $('#tmp_nlsw' + datos.idgui).children('.respuestaAlumno').children('.msjRespuesta').find('audio').get(0).load()
                    }
                } else editarcontenido_nlsw({ tipo: 'record', ruta: _sysUrlBase_ + '/' + res.namelink, pnlidgui: datos.idgui, medio: datos.medio });
            }
        },
        error: function(e) {
            console.log('Error inesperado intententelo mas tarde', e);
        }
    });
}

var addnewbuttonhtml = function(respuestaAlumno) {
    var msjRespuesta = respuestaAlumno.find('.msjRespuesta');
    if (msjRespuesta.hasClass('grabarfile')) {
        var urlaudio = msjRespuesta.children('audio').attr('src') || '';
        if (urlaudio != '') {
            var index1_ = urlaudio.lastIndexOf('static/');
            if (index1_ > -1) urlaudio = urlaudio.substring(index1_, 10000);
            urlaudio = _sysUrlBase_ + '/' + urlaudio;
            msjRespuesta.children('audio').attr('src', urlaudio);
            msjRespuesta.show();
        }
        return false;
    } else {
        respuestaAlumno.children('.mce-tinymce').remove();
        respuestaAlumno.children('.tox-tinymce').remove();
        var textareaid = respuestaAlumno.find('textarea').show().attr('id');
        if ($('textarea[id="' + textareaid + '"]').length > 1) textareaid = textareaid + Date.now();
        respuestaAlumno.find('textarea').attr('id', textareaid);
        edithtml_all({ id: textareaid });
    }
}

var rinittemplate_nlsw = function(tipoacc) {
    var tabactivo = $('.tabhijo.active').find('.plantilla-nlsw');
    tabactivo.each(function(index, el) {
        if ($(el).hasClass('plantilla-nlsw')) {
            var rptAlumno = $(el).children('.respuestaAlumno');
            var label = rptAlumno.children('label.hidden');
            var textarea = rptAlumno.children('.txtrespuestaAlumno');
            var idtexarea = textarea.attr('id') || 'noexiste';
            if (tipoacc == 'saveedit') {
                $('#' + idtexarea).val('');
                $(el).find('.speachtextook').html('');
            } else if (tipoacc == true) {
                $('#' + idtexarea).siblings('.mce-tinymce').remove();
                $('#' + idtexarea).show();
                var htmlbtn = '<a class="btn btnenformatohtml btn-info btn-xs"><i class="fa fa-code"></i> <span>' + ($('input#apply_format').val() || '') + '</span></a>';
                if (rptAlumno.find('.btnenformatohtml').length == 0) rptAlumno.find('label').append(htmlbtn);
                else rptAlumno.find('.btnenformatohtml').show();
            } else {
                if ($('#' + idtexarea).siblings('.mce-tinymce').length) {
                    tinyMCE.triggerSave();
                    tinyMCE.remove();
                }
            }
        }
    });
}

var savetemplate_nlsw = function(elo) {
    var el = elo.find('.plantilla-nlsw');
    if (el.length) {
        var rptAlumno = el.children('.respuestaAlumno');
        var textarea = rptAlumno.children('.txtrespuestaAlumno');
        var idtexarea = textarea.attr('id') || 'noexiste';
        if ($('#' + idtexarea).siblings('.mce-tinymce').length) {
            var tmphtml = '';
            try {
                tinyMCE.triggerSave();
                tmphtml = tinyMCE.get(idtexarea).getContent() || $('#' + idtexarea).val();
                tinyMCE.remove();
            } catch (ex) {
                tmphtml = $('#' + idtexarea).val();
            }

            $('#' + idtexarea).html(tmphtml);
            rptAlumno.trigger('calcularprogreso');
        }
    }
}
var textosimilar_nlsw = function(str1, str2) {
    var stroriginal = str1;
    var pt = 0;
    var result = '';
    str1 = str1.toLowerCase().trim().replace(/\s+/gi, ' ').replace(/  /, ' ');
    str2 = str2.toLowerCase().trim().replace(/\s+/gi, ' ').replace(/  /, ' ');
    str3 = str1.replace(/[^a-zA-Z 0-9]+/gi, '').replace(/\s+/gi, ' ');
    str4 = str2.replace(/[^a-zA-Z 0-9]+/gi, '').replace(/\s+/gi, ' ');
    str5 = str3.replace(/\s/gi, '');
    str6 = str4.replace(/\s/gi, '');
    if (str5 === str6) return { completado: true, totalok: 100, ncaracteres: 1, caracertados: 1, txtrespuesta: '<span class="speachtextook">' + stroriginal + '</span>' };
    if (str1.length == 0 || str2.length == 0) return { completado: false };
    if (str3 === str4) return { completado: true, totalok: 100, ncaracteres: 1, caracertados: 1, txtrespuesta: '<span class="speachtextook">' + stroriginal + '</span>' };
    var _rstr1 = str1.split(" ");
    var rstr1 = str3.split(" ");
    var rstr2 = str4.split(" ");
    n1 = rstr1.length;
    n2 = rstr2.length;
    n3 = n1 > n2 ? n1 : n2;
    var rstmp = [];
    var iok = 0;
    for (var i = 0; i < rstr1.length; i++) {
        if (rstr1[i] == rstr2[i]) {
            rstmp[i] = '<span class="speachtextook">' + _rstr1[i] + '</span>';
            iok++
        } else { //ierror++;
            rstmp[i] = '<span class="speachtextobad">' + _rstr1[i] + '</span>';
        }
    }
    var ok = iok;
    iok = ((iok * 100) / n3);
    return { completado: true, totalok: iok, ncaracteres: n3, caracertados: ok, txtrespuesta: rstmp.join(' ').trim() };
}

var respustasistema = function() {
    /* var $tmplActiva = getTmplActiva();
     var $plantillaActiva = $tmplActiva.find('.plantilla-nlsw');
     $pnlalumno=$plantillaActiva.find('.pnl-speach.alumno');

     if($pnlalumno.find('.btnAccionspeachdby').length>0){
       $pnlalumno.find('.btnGrabarAudio').attr('disabled',true).addClass('disabled');
       var elemsTotal = $plantillaActiva.find('.textohablado span').length-1;
       var elemsCorregidas = $plantillaActiva.find('.textohablado span.speachtextook').length;
       //console.log(elemsTotal,elemsCorregidas);    
       var $panelIntentos = $('#panel-intentos-dby');
       var intento_actual = $panelIntentos.find('.actual').text();
       var intentos_total = $panelIntentos.find('.total').text();
       if(( elemsTotal === elemsCorregidas && elemsTotal>0)||intento_actual>=intentos_total){
         $tmplActiva.find('.save-progreso').show('fast');        
       }      
     }*/
}
var grabando_streamnlsw = null;
var mediaRecordernlsw = undefined;
$(document).ready(function(ev) {
    /*$('body').on('click','.plantilla-nlsw .btnadd_record',function(ev){
        ev.preventDefault();
        var grabando=$(this).children('i');
        var idgui=$(this).attr('data-idgui')
        if(grabando.hasClass('fa-microphone-slash')){
            grabarAudio_nlsw({grabando:grabando,acc:'new',idgui:idgui});          
        }else{
            stopAudio_nlsw({grabando:grabando});            
        }     
    }).on('click','.plantilla-nlsw .btnadd_textorespuesta',function(ev){
        ev.preventDefault();
        var respA=$(this).closest('.respuestaAlumno');
        var obj=respA.children('textarea');
        var idtmp=respA.attr('id')||('idtmp_'+Date.now());
        var respuesta=fnencode({encode:false,html:(respA.attr('data-texto')||'')});
        var id="textarea"+Date.now();
        respA.attr('id',idtmp);
        var html='<div class="col-md-12"><textarea class="" rows="10" style="width:100%" id="'+id+'">'+respuesta+'</textarea></div>';
        html+='<div class="col-md-12 text-center"> <a class="btn btn-primary btnsavetextorespuesta_nlsw cerrarmodal" data-idtmp="'+idtmp+'" data-textareaid="'+id+'" >Guardar Respuesta <i class="fa fa-save"></i></a></div>';
        var data={titulo:' Respuesta',  htmltxt:html, ventanaid:'biblioteca-texto', borrar:true}
        var modal=sysmodal(data);
        modal.on('load',function(ev){
            edithtml_all({id:id}); 
        })
        setTimeout(function(){modal.trigger('load')},450);      
    }).on('click','.modal .btnsavetextorespuesta_nlsw',function(ev){
        ev.preventDefault();
        var txt=$(this).attr('data-textareaid');
        tinyMCE.triggerSave();
        var txtadd=$('#'+txt).val();
        $(this).closest('.modal').modal('hide');
        var idrespuesta=$(this).attr('data-idtmp');
        $('#'+idrespuesta).attr('data-texto',fnencode({encode:true,html:txtadd}))
    }).on('click','.modal .btnsavetexto_nlsw',function(ev){
        ev.preventDefault();
        var txt=$(this).attr('data-textareaid');
        tinyMCE.triggerSave();
        var txtadd=$('#'+txt).val();
        //var txtadd= fnencode({encode:true,html:txtadd});
        $(this).closest('.modal').modal('hide');
            var acc=$(this).attr('data-accion');
        if(acc=='edit')
            editarcontenido_nlsw({tipo:'texto',ruta:txtadd,pnlidgui:$(this).attr('data-pnlidgui')});
        else
            crearcontenido_nlsw({tipo:'texto',ruta:txtadd,pnlidgui:$(this).attr('data-pnlidgui')});
    }).on('click','.plantilla-nlsw .btneliminarpnl_nlsw',function(ev){
        ev.preventDefault();
        var tipo=$(this).attr('data-tipo');
        if(tipo=='record'){
            var record=$(this).parent().parent().children('audio').attr('src');
            record=record.replace(_sysUrlBase_,'');
            var res = xajax__('', 'biblioteca', 'eliminarFile', record);
        }
        $(this).parent().parent().remove();
    }).on('click','.plantilla-nlsw .btneditarpnl_nlsw',function(ev){
        ev.preventDefault();
        var tipo=$(this).attr('data-tipo');
        var pnlidgui=$(this).attr('data-pnlidgui');
        var pnlpreg=$(this).closest('.contentpregunta_nlsw');
        if(tipo=='texto'){
            var texto=$(this).closest('.contentpregunta_nlsw').children('span.texto').html();
            var id="textarea"+Date.now();
            var html='<div class="col-md-12"><textarea class="" rows="10" style="width:100%" id="'+id+'">'+texto+'</textarea></div>';
            html+='<div class="col-md-12 text-center"> <a class="btn btn-primary btnsavetexto_nlsw cerrarmodal" data-accion="edit" data-textareaid="'+id+'" data-pnlidgui="'+pnlidgui+'">Guardar <i class="fa fa-save"></i></a></div>'
            var data={titulo:' Biblioteca - texto',  htmltxt:html, ventanaid:'biblioteca-texto', borrar:true}
            var modal=sysmodal(data);
            modal.on('load',function(ev){
                 edithtml_all({id:id}); 
            })
            setTimeout(function(){modal.trigger('load')},450); 
        }else if(tipo=='record'){
            var grabando=$(this).children('i');
            var idgui=$(this).attr('data-idgui')
            if(grabando.hasClass('fa-microphone-slash')){
                nombre=$(this).closest('.contentpregunta_nlsw').children('audio');
                grabarAudio_nlsw({grabando:grabando,acc:'edit',idgui:pnlidgui,nombre:'',medio:pnlpreg});          
            }else{
                stopAudio_nlsw({grabando:grabando});            
            }
        }else{
            var tmpid='idtmpedit'+Date.now();
            pnlpreg.addClass(tmpid);     
            var rutabiblioteca= _sysUrlBase_+'/biblioteca/?plt=modal&robj=returndata&fcall=editarcontenido_nlsw&type='+tipo+'&pnlidgui='+tmpid;
            var data={titulo:' Biblioteca - '+tipo,  url:rutabiblioteca, ventanaid:'biblioteca-image', borrar:true}
            var modal=sysmodal(data);
        }
    })*/
    /*$('body').on('click','.plantilla-nlsw .btnadd_recordAlumno',function(ev){
        ev.preventDefault(); 
        var _el=$(this);
        var grabando=$(this).children('i');
        var idgui=$(this).attr('data-idgui');
        var rptAlumno=$(this).closest('.plantilla-nlsw').children('.respuestaAlumno');
        rptAlumno.find('.grabarfile').show();
        var txtrespuesta=rptAlumno.attr('data-texto')||'';
        var hayrespuesta=txtrespuesta.trim()==''?false:true;
        if(grabando.hasClass('fa-microphone-slash')){
            _el.addClass('btn-success').removeClass('btn-danger');
            grabarAudio_nlsw({grabando:grabando,acc:'recordAlumno',idgui:idgui,donde:rptAlumno});
        }else{
            _el.removeClass('btn-success').addClass('btn-danger');
            rptAlumno.attr('total-palabras',1);
            rptAlumno.attr('total-ok',1);
            _el.hide();
            console.log('aaaaaa');
            stopAudio_nlsw({grabando:grabando});
            rptAlumno.trigger('calcularprogreso');
        }       
    })*/
    $('body').on('keyup', '.plantilla-nlsw .txtrespuestaAlumno', function(ev) {
        ev.preventDefault();
        var rptAlumno = $(this).closest('.plantilla-nlsw').children('.respuestaAlumno');
        var txtrespuesta = fnencode({ encode: false, html: (rptAlumno.attr('data-texto') || '') });
        var hayrespuesta = txtrespuesta.trim() == '' ? false : true;
        var contenido = $(this).val() || ''
        var ncaracteresmax = 250;
        if (hayrespuesta) ncaracteresmax = txtrespuesta.length + 15;
        if (contenido.length >= ncaracteresmax) $(this).val(contenido.substring(0, ncaracteresmax));
        rptAlumno.children('.msjRespuesta').html('');
    }).on('blur', '.plantilla-nlsw .txtrespuestaAlumno', function(ev) { // comparar respuestas 
        $(this).attr('hizoblur', 1).trigger('validarrespuesta');
    }).on('validarrespuesta', '.plantilla-nlsw .txtrespuestaAlumno', function(ev) {
        ev.preventDefault();
        var rptAlumno = $(this).closest('.plantilla-nlsw').children('.respuestaAlumno');
        var txttexto = rptAlumno.attr('data-texto') || '';
        var txtrespuesta = fnencode({ encode: false, html: txttexto });
        var hayrespuesta = txtrespuesta.trim() == '' ? false : true;
        if (hayrespuesta == false) {
            rptAlumno.attr('total-palabras', 1);
            rptAlumno.attr('total-ok', 1);
        } else {
            tinyMCE.triggerSave();
            var rptalumn = $(this).closest('.respuestaAlumno');
            var tmphtml = '';
            if (rptalumn.find('.mce-tinymce').length) {
                try {
                    tmphtml = tinyMCE.get($(this).attr('id')).getContent() || $($(this).attr('id')).val();
                } catch (ex) {
                    tmphtml = $($(this).attr('id')).val();
                }
            } else tmphtml = $(this).val();
            var t2 = fnencode({ encode: false, html: tmphtml });
            var hizoblur = $(this).attr('hizoblur') || false;
            if (t2.length >= txtrespuesta.length) { hizoblur = true }
            if (t2.length < txtrespuesta.length && hizoblur == false) {
                $(this).removeAttr('hizoblur');
                return;
            }
            var datosrpt = textosimilar_nlsw(txtrespuesta, t2);
            if (datosrpt.completado == true) {
                rptAlumno.attr('total-palabras', datosrpt.ncaracteres);
                rptAlumno.attr('total-ok', datosrpt.totalok);
                rptAlumno.attr('total-acertado', datosrpt.caracertados);
                rptAlumno.children('.msjRespuesta').html(datosrpt.txtrespuesta);
            }
        }
        rptAlumno.trigger('calcularprogreso');
    })
})