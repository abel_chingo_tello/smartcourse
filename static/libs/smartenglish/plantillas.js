var idcurso_sc__=($('#idcurso_sc').val()||'');
var idcc__=($('#idcc').val()||'');
var idcursodetalle_sc__=($('#idcursodetalle_sc').val()||'');
var idgrupoauladetalle__=($('#idgrupoauladetalle').val()||'');
var PV__=($('#PV').val()||'');
var MSJES_PHP = {};
var cargarMensajesPHP = function(){
      var $sectionMensajes = $('#msjes_idioma');
      $sectionMensajes.find('input').each(function() {
        var id = $(this).attr('id');
        var valor = $(this).val();
        MSJES_PHP[id] = valor;
      });
    };

var initImageTagged=function(){};
var iniciarCompletar_DBY=function(){};
var initspeach=function(){};
var initOrdenarSimple=function(){};
var calcularBarraProgreso = function(_curmetod){
	if(_curmetod.length==0) _curmetod=$('#tabContent').children('.tab-pane.active').children('.content-smartic');
	if(_curmetod.hasClass('plantilla')) _curmetod=_curmetod.closest('.content-smartic');
    var $ul=$('ul.ejercicios',_curmetod);
    var $li = $ul.find('li');
    var nli=$li.length-1;   
    var idmet=_curmetod.attr('data-idmet');
    if(idmet>1){
    _curmetod.find('.tabhijo').each(function(index, tabHijo) {                   
        var porcProgresoEjerc = $(tabHijo).attr('data-progreso');
        porcProgresoEjerc = (porcProgresoEjerc!=undefined&&porcProgresoEjerc!='')?parseFloat(porcProgresoEjerc):0.00;
        var porcXejerc=100/nli;
        var $plantilla = $(tabHijo).find('.plantilla');         
        if($plantilla.hasClass('plantilla-completar')){
            var cantTodos = $plantilla.find('.panelEjercicio input.mce-object-input').length;
            var cantCorrectos = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="good"]').length;
        } else if($plantilla.hasClass('plantilla-verdad_falso')){
            var cantTodos = $plantilla.find('.list-premises .premise').length;
            var cantCorrectos = $plantilla.find('.list-premises *[data-corregido="good"]').length;
        } else if($plantilla.hasClass('plantilla-fichas')){
            var cantTodos = $plantilla.find('.partes-2 .ficha').length;
            var cantCorrectos = $plantilla.find('.partes-2 .ficha.corregido.good').length;
        } else if($plantilla.hasClass('plantilla-ordenar ord_simple')){
            var cantTodos = $plantilla.find('.drop .parte.blank').length;
            var cantCorrectos = $plantilla.find('.drop .parte.blank.good').length;
        } else if($plantilla.hasClass('plantilla-ordenar ord_parrafo')){
            var cantTodos = $plantilla.find('.drop-parr div:not(.fixed)').length;
            var cantCorrectos = $plantilla.find('.drop-parr .filled.good').length;
        } else if($plantilla.hasClass('plantilla-img_puntos')){
            var cantTodos = $plantilla.find('.mask-dots .dot-container .dot-tag').length;
            var cantCorrectos = $plantilla.find('.mask-dots .dot-container .dot-tag.good').length;
        } else if($plantilla.hasClass('plantilla-speach')){
            var $txthablado=$plantilla.find('.speachtexttotal');
            var _msjRespuesta=$txthablado.children('.msjRespuesta').text();
            var cantTodos = $txthablado.attr('total-palabras')||1;
            var cantCorrectos = $txthablado.attr('total-ok')||0;
        } else if($plantilla.hasClass('plantilla-nlsw')){
            var $txthablado=$plantilla.find('.respuestaAlumno')
            var _msjRespuesta=$txthablado.children('.msjRespuesta').text();
            if(_msjRespuesta==''){
                var cantTodos = 1;
                var cantCorrectos = 0;
            }else{
                var cantTodos = $plantilla.attr('total-palabras')||1;
                var cantCorrectos = $txthablado.attr('total-ok')||0;
            }
            if($plantilla.closest('.tabhijo').find('.calificacion-docente').length==0){
              var cantTodos = 1;
              var cantCorrectos = 0;
            }  
        } 
        var porcAcum = (cantCorrectos*porcXejerc)/cantTodos;
        var newPorc = (porcAcum).toFixed(4);
        $(tabHijo).attr('data-progreso', newPorc);

        var progresoGeneral = 0.00;
        $li.each(function() {
            var idTab = $(this).find('a').attr('href');
            var progreso = $(idTab).attr('data-progreso');
            progreso = (progreso!=undefined && progreso!='' && !isNaN(progreso))?parseFloat(progreso):0.00;
            progresoGeneral += progreso;
        });

        var newProgrGnral = progresoGeneral.toFixed(1);
        newProgrGnral = (newProgrGnral>100)? 100.0:newProgrGnral;
        _curmetod.attr('data-progreso', newProgrGnral);                   
        $('#panel-barraprogreso .progress-bar, .panel-barraprogreso .progress-bar').css('width',newProgrGnral+'%');
        $('#panel-barraprogreso .progress-bar span, .panel-barraprogreso .progress-bar span').text(newProgrGnral); 
    });
        $('#panel-barraprogreso, .panel-barraprogreso').show();
    }};

var guardarProgreso = function(){  
  var rol=$('#rolUsuario').val()||'';
  if( rol.toLowerCase() == 'administrador' || rol==1 ){ console.log('Administrador no guarda su avance'); return false; }

  var id_metodologia = $('ul.nav.metodologias li.active>a').attr('href');
  var $navEjercicios = $(id_metodologia).find('ul.nav.nav-tabs.ejercicios');
  var nro_ejercicio = '';

  if( $navEjercicios.length>0 ){
    nro_ejercicio = $navEjercicios.find('li.active>a').text();
    var id_Ejercicio = $navEjercicios.find('li.active>a').attr('href');
  }
 
  var iddetalle = $(id_Ejercicio).attr('data-iddetalle');
  var avance = $(id_Ejercicio).attr('data-progreso') || 0.00;
  var habilidades = $(id_Ejercicio).find('input.selected-skills').val();
  var _idrecurso = $('#idrecurso_cursodetalle').length > 0 ?  $('#idrecurso_cursodetalle').val() : 0;
  
  $(id_Ejercicio).find('textarea.txtareaedit').remove();
  $(id_Ejercicio).find('input').each(function(i,v){
    var value=$(this).val();
    $(this).val(value);
    $(this).attr('value',value);
  })
  //savetemplate_nlsw($(id_Ejercicio));----------------------------------------------------------- revisar cuando toque

  var html_solucion = $(id_Ejercicio).html()||''
  if(html_solucion!='') html_solucion=html_solucion.replace(/(\r\n\t|\n|\r\t)/gm,"").trim();
  html_solucion=html_solucion.trim().replace(/\s+/gi,' ').replace(/  /,' ');
  var cantTotalEjerc = $navEjercicios.find('li').length;
  var ptjexEjerc = 100/cantTotalEjerc;
  var progreso=avance*100/ptjexEjerc;
  var estado = 'P'; /* pendiente */
  var $tmplActiva = $(id_Ejercicio).find('.aquicargaplantilla .plantilla');
  if(id_metodologia=='#div_autoevaluar'){
    totalintentos=parseInt($('#panel-intentos-dby').find('.total').text());
    actualintento=parseInt($(id_Ejercicio).find('.plantilla').attr('data-intento')||1);
    if(actualintento>=totalintentos) estado='T';    
    if(!$tmplActiva.hasClass('yalointento'))$tmplActiva.addClass('yalointento');    
  }
  if(progreso>=100) {
    progreso=100.0;
    estado = 'T'; /* terminado */
  }
  if (typeof iddetalle=="undefined" || iddetalle=="") { console.log('falta iddetalle guardarprogreso',id_Ejercicio); return false; }
  var tipofile='';
  var file='';
  if($tmplActiva.hasClass('plantilla-nlsw')){
    var grabarfiletmp=$tmplActiva.find('.grabarfile');
    tipofile=grabarfiletmp.attr('data-tipo');
    if(tipofile=='texto') file=grabarfiletmp.val()||'';
    else if(tipofile=='audio'){
      var nombreaudio=grabarfiletmp.children('audio').attr('src')||'';
      file=nombreaudio.replace(_sysUrlBase_,'');
    }

  }
  //console.log(file,tipofile);

  $.ajax({
      url: _sysUrlBase_+'/smartbook/actividad_alumno/guardaractividad',
      type: 'POST',
      dataType: 'json',
      async: false,
      data: {
        'iddetalle_actividad' : iddetalle,
        'estado' : estado,
        'idrecurso' : _idrecurso,
        'progreso' : progreso,
        'habilidades' : habilidades,
        'html_solucion' : html_solucion,
        'file':file,
        'tipofile':tipofile,
        'idcurso':idcurso_sc__,
        'idcc':idcc__,
        'idcursodetalle_sc':idcursodetalle_sc__,
        'PV':PV__,
        'idgrupoauladetalle':idgrupoauladetalle__
      },
    }).done(function(resp) {// console.log("Success");
      if(resp.code=='ok'){
        console.log(resp.data);
      }else{
        mostrar_notificacion(MSJES_PHP.attention, resp.mensaje, 'error');
      }
    }).fail(function(err) {
      console.log("Error: ",err);
    });
};

var addBtnsControlAlumno = function(pnlactive,isDBY){
	var html = $('#btns_progreso_intentos').clone().html();
    $('.plantilla #btns_control_alumno',pnlactive).remove();
    $('.plantilla',pnlactive).each(function(i,v){
    	var $html=$(html);
    	if(isDBY==false){
    		$html.find('.btn.try-again').remove();
    		$html.find('.btn.save-progreso').show();
    	}else{
        	$html.find('.btn.try-again').hide();
        	$html.find('.btn.save-progreso').hide();
    	}        	
        $(v).append($html);
    });

}


//inicio omcpletar 
var addclickaqui=function($tpl){
	$tpl.find('.nubeclick').remove();
	var inputs = $tpl.find(".mce-object-input");
	inputs.each(function(){
	    if(!$(this).hasClass('good')){     
	        var idDinamico = $(this).attr("id")+"nube";            
	        var padre = $(this).parent();
	        var input = $(this);
	        $(this).attr("nube",idDinamico);
	        padre.append("<span class='nubeclick' id='"+idDinamico+"'>"+(MSJES_PHP.clik_here||'Click Here')+" </span>");
	        padre.find("#"+idDinamico).css({ top:(input.position().top + 23.5).toString() + "px", "left": input.position().left.toString() + "px" });
    	}
        	
    });
};

/*var bloqueadropable=function(cont){
    var panelAlt=cont.find('.panelAlternativas');
    panelAlt.children('span').removeClass('disabled active');
    cont.find('input').each(function(i,v){
        var span=panelAlt.children('span[idtexto="'+$(v).attr('idtexto')+'"]');
        if(span.length>0){
            span.addClass('disabled');
        }
    })
}*/

var inputRespuesta = function($input, valor,escuhar=true){
    var txtrespuesta=valor.trim();
    if(txtrespuesta=='') return false;
    $input.parents('.inp-corregido.wrapper').find('span.inner-icon').remove();
    var texto=($input.attr('data-texto')||'').trim();
    if(!$input.parents('.wrapper').hasClass('inp-corregido')){
	    if($input.parent('.wrapper').hasClass('input-flotantes')){
	      $input.parent('.input-flotantes.wrapper').wrap('<div class="inp-corregido wrapper"></div>');
	    } else {
	      $input.wrap('<div class="inp-corregido wrapper"></div>');
	    }
	}
    if(texto==txtrespuesta){
    	$input.parents('.inp-corregido.wrapper').attr('data-corregido','good');
    	$input.parents('.inp-corregido.wrapper').append('<span class="inner-icon"><i class="fa fa-check color-green2"></i></span>');  
    	$input.addClass('good').removeClass('bad');
    	if(escuhar) pronunciar(texto,false,'EN');  	
    	return true;
    }else{
    	$input.parents('.inp-corregido.wrapper').attr('data-corregido','bad');
    	$input.addClass('bad').removeClass('good');
    	$input.parents('.inp-corregido.wrapper').append('<span class="inner-icon"><i class="fa fa-times color-red"></i></span>');
    	return false;
    }
};

var cerrarFlotantes = function(inputMCE){
  if($(inputMCE).val() == ''){ 
    if($(inputMCE).closest(".plantilla-completar").find('.nopreview').length == 0 && $(inputMCE).closest(".panelEjercicio").find("#"+$(inputMCE).attr("nube")).length == 0){
      var padre = $(inputMCE).parent();
      padre.append("<span class='nubeclick' id='"+$(inputMCE).attr("nube")+"'>Click aquí</span>");
      padre.find("#"+$(inputMCE).attr("nube")).css({ top:($(inputMCE).position().top + 23.5).toString() + "px", "left": $(inputMCE).position().left.toString() + "px" });
    }
    $(inputMCE).closest(".panelEjercicio").find("#"+$(inputMCE).attr("nube")).show();
  }
  if($(inputMCE).parent().hasClass('input-flotantes wrapper')) {
    var target = $(inputMCE);
  } else if($(inputMCE).parent().parent().hasClass('input-flotantes wrapper')){
    var target = $(inputMCE).parent();    
  }
  if(target!=undefined){
    try{
    target.siblings().not('.nubeclick').remove();
    target.unwrap();
    }catch(ex){}
  }
  $(inputMCE).removeClass('open');
};

var aumentarIntentos = function($plantilla){
  var $panelIntentos = $('#panel-intentos-dby');
  var intentos_total = $panelIntentos.find('.total').text();
  var intento_actual=parseInt($plantilla.attr('data-intento')||1); 
  var rspta = false;
  if(intento_actual<intentos_total){
    //$plantilla.find('.btn.try-again').show();
    $plantilla.find('.save-progreso').hide();
    intento_actual++; 
    rspta = true;
  }else if(intento_actual==intentos_total){
  	$plantilla.find('.btn.try-again').remove();
    $plantilla.find('.save-progreso').show();
    $plantilla.addClass('terminointentos');
  }
  $plantilla.attr('data-intento',intento_actual);
  $panelIntentos.find('.actual').text(intento_actual);
  return rspta;
};



function modResponsive(){
    if($('#panel-intentos-dby').css('display') != 'none'){
        $('.btnejercicio-next_').closest('div').removeClass('col-xs-4');
        $('.btnejercicio-next_').closest('div').addClass('col-xs-3');
        $('.btnejercicio-back_').closest('div').removeClass('col-xs-4');
        $('.btnejercicio-back_').closest('div').addClass('col-xs-3');
        $('.ejeinfo-numero').closest('div').removeClass('col-xs-4');
        $('.ejeinfo-numero').closest('div').addClass('col-xs-6');
        if($(this).width()<= 458 && $(this).width() > 400 ){
            $('#panel-intentos-dby').css({padding:'0px',fontSize:'12.5px'});
            $('.ejeinfo-numero').css({padding:'0px',fontSize:'12.5px'});
        }else if($(this).width() <= 400){
            $('#panel-intentos-dby').css({padding:'0px',fontSize:'10px'});
            $('.ejeinfo-numero').css({padding:'0px',fontSize:'10px'});
            $('.btnejercicio-next_').closest('div').removeClass('col-xs-3');
            $('.btnejercicio-next_').closest('div').addClass('col-xs-4');
            $('.btnejercicio-back_').closest('div').removeClass('col-xs-3');
            $('.btnejercicio-back_').closest('div').addClass('col-xs-4');
            $('.ejeinfo-numero').closest('div').removeClass('col-xs-6');
            $('.ejeinfo-numero').closest('div').addClass('col-xs-4');
        }else{
            $('#panel-intentos-dby').css({padding:'',fontSize:''});
            $('.ejeinfo-numero').css({padding:'',fontSize:''});
        }
    }else{
        $('#panel-intentos-dby').css({padding:'0px',fontSize:''});
        $('.ejeinfo-numero').css({padding:'0px',fontSize:''});
    }  
}

var plantillalook=function($plantilla){
   var tpl=$plantilla.closest('.aquicargaplantilla').attr('data-tpl')||'';
   if(tpl==='look_video_texto'){
   	    $plantilla.on('click','#txtdialogocole .playvideo',function(){  
            var time=$(this).attr('data-time'); 
            var $video = $plantilla.find('video');
            if($video.attr('src')!=''){
              $video.trigger('stop');
              $video[0].currentTime=time;
              $video.trigger('play');
            }
        })
   }else if(tpl=='look_dialogo'){
   		$plantilla.on('shown.bs.tab', '.dialogue-pagination ul.nav-pills li a', function(e){
            var estado = $plantilla.find('#playdialogo').attr('data-estado');
            if(estado=='playing' ){
                playAudio();
            }else if(estado=='stopped'){
            	stopAudio();
            }                
        }).on('click','#playdialogo',function(){            
            var estado = $(this).attr('data-estado');
            if( estado=='stopped' ){
                playAudio();
            }
            if( estado=='playing' ){
                stopAudio();
            }
        }).on('click','#replaydialogo',function(){
            $plantilla.find('.dialogue-pagination>ul>li>a[data-toggle="pill"]:first').trigger('click');
            $plantilla.find('#playdialogo').trigger('click');
        })
   }else if(tpl=='look_imagen'){
      $audio=$plantilla.children('audio');
      $audio.attr('src',$('#PV_SE_URL').val()+'/static/media/audio/'+$plantilla.find('.playaudioimagen').attr('data-audio'));
      $plantilla.on('click','.playaudioimagen',function(ev){                 
          $audio[0].play();
      })
   }
}

// plantillade puntos
$('body').on('click', '.plantilla-img_puntos .contenedor-img .mask-dots.playing .dot', function(e){
        $this=$(this);
        $plantilla=$(this).closest('.plantilla');
        $plantilla.find('.tag-alts>.tag').removeClass('active');
        $plantilla.find('.mask-dots.playing .dot-container').removeClass('hover');
        if($this.hasClass('_success_')) return false;
        else if($plantilla.hasClass('isDBY') && ($this.hasClass('_fallo_')||$this.hasClass('_success_'))){
        	mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar, 'warning');
        	return false;
    	}
    	$(this).parent().addClass('hover');
        $plantilla.find('.tag-alts>.tag').addClass('active');
    }).on('click', '.plantilla-img_puntos .tag-alts .tag.active', function(e){            
        e.preventDefault();
        var $this = $(this);
        if($this.hasClass('_success_')) return false;
        $plantilla=$(this).closest('.plantilla');
        var dotactive=$plantilla.find('.mask-dots .dot-container.hover');           
        var dotactiveid=dotactive.attr('id').split('_').pop();
        var tagactiveid=$this.attr('id').split('_').pop();
        dotactive.find('.dot-tag').text($this.text());
        if( dotactiveid == tagactiveid ){
	        dotactive.find('.dot-tag').removeClass('bad');
	        dotactive.find('.dot-tag').addClass('corregido good');
	        var textoTag = dotactive.find('.dot-tag').text().trim();
	        if(textoTag.length>0){ pronunciar(textoTag,false,'EN');} /*pronunciacion.js*/
	        $this.addClass('_success_');
	        dotactive.children('.dot').addClass('_success_');
	        dotactive.children('.dot').removeClass('_fallo_');    //@edu se agrego
	        var cantTodos = $plantilla.find('.mask-dots .dot-container .dot-tag').length;
            var cantCorrectos = $plantilla.find('.mask-dots .dot-container .dot-tag.good').length;
            calcularBarraProgreso($plantilla);
	    } else {        
	        dotactive.find('.dot-tag').removeClass('good');
	        dotactive.find('.dot-tag').addClass('corregido bad');
	        dotactive.children('.dot').removeClass('_success_');
	        dotactive.children('.dot').addClass('_fallo_');   //@edu se agrego
	        //if( esDBYself($dotContainerActive) ) restarPuntaje();
	    } 
		var cantTodos = $plantilla.find('.mask-dots .dot-container .dot-tag').length;
        var cantCorrectos = $plantilla.find('.mask-dots .dot-container .dot-tag.good').length;
        if(cantTodos==cantCorrectos){
        	$plantilla.find('.btn.try-again').remove();				
			$plantilla.find('.save-progreso').show();
			$plantilla.addClass('ejercicioterminado');
        }else if($plantilla.hasClass('isDBY')){
        	var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
			var intento_actual=parseInt($plantilla.attr('data-intento')||1);
			if(intentos_total==intento_actual){
	    		$plantilla.find('.btn.try-again').remove();				
				$plantilla.find('.save-progreso').show();
				$plantilla.addClass('ejercicioterminado');
			}else{
        		$plantilla.find('.btn.try-again').show();				
				$plantilla.find('.save-progreso').hide();
			}
        }
        if( $plantilla.find('.mask-dots.playing').children().hasClass('hover') ){
            $plantilla.find('.mask-dots.playing').children().removeClass('hover');
            $plantilla.find('.tag-alts>.tag').removeClass('active');
        }
    }).on('click','.plantilla-img_puntos .btn.try-again',function(ev){
    	$(this).hide(); 
    	$plantilla=$(this).closest('.plantilla');
    	$plantilla.find('.dot-container .dot-tag').each(function() {
	        $(this).removeClass('corregido').removeClass('good').removeClass('bad');
	        $(this).text('');
	        $(this).siblings('._success_').removeClass('_success_');
	        $(this).siblings('._fallo_').removeClass('_fallo_');    //@edu se agrego
	    });
	    $plantilla.find('.tag-alts .tag').show().removeClass('_success_');    
    	var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
	    var intento_actual=parseInt($plantilla.attr('data-intento')||1);
		intento_actual=intento_actual+1;
		$plantilla.attr('data-intento',intento_actual);
		$('#panel-intentos-dby').find('.actual').text(intento_actual);	
		if(intentos_total==intento_actual){
    		$plantilla.find('.btn.try-again').remove();
			$plantilla.find('.save-progreso').show();
			$plantilla.addClass('ejercicioterminado');	
		}
    })

// Plantilla Ordenar
var ANS= {}; /* global variable of answers in ORDENAR template, filled in initOrdenarSimple() */
var TIPO_DIVISION = 'letra';
var AYUDA_ORD= {};
var initOrdenarSimple2 = function($plantilla,ayuda){
	var idOrdenar=$plantilla.attr('id');
    var IDGUI=idOrdenar.split('_').pop();
    ANS[IDGUI] = {};
    if(AYUDA_ORD[IDGUI]==undefined && ayuda!=undefined) { AYUDA_ORD[IDGUI] = ayuda; }
    var now = Date.now();
    $plantilla.find('#elem_0').attr('id', 'elem_'+now);
    $plantilla.find('a[data-url=".img_0"]').attr('data-url', '.img_'+now);
    $plantilla.find('img.img_0').removeClass('img_0').addClass('img_'+now);
    $plantilla.find('a[data-url=".audio_0"]').attr('data-url', '.audio_'+now);
    $plantilla.find('audio.audio_0').removeClass('audio_0').addClass('audio_'+now);
    $plantilla.find('.element').each(function() {
        var resp = $(this).attr('data-resp');
        var id = $(this).attr('id');
        ANS[IDGUI][id] = resp;
    });
};

var evaluarFinal_simple = function($element , IDGUI){
    var id = $element.attr('id');
    var string = '' ;
    if( $element.find('.drop>div.parte').hasClass('letter') ){
        $element.find('.drop>div.parte').each(function() {
            string += $(this).text();
        });
        if( ANS[IDGUI][id]!=undefined ) var isMatch = ( ANS[IDGUI][id].toUpperCase() == string.toUpperCase() );
    }
    if( $element.find('.drop>div.parte').hasClass('word') ){
        $element.find('.drop>div.parte').each(function() {
            string += $(this).text()+' ';
        });
        if( ANS[IDGUI][id]!=undefined ) var isMatch = ( ANS[IDGUI][id] == string.trim() ) ; 
    }

    if( isMatch!=undefined ){
        if( isMatch ){
            $element.attr('data-corregido', 'good');
            $element.find('.drop').removeClass('bad').addClass('good');
            $element.find('.drop .parte.blank').removeClass('bad').addClass('good'); 
            /* Pronunciar */
            var arrPartes = [];
            $element.find('.drop.good .parte div').each(function(i, elem) {
                arrPartes.push($(elem).text());
            });
            var textoOrdenado = arrPartes.join(' ');
            if(textoOrdenado.length>0){ 
            	pronunciar(textoOrdenado,false,'EN');
            } /*pronunciacion.js*/
            return true;
        } else {
            $element.attr('data-corregido', 'bad');
            $element.find('.drop').removeClass('good').addClass('bad');
            $element.find('.drop .parte.blank').removeClass('good').addClass('bad');
            return false;
        }
    }
};

var evaluarUno_simple = function($element, IDGUI){
    var cantVacios = $element.find('.drop>div:empty').length;
    var id = $element.attr('id');
    var i=0;
    var rspta_completa='';
    if( ANS[IDGUI][id]!=undefined ) {
        rspta_completa = ANS[IDGUI][id];
        if( $element.find('.drop>.parte').hasClass('word') ) rspta_completa = rspta_completa.split(' ');
    }
    $element.find('.drop>.parte:empty').removeClass('bad').removeClass('good');
    $element.find('.drop>.parte').each(function() {
        var $this = $(this);
        if( $this.is(':empty') ){ return false; }
        if( !$this.hasClass('fixed') ){
            var parte = $(this).text();
            if( parte.toUpperCase()==rspta_completa[i].toUpperCase() ){
                $this.removeClass('bad').addClass('good');
            }else{            	
                $this.removeClass('good').addClass('bad');
            }
        }
        i++;
    });
    if( cantVacios==0 ){ return evaluarFinal_simple($element, IDGUI); }
};

var evaluarOrdenCorrecto = function( $element, IDGUI ){
    var cantVacios = $element.find('.drop>div:empty').length;
    var ayuda = AYUDA_ORD[IDGUI];
    if( !ayuda && cantVacios == 0 ) {
        evaluarFinal_simple($element, IDGUI);
    } else {
        $element.removeAttr('data-corregido');
        $element.find('.drop').removeClass('bad').removeClass('good');
    }
    if(ayuda){
        evaluarUno_simple($element, IDGUI);
    }
};

$('body').on('click', '.plantilla-ordenar.ord_simple .ejerc-ordenar .drag>div', function(e) {
            e.preventDefault();
            var $plantilla=$(this).closest('.plantilla');
            var idTmpl = $plantilla.attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var isDBY=$plantilla.hasClass('isDBY')?true:false;
            var $this = $(this);
            var value= $this.text();
            var $element=$this.closest('.element');
            var idElem = $element.attr('id');
            var $primerVacio = $element.find('.drop>div:empty').first();            
            $primerVacio.html('<div>'+value+'</div>');
            $this.remove();
            evaluarOrdenCorrecto($element , idguiTmpl);
            if($element.attr('data-corregido')=='good')
		    calcularBarraProgreso($plantilla);
		    if(isDBY){
		    	var cantTodos = $plantilla.find('.drop .parte.blank').length;
                var cantCorrectos = $plantilla.find('.drop .parte.blank.good').length;
                var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
		        var intento_actual=parseInt($plantilla.attr('data-intento')||1);
                if(cantTodos==cantCorrectos||intento_actual>=intentos_total){
                	$plantilla.find('.btn.try-again').hide();				
					$plantilla.find('.save-progreso').show();
                }else if($plantilla.find('.drop .parte.blank.bad').length>0){
                	$plantilla.find('.btn.try-again').show();
					$plantilla.find('.save-progreso').hide();
                }
		    }
    }).on('click', '.plantilla-ordenar .ejerc-ordenar .drop>div.parte.blank>div', function(e) {
        e.preventDefault();
        var $this= $(this);
        var $plantilla=$this.closest('.plantilla');
        var isDBY=$plantilla.hasClass('isDBY')?true:false;
        if($this.closest('.parte').hasClass('good')==true || isDBY){
        	return false;
        }
        $this.parent().removeClass('bad'); 

        var idTmpl = $plantilla.attr('id');
        var idguiTmpl = idTmpl.split('_').pop();
        //var buscarTodos = '.element';
        //var buscarCorrectos = '.element[data-corregido="good"]';          
        var value= $this;
        var $element=$this.closest('.element');
        var $parents = $this.parents('.element');
        $element.find('.drag').append(value);
        
        evaluarOrdenCorrecto($element , idguiTmpl);
        calcularBarraProgreso($plantilla);           
    }).on('click', '.multimedia a[data-audio]', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $plantilla=$(this).closest('.plantilla');
        var idTmpl = $plantilla.attr('id');
        var idguiTmpl = idTmpl.split('_').pop();
        var $audio = $('#audio-ordenar'+idguiTmpl),
        src = $(this).attr('data-audio');
        var urlingles=($('#PV_SE_URL').val()||_sysUrlBase_+'/')+'static';
        $audio.attr('src', urlingles+'/media/audio/'+src);
        try{$audio.trigger('play');}catch(error){console.log('error al escucha audio');};
    }).on('click','.plantilla-ordenar.ord_simple .btn.try-again',function(e){
    	e.preventDefault();
    	$(this).hide();
    	$plantilla=$(this).closest('.plantilla');
    	$plantilla.find('.drop').removeClass('bad good').children().removeClass('bad good');
    	$plantilla.find('.parte.blank').each(function() {
	        var value= $(this).children('div');
	        $parents = $(this).parents('.element');
	        $parents.find('.drag').append(value);
	    });
	    var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
	    var intento_actual=parseInt($plantilla.attr('data-intento')||1);
  		intento_actual=intento_actual+1;				
  		if(intentos_total<=intento_actual){
      		$plantilla.find('.btn.try-again').remove();
  			$plantilla.find('.save-progreso').show();
  			$plantilla.addClass('ejercicioterminado');	
		}
		$plantilla.attr('data-intento',intento_actual);
		$('#panel-intentos-dby').find('.actual').text(intento_actual);



    	/*$plantilla.find('.drop-parr>div.filled').each(function() {
	        var $this = $(this);
	        var value= $this.html();
	        var id = $this.attr('id');
	        var orden=$this.attr('data-orden');
	        var $ejercOrdenar = $this.parents('#ejerc-ordenar'+IDGUI);
	        $ejercOrdenar.find('.drag-parr').append('<div id="'+id+'" data-orden="'+orden+'">'+value+'</div>');
	        $this.html('').removeAttr('class').removeAttr('id');
	        evaluarOrdenParrafo($this, IDGUI);
	    });*/
    });

var ANS_P= {}; /* global variable of answers in ORDENAR PARRAFO template, filled in initOrdenarSimple() */
var AYUDA_P = {};
var initOrdenarParrafos = function($plantilla, LANG, ayuda){
	var idOrdenar=$plantilla.attr('id');
    var IDGUI=idOrdenar.split('_').pop();
    var LANG = LANG||'';
    ANS_P[IDGUI] = {};
    if(AYUDA_P[IDGUI]==undefined && ayuda!=undefined) { AYUDA_P[IDGUI] = ayuda; }
    var now = Date.now();
    $plantilla.find('#parrafo_0').attr('id', 'parrafo_'+now);
    $plantilla.find('#txt_parrafo_0').attr('id', 'txt_parrafo_'+now);

    $plantilla.find('.drag-parr>div').each(function() {
        var idParr = $(this).attr('id');
        var orden = $(this).attr('data-orden');
        if(idParr!=undefined)ANS_P[IDGUI][idParr] = orden;
    });
    $plantilla.find('.drop-parr>div').each(function() {
        var idParr = $(this).attr('id');
        var orden = $(this).attr('data-orden');
        if(idParr!=undefined)ANS_P[IDGUI][idParr] = orden;
    });
};
var evaluarUno = function($parrafo, IDGUI){
    var idParafo = $parrafo.attr('id');
    var index = $('#ejerc-ordenar'+IDGUI+' .drop-parr>div').index($parrafo);
    if( $parrafo.hasClass('filled') ){
        if( ANS_P[IDGUI][idParafo] == (index+1) ){
            $parrafo.addClass('good').removeClass('bad');
            return true;        
        } else {
            $parrafo.addClass('bad').removeClass('good');
            return false;
        }
    } else {
        $parrafo.removeClass('bad').removeClass('good');
        return false;
    }
};
var evaluarFinal = function(IDGUI){
    $('#ejerc-ordenar'+IDGUI+' .drop-parr>div').each(function() {
        evaluarUno( $(this), IDGUI );
        $(this).addClass('finish');
    });
};


// plantilla ordernar parrafos
$('body').on('click', '.plantilla-ordenar .ejerc-ordenar .drag-parr>div', function(e) {
        e.preventDefault();
        var $plantilla=$(this).closest('.plantilla');
        var isDBY=$plantilla.hasClass('isDBY')?true:false;
        var idTmpl = $plantilla.attr('id');
        var idguiTmpl = idTmpl.split('_').pop();
        var $this= $(this);
        var value= $this.html();
        var id = $this.attr('id');
        var orden=$this.attr('data-orden'); 
        var $ejercOrdenar = $this.parents('#ejerc-ordenar'+idguiTmpl);
        var primerVacio = $ejercOrdenar.find('.drop-parr div:empty').first();
        primerVacio.html(value).attr('data-orden',orden);
        primerVacio.addClass('filled').attr('id', id);
        $this.remove();
        if(isDBY==false){
        	var iscorrecto=evaluarUno(primerVacio, idguiTmpl);
        	if(iscorrecto) calcularBarraProgreso($plantilla);
        }else{
        	var cantVacios = $plantilla.find('.drop-parr>div:empty').length;
        	if(cantVacios==0){
        		evaluarFinal(idguiTmpl);
        		var $completados = $plantilla.find('.drop-parr div.filled');
		        var $correctos = $plantilla.find('.drop-parr div.filled.good');
		        if($completados.length==$correctos.length){
		            var arrPartes = [];
		            $completados.children().each(function(i, elem) {
		                arrPartes.push($(elem).text()+' ');
		            });
		            var textoOrdenado = arrPartes.join('. ');
		            if(textoOrdenado.length>0){ 
		            	pronunciar(textoOrdenado);
		            	calcularBarraProgreso($plantilla);
		            } /*pronunciacion.js*/
		            $plantilla.find('.btn.try-again').remove();
					$plantilla.find('.save-progreso').show();
					$plantilla.addClass('ejercicioterminado');
		        }else{
		        	$plantilla.find('.btn.try-again').show();
					$plantilla.find('.save-progreso').hide();
		        }
        	}
        }       
    }).on('click', '.plantilla-ordenar .ejerc-ordenar .drop-parr>div.filled', function(e) {
        e.preventDefault();
        $this=$(this);
        var $plantilla=$(this).closest('.plantilla');
        var isDBY=$plantilla.hasClass('isDBY')?true:false;
        if(isDBY) return false;
        var idTmpl = $plantilla.attr('id');
        var idguiTmpl = idTmpl.split('_').pop();
        var $this= $(this);
        if(!$this.hasClass('finish')){
            var value= $this.html();
            var id = $this.attr('id');
            var orden=$this.attr('data-orden');
            var $ejercOrdenar = $this.parents('#ejerc-ordenar'+idguiTmpl);
            $ejercOrdenar.find('.drag-parr').append('<div id="'+id+'" data-orden="'+orden+'">'+value+'</div>');
            $this.html('').removeClass('filled good bad').removeAttr('id');            
        }
    }).on('click','.plantilla-ordenar.ord_parrafo .btn.try-again',function(e){
    	e.preventDefault();
    	$(this).hide();
    	$plantilla=$(this).closest('.plantilla');
    	$plantilla.find('.drop-parr>div.filled').each(function() {
	        var $this = $(this);
	        var value= $this.html();
	        var id = $this.attr('id');
	        var orden=$this.attr('data-orden');
	        $plantilla.find('.drag-parr').append('<div id="'+id+'" data-orden="'+orden+'">'+value+'</div>');
	        $this.html('').removeAttr('class').removeAttr('id');	        
	    });
	    var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
	    var intento_actual=parseInt($plantilla.attr('data-intento')||1);
		intento_actual=intento_actual+1;				
		if(intentos_total<=intento_actual){
    		$plantilla.find('.btn.try-again').remove();
			$plantilla.find('.save-progreso').show();
			$plantilla.addClass('ejercicioterminado');	
		}
		$plantilla.attr('data-intento',intento_actual);
		$('#panel-intentos-dby').find('.actual').text(intento_actual);
    });



var CLAVE_ACTIVA = null;
var COLOR_SELECC = null;
var arrColores = ['blue','red', 'green', 'pink', 'orange', 'skyblue', 'purple', 'aqua'];
var desmarcarIncorrectas = function($plantilla){
	var isDBY=$plantilla.hasClass('isDBY')?true:false;
    $plantilla.find('.ficha.active').each(function() {
        if( !$(this).hasClass('corregido good') ){ 
            $(this).removeClass('active');
            if(!isDBY){ $(this).removeClass('corregido bad'); }
        }
    });
};
var corregirFicha = function($ficha){
    if( $ficha.hasClass('active') ){
        $ficha.addClass('corregido');
        var valor = $ficha.attr('data-key');
        if(CLAVE_ACTIVA==valor){ /* Match correcto */
            $ficha.removeClass('bad').addClass('good');
            var textoFicha = $ficha.text().trim();
            if(textoFicha.length>0){ pronunciar(textoFicha,false,'EN'); } /*pronunciacion.js*/
            return true;
        } else { /* Match incorrecto */
            $ficha.removeClass('good').addClass('bad');           
            return false;
        }
    } else {
        $ficha.removeClass('corregido').removeClass('bad').removeClass('good');
        return false;
    }
};
// plantilla de fichas.
$('body').on('click', '.plantilla-fichas .ejerc-fichas .ficha', function(e) {          
            e.stopPropagation();
            var $tpl=$(this).closest('.plantilla');
            var isDBY=$tpl.hasClass('isDBY')?true:false;          
            if($(this).parents('.partes-1').length!=0){
                var seleccionado = $(this).attr('data-key');
                if(CLAVE_ACTIVA != seleccionado){
                    CLAVE_ACTIVA = seleccionado;
                    COLOR_SELECC = $(this).attr('data-color');
                    desmarcarIncorrectas($tpl);
                    $('.partes-1 .ficha[data-key="'+CLAVE_ACTIVA+'"]').addClass('active');
                } else {
                    $(this).removeClass('active');
                    desmarcarIncorrectas($tpl);
                    CLAVE_ACTIVA = null;
                    COLOR_SELECC = null;
                }
            }
            if( $(this).parents('.partes-2').length!=0 ){
                e.stopPropagation();
                if(CLAVE_ACTIVA!=null){
                    if( !$(this).hasClass('corregido') ){
                        $(this).toggleClass('active');
                        var $this = $(this);
                        var iscorrecto=corregirFicha($this);
                        if(iscorrecto==true)calcularBarraProgreso($tpl);                        
                        $(this).attr('data-color', COLOR_SELECC);
                        $('.partes-1 .ficha.active').removeClass('active');
                        CLAVE_ACTIVA = null;
                        COLOR_SELECC = null;
                    } else if(isDBY){
                        mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar, 'warning');
                    }
                }
            }
            var cantTodos = $tpl.find('.partes-2 .ficha').length;
	        var cantCorrectos = $tpl.find('.partes-2 .ficha.corregido.good').length;
	        var cantbad = $tpl.find('.partes-2 .ficha.corregido.bad').length;
	        var rtp=validarsicontinua($tpl,cantTodos,cantCorrectos,cantbad,isDBY);
	        console.log(cantTodos,cantCorrectos,cantbad,isDBY);
	        if(rtp==false && isDBY==true && cantbad >0){
	        	$tpl.find('.btn.try-again').show();
	        }
    }).on('click', '.plantilla-fichas .ejerc-fichas .ficha a', function(e) {            
        e.preventDefault();
        e.stopPropagation();
        var $plantilla=$(this).parents('.plantilla-fichas');
        var idgui=$plantilla.attr('data-idgui');
        var $audio = $('#audio-ejercicio'+idgui),
        src = $(this).attr('data-audio');
        var urlingles=($('#PV_SE_URL').val()||_sysUrlBase_+'/')+'static';
        $audio.attr('src', urlingles +'/media/audio/'+src);
        $audio.trigger('play');
        $(this).closest('.ficha').trigger('click');
    }).on('click','.plantilla-fichas .btn.try-again',function(e){
    	e.preventDefault();
    	$(this).hide();
    	$plantilla=$(this).closest('.plantilla');
    	$plantilla.find('.ficha').each(function() {
	        //if(!$(this).hasClass('corregido good')){ 
	            $(this).removeClass('active');
	            $(this).removeClass('corregido bad good');
	        //}
	    });
	    var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
	    var intento_actual=parseInt($plantilla.attr('data-intento')||1);
		intento_actual=intento_actual+1;				
		if(intentos_total<=intento_actual){
    		$plantilla.find('.btn.try-again').remove();
			$plantilla.find('.save-progreso').show();
			if(!$plantilla.hasClass('ejercicioterminado'))$plantilla.addClass('ejercicioterminado');
		}
		var ii=(intentos_total<intento_actual?intentos_total:intento_actual);
		$plantilla.attr('data-intento',ii);
		$('#panel-intentos-dby').find('.actual').text(ii);
    });

// plantilla verdaero y falso. 
$('body').on('change','.plantilla-verdad_falso .list-premises .options input[type="radio"].radio-ctrl', function(e){
            e.stopPropagation();
            var $this=$(this);
            var $plantilla=$(this).closest('.plantilla');
            var isDBY =$plantilla.hasClass('isDBY')?true:false;
            var $opciones=$this.closest('.options');
            var name = $this.attr('name');
            var value = $opciones.find('input[name="'+name+'"].radio-ctrl:checked').val(); 
            if(isDBY){
            	var iscorregido=$opciones.attr('corregido')||false;
            	if(iscorregido!=false){
            		if(iscorregido!=value){
            			$this.removeAttr('checked');
            			$opciones.find('input.radio-ctrl',$plantilla).not($this).trigger('click');
            		}
            		return false;
            	}            	
            } 
                $opciones.find('input.radio-ctrl',$plantilla).not($this).removeAttr('checked');
                $this.attr('checked','checked');
            var correcto=evaluarRsptaVoF($this,$plantilla,isDBY);
            $opciones.attr('corregido',$this.val());
            if(correcto){            	
            	calcularBarraProgreso($panel);
            }

            var cantTodos = $plantilla.find('.list-premises .premise').length;
            var cantCorrectos = $plantilla.find('.list-premises *[data-corregido="good"]').length;
            if(cantTodos==cantCorrectos && correcto){
            	$plantilla.find('.btn.try-again').remove();				
				$plantilla.find('.save-progreso').show();
				$plantilla.addClass('ejercicioterminado');
            }else if(isDBY && !correcto){
            	$plantilla.find('.btn.try-again').show();				
				$plantilla.find('.save-progreso').hide();
            }
    }).on('mouseup', '.plantilla-verdad_falso .list-premises .options label', function(e){ 
        e.stopPropagation(); 
        var $plantilla=$(this).closest('.plantilla');
        var isDBY =$plantilla.hasClass('isDBY')?true:false;           
        if(isDBY){ 
           	var $sibling = $(this).siblings('label').find('input[type="radio"]');
            if($sibling.is(':checked')){                	
                mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar, 'warning');
                return false;
            }
        }              
    }).on('click','.plantilla-verdad_falso .btn.try-again',function(ev){
    	$(this).hide(); 
    	var $plantilla=$(this).closest('.plantilla');
    	$plantilla.find('.list-premises .options').removeAttr('corregido');
    	$plantilla.find('.list-premises .icon-result').removeAttr('data-corregido').html('');
    	$plantilla.find('.list-premises input.radio-ctrl').removeAttr('checked');
    	var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
	    var intento_actual=parseInt($plantilla.attr('data-intento')||1);
		intento_actual=intento_actual+1;
		$plantilla.attr('data-intento',intento_actual);
		$('#panel-intentos-dby').find('.actual').text(intento_actual);	
		if(intentos_total==intento_actual){
    		$plantilla.find('.btn.try-again').remove();
			$plantilla.find('.save-progreso').show();
			$plantilla.addClass('ejercicioterminado');	
		}
    });

// plantilla completar  isdrop
$('body').on('drag','.aquicargaplantilla[data-addclass="isdrop"] .panelAlternativas span.isdragable' ,function(e){
	  	var _el=$(this);
	  	if(_el.hasClass('disabled')==true){
	    	return false;               
	  	} 
	}).on('click','.aquicargaplantilla[data-addclass="isdrop"] .btn.try-again',function(ev){
    	$(this).hide();
    	var $plantilla=$(this).closest('.plantilla');
    	$plantilla.find('.tpl_alternatives').show();
    	$plantilla.find('input').each(function(i,input){
    		$(input).attr('value','').removeAttr('idtexto').removeClass('good bad disabled').val('');
    		$(input).parent().removeAttr('corregido').attr('data-corregido','');
    		$(input).siblings('.inner-icon').remove();
    	})
    	$plantilla.find('.panelAlternativas span').removeClass('disabled good bad active');
    	aumentarIntentos($plantilla);
    })

// plantilla completar isclicked
$('body').on('click', '.aquicargaplantilla[data-addclass="isclicked"] .panelAlternativas ul.alternativas-list li.alt-item a', function(e){
        e.preventDefault();
        e.stopPropagation();
        var $plantilla=$(this).closest('.plantilla');
        var isDBY=$plantilla.hasClass('isDBY')?true:false;
        if($plantilla.find('.btn.try-again').is(':visible')){
        	mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar, 'warning');
        	return false;
        }

        var _this = this;                                        
        var seleccionada = $(this).text();
        $('ul.alternativas-list li.alt-item a', $plantilla).each(function() {
            if( $(this).parent().hasClass('wrapper') ){
                $(this).siblings('span.inner-icon').remove();
                $(this).unwrap();
            }
        });
        var $inpClicked = $plantilla.find('input.isclicked[data-mce-object="input"]'); 
        var arrRspta = seleccionada.split(' - ');
        var i=0
        $inpClicked.each(function() {
        	let txtrespuesta=arrRspta[i];
        	if(($(this).val()=='' && isDBY) || isDBY==false ){
		    	$(this).val(txtrespuesta);
		    	$(this).attr('value', txtrespuesta);
		    	inputRespuesta($(this),txtrespuesta);
			}
		    i++;
		 });
        var termino=revisarplantillas($plantilla,isDBY);  
        if(termino==false && isDBY)
        	$plantilla.find('.btn.try-again').show();
        else calcularBarraProgreso($plantilla);
    }).on('click','.aquicargaplantilla[data-addclass="isclicked"] .btn.try-again',function(ev){
    	$(this).hide();
    	var $plantilla=$(this).closest('.plantilla');
        //var isDBY=$plantilla.hasClass('isDBY')?true:false;
        $plantilla.find('.tpl_alternatives').show();
    	$plantilla.find('input').each(function(i,input){
    		$(input).attr('value','').removeAttr('idtexto').removeClass('good bad').val('');
    		$(input).parent().removeAttr('corregido').attr('data-corregido','');
    		$(input).siblings('.inner-icon').remove();
    		$(input).parent().siblings('.inner-icon').remove();
    		$plantilla.find('.panelAlternativas .inner-icon').remove();
    	});
    	aumentarIntentos($plantilla);
    })

// plantilla completar ischoise - seleccionar opcion
$('body').on('click',  '.aquicargaplantilla[data-addclass="ischoice"] input.mce-object-input', function(e){
    e.preventDefault();
    e.stopPropagation();	
	// if(isMobile){
    // }
    _this=$(this);
    // console.log("ischoice evento ",_this);
		var $plantilla=$(this).closest('.plantilla');
	    var isDBY=$plantilla.hasClass('isDBY')?true:false;

		if(_this.hasClass('good')) return false;
		if(isDBY && _this.val()!=''){
			return false;
		}
		_this.siblings('.nubeclick').hide();
        console.log("PASANDO2 ischoice")
    $plantilla.find('input.mce-object-input').removeClass('ejecutando');
        $plantilla.find('#'+_this.attr('id')+'nube').hide();  
    $plantilla.find('.helper-zone').hide();     
    _this.addClass('ejecutando');
    if($plantilla.find('input.mce-object-input').length>1)
      $plantilla.find('input.mce-object-input').not('.ejecutando').each(function(i,input){          
        var txtrespuesta=$(input).val();
        cerrarFlotantes($(input));
        if(txtrespuesta!=''){
                $(input).val(txtrespuesta);
          $(input).attr('value', txtrespuesta);
                var resinput=inputRespuesta($(input),txtrespuesta,false);
                if(resinput==true){
                $(input).addClass('good');
                $(input).attr('readonly','readonly');
                $plantilla.find('#'+$(input).attr('id')+'nube').hide();
                entro=true;
              }else{
                if(isDBY==false){
                  $(input).removeAttr('readonly');
                  $plantilla.find('#'+$(input).attr('id')+'nube').show();
                }else $(input).removeAttr('readonly','readonly');
              }
            }else{
              $(input).removeAttr('readonly');
              $plantilla.find('#'+$(input).attr('id')+'nube').show();
            }             
      })


		if(!_this.hasClass('open')){
	        _this.addClass('open active');
	        var id='input_'+Date.now();
	        if(!_this.parents().hasClass('input-flotantes wrapper')){
			    _this.wrap('<div class="input-flotantes wrapper" id="wrapper-'+id+'"></div>')
			}
			if(isMobile.getBrowserInfo().navegador == 'Safari'){
			    setTimeout(function(){_this.focus();},100);
			}
	        if( _this.hasClass('isayuda')){
	        	var posicion = _this.position();
  					var ancho = _this.outerWidth();
  					var top = _this.outerHeight() * (-1);
  					var estilos = ' width:'+ancho+'px; min-width: 150px; left:'+posicion.left+'px; top: '+top+'px; ' ;
  					var html = '<div class="helper-zone" id="help-'+id+'" style="'+estilos+'">'+
	              	'<a href="#" class="btn-help" style="padding:5px 0px!important;"><i class="fa fa-question-circle"></i>&nbsp;Help!</a>'+
	              	'</div>';
	  			_this.before(html);	
	        }
	        if( _this.hasClass('ischoice')){
	        	var opc = _this.attr('data-options')||'';
			  	var opc_corr = _this.attr('data-texto');
			  	var list = '';

			    var arr_opc = _isJson(opc)?JSON.parse(opc):opc.split(',');
			    var aleat = Math.floor(Math.random()*arr_opc.length);
			    $.each(arr_opc, function(key, val){
			    	if(aleat==key){
			    		list += '<li class="opt-item"><a href="#">'+opc_corr+'</a></li>';
			    	}
			      	list += '<li class="opt-item"><a href="#">'+val.trim()+'</a></li>';
			    });

			  	var posicion = _this.position();
					var ancho = _this.outerWidth();
					var top = _this.outerHeight() * (-1);
					var estilos = ' width:'+ancho+'px; min-width: 150px; left:'+posicion.left+'px; ' ;
			  	var html_opt = '<ul class="options-list" id="opts-'+id+'" style="'+estilos+'">'+ list + '</ul>';
			  	_this.after(html_opt);
	        }			        
	        _this.attr('readonly', 'readonly');			        
	    }
	}).on('click', '.aquicargaplantilla[data-addclass="ischoice"] ul.options-list>li>a', function(e){
        e.preventDefault();
        e.stopPropagation();
        var _this = this;
        var $plantilla=$(this).closest('.plantilla');
	      var isDBY=$plantilla.hasClass('isDBY')?true:false;
        var txtrespuesta = $(_this).text();
        var input = $(_this).parents('ul.options-list').siblings('input.open');
        input.val(txtrespuesta);
		    input.attr('value', txtrespuesta);
        cerrarFlotantes(input);
        var resinput=inputRespuesta(input,txtrespuesta);        
        var cantTodos = $plantilla.find('.panelEjercicio input.mce-object-input').length;
		    var cantCorrectos = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="good"]').length
		    var cantbad = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="bad"]').length
        var continua=validarsicontinua($plantilla,cantTodos,cantCorrectos,cantbad,isDBY)
        if(continua==false || resinput==true){
        	input.addClass('good');
            calcularBarraProgreso($plantilla);
        }
    }).on('click', '.aquicargaplantilla[data-addclass="ischoice"] div.helper-zone>a.btn-help', function(e){
        e.preventDefault();
        e.stopPropagation();
        var $inputMCE = $(this).parent().siblings('input[data-mce-object="input"]');
        if( $(this).find('span.tip-word').length==0 ){
            $(this).append('<span class="tip-word"> : </span>');
        }
        var rspta= $inputMCE.attr('data-texto');
        var arr_rspta= rspta.split("");

        var i = $inputMCE.siblings().find('span.tip-word').attr('data-pulsado');
        if(i==''||i==undefined){
            i = 0;
        }
        i = parseInt(i);
        if( i < arr_rspta.length){
            $inputMCE.siblings().find('span.tip-word').attr('data-pulsado',(i+1));
            var texto = $inputMCE.siblings().find('span.tip-word').text();
            texto = texto.concat(arr_rspta[i]);
            $inputMCE.siblings().find('span.tip-word').text(texto);
        }
        //$inputMCE.focus();
    }).on('click','.aquicargaplantilla[data-addclass="ischoice"] .btn.try-again',function(ev){
    	$(this).hide();
    	var $plantilla=$(this).closest('.plantilla');
    	$plantilla.find('input').each(function(i,input){
    		$(input).attr('value','').removeAttr('idtexto').removeClass('good bad').val('');
    		$(input).parent().removeAttr('corregido').attr('data-corregido','');
    		$(input).siblings('.inner-icon').remove();
    		$(input).parent().siblings('.inner-icon').remove();     		
    	})    	
    	//$plantilla.find('.panelAlternativas .inner-icon').remove();    	
    	aumentarIntentos($plantilla);
    	addclickaqui($plantilla);
    }).on('click','.aquicargaplantilla[data-addclass="ischoice"] .plantilla',function(e){
        e.preventDefault();
        e.stopPropagation();
        var $plantilla=$(this);
      $plantilla.find('input.mce-object-input.ejecutando').each(function(i,input){  
      var txtrespuesta=$(input).val();
        if(txtrespuesta!=''){
            $(input).val(txtrespuesta);
            $(input).attr('value', txtrespuesta);
            var resinput=inputRespuesta($(input),txtrespuesta);
            if(resinput==true){
                $(input).addClass('good');
                $plantilla.find('#'+$(input).attr('id')+'nube').hide();
            }else {
                $plantilla.find('#'+$(input).attr('id')+'nube').show();                 
            }
        }else {
            $plantilla.find('#'+$(input).attr('id')+'nube').show();
        }
        $(input).removeClass('ejecutando');
        cerrarFlotantes($(input));
      })
    })

// plantilla completar iswrite - seleccionar opcion
$('body').on('click',  '.aquicargaplantilla[data-addclass="iswrite"]  input.mce-object-input', function(e){
		e.preventDefault();
		e.stopPropagation();
		var $plantilla=$(this).closest('.plantilla');
		var isDBY=$plantilla.hasClass('isDBY')?true:false;				
		_this=$(this);				
		$plantilla.find('input.mce-object-input').removeClass('ejecutando');
		$plantilla.find('#'+_this.attr('id')+'nube').hide();	
		$plantilla.find('.helper-zone').hide();			
		_this.addClass('ejecutando');
		var entro=false;
		if($plantilla.find('input.mce-object-input').length>1)
			$plantilla.find('input.mce-object-input').not('.ejecutando').each(function(i,input){					
				var txtrespuesta=$(input).val();
				cerrarFlotantes($(input));
				if(txtrespuesta!=''){
	            	$(input).val(txtrespuesta);
					$(input).attr('value', txtrespuesta);
	            	var resinput=inputRespuesta($(input),txtrespuesta,false);
	            	if(resinput==true){
		        		$(input).addClass('good');
		        		$(input).attr('readonly','readonly');
		        		$plantilla.find('#'+$(input).attr('id')+'nube').hide();
		        		entro=true;
		        	}else{
		        		if(isDBY==false){
		        			$(input).removeAttr('readonly');
		        			$plantilla.find('#'+$(input).attr('id')+'nube').show();
		        		}else $(input).removeAttr('readonly','readonly');
		        	}
		        }else{
		        	$(input).removeAttr('readonly');
		        	$plantilla.find('#'+$(input).attr('id')+'nube').show();
		        }			        
			})
		if(entro==true){calcularBarraProgreso($plantilla);};
		if(_this.hasClass('good')) return false;
		if(_this.hasClass('vienedehelp')){
			_this.removeClass('vienedehelp');
			return false;
		}
		_this.on('blur');
		$plantilla.find('#'+_this.attr('id')+'nube').hide();
		$plantilla.find('.nubeclick').not('#'+_this.attr('id')+'nube').show();
		if(!_this.hasClass('open')){
	        _this.addClass('open active');
	        var id='input_'+Date.now();
	        try{
	        	if(!_this.parents().hasClass('input-flotantes wrapper')){
			    	_this.wrap('<div class="input-flotantes wrapper" id="wrapper-'+id+'"></div>')
				}
			}catch(ex){}
			/*if(isMobile.getBrowserInfo().navegador == 'Safari'){
			    setTimeout(function(){$(inputMCE).focus();},100);
			}*/
	        if( _this.hasClass('isayuda')){
	        	var posicion = _this.position();
					var ancho = _this.outerWidth();
					var top = _this.outerHeight() * (-1);
					var estilos = ' width:'+ancho+'px; min-width: 150px; left:'+posicion.left+'px; top: '+top+'px; ' ;
					var html = '<div class="helper-zone" id="help-'+id+'" style="'+estilos+'">'+
	              	'<a href="#" class="btn-help" style="padding:5px 0px!important;"><i class="fa fa-question-circle"></i>&nbsp;Help!</a>'+
	              	'</div>';
      			_this.before(html);	
	        }			    	
	    }
	    _this.focus();
	}).on('click', '.aquicargaplantilla[data-addclass="iswrite"]  div.helper-zone>a.btn-help', function(e){
        e.preventDefault();
        e.stopPropagation();
        var $inputMCE = $(this).parent().siblings('input[data-mce-object="input"]');
        $inputMCE.off('blur');
        $inputMCE.addClass('vienedehelp');
        if( $(this).find('span.tip-word').length==0 ){
            $(this).append('<span class="tip-word"> : </span>');
        }
        var rspta= $inputMCE.attr('data-texto');
        var arr_rspta= rspta.split("");

        var i = $inputMCE.siblings().find('span.tip-word').attr('data-pulsado');
        if(i==''||i==undefined){
            i = 0;
        }
        i = parseInt(i);
        if( i < arr_rspta.length){
            $inputMCE.siblings().find('span.tip-word').attr('data-pulsado',(i+1));
            var texto = $inputMCE.siblings().find('span.tip-word').text();
            texto = texto.concat(arr_rspta[i]);
            $inputMCE.siblings().find('span.tip-word').text(texto);
        }
        $inputMCE.focus();
  }).on('blur', '.aquicargaplantilla[data-addclass="iswrite"]  input.mce-object-input', function(e){
      e.preventDefault();
      var input = $(this); 
      var $plantilla=$(this).closest('.plantilla');
	    var isDBY=$plantilla.hasClass('isDBY')?true:false;
      txtrespuesta=input.val()||'';
      if(txtrespuesta=='') return false;
      input.val(txtrespuesta);
	    input.attr('value', txtrespuesta);
      var resinput=inputRespuesta(input,txtrespuesta);
      if(resinput==true){                	
      	input.addClass('good');
      	$plantilla.find('#'+$(input).attr('id')+'nube').hide();
      	input.attr('readonly','readonly');
          calcularBarraProgreso($plantilla);
          cerrarFlotantes(input);
          revisarplantillas($plantilla,isDBY);	                
      }
      if(isDBY) input.attr('readonly','readonly');        
      var termino=revisarplantillas($plantilla,isDBY);  
      if(termino==false && isDBY)
         $plantilla.find('.btn.try-again').show();
  }).on('keyup', '.aquicargaplantilla[data-addclass="iswrite"]  input.mce-object-input', function(e){
      var _this = $(this);
      _this.off('blur');
      if(e.which != 13){
          var value = _this.val().trim();
          var rspta_crrta = _this.attr('data-texto');
          if(value.length>0){
          	if(value==rspta_crrta){ 
          	    _this.removeClass('ejecutando');           		
		        _this.on('blur').trigger('blur');
          	}                                               
          }
      }else{                    
         _this.on('blur').trigger('blur');
      }                               
  }).on('click','.aquicargaplantilla[data-addclass="iswrite"]  .plantilla',function(e){
  	var $plantilla=$(this);
  	$plantilla.find('input.mce-object-input.ejecutando').each(function(i,input){	
		var txtrespuesta=$(input).val();
		if(txtrespuesta!=''){
          	$(input).val(txtrespuesta);
			$(input).attr('value', txtrespuesta);
          	var resinput=inputRespuesta($(input),txtrespuesta);
          	if(resinput==true){
        		$(input).addClass('good');
        		$plantilla.find('#'+$(input).attr('id')+'nube').hide();
        	}else {
        		$plantilla.find('#'+$(input).attr('id')+'nube').show();			        		
        	}
        }else {
        	$plantilla.find('#'+$(input).attr('id')+'nube').show();
        }
        $(input).removeClass('ejecutando');
		cerrarFlotantes($(input));
	})
  }).on('click','.aquicargaplantilla[data-addclass="iswrite"]  .btn.try-again',function(ev){
  	$(this).hide();
  	var $plantilla=$(this).closest('.plantilla');
  	$plantilla.find('input').each(function(i,input){
  		$(input).attr('value','').removeAttr('idtexto').removeClass('good bad').val('');
  		$(input).parent().removeAttr('corregido').attr('data-corregido','');
  		$(input).siblings('.inner-icon').remove();
  		$(input).parent().siblings('.inner-icon').remove();
  		$(input).removeAttr('readonly');            		
  	})      
  	addclickaqui($plantilla);      	
  	aumentarIntentos($plantilla);
  })

// plantilla speach
$('body').on('click','.plantilla-speach .pnl-speach .btnPlaytexto',function(ev){
	    ev.preventDefault();
	    if(!$(this).hasClass('inwave')){
	        var pnlspeach=$(this).closest('.pnl-speach');
	        speachpronunciar(pnlspeach);
	    }
    }).on('click','.plantilla-speach .pnl-speach.alumno .btnGrabarAudio',function(ev){
      ev.preventDefault();
      var pnlspeach=$(this).closest('.pnl-speach');
      var btn=$(this);
      if(btn.hasClass('disabled')) return false;
        btn.addClass('disabled');
      var pnl=btn.closest('.pnl-speach');
      var idgui=pnl.attr('data-idgui');
      var $plantilla=$(this).closest('.plantilla');
		  var isDBY=$plantilla.hasClass('isDBY')?true:false;
      $plantilla.find('audio').each(function(){
          this.pause(); // Stop playing          
      }); 
        if(btn.hasClass('btn-danger')){        
          if(speachonline){
            //btn.removeClass('btn-danger').addClass('btn-success').children('i').removeClass('fa fa-microphone-slash ').addClass('fa fa-microphone animated infinite zoomIn');
            btn.siblings('.btnPlaytexto').addClass('disabled').attr('disabled',true);
          }
          speach_iniciargrabacion(pnl,idgui);
        }else{
            btn.addClass('btn-danger').removeClass('btn-success disabled').children('i').removeClass('fa fa-microphone animated infinite zoomIn').addClass('fa fa-microphone-slash');
            speach_detenergrabacion(pnl,idgui);
            if(isDBY){
            	btn.hide();
            	var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
      				var intento_actual=parseInt($plantilla.attr('data-intento')||1);
      				if(intentos_total==intento_actual){
      					$plantilla.find('.save-progreso').show();
      					$plantilla.addClass('ejercicioterminado');
      					$plantilla.find('.btn.try-again').hide();
      				}else
            	$plantilla.find('.btn.try-again').show();
            }
            btn.siblings('.btnPlaytexto').removeClass('disabled').attr('disabled',false).removeClass('hidden');
            /*var DBYgrabar=$(this).siblings('.btnAccionspeachdby');
            if(DBYgrabar.length){
                btn.addClass('disabled').attr('disabled',true).addClass('hide');
            }
            if(pnlspeach.closest('#met-2').length) btn.removeClass('disabled').removeAttr('disabled').removeClass('hide');*/
        }
    }).on('click','.plantilla-speach .btn.try-again',function(ev){
        	$(this).hide(); 
        	var $plantilla=$(this).closest('.plantilla');
        	$plantilla.find('.btnGrabarAudio').show().removeClass('disabled hide').removeAttr('disabled').css('display','inline-block');
        	//$plantilla.find('.msjRespuesta.grabarfile').hide(); 
        	//$plantilla.find('.save-progreso').show();
        	var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
			 var intento_actual=parseInt($plantilla.attr('data-intento')||1);
			 intento_actual=intento_actual+1;
			 $plantilla.attr('data-intento',intento_actual);
			 $('#panel-intentos-dby').find('.actual').text(intento_actual);	
			 if(intentos_total==intento_actual){
	    	$plantilla.find('.btn.try-again').remove();
				$plantilla.find('.save-progreso').show();
				$plantilla.addClass('ejercicioterminado');	
			 }
    }).on('click','.plantilla-speach .btncalcular',function(ev){
    	var $plantilla=$(this).closest('.plantilla');
    	var isDBY=$plantilla.hasClass('isDBY')?true:false;
    	var puntaje=$plantilla.find('.textohablado > .speachtexttotal');
    	var cantTotal=parseInt(puntaje.attr('total-palabras')||0);
    	var cantCorrectos=parseInt(puntaje.attr('total-ok')||0);
    	if(cantTotal>0){
    		calcularBarraProgreso($plantilla);
    		if(cantTotal==cantCorrectos && isDBY){
    			$plantilla.find('.btn.try-again').hide();				
		        $plantilla.find('.save-progreso').show();
		        $plantilla.hasClass('ejercicioterminado');
    		}
    	}
    })

// plantilla plantilla-nlsw
$('body').on('click','.plantilla-nlsw .btnadd_recordAlumno',function(ev){
      ev.preventDefault(); 
      var _el=$(this);
      if(_el.hasClass('disabled')) return false;
      _el.addClass('disabled');
      var $plantilla=$(this).closest('.plantilla');
	    var isDBY=$plantilla.hasClass('isDBY')?true:false;	
      var grabando=$(this).children('i');
      var idgui=$(this).attr('data-idgui');
      var rptAlumno=$(this).closest('.plantilla-nlsw').children('.respuestaAlumno');	        
      var txtrespuesta=rptAlumno.attr('data-texto')||'';
      var hayrespuesta=txtrespuesta.trim()==''?false:true;
      if(grabando.hasClass('fa-microphone-slash')){         
          let timen=3;
          var txtini=_el.text();
          _el.removeClass('btn-success').addClass('btn-danger').html('<i class="fa fa-microphone-slash "></i> '+timen);
          var timerInterval = setInterval(() => {
              if(timen==0){
                  clearInterval(timerInterval);
                  _el.html('<i class="fa fa-microphone animated infinite zoomIn"></i> '+txtini);
                  _el.addClass('btn-success').removeClass('btn-danger');
                  _el.removeClass('disabled');
              }else {
                _el.removeClass('btn-success').addClass('btn-danger').html('<i class="fa fa-microphone-slash "></i> '+timen);                
              }             
              timen--;
          },400);                    
          $plantilla.find('.save-progreso').hide(); 
          grabarAudio_nlsw({grabando:grabando,acc:'recordAlumno',idgui:idgui,donde:rptAlumno});
      }else{
        _el.removeClass('disabled');
      	rptAlumno.find('.grabarfile').show();
          _el.removeClass('btn-success').addClass('btn-danger');
          rptAlumno.attr('total-palabras',1);
          rptAlumno.attr('total-ok',1);
          if(isDBY){
          	var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
		    var intento_actual=parseInt($plantilla.attr('data-intento')||1);
		    if(intento_actual<intentos_total){
		    	$plantilla.find('.btn.try-again').show();
		    }	            	
          	_el.hide();
          }
          $plantilla.find('.save-progreso').show();          
          stopAudio_nlsw({grabando:grabando});
          rptAlumno.trigger('calcularprogreso');
      }   
    }).on('click','.plantilla-nlsw .btn.try-again',function(ev){
    	$(this).hide(); 
    	var $plantilla=$(this).closest('.plantilla');       
    	$plantilla.find('.btnadd_recordAlumno').show();
    	$plantilla.find('.msjRespuesta.grabarfile').hide(); 
    	$plantilla.find('.save-progreso').show();
    	var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
		var intento_actual=parseInt($plantilla.attr('data-intento')||1);
		intento_actual=intento_actual+1;
		$plantilla.attr('data-intento',intento_actual);
		$('#panel-intentos-dby').find('.actual').text(intento_actual);	
		if(intentos_total==intento_actual){
    		$plantilla.find('.btn.try-again').remove();
			$plantilla.find('.save-progreso').show();
			$plantilla.addClass('ejercicioterminado');	
		}
    })


var validarsicontinua=function($ptl,ct,cb,cm,isDBY){
	var intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
	var intento_actual=parseInt($ptl.attr('data-intento')||1);
	if(($ptl.hasClass('ejercicioterminado') && ct==cb)||(ct==cb)||(intentos_total<=intento_actual)){ 
    	$ptl.find('.btn.try-again').remove();				
		$ptl.find('.save-progreso').show();
		if(!$ptl.hasClass('ejercicioterminado'))$ptl.addClass('ejercicioterminado');
		if(intentos_total<intento_actual && isDBY )$ptl.attr('data-intento',intentos_total);
		return false; // no continua
    }else if(isDBY){     		
		if(cm>0){
			$ptl.find('.btn.try-again').show();				
		    $ptl.find('.save-progreso').hide();
		    // si continua
		}        	
    }
     return true;
}

var revisarplantillas=function($plantilla,isDBY){
	var termino=false
	if($plantilla.hasClass('plantilla-completar')){
		var cantTodos = $plantilla.find('.panelEjercicio input.mce-object-input').length;
        var cantCorrectos = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="good"]').length;
        var cantbad = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="bad"]').length;
        if(cantTodos==cantCorrectos){
    		$plantilla.find('.panelAlternativas').parent().hide();
    		$plantilla.find('.btn.try-again').remove();
			$plantilla.find('.save-progreso').show();
			$plantilla.addClass('ejercicioterminado');
			termino=true;
    	}else if(cantbad==cantTodos){
    		if(isDBY){
    			$plantilla.find('.panelAlternativas').children('span').addClass('disabled');
    		}else{
    			$plantilla.find('.panelAlternativas').children('span').not('.good').removeClass('disabled');
    		}
    	}else if(cantTodos==cantbad+cantCorrectos){
    		if(isDBY){
    			$plantilla.find('.panelAlternativas').children('span').addClass('disabled');
    		}else{
    			$plantilla.find('.panelAlternativas').children('span').not('.good').removeClass('disabled');
    		}        	
    	}
	}
	return termino;
}




var plantillaejercicios=function($plantilla,isDBY=false){
	var intentos_total=999;
	var intento_actual=parseInt($plantilla.attr('data-intento')||1);
	if(isDBY==false){
		intentos_total = parseInt($('#panel-intentos-dby').find('.total').text());
		$plantilla.removeClass('isDBY');
		$('#panel-intentos-dby').hide();
	}else{
		$('#panel-intentos-dby').find('.actual').text(intento_actual);
		$('#panel-intentos-dby').show();
		$plantilla.addClass('isDBY');
	}		
	var tpl=$plantilla.closest('.aquicargaplantilla').attr('data-tpl')||'';
	$panel=$plantilla.closest('.content-smartic.active');
	calcularBarraProgreso($panel);
	if(isDBY==false){
		$plantilla.find('.btn.try-again').hide();
    	$plantilla.find('.save-progreso').show();
    }
	if(tpl==='practice_verdad_falso'||tpl=='dby_verdad_falso'){
		var cantTodos = $plantilla.find('.list-premises .premise').length;
        var cantCorrectos = $plantilla.find('.list-premises *[data-corregido="good"]').length;
		if(isDBY)
		$plantilla.find('input.radio-ctrl:checked').each(function(i,v){
			$(v).closest('.options').attr('corregido',$(v).val())
		})
		var cantbad = $plantilla.find('.icon-result[data-corregido="bad"]').length;
	    validarsicontinua($plantilla,cantTodos,cantCorrectos,cantbad,isDBY);
	}else if(tpl=='practice_imagen_puntos'||tpl=='dby_imagen_puntos'){		
		var cantTodos = $plantilla.find('.mask-dots .dot-container .dot-tag').length;
        var cantCorrectos = $plantilla.find('.mask-dots .dot-container .dot-tag.good').length; 
        var cantbad = $plantilla.find('.mask-dots .dot-container .dot-tag.bad').length;
        validarsicontinua($plantilla,cantTodos,cantCorrectos,cantbad,isDBY);   
	}else if(tpl=='pra_arrastre_texto'||tpl=='dby_multiplantilla'){
		var cantTodos = $plantilla.find('.panelEjercicio input.mce-object-input').length;
    var cantCorrectos = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="good"]').length;
    var cantbad = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="bad"]').length;
    var continua=validarsicontinua($plantilla,cantTodos,cantCorrectos,cantbad,isDBY);    
		var tipotpl=$plantilla.closest('.aquicargaplantilla').attr('data-addclass')||'';		
		if(continua==false && isDBY ){
			$plantilla.find('.panelAlternativas').parent().hide();
			$plantilla.find('input').attr('readonly','readonly');
			$plantilla.find('.nubeclick').remove();
			$plantilla.find('input').not('.good').each(function(i,input){
				inputRespuesta($(input),'no',false);
			})
		}  	
		if(tipotpl=='isdrop'){
			$plantilla.find('.panelAlternativas span').removeClass('hidden').show();
			$plantilla.find('input').attr('readonly','readonly');
			$('.isdragable',$plantilla).draggable({
        start:function(event,ui){
          	obj=ui.helper;  
				    var d = new Date();     
				    var id="idtmp"+d.getDate()+"_"+d.getMonth()+"_"+d.getFullYear()+''+d.getHours()+''+d.getMinutes()+''+d.getSeconds();
				    $(obj).addClass('active').attr('id',id).attr('idtexto',$(obj).attr('idtexto')||id);
        },
        stop:function(event,ui){
           	obj=ui.helper;    
				    $(obj).closest('.tpl_plantilla').find('.isdragable').removeClass('active');
				    $(obj).removeAttr('id').attr('style','position:relative'); 
                }
      });
      $('.isdrop',$plantilla).droppable({
        drop:function(event,ui){
            	$plantilla=$(this).closest('.plantilla');
            	var intento_actual=parseInt($plantilla.attr('data-intento')||1);
            	//console.log(intento_actual,$(this).val());
            	if($(this).hasClass('good')|| $(this).hasClass('disabled') ) return false;
            	var dragable=ui.draggable;
    				  var txtrespuesta=$(dragable).text().trim();
    				  $(this).val(txtrespuesta);
			        $(this).attr('value', txtrespuesta).attr('idtexto',$(dragable).attr('idtexto'));    
    				//bloqueadropable($plantilla);
			        var resinput=inputRespuesta($(this),txtrespuesta);			        
			        if(resinput==true){
			        	$(dragable).addClass('good');
			        	$(this).addClass('good');
   	                 	calcularBarraProgreso($plantilla);
			        }
              if(isDBY) $(this).addClass('disabled');
			        $(dragable).addClass('disabled');
			        var cantTodos = $plantilla.find('.panelEjercicio input.mce-object-input').length;
        			var cantCorrectos = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="good"]').length;
        			var cantbad = $plantilla.find('.panelEjercicio .inp-corregido[data-corregido="bad"]').length;

			        var continua=validarsicontinua($plantilla,cantTodos,cantCorrectos,cantbad,isDBY); 
              var inactivos=$plantilla.find('.panelAlternativas').children('span.disabled').not('.good').length;
			        if((cantTodos == cantbad+cantCorrectos && isDBY) || cantTodos==cantCorrectos){
			        	$plantilla.find('.panelAlternativas').parent().hide();			        	
			        }else if((cantTodos == cantbad+cantCorrectos && isDBY==false && cantTodos >1) || ((inactivos+ cantCorrectos) == cantTodos && isDBY==false )){
			        	$plantilla.find('.panelAlternativas').children('span.disabled').removeClass('disabled');
			        }
		    	}
      });
		}else if(tipotpl=='isclicked'){
			$plantilla.find('input').attr('readonly','readonly');
      if(continua && isDBY){
        if(cantTodos==cantCorrectos+cantbad){
          $plantilla.find('.btn.try-again').show();
          $plantilla.find('.save-progreso').hide();
        }
      }
		}else if(tipotpl=='ischoice'){
			$plantilla.find('.tpl_alternatives').remove();
			setTimeout(function(){addclickaqui($plantilla);},1000);
      if(continua && isDBY){
        if(cantTodos==cantCorrectos+cantbad){
          $plantilla.find('.btn.try-again').show();
          $plantilla.find('.save-progreso').hide();
        }
      }
		}else if(tipotpl=='iswrite'){
			//$plantilla.find('.nubeclick').remove();
			$plantilla.find('.tpl_alternatives').remove();
			$plantilla.find('input.mce-object-input').attr('autocomplete',false).removeClass('ejecutando');			
			$plantilla.find('input.mce-object-input').each(function(ii,input){
				var txtrespuesta=$(input).val()||'';
				if(txtrespuesta!=''){
					$(input).val(txtrespuesta);
					$(input).attr('value', txtrespuesta);
            	var resinput=inputRespuesta($(input),txtrespuesta,false);
            	if(resinput==true){
		        		$(input).addClass('good');
		        		$(input).attr('readonly','readonly');	        		
		        	}else{
		        		if(isDBY) $(input).attr('readonly','readonly');
		        		else $(input).removeAttr('readonly');		        		
		        	}
				}else{
					$(input).removeAttr('readonly');
				}
				cerrarFlotantes($(input));				
			})
			setTimeout(function(){addclickaqui($plantilla);},2000);
			$plantilla.find('input.mce-object-input').each(function(ii,input){
				if($(input).hasClass('good')) $plantilla.find('#'+$(input).attr('id')+'nube').hide();
			})
		}
	}else if(tpl=='practice_nwrite'||tpl=='dby_nwrite'){   
		var respuestaAlumno=$plantilla.find('.respuestaAlumno'); 
    if($plantilla.closest('.tabhijo').find('.calificacion-docente').length==0){
      addnewbuttonhtml(respuestaAlumno); 
    }else{
      respuestaAlumno.find('.tox-tinymce').remove();     
    }
    $plantilla.find('.save-progreso').show();
	}else if(tpl=='practice_nspeach'||tpl=='dby_nspeach'){
		var msjRespuesta=$plantilla.find('.respuestaAlumno').children('.msjRespuesta');
    $plantilla.find('.btnadd_recordAlumno').removeClass('disabled');
		var urlaudio=msjRespuesta.children('audio').attr('src')||'';
        if(urlaudio!=''){
            var index1_=urlaudio.lastIndexOf('static/');
            if(index1_>-1) urlaudio=urlaudio.substring(index1_,10000);
            urlaudio =_sysUrlBase_+'/'+urlaudio;
            msjRespuesta.children('audio').attr('src',urlaudio);            
        }
		if($plantilla.hasClass('ejercicioterminado')){
			$plantilla.find('.btn.try-again').remove();
			$plantilla.find('.btnadd_recordAlumno').hide();
			$plantilla.find('.save-progreso').show();
			return false;
		}
        var continua=validarsicontinua($plantilla,1,0,0,isDBY);
        if(continua){        
        	if(isDBY){
        		if($plantilla.find('.msjRespuesta.grabarfile').children('audio').length==0){
        			$plantilla.find('.btnadd_recordAlumno').show();
              $plantilla.find('.btnadd_recordAlumno').removeClass('btn-success').addClass('btn-danger').children('i').removeClass('fa-microphone fa-spinner').addClass('fa-microphone-slash');
        			$plantilla.find('.btn.try-again').hide();
        			$plantilla.find('.save-progreso').hide();
        		}else{
        			if(!$plantilla.find('.btnadd_recordAlumno').is(':visible')||$plantilla.find('.btnadd_recordAlumno').css('display')== 'none'||$plantilla.find('.btnadd_recordAlumno').hasClass('hide')){
        				$plantilla.find('.btn.try-again').show();
        				$plantilla.find('.save-progreso').show();
        			}else{
        				$plantilla.find('.btn.try-again').hide();
        				$plantilla.find('.save-progreso').hide();
        			}        			
        		}
        	}else	{
            $plantilla.find('.btnadd_recordAlumno').removeClass('btn-success').addClass('btn-danger').children('i').removeClass('fa-microphone fa-spinner').addClass('fa-microphone-slash');
				    $plantilla.find('.save-progreso').show();
          }
        }
	}else if(tpl=='practice_speach'||tpl=='dby_practice_speach'){
		$plantilla.find('.inpreview').removeClass('hidden');      
    $plantilla.find('.pnl-speach.editable').removeClass('editable');
    var eldoncente=$plantilla.find('.pnl-speach.docente');
          eldoncente.find('.btnPlaytexto').show();
          eldoncente.find('.btnGrabarAudio').hide();
          var audiostmp= $plantilla.find('audio');
          audiostmp.each(function(i, audiotag){
              var url=$(audiotag).attr('src');
              var donde=$(audiotag).closest('.pnl-speach');
              donde.find('.grafico').html('');
              crearonda(url,donde,true);
          });
    var elalumno=$plantilla.find('.pnl-speach.alumno');
    elalumno.find('i.fa.color-green').removeClass('fa-microphone color-green animated infinite zoomIn').addClass('fa-microphone-slash').siblings('span').text('Record');
      elalumno.find('.btnGrabarAudio').removeClass('disabled');
		if($plantilla.hasClass('ejercicioterminado') || $plantilla.find('.speachtexttotal').text()=='100%'|| intentos_total==intento_actual){
      elalumno.find('.btnPlaytexto').show();
			$plantilla.find('.btn.try-again').remove();
			elalumno.find('.btnGrabarAudio').hide();
			$plantilla.find('.save-progreso').show();			
			if(!$plantilla.hasClass('ejercicioterminado'))$plantilla.addClass('ejercicioterminado');
			return false;
		}else{
      if(elalumno.children('.grafico').children('.wavesurfer').length>0){
         if(isDBY) $plantilla.find('.btn.try-again').show();
         elalumno.find('.btnPlaytexto').show();
         $plantilla.find('.save-progreso').show();
      }else{
         if(isDBY) $plantilla.find('.btn.try-again').hide();
         elalumno.find('.btnPlaytexto').hide();
         $plantilla.find('.save-progreso').hide();
      }
      elalumno.find('.btnGrabarAudio').show();
      if($plantilla.find('.speachtexttotal').text()=='100%'){
        elalumno.find('.btnGrabarAudio').hide();
        $plantilla.find('.save-progreso').show();
      }     
		}	
	}else if(tpl=='practice_fichas'||tpl=='dby_fichas'){
		var cantTodos = $plantilla.find('.partes-2 .ficha').length;
        var cantCorrectos = $plantilla.find('.partes-2 .ficha.corregido.good').length;
        var cantbad = $plantilla.find('.partes-2 .ficha.corregido.bad').length;
        validarsicontinua($plantilla,cantTodos,cantCorrectos,cantbad,isDBY);
	}else if(tpl=='practice_ordenar_simple'||tpl=='dby_ordenar_simple'){
		var cantTodos = $plantilla.find('.drop .parte.blank').length;
        var cantCorrectos = $plantilla.find('.drop .parte.blank.good').length;
        var cantbad = $plantilla.find('.drop.bad').length;
        validarsicontinua($plantilla,cantTodos,cantCorrectos,cantbad,isDBY);        
		if(isDBY) initOrdenarSimple2($plantilla,false);
		else initOrdenarSimple2($plantilla,true);
	}else if(tpl=='practice_ordenar_parrafos'||tpl=='dby_ordenar_parrafos'){
		var cantTodos = $plantilla.find('.drop-parr div:not(.fixed)').length;
        var cantCorrectos = $plantilla.find('.drop-parr .filled.good').length;
        var cantbad = $plantilla.find('.drop-parr .filled.bad').length;
        validarsicontinua($plantilla,cantTodos,cantCorrectos,cantbad,isDBY);		
		if(isDBY) initOrdenarParrafos($plantilla,'',false);
		else initOrdenarParrafos($plantilla,'',true);       
	}
}